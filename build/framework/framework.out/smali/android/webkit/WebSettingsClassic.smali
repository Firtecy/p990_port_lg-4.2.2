.class public Landroid/webkit/WebSettingsClassic;
.super Landroid/webkit/WebSettings;
.source "WebSettingsClassic.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/WebSettingsClassic$1;,
        Landroid/webkit/WebSettingsClassic$EventHandler;,
        Landroid/webkit/WebSettingsClassic$AutoFillProfile;
    }
.end annotation


# static fields
.field private static final ACCEPT_LANG_FOR_US_LOCALE:Ljava/lang/String; = "en-US"

.field private static final DESKTOP_USERAGENT:Ljava/lang/String; = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.34 Safari/534.24"

.field private static final DOUBLE_TAP_TOAST_COUNT:Ljava/lang/String; = "double_tap_toast_count"

.field private static final IPHONE_USERAGENT:Ljava/lang/String; = "Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_0 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7A341 Safari/528.16"

.field private static final PREF_FILE:Ljava/lang/String; = "WebViewSettings"

.field private static final PREVIOUS_VERSION:Ljava/lang/String; = "4.1.1"

.field static final WEB_NOTI_PERMISSION_ALLOWED:I = 0x0

.field static final WEB_NOTI_PERMISSION_DENIED:I = 0x2

.field static final WEB_NOTI_PERMISSION_NOT_ALLOWED:I = 0x1

.field private static mDoubleTapToastCount:I

.field private static sLocale:Ljava/util/Locale;

.field private static sLockForLocaleSettings:Ljava/lang/Object;


# instance fields
.field private mAcceptLanguage:Ljava/lang/String;

.field private mAdditionalHeaders:Ljava/lang/String;

.field private mAdditionalHeadersTable:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mAllowContentAccess:Z

.field private mAllowFileAccess:Z

.field private mAllowFileAccessFromFileURLs:Z

.field private mAllowUniversalAccessFromFileURLs:Z

.field private mAppCacheEnabled:Z

.field private mAppCacheMaxSize:J

.field private mAppCachePath:Ljava/lang/String;

.field private mAutoFillEnabled:Z

.field private mAutoFillProfile:Landroid/webkit/WebSettingsClassic$AutoFillProfile;

.field private mBlockNetworkImage:Z

.field private mBlockNetworkLoads:Z

.field private mBrowserFrame:Landroid/webkit/BrowserFrame;

.field private mBuiltInZoomControls:Z

.field private mCliptrayEnabled:Z

.field private mContext:Landroid/content/Context;

.field private mCursiveFontFamily:Ljava/lang/String;

.field private mDatabaseEnabled:Z

.field private mDatabasePath:Ljava/lang/String;

.field private mDatabasePathHasBeenSet:Z

.field private mDefaultFixedFontSize:I

.field private mDefaultFontSize:I

.field private mDefaultTextEncoding:Ljava/lang/String;

.field private mDefaultZoom:Landroid/webkit/WebSettings$ZoomDensity;

.field private mDisplayZoomControls:Z

.field private mDomStorageEnabled:Z

.field private mDoubleTapZoom:I

.field private mEnableSmoothTransition:Z

.field private final mEventHandler:Landroid/webkit/WebSettingsClassic$EventHandler;

.field private mFantasyFontFamily:Ljava/lang/String;

.field private mFixedFontFamily:Ljava/lang/String;

.field private mForceUserScalable:Z

.field private mForceZoomoutEnabled:Z

.field private mGeolocationDatabasePath:Ljava/lang/String;

.field private mGeolocationEnabled:Z

.field private mHardwareAccelSkia:Z

.field private mJavaScriptCanOpenWindowsAutomatically:Z

.field private mJavaScriptEnabled:Z

.field private mLGBubbleAction:Z

.field private mLGFloatingMode:Z

.field private mLGParagraphCopy:Z

.field private mLGScrollToTopBottomEnabled:Z

.field private mLGTextDragDropEnabled:Z

.field private mLGTranslateEnabled:Z

.field private mLGWebscrapEnabled:Z

.field private mLayoutAlgorithm:Landroid/webkit/WebSettings$LayoutAlgorithm;

.field private mLightTouchEnabled:Z

.field private mLinkPrefetchEnabled:Z

.field private mLoadWithOverviewMode:Z

.field private mLoadsImagesAutomatically:Z

.field private mMaximumDecodedImageSize:J

.field private mMediaPlaybackRequiresUserGesture:Z

.field private mMinimumFontSize:I

.field private mMinimumLogicalFontSize:I

.field private mNavDump:Z

.field private mNeedInitialFocus:Z

.field private mOverrideCacheMode:I

.field private mPageCacheCapacity:I

.field private mPasswordEchoEnabled:Z

.field private mPluginState:Landroid/webkit/WebSettings$PluginState;

.field private mPrivateBrowsingEnabled:Z

.field private mRenderPriority:Landroid/webkit/WebSettings$RenderPriority;

.field private mSansSerifFontFamily:Ljava/lang/String;

.field private mSaveFormData:Z

.field private mSavePassword:Z

.field private mSerifFontFamily:Ljava/lang/String;

.field private mShowVisualIndicator:Z

.field private mShrinksStandaloneImagesToFit:Z

.field private mStandardFontFamily:Ljava/lang/String;

.field private mSupportMultipleWindows:Z

.field private mSupportZoom:Z

.field private mSyncPending:Z

.field private mSyntheticLinksEnabled:Z

.field private mTextSize:I

.field private mUAProfile:Ljava/lang/String;

.field private mUseDefaultUserAgent:Z

.field private mUseDoubleTree:Z

.field private mUseWebViewBackgroundForOverscroll:Z

.field private mUseWideViewport:Z

.field private mUserAgent:Ljava/lang/String;

.field private mWOFFEnabled:Z

.field private mWebGLEnabled:Z

.field private mWebNotiEnabled:I

.field private mWebView:Landroid/webkit/WebViewClassic;

.field private mWorkersEnabled:Z

.field private mXSSAuditorEnabled:Z

.field public mWebSocketsEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 233
    const/4 v0, 0x3

    #@1
    sput v0, Landroid/webkit/WebSettingsClassic;->mDoubleTapToastCount:I

    #@3
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/webkit/WebViewClassic;)V
    .registers 11
    .parameter "context"
    .parameter "webview"

    #@0
    .prologue
    const/16 v6, 0x64

    #@2
    const/16 v7, 0x10

    #@4
    const/16 v4, 0x8

    #@6
    const/4 v2, 0x1

    #@7
    const/4 v3, 0x0

    #@8
    .line 330
    invoke-direct {p0}, Landroid/webkit/WebSettings;-><init>()V

    #@b
    .line 51
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mSyncPending:Z

    #@d
    .line 61
    sget-object v1, Landroid/webkit/WebSettings$LayoutAlgorithm;->NARROW_COLUMNS:Landroid/webkit/WebSettings$LayoutAlgorithm;

    #@f
    iput-object v1, p0, Landroid/webkit/WebSettingsClassic;->mLayoutAlgorithm:Landroid/webkit/WebSettings$LayoutAlgorithm;

    #@11
    .line 63
    iput v6, p0, Landroid/webkit/WebSettingsClassic;->mTextSize:I

    #@13
    .line 64
    const-string/jumbo v1, "sans-serif"

    #@16
    iput-object v1, p0, Landroid/webkit/WebSettingsClassic;->mStandardFontFamily:Ljava/lang/String;

    #@18
    .line 65
    const-string/jumbo v1, "monospace"

    #@1b
    iput-object v1, p0, Landroid/webkit/WebSettingsClassic;->mFixedFontFamily:Ljava/lang/String;

    #@1d
    .line 66
    const-string/jumbo v1, "sans-serif"

    #@20
    iput-object v1, p0, Landroid/webkit/WebSettingsClassic;->mSansSerifFontFamily:Ljava/lang/String;

    #@22
    .line 67
    const-string/jumbo v1, "serif"

    #@25
    iput-object v1, p0, Landroid/webkit/WebSettingsClassic;->mSerifFontFamily:Ljava/lang/String;

    #@27
    .line 68
    const-string v1, "cursive"

    #@29
    iput-object v1, p0, Landroid/webkit/WebSettingsClassic;->mCursiveFontFamily:Ljava/lang/String;

    #@2b
    .line 69
    const-string v1, "fantasy"

    #@2d
    iput-object v1, p0, Landroid/webkit/WebSettingsClassic;->mFantasyFontFamily:Ljava/lang/String;

    #@2f
    .line 74
    iput v4, p0, Landroid/webkit/WebSettingsClassic;->mMinimumFontSize:I

    #@31
    .line 75
    iput v4, p0, Landroid/webkit/WebSettingsClassic;->mMinimumLogicalFontSize:I

    #@33
    .line 76
    iput v7, p0, Landroid/webkit/WebSettingsClassic;->mDefaultFontSize:I

    #@35
    .line 77
    const/16 v1, 0xd

    #@37
    iput v1, p0, Landroid/webkit/WebSettingsClassic;->mDefaultFixedFontSize:I

    #@39
    .line 78
    iput v3, p0, Landroid/webkit/WebSettingsClassic;->mPageCacheCapacity:I

    #@3b
    .line 79
    iput-boolean v2, p0, Landroid/webkit/WebSettingsClassic;->mLoadsImagesAutomatically:Z

    #@3d
    .line 80
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mBlockNetworkImage:Z

    #@3f
    .line 82
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mJavaScriptEnabled:Z

    #@41
    .line 83
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mAllowUniversalAccessFromFileURLs:Z

    #@43
    .line 84
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mAllowFileAccessFromFileURLs:Z

    #@45
    .line 85
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mHardwareAccelSkia:Z

    #@47
    .line 86
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mShowVisualIndicator:Z

    #@49
    .line 87
    sget-object v1, Landroid/webkit/WebSettings$PluginState;->OFF:Landroid/webkit/WebSettings$PluginState;

    #@4b
    iput-object v1, p0, Landroid/webkit/WebSettingsClassic;->mPluginState:Landroid/webkit/WebSettings$PluginState;

    #@4d
    .line 88
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mJavaScriptCanOpenWindowsAutomatically:Z

    #@4f
    .line 89
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mUseDoubleTree:Z

    #@51
    .line 90
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mUseWideViewport:Z

    #@53
    .line 91
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mSupportMultipleWindows:Z

    #@55
    .line 92
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mShrinksStandaloneImagesToFit:Z

    #@57
    .line 93
    const-wide/16 v4, 0x0

    #@59
    iput-wide v4, p0, Landroid/webkit/WebSettingsClassic;->mMaximumDecodedImageSize:J

    #@5b
    .line 94
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mPrivateBrowsingEnabled:Z

    #@5d
    .line 95
    iput-boolean v2, p0, Landroid/webkit/WebSettingsClassic;->mSyntheticLinksEnabled:Z

    #@5f
    .line 97
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mAppCacheEnabled:Z

    #@61
    .line 98
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mDatabaseEnabled:Z

    #@63
    .line 99
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mDomStorageEnabled:Z

    #@65
    .line 100
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mWorkersEnabled:Z

    #@67
    .line 101
    iput-boolean v2, p0, Landroid/webkit/WebSettingsClassic;->mGeolocationEnabled:Z

    #@69
    .line 102
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mXSSAuditorEnabled:Z

    #@6b
    .line 103
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mLinkPrefetchEnabled:Z

    #@6d
    .line 105
    const-wide v4, 0x7fffffffffffffffL

    #@72
    iput-wide v4, p0, Landroid/webkit/WebSettingsClassic;->mAppCacheMaxSize:J

    #@74
    .line 106
    const/4 v1, 0x0

    #@75
    iput-object v1, p0, Landroid/webkit/WebSettingsClassic;->mAppCachePath:Ljava/lang/String;

    #@77
    .line 107
    const-string v1, ""

    #@79
    iput-object v1, p0, Landroid/webkit/WebSettingsClassic;->mDatabasePath:Ljava/lang/String;

    #@7b
    .line 110
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mDatabasePathHasBeenSet:Z

    #@7d
    .line 111
    const-string v1, ""

    #@7f
    iput-object v1, p0, Landroid/webkit/WebSettingsClassic;->mGeolocationDatabasePath:Ljava/lang/String;

    #@81
    .line 115
    sget-object v1, Landroid/webkit/WebSettings$ZoomDensity;->MEDIUM:Landroid/webkit/WebSettings$ZoomDensity;

    #@83
    iput-object v1, p0, Landroid/webkit/WebSettingsClassic;->mDefaultZoom:Landroid/webkit/WebSettings$ZoomDensity;

    #@85
    .line 116
    sget-object v1, Landroid/webkit/WebSettings$RenderPriority;->NORMAL:Landroid/webkit/WebSettings$RenderPriority;

    #@87
    iput-object v1, p0, Landroid/webkit/WebSettingsClassic;->mRenderPriority:Landroid/webkit/WebSettings$RenderPriority;

    #@89
    .line 117
    const/4 v1, -0x1

    #@8a
    iput v1, p0, Landroid/webkit/WebSettingsClassic;->mOverrideCacheMode:I

    #@8c
    .line 118
    iput v6, p0, Landroid/webkit/WebSettingsClassic;->mDoubleTapZoom:I

    #@8e
    .line 119
    iput-boolean v2, p0, Landroid/webkit/WebSettingsClassic;->mSaveFormData:Z

    #@90
    .line 120
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mAutoFillEnabled:Z

    #@92
    .line 121
    iput-boolean v2, p0, Landroid/webkit/WebSettingsClassic;->mSavePassword:Z

    #@94
    .line 122
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mLightTouchEnabled:Z

    #@96
    .line 123
    iput-boolean v2, p0, Landroid/webkit/WebSettingsClassic;->mNeedInitialFocus:Z

    #@98
    .line 124
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mNavDump:Z

    #@9a
    .line 125
    iput-boolean v2, p0, Landroid/webkit/WebSettingsClassic;->mSupportZoom:Z

    #@9c
    .line 126
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mMediaPlaybackRequiresUserGesture:Z

    #@9e
    .line 127
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mBuiltInZoomControls:Z

    #@a0
    .line 128
    iput-boolean v2, p0, Landroid/webkit/WebSettingsClassic;->mDisplayZoomControls:Z

    #@a2
    .line 129
    iput-boolean v2, p0, Landroid/webkit/WebSettingsClassic;->mAllowFileAccess:Z

    #@a4
    .line 130
    iput-boolean v2, p0, Landroid/webkit/WebSettingsClassic;->mAllowContentAccess:Z

    #@a6
    .line 131
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mLoadWithOverviewMode:Z

    #@a8
    .line 132
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mEnableSmoothTransition:Z

    #@aa
    .line 133
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mForceUserScalable:Z

    #@ac
    .line 135
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mLGBubbleAction:Z

    #@ae
    .line 139
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mLGParagraphCopy:Z

    #@b0
    .line 146
    const-string v1, ""

    #@b2
    iput-object v1, p0, Landroid/webkit/WebSettingsClassic;->mAdditionalHeaders:Ljava/lang/String;

    #@b4
    .line 147
    new-instance v1, Ljava/util/Hashtable;

    #@b6
    invoke-direct {v1}, Ljava/util/Hashtable;-><init>()V

    #@b9
    iput-object v1, p0, Landroid/webkit/WebSettingsClassic;->mAdditionalHeadersTable:Ljava/util/Hashtable;

    #@bb
    .line 149
    iput-boolean v2, p0, Landroid/webkit/WebSettingsClassic;->mPasswordEchoEnabled:Z

    #@bd
    .line 151
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mForceZoomoutEnabled:Z

    #@bf
    .line 155
    iput-boolean v2, p0, Landroid/webkit/WebSettingsClassic;->mWOFFEnabled:Z

    #@c1
    .line 158
    iput-boolean v2, p0, Landroid/webkit/WebSettingsClassic;->mWebGLEnabled:Z

    #@c3
    .line 162
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mLGFloatingMode:Z

    #@c5
    .line 166
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mLGWebscrapEnabled:Z

    #@c7
    .line 170
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mLGScrollToTopBottomEnabled:Z

    #@c9
    .line 174
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mLGTextDragDropEnabled:Z

    #@cb
    .line 177
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mLGTranslateEnabled:Z

    #@cd
    .line 181
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mCliptrayEnabled:Z

    #@cf
    .line 230
    iput-boolean v2, p0, Landroid/webkit/WebSettingsClassic;->mUseWebViewBackgroundForOverscroll:Z

    #@d1
    .line 1232
    iput v2, p0, Landroid/webkit/WebSettingsClassic;->mWebNotiEnabled:I

    #@d3
    .line 331
    new-instance v1, Landroid/webkit/WebSettingsClassic$EventHandler;

    #@d5
    const/4 v4, 0x0

    #@d6
    invoke-direct {v1, p0, v4}, Landroid/webkit/WebSettingsClassic$EventHandler;-><init>(Landroid/webkit/WebSettingsClassic;Landroid/webkit/WebSettingsClassic$1;)V

    #@d9
    iput-object v1, p0, Landroid/webkit/WebSettingsClassic;->mEventHandler:Landroid/webkit/WebSettingsClassic$EventHandler;

    #@db
    .line 332
    iput-object p1, p0, Landroid/webkit/WebSettingsClassic;->mContext:Landroid/content/Context;

    #@dd
    .line 333
    iput-object p2, p0, Landroid/webkit/WebSettingsClassic;->mWebView:Landroid/webkit/WebViewClassic;

    #@df
    .line 334
    const v1, 0x1040085

    #@e2
    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@e5
    move-result-object v1

    #@e6
    iput-object v1, p0, Landroid/webkit/WebSettingsClassic;->mDefaultTextEncoding:Ljava/lang/String;

    #@e8
    .line 337
    sget-object v1, Landroid/webkit/WebSettingsClassic;->sLockForLocaleSettings:Ljava/lang/Object;

    #@ea
    if-nez v1, :cond_f9

    #@ec
    .line 338
    new-instance v1, Ljava/lang/Object;

    #@ee
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@f1
    sput-object v1, Landroid/webkit/WebSettingsClassic;->sLockForLocaleSettings:Ljava/lang/Object;

    #@f3
    .line 339
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@f6
    move-result-object v1

    #@f7
    sput-object v1, Landroid/webkit/WebSettingsClassic;->sLocale:Ljava/util/Locale;

    #@f9
    .line 341
    :cond_f9
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->getCurrentAcceptLanguage()Ljava/lang/String;

    #@fc
    move-result-object v1

    #@fd
    iput-object v1, p0, Landroid/webkit/WebSettingsClassic;->mAcceptLanguage:Ljava/lang/String;

    #@ff
    .line 342
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->getCurrentUserAgent()Ljava/lang/String;

    #@102
    move-result-object v1

    #@103
    iput-object v1, p0, Landroid/webkit/WebSettingsClassic;->mUserAgent:Ljava/lang/String;

    #@105
    .line 343
    iput-boolean v2, p0, Landroid/webkit/WebSettingsClassic;->mUseDefaultUserAgent:Z

    #@107
    .line 345
    iget-object v1, p0, Landroid/webkit/WebSettingsClassic;->mContext:Landroid/content/Context;

    #@109
    const-string v4, "android.permission.INTERNET"

    #@10b
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@10e
    move-result v5

    #@10f
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@112
    move-result v6

    #@113
    invoke-virtual {v1, v4, v5, v6}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    #@116
    move-result v1

    #@117
    if-eqz v1, :cond_13e

    #@119
    move v1, v2

    #@11a
    :goto_11a
    iput-boolean v1, p0, Landroid/webkit/WebSettingsClassic;->mBlockNetworkLoads:Z

    #@11c
    .line 350
    iget-object v1, p0, Landroid/webkit/WebSettingsClassic;->mContext:Landroid/content/Context;

    #@11e
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@121
    move-result-object v1

    #@122
    iget v1, v1, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@124
    if-ge v1, v7, :cond_12a

    #@126
    .line 352
    iput-boolean v2, p0, Landroid/webkit/WebSettingsClassic;->mAllowUniversalAccessFromFileURLs:Z

    #@128
    .line 353
    iput-boolean v2, p0, Landroid/webkit/WebSettingsClassic;->mAllowFileAccessFromFileURLs:Z

    #@12a
    .line 356
    :cond_12a
    :try_start_12a
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@12d
    move-result-object v1

    #@12e
    const-string/jumbo v4, "show_password"

    #@131
    invoke-static {v1, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    #@134
    move-result v1

    #@135
    if-eqz v1, :cond_138

    #@137
    move v3, v2

    #@138
    :cond_138
    iput-boolean v3, p0, Landroid/webkit/WebSettingsClassic;->mPasswordEchoEnabled:Z
    :try_end_13a
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_12a .. :try_end_13a} :catch_140

    #@13a
    .line 364
    :goto_13a
    invoke-direct {p0, v2}, Landroid/webkit/WebSettingsClassic;->setUAProfileEnabled(Z)V

    #@13d
    .line 366
    return-void

    #@13e
    :cond_13e
    move v1, v3

    #@13f
    .line 345
    goto :goto_11a

    #@140
    .line 359
    :catch_140
    move-exception v0

    #@141
    .line 360
    .local v0, e:Landroid/provider/Settings$SettingNotFoundException;
    iput-boolean v2, p0, Landroid/webkit/WebSettingsClassic;->mPasswordEchoEnabled:Z

    #@143
    goto :goto_13a
.end method

.method static synthetic access$000(Landroid/webkit/WebSettingsClassic;)Landroid/webkit/BrowserFrame;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 42
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mBrowserFrame:Landroid/webkit/BrowserFrame;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/webkit/WebSettingsClassic;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 42
    invoke-direct {p0, p1}, Landroid/webkit/WebSettingsClassic;->nativeSync(I)V

    #@3
    return-void
.end method

.method static synthetic access$202(Landroid/webkit/WebSettingsClassic;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 42
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mSyncPending:Z

    #@2
    return p1
.end method

.method static synthetic access$400(Landroid/webkit/WebSettingsClassic;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 42
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$500()I
    .registers 1

    #@0
    .prologue
    .line 42
    sget v0, Landroid/webkit/WebSettingsClassic;->mDoubleTapToastCount:I

    #@2
    return v0
.end method

.method static synthetic access$600(Landroid/webkit/WebSettingsClassic;)Landroid/webkit/WebSettings$RenderPriority;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 42
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mRenderPriority:Landroid/webkit/WebSettings$RenderPriority;

    #@2
    return-object v0
.end method

.method private static addLocaleToHttpAcceptLanguage(Ljava/lang/StringBuilder;Ljava/util/Locale;)V
    .registers 5
    .parameter "builder"
    .parameter "locale"

    #@0
    .prologue
    .line 415
    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    #@3
    move-result-object v2

    #@4
    invoke-static {v2}, Landroid/webkit/WebSettingsClassic;->convertObsoleteLanguageCodeToNew(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    .line 416
    .local v1, language:Ljava/lang/String;
    if-eqz v1, :cond_1b

    #@a
    .line 417
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    .line 418
    invoke-virtual {p1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    #@10
    move-result-object v0

    #@11
    .line 419
    .local v0, country:Ljava/lang/String;
    if-eqz v0, :cond_1b

    #@13
    .line 420
    const-string v2, "-"

    #@15
    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    .line 421
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    .line 424
    .end local v0           #country:Ljava/lang/String;
    :cond_1b
    return-void
.end method

.method private static convertObsoleteLanguageCodeToNew(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "langCode"

    #@0
    .prologue
    .line 397
    if-nez p0, :cond_4

    #@2
    .line 398
    const/4 p0, 0x0

    #@3
    .line 410
    .end local p0
    :cond_3
    :goto_3
    return-object p0

    #@4
    .line 400
    .restart local p0
    :cond_4
    const-string/jumbo v0, "iw"

    #@7
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_10

    #@d
    .line 402
    const-string p0, "he"

    #@f
    goto :goto_3

    #@10
    .line 403
    :cond_10
    const-string v0, "in"

    #@12
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_1b

    #@18
    .line 405
    const-string p0, "id"

    #@1a
    goto :goto_3

    #@1b
    .line 406
    :cond_1b
    const-string/jumbo v0, "ji"

    #@1e
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v0

    #@22
    if-eqz v0, :cond_3

    #@24
    .line 408
    const-string/jumbo p0, "yi"

    #@27
    goto :goto_3
.end method

.method public static final native nativeIsWebGLAvailable()Z
.end method

.method private getCurrentAcceptLanguage()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 376
    sget-object v3, Landroid/webkit/WebSettingsClassic;->sLockForLocaleSettings:Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 377
    :try_start_3
    sget-object v1, Landroid/webkit/WebSettingsClassic;->sLocale:Ljava/util/Locale;

    #@5
    .line 378
    .local v1, locale:Ljava/util/Locale;
    monitor-exit v3
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_2b

    #@6
    .line 379
    new-instance v0, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    .line 380
    .local v0, buffer:Ljava/lang/StringBuilder;
    invoke-static {v0, v1}, Landroid/webkit/WebSettingsClassic;->addLocaleToHttpAcceptLanguage(Ljava/lang/StringBuilder;Ljava/util/Locale;)V

    #@e
    .line 382
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    #@10
    invoke-virtual {v2, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v2

    #@14
    if-nez v2, :cond_26

    #@16
    .line 383
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    #@19
    move-result v2

    #@1a
    if-lez v2, :cond_21

    #@1c
    .line 384
    const-string v2, ", "

    #@1e
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    .line 386
    :cond_21
    const-string v2, "en-US"

    #@23
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    .line 389
    :cond_26
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    return-object v2

    #@2b
    .line 378
    .end local v0           #buffer:Ljava/lang/StringBuilder;
    .end local v1           #locale:Ljava/util/Locale;
    :catchall_2b
    move-exception v2

    #@2c
    :try_start_2c
    monitor-exit v3
    :try_end_2d
    .catchall {:try_start_2c .. :try_end_2d} :catchall_2b

    #@2d
    throw v2
.end method

.method private declared-synchronized getCurrentUserAgent()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 432
    monitor-enter p0

    #@1
    :try_start_1
    sget-object v2, Landroid/webkit/WebSettingsClassic;->sLockForLocaleSettings:Ljava/lang/Object;

    #@3
    monitor-enter v2
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_12

    #@4
    .line 433
    :try_start_4
    sget-object v0, Landroid/webkit/WebSettingsClassic;->sLocale:Ljava/util/Locale;

    #@6
    .line 434
    .local v0, locale:Ljava/util/Locale;
    monitor-exit v2
    :try_end_7
    .catchall {:try_start_4 .. :try_end_7} :catchall_f

    #@7
    .line 435
    :try_start_7
    iget-object v1, p0, Landroid/webkit/WebSettingsClassic;->mContext:Landroid/content/Context;

    #@9
    invoke-static {v1, v0}, Landroid/webkit/WebSettingsClassic;->getDefaultUserAgentForLocale(Landroid/content/Context;Ljava/util/Locale;)Ljava/lang/String;
    :try_end_c
    .catchall {:try_start_7 .. :try_end_c} :catchall_12

    #@c
    move-result-object v1

    #@d
    monitor-exit p0

    #@e
    return-object v1

    #@f
    .line 434
    .end local v0           #locale:Ljava/util/Locale;
    :catchall_f
    move-exception v1

    #@10
    :try_start_10
    monitor-exit v2
    :try_end_11
    .catchall {:try_start_10 .. :try_end_11} :catchall_f

    #@11
    :try_start_11
    throw v1
    :try_end_12
    .catchall {:try_start_11 .. :try_end_12} :catchall_12

    #@12
    .line 432
    :catchall_12
    move-exception v1

    #@13
    monitor-exit p0

    #@14
    throw v1
.end method

.method public static getDefaultUserAgentForLocale(Landroid/content/Context;Ljava/util/Locale;)Ljava/lang/String;
    .registers 13
    .parameter "context"
    .parameter "locale"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    .line 450
    new-instance v1, Ljava/lang/StringBuffer;

    #@3
    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    #@6
    .line 452
    .local v1, buffer:Ljava/lang/StringBuffer;
    sget-object v7, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    #@8
    .line 453
    .local v7, version:Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@b
    move-result v8

    #@c
    if-lez v8, :cond_a0

    #@e
    .line 454
    invoke-virtual {v7, v10}, Ljava/lang/String;->charAt(I)C

    #@11
    move-result v8

    #@12
    invoke-static {v8}, Ljava/lang/Character;->isDigit(C)Z

    #@15
    move-result v8

    #@16
    if-eqz v8, :cond_99

    #@18
    .line 456
    invoke-virtual {v1, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1b
    .line 466
    :goto_1b
    const-string v8, "; "

    #@1d
    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@20
    .line 467
    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    #@23
    move-result-object v4

    #@24
    .line 468
    .local v4, language:Ljava/lang/String;
    if-eqz v4, :cond_a7

    #@26
    .line 469
    invoke-static {v4}, Landroid/webkit/WebSettingsClassic;->convertObsoleteLanguageCodeToNew(Ljava/lang/String;)Ljava/lang/String;

    #@29
    move-result-object v8

    #@2a
    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@2d
    .line 470
    invoke-virtual {p1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    .line 471
    .local v2, country:Ljava/lang/String;
    if-eqz v2, :cond_3f

    #@33
    .line 472
    const-string v8, "-"

    #@35
    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@38
    .line 473
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@3b
    move-result-object v8

    #@3c
    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@3f
    .line 479
    .end local v2           #country:Ljava/lang/String;
    :cond_3f
    :goto_3f
    const-string v8, ";"

    #@41
    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@44
    .line 481
    const-string v8, "REL"

    #@46
    sget-object v9, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    #@48
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4b
    move-result v8

    #@4c
    if-eqz v8, :cond_5e

    #@4e
    .line 482
    sget-object v6, Landroid/os/Build;->MODEL:Ljava/lang/String;

    #@50
    .line 483
    .local v6, model:Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@53
    move-result v8

    #@54
    if-lez v8, :cond_5e

    #@56
    .line 484
    const-string v8, " "

    #@58
    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@5b
    .line 485
    invoke-virtual {v1, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@5e
    .line 488
    .end local v6           #model:Ljava/lang/String;
    :cond_5e
    sget-object v3, Landroid/os/Build;->ID:Ljava/lang/String;

    #@60
    .line 489
    .local v3, id:Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@63
    move-result v8

    #@64
    if-lez v8, :cond_6e

    #@66
    .line 490
    const-string v8, " Build/"

    #@68
    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@6b
    .line 491
    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@6e
    .line 493
    :cond_6e
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@71
    move-result-object v8

    #@72
    const v9, 0x1040368

    #@75
    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@78
    move-result-object v8

    #@79
    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@7c
    move-result-object v5

    #@7d
    .line 495
    .local v5, mobile:Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@80
    move-result-object v8

    #@81
    const v9, 0x1040367

    #@84
    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@87
    move-result-object v8

    #@88
    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@8b
    move-result-object v0

    #@8c
    .line 497
    .local v0, base:Ljava/lang/String;
    const/4 v8, 0x2

    #@8d
    new-array v8, v8, [Ljava/lang/Object;

    #@8f
    aput-object v1, v8, v10

    #@91
    const/4 v9, 0x1

    #@92
    aput-object v5, v8, v9

    #@94
    invoke-static {v0, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@97
    move-result-object v8

    #@98
    return-object v8

    #@99
    .line 460
    .end local v0           #base:Ljava/lang/String;
    .end local v3           #id:Ljava/lang/String;
    .end local v4           #language:Ljava/lang/String;
    .end local v5           #mobile:Ljava/lang/String;
    :cond_99
    const-string v8, "4.1.1"

    #@9b
    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@9e
    goto/16 :goto_1b

    #@a0
    .line 464
    :cond_a0
    const-string v8, "1.0"

    #@a2
    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@a5
    goto/16 :goto_1b

    #@a7
    .line 477
    .restart local v4       #language:Ljava/lang/String;
    :cond_a7
    const-string v8, "en"

    #@a9
    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@ac
    goto :goto_3f
.end method

.method private native nativeSync(I)V
.end method

.method private pin(I)I
    .registers 4
    .parameter "size"

    #@0
    .prologue
    const/16 v1, 0x48

    #@2
    const/4 v0, 0x1

    #@3
    .line 2109
    if-ge p1, v0, :cond_7

    #@5
    move p1, v0

    #@6
    .line 2114
    .end local p1
    :cond_6
    :goto_6
    return p1

    #@7
    .line 2111
    .restart local p1
    :cond_7
    if-le p1, v1, :cond_6

    #@9
    move p1, v1

    #@a
    .line 2112
    goto :goto_6
.end method

.method private declared-synchronized postSync()V
    .registers 4

    #@0
    .prologue
    .line 2120
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mSyncPending:Z

    #@3
    if-nez v0, :cond_13

    #@5
    .line 2121
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mEventHandler:Landroid/webkit/WebSettingsClassic$EventHandler;

    #@7
    const/4 v1, 0x0

    #@8
    const/4 v2, 0x0

    #@9
    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@c
    move-result-object v1

    #@d
    invoke-static {v0, v1}, Landroid/webkit/WebSettingsClassic$EventHandler;->access$800(Landroid/webkit/WebSettingsClassic$EventHandler;Landroid/os/Message;)Z

    #@10
    move-result v0

    #@11
    iput-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mSyncPending:Z
    :try_end_13
    .catchall {:try_start_1 .. :try_end_13} :catchall_15

    #@13
    .line 2124
    :cond_13
    monitor-exit p0

    #@14
    return-void

    #@15
    .line 2120
    :catchall_15
    move-exception v0

    #@16
    monitor-exit p0

    #@17
    throw v0
.end method

.method private setUAProfileEnabled(Z)V
    .registers 4
    .parameter "flag"

    #@0
    .prologue
    .line 1863
    if-eqz p1, :cond_12

    #@2
    .line 1864
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v0

    #@8
    const v1, 0x1040040

    #@b
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    iput-object v0, p0, Landroid/webkit/WebSettingsClassic;->mUAProfile:Ljava/lang/String;

    #@11
    .line 1868
    :goto_11
    return-void

    #@12
    .line 1867
    :cond_12
    const-string v0, ""

    #@14
    iput-object v0, p0, Landroid/webkit/WebSettingsClassic;->mUAProfile:Ljava/lang/String;

    #@16
    goto :goto_11
.end method

.method private verifyNetworkAccess()V
    .registers 5

    #@0
    .prologue
    .line 1164
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mBlockNetworkLoads:Z

    #@2
    if-nez v0, :cond_1e

    #@4
    .line 1165
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mContext:Landroid/content/Context;

    #@6
    const-string v1, "android.permission.INTERNET"

    #@8
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@b
    move-result v2

    #@c
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@f
    move-result v3

    #@10
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    #@13
    move-result v0

    #@14
    if-eqz v0, :cond_1e

    #@16
    .line 1168
    new-instance v0, Ljava/lang/SecurityException;

    #@18
    const-string v1, "Permission denied - application missing INTERNET permission"

    #@1a
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v0

    #@1e
    .line 1173
    :cond_1e
    return-void
.end method


# virtual methods
.method public addAdditionalHeader(Ljava/lang/String;Ljava/lang/String;)V
    .registers 12
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 1899
    if-eqz p1, :cond_8

    #@2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@5
    move-result v5

    #@6
    if-nez v5, :cond_9

    #@8
    .line 1919
    :cond_8
    :goto_8
    return-void

    #@9
    .line 1900
    :cond_9
    if-eqz p2, :cond_8

    #@b
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    #@e
    move-result v5

    #@f
    if-eqz v5, :cond_8

    #@11
    .line 1902
    iget-object v5, p0, Landroid/webkit/WebSettingsClassic;->mAdditionalHeadersTable:Ljava/util/Hashtable;

    #@13
    invoke-virtual {v5, p1, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@16
    move-result-object v4

    #@17
    check-cast v4, Ljava/lang/String;

    #@19
    .line 1903
    .local v4, prevValue:Ljava/lang/String;
    if-nez v4, :cond_5a

    #@1b
    .line 1904
    new-instance v5, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    iget-object v6, p0, Landroid/webkit/WebSettingsClassic;->mAdditionalHeaders:Ljava/lang/String;

    #@22
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v5

    #@26
    new-instance v6, Ljava/lang/StringBuffer;

    #@28
    new-instance v7, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v7

    #@31
    const-string v8, ": "

    #@33
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v7

    #@37
    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v7

    #@3b
    const-string v8, "\r\n"

    #@3d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v7

    #@41
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v7

    #@45
    invoke-direct {v6, v7}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    #@48
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@4b
    move-result-object v6

    #@4c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v5

    #@50
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v5

    #@54
    iput-object v5, p0, Landroid/webkit/WebSettingsClassic;->mAdditionalHeaders:Ljava/lang/String;

    #@56
    .line 1918
    :goto_56
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V

    #@59
    goto :goto_8

    #@5a
    .line 1906
    :cond_5a
    new-instance v0, Ljava/lang/StringBuffer;

    #@5c
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    #@5f
    .line 1907
    .local v0, buffer:Ljava/lang/StringBuffer;
    iget-object v5, p0, Landroid/webkit/WebSettingsClassic;->mAdditionalHeadersTable:Ljava/util/Hashtable;

    #@61
    invoke-virtual {v5}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    #@64
    move-result-object v1

    #@65
    .line 1908
    .local v1, e:Ljava/util/Enumeration;,"Ljava/util/Enumeration<Ljava/lang/String;>;"
    :goto_65
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    #@68
    move-result v5

    #@69
    if-eqz v5, :cond_8a

    #@6b
    .line 1909
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    #@6e
    move-result-object v2

    #@6f
    check-cast v2, Ljava/lang/String;

    #@71
    .line 1910
    .local v2, headerKey:Ljava/lang/String;
    iget-object v5, p0, Landroid/webkit/WebSettingsClassic;->mAdditionalHeadersTable:Ljava/util/Hashtable;

    #@73
    invoke-virtual {v5, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@76
    move-result-object v3

    #@77
    check-cast v3, Ljava/lang/String;

    #@79
    .line 1911
    .local v3, headerValue:Ljava/lang/String;
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@7c
    .line 1912
    const-string v5, ": "

    #@7e
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@81
    .line 1913
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@84
    .line 1914
    const-string v5, "\r\n"

    #@86
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@89
    goto :goto_65

    #@8a
    .line 1916
    .end local v2           #headerKey:Ljava/lang/String;
    .end local v3           #headerValue:Ljava/lang/String;
    :cond_8a
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@8d
    move-result-object v5

    #@8e
    iput-object v5, p0, Landroid/webkit/WebSettingsClassic;->mAdditionalHeaders:Ljava/lang/String;

    #@90
    goto :goto_56
.end method

.method public enableSmoothTransition()Z
    .registers 2

    #@0
    .prologue
    .line 649
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mEnableSmoothTransition:Z

    #@2
    return v0
.end method

.method public forceUserScalable()Z
    .registers 2

    #@0
    .prologue
    .line 1752
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mForceUserScalable:Z

    #@2
    return v0
.end method

.method declared-synchronized getAcceptLanguage()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 1607
    monitor-enter p0

    #@1
    :try_start_1
    sget-object v2, Landroid/webkit/WebSettingsClassic;->sLockForLocaleSettings:Ljava/lang/Object;

    #@3
    monitor-enter v2
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_20

    #@4
    .line 1608
    :try_start_4
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@7
    move-result-object v0

    #@8
    .line 1609
    .local v0, currentLocale:Ljava/util/Locale;
    sget-object v1, Landroid/webkit/WebSettingsClassic;->sLocale:Ljava/util/Locale;

    #@a
    invoke-virtual {v1, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_18

    #@10
    .line 1610
    sput-object v0, Landroid/webkit/WebSettingsClassic;->sLocale:Ljava/util/Locale;

    #@12
    .line 1611
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->getCurrentAcceptLanguage()Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    iput-object v1, p0, Landroid/webkit/WebSettingsClassic;->mAcceptLanguage:Ljava/lang/String;

    #@18
    .line 1613
    :cond_18
    monitor-exit v2
    :try_end_19
    .catchall {:try_start_4 .. :try_end_19} :catchall_1d

    #@19
    .line 1614
    :try_start_19
    iget-object v1, p0, Landroid/webkit/WebSettingsClassic;->mAcceptLanguage:Ljava/lang/String;
    :try_end_1b
    .catchall {:try_start_19 .. :try_end_1b} :catchall_20

    #@1b
    monitor-exit p0

    #@1c
    return-object v1

    #@1d
    .line 1613
    .end local v0           #currentLocale:Ljava/util/Locale;
    :catchall_1d
    move-exception v1

    #@1e
    :try_start_1e
    monitor-exit v2
    :try_end_1f
    .catchall {:try_start_1e .. :try_end_1f} :catchall_1d

    #@1f
    :try_start_1f
    throw v1
    :try_end_20
    .catchall {:try_start_1f .. :try_end_20} :catchall_20

    #@20
    .line 1607
    :catchall_20
    move-exception v1

    #@21
    monitor-exit p0

    #@22
    throw v1
.end method

.method public getAllowContentAccess()Z
    .registers 2

    #@0
    .prologue
    .line 617
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mAllowContentAccess:Z

    #@2
    return v0
.end method

.method public getAllowFileAccess()Z
    .registers 2

    #@0
    .prologue
    .line 601
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mAllowFileAccess:Z

    #@2
    return v0
.end method

.method public declared-synchronized getAllowFileAccessFromFileURLs()Z
    .registers 2

    #@0
    .prologue
    .line 1467
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mAllowFileAccessFromFileURLs:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getAllowUniversalAccessFromFileURLs()Z
    .registers 2

    #@0
    .prologue
    .line 1459
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mAllowUniversalAccessFromFileURLs:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getAutoFillEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 1793
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mAutoFillEnabled:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getAutoFillProfile()Landroid/webkit/WebSettingsClassic$AutoFillProfile;
    .registers 2

    #@0
    .prologue
    .line 1804
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mAutoFillProfile:Landroid/webkit/WebSettingsClassic$AutoFillProfile;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getBlockNetworkImage()Z
    .registers 2

    #@0
    .prologue
    .line 1139
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mBlockNetworkImage:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getBlockNetworkLoads()Z
    .registers 2

    #@0
    .prologue
    .line 1159
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mBlockNetworkLoads:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public getBuiltInZoomControls()Z
    .registers 2

    #@0
    .prologue
    .line 568
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mBuiltInZoomControls:Z

    #@2
    return v0
.end method

.method public getCacheMode()I
    .registers 2

    #@0
    .prologue
    .line 1664
    iget v0, p0, Landroid/webkit/WebSettingsClassic;->mOverrideCacheMode:I

    #@2
    return v0
.end method

.method public declared-synchronized getCliptrayEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 2074
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mCliptrayEnabled:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getCursiveFontFamily()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 989
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mCursiveFontFamily:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getDatabaseEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 1397
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mDatabaseEnabled:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getDatabasePath()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1389
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mDatabasePath:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getDefaultFixedFontSize()I
    .registers 2

    #@0
    .prologue
    .line 1088
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/webkit/WebSettingsClassic;->mDefaultFixedFontSize:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getDefaultFontSize()I
    .registers 2

    #@0
    .prologue
    .line 1068
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/webkit/WebSettingsClassic;->mDefaultFontSize:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getDefaultTextEncodingName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1532
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mDefaultTextEncoding:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public getDefaultZoom()Landroid/webkit/WebSettings$ZoomDensity;
    .registers 2

    #@0
    .prologue
    .line 760
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mDefaultZoom:Landroid/webkit/WebSettings$ZoomDensity;

    #@2
    return-object v0
.end method

.method public getDisplayZoomControls()Z
    .registers 2

    #@0
    .prologue
    .line 585
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mDisplayZoomControls:Z

    #@2
    return v0
.end method

.method public declared-synchronized getDomStorageEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 1381
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mDomStorageEnabled:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method getDoubleTapToastCount()I
    .registers 2

    #@0
    .prologue
    .line 1808
    sget v0, Landroid/webkit/WebSettingsClassic;->mDoubleTapToastCount:I

    #@2
    return v0
.end method

.method public getDoubleTapZoom()I
    .registers 2

    #@0
    .prologue
    .line 741
    iget v0, p0, Landroid/webkit/WebSettingsClassic;->mDoubleTapZoom:I

    #@2
    return v0
.end method

.method public declared-synchronized getFantasyFontFamily()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1008
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mFantasyFontFamily:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getFixedFontFamily()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 932
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mFixedFontFamily:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getFloatingMode()Z
    .registers 2

    #@0
    .prologue
    .line 1970
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mLGFloatingMode:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getForceZoomoutEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 1688
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mForceZoomoutEnabled:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getHardwareAccelSkiaEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 1224
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mHardwareAccelSkia:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getJavaScriptCanOpenWindowsAutomatically()Z
    .registers 2

    #@0
    .prologue
    .line 1513
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mJavaScriptCanOpenWindowsAutomatically:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getJavaScriptEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 1451
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mJavaScriptEnabled:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getLGBubbleActionEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 1852
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mLGBubbleAction:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getLayoutAlgorithm()Landroid/webkit/WebSettings$LayoutAlgorithm;
    .registers 2

    #@0
    .prologue
    .line 894
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mLayoutAlgorithm:Landroid/webkit/WebSettings$LayoutAlgorithm;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public getLightTouchEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 776
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mLightTouchEnabled:Z

    #@2
    return v0
.end method

.method public getLoadWithOverviewMode()Z
    .registers 2

    #@0
    .prologue
    .line 633
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mLoadWithOverviewMode:Z

    #@2
    return v0
.end method

.method public declared-synchronized getLoadsImagesAutomatically()Z
    .registers 2

    #@0
    .prologue
    .line 1120
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mLoadsImagesAutomatically:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public getMediaPlaybackRequiresUserGesture()Z
    .registers 2

    #@0
    .prologue
    .line 551
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mMediaPlaybackRequiresUserGesture:Z

    #@2
    return v0
.end method

.method public declared-synchronized getMinimumFontSize()I
    .registers 2

    #@0
    .prologue
    .line 1028
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/webkit/WebSettingsClassic;->mMinimumFontSize:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getMinimumLogicalFontSize()I
    .registers 2

    #@0
    .prologue
    .line 1048
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/webkit/WebSettingsClassic;->mMinimumLogicalFontSize:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public getNavDump()Z
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 515
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mNavDump:Z

    #@2
    return v0
.end method

.method getNeedInitialFocus()Z
    .registers 2

    #@0
    .prologue
    .line 1633
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mNeedInitialFocus:Z

    #@2
    return v0
.end method

.method public declared-synchronized getParagraphCopyEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 1936
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mLGParagraphCopy:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getPluginState()Landroid/webkit/WebSettings$PluginState;
    .registers 2

    #@0
    .prologue
    .line 1484
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mPluginState:Landroid/webkit/WebSettings$PluginState;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getPluginsEnabled()Z
    .registers 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1476
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mPluginState:Landroid/webkit/WebSettings$PluginState;

    #@3
    sget-object v1, Landroid/webkit/WebSettings$PluginState;->ON:Landroid/webkit/WebSettings$PluginState;
    :try_end_5
    .catchall {:try_start_1 .. :try_end_5} :catchall_c

    #@5
    if-ne v0, v1, :cond_a

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    monitor-exit p0

    #@9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_8

    #@c
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized getPluginsPath()Ljava/lang/String;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1493
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, ""
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public getProperty(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 1827
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mWebView:Landroid/webkit/WebViewClassic;

    #@2
    invoke-virtual {v0, p1}, Landroid/webkit/WebViewClassic;->nativeGetProperty(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public declared-synchronized getSansSerifFontFamily()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 951
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mSansSerifFontFamily:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public getSaveFormData()Z
    .registers 2

    #@0
    .prologue
    .line 683
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mSaveFormData:Z

    #@2
    if-eqz v0, :cond_a

    #@4
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mPrivateBrowsingEnabled:Z

    #@6
    if-nez v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public getSavePassword()Z
    .registers 2

    #@0
    .prologue
    .line 699
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mSavePassword:Z

    #@2
    return v0
.end method

.method public declared-synchronized getScrollToTopBottomEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 2010
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mLGScrollToTopBottomEnabled:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getSerifFontFamily()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 970
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mSerifFontFamily:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getShowVisualIndicator()Z
    .registers 2

    #@0
    .prologue
    .line 1264
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mShowVisualIndicator:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getStandardFontFamily()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 913
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mStandardFontFamily:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getTextDragDropEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 2030
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mLGTextDragDropEnabled:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getTextZoom()I
    .registers 2

    #@0
    .prologue
    .line 722
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/webkit/WebSettingsClassic;->mTextSize:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getTranslateEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 2050
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mLGTranslateEnabled:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getUAProfile()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1877
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mUAProfile:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getUseDoubleTree()Z
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 794
    monitor-enter p0

    #@1
    const/4 v0, 0x0

    #@2
    monitor-exit p0

    #@3
    return v0
.end method

.method getUseFixedViewport()Z
    .registers 2

    #@0
    .prologue
    .line 1722
    invoke-virtual {p0}, Landroid/webkit/WebSettingsClassic;->getUseWideViewPort()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getUseWebViewBackgroundForOverscrollBackground()Z
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 667
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mUseWebViewBackgroundForOverscroll:Z

    #@2
    return v0
.end method

.method public declared-synchronized getUseWideViewPort()Z
    .registers 2

    #@0
    .prologue
    .line 854
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mUseWideViewport:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getUserAgent()I
    .registers 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 828
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.34 Safari/534.24"

    #@3
    iget-object v1, p0, Landroid/webkit/WebSettingsClassic;->mUserAgent:Ljava/lang/String;

    #@5
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_22

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_e

    #@b
    .line 829
    const/4 v0, 0x1

    #@c
    .line 835
    :goto_c
    monitor-exit p0

    #@d
    return v0

    #@e
    .line 830
    :cond_e
    :try_start_e
    const-string v0, "Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_0 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7A341 Safari/528.16"

    #@10
    iget-object v1, p0, Landroid/webkit/WebSettingsClassic;->mUserAgent:Ljava/lang/String;

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_1a

    #@18
    .line 831
    const/4 v0, 0x2

    #@19
    goto :goto_c

    #@1a
    .line 832
    :cond_1a
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mUseDefaultUserAgent:Z
    :try_end_1c
    .catchall {:try_start_e .. :try_end_1c} :catchall_22

    #@1c
    if-eqz v0, :cond_20

    #@1e
    .line 833
    const/4 v0, 0x0

    #@1f
    goto :goto_c

    #@20
    .line 835
    :cond_20
    const/4 v0, -0x1

    #@21
    goto :goto_c

    #@22
    .line 828
    :catchall_22
    move-exception v0

    #@23
    monitor-exit p0

    #@24
    throw v0
.end method

.method public declared-synchronized getUserAgentString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 1583
    monitor-enter p0

    #@1
    :try_start_1
    const-string v2, "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.34 Safari/534.24"

    #@3
    iget-object v3, p0, Landroid/webkit/WebSettingsClassic;->mUserAgent:Ljava/lang/String;

    #@5
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8
    move-result v2

    #@9
    if-nez v2, :cond_19

    #@b
    const-string v2, "Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_0 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7A341 Safari/528.16"

    #@d
    iget-object v3, p0, Landroid/webkit/WebSettingsClassic;->mUserAgent:Ljava/lang/String;

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v2

    #@13
    if-nez v2, :cond_19

    #@15
    iget-boolean v2, p0, Landroid/webkit/WebSettingsClassic;->mUseDefaultUserAgent:Z

    #@17
    if-nez v2, :cond_1d

    #@19
    .line 1586
    :cond_19
    iget-object v2, p0, Landroid/webkit/WebSettingsClassic;->mUserAgent:Ljava/lang/String;
    :try_end_1b
    .catchall {:try_start_1 .. :try_end_1b} :catchall_48

    #@1b
    .line 1602
    :goto_1b
    monitor-exit p0

    #@1c
    return-object v2

    #@1d
    .line 1589
    :cond_1d
    const/4 v1, 0x0

    #@1e
    .line 1590
    .local v1, doPostSync:Z
    :try_start_1e
    sget-object v3, Landroid/webkit/WebSettingsClassic;->sLockForLocaleSettings:Ljava/lang/Object;

    #@20
    monitor-enter v3
    :try_end_21
    .catchall {:try_start_1e .. :try_end_21} :catchall_48

    #@21
    .line 1591
    :try_start_21
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@24
    move-result-object v0

    #@25
    .line 1592
    .local v0, currentLocale:Ljava/util/Locale;
    sget-object v2, Landroid/webkit/WebSettingsClassic;->sLocale:Ljava/util/Locale;

    #@27
    invoke-virtual {v2, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    #@2a
    move-result v2

    #@2b
    if-nez v2, :cond_3c

    #@2d
    .line 1593
    sput-object v0, Landroid/webkit/WebSettingsClassic;->sLocale:Ljava/util/Locale;

    #@2f
    .line 1594
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->getCurrentUserAgent()Ljava/lang/String;

    #@32
    move-result-object v2

    #@33
    iput-object v2, p0, Landroid/webkit/WebSettingsClassic;->mUserAgent:Ljava/lang/String;

    #@35
    .line 1595
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->getCurrentAcceptLanguage()Ljava/lang/String;

    #@38
    move-result-object v2

    #@39
    iput-object v2, p0, Landroid/webkit/WebSettingsClassic;->mAcceptLanguage:Ljava/lang/String;

    #@3b
    .line 1596
    const/4 v1, 0x1

    #@3c
    .line 1598
    :cond_3c
    monitor-exit v3
    :try_end_3d
    .catchall {:try_start_21 .. :try_end_3d} :catchall_45

    #@3d
    .line 1599
    if-eqz v1, :cond_42

    #@3f
    .line 1600
    :try_start_3f
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V

    #@42
    .line 1602
    :cond_42
    iget-object v2, p0, Landroid/webkit/WebSettingsClassic;->mUserAgent:Ljava/lang/String;
    :try_end_44
    .catchall {:try_start_3f .. :try_end_44} :catchall_48

    #@44
    goto :goto_1b

    #@45
    .line 1598
    .end local v0           #currentLocale:Ljava/util/Locale;
    :catchall_45
    move-exception v2

    #@46
    :try_start_46
    monitor-exit v3
    :try_end_47
    .catchall {:try_start_46 .. :try_end_47} :catchall_45

    #@47
    :try_start_47
    throw v2
    :try_end_48
    .catchall {:try_start_47 .. :try_end_48} :catchall_48

    #@48
    .line 1583
    .end local v1           #doPostSync:Z
    :catchall_48
    move-exception v2

    #@49
    monitor-exit p0

    #@4a
    throw v2
.end method

.method public declared-synchronized getWebNotiEnabled()I
    .registers 2

    #@0
    .prologue
    .line 1245
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/webkit/WebSettingsClassic;->mWebNotiEnabled:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getWebScrapEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 1990
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mLGWebscrapEnabled:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method isNarrowColumnLayout()Z
    .registers 3

    #@0
    .prologue
    .line 1618
    invoke-virtual {p0}, Landroid/webkit/WebSettingsClassic;->getLayoutAlgorithm()Landroid/webkit/WebSettings$LayoutAlgorithm;

    #@3
    move-result-object v0

    #@4
    sget-object v1, Landroid/webkit/WebSettings$LayoutAlgorithm;->NARROW_COLUMNS:Landroid/webkit/WebSettings$LayoutAlgorithm;

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method isPrivateBrowsingEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 1729
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mPrivateBrowsingEnabled:Z

    #@2
    return v0
.end method

.method declared-synchronized onDestroyed()V
    .registers 1

    #@0
    .prologue
    .line 2105
    monitor-enter p0

    #@1
    monitor-exit p0

    #@2
    return-void
.end method

.method public setAllowContentAccess(Z)V
    .registers 2
    .parameter "allow"

    #@0
    .prologue
    .line 609
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mAllowContentAccess:Z

    #@2
    .line 610
    return-void
.end method

.method public setAllowFileAccess(Z)V
    .registers 2
    .parameter "allow"

    #@0
    .prologue
    .line 593
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mAllowFileAccess:Z

    #@2
    .line 594
    return-void
.end method

.method public declared-synchronized setAllowFileAccessFromFileURLs(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1203
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mAllowFileAccessFromFileURLs:Z

    #@3
    if-eq v0, p1, :cond_a

    #@5
    .line 1204
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mAllowFileAccessFromFileURLs:Z

    #@7
    .line 1205
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 1207
    :cond_a
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 1203
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized setAllowUniversalAccessFromFileURLs(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1192
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mAllowUniversalAccessFromFileURLs:Z

    #@3
    if-eq v0, p1, :cond_a

    #@5
    .line 1193
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mAllowUniversalAccessFromFileURLs:Z

    #@7
    .line 1194
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 1196
    :cond_a
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 1192
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized setAppCacheEnabled(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1324
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mAppCacheEnabled:Z

    #@3
    if-eq v0, p1, :cond_a

    #@5
    .line 1325
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mAppCacheEnabled:Z

    #@7
    .line 1326
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 1328
    :cond_a
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 1324
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized setAppCacheMaxSize(J)V
    .registers 5
    .parameter "appCacheMaxSize"

    #@0
    .prologue
    .line 1348
    monitor-enter p0

    #@1
    :try_start_1
    iget-wide v0, p0, Landroid/webkit/WebSettingsClassic;->mAppCacheMaxSize:J

    #@3
    cmp-long v0, p1, v0

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 1349
    iput-wide p1, p0, Landroid/webkit/WebSettingsClassic;->mAppCacheMaxSize:J

    #@9
    .line 1350
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_e

    #@c
    .line 1352
    :cond_c
    monitor-exit p0

    #@d
    return-void

    #@e
    .line 1348
    :catchall_e
    move-exception v0

    #@f
    monitor-exit p0

    #@10
    throw v0
.end method

.method public declared-synchronized setAppCachePath(Ljava/lang/String;)V
    .registers 3
    .parameter "path"

    #@0
    .prologue
    .line 1337
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mAppCachePath:Ljava/lang/String;

    #@3
    if-nez v0, :cond_12

    #@5
    if-eqz p1, :cond_12

    #@7
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    #@a
    move-result v0

    #@b
    if-nez v0, :cond_12

    #@d
    .line 1338
    iput-object p1, p0, Landroid/webkit/WebSettingsClassic;->mAppCachePath:Ljava/lang/String;

    #@f
    .line 1339
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_14

    #@12
    .line 1341
    :cond_12
    monitor-exit p0

    #@13
    return-void

    #@14
    .line 1337
    :catchall_14
    move-exception v0

    #@15
    monitor-exit p0

    #@16
    throw v0
.end method

.method public declared-synchronized setAutoFillEnabled(Z)V
    .registers 4
    .parameter "enabled"

    #@0
    .prologue
    .line 1785
    monitor-enter p0

    #@1
    if-eqz p1, :cond_13

    #@3
    :try_start_3
    iget-boolean v1, p0, Landroid/webkit/WebSettingsClassic;->mPrivateBrowsingEnabled:Z

    #@5
    if-nez v1, :cond_13

    #@7
    const/4 v0, 0x1

    #@8
    .line 1786
    .local v0, autoFillEnabled:Z
    :goto_8
    iget-boolean v1, p0, Landroid/webkit/WebSettingsClassic;->mAutoFillEnabled:Z

    #@a
    if-eq v1, v0, :cond_11

    #@c
    .line 1787
    iput-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mAutoFillEnabled:Z

    #@e
    .line 1788
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_15

    #@11
    .line 1790
    :cond_11
    monitor-exit p0

    #@12
    return-void

    #@13
    .line 1785
    .end local v0           #autoFillEnabled:Z
    :cond_13
    const/4 v0, 0x0

    #@14
    goto :goto_8

    #@15
    :catchall_15
    move-exception v1

    #@16
    monitor-exit p0

    #@17
    throw v1
.end method

.method public declared-synchronized setAutoFillProfile(Landroid/webkit/WebSettingsClassic$AutoFillProfile;)V
    .registers 3
    .parameter "profile"

    #@0
    .prologue
    .line 1797
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mAutoFillProfile:Landroid/webkit/WebSettingsClassic$AutoFillProfile;

    #@3
    if-eq v0, p1, :cond_a

    #@5
    .line 1798
    iput-object p1, p0, Landroid/webkit/WebSettingsClassic;->mAutoFillProfile:Landroid/webkit/WebSettingsClassic$AutoFillProfile;

    #@7
    .line 1799
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 1801
    :cond_a
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 1797
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized setBlockNetworkImage(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1128
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mBlockNetworkImage:Z

    #@3
    if-eq v0, p1, :cond_a

    #@5
    .line 1129
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mBlockNetworkImage:Z

    #@7
    .line 1130
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 1132
    :cond_a
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 1128
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized setBlockNetworkLoads(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1147
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mBlockNetworkLoads:Z

    #@3
    if-eq v0, p1, :cond_d

    #@5
    .line 1148
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mBlockNetworkLoads:Z

    #@7
    .line 1149
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->verifyNetworkAccess()V

    #@a
    .line 1150
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_f

    #@d
    .line 1152
    :cond_d
    monitor-exit p0

    #@e
    return-void

    #@f
    .line 1147
    :catchall_f
    move-exception v0

    #@10
    monitor-exit p0

    #@11
    throw v0
.end method

.method public setBuiltInZoomControls(Z)V
    .registers 4
    .parameter "enabled"

    #@0
    .prologue
    .line 559
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mBuiltInZoomControls:Z

    #@2
    .line 560
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mWebView:Landroid/webkit/WebViewClassic;

    #@4
    iget-object v1, p0, Landroid/webkit/WebSettingsClassic;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewClassic;->updateMultiTouchSupport(Landroid/content/Context;)V

    #@9
    .line 561
    return-void
.end method

.method public setCacheMode(I)V
    .registers 3
    .parameter "mode"

    #@0
    .prologue
    .line 1653
    iget v0, p0, Landroid/webkit/WebSettingsClassic;->mOverrideCacheMode:I

    #@2
    if-eq p1, v0, :cond_9

    #@4
    .line 1654
    iput p1, p0, Landroid/webkit/WebSettingsClassic;->mOverrideCacheMode:I

    #@6
    .line 1655
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V

    #@9
    .line 1657
    :cond_9
    return-void
.end method

.method public declared-synchronized setCliptrayEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 2060
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mCliptrayEnabled:Z

    #@3
    if-eq v0, p1, :cond_12

    #@5
    .line 2061
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mCliptrayEnabled:Z

    #@7
    .line 2063
    if-eqz p1, :cond_12

    #@9
    .line 2064
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mWebView:Landroid/webkit/WebViewClassic;

    #@b
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->getContext()Landroid/content/Context;

    #@e
    move-result-object v0

    #@f
    invoke-static {v0}, Landroid/webkit/LGCliptrayManager;->initCliptray(Landroid/content/Context;)V
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_14

    #@12
    .line 2067
    :cond_12
    monitor-exit p0

    #@13
    return-void

    #@14
    .line 2060
    :catchall_14
    move-exception v0

    #@15
    monitor-exit p0

    #@16
    throw v0
.end method

.method public declared-synchronized setCursiveFontFamily(Ljava/lang/String;)V
    .registers 3
    .parameter "font"

    #@0
    .prologue
    .line 978
    monitor-enter p0

    #@1
    if-eqz p1, :cond_10

    #@3
    :try_start_3
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mCursiveFontFamily:Ljava/lang/String;

    #@5
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_10

    #@b
    .line 979
    iput-object p1, p0, Landroid/webkit/WebSettingsClassic;->mCursiveFontFamily:Ljava/lang/String;

    #@d
    .line 980
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    #@10
    .line 982
    :cond_10
    monitor-exit p0

    #@11
    return-void

    #@12
    .line 978
    :catchall_12
    move-exception v0

    #@13
    monitor-exit p0

    #@14
    throw v0
.end method

.method public declared-synchronized setDatabaseEnabled(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1359
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mDatabaseEnabled:Z

    #@3
    if-eq v0, p1, :cond_a

    #@5
    .line 1360
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mDatabaseEnabled:Z

    #@7
    .line 1361
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 1363
    :cond_a
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 1359
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized setDatabasePath(Ljava/lang/String;)V
    .registers 3
    .parameter "databasePath"

    #@0
    .prologue
    .line 1300
    monitor-enter p0

    #@1
    if-eqz p1, :cond_f

    #@3
    :try_start_3
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mDatabasePathHasBeenSet:Z

    #@5
    if-nez v0, :cond_f

    #@7
    .line 1301
    iput-object p1, p0, Landroid/webkit/WebSettingsClassic;->mDatabasePath:Ljava/lang/String;

    #@9
    .line 1302
    const/4 v0, 0x1

    #@a
    iput-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mDatabasePathHasBeenSet:Z

    #@c
    .line 1303
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_11

    #@f
    .line 1305
    :cond_f
    monitor-exit p0

    #@10
    return-void

    #@11
    .line 1300
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0

    #@13
    throw v0
.end method

.method public declared-synchronized setDefaultFixedFontSize(I)V
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 1076
    monitor-enter p0

    #@1
    :try_start_1
    invoke-direct {p0, p1}, Landroid/webkit/WebSettingsClassic;->pin(I)I

    #@4
    move-result p1

    #@5
    .line 1077
    iget v0, p0, Landroid/webkit/WebSettingsClassic;->mDefaultFixedFontSize:I

    #@7
    if-eq v0, p1, :cond_e

    #@9
    .line 1078
    iput p1, p0, Landroid/webkit/WebSettingsClassic;->mDefaultFixedFontSize:I

    #@b
    .line 1079
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_10

    #@e
    .line 1081
    :cond_e
    monitor-exit p0

    #@f
    return-void

    #@10
    .line 1076
    :catchall_10
    move-exception v0

    #@11
    monitor-exit p0

    #@12
    throw v0
.end method

.method public declared-synchronized setDefaultFontSize(I)V
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 1056
    monitor-enter p0

    #@1
    :try_start_1
    invoke-direct {p0, p1}, Landroid/webkit/WebSettingsClassic;->pin(I)I

    #@4
    move-result p1

    #@5
    .line 1057
    iget v0, p0, Landroid/webkit/WebSettingsClassic;->mDefaultFontSize:I

    #@7
    if-eq v0, p1, :cond_e

    #@9
    .line 1058
    iput p1, p0, Landroid/webkit/WebSettingsClassic;->mDefaultFontSize:I

    #@b
    .line 1059
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_10

    #@e
    .line 1061
    :cond_e
    monitor-exit p0

    #@f
    return-void

    #@10
    .line 1056
    :catchall_10
    move-exception v0

    #@11
    monitor-exit p0

    #@12
    throw v0
.end method

.method public declared-synchronized setDefaultTextEncodingName(Ljava/lang/String;)V
    .registers 3
    .parameter "encoding"

    #@0
    .prologue
    .line 1521
    monitor-enter p0

    #@1
    if-eqz p1, :cond_10

    #@3
    :try_start_3
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mDefaultTextEncoding:Ljava/lang/String;

    #@5
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_10

    #@b
    .line 1522
    iput-object p1, p0, Landroid/webkit/WebSettingsClassic;->mDefaultTextEncoding:Ljava/lang/String;

    #@d
    .line 1523
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    #@10
    .line 1525
    :cond_10
    monitor-exit p0

    #@11
    return-void

    #@12
    .line 1521
    :catchall_12
    move-exception v0

    #@13
    monitor-exit p0

    #@14
    throw v0
.end method

.method public setDefaultZoom(Landroid/webkit/WebSettings$ZoomDensity;)V
    .registers 4
    .parameter "zoom"

    #@0
    .prologue
    .line 749
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mDefaultZoom:Landroid/webkit/WebSettings$ZoomDensity;

    #@2
    if-eq v0, p1, :cond_d

    #@4
    .line 750
    iput-object p1, p0, Landroid/webkit/WebSettingsClassic;->mDefaultZoom:Landroid/webkit/WebSettings$ZoomDensity;

    #@6
    .line 751
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mWebView:Landroid/webkit/WebViewClassic;

    #@8
    iget v1, p1, Landroid/webkit/WebSettings$ZoomDensity;->value:I

    #@a
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewClassic;->adjustDefaultZoomDensity(I)V

    #@d
    .line 753
    :cond_d
    return-void
.end method

.method public setDisplayZoomControls(Z)V
    .registers 4
    .parameter "enabled"

    #@0
    .prologue
    .line 576
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mDisplayZoomControls:Z

    #@2
    .line 577
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mWebView:Landroid/webkit/WebViewClassic;

    #@4
    iget-object v1, p0, Landroid/webkit/WebSettingsClassic;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewClassic;->updateMultiTouchSupport(Landroid/content/Context;)V

    #@9
    .line 578
    return-void
.end method

.method public declared-synchronized setDomStorageEnabled(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1370
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mDomStorageEnabled:Z

    #@3
    if-eq v0, p1, :cond_a

    #@5
    .line 1371
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mDomStorageEnabled:Z

    #@7
    .line 1372
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 1374
    :cond_a
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 1370
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method setDoubleTapToastCount(I)V
    .registers 5
    .parameter "count"

    #@0
    .prologue
    .line 1812
    sget v0, Landroid/webkit/WebSettingsClassic;->mDoubleTapToastCount:I

    #@2
    if-eq v0, p1, :cond_11

    #@4
    .line 1813
    sput p1, Landroid/webkit/WebSettingsClassic;->mDoubleTapToastCount:I

    #@6
    .line 1815
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mEventHandler:Landroid/webkit/WebSettingsClassic$EventHandler;

    #@8
    const/4 v1, 0x0

    #@9
    const/4 v2, 0x2

    #@a
    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@d
    move-result-object v1

    #@e
    invoke-static {v0, v1}, Landroid/webkit/WebSettingsClassic$EventHandler;->access$800(Landroid/webkit/WebSettingsClassic$EventHandler;Landroid/os/Message;)Z

    #@11
    .line 1818
    :cond_11
    return-void
.end method

.method public setDoubleTapZoom(I)V
    .registers 3
    .parameter "doubleTapZoom"

    #@0
    .prologue
    .line 730
    iget v0, p0, Landroid/webkit/WebSettingsClassic;->mDoubleTapZoom:I

    #@2
    if-eq v0, p1, :cond_b

    #@4
    .line 731
    iput p1, p0, Landroid/webkit/WebSettingsClassic;->mDoubleTapZoom:I

    #@6
    .line 732
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mWebView:Landroid/webkit/WebViewClassic;

    #@8
    invoke-virtual {v0, p1}, Landroid/webkit/WebViewClassic;->updateDoubleTapZoom(I)V

    #@b
    .line 734
    :cond_b
    return-void
.end method

.method public setEnableSmoothTransition(Z)V
    .registers 2
    .parameter "enable"

    #@0
    .prologue
    .line 641
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mEnableSmoothTransition:Z

    #@2
    .line 642
    return-void
.end method

.method public declared-synchronized setFantasyFontFamily(Ljava/lang/String;)V
    .registers 3
    .parameter "font"

    #@0
    .prologue
    .line 997
    monitor-enter p0

    #@1
    if-eqz p1, :cond_10

    #@3
    :try_start_3
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mFantasyFontFamily:Ljava/lang/String;

    #@5
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_10

    #@b
    .line 998
    iput-object p1, p0, Landroid/webkit/WebSettingsClassic;->mFantasyFontFamily:Ljava/lang/String;

    #@d
    .line 999
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    #@10
    .line 1001
    :cond_10
    monitor-exit p0

    #@11
    return-void

    #@12
    .line 997
    :catchall_12
    move-exception v0

    #@13
    monitor-exit p0

    #@14
    throw v0
.end method

.method public declared-synchronized setFixedFontFamily(Ljava/lang/String;)V
    .registers 3
    .parameter "font"

    #@0
    .prologue
    .line 921
    monitor-enter p0

    #@1
    if-eqz p1, :cond_10

    #@3
    :try_start_3
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mFixedFontFamily:Ljava/lang/String;

    #@5
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_10

    #@b
    .line 922
    iput-object p1, p0, Landroid/webkit/WebSettingsClassic;->mFixedFontFamily:Ljava/lang/String;

    #@d
    .line 923
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    #@10
    .line 925
    :cond_10
    monitor-exit p0

    #@11
    return-void

    #@12
    .line 921
    :catchall_12
    move-exception v0

    #@13
    monitor-exit p0

    #@14
    throw v0
.end method

.method public declared-synchronized setFloatingMode(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 1959
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mLGFloatingMode:Z

    #@3
    if-eq v0, p1, :cond_a

    #@5
    .line 1960
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mLGFloatingMode:Z

    #@7
    .line 1961
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 1963
    :cond_a
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 1959
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized setForceUserScalable(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1773
    monitor-enter p0

    #@1
    :try_start_1
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mForceUserScalable:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    .line 1774
    monitor-exit p0

    #@4
    return-void

    #@5
    .line 1773
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized setForceZoomoutEnabled(Z)V
    .registers 3
    .parameter "use"

    #@0
    .prologue
    .line 1675
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mForceZoomoutEnabled:Z

    #@3
    if-eq v0, p1, :cond_a

    #@5
    .line 1676
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mForceZoomoutEnabled:Z

    #@7
    .line 1677
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 1679
    :cond_a
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 1675
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized setGeolocationDatabasePath(Ljava/lang/String;)V
    .registers 3
    .parameter "databasePath"

    #@0
    .prologue
    .line 1312
    monitor-enter p0

    #@1
    if-eqz p1, :cond_10

    #@3
    :try_start_3
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mGeolocationDatabasePath:Ljava/lang/String;

    #@5
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_10

    #@b
    .line 1314
    iput-object p1, p0, Landroid/webkit/WebSettingsClassic;->mGeolocationDatabasePath:Ljava/lang/String;

    #@d
    .line 1315
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    #@10
    .line 1317
    :cond_10
    monitor-exit p0

    #@11
    return-void

    #@12
    .line 1312
    :catchall_12
    move-exception v0

    #@13
    monitor-exit p0

    #@14
    throw v0
.end method

.method public declared-synchronized setGeolocationEnabled(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1418
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mGeolocationEnabled:Z

    #@3
    if-eq v0, p1, :cond_a

    #@5
    .line 1419
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mGeolocationEnabled:Z

    #@7
    .line 1420
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 1422
    :cond_a
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 1418
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized setHardwareAccelSkiaEnabled(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1214
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mHardwareAccelSkia:Z

    #@3
    if-eq v0, p1, :cond_a

    #@5
    .line 1215
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mHardwareAccelSkia:Z

    #@7
    .line 1216
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 1218
    :cond_a
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 1214
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized setJavaScriptCanOpenWindowsAutomatically(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1502
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mJavaScriptCanOpenWindowsAutomatically:Z

    #@3
    if-eq v0, p1, :cond_a

    #@5
    .line 1503
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mJavaScriptCanOpenWindowsAutomatically:Z

    #@7
    .line 1504
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 1506
    :cond_a
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 1502
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized setJavaScriptEnabled(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1180
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mJavaScriptEnabled:Z

    #@3
    if-eq v0, p1, :cond_f

    #@5
    .line 1181
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mJavaScriptEnabled:Z

    #@7
    .line 1182
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V

    #@a
    .line 1183
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mWebView:Landroid/webkit/WebViewClassic;

    #@c
    invoke-virtual {v0, p1}, Landroid/webkit/WebViewClassic;->updateJavaScriptEnabled(Z)V
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_11

    #@f
    .line 1185
    :cond_f
    monitor-exit p0

    #@10
    return-void

    #@11
    .line 1180
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0

    #@13
    throw v0
.end method

.method public declared-synchronized setLGBubbleActionEnabled(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1839
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mLGBubbleAction:Z

    #@3
    if-eq v0, p1, :cond_7

    #@5
    .line 1840
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mLGBubbleAction:Z
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    #@7
    .line 1842
    :cond_7
    monitor-exit p0

    #@8
    return-void

    #@9
    .line 1839
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method public declared-synchronized setLayoutAlgorithm(Landroid/webkit/WebSettings$LayoutAlgorithm;)V
    .registers 3
    .parameter "l"

    #@0
    .prologue
    .line 883
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mLayoutAlgorithm:Landroid/webkit/WebSettings$LayoutAlgorithm;

    #@3
    if-eq v0, p1, :cond_a

    #@5
    .line 884
    iput-object p1, p0, Landroid/webkit/WebSettingsClassic;->mLayoutAlgorithm:Landroid/webkit/WebSettings$LayoutAlgorithm;

    #@7
    .line 885
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 887
    :cond_a
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 883
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public setLightTouchEnabled(Z)V
    .registers 2
    .parameter "enabled"

    #@0
    .prologue
    .line 768
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mLightTouchEnabled:Z

    #@2
    .line 769
    return-void
.end method

.method public declared-synchronized setLinkPrefetchEnabled(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1440
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mLinkPrefetchEnabled:Z

    #@3
    if-eq v0, p1, :cond_a

    #@5
    .line 1441
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mLinkPrefetchEnabled:Z

    #@7
    .line 1442
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 1444
    :cond_a
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 1440
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public setLoadWithOverviewMode(Z)V
    .registers 2
    .parameter "overview"

    #@0
    .prologue
    .line 625
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mLoadWithOverviewMode:Z

    #@2
    .line 626
    return-void
.end method

.method public declared-synchronized setLoadsImagesAutomatically(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1109
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mLoadsImagesAutomatically:Z

    #@3
    if-eq v0, p1, :cond_a

    #@5
    .line 1110
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mLoadsImagesAutomatically:Z

    #@7
    .line 1111
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 1113
    :cond_a
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 1109
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public setMaximumDecodedImageSize(J)V
    .registers 5
    .parameter "size"

    #@0
    .prologue
    .line 1711
    iget-wide v0, p0, Landroid/webkit/WebSettingsClassic;->mMaximumDecodedImageSize:J

    #@2
    cmp-long v0, v0, p1

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 1712
    iput-wide p1, p0, Landroid/webkit/WebSettingsClassic;->mMaximumDecodedImageSize:J

    #@8
    .line 1713
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V

    #@b
    .line 1715
    :cond_b
    return-void
.end method

.method public setMediaPlaybackRequiresUserGesture(Z)V
    .registers 3
    .parameter "support"

    #@0
    .prologue
    .line 540
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mMediaPlaybackRequiresUserGesture:Z

    #@2
    if-eq v0, p1, :cond_9

    #@4
    .line 541
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mMediaPlaybackRequiresUserGesture:Z

    #@6
    .line 542
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V

    #@9
    .line 544
    :cond_9
    return-void
.end method

.method public declared-synchronized setMinimumFontSize(I)V
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 1016
    monitor-enter p0

    #@1
    :try_start_1
    invoke-direct {p0, p1}, Landroid/webkit/WebSettingsClassic;->pin(I)I

    #@4
    move-result p1

    #@5
    .line 1017
    iget v0, p0, Landroid/webkit/WebSettingsClassic;->mMinimumFontSize:I

    #@7
    if-eq v0, p1, :cond_e

    #@9
    .line 1018
    iput p1, p0, Landroid/webkit/WebSettingsClassic;->mMinimumFontSize:I

    #@b
    .line 1019
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_10

    #@e
    .line 1021
    :cond_e
    monitor-exit p0

    #@f
    return-void

    #@10
    .line 1016
    :catchall_10
    move-exception v0

    #@11
    monitor-exit p0

    #@12
    throw v0
.end method

.method public declared-synchronized setMinimumLogicalFontSize(I)V
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 1036
    monitor-enter p0

    #@1
    :try_start_1
    invoke-direct {p0, p1}, Landroid/webkit/WebSettingsClassic;->pin(I)I

    #@4
    move-result p1

    #@5
    .line 1037
    iget v0, p0, Landroid/webkit/WebSettingsClassic;->mMinimumLogicalFontSize:I

    #@7
    if-eq v0, p1, :cond_e

    #@9
    .line 1038
    iput p1, p0, Landroid/webkit/WebSettingsClassic;->mMinimumLogicalFontSize:I

    #@b
    .line 1039
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_10

    #@e
    .line 1041
    :cond_e
    monitor-exit p0

    #@f
    return-void

    #@10
    .line 1036
    :catchall_10
    move-exception v0

    #@11
    monitor-exit p0

    #@12
    throw v0
.end method

.method public setNavDump(Z)V
    .registers 2
    .parameter "enabled"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 506
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mNavDump:Z

    #@2
    .line 507
    return-void
.end method

.method public setNeedInitialFocus(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1626
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mNeedInitialFocus:Z

    #@2
    if-eq v0, p1, :cond_6

    #@4
    .line 1627
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mNeedInitialFocus:Z

    #@6
    .line 1629
    :cond_6
    return-void
.end method

.method public declared-synchronized setPageCacheCapacity(I)V
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 1096
    monitor-enter p0

    #@1
    if-gez p1, :cond_4

    #@3
    const/4 p1, 0x0

    #@4
    .line 1097
    :cond_4
    const/16 v0, 0x14

    #@6
    if-le p1, v0, :cond_a

    #@8
    const/16 p1, 0x14

    #@a
    .line 1098
    :cond_a
    :try_start_a
    iget v0, p0, Landroid/webkit/WebSettingsClassic;->mPageCacheCapacity:I

    #@c
    if-eq v0, p1, :cond_13

    #@e
    .line 1099
    iput p1, p0, Landroid/webkit/WebSettingsClassic;->mPageCacheCapacity:I

    #@10
    .line 1100
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_13
    .catchall {:try_start_a .. :try_end_13} :catchall_15

    #@13
    .line 1102
    :cond_13
    monitor-exit p0

    #@14
    return-void

    #@15
    .line 1096
    :catchall_15
    move-exception v0

    #@16
    monitor-exit p0

    #@17
    throw v0
.end method

.method public declared-synchronized setParagraphCopyEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 1927
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mLGParagraphCopy:Z

    #@3
    if-eq v0, p1, :cond_7

    #@5
    .line 1928
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mLGParagraphCopy:Z
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    #@7
    .line 1930
    :cond_7
    monitor-exit p0

    #@8
    return-void

    #@9
    .line 1927
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method public declared-synchronized setPluginState(Landroid/webkit/WebSettings$PluginState;)V
    .registers 3
    .parameter "state"

    #@0
    .prologue
    .line 1281
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mPluginState:Landroid/webkit/WebSettings$PluginState;

    #@3
    if-eq v0, p1, :cond_a

    #@5
    .line 1282
    iput-object p1, p0, Landroid/webkit/WebSettingsClassic;->mPluginState:Landroid/webkit/WebSettings$PluginState;

    #@7
    .line 1283
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 1285
    :cond_a
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 1281
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized setPluginsEnabled(Z)V
    .registers 3
    .parameter "flag"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1273
    monitor-enter p0

    #@1
    if-eqz p1, :cond_a

    #@3
    :try_start_3
    sget-object v0, Landroid/webkit/WebSettings$PluginState;->ON:Landroid/webkit/WebSettings$PluginState;

    #@5
    :goto_5
    invoke-virtual {p0, v0}, Landroid/webkit/WebSettingsClassic;->setPluginState(Landroid/webkit/WebSettings$PluginState;)V
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_d

    #@8
    .line 1274
    monitor-exit p0

    #@9
    return-void

    #@a
    .line 1273
    :cond_a
    :try_start_a
    sget-object v0, Landroid/webkit/WebSettings$PluginState;->OFF:Landroid/webkit/WebSettings$PluginState;
    :try_end_c
    .catchall {:try_start_a .. :try_end_c} :catchall_d

    #@c
    goto :goto_5

    #@d
    :catchall_d
    move-exception v0

    #@e
    monitor-exit p0

    #@f
    throw v0
.end method

.method public declared-synchronized setPluginsPath(Ljava/lang/String;)V
    .registers 2
    .parameter "pluginsPath"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1293
    monitor-enter p0

    #@1
    monitor-exit p0

    #@2
    return-void
.end method

.method declared-synchronized setPrivateBrowsingEnabled(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1737
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mPrivateBrowsingEnabled:Z

    #@3
    if-eq v0, p1, :cond_f

    #@5
    .line 1738
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mPrivateBrowsingEnabled:Z

    #@7
    .line 1742
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mAutoFillEnabled:Z

    #@9
    invoke-virtual {p0, v0}, Landroid/webkit/WebSettingsClassic;->setAutoFillEnabled(Z)V

    #@c
    .line 1744
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_11

    #@f
    .line 1746
    :cond_f
    monitor-exit p0

    #@10
    return-void

    #@11
    .line 1737
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0

    #@13
    throw v0
.end method

.method public setProperty(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 1821
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mWebView:Landroid/webkit/WebViewClassic;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/webkit/WebViewClassic;->nativeSetProperty(Ljava/lang/String;Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_d

    #@8
    .line 1822
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mWebView:Landroid/webkit/WebViewClassic;

    #@a
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@d
    .line 1824
    :cond_d
    return-void
.end method

.method public declared-synchronized setRenderPriority(Landroid/webkit/WebSettings$RenderPriority;)V
    .registers 5
    .parameter "priority"

    #@0
    .prologue
    .line 1641
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mRenderPriority:Landroid/webkit/WebSettings$RenderPriority;

    #@3
    if-eq v0, p1, :cond_12

    #@5
    .line 1642
    iput-object p1, p0, Landroid/webkit/WebSettingsClassic;->mRenderPriority:Landroid/webkit/WebSettings$RenderPriority;

    #@7
    .line 1643
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mEventHandler:Landroid/webkit/WebSettingsClassic$EventHandler;

    #@9
    const/4 v1, 0x0

    #@a
    const/4 v2, 0x1

    #@b
    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@e
    move-result-object v1

    #@f
    invoke-static {v0, v1}, Landroid/webkit/WebSettingsClassic$EventHandler;->access$800(Landroid/webkit/WebSettingsClassic$EventHandler;Landroid/os/Message;)Z
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_14

    #@12
    .line 1646
    :cond_12
    monitor-exit p0

    #@13
    return-void

    #@14
    .line 1641
    :catchall_14
    move-exception v0

    #@15
    monitor-exit p0

    #@16
    throw v0
.end method

.method public declared-synchronized setSansSerifFontFamily(Ljava/lang/String;)V
    .registers 3
    .parameter "font"

    #@0
    .prologue
    .line 940
    monitor-enter p0

    #@1
    if-eqz p1, :cond_10

    #@3
    :try_start_3
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mSansSerifFontFamily:Ljava/lang/String;

    #@5
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_10

    #@b
    .line 941
    iput-object p1, p0, Landroid/webkit/WebSettingsClassic;->mSansSerifFontFamily:Ljava/lang/String;

    #@d
    .line 942
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    #@10
    .line 944
    :cond_10
    monitor-exit p0

    #@11
    return-void

    #@12
    .line 940
    :catchall_12
    move-exception v0

    #@13
    monitor-exit p0

    #@14
    throw v0
.end method

.method public setSaveFormData(Z)V
    .registers 2
    .parameter "save"

    #@0
    .prologue
    .line 675
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mSaveFormData:Z

    #@2
    .line 676
    return-void
.end method

.method public setSavePassword(Z)V
    .registers 2
    .parameter "save"

    #@0
    .prologue
    .line 691
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mSavePassword:Z

    #@2
    .line 692
    return-void
.end method

.method public declared-synchronized setScrollToTopBottomEnabled(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 2000
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mLGScrollToTopBottomEnabled:Z

    #@3
    if-eq v0, p1, :cond_7

    #@5
    .line 2001
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mLGScrollToTopBottomEnabled:Z
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    #@7
    .line 2003
    :cond_7
    monitor-exit p0

    #@8
    return-void

    #@9
    .line 2000
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method public declared-synchronized setSerifFontFamily(Ljava/lang/String;)V
    .registers 3
    .parameter "font"

    #@0
    .prologue
    .line 959
    monitor-enter p0

    #@1
    if-eqz p1, :cond_10

    #@3
    :try_start_3
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mSerifFontFamily:Ljava/lang/String;

    #@5
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_10

    #@b
    .line 960
    iput-object p1, p0, Landroid/webkit/WebSettingsClassic;->mSerifFontFamily:Ljava/lang/String;

    #@d
    .line 961
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    #@10
    .line 963
    :cond_10
    monitor-exit p0

    #@11
    return-void

    #@12
    .line 959
    :catchall_12
    move-exception v0

    #@13
    monitor-exit p0

    #@14
    throw v0
.end method

.method public declared-synchronized setShowVisualIndicator(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1254
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mShowVisualIndicator:Z

    #@3
    if-eq v0, p1, :cond_a

    #@5
    .line 1255
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mShowVisualIndicator:Z

    #@7
    .line 1256
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 1258
    :cond_a
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 1254
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public setShrinksStandaloneImagesToFit(Z)V
    .registers 3
    .parameter "shrink"

    #@0
    .prologue
    .line 1699
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mShrinksStandaloneImagesToFit:Z

    #@2
    if-eq v0, p1, :cond_9

    #@4
    .line 1700
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mShrinksStandaloneImagesToFit:Z

    #@6
    .line 1701
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V

    #@9
    .line 1703
    :cond_9
    return-void
.end method

.method public declared-synchronized setStandardFontFamily(Ljava/lang/String;)V
    .registers 3
    .parameter "font"

    #@0
    .prologue
    .line 902
    monitor-enter p0

    #@1
    if-eqz p1, :cond_10

    #@3
    :try_start_3
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mStandardFontFamily:Ljava/lang/String;

    #@5
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_10

    #@b
    .line 903
    iput-object p1, p0, Landroid/webkit/WebSettingsClassic;->mStandardFontFamily:Ljava/lang/String;

    #@d
    .line 904
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    #@10
    .line 906
    :cond_10
    monitor-exit p0

    #@11
    return-void

    #@12
    .line 902
    :catchall_12
    move-exception v0

    #@13
    monitor-exit p0

    #@14
    throw v0
.end method

.method public declared-synchronized setSupportMultipleWindows(Z)V
    .registers 3
    .parameter "support"

    #@0
    .prologue
    .line 862
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mSupportMultipleWindows:Z

    #@3
    if-eq v0, p1, :cond_a

    #@5
    .line 863
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mSupportMultipleWindows:Z

    #@7
    .line 864
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 866
    :cond_a
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 862
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public setSupportZoom(Z)V
    .registers 4
    .parameter "support"

    #@0
    .prologue
    .line 523
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mSupportZoom:Z

    #@2
    .line 524
    iget-object v0, p0, Landroid/webkit/WebSettingsClassic;->mWebView:Landroid/webkit/WebViewClassic;

    #@4
    iget-object v1, p0, Landroid/webkit/WebSettingsClassic;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewClassic;->updateMultiTouchSupport(Landroid/content/Context;)V

    #@9
    .line 525
    return-void
.end method

.method declared-synchronized setSyntheticLinksEnabled(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1777
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mSyntheticLinksEnabled:Z

    #@3
    if-eq v0, p1, :cond_a

    #@5
    .line 1778
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mSyntheticLinksEnabled:Z

    #@7
    .line 1779
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 1781
    :cond_a
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 1777
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized setTextDragDropEnabled(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 2020
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mLGTextDragDropEnabled:Z

    #@3
    if-eq v0, p1, :cond_7

    #@5
    .line 2021
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mLGTextDragDropEnabled:Z
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    #@7
    .line 2023
    :cond_7
    monitor-exit p0

    #@8
    return-void

    #@9
    .line 2020
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method public declared-synchronized setTextZoom(I)V
    .registers 6
    .parameter "textZoom"

    #@0
    .prologue
    .line 707
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/webkit/WebSettingsClassic;->mTextSize:I

    #@3
    if-eq v0, p1, :cond_27

    #@5
    .line 708
    sget-boolean v0, Landroid/webkit/WebViewClassic;->mLogEvent:Z

    #@7
    if-eqz v0, :cond_22

    #@9
    .line 709
    const v0, 0x11207

    #@c
    const/4 v1, 0x2

    #@d
    new-array v1, v1, [Ljava/lang/Object;

    #@f
    const/4 v2, 0x0

    #@10
    iget v3, p0, Landroid/webkit/WebSettingsClassic;->mTextSize:I

    #@12
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@15
    move-result-object v3

    #@16
    aput-object v3, v1, v2

    #@18
    const/4 v2, 0x1

    #@19
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1c
    move-result-object v3

    #@1d
    aput-object v3, v1, v2

    #@1f
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@22
    .line 712
    :cond_22
    iput p1, p0, Landroid/webkit/WebSettingsClassic;->mTextSize:I

    #@24
    .line 713
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_27
    .catchall {:try_start_1 .. :try_end_27} :catchall_29

    #@27
    .line 715
    :cond_27
    monitor-exit p0

    #@28
    return-void

    #@29
    .line 707
    :catchall_29
    move-exception v0

    #@2a
    monitor-exit p0

    #@2b
    throw v0
.end method

.method public declared-synchronized setTranslateEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 2040
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mLGTranslateEnabled:Z

    #@3
    if-eq v0, p1, :cond_7

    #@5
    .line 2041
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mLGTranslateEnabled:Z
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    #@7
    .line 2043
    :cond_7
    monitor-exit p0

    #@8
    return-void

    #@9
    .line 2040
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method public declared-synchronized setUAProfile(Ljava/lang/String;)V
    .registers 3
    .parameter "uaProf"

    #@0
    .prologue
    .line 1887
    monitor-enter p0

    #@1
    :try_start_1
    iput-object p1, p0, Landroid/webkit/WebSettingsClassic;->mUAProfile:Ljava/lang/String;

    #@3
    .line 1888
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    #@6
    .line 1889
    monitor-exit p0

    #@7
    return-void

    #@8
    .line 1887
    :catchall_8
    move-exception v0

    #@9
    monitor-exit p0

    #@a
    throw v0
.end method

.method public declared-synchronized setUseDoubleTree(Z)V
    .registers 2
    .parameter "use"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 785
    monitor-enter p0

    #@1
    monitor-exit p0

    #@2
    return-void
.end method

.method public setUseWebViewBackgroundForOverscrollBackground(Z)V
    .registers 2
    .parameter "view"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 658
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mUseWebViewBackgroundForOverscroll:Z

    #@2
    .line 659
    return-void
.end method

.method public declared-synchronized setUseWideViewPort(Z)V
    .registers 3
    .parameter "use"

    #@0
    .prologue
    .line 843
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mUseWideViewport:Z

    #@3
    if-eq v0, p1, :cond_a

    #@5
    .line 844
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mUseWideViewport:Z

    #@7
    .line 845
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 847
    :cond_a
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 843
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized setUserAgent(I)V
    .registers 5
    .parameter "ua"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 803
    monitor-enter p0

    #@1
    const/4 v0, 0x0

    #@2
    .line 804
    .local v0, uaString:Ljava/lang/String;
    const/4 v1, 0x1

    #@3
    if-ne p1, v1, :cond_1a

    #@5
    .line 805
    :try_start_5
    const-string v1, "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.34 Safari/534.24"

    #@7
    iget-object v2, p0, Landroid/webkit/WebSettingsClassic;->mUserAgent:Ljava/lang/String;

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_c
    .catchall {:try_start_5 .. :try_end_c} :catchall_17

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_11

    #@f
    .line 820
    :cond_f
    :goto_f
    monitor-exit p0

    #@10
    return-void

    #@11
    .line 808
    :cond_11
    :try_start_11
    const-string v0, "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.34 Safari/534.24"

    #@13
    .line 819
    :cond_13
    :goto_13
    invoke-virtual {p0, v0}, Landroid/webkit/WebSettingsClassic;->setUserAgentString(Ljava/lang/String;)V
    :try_end_16
    .catchall {:try_start_11 .. :try_end_16} :catchall_17

    #@16
    goto :goto_f

    #@17
    .line 803
    :catchall_17
    move-exception v1

    #@18
    monitor-exit p0

    #@19
    throw v1

    #@1a
    .line 810
    :cond_1a
    const/4 v1, 0x2

    #@1b
    if-ne p1, v1, :cond_2a

    #@1d
    .line 811
    :try_start_1d
    const-string v1, "Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_0 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7A341 Safari/528.16"

    #@1f
    iget-object v2, p0, Landroid/webkit/WebSettingsClassic;->mUserAgent:Ljava/lang/String;

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v1

    #@25
    if-nez v1, :cond_f

    #@27
    .line 814
    const-string v0, "Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_0 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7A341 Safari/528.16"
    :try_end_29
    .catchall {:try_start_1d .. :try_end_29} :catchall_17

    #@29
    goto :goto_13

    #@2a
    .line 816
    :cond_2a
    if-eqz p1, :cond_13

    #@2c
    goto :goto_f
.end method

.method public declared-synchronized setUserAgentString(Ljava/lang/String;)V
    .registers 5
    .parameter "ua"

    #@0
    .prologue
    .line 1540
    monitor-enter p0

    #@1
    if-eqz p1, :cond_9

    #@3
    :try_start_3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@6
    move-result v1

    #@7
    if-nez v1, :cond_4b

    #@9
    .line 1541
    :cond_9
    sget-object v2, Landroid/webkit/WebSettingsClassic;->sLockForLocaleSettings:Ljava/lang/Object;

    #@b
    monitor-enter v2
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_48

    #@c
    .line 1542
    :try_start_c
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@f
    move-result-object v0

    #@10
    .line 1543
    .local v0, currentLocale:Ljava/util/Locale;
    sget-object v1, Landroid/webkit/WebSettingsClassic;->sLocale:Ljava/util/Locale;

    #@12
    invoke-virtual {v1, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v1

    #@16
    if-nez v1, :cond_20

    #@18
    .line 1544
    sput-object v0, Landroid/webkit/WebSettingsClassic;->sLocale:Ljava/util/Locale;

    #@1a
    .line 1545
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->getCurrentAcceptLanguage()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    iput-object v1, p0, Landroid/webkit/WebSettingsClassic;->mAcceptLanguage:Ljava/lang/String;

    #@20
    .line 1547
    :cond_20
    monitor-exit v2
    :try_end_21
    .catchall {:try_start_c .. :try_end_21} :catchall_45

    #@21
    .line 1548
    :try_start_21
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->getCurrentUserAgent()Ljava/lang/String;

    #@24
    move-result-object p1

    #@25
    .line 1549
    const/4 v1, 0x1

    #@26
    iput-boolean v1, p0, Landroid/webkit/WebSettingsClassic;->mUseDefaultUserAgent:Z

    #@28
    .line 1563
    :goto_28
    iget-object v1, p0, Landroid/webkit/WebSettingsClassic;->mUserAgent:Ljava/lang/String;

    #@2a
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d
    move-result v1

    #@2e
    if-nez v1, :cond_43

    #@30
    .line 1564
    iput-object p1, p0, Landroid/webkit/WebSettingsClassic;->mUserAgent:Ljava/lang/String;

    #@32
    .line 1568
    iget-object v1, p0, Landroid/webkit/WebSettingsClassic;->mUserAgent:Ljava/lang/String;

    #@34
    const-string v2, "Mobile"

    #@36
    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@39
    move-result v1

    #@3a
    if-nez v1, :cond_6a

    #@3c
    .line 1569
    const/4 v1, 0x0

    #@3d
    invoke-direct {p0, v1}, Landroid/webkit/WebSettingsClassic;->setUAProfileEnabled(Z)V

    #@40
    .line 1574
    :goto_40
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_43
    .catchall {:try_start_21 .. :try_end_43} :catchall_48

    #@43
    .line 1576
    :cond_43
    monitor-exit p0

    #@44
    return-void

    #@45
    .line 1547
    .end local v0           #currentLocale:Ljava/util/Locale;
    :catchall_45
    move-exception v1

    #@46
    :try_start_46
    monitor-exit v2
    :try_end_47
    .catchall {:try_start_46 .. :try_end_47} :catchall_45

    #@47
    :try_start_47
    throw v1
    :try_end_48
    .catchall {:try_start_47 .. :try_end_48} :catchall_48

    #@48
    .line 1540
    :catchall_48
    move-exception v1

    #@49
    monitor-exit p0

    #@4a
    throw v1

    #@4b
    .line 1552
    :cond_4b
    :try_start_4b
    sget-object v2, Landroid/webkit/WebSettingsClassic;->sLockForLocaleSettings:Ljava/lang/Object;

    #@4d
    monitor-enter v2
    :try_end_4e
    .catchall {:try_start_4b .. :try_end_4e} :catchall_48

    #@4e
    .line 1553
    :try_start_4e
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@51
    move-result-object v0

    #@52
    .line 1554
    .restart local v0       #currentLocale:Ljava/util/Locale;
    sget-object v1, Landroid/webkit/WebSettingsClassic;->sLocale:Ljava/util/Locale;

    #@54
    invoke-virtual {v1, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    #@57
    move-result v1

    #@58
    if-nez v1, :cond_62

    #@5a
    .line 1555
    sput-object v0, Landroid/webkit/WebSettingsClassic;->sLocale:Ljava/util/Locale;

    #@5c
    .line 1556
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->getCurrentAcceptLanguage()Ljava/lang/String;

    #@5f
    move-result-object v1

    #@60
    iput-object v1, p0, Landroid/webkit/WebSettingsClassic;->mAcceptLanguage:Ljava/lang/String;

    #@62
    .line 1558
    :cond_62
    monitor-exit v2
    :try_end_63
    .catchall {:try_start_4e .. :try_end_63} :catchall_67

    #@63
    .line 1560
    const/4 v1, 0x0

    #@64
    :try_start_64
    iput-boolean v1, p0, Landroid/webkit/WebSettingsClassic;->mUseDefaultUserAgent:Z
    :try_end_66
    .catchall {:try_start_64 .. :try_end_66} :catchall_48

    #@66
    goto :goto_28

    #@67
    .line 1558
    .end local v0           #currentLocale:Ljava/util/Locale;
    :catchall_67
    move-exception v1

    #@68
    :try_start_68
    monitor-exit v2
    :try_end_69
    .catchall {:try_start_68 .. :try_end_69} :catchall_67

    #@69
    :try_start_69
    throw v1

    #@6a
    .line 1571
    .restart local v0       #currentLocale:Ljava/util/Locale;
    :cond_6a
    const/4 v1, 0x1

    #@6b
    invoke-direct {p0, v1}, Landroid/webkit/WebSettingsClassic;->setUAProfileEnabled(Z)V
    :try_end_6e
    .catchall {:try_start_69 .. :try_end_6e} :catchall_48

    #@6e
    goto :goto_40
.end method

.method public declared-synchronized setWOFFEnabled(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1946
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mWOFFEnabled:Z

    #@3
    if-eq v0, p1, :cond_a

    #@5
    .line 1947
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mWOFFEnabled:Z

    #@7
    .line 1948
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 1950
    :cond_a
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 1946
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized setWebGLEnabled(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1761
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mWebGLEnabled:Z

    #@3
    if-eq v0, p1, :cond_a

    #@5
    .line 1762
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mWebGLEnabled:Z

    #@7
    .line 1763
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 1765
    :cond_a
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 1761
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized setWebNotiEnabled(I)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 1238
    monitor-enter p0

    #@1
    :try_start_1
    iput p1, p0, Landroid/webkit/WebSettingsClassic;->mWebNotiEnabled:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    .line 1239
    monitor-exit p0

    #@4
    return-void

    #@5
    .line 1238
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized setWebScrapEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 1980
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mLGWebscrapEnabled:Z

    #@3
    if-eq v0, p1, :cond_7

    #@5
    .line 1981
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mLGWebscrapEnabled:Z
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    #@7
    .line 1983
    :cond_7
    monitor-exit p0

    #@8
    return-void

    #@9
    .line 1980
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method public declared-synchronized setWorkersEnabled(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1407
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mWorkersEnabled:Z

    #@3
    if-eq v0, p1, :cond_a

    #@5
    .line 1408
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mWorkersEnabled:Z

    #@7
    .line 1409
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 1411
    :cond_a
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 1407
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized setXSSAuditorEnabled(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1430
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mXSSAuditorEnabled:Z

    #@3
    if-eq v0, p1, :cond_a

    #@5
    .line 1431
    iput-boolean p1, p0, Landroid/webkit/WebSettingsClassic;->mXSSAuditorEnabled:Z

    #@7
    .line 1432
    invoke-direct {p0}, Landroid/webkit/WebSettingsClassic;->postSync()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 1434
    :cond_a
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 1430
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized supportMultipleWindows()Z
    .registers 2

    #@0
    .prologue
    .line 873
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mSupportMultipleWindows:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public supportZoom()Z
    .registers 2

    #@0
    .prologue
    .line 532
    iget-boolean v0, p0, Landroid/webkit/WebSettingsClassic;->mSupportZoom:Z

    #@2
    return v0
.end method

.method declared-synchronized syncSettingsAndCreateHandler(Landroid/webkit/BrowserFrame;)V
    .registers 6
    .parameter "frame"

    #@0
    .prologue
    .line 2084
    monitor-enter p0

    #@1
    :try_start_1
    iput-object p1, p0, Landroid/webkit/WebSettingsClassic;->mBrowserFrame:Landroid/webkit/BrowserFrame;

    #@3
    .line 2089
    iget-object v1, p0, Landroid/webkit/WebSettingsClassic;->mContext:Landroid/content/Context;

    #@5
    const-string v2, "WebViewSettings"

    #@7
    const/4 v3, 0x0

    #@8
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@b
    move-result-object v0

    #@c
    .line 2091
    .local v0, sp:Landroid/content/SharedPreferences;
    sget v1, Landroid/webkit/WebSettingsClassic;->mDoubleTapToastCount:I

    #@e
    if-lez v1, :cond_1a

    #@10
    .line 2092
    const-string v1, "double_tap_toast_count"

    #@12
    sget v2, Landroid/webkit/WebSettingsClassic;->mDoubleTapToastCount:I

    #@14
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    #@17
    move-result v1

    #@18
    sput v1, Landroid/webkit/WebSettingsClassic;->mDoubleTapToastCount:I

    #@1a
    .line 2095
    :cond_1a
    iget v1, p1, Landroid/webkit/BrowserFrame;->mNativeFrame:I

    #@1c
    invoke-direct {p0, v1}, Landroid/webkit/WebSettingsClassic;->nativeSync(I)V

    #@1f
    .line 2096
    const/4 v1, 0x0

    #@20
    iput-boolean v1, p0, Landroid/webkit/WebSettingsClassic;->mSyncPending:Z

    #@22
    .line 2097
    iget-object v1, p0, Landroid/webkit/WebSettingsClassic;->mEventHandler:Landroid/webkit/WebSettingsClassic$EventHandler;

    #@24
    invoke-static {v1}, Landroid/webkit/WebSettingsClassic$EventHandler;->access$900(Landroid/webkit/WebSettingsClassic$EventHandler;)V
    :try_end_27
    .catchall {:try_start_1 .. :try_end_27} :catchall_29

    #@27
    .line 2098
    monitor-exit p0

    #@28
    return-void

    #@29
    .line 2084
    .end local v0           #sp:Landroid/content/SharedPreferences;
    :catchall_29
    move-exception v1

    #@2a
    monitor-exit p0

    #@2b
    throw v1
.end method
