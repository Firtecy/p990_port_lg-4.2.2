.class Landroid/webkit/SelectActionModeCallback;
.super Ljava/lang/Object;
.source "SelectActionModeCallback.java"

# interfaces
.implements Landroid/view/ActionMode$Callback;


# instance fields
.field private mActionMode:Landroid/view/ActionMode;

.field mIsEditable:Z

.field private mIsTextSelected:Z

.field private mWebView:Landroid/webkit/WebViewClassic;


# direct methods
.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 29
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 32
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/webkit/SelectActionModeCallback;->mIsTextSelected:Z

    #@6
    .line 33
    const/4 v0, 0x0

    #@7
    iput-boolean v0, p0, Landroid/webkit/SelectActionModeCallback;->mIsEditable:Z

    #@9
    return-void
.end method

.method private setMenuVisibility(Landroid/view/Menu;ZI)V
    .registers 5
    .parameter "menu"
    .parameter "visible"
    .parameter "resourceId"

    #@0
    .prologue
    .line 152
    invoke-interface {p1, p3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    #@3
    move-result-object v0

    #@4
    .line 153
    .local v0, item:Landroid/view/MenuItem;
    if-eqz v0, :cond_9

    #@6
    .line 154
    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    #@9
    .line 156
    :cond_9
    return-void
.end method


# virtual methods
.method finish()V
    .registers 2

    #@0
    .prologue
    .line 46
    iget-object v0, p0, Landroid/webkit/SelectActionModeCallback;->mActionMode:Landroid/view/ActionMode;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 47
    iget-object v0, p0, Landroid/webkit/SelectActionModeCallback;->mActionMode:Landroid/view/ActionMode;

    #@6
    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    #@9
    .line 49
    :cond_9
    return-void
.end method

.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .registers 10
    .parameter "mode"
    .parameter "item"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 92
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    #@5
    move-result v6

    #@6
    sparse-switch v6, :sswitch_data_96

    #@9
    .line 143
    :goto_9
    return v4

    #@a
    .line 94
    :sswitch_a
    iget-object v4, p0, Landroid/webkit/SelectActionModeCallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@c
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->cutSelection()V

    #@f
    .line 95
    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    #@12
    :goto_12
    move v4, v5

    #@13
    .line 143
    goto :goto_9

    #@14
    .line 99
    :sswitch_14
    iget-object v4, p0, Landroid/webkit/SelectActionModeCallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@16
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->copySelection()Z

    #@19
    .line 100
    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    #@1c
    goto :goto_12

    #@1d
    .line 104
    :sswitch_1d
    iget-object v4, p0, Landroid/webkit/SelectActionModeCallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@1f
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->pasteFromClipboard()V

    #@22
    .line 105
    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    #@25
    goto :goto_12

    #@26
    .line 109
    :sswitch_26
    iget-object v4, p0, Landroid/webkit/SelectActionModeCallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@28
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getSelection()Ljava/lang/String;

    #@2b
    move-result-object v3

    #@2c
    .line 111
    .local v3, selection:Ljava/lang/String;
    iget-object v4, p0, Landroid/webkit/SelectActionModeCallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@2e
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getContext()Landroid/content/Context;

    #@31
    move-result-object v0

    #@32
    .line 112
    .local v0, c:Landroid/content/Context;
    if-eqz v0, :cond_3f

    #@34
    instance-of v4, v0, Landroid/app/Activity;

    #@36
    if-eqz v4, :cond_3f

    #@38
    .line 113
    invoke-static {v0, v3}, Landroid/provider/Browser;->sendStringEx(Landroid/content/Context;Ljava/lang/String;)V

    #@3b
    .line 117
    :goto_3b
    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    #@3e
    goto :goto_12

    #@3f
    .line 116
    :cond_3f
    iget-object v4, p0, Landroid/webkit/SelectActionModeCallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@41
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getContext()Landroid/content/Context;

    #@44
    move-result-object v4

    #@45
    invoke-static {v4, v3}, Landroid/provider/Browser;->sendString(Landroid/content/Context;Ljava/lang/String;)V

    #@48
    goto :goto_3b

    #@49
    .line 121
    .end local v0           #c:Landroid/content/Context;
    .end local v3           #selection:Ljava/lang/String;
    :sswitch_49
    iget-object v4, p0, Landroid/webkit/SelectActionModeCallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@4b
    iget-boolean v6, p0, Landroid/webkit/SelectActionModeCallback;->mIsEditable:Z

    #@4d
    invoke-virtual {v4, v6}, Landroid/webkit/WebViewClassic;->selectAll(Z)V

    #@50
    goto :goto_12

    #@51
    .line 125
    :sswitch_51
    iget-object v6, p0, Landroid/webkit/SelectActionModeCallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@53
    invoke-virtual {v6}, Landroid/webkit/WebViewClassic;->getSelection()Ljava/lang/String;

    #@56
    move-result-object v2

    #@57
    .line 126
    .local v2, sel:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    #@5a
    .line 127
    iget-object v6, p0, Landroid/webkit/SelectActionModeCallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@5c
    invoke-virtual {v6, v2, v4}, Landroid/webkit/WebViewClassic;->showFindDialog(Ljava/lang/String;Z)Z

    #@5f
    goto :goto_12

    #@60
    .line 130
    .end local v2           #sel:Ljava/lang/String;
    :sswitch_60
    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    #@63
    .line 131
    new-instance v1, Landroid/content/Intent;

    #@65
    const-string v4, "android.intent.action.WEB_SEARCH"

    #@67
    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@6a
    .line 132
    .local v1, i:Landroid/content/Intent;
    const-string/jumbo v4, "new_search"

    #@6d
    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@70
    .line 133
    const-string/jumbo v4, "query"

    #@73
    iget-object v6, p0, Landroid/webkit/SelectActionModeCallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@75
    invoke-virtual {v6}, Landroid/webkit/WebViewClassic;->getSelection()Ljava/lang/String;

    #@78
    move-result-object v6

    #@79
    invoke-virtual {v1, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@7c
    .line 134
    iget-object v4, p0, Landroid/webkit/SelectActionModeCallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@7e
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getContext()Landroid/content/Context;

    #@81
    move-result-object v4

    #@82
    instance-of v4, v4, Landroid/app/Activity;

    #@84
    if-nez v4, :cond_8b

    #@86
    .line 135
    const/high16 v4, 0x1000

    #@88
    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@8b
    .line 137
    :cond_8b
    iget-object v4, p0, Landroid/webkit/SelectActionModeCallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@8d
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getContext()Landroid/content/Context;

    #@90
    move-result-object v4

    #@91
    invoke-virtual {v4, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@94
    goto/16 :goto_12

    #@96
    .line 92
    :sswitch_data_96
    .sparse-switch
        0x1020020 -> :sswitch_a
        0x1020021 -> :sswitch_14
        0x1020022 -> :sswitch_1d
        0x10203ca -> :sswitch_49
        0x10203cb -> :sswitch_26
        0x10203cc -> :sswitch_51
        0x10203cd -> :sswitch_60
    .end sparse-switch
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .registers 15
    .parameter "mode"
    .parameter "menu"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v10, 0x1

    #@2
    .line 55
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    #@5
    move-result-object v9

    #@6
    const/high16 v11, 0x114

    #@8
    invoke-virtual {v9, v11, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    #@b
    .line 57
    iget-object v9, p0, Landroid/webkit/SelectActionModeCallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@d
    invoke-virtual {v9}, Landroid/webkit/WebViewClassic;->getContext()Landroid/content/Context;

    #@10
    move-result-object v6

    #@11
    .line 58
    .local v6, context:Landroid/content/Context;
    const v9, 0x10403eb

    #@14
    invoke-virtual {v6, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@17
    move-result-object v9

    #@18
    invoke-virtual {p1, v9}, Landroid/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    #@1b
    .line 59
    invoke-virtual {p1, v10}, Landroid/view/ActionMode;->setTitleOptionalHint(Z)V

    #@1e
    .line 66
    const-string v9, "clipboard"

    #@20
    invoke-virtual {v6, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@23
    move-result-object v9

    #@24
    check-cast v9, Landroid/content/ClipboardManager;

    #@26
    move-object v5, v9

    #@27
    check-cast v5, Landroid/content/ClipboardManager;

    #@29
    .line 68
    .local v5, cm:Landroid/content/ClipboardManager;
    invoke-virtual {p1}, Landroid/view/ActionMode;->isUiFocusable()Z

    #@2c
    move-result v8

    #@2d
    .line 69
    .local v8, isFocusable:Z
    iget-object v9, p0, Landroid/webkit/SelectActionModeCallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@2f
    invoke-virtual {v9}, Landroid/webkit/WebViewClassic;->focusCandidateIsEditableText()Z

    #@32
    move-result v7

    #@33
    .line 70
    .local v7, isEditable:Z
    if-eqz v7, :cond_73

    #@35
    invoke-virtual {v5}, Landroid/content/ClipboardManager;->hasPrimaryClip()Z

    #@38
    move-result v9

    #@39
    if-eqz v9, :cond_73

    #@3b
    if-eqz v8, :cond_73

    #@3d
    move v3, v10

    #@3e
    .line 71
    .local v3, canPaste:Z
    :goto_3e
    if-nez v7, :cond_75

    #@40
    if-eqz v8, :cond_75

    #@42
    move v2, v10

    #@43
    .line 72
    .local v2, canFind:Z
    :goto_43
    if-eqz v7, :cond_4c

    #@45
    iget-boolean v9, p0, Landroid/webkit/SelectActionModeCallback;->mIsTextSelected:Z

    #@47
    if-eqz v9, :cond_4c

    #@49
    if-eqz v8, :cond_4c

    #@4b
    move v1, v10

    #@4c
    .line 73
    .local v1, canCut:Z
    :cond_4c
    iget-boolean v0, p0, Landroid/webkit/SelectActionModeCallback;->mIsTextSelected:Z

    #@4e
    .line 74
    .local v0, canCopy:Z
    iget-boolean v4, p0, Landroid/webkit/SelectActionModeCallback;->mIsTextSelected:Z

    #@50
    .line 75
    .local v4, canWebSearch:Z
    const v9, 0x10203cc

    #@53
    invoke-direct {p0, p2, v2, v9}, Landroid/webkit/SelectActionModeCallback;->setMenuVisibility(Landroid/view/Menu;ZI)V

    #@56
    .line 76
    const v9, 0x1020022

    #@59
    invoke-direct {p0, p2, v3, v9}, Landroid/webkit/SelectActionModeCallback;->setMenuVisibility(Landroid/view/Menu;ZI)V

    #@5c
    .line 77
    const v9, 0x1020020

    #@5f
    invoke-direct {p0, p2, v1, v9}, Landroid/webkit/SelectActionModeCallback;->setMenuVisibility(Landroid/view/Menu;ZI)V

    #@62
    .line 78
    const v9, 0x1020021

    #@65
    invoke-direct {p0, p2, v0, v9}, Landroid/webkit/SelectActionModeCallback;->setMenuVisibility(Landroid/view/Menu;ZI)V

    #@68
    .line 79
    const v9, 0x10203cd

    #@6b
    invoke-direct {p0, p2, v4, v9}, Landroid/webkit/SelectActionModeCallback;->setMenuVisibility(Landroid/view/Menu;ZI)V

    #@6e
    .line 80
    iput-object p1, p0, Landroid/webkit/SelectActionModeCallback;->mActionMode:Landroid/view/ActionMode;

    #@70
    .line 81
    iput-boolean v7, p0, Landroid/webkit/SelectActionModeCallback;->mIsEditable:Z

    #@72
    .line 82
    return v10

    #@73
    .end local v0           #canCopy:Z
    .end local v1           #canCut:Z
    .end local v2           #canFind:Z
    .end local v3           #canPaste:Z
    .end local v4           #canWebSearch:Z
    :cond_73
    move v3, v1

    #@74
    .line 70
    goto :goto_3e

    #@75
    .restart local v3       #canPaste:Z
    :cond_75
    move v2, v1

    #@76
    .line 71
    goto :goto_43
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .registers 3
    .parameter "mode"

    #@0
    .prologue
    .line 148
    iget-object v0, p0, Landroid/webkit/SelectActionModeCallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@2
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->selectionDone()V

    #@5
    .line 149
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .registers 4
    .parameter "mode"
    .parameter "menu"

    #@0
    .prologue
    .line 87
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method setTextSelected(Z)V
    .registers 2
    .parameter "isTextSelected"

    #@0
    .prologue
    .line 40
    iput-boolean p1, p0, Landroid/webkit/SelectActionModeCallback;->mIsTextSelected:Z

    #@2
    .line 41
    return-void
.end method

.method setWebView(Landroid/webkit/WebViewClassic;)V
    .registers 2
    .parameter "webView"

    #@0
    .prologue
    .line 36
    iput-object p1, p0, Landroid/webkit/SelectActionModeCallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@2
    .line 37
    return-void
.end method
