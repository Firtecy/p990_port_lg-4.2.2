.class Landroid/webkit/WebViewClassic$6;
.super Ljava/lang/Object;
.source "WebViewClassic.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/webkit/WebViewClassic;->savePicture(Landroid/os/Bundle;Ljava/io/File;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/webkit/WebViewClassic;

.field final synthetic val$dest:Ljava/io/File;

.field final synthetic val$p:Landroid/graphics/Picture;

.field final synthetic val$temp:Ljava/io/File;


# direct methods
.method constructor <init>(Landroid/webkit/WebViewClassic;Ljava/io/File;Landroid/graphics/Picture;Ljava/io/File;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 2774
    iput-object p1, p0, Landroid/webkit/WebViewClassic$6;->this$0:Landroid/webkit/WebViewClassic;

    #@2
    iput-object p2, p0, Landroid/webkit/WebViewClassic$6;->val$temp:Ljava/io/File;

    #@4
    iput-object p3, p0, Landroid/webkit/WebViewClassic$6;->val$p:Landroid/graphics/Picture;

    #@6
    iput-object p4, p0, Landroid/webkit/WebViewClassic$6;->val$dest:Ljava/io/File;

    #@8
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@b
    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    #@0
    .prologue
    .line 2777
    const/4 v0, 0x0

    #@1
    .line 2779
    .local v0, out:Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v1, Ljava/io/FileOutputStream;

    #@3
    iget-object v2, p0, Landroid/webkit/WebViewClassic$6;->val$temp:Ljava/io/File;

    #@5
    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_20
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_8} :catch_2c

    #@8
    .line 2780
    .end local v0           #out:Ljava/io/FileOutputStream;
    .local v1, out:Ljava/io/FileOutputStream;
    :try_start_8
    iget-object v2, p0, Landroid/webkit/WebViewClassic$6;->val$p:Landroid/graphics/Picture;

    #@a
    invoke-virtual {v2, v1}, Landroid/graphics/Picture;->writeToStream(Ljava/io/OutputStream;)V

    #@d
    .line 2783
    iget-object v2, p0, Landroid/webkit/WebViewClassic$6;->val$temp:Ljava/io/File;

    #@f
    iget-object v3, p0, Landroid/webkit/WebViewClassic$6;->val$dest:Ljava/io/File;

    #@11
    invoke-virtual {v2, v3}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
    :try_end_14
    .catchall {:try_start_8 .. :try_end_14} :catchall_3e
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_14} :catch_41

    #@14
    .line 2787
    if-eqz v1, :cond_19

    #@16
    .line 2789
    :try_start_16
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_19} :catch_3c

    #@19
    .line 2794
    :cond_19
    :goto_19
    iget-object v2, p0, Landroid/webkit/WebViewClassic$6;->val$temp:Ljava/io/File;

    #@1b
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    #@1e
    move-object v0, v1

    #@1f
    .line 2796
    .end local v1           #out:Ljava/io/FileOutputStream;
    .restart local v0       #out:Ljava/io/FileOutputStream;
    :goto_1f
    return-void

    #@20
    .line 2787
    :catchall_20
    move-exception v2

    #@21
    :goto_21
    if-eqz v0, :cond_26

    #@23
    .line 2789
    :try_start_23
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_26
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_26} :catch_3a

    #@26
    .line 2794
    :cond_26
    :goto_26
    iget-object v3, p0, Landroid/webkit/WebViewClassic$6;->val$temp:Ljava/io/File;

    #@28
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    #@2b
    .line 2787
    throw v2

    #@2c
    .line 2784
    :catch_2c
    move-exception v2

    #@2d
    .line 2787
    :goto_2d
    if-eqz v0, :cond_32

    #@2f
    .line 2789
    :try_start_2f
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_32
    .catch Ljava/lang/Exception; {:try_start_2f .. :try_end_32} :catch_38

    #@32
    .line 2794
    :cond_32
    :goto_32
    iget-object v2, p0, Landroid/webkit/WebViewClassic$6;->val$temp:Ljava/io/File;

    #@34
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    #@37
    goto :goto_1f

    #@38
    .line 2790
    :catch_38
    move-exception v2

    #@39
    goto :goto_32

    #@3a
    :catch_3a
    move-exception v3

    #@3b
    goto :goto_26

    #@3c
    .end local v0           #out:Ljava/io/FileOutputStream;
    .restart local v1       #out:Ljava/io/FileOutputStream;
    :catch_3c
    move-exception v2

    #@3d
    goto :goto_19

    #@3e
    .line 2787
    :catchall_3e
    move-exception v2

    #@3f
    move-object v0, v1

    #@40
    .end local v1           #out:Ljava/io/FileOutputStream;
    .restart local v0       #out:Ljava/io/FileOutputStream;
    goto :goto_21

    #@41
    .line 2784
    .end local v0           #out:Ljava/io/FileOutputStream;
    .restart local v1       #out:Ljava/io/FileOutputStream;
    :catch_41
    move-exception v2

    #@42
    move-object v0, v1

    #@43
    .end local v1           #out:Ljava/io/FileOutputStream;
    .restart local v0       #out:Ljava/io/FileOutputStream;
    goto :goto_2d
.end method
