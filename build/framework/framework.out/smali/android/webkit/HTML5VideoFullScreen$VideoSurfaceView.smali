.class Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;
.super Landroid/view/SurfaceView;
.source "HTML5VideoFullScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/HTML5VideoFullScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VideoSurfaceView"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/webkit/HTML5VideoFullScreen;


# direct methods
.method public constructor <init>(Landroid/webkit/HTML5VideoFullScreen;Landroid/content/Context;)V
    .registers 3
    .parameter
    .parameter "context"

    #@0
    .prologue
    .line 43
    iput-object p1, p0, Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;->this$0:Landroid/webkit/HTML5VideoFullScreen;

    #@2
    .line 44
    invoke-direct {p0, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    #@5
    .line 45
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .registers 7
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 49
    iget-object v2, p0, Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;->this$0:Landroid/webkit/HTML5VideoFullScreen;

    #@2
    invoke-static {v2}, Landroid/webkit/HTML5VideoFullScreen;->access$000(Landroid/webkit/HTML5VideoFullScreen;)I

    #@5
    move-result v2

    #@6
    invoke-static {v2, p1}, Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;->getDefaultSize(II)I

    #@9
    move-result v1

    #@a
    .line 50
    .local v1, width:I
    iget-object v2, p0, Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;->this$0:Landroid/webkit/HTML5VideoFullScreen;

    #@c
    invoke-static {v2}, Landroid/webkit/HTML5VideoFullScreen;->access$100(Landroid/webkit/HTML5VideoFullScreen;)I

    #@f
    move-result v2

    #@10
    invoke-static {v2, p2}, Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;->getDefaultSize(II)I

    #@13
    move-result v0

    #@14
    .line 51
    .local v0, height:I
    iget-object v2, p0, Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;->this$0:Landroid/webkit/HTML5VideoFullScreen;

    #@16
    invoke-static {v2}, Landroid/webkit/HTML5VideoFullScreen;->access$000(Landroid/webkit/HTML5VideoFullScreen;)I

    #@19
    move-result v2

    #@1a
    if-lez v2, :cond_43

    #@1c
    iget-object v2, p0, Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;->this$0:Landroid/webkit/HTML5VideoFullScreen;

    #@1e
    invoke-static {v2}, Landroid/webkit/HTML5VideoFullScreen;->access$100(Landroid/webkit/HTML5VideoFullScreen;)I

    #@21
    move-result v2

    #@22
    if-lez v2, :cond_43

    #@24
    .line 52
    iget-object v2, p0, Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;->this$0:Landroid/webkit/HTML5VideoFullScreen;

    #@26
    invoke-static {v2}, Landroid/webkit/HTML5VideoFullScreen;->access$000(Landroid/webkit/HTML5VideoFullScreen;)I

    #@29
    move-result v2

    #@2a
    mul-int/2addr v2, v0

    #@2b
    iget-object v3, p0, Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;->this$0:Landroid/webkit/HTML5VideoFullScreen;

    #@2d
    invoke-static {v3}, Landroid/webkit/HTML5VideoFullScreen;->access$100(Landroid/webkit/HTML5VideoFullScreen;)I

    #@30
    move-result v3

    #@31
    mul-int/2addr v3, v1

    #@32
    if-le v2, v3, :cond_47

    #@34
    .line 53
    iget-object v2, p0, Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;->this$0:Landroid/webkit/HTML5VideoFullScreen;

    #@36
    invoke-static {v2}, Landroid/webkit/HTML5VideoFullScreen;->access$100(Landroid/webkit/HTML5VideoFullScreen;)I

    #@39
    move-result v2

    #@3a
    mul-int/2addr v2, v1

    #@3b
    iget-object v3, p0, Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;->this$0:Landroid/webkit/HTML5VideoFullScreen;

    #@3d
    invoke-static {v3}, Landroid/webkit/HTML5VideoFullScreen;->access$000(Landroid/webkit/HTML5VideoFullScreen;)I

    #@40
    move-result v3

    #@41
    div-int v0, v2, v3

    #@43
    .line 58
    :cond_43
    :goto_43
    invoke-virtual {p0, v1, v0}, Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;->setMeasuredDimension(II)V

    #@46
    .line 59
    return-void

    #@47
    .line 54
    :cond_47
    iget-object v2, p0, Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;->this$0:Landroid/webkit/HTML5VideoFullScreen;

    #@49
    invoke-static {v2}, Landroid/webkit/HTML5VideoFullScreen;->access$000(Landroid/webkit/HTML5VideoFullScreen;)I

    #@4c
    move-result v2

    #@4d
    mul-int/2addr v2, v0

    #@4e
    iget-object v3, p0, Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;->this$0:Landroid/webkit/HTML5VideoFullScreen;

    #@50
    invoke-static {v3}, Landroid/webkit/HTML5VideoFullScreen;->access$100(Landroid/webkit/HTML5VideoFullScreen;)I

    #@53
    move-result v3

    #@54
    mul-int/2addr v3, v1

    #@55
    if-ge v2, v3, :cond_43

    #@57
    .line 55
    iget-object v2, p0, Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;->this$0:Landroid/webkit/HTML5VideoFullScreen;

    #@59
    invoke-static {v2}, Landroid/webkit/HTML5VideoFullScreen;->access$000(Landroid/webkit/HTML5VideoFullScreen;)I

    #@5c
    move-result v2

    #@5d
    mul-int/2addr v2, v0

    #@5e
    iget-object v3, p0, Landroid/webkit/HTML5VideoFullScreen$VideoSurfaceView;->this$0:Landroid/webkit/HTML5VideoFullScreen;

    #@60
    invoke-static {v3}, Landroid/webkit/HTML5VideoFullScreen;->access$100(Landroid/webkit/HTML5VideoFullScreen;)I

    #@63
    move-result v3

    #@64
    div-int v1, v2, v3

    #@66
    goto :goto_43
.end method
