.class Landroid/webkit/WebSocket;
.super Landroid/os/Handler;
.source "WebSocket.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/WebSocket$2;,
        Landroid/webkit/WebSocket$WebSocketImpl;
    }
.end annotation


# static fields
.field private static final CLOSE:I = 0x65

.field private static final LOG_TAG:Ljava/lang/String; = "WEBSOCKET"

.field private static final ON_CLOSED:I = 0xc9

.field private static final ON_CONNECTED:I = 0xc8

.field private static final ON_ERROR:I = 0xcb

.field private static final ON_MESSAGE:I = 0xca

.field private static final SEND:I = 0x64


# instance fields
.field mNativePointer:I

.field private mWebCoreHandler:Landroid/os/Handler;

.field mWebSocketImpl:Landroid/webkit/WebSocket$WebSocketImpl;


# direct methods
.method private constructor <init>(ILjava/lang/String;)V
    .registers 7
    .parameter "nativePtr"
    .parameter "uri"

    #@0
    .prologue
    .line 678
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    #@3
    move-result-object v2

    #@4
    invoke-direct {p0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@7
    .line 680
    iput p1, p0, Landroid/webkit/WebSocket;->mNativePointer:I

    #@9
    .line 682
    invoke-direct {p0}, Landroid/webkit/WebSocket;->createWebCoreHandler()V

    #@c
    .line 684
    const/4 v1, 0x0

    #@d
    .line 686
    .local v1, th:Ljava/lang/Thread;
    :try_start_d
    new-instance v2, Landroid/webkit/WebSocket$WebSocketImpl;

    #@f
    invoke-direct {v2, p0}, Landroid/webkit/WebSocket$WebSocketImpl;-><init>(Landroid/webkit/WebSocket;)V

    #@12
    iput-object v2, p0, Landroid/webkit/WebSocket;->mWebSocketImpl:Landroid/webkit/WebSocket$WebSocketImpl;

    #@14
    .line 687
    iget-object v2, p0, Landroid/webkit/WebSocket;->mWebSocketImpl:Landroid/webkit/WebSocket$WebSocketImpl;

    #@16
    new-instance v3, Ljava/net/URI;

    #@18
    invoke-direct {v3, p2}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    #@1b
    invoke-virtual {v2, v3}, Landroid/webkit/WebSocket$WebSocketImpl;->connect(Ljava/net/URI;)Ljava/lang/Thread;
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_1e} :catch_20

    #@1e
    move-result-object v1

    #@1f
    .line 693
    :cond_1f
    :goto_1f
    return-void

    #@20
    .line 688
    :catch_20
    move-exception v0

    #@21
    .line 689
    .local v0, e:Ljava/lang/Exception;
    if-eqz v1, :cond_1f

    #@23
    .line 690
    #Replaced unresolvable odex instruction with a throw
    throw v1
    #invoke-virtual-quick {v1}, vtable@0x16

    #@26
    goto :goto_1f
.end method

.method static synthetic access$100(Landroid/webkit/WebSocket;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    invoke-direct {p0, p1}, Landroid/webkit/WebSocket;->nativeOnWebSocketConnected(I)V

    #@3
    return-void
.end method

.method static synthetic access$200(Landroid/webkit/WebSocket;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    invoke-direct {p0, p1}, Landroid/webkit/WebSocket;->nativeOnWebSocketClosed(I)V

    #@3
    return-void
.end method

.method static synthetic access$300(Landroid/webkit/WebSocket;I[BI)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebSocket;->nativeOnWebSocketMessage(I[BI)V

    #@3
    return-void
.end method

.method static synthetic access$400(Landroid/webkit/WebSocket;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    invoke-direct {p0, p1}, Landroid/webkit/WebSocket;->nativeOnWebSocketError(I)V

    #@3
    return-void
.end method

.method private createWebCoreHandler()V
    .registers 2

    #@0
    .prologue
    .line 696
    new-instance v0, Landroid/webkit/WebSocket$1;

    #@2
    invoke-direct {v0, p0}, Landroid/webkit/WebSocket$1;-><init>(Landroid/webkit/WebSocket;)V

    #@5
    iput-object v0, p0, Landroid/webkit/WebSocket;->mWebCoreHandler:Landroid/os/Handler;

    #@7
    .line 726
    return-void
.end method

.method public static getInstance(ILjava/lang/String;)Landroid/webkit/WebSocket;
    .registers 3
    .parameter "nativePtr"
    .parameter "uri"

    #@0
    .prologue
    .line 757
    new-instance v0, Landroid/webkit/WebSocket;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/webkit/WebSocket;-><init>(ILjava/lang/String;)V

    #@5
    return-object v0
.end method

.method private native nativeOnWebSocketClosed(I)V
.end method

.method private native nativeOnWebSocketConnected(I)V
.end method

.method private native nativeOnWebSocketError(I)V
.end method

.method private native nativeOnWebSocketMessage(I[BI)V
.end method


# virtual methods
.method public closeWebSocket()V
    .registers 3

    #@0
    .prologue
    .line 748
    const/16 v1, 0x65

    #@2
    invoke-virtual {p0, v1}, Landroid/webkit/WebSocket;->obtainMessage(I)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    .line 749
    .local v0, message:Landroid/os/Message;
    invoke-virtual {p0, v0}, Landroid/webkit/WebSocket;->sendMessage(Landroid/os/Message;)Z

    #@9
    .line 750
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 106
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v0, :pswitch_data_12

    #@5
    .line 116
    :goto_5
    return-void

    #@6
    .line 108
    :pswitch_6
    iget-object v0, p0, Landroid/webkit/WebSocket;->mWebSocketImpl:Landroid/webkit/WebSocket$WebSocketImpl;

    #@8
    invoke-virtual {v0}, Landroid/webkit/WebSocket$WebSocketImpl;->send()V

    #@b
    goto :goto_5

    #@c
    .line 112
    :pswitch_c
    iget-object v0, p0, Landroid/webkit/WebSocket;->mWebSocketImpl:Landroid/webkit/WebSocket$WebSocketImpl;

    #@e
    invoke-virtual {v0}, Landroid/webkit/WebSocket$WebSocketImpl;->close()V

    #@11
    goto :goto_5

    #@12
    .line 106
    :pswitch_data_12
    .packed-switch 0x64
        :pswitch_6
        :pswitch_c
    .end packed-switch
.end method

.method public onClosed()V
    .registers 4

    #@0
    .prologue
    .line 82
    iget-object v1, p0, Landroid/webkit/WebSocket;->mWebCoreHandler:Landroid/os/Handler;

    #@2
    const/16 v2, 0xc9

    #@4
    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 83
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Landroid/webkit/WebSocket;->mWebCoreHandler:Landroid/os/Handler;

    #@a
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@d
    .line 84
    return-void
.end method

.method public onConnected()V
    .registers 4

    #@0
    .prologue
    .line 75
    iget-object v1, p0, Landroid/webkit/WebSocket;->mWebCoreHandler:Landroid/os/Handler;

    #@2
    const/16 v2, 0xc8

    #@4
    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 76
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Landroid/webkit/WebSocket;->mWebCoreHandler:Landroid/os/Handler;

    #@a
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@d
    .line 77
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .registers 5
    .parameter "t"

    #@0
    .prologue
    .line 98
    iget-object v1, p0, Landroid/webkit/WebSocket;->mWebCoreHandler:Landroid/os/Handler;

    #@2
    const/16 v2, 0xcb

    #@4
    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 99
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Landroid/webkit/WebSocket;->mWebCoreHandler:Landroid/os/Handler;

    #@a
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@d
    .line 100
    return-void
.end method

.method public onMessage()V
    .registers 4

    #@0
    .prologue
    .line 89
    iget-object v1, p0, Landroid/webkit/WebSocket;->mWebCoreHandler:Landroid/os/Handler;

    #@2
    const/16 v2, 0xca

    #@4
    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 90
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Landroid/webkit/WebSocket;->mWebCoreHandler:Landroid/os/Handler;

    #@a
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@d
    .line 91
    return-void
.end method

.method public sendWebSocket([B)V
    .registers 6
    .parameter "bytes"

    #@0
    .prologue
    .line 731
    if-nez p1, :cond_3

    #@2
    .line 743
    :goto_2
    return-void

    #@3
    .line 734
    :cond_3
    array-length v3, p1

    #@4
    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@7
    move-result-object v0

    #@8
    .line 735
    .local v0, data:Ljava/nio/ByteBuffer;
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@b
    .line 737
    :try_start_b
    iget-object v3, p0, Landroid/webkit/WebSocket;->mWebSocketImpl:Landroid/webkit/WebSocket$WebSocketImpl;

    #@d
    invoke-virtual {v3, v0}, Landroid/webkit/WebSocket$WebSocketImpl;->putWriteQueueData(Ljava/nio/ByteBuffer;)V
    :try_end_10
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_10} :catch_1a

    #@10
    .line 741
    :goto_10
    const/16 v3, 0x64

    #@12
    invoke-virtual {p0, v3}, Landroid/webkit/WebSocket;->obtainMessage(I)Landroid/os/Message;

    #@15
    move-result-object v2

    #@16
    .line 742
    .local v2, message:Landroid/os/Message;
    invoke-virtual {p0, v2}, Landroid/webkit/WebSocket;->sendMessage(Landroid/os/Message;)Z

    #@19
    goto :goto_2

    #@1a
    .line 738
    .end local v2           #message:Landroid/os/Message;
    :catch_1a
    move-exception v1

    #@1b
    .line 739
    .local v1, e:Ljava/lang/InterruptedException;
    invoke-virtual {p0, v1}, Landroid/webkit/WebSocket;->onError(Ljava/lang/Throwable;)V

    #@1e
    goto :goto_10
.end method
