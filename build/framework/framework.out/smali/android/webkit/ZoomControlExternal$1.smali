.class Landroid/webkit/ZoomControlExternal$1;
.super Ljava/lang/Object;
.source "ZoomControlExternal.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/webkit/ZoomControlExternal;->getControls()Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/webkit/ZoomControlExternal;


# direct methods
.method constructor <init>(Landroid/webkit/ZoomControlExternal;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 77
    iput-object p1, p0, Landroid/webkit/ZoomControlExternal$1;->this$0:Landroid/webkit/ZoomControlExternal;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    #@0
    .prologue
    .line 82
    iget-object v0, p0, Landroid/webkit/ZoomControlExternal$1;->this$0:Landroid/webkit/ZoomControlExternal;

    #@2
    invoke-static {v0}, Landroid/webkit/ZoomControlExternal;->access$000(Landroid/webkit/ZoomControlExternal;)Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;->hasFocus()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_16

    #@c
    .line 83
    iget-object v0, p0, Landroid/webkit/ZoomControlExternal$1;->this$0:Landroid/webkit/ZoomControlExternal;

    #@e
    invoke-static {v0}, Landroid/webkit/ZoomControlExternal;->access$000(Landroid/webkit/ZoomControlExternal;)Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0}, Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;->hide()V

    #@15
    .line 89
    :goto_15
    return-void

    #@16
    .line 85
    :cond_16
    iget-object v0, p0, Landroid/webkit/ZoomControlExternal$1;->this$0:Landroid/webkit/ZoomControlExternal;

    #@18
    invoke-static {v0}, Landroid/webkit/ZoomControlExternal;->access$200(Landroid/webkit/ZoomControlExternal;)Landroid/os/Handler;

    #@1b
    move-result-object v0

    #@1c
    iget-object v1, p0, Landroid/webkit/ZoomControlExternal$1;->this$0:Landroid/webkit/ZoomControlExternal;

    #@1e
    invoke-static {v1}, Landroid/webkit/ZoomControlExternal;->access$100(Landroid/webkit/ZoomControlExternal;)Ljava/lang/Runnable;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@25
    .line 86
    iget-object v0, p0, Landroid/webkit/ZoomControlExternal$1;->this$0:Landroid/webkit/ZoomControlExternal;

    #@27
    invoke-static {v0}, Landroid/webkit/ZoomControlExternal;->access$200(Landroid/webkit/ZoomControlExternal;)Landroid/os/Handler;

    #@2a
    move-result-object v0

    #@2b
    iget-object v1, p0, Landroid/webkit/ZoomControlExternal$1;->this$0:Landroid/webkit/ZoomControlExternal;

    #@2d
    invoke-static {v1}, Landroid/webkit/ZoomControlExternal;->access$100(Landroid/webkit/ZoomControlExternal;)Ljava/lang/Runnable;

    #@30
    move-result-object v1

    #@31
    invoke-static {}, Landroid/webkit/ZoomControlExternal;->access$300()J

    #@34
    move-result-wide v2

    #@35
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@38
    goto :goto_15
.end method
