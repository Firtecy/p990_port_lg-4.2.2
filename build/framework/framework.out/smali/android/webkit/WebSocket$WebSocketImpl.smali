.class public Landroid/webkit/WebSocket$WebSocketImpl;
.super Ljava/lang/Object;
.source "WebSocket.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/WebSocket;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "WebSocketImpl"
.end annotation


# static fields
#the value of this static final field might be set in the static constructor
.field static final synthetic $assertionsDisabled:Z = false

.field private static final BUFFER_SIZE:I = 0x1000


# instance fields
.field private final LOG_TAG:Ljava/lang/String;

.field private mAppRecvBuffer:Ljava/nio/ByteBuffer;

.field private mAppSendBuffer:Ljava/nio/ByteBuffer;

.field private mBufferReadQueue:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field private mBufferWriteQueue:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field private mClosed:Z

.field private mHost:Ljava/lang/String;

.field private mIsSecure:Z

.field private mNetRecvBuffer:Ljava/nio/ByteBuffer;

.field private mNetSendBuffer:Ljava/nio/ByteBuffer;

.field private mPort:I

.field private mRunning:Z

.field private mSSLContext:Ljavax/net/ssl/SSLContext;

.field private mSSLEngine:Ljavax/net/ssl/SSLEngine;

.field private mSSLEngineResult:Ljavax/net/ssl/SSLEngineResult;

.field private mSelector:Ljava/nio/channels/Selector;

.field private mSocketChannel:Ljava/nio/channels/SocketChannel;

.field private mWebSocket:Landroid/webkit/WebSocket;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 120
    const-class v0, Landroid/webkit/WebSocket;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_c

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    sput-boolean v0, Landroid/webkit/WebSocket$WebSocketImpl;->$assertionsDisabled:Z

    #@b
    return-void

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_9
.end method

.method public constructor <init>(Landroid/webkit/WebSocket;)V
    .registers 5
    .parameter "webSocket"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/KeyManagementException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 153
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 121
    const-string v0, "WEBSOCKET"

    #@7
    iput-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->LOG_TAG:Ljava/lang/String;

    #@9
    .line 127
    iput-boolean v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mRunning:Z

    #@b
    .line 129
    iput-boolean v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mIsSecure:Z

    #@d
    .line 131
    iput-object v1, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@f
    .line 132
    iput-object v1, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngineResult:Ljavax/net/ssl/SSLEngineResult;

    #@11
    .line 148
    iput-boolean v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mClosed:Z

    #@13
    .line 154
    iput-object p1, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mWebSocket:Landroid/webkit/WebSocket;

    #@15
    .line 155
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    #@17
    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    #@1a
    iput-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mBufferWriteQueue:Ljava/util/concurrent/BlockingQueue;

    #@1c
    .line 156
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    #@1e
    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    #@21
    iput-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mBufferReadQueue:Ljava/util/concurrent/BlockingQueue;

    #@23
    .line 157
    const-string v0, "TLS"

    #@25
    invoke-static {v0}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    #@28
    move-result-object v0

    #@29
    iput-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLContext:Ljavax/net/ssl/SSLContext;

    #@2b
    .line 158
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLContext:Ljavax/net/ssl/SSLContext;

    #@2d
    invoke-virtual {v0, v1, v1, v1}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    #@30
    .line 159
    return-void
.end method

.method static synthetic access$000(Landroid/webkit/WebSocket$WebSocketImpl;)Ljavax/net/ssl/SSLEngine;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 120
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@2
    return-object v0
.end method

.method private closeImpl()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 579
    :try_start_2
    invoke-direct {p0}, Landroid/webkit/WebSocket$WebSocketImpl;->isSocketRunning()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_93

    #@8
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSocketChannel:Ljava/nio/channels/SocketChannel;

    #@a
    invoke-virtual {v0}, Ljava/nio/channels/SocketChannel;->isConnected()Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_93

    #@10
    iget-boolean v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mIsSecure:Z

    #@12
    if-eqz v0, :cond_93

    #@14
    .line 580
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@16
    if-eqz v0, :cond_2b

    #@18
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@1a
    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->isInboundDone()Z

    #@1d
    move-result v0

    #@1e
    if-nez v0, :cond_2b

    #@20
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSocketChannel:Ljava/nio/channels/SocketChannel;

    #@22
    invoke-virtual {v0}, Ljava/nio/channels/SocketChannel;->isBlocking()Z

    #@25
    move-result v0

    #@26
    if-nez v0, :cond_2b

    #@28
    .line 582
    invoke-direct {p0}, Landroid/webkit/WebSocket$WebSocketImpl;->readSSLImpl()I

    #@2b
    .line 584
    :cond_2b
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mNetSendBuffer:Ljava/nio/ByteBuffer;

    #@2d
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    #@30
    move-result v0

    #@31
    if-lez v0, :cond_39

    #@33
    .line 585
    invoke-direct {p0}, Landroid/webkit/WebSocket$WebSocketImpl;->flushSSLBuffer()I

    #@36
    move-result v0

    #@37
    if-nez v0, :cond_2b

    #@39
    .line 589
    :cond_39
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@3b
    if-eqz v0, :cond_4a

    #@3d
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@3f
    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->isInboundDone()Z

    #@42
    move-result v0

    #@43
    if-nez v0, :cond_4a

    #@45
    .line 590
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@47
    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->closeOutbound()V

    #@4a
    .line 592
    :cond_4a
    invoke-direct {p0}, Landroid/webkit/WebSocket$WebSocketImpl;->processSSLHandshakeStatus()Z

    #@4d
    move-result v0

    #@4e
    if-nez v0, :cond_4a

    #@50
    .line 595
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mNetSendBuffer:Ljava/nio/ByteBuffer;

    #@52
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    #@55
    move-result v0

    #@56
    if-lez v0, :cond_82

    #@58
    invoke-direct {p0}, Landroid/webkit/WebSocket$WebSocketImpl;->flushSSLBuffer()I

    #@5b
    move-result v0

    #@5c
    if-nez v0, :cond_82

    #@5e
    .line 596
    const-string v0, "WEBSOCKET"

    #@60
    new-instance v1, Ljava/lang/StringBuilder;

    #@62
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@65
    const-string v2, "Can\'t flush remaining "

    #@67
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v1

    #@6b
    iget-object v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mNetSendBuffer:Ljava/nio/ByteBuffer;

    #@6d
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->position()I

    #@70
    move-result v2

    #@71
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@74
    move-result-object v1

    #@75
    const-string v2, " bytes"

    #@77
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v1

    #@7b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v1

    #@7f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@82
    .line 599
    :cond_82
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@84
    if-eqz v0, :cond_93

    #@86
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@88
    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->isInboundDone()Z

    #@8b
    move-result v0

    #@8c
    if-nez v0, :cond_93

    #@8e
    .line 600
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@90
    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->closeInbound()V
    :try_end_93
    .catchall {:try_start_2 .. :try_end_93} :catchall_b0

    #@93
    .line 604
    :cond_93
    invoke-direct {p0, v3}, Landroid/webkit/WebSocket$WebSocketImpl;->setSocketRunning(Z)V

    #@96
    .line 605
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mWebSocket:Landroid/webkit/WebSocket;

    #@98
    invoke-virtual {v0}, Landroid/webkit/WebSocket;->onClosed()V

    #@9b
    .line 606
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSocketChannel:Ljava/nio/channels/SocketChannel;

    #@9d
    if-eqz v0, :cond_a4

    #@9f
    .line 607
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSocketChannel:Ljava/nio/channels/SocketChannel;

    #@a1
    invoke-virtual {v0}, Ljava/nio/channels/SocketChannel;->close()V

    #@a4
    .line 609
    :cond_a4
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSelector:Ljava/nio/channels/Selector;

    #@a6
    if-eqz v0, :cond_ad

    #@a8
    .line 610
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSelector:Ljava/nio/channels/Selector;

    #@aa
    invoke-virtual {v0}, Ljava/nio/channels/Selector;->wakeup()Ljava/nio/channels/Selector;

    #@ad
    .line 613
    :cond_ad
    iput-boolean v4, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mClosed:Z

    #@af
    .line 615
    return-void

    #@b0
    .line 604
    :catchall_b0
    move-exception v0

    #@b1
    invoke-direct {p0, v3}, Landroid/webkit/WebSocket$WebSocketImpl;->setSocketRunning(Z)V

    #@b4
    .line 605
    iget-object v1, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mWebSocket:Landroid/webkit/WebSocket;

    #@b6
    invoke-virtual {v1}, Landroid/webkit/WebSocket;->onClosed()V

    #@b9
    .line 606
    iget-object v1, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSocketChannel:Ljava/nio/channels/SocketChannel;

    #@bb
    if-eqz v1, :cond_c2

    #@bd
    .line 607
    iget-object v1, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSocketChannel:Ljava/nio/channels/SocketChannel;

    #@bf
    invoke-virtual {v1}, Ljava/nio/channels/SocketChannel;->close()V

    #@c2
    .line 609
    :cond_c2
    iget-object v1, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSelector:Ljava/nio/channels/Selector;

    #@c4
    if-eqz v1, :cond_cb

    #@c6
    .line 610
    iget-object v1, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSelector:Ljava/nio/channels/Selector;

    #@c8
    invoke-virtual {v1}, Ljava/nio/channels/Selector;->wakeup()Ljava/nio/channels/Selector;

    #@cb
    .line 613
    :cond_cb
    iput-boolean v4, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mClosed:Z

    #@cd
    .line 604
    throw v0
.end method

.method private flushSSLBuffer()I
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 519
    iget-object v1, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mNetSendBuffer:Ljava/nio/ByteBuffer;

    #@2
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    #@5
    .line 520
    iget-object v1, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSocketChannel:Ljava/nio/channels/SocketChannel;

    #@7
    iget-object v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mNetSendBuffer:Ljava/nio/ByteBuffer;

    #@9
    invoke-virtual {v1, v2}, Ljava/nio/channels/SocketChannel;->write(Ljava/nio/ByteBuffer;)I

    #@c
    move-result v0

    #@d
    .line 521
    .local v0, count:I
    iget-object v1, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mNetSendBuffer:Ljava/nio/ByteBuffer;

    #@f
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->compact()Ljava/nio/ByteBuffer;

    #@12
    .line 522
    return v0
.end method

.method private handleConnectable(Ljava/nio/channels/SelectionKey;)V
    .registers 5
    .parameter "key"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 374
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSocketChannel:Ljava/nio/channels/SocketChannel;

    #@2
    invoke-virtual {v0}, Ljava/nio/channels/SocketChannel;->isConnectionPending()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_d

    #@8
    .line 375
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSocketChannel:Ljava/nio/channels/SocketChannel;

    #@a
    invoke-virtual {v0}, Ljava/nio/channels/SocketChannel;->finishConnect()Z

    #@d
    .line 377
    :cond_d
    iget-boolean v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mIsSecure:Z

    #@f
    if-nez v0, :cond_22

    #@11
    .line 378
    const/16 v0, 0x1000

    #@13
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@16
    move-result-object v0

    #@17
    iput-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mNetRecvBuffer:Ljava/nio/ByteBuffer;

    #@19
    .line 379
    const/4 v0, 0x0

    #@1a
    iput-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mAppRecvBuffer:Ljava/nio/ByteBuffer;

    #@1c
    .line 394
    :goto_1c
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mWebSocket:Landroid/webkit/WebSocket;

    #@1e
    invoke-virtual {v0}, Landroid/webkit/WebSocket;->onConnected()V

    #@21
    .line 395
    return-void

    #@22
    .line 381
    :cond_22
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLContext:Ljavax/net/ssl/SSLContext;

    #@24
    iget-object v1, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mHost:Ljava/lang/String;

    #@26
    iget v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mPort:I

    #@28
    invoke-virtual {v0, v1, v2}, Ljavax/net/ssl/SSLContext;->createSSLEngine(Ljava/lang/String;I)Ljavax/net/ssl/SSLEngine;

    #@2b
    move-result-object v0

    #@2c
    iput-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@2e
    .line 382
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@30
    const/4 v1, 0x1

    #@31
    invoke-virtual {v0, v1}, Ljavax/net/ssl/SSLEngine;->setUseClientMode(Z)V

    #@34
    .line 383
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@36
    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->beginHandshake()V

    #@39
    .line 385
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@3b
    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->getSession()Ljavax/net/ssl/SSLSession;

    #@3e
    move-result-object v0

    #@3f
    invoke-interface {v0}, Ljavax/net/ssl/SSLSession;->getApplicationBufferSize()I

    #@42
    move-result v0

    #@43
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@46
    move-result-object v0

    #@47
    iput-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mAppSendBuffer:Ljava/nio/ByteBuffer;

    #@49
    .line 387
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@4b
    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->getSession()Ljavax/net/ssl/SSLSession;

    #@4e
    move-result-object v0

    #@4f
    invoke-interface {v0}, Ljavax/net/ssl/SSLSession;->getPacketBufferSize()I

    #@52
    move-result v0

    #@53
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@56
    move-result-object v0

    #@57
    iput-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mNetSendBuffer:Ljava/nio/ByteBuffer;

    #@59
    .line 389
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@5b
    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->getSession()Ljavax/net/ssl/SSLSession;

    #@5e
    move-result-object v0

    #@5f
    invoke-interface {v0}, Ljavax/net/ssl/SSLSession;->getApplicationBufferSize()I

    #@62
    move-result v0

    #@63
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@66
    move-result-object v0

    #@67
    iput-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mAppRecvBuffer:Ljava/nio/ByteBuffer;

    #@69
    .line 391
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@6b
    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->getSession()Ljavax/net/ssl/SSLSession;

    #@6e
    move-result-object v0

    #@6f
    invoke-interface {v0}, Ljavax/net/ssl/SSLSession;->getPacketBufferSize()I

    #@72
    move-result v0

    #@73
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@76
    move-result-object v0

    #@77
    iput-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mNetRecvBuffer:Ljava/nio/ByteBuffer;

    #@79
    goto :goto_1c
.end method

.method private handleReadable(Ljava/nio/channels/SelectionKey;)V
    .registers 5
    .parameter "key"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    #@0
    .prologue
    .line 420
    const/4 v0, 0x0

    #@1
    .line 421
    .local v0, count:I
    :try_start_1
    iget-boolean v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mIsSecure:Z

    #@3
    if-nez v2, :cond_21

    #@5
    .line 422
    invoke-direct {p0}, Landroid/webkit/WebSocket$WebSocketImpl;->readImpl()I

    #@8
    move-result v0

    #@9
    .line 426
    :goto_9
    if-gez v0, :cond_20

    #@b
    .line 427
    iget-boolean v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mIsSecure:Z

    #@d
    if-eqz v2, :cond_18

    #@f
    .line 428
    invoke-virtual {p1}, Ljava/nio/channels/SelectionKey;->interestOps()I

    #@12
    move-result v2

    #@13
    and-int/lit8 v2, v2, -0x2

    #@15
    invoke-virtual {p1, v2}, Ljava/nio/channels/SelectionKey;->interestOps(I)Ljava/nio/channels/SelectionKey;

    #@18
    .line 430
    :cond_18
    iget-object v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mWebSocket:Landroid/webkit/WebSocket;

    #@1a
    invoke-virtual {v2}, Landroid/webkit/WebSocket;->onMessage()V

    #@1d
    .line 431
    invoke-direct {p0, p1}, Landroid/webkit/WebSocket$WebSocketImpl;->handleWritable(Ljava/nio/channels/SelectionKey;)V

    #@20
    .line 437
    :cond_20
    :goto_20
    return-void

    #@21
    .line 424
    :cond_21
    invoke-direct {p0}, Landroid/webkit/WebSocket$WebSocketImpl;->readSSLImpl()I
    :try_end_24
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_24} :catch_26

    #@24
    move-result v0

    #@25
    goto :goto_9

    #@26
    .line 433
    :catch_26
    move-exception v1

    #@27
    .line 434
    .local v1, ex:Ljava/io/IOException;
    iget-object v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mWebSocket:Landroid/webkit/WebSocket;

    #@29
    invoke-virtual {v2, v1}, Landroid/webkit/WebSocket;->onError(Ljava/lang/Throwable;)V

    #@2c
    .line 435
    invoke-virtual {p1}, Ljava/nio/channels/SelectionKey;->cancel()V

    #@2f
    goto :goto_20
.end method

.method private handleRunnable()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 327
    invoke-direct {p0}, Landroid/webkit/WebSocket$WebSocketImpl;->isSocketRunning()Z

    #@3
    move-result v4

    #@4
    if-nez v4, :cond_7

    #@6
    .line 371
    :cond_6
    :goto_6
    return-void

    #@7
    .line 330
    :cond_7
    iget-object v4, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSelector:Ljava/nio/channels/Selector;

    #@9
    invoke-virtual {v4}, Ljava/nio/channels/Selector;->isOpen()Z

    #@c
    move-result v4

    #@d
    if-eqz v4, :cond_6

    #@f
    .line 335
    :try_start_f
    iget-object v4, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSelector:Ljava/nio/channels/Selector;

    #@11
    invoke-virtual {v4}, Ljava/nio/channels/Selector;->select()I
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_14} :catch_40
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_f .. :try_end_14} :catch_42
    .catch Ljava/lang/IllegalArgumentException; {:try_start_f .. :try_end_14} :catch_44

    #@14
    move-result v4

    #@15
    if-eqz v4, :cond_6

    #@17
    .line 346
    iget-object v4, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSelector:Ljava/nio/channels/Selector;

    #@19
    invoke-virtual {v4}, Ljava/nio/channels/Selector;->selectedKeys()Ljava/util/Set;

    #@1c
    move-result-object v3

    #@1d
    .line 347
    .local v3, keys:Ljava/util/Set;,"Ljava/util/Set<Ljava/nio/channels/SelectionKey;>;"
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@20
    move-result-object v1

    #@21
    .line 349
    .local v1, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/nio/channels/SelectionKey;>;"
    :cond_21
    :goto_21
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@24
    move-result v4

    #@25
    if-eqz v4, :cond_6

    #@27
    .line 350
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2a
    move-result-object v2

    #@2b
    check-cast v2, Ljava/nio/channels/SelectionKey;

    #@2d
    .line 351
    .local v2, key:Ljava/nio/channels/SelectionKey;
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    #@30
    .line 353
    invoke-virtual {v2}, Ljava/nio/channels/SelectionKey;->isValid()Z

    #@33
    move-result v4

    #@34
    if-eqz v4, :cond_21

    #@36
    .line 356
    invoke-virtual {v2}, Ljava/nio/channels/SelectionKey;->isConnectable()Z

    #@39
    move-result v4

    #@3a
    if-eqz v4, :cond_46

    #@3c
    .line 357
    invoke-direct {p0, v2}, Landroid/webkit/WebSocket$WebSocketImpl;->handleConnectable(Ljava/nio/channels/SelectionKey;)V

    #@3f
    goto :goto_21

    #@40
    .line 338
    .end local v1           #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/nio/channels/SelectionKey;>;"
    .end local v2           #key:Ljava/nio/channels/SelectionKey;
    .end local v3           #keys:Ljava/util/Set;,"Ljava/util/Set<Ljava/nio/channels/SelectionKey;>;"
    :catch_40
    move-exception v0

    #@41
    .line 339
    .local v0, e:Ljava/io/IOException;
    goto :goto_6

    #@42
    .line 340
    .end local v0           #e:Ljava/io/IOException;
    :catch_42
    move-exception v0

    #@43
    .line 341
    .local v0, e:Ljava/nio/channels/ClosedSelectorException;
    goto :goto_6

    #@44
    .line 342
    .end local v0           #e:Ljava/nio/channels/ClosedSelectorException;
    :catch_44
    move-exception v0

    #@45
    .line 343
    .local v0, e:Ljava/lang/IllegalArgumentException;
    goto :goto_6

    #@46
    .line 361
    .end local v0           #e:Ljava/lang/IllegalArgumentException;
    .restart local v1       #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/nio/channels/SelectionKey;>;"
    .restart local v2       #key:Ljava/nio/channels/SelectionKey;
    .restart local v3       #keys:Ljava/util/Set;,"Ljava/util/Set<Ljava/nio/channels/SelectionKey;>;"
    :cond_46
    iget-object v4, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSocketChannel:Ljava/nio/channels/SocketChannel;

    #@48
    invoke-virtual {v4}, Ljava/nio/channels/SocketChannel;->isConnected()Z

    #@4b
    move-result v4

    #@4c
    if-eqz v4, :cond_58

    #@4e
    invoke-virtual {v2}, Ljava/nio/channels/SelectionKey;->isWritable()Z

    #@51
    move-result v4

    #@52
    if-eqz v4, :cond_58

    #@54
    .line 362
    invoke-direct {p0, v2}, Landroid/webkit/WebSocket$WebSocketImpl;->handleWritable(Ljava/nio/channels/SelectionKey;)V

    #@57
    goto :goto_21

    #@58
    .line 366
    :cond_58
    iget-object v4, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSocketChannel:Ljava/nio/channels/SocketChannel;

    #@5a
    invoke-virtual {v4}, Ljava/nio/channels/SocketChannel;->isConnected()Z

    #@5d
    move-result v4

    #@5e
    if-eqz v4, :cond_21

    #@60
    invoke-virtual {v2}, Ljava/nio/channels/SelectionKey;->isReadable()Z

    #@63
    move-result v4

    #@64
    if-eqz v4, :cond_21

    #@66
    .line 367
    invoke-direct {p0, v2}, Landroid/webkit/WebSocket$WebSocketImpl;->handleReadable(Ljava/nio/channels/SelectionKey;)V

    #@69
    goto :goto_21
.end method

.method private handleWritable(Ljava/nio/channels/SelectionKey;)V
    .registers 6
    .parameter "key"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 399
    const/4 v0, 0x0

    #@1
    .line 400
    .local v0, count:I
    :try_start_1
    iget-boolean v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mIsSecure:Z

    #@3
    if-nez v3, :cond_1b

    #@5
    .line 401
    invoke-virtual {p0}, Landroid/webkit/WebSocket$WebSocketImpl;->getWriteQueueData()Ljava/nio/ByteBuffer;

    #@8
    move-result-object v1

    #@9
    .line 402
    .local v1, data:Ljava/nio/ByteBuffer;
    if-eqz v1, :cond_f

    #@b
    .line 403
    invoke-direct {p0, v1}, Landroid/webkit/WebSocket$WebSocketImpl;->writeImpl(Ljava/nio/ByteBuffer;)I

    #@e
    move-result v0

    #@f
    .line 409
    .end local v1           #data:Ljava/nio/ByteBuffer;
    :cond_f
    :goto_f
    if-lez v0, :cond_1a

    #@11
    .line 410
    invoke-virtual {p1}, Ljava/nio/channels/SelectionKey;->interestOps()I

    #@14
    move-result v3

    #@15
    and-int/lit8 v3, v3, -0x5

    #@17
    invoke-virtual {p1, v3}, Ljava/nio/channels/SelectionKey;->interestOps(I)Ljava/nio/channels/SelectionKey;

    #@1a
    .line 416
    :cond_1a
    :goto_1a
    return-void

    #@1b
    .line 406
    :cond_1b
    invoke-direct {p0}, Landroid/webkit/WebSocket$WebSocketImpl;->writeSSLImpl()I
    :try_end_1e
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1e} :catch_20

    #@1e
    move-result v0

    #@1f
    goto :goto_f

    #@20
    .line 412
    :catch_20
    move-exception v2

    #@21
    .line 413
    .local v2, ex:Ljava/io/IOException;
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mWebSocket:Landroid/webkit/WebSocket;

    #@23
    invoke-virtual {v3, v2}, Landroid/webkit/WebSocket;->onError(Ljava/lang/Throwable;)V

    #@26
    .line 414
    invoke-virtual {p1}, Ljava/nio/channels/SelectionKey;->cancel()V

    #@29
    goto :goto_1a
.end method

.method private declared-synchronized isSocketRunning()Z
    .registers 2

    #@0
    .prologue
    .line 315
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mRunning:Z

    #@3
    if-eqz v0, :cond_18

    #@5
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSocketChannel:Ljava/nio/channels/SocketChannel;

    #@7
    if-eqz v0, :cond_18

    #@9
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSocketChannel:Ljava/nio/channels/SocketChannel;

    #@b
    invoke-virtual {v0}, Ljava/nio/channels/SocketChannel;->socket()Ljava/net/Socket;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/net/Socket;->isClosed()Z
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_1a

    #@12
    move-result v0

    #@13
    if-nez v0, :cond_18

    #@15
    .line 317
    const/4 v0, 0x1

    #@16
    .line 319
    :goto_16
    monitor-exit p0

    #@17
    return v0

    #@18
    :cond_18
    const/4 v0, 0x0

    #@19
    goto :goto_16

    #@1a
    .line 315
    :catchall_1a
    move-exception v0

    #@1b
    monitor-exit p0

    #@1c
    throw v0
.end method

.method private processSSLHandshakeStatus()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 618
    const/4 v0, 0x0

    #@3
    .line 620
    .local v0, count:I
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@5
    if-nez v3, :cond_8

    #@7
    .line 667
    :cond_7
    :goto_7
    return v2

    #@8
    .line 623
    :cond_8
    sget-object v3, Landroid/webkit/WebSocket$2;->$SwitchMap$javax$net$ssl$SSLEngineResult$HandshakeStatus:[I

    #@a
    iget-object v4, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@c
    invoke-virtual {v4}, Ljavax/net/ssl/SSLEngine;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    #@f
    move-result-object v4

    #@10
    invoke-virtual {v4}, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->ordinal()I

    #@13
    move-result v4

    #@14
    aget v3, v3, v4

    #@16
    packed-switch v3, :pswitch_data_d8

    #@19
    goto :goto_7

    #@1a
    .line 625
    :pswitch_1a
    const-string v1, "WEBSOCKET"

    #@1c
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@1e
    invoke-virtual {v3}, Ljavax/net/ssl/SSLEngine;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3}, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->toString()Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    goto :goto_7

    #@2a
    .line 628
    :pswitch_2a
    const-string v2, "WEBSOCKET"

    #@2c
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@2e
    invoke-virtual {v3}, Ljavax/net/ssl/SSLEngine;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v3}, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->toString()Ljava/lang/String;

    #@35
    move-result-object v3

    #@36
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 629
    new-instance v2, Landroid/webkit/WebSocket$WebSocketImpl$1;

    #@3b
    invoke-direct {v2, p0}, Landroid/webkit/WebSocket$WebSocketImpl$1;-><init>(Landroid/webkit/WebSocket$WebSocketImpl;)V

    #@3e
    invoke-virtual {v2}, Landroid/webkit/WebSocket$WebSocketImpl$1;->start()V

    #@41
    move v2, v1

    #@42
    .line 637
    goto :goto_7

    #@43
    .line 639
    :pswitch_43
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mAppSendBuffer:Ljava/nio/ByteBuffer;

    #@45
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    #@48
    .line 640
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@4a
    iget-object v4, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mAppSendBuffer:Ljava/nio/ByteBuffer;

    #@4c
    iget-object v5, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mNetSendBuffer:Ljava/nio/ByteBuffer;

    #@4e
    invoke-virtual {v3, v4, v5}, Ljavax/net/ssl/SSLEngine;->wrap(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)Ljavax/net/ssl/SSLEngineResult;

    #@51
    move-result-object v3

    #@52
    iput-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngineResult:Ljavax/net/ssl/SSLEngineResult;

    #@54
    .line 641
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mAppSendBuffer:Ljava/nio/ByteBuffer;

    #@56
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->compact()Ljava/nio/ByteBuffer;

    #@59
    .line 642
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngineResult:Ljavax/net/ssl/SSLEngineResult;

    #@5b
    invoke-virtual {v3}, Ljavax/net/ssl/SSLEngineResult;->getStatus()Ljavax/net/ssl/SSLEngineResult$Status;

    #@5e
    move-result-object v3

    #@5f
    sget-object v4, Ljavax/net/ssl/SSLEngineResult$Status;->BUFFER_OVERFLOW:Ljavax/net/ssl/SSLEngineResult$Status;

    #@61
    if-ne v3, v4, :cond_6d

    #@63
    .line 643
    invoke-direct {p0}, Landroid/webkit/WebSocket$WebSocketImpl;->flushSSLBuffer()I

    #@66
    move-result v0

    #@67
    if-lez v0, :cond_6b

    #@69
    :goto_69
    move v2, v1

    #@6a
    goto :goto_7

    #@6b
    :cond_6b
    move v1, v2

    #@6c
    goto :goto_69

    #@6d
    :cond_6d
    move v2, v1

    #@6e
    .line 644
    goto :goto_7

    #@6f
    .line 646
    :pswitch_6f
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mNetRecvBuffer:Ljava/nio/ByteBuffer;

    #@71
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    #@74
    .line 647
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@76
    iget-object v4, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mNetRecvBuffer:Ljava/nio/ByteBuffer;

    #@78
    iget-object v5, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mAppRecvBuffer:Ljava/nio/ByteBuffer;

    #@7a
    invoke-virtual {v3, v4, v5}, Ljavax/net/ssl/SSLEngine;->unwrap(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)Ljavax/net/ssl/SSLEngineResult;

    #@7d
    move-result-object v3

    #@7e
    iput-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngineResult:Ljavax/net/ssl/SSLEngineResult;

    #@80
    .line 649
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mNetRecvBuffer:Ljava/nio/ByteBuffer;

    #@82
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->compact()Ljava/nio/ByteBuffer;

    #@85
    .line 650
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngineResult:Ljavax/net/ssl/SSLEngineResult;

    #@87
    invoke-virtual {v3}, Ljavax/net/ssl/SSLEngineResult;->getStatus()Ljavax/net/ssl/SSLEngineResult$Status;

    #@8a
    move-result-object v3

    #@8b
    sget-object v4, Ljavax/net/ssl/SSLEngineResult$Status;->BUFFER_UNDERFLOW:Ljavax/net/ssl/SSLEngineResult$Status;

    #@8d
    if-ne v3, v4, :cond_ba

    #@8f
    .line 651
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@91
    invoke-virtual {v3}, Ljavax/net/ssl/SSLEngine;->isInboundDone()Z

    #@94
    move-result v3

    #@95
    if-eqz v3, :cond_9d

    #@97
    .line 652
    const/4 v0, -0x1

    #@98
    .line 657
    :goto_98
    if-lez v0, :cond_b8

    #@9a
    :goto_9a
    move v2, v1

    #@9b
    goto/16 :goto_7

    #@9d
    .line 654
    :cond_9d
    sget-boolean v3, Landroid/webkit/WebSocket$WebSocketImpl;->$assertionsDisabled:Z

    #@9f
    if-nez v3, :cond_af

    #@a1
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSocketChannel:Ljava/nio/channels/SocketChannel;

    #@a3
    invoke-virtual {v3}, Ljava/nio/channels/SocketChannel;->isOpen()Z

    #@a6
    move-result v3

    #@a7
    if-nez v3, :cond_af

    #@a9
    new-instance v1, Ljava/lang/AssertionError;

    #@ab
    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    #@ae
    throw v1

    #@af
    .line 655
    :cond_af
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSocketChannel:Ljava/nio/channels/SocketChannel;

    #@b1
    iget-object v4, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mNetRecvBuffer:Ljava/nio/ByteBuffer;

    #@b3
    invoke-virtual {v3, v4}, Ljava/nio/channels/SocketChannel;->read(Ljava/nio/ByteBuffer;)I

    #@b6
    move-result v0

    #@b7
    goto :goto_98

    #@b8
    :cond_b8
    move v1, v2

    #@b9
    .line 657
    goto :goto_9a

    #@ba
    .line 659
    :cond_ba
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngineResult:Ljavax/net/ssl/SSLEngineResult;

    #@bc
    invoke-virtual {v3}, Ljavax/net/ssl/SSLEngineResult;->getStatus()Ljavax/net/ssl/SSLEngineResult$Status;

    #@bf
    move-result-object v3

    #@c0
    sget-object v4, Ljavax/net/ssl/SSLEngineResult$Status;->BUFFER_OVERFLOW:Ljavax/net/ssl/SSLEngineResult$Status;

    #@c2
    if-eq v3, v4, :cond_7

    #@c4
    move v2, v1

    #@c5
    .line 662
    goto/16 :goto_7

    #@c7
    .line 664
    :pswitch_c7
    const-string v1, "WEBSOCKET"

    #@c9
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@cb
    invoke-virtual {v3}, Ljavax/net/ssl/SSLEngine;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    #@ce
    move-result-object v3

    #@cf
    invoke-virtual {v3}, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->toString()Ljava/lang/String;

    #@d2
    move-result-object v3

    #@d3
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d6
    goto/16 :goto_7

    #@d8
    .line 623
    :pswitch_data_d8
    .packed-switch 0x1
        :pswitch_1a
        :pswitch_2a
        :pswitch_43
        :pswitch_6f
        :pswitch_c7
    .end packed-switch
.end method

.method private readImpl()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    #@0
    .prologue
    .line 462
    const/4 v1, -0x1

    #@1
    .line 464
    .local v1, plainDataCount:I
    :cond_1
    invoke-direct {p0}, Landroid/webkit/WebSocket$WebSocketImpl;->isSocketRunning()Z

    #@4
    move-result v2

    #@5
    if-nez v2, :cond_8

    #@7
    .line 481
    :goto_7
    return v1

    #@8
    .line 467
    :cond_8
    iget-object v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mNetRecvBuffer:Ljava/nio/ByteBuffer;

    #@a
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    #@d
    .line 468
    iget-object v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSocketChannel:Ljava/nio/channels/SocketChannel;

    #@f
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mNetRecvBuffer:Ljava/nio/ByteBuffer;

    #@11
    invoke-virtual {v2, v3}, Ljava/nio/channels/SocketChannel;->read(Ljava/nio/ByteBuffer;)I

    #@14
    move-result v1

    #@15
    .line 470
    if-gtz v1, :cond_19

    #@17
    .line 471
    const/4 v1, -0x1

    #@18
    .line 472
    goto :goto_7

    #@19
    .line 474
    :cond_19
    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@1c
    move-result-object v0

    #@1d
    .line 475
    .local v0, data:Ljava/nio/ByteBuffer;
    if-eqz v0, :cond_2c

    #@1f
    .line 476
    iget-object v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mNetRecvBuffer:Ljava/nio/ByteBuffer;

    #@21
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    #@24
    move-result-object v2

    #@25
    const/4 v3, 0x0

    #@26
    invoke-virtual {v0, v2, v3, v1}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    #@29
    .line 477
    invoke-virtual {p0, v0}, Landroid/webkit/WebSocket$WebSocketImpl;->putReadQueueData(Ljava/nio/ByteBuffer;)V

    #@2c
    .line 479
    :cond_2c
    if-gtz v1, :cond_1

    #@2e
    goto :goto_7
.end method

.method private readSSLImpl()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljavax/net/ssl/SSLException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 526
    const/4 v0, 0x0

    #@2
    .line 527
    .local v0, cipherTextCount:I
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mAppRecvBuffer:Ljava/nio/ByteBuffer;

    #@4
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->position()I

    #@7
    move-result v1

    #@8
    .line 529
    .local v1, plainTextCount:I
    :cond_8
    invoke-direct {p0}, Landroid/webkit/WebSocket$WebSocketImpl;->isSocketRunning()Z

    #@b
    move-result v3

    #@c
    if-nez v3, :cond_f

    #@e
    .line 573
    :cond_e
    :goto_e
    return v2

    #@f
    .line 533
    :cond_f
    invoke-direct {p0}, Landroid/webkit/WebSocket$WebSocketImpl;->flushSSLBuffer()I

    #@12
    .line 534
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@14
    invoke-virtual {v3}, Ljavax/net/ssl/SSLEngine;->isInboundDone()Z

    #@17
    move-result v3

    #@18
    if-nez v3, :cond_e

    #@1a
    .line 538
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSocketChannel:Ljava/nio/channels/SocketChannel;

    #@1c
    iget-object v4, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mNetRecvBuffer:Ljava/nio/ByteBuffer;

    #@1e
    invoke-virtual {v3, v4}, Ljava/nio/channels/SocketChannel;->read(Ljava/nio/ByteBuffer;)I

    #@21
    .line 539
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mNetRecvBuffer:Ljava/nio/ByteBuffer;

    #@23
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    #@26
    .line 540
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@28
    iget-object v4, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mNetRecvBuffer:Ljava/nio/ByteBuffer;

    #@2a
    iget-object v5, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mAppRecvBuffer:Ljava/nio/ByteBuffer;

    #@2c
    invoke-virtual {v3, v4, v5}, Ljavax/net/ssl/SSLEngine;->unwrap(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)Ljavax/net/ssl/SSLEngineResult;

    #@2f
    move-result-object v3

    #@30
    iput-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngineResult:Ljavax/net/ssl/SSLEngineResult;

    #@32
    .line 541
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mNetRecvBuffer:Ljava/nio/ByteBuffer;

    #@34
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->compact()Ljava/nio/ByteBuffer;

    #@37
    .line 542
    sget-object v3, Landroid/webkit/WebSocket$2;->$SwitchMap$javax$net$ssl$SSLEngineResult$Status:[I

    #@39
    iget-object v4, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngineResult:Ljavax/net/ssl/SSLEngineResult;

    #@3b
    invoke-virtual {v4}, Ljavax/net/ssl/SSLEngineResult;->getStatus()Ljavax/net/ssl/SSLEngineResult$Status;

    #@3e
    move-result-object v4

    #@3f
    invoke-virtual {v4}, Ljavax/net/ssl/SSLEngineResult$Status;->ordinal()I

    #@42
    move-result v4

    #@43
    aget v3, v3, v4

    #@45
    packed-switch v3, :pswitch_data_9e

    #@48
    .line 563
    :cond_48
    :goto_48
    invoke-direct {p0}, Landroid/webkit/WebSocket$WebSocketImpl;->processSSLHandshakeStatus()Z

    #@4b
    move-result v3

    #@4c
    if-nez v3, :cond_48

    #@4e
    .line 567
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mAppRecvBuffer:Ljava/nio/ByteBuffer;

    #@50
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->position()I

    #@53
    move-result v1

    #@54
    .line 568
    if-eqz v1, :cond_8

    #@56
    .line 570
    iget-object v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@58
    invoke-virtual {v2}, Ljavax/net/ssl/SSLEngine;->isInboundDone()Z

    #@5b
    move-result v2

    #@5c
    if-eqz v2, :cond_5f

    #@5e
    .line 571
    const/4 v1, -0x1

    #@5f
    :cond_5f
    move v2, v1

    #@60
    .line 573
    goto :goto_e

    #@61
    .line 544
    :pswitch_61
    sget-boolean v3, Landroid/webkit/WebSocket$WebSocketImpl;->$assertionsDisabled:Z

    #@63
    if-nez v3, :cond_73

    #@65
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSocketChannel:Ljava/nio/channels/SocketChannel;

    #@67
    invoke-virtual {v3}, Ljava/nio/channels/SocketChannel;->isOpen()Z

    #@6a
    move-result v3

    #@6b
    if-nez v3, :cond_73

    #@6d
    new-instance v2, Ljava/lang/AssertionError;

    #@6f
    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    #@72
    throw v2

    #@73
    .line 545
    :cond_73
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSocketChannel:Ljava/nio/channels/SocketChannel;

    #@75
    iget-object v4, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mNetRecvBuffer:Ljava/nio/ByteBuffer;

    #@77
    invoke-virtual {v3, v4}, Ljava/nio/channels/SocketChannel;->read(Ljava/nio/ByteBuffer;)I

    #@7a
    move-result v0

    #@7b
    .line 546
    if-nez v0, :cond_7f

    #@7d
    move v2, v1

    #@7e
    .line 547
    goto :goto_e

    #@7f
    .line 548
    :cond_7f
    if-ne v0, v2, :cond_48

    #@81
    .line 549
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@83
    invoke-virtual {v3}, Ljavax/net/ssl/SSLEngine;->closeInbound()V

    #@86
    goto :goto_48

    #@87
    .line 553
    :pswitch_87
    new-instance v2, Ljava/nio/BufferOverflowException;

    #@89
    invoke-direct {v2}, Ljava/nio/BufferOverflowException;-><init>()V

    #@8c
    throw v2

    #@8d
    .line 555
    :pswitch_8d
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSocketChannel:Ljava/nio/channels/SocketChannel;

    #@8f
    invoke-virtual {v3}, Ljava/nio/channels/SocketChannel;->socket()Ljava/net/Socket;

    #@92
    move-result-object v3

    #@93
    invoke-virtual {v3}, Ljava/net/Socket;->shutdownInput()V

    #@96
    goto :goto_48

    #@97
    .line 558
    :pswitch_97
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mAppRecvBuffer:Ljava/nio/ByteBuffer;

    #@99
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->position()I

    #@9c
    move-result v1

    #@9d
    .line 559
    goto :goto_48

    #@9e
    .line 542
    :pswitch_data_9e
    .packed-switch 0x1
        :pswitch_61
        :pswitch_87
        :pswitch_8d
        :pswitch_97
    .end packed-switch
.end method

.method private declared-synchronized setSocketRunning(Z)V
    .registers 3
    .parameter "running"

    #@0
    .prologue
    .line 323
    monitor-enter p0

    #@1
    :try_start_1
    iput-boolean p1, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mRunning:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    .line 324
    monitor-exit p0

    #@4
    return-void

    #@5
    .line 323
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method private writeImpl(Ljava/nio/ByteBuffer;)I
    .registers 8
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 440
    const/4 v3, -0x1

    #@1
    .line 441
    .local v3, plainDataCount:I
    if-nez p1, :cond_5

    #@3
    move v4, v3

    #@4
    .line 458
    .end local v3           #plainDataCount:I
    .local v4, plainDataCount:I
    :goto_4
    return v4

    #@5
    .line 445
    .end local v4           #plainDataCount:I
    .restart local v3       #plainDataCount:I
    :cond_5
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    #@8
    move-result-object v1

    #@9
    .line 446
    .local v1, bytes:[B
    array-length v2, v1

    #@a
    .line 449
    .local v2, dataLength:I
    :cond_a
    invoke-direct {p0}, Landroid/webkit/WebSocket$WebSocketImpl;->isSocketRunning()Z

    #@d
    move-result v5

    #@e
    if-nez v5, :cond_12

    #@10
    :goto_10
    move v4, v3

    #@11
    .line 458
    .end local v3           #plainDataCount:I
    .restart local v4       #plainDataCount:I
    goto :goto_4

    #@12
    .line 452
    .end local v4           #plainDataCount:I
    .restart local v3       #plainDataCount:I
    :cond_12
    array-length v5, v1

    #@13
    invoke-static {v5}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@16
    move-result-object v0

    #@17
    .line 453
    .local v0, b:Ljava/nio/ByteBuffer;
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@1a
    .line 454
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    #@1d
    .line 455
    iget-object v5, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSocketChannel:Ljava/nio/channels/SocketChannel;

    #@1f
    invoke-virtual {v5, v0}, Ljava/nio/channels/SocketChannel;->write(Ljava/nio/ByteBuffer;)I

    #@22
    move-result v3

    #@23
    .line 456
    if-lt v3, v2, :cond_a

    #@25
    goto :goto_10
.end method

.method private writeSSLImpl()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 485
    iget-object v1, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mAppSendBuffer:Ljava/nio/ByteBuffer;

    #@2
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    #@5
    move-result v0

    #@6
    .line 486
    .local v0, count:I
    invoke-direct {p0}, Landroid/webkit/WebSocket$WebSocketImpl;->flushSSLBuffer()I

    #@9
    move-result v1

    #@a
    if-lez v1, :cond_10

    #@c
    if-lez v0, :cond_10

    #@e
    .line 487
    const/4 v1, 0x0

    #@f
    .line 515
    :goto_f
    return v1

    #@10
    .line 490
    :cond_10
    iget-object v1, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mAppSendBuffer:Ljava/nio/ByteBuffer;

    #@12
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    #@15
    .line 491
    iget-object v1, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngine:Ljavax/net/ssl/SSLEngine;

    #@17
    iget-object v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mAppSendBuffer:Ljava/nio/ByteBuffer;

    #@19
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mNetSendBuffer:Ljava/nio/ByteBuffer;

    #@1b
    invoke-virtual {v1, v2, v3}, Ljavax/net/ssl/SSLEngine;->wrap(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)Ljavax/net/ssl/SSLEngineResult;

    #@1e
    move-result-object v1

    #@1f
    iput-object v1, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngineResult:Ljavax/net/ssl/SSLEngineResult;

    #@21
    .line 492
    iget-object v1, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mAppSendBuffer:Ljava/nio/ByteBuffer;

    #@23
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->compact()Ljava/nio/ByteBuffer;

    #@26
    .line 494
    sget-object v1, Landroid/webkit/WebSocket$2;->$SwitchMap$javax$net$ssl$SSLEngineResult$Status:[I

    #@28
    iget-object v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngineResult:Ljavax/net/ssl/SSLEngineResult;

    #@2a
    invoke-virtual {v2}, Ljavax/net/ssl/SSLEngineResult;->getStatus()Ljavax/net/ssl/SSLEngineResult$Status;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v2}, Ljavax/net/ssl/SSLEngineResult$Status;->ordinal()I

    #@31
    move-result v2

    #@32
    aget v1, v1, v2

    #@34
    packed-switch v1, :pswitch_data_6a

    #@37
    .line 512
    :cond_37
    :goto_37
    :pswitch_37
    invoke-direct {p0}, Landroid/webkit/WebSocket$WebSocketImpl;->processSSLHandshakeStatus()Z

    #@3a
    move-result v1

    #@3b
    if-nez v1, :cond_37

    #@3d
    move v1, v0

    #@3e
    .line 515
    goto :goto_f

    #@3f
    .line 496
    :pswitch_3f
    new-instance v1, Ljava/nio/BufferUnderflowException;

    #@41
    invoke-direct {v1}, Ljava/nio/BufferUnderflowException;-><init>()V

    #@44
    throw v1

    #@45
    .line 506
    :pswitch_45
    new-instance v1, Ljavax/net/ssl/SSLException;

    #@47
    new-instance v2, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v3, "SSLEngine: invalid state for write - "

    #@4e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v2

    #@52
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSSLEngineResult:Ljavax/net/ssl/SSLEngineResult;

    #@54
    invoke-virtual {v3}, Ljavax/net/ssl/SSLEngineResult;->getStatus()Ljavax/net/ssl/SSLEngineResult$Status;

    #@57
    move-result-object v3

    #@58
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v2

    #@5c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v2

    #@60
    invoke-direct {v1, v2}, Ljavax/net/ssl/SSLException;-><init>(Ljava/lang/String;)V

    #@63
    throw v1

    #@64
    .line 509
    :pswitch_64
    invoke-direct {p0}, Landroid/webkit/WebSocket$WebSocketImpl;->flushSSLBuffer()I

    #@67
    move-result v1

    #@68
    sub-int/2addr v0, v1

    #@69
    goto :goto_37

    #@6a
    .line 494
    :pswitch_data_6a
    .packed-switch 0x1
        :pswitch_3f
        :pswitch_37
        :pswitch_45
        :pswitch_64
    .end packed-switch
.end method


# virtual methods
.method public close()V
    .registers 3

    #@0
    .prologue
    .line 224
    iget-boolean v1, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mClosed:Z

    #@2
    if-eqz v1, :cond_5

    #@4
    .line 234
    :goto_4
    return-void

    #@5
    .line 230
    :cond_5
    :try_start_5
    invoke-direct {p0}, Landroid/webkit/WebSocket$WebSocketImpl;->closeImpl()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_8} :catch_9

    #@8
    goto :goto_4

    #@9
    .line 231
    :catch_9
    move-exception v0

    #@a
    .line 232
    .local v0, e:Ljava/io/IOException;
    goto :goto_4
.end method

.method public connect(Ljava/net/URI;)Ljava/lang/Thread;
    .registers 8
    .parameter "uri"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljavax/net/ssl/SSLException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 164
    invoke-virtual {p1}, Ljava/net/URI;->getHost()Ljava/lang/String;

    #@5
    move-result-object v2

    #@6
    iput-object v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mHost:Ljava/lang/String;

    #@8
    .line 165
    invoke-virtual {p1}, Ljava/net/URI;->getPort()I

    #@b
    move-result v2

    #@c
    iput v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mPort:I

    #@e
    .line 167
    invoke-virtual {p1}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    #@11
    move-result-object v2

    #@12
    const-string v3, "https"

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@17
    move-result v2

    #@18
    if-eqz v2, :cond_63

    #@1a
    .line 168
    iput-boolean v5, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mIsSecure:Z

    #@1c
    .line 175
    :goto_1c
    iget-boolean v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mIsSecure:Z

    #@1e
    if-eqz v2, :cond_66

    #@20
    .line 176
    invoke-direct {p0, v4}, Landroid/webkit/WebSocket$WebSocketImpl;->setSocketRunning(Z)V

    #@23
    .line 182
    :goto_23
    invoke-static {}, Ljava/nio/channels/SocketChannel;->open()Ljava/nio/channels/SocketChannel;

    #@26
    move-result-object v2

    #@27
    iput-object v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSocketChannel:Ljava/nio/channels/SocketChannel;

    #@29
    .line 183
    iget-object v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSocketChannel:Ljava/nio/channels/SocketChannel;

    #@2b
    invoke-virtual {v2, v4}, Ljava/nio/channels/SocketChannel;->configureBlocking(Z)Ljava/nio/channels/SelectableChannel;

    #@2e
    .line 184
    iget-object v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSocketChannel:Ljava/nio/channels/SocketChannel;

    #@30
    new-instance v3, Ljava/net/InetSocketAddress;

    #@32
    iget-object v4, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mHost:Ljava/lang/String;

    #@34
    iget v5, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mPort:I

    #@36
    invoke-direct {v3, v4, v5}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    #@39
    invoke-virtual {v2, v3}, Ljava/nio/channels/SocketChannel;->connect(Ljava/net/SocketAddress;)Z

    #@3c
    .line 186
    const-string/jumbo v2, "java.net.preferIPv4Stack"

    #@3f
    const-string/jumbo v3, "true"

    #@42
    invoke-static {v2, v3}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@45
    .line 187
    const-string/jumbo v2, "java.net.preferIPv6Addresses"

    #@48
    const-string v3, "false"

    #@4a
    invoke-static {v2, v3}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@4d
    .line 189
    invoke-static {}, Ljava/nio/channels/Selector;->open()Ljava/nio/channels/Selector;

    #@50
    move-result-object v2

    #@51
    iput-object v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSelector:Ljava/nio/channels/Selector;

    #@53
    .line 190
    iget-object v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSocketChannel:Ljava/nio/channels/SocketChannel;

    #@55
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSelector:Ljava/nio/channels/Selector;

    #@57
    const/16 v4, 0x9

    #@59
    invoke-virtual {v2, v3, v4}, Ljava/nio/channels/SocketChannel;->register(Ljava/nio/channels/Selector;I)Ljava/nio/channels/SelectionKey;

    #@5c
    .line 195
    iget-boolean v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mIsSecure:Z

    #@5e
    if-eqz v2, :cond_6a

    #@60
    .line 196
    const/4 v0, 0x0

    #@61
    .local v0, th:Ljava/lang/Thread;
    move-object v1, v0

    #@62
    .line 203
    .end local v0           #th:Ljava/lang/Thread;
    .local v1, th:Ljava/lang/Thread;
    :goto_62
    return-object v1

    #@63
    .line 170
    .end local v1           #th:Ljava/lang/Thread;
    :cond_63
    iput-boolean v4, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mIsSecure:Z

    #@65
    goto :goto_1c

    #@66
    .line 178
    :cond_66
    invoke-direct {p0, v5}, Landroid/webkit/WebSocket$WebSocketImpl;->setSocketRunning(Z)V

    #@69
    goto :goto_23

    #@6a
    .line 201
    :cond_6a
    new-instance v0, Ljava/lang/Thread;

    #@6c
    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@6f
    .line 202
    .restart local v0       #th:Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@72
    move-object v1, v0

    #@73
    .line 203
    .end local v0           #th:Ljava/lang/Thread;
    .restart local v1       #th:Ljava/lang/Thread;
    goto :goto_62
.end method

.method public getData()Ljava/nio/ByteBuffer;
    .registers 7

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 263
    const/4 v1, 0x0

    #@2
    .line 264
    .local v1, data:Ljava/nio/ByteBuffer;
    const/4 v2, 0x0

    #@3
    .line 266
    .local v2, msg:Ljava/nio/ByteBuffer;
    :cond_3
    invoke-virtual {p0}, Landroid/webkit/WebSocket$WebSocketImpl;->getReadQueueData()Ljava/nio/ByteBuffer;

    #@6
    move-result-object v2

    #@7
    .line 267
    if-nez v2, :cond_a

    #@9
    .line 285
    :cond_9
    :goto_9
    return-object v1

    #@a
    .line 270
    :cond_a
    if-eqz v1, :cond_3b

    #@c
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    #@f
    move-result v3

    #@10
    :goto_10
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->capacity()I

    #@13
    move-result v5

    #@14
    add-int/2addr v3, v5

    #@15
    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@18
    move-result-object v0

    #@19
    .line 273
    .local v0, ch:Ljava/nio/ByteBuffer;
    if-eqz v1, :cond_21

    #@1b
    .line 274
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    #@1e
    .line 275
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    #@21
    .line 277
    :cond_21
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    #@24
    .line 278
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->capacity()I

    #@2b
    move-result v5

    #@2c
    invoke-virtual {v0, v3, v4, v5}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    #@2f
    .line 279
    move-object v1, v0

    #@30
    .line 281
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    #@33
    move-result v3

    #@34
    const/16 v5, 0x2000

    #@36
    if-gt v3, v5, :cond_9

    #@38
    .line 284
    if-nez v2, :cond_3

    #@3a
    goto :goto_9

    #@3b
    .end local v0           #ch:Ljava/nio/ByteBuffer;
    :cond_3b
    move v3, v4

    #@3c
    .line 270
    goto :goto_10
.end method

.method public declared-synchronized getReadQueueData()Ljava/nio/ByteBuffer;
    .registers 2

    #@0
    .prologue
    .line 291
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mBufferReadQueue:Ljava/util/concurrent/BlockingQueue;

    #@3
    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->poll()Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Ljava/nio/ByteBuffer;
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_b

    #@9
    monitor-exit p0

    #@a
    return-object v0

    #@b
    :catchall_b
    move-exception v0

    #@c
    monitor-exit p0

    #@d
    throw v0
.end method

.method public declared-synchronized getWriteQueueData()Ljava/nio/ByteBuffer;
    .registers 2

    #@0
    .prologue
    .line 297
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mBufferWriteQueue:Ljava/util/concurrent/BlockingQueue;

    #@3
    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->poll()Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Ljava/nio/ByteBuffer;
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_b

    #@9
    monitor-exit p0

    #@a
    return-object v0

    #@b
    :catchall_b
    move-exception v0

    #@c
    monitor-exit p0

    #@d
    throw v0
.end method

.method public declared-synchronized putReadQueueData(Ljava/nio/ByteBuffer;)V
    .registers 3
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    #@0
    .prologue
    .line 303
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    #@4
    .line 304
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mBufferReadQueue:Ljava/util/concurrent/BlockingQueue;

    #@6
    invoke-interface {v0, p1}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_b

    #@9
    .line 305
    monitor-exit p0

    #@a
    return-void

    #@b
    .line 303
    :catchall_b
    move-exception v0

    #@c
    monitor-exit p0

    #@d
    throw v0
.end method

.method public declared-synchronized putWriteQueueData(Ljava/nio/ByteBuffer;)V
    .registers 3
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    #@0
    .prologue
    .line 310
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    #@4
    .line 311
    iget-object v0, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mBufferWriteQueue:Ljava/util/concurrent/BlockingQueue;

    #@6
    invoke-interface {v0, p1}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_b

    #@9
    .line 312
    monitor-exit p0

    #@a
    return-void

    #@b
    .line 310
    :catchall_b
    move-exception v0

    #@c
    monitor-exit p0

    #@d
    throw v0
.end method

.method public run()V
    .registers 3

    #@0
    .prologue
    .line 209
    :goto_0
    invoke-direct {p0}, Landroid/webkit/WebSocket$WebSocketImpl;->isSocketRunning()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_18

    #@6
    .line 211
    :try_start_6
    invoke-direct {p0}, Landroid/webkit/WebSocket$WebSocketImpl;->handleRunnable()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_9} :catch_a
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_9} :catch_11

    #@9
    goto :goto_0

    #@a
    .line 212
    :catch_a
    move-exception v0

    #@b
    .line 213
    .local v0, e:Ljava/io/IOException;
    iget-object v1, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mWebSocket:Landroid/webkit/WebSocket;

    #@d
    invoke-virtual {v1, v0}, Landroid/webkit/WebSocket;->onError(Ljava/lang/Throwable;)V

    #@10
    goto :goto_0

    #@11
    .line 214
    .end local v0           #e:Ljava/io/IOException;
    :catch_11
    move-exception v0

    #@12
    .line 215
    .local v0, e:Ljava/lang/InterruptedException;
    iget-object v1, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mWebSocket:Landroid/webkit/WebSocket;

    #@14
    invoke-virtual {v1, v0}, Landroid/webkit/WebSocket;->onError(Ljava/lang/Throwable;)V

    #@17
    goto :goto_0

    #@18
    .line 218
    .end local v0           #e:Ljava/lang/InterruptedException;
    :cond_18
    return-void
.end method

.method public send()V
    .registers 6

    #@0
    .prologue
    .line 240
    iget-boolean v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mClosed:Z

    #@2
    if-eqz v2, :cond_5

    #@4
    .line 258
    :goto_4
    return-void

    #@5
    .line 246
    :cond_5
    :try_start_5
    iget-boolean v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mIsSecure:Z

    #@7
    if-eqz v2, :cond_18

    #@9
    .line 247
    invoke-virtual {p0}, Landroid/webkit/WebSocket$WebSocketImpl;->getWriteQueueData()Ljava/nio/ByteBuffer;

    #@c
    move-result-object v1

    #@d
    .line 248
    .local v1, msg:Ljava/nio/ByteBuffer;
    if-eqz v1, :cond_18

    #@f
    .line 249
    iget-object v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mAppSendBuffer:Ljava/nio/ByteBuffer;

    #@11
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    #@14
    move-result-object v3

    #@15
    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@18
    .line 252
    .end local v1           #msg:Ljava/nio/ByteBuffer;
    :cond_18
    iget-object v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSocketChannel:Ljava/nio/channels/SocketChannel;

    #@1a
    iget-object v3, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mSelector:Ljava/nio/channels/Selector;

    #@1c
    const/16 v4, 0xd

    #@1e
    invoke-virtual {v2, v3, v4}, Ljava/nio/channels/SocketChannel;->register(Ljava/nio/channels/Selector;I)Ljava/nio/channels/SelectionKey;
    :try_end_21
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_5 .. :try_end_21} :catch_22

    #@21
    goto :goto_4

    #@22
    .line 255
    :catch_22
    move-exception v0

    #@23
    .line 256
    .local v0, e:Ljava/nio/channels/ClosedChannelException;
    iget-object v2, p0, Landroid/webkit/WebSocket$WebSocketImpl;->mWebSocket:Landroid/webkit/WebSocket;

    #@25
    invoke-virtual {v2, v0}, Landroid/webkit/WebSocket;->onError(Ljava/lang/Throwable;)V

    #@28
    goto :goto_4
.end method
