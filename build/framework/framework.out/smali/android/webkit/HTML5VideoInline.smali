.class public Landroid/webkit/HTML5VideoInline;
.super Landroid/webkit/HTML5VideoView;
.source "HTML5VideoInline.java"


# static fields
.field private static mSurfaceTexture:Landroid/graphics/SurfaceTexture;

.field private static mTextureNames:[I

.field private static mVideoLayerUsingSurfaceTexture:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 37
    sput-object v0, Landroid/webkit/HTML5VideoInline;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@3
    .line 38
    sput-object v0, Landroid/webkit/HTML5VideoInline;->mTextureNames:[I

    #@5
    .line 41
    const/4 v0, -0x1

    #@6
    sput v0, Landroid/webkit/HTML5VideoInline;->mVideoLayerUsingSurfaceTexture:I

    #@8
    return-void
.end method

.method constructor <init>(IIZ)V
    .registers 4
    .parameter "videoLayerId"
    .parameter "position"
    .parameter "skipPrepare"

    #@0
    .prologue
    .line 51
    invoke-direct {p0}, Landroid/webkit/HTML5VideoView;-><init>()V

    #@3
    .line 52
    invoke-virtual {p0, p1, p2, p3}, Landroid/webkit/HTML5VideoInline;->init(IIZ)V

    #@6
    .line 53
    return-void
.end method

.method public static cleanupSurfaceTexture()V
    .registers 1

    #@0
    .prologue
    .line 112
    const/4 v0, 0x0

    #@1
    sput-object v0, Landroid/webkit/HTML5VideoInline;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@3
    .line 113
    const/4 v0, -0x1

    #@4
    sput v0, Landroid/webkit/HTML5VideoInline;->mVideoLayerUsingSurfaceTexture:I

    #@6
    .line 114
    return-void
.end method

.method public static getSurfaceTexture(I)Landroid/graphics/SurfaceTexture;
    .registers 4
    .parameter "videoLayerId"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 87
    sget v0, Landroid/webkit/HTML5VideoInline;->mVideoLayerUsingSurfaceTexture:I

    #@4
    if-ne p0, v0, :cond_e

    #@6
    sget-object v0, Landroid/webkit/HTML5VideoInline;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@8
    if-eqz v0, :cond_e

    #@a
    sget-object v0, Landroid/webkit/HTML5VideoInline;->mTextureNames:[I

    #@c
    if-nez v0, :cond_22

    #@e
    .line 93
    :cond_e
    new-array v0, v1, [I

    #@10
    sput-object v0, Landroid/webkit/HTML5VideoInline;->mTextureNames:[I

    #@12
    .line 94
    sget-object v0, Landroid/webkit/HTML5VideoInline;->mTextureNames:[I

    #@14
    invoke-static {v1, v0, v2}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    #@17
    .line 95
    new-instance v0, Landroid/graphics/SurfaceTexture;

    #@19
    sget-object v1, Landroid/webkit/HTML5VideoInline;->mTextureNames:[I

    #@1b
    aget v1, v1, v2

    #@1d
    invoke-direct {v0, v1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    #@20
    sput-object v0, Landroid/webkit/HTML5VideoInline;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@22
    .line 97
    :cond_22
    sput p0, Landroid/webkit/HTML5VideoInline;->mVideoLayerUsingSurfaceTexture:I

    #@24
    .line 98
    sget-object v0, Landroid/webkit/HTML5VideoInline;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@26
    return-object v0
.end method

.method private setFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V
    .registers 3
    .parameter "l"

    #@0
    .prologue
    .line 127
    sget-object v0, Landroid/webkit/HTML5VideoInline;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 128
    sget-object v0, Landroid/webkit/HTML5VideoInline;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@6
    invoke-virtual {v0, p1}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    #@9
    .line 130
    :cond_9
    return-void
.end method

.method public static surfaceTextureDeleted()Z
    .registers 1

    #@0
    .prologue
    .line 102
    sget-object v0, Landroid/webkit/HTML5VideoInline;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method


# virtual methods
.method public decideDisplayMode()V
    .registers 4

    #@0
    .prologue
    .line 57
    invoke-virtual {p0}, Landroid/webkit/HTML5VideoInline;->getVideoLayerId()I

    #@3
    move-result v2

    #@4
    invoke-static {v2}, Landroid/webkit/HTML5VideoInline;->getSurfaceTexture(I)Landroid/graphics/SurfaceTexture;

    #@7
    move-result-object v1

    #@8
    .line 58
    .local v1, surfaceTexture:Landroid/graphics/SurfaceTexture;
    new-instance v0, Landroid/view/Surface;

    #@a
    invoke-direct {v0, v1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    #@d
    .line 59
    .local v0, surface:Landroid/view/Surface;
    sget-object v2, Landroid/webkit/HTML5VideoInline;->mPlayer:Landroid/media/MediaPlayer;

    #@f
    invoke-virtual {v2, v0}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    #@12
    .line 60
    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    #@15
    .line 61
    return-void
.end method

.method public deleteSurfaceTexture()V
    .registers 1

    #@0
    .prologue
    .line 107
    invoke-static {}, Landroid/webkit/HTML5VideoInline;->cleanupSurfaceTexture()V

    #@3
    .line 108
    return-void
.end method

.method public getTextureName()I
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 119
    sget-object v1, Landroid/webkit/HTML5VideoInline;->mTextureNames:[I

    #@3
    if-eqz v1, :cond_9

    #@5
    .line 120
    sget-object v1, Landroid/webkit/HTML5VideoInline;->mTextureNames:[I

    #@7
    aget v0, v1, v0

    #@9
    .line 122
    :cond_9
    return v0
.end method

.method public pauseAndDispatch(Landroid/webkit/HTML5VideoViewProxy;)V
    .registers 2
    .parameter "proxy"

    #@0
    .prologue
    .line 80
    invoke-super {p0, p1}, Landroid/webkit/HTML5VideoView;->pauseAndDispatch(Landroid/webkit/HTML5VideoViewProxy;)V

    #@3
    .line 81
    return-void
.end method

.method public prepareDataAndDisplayMode(Landroid/webkit/HTML5VideoViewProxy;)V
    .registers 5
    .parameter "proxy"

    #@0
    .prologue
    .line 67
    invoke-super {p0, p1}, Landroid/webkit/HTML5VideoView;->prepareDataAndDisplayMode(Landroid/webkit/HTML5VideoViewProxy;)V

    #@3
    .line 68
    invoke-direct {p0, p1}, Landroid/webkit/HTML5VideoInline;->setFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    #@6
    .line 71
    iget-object v0, p0, Landroid/webkit/HTML5VideoView;->mProxy:Landroid/webkit/HTML5VideoViewProxy;

    #@8
    invoke-virtual {v0}, Landroid/webkit/HTML5VideoViewProxy;->getContext()Landroid/content/Context;

    #@b
    move-result-object v0

    #@c
    const-string v1, "android.permission.WAKE_LOCK"

    #@e
    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@11
    move-result v0

    #@12
    if-nez v0, :cond_1f

    #@14
    .line 73
    sget-object v0, Landroid/webkit/HTML5VideoInline;->mPlayer:Landroid/media/MediaPlayer;

    #@16
    invoke-virtual {p1}, Landroid/webkit/HTML5VideoViewProxy;->getContext()Landroid/content/Context;

    #@19
    move-result-object v1

    #@1a
    const/16 v2, 0x1a

    #@1c
    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setWakeMode(Landroid/content/Context;I)V

    #@1f
    .line 75
    :cond_1f
    return-void
.end method

.method public start()V
    .registers 2

    #@0
    .prologue
    .line 46
    invoke-virtual {p0}, Landroid/webkit/HTML5VideoInline;->getPauseDuringPreparing()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_9

    #@6
    .line 47
    invoke-super {p0}, Landroid/webkit/HTML5VideoView;->start()V

    #@9
    .line 49
    :cond_9
    return-void
.end method
