.class Landroid/webkit/GeolocationPermissionsClassic$1;
.super Landroid/os/Handler;
.source "GeolocationPermissionsClassic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/webkit/GeolocationPermissionsClassic;->createUIHandler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/webkit/GeolocationPermissionsClassic;


# direct methods
.method constructor <init>(Landroid/webkit/GeolocationPermissionsClassic;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 68
    iput-object p1, p0, Landroid/webkit/GeolocationPermissionsClassic$1;->this$0:Landroid/webkit/GeolocationPermissionsClassic;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 8
    .parameter "msg"

    #@0
    .prologue
    .line 72
    iget v5, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v5, :pswitch_data_38

    #@5
    .line 86
    :goto_5
    return-void

    #@6
    .line 74
    :pswitch_6
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8
    check-cast v4, Ljava/util/Map;

    #@a
    .line 75
    .local v4, values:Ljava/util/Map;
    const-string/jumbo v5, "origins"

    #@d
    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@10
    move-result-object v3

    #@11
    check-cast v3, Ljava/util/Set;

    #@13
    .line 76
    .local v3, origins:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    const-string v5, "callback"

    #@15
    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@18
    move-result-object v2

    #@19
    check-cast v2, Landroid/webkit/ValueCallback;

    #@1b
    .line 77
    .local v2, callback:Landroid/webkit/ValueCallback;,"Landroid/webkit/ValueCallback<Ljava/util/Set<Ljava/lang/String;>;>;"
    invoke-interface {v2, v3}, Landroid/webkit/ValueCallback;->onReceiveValue(Ljava/lang/Object;)V

    #@1e
    goto :goto_5

    #@1f
    .line 80
    .end local v2           #callback:Landroid/webkit/ValueCallback;,"Landroid/webkit/ValueCallback<Ljava/util/Set<Ljava/lang/String;>;>;"
    .end local v3           #origins:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    .end local v4           #values:Ljava/util/Map;
    :pswitch_1f
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@21
    check-cast v4, Ljava/util/Map;

    #@23
    .line 81
    .restart local v4       #values:Ljava/util/Map;
    const-string v5, "allowed"

    #@25
    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@28
    move-result-object v0

    #@29
    check-cast v0, Ljava/lang/Boolean;

    #@2b
    .line 82
    .local v0, allowed:Ljava/lang/Boolean;
    const-string v5, "callback"

    #@2d
    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@30
    move-result-object v1

    #@31
    check-cast v1, Landroid/webkit/ValueCallback;

    #@33
    .line 83
    .local v1, callback:Landroid/webkit/ValueCallback;,"Landroid/webkit/ValueCallback<Ljava/lang/Boolean;>;"
    invoke-interface {v1, v0}, Landroid/webkit/ValueCallback;->onReceiveValue(Ljava/lang/Object;)V

    #@36
    goto :goto_5

    #@37
    .line 72
    nop

    #@38
    :pswitch_data_38
    .packed-switch 0x0
        :pswitch_6
        :pswitch_1f
    .end packed-switch
.end method
