.class final Landroid/webkit/HTML5VideoViewProxy$VideoPlayer$1;
.super Landroid/content/BroadcastReceiver;
.source "HTML5VideoViewProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->enterFullScreenVideo(ILjava/lang/String;Landroid/webkit/HTML5VideoViewProxy;Landroid/webkit/WebViewClassic;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 323
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 7
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 326
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@4
    move-result-object v1

    #@5
    const-string/jumbo v2, "lge.browser.intent.action.HTML5VIDEO_BROWSER_PLAY"

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_37

    #@e
    .line 327
    const-string/jumbo v1, "position"

    #@11
    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@14
    move-result v0

    #@15
    .line 330
    .local v0, position:I
    invoke-static {}, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->access$600()Landroid/webkit/HTML5VideoViewProxy;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1, v0}, Landroid/webkit/HTML5VideoViewProxy;->onTimeupdateManually(I)V

    #@1c
    .line 331
    invoke-static {}, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->access$600()Landroid/webkit/HTML5VideoViewProxy;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, v0}, Landroid/webkit/HTML5VideoViewProxy;->seek(I)V

    #@23
    .line 332
    invoke-static {}, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->access$600()Landroid/webkit/HTML5VideoViewProxy;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v1}, Landroid/webkit/HTML5VideoViewProxy;->pause()V

    #@2a
    .line 333
    const/4 v1, 0x1

    #@2b
    if-ne v0, v1, :cond_34

    #@2d
    .line 334
    invoke-static {}, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->access$600()Landroid/webkit/HTML5VideoViewProxy;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v1}, Landroid/webkit/HTML5VideoViewProxy;->dispatchOnEnded()V

    #@34
    .line 336
    :cond_34
    invoke-static {v3}, Landroid/webkit/HTML5VideoViewProxy$VideoPlayer;->access$700(Z)V

    #@37
    .line 338
    .end local v0           #position:I
    :cond_37
    return-void
.end method
