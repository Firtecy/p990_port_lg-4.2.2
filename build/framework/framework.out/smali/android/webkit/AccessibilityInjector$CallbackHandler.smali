.class Landroid/webkit/AccessibilityInjector$CallbackHandler;
.super Ljava/lang/Object;
.source "AccessibilityInjector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/AccessibilityInjector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CallbackHandler"
.end annotation


# static fields
.field private static final JAVASCRIPT_ACTION_TEMPLATE:Ljava/lang/String; = "javascript:(function() { %s.onResult(%d, %s); })();"

.field private static final RESULT_TIMEOUT:J = 0x1388L


# instance fields
.field private mCallbackRunnable:Ljava/lang/Runnable;

.field private final mInterfaceName:Ljava/lang/String;

.field private final mMainHandler:Landroid/os/Handler;

.field private mResult:Z

.field private mResultId:I

.field private final mResultIdCounter:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final mResultLock:Ljava/lang/Object;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "interfaceName"

    #@0
    .prologue
    .line 821
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 811
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    #@5
    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    #@8
    iput-object v0, p0, Landroid/webkit/AccessibilityInjector$CallbackHandler;->mResultIdCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    #@a
    .line 812
    new-instance v0, Ljava/lang/Object;

    #@c
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@f
    iput-object v0, p0, Landroid/webkit/AccessibilityInjector$CallbackHandler;->mResultLock:Ljava/lang/Object;

    #@11
    .line 818
    const/4 v0, 0x0

    #@12
    iput-boolean v0, p0, Landroid/webkit/AccessibilityInjector$CallbackHandler;->mResult:Z

    #@14
    .line 819
    const/4 v0, -0x1

    #@15
    iput v0, p0, Landroid/webkit/AccessibilityInjector$CallbackHandler;->mResultId:I

    #@17
    .line 822
    iput-object p1, p0, Landroid/webkit/AccessibilityInjector$CallbackHandler;->mInterfaceName:Ljava/lang/String;

    #@19
    .line 823
    new-instance v0, Landroid/os/Handler;

    #@1b
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@1e
    iput-object v0, p0, Landroid/webkit/AccessibilityInjector$CallbackHandler;->mMainHandler:Landroid/os/Handler;

    #@20
    .line 824
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Landroid/webkit/AccessibilityInjector$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 804
    invoke-direct {p0, p1}, Landroid/webkit/AccessibilityInjector$CallbackHandler;-><init>(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$500(Landroid/webkit/AccessibilityInjector$CallbackHandler;Landroid/webkit/WebView;Ljava/lang/String;)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 804
    invoke-direct {p0, p1, p2}, Landroid/webkit/AccessibilityInjector$CallbackHandler;->performAction(Landroid/webkit/WebView;Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private clearResultLocked()V
    .registers 2

    #@0
    .prologue
    .line 861
    const/4 v0, -0x1

    #@1
    iput v0, p0, Landroid/webkit/AccessibilityInjector$CallbackHandler;->mResultId:I

    #@3
    .line 862
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Landroid/webkit/AccessibilityInjector$CallbackHandler;->mResult:Z

    #@6
    .line 863
    return-void
.end method

.method private getResultAndClear(I)Z
    .registers 6
    .parameter "resultId"

    #@0
    .prologue
    .line 849
    iget-object v3, p0, Landroid/webkit/AccessibilityInjector$CallbackHandler;->mResultLock:Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 850
    :try_start_3
    invoke-direct {p0, p1}, Landroid/webkit/AccessibilityInjector$CallbackHandler;->waitForResultTimedLocked(I)Z

    #@6
    move-result v1

    #@7
    .line 851
    .local v1, success:Z
    if-eqz v1, :cond_10

    #@9
    iget-boolean v0, p0, Landroid/webkit/AccessibilityInjector$CallbackHandler;->mResult:Z

    #@b
    .line 852
    .local v0, result:Z
    :goto_b
    invoke-direct {p0}, Landroid/webkit/AccessibilityInjector$CallbackHandler;->clearResultLocked()V

    #@e
    .line 853
    monitor-exit v3

    #@f
    return v0

    #@10
    .line 851
    .end local v0           #result:Z
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_b

    #@12
    .line 854
    .end local v1           #success:Z
    :catchall_12
    move-exception v2

    #@13
    monitor-exit v3
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    #@14
    throw v2
.end method

.method private performAction(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .registers 9
    .parameter "webView"
    .parameter "code"

    #@0
    .prologue
    .line 834
    iget-object v2, p0, Landroid/webkit/AccessibilityInjector$CallbackHandler;->mResultIdCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    #@2
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    #@5
    move-result v0

    #@6
    .line 835
    .local v0, resultId:I
    const-string/jumbo v2, "javascript:(function() { %s.onResult(%d, %s); })();"

    #@9
    const/4 v3, 0x3

    #@a
    new-array v3, v3, [Ljava/lang/Object;

    #@c
    const/4 v4, 0x0

    #@d
    iget-object v5, p0, Landroid/webkit/AccessibilityInjector$CallbackHandler;->mInterfaceName:Ljava/lang/String;

    #@f
    aput-object v5, v3, v4

    #@11
    const/4 v4, 0x1

    #@12
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@15
    move-result-object v5

    #@16
    aput-object v5, v3, v4

    #@18
    const/4 v4, 0x2

    #@19
    aput-object p2, v3, v4

    #@1b
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    .line 837
    .local v1, url:Ljava/lang/String;
    invoke-virtual {p1, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    #@22
    .line 839
    invoke-direct {p0, v0}, Landroid/webkit/AccessibilityInjector$CallbackHandler;->getResultAndClear(I)Z

    #@25
    move-result v2

    #@26
    return v2
.end method

.method private waitForResultTimedLocked(I)Z
    .registers 13
    .parameter "resultId"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 872
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@4
    move-result-wide v3

    #@5
    .line 874
    .local v3, startTimeMillis:J
    invoke-static {}, Landroid/webkit/AccessibilityInjector;->access$000()Z

    #@8
    move-result v8

    #@9
    if-eqz v8, :cond_2b

    #@b
    .line 875
    invoke-static {}, Landroid/webkit/AccessibilityInjector;->access$100()Ljava/lang/String;

    #@e
    move-result-object v8

    #@f
    new-instance v9, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v10, "Waiting for CVOX result with ID "

    #@16
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v9

    #@1a
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v9

    #@1e
    const-string v10, "..."

    #@20
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v9

    #@24
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v9

    #@28
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 880
    :cond_2b
    :goto_2b
    iget v8, p0, Landroid/webkit/AccessibilityInjector$CallbackHandler;->mResultId:I

    #@2d
    if-le v8, p1, :cond_3f

    #@2f
    .line 881
    invoke-static {}, Landroid/webkit/AccessibilityInjector;->access$000()Z

    #@32
    move-result v8

    #@33
    if-eqz v8, :cond_3e

    #@35
    .line 882
    invoke-static {}, Landroid/webkit/AccessibilityInjector;->access$100()Ljava/lang/String;

    #@38
    move-result-object v8

    #@39
    const-string v9, "Aborted CVOX result"

    #@3b
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 907
    :cond_3e
    :goto_3e
    return v7

    #@3f
    .line 887
    :cond_3f
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@42
    move-result-wide v8

    #@43
    sub-long v0, v8, v3

    #@45
    .line 890
    .local v0, elapsedTimeMillis:J
    invoke-static {}, Landroid/webkit/AccessibilityInjector;->access$000()Z

    #@48
    move-result v8

    #@49
    if-eqz v8, :cond_71

    #@4b
    .line 891
    invoke-static {}, Landroid/webkit/AccessibilityInjector;->access$100()Ljava/lang/String;

    #@4e
    move-result-object v8

    #@4f
    new-instance v9, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    const-string v10, "Check "

    #@56
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v9

    #@5a
    iget v10, p0, Landroid/webkit/AccessibilityInjector$CallbackHandler;->mResultId:I

    #@5c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v9

    #@60
    const-string v10, " versus expected "

    #@62
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v9

    #@66
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@69
    move-result-object v9

    #@6a
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v9

    #@6e
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@71
    .line 893
    :cond_71
    iget v8, p0, Landroid/webkit/AccessibilityInjector$CallbackHandler;->mResultId:I

    #@73
    if-ne v8, p1, :cond_9d

    #@75
    .line 894
    invoke-static {}, Landroid/webkit/AccessibilityInjector;->access$000()Z

    #@78
    move-result v7

    #@79
    if-eqz v7, :cond_9b

    #@7b
    .line 895
    invoke-static {}, Landroid/webkit/AccessibilityInjector;->access$100()Ljava/lang/String;

    #@7e
    move-result-object v7

    #@7f
    new-instance v8, Ljava/lang/StringBuilder;

    #@81
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@84
    const-string v9, "Received CVOX result after "

    #@86
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v8

    #@8a
    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v8

    #@8e
    const-string v9, " ms"

    #@90
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v8

    #@94
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@97
    move-result-object v8

    #@98
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@9b
    .line 897
    :cond_9b
    const/4 v7, 0x1

    #@9c
    goto :goto_3e

    #@9d
    .line 900
    :cond_9d
    const-wide/16 v8, 0x1388

    #@9f
    sub-long v5, v8, v0

    #@a1
    .line 903
    .local v5, waitTimeMillis:J
    const-wide/16 v8, 0x0

    #@a3
    cmp-long v8, v5, v8

    #@a5
    if-gtz v8, :cond_b7

    #@a7
    .line 904
    invoke-static {}, Landroid/webkit/AccessibilityInjector;->access$000()Z

    #@aa
    move-result v8

    #@ab
    if-eqz v8, :cond_3e

    #@ad
    .line 905
    invoke-static {}, Landroid/webkit/AccessibilityInjector;->access$100()Ljava/lang/String;

    #@b0
    move-result-object v8

    #@b1
    const-string v9, "Timed out while waiting for CVOX result"

    #@b3
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b6
    goto :goto_3e

    #@b7
    .line 911
    :cond_b7
    :try_start_b7
    invoke-static {}, Landroid/webkit/AccessibilityInjector;->access$000()Z

    #@ba
    move-result v8

    #@bb
    if-eqz v8, :cond_c6

    #@bd
    .line 912
    invoke-static {}, Landroid/webkit/AccessibilityInjector;->access$100()Ljava/lang/String;

    #@c0
    move-result-object v8

    #@c1
    const-string v9, "Start waiting..."

    #@c3
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@c6
    .line 914
    :cond_c6
    iget-object v8, p0, Landroid/webkit/AccessibilityInjector$CallbackHandler;->mResultLock:Ljava/lang/Object;

    #@c8
    invoke-virtual {v8, v5, v6}, Ljava/lang/Object;->wait(J)V
    :try_end_cb
    .catch Ljava/lang/InterruptedException; {:try_start_b7 .. :try_end_cb} :catch_cd

    #@cb
    goto/16 :goto_2b

    #@cd
    .line 915
    :catch_cd
    move-exception v2

    #@ce
    .line 916
    .local v2, ie:Ljava/lang/InterruptedException;
    invoke-static {}, Landroid/webkit/AccessibilityInjector;->access$000()Z

    #@d1
    move-result v8

    #@d2
    if-eqz v8, :cond_2b

    #@d4
    .line 917
    invoke-static {}, Landroid/webkit/AccessibilityInjector;->access$100()Ljava/lang/String;

    #@d7
    move-result-object v8

    #@d8
    const-string v9, "Interrupted while waiting for CVOX result"

    #@da
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@dd
    goto/16 :goto_2b
.end method


# virtual methods
.method public callback()V
    .registers 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    #@0
    .prologue
    .line 973
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector$CallbackHandler;->mCallbackRunnable:Ljava/lang/Runnable;

    #@2
    if-eqz v0, :cond_e

    #@4
    .line 974
    iget-object v0, p0, Landroid/webkit/AccessibilityInjector$CallbackHandler;->mMainHandler:Landroid/os/Handler;

    #@6
    iget-object v1, p0, Landroid/webkit/AccessibilityInjector$CallbackHandler;->mCallbackRunnable:Ljava/lang/Runnable;

    #@8
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@b
    .line 975
    const/4 v0, 0x0

    #@c
    iput-object v0, p0, Landroid/webkit/AccessibilityInjector$CallbackHandler;->mCallbackRunnable:Ljava/lang/Runnable;

    #@e
    .line 977
    :cond_e
    return-void
.end method

.method public onResult(Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "id"
    .parameter "result"
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    #@0
    .prologue
    .line 933
    invoke-static {}, Landroid/webkit/AccessibilityInjector;->access$000()Z

    #@3
    move-result v2

    #@4
    if-eqz v2, :cond_2a

    #@6
    .line 934
    invoke-static {}, Landroid/webkit/AccessibilityInjector;->access$100()Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    new-instance v3, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v4, "Saw CVOX result of \'"

    #@11
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    const-string v4, "\' for ID "

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v3

    #@27
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 939
    :cond_2a
    :try_start_2a
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2d
    .catch Ljava/lang/NumberFormatException; {:try_start_2a .. :try_end_2d} :catch_44

    #@2d
    move-result v1

    #@2e
    .line 944
    .local v1, resultId:I
    iget-object v3, p0, Landroid/webkit/AccessibilityInjector$CallbackHandler;->mResultLock:Ljava/lang/Object;

    #@30
    monitor-enter v3

    #@31
    .line 945
    :try_start_31
    iget v2, p0, Landroid/webkit/AccessibilityInjector$CallbackHandler;->mResultId:I

    #@33
    if-le v1, v2, :cond_46

    #@35
    .line 946
    invoke-static {p2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@38
    move-result v2

    #@39
    iput-boolean v2, p0, Landroid/webkit/AccessibilityInjector$CallbackHandler;->mResult:Z

    #@3b
    .line 947
    iput v1, p0, Landroid/webkit/AccessibilityInjector$CallbackHandler;->mResultId:I

    #@3d
    .line 953
    :cond_3d
    :goto_3d
    iget-object v2, p0, Landroid/webkit/AccessibilityInjector$CallbackHandler;->mResultLock:Ljava/lang/Object;

    #@3f
    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    #@42
    .line 954
    monitor-exit v3

    #@43
    .line 955
    .end local v1           #resultId:I
    :goto_43
    return-void

    #@44
    .line 940
    :catch_44
    move-exception v0

    #@45
    .line 941
    .local v0, e:Ljava/lang/NumberFormatException;
    goto :goto_43

    #@46
    .line 949
    .end local v0           #e:Ljava/lang/NumberFormatException;
    .restart local v1       #resultId:I
    :cond_46
    invoke-static {}, Landroid/webkit/AccessibilityInjector;->access$000()Z

    #@49
    move-result v2

    #@4a
    if-eqz v2, :cond_3d

    #@4c
    .line 950
    invoke-static {}, Landroid/webkit/AccessibilityInjector;->access$100()Ljava/lang/String;

    #@4f
    move-result-object v2

    #@50
    new-instance v4, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v5, "Result with ID "

    #@57
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v4

    #@5b
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v4

    #@5f
    const-string v5, " was stale vesus "

    #@61
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v4

    #@65
    iget v5, p0, Landroid/webkit/AccessibilityInjector$CallbackHandler;->mResultId:I

    #@67
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v4

    #@6b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v4

    #@6f
    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@72
    goto :goto_3d

    #@73
    .line 954
    :catchall_73
    move-exception v2

    #@74
    monitor-exit v3
    :try_end_75
    .catchall {:try_start_31 .. :try_end_75} :catchall_73

    #@75
    throw v2
.end method

.method public requestCallback(Landroid/webkit/WebView;Ljava/lang/Runnable;)V
    .registers 5
    .parameter "webView"
    .parameter "callbackRunnable"

    #@0
    .prologue
    .line 965
    iput-object p2, p0, Landroid/webkit/AccessibilityInjector$CallbackHandler;->mCallbackRunnable:Ljava/lang/Runnable;

    #@2
    .line 967
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v1, "javascript:(function() { "

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v0

    #@e
    iget-object v1, p0, Landroid/webkit/AccessibilityInjector$CallbackHandler;->mInterfaceName:Ljava/lang/String;

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v0

    #@14
    const-string v1, ".callback(); })();"

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    #@21
    .line 968
    return-void
.end method
