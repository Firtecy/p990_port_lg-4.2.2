.class Landroid/webkit/CallbackProxy;
.super Landroid/os/Handler;
.source "CallbackProxy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/CallbackProxy$16;,
        Landroid/webkit/CallbackProxy$PickerMessageData;,
        Landroid/webkit/CallbackProxy$UploadFile;,
        Landroid/webkit/CallbackProxy$UploadFileMessageData;,
        Landroid/webkit/CallbackProxy$JsResultReceiver;,
        Landroid/webkit/CallbackProxy$ResultTransport;
    }
.end annotation


# static fields
.field private static final ADD_HISTORY_ITEM:I = 0x87

.field private static final ADD_MESSAGE_TO_CONSOLE:I = 0x81

.field private static final ASYNC_KEYEVENTS:I = 0x74

.field private static final AUTH_CREDENTIALS:I = 0x89

.field private static final AUTH_REQUEST:I = 0x68

.field private static final AUTO_LOGIN:I = 0x8c

.field private static final CLIENT_CERT_REQUEST:I = 0x8d

.field private static final CLOSE_WINDOW:I = 0x6e

.field private static final CREATE_WINDOW:I = 0x6d

.field private static final DOWNLOAD_FILE:I = 0x76

.field private static final EXCEEDED_DATABASE_QUOTA:I = 0x7e

.field private static final GEOLOCATION_PERMISSIONS_HIDE_PROMPT:I = 0x83

.field private static final GEOLOCATION_PERMISSIONS_SHOW_PROMPT:I = 0x82

.field private static final GET_VISITED_HISTORY:I = 0x85

.field private static final HISTORY_INDEX_CHANGED:I = 0x88

.field private static final JS_ALERT:I = 0x70

.field private static final JS_CONFIRM:I = 0x71

.field private static final JS_PROMPT:I = 0x72

.field private static final JS_TIMEOUT:I = 0x80

.field private static final JS_UNLOAD:I = 0x73

.field private static final LOAD_RESOURCE:I = 0x6c

.field private static final LOGTAG:Ljava/lang/String; = "CallbackProxy"

.field private static final NOTIFY:I = 0xc8

.field private static final OPEN_COLOR_PICKER:I = 0x92

.field private static final OPEN_DATETIME_PICKER:I = 0x91

.field private static final OPEN_FILE_CHOOSER:I = 0x86

.field private static final OVERRIDE_URL:I = 0x67

.field private static final PAGE_FINISHED:I = 0x79

.field private static final PAGE_STARTED:I = 0x64

.field private static final PERF_PROBE:Z = false

.field private static final PROCEEDED_AFTER_SSL_ERROR:I = 0x90

.field private static final PROGRESS:I = 0x6a

.field private static final REACHED_APPCACHE_MAXSIZE:I = 0x7f

.field private static final RECEIVED_CERTIFICATE:I = 0x7c

.field private static final RECEIVED_ICON:I = 0x65

.field private static final RECEIVED_TITLE:I = 0x66

.field private static final RECEIVED_TOUCH_ICON_URL:I = 0x84

.field private static final REPORT_ERROR:I = 0x77

.field private static final REQUEST_FOCUS:I = 0x7a

.field private static final RESEND_POST_DATA:I = 0x78

.field private static final SAVE_PASSWORD:I = 0x6f

.field private static final SCALE_CHANGED:I = 0x7b

.field private static final SSL_ERROR:I = 0x69

.field private static final SWITCH_OUT_HISTORY:I = 0x7d

.field private static final UPDATE_VISITED:I = 0x6b


# instance fields
.field private final mBackForwardList:Landroid/webkit/WebBackForwardListClassic;

.field private mBlockMessages:Z

.field private final mContext:Landroid/content/Context;

.field private volatile mDownloadListener:Landroid/webkit/DownloadListener;

.field private volatile mDownloadListenerExt:Landroid/webkit/DownloadListenerExt;

.field private volatile mLatestProgress:I

.field private mProgressMustUpdate:Z

.field private mProgressUpdatePending:Z

.field private mUserGesture:Z

.field private mWaitForUiThreadSync:Z

.field private volatile mWebBackForwardListClient:Landroid/webkit/WebBackForwardListClient;

.field private volatile mWebChromeClient:Landroid/webkit/WebChromeClient;

.field private mWebCoreIdleTime:J

.field private mWebCoreThreadTime:J

.field private final mWebView:Landroid/webkit/WebViewClassic;

.field private volatile mWebViewClient:Landroid/webkit/WebViewClient;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/webkit/WebViewClassic;)V
    .registers 4
    .parameter "context"
    .parameter "w"

    #@0
    .prologue
    .line 198
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@3
    .line 74
    const/16 v0, 0x64

    #@5
    iput v0, p0, Landroid/webkit/CallbackProxy;->mLatestProgress:I

    #@7
    .line 84
    const/4 v0, 0x0

    #@8
    iput-boolean v0, p0, Landroid/webkit/CallbackProxy;->mUserGesture:Z

    #@a
    .line 200
    iput-object p1, p0, Landroid/webkit/CallbackProxy;->mContext:Landroid/content/Context;

    #@c
    .line 201
    iput-object p2, p0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@e
    .line 202
    new-instance v0, Landroid/webkit/WebBackForwardListClassic;

    #@10
    invoke-direct {v0, p0}, Landroid/webkit/WebBackForwardListClassic;-><init>(Landroid/webkit/CallbackProxy;)V

    #@13
    iput-object v0, p0, Landroid/webkit/CallbackProxy;->mBackForwardList:Landroid/webkit/WebBackForwardListClassic;

    #@15
    .line 203
    return-void
.end method

.method static synthetic access$000(Landroid/webkit/CallbackProxy;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 57
    iget-boolean v0, p0, Landroid/webkit/CallbackProxy;->mUserGesture:Z

    #@2
    return v0
.end method

.method static synthetic access$002(Landroid/webkit/CallbackProxy;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 57
    iput-boolean p1, p0, Landroid/webkit/CallbackProxy;->mUserGesture:Z

    #@2
    return p1
.end method

.method private getJsDialogTitle(Ljava/lang/String;)Ljava/lang/String;
    .registers 10
    .parameter "url"

    #@0
    .prologue
    .line 1027
    move-object v1, p1

    #@1
    .line 1028
    .local v1, title:Ljava/lang/String;
    invoke-static {p1}, Landroid/webkit/URLUtil;->isDataUrl(Ljava/lang/String;)Z

    #@4
    move-result v2

    #@5
    if-eqz v2, :cond_11

    #@7
    .line 1030
    iget-object v2, p0, Landroid/webkit/CallbackProxy;->mContext:Landroid/content/Context;

    #@9
    const v3, 0x104036a

    #@c
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    .line 1041
    :goto_10
    return-object v1

    #@11
    .line 1033
    :cond_11
    :try_start_11
    new-instance v0, Ljava/net/URL;

    #@13
    invoke-direct {v0, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    #@16
    .line 1035
    .local v0, aUrl:Ljava/net/URL;
    iget-object v2, p0, Landroid/webkit/CallbackProxy;->mContext:Landroid/content/Context;

    #@18
    const v3, 0x1040369

    #@1b
    const/4 v4, 0x1

    #@1c
    new-array v4, v4, [Ljava/lang/Object;

    #@1e
    const/4 v5, 0x0

    #@1f
    new-instance v6, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    invoke-virtual {v0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    #@27
    move-result-object v7

    #@28
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v6

    #@2c
    const-string v7, "://"

    #@2e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v6

    #@32
    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    #@35
    move-result-object v7

    #@36
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v6

    #@3a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v6

    #@3e
    aput-object v6, v4, v5

    #@40
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_43
    .catch Ljava/net/MalformedURLException; {:try_start_11 .. :try_end_43} :catch_45

    #@43
    move-result-object v1

    #@44
    goto :goto_10

    #@45
    .line 1037
    .end local v0           #aUrl:Ljava/net/URL;
    :catch_45
    move-exception v2

    #@46
    goto :goto_10
.end method

.method private declared-synchronized sendMessageToUiThreadSync(Landroid/os/Message;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 1763
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p0, p1}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@4
    .line 1764
    invoke-static {}, Landroid/webkit/WebCoreThreadWatchdog;->pause()V
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_21

    #@7
    .line 1766
    :try_start_7
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_a
    .catchall {:try_start_7 .. :try_end_a} :catchall_21
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_a} :catch_f

    #@a
    .line 1771
    :goto_a
    :try_start_a
    invoke-static {}, Landroid/webkit/WebCoreThreadWatchdog;->resume()V
    :try_end_d
    .catchall {:try_start_a .. :try_end_d} :catchall_21

    #@d
    .line 1772
    monitor-exit p0

    #@e
    return-void

    #@f
    .line 1767
    :catch_f
    move-exception v0

    #@10
    .line 1768
    .local v0, e:Ljava/lang/InterruptedException;
    :try_start_10
    const-string v1, "CallbackProxy"

    #@12
    const-string v2, "Caught exception waiting for synchronous UI message to be processed"

    #@14
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 1769
    const-string v1, "CallbackProxy"

    #@19
    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_20
    .catchall {:try_start_10 .. :try_end_20} :catchall_21

    #@20
    goto :goto_a

    #@21
    .line 1763
    .end local v0           #e:Ljava/lang/InterruptedException;
    :catchall_21
    move-exception v1

    #@22
    monitor-exit p0

    #@23
    throw v1
.end method


# virtual methods
.method public addMessageToConsole(Ljava/lang/String;ILjava/lang/String;I)V
    .registers 8
    .parameter "message"
    .parameter "lineNumber"
    .parameter "sourceID"
    .parameter "msgLevel"

    #@0
    .prologue
    .line 1652
    iget-object v1, p0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 1662
    :goto_4
    return-void

    #@5
    .line 1656
    :cond_5
    const/16 v1, 0x81

    #@7
    invoke-virtual {p0, v1}, Landroid/webkit/CallbackProxy;->obtainMessage(I)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    .line 1657
    .local v0, msg:Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@e
    move-result-object v1

    #@f
    const-string/jumbo v2, "message"

    #@12
    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@15
    .line 1658
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@18
    move-result-object v1

    #@19
    const-string/jumbo v2, "sourceID"

    #@1c
    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@1f
    .line 1659
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@22
    move-result-object v1

    #@23
    const-string/jumbo v2, "lineNumber"

    #@26
    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@29
    .line 1660
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@2c
    move-result-object v1

    #@2d
    const-string/jumbo v2, "msgLevel"

    #@30
    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@33
    .line 1661
    invoke-virtual {p0, v0}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@36
    goto :goto_4
.end method

.method protected declared-synchronized blockMessages()V
    .registers 2

    #@0
    .prologue
    .line 206
    monitor-enter p0

    #@1
    const/4 v0, 0x1

    #@2
    :try_start_2
    iput-boolean v0, p0, Landroid/webkit/CallbackProxy;->mBlockMessages:Z
    :try_end_4
    .catchall {:try_start_2 .. :try_end_4} :catchall_6

    #@4
    .line 207
    monitor-exit p0

    #@5
    return-void

    #@6
    .line 206
    :catchall_6
    move-exception v0

    #@7
    monitor-exit p0

    #@8
    throw v0
.end method

.method canShowAlertDialog()Z
    .registers 2

    #@0
    .prologue
    .line 1759
    iget-object v0, p0, Landroid/webkit/CallbackProxy;->mContext:Landroid/content/Context;

    #@2
    instance-of v0, v0, Landroid/app/Activity;

    #@4
    return v0
.end method

.method public createWindow(ZZ)Landroid/webkit/BrowserFrame;
    .registers 12
    .parameter "dialog"
    .parameter "userGesture"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v5, 0x0

    #@3
    .line 1400
    iget-object v7, p0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@5
    if-nez v7, :cond_9

    #@7
    move-object v4, v6

    #@8
    .line 1420
    :goto_8
    return-object v4

    #@9
    .line 1404
    :cond_9
    new-instance v2, Landroid/webkit/WebView$WebViewTransport;

    #@b
    iget-object v7, p0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@d
    invoke-virtual {v7}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@10
    move-result-object v7

    #@11
    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@14
    invoke-direct {v2, v7}, Landroid/webkit/WebView$WebViewTransport;-><init>(Landroid/webkit/WebView;)V

    #@17
    .line 1406
    .local v2, transport:Landroid/webkit/WebView$WebViewTransport;
    const/16 v7, 0xc8

    #@19
    invoke-virtual {p0, v7}, Landroid/webkit/CallbackProxy;->obtainMessage(I)Landroid/os/Message;

    #@1c
    move-result-object v1

    #@1d
    .line 1407
    .local v1, msg:Landroid/os/Message;
    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1f
    .line 1408
    const/16 v8, 0x6d

    #@21
    if-eqz p1, :cond_45

    #@23
    move v7, v4

    #@24
    :goto_24
    if-eqz p2, :cond_47

    #@26
    :goto_26
    invoke-virtual {p0, v8, v7, v4, v1}, Landroid/webkit/CallbackProxy;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@29
    move-result-object v4

    #@2a
    invoke-direct {p0, v4}, Landroid/webkit/CallbackProxy;->sendMessageToUiThreadSync(Landroid/os/Message;)V

    #@2d
    .line 1410
    invoke-virtual {v2}, Landroid/webkit/WebView$WebViewTransport;->getWebView()Landroid/webkit/WebView;

    #@30
    move-result-object v4

    #@31
    invoke-static {v4}, Landroid/webkit/WebViewClassic;->fromWebView(Landroid/webkit/WebView;)Landroid/webkit/WebViewClassic;

    #@34
    move-result-object v3

    #@35
    .line 1411
    .local v3, w:Landroid/webkit/WebViewClassic;
    if-eqz v3, :cond_49

    #@37
    .line 1412
    invoke-virtual {v3}, Landroid/webkit/WebViewClassic;->getWebViewCore()Landroid/webkit/WebViewCore;

    #@3a
    move-result-object v0

    #@3b
    .line 1415
    .local v0, core:Landroid/webkit/WebViewCore;
    if-eqz v0, :cond_49

    #@3d
    .line 1416
    invoke-virtual {v0}, Landroid/webkit/WebViewCore;->initializeSubwindow()V

    #@40
    .line 1417
    invoke-virtual {v0}, Landroid/webkit/WebViewCore;->getBrowserFrame()Landroid/webkit/BrowserFrame;

    #@43
    move-result-object v4

    #@44
    goto :goto_8

    #@45
    .end local v0           #core:Landroid/webkit/WebViewCore;
    .end local v3           #w:Landroid/webkit/WebViewClassic;
    :cond_45
    move v7, v5

    #@46
    .line 1408
    goto :goto_24

    #@47
    :cond_47
    move v4, v5

    #@48
    goto :goto_26

    #@49
    .restart local v3       #w:Landroid/webkit/WebViewClassic;
    :cond_49
    move-object v4, v6

    #@4a
    .line 1420
    goto :goto_8
.end method

.method public doUpdateVisitedHistory(Ljava/lang/String;Z)V
    .registers 6
    .parameter "url"
    .parameter "isReload"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1247
    iget-object v0, p0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@3
    if-nez v0, :cond_6

    #@5
    .line 1251
    :goto_5
    return-void

    #@6
    .line 1250
    :cond_6
    const/16 v2, 0x6b

    #@8
    if-eqz p2, :cond_13

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    invoke-virtual {p0, v2, v0, v1, p1}, Landroid/webkit/CallbackProxy;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {p0, v0}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@12
    goto :goto_5

    #@13
    :cond_13
    move v0, v1

    #@14
    goto :goto_b
.end method

.method public getBackForwardList()Landroid/webkit/WebBackForwardListClassic;
    .registers 2

    #@0
    .prologue
    .line 273
    iget-object v0, p0, Landroid/webkit/CallbackProxy;->mBackForwardList:Landroid/webkit/WebBackForwardListClassic;

    #@2
    return-object v0
.end method

.method public getProgress()I
    .registers 2

    #@0
    .prologue
    .line 1016
    iget v0, p0, Landroid/webkit/CallbackProxy;->mLatestProgress:I

    #@2
    return v0
.end method

.method public getVisitedHistory(Landroid/webkit/ValueCallback;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/webkit/ValueCallback",
            "<[",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1676
    .local p1, callback:Landroid/webkit/ValueCallback;,"Landroid/webkit/ValueCallback<[Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 1682
    :goto_4
    return-void

    #@5
    .line 1679
    :cond_5
    const/16 v1, 0x85

    #@7
    invoke-virtual {p0, v1}, Landroid/webkit/CallbackProxy;->obtainMessage(I)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    .line 1680
    .local v0, msg:Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@d
    .line 1681
    invoke-virtual {p0, v0}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@10
    goto :goto_4
.end method

.method getWebBackForwardListClient()Landroid/webkit/WebBackForwardListClient;
    .registers 2

    #@0
    .prologue
    .line 281
    iget-object v0, p0, Landroid/webkit/CallbackProxy;->mWebBackForwardListClient:Landroid/webkit/WebBackForwardListClient;

    #@2
    return-object v0
.end method

.method public getWebChromeClient()Landroid/webkit/WebChromeClient;
    .registers 2

    #@0
    .prologue
    .line 248
    iget-object v0, p0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@2
    return-object v0
.end method

.method public getWebViewClient()Landroid/webkit/WebViewClient;
    .registers 2

    #@0
    .prologue
    .line 232
    iget-object v0, p0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@2
    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 71
    .parameter "msg"

    #@0
    .prologue
    .line 332
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/CallbackProxy;->messagesBlocked()Z

    #@3
    move-result v4

    #@4
    if-eqz v4, :cond_15

    #@6
    .line 334
    move-object/from16 v0, p0

    #@8
    iget-boolean v4, v0, Landroid/webkit/CallbackProxy;->mWaitForUiThreadSync:Z

    #@a
    if-eqz v4, :cond_11

    #@c
    .line 335
    monitor-enter p0

    #@d
    .line 336
    :try_start_d
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->notify()V

    #@10
    .line 337
    monitor-exit p0

    #@11
    .line 1010
    :cond_11
    :goto_11
    return-void

    #@12
    .line 337
    :catchall_12
    move-exception v4

    #@13
    monitor-exit p0
    :try_end_14
    .catchall {:try_start_d .. :try_end_14} :catchall_12

    #@14
    throw v4

    #@15
    .line 343
    :cond_15
    move-object/from16 v0, p1

    #@17
    iget v4, v0, Landroid/os/Message;->what:I

    #@19
    packed-switch v4, :pswitch_data_a5a

    #@1c
    :pswitch_1c
    goto :goto_11

    #@1d
    .line 345
    :pswitch_1d
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@20
    move-result-object v4

    #@21
    const-string/jumbo v10, "url"

    #@24
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@27
    move-result-object v65

    #@28
    .line 346
    .local v65, startedUrl:Ljava/lang/String;
    move-object/from16 v0, p0

    #@2a
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@2c
    move-object/from16 v0, v65

    #@2e
    invoke-virtual {v4, v0}, Landroid/webkit/WebViewClassic;->onPageStarted(Ljava/lang/String;)V

    #@31
    .line 347
    move-object/from16 v0, p0

    #@33
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@35
    if-eqz v4, :cond_11

    #@37
    .line 348
    move-object/from16 v0, p0

    #@39
    iget-object v10, v0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@3b
    move-object/from16 v0, p0

    #@3d
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@3f
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@42
    move-result-object v11

    #@43
    move-object/from16 v0, p1

    #@45
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@47
    check-cast v4, Landroid/graphics/Bitmap;

    #@49
    move-object/from16 v0, v65

    #@4b
    invoke-virtual {v10, v11, v0, v4}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    #@4e
    goto :goto_11

    #@4f
    .line 354
    .end local v65           #startedUrl:Ljava/lang/String;
    :pswitch_4f
    move-object/from16 v0, p1

    #@51
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@53
    move-object/from16 v43, v0

    #@55
    check-cast v43, Ljava/lang/String;

    #@57
    .line 355
    .local v43, finishedUrl:Ljava/lang/String;
    move-object/from16 v0, p0

    #@59
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@5b
    move-object/from16 v0, v43

    #@5d
    invoke-virtual {v4, v0}, Landroid/webkit/WebViewClassic;->onPageFinished(Ljava/lang/String;)V

    #@60
    .line 356
    move-object/from16 v0, p0

    #@62
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@64
    if-eqz v4, :cond_11

    #@66
    .line 357
    move-object/from16 v0, p0

    #@68
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@6a
    move-object/from16 v0, p0

    #@6c
    iget-object v10, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@6e
    invoke-virtual {v10}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@71
    move-result-object v10

    #@72
    move-object/from16 v0, v43

    #@74
    invoke-virtual {v4, v10, v0}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    #@77
    goto :goto_11

    #@78
    .line 362
    .end local v43           #finishedUrl:Ljava/lang/String;
    :pswitch_78
    move-object/from16 v0, p0

    #@7a
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@7c
    if-eqz v4, :cond_11

    #@7e
    .line 363
    move-object/from16 v0, p0

    #@80
    iget-object v10, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@82
    move-object/from16 v0, p0

    #@84
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@86
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@89
    move-result-object v11

    #@8a
    move-object/from16 v0, p1

    #@8c
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8e
    check-cast v4, Landroid/graphics/Bitmap;

    #@90
    invoke-virtual {v10, v11, v4}, Landroid/webkit/WebChromeClient;->onReceivedIcon(Landroid/webkit/WebView;Landroid/graphics/Bitmap;)V

    #@93
    goto/16 :goto_11

    #@95
    .line 368
    :pswitch_95
    move-object/from16 v0, p0

    #@97
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@99
    if-eqz v4, :cond_11

    #@9b
    .line 369
    move-object/from16 v0, p0

    #@9d
    iget-object v11, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@9f
    move-object/from16 v0, p0

    #@a1
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@a3
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@a6
    move-result-object v13

    #@a7
    move-object/from16 v0, p1

    #@a9
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@ab
    check-cast v4, Ljava/lang/String;

    #@ad
    move-object/from16 v0, p1

    #@af
    iget v10, v0, Landroid/os/Message;->arg1:I

    #@b1
    const/4 v14, 0x1

    #@b2
    if-ne v10, v14, :cond_ba

    #@b4
    const/4 v10, 0x1

    #@b5
    :goto_b5
    invoke-virtual {v11, v13, v4, v10}, Landroid/webkit/WebChromeClient;->onReceivedTouchIconUrl(Landroid/webkit/WebView;Ljava/lang/String;Z)V

    #@b8
    goto/16 :goto_11

    #@ba
    :cond_ba
    const/4 v10, 0x0

    #@bb
    goto :goto_b5

    #@bc
    .line 375
    :pswitch_bc
    move-object/from16 v0, p0

    #@be
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@c0
    if-eqz v4, :cond_11

    #@c2
    .line 376
    move-object/from16 v0, p0

    #@c4
    iget-object v10, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@c6
    move-object/from16 v0, p0

    #@c8
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@ca
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@cd
    move-result-object v11

    #@ce
    move-object/from16 v0, p1

    #@d0
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@d2
    check-cast v4, Ljava/lang/String;

    #@d4
    invoke-virtual {v10, v11, v4}, Landroid/webkit/WebChromeClient;->onReceivedTitle(Landroid/webkit/WebView;Ljava/lang/String;)V

    #@d7
    goto/16 :goto_11

    #@d9
    .line 382
    :pswitch_d9
    move-object/from16 v0, p0

    #@db
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@dd
    if-eqz v4, :cond_11

    #@df
    .line 383
    move-object/from16 v0, p1

    #@e1
    iget v0, v0, Landroid/os/Message;->arg1:I

    #@e3
    move/from16 v59, v0

    #@e5
    .line 384
    .local v59, reasonCode:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@e8
    move-result-object v4

    #@e9
    const-string v10, "description"

    #@eb
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@ee
    move-result-object v38

    #@ef
    .line 385
    .local v38, description:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@f2
    move-result-object v4

    #@f3
    const-string v10, "failingUrl"

    #@f5
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@f8
    move-result-object v42

    #@f9
    .line 386
    .local v42, failUrl:Ljava/lang/String;
    move-object/from16 v0, p0

    #@fb
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@fd
    move-object/from16 v0, p0

    #@ff
    iget-object v10, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@101
    invoke-virtual {v10}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@104
    move-result-object v10

    #@105
    move/from16 v0, v59

    #@107
    move-object/from16 v1, v38

    #@109
    move-object/from16 v2, v42

    #@10b
    invoke-virtual {v4, v10, v0, v1, v2}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    #@10e
    goto/16 :goto_11

    #@110
    .line 392
    .end local v38           #description:Ljava/lang/String;
    .end local v42           #failUrl:Ljava/lang/String;
    .end local v59           #reasonCode:I
    :pswitch_110
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@113
    move-result-object v4

    #@114
    const-string/jumbo v10, "resend"

    #@117
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@11a
    move-result-object v61

    #@11b
    check-cast v61, Landroid/os/Message;

    #@11d
    .line 394
    .local v61, resend:Landroid/os/Message;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@120
    move-result-object v4

    #@121
    const-string v10, "dontResend"

    #@123
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@126
    move-result-object v40

    #@127
    check-cast v40, Landroid/os/Message;

    #@129
    .line 396
    .local v40, dontResend:Landroid/os/Message;
    move-object/from16 v0, p0

    #@12b
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@12d
    if-eqz v4, :cond_144

    #@12f
    .line 397
    move-object/from16 v0, p0

    #@131
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@133
    move-object/from16 v0, p0

    #@135
    iget-object v10, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@137
    invoke-virtual {v10}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@13a
    move-result-object v10

    #@13b
    move-object/from16 v0, v40

    #@13d
    move-object/from16 v1, v61

    #@13f
    invoke-virtual {v4, v10, v0, v1}, Landroid/webkit/WebViewClient;->onFormResubmission(Landroid/webkit/WebView;Landroid/os/Message;Landroid/os/Message;)V

    #@142
    goto/16 :goto_11

    #@144
    .line 400
    :cond_144
    invoke-virtual/range {v40 .. v40}, Landroid/os/Message;->sendToTarget()V

    #@147
    goto/16 :goto_11

    #@149
    .line 405
    .end local v40           #dontResend:Landroid/os/Message;
    .end local v61           #resend:Landroid/os/Message;
    :pswitch_149
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@14c
    move-result-object v4

    #@14d
    const-string/jumbo v10, "url"

    #@150
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@153
    move-result-object v56

    #@154
    .line 406
    .local v56, overrideUrl:Ljava/lang/String;
    move-object/from16 v0, p0

    #@156
    move-object/from16 v1, v56

    #@158
    invoke-virtual {v0, v1}, Landroid/webkit/CallbackProxy;->uiOverrideUrlLoading(Ljava/lang/String;)Z

    #@15b
    move-result v55

    #@15c
    .line 407
    .local v55, override:Z
    move-object/from16 v0, p1

    #@15e
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@160
    move-object/from16 v62, v0

    #@162
    check-cast v62, Landroid/webkit/CallbackProxy$ResultTransport;

    #@164
    .line 409
    .local v62, result:Landroid/webkit/CallbackProxy$ResultTransport;,"Landroid/webkit/CallbackProxy$ResultTransport<Ljava/lang/Boolean;>;"
    monitor-enter p0

    #@165
    .line 410
    :try_start_165
    invoke-static/range {v55 .. v55}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@168
    move-result-object v4

    #@169
    move-object/from16 v0, v62

    #@16b
    invoke-virtual {v0, v4}, Landroid/webkit/CallbackProxy$ResultTransport;->setResult(Ljava/lang/Object;)V

    #@16e
    .line 411
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->notify()V

    #@171
    .line 412
    monitor-exit p0

    #@172
    goto/16 :goto_11

    #@174
    :catchall_174
    move-exception v4

    #@175
    monitor-exit p0
    :try_end_176
    .catchall {:try_start_165 .. :try_end_176} :catchall_174

    #@176
    throw v4

    #@177
    .line 416
    .end local v55           #override:Z
    .end local v56           #overrideUrl:Ljava/lang/String;
    .end local v62           #result:Landroid/webkit/CallbackProxy$ResultTransport;,"Landroid/webkit/CallbackProxy$ResultTransport<Ljava/lang/Boolean;>;"
    :pswitch_177
    move-object/from16 v0, p0

    #@179
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@17b
    if-eqz v4, :cond_11

    #@17d
    .line 417
    move-object/from16 v0, p1

    #@17f
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@181
    move-object/from16 v44, v0

    #@183
    check-cast v44, Landroid/webkit/HttpAuthHandler;

    #@185
    .line 418
    .local v44, handler:Landroid/webkit/HttpAuthHandler;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@188
    move-result-object v4

    #@189
    const-string v10, "host"

    #@18b
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@18e
    move-result-object v45

    #@18f
    .line 419
    .local v45, host:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@192
    move-result-object v4

    #@193
    const-string/jumbo v10, "realm"

    #@196
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@199
    move-result-object v58

    #@19a
    .line 420
    .local v58, realm:Ljava/lang/String;
    move-object/from16 v0, p0

    #@19c
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@19e
    move-object/from16 v0, p0

    #@1a0
    iget-object v10, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@1a2
    invoke-virtual {v10}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@1a5
    move-result-object v10

    #@1a6
    move-object/from16 v0, v44

    #@1a8
    move-object/from16 v1, v45

    #@1aa
    move-object/from16 v2, v58

    #@1ac
    invoke-virtual {v4, v10, v0, v1, v2}, Landroid/webkit/WebViewClient;->onReceivedHttpAuthRequest(Landroid/webkit/WebView;Landroid/webkit/HttpAuthHandler;Ljava/lang/String;Ljava/lang/String;)V

    #@1af
    goto/16 :goto_11

    #@1b1
    .line 426
    .end local v44           #handler:Landroid/webkit/HttpAuthHandler;
    .end local v45           #host:Ljava/lang/String;
    .end local v58           #realm:Ljava/lang/String;
    :pswitch_1b1
    move-object/from16 v0, p0

    #@1b3
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@1b5
    if-eqz v4, :cond_11

    #@1b7
    .line 427
    move-object/from16 v0, p1

    #@1b9
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1bb
    move-object/from16 v50, v0

    #@1bd
    check-cast v50, Ljava/util/HashMap;

    #@1bf
    .line 429
    .local v50, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    move-object/from16 v0, p0

    #@1c1
    iget-object v11, v0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@1c3
    move-object/from16 v0, p0

    #@1c5
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@1c7
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@1ca
    move-result-object v13

    #@1cb
    const-string v4, "handler"

    #@1cd
    move-object/from16 v0, v50

    #@1cf
    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1d2
    move-result-object v4

    #@1d3
    check-cast v4, Landroid/webkit/SslErrorHandler;

    #@1d5
    const-string v10, "error"

    #@1d7
    move-object/from16 v0, v50

    #@1d9
    invoke-virtual {v0, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1dc
    move-result-object v10

    #@1dd
    check-cast v10, Landroid/net/http/SslError;

    #@1df
    invoke-virtual {v11, v13, v4, v10}, Landroid/webkit/WebViewClient;->onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V

    #@1e2
    goto/16 :goto_11

    #@1e4
    .line 436
    .end local v50           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :pswitch_1e4
    move-object/from16 v0, p0

    #@1e6
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@1e8
    if-eqz v4, :cond_11

    #@1ea
    move-object/from16 v0, p0

    #@1ec
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@1ee
    instance-of v4, v4, Landroid/webkit/WebViewClientClassicExt;

    #@1f0
    if-eqz v4, :cond_11

    #@1f2
    .line 437
    move-object/from16 v0, p0

    #@1f4
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@1f6
    check-cast v4, Landroid/webkit/WebViewClientClassicExt;

    #@1f8
    move-object/from16 v0, p0

    #@1fa
    iget-object v10, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@1fc
    invoke-virtual {v10}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@1ff
    move-result-object v11

    #@200
    move-object/from16 v0, p1

    #@202
    iget-object v10, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@204
    check-cast v10, Landroid/net/http/SslError;

    #@206
    invoke-virtual {v4, v11, v10}, Landroid/webkit/WebViewClientClassicExt;->onProceededAfterSslError(Landroid/webkit/WebView;Landroid/net/http/SslError;)V

    #@209
    goto/16 :goto_11

    #@20b
    .line 444
    :pswitch_20b
    move-object/from16 v0, p0

    #@20d
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@20f
    if-eqz v4, :cond_11

    #@211
    move-object/from16 v0, p0

    #@213
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@215
    instance-of v4, v4, Landroid/webkit/WebViewClientClassicExt;

    #@217
    if-eqz v4, :cond_11

    #@219
    .line 445
    move-object/from16 v0, p1

    #@21b
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@21d
    move-object/from16 v50, v0

    #@21f
    check-cast v50, Ljava/util/HashMap;

    #@221
    .line 446
    .restart local v50       #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    move-object/from16 v0, p0

    #@223
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@225
    check-cast v4, Landroid/webkit/WebViewClientClassicExt;

    #@227
    move-object/from16 v0, p0

    #@229
    iget-object v10, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@22b
    invoke-virtual {v10}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@22e
    move-result-object v13

    #@22f
    const-string v10, "handler"

    #@231
    move-object/from16 v0, v50

    #@233
    invoke-virtual {v0, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@236
    move-result-object v10

    #@237
    check-cast v10, Landroid/webkit/ClientCertRequestHandler;

    #@239
    const-string v11, "host_and_port"

    #@23b
    move-object/from16 v0, v50

    #@23d
    invoke-virtual {v0, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@240
    move-result-object v11

    #@241
    check-cast v11, Ljava/lang/String;

    #@243
    invoke-virtual {v4, v13, v10, v11}, Landroid/webkit/WebViewClientClassicExt;->onReceivedClientCertRequest(Landroid/webkit/WebView;Landroid/webkit/ClientCertRequestHandler;Ljava/lang/String;)V

    #@246
    goto/16 :goto_11

    #@248
    .line 457
    .end local v50           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :pswitch_248
    monitor-enter p0

    #@249
    .line 458
    :try_start_249
    move-object/from16 v0, p0

    #@24b
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@24d
    if-eqz v4, :cond_262

    #@24f
    .line 459
    move-object/from16 v0, p0

    #@251
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@253
    move-object/from16 v0, p0

    #@255
    iget-object v10, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@257
    invoke-virtual {v10}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@25a
    move-result-object v10

    #@25b
    move-object/from16 v0, p0

    #@25d
    iget v11, v0, Landroid/webkit/CallbackProxy;->mLatestProgress:I

    #@25f
    invoke-virtual {v4, v10, v11}, Landroid/webkit/WebChromeClient;->onProgressChanged(Landroid/webkit/WebView;I)V

    #@262
    .line 462
    :cond_262
    const/4 v4, 0x0

    #@263
    move-object/from16 v0, p0

    #@265
    iput-boolean v4, v0, Landroid/webkit/CallbackProxy;->mProgressUpdatePending:Z

    #@267
    .line 463
    monitor-exit p0

    #@268
    goto/16 :goto_11

    #@26a
    :catchall_26a
    move-exception v4

    #@26b
    monitor-exit p0
    :try_end_26c
    .catchall {:try_start_249 .. :try_end_26c} :catchall_26a

    #@26c
    throw v4

    #@26d
    .line 467
    :pswitch_26d
    move-object/from16 v0, p0

    #@26f
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@271
    if-eqz v4, :cond_11

    #@273
    .line 468
    move-object/from16 v0, p0

    #@275
    iget-object v11, v0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@277
    move-object/from16 v0, p0

    #@279
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@27b
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@27e
    move-result-object v13

    #@27f
    move-object/from16 v0, p1

    #@281
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@283
    check-cast v4, Ljava/lang/String;

    #@285
    move-object/from16 v0, p1

    #@287
    iget v10, v0, Landroid/os/Message;->arg1:I

    #@289
    if-eqz v10, :cond_291

    #@28b
    const/4 v10, 0x1

    #@28c
    :goto_28c
    invoke-virtual {v11, v13, v4, v10}, Landroid/webkit/WebViewClient;->doUpdateVisitedHistory(Landroid/webkit/WebView;Ljava/lang/String;Z)V

    #@28f
    goto/16 :goto_11

    #@291
    :cond_291
    const/4 v10, 0x0

    #@292
    goto :goto_28c

    #@293
    .line 474
    :pswitch_293
    move-object/from16 v0, p0

    #@295
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@297
    if-eqz v4, :cond_11

    #@299
    .line 475
    move-object/from16 v0, p0

    #@29b
    iget-object v10, v0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@29d
    move-object/from16 v0, p0

    #@29f
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@2a1
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@2a4
    move-result-object v11

    #@2a5
    move-object/from16 v0, p1

    #@2a7
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2a9
    check-cast v4, Ljava/lang/String;

    #@2ab
    invoke-virtual {v10, v11, v4}, Landroid/webkit/WebViewClient;->onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V

    #@2ae
    goto/16 :goto_11

    #@2b0
    .line 481
    :pswitch_2b0
    move-object/from16 v0, p0

    #@2b2
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mDownloadListener:Landroid/webkit/DownloadListener;

    #@2b4
    if-nez v4, :cond_2bc

    #@2b6
    move-object/from16 v0, p0

    #@2b8
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mDownloadListenerExt:Landroid/webkit/DownloadListenerExt;

    #@2ba
    if-eqz v4, :cond_11

    #@2bc
    .line 482
    :cond_2bc
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@2bf
    move-result-object v4

    #@2c0
    const-string/jumbo v10, "url"

    #@2c3
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@2c6
    move-result-object v5

    #@2c7
    .line 483
    .local v5, url:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@2ca
    move-result-object v4

    #@2cb
    const-string/jumbo v10, "userAgent"

    #@2ce
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@2d1
    move-result-object v6

    #@2d2
    .line 484
    .local v6, userAgent:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@2d5
    move-result-object v4

    #@2d6
    const-string v10, "contentDisposition"

    #@2d8
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@2db
    move-result-object v7

    #@2dc
    .line 486
    .local v7, contentDisposition:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@2df
    move-result-object v4

    #@2e0
    const-string/jumbo v10, "mimetype"

    #@2e3
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@2e6
    move-result-object v8

    #@2e7
    .line 487
    .local v8, mimetype:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@2ea
    move-result-object v4

    #@2eb
    const-string/jumbo v10, "referer"

    #@2ee
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@2f1
    move-result-object v9

    #@2f2
    .line 488
    .local v9, referer:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@2f5
    move-result-object v4

    #@2f6
    const-string v10, "contentLength"

    #@2f8
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    #@2fb
    move-result-wide v10

    #@2fc
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@2ff
    move-result-object v36

    #@300
    .line 490
    .local v36, contentLength:Ljava/lang/Long;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@303
    move-result-object v4

    #@304
    const-string/jumbo v10, "suggestedName"

    #@307
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@30a
    move-result-object v12

    #@30b
    .line 493
    .local v12, suggestedName:Ljava/lang/String;
    move-object/from16 v0, p0

    #@30d
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mDownloadListener:Landroid/webkit/DownloadListener;

    #@30f
    if-eqz v4, :cond_339

    #@311
    .line 494
    move-object/from16 v0, p0

    #@313
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mDownloadListener:Landroid/webkit/DownloadListener;

    #@315
    instance-of v4, v4, Landroid/webkit/BrowserDownloadListener;

    #@317
    if-eqz v4, :cond_328

    #@319
    .line 495
    move-object/from16 v0, p0

    #@31b
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mDownloadListener:Landroid/webkit/DownloadListener;

    #@31d
    check-cast v4, Landroid/webkit/BrowserDownloadListener;

    #@31f
    invoke-virtual/range {v36 .. v36}, Ljava/lang/Long;->longValue()J

    #@322
    move-result-wide v10

    #@323
    invoke-virtual/range {v4 .. v11}, Landroid/webkit/BrowserDownloadListener;->onDownloadStart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    #@326
    goto/16 :goto_11

    #@328
    .line 498
    :cond_328
    move-object/from16 v0, p0

    #@32a
    iget-object v10, v0, Landroid/webkit/CallbackProxy;->mDownloadListener:Landroid/webkit/DownloadListener;

    #@32c
    invoke-virtual/range {v36 .. v36}, Ljava/lang/Long;->longValue()J

    #@32f
    move-result-wide v15

    #@330
    move-object v11, v5

    #@331
    move-object v12, v6

    #@332
    move-object v13, v7

    #@333
    move-object v14, v8

    #@334
    invoke-interface/range {v10 .. v16}, Landroid/webkit/DownloadListener;->onDownloadStart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    #@337
    goto/16 :goto_11

    #@339
    .line 501
    :cond_339
    move-object/from16 v0, p0

    #@33b
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mDownloadListenerExt:Landroid/webkit/DownloadListenerExt;

    #@33d
    if-eqz v4, :cond_11

    #@33f
    .line 502
    move-object/from16 v0, p0

    #@341
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mDownloadListenerExt:Landroid/webkit/DownloadListenerExt;

    #@343
    instance-of v4, v4, Landroid/webkit/BrowserDownloadListenerExt;

    #@345
    if-eqz v4, :cond_356

    #@347
    .line 503
    move-object/from16 v0, p0

    #@349
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mDownloadListenerExt:Landroid/webkit/DownloadListenerExt;

    #@34b
    check-cast v4, Landroid/webkit/BrowserDownloadListenerExt;

    #@34d
    invoke-virtual/range {v36 .. v36}, Ljava/lang/Long;->longValue()J

    #@350
    move-result-wide v10

    #@351
    invoke-virtual/range {v4 .. v12}, Landroid/webkit/BrowserDownloadListenerExt;->onDownloadStart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    #@354
    goto/16 :goto_11

    #@356
    .line 506
    :cond_356
    move-object/from16 v0, p0

    #@358
    iget-object v13, v0, Landroid/webkit/CallbackProxy;->mDownloadListenerExt:Landroid/webkit/DownloadListenerExt;

    #@35a
    invoke-virtual/range {v36 .. v36}, Ljava/lang/Long;->longValue()J

    #@35d
    move-result-wide v18

    #@35e
    move-object v14, v5

    #@35f
    move-object v15, v6

    #@360
    move-object/from16 v16, v7

    #@362
    move-object/from16 v17, v8

    #@364
    move-object/from16 v20, v12

    #@366
    invoke-interface/range {v13 .. v20}, Landroid/webkit/DownloadListenerExt;->onDownloadStart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    #@369
    goto/16 :goto_11

    #@36b
    .line 515
    .end local v5           #url:Ljava/lang/String;
    .end local v6           #userAgent:Ljava/lang/String;
    .end local v7           #contentDisposition:Ljava/lang/String;
    .end local v8           #mimetype:Ljava/lang/String;
    .end local v9           #referer:Ljava/lang/String;
    .end local v12           #suggestedName:Ljava/lang/String;
    .end local v36           #contentLength:Ljava/lang/Long;
    :pswitch_36b
    move-object/from16 v0, p0

    #@36d
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@36f
    if-eqz v4, :cond_11

    #@371
    .line 516
    move-object/from16 v0, p0

    #@373
    iget-object v13, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@375
    move-object/from16 v0, p0

    #@377
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@379
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@37c
    move-result-object v14

    #@37d
    move-object/from16 v0, p1

    #@37f
    iget v4, v0, Landroid/os/Message;->arg1:I

    #@381
    const/4 v10, 0x1

    #@382
    if-ne v4, v10, :cond_3a9

    #@384
    const/4 v4, 0x1

    #@385
    move v10, v4

    #@386
    :goto_386
    move-object/from16 v0, p1

    #@388
    iget v4, v0, Landroid/os/Message;->arg2:I

    #@38a
    const/4 v11, 0x1

    #@38b
    if-ne v4, v11, :cond_3ac

    #@38d
    const/4 v4, 0x1

    #@38e
    move v11, v4

    #@38f
    :goto_38f
    move-object/from16 v0, p1

    #@391
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@393
    check-cast v4, Landroid/os/Message;

    #@395
    invoke-virtual {v13, v14, v10, v11, v4}, Landroid/webkit/WebChromeClient;->onCreateWindow(Landroid/webkit/WebView;ZZLandroid/os/Message;)Z

    #@398
    move-result v4

    #@399
    if-nez v4, :cond_3a0

    #@39b
    .line 519
    monitor-enter p0

    #@39c
    .line 520
    :try_start_39c
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->notify()V

    #@39f
    .line 521
    monitor-exit p0
    :try_end_3a0
    .catchall {:try_start_39c .. :try_end_3a0} :catchall_3af

    #@3a0
    .line 523
    :cond_3a0
    move-object/from16 v0, p0

    #@3a2
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@3a4
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->dismissZoomControl()V

    #@3a7
    goto/16 :goto_11

    #@3a9
    .line 516
    :cond_3a9
    const/4 v4, 0x0

    #@3aa
    move v10, v4

    #@3ab
    goto :goto_386

    #@3ac
    :cond_3ac
    const/4 v4, 0x0

    #@3ad
    move v11, v4

    #@3ae
    goto :goto_38f

    #@3af
    .line 521
    :catchall_3af
    move-exception v4

    #@3b0
    :try_start_3b0
    monitor-exit p0
    :try_end_3b1
    .catchall {:try_start_3b0 .. :try_end_3b1} :catchall_3af

    #@3b1
    throw v4

    #@3b2
    .line 528
    :pswitch_3b2
    move-object/from16 v0, p0

    #@3b4
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@3b6
    if-eqz v4, :cond_11

    #@3b8
    .line 529
    move-object/from16 v0, p0

    #@3ba
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@3bc
    move-object/from16 v0, p0

    #@3be
    iget-object v10, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@3c0
    invoke-virtual {v10}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@3c3
    move-result-object v10

    #@3c4
    invoke-virtual {v4, v10}, Landroid/webkit/WebChromeClient;->onRequestFocus(Landroid/webkit/WebView;)V

    #@3c7
    goto/16 :goto_11

    #@3c9
    .line 534
    :pswitch_3c9
    move-object/from16 v0, p0

    #@3cb
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@3cd
    if-eqz v4, :cond_11

    #@3cf
    .line 535
    move-object/from16 v0, p0

    #@3d1
    iget-object v10, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@3d3
    move-object/from16 v0, p1

    #@3d5
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3d7
    check-cast v4, Landroid/webkit/WebViewClassic;

    #@3d9
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@3dc
    move-result-object v4

    #@3dd
    invoke-virtual {v10, v4}, Landroid/webkit/WebChromeClient;->onCloseWindow(Landroid/webkit/WebView;)V

    #@3e0
    goto/16 :goto_11

    #@3e2
    .line 540
    :pswitch_3e2
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@3e5
    move-result-object v34

    #@3e6
    .line 541
    .local v34, bundle:Landroid/os/Bundle;
    const-string v4, "host"

    #@3e8
    move-object/from16 v0, v34

    #@3ea
    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@3ed
    move-result-object v63

    #@3ee
    .line 542
    .local v63, schemePlusHost:Ljava/lang/String;
    const-string/jumbo v4, "username"

    #@3f1
    move-object/from16 v0, v34

    #@3f3
    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@3f6
    move-result-object v66

    #@3f7
    .line 543
    .local v66, username:Ljava/lang/String;
    const-string/jumbo v4, "password"

    #@3fa
    move-object/from16 v0, v34

    #@3fc
    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@3ff
    move-result-object v57

    #@400
    .line 546
    .local v57, password:Ljava/lang/String;
    move-object/from16 v0, p0

    #@402
    iget-object v10, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@404
    move-object/from16 v0, p1

    #@406
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@408
    check-cast v4, Landroid/os/Message;

    #@40a
    move-object/from16 v0, v63

    #@40c
    move-object/from16 v1, v66

    #@40e
    move-object/from16 v2, v57

    #@410
    invoke-virtual {v10, v0, v1, v2, v4}, Landroid/webkit/WebViewClassic;->onSavePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)Z

    #@413
    move-result v4

    #@414
    if-nez v4, :cond_11

    #@416
    .line 548
    monitor-enter p0

    #@417
    .line 549
    :try_start_417
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->notify()V

    #@41a
    .line 550
    monitor-exit p0

    #@41b
    goto/16 :goto_11

    #@41d
    :catchall_41d
    move-exception v4

    #@41e
    monitor-exit p0
    :try_end_41f
    .catchall {:try_start_417 .. :try_end_41f} :catchall_41d

    #@41f
    throw v4

    #@420
    .line 555
    .end local v34           #bundle:Landroid/os/Bundle;
    .end local v57           #password:Ljava/lang/String;
    .end local v63           #schemePlusHost:Ljava/lang/String;
    .end local v66           #username:Ljava/lang/String;
    :pswitch_420
    move-object/from16 v0, p0

    #@422
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@424
    if-eqz v4, :cond_11

    #@426
    .line 556
    move-object/from16 v0, p0

    #@428
    iget-object v10, v0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@42a
    move-object/from16 v0, p0

    #@42c
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@42e
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@431
    move-result-object v11

    #@432
    move-object/from16 v0, p1

    #@434
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@436
    check-cast v4, Landroid/view/KeyEvent;

    #@438
    invoke-virtual {v10, v11, v4}, Landroid/webkit/WebViewClient;->onUnhandledKeyEvent(Landroid/webkit/WebView;Landroid/view/KeyEvent;)V

    #@43b
    goto/16 :goto_11

    #@43d
    .line 562
    :pswitch_43d
    move-object/from16 v0, p0

    #@43f
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@441
    if-eqz v4, :cond_11

    #@443
    .line 563
    move-object/from16 v0, p1

    #@445
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@447
    move-object/from16 v50, v0

    #@449
    check-cast v50, Ljava/util/HashMap;

    #@44b
    .line 565
    .restart local v50       #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v4, "databaseIdentifier"

    #@44d
    move-object/from16 v0, v50

    #@44f
    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@452
    move-result-object v15

    #@453
    check-cast v15, Ljava/lang/String;

    #@455
    .line 567
    .local v15, databaseIdentifier:Ljava/lang/String;
    const-string/jumbo v4, "url"

    #@458
    move-object/from16 v0, v50

    #@45a
    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@45d
    move-result-object v5

    #@45e
    check-cast v5, Ljava/lang/String;

    #@460
    .line 568
    .restart local v5       #url:Ljava/lang/String;
    const-string/jumbo v4, "quota"

    #@463
    move-object/from16 v0, v50

    #@465
    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@468
    move-result-object v4

    #@469
    check-cast v4, Ljava/lang/Long;

    #@46b
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    #@46e
    move-result-wide v16

    #@46f
    .line 570
    .local v16, quota:J
    const-string/jumbo v4, "totalQuota"

    #@472
    move-object/from16 v0, v50

    #@474
    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@477
    move-result-object v4

    #@478
    check-cast v4, Ljava/lang/Long;

    #@47a
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    #@47d
    move-result-wide v20

    #@47e
    .line 572
    .local v20, totalQuota:J
    const-string v4, "estimatedDatabaseSize"

    #@480
    move-object/from16 v0, v50

    #@482
    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@485
    move-result-object v4

    #@486
    check-cast v4, Ljava/lang/Long;

    #@488
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    #@48b
    move-result-wide v18

    #@48c
    .line 574
    .local v18, estimatedDatabaseSize:J
    const-string/jumbo v4, "quotaUpdater"

    #@48f
    move-object/from16 v0, v50

    #@491
    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@494
    move-result-object v22

    #@495
    check-cast v22, Landroid/webkit/WebStorage$QuotaUpdater;

    #@497
    .line 577
    .local v22, quotaUpdater:Landroid/webkit/WebStorage$QuotaUpdater;
    move-object/from16 v0, p0

    #@499
    iget-object v13, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@49b
    move-object v14, v5

    #@49c
    invoke-virtual/range {v13 .. v22}, Landroid/webkit/WebChromeClient;->onExceededDatabaseQuota(Ljava/lang/String;Ljava/lang/String;JJJLandroid/webkit/WebStorage$QuotaUpdater;)V

    #@49f
    goto/16 :goto_11

    #@4a1
    .line 584
    .end local v5           #url:Ljava/lang/String;
    .end local v15           #databaseIdentifier:Ljava/lang/String;
    .end local v16           #quota:J
    .end local v18           #estimatedDatabaseSize:J
    .end local v20           #totalQuota:J
    .end local v22           #quotaUpdater:Landroid/webkit/WebStorage$QuotaUpdater;
    .end local v50           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :pswitch_4a1
    move-object/from16 v0, p0

    #@4a3
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@4a5
    if-eqz v4, :cond_11

    #@4a7
    .line 585
    move-object/from16 v0, p1

    #@4a9
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4ab
    move-object/from16 v50, v0

    #@4ad
    check-cast v50, Ljava/util/HashMap;

    #@4af
    .line 587
    .restart local v50       #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string/jumbo v4, "requiredStorage"

    #@4b2
    move-object/from16 v0, v50

    #@4b4
    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@4b7
    move-result-object v4

    #@4b8
    check-cast v4, Ljava/lang/Long;

    #@4ba
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    #@4bd
    move-result-wide v24

    #@4be
    .line 589
    .local v24, requiredStorage:J
    const-string/jumbo v4, "quota"

    #@4c1
    move-object/from16 v0, v50

    #@4c3
    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@4c6
    move-result-object v4

    #@4c7
    check-cast v4, Ljava/lang/Long;

    #@4c9
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    #@4cc
    move-result-wide v16

    #@4cd
    .line 591
    .restart local v16       #quota:J
    const-string/jumbo v4, "quotaUpdater"

    #@4d0
    move-object/from16 v0, v50

    #@4d2
    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@4d5
    move-result-object v22

    #@4d6
    check-cast v22, Landroid/webkit/WebStorage$QuotaUpdater;

    #@4d8
    .line 594
    .restart local v22       #quotaUpdater:Landroid/webkit/WebStorage$QuotaUpdater;
    move-object/from16 v0, p0

    #@4da
    iget-object v0, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@4dc
    move-object/from16 v23, v0

    #@4de
    move-wide/from16 v26, v16

    #@4e0
    move-object/from16 v28, v22

    #@4e2
    invoke-virtual/range {v23 .. v28}, Landroid/webkit/WebChromeClient;->onReachedMaxAppCacheSize(JJLandroid/webkit/WebStorage$QuotaUpdater;)V

    #@4e5
    goto/16 :goto_11

    #@4e7
    .line 600
    .end local v16           #quota:J
    .end local v22           #quotaUpdater:Landroid/webkit/WebStorage$QuotaUpdater;
    .end local v24           #requiredStorage:J
    .end local v50           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :pswitch_4e7
    move-object/from16 v0, p0

    #@4e9
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@4eb
    if-eqz v4, :cond_11

    #@4ed
    .line 601
    move-object/from16 v0, p1

    #@4ef
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4f1
    move-object/from16 v50, v0

    #@4f3
    check-cast v50, Ljava/util/HashMap;

    #@4f5
    .line 603
    .restart local v50       #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string/jumbo v4, "origin"

    #@4f8
    move-object/from16 v0, v50

    #@4fa
    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@4fd
    move-result-object v54

    #@4fe
    check-cast v54, Ljava/lang/String;

    #@500
    .line 604
    .local v54, origin:Ljava/lang/String;
    const-string v4, "callback"

    #@502
    move-object/from16 v0, v50

    #@504
    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@507
    move-result-object v35

    #@508
    check-cast v35, Landroid/webkit/GeolocationPermissions$Callback;

    #@50a
    .line 607
    .local v35, callback:Landroid/webkit/GeolocationPermissions$Callback;
    move-object/from16 v0, p0

    #@50c
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@50e
    move-object/from16 v0, v54

    #@510
    move-object/from16 v1, v35

    #@512
    invoke-virtual {v4, v0, v1}, Landroid/webkit/WebChromeClient;->onGeolocationPermissionsShowPrompt(Ljava/lang/String;Landroid/webkit/GeolocationPermissions$Callback;)V

    #@515
    goto/16 :goto_11

    #@517
    .line 613
    .end local v35           #callback:Landroid/webkit/GeolocationPermissions$Callback;
    .end local v50           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v54           #origin:Ljava/lang/String;
    :pswitch_517
    move-object/from16 v0, p0

    #@519
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@51b
    if-eqz v4, :cond_11

    #@51d
    .line 614
    move-object/from16 v0, p0

    #@51f
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@521
    invoke-virtual {v4}, Landroid/webkit/WebChromeClient;->onGeolocationPermissionsHidePrompt()V

    #@524
    goto/16 :goto_11

    #@526
    .line 619
    :pswitch_526
    move-object/from16 v0, p0

    #@528
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@52a
    if-eqz v4, :cond_11

    #@52c
    .line 620
    move-object/from16 v0, p1

    #@52e
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@530
    move-object/from16 v60, v0

    #@532
    check-cast v60, Landroid/webkit/CallbackProxy$JsResultReceiver;

    #@534
    .line 621
    .local v60, receiver:Landroid/webkit/CallbackProxy$JsResultReceiver;
    move-object/from16 v0, v60

    #@536
    iget-object v0, v0, Landroid/webkit/CallbackProxy$JsResultReceiver;->mJsResult:Landroid/webkit/JsPromptResult;

    #@538
    move-object/from16 v31, v0

    #@53a
    .line 622
    .local v31, res:Landroid/webkit/JsResult;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@53d
    move-result-object v4

    #@53e
    const-string/jumbo v10, "message"

    #@541
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@544
    move-result-object v29

    #@545
    .line 623
    .local v29, message:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@548
    move-result-object v4

    #@549
    const-string/jumbo v10, "url"

    #@54c
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@54f
    move-result-object v5

    #@550
    .line 624
    .restart local v5       #url:Ljava/lang/String;
    move-object/from16 v0, p0

    #@552
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@554
    move-object/from16 v0, p0

    #@556
    iget-object v10, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@558
    invoke-virtual {v10}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@55b
    move-result-object v10

    #@55c
    move-object/from16 v0, v29

    #@55e
    move-object/from16 v1, v31

    #@560
    invoke-virtual {v4, v10, v5, v0, v1}, Landroid/webkit/WebChromeClient;->onJsAlert(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsResult;)Z

    #@563
    move-result v4

    #@564
    if-nez v4, :cond_5bc

    #@566
    .line 626
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/CallbackProxy;->canShowAlertDialog()Z

    #@569
    move-result v4

    #@56a
    if-nez v4, :cond_574

    #@56c
    .line 627
    invoke-virtual/range {v31 .. v31}, Landroid/webkit/JsPromptResult;->cancel()V

    #@56f
    .line 628
    invoke-virtual/range {v60 .. v60}, Landroid/webkit/CallbackProxy$JsResultReceiver;->setReady()V

    #@572
    goto/16 :goto_11

    #@574
    .line 632
    :cond_574
    new-instance v4, Landroid/app/AlertDialog$Builder;

    #@576
    move-object/from16 v0, p0

    #@578
    iget-object v10, v0, Landroid/webkit/CallbackProxy;->mContext:Landroid/content/Context;

    #@57a
    invoke-direct {v4, v10}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@57d
    move-object/from16 v0, p0

    #@57f
    invoke-direct {v0, v5}, Landroid/webkit/CallbackProxy;->getJsDialogTitle(Ljava/lang/String;)Ljava/lang/String;

    #@582
    move-result-object v10

    #@583
    invoke-virtual {v4, v10}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@586
    move-result-object v4

    #@587
    move-object/from16 v0, v29

    #@589
    invoke-virtual {v4, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@58c
    move-result-object v4

    #@58d
    const v10, 0x104000a

    #@590
    new-instance v11, Landroid/webkit/CallbackProxy$2;

    #@592
    move-object/from16 v0, p0

    #@594
    move-object/from16 v1, v31

    #@596
    invoke-direct {v11, v0, v1}, Landroid/webkit/CallbackProxy$2;-><init>(Landroid/webkit/CallbackProxy;Landroid/webkit/JsResult;)V

    #@599
    invoke-virtual {v4, v10, v11}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@59c
    move-result-object v4

    #@59d
    new-instance v10, Landroid/webkit/CallbackProxy$1;

    #@59f
    move-object/from16 v0, p0

    #@5a1
    move-object/from16 v1, v31

    #@5a3
    invoke-direct {v10, v0, v1}, Landroid/webkit/CallbackProxy$1;-><init>(Landroid/webkit/CallbackProxy;Landroid/webkit/JsResult;)V

    #@5a6
    invoke-virtual {v4, v10}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    #@5a9
    move-result-object v4

    #@5aa
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    #@5ad
    move-result-object v39

    #@5ae
    .line 656
    .local v39, dialog:Landroid/app/AlertDialog;
    new-instance v4, Landroid/webkit/CallbackProxy$3;

    #@5b0
    move-object/from16 v0, p0

    #@5b2
    move-object/from16 v1, v31

    #@5b4
    invoke-direct {v4, v0, v1}, Landroid/webkit/CallbackProxy$3;-><init>(Landroid/webkit/CallbackProxy;Landroid/webkit/JsResult;)V

    #@5b7
    move-object/from16 v0, v39

    #@5b9
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    #@5bc
    .line 666
    .end local v39           #dialog:Landroid/app/AlertDialog;
    :cond_5bc
    invoke-virtual/range {v60 .. v60}, Landroid/webkit/CallbackProxy$JsResultReceiver;->setReady()V

    #@5bf
    goto/16 :goto_11

    #@5c1
    .line 671
    .end local v5           #url:Ljava/lang/String;
    .end local v29           #message:Ljava/lang/String;
    .end local v31           #res:Landroid/webkit/JsResult;
    .end local v60           #receiver:Landroid/webkit/CallbackProxy$JsResultReceiver;
    :pswitch_5c1
    move-object/from16 v0, p0

    #@5c3
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@5c5
    if-eqz v4, :cond_11

    #@5c7
    .line 672
    move-object/from16 v0, p1

    #@5c9
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@5cb
    move-object/from16 v60, v0

    #@5cd
    check-cast v60, Landroid/webkit/CallbackProxy$JsResultReceiver;

    #@5cf
    .line 673
    .restart local v60       #receiver:Landroid/webkit/CallbackProxy$JsResultReceiver;
    move-object/from16 v0, v60

    #@5d1
    iget-object v0, v0, Landroid/webkit/CallbackProxy$JsResultReceiver;->mJsResult:Landroid/webkit/JsPromptResult;

    #@5d3
    move-object/from16 v31, v0

    #@5d5
    .line 674
    .restart local v31       #res:Landroid/webkit/JsResult;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@5d8
    move-result-object v4

    #@5d9
    const-string/jumbo v10, "message"

    #@5dc
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@5df
    move-result-object v29

    #@5e0
    .line 675
    .restart local v29       #message:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@5e3
    move-result-object v4

    #@5e4
    const-string/jumbo v10, "url"

    #@5e7
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@5ea
    move-result-object v5

    #@5eb
    .line 676
    .restart local v5       #url:Ljava/lang/String;
    move-object/from16 v0, p0

    #@5ed
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@5ef
    move-object/from16 v0, p0

    #@5f1
    iget-object v10, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@5f3
    invoke-virtual {v10}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@5f6
    move-result-object v10

    #@5f7
    move-object/from16 v0, v29

    #@5f9
    move-object/from16 v1, v31

    #@5fb
    invoke-virtual {v4, v10, v5, v0, v1}, Landroid/webkit/WebChromeClient;->onJsConfirm(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsResult;)Z

    #@5fe
    move-result v4

    #@5ff
    if-nez v4, :cond_666

    #@601
    .line 678
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/CallbackProxy;->canShowAlertDialog()Z

    #@604
    move-result v4

    #@605
    if-nez v4, :cond_60f

    #@607
    .line 679
    invoke-virtual/range {v31 .. v31}, Landroid/webkit/JsPromptResult;->cancel()V

    #@60a
    .line 680
    invoke-virtual/range {v60 .. v60}, Landroid/webkit/CallbackProxy$JsResultReceiver;->setReady()V

    #@60d
    goto/16 :goto_11

    #@60f
    .line 684
    :cond_60f
    new-instance v4, Landroid/app/AlertDialog$Builder;

    #@611
    move-object/from16 v0, p0

    #@613
    iget-object v10, v0, Landroid/webkit/CallbackProxy;->mContext:Landroid/content/Context;

    #@615
    invoke-direct {v4, v10}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@618
    move-object/from16 v0, p0

    #@61a
    invoke-direct {v0, v5}, Landroid/webkit/CallbackProxy;->getJsDialogTitle(Ljava/lang/String;)Ljava/lang/String;

    #@61d
    move-result-object v10

    #@61e
    invoke-virtual {v4, v10}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@621
    move-result-object v4

    #@622
    move-object/from16 v0, v29

    #@624
    invoke-virtual {v4, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@627
    move-result-object v4

    #@628
    const v10, 0x104000a

    #@62b
    new-instance v11, Landroid/webkit/CallbackProxy$6;

    #@62d
    move-object/from16 v0, p0

    #@62f
    move-object/from16 v1, v31

    #@631
    invoke-direct {v11, v0, v1}, Landroid/webkit/CallbackProxy$6;-><init>(Landroid/webkit/CallbackProxy;Landroid/webkit/JsResult;)V

    #@634
    invoke-virtual {v4, v10, v11}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@637
    move-result-object v4

    #@638
    const/high16 v10, 0x104

    #@63a
    new-instance v11, Landroid/webkit/CallbackProxy$5;

    #@63c
    move-object/from16 v0, p0

    #@63e
    move-object/from16 v1, v31

    #@640
    invoke-direct {v11, v0, v1}, Landroid/webkit/CallbackProxy$5;-><init>(Landroid/webkit/CallbackProxy;Landroid/webkit/JsResult;)V

    #@643
    invoke-virtual {v4, v10, v11}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@646
    move-result-object v4

    #@647
    new-instance v10, Landroid/webkit/CallbackProxy$4;

    #@649
    move-object/from16 v0, p0

    #@64b
    move-object/from16 v1, v31

    #@64d
    invoke-direct {v10, v0, v1}, Landroid/webkit/CallbackProxy$4;-><init>(Landroid/webkit/CallbackProxy;Landroid/webkit/JsResult;)V

    #@650
    invoke-virtual {v4, v10}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    #@653
    move-result-object v4

    #@654
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    #@657
    move-result-object v39

    #@658
    .line 715
    .restart local v39       #dialog:Landroid/app/AlertDialog;
    new-instance v4, Landroid/webkit/CallbackProxy$7;

    #@65a
    move-object/from16 v0, p0

    #@65c
    move-object/from16 v1, v31

    #@65e
    invoke-direct {v4, v0, v1}, Landroid/webkit/CallbackProxy$7;-><init>(Landroid/webkit/CallbackProxy;Landroid/webkit/JsResult;)V

    #@661
    move-object/from16 v0, v39

    #@663
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    #@666
    .line 727
    .end local v39           #dialog:Landroid/app/AlertDialog;
    :cond_666
    invoke-virtual/range {v60 .. v60}, Landroid/webkit/CallbackProxy$JsResultReceiver;->setReady()V

    #@669
    goto/16 :goto_11

    #@66b
    .line 732
    .end local v5           #url:Ljava/lang/String;
    .end local v29           #message:Ljava/lang/String;
    .end local v31           #res:Landroid/webkit/JsResult;
    .end local v60           #receiver:Landroid/webkit/CallbackProxy$JsResultReceiver;
    :pswitch_66b
    move-object/from16 v0, p0

    #@66d
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@66f
    if-eqz v4, :cond_11

    #@671
    .line 733
    move-object/from16 v0, p1

    #@673
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@675
    move-object/from16 v60, v0

    #@677
    check-cast v60, Landroid/webkit/CallbackProxy$JsResultReceiver;

    #@679
    .line 734
    .restart local v60       #receiver:Landroid/webkit/CallbackProxy$JsResultReceiver;
    move-object/from16 v0, v60

    #@67b
    iget-object v0, v0, Landroid/webkit/CallbackProxy$JsResultReceiver;->mJsResult:Landroid/webkit/JsPromptResult;

    #@67d
    move-object/from16 v31, v0

    #@67f
    .line 735
    .local v31, res:Landroid/webkit/JsPromptResult;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@682
    move-result-object v4

    #@683
    const-string/jumbo v10, "message"

    #@686
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@689
    move-result-object v29

    #@68a
    .line 736
    .restart local v29       #message:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@68d
    move-result-object v4

    #@68e
    const-string v10, "default"

    #@690
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@693
    move-result-object v30

    #@694
    .line 737
    .local v30, defaultVal:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@697
    move-result-object v4

    #@698
    const-string/jumbo v10, "url"

    #@69b
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@69e
    move-result-object v5

    #@69f
    .line 738
    .restart local v5       #url:Ljava/lang/String;
    move-object/from16 v0, p0

    #@6a1
    iget-object v0, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@6a3
    move-object/from16 v26, v0

    #@6a5
    move-object/from16 v0, p0

    #@6a7
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@6a9
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@6ac
    move-result-object v27

    #@6ad
    move-object/from16 v28, v5

    #@6af
    invoke-virtual/range {v26 .. v31}, Landroid/webkit/WebChromeClient;->onJsPrompt(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsPromptResult;)Z

    #@6b2
    move-result v4

    #@6b3
    if-nez v4, :cond_750

    #@6b5
    .line 740
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/CallbackProxy;->canShowAlertDialog()Z

    #@6b8
    move-result v4

    #@6b9
    if-nez v4, :cond_6c3

    #@6bb
    .line 741
    invoke-virtual/range {v31 .. v31}, Landroid/webkit/JsPromptResult;->cancel()V

    #@6be
    .line 742
    invoke-virtual/range {v60 .. v60}, Landroid/webkit/CallbackProxy$JsResultReceiver;->setReady()V

    #@6c1
    goto/16 :goto_11

    #@6c3
    .line 745
    :cond_6c3
    move-object/from16 v0, p0

    #@6c5
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mContext:Landroid/content/Context;

    #@6c7
    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@6ca
    move-result-object v41

    #@6cb
    .line 747
    .local v41, factory:Landroid/view/LayoutInflater;
    const v4, 0x109004d

    #@6ce
    const/4 v10, 0x0

    #@6cf
    move-object/from16 v0, v41

    #@6d1
    invoke-virtual {v0, v4, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@6d4
    move-result-object v68

    #@6d5
    .line 749
    .local v68, view:Landroid/view/View;
    const v4, 0x10202ae

    #@6d8
    move-object/from16 v0, v68

    #@6da
    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@6dd
    move-result-object v67

    #@6de
    check-cast v67, Landroid/widget/EditText;

    #@6e0
    .line 751
    .local v67, v:Landroid/widget/EditText;
    move-object/from16 v0, v67

    #@6e2
    move-object/from16 v1, v30

    #@6e4
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    #@6e7
    .line 752
    const v4, 0x102000b

    #@6ea
    move-object/from16 v0, v68

    #@6ec
    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@6ef
    move-result-object v4

    #@6f0
    check-cast v4, Landroid/widget/TextView;

    #@6f2
    move-object/from16 v0, v29

    #@6f4
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@6f7
    .line 755
    new-instance v4, Landroid/app/AlertDialog$Builder;

    #@6f9
    move-object/from16 v0, p0

    #@6fb
    iget-object v10, v0, Landroid/webkit/CallbackProxy;->mContext:Landroid/content/Context;

    #@6fd
    invoke-direct {v4, v10}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@700
    move-object/from16 v0, p0

    #@702
    invoke-direct {v0, v5}, Landroid/webkit/CallbackProxy;->getJsDialogTitle(Ljava/lang/String;)Ljava/lang/String;

    #@705
    move-result-object v10

    #@706
    invoke-virtual {v4, v10}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@709
    move-result-object v4

    #@70a
    move-object/from16 v0, v68

    #@70c
    invoke-virtual {v4, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    #@70f
    move-result-object v4

    #@710
    const v10, 0x104000a

    #@713
    new-instance v11, Landroid/webkit/CallbackProxy$10;

    #@715
    move-object/from16 v0, p0

    #@717
    move-object/from16 v1, v31

    #@719
    move-object/from16 v2, v67

    #@71b
    invoke-direct {v11, v0, v1, v2}, Landroid/webkit/CallbackProxy$10;-><init>(Landroid/webkit/CallbackProxy;Landroid/webkit/JsPromptResult;Landroid/widget/EditText;)V

    #@71e
    invoke-virtual {v4, v10, v11}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@721
    move-result-object v4

    #@722
    const/high16 v10, 0x104

    #@724
    new-instance v11, Landroid/webkit/CallbackProxy$9;

    #@726
    move-object/from16 v0, p0

    #@728
    move-object/from16 v1, v31

    #@72a
    invoke-direct {v11, v0, v1}, Landroid/webkit/CallbackProxy$9;-><init>(Landroid/webkit/CallbackProxy;Landroid/webkit/JsPromptResult;)V

    #@72d
    invoke-virtual {v4, v10, v11}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@730
    move-result-object v4

    #@731
    new-instance v10, Landroid/webkit/CallbackProxy$8;

    #@733
    move-object/from16 v0, p0

    #@735
    move-object/from16 v1, v31

    #@737
    invoke-direct {v10, v0, v1}, Landroid/webkit/CallbackProxy$8;-><init>(Landroid/webkit/CallbackProxy;Landroid/webkit/JsPromptResult;)V

    #@73a
    invoke-virtual {v4, v10}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    #@73d
    move-result-object v4

    #@73e
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    #@741
    move-result-object v39

    #@742
    .line 789
    .restart local v39       #dialog:Landroid/app/AlertDialog;
    new-instance v4, Landroid/webkit/CallbackProxy$11;

    #@744
    move-object/from16 v0, p0

    #@746
    move-object/from16 v1, v31

    #@748
    invoke-direct {v4, v0, v1}, Landroid/webkit/CallbackProxy$11;-><init>(Landroid/webkit/CallbackProxy;Landroid/webkit/JsPromptResult;)V

    #@74b
    move-object/from16 v0, v39

    #@74d
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    #@750
    .line 801
    .end local v39           #dialog:Landroid/app/AlertDialog;
    .end local v41           #factory:Landroid/view/LayoutInflater;
    .end local v67           #v:Landroid/widget/EditText;
    .end local v68           #view:Landroid/view/View;
    :cond_750
    invoke-virtual/range {v60 .. v60}, Landroid/webkit/CallbackProxy$JsResultReceiver;->setReady()V

    #@753
    goto/16 :goto_11

    #@755
    .line 806
    .end local v5           #url:Ljava/lang/String;
    .end local v29           #message:Ljava/lang/String;
    .end local v30           #defaultVal:Ljava/lang/String;
    .end local v31           #res:Landroid/webkit/JsPromptResult;
    .end local v60           #receiver:Landroid/webkit/CallbackProxy$JsResultReceiver;
    :pswitch_755
    move-object/from16 v0, p0

    #@757
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@759
    if-eqz v4, :cond_11

    #@75b
    .line 807
    move-object/from16 v0, p1

    #@75d
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@75f
    move-object/from16 v60, v0

    #@761
    check-cast v60, Landroid/webkit/CallbackProxy$JsResultReceiver;

    #@763
    .line 808
    .restart local v60       #receiver:Landroid/webkit/CallbackProxy$JsResultReceiver;
    move-object/from16 v0, v60

    #@765
    iget-object v0, v0, Landroid/webkit/CallbackProxy$JsResultReceiver;->mJsResult:Landroid/webkit/JsPromptResult;

    #@767
    move-object/from16 v31, v0

    #@769
    .line 809
    .local v31, res:Landroid/webkit/JsResult;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@76c
    move-result-object v4

    #@76d
    const-string/jumbo v10, "message"

    #@770
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@773
    move-result-object v29

    #@774
    .line 810
    .restart local v29       #message:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@777
    move-result-object v4

    #@778
    const-string/jumbo v10, "url"

    #@77b
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@77e
    move-result-object v5

    #@77f
    .line 811
    .restart local v5       #url:Ljava/lang/String;
    move-object/from16 v0, p0

    #@781
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@783
    move-object/from16 v0, p0

    #@785
    iget-object v10, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@787
    invoke-virtual {v10}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@78a
    move-result-object v10

    #@78b
    move-object/from16 v0, v29

    #@78d
    move-object/from16 v1, v31

    #@78f
    invoke-virtual {v4, v10, v5, v0, v1}, Landroid/webkit/WebChromeClient;->onJsBeforeUnload(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsResult;)Z

    #@792
    move-result v4

    #@793
    if-nez v4, :cond_801

    #@795
    .line 813
    invoke-virtual/range {p0 .. p0}, Landroid/webkit/CallbackProxy;->canShowAlertDialog()Z

    #@798
    move-result v4

    #@799
    if-nez v4, :cond_7a3

    #@79b
    .line 814
    invoke-virtual/range {v31 .. v31}, Landroid/webkit/JsPromptResult;->cancel()V

    #@79e
    .line 815
    invoke-virtual/range {v60 .. v60}, Landroid/webkit/CallbackProxy$JsResultReceiver;->setReady()V

    #@7a1
    goto/16 :goto_11

    #@7a3
    .line 818
    :cond_7a3
    move-object/from16 v0, p0

    #@7a5
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mContext:Landroid/content/Context;

    #@7a7
    const v10, 0x104036b

    #@7aa
    const/4 v11, 0x1

    #@7ab
    new-array v11, v11, [Ljava/lang/Object;

    #@7ad
    const/4 v13, 0x0

    #@7ae
    aput-object v29, v11, v13

    #@7b0
    invoke-virtual {v4, v10, v11}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@7b3
    move-result-object v49

    #@7b4
    .line 821
    .local v49, m:Ljava/lang/String;
    new-instance v4, Landroid/app/AlertDialog$Builder;

    #@7b6
    move-object/from16 v0, p0

    #@7b8
    iget-object v10, v0, Landroid/webkit/CallbackProxy;->mContext:Landroid/content/Context;

    #@7ba
    invoke-direct {v4, v10}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@7bd
    move-object/from16 v0, v49

    #@7bf
    invoke-virtual {v4, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@7c2
    move-result-object v4

    #@7c3
    const v10, 0x104000a

    #@7c6
    new-instance v11, Landroid/webkit/CallbackProxy$14;

    #@7c8
    move-object/from16 v0, p0

    #@7ca
    move-object/from16 v1, v31

    #@7cc
    invoke-direct {v11, v0, v1}, Landroid/webkit/CallbackProxy$14;-><init>(Landroid/webkit/CallbackProxy;Landroid/webkit/JsResult;)V

    #@7cf
    invoke-virtual {v4, v10, v11}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@7d2
    move-result-object v4

    #@7d3
    const/high16 v10, 0x104

    #@7d5
    new-instance v11, Landroid/webkit/CallbackProxy$13;

    #@7d7
    move-object/from16 v0, p0

    #@7d9
    move-object/from16 v1, v31

    #@7db
    invoke-direct {v11, v0, v1}, Landroid/webkit/CallbackProxy$13;-><init>(Landroid/webkit/CallbackProxy;Landroid/webkit/JsResult;)V

    #@7de
    invoke-virtual {v4, v10, v11}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@7e1
    move-result-object v4

    #@7e2
    new-instance v10, Landroid/webkit/CallbackProxy$12;

    #@7e4
    move-object/from16 v0, p0

    #@7e6
    move-object/from16 v1, v31

    #@7e8
    invoke-direct {v10, v0, v1}, Landroid/webkit/CallbackProxy$12;-><init>(Landroid/webkit/CallbackProxy;Landroid/webkit/JsResult;)V

    #@7eb
    invoke-virtual {v4, v10}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    #@7ee
    move-result-object v4

    #@7ef
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    #@7f2
    move-result-object v39

    #@7f3
    .line 854
    .restart local v39       #dialog:Landroid/app/AlertDialog;
    new-instance v4, Landroid/webkit/CallbackProxy$15;

    #@7f5
    move-object/from16 v0, p0

    #@7f7
    move-object/from16 v1, v31

    #@7f9
    invoke-direct {v4, v0, v1}, Landroid/webkit/CallbackProxy$15;-><init>(Landroid/webkit/CallbackProxy;Landroid/webkit/JsResult;)V

    #@7fc
    move-object/from16 v0, v39

    #@7fe
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    #@801
    .line 864
    .end local v39           #dialog:Landroid/app/AlertDialog;
    .end local v49           #m:Ljava/lang/String;
    :cond_801
    invoke-virtual/range {v60 .. v60}, Landroid/webkit/CallbackProxy$JsResultReceiver;->setReady()V

    #@804
    goto/16 :goto_11

    #@806
    .line 869
    .end local v5           #url:Ljava/lang/String;
    .end local v29           #message:Ljava/lang/String;
    .end local v31           #res:Landroid/webkit/JsResult;
    .end local v60           #receiver:Landroid/webkit/CallbackProxy$JsResultReceiver;
    :pswitch_806
    move-object/from16 v0, p0

    #@808
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@80a
    if-eqz v4, :cond_11

    #@80c
    .line 870
    move-object/from16 v0, p1

    #@80e
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@810
    move-object/from16 v60, v0

    #@812
    check-cast v60, Landroid/webkit/CallbackProxy$JsResultReceiver;

    #@814
    .line 871
    .restart local v60       #receiver:Landroid/webkit/CallbackProxy$JsResultReceiver;
    move-object/from16 v0, v60

    #@816
    iget-object v0, v0, Landroid/webkit/CallbackProxy$JsResultReceiver;->mJsResult:Landroid/webkit/JsPromptResult;

    #@818
    move-object/from16 v31, v0

    #@81a
    .line 872
    .restart local v31       #res:Landroid/webkit/JsResult;
    move-object/from16 v0, p0

    #@81c
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@81e
    invoke-virtual {v4}, Landroid/webkit/WebChromeClient;->onJsTimeout()Z

    #@821
    move-result v4

    #@822
    if-eqz v4, :cond_82c

    #@824
    .line 873
    invoke-virtual/range {v31 .. v31}, Landroid/webkit/JsPromptResult;->confirm()V

    #@827
    .line 877
    :goto_827
    invoke-virtual/range {v60 .. v60}, Landroid/webkit/CallbackProxy$JsResultReceiver;->setReady()V

    #@82a
    goto/16 :goto_11

    #@82c
    .line 875
    :cond_82c
    invoke-virtual/range {v31 .. v31}, Landroid/webkit/JsPromptResult;->cancel()V

    #@82f
    goto :goto_827

    #@830
    .line 882
    .end local v31           #res:Landroid/webkit/JsResult;
    .end local v60           #receiver:Landroid/webkit/CallbackProxy$JsResultReceiver;
    :pswitch_830
    move-object/from16 v0, p0

    #@832
    iget-object v10, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@834
    move-object/from16 v0, p1

    #@836
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@838
    check-cast v4, Landroid/net/http/SslCertificate;

    #@83a
    invoke-virtual {v10, v4}, Landroid/webkit/WebViewClassic;->setCertificate(Landroid/net/http/SslCertificate;)V

    #@83d
    goto/16 :goto_11

    #@83f
    .line 886
    :pswitch_83f
    monitor-enter p0

    #@840
    .line 887
    :try_start_840
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->notify()V

    #@843
    .line 888
    monitor-exit p0

    #@844
    goto/16 :goto_11

    #@846
    :catchall_846
    move-exception v4

    #@847
    monitor-exit p0
    :try_end_848
    .catchall {:try_start_840 .. :try_end_848} :catchall_846

    #@848
    throw v4

    #@849
    .line 892
    :pswitch_849
    move-object/from16 v0, p0

    #@84b
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@84d
    if-eqz v4, :cond_11

    #@84f
    .line 893
    move-object/from16 v0, p0

    #@851
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@853
    move-object/from16 v0, p0

    #@855
    iget-object v10, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@857
    invoke-virtual {v10}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@85a
    move-result-object v10

    #@85b
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@85e
    move-result-object v11

    #@85f
    const-string/jumbo v13, "old"

    #@862
    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    #@865
    move-result v11

    #@866
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@869
    move-result-object v13

    #@86a
    const-string/jumbo v14, "new"

    #@86d
    invoke-virtual {v13, v14}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    #@870
    move-result v13

    #@871
    invoke-virtual {v4, v10, v11, v13}, Landroid/webkit/WebViewClient;->onScaleChanged(Landroid/webkit/WebView;FF)V

    #@874
    goto/16 :goto_11

    #@876
    .line 899
    :pswitch_876
    move-object/from16 v0, p0

    #@878
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@87a
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->switchOutDrawHistory()V

    #@87d
    goto/16 :goto_11

    #@87f
    .line 903
    :pswitch_87f
    move-object/from16 v0, p0

    #@881
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@883
    if-eqz v4, :cond_11

    #@885
    .line 906
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@888
    move-result-object v4

    #@889
    const-string/jumbo v10, "message"

    #@88c
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@88f
    move-result-object v29

    #@890
    .line 907
    .restart local v29       #message:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@893
    move-result-object v4

    #@894
    const-string/jumbo v10, "sourceID"

    #@897
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@89a
    move-result-object v64

    #@89b
    .line 908
    .local v64, sourceID:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@89e
    move-result-object v4

    #@89f
    const-string/jumbo v10, "lineNumber"

    #@8a2
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@8a5
    move-result v46

    #@8a6
    .line 909
    .local v46, lineNumber:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@8a9
    move-result-object v4

    #@8aa
    const-string/jumbo v10, "msgLevel"

    #@8ad
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@8b0
    move-result v52

    #@8b1
    .line 910
    .local v52, msgLevel:I
    invoke-static {}, Landroid/webkit/ConsoleMessage$MessageLevel;->values()[Landroid/webkit/ConsoleMessage$MessageLevel;

    #@8b4
    move-result-object v4

    #@8b5
    array-length v0, v4

    #@8b6
    move/from16 v53, v0

    #@8b8
    .line 912
    .local v53, numberOfMessageLevels:I
    if-ltz v52, :cond_8c0

    #@8ba
    move/from16 v0, v52

    #@8bc
    move/from16 v1, v53

    #@8be
    if-lt v0, v1, :cond_8c2

    #@8c0
    .line 913
    :cond_8c0
    const/16 v52, 0x0

    #@8c2
    .line 916
    :cond_8c2
    invoke-static {}, Landroid/webkit/ConsoleMessage$MessageLevel;->values()[Landroid/webkit/ConsoleMessage$MessageLevel;

    #@8c5
    move-result-object v4

    #@8c6
    aget-object v51, v4, v52

    #@8c8
    .line 919
    .local v51, messageLevel:Landroid/webkit/ConsoleMessage$MessageLevel;
    move-object/from16 v0, p0

    #@8ca
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@8cc
    new-instance v10, Landroid/webkit/ConsoleMessage;

    #@8ce
    move-object/from16 v0, v29

    #@8d0
    move-object/from16 v1, v64

    #@8d2
    move/from16 v2, v46

    #@8d4
    move-object/from16 v3, v51

    #@8d6
    invoke-direct {v10, v0, v1, v2, v3}, Landroid/webkit/ConsoleMessage;-><init>(Ljava/lang/String;Ljava/lang/String;ILandroid/webkit/ConsoleMessage$MessageLevel;)V

    #@8d9
    invoke-virtual {v4, v10}, Landroid/webkit/WebChromeClient;->onConsoleMessage(Landroid/webkit/ConsoleMessage;)Z

    #@8dc
    move-result v4

    #@8dd
    if-nez v4, :cond_11

    #@8df
    .line 923
    const-string v48, "Web Console"

    #@8e1
    .line 924
    .local v48, logTag:Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    #@8e3
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@8e6
    move-object/from16 v0, v29

    #@8e8
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8eb
    move-result-object v4

    #@8ec
    const-string v10, " at "

    #@8ee
    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f1
    move-result-object v4

    #@8f2
    move-object/from16 v0, v64

    #@8f4
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f7
    move-result-object v4

    #@8f8
    const-string v10, ":"

    #@8fa
    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8fd
    move-result-object v4

    #@8fe
    move/from16 v0, v46

    #@900
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@903
    move-result-object v4

    #@904
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@907
    move-result-object v47

    #@908
    .line 926
    .local v47, logMessage:Ljava/lang/String;
    sget-object v4, Landroid/webkit/CallbackProxy$16;->$SwitchMap$android$webkit$ConsoleMessage$MessageLevel:[I

    #@90a
    invoke-virtual/range {v51 .. v51}, Landroid/webkit/ConsoleMessage$MessageLevel;->ordinal()I

    #@90d
    move-result v10

    #@90e
    aget v4, v4, v10

    #@910
    packed-switch v4, :pswitch_data_b28

    #@913
    goto/16 :goto_11

    #@915
    .line 928
    :pswitch_915
    move-object/from16 v0, v48

    #@917
    move-object/from16 v1, v47

    #@919
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@91c
    goto/16 :goto_11

    #@91e
    .line 931
    :pswitch_91e
    move-object/from16 v0, v48

    #@920
    move-object/from16 v1, v47

    #@922
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@925
    goto/16 :goto_11

    #@927
    .line 934
    :pswitch_927
    move-object/from16 v0, v48

    #@929
    move-object/from16 v1, v47

    #@92b
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@92e
    goto/16 :goto_11

    #@930
    .line 937
    :pswitch_930
    move-object/from16 v0, v48

    #@932
    move-object/from16 v1, v47

    #@934
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@937
    goto/16 :goto_11

    #@939
    .line 940
    :pswitch_939
    move-object/from16 v0, v48

    #@93b
    move-object/from16 v1, v47

    #@93d
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@940
    goto/16 :goto_11

    #@942
    .line 948
    .end local v29           #message:Ljava/lang/String;
    .end local v46           #lineNumber:I
    .end local v47           #logMessage:Ljava/lang/String;
    .end local v48           #logTag:Ljava/lang/String;
    .end local v51           #messageLevel:Landroid/webkit/ConsoleMessage$MessageLevel;
    .end local v52           #msgLevel:I
    .end local v53           #numberOfMessageLevels:I
    .end local v64           #sourceID:Ljava/lang/String;
    :pswitch_942
    move-object/from16 v0, p0

    #@944
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@946
    if-eqz v4, :cond_11

    #@948
    .line 949
    move-object/from16 v0, p0

    #@94a
    iget-object v10, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@94c
    move-object/from16 v0, p1

    #@94e
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@950
    check-cast v4, Landroid/webkit/ValueCallback;

    #@952
    invoke-virtual {v10, v4}, Landroid/webkit/WebChromeClient;->getVisitedHistory(Landroid/webkit/ValueCallback;)V

    #@955
    goto/16 :goto_11

    #@957
    .line 954
    :pswitch_957
    move-object/from16 v0, p0

    #@959
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@95b
    if-eqz v4, :cond_11

    #@95d
    .line 955
    move-object/from16 v0, p1

    #@95f
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@961
    move-object/from16 v37, v0

    #@963
    check-cast v37, Landroid/webkit/CallbackProxy$UploadFileMessageData;

    #@965
    .line 956
    .local v37, data:Landroid/webkit/CallbackProxy$UploadFileMessageData;
    move-object/from16 v0, p0

    #@967
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@969
    invoke-virtual/range {v37 .. v37}, Landroid/webkit/CallbackProxy$UploadFileMessageData;->getUploadFile()Landroid/webkit/CallbackProxy$UploadFile;

    #@96c
    move-result-object v10

    #@96d
    invoke-virtual/range {v37 .. v37}, Landroid/webkit/CallbackProxy$UploadFileMessageData;->getAcceptType()Ljava/lang/String;

    #@970
    move-result-object v11

    #@971
    invoke-virtual/range {v37 .. v37}, Landroid/webkit/CallbackProxy$UploadFileMessageData;->getCapture()Ljava/lang/String;

    #@974
    move-result-object v13

    #@975
    invoke-virtual {v4, v10, v11, v13}, Landroid/webkit/WebChromeClient;->openFileChooser(Landroid/webkit/ValueCallback;Ljava/lang/String;Ljava/lang/String;)V

    #@978
    goto/16 :goto_11

    #@97a
    .line 962
    .end local v37           #data:Landroid/webkit/CallbackProxy$UploadFileMessageData;
    :pswitch_97a
    move-object/from16 v0, p0

    #@97c
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebBackForwardListClient:Landroid/webkit/WebBackForwardListClient;

    #@97e
    if-eqz v4, :cond_11

    #@980
    .line 963
    move-object/from16 v0, p0

    #@982
    iget-object v10, v0, Landroid/webkit/CallbackProxy;->mWebBackForwardListClient:Landroid/webkit/WebBackForwardListClient;

    #@984
    move-object/from16 v0, p1

    #@986
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@988
    check-cast v4, Landroid/webkit/WebHistoryItem;

    #@98a
    invoke-virtual {v10, v4}, Landroid/webkit/WebBackForwardListClient;->onNewHistoryItem(Landroid/webkit/WebHistoryItem;)V

    #@98d
    goto/16 :goto_11

    #@98f
    .line 969
    :pswitch_98f
    move-object/from16 v0, p0

    #@991
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebBackForwardListClient:Landroid/webkit/WebBackForwardListClient;

    #@993
    if-eqz v4, :cond_11

    #@995
    .line 970
    move-object/from16 v0, p0

    #@997
    iget-object v10, v0, Landroid/webkit/CallbackProxy;->mWebBackForwardListClient:Landroid/webkit/WebBackForwardListClient;

    #@999
    move-object/from16 v0, p1

    #@99b
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@99d
    check-cast v4, Landroid/webkit/WebHistoryItem;

    #@99f
    move-object/from16 v0, p1

    #@9a1
    iget v11, v0, Landroid/os/Message;->arg1:I

    #@9a3
    invoke-virtual {v10, v4, v11}, Landroid/webkit/WebBackForwardListClient;->onIndexChanged(Landroid/webkit/WebHistoryItem;I)V

    #@9a6
    goto/16 :goto_11

    #@9a8
    .line 975
    :pswitch_9a8
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@9ab
    move-result-object v4

    #@9ac
    const-string v10, "host"

    #@9ae
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@9b1
    move-result-object v45

    #@9b2
    .line 976
    .restart local v45       #host:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@9b5
    move-result-object v4

    #@9b6
    const-string/jumbo v10, "realm"

    #@9b9
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@9bc
    move-result-object v58

    #@9bd
    .line 977
    .restart local v58       #realm:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@9c0
    move-result-object v4

    #@9c1
    const-string/jumbo v10, "username"

    #@9c4
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@9c7
    move-result-object v66

    #@9c8
    .line 978
    .restart local v66       #username:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@9cb
    move-result-object v4

    #@9cc
    const-string/jumbo v10, "password"

    #@9cf
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@9d2
    move-result-object v57

    #@9d3
    .line 979
    .restart local v57       #password:Ljava/lang/String;
    move-object/from16 v0, p0

    #@9d5
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@9d7
    move-object/from16 v0, v45

    #@9d9
    move-object/from16 v1, v58

    #@9db
    move-object/from16 v2, v66

    #@9dd
    move-object/from16 v3, v57

    #@9df
    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/webkit/WebViewClassic;->setHttpAuthUsernamePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@9e2
    goto/16 :goto_11

    #@9e4
    .line 984
    .end local v45           #host:Ljava/lang/String;
    .end local v57           #password:Ljava/lang/String;
    .end local v58           #realm:Ljava/lang/String;
    .end local v66           #username:Ljava/lang/String;
    :pswitch_9e4
    move-object/from16 v0, p0

    #@9e6
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@9e8
    if-eqz v4, :cond_11

    #@9ea
    .line 985
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@9ed
    move-result-object v4

    #@9ee
    const-string/jumbo v10, "realm"

    #@9f1
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@9f4
    move-result-object v58

    #@9f5
    .line 986
    .restart local v58       #realm:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@9f8
    move-result-object v4

    #@9f9
    const-string v10, "account"

    #@9fb
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@9fe
    move-result-object v32

    #@9ff
    .line 987
    .local v32, account:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@a02
    move-result-object v4

    #@a03
    const-string v10, "args"

    #@a05
    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@a08
    move-result-object v33

    #@a09
    .line 988
    .local v33, args:Ljava/lang/String;
    move-object/from16 v0, p0

    #@a0b
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@a0d
    move-object/from16 v0, p0

    #@a0f
    iget-object v10, v0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@a11
    invoke-virtual {v10}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@a14
    move-result-object v10

    #@a15
    move-object/from16 v0, v58

    #@a17
    move-object/from16 v1, v32

    #@a19
    move-object/from16 v2, v33

    #@a1b
    invoke-virtual {v4, v10, v0, v1, v2}, Landroid/webkit/WebViewClient;->onReceivedLoginRequest(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@a1e
    goto/16 :goto_11

    #@a20
    .line 995
    .end local v32           #account:Ljava/lang/String;
    .end local v33           #args:Ljava/lang/String;
    .end local v58           #realm:Ljava/lang/String;
    :pswitch_a20
    move-object/from16 v0, p0

    #@a22
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@a24
    if-eqz v4, :cond_11

    #@a26
    .line 996
    move-object/from16 v0, p1

    #@a28
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a2a
    move-object/from16 v37, v0

    #@a2c
    check-cast v37, Landroid/webkit/CallbackProxy$PickerMessageData;

    #@a2e
    .line 997
    .local v37, data:Landroid/webkit/CallbackProxy$PickerMessageData;
    move-object/from16 v0, p0

    #@a30
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@a32
    invoke-virtual/range {v37 .. v37}, Landroid/webkit/CallbackProxy$PickerMessageData;->getType()Ljava/lang/String;

    #@a35
    move-result-object v10

    #@a36
    invoke-virtual/range {v37 .. v37}, Landroid/webkit/CallbackProxy$PickerMessageData;->getValue()Ljava/lang/String;

    #@a39
    move-result-object v11

    #@a3a
    invoke-virtual {v4, v10, v11}, Landroid/webkit/WebChromeClient;->openDateTimePicker(Ljava/lang/String;Ljava/lang/String;)V

    #@a3d
    goto/16 :goto_11

    #@a3f
    .line 1003
    .end local v37           #data:Landroid/webkit/CallbackProxy$PickerMessageData;
    :pswitch_a3f
    move-object/from16 v0, p0

    #@a41
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@a43
    if-eqz v4, :cond_11

    #@a45
    .line 1004
    move-object/from16 v0, p1

    #@a47
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a49
    move-object/from16 v37, v0

    #@a4b
    check-cast v37, Landroid/webkit/CallbackProxy$PickerMessageData;

    #@a4d
    .line 1005
    .restart local v37       #data:Landroid/webkit/CallbackProxy$PickerMessageData;
    move-object/from16 v0, p0

    #@a4f
    iget-object v4, v0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@a51
    invoke-virtual/range {v37 .. v37}, Landroid/webkit/CallbackProxy$PickerMessageData;->getValue()Ljava/lang/String;

    #@a54
    move-result-object v10

    #@a55
    invoke-virtual {v4, v10}, Landroid/webkit/WebChromeClient;->openColorPicker(Ljava/lang/String;)V

    #@a58
    goto/16 :goto_11

    #@a5a
    .line 343
    :pswitch_data_a5a
    .packed-switch 0x64
        :pswitch_1d
        :pswitch_78
        :pswitch_bc
        :pswitch_149
        :pswitch_177
        :pswitch_1b1
        :pswitch_248
        :pswitch_26d
        :pswitch_293
        :pswitch_36b
        :pswitch_3c9
        :pswitch_3e2
        :pswitch_526
        :pswitch_5c1
        :pswitch_66b
        :pswitch_755
        :pswitch_420
        :pswitch_1c
        :pswitch_2b0
        :pswitch_d9
        :pswitch_110
        :pswitch_4f
        :pswitch_3b2
        :pswitch_849
        :pswitch_830
        :pswitch_876
        :pswitch_43d
        :pswitch_4a1
        :pswitch_806
        :pswitch_87f
        :pswitch_4e7
        :pswitch_517
        :pswitch_95
        :pswitch_942
        :pswitch_957
        :pswitch_97a
        :pswitch_98f
        :pswitch_9a8
        :pswitch_1c
        :pswitch_1c
        :pswitch_9e4
        :pswitch_20b
        :pswitch_1c
        :pswitch_1c
        :pswitch_1e4
        :pswitch_a20
        :pswitch_a3f
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_83f
    .end packed-switch

    #@b28
    .line 926
    :pswitch_data_b28
    .packed-switch 0x1
        :pswitch_915
        :pswitch_91e
        :pswitch_927
        :pswitch_930
        :pswitch_939
    .end packed-switch
.end method

.method protected declared-synchronized messagesBlocked()Z
    .registers 2

    #@0
    .prologue
    .line 210
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/CallbackProxy;->mBlockMessages:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public notifyForUiThread()V
    .registers 2

    #@0
    .prologue
    .line 1826
    monitor-enter p0

    #@1
    .line 1827
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    #@4
    .line 1828
    monitor-exit p0

    #@5
    .line 1829
    return-void

    #@6
    .line 1828
    :catchall_6
    move-exception v0

    #@7
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_6

    #@8
    throw v0
.end method

.method public onCloseWindow(Landroid/webkit/WebViewClassic;)V
    .registers 3
    .parameter "window"

    #@0
    .prologue
    .line 1436
    iget-object v0, p0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 1440
    :goto_4
    return-void

    #@5
    .line 1439
    :cond_5
    const/16 v0, 0x6e

    #@7
    invoke-virtual {p0, v0, p1}, Landroid/webkit/CallbackProxy;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p0, v0}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@e
    goto :goto_4
.end method

.method public onDownloadStart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)Z
    .registers 12
    .parameter "url"
    .parameter "userAgent"
    .parameter "contentDisposition"
    .parameter "mimetype"
    .parameter "referer"
    .parameter "contentLength"
    .parameter "suggestedName"

    #@0
    .prologue
    .line 1317
    iget-object v2, p0, Landroid/webkit/CallbackProxy;->mDownloadListener:Landroid/webkit/DownloadListener;

    #@2
    if-nez v2, :cond_a

    #@4
    iget-object v2, p0, Landroid/webkit/CallbackProxy;->mDownloadListenerExt:Landroid/webkit/DownloadListenerExt;

    #@6
    if-nez v2, :cond_a

    #@8
    .line 1319
    const/4 v2, 0x0

    #@9
    .line 1333
    :goto_9
    return v2

    #@a
    .line 1322
    :cond_a
    const/16 v2, 0x76

    #@c
    invoke-virtual {p0, v2}, Landroid/webkit/CallbackProxy;->obtainMessage(I)Landroid/os/Message;

    #@f
    move-result-object v1

    #@10
    .line 1323
    .local v1, msg:Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@13
    move-result-object v0

    #@14
    .line 1324
    .local v0, bundle:Landroid/os/Bundle;
    const-string/jumbo v2, "url"

    #@17
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@1a
    .line 1325
    const-string/jumbo v2, "userAgent"

    #@1d
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@20
    .line 1326
    const-string/jumbo v2, "mimetype"

    #@23
    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@26
    .line 1327
    const-string/jumbo v2, "referer"

    #@29
    invoke-virtual {v0, v2, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@2c
    .line 1328
    const-string v2, "contentLength"

    #@2e
    invoke-virtual {v0, v2, p6, p7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@31
    .line 1329
    const-string v2, "contentDisposition"

    #@33
    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@36
    .line 1331
    const-string/jumbo v2, "suggestedName"

    #@39
    invoke-virtual {v0, v2, p8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@3c
    .line 1332
    invoke-virtual {p0, v1}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@3f
    .line 1333
    const/4 v2, 0x1

    #@40
    goto :goto_9
.end method

.method public onExceededDatabaseQuota(Ljava/lang/String;Ljava/lang/String;JJJLandroid/webkit/WebStorage$QuotaUpdater;)V
    .registers 14
    .parameter "url"
    .parameter "databaseIdentifier"
    .parameter "quota"
    .parameter "estimatedDatabaseSize"
    .parameter "totalQuota"
    .parameter "quotaUpdater"

    #@0
    .prologue
    .line 1558
    iget-object v2, p0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@2
    if-nez v2, :cond_8

    #@4
    .line 1561
    invoke-interface {p9, p3, p4}, Landroid/webkit/WebStorage$QuotaUpdater;->updateQuota(J)V

    #@7
    .line 1575
    :goto_7
    return-void

    #@8
    .line 1565
    :cond_8
    const/16 v2, 0x7e

    #@a
    invoke-virtual {p0, v2}, Landroid/webkit/CallbackProxy;->obtainMessage(I)Landroid/os/Message;

    #@d
    move-result-object v0

    #@e
    .line 1566
    .local v0, exceededQuota:Landroid/os/Message;
    new-instance v1, Ljava/util/HashMap;

    #@10
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@13
    .line 1567
    .local v1, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v2, "databaseIdentifier"

    #@15
    invoke-virtual {v1, v2, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@18
    .line 1568
    const-string/jumbo v2, "url"

    #@1b
    invoke-virtual {v1, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    .line 1569
    const-string/jumbo v2, "quota"

    #@21
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@28
    .line 1570
    const-string v2, "estimatedDatabaseSize"

    #@2a
    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@31
    .line 1571
    const-string/jumbo v2, "totalQuota"

    #@34
    invoke-static {p7, p8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@37
    move-result-object v3

    #@38
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3b
    .line 1572
    const-string/jumbo v2, "quotaUpdater"

    #@3e
    invoke-virtual {v1, v2, p9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@41
    .line 1573
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@43
    .line 1574
    invoke-virtual {p0, v0}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@46
    goto :goto_7
.end method

.method public onFormResubmission(Landroid/os/Message;Landroid/os/Message;)V
    .registers 6
    .parameter "dontResend"
    .parameter "resend"

    #@0
    .prologue
    .line 1156
    iget-object v2, p0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@2
    if-nez v2, :cond_8

    #@4
    .line 1157
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    #@7
    .line 1166
    :goto_7
    return-void

    #@8
    .line 1161
    :cond_8
    const/16 v2, 0x78

    #@a
    invoke-virtual {p0, v2}, Landroid/webkit/CallbackProxy;->obtainMessage(I)Landroid/os/Message;

    #@d
    move-result-object v1

    #@e
    .line 1162
    .local v1, msg:Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@11
    move-result-object v0

    #@12
    .line 1163
    .local v0, bundle:Landroid/os/Bundle;
    const-string/jumbo v2, "resend"

    #@15
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@18
    .line 1164
    const-string v2, "dontResend"

    #@1a
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@1d
    .line 1165
    invoke-virtual {p0, v1}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@20
    goto :goto_7
.end method

.method public onGeolocationPermissionsHidePrompt()V
    .registers 3

    #@0
    .prologue
    .line 1632
    iget-object v1, p0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 1638
    :goto_4
    return-void

    #@5
    .line 1636
    :cond_5
    const/16 v1, 0x83

    #@7
    invoke-virtual {p0, v1}, Landroid/webkit/CallbackProxy;->obtainMessage(I)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    .line 1637
    .local v0, hideMessage:Landroid/os/Message;
    invoke-virtual {p0, v0}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@e
    goto :goto_4
.end method

.method public onGeolocationPermissionsShowPrompt(Ljava/lang/String;Landroid/webkit/GeolocationPermissions$Callback;)V
    .registers 6
    .parameter "origin"
    .parameter "callback"

    #@0
    .prologue
    .line 1614
    iget-object v2, p0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@2
    if-nez v2, :cond_5

    #@4
    .line 1625
    :goto_4
    return-void

    #@5
    .line 1618
    :cond_5
    const/16 v2, 0x82

    #@7
    invoke-virtual {p0, v2}, Landroid/webkit/CallbackProxy;->obtainMessage(I)Landroid/os/Message;

    #@a
    move-result-object v1

    #@b
    .line 1620
    .local v1, showMessage:Landroid/os/Message;
    new-instance v0, Ljava/util/HashMap;

    #@d
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@10
    .line 1621
    .local v0, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string/jumbo v2, "origin"

    #@13
    invoke-virtual {v0, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@16
    .line 1622
    const-string v2, "callback"

    #@18
    invoke-virtual {v0, v2, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1b
    .line 1623
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1d
    .line 1624
    invoke-virtual {p0, v1}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@20
    goto :goto_4
.end method

.method onIndexChanged(Landroid/webkit/WebHistoryItem;I)V
    .registers 6
    .parameter "item"
    .parameter "index"

    #@0
    .prologue
    .line 1745
    iget-object v1, p0, Landroid/webkit/CallbackProxy;->mWebBackForwardListClient:Landroid/webkit/WebBackForwardListClient;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 1750
    :goto_4
    return-void

    #@5
    .line 1748
    :cond_5
    const/16 v1, 0x88

    #@7
    const/4 v2, 0x0

    #@8
    invoke-virtual {p0, v1, p2, v2, p1}, Landroid/webkit/CallbackProxy;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@b
    move-result-object v0

    #@c
    .line 1749
    .local v0, msg:Landroid/os/Message;
    invoke-virtual {p0, v0}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@f
    goto :goto_4
.end method

.method public onJsAlert(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "url"
    .parameter "message"

    #@0
    .prologue
    .line 1485
    iget-object v2, p0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@2
    if-nez v2, :cond_5

    #@4
    .line 1493
    :goto_4
    return-void

    #@5
    .line 1488
    :cond_5
    new-instance v1, Landroid/webkit/CallbackProxy$JsResultReceiver;

    #@7
    const/4 v2, 0x0

    #@8
    invoke-direct {v1, p0, v2}, Landroid/webkit/CallbackProxy$JsResultReceiver;-><init>(Landroid/webkit/CallbackProxy;Landroid/webkit/CallbackProxy$1;)V

    #@b
    .line 1489
    .local v1, result:Landroid/webkit/CallbackProxy$JsResultReceiver;
    const/16 v2, 0x70

    #@d
    invoke-virtual {p0, v2, v1}, Landroid/webkit/CallbackProxy;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@10
    move-result-object v0

    #@11
    .line 1490
    .local v0, alert:Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@14
    move-result-object v2

    #@15
    const-string/jumbo v3, "message"

    #@18
    invoke-virtual {v2, v3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@1b
    .line 1491
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@1e
    move-result-object v2

    #@1f
    const-string/jumbo v3, "url"

    #@22
    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@25
    .line 1492
    invoke-direct {p0, v0}, Landroid/webkit/CallbackProxy;->sendMessageToUiThreadSync(Landroid/os/Message;)V

    #@28
    goto :goto_4
.end method

.method public onJsBeforeUnload(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 7
    .parameter "url"
    .parameter "message"

    #@0
    .prologue
    .line 1527
    iget-object v2, p0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@2
    if-nez v2, :cond_6

    #@4
    .line 1528
    const/4 v2, 0x1

    #@5
    .line 1535
    :goto_5
    return v2

    #@6
    .line 1530
    :cond_6
    new-instance v1, Landroid/webkit/CallbackProxy$JsResultReceiver;

    #@8
    const/4 v2, 0x0

    #@9
    invoke-direct {v1, p0, v2}, Landroid/webkit/CallbackProxy$JsResultReceiver;-><init>(Landroid/webkit/CallbackProxy;Landroid/webkit/CallbackProxy$1;)V

    #@c
    .line 1531
    .local v1, result:Landroid/webkit/CallbackProxy$JsResultReceiver;
    const/16 v2, 0x73

    #@e
    invoke-virtual {p0, v2, v1}, Landroid/webkit/CallbackProxy;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@11
    move-result-object v0

    #@12
    .line 1532
    .local v0, confirm:Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@15
    move-result-object v2

    #@16
    const-string/jumbo v3, "message"

    #@19
    invoke-virtual {v2, v3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@1c
    .line 1533
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@1f
    move-result-object v2

    #@20
    const-string/jumbo v3, "url"

    #@23
    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@26
    .line 1534
    invoke-direct {p0, v0}, Landroid/webkit/CallbackProxy;->sendMessageToUiThreadSync(Landroid/os/Message;)V

    #@29
    .line 1535
    iget-object v2, v1, Landroid/webkit/CallbackProxy$JsResultReceiver;->mJsResult:Landroid/webkit/JsPromptResult;

    #@2b
    invoke-virtual {v2}, Landroid/webkit/JsPromptResult;->getResult()Z

    #@2e
    move-result v2

    #@2f
    goto :goto_5
.end method

.method public onJsConfirm(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 7
    .parameter "url"
    .parameter "message"

    #@0
    .prologue
    .line 1498
    iget-object v2, p0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@2
    if-nez v2, :cond_6

    #@4
    .line 1499
    const/4 v2, 0x0

    #@5
    .line 1506
    :goto_5
    return v2

    #@6
    .line 1501
    :cond_6
    new-instance v1, Landroid/webkit/CallbackProxy$JsResultReceiver;

    #@8
    const/4 v2, 0x0

    #@9
    invoke-direct {v1, p0, v2}, Landroid/webkit/CallbackProxy$JsResultReceiver;-><init>(Landroid/webkit/CallbackProxy;Landroid/webkit/CallbackProxy$1;)V

    #@c
    .line 1502
    .local v1, result:Landroid/webkit/CallbackProxy$JsResultReceiver;
    const/16 v2, 0x71

    #@e
    invoke-virtual {p0, v2, v1}, Landroid/webkit/CallbackProxy;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@11
    move-result-object v0

    #@12
    .line 1503
    .local v0, confirm:Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@15
    move-result-object v2

    #@16
    const-string/jumbo v3, "message"

    #@19
    invoke-virtual {v2, v3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@1c
    .line 1504
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@1f
    move-result-object v2

    #@20
    const-string/jumbo v3, "url"

    #@23
    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@26
    .line 1505
    invoke-direct {p0, v0}, Landroid/webkit/CallbackProxy;->sendMessageToUiThreadSync(Landroid/os/Message;)V

    #@29
    .line 1506
    iget-object v2, v1, Landroid/webkit/CallbackProxy$JsResultReceiver;->mJsResult:Landroid/webkit/JsPromptResult;

    #@2b
    invoke-virtual {v2}, Landroid/webkit/JsPromptResult;->getResult()Z

    #@2e
    move-result v2

    #@2f
    goto :goto_5
.end method

.method public onJsPrompt(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .parameter "url"
    .parameter "message"
    .parameter "defaultValue"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1512
    iget-object v3, p0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@3
    if-nez v3, :cond_6

    #@5
    .line 1521
    :goto_5
    return-object v2

    #@6
    .line 1515
    :cond_6
    new-instance v1, Landroid/webkit/CallbackProxy$JsResultReceiver;

    #@8
    invoke-direct {v1, p0, v2}, Landroid/webkit/CallbackProxy$JsResultReceiver;-><init>(Landroid/webkit/CallbackProxy;Landroid/webkit/CallbackProxy$1;)V

    #@b
    .line 1516
    .local v1, result:Landroid/webkit/CallbackProxy$JsResultReceiver;
    const/16 v2, 0x72

    #@d
    invoke-virtual {p0, v2, v1}, Landroid/webkit/CallbackProxy;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@10
    move-result-object v0

    #@11
    .line 1517
    .local v0, prompt:Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@14
    move-result-object v2

    #@15
    const-string/jumbo v3, "message"

    #@18
    invoke-virtual {v2, v3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@1b
    .line 1518
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@1e
    move-result-object v2

    #@1f
    const-string v3, "default"

    #@21
    invoke-virtual {v2, v3, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@24
    .line 1519
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@27
    move-result-object v2

    #@28
    const-string/jumbo v3, "url"

    #@2b
    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@2e
    .line 1520
    invoke-direct {p0, v0}, Landroid/webkit/CallbackProxy;->sendMessageToUiThreadSync(Landroid/os/Message;)V

    #@31
    .line 1521
    iget-object v2, v1, Landroid/webkit/CallbackProxy$JsResultReceiver;->mJsResult:Landroid/webkit/JsPromptResult;

    #@33
    invoke-virtual {v2}, Landroid/webkit/JsPromptResult;->getStringResult()Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    goto :goto_5
.end method

.method public onJsTimeout()Z
    .registers 4

    #@0
    .prologue
    .line 1666
    iget-object v2, p0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@2
    if-nez v2, :cond_6

    #@4
    .line 1667
    const/4 v2, 0x1

    #@5
    .line 1672
    :goto_5
    return v2

    #@6
    .line 1669
    :cond_6
    new-instance v0, Landroid/webkit/CallbackProxy$JsResultReceiver;

    #@8
    const/4 v2, 0x0

    #@9
    invoke-direct {v0, p0, v2}, Landroid/webkit/CallbackProxy$JsResultReceiver;-><init>(Landroid/webkit/CallbackProxy;Landroid/webkit/CallbackProxy$1;)V

    #@c
    .line 1670
    .local v0, result:Landroid/webkit/CallbackProxy$JsResultReceiver;
    const/16 v2, 0x80

    #@e
    invoke-virtual {p0, v2, v0}, Landroid/webkit/CallbackProxy;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@11
    move-result-object v1

    #@12
    .line 1671
    .local v1, timeout:Landroid/os/Message;
    invoke-direct {p0, v1}, Landroid/webkit/CallbackProxy;->sendMessageToUiThreadSync(Landroid/os/Message;)V

    #@15
    .line 1672
    iget-object v2, v0, Landroid/webkit/CallbackProxy$JsResultReceiver;->mJsResult:Landroid/webkit/JsPromptResult;

    #@17
    invoke-virtual {v2}, Landroid/webkit/JsPromptResult;->getResult()Z

    #@1a
    move-result v2

    #@1b
    goto :goto_5
.end method

.method onNewHistoryItem(Landroid/webkit/WebHistoryItem;)V
    .registers 4
    .parameter "item"

    #@0
    .prologue
    .line 1737
    iget-object v1, p0, Landroid/webkit/CallbackProxy;->mWebBackForwardListClient:Landroid/webkit/WebBackForwardListClient;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 1742
    :goto_4
    return-void

    #@5
    .line 1740
    :cond_5
    const/16 v1, 0x87

    #@7
    invoke-virtual {p0, v1, p1}, Landroid/webkit/CallbackProxy;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    .line 1741
    .local v0, msg:Landroid/os/Message;
    invoke-virtual {p0, v0}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@e
    goto :goto_4
.end method

.method public onPageFinished(Ljava/lang/String;)V
    .registers 5
    .parameter "url"

    #@0
    .prologue
    const/16 v2, 0x64

    #@2
    .line 1114
    const/16 v1, 0x79

    #@4
    invoke-virtual {p0, v1, p1}, Landroid/webkit/CallbackProxy;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 1115
    .local v0, msg:Landroid/os/Message;
    invoke-virtual {p0, v0}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@b
    .line 1119
    iget-boolean v1, p0, Landroid/webkit/CallbackProxy;->mProgressMustUpdate:Z

    #@d
    if-eqz v1, :cond_1d

    #@f
    .line 1123
    iget v1, p0, Landroid/webkit/CallbackProxy;->mLatestProgress:I

    #@11
    if-eq v1, v2, :cond_15

    #@13
    .line 1124
    iput v2, p0, Landroid/webkit/CallbackProxy;->mLatestProgress:I

    #@15
    .line 1125
    :cond_15
    const/16 v1, 0x6a

    #@17
    invoke-virtual {p0, v1}, Landroid/webkit/CallbackProxy;->sendEmptyMessage(I)Z

    #@1a
    .line 1126
    const/4 v1, 0x0

    #@1b
    iput-boolean v1, p0, Landroid/webkit/CallbackProxy;->mProgressMustUpdate:Z

    #@1d
    .line 1129
    :cond_1d
    return-void
.end method

.method public onPageStarted(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .registers 6
    .parameter "url"
    .parameter "favicon"

    #@0
    .prologue
    .line 1097
    const/16 v1, 0x64

    #@2
    invoke-virtual {p0, v1}, Landroid/webkit/CallbackProxy;->obtainMessage(I)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    .line 1098
    .local v0, msg:Landroid/os/Message;
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8
    .line 1099
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@b
    move-result-object v1

    #@c
    const-string/jumbo v2, "url"

    #@f
    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@12
    .line 1100
    invoke-virtual {p0, v0}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@15
    .line 1102
    const/4 v1, 0x1

    #@16
    iput-boolean v1, p0, Landroid/webkit/CallbackProxy;->mProgressMustUpdate:Z

    #@18
    .line 1103
    return-void
.end method

.method public onProceededAfterSslError(Landroid/net/http/SslError;)V
    .registers 4
    .parameter "error"

    #@0
    .prologue
    .line 1216
    iget-object v1, p0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@2
    if-eqz v1, :cond_a

    #@4
    iget-object v1, p0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@6
    instance-of v1, v1, Landroid/webkit/WebViewClientClassicExt;

    #@8
    if-nez v1, :cond_b

    #@a
    .line 1222
    :cond_a
    :goto_a
    return-void

    #@b
    .line 1219
    :cond_b
    const/16 v1, 0x90

    #@d
    invoke-virtual {p0, v1}, Landroid/webkit/CallbackProxy;->obtainMessage(I)Landroid/os/Message;

    #@10
    move-result-object v0

    #@11
    .line 1220
    .local v0, msg:Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@13
    .line 1221
    invoke-virtual {p0, v0}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@16
    goto :goto_a
.end method

.method public onProgressChanged(I)V
    .registers 3
    .parameter "newProgress"

    #@0
    .prologue
    .line 1377
    monitor-enter p0

    #@1
    .line 1380
    :try_start_1
    iget v0, p0, Landroid/webkit/CallbackProxy;->mLatestProgress:I

    #@3
    if-ne v0, p1, :cond_7

    #@5
    .line 1381
    monitor-exit p0

    #@6
    .line 1395
    :goto_6
    return-void

    #@7
    .line 1383
    :cond_7
    iput p1, p0, Landroid/webkit/CallbackProxy;->mLatestProgress:I

    #@9
    .line 1384
    iget-object v0, p0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@b
    if-nez v0, :cond_12

    #@d
    .line 1385
    monitor-exit p0

    #@e
    goto :goto_6

    #@f
    .line 1394
    :catchall_f
    move-exception v0

    #@10
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_f

    #@11
    throw v0

    #@12
    .line 1387
    :cond_12
    :try_start_12
    iget-boolean v0, p0, Landroid/webkit/CallbackProxy;->mProgressUpdatePending:Z

    #@14
    if-nez v0, :cond_21

    #@16
    .line 1388
    const/16 v0, 0x6a

    #@18
    invoke-virtual {p0, v0}, Landroid/webkit/CallbackProxy;->sendEmptyMessage(I)Z

    #@1b
    .line 1389
    const/4 v0, 0x1

    #@1c
    iput-boolean v0, p0, Landroid/webkit/CallbackProxy;->mProgressUpdatePending:Z

    #@1e
    .line 1391
    const/4 v0, 0x0

    #@1f
    iput-boolean v0, p0, Landroid/webkit/CallbackProxy;->mProgressMustUpdate:Z

    #@21
    .line 1394
    :cond_21
    monitor-exit p0
    :try_end_22
    .catchall {:try_start_12 .. :try_end_22} :catchall_f

    #@22
    goto :goto_6
.end method

.method public onReachedMaxAppCacheSize(JJLandroid/webkit/WebStorage$QuotaUpdater;)V
    .registers 10
    .parameter "requiredStorage"
    .parameter "quota"
    .parameter "quotaUpdater"

    #@0
    .prologue
    .line 1589
    iget-object v2, p0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@2
    if-nez v2, :cond_8

    #@4
    .line 1592
    invoke-interface {p5, p3, p4}, Landroid/webkit/WebStorage$QuotaUpdater;->updateQuota(J)V

    #@7
    .line 1603
    :goto_7
    return-void

    #@8
    .line 1596
    :cond_8
    const/16 v2, 0x7f

    #@a
    invoke-virtual {p0, v2}, Landroid/webkit/CallbackProxy;->obtainMessage(I)Landroid/os/Message;

    #@d
    move-result-object v1

    #@e
    .line 1597
    .local v1, msg:Landroid/os/Message;
    new-instance v0, Ljava/util/HashMap;

    #@10
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@13
    .line 1598
    .local v0, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string/jumbo v2, "requiredStorage"

    #@16
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1d
    .line 1599
    const-string/jumbo v2, "quota"

    #@20
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@27
    .line 1600
    const-string/jumbo v2, "quotaUpdater"

    #@2a
    invoke-virtual {v0, v2, p5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2d
    .line 1601
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2f
    .line 1602
    invoke-virtual {p0, v1}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@32
    goto :goto_7
.end method

.method public onReceivedCertificate(Landroid/net/http/SslCertificate;)V
    .registers 3
    .parameter "certificate"

    #@0
    .prologue
    .line 1241
    const/16 v0, 0x7c

    #@2
    invoke-virtual {p0, v0, p1}, Landroid/webkit/CallbackProxy;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@9
    .line 1242
    return-void
.end method

.method public onReceivedClientCertRequest(Landroid/webkit/ClientCertRequestHandler;Ljava/lang/String;)V
    .registers 6
    .parameter "handler"
    .parameter "host_and_port"

    #@0
    .prologue
    .line 1227
    iget-object v2, p0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@2
    if-eqz v2, :cond_a

    #@4
    iget-object v2, p0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@6
    instance-of v2, v2, Landroid/webkit/WebViewClientClassicExt;

    #@8
    if-nez v2, :cond_e

    #@a
    .line 1228
    :cond_a
    invoke-virtual {p1}, Landroid/webkit/ClientCertRequestHandler;->cancel()V

    #@d
    .line 1237
    :goto_d
    return-void

    #@e
    .line 1231
    :cond_e
    const/16 v2, 0x8d

    #@10
    invoke-virtual {p0, v2}, Landroid/webkit/CallbackProxy;->obtainMessage(I)Landroid/os/Message;

    #@13
    move-result-object v1

    #@14
    .line 1232
    .local v1, msg:Landroid/os/Message;
    new-instance v0, Ljava/util/HashMap;

    #@16
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@19
    .line 1233
    .local v0, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v2, "handler"

    #@1b
    invoke-virtual {v0, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    .line 1234
    const-string v2, "host_and_port"

    #@20
    invoke-virtual {v0, v2, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@23
    .line 1235
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@25
    .line 1236
    invoke-virtual {p0, v1}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@28
    goto :goto_d
.end method

.method public onReceivedError(ILjava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "errorCode"
    .parameter "description"
    .parameter "failingUrl"

    #@0
    .prologue
    .line 1141
    iget-object v1, p0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 1150
    :goto_4
    return-void

    #@5
    .line 1145
    :cond_5
    const/16 v1, 0x77

    #@7
    invoke-virtual {p0, v1}, Landroid/webkit/CallbackProxy;->obtainMessage(I)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    .line 1146
    .local v0, msg:Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    #@d
    .line 1147
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@10
    move-result-object v1

    #@11
    const-string v2, "description"

    #@13
    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@16
    .line 1148
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@19
    move-result-object v1

    #@1a
    const-string v2, "failingUrl"

    #@1c
    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@1f
    .line 1149
    invoke-virtual {p0, v0}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@22
    goto :goto_4
.end method

.method public onReceivedHttpAuthCredentials(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "host"
    .parameter "realm"
    .parameter "username"
    .parameter "password"

    #@0
    .prologue
    .line 1363
    const/16 v1, 0x89

    #@2
    invoke-virtual {p0, v1}, Landroid/webkit/CallbackProxy;->obtainMessage(I)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    .line 1364
    .local v0, msg:Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@9
    move-result-object v1

    #@a
    const-string v2, "host"

    #@c
    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@f
    .line 1365
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@12
    move-result-object v1

    #@13
    const-string/jumbo v2, "realm"

    #@16
    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@19
    .line 1366
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@1c
    move-result-object v1

    #@1d
    const-string/jumbo v2, "username"

    #@20
    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@23
    .line 1367
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@26
    move-result-object v1

    #@27
    const-string/jumbo v2, "password"

    #@2a
    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@2d
    .line 1368
    invoke-virtual {p0, v0}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@30
    .line 1369
    return-void
.end method

.method public onReceivedHttpAuthRequest(Landroid/webkit/HttpAuthHandler;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "handler"
    .parameter "hostName"
    .parameter "realmName"

    #@0
    .prologue
    .line 1190
    iget-object v1, p0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@2
    if-nez v1, :cond_8

    #@4
    .line 1191
    invoke-virtual {p1}, Landroid/webkit/HttpAuthHandler;->cancel()V

    #@7
    .line 1198
    :goto_7
    return-void

    #@8
    .line 1194
    :cond_8
    const/16 v1, 0x68

    #@a
    invoke-virtual {p0, v1, p1}, Landroid/webkit/CallbackProxy;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@d
    move-result-object v0

    #@e
    .line 1195
    .local v0, msg:Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@11
    move-result-object v1

    #@12
    const-string v2, "host"

    #@14
    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@17
    .line 1196
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@1a
    move-result-object v1

    #@1b
    const-string/jumbo v2, "realm"

    #@1e
    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@21
    .line 1197
    invoke-virtual {p0, v0}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@24
    goto :goto_7
.end method

.method public onReceivedIcon(Landroid/graphics/Bitmap;)V
    .registers 4
    .parameter "icon"

    #@0
    .prologue
    .line 1445
    iget-object v1, p0, Landroid/webkit/CallbackProxy;->mBackForwardList:Landroid/webkit/WebBackForwardListClassic;

    #@2
    invoke-virtual {v1}, Landroid/webkit/WebBackForwardListClassic;->getCurrentItem()Landroid/webkit/WebHistoryItemClassic;

    #@5
    move-result-object v0

    #@6
    .line 1446
    .local v0, i:Landroid/webkit/WebHistoryItemClassic;
    if-eqz v0, :cond_b

    #@8
    .line 1447
    invoke-virtual {v0, p1}, Landroid/webkit/WebHistoryItemClassic;->setFavicon(Landroid/graphics/Bitmap;)V

    #@b
    .line 1451
    :cond_b
    iget-object v1, p0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@d
    if-nez v1, :cond_10

    #@f
    .line 1455
    :goto_f
    return-void

    #@10
    .line 1454
    :cond_10
    const/16 v1, 0x65

    #@12
    invoke-virtual {p0, v1, p1}, Landroid/webkit/CallbackProxy;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {p0, v1}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@19
    goto :goto_f
.end method

.method onReceivedLoginRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "realm"
    .parameter "account"
    .parameter "args"

    #@0
    .prologue
    .line 1291
    iget-object v2, p0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@2
    if-nez v2, :cond_5

    #@4
    .line 1300
    :goto_4
    return-void

    #@5
    .line 1294
    :cond_5
    const/16 v2, 0x8c

    #@7
    invoke-virtual {p0, v2}, Landroid/webkit/CallbackProxy;->obtainMessage(I)Landroid/os/Message;

    #@a
    move-result-object v1

    #@b
    .line 1295
    .local v1, msg:Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@e
    move-result-object v0

    #@f
    .line 1296
    .local v0, bundle:Landroid/os/Bundle;
    const-string/jumbo v2, "realm"

    #@12
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@15
    .line 1297
    const-string v2, "account"

    #@17
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@1a
    .line 1298
    const-string v2, "args"

    #@1c
    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@1f
    .line 1299
    invoke-virtual {p0, v1}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@22
    goto :goto_4
.end method

.method public onReceivedSslError(Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .registers 6
    .parameter "handler"
    .parameter "error"

    #@0
    .prologue
    .line 1203
    iget-object v2, p0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@2
    if-nez v2, :cond_8

    #@4
    .line 1204
    invoke-virtual {p1}, Landroid/webkit/SslErrorHandler;->cancel()V

    #@7
    .line 1213
    :goto_7
    return-void

    #@8
    .line 1207
    :cond_8
    const/16 v2, 0x69

    #@a
    invoke-virtual {p0, v2}, Landroid/webkit/CallbackProxy;->obtainMessage(I)Landroid/os/Message;

    #@d
    move-result-object v1

    #@e
    .line 1208
    .local v1, msg:Landroid/os/Message;
    new-instance v0, Ljava/util/HashMap;

    #@10
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@13
    .line 1209
    .local v0, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v2, "handler"

    #@15
    invoke-virtual {v0, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@18
    .line 1210
    const-string v2, "error"

    #@1a
    invoke-virtual {v0, v2, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1d
    .line 1211
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1f
    .line 1212
    invoke-virtual {p0, v1}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@22
    goto :goto_7
.end method

.method public onReceivedTitle(Ljava/lang/String;)V
    .registers 3
    .parameter "title"

    #@0
    .prologue
    .line 1476
    iget-object v0, p0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 1480
    :goto_4
    return-void

    #@5
    .line 1479
    :cond_5
    const/16 v0, 0x66

    #@7
    invoke-virtual {p0, v0, p1}, Landroid/webkit/CallbackProxy;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p0, v0}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@e
    goto :goto_4
.end method

.method onReceivedTouchIconUrl(Ljava/lang/String;Z)V
    .registers 7
    .parameter "url"
    .parameter "precomposed"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1460
    iget-object v1, p0, Landroid/webkit/CallbackProxy;->mBackForwardList:Landroid/webkit/WebBackForwardListClassic;

    #@3
    invoke-virtual {v1}, Landroid/webkit/WebBackForwardListClassic;->getCurrentItem()Landroid/webkit/WebHistoryItemClassic;

    #@6
    move-result-object v0

    #@7
    .line 1461
    .local v0, i:Landroid/webkit/WebHistoryItemClassic;
    if-eqz v0, :cond_c

    #@9
    .line 1462
    invoke-virtual {v0, p1, p2}, Landroid/webkit/WebHistoryItemClassic;->setTouchIconUrl(Ljava/lang/String;Z)V

    #@c
    .line 1466
    :cond_c
    iget-object v1, p0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@e
    if-nez v1, :cond_11

    #@10
    .line 1471
    :goto_10
    return-void

    #@11
    .line 1469
    :cond_11
    const/16 v3, 0x84

    #@13
    if-eqz p2, :cond_1e

    #@15
    const/4 v1, 0x1

    #@16
    :goto_16
    invoke-virtual {p0, v3, v1, v2, p1}, Landroid/webkit/CallbackProxy;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {p0, v1}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@1d
    goto :goto_10

    #@1e
    :cond_1e
    move v1, v2

    #@1f
    goto :goto_16
.end method

.method public onRequestFocus()V
    .registers 2

    #@0
    .prologue
    .line 1426
    iget-object v0, p0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 1431
    :goto_4
    return-void

    #@5
    .line 1430
    :cond_5
    const/16 v0, 0x7a

    #@7
    invoke-virtual {p0, v0}, Landroid/webkit/CallbackProxy;->sendEmptyMessage(I)Z

    #@a
    goto :goto_4
.end method

.method public onSavePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)Z
    .registers 8
    .parameter "schemePlusHost"
    .parameter "username"
    .parameter "password"
    .parameter "resumeMsg"

    #@0
    .prologue
    .line 1349
    const/16 v2, 0xc8

    #@2
    invoke-virtual {p0, v2}, Landroid/webkit/CallbackProxy;->obtainMessage(I)Landroid/os/Message;

    #@5
    move-result-object p4

    #@6
    .line 1351
    const/16 v2, 0x6f

    #@8
    invoke-virtual {p0, v2, p4}, Landroid/webkit/CallbackProxy;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@b
    move-result-object v1

    #@c
    .line 1352
    .local v1, msg:Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@f
    move-result-object v0

    #@10
    .line 1353
    .local v0, bundle:Landroid/os/Bundle;
    const-string v2, "host"

    #@12
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@15
    .line 1354
    const-string/jumbo v2, "username"

    #@18
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@1b
    .line 1355
    const-string/jumbo v2, "password"

    #@1e
    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@21
    .line 1356
    invoke-direct {p0, v1}, Landroid/webkit/CallbackProxy;->sendMessageToUiThreadSync(Landroid/os/Message;)V

    #@24
    .line 1358
    const/4 v2, 0x0

    #@25
    return v2
.end method

.method public onScaleChanged(FF)V
    .registers 6
    .parameter "oldScale"
    .parameter "newScale"

    #@0
    .prologue
    .line 1278
    iget-object v2, p0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@2
    if-nez v2, :cond_5

    #@4
    .line 1286
    :goto_4
    return-void

    #@5
    .line 1281
    :cond_5
    const/16 v2, 0x7b

    #@7
    invoke-virtual {p0, v2}, Landroid/webkit/CallbackProxy;->obtainMessage(I)Landroid/os/Message;

    #@a
    move-result-object v1

    #@b
    .line 1282
    .local v1, msg:Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@e
    move-result-object v0

    #@f
    .line 1283
    .local v0, bundle:Landroid/os/Bundle;
    const-string/jumbo v2, "old"

    #@12
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    #@15
    .line 1284
    const-string/jumbo v2, "new"

    #@18
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    #@1b
    .line 1285
    invoke-virtual {p0, v1}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@1e
    goto :goto_4
.end method

.method public onTooManyRedirects(Landroid/os/Message;Landroid/os/Message;)V
    .registers 3
    .parameter "cancelMsg"
    .parameter "continueMsg"

    #@0
    .prologue
    .line 1135
    return-void
.end method

.method public onUnhandledKeyEvent(Landroid/view/KeyEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 1269
    iget-object v0, p0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 1273
    :goto_4
    return-void

    #@5
    .line 1272
    :cond_5
    const/16 v0, 0x74

    #@7
    invoke-virtual {p0, v0, p1}, Landroid/webkit/CallbackProxy;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p0, v0}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@e
    goto :goto_4
.end method

.method openColorPicker(Ljava/lang/String;)V
    .registers 5
    .parameter "value"

    #@0
    .prologue
    .line 1811
    iget-object v1, p0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 1817
    :goto_4
    return-void

    #@5
    .line 1814
    :cond_5
    const/16 v1, 0x92

    #@7
    invoke-virtual {p0, v1}, Landroid/webkit/CallbackProxy;->obtainMessage(I)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    .line 1815
    .local v0, msg:Landroid/os/Message;
    new-instance v1, Landroid/webkit/CallbackProxy$PickerMessageData;

    #@d
    const/4 v2, 0x0

    #@e
    invoke-direct {v1, v2, p1}, Landroid/webkit/CallbackProxy$PickerMessageData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@13
    .line 1816
    invoke-virtual {p0, v0}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@16
    goto :goto_4
.end method

.method openDateTimePicker(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "type"
    .parameter "value"

    #@0
    .prologue
    .line 1797
    iget-object v1, p0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 1803
    :goto_4
    return-void

    #@5
    .line 1800
    :cond_5
    const/16 v1, 0x91

    #@7
    invoke-virtual {p0, v1}, Landroid/webkit/CallbackProxy;->obtainMessage(I)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    .line 1801
    .local v0, msg:Landroid/os/Message;
    new-instance v1, Landroid/webkit/CallbackProxy$PickerMessageData;

    #@d
    invoke-direct {v1, p1, p2}, Landroid/webkit/CallbackProxy$PickerMessageData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@10
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@12
    .line 1802
    invoke-virtual {p0, v0}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@15
    goto :goto_4
.end method

.method openFileChooser(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .registers 8
    .parameter "acceptType"
    .parameter "capture"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1725
    iget-object v4, p0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@3
    if-nez v4, :cond_6

    #@5
    .line 1733
    :goto_5
    return-object v3

    #@6
    .line 1728
    :cond_6
    const/16 v4, 0x86

    #@8
    invoke-virtual {p0, v4}, Landroid/webkit/CallbackProxy;->obtainMessage(I)Landroid/os/Message;

    #@b
    move-result-object v1

    #@c
    .line 1729
    .local v1, myMessage:Landroid/os/Message;
    new-instance v2, Landroid/webkit/CallbackProxy$UploadFile;

    #@e
    invoke-direct {v2, p0, v3}, Landroid/webkit/CallbackProxy$UploadFile;-><init>(Landroid/webkit/CallbackProxy;Landroid/webkit/CallbackProxy$1;)V

    #@11
    .line 1730
    .local v2, uploadFile:Landroid/webkit/CallbackProxy$UploadFile;
    new-instance v0, Landroid/webkit/CallbackProxy$UploadFileMessageData;

    #@13
    invoke-direct {v0, v2, p1, p2}, Landroid/webkit/CallbackProxy$UploadFileMessageData;-><init>(Landroid/webkit/CallbackProxy$UploadFile;Ljava/lang/String;Ljava/lang/String;)V

    #@16
    .line 1731
    .local v0, data:Landroid/webkit/CallbackProxy$UploadFileMessageData;
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@18
    .line 1732
    invoke-direct {p0, v1}, Landroid/webkit/CallbackProxy;->sendMessageToUiThreadSync(Landroid/os/Message;)V

    #@1b
    .line 1733
    invoke-virtual {v2}, Landroid/webkit/CallbackProxy$UploadFile;->getResult()Landroid/net/Uri;

    #@1e
    move-result-object v3

    #@1f
    goto :goto_5
.end method

.method public setDownloadListener(Landroid/webkit/DownloadListener;)V
    .registers 2
    .parameter "client"

    #@0
    .prologue
    .line 256
    iput-object p1, p0, Landroid/webkit/CallbackProxy;->mDownloadListener:Landroid/webkit/DownloadListener;

    #@2
    .line 257
    return-void
.end method

.method public setDownloadListenerExt(Landroid/webkit/DownloadListenerExt;)V
    .registers 2
    .parameter "client"

    #@0
    .prologue
    .line 264
    iput-object p1, p0, Landroid/webkit/CallbackProxy;->mDownloadListenerExt:Landroid/webkit/DownloadListenerExt;

    #@2
    .line 265
    return-void
.end method

.method setWebBackForwardListClient(Landroid/webkit/WebBackForwardListClient;)V
    .registers 2
    .parameter "client"

    #@0
    .prologue
    .line 277
    iput-object p1, p0, Landroid/webkit/CallbackProxy;->mWebBackForwardListClient:Landroid/webkit/WebBackForwardListClient;

    #@2
    .line 278
    return-void
.end method

.method public setWebChromeClient(Landroid/webkit/WebChromeClient;)V
    .registers 2
    .parameter "client"

    #@0
    .prologue
    .line 240
    iput-object p1, p0, Landroid/webkit/CallbackProxy;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    #@2
    .line 241
    return-void
.end method

.method public setWebViewClient(Landroid/webkit/WebViewClient;)V
    .registers 2
    .parameter "client"

    #@0
    .prologue
    .line 224
    iput-object p1, p0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@2
    .line 225
    return-void
.end method

.method shouldInterceptRequest(Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .registers 5
    .parameter "url"

    #@0
    .prologue
    .line 1254
    iget-object v1, p0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@2
    if-nez v1, :cond_6

    #@4
    .line 1255
    const/4 v0, 0x0

    #@5
    .line 1263
    :cond_5
    :goto_5
    return-object v0

    #@6
    .line 1258
    :cond_6
    iget-object v1, p0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@8
    iget-object v2, p0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@a
    invoke-virtual {v2}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v1, v2, p1}, Landroid/webkit/WebViewClient;->shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    #@11
    move-result-object v0

    #@12
    .line 1260
    .local v0, r:Landroid/webkit/WebResourceResponse;
    if-nez v0, :cond_5

    #@14
    .line 1261
    const/16 v1, 0x6c

    #@16
    invoke-virtual {p0, v1, p1}, Landroid/webkit/CallbackProxy;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {p0, v1}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@1d
    goto :goto_5
.end method

.method public shouldOverrideUrlLoading(Ljava/lang/String;)Z
    .registers 7
    .parameter "url"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1174
    new-instance v1, Landroid/webkit/CallbackProxy$ResultTransport;

    #@3
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@6
    move-result-object v2

    #@7
    invoke-direct {v1, v2}, Landroid/webkit/CallbackProxy$ResultTransport;-><init>(Ljava/lang/Object;)V

    #@a
    .line 1175
    .local v1, res:Landroid/webkit/CallbackProxy$ResultTransport;,"Landroid/webkit/CallbackProxy$ResultTransport<Ljava/lang/Boolean;>;"
    const/16 v2, 0x67

    #@c
    invoke-virtual {p0, v2}, Landroid/webkit/CallbackProxy;->obtainMessage(I)Landroid/os/Message;

    #@f
    move-result-object v0

    #@10
    .line 1176
    .local v0, msg:Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@13
    move-result-object v2

    #@14
    const-string/jumbo v3, "url"

    #@17
    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@1a
    .line 1177
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1c
    .line 1179
    const/4 v2, 0x1

    #@1d
    iput-boolean v2, p0, Landroid/webkit/CallbackProxy;->mWaitForUiThreadSync:Z

    #@1f
    .line 1180
    invoke-direct {p0, v0}, Landroid/webkit/CallbackProxy;->sendMessageToUiThreadSync(Landroid/os/Message;)V

    #@22
    .line 1182
    iput-boolean v4, p0, Landroid/webkit/CallbackProxy;->mWaitForUiThreadSync:Z

    #@24
    .line 1183
    invoke-virtual {v1}, Landroid/webkit/CallbackProxy$ResultTransport;->getResult()Ljava/lang/Object;

    #@27
    move-result-object v2

    #@28
    check-cast v2, Ljava/lang/Boolean;

    #@2a
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    #@2d
    move-result v2

    #@2e
    return v2
.end method

.method protected shutdown()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 214
    invoke-virtual {p0, v0}, Landroid/webkit/CallbackProxy;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    #@4
    .line 215
    invoke-virtual {p0, v0}, Landroid/webkit/CallbackProxy;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    #@7
    .line 216
    invoke-virtual {p0, v0}, Landroid/webkit/CallbackProxy;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    #@a
    .line 217
    return-void
.end method

.method switchOutDrawHistory()V
    .registers 2

    #@0
    .prologue
    .line 1023
    const/16 v0, 0x7d

    #@2
    invoke-virtual {p0, v0}, Landroid/webkit/CallbackProxy;->obtainMessage(I)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Landroid/webkit/CallbackProxy;->sendMessage(Landroid/os/Message;)Z

    #@9
    .line 1024
    return-void
.end method

.method public uiOverrideKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 320
    iget-object v0, p0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@2
    if-eqz v0, :cond_11

    #@4
    .line 321
    iget-object v0, p0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@6
    iget-object v1, p0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@8
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v0, v1, p1}, Landroid/webkit/WebViewClient;->shouldOverrideKeyEvent(Landroid/webkit/WebView;Landroid/view/KeyEvent;)Z

    #@f
    move-result v0

    #@10
    .line 323
    :goto_10
    return v0

    #@11
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_10
.end method

.method public uiOverrideUrlLoading(Ljava/lang/String;)Z
    .registers 6
    .parameter "overrideUrl"

    #@0
    .prologue
    .line 289
    if-eqz p1, :cond_8

    #@2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@5
    move-result v2

    #@6
    if-nez v2, :cond_a

    #@8
    .line 290
    :cond_8
    const/4 v1, 0x0

    #@9
    .line 313
    :goto_9
    return v1

    #@a
    .line 292
    :cond_a
    const/4 v1, 0x0

    #@b
    .line 293
    .local v1, override:Z
    iget-object v2, p0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@d
    if-eqz v2, :cond_1c

    #@f
    .line 294
    iget-object v2, p0, Landroid/webkit/CallbackProxy;->mWebViewClient:Landroid/webkit/WebViewClient;

    #@11
    iget-object v3, p0, Landroid/webkit/CallbackProxy;->mWebView:Landroid/webkit/WebViewClassic;

    #@13
    invoke-virtual {v3}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v2, v3, p1}, Landroid/webkit/WebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    #@1a
    move-result v1

    #@1b
    goto :goto_9

    #@1c
    .line 297
    :cond_1c
    new-instance v0, Landroid/content/Intent;

    #@1e
    const-string v2, "android.intent.action.VIEW"

    #@20
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@23
    move-result-object v3

    #@24
    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@27
    .line 299
    .local v0, intent:Landroid/content/Intent;
    const-string v2, "android.intent.category.BROWSABLE"

    #@29
    invoke-virtual {v0, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    #@2c
    .line 303
    const-string v2, "com.android.browser.application_id"

    #@2e
    iget-object v3, p0, Landroid/webkit/CallbackProxy;->mContext:Landroid/content/Context;

    #@30
    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@37
    .line 306
    :try_start_37
    iget-object v2, p0, Landroid/webkit/CallbackProxy;->mContext:Landroid/content/Context;

    #@39
    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_3c
    .catch Landroid/content/ActivityNotFoundException; {:try_start_37 .. :try_end_3c} :catch_3e

    #@3c
    .line 307
    const/4 v1, 0x1

    #@3d
    goto :goto_9

    #@3e
    .line 308
    :catch_3e
    move-exception v2

    #@3f
    goto :goto_9
.end method

.method public waitForUiThreadSync()Z
    .registers 2

    #@0
    .prologue
    .line 1822
    iget-boolean v0, p0, Landroid/webkit/CallbackProxy;->mWaitForUiThreadSync:Z

    #@2
    return v0
.end method
