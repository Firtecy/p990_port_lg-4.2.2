.class Landroid/webkit/WebBackForwardListClassic;
.super Landroid/webkit/WebBackForwardList;
.source "WebBackForwardListClassic.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Ljava/io/Serializable;


# instance fields
.field private mArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/webkit/WebHistoryItemClassic;",
            ">;"
        }
    .end annotation
.end field

.field private final mCallbackProxy:Landroid/webkit/CallbackProxy;

.field private mClearPending:Z

.field private mCurrentIndex:I


# direct methods
.method constructor <init>(Landroid/webkit/CallbackProxy;)V
    .registers 3
    .parameter "proxy"

    #@0
    .prologue
    .line 34
    invoke-direct {p0}, Landroid/webkit/WebBackForwardList;-><init>()V

    #@3
    .line 35
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/webkit/WebBackForwardListClassic;->mCurrentIndex:I

    #@6
    .line 36
    new-instance v0, Ljava/util/ArrayList;

    #@8
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@b
    iput-object v0, p0, Landroid/webkit/WebBackForwardListClassic;->mArray:Ljava/util/ArrayList;

    #@d
    .line 37
    iput-object p1, p0, Landroid/webkit/WebBackForwardListClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@f
    .line 38
    return-void
.end method

.method private static native nativeClose(I)V
.end method

.method private declared-synchronized removeHistoryItem(I)V
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 124
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v1, p0, Landroid/webkit/WebBackForwardListClassic;->mArray:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Landroid/webkit/WebHistoryItem;

    #@9
    .line 127
    .local v0, h:Landroid/webkit/WebHistoryItem;
    iget v1, p0, Landroid/webkit/WebBackForwardListClassic;->mCurrentIndex:I

    #@b
    add-int/lit8 v1, v1, -0x1

    #@d
    iput v1, p0, Landroid/webkit/WebBackForwardListClassic;->mCurrentIndex:I
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_11

    #@f
    .line 128
    monitor-exit p0

    #@10
    return-void

    #@11
    .line 124
    .end local v0           #h:Landroid/webkit/WebHistoryItem;
    :catchall_11
    move-exception v1

    #@12
    monitor-exit p0

    #@13
    throw v1
.end method

.method static synchronized native declared-synchronized restoreIndex(II)V
.end method


# virtual methods
.method declared-synchronized addHistoryItem(Landroid/webkit/WebHistoryItem;)V
    .registers 8
    .parameter "item"

    #@0
    .prologue
    .line 85
    monitor-enter p0

    #@1
    :try_start_1
    iget v4, p0, Landroid/webkit/WebBackForwardListClassic;->mCurrentIndex:I

    #@3
    add-int/lit8 v4, v4, 0x1

    #@5
    iput v4, p0, Landroid/webkit/WebBackForwardListClassic;->mCurrentIndex:I

    #@7
    .line 88
    iget-object v4, p0, Landroid/webkit/WebBackForwardListClassic;->mArray:Ljava/util/ArrayList;

    #@9
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@c
    move-result v3

    #@d
    .line 89
    .local v3, size:I
    iget v2, p0, Landroid/webkit/WebBackForwardListClassic;->mCurrentIndex:I

    #@f
    .line 90
    .local v2, newPos:I
    if-eq v2, v3, :cond_20

    #@11
    .line 91
    add-int/lit8 v1, v3, -0x1

    #@13
    .local v1, i:I
    :goto_13
    if-lt v1, v2, :cond_20

    #@15
    .line 92
    iget-object v4, p0, Landroid/webkit/WebBackForwardListClassic;->mArray:Ljava/util/ArrayList;

    #@17
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@1a
    move-result-object v4

    #@1b
    check-cast v4, Landroid/webkit/WebHistoryItem;

    #@1d
    .line 91
    add-int/lit8 v1, v1, -0x1

    #@1f
    goto :goto_13

    #@20
    .line 96
    .end local v1           #i:I
    :cond_20
    iget-object v5, p0, Landroid/webkit/WebBackForwardListClassic;->mArray:Ljava/util/ArrayList;

    #@22
    move-object v0, p1

    #@23
    check-cast v0, Landroid/webkit/WebHistoryItemClassic;

    #@25
    move-object v4, v0

    #@26
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@29
    .line 97
    iget-object v4, p0, Landroid/webkit/WebBackForwardListClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2b
    if-eqz v4, :cond_32

    #@2d
    .line 98
    iget-object v4, p0, Landroid/webkit/WebBackForwardListClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2f
    invoke-virtual {v4, p1}, Landroid/webkit/CallbackProxy;->onNewHistoryItem(Landroid/webkit/WebHistoryItem;)V
    :try_end_32
    .catchall {:try_start_1 .. :try_end_32} :catchall_34

    #@32
    .line 100
    :cond_32
    monitor-exit p0

    #@33
    return-void

    #@34
    .line 85
    .end local v2           #newPos:I
    .end local v3           #size:I
    :catchall_34
    move-exception v4

    #@35
    monitor-exit p0

    #@36
    throw v4
.end method

.method public bridge synthetic clone()Landroid/webkit/WebBackForwardList;
    .registers 2

    #@0
    .prologue
    .line 22
    invoke-virtual {p0}, Landroid/webkit/WebBackForwardListClassic;->clone()Landroid/webkit/WebBackForwardListClassic;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public declared-synchronized clone()Landroid/webkit/WebBackForwardListClassic;
    .registers 6

    #@0
    .prologue
    .line 131
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v1, Landroid/webkit/WebBackForwardListClassic;

    #@3
    const/4 v3, 0x0

    #@4
    invoke-direct {v1, v3}, Landroid/webkit/WebBackForwardListClassic;-><init>(Landroid/webkit/CallbackProxy;)V

    #@7
    .line 132
    .local v1, l:Landroid/webkit/WebBackForwardListClassic;
    iget-boolean v3, p0, Landroid/webkit/WebBackForwardListClassic;->mClearPending:Z

    #@9
    if-eqz v3, :cond_14

    #@b
    .line 134
    invoke-virtual {p0}, Landroid/webkit/WebBackForwardListClassic;->getCurrentItem()Landroid/webkit/WebHistoryItemClassic;

    #@e
    move-result-object v3

    #@f
    invoke-virtual {v1, v3}, Landroid/webkit/WebBackForwardListClassic;->addHistoryItem(Landroid/webkit/WebHistoryItem;)V
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_3a

    #@12
    .line 144
    :cond_12
    monitor-exit p0

    #@13
    return-object v1

    #@14
    .line 137
    :cond_14
    :try_start_14
    iget v3, p0, Landroid/webkit/WebBackForwardListClassic;->mCurrentIndex:I

    #@16
    iput v3, v1, Landroid/webkit/WebBackForwardListClassic;->mCurrentIndex:I

    #@18
    .line 138
    invoke-virtual {p0}, Landroid/webkit/WebBackForwardListClassic;->getSize()I

    #@1b
    move-result v2

    #@1c
    .line 139
    .local v2, size:I
    new-instance v3, Ljava/util/ArrayList;

    #@1e
    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    #@21
    iput-object v3, v1, Landroid/webkit/WebBackForwardListClassic;->mArray:Ljava/util/ArrayList;

    #@23
    .line 140
    const/4 v0, 0x0

    #@24
    .local v0, i:I
    :goto_24
    if-ge v0, v2, :cond_12

    #@26
    .line 142
    iget-object v4, v1, Landroid/webkit/WebBackForwardListClassic;->mArray:Ljava/util/ArrayList;

    #@28
    iget-object v3, p0, Landroid/webkit/WebBackForwardListClassic;->mArray:Ljava/util/ArrayList;

    #@2a
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2d
    move-result-object v3

    #@2e
    check-cast v3, Landroid/webkit/WebHistoryItemClassic;

    #@30
    invoke-virtual {v3}, Landroid/webkit/WebHistoryItemClassic;->clone()Landroid/webkit/WebHistoryItemClassic;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_37
    .catchall {:try_start_14 .. :try_end_37} :catchall_3a

    #@37
    .line 140
    add-int/lit8 v0, v0, 0x1

    #@39
    goto :goto_24

    #@3a
    .line 131
    .end local v0           #i:I
    .end local v1           #l:Landroid/webkit/WebBackForwardListClassic;
    .end local v2           #size:I
    :catchall_3a
    move-exception v3

    #@3b
    monitor-exit p0

    #@3c
    throw v3
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 22
    invoke-virtual {p0}, Landroid/webkit/WebBackForwardListClassic;->clone()Landroid/webkit/WebBackForwardListClassic;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method declared-synchronized close(I)V
    .registers 3
    .parameter "nativeFrame"

    #@0
    .prologue
    .line 108
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/WebBackForwardListClassic;->mArray:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@6
    .line 109
    const/4 v0, -0x1

    #@7
    iput v0, p0, Landroid/webkit/WebBackForwardListClassic;->mCurrentIndex:I

    #@9
    .line 110
    invoke-static {p1}, Landroid/webkit/WebBackForwardListClassic;->nativeClose(I)V

    #@c
    .line 112
    const/4 v0, 0x0

    #@d
    iput-boolean v0, p0, Landroid/webkit/WebBackForwardListClassic;->mClearPending:Z
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_11

    #@f
    .line 113
    monitor-exit p0

    #@10
    return-void

    #@11
    .line 108
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0

    #@13
    throw v0
.end method

.method declared-synchronized getClearPending()Z
    .registers 2

    #@0
    .prologue
    .line 72
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/webkit/WebBackForwardListClassic;->mClearPending:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getCurrentIndex()I
    .registers 2

    #@0
    .prologue
    .line 45
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/webkit/WebBackForwardListClassic;->mCurrentIndex:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public bridge synthetic getCurrentItem()Landroid/webkit/WebHistoryItem;
    .registers 2

    #@0
    .prologue
    .line 22
    invoke-virtual {p0}, Landroid/webkit/WebBackForwardListClassic;->getCurrentItem()Landroid/webkit/WebHistoryItemClassic;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public declared-synchronized getCurrentItem()Landroid/webkit/WebHistoryItemClassic;
    .registers 2

    #@0
    .prologue
    .line 41
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/webkit/WebBackForwardListClassic;->mCurrentIndex:I

    #@3
    invoke-virtual {p0, v0}, Landroid/webkit/WebBackForwardListClassic;->getItemAtIndex(I)Landroid/webkit/WebHistoryItemClassic;
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_9

    #@6
    move-result-object v0

    #@7
    monitor-exit p0

    #@8
    return-object v0

    #@9
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method public bridge synthetic getItemAtIndex(I)Landroid/webkit/WebHistoryItem;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 22
    invoke-virtual {p0, p1}, Landroid/webkit/WebBackForwardListClassic;->getItemAtIndex(I)Landroid/webkit/WebHistoryItemClassic;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public declared-synchronized getItemAtIndex(I)Landroid/webkit/WebHistoryItemClassic;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 49
    monitor-enter p0

    #@1
    if-ltz p1, :cond_9

    #@3
    :try_start_3
    invoke-virtual {p0}, Landroid/webkit/WebBackForwardListClassic;->getSize()I
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_15

    #@6
    move-result v0

    #@7
    if-lt p1, v0, :cond_c

    #@9
    .line 50
    :cond_9
    const/4 v0, 0x0

    #@a
    .line 52
    :goto_a
    monitor-exit p0

    #@b
    return-object v0

    #@c
    :cond_c
    :try_start_c
    iget-object v0, p0, Landroid/webkit/WebBackForwardListClassic;->mArray:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Landroid/webkit/WebHistoryItemClassic;
    :try_end_14
    .catchall {:try_start_c .. :try_end_14} :catchall_15

    #@14
    goto :goto_a

    #@15
    .line 49
    :catchall_15
    move-exception v0

    #@16
    monitor-exit p0

    #@17
    throw v0
.end method

.method public declared-synchronized getSize()I
    .registers 2

    #@0
    .prologue
    .line 56
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/WebBackForwardListClassic;->mArray:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_9

    #@6
    move-result v0

    #@7
    monitor-exit p0

    #@8
    return v0

    #@9
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method declared-synchronized setClearPending()V
    .registers 2

    #@0
    .prologue
    .line 64
    monitor-enter p0

    #@1
    const/4 v0, 0x1

    #@2
    :try_start_2
    iput-boolean v0, p0, Landroid/webkit/WebBackForwardListClassic;->mClearPending:Z
    :try_end_4
    .catchall {:try_start_2 .. :try_end_4} :catchall_6

    #@4
    .line 65
    monitor-exit p0

    #@5
    return-void

    #@6
    .line 64
    :catchall_6
    move-exception v0

    #@7
    monitor-exit p0

    #@8
    throw v0
.end method

.method declared-synchronized setCurrentIndex(I)V
    .registers 4
    .parameter "newIndex"

    #@0
    .prologue
    .line 152
    monitor-enter p0

    #@1
    :try_start_1
    iput p1, p0, Landroid/webkit/WebBackForwardListClassic;->mCurrentIndex:I

    #@3
    .line 153
    iget-object v0, p0, Landroid/webkit/WebBackForwardListClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@5
    if-eqz v0, :cond_10

    #@7
    .line 154
    iget-object v0, p0, Landroid/webkit/WebBackForwardListClassic;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@9
    invoke-virtual {p0, p1}, Landroid/webkit/WebBackForwardListClassic;->getItemAtIndex(I)Landroid/webkit/WebHistoryItemClassic;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v0, v1, p1}, Landroid/webkit/CallbackProxy;->onIndexChanged(Landroid/webkit/WebHistoryItem;I)V
    :try_end_10
    .catchall {:try_start_1 .. :try_end_10} :catchall_12

    #@10
    .line 156
    :cond_10
    monitor-exit p0

    #@11
    return-void

    #@12
    .line 152
    :catchall_12
    move-exception v0

    #@13
    monitor-exit p0

    #@14
    throw v0
.end method
