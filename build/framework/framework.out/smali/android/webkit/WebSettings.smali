.class public abstract Landroid/webkit/WebSettings;
.super Ljava/lang/Object;
.source "WebSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/WebSettings$PluginState;,
        Landroid/webkit/WebSettings$RenderPriority;,
        Landroid/webkit/WebSettings$ZoomDensity;,
        Landroid/webkit/WebSettings$TextSize;,
        Landroid/webkit/WebSettings$LayoutAlgorithm;
    }
.end annotation


# static fields
.field public static final LOAD_CACHE_ELSE_NETWORK:I = 0x1

.field public static final LOAD_CACHE_ONLY:I = 0x3

.field public static final LOAD_DEFAULT:I = -0x1

.field public static final LOAD_NORMAL:I = 0x0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final LOAD_NO_CACHE:I = 0x2


# direct methods
.method protected constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 159
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 160
    return-void
.end method

.method public static getDefaultUserAgent(Landroid/content/Context;)Ljava/lang/String;
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 1267
    invoke-static {}, Landroid/webkit/WebViewFactory;->getProvider()Landroid/webkit/WebViewFactoryProvider;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0}, Landroid/webkit/WebViewFactoryProvider;->getStatics()Landroid/webkit/WebViewFactoryProvider$Statics;

    #@7
    move-result-object v0

    #@8
    invoke-interface {v0, p0}, Landroid/webkit/WebViewFactoryProvider$Statics;->getDefaultUserAgent(Landroid/content/Context;)Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method


# virtual methods
.method public addAdditionalHeader(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 1386
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public enableSmoothTransition()Z
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 361
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public getAllowContentAccess()Z
    .registers 2

    #@0
    .prologue
    .line 316
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public getAllowFileAccess()Z
    .registers 2

    #@0
    .prologue
    .line 298
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public abstract getAllowFileAccessFromFileURLs()Z
.end method

.method public abstract getAllowUniversalAccessFromFileURLs()Z
.end method

.method public declared-synchronized getBlockNetworkImage()Z
    .registers 2

    #@0
    .prologue
    .line 888
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getBlockNetworkLoads()Z
    .registers 2

    #@0
    .prologue
    .line 920
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public getBuiltInZoomControls()Z
    .registers 2

    #@0
    .prologue
    .line 256
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public getCacheMode()I
    .registers 2

    #@0
    .prologue
    .line 1315
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public declared-synchronized getCliptrayEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 1500
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getCursiveFontFamily()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 734
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getDatabaseEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 1120
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getDatabasePath()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1110
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getDefaultFixedFontSize()I
    .registers 2

    #@0
    .prologue
    .line 833
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getDefaultFontSize()I
    .registers 2

    #@0
    .prologue
    .line 813
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getDefaultTextEncodingName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1238
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public getDefaultZoom()Landroid/webkit/WebSettings$ZoomDensity;
    .registers 2

    #@0
    .prologue
    .line 499
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public getDisplayZoomControls()Z
    .registers 2

    #@0
    .prologue
    .line 279
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public declared-synchronized getDomStorageEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 1101
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getFantasyFontFamily()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 753
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getFixedFontFamily()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 677
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getFloatingMode()Z
    .registers 2

    #@0
    .prologue
    .line 1420
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getForceZoomoutEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 1334
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getJavaScriptCanOpenWindowsAutomatically()Z
    .registers 2

    #@0
    .prologue
    .line 1219
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getJavaScriptEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 1141
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getLGBubbleActionEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 1356
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getLayoutAlgorithm()Landroid/webkit/WebSettings$LayoutAlgorithm;
    .registers 2

    #@0
    .prologue
    .line 639
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public getLightTouchEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 517
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public getLoadWithOverviewMode()Z
    .registers 2

    #@0
    .prologue
    .line 334
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public declared-synchronized getLoadsImagesAutomatically()Z
    .registers 2

    #@0
    .prologue
    .line 859
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public getMediaPlaybackRequiresUserGesture()Z
    .registers 2

    #@0
    .prologue
    .line 227
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public declared-synchronized getMinimumFontSize()I
    .registers 2

    #@0
    .prologue
    .line 773
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getMinimumLogicalFontSize()I
    .registers 2

    #@0
    .prologue
    .line 793
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public getNavDump()Z
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 184
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public declared-synchronized getPluginState()Landroid/webkit/WebSettings$PluginState;
    .registers 2

    #@0
    .prologue
    .line 1184
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getPluginsEnabled()Z
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1174
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getPluginsPath()Ljava/lang/String;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1198
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, ""
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getSansSerifFontFamily()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 696
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public getSaveFormData()Z
    .registers 2

    #@0
    .prologue
    .line 406
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public getSavePassword()Z
    .registers 2

    #@0
    .prologue
    .line 423
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public declared-synchronized getScrollToTopBottomEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 1452
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getSerifFontFamily()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 715
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getStandardFontFamily()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 658
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getTextDragDropEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 1468
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getTextSize()Landroid/webkit/WebSettings$TextSize;
    .registers 10

    #@0
    .prologue
    .line 465
    monitor-enter p0

    #@1
    const/4 v1, 0x0

    #@2
    .line 466
    .local v1, closestSize:Landroid/webkit/WebSettings$TextSize;
    const v6, 0x7fffffff

    #@5
    .line 467
    .local v6, smallestDelta:I
    :try_start_5
    invoke-virtual {p0}, Landroid/webkit/WebSettings;->getTextZoom()I

    #@8
    move-result v7

    #@9
    .line 468
    .local v7, textSize:I
    invoke-static {}, Landroid/webkit/WebSettings$TextSize;->values()[Landroid/webkit/WebSettings$TextSize;

    #@c
    move-result-object v0

    #@d
    .local v0, arr$:[Landroid/webkit/WebSettings$TextSize;
    array-length v4, v0

    #@e
    .local v4, len$:I
    const/4 v3, 0x0

    #@f
    .local v3, i$:I
    :goto_f
    if-ge v3, v4, :cond_26

    #@11
    aget-object v5, v0, v3

    #@13
    .line 469
    .local v5, size:Landroid/webkit/WebSettings$TextSize;
    iget v8, v5, Landroid/webkit/WebSettings$TextSize;->value:I

    #@15
    sub-int v8, v7, v8

    #@17
    invoke-static {v8}, Ljava/lang/Math;->abs(I)I
    :try_end_1a
    .catchall {:try_start_5 .. :try_end_1a} :catchall_2d

    #@1a
    move-result v2

    #@1b
    .line 470
    .local v2, delta:I
    if-nez v2, :cond_1f

    #@1d
    .line 478
    .end local v1           #closestSize:Landroid/webkit/WebSettings$TextSize;
    .end local v2           #delta:I
    .end local v5           #size:Landroid/webkit/WebSettings$TextSize;
    :goto_1d
    monitor-exit p0

    #@1e
    return-object v5

    #@1f
    .line 473
    .restart local v1       #closestSize:Landroid/webkit/WebSettings$TextSize;
    .restart local v2       #delta:I
    .restart local v5       #size:Landroid/webkit/WebSettings$TextSize;
    :cond_1f
    if-ge v2, v6, :cond_23

    #@21
    .line 474
    move v6, v2

    #@22
    .line 475
    move-object v1, v5

    #@23
    .line 468
    :cond_23
    add-int/lit8 v3, v3, 0x1

    #@25
    goto :goto_f

    #@26
    .line 478
    .end local v2           #delta:I
    .end local v5           #size:Landroid/webkit/WebSettings$TextSize;
    :cond_26
    if-eqz v1, :cond_2a

    #@28
    .end local v1           #closestSize:Landroid/webkit/WebSettings$TextSize;
    :goto_28
    move-object v5, v1

    #@29
    goto :goto_1d

    #@2a
    .restart local v1       #closestSize:Landroid/webkit/WebSettings$TextSize;
    :cond_2a
    :try_start_2a
    sget-object v1, Landroid/webkit/WebSettings$TextSize;->NORMAL:Landroid/webkit/WebSettings$TextSize;
    :try_end_2c
    .catchall {:try_start_2a .. :try_end_2c} :catchall_2d

    #@2c
    goto :goto_28

    #@2d
    .line 465
    .end local v0           #arr$:[Landroid/webkit/WebSettings$TextSize;
    .end local v3           #i$:I
    .end local v4           #len$:I
    .end local v7           #textSize:I
    :catchall_2d
    move-exception v8

    #@2e
    monitor-exit p0

    #@2f
    throw v8
.end method

.method public declared-synchronized getTextZoom()I
    .registers 2

    #@0
    .prologue
    .line 442
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getTranslateEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 1484
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getUAProfile()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1367
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getUseDoubleTree()Z
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 542
    monitor-enter p0

    #@1
    const/4 v0, 0x0

    #@2
    monitor-exit p0

    #@3
    return v0
.end method

.method public getUseWebViewBackgroundForOverscrollBackground()Z
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 387
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public declared-synchronized getUseWideViewPort()Z
    .registers 2

    #@0
    .prologue
    .line 598
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getUserAgent()I
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 579
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getUserAgentString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1256
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized getWebScrapEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 1436
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public setAllowContentAccess(Z)V
    .registers 3
    .parameter "allow"

    #@0
    .prologue
    .line 307
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public setAllowFileAccess(Z)V
    .registers 3
    .parameter "allow"

    #@0
    .prologue
    .line 289
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public abstract setAllowFileAccessFromFileURLs(Z)V
.end method

.method public abstract setAllowUniversalAccessFromFileURLs(Z)V
.end method

.method public declared-synchronized setAppCacheEnabled(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1044
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setAppCacheMaxSize(J)V
    .registers 4
    .parameter "appCacheMaxSize"

    #@0
    .prologue
    .line 1071
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setAppCachePath(Ljava/lang/String;)V
    .registers 3
    .parameter "appCachePath"

    #@0
    .prologue
    .line 1058
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setBlockNetworkImage(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 878
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setBlockNetworkLoads(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 910
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public setBuiltInZoomControls(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 246
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public setCacheMode(I)V
    .registers 3
    .parameter "mode"

    #@0
    .prologue
    .line 1305
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public declared-synchronized setCliptrayEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 1493
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setCursiveFontFamily(Ljava/lang/String;)V
    .registers 3
    .parameter "font"

    #@0
    .prologue
    .line 724
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setDatabaseEnabled(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1082
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setDatabasePath(Ljava/lang/String;)V
    .registers 3
    .parameter "databasePath"

    #@0
    .prologue
    .line 1019
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setDefaultFixedFontSize(I)V
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 823
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setDefaultFontSize(I)V
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 803
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setDefaultTextEncodingName(Ljava/lang/String;)V
    .registers 3
    .parameter "encoding"

    #@0
    .prologue
    .line 1228
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public setDefaultZoom(Landroid/webkit/WebSettings$ZoomDensity;)V
    .registers 3
    .parameter "zoom"

    #@0
    .prologue
    .line 488
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public setDisplayZoomControls(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 267
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public declared-synchronized setDomStorageEnabled(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1091
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public setEnableSmoothTransition(Z)V
    .registers 3
    .parameter "enable"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 348
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public declared-synchronized setFantasyFontFamily(Ljava/lang/String;)V
    .registers 3
    .parameter "font"

    #@0
    .prologue
    .line 743
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setFixedFontFamily(Ljava/lang/String;)V
    .registers 3
    .parameter "font"

    #@0
    .prologue
    .line 667
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setFloatingMode(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 1413
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setForceZoomoutEnabled(Z)V
    .registers 3
    .parameter "use"

    #@0
    .prologue
    .line 1325
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setGeolocationDatabasePath(Ljava/lang/String;)V
    .registers 3
    .parameter "databasePath"

    #@0
    .prologue
    .line 1032
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setGeolocationEnabled(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1131
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setJavaScriptCanOpenWindowsAutomatically(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1208
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setJavaScriptEnabled(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 930
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setLGBubbleActionEnabled(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1346
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setLayoutAlgorithm(Landroid/webkit/WebSettings$LayoutAlgorithm;)V
    .registers 3
    .parameter "l"

    #@0
    .prologue
    .line 629
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public setLightTouchEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 507
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public setLoadWithOverviewMode(Z)V
    .registers 3
    .parameter "overview"

    #@0
    .prologue
    .line 324
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public declared-synchronized setLoadsImagesAutomatically(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 848
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public setMediaPlaybackRequiresUserGesture(Z)V
    .registers 3
    .parameter "require"

    #@0
    .prologue
    .line 217
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public declared-synchronized setMinimumFontSize(I)V
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 763
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setMinimumLogicalFontSize(I)V
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 783
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public setNavDump(Z)V
    .registers 3
    .parameter "enabled"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 171
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public setNeedInitialFocus(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1278
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public declared-synchronized setPluginState(Landroid/webkit/WebSettings$PluginState;)V
    .registers 3
    .parameter "state"

    #@0
    .prologue
    .line 990
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setPluginsEnabled(Z)V
    .registers 3
    .parameter "flag"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 977
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setPluginsPath(Ljava/lang/String;)V
    .registers 2
    .parameter "pluginsPath"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1004
    monitor-enter p0

    #@1
    monitor-exit p0

    #@2
    return-void
.end method

.method public declared-synchronized setRenderPriority(Landroid/webkit/WebSettings$RenderPriority;)V
    .registers 3
    .parameter "priority"

    #@0
    .prologue
    .line 1289
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setSansSerifFontFamily(Ljava/lang/String;)V
    .registers 3
    .parameter "font"

    #@0
    .prologue
    .line 686
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public setSaveFormData(Z)V
    .registers 3
    .parameter "save"

    #@0
    .prologue
    .line 395
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public setSavePassword(Z)V
    .registers 3
    .parameter "save"

    #@0
    .prologue
    .line 413
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public declared-synchronized setScrollToTopBottomEnabled(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1445
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setSerifFontFamily(Ljava/lang/String;)V
    .registers 3
    .parameter "font"

    #@0
    .prologue
    .line 705
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setStandardFontFamily(Ljava/lang/String;)V
    .registers 3
    .parameter "font"

    #@0
    .prologue
    .line 648
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setSupportMultipleWindows(Z)V
    .registers 3
    .parameter "support"

    #@0
    .prologue
    .line 609
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public setSupportZoom(Z)V
    .registers 3
    .parameter "support"

    #@0
    .prologue
    .line 197
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public declared-synchronized setTextDragDropEnabled(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1461
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setTextSize(Landroid/webkit/WebSettings$TextSize;)V
    .registers 3
    .parameter "t"

    #@0
    .prologue
    .line 452
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p1, Landroid/webkit/WebSettings$TextSize;->value:I

    #@3
    invoke-virtual {p0, v0}, Landroid/webkit/WebSettings;->setTextZoom(I)V
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    #@6
    .line 453
    monitor-exit p0

    #@7
    return-void

    #@8
    .line 452
    :catchall_8
    move-exception v0

    #@9
    monitor-exit p0

    #@a
    throw v0
.end method

.method public declared-synchronized setTextZoom(I)V
    .registers 3
    .parameter "textZoom"

    #@0
    .prologue
    .line 432
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setTranslateEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 1477
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setUAProfile(Ljava/lang/String;)V
    .registers 3
    .parameter "uaProf"

    #@0
    .prologue
    .line 1376
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setUseDoubleTree(Z)V
    .registers 2
    .parameter "use"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 530
    monitor-enter p0

    #@1
    monitor-exit p0

    #@2
    return-void
.end method

.method public setUseWebViewBackgroundForOverscrollBackground(Z)V
    .registers 3
    .parameter "view"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 374
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method

.method public declared-synchronized setUseWideViewPort(Z)V
    .registers 3
    .parameter "use"

    #@0
    .prologue
    .line 588
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setUserAgent(I)V
    .registers 3
    .parameter "ua"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 560
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setUserAgentString(Ljava/lang/String;)V
    .registers 3
    .parameter "ua"

    #@0
    .prologue
    .line 1246
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setWOFFEnabled(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1395
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setWebGLEnabled(Z)V
    .registers 3
    .parameter "flag"

    #@0
    .prologue
    .line 1404
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized setWebScrapEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 1429
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized supportMultipleWindows()Z
    .registers 2

    #@0
    .prologue
    .line 619
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@3
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@6
    throw v0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_7

    #@7
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public supportZoom()Z
    .registers 2

    #@0
    .prologue
    .line 207
    new-instance v0, Landroid/webkit/MustOverrideException;

    #@2
    invoke-direct {v0}, Landroid/webkit/MustOverrideException;-><init>()V

    #@5
    throw v0
.end method
