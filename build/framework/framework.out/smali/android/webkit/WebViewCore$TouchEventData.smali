.class Landroid/webkit/WebViewCore$TouchEventData;
.super Ljava/lang/Object;
.source "WebViewCore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/WebViewCore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TouchEventData"
.end annotation


# instance fields
.field mAction:I

.field mActionIndex:I

.field mIds:[I

.field mMetaState:I

.field mMotionEvent:Landroid/view/MotionEvent;

.field mNativeLayer:I

.field mNativeLayerRect:Landroid/graphics/Rect;

.field mNativeResult:Z

.field mPoints:[Landroid/graphics/Point;

.field mPointsInView:[Landroid/graphics/Point;

.field mReprocess:Z

.field mSequence:J


# direct methods
.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 1016
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1026
    new-instance v0, Landroid/graphics/Rect;

    #@5
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@8
    iput-object v0, p0, Landroid/webkit/WebViewCore$TouchEventData;->mNativeLayerRect:Landroid/graphics/Rect;

    #@a
    return-void
.end method
