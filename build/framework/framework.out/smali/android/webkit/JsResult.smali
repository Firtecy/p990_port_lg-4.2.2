.class public Landroid/webkit/JsResult;
.super Ljava/lang/Object;
.source "JsResult.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/JsResult$ResultReceiver;
    }
.end annotation


# instance fields
.field private final mReceiver:Landroid/webkit/JsResult$ResultReceiver;

.field private mResult:Z


# direct methods
.method public constructor <init>(Landroid/webkit/JsResult$ResultReceiver;)V
    .registers 2
    .parameter "receiver"

    #@0
    .prologue
    .line 57
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 58
    iput-object p1, p0, Landroid/webkit/JsResult;->mReceiver:Landroid/webkit/JsResult$ResultReceiver;

    #@5
    .line 59
    return-void
.end method

.method private final wakeUp()V
    .registers 2

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Landroid/webkit/JsResult;->mReceiver:Landroid/webkit/JsResult$ResultReceiver;

    #@2
    invoke-interface {v0, p0}, Landroid/webkit/JsResult$ResultReceiver;->onJsResultComplete(Landroid/webkit/JsResult;)V

    #@5
    .line 71
    return-void
.end method


# virtual methods
.method public final cancel()V
    .registers 2

    #@0
    .prologue
    .line 42
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/webkit/JsResult;->mResult:Z

    #@3
    .line 43
    invoke-direct {p0}, Landroid/webkit/JsResult;->wakeUp()V

    #@6
    .line 44
    return-void
.end method

.method public final confirm()V
    .registers 2

    #@0
    .prologue
    .line 50
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/webkit/JsResult;->mResult:Z

    #@3
    .line 51
    invoke-direct {p0}, Landroid/webkit/JsResult;->wakeUp()V

    #@6
    .line 52
    return-void
.end method

.method public final getResult()Z
    .registers 2

    #@0
    .prologue
    .line 65
    iget-boolean v0, p0, Landroid/webkit/JsResult;->mResult:Z

    #@2
    return v0
.end method
