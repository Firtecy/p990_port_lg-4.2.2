.class Landroid/webkit/ViewManager$3;
.super Ljava/lang/Object;
.source "ViewManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/webkit/ViewManager;->postReadyToDrawAll()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/webkit/ViewManager;


# direct methods
.method constructor <init>(Landroid/webkit/ViewManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 272
    iput-object p1, p0, Landroid/webkit/ViewManager$3;->this$0:Landroid/webkit/ViewManager;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    #@0
    .prologue
    .line 274
    iget-object v2, p0, Landroid/webkit/ViewManager$3;->this$0:Landroid/webkit/ViewManager;

    #@2
    const/4 v3, 0x1

    #@3
    invoke-static {v2, v3}, Landroid/webkit/ViewManager;->access$402(Landroid/webkit/ViewManager;Z)Z

    #@6
    .line 275
    iget-object v2, p0, Landroid/webkit/ViewManager$3;->this$0:Landroid/webkit/ViewManager;

    #@8
    invoke-static {v2}, Landroid/webkit/ViewManager;->access$300(Landroid/webkit/ViewManager;)Ljava/util/ArrayList;

    #@b
    move-result-object v2

    #@c
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@f
    move-result-object v0

    #@10
    .local v0, i$:Ljava/util/Iterator;
    :goto_10
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@13
    move-result v2

    #@14
    if-eqz v2, :cond_23

    #@16
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@19
    move-result-object v1

    #@1a
    check-cast v1, Landroid/webkit/ViewManager$ChildView;

    #@1c
    .line 276
    .local v1, v:Landroid/webkit/ViewManager$ChildView;
    iget-object v2, v1, Landroid/webkit/ViewManager$ChildView;->mView:Landroid/view/View;

    #@1e
    const/4 v3, 0x0

    #@1f
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    #@22
    goto :goto_10

    #@23
    .line 278
    .end local v1           #v:Landroid/webkit/ViewManager$ChildView;
    :cond_23
    return-void
.end method
