.class public Landroid/webkit/WebViewClientClassicExt;
.super Landroid/webkit/WebViewClient;
.source "WebViewClientClassicExt.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 26
    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public onProceededAfterSslError(Landroid/webkit/WebView;Landroid/net/http/SslError;)V
    .registers 3
    .parameter "view"
    .parameter "error"

    #@0
    .prologue
    .line 34
    return-void
.end method

.method public onReceivedClientCertRequest(Landroid/webkit/WebView;Landroid/webkit/ClientCertRequestHandler;Ljava/lang/String;)V
    .registers 4
    .parameter "view"
    .parameter "handler"
    .parameter "host_and_port"

    #@0
    .prologue
    .line 51
    invoke-virtual {p2}, Landroid/webkit/ClientCertRequestHandler;->cancel()V

    #@3
    .line 52
    return-void
.end method
