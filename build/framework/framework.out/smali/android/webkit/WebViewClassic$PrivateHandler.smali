.class Landroid/webkit/WebViewClassic$PrivateHandler;
.super Landroid/os/Handler;
.source "WebViewClassic.java"

# interfaces
.implements Landroid/webkit/WebViewInputDispatcher$UiCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/WebViewClassic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PrivateHandler"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/webkit/WebViewClassic;


# direct methods
.method constructor <init>(Landroid/webkit/WebViewClassic;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 8809
    iput-object p1, p0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public clearPreviousHitTest()V
    .registers 3

    #@0
    .prologue
    .line 9368
    iget-object v0, p0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v0, v1}, Landroid/webkit/WebViewClassic;->access$6900(Landroid/webkit/WebViewClassic;Landroid/webkit/WebViewCore$WebKitHitTest;)V

    #@6
    .line 9369
    return-void
.end method

.method public dispatchUiEvent(Landroid/view/MotionEvent;II)V
    .registers 5
    .parameter "event"
    .parameter "eventType"
    .parameter "flags"

    #@0
    .prologue
    .line 9305
    iget-object v0, p0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@2
    invoke-static {v0, p1, p2, p3}, Landroid/webkit/WebViewClassic;->access$8200(Landroid/webkit/WebViewClassic;Landroid/view/MotionEvent;II)V

    #@5
    .line 9306
    return-void
.end method

.method public getContext()Landroid/content/Context;
    .registers 2

    #@0
    .prologue
    .line 9310
    iget-object v0, p0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@2
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->getContext()Landroid/content/Context;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getUiLooper()Landroid/os/Looper;
    .registers 2

    #@0
    .prologue
    .line 9300
    invoke-virtual {p0}, Landroid/webkit/WebViewClassic$PrivateHandler;->getLooper()Landroid/os/Looper;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 50
    .parameter "msg"

    #@0
    .prologue
    .line 8826
    move-object/from16 v0, p0

    #@2
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@4
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$1900(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebViewCore;

    #@7
    move-result-object v3

    #@8
    if-nez v3, :cond_b

    #@a
    .line 9296
    :cond_a
    :goto_a
    return-void

    #@b
    .line 8830
    :cond_b
    move-object/from16 v0, p0

    #@d
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@f
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$3000(Landroid/webkit/WebViewClassic;)Z

    #@12
    move-result v3

    #@13
    if-eqz v3, :cond_1d

    #@15
    move-object/from16 v0, p1

    #@17
    iget v3, v0, Landroid/os/Message;->what:I

    #@19
    const/16 v8, 0x6b

    #@1b
    if-ne v3, v8, :cond_a

    #@1d
    .line 8836
    :cond_1d
    move-object/from16 v0, p0

    #@1f
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@21
    invoke-virtual {v3}, Landroid/webkit/WebViewClassic;->getSettings()Landroid/webkit/WebSettingsClassic;

    #@24
    move-result-object v36

    #@25
    .line 8837
    .local v36, settings:Landroid/webkit/WebSettingsClassic;
    move-object/from16 v0, p1

    #@27
    iget v3, v0, Landroid/os/Message;->what:I

    #@29
    sparse-switch v3, :sswitch_data_a0c

    #@2c
    .line 9293
    invoke-super/range {p0 .. p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    #@2f
    goto :goto_a

    #@30
    .line 8839
    :sswitch_30
    move-object/from16 v0, p0

    #@32
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@34
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$2600(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebViewDatabaseClassic;

    #@37
    move-result-object v3

    #@38
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@3b
    move-result-object v8

    #@3c
    const-string v45, "host"

    #@3e
    move-object/from16 v0, v45

    #@40
    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@43
    move-result-object v8

    #@44
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@47
    move-result-object v45

    #@48
    const-string/jumbo v46, "username"

    #@4b
    invoke-virtual/range {v45 .. v46}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@4e
    move-result-object v45

    #@4f
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@52
    move-result-object v46

    #@53
    const-string/jumbo v47, "password"

    #@56
    invoke-virtual/range {v46 .. v47}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@59
    move-result-object v46

    #@5a
    move-object/from16 v0, v45

    #@5c
    move-object/from16 v1, v46

    #@5e
    invoke-virtual {v3, v8, v0, v1}, Landroid/webkit/WebViewDatabaseClassic;->setUsernamePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@61
    .line 8843
    move-object/from16 v0, p1

    #@63
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@65
    check-cast v3, Landroid/os/Message;

    #@67
    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    #@6a
    goto :goto_a

    #@6b
    .line 8847
    :sswitch_6b
    move-object/from16 v0, p0

    #@6d
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@6f
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$2600(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebViewDatabaseClassic;

    #@72
    move-result-object v3

    #@73
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@76
    move-result-object v8

    #@77
    const-string v45, "host"

    #@79
    move-object/from16 v0, v45

    #@7b
    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@7e
    move-result-object v8

    #@7f
    const/16 v45, 0x0

    #@81
    const/16 v46, 0x0

    #@83
    move-object/from16 v0, v45

    #@85
    move-object/from16 v1, v46

    #@87
    invoke-virtual {v3, v8, v0, v1}, Landroid/webkit/WebViewDatabaseClassic;->setUsernamePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@8a
    .line 8848
    move-object/from16 v0, p1

    #@8c
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8e
    check-cast v3, Landroid/os/Message;

    #@90
    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    #@93
    goto/16 :goto_a

    #@95
    .line 8852
    :sswitch_95
    move-object/from16 v0, p0

    #@97
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@99
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$3100(Landroid/webkit/WebViewClassic;)I

    #@9c
    move-result v3

    #@9d
    if-nez v3, :cond_d6

    #@9f
    move-object/from16 v0, p0

    #@a1
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@a3
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$3200(Landroid/webkit/WebViewClassic;)I

    #@a6
    move-result v3

    #@a7
    if-nez v3, :cond_d6

    #@a9
    .line 8854
    move-object/from16 v0, p0

    #@ab
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@ad
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$3300(Landroid/webkit/WebViewClassic;)Landroid/os/PowerManager$WakeLock;

    #@b0
    move-result-object v3

    #@b1
    if-eqz v3, :cond_cc

    #@b3
    move-object/from16 v0, p0

    #@b5
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@b7
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$3300(Landroid/webkit/WebViewClassic;)Landroid/os/PowerManager$WakeLock;

    #@ba
    move-result-object v3

    #@bb
    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@be
    move-result v3

    #@bf
    if-eqz v3, :cond_cc

    #@c1
    .line 8855
    move-object/from16 v0, p0

    #@c3
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@c5
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$3300(Landroid/webkit/WebViewClassic;)Landroid/os/PowerManager$WakeLock;

    #@c8
    move-result-object v3

    #@c9
    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->release()V

    #@cc
    .line 8858
    :cond_cc
    move-object/from16 v0, p0

    #@ce
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@d0
    const/4 v8, 0x0

    #@d1
    invoke-static {v3, v8}, Landroid/webkit/WebViewClassic;->access$3402(Landroid/webkit/WebViewClassic;Z)Z

    #@d4
    goto/16 :goto_a

    #@d6
    .line 8861
    :cond_d6
    move-object/from16 v0, p0

    #@d8
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@da
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$3500(Landroid/webkit/WebViewClassic;)I

    #@dd
    move-result v3

    #@de
    if-nez v3, :cond_110

    #@e0
    .line 8862
    move-object/from16 v0, p0

    #@e2
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@e4
    move-object/from16 v0, p0

    #@e6
    iget-object v8, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@e8
    invoke-static {v8}, Landroid/webkit/WebViewClassic;->access$3100(Landroid/webkit/WebViewClassic;)I

    #@eb
    move-result v8

    #@ec
    move-object/from16 v0, p0

    #@ee
    iget-object v0, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@f0
    move-object/from16 v45, v0

    #@f2
    invoke-static/range {v45 .. v45}, Landroid/webkit/WebViewClassic;->access$3200(Landroid/webkit/WebViewClassic;)I

    #@f5
    move-result v45

    #@f6
    const/16 v46, 0x1

    #@f8
    const/16 v47, 0x0

    #@fa
    move/from16 v0, v45

    #@fc
    move/from16 v1, v46

    #@fe
    move/from16 v2, v47

    #@100
    invoke-static {v3, v8, v0, v1, v2}, Landroid/webkit/WebViewClassic;->access$3600(Landroid/webkit/WebViewClassic;IIZI)Z

    #@103
    .line 8867
    :goto_103
    const/16 v3, 0xb

    #@105
    const-wide/16 v45, 0x10

    #@107
    move-object/from16 v0, p0

    #@109
    move-wide/from16 v1, v45

    #@10b
    invoke-virtual {v0, v3, v1, v2}, Landroid/webkit/WebViewClassic$PrivateHandler;->sendEmptyMessageDelayed(IJ)Z

    #@10e
    goto/16 :goto_a

    #@110
    .line 8864
    :cond_110
    move-object/from16 v0, p0

    #@112
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@114
    move-object/from16 v0, p0

    #@116
    iget-object v8, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@118
    invoke-static {v8}, Landroid/webkit/WebViewClassic;->access$3700(Landroid/webkit/WebViewClassic;)Landroid/graphics/Rect;

    #@11b
    move-result-object v8

    #@11c
    iget v8, v8, Landroid/graphics/Rect;->left:I

    #@11e
    move-object/from16 v0, p0

    #@120
    iget-object v0, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@122
    move-object/from16 v45, v0

    #@124
    invoke-static/range {v45 .. v45}, Landroid/webkit/WebViewClassic;->access$3100(Landroid/webkit/WebViewClassic;)I

    #@127
    move-result v45

    #@128
    add-int v8, v8, v45

    #@12a
    move-object/from16 v0, p0

    #@12c
    iget-object v0, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@12e
    move-object/from16 v45, v0

    #@130
    invoke-static/range {v45 .. v45}, Landroid/webkit/WebViewClassic;->access$3700(Landroid/webkit/WebViewClassic;)Landroid/graphics/Rect;

    #@133
    move-result-object v45

    #@134
    move-object/from16 v0, v45

    #@136
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@138
    move/from16 v45, v0

    #@13a
    move-object/from16 v0, p0

    #@13c
    iget-object v0, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@13e
    move-object/from16 v46, v0

    #@140
    invoke-static/range {v46 .. v46}, Landroid/webkit/WebViewClassic;->access$3200(Landroid/webkit/WebViewClassic;)I

    #@143
    move-result v46

    #@144
    add-int v45, v45, v46

    #@146
    move/from16 v0, v45

    #@148
    invoke-static {v3, v8, v0}, Landroid/webkit/WebViewClassic;->access$3800(Landroid/webkit/WebViewClassic;II)V

    #@14b
    goto :goto_103

    #@14c
    .line 8874
    :sswitch_14c
    move-object/from16 v0, p1

    #@14e
    iget v3, v0, Landroid/os/Message;->arg2:I

    #@150
    const/4 v8, 0x1

    #@151
    if-ne v3, v8, :cond_16f

    #@153
    .line 8877
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@156
    move-result-object v19

    #@157
    .line 8878
    .local v19, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v19, :cond_a

    #@159
    invoke-virtual/range {v19 .. v19}, Landroid/view/inputmethod/InputMethodManager;->isAcceptingText()Z

    #@15c
    move-result v3

    #@15d
    if-eqz v3, :cond_a

    #@15f
    move-object/from16 v0, p0

    #@161
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@163
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$000(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebView;

    #@166
    move-result-object v3

    #@167
    move-object/from16 v0, v19

    #@169
    invoke-virtual {v0, v3}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    #@16c
    move-result v3

    #@16d
    if-eqz v3, :cond_a

    #@16f
    .line 8884
    .end local v19           #imm:Landroid/view/inputmethod/InputMethodManager;
    :cond_16f
    move-object/from16 v0, p0

    #@171
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@173
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$400(Landroid/webkit/WebViewClassic;)Z

    #@176
    move-result v3

    #@177
    if-eqz v3, :cond_19b

    #@179
    move-object/from16 v0, p0

    #@17b
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@17d
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$3900(Landroid/webkit/WebViewClassic;)Z

    #@180
    move-result v3

    #@181
    if-eqz v3, :cond_19b

    #@183
    move-object/from16 v0, p0

    #@185
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@187
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$4000(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebViewClassic$PastePopupWindow;

    #@18a
    move-result-object v3

    #@18b
    if-eqz v3, :cond_19b

    #@18d
    move-object/from16 v0, p0

    #@18f
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@191
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$4000(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebViewClassic$PastePopupWindow;

    #@194
    move-result-object v3

    #@195
    invoke-virtual {v3}, Landroid/webkit/WebViewClassic$PastePopupWindow;->isShowing()Z

    #@198
    move-result v3

    #@199
    if-nez v3, :cond_a

    #@19b
    .line 8888
    :cond_19b
    move-object/from16 v0, p1

    #@19d
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@19f
    move-object/from16 v29, v0

    #@1a1
    check-cast v29, Landroid/graphics/Point;

    #@1a3
    .line 8889
    .local v29, p:Landroid/graphics/Point;
    move-object/from16 v0, p0

    #@1a5
    iget-object v8, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@1a7
    move-object/from16 v0, v29

    #@1a9
    iget v0, v0, Landroid/graphics/Point;->x:I

    #@1ab
    move/from16 v45, v0

    #@1ad
    move-object/from16 v0, v29

    #@1af
    iget v0, v0, Landroid/graphics/Point;->y:I

    #@1b1
    move/from16 v46, v0

    #@1b3
    move-object/from16 v0, p1

    #@1b5
    iget v3, v0, Landroid/os/Message;->arg1:I

    #@1b7
    const/16 v47, 0x1

    #@1b9
    move/from16 v0, v47

    #@1bb
    if-ne v3, v0, :cond_1c7

    #@1bd
    const/4 v3, 0x1

    #@1be
    :goto_1be
    move/from16 v0, v45

    #@1c0
    move/from16 v1, v46

    #@1c2
    invoke-static {v8, v0, v1, v3}, Landroid/webkit/WebViewClassic;->access$4100(Landroid/webkit/WebViewClassic;IIZ)V

    #@1c5
    goto/16 :goto_a

    #@1c7
    :cond_1c7
    const/4 v3, 0x0

    #@1c8
    goto :goto_1be

    #@1c9
    .line 8893
    .end local v29           #p:Landroid/graphics/Point;
    :sswitch_1c9
    move-object/from16 v0, p1

    #@1cb
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1cd
    move-object/from16 v40, v0

    #@1cf
    check-cast v40, Landroid/webkit/WebViewCore$ViewState;

    #@1d1
    .line 8895
    .local v40, viewState:Landroid/webkit/WebViewCore$ViewState;
    move-object/from16 v0, p0

    #@1d3
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@1d5
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$4200(Landroid/webkit/WebViewClassic;)Landroid/webkit/ZoomManager;

    #@1d8
    move-result-object v3

    #@1d9
    move-object/from16 v0, p0

    #@1db
    iget-object v8, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@1dd
    invoke-virtual {v8}, Landroid/webkit/WebViewClassic;->getViewWidth()I

    #@1e0
    move-result v8

    #@1e1
    move-object/from16 v0, v40

    #@1e3
    iget v0, v0, Landroid/webkit/WebViewCore$ViewState;->mScrollX:I

    #@1e5
    move/from16 v45, v0

    #@1e7
    move-object/from16 v0, v40

    #@1e9
    move/from16 v1, v45

    #@1eb
    invoke-virtual {v3, v0, v8, v1}, Landroid/webkit/ZoomManager;->updateZoomRange(Landroid/webkit/WebViewCore$ViewState;II)V

    #@1ee
    goto/16 :goto_a

    #@1f0
    .line 8899
    .end local v40           #viewState:Landroid/webkit/WebViewCore$ViewState;
    :sswitch_1f0
    move-object/from16 v0, p1

    #@1f2
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1f4
    check-cast v3, Ljava/lang/Float;

    #@1f6
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    #@1f9
    move-result v12

    #@1fa
    .line 8900
    .local v12, density:F
    move-object/from16 v0, p0

    #@1fc
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@1fe
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$4200(Landroid/webkit/WebViewClassic;)Landroid/webkit/ZoomManager;

    #@201
    move-result-object v3

    #@202
    invoke-virtual {v3, v12}, Landroid/webkit/ZoomManager;->updateDefaultZoomDensity(F)V

    #@205
    goto/16 :goto_a

    #@207
    .line 8905
    .end local v12           #density:F
    :sswitch_207
    move-object/from16 v0, p1

    #@209
    iget-object v14, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@20b
    check-cast v14, Landroid/webkit/WebViewCore$DrawData;

    #@20d
    .line 8906
    .local v14, draw:Landroid/webkit/WebViewCore$DrawData;
    move-object/from16 v0, p0

    #@20f
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@211
    const/4 v8, 0x1

    #@212
    invoke-virtual {v3, v14, v8}, Landroid/webkit/WebViewClassic;->setNewPicture(Landroid/webkit/WebViewCore$DrawData;Z)V

    #@215
    goto/16 :goto_a

    #@217
    .line 8911
    .end local v14           #draw:Landroid/webkit/WebViewCore$DrawData;
    :sswitch_217
    const/4 v3, 0x3

    #@218
    move-object/from16 v0, p0

    #@21a
    iget-object v8, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@21c
    invoke-static {v8}, Landroid/webkit/WebViewClassic;->access$800(Landroid/webkit/WebViewClassic;)Landroid/content/Context;

    #@21f
    move-result-object v8

    #@220
    invoke-static {v3, v8}, Landroid/webkit/BrowserFrame;->getRawResFilename(ILandroid/content/Context;)Ljava/lang/String;

    #@223
    move-result-object v15

    #@224
    .line 8913
    .local v15, drawableDir:Ljava/lang/String;
    move-object/from16 v0, p0

    #@226
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@228
    move-object/from16 v0, p1

    #@22a
    iget v8, v0, Landroid/os/Message;->arg1:I

    #@22c
    invoke-static {}, Landroid/app/ActivityManager;->isHighEndGfx()Z

    #@22f
    move-result v45

    #@230
    move/from16 v0, v45

    #@232
    invoke-static {v3, v8, v15, v0}, Landroid/webkit/WebViewClassic;->access$4300(Landroid/webkit/WebViewClassic;ILjava/lang/String;Z)V

    #@235
    .line 8914
    move-object/from16 v0, p0

    #@237
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@239
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$4400(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebViewCore$DrawData;

    #@23c
    move-result-object v3

    #@23d
    if-eqz v3, :cond_25a

    #@23f
    .line 8915
    move-object/from16 v0, p0

    #@241
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@243
    move-object/from16 v0, p0

    #@245
    iget-object v8, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@247
    invoke-static {v8}, Landroid/webkit/WebViewClassic;->access$4400(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebViewCore$DrawData;

    #@24a
    move-result-object v8

    #@24b
    const/16 v45, 0x1

    #@24d
    move/from16 v0, v45

    #@24f
    invoke-virtual {v3, v8, v0}, Landroid/webkit/WebViewClassic;->setNewPicture(Landroid/webkit/WebViewCore$DrawData;Z)V

    #@252
    .line 8916
    move-object/from16 v0, p0

    #@254
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@256
    const/4 v8, 0x0

    #@257
    invoke-static {v3, v8}, Landroid/webkit/WebViewClassic;->access$4402(Landroid/webkit/WebViewClassic;Landroid/webkit/WebViewCore$DrawData;)Landroid/webkit/WebViewCore$DrawData;

    #@25a
    .line 8918
    :cond_25a
    move-object/from16 v0, p0

    #@25c
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@25e
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$4500(Landroid/webkit/WebViewClassic;)Z

    #@261
    move-result v3

    #@262
    if-eqz v3, :cond_270

    #@264
    .line 8919
    move-object/from16 v0, p0

    #@266
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@268
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$4600(Landroid/webkit/WebViewClassic;)I

    #@26b
    move-result v3

    #@26c
    const/4 v8, 0x1

    #@26d
    invoke-static {v3, v8}, Landroid/webkit/WebViewClassic;->access$4700(IZ)V

    #@270
    .line 8921
    :cond_270
    move-object/from16 v0, p0

    #@272
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@274
    new-instance v8, Landroid/webkit/WebViewInputDispatcher;

    #@276
    move-object/from16 v0, p0

    #@278
    iget-object v0, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@27a
    move-object/from16 v45, v0

    #@27c
    invoke-static/range {v45 .. v45}, Landroid/webkit/WebViewClassic;->access$1900(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebViewCore;

    #@27f
    move-result-object v45

    #@280
    invoke-virtual/range {v45 .. v45}, Landroid/webkit/WebViewCore;->getInputDispatcherCallbacks()Landroid/webkit/WebViewInputDispatcher$WebKitCallbacks;

    #@283
    move-result-object v45

    #@284
    move-object/from16 v0, p0

    #@286
    move-object/from16 v1, v45

    #@288
    invoke-direct {v8, v0, v1}, Landroid/webkit/WebViewInputDispatcher;-><init>(Landroid/webkit/WebViewInputDispatcher$UiCallbacks;Landroid/webkit/WebViewInputDispatcher$WebKitCallbacks;)V

    #@28b
    invoke-static {v3, v8}, Landroid/webkit/WebViewClassic;->access$4802(Landroid/webkit/WebViewClassic;Landroid/webkit/WebViewInputDispatcher;)Landroid/webkit/WebViewInputDispatcher;

    #@28e
    goto/16 :goto_a

    #@290
    .line 8927
    .end local v15           #drawableDir:Ljava/lang/String;
    :sswitch_290
    move-object/from16 v0, p1

    #@292
    iget v3, v0, Landroid/os/Message;->arg2:I

    #@294
    move-object/from16 v0, p0

    #@296
    iget-object v8, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@298
    invoke-static {v8}, Landroid/webkit/WebViewClassic;->access$4900(Landroid/webkit/WebViewClassic;)I

    #@29b
    move-result v8

    #@29c
    if-ne v3, v8, :cond_a

    #@29e
    .line 8928
    move-object/from16 v0, p1

    #@2a0
    iget-object v6, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2a2
    check-cast v6, Ljava/lang/String;

    #@2a4
    .line 8929
    .local v6, text:Ljava/lang/String;
    if-nez v6, :cond_2a8

    #@2a6
    .line 8930
    const-string v6, ""

    #@2a8
    .line 8932
    :cond_2a8
    move-object/from16 v0, p0

    #@2aa
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@2ac
    iget-object v3, v3, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@2ae
    if-eqz v3, :cond_a

    #@2b0
    move-object/from16 v0, p0

    #@2b2
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@2b4
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$100(Landroid/webkit/WebViewClassic;)I

    #@2b7
    move-result v3

    #@2b8
    move-object/from16 v0, p1

    #@2ba
    iget v8, v0, Landroid/os/Message;->arg1:I

    #@2bc
    if-ne v3, v8, :cond_a

    #@2be
    .line 8934
    move-object/from16 v0, p0

    #@2c0
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@2c2
    iget-object v3, v3, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@2c4
    invoke-virtual {v3, v6}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->setTextAndKeepSelection(Ljava/lang/CharSequence;)V

    #@2c7
    goto/16 :goto_a

    #@2c9
    .line 8939
    .end local v6           #text:Ljava/lang/String;
    :sswitch_2c9
    move-object/from16 v0, p0

    #@2cb
    iget-object v8, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@2cd
    move-object/from16 v0, p1

    #@2cf
    iget v0, v0, Landroid/os/Message;->arg1:I

    #@2d1
    move/from16 v45, v0

    #@2d3
    move-object/from16 v0, p1

    #@2d5
    iget v0, v0, Landroid/os/Message;->arg2:I

    #@2d7
    move/from16 v46, v0

    #@2d9
    move-object/from16 v0, p1

    #@2db
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2dd
    check-cast v3, Landroid/webkit/WebViewCore$TextSelectionData;

    #@2df
    move/from16 v0, v45

    #@2e1
    move/from16 v1, v46

    #@2e3
    invoke-static {v8, v0, v1, v3}, Landroid/webkit/WebViewClassic;->access$5000(Landroid/webkit/WebViewClassic;IILandroid/webkit/WebViewCore$TextSelectionData;)V

    #@2e6
    .line 8942
    if-eqz v36, :cond_a

    #@2e8
    move-object/from16 v0, p0

    #@2ea
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@2ec
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$5100(Landroid/webkit/WebViewClassic;)Z

    #@2ef
    move-result v3

    #@2f0
    invoke-virtual/range {v36 .. v36}, Landroid/webkit/WebSettingsClassic;->getFloatingMode()Z

    #@2f3
    move-result v8

    #@2f4
    if-ne v3, v8, :cond_300

    #@2f6
    move-object/from16 v0, p0

    #@2f8
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@2fa
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$5200(Landroid/webkit/WebViewClassic;)Z

    #@2fd
    move-result v3

    #@2fe
    if-eqz v3, :cond_a

    #@300
    :cond_300
    move-object/from16 v0, p0

    #@302
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@304
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$400(Landroid/webkit/WebViewClassic;)Z

    #@307
    move-result v3

    #@308
    if-nez v3, :cond_a

    #@30a
    move-object/from16 v0, p0

    #@30c
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@30e
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$3900(Landroid/webkit/WebViewClassic;)Z

    #@311
    move-result v3

    #@312
    if-eqz v3, :cond_a

    #@314
    .line 8943
    move-object/from16 v0, p0

    #@316
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@318
    const/4 v8, 0x0

    #@319
    invoke-static {v3, v8}, Landroid/webkit/WebViewClassic;->access$5300(Landroid/webkit/WebViewClassic;I)V

    #@31c
    .line 8944
    move-object/from16 v0, p0

    #@31e
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@320
    invoke-virtual/range {v36 .. v36}, Landroid/webkit/WebSettingsClassic;->getFloatingMode()Z

    #@323
    move-result v8

    #@324
    invoke-static {v3, v8}, Landroid/webkit/WebViewClassic;->access$5102(Landroid/webkit/WebViewClassic;Z)Z

    #@327
    .line 8945
    move-object/from16 v0, p0

    #@329
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@32b
    const/4 v8, 0x0

    #@32c
    invoke-static {v3, v8}, Landroid/webkit/WebViewClassic;->access$5202(Landroid/webkit/WebViewClassic;Z)Z

    #@32f
    goto/16 :goto_a

    #@331
    .line 8950
    :sswitch_331
    move-object/from16 v0, p1

    #@333
    iget v13, v0, Landroid/os/Message;->arg1:I

    #@335
    .line 8951
    .local v13, direction:I
    move-object/from16 v0, p0

    #@337
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@339
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$000(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebView;

    #@33c
    move-result-object v3

    #@33d
    invoke-virtual {v3, v13}, Landroid/webkit/WebView;->focusSearch(I)Landroid/view/View;

    #@340
    move-result-object v16

    #@341
    .line 8952
    .local v16, focusSearch:Landroid/view/View;
    if-eqz v16, :cond_a

    #@343
    move-object/from16 v0, p0

    #@345
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@347
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$000(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebView;

    #@34a
    move-result-object v3

    #@34b
    move-object/from16 v0, v16

    #@34d
    if-eq v0, v3, :cond_a

    #@34f
    .line 8953
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->requestFocus()Z

    #@352
    goto/16 :goto_a

    #@354
    .line 8958
    .end local v13           #direction:I
    .end local v16           #focusSearch:Landroid/view/View;
    :sswitch_354
    if-eqz v36, :cond_365

    #@356
    invoke-virtual/range {v36 .. v36}, Landroid/webkit/WebSettingsClassic;->getCliptrayEnabled()Z

    #@359
    move-result v3

    #@35a
    if-eqz v3, :cond_365

    #@35c
    .line 8959
    invoke-static {}, Landroid/webkit/LGCliptrayManager;->getInstance()Landroid/webkit/LGCliptrayManager;

    #@35f
    move-result-object v10

    #@360
    .line 8960
    .local v10, cliptray:Landroid/webkit/LGCliptrayManager;
    if-eqz v10, :cond_365

    #@362
    .line 8961
    invoke-virtual {v10}, Landroid/webkit/LGCliptrayManager;->hideCliptray()V

    #@365
    .line 8965
    .end local v10           #cliptray:Landroid/webkit/LGCliptrayManager;
    :cond_365
    move-object/from16 v0, p0

    #@367
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@369
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$500(Landroid/webkit/WebViewClassic;)V

    #@36c
    goto/16 :goto_a

    #@36e
    .line 8968
    :sswitch_36e
    move-object/from16 v0, p1

    #@370
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@372
    move-object/from16 v31, v0

    #@374
    check-cast v31, Landroid/graphics/Rect;

    #@376
    .line 8969
    .local v31, r:Landroid/graphics/Rect;
    if-nez v31, :cond_381

    #@378
    .line 8970
    move-object/from16 v0, p0

    #@37a
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@37c
    invoke-virtual {v3}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@37f
    goto/16 :goto_a

    #@381
    .line 8974
    :cond_381
    move-object/from16 v0, p0

    #@383
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@385
    move-object/from16 v0, v31

    #@387
    iget v8, v0, Landroid/graphics/Rect;->left:I

    #@389
    move-object/from16 v0, v31

    #@38b
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@38d
    move/from16 v45, v0

    #@38f
    move-object/from16 v0, v31

    #@391
    iget v0, v0, Landroid/graphics/Rect;->right:I

    #@393
    move/from16 v46, v0

    #@395
    move-object/from16 v0, v31

    #@397
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    #@399
    move/from16 v47, v0

    #@39b
    move/from16 v0, v45

    #@39d
    move/from16 v1, v46

    #@39f
    move/from16 v2, v47

    #@3a1
    invoke-static {v3, v8, v0, v1, v2}, Landroid/webkit/WebViewClassic;->access$5400(Landroid/webkit/WebViewClassic;IIII)V

    #@3a4
    goto/16 :goto_a

    #@3a6
    .line 8979
    .end local v31           #r:Landroid/graphics/Rect;
    :sswitch_3a6
    move-object/from16 v0, p0

    #@3a8
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@3aa
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$100(Landroid/webkit/WebViewClassic;)I

    #@3ad
    move-result v3

    #@3ae
    move-object/from16 v0, p1

    #@3b0
    iget v8, v0, Landroid/os/Message;->arg1:I

    #@3b2
    if-ne v3, v8, :cond_a

    #@3b4
    .line 8980
    move-object/from16 v0, p1

    #@3b6
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3b8
    check-cast v9, Landroid/widget/ArrayAdapter;

    #@3ba
    .line 8981
    .local v9, adapter:Landroid/widget/ArrayAdapter;,"Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    #@3bc
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@3be
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$600(Landroid/webkit/WebViewClassic;)Landroid/webkit/AutoCompletePopup;

    #@3c1
    move-result-object v3

    #@3c2
    invoke-virtual {v3, v9}, Landroid/webkit/AutoCompletePopup;->setAdapter(Landroid/widget/ListAdapter;)V

    #@3c5
    goto/16 :goto_a

    #@3c7
    .line 8988
    .end local v9           #adapter:Landroid/widget/ArrayAdapter;,"Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    :sswitch_3c7
    move-object/from16 v0, p0

    #@3c9
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@3cb
    const/4 v8, 0x0

    #@3cc
    invoke-static {v3, v8}, Landroid/webkit/WebViewClassic;->access$5502(Landroid/webkit/WebViewClassic;Z)Z

    #@3cf
    .line 8989
    move-object/from16 v0, p0

    #@3d1
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@3d3
    const/4 v8, 0x0

    #@3d4
    invoke-static {v3, v8}, Landroid/webkit/WebViewClassic;->access$5602(Landroid/webkit/WebViewClassic;Z)Z

    #@3d7
    .line 8990
    move-object/from16 v0, p0

    #@3d9
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@3db
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$000(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebView;

    #@3de
    move-result-object v3

    #@3df
    invoke-virtual {v3}, Landroid/webkit/WebView;->performLongClick()Z

    #@3e2
    goto/16 :goto_a

    #@3e4
    .line 8994
    :sswitch_3e4
    move-object/from16 v0, p0

    #@3e6
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@3e8
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$4800(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebViewInputDispatcher;

    #@3eb
    move-result-object v8

    #@3ec
    move-object/from16 v0, p1

    #@3ee
    iget v3, v0, Landroid/os/Message;->arg1:I

    #@3f0
    if-eqz v3, :cond_3f8

    #@3f2
    const/4 v3, 0x1

    #@3f3
    :goto_3f3
    invoke-virtual {v8, v3}, Landroid/webkit/WebViewInputDispatcher;->setWebKitWantsTouchEvents(Z)V

    #@3f6
    goto/16 :goto_a

    #@3f8
    :cond_3f8
    const/4 v3, 0x0

    #@3f9
    goto :goto_3f3

    #@3fa
    .line 8998
    :sswitch_3fa
    move-object/from16 v0, p1

    #@3fc
    iget v3, v0, Landroid/os/Message;->arg1:I

    #@3fe
    if-nez v3, :cond_409

    #@400
    .line 8999
    move-object/from16 v0, p0

    #@402
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@404
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$500(Landroid/webkit/WebViewClassic;)V

    #@407
    goto/16 :goto_a

    #@409
    .line 9001
    :cond_409
    move-object/from16 v0, p0

    #@40b
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@40d
    const/4 v8, 0x0

    #@40e
    invoke-static {v3, v8}, Landroid/webkit/WebViewClassic;->access$5700(Landroid/webkit/WebViewClassic;Z)V

    #@411
    goto/16 :goto_a

    #@413
    .line 9006
    :sswitch_413
    move-object/from16 v0, p0

    #@415
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@417
    const/4 v8, 0x2

    #@418
    invoke-static {v3, v8}, Landroid/webkit/WebViewClassic;->access$5802(Landroid/webkit/WebViewClassic;I)I

    #@41b
    .line 9007
    move-object/from16 v0, p0

    #@41d
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@41f
    invoke-virtual {v3}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@422
    goto/16 :goto_a

    #@424
    .line 9011
    :sswitch_424
    move-object/from16 v0, p0

    #@426
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@428
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$000(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebView;

    #@42b
    move-result-object v8

    #@42c
    move-object/from16 v0, p1

    #@42e
    iget v3, v0, Landroid/os/Message;->arg1:I

    #@430
    const/16 v45, 0x1

    #@432
    move/from16 v0, v45

    #@434
    if-ne v3, v0, :cond_43c

    #@436
    const/4 v3, 0x1

    #@437
    :goto_437
    invoke-virtual {v8, v3}, Landroid/webkit/WebView;->setKeepScreenOn(Z)V

    #@43a
    goto/16 :goto_a

    #@43c
    :cond_43c
    const/4 v3, 0x0

    #@43d
    goto :goto_437

    #@43e
    .line 9015
    :sswitch_43e
    move-object/from16 v0, p0

    #@440
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@442
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$5900(Landroid/webkit/WebViewClassic;)Landroid/webkit/HTML5VideoViewProxy;

    #@445
    move-result-object v3

    #@446
    if-eqz v3, :cond_a

    #@448
    .line 9016
    move-object/from16 v0, p0

    #@44a
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@44c
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$5900(Landroid/webkit/WebViewClassic;)Landroid/webkit/HTML5VideoViewProxy;

    #@44f
    move-result-object v3

    #@450
    invoke-virtual {v3}, Landroid/webkit/HTML5VideoViewProxy;->exitFullScreenVideo()V

    #@453
    goto/16 :goto_a

    #@455
    .line 9021
    :sswitch_455
    move-object/from16 v0, p1

    #@457
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@459
    move-object/from16 v38, v0

    #@45b
    check-cast v38, Landroid/view/View;

    #@45d
    .line 9022
    .local v38, view:Landroid/view/View;
    move-object/from16 v0, p1

    #@45f
    iget v0, v0, Landroid/os/Message;->arg1:I

    #@461
    move/from16 v28, v0

    #@463
    .line 9023
    .local v28, orientation:I
    move-object/from16 v0, p1

    #@465
    iget v0, v0, Landroid/os/Message;->arg2:I

    #@467
    move/from16 v27, v0

    #@469
    .line 9025
    .local v27, npp:I
    move-object/from16 v0, p0

    #@46b
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@46d
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$6000(Landroid/webkit/WebViewClassic;)Z

    #@470
    move-result v3

    #@471
    if-eqz v3, :cond_482

    #@473
    .line 9026
    const-string/jumbo v3, "webview"

    #@476
    const-string v8, "Should not have another full screen."

    #@478
    invoke-static {v3, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@47b
    .line 9027
    move-object/from16 v0, p0

    #@47d
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@47f
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$6100(Landroid/webkit/WebViewClassic;)V

    #@482
    .line 9029
    :cond_482
    move-object/from16 v0, p0

    #@484
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@486
    new-instance v8, Landroid/webkit/PluginFullScreenHolder;

    #@488
    move-object/from16 v0, p0

    #@48a
    iget-object v0, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@48c
    move-object/from16 v45, v0

    #@48e
    move-object/from16 v0, v45

    #@490
    move/from16 v1, v28

    #@492
    move/from16 v2, v27

    #@494
    invoke-direct {v8, v0, v1, v2}, Landroid/webkit/PluginFullScreenHolder;-><init>(Landroid/webkit/WebViewClassic;II)V

    #@497
    iput-object v8, v3, Landroid/webkit/WebViewClassic;->mFullScreenHolder:Landroid/webkit/PluginFullScreenHolder;

    #@499
    .line 9030
    move-object/from16 v0, p0

    #@49b
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@49d
    iget-object v3, v3, Landroid/webkit/WebViewClassic;->mFullScreenHolder:Landroid/webkit/PluginFullScreenHolder;

    #@49f
    move-object/from16 v0, v38

    #@4a1
    invoke-virtual {v3, v0}, Landroid/webkit/PluginFullScreenHolder;->setContentView(Landroid/view/View;)V

    #@4a4
    .line 9031
    move-object/from16 v0, p0

    #@4a6
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@4a8
    iget-object v3, v3, Landroid/webkit/WebViewClassic;->mFullScreenHolder:Landroid/webkit/PluginFullScreenHolder;

    #@4aa
    invoke-virtual {v3}, Landroid/webkit/PluginFullScreenHolder;->show()V

    #@4ad
    .line 9032
    move-object/from16 v0, p0

    #@4af
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@4b1
    invoke-virtual {v3}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@4b4
    goto/16 :goto_a

    #@4b6
    .line 9037
    .end local v27           #npp:I
    .end local v28           #orientation:I
    .end local v38           #view:Landroid/view/View;
    :sswitch_4b6
    move-object/from16 v0, p0

    #@4b8
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@4ba
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$6100(Landroid/webkit/WebViewClassic;)V

    #@4bd
    goto/16 :goto_a

    #@4bf
    .line 9041
    :sswitch_4bf
    move-object/from16 v0, p1

    #@4c1
    iget-object v11, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4c3
    check-cast v11, Landroid/webkit/WebViewCore$ShowRectData;

    #@4c5
    .line 9042
    .local v11, data:Landroid/webkit/WebViewCore$ShowRectData;
    move-object/from16 v0, p0

    #@4c7
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@4c9
    iget v8, v11, Landroid/webkit/WebViewCore$ShowRectData;->mLeft:I

    #@4cb
    invoke-virtual {v3, v8}, Landroid/webkit/WebViewClassic;->contentToViewX(I)I

    #@4ce
    move-result v21

    #@4cf
    .line 9043
    .local v21, left:I
    move-object/from16 v0, p0

    #@4d1
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@4d3
    iget v8, v11, Landroid/webkit/WebViewCore$ShowRectData;->mWidth:I

    #@4d5
    invoke-virtual {v3, v8}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@4d8
    move-result v42

    #@4d9
    .line 9044
    .local v42, width:I
    move-object/from16 v0, p0

    #@4db
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@4dd
    iget v8, v11, Landroid/webkit/WebViewCore$ShowRectData;->mContentWidth:I

    #@4df
    invoke-virtual {v3, v8}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@4e2
    move-result v26

    #@4e3
    .line 9045
    .local v26, maxWidth:I
    move-object/from16 v0, p0

    #@4e5
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@4e7
    invoke-virtual {v3}, Landroid/webkit/WebViewClassic;->getViewWidth()I

    #@4ea
    move-result v41

    #@4eb
    .line 9046
    .local v41, viewWidth:I
    move/from16 v0, v21

    #@4ed
    int-to-float v3, v0

    #@4ee
    iget v8, v11, Landroid/webkit/WebViewCore$ShowRectData;->mXPercentInDoc:F

    #@4f0
    move/from16 v0, v42

    #@4f2
    int-to-float v0, v0

    #@4f3
    move/from16 v45, v0

    #@4f5
    mul-float v8, v8, v45

    #@4f7
    add-float/2addr v3, v8

    #@4f8
    iget v8, v11, Landroid/webkit/WebViewCore$ShowRectData;->mXPercentInView:F

    #@4fa
    move/from16 v0, v41

    #@4fc
    int-to-float v0, v0

    #@4fd
    move/from16 v45, v0

    #@4ff
    mul-float v8, v8, v45

    #@501
    sub-float/2addr v3, v8

    #@502
    float-to-int v0, v3

    #@503
    move/from16 v43, v0

    #@505
    .line 9057
    .local v43, x:I
    const/4 v3, 0x0

    #@506
    add-int v8, v43, v41

    #@508
    move/from16 v0, v26

    #@50a
    invoke-static {v0, v8}, Ljava/lang/Math;->min(II)I

    #@50d
    move-result v8

    #@50e
    sub-int v8, v8, v41

    #@510
    invoke-static {v3, v8}, Ljava/lang/Math;->max(II)I

    #@513
    move-result v43

    #@514
    .line 9059
    move-object/from16 v0, p0

    #@516
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@518
    iget v8, v11, Landroid/webkit/WebViewCore$ShowRectData;->mTop:I

    #@51a
    invoke-virtual {v3, v8}, Landroid/webkit/WebViewClassic;->contentToViewY(I)I

    #@51d
    move-result v37

    #@51e
    .line 9060
    .local v37, top:I
    move-object/from16 v0, p0

    #@520
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@522
    iget v8, v11, Landroid/webkit/WebViewCore$ShowRectData;->mHeight:I

    #@524
    invoke-virtual {v3, v8}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@527
    move-result v17

    #@528
    .line 9061
    .local v17, height:I
    move-object/from16 v0, p0

    #@52a
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@52c
    iget v8, v11, Landroid/webkit/WebViewCore$ShowRectData;->mContentHeight:I

    #@52e
    invoke-virtual {v3, v8}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@531
    move-result v25

    #@532
    .line 9062
    .local v25, maxHeight:I
    move-object/from16 v0, p0

    #@534
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@536
    invoke-virtual {v3}, Landroid/webkit/WebViewClassic;->getViewHeight()I

    #@539
    move-result v39

    #@53a
    .line 9063
    .local v39, viewHeight:I
    move/from16 v0, v37

    #@53c
    int-to-float v3, v0

    #@53d
    iget v8, v11, Landroid/webkit/WebViewCore$ShowRectData;->mYPercentInDoc:F

    #@53f
    move/from16 v0, v17

    #@541
    int-to-float v0, v0

    #@542
    move/from16 v45, v0

    #@544
    mul-float v8, v8, v45

    #@546
    add-float/2addr v3, v8

    #@547
    iget v8, v11, Landroid/webkit/WebViewCore$ShowRectData;->mYPercentInView:F

    #@549
    move/from16 v0, v39

    #@54b
    int-to-float v0, v0

    #@54c
    move/from16 v45, v0

    #@54e
    mul-float v8, v8, v45

    #@550
    sub-float/2addr v3, v8

    #@551
    float-to-int v0, v3

    #@552
    move/from16 v44, v0

    #@554
    .line 9074
    .local v44, y:I
    const/4 v3, 0x0

    #@555
    add-int v8, v44, v39

    #@557
    move/from16 v0, v25

    #@559
    invoke-static {v0, v8}, Ljava/lang/Math;->min(II)I

    #@55c
    move-result v8

    #@55d
    sub-int v8, v8, v39

    #@55f
    invoke-static {v3, v8}, Ljava/lang/Math;->max(II)I

    #@562
    move-result v44

    #@563
    .line 9078
    const/4 v3, 0x0

    #@564
    move-object/from16 v0, p0

    #@566
    iget-object v8, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@568
    invoke-static {v8}, Landroid/webkit/WebViewClassic;->access$6200(Landroid/webkit/WebViewClassic;)I

    #@56b
    move-result v8

    #@56c
    sub-int v8, v44, v8

    #@56e
    invoke-static {v3, v8}, Ljava/lang/Math;->max(II)I

    #@571
    move-result v44

    #@572
    .line 9079
    move-object/from16 v0, p0

    #@574
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@576
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$000(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebView;

    #@579
    move-result-object v3

    #@57a
    move/from16 v0, v43

    #@57c
    move/from16 v1, v44

    #@57e
    invoke-virtual {v3, v0, v1}, Landroid/webkit/WebView;->scrollTo(II)V

    #@581
    goto/16 :goto_a

    #@583
    .line 9084
    .end local v11           #data:Landroid/webkit/WebViewCore$ShowRectData;
    .end local v17           #height:I
    .end local v21           #left:I
    .end local v25           #maxHeight:I
    .end local v26           #maxWidth:I
    .end local v37           #top:I
    .end local v39           #viewHeight:I
    .end local v41           #viewWidth:I
    .end local v42           #width:I
    .end local v43           #x:I
    .end local v44           #y:I
    :sswitch_583
    move-object/from16 v0, p0

    #@585
    iget-object v8, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@587
    move-object/from16 v0, p1

    #@589
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@58b
    check-cast v3, Landroid/graphics/Rect;

    #@58d
    invoke-virtual {v8, v3}, Landroid/webkit/WebViewClassic;->centerFitRect(Landroid/graphics/Rect;)V

    #@590
    goto/16 :goto_a

    #@592
    .line 9088
    :sswitch_592
    move-object/from16 v0, p0

    #@594
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@596
    move-object/from16 v0, p1

    #@598
    iget v8, v0, Landroid/os/Message;->arg1:I

    #@59a
    invoke-static {v3, v8}, Landroid/webkit/WebViewClassic;->access$6302(Landroid/webkit/WebViewClassic;I)I

    #@59d
    .line 9089
    move-object/from16 v0, p0

    #@59f
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@5a1
    move-object/from16 v0, p1

    #@5a3
    iget v8, v0, Landroid/os/Message;->arg2:I

    #@5a5
    invoke-static {v3, v8}, Landroid/webkit/WebViewClassic;->access$6402(Landroid/webkit/WebViewClassic;I)I

    #@5a8
    goto/16 :goto_a

    #@5aa
    .line 9093
    :sswitch_5aa
    move-object/from16 v0, p0

    #@5ac
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@5ae
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$6500(Landroid/webkit/WebViewClassic;)Z

    #@5b1
    move-result v3

    #@5b2
    if-eqz v3, :cond_a

    #@5b4
    .line 9094
    move-object/from16 v0, p0

    #@5b6
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@5b8
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$6600(Landroid/webkit/WebViewClassic;)Landroid/webkit/AccessibilityInjector;

    #@5bb
    move-result-object v8

    #@5bc
    move-object/from16 v0, p1

    #@5be
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@5c0
    check-cast v3, Ljava/lang/String;

    #@5c2
    invoke-virtual {v8, v3}, Landroid/webkit/AccessibilityInjector;->handleSelectionChangedIfNecessary(Ljava/lang/String;)V

    #@5c5
    goto/16 :goto_a

    #@5c7
    .line 9101
    :sswitch_5c7
    move-object/from16 v0, p0

    #@5c9
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@5cb
    iget-boolean v3, v3, Landroid/webkit/WebViewClassic;->mIsEditingText:Z

    #@5cd
    if-eqz v3, :cond_5f5

    #@5cf
    move-object/from16 v0, p1

    #@5d1
    iget v3, v0, Landroid/os/Message;->arg1:I

    #@5d3
    move-object/from16 v0, p0

    #@5d5
    iget-object v8, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@5d7
    invoke-static {v8}, Landroid/webkit/WebViewClassic;->access$100(Landroid/webkit/WebViewClassic;)I

    #@5da
    move-result v8

    #@5db
    if-eq v3, v8, :cond_5f5

    #@5dd
    .line 9103
    if-eqz v36, :cond_5ee

    #@5df
    invoke-virtual/range {v36 .. v36}, Landroid/webkit/WebSettingsClassic;->getCliptrayEnabled()Z

    #@5e2
    move-result v3

    #@5e3
    if-eqz v3, :cond_5ee

    #@5e5
    .line 9104
    invoke-static {}, Landroid/webkit/LGCliptrayManager;->getInstance()Landroid/webkit/LGCliptrayManager;

    #@5e8
    move-result-object v10

    #@5e9
    .line 9105
    .restart local v10       #cliptray:Landroid/webkit/LGCliptrayManager;
    if-eqz v10, :cond_5ee

    #@5eb
    .line 9106
    invoke-virtual {v10}, Landroid/webkit/LGCliptrayManager;->hideCliptray()V

    #@5ee
    .line 9110
    .end local v10           #cliptray:Landroid/webkit/LGCliptrayManager;
    :cond_5ee
    move-object/from16 v0, p0

    #@5f0
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@5f2
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$500(Landroid/webkit/WebViewClassic;)V

    #@5f5
    .line 9113
    :cond_5f5
    move-object/from16 v0, p0

    #@5f7
    iget-object v8, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@5f9
    move-object/from16 v0, p1

    #@5fb
    iget v3, v0, Landroid/os/Message;->arg1:I

    #@5fd
    move-object/from16 v0, p0

    #@5ff
    iget-object v0, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@601
    move-object/from16 v45, v0

    #@603
    invoke-static/range {v45 .. v45}, Landroid/webkit/WebViewClassic;->access$100(Landroid/webkit/WebViewClassic;)I

    #@606
    move-result v45

    #@607
    move/from16 v0, v45

    #@609
    if-ne v3, v0, :cond_694

    #@60b
    const/4 v3, 0x1

    #@60c
    :goto_60c
    iput-boolean v3, v8, Landroid/webkit/WebViewClassic;->mIsEditingText:Z

    #@60e
    .line 9114
    move-object/from16 v0, p0

    #@610
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@612
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$600(Landroid/webkit/WebViewClassic;)Landroid/webkit/AutoCompletePopup;

    #@615
    move-result-object v3

    #@616
    if-eqz v3, :cond_62b

    #@618
    move-object/from16 v0, p0

    #@61a
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@61c
    iget-boolean v3, v3, Landroid/webkit/WebViewClassic;->mIsEditingText:Z

    #@61e
    if-nez v3, :cond_62b

    #@620
    .line 9115
    move-object/from16 v0, p0

    #@622
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@624
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$600(Landroid/webkit/WebViewClassic;)Landroid/webkit/AutoCompletePopup;

    #@627
    move-result-object v3

    #@628
    invoke-virtual {v3}, Landroid/webkit/AutoCompletePopup;->clearAdapter()V

    #@62b
    .line 9118
    :cond_62b
    move-object/from16 v0, p0

    #@62d
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@62f
    iget-boolean v3, v3, Landroid/webkit/WebViewClassic;->mIsEditingText:Z

    #@631
    if-eqz v3, :cond_66f

    #@633
    move-object/from16 v0, p0

    #@635
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@637
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$000(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebView;

    #@63a
    move-result-object v3

    #@63b
    invoke-virtual {v3}, Landroid/webkit/WebView;->isInTouchMode()Z

    #@63e
    move-result v3

    #@63f
    if-nez v3, :cond_66f

    #@641
    .line 9119
    move-object/from16 v0, p0

    #@643
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@645
    iget-object v3, v3, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@647
    if-eqz v3, :cond_66f

    #@649
    .line 9120
    move-object/from16 v0, p0

    #@64b
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@64d
    iget-object v3, v3, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@64f
    invoke-virtual {v3}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->getEditable()Landroid/text/Editable;

    #@652
    move-result-object v3

    #@653
    invoke-interface {v3}, Landroid/text/Editable;->length()I

    #@656
    move-result v22

    #@657
    .line 9121
    .local v22, len:I
    move-object/from16 v0, p0

    #@659
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@65b
    iget-object v3, v3, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@65d
    move/from16 v0, v22

    #@65f
    move/from16 v1, v22

    #@661
    invoke-virtual {v3, v0, v1}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->setSelection(II)Z

    #@664
    .line 9122
    move-object/from16 v0, p0

    #@666
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@668
    move/from16 v0, v22

    #@66a
    move/from16 v1, v22

    #@66c
    invoke-virtual {v3, v0, v1}, Landroid/webkit/WebViewClassic;->setSelection(II)V

    #@66f
    .line 9128
    .end local v22           #len:I
    :cond_66f
    :sswitch_66f
    move-object/from16 v0, p1

    #@671
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@673
    move-object/from16 v18, v0

    #@675
    check-cast v18, Landroid/webkit/WebViewCore$WebKitHitTest;

    #@677
    .line 9129
    .local v18, hit:Landroid/webkit/WebViewCore$WebKitHitTest;
    move-object/from16 v0, p0

    #@679
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@67b
    move-object/from16 v0, v18

    #@67d
    invoke-static {v3, v0}, Landroid/webkit/WebViewClassic;->access$6702(Landroid/webkit/WebViewClassic;Landroid/webkit/WebViewCore$WebKitHitTest;)Landroid/webkit/WebViewCore$WebKitHitTest;

    #@680
    .line 9130
    move-object/from16 v0, p0

    #@682
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@684
    move-object/from16 v0, v18

    #@686
    invoke-static {v3, v0}, Landroid/webkit/WebViewClassic;->access$6800(Landroid/webkit/WebViewClassic;Landroid/webkit/WebViewCore$WebKitHitTest;)V

    #@689
    .line 9131
    move-object/from16 v0, p0

    #@68b
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@68d
    move-object/from16 v0, v18

    #@68f
    invoke-static {v3, v0}, Landroid/webkit/WebViewClassic;->access$6900(Landroid/webkit/WebViewClassic;Landroid/webkit/WebViewCore$WebKitHitTest;)V

    #@692
    goto/16 :goto_a

    #@694
    .line 9113
    .end local v18           #hit:Landroid/webkit/WebViewCore$WebKitHitTest;
    :cond_694
    const/4 v3, 0x0

    #@695
    goto/16 :goto_60c

    #@697
    .line 9135
    :sswitch_697
    move-object/from16 v0, p1

    #@699
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@69b
    move-object/from16 v34, v0

    #@69d
    check-cast v34, Landroid/webkit/WebViewClassic$SaveWebArchiveMessage;

    #@69f
    .line 9136
    .local v34, saveMessage:Landroid/webkit/WebViewClassic$SaveWebArchiveMessage;
    move-object/from16 v0, v34

    #@6a1
    iget-object v3, v0, Landroid/webkit/WebViewClassic$SaveWebArchiveMessage;->mCallback:Landroid/webkit/ValueCallback;

    #@6a3
    if-eqz v3, :cond_a

    #@6a5
    .line 9137
    move-object/from16 v0, v34

    #@6a7
    iget-object v3, v0, Landroid/webkit/WebViewClassic$SaveWebArchiveMessage;->mCallback:Landroid/webkit/ValueCallback;

    #@6a9
    move-object/from16 v0, v34

    #@6ab
    iget-object v8, v0, Landroid/webkit/WebViewClassic$SaveWebArchiveMessage;->mResultFile:Ljava/lang/String;

    #@6ad
    invoke-interface {v3, v8}, Landroid/webkit/ValueCallback;->onReceiveValue(Ljava/lang/Object;)V

    #@6b0
    goto/16 :goto_a

    #@6b2
    .line 9142
    .end local v34           #saveMessage:Landroid/webkit/WebViewClassic$SaveWebArchiveMessage;
    :sswitch_6b2
    move-object/from16 v0, p0

    #@6b4
    iget-object v8, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@6b6
    move-object/from16 v0, p1

    #@6b8
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@6ba
    check-cast v3, Landroid/webkit/WebViewCore$AutoFillData;

    #@6bc
    invoke-static {v8, v3}, Landroid/webkit/WebViewClassic;->access$2502(Landroid/webkit/WebViewClassic;Landroid/webkit/WebViewCore$AutoFillData;)Landroid/webkit/WebViewCore$AutoFillData;

    #@6bf
    .line 9143
    move-object/from16 v0, p0

    #@6c1
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@6c3
    iget-object v3, v3, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@6c5
    if-eqz v3, :cond_a

    #@6c7
    .line 9144
    move-object/from16 v0, p0

    #@6c9
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@6cb
    iget-object v3, v3, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@6cd
    move-object/from16 v0, p0

    #@6cf
    iget-object v8, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@6d1
    invoke-static {v8}, Landroid/webkit/WebViewClassic;->access$2500(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebViewCore$AutoFillData;

    #@6d4
    move-result-object v8

    #@6d5
    invoke-virtual {v8}, Landroid/webkit/WebViewCore$AutoFillData;->getQueryId()I

    #@6d8
    move-result v8

    #@6d9
    invoke-virtual {v3, v8}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->setAutoFillable(I)V

    #@6dc
    .line 9145
    move-object/from16 v0, p0

    #@6de
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@6e0
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$600(Landroid/webkit/WebViewClassic;)Landroid/webkit/AutoCompletePopup;

    #@6e3
    move-result-object v3

    #@6e4
    move-object/from16 v0, p0

    #@6e6
    iget-object v8, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@6e8
    invoke-static {v8}, Landroid/webkit/WebViewClassic;->access$2500(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebViewCore$AutoFillData;

    #@6eb
    move-result-object v8

    #@6ec
    invoke-virtual {v8}, Landroid/webkit/WebViewCore$AutoFillData;->getQueryId()I

    #@6ef
    move-result v8

    #@6f0
    invoke-virtual {v3, v8}, Landroid/webkit/AutoCompletePopup;->setAutoFillQueryId(I)V

    #@6f3
    goto/16 :goto_a

    #@6f5
    .line 9150
    :sswitch_6f5
    move-object/from16 v0, p0

    #@6f7
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@6f9
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$600(Landroid/webkit/WebViewClassic;)Landroid/webkit/AutoCompletePopup;

    #@6fc
    move-result-object v3

    #@6fd
    if-eqz v3, :cond_a

    #@6ff
    .line 9151
    new-instance v30, Ljava/util/ArrayList;

    #@701
    invoke-direct/range {v30 .. v30}, Ljava/util/ArrayList;-><init>()V

    #@704
    .line 9152
    .local v30, pastEntries:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    #@706
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@708
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$600(Landroid/webkit/WebViewClassic;)Landroid/webkit/AutoCompletePopup;

    #@70b
    move-result-object v3

    #@70c
    new-instance v8, Landroid/widget/ArrayAdapter;

    #@70e
    move-object/from16 v0, p0

    #@710
    iget-object v0, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@712
    move-object/from16 v45, v0

    #@714
    invoke-static/range {v45 .. v45}, Landroid/webkit/WebViewClassic;->access$800(Landroid/webkit/WebViewClassic;)Landroid/content/Context;

    #@717
    move-result-object v45

    #@718
    const v46, 0x10900e9

    #@71b
    move-object/from16 v0, v45

    #@71d
    move/from16 v1, v46

    #@71f
    move-object/from16 v2, v30

    #@721
    invoke-direct {v8, v0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    #@724
    invoke-virtual {v3, v8}, Landroid/webkit/AutoCompletePopup;->setAdapter(Landroid/widget/ListAdapter;)V

    #@727
    goto/16 :goto_a

    #@729
    .line 9160
    .end local v30           #pastEntries:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :sswitch_729
    move-object/from16 v0, p0

    #@72b
    iget-object v8, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@72d
    move-object/from16 v0, p1

    #@72f
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@731
    check-cast v3, Ljava/lang/String;

    #@733
    invoke-static {v8, v3}, Landroid/webkit/WebViewClassic;->access$7000(Landroid/webkit/WebViewClassic;Ljava/lang/String;)V

    #@736
    goto/16 :goto_a

    #@738
    .line 9164
    :sswitch_738
    move-object/from16 v0, p0

    #@73a
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@73c
    iget-object v3, v3, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@73e
    if-eqz v3, :cond_7bd

    #@740
    .line 9165
    move-object/from16 v0, p1

    #@742
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@744
    move-object/from16 v20, v0

    #@746
    check-cast v20, Landroid/webkit/WebViewCore$TextFieldInitData;

    #@748
    .line 9166
    .local v20, initData:Landroid/webkit/WebViewCore$TextFieldInitData;
    move-object/from16 v0, p0

    #@74a
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@74c
    const/4 v8, 0x0

    #@74d
    invoke-static {v3, v8}, Landroid/webkit/WebViewClassic;->access$4902(Landroid/webkit/WebViewClassic;I)I

    #@750
    .line 9167
    move-object/from16 v0, p0

    #@752
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@754
    move-object/from16 v0, v20

    #@756
    iget v8, v0, Landroid/webkit/WebViewCore$TextFieldInitData;->mFieldPointer:I

    #@758
    invoke-static {v3, v8}, Landroid/webkit/WebViewClassic;->access$102(Landroid/webkit/WebViewClassic;I)I

    #@75b
    .line 9168
    move-object/from16 v0, p0

    #@75d
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@75f
    iget-object v3, v3, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@761
    move-object/from16 v0, v20

    #@763
    invoke-virtual {v3, v0}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->initEditorInfo(Landroid/webkit/WebViewCore$TextFieldInitData;)V

    #@766
    .line 9169
    move-object/from16 v0, p0

    #@768
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@76a
    iget-object v3, v3, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@76c
    move-object/from16 v0, v20

    #@76e
    iget-object v8, v0, Landroid/webkit/WebViewCore$TextFieldInitData;->mText:Ljava/lang/String;

    #@770
    invoke-virtual {v3, v8}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->setTextAndKeepSelection(Ljava/lang/CharSequence;)V

    #@773
    .line 9170
    move-object/from16 v0, p0

    #@775
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@777
    iget-object v3, v3, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@779
    move-object/from16 v0, v20

    #@77b
    iget-object v8, v0, Landroid/webkit/WebViewCore$TextFieldInitData;->mContentBounds:Landroid/graphics/Rect;

    #@77d
    invoke-virtual {v3, v8}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@780
    .line 9171
    move-object/from16 v0, p0

    #@782
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@784
    move-object/from16 v0, v20

    #@786
    iget v8, v0, Landroid/webkit/WebViewCore$TextFieldInitData;->mNodeLayerId:I

    #@788
    iput v8, v3, Landroid/webkit/WebViewClassic;->mEditTextLayerId:I

    #@78a
    .line 9172
    move-object/from16 v0, p0

    #@78c
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@78e
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$4600(Landroid/webkit/WebViewClassic;)I

    #@791
    move-result v3

    #@792
    move-object/from16 v0, p0

    #@794
    iget-object v8, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@796
    iget v8, v8, Landroid/webkit/WebViewClassic;->mEditTextLayerId:I

    #@798
    move-object/from16 v0, p0

    #@79a
    iget-object v0, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@79c
    move-object/from16 v45, v0

    #@79e
    move-object/from16 v0, v45

    #@7a0
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@7a2
    move-object/from16 v45, v0

    #@7a4
    move-object/from16 v0, v45

    #@7a6
    invoke-static {v3, v8, v0}, Landroid/webkit/WebViewClassic;->access$7100(IILandroid/graphics/Rect;)V

    #@7a9
    .line 9174
    move-object/from16 v0, p0

    #@7ab
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@7ad
    iget-object v3, v3, Landroid/webkit/WebViewClassic;->mEditTextContent:Landroid/graphics/Rect;

    #@7af
    move-object/from16 v0, v20

    #@7b1
    iget-object v8, v0, Landroid/webkit/WebViewCore$TextFieldInitData;->mClientRect:Landroid/graphics/Rect;

    #@7b3
    invoke-virtual {v3, v8}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@7b6
    .line 9175
    move-object/from16 v0, p0

    #@7b8
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@7ba
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$7200(Landroid/webkit/WebViewClassic;)V

    #@7bd
    .line 9177
    .end local v20           #initData:Landroid/webkit/WebViewCore$TextFieldInitData;
    :cond_7bd
    move-object/from16 v0, p0

    #@7bf
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@7c1
    const/4 v8, 0x1

    #@7c2
    invoke-static {v3, v8}, Landroid/webkit/WebViewClassic;->access$7302(Landroid/webkit/WebViewClassic;Z)Z

    #@7c5
    goto/16 :goto_a

    #@7c7
    .line 9181
    :sswitch_7c7
    move-object/from16 v0, p1

    #@7c9
    iget-object v6, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@7cb
    check-cast v6, Ljava/lang/String;

    #@7cd
    .line 9182
    .restart local v6       #text:Ljava/lang/String;
    move-object/from16 v0, p1

    #@7cf
    iget v4, v0, Landroid/os/Message;->arg1:I

    #@7d1
    .line 9183
    .local v4, start:I
    move-object/from16 v0, p1

    #@7d3
    iget v5, v0, Landroid/os/Message;->arg2:I

    #@7d5
    .line 9184
    .local v5, end:I
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@7d8
    move-result v3

    #@7d9
    add-int v7, v4, v3

    #@7db
    .line 9185
    .local v7, cursorPosition:I
    move-object/from16 v0, p0

    #@7dd
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@7df
    move v8, v7

    #@7e0
    invoke-virtual/range {v3 .. v8}, Landroid/webkit/WebViewClassic;->replaceTextfieldText(IILjava/lang/String;II)V

    #@7e3
    .line 9187
    move-object/from16 v0, p0

    #@7e5
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@7e7
    invoke-virtual {v3}, Landroid/webkit/WebViewClassic;->selectionDone()V

    #@7ea
    goto/16 :goto_a

    #@7ec
    .line 9192
    .end local v4           #start:I
    .end local v5           #end:I
    .end local v6           #text:Ljava/lang/String;
    .end local v7           #cursorPosition:I
    :sswitch_7ec
    move-object/from16 v0, p1

    #@7ee
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@7f0
    move-object/from16 v32, v0

    #@7f2
    check-cast v32, Landroid/webkit/WebViewCore$FindAllRequest;

    #@7f4
    .line 9193
    .local v32, request:Landroid/webkit/WebViewCore$FindAllRequest;
    if-nez v32, :cond_816

    #@7f6
    .line 9194
    move-object/from16 v0, p0

    #@7f8
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@7fa
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$7400(Landroid/webkit/WebViewClassic;)Landroid/webkit/FindActionModeCallback;

    #@7fd
    move-result-object v3

    #@7fe
    if-eqz v3, :cond_a

    #@800
    .line 9195
    move-object/from16 v0, p0

    #@802
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@804
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$7400(Landroid/webkit/WebViewClassic;)Landroid/webkit/FindActionModeCallback;

    #@807
    move-result-object v3

    #@808
    const/4 v8, 0x0

    #@809
    const/16 v45, 0x0

    #@80b
    const/16 v46, 0x1

    #@80d
    move/from16 v0, v45

    #@80f
    move/from16 v1, v46

    #@811
    invoke-virtual {v3, v8, v0, v1}, Landroid/webkit/FindActionModeCallback;->updateMatchCount(IIZ)V

    #@814
    goto/16 :goto_a

    #@816
    .line 9197
    :cond_816
    move-object/from16 v0, p0

    #@818
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@81a
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$7500(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebViewCore$FindAllRequest;

    #@81d
    move-result-object v3

    #@81e
    move-object/from16 v0, v32

    #@820
    if-ne v0, v3, :cond_a

    #@822
    .line 9199
    move-object/from16 v0, p0

    #@824
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@826
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$7500(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebViewCore$FindAllRequest;

    #@829
    move-result-object v8

    #@82a
    monitor-enter v8

    #@82b
    .line 9200
    :try_start_82b
    move-object/from16 v0, v32

    #@82d
    iget v0, v0, Landroid/webkit/WebViewCore$FindAllRequest;->mMatchCount:I

    #@82f
    move/from16 v23, v0

    #@831
    .line 9201
    .local v23, matchCount:I
    move-object/from16 v0, v32

    #@833
    iget v0, v0, Landroid/webkit/WebViewCore$FindAllRequest;->mMatchIndex:I

    #@835
    move/from16 v24, v0

    #@837
    .line 9202
    .local v24, matchIndex:I
    monitor-exit v8
    :try_end_838
    .catchall {:try_start_82b .. :try_end_838} :catchall_86e

    #@838
    .line 9203
    move-object/from16 v0, p0

    #@83a
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@83c
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$7400(Landroid/webkit/WebViewClassic;)Landroid/webkit/FindActionModeCallback;

    #@83f
    move-result-object v3

    #@840
    if-eqz v3, :cond_852

    #@842
    .line 9204
    move-object/from16 v0, p0

    #@844
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@846
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$7400(Landroid/webkit/WebViewClassic;)Landroid/webkit/FindActionModeCallback;

    #@849
    move-result-object v3

    #@84a
    const/4 v8, 0x0

    #@84b
    move/from16 v0, v24

    #@84d
    move/from16 v1, v23

    #@84f
    invoke-virtual {v3, v0, v1, v8}, Landroid/webkit/FindActionModeCallback;->updateMatchCount(IIZ)V

    #@852
    .line 9206
    :cond_852
    move-object/from16 v0, p0

    #@854
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@856
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$7600(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebView$FindListener;

    #@859
    move-result-object v3

    #@85a
    if-eqz v3, :cond_a

    #@85c
    .line 9207
    move-object/from16 v0, p0

    #@85e
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@860
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$7600(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebView$FindListener;

    #@863
    move-result-object v3

    #@864
    const/4 v8, 0x1

    #@865
    move/from16 v0, v24

    #@867
    move/from16 v1, v23

    #@869
    invoke-interface {v3, v0, v1, v8}, Landroid/webkit/WebView$FindListener;->onFindResultReceived(IIZ)V

    #@86c
    goto/16 :goto_a

    #@86e
    .line 9202
    .end local v23           #matchCount:I
    .end local v24           #matchIndex:I
    :catchall_86e
    move-exception v3

    #@86f
    :try_start_86f
    monitor-exit v8
    :try_end_870
    .catchall {:try_start_86f .. :try_end_870} :catchall_86e

    #@870
    throw v3

    #@871
    .line 9214
    .end local v32           #request:Landroid/webkit/WebViewCore$FindAllRequest;
    :sswitch_871
    move-object/from16 v0, p0

    #@873
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@875
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$400(Landroid/webkit/WebViewClassic;)Z

    #@878
    move-result v3

    #@879
    if-eqz v3, :cond_a

    #@87b
    .line 9215
    move-object/from16 v0, p0

    #@87d
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@87f
    invoke-virtual {v3}, Landroid/webkit/WebViewClassic;->selectionDone()V

    #@882
    goto/16 :goto_a

    #@884
    .line 9220
    :sswitch_884
    move-object/from16 v0, p0

    #@886
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@888
    const/16 v8, 0xdf

    #@88a
    move-object/from16 v0, p1

    #@88c
    iget v0, v0, Landroid/os/Message;->arg1:I

    #@88e
    move/from16 v45, v0

    #@890
    const/16 v46, 0x0

    #@892
    const/16 v47, 0x0

    #@894
    move/from16 v0, v45

    #@896
    move/from16 v1, v46

    #@898
    move-object/from16 v2, v47

    #@89a
    invoke-virtual {v3, v8, v0, v1, v2}, Landroid/webkit/WebViewClassic;->sendBatchableInputMessage(IIILjava/lang/Object;)V

    #@89d
    goto/16 :goto_a

    #@89f
    .line 9224
    :sswitch_89f
    move-object/from16 v0, p0

    #@8a1
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@8a3
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$7200(Landroid/webkit/WebViewClassic;)V

    #@8a6
    goto/16 :goto_a

    #@8a8
    .line 9228
    :sswitch_8a8
    move-object/from16 v0, p0

    #@8aa
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@8ac
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$1900(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebViewCore;

    #@8af
    move-result-object v3

    #@8b0
    const/16 v8, 0xc0

    #@8b2
    move-object/from16 v0, p1

    #@8b4
    iget v0, v0, Landroid/os/Message;->arg1:I

    #@8b6
    move/from16 v45, v0

    #@8b8
    const/16 v46, 0x0

    #@8ba
    move/from16 v0, v45

    #@8bc
    move/from16 v1, v46

    #@8be
    invoke-virtual {v3, v8, v0, v1}, Landroid/webkit/WebViewCore;->sendMessage(III)V

    #@8c1
    goto/16 :goto_a

    #@8c3
    .line 9233
    :sswitch_8c3
    move-object/from16 v0, p1

    #@8c5
    iget v3, v0, Landroid/os/Message;->arg1:I

    #@8c7
    move-object/from16 v0, p0

    #@8c9
    iget-object v8, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@8cb
    invoke-static {v8}, Landroid/webkit/WebViewClassic;->access$100(Landroid/webkit/WebViewClassic;)I

    #@8ce
    move-result v8

    #@8cf
    if-ne v3, v8, :cond_a

    #@8d1
    .line 9234
    move-object/from16 v0, p0

    #@8d3
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@8d5
    iget-object v8, v3, Landroid/webkit/WebViewClassic;->mEditTextContent:Landroid/graphics/Rect;

    #@8d7
    move-object/from16 v0, p1

    #@8d9
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8db
    check-cast v3, Landroid/graphics/Rect;

    #@8dd
    invoke-virtual {v8, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@8e0
    goto/16 :goto_a

    #@8e2
    .line 9239
    :sswitch_8e2
    move-object/from16 v0, p0

    #@8e4
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@8e6
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$3900(Landroid/webkit/WebViewClassic;)Z

    #@8e9
    move-result v3

    #@8ea
    if-nez v3, :cond_a

    #@8ec
    move-object/from16 v0, p0

    #@8ee
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@8f0
    iget-boolean v3, v3, Landroid/webkit/WebViewClassic;->mIsEditingText:Z

    #@8f2
    if-eqz v3, :cond_a

    #@8f4
    move-object/from16 v0, p0

    #@8f6
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@8f8
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$400(Landroid/webkit/WebViewClassic;)Z

    #@8fb
    move-result v3

    #@8fc
    if-eqz v3, :cond_a

    #@8fe
    .line 9240
    move-object/from16 v0, p0

    #@900
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@902
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$7700(Landroid/webkit/WebViewClassic;)Z

    #@905
    .line 9241
    move-object/from16 v0, p0

    #@907
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@909
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$7800(Landroid/webkit/WebViewClassic;)V

    #@90c
    .line 9242
    move-object/from16 v0, p0

    #@90e
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@910
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$7900(Landroid/webkit/WebViewClassic;)V

    #@913
    goto/16 :goto_a

    #@915
    .line 9247
    :sswitch_915
    move-object/from16 v0, p0

    #@917
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@919
    iget-object v8, v3, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@91b
    move-object/from16 v0, p1

    #@91d
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@91f
    check-cast v3, Landroid/graphics/Rect;

    #@921
    invoke-virtual {v8, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@924
    .line 9248
    move-object/from16 v0, p0

    #@926
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@928
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$4600(Landroid/webkit/WebViewClassic;)I

    #@92b
    move-result v3

    #@92c
    move-object/from16 v0, p0

    #@92e
    iget-object v8, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@930
    iget v8, v8, Landroid/webkit/WebViewClassic;->mEditTextLayerId:I

    #@932
    move-object/from16 v0, p0

    #@934
    iget-object v0, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@936
    move-object/from16 v45, v0

    #@938
    move-object/from16 v0, v45

    #@93a
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mEditTextContentBounds:Landroid/graphics/Rect;

    #@93c
    move-object/from16 v45, v0

    #@93e
    move-object/from16 v0, v45

    #@940
    invoke-static {v3, v8, v0}, Landroid/webkit/WebViewClassic;->access$7100(IILandroid/graphics/Rect;)V

    #@943
    .line 9250
    move-object/from16 v0, p0

    #@945
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@947
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$7200(Landroid/webkit/WebViewClassic;)V

    #@94a
    goto/16 :goto_a

    #@94c
    .line 9254
    :sswitch_94c
    move-object/from16 v0, p0

    #@94e
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@950
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$8000(Landroid/webkit/WebViewClassic;)V

    #@953
    goto/16 :goto_a

    #@955
    .line 9258
    :sswitch_955
    move-object/from16 v0, p0

    #@957
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@959
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$8100(Landroid/webkit/WebViewClassic;)V

    #@95c
    goto/16 :goto_a

    #@95e
    .line 9263
    :sswitch_95e
    move-object/from16 v0, p1

    #@960
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@962
    move-object/from16 v35, v0

    #@964
    check-cast v35, Ljava/lang/String;

    #@966
    .line 9264
    .local v35, savedFilePath:Ljava/lang/String;
    move-object/from16 v0, p1

    #@968
    iget v0, v0, Landroid/os/Message;->arg1:I

    #@96a
    move/from16 v33, v0

    #@96c
    .line 9266
    .local v33, result:I
    if-eqz v36, :cond_a

    #@96e
    invoke-virtual/range {v36 .. v36}, Landroid/webkit/WebSettingsClassic;->getCliptrayEnabled()Z

    #@971
    move-result v3

    #@972
    if-eqz v3, :cond_a

    #@974
    .line 9267
    const/4 v3, 0x1

    #@975
    move/from16 v0, v33

    #@977
    if-ne v0, v3, :cond_99d

    #@979
    .line 9268
    invoke-static {}, Landroid/webkit/LGCliptrayManager;->getInstance()Landroid/webkit/LGCliptrayManager;

    #@97c
    move-result-object v10

    #@97d
    .line 9269
    .restart local v10       #cliptray:Landroid/webkit/LGCliptrayManager;
    if-eqz v10, :cond_a

    #@97f
    .line 9270
    new-instance v3, Ljava/lang/StringBuilder;

    #@981
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@984
    const-string v8, "file://"

    #@986
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@989
    move-result-object v3

    #@98a
    move-object/from16 v0, v35

    #@98c
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98f
    move-result-object v3

    #@990
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@993
    move-result-object v3

    #@994
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@997
    move-result-object v3

    #@998
    invoke-virtual {v10, v3}, Landroid/webkit/LGCliptrayManager;->copyImageToCliptray(Landroid/net/Uri;)V

    #@99b
    goto/16 :goto_a

    #@99d
    .line 9273
    .end local v10           #cliptray:Landroid/webkit/LGCliptrayManager;
    :cond_99d
    const-string/jumbo v3, "webview"

    #@9a0
    new-instance v8, Ljava/lang/StringBuilder;

    #@9a2
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@9a5
    const-string v45, "Fail("

    #@9a7
    move-object/from16 v0, v45

    #@9a9
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9ac
    move-result-object v8

    #@9ad
    move/from16 v0, v33

    #@9af
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9b2
    move-result-object v8

    #@9b3
    const-string v45, "): CACHED_IMAGE_SAVED: file="

    #@9b5
    move-object/from16 v0, v45

    #@9b7
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9ba
    move-result-object v8

    #@9bb
    move-object/from16 v0, v35

    #@9bd
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c0
    move-result-object v8

    #@9c1
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9c4
    move-result-object v8

    #@9c5
    invoke-static {v3, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9c8
    .line 9274
    const/4 v3, -0x1

    #@9c9
    move/from16 v0, v33

    #@9cb
    if-ne v0, v3, :cond_9e5

    #@9cd
    .line 9276
    move-object/from16 v0, p0

    #@9cf
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@9d1
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$800(Landroid/webkit/WebViewClassic;)Landroid/content/Context;

    #@9d4
    move-result-object v3

    #@9d5
    const v8, 0x2090262

    #@9d8
    const/16 v45, 0x0

    #@9da
    move/from16 v0, v45

    #@9dc
    invoke-static {v3, v8, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    #@9df
    move-result-object v3

    #@9e0
    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    #@9e3
    goto/16 :goto_a

    #@9e5
    .line 9278
    :cond_9e5
    move-object/from16 v0, p0

    #@9e7
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@9e9
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$800(Landroid/webkit/WebViewClassic;)Landroid/content/Context;

    #@9ec
    move-result-object v3

    #@9ed
    const v8, 0x20902a5

    #@9f0
    const/16 v45, 0x0

    #@9f2
    move/from16 v0, v45

    #@9f4
    invoke-static {v3, v8, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    #@9f7
    move-result-object v3

    #@9f8
    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    #@9fb
    goto/16 :goto_a

    #@9fd
    .line 9288
    .end local v33           #result:I
    .end local v35           #savedFilePath:Ljava/lang/String;
    :sswitch_9fd
    move-object/from16 v0, p0

    #@9ff
    iget-object v3, v0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@a01
    iget-object v3, v3, Landroid/webkit/WebViewClassic;->mInputConnection:Landroid/webkit/WebViewClassic$WebViewInputConnection;

    #@a03
    move-object/from16 v0, p1

    #@a05
    iget v8, v0, Landroid/os/Message;->arg1:I

    #@a07
    invoke-virtual {v3, v8}, Landroid/webkit/WebViewClassic$WebViewInputConnection;->updateSelectionPos(I)V

    #@a0a
    goto/16 :goto_a

    #@a0c
    .line 8837
    :sswitch_data_a0c
    .sparse-switch
        0x1 -> :sswitch_30
        0x2 -> :sswitch_6b
        0x6 -> :sswitch_3a6
        0x8 -> :sswitch_413
        0xb -> :sswitch_95
        0x65 -> :sswitch_14c
        0x69 -> :sswitch_207
        0x6b -> :sswitch_217
        0x6c -> :sswitch_290
        0x6d -> :sswitch_1c9
        0x6e -> :sswitch_331
        0x6f -> :sswitch_354
        0x70 -> :sswitch_2c9
        0x71 -> :sswitch_4bf
        0x72 -> :sswitch_3c7
        0x74 -> :sswitch_3e4
        0x75 -> :sswitch_36e
        0x76 -> :sswitch_3fa
        0x78 -> :sswitch_455
        0x79 -> :sswitch_4b6
        0x7e -> :sswitch_7ec
        0x7f -> :sswitch_583
        0x81 -> :sswitch_592
        0x82 -> :sswitch_5aa
        0x83 -> :sswitch_66f
        0x84 -> :sswitch_697
        0x85 -> :sswitch_6b2
        0x86 -> :sswitch_6f5
        0x88 -> :sswitch_424
        0x8b -> :sswitch_1f0
        0x8c -> :sswitch_43e
        0x8d -> :sswitch_729
        0x8e -> :sswitch_738
        0x8f -> :sswitch_7c7
        0x90 -> :sswitch_871
        0x91 -> :sswitch_884
        0x92 -> :sswitch_89f
        0x93 -> :sswitch_5c7
        0x94 -> :sswitch_8a8
        0x95 -> :sswitch_94c
        0x96 -> :sswitch_8c3
        0x97 -> :sswitch_8e2
        0x98 -> :sswitch_915
        0x99 -> :sswitch_955
        0xa0 -> :sswitch_95e
        0xb9 -> :sswitch_9fd
    .end sparse-switch
.end method

.method public shouldIgnoreClickEvent(Landroid/view/MotionEvent;)Z
    .registers 7
    .parameter "event"

    #@0
    .prologue
    .line 9340
    iget-object v3, p0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@2
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$900(Landroid/webkit/WebViewClassic;)V

    #@5
    .line 9341
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@8
    move-result v3

    #@9
    iget-object v4, p0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@b
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getTitleHeight()I

    #@e
    move-result v4

    #@f
    int-to-float v4, v4

    #@10
    sub-float/2addr v3, v4

    #@11
    iget-object v4, p0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@13
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@16
    move-result v4

    #@17
    int-to-float v4, v4

    #@18
    add-float/2addr v3, v4

    #@19
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    #@1c
    move-result v2

    #@1d
    .line 9342
    .local v2, y:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@20
    move-result v3

    #@21
    iget-object v4, p0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@23
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@26
    move-result v4

    #@27
    int-to-float v4, v4

    #@28
    add-float/2addr v3, v4

    #@29
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    #@2c
    move-result v1

    #@2d
    .line 9343
    .local v1, x:I
    const/4 v0, 0x0

    #@2e
    .line 9344
    .local v0, isPressingHandle:Z
    iget-object v3, p0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@30
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$8600(Landroid/webkit/WebViewClassic;)Z

    #@33
    move-result v3

    #@34
    if-eqz v3, :cond_4c

    #@36
    iget-object v3, p0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@38
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$400(Landroid/webkit/WebViewClassic;)Z

    #@3b
    move-result v3

    #@3c
    if-eqz v3, :cond_4c

    #@3e
    .line 9345
    iget-object v3, p0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@40
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$1000(Landroid/webkit/WebViewClassic;)Landroid/graphics/drawable/Drawable;

    #@43
    move-result-object v3

    #@44
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    #@47
    move-result-object v3

    #@48
    invoke-virtual {v3, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    #@4b
    move-result v0

    #@4c
    .line 9349
    :cond_4c
    return v0
.end method

.method public shouldIgnoreTouchMoveEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 9354
    iget-object v0, p0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@2
    invoke-static {v0}, Landroid/webkit/WebViewClassic;->access$3900(Landroid/webkit/WebViewClassic;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public shouldInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 7
    .parameter "event"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 9315
    iget-object v3, p0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@3
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$3900(Landroid/webkit/WebViewClassic;)Z

    #@6
    move-result v3

    #@7
    if-nez v3, :cond_a

    #@9
    .line 9334
    :goto_9
    return v0

    #@a
    .line 9318
    :cond_a
    iget-object v3, p0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@c
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$900(Landroid/webkit/WebViewClassic;)V

    #@f
    .line 9319
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@12
    move-result v3

    #@13
    iget-object v4, p0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@15
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getTitleHeight()I

    #@18
    move-result v4

    #@19
    int-to-float v4, v4

    #@1a
    sub-float/2addr v3, v4

    #@1b
    iget-object v4, p0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@1d
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getScrollY()I

    #@20
    move-result v4

    #@21
    int-to-float v4, v4

    #@22
    add-float/2addr v3, v4

    #@23
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    #@26
    move-result v2

    #@27
    .line 9320
    .local v2, y:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@2a
    move-result v3

    #@2b
    iget-object v4, p0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@2d
    invoke-virtual {v4}, Landroid/webkit/WebViewClassic;->getScrollX()I

    #@30
    move-result v4

    #@31
    int-to-float v4, v4

    #@32
    add-float/2addr v3, v4

    #@33
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    #@36
    move-result v1

    #@37
    .line 9322
    .local v1, x:I
    iget-object v3, p0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@39
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$400(Landroid/webkit/WebViewClassic;)Z

    #@3c
    move-result v3

    #@3d
    if-eqz v3, :cond_4e

    #@3f
    .line 9323
    iget-object v3, p0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@41
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$1000(Landroid/webkit/WebViewClassic;)Landroid/graphics/drawable/Drawable;

    #@44
    move-result-object v3

    #@45
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    #@48
    move-result-object v3

    #@49
    invoke-virtual {v3, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    #@4c
    move-result v0

    #@4d
    .local v0, isPressingHandle:Z
    goto :goto_9

    #@4e
    .line 9326
    .end local v0           #isPressingHandle:Z
    :cond_4e
    iget-object v3, p0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@50
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$1900(Landroid/webkit/WebViewClassic;)Landroid/webkit/WebViewCore;

    #@53
    move-result-object v3

    #@54
    invoke-virtual {v3}, Landroid/webkit/WebViewCore;->inParagraphMode()Z

    #@57
    move-result v3

    #@58
    if-eqz v3, :cond_61

    #@5a
    .line 9327
    iget-object v3, p0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@5c
    invoke-static {v3, v1, v2}, Landroid/webkit/WebViewClassic;->access$8300(Landroid/webkit/WebViewClassic;II)Z

    #@5f
    move-result v0

    #@60
    .restart local v0       #isPressingHandle:Z
    goto :goto_9

    #@61
    .line 9330
    .end local v0           #isPressingHandle:Z
    :cond_61
    iget-object v3, p0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@63
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$8400(Landroid/webkit/WebViewClassic;)Landroid/graphics/Rect;

    #@66
    move-result-object v3

    #@67
    invoke-virtual {v3, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    #@6a
    move-result v3

    #@6b
    if-nez v3, :cond_79

    #@6d
    iget-object v3, p0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@6f
    invoke-static {v3}, Landroid/webkit/WebViewClassic;->access$8500(Landroid/webkit/WebViewClassic;)Landroid/graphics/Rect;

    #@72
    move-result-object v3

    #@73
    invoke-virtual {v3, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    #@76
    move-result v3

    #@77
    if-eqz v3, :cond_7a

    #@79
    :cond_79
    const/4 v0, 0x1

    #@7a
    .restart local v0       #isPressingHandle:Z
    :cond_7a
    goto :goto_9
.end method

.method public showTapHighlight(Z)V
    .registers 3
    .parameter "show"

    #@0
    .prologue
    .line 9360
    iget-object v0, p0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@2
    invoke-static {v0}, Landroid/webkit/WebViewClassic;->access$8700(Landroid/webkit/WebViewClassic;)Z

    #@5
    move-result v0

    #@6
    if-eq v0, p1, :cond_12

    #@8
    .line 9361
    iget-object v0, p0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@a
    invoke-static {v0, p1}, Landroid/webkit/WebViewClassic;->access$8702(Landroid/webkit/WebViewClassic;Z)Z

    #@d
    .line 9362
    iget-object v0, p0, Landroid/webkit/WebViewClassic$PrivateHandler;->this$0:Landroid/webkit/WebViewClassic;

    #@f
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->invalidate()V

    #@12
    .line 9364
    :cond_12
    return-void
.end method
