.class Landroid/webkit/HTML5VideoFullScreen$FullScreenMediaController;
.super Landroid/widget/MediaController;
.source "HTML5VideoFullScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/HTML5VideoFullScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FullScreenMediaController"
.end annotation


# instance fields
.field mVideoView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .registers 3
    .parameter "context"
    .parameter "video"

    #@0
    .prologue
    .line 385
    invoke-direct {p0, p1}, Landroid/widget/MediaController;-><init>(Landroid/content/Context;)V

    #@3
    .line 386
    iput-object p2, p0, Landroid/webkit/HTML5VideoFullScreen$FullScreenMediaController;->mVideoView:Landroid/view/View;

    #@5
    .line 387
    return-void
.end method


# virtual methods
.method public hide()V
    .registers 3

    #@0
    .prologue
    .line 399
    iget-object v0, p0, Landroid/webkit/HTML5VideoFullScreen$FullScreenMediaController;->mVideoView:Landroid/view/View;

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 400
    iget-object v0, p0, Landroid/webkit/HTML5VideoFullScreen$FullScreenMediaController;->mVideoView:Landroid/view/View;

    #@6
    const/4 v1, 0x3

    #@7
    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    #@a
    .line 403
    :cond_a
    invoke-super {p0}, Landroid/widget/MediaController;->hide()V

    #@d
    .line 404
    return-void
.end method

.method public show()V
    .registers 3

    #@0
    .prologue
    .line 391
    invoke-super {p0}, Landroid/widget/MediaController;->show()V

    #@3
    .line 392
    iget-object v0, p0, Landroid/webkit/HTML5VideoFullScreen$FullScreenMediaController;->mVideoView:Landroid/view/View;

    #@5
    if-eqz v0, :cond_d

    #@7
    .line 393
    iget-object v0, p0, Landroid/webkit/HTML5VideoFullScreen$FullScreenMediaController;->mVideoView:Landroid/view/View;

    #@9
    const/4 v1, 0x0

    #@a
    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    #@d
    .line 395
    :cond_d
    return-void
.end method
