.class Landroid/webkit/WebIconDatabaseClassic$EventHandler;
.super Landroid/os/Handler;
.source "WebIconDatabaseClassic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/WebIconDatabaseClassic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EventHandler"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/WebIconDatabaseClassic$EventHandler$IconResult;
    }
.end annotation


# static fields
.field static final BULK_REQUEST_ICON:I = 0x6

.field static final CLOSE:I = 0x1

.field private static final ICON_RESULT:I = 0xa

.field static final OPEN:I = 0x0

.field static final RELEASE_ICON:I = 0x5

.field static final REMOVE_ALL:I = 0x2

.field static final REQUEST_ICON:I = 0x3

.field static final RETAIN_ICON:I = 0x4


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mMessages:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Landroid/os/Message;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 40
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@3
    .line 54
    new-instance v0, Ljava/util/Vector;

    #@5
    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    #@8
    iput-object v0, p0, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->mMessages:Ljava/util/Vector;

    #@a
    .line 56
    return-void
.end method

.method synthetic constructor <init>(Landroid/webkit/WebIconDatabaseClassic$1;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 40
    invoke-direct {p0}, Landroid/webkit/WebIconDatabaseClassic$EventHandler;-><init>()V

    #@3
    return-void
.end method

.method static synthetic access$1000(Landroid/webkit/WebIconDatabaseClassic$EventHandler;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 40
    invoke-direct {p0}, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->hasHandler()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1100(Landroid/webkit/WebIconDatabaseClassic$EventHandler;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 40
    invoke-direct {p0}, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->createHandler()V

    #@3
    return-void
.end method

.method static synthetic access$400(Landroid/webkit/WebIconDatabaseClassic$EventHandler;Ljava/lang/String;Landroid/webkit/WebIconDatabase$IconListener;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->requestIconAndSendResult(Ljava/lang/String;Landroid/webkit/WebIconDatabase$IconListener;)V

    #@3
    return-void
.end method

.method static synthetic access$500(Landroid/webkit/WebIconDatabaseClassic$EventHandler;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 40
    invoke-direct {p0, p1}, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->bulkRequestIcons(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$900(Landroid/webkit/WebIconDatabaseClassic$EventHandler;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 40
    invoke-direct {p0, p1}, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->postMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method private bulkRequestIcons(Landroid/os/Message;)V
    .registers 13
    .parameter "msg"

    #@0
    .prologue
    .line 142
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2
    check-cast v9, Ljava/util/HashMap;

    #@4
    .line 143
    .local v9, map:Ljava/util/HashMap;
    const-string/jumbo v1, "listener"

    #@7
    invoke-virtual {v9, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    move-result-object v8

    #@b
    check-cast v8, Landroid/webkit/WebIconDatabase$IconListener;

    #@d
    .line 144
    .local v8, listener:Landroid/webkit/WebIconDatabase$IconListener;
    const-string v1, "contentResolver"

    #@f
    invoke-virtual {v9, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Landroid/content/ContentResolver;

    #@15
    .line 145
    .local v0, cr:Landroid/content/ContentResolver;
    const-string/jumbo v1, "where"

    #@18
    invoke-virtual {v9, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1b
    move-result-object v3

    #@1c
    check-cast v3, Ljava/lang/String;

    #@1e
    .line 147
    .local v3, where:Ljava/lang/String;
    const/4 v6, 0x0

    #@1f
    .line 149
    .local v6, c:Landroid/database/Cursor;
    :try_start_1f
    sget-object v1, Landroid/provider/Browser;->BOOKMARKS_URI:Landroid/net/Uri;

    #@21
    const/4 v2, 0x1

    #@22
    new-array v2, v2, [Ljava/lang/String;

    #@24
    const/4 v4, 0x0

    #@25
    const-string/jumbo v5, "url"

    #@28
    aput-object v5, v2, v4

    #@2a
    const/4 v4, 0x0

    #@2b
    const/4 v5, 0x0

    #@2c
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@2f
    move-result-object v6

    #@30
    .line 153
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@33
    move-result v1

    #@34
    if-eqz v1, :cond_44

    #@36
    .line 155
    :cond_36
    const/4 v1, 0x0

    #@37
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@3a
    move-result-object v10

    #@3b
    .line 156
    .local v10, url:Ljava/lang/String;
    invoke-direct {p0, v10, v8}, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->requestIconAndSendResult(Ljava/lang/String;Landroid/webkit/WebIconDatabase$IconListener;)V

    #@3e
    .line 157
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_41
    .catchall {:try_start_1f .. :try_end_41} :catchall_55
    .catch Ljava/lang/IllegalStateException; {:try_start_1f .. :try_end_41} :catch_4a

    #@41
    move-result v1

    #@42
    if-nez v1, :cond_36

    #@44
    .line 162
    .end local v10           #url:Ljava/lang/String;
    :cond_44
    if-eqz v6, :cond_49

    #@46
    :goto_46
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@49
    .line 164
    :cond_49
    return-void

    #@4a
    .line 159
    :catch_4a
    move-exception v7

    #@4b
    .line 160
    .local v7, e:Ljava/lang/IllegalStateException;
    :try_start_4b
    const-string v1, "WebIconDatabase"

    #@4d
    const-string v2, "BulkRequestIcons"

    #@4f
    invoke-static {v1, v2, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_52
    .catchall {:try_start_4b .. :try_end_52} :catchall_55

    #@52
    .line 162
    if-eqz v6, :cond_49

    #@54
    goto :goto_46

    #@55
    .end local v7           #e:Ljava/lang/IllegalStateException;
    :catchall_55
    move-exception v1

    #@56
    if-eqz v6, :cond_5b

    #@58
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@5b
    :cond_5b
    throw v1
.end method

.method private declared-synchronized createHandler()V
    .registers 5

    #@0
    .prologue
    .line 82
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v1, p0, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->mHandler:Landroid/os/Handler;

    #@3
    if-nez v1, :cond_28

    #@5
    .line 83
    new-instance v1, Landroid/webkit/WebIconDatabaseClassic$EventHandler$1;

    #@7
    invoke-direct {v1, p0}, Landroid/webkit/WebIconDatabaseClassic$EventHandler$1;-><init>(Landroid/webkit/WebIconDatabaseClassic$EventHandler;)V

    #@a
    iput-object v1, p0, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->mHandler:Landroid/os/Handler;

    #@c
    .line 122
    iget-object v1, p0, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->mMessages:Ljava/util/Vector;

    #@e
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    #@11
    move-result v0

    #@12
    .local v0, size:I
    :goto_12
    if-lez v0, :cond_25

    #@14
    .line 123
    iget-object v2, p0, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->mHandler:Landroid/os/Handler;

    #@16
    iget-object v1, p0, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->mMessages:Ljava/util/Vector;

    #@18
    const/4 v3, 0x0

    #@19
    invoke-virtual {v1, v3}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    #@1c
    move-result-object v1

    #@1d
    check-cast v1, Landroid/os/Message;

    #@1f
    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@22
    .line 122
    add-int/lit8 v0, v0, -0x1

    #@24
    goto :goto_12

    #@25
    .line 125
    :cond_25
    const/4 v1, 0x0

    #@26
    iput-object v1, p0, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->mMessages:Ljava/util/Vector;
    :try_end_28
    .catchall {:try_start_1 .. :try_end_28} :catchall_2a

    #@28
    .line 127
    .end local v0           #size:I
    :cond_28
    monitor-exit p0

    #@29
    return-void

    #@2a
    .line 82
    :catchall_2a
    move-exception v1

    #@2b
    monitor-exit p0

    #@2c
    throw v1
.end method

.method private declared-synchronized hasHandler()Z
    .registers 2

    #@0
    .prologue
    .line 130
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->mHandler:Landroid/os/Handler;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_a

    #@3
    if-eqz v0, :cond_8

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    monitor-exit p0

    #@7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_6

    #@a
    :catchall_a
    move-exception v0

    #@b
    monitor-exit p0

    #@c
    throw v0
.end method

.method private declared-synchronized postMessage(Landroid/os/Message;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 134
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->mMessages:Ljava/util/Vector;

    #@3
    if-eqz v0, :cond_c

    #@5
    .line 135
    iget-object v0, p0, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->mMessages:Ljava/util/Vector;

    #@7
    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_12

    #@a
    .line 139
    :goto_a
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 137
    :cond_c
    :try_start_c
    iget-object v0, p0, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->mHandler:Landroid/os/Handler;

    #@e
    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_11
    .catchall {:try_start_c .. :try_end_11} :catchall_12

    #@11
    goto :goto_a

    #@12
    .line 134
    :catchall_12
    move-exception v0

    #@13
    monitor-exit p0

    #@14
    throw v0
.end method

.method private requestIconAndSendResult(Ljava/lang/String;Landroid/webkit/WebIconDatabase$IconListener;)V
    .registers 6
    .parameter "url"
    .parameter "listener"

    #@0
    .prologue
    .line 167
    invoke-static {p1}, Landroid/webkit/WebIconDatabaseClassic;->access$800(Ljava/lang/String;)Landroid/graphics/Bitmap;

    #@3
    move-result-object v0

    #@4
    .line 168
    .local v0, icon:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_14

    #@6
    .line 169
    const/16 v1, 0xa

    #@8
    new-instance v2, Landroid/webkit/WebIconDatabaseClassic$EventHandler$IconResult;

    #@a
    invoke-direct {v2, p0, p1, v0, p2}, Landroid/webkit/WebIconDatabaseClassic$EventHandler$IconResult;-><init>(Landroid/webkit/WebIconDatabaseClassic$EventHandler;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/webkit/WebIconDatabase$IconListener;)V

    #@d
    invoke-virtual {p0, v1, v2}, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {p0, v1}, Landroid/webkit/WebIconDatabaseClassic$EventHandler;->sendMessage(Landroid/os/Message;)Z

    #@14
    .line 172
    :cond_14
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 73
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v0, :pswitch_data_e

    #@5
    .line 78
    :goto_5
    return-void

    #@6
    .line 75
    :pswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8
    check-cast v0, Landroid/webkit/WebIconDatabaseClassic$EventHandler$IconResult;

    #@a
    invoke-virtual {v0}, Landroid/webkit/WebIconDatabaseClassic$EventHandler$IconResult;->dispatch()V

    #@d
    goto :goto_5

    #@e
    .line 73
    :pswitch_data_e
    .packed-switch 0xa
        :pswitch_6
    .end packed-switch
.end method
