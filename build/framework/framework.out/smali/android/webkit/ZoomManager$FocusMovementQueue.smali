.class Landroid/webkit/ZoomManager$FocusMovementQueue;
.super Ljava/lang/Object;
.source "ZoomManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/ZoomManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FocusMovementQueue"
.end annotation


# static fields
.field private static final QUEUE_CAPACITY:I = 0x5


# instance fields
.field private mIndex:I

.field private mQueue:[F

.field private mSize:I

.field private mSum:F

.field final synthetic this$0:Landroid/webkit/ZoomManager;


# direct methods
.method constructor <init>(Landroid/webkit/ZoomManager;)V
    .registers 4
    .parameter

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 787
    iput-object p1, p0, Landroid/webkit/ZoomManager$FocusMovementQueue;->this$0:Landroid/webkit/ZoomManager;

    #@3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 788
    const/4 v0, 0x5

    #@7
    new-array v0, v0, [F

    #@9
    iput-object v0, p0, Landroid/webkit/ZoomManager$FocusMovementQueue;->mQueue:[F

    #@b
    .line 789
    iput v1, p0, Landroid/webkit/ZoomManager$FocusMovementQueue;->mSize:I

    #@d
    .line 790
    const/4 v0, 0x0

    #@e
    iput v0, p0, Landroid/webkit/ZoomManager$FocusMovementQueue;->mSum:F

    #@10
    .line 791
    iput v1, p0, Landroid/webkit/ZoomManager$FocusMovementQueue;->mIndex:I

    #@12
    .line 792
    return-void
.end method

.method static synthetic access$300(Landroid/webkit/ZoomManager$FocusMovementQueue;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 780
    invoke-direct {p0}, Landroid/webkit/ZoomManager$FocusMovementQueue;->clear()V

    #@3
    return-void
.end method

.method static synthetic access$700(Landroid/webkit/ZoomManager$FocusMovementQueue;F)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 780
    invoke-direct {p0, p1}, Landroid/webkit/ZoomManager$FocusMovementQueue;->add(F)V

    #@3
    return-void
.end method

.method static synthetic access$800(Landroid/webkit/ZoomManager$FocusMovementQueue;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 780
    invoke-direct {p0}, Landroid/webkit/ZoomManager$FocusMovementQueue;->getSum()F

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private add(F)V
    .registers 5
    .parameter "focusDelta"

    #@0
    .prologue
    .line 804
    iget v0, p0, Landroid/webkit/ZoomManager$FocusMovementQueue;->mSum:F

    #@2
    add-float/2addr v0, p1

    #@3
    iput v0, p0, Landroid/webkit/ZoomManager$FocusMovementQueue;->mSum:F

    #@5
    .line 805
    iget v0, p0, Landroid/webkit/ZoomManager$FocusMovementQueue;->mSize:I

    #@7
    const/4 v1, 0x5

    #@8
    if-ge v0, v1, :cond_1f

    #@a
    .line 806
    iget v0, p0, Landroid/webkit/ZoomManager$FocusMovementQueue;->mSize:I

    #@c
    add-int/lit8 v0, v0, 0x1

    #@e
    iput v0, p0, Landroid/webkit/ZoomManager$FocusMovementQueue;->mSize:I

    #@10
    .line 810
    :goto_10
    iget-object v0, p0, Landroid/webkit/ZoomManager$FocusMovementQueue;->mQueue:[F

    #@12
    iget v1, p0, Landroid/webkit/ZoomManager$FocusMovementQueue;->mIndex:I

    #@14
    aput p1, v0, v1

    #@16
    .line 811
    iget v0, p0, Landroid/webkit/ZoomManager$FocusMovementQueue;->mIndex:I

    #@18
    add-int/lit8 v0, v0, 0x1

    #@1a
    rem-int/lit8 v0, v0, 0x5

    #@1c
    iput v0, p0, Landroid/webkit/ZoomManager$FocusMovementQueue;->mIndex:I

    #@1e
    .line 812
    return-void

    #@1f
    .line 808
    :cond_1f
    iget v0, p0, Landroid/webkit/ZoomManager$FocusMovementQueue;->mSum:F

    #@21
    iget-object v1, p0, Landroid/webkit/ZoomManager$FocusMovementQueue;->mQueue:[F

    #@23
    iget v2, p0, Landroid/webkit/ZoomManager$FocusMovementQueue;->mIndex:I

    #@25
    aget v1, v1, v2

    #@27
    sub-float/2addr v0, v1

    #@28
    iput v0, p0, Landroid/webkit/ZoomManager$FocusMovementQueue;->mSum:F

    #@2a
    goto :goto_10
.end method

.method private clear()V
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 795
    iput v1, p0, Landroid/webkit/ZoomManager$FocusMovementQueue;->mSize:I

    #@4
    .line 796
    iput v2, p0, Landroid/webkit/ZoomManager$FocusMovementQueue;->mSum:F

    #@6
    .line 797
    iput v1, p0, Landroid/webkit/ZoomManager$FocusMovementQueue;->mIndex:I

    #@8
    .line 798
    const/4 v0, 0x0

    #@9
    .local v0, i:I
    :goto_9
    const/4 v1, 0x5

    #@a
    if-ge v0, v1, :cond_13

    #@c
    .line 799
    iget-object v1, p0, Landroid/webkit/ZoomManager$FocusMovementQueue;->mQueue:[F

    #@e
    aput v2, v1, v0

    #@10
    .line 798
    add-int/lit8 v0, v0, 0x1

    #@12
    goto :goto_9

    #@13
    .line 801
    :cond_13
    return-void
.end method

.method private getSum()F
    .registers 2

    #@0
    .prologue
    .line 815
    iget v0, p0, Landroid/webkit/ZoomManager$FocusMovementQueue;->mSum:F

    #@2
    return v0
.end method
