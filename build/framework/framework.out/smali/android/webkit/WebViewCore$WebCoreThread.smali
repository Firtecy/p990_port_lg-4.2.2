.class Landroid/webkit/WebViewCore$WebCoreThread;
.super Ljava/lang/Object;
.source "WebViewCore.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/WebViewCore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WebCoreThread"
.end annotation


# static fields
.field private static final INITIALIZE:I = 0x0

.field private static final REDUCE_PRIORITY:I = 0x1

.field private static final RESUME_PRIORITY:I = 0x2


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 785
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method synthetic constructor <init>(Landroid/webkit/WebViewCore$1;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 785
    invoke-direct {p0}, Landroid/webkit/WebViewCore$WebCoreThread;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public run()V
    .registers 3

    #@0
    .prologue
    .line 793
    invoke-static {}, Landroid/os/Looper;->prepare()V

    #@3
    .line 794
    invoke-static {}, Landroid/webkit/WebViewCore;->access$500()Landroid/os/Handler;

    #@6
    move-result-object v0

    #@7
    invoke-static {v0}, Ljunit/framework/Assert;->assertNull(Ljava/lang/Object;)V

    #@a
    .line 795
    const-class v1, Landroid/webkit/WebViewCore;

    #@c
    monitor-enter v1

    #@d
    .line 796
    :try_start_d
    new-instance v0, Landroid/webkit/WebViewCore$WebCoreThread$1;

    #@f
    invoke-direct {v0, p0}, Landroid/webkit/WebViewCore$WebCoreThread$1;-><init>(Landroid/webkit/WebViewCore$WebCoreThread;)V

    #@12
    invoke-static {v0}, Landroid/webkit/WebViewCore;->access$502(Landroid/os/Handler;)Landroid/os/Handler;

    #@15
    .line 855
    const-class v0, Landroid/webkit/WebViewCore;

    #@17
    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    #@1a
    .line 856
    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_d .. :try_end_1b} :catchall_1f

    #@1b
    .line 857
    invoke-static {}, Landroid/os/Looper;->loop()V

    #@1e
    .line 858
    return-void

    #@1f
    .line 856
    :catchall_1f
    move-exception v0

    #@20
    :try_start_20
    monitor-exit v1
    :try_end_21
    .catchall {:try_start_20 .. :try_end_21} :catchall_1f

    #@21
    throw v0
.end method
