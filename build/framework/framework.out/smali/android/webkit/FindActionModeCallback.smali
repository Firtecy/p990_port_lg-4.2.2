.class Landroid/webkit/FindActionModeCallback;
.super Ljava/lang/Object;
.source "FindActionModeCallback.java"

# interfaces
.implements Landroid/view/ActionMode$Callback;
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/FindActionModeCallback$NoAction;
    }
.end annotation


# instance fields
.field private mActionMode:Landroid/view/ActionMode;

.field private mActiveMatchIndex:I

.field private mCustomView:Landroid/view/View;

.field private mEditText:Landroid/widget/EditText;

.field private mGlobalVisibleOffset:Landroid/graphics/Point;

.field private mGlobalVisibleRect:Landroid/graphics/Rect;

.field private mInput:Landroid/view/inputmethod/InputMethodManager;

.field private mMatches:Landroid/widget/TextView;

.field private mMatchesFound:Z

.field private mNumberOfMatches:I

.field private mResources:Landroid/content/res/Resources;

.field private mWebView:Landroid/webkit/WebViewClassic;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 51
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 269
    new-instance v0, Landroid/graphics/Rect;

    #@6
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@9
    iput-object v0, p0, Landroid/webkit/FindActionModeCallback;->mGlobalVisibleRect:Landroid/graphics/Rect;

    #@b
    .line 270
    new-instance v0, Landroid/graphics/Point;

    #@d
    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    #@10
    iput-object v0, p0, Landroid/webkit/FindActionModeCallback;->mGlobalVisibleOffset:Landroid/graphics/Point;

    #@12
    .line 52
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@15
    move-result-object v0

    #@16
    const v1, 0x10900ea

    #@19
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@1c
    move-result-object v0

    #@1d
    iput-object v0, p0, Landroid/webkit/FindActionModeCallback;->mCustomView:Landroid/view/View;

    #@1f
    .line 54
    iget-object v0, p0, Landroid/webkit/FindActionModeCallback;->mCustomView:Landroid/view/View;

    #@21
    const v1, 0x1020003

    #@24
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@27
    move-result-object v0

    #@28
    check-cast v0, Landroid/widget/EditText;

    #@2a
    iput-object v0, p0, Landroid/webkit/FindActionModeCallback;->mEditText:Landroid/widget/EditText;

    #@2c
    .line 56
    iget-object v0, p0, Landroid/webkit/FindActionModeCallback;->mEditText:Landroid/widget/EditText;

    #@2e
    new-instance v1, Landroid/webkit/FindActionModeCallback$NoAction;

    #@30
    invoke-direct {v1}, Landroid/webkit/FindActionModeCallback$NoAction;-><init>()V

    #@33
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setCustomSelectionActionModeCallback(Landroid/view/ActionMode$Callback;)V

    #@36
    .line 57
    iget-object v0, p0, Landroid/webkit/FindActionModeCallback;->mEditText:Landroid/widget/EditText;

    #@38
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@3b
    .line 59
    iget-object v0, p0, Landroid/webkit/FindActionModeCallback;->mEditText:Landroid/widget/EditText;

    #@3d
    const/4 v1, 0x1

    #@3e
    invoke-virtual {v0, v2, v1}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;I)V

    #@41
    .line 60
    iget-object v0, p0, Landroid/webkit/FindActionModeCallback;->mEditText:Landroid/widget/EditText;

    #@43
    const v1, 0x2090158

    #@46
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    #@49
    .line 61
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@4b
    if-eqz v0, :cond_54

    #@4d
    .line 62
    iget-object v0, p0, Landroid/webkit/FindActionModeCallback;->mEditText:Landroid/widget/EditText;

    #@4f
    const-string v1, "DISABLE_BUBBLE_POPUP"

    #@51
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    #@54
    .line 63
    :cond_54
    const-string v0, ""

    #@56
    invoke-virtual {p0, v0}, Landroid/webkit/FindActionModeCallback;->setText(Ljava/lang/String;)V

    #@59
    .line 64
    iget-object v0, p0, Landroid/webkit/FindActionModeCallback;->mCustomView:Landroid/view/View;

    #@5b
    const v1, 0x10203bf

    #@5e
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@61
    move-result-object v0

    #@62
    check-cast v0, Landroid/widget/TextView;

    #@64
    iput-object v0, p0, Landroid/webkit/FindActionModeCallback;->mMatches:Landroid/widget/TextView;

    #@66
    .line 66
    const-string v0, "input_method"

    #@68
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@6b
    move-result-object v0

    #@6c
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    #@6e
    iput-object v0, p0, Landroid/webkit/FindActionModeCallback;->mInput:Landroid/view/inputmethod/InputMethodManager;

    #@70
    .line 68
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@73
    move-result-object v0

    #@74
    iput-object v0, p0, Landroid/webkit/FindActionModeCallback;->mResources:Landroid/content/res/Resources;

    #@76
    .line 69
    return-void
.end method

.method private findNext(Z)V
    .registers 4
    .parameter "next"

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Landroid/webkit/FindActionModeCallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 112
    new-instance v0, Ljava/lang/AssertionError;

    #@6
    const-string v1, "No WebView for FindActionModeCallback::findNext"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    #@b
    throw v0

    #@c
    .line 115
    :cond_c
    iget-boolean v0, p0, Landroid/webkit/FindActionModeCallback;->mMatchesFound:Z

    #@e
    if-nez v0, :cond_14

    #@10
    .line 116
    invoke-virtual {p0}, Landroid/webkit/FindActionModeCallback;->findAll()V

    #@13
    .line 126
    :cond_13
    :goto_13
    return-void

    #@14
    .line 119
    :cond_14
    iget v0, p0, Landroid/webkit/FindActionModeCallback;->mNumberOfMatches:I

    #@16
    if-eqz v0, :cond_13

    #@18
    .line 124
    iget-object v0, p0, Landroid/webkit/FindActionModeCallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@1a
    invoke-virtual {v0, p1}, Landroid/webkit/WebViewClassic;->findNext(Z)V

    #@1d
    .line 125
    invoke-direct {p0}, Landroid/webkit/FindActionModeCallback;->updateMatchesString()V

    #@20
    goto :goto_13
.end method

.method private updateMatchesString()V
    .registers 4

    #@0
    .prologue
    .line 173
    iget-object v0, p0, Landroid/webkit/FindActionModeCallback;->mMatches:Landroid/widget/TextView;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    iget v2, p0, Landroid/webkit/FindActionModeCallback;->mActiveMatchIndex:I

    #@9
    add-int/lit8 v2, v2, 0x1

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    const-string v2, "/"

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    iget v2, p0, Landroid/webkit/FindActionModeCallback;->mNumberOfMatches:I

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@22
    .line 175
    iget-object v0, p0, Landroid/webkit/FindActionModeCallback;->mMatches:Landroid/widget/TextView;

    #@24
    const/4 v1, 0x0

    #@25
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    #@28
    .line 176
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 267
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter "s"
    .parameter "start"
    .parameter "count"
    .parameter "after"

    #@0
    .prologue
    .line 254
    return-void
.end method

.method findAll()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 132
    iget-object v1, p0, Landroid/webkit/FindActionModeCallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@3
    if-nez v1, :cond_d

    #@5
    .line 133
    new-instance v1, Ljava/lang/AssertionError;

    #@7
    const-string v2, "No WebView for FindActionModeCallback::findAll"

    #@9
    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    #@c
    throw v1

    #@d
    .line 136
    :cond_d
    iget-object v1, p0, Landroid/webkit/FindActionModeCallback;->mEditText:Landroid/widget/EditText;

    #@f
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@12
    move-result-object v0

    #@13
    .line 137
    .local v0, find:Ljava/lang/CharSequence;
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    #@16
    move-result v1

    #@17
    if-nez v1, :cond_27

    #@19
    .line 138
    iget-object v1, p0, Landroid/webkit/FindActionModeCallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@1b
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->clearMatches()V

    #@1e
    .line 140
    iput-boolean v2, p0, Landroid/webkit/FindActionModeCallback;->mMatchesFound:Z

    #@20
    .line 141
    iget-object v1, p0, Landroid/webkit/FindActionModeCallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@22
    const/4 v2, 0x0

    #@23
    invoke-virtual {v1, v2}, Landroid/webkit/WebViewClassic;->findAll(Ljava/lang/String;)I

    #@26
    .line 148
    :goto_26
    return-void

    #@27
    .line 143
    :cond_27
    const/4 v1, 0x1

    #@28
    iput-boolean v1, p0, Landroid/webkit/FindActionModeCallback;->mMatchesFound:Z

    #@2a
    .line 145
    iput v2, p0, Landroid/webkit/FindActionModeCallback;->mNumberOfMatches:I

    #@2c
    .line 146
    iget-object v1, p0, Landroid/webkit/FindActionModeCallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@2e
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@31
    move-result-object v2

    #@32
    invoke-virtual {v1, v2}, Landroid/webkit/WebViewClassic;->findAllAsync(Ljava/lang/String;)V

    #@35
    goto :goto_26
.end method

.method finish()V
    .registers 2

    #@0
    .prologue
    .line 72
    iget-object v0, p0, Landroid/webkit/FindActionModeCallback;->mActionMode:Landroid/view/ActionMode;

    #@2
    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    #@5
    .line 73
    return-void
.end method

.method public getActionModeGlobalBottom()I
    .registers 4

    #@0
    .prologue
    .line 272
    iget-object v1, p0, Landroid/webkit/FindActionModeCallback;->mActionMode:Landroid/view/ActionMode;

    #@2
    if-nez v1, :cond_6

    #@4
    .line 273
    const/4 v1, 0x0

    #@5
    .line 280
    :goto_5
    return v1

    #@6
    .line 275
    :cond_6
    iget-object v1, p0, Landroid/webkit/FindActionModeCallback;->mCustomView:Landroid/view/View;

    #@8
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Landroid/view/View;

    #@e
    .line 276
    .local v0, view:Landroid/view/View;
    if-nez v0, :cond_12

    #@10
    .line 277
    iget-object v0, p0, Landroid/webkit/FindActionModeCallback;->mCustomView:Landroid/view/View;

    #@12
    .line 279
    :cond_12
    iget-object v1, p0, Landroid/webkit/FindActionModeCallback;->mGlobalVisibleRect:Landroid/graphics/Rect;

    #@14
    iget-object v2, p0, Landroid/webkit/FindActionModeCallback;->mGlobalVisibleOffset:Landroid/graphics/Point;

    #@16
    invoke-virtual {v0, v1, v2}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    #@19
    .line 280
    iget-object v1, p0, Landroid/webkit/FindActionModeCallback;->mGlobalVisibleRect:Landroid/graphics/Rect;

    #@1b
    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    #@1d
    goto :goto_5
.end method

.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .registers 7
    .parameter "mode"
    .parameter "item"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 228
    iget-object v2, p0, Landroid/webkit/FindActionModeCallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@4
    if-nez v2, :cond_e

    #@6
    .line 229
    new-instance v0, Ljava/lang/AssertionError;

    #@8
    const-string v1, "No WebView for FindActionModeCallback::onActionItemClicked"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    #@d
    throw v0

    #@e
    .line 232
    :cond_e
    iget-object v2, p0, Landroid/webkit/FindActionModeCallback;->mInput:Landroid/view/inputmethod/InputMethodManager;

    #@10
    iget-object v3, p0, Landroid/webkit/FindActionModeCallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@12
    invoke-virtual {v3}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v3}, Landroid/webkit/WebView;->getWindowToken()Landroid/os/IBinder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v2, v3, v0}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    #@1d
    .line 233
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    #@20
    move-result v2

    #@21
    packed-switch v2, :pswitch_data_2e

    #@24
    .line 243
    :goto_24
    return v0

    #@25
    .line 235
    :pswitch_25
    invoke-direct {p0, v0}, Landroid/webkit/FindActionModeCallback;->findNext(Z)V

    #@28
    :goto_28
    move v0, v1

    #@29
    .line 243
    goto :goto_24

    #@2a
    .line 238
    :pswitch_2a
    invoke-direct {p0, v1}, Landroid/webkit/FindActionModeCallback;->findNext(Z)V

    #@2d
    goto :goto_28

    #@2e
    .line 233
    :pswitch_data_2e
    .packed-switch 0x10203ce
        :pswitch_25
        :pswitch_2a
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 182
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, v0}, Landroid/webkit/FindActionModeCallback;->findNext(Z)V

    #@4
    .line 183
    return-void
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .registers 7
    .parameter "mode"
    .parameter "menu"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 189
    invoke-virtual {p1}, Landroid/view/ActionMode;->isUiFocusable()Z

    #@4
    move-result v2

    #@5
    if-nez v2, :cond_8

    #@7
    .line 210
    :goto_7
    return v1

    #@8
    .line 197
    :cond_8
    iget-object v2, p0, Landroid/webkit/FindActionModeCallback;->mCustomView:Landroid/view/View;

    #@a
    invoke-virtual {p1, v2}, Landroid/view/ActionMode;->setCustomView(Landroid/view/View;)V

    #@d
    .line 198
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    #@10
    move-result-object v2

    #@11
    const v3, 0x1140001

    #@14
    invoke-virtual {v2, v3, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    #@17
    .line 200
    iput-object p1, p0, Landroid/webkit/FindActionModeCallback;->mActionMode:Landroid/view/ActionMode;

    #@19
    .line 201
    iget-object v2, p0, Landroid/webkit/FindActionModeCallback;->mEditText:Landroid/widget/EditText;

    #@1b
    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@1e
    move-result-object v0

    #@1f
    .line 202
    .local v0, edit:Landroid/text/Editable;
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    #@22
    move-result v2

    #@23
    invoke-static {v0, v2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@26
    .line 205
    iget-object v2, p0, Landroid/webkit/FindActionModeCallback;->mMatches:Landroid/widget/TextView;

    #@28
    const/4 v3, 0x4

    #@29
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    #@2c
    .line 207
    iput-boolean v1, p0, Landroid/webkit/FindActionModeCallback;->mMatchesFound:Z

    #@2e
    .line 208
    iget-object v1, p0, Landroid/webkit/FindActionModeCallback;->mMatches:Landroid/widget/TextView;

    #@30
    const-string v2, "0/0"

    #@32
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@35
    .line 209
    iget-object v1, p0, Landroid/webkit/FindActionModeCallback;->mEditText:Landroid/widget/EditText;

    #@37
    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    #@3a
    .line 210
    const/4 v1, 0x1

    #@3b
    goto :goto_7
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .registers 5
    .parameter "mode"

    #@0
    .prologue
    .line 215
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Landroid/webkit/FindActionModeCallback;->mActionMode:Landroid/view/ActionMode;

    #@3
    .line 216
    iget-object v0, p0, Landroid/webkit/FindActionModeCallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@5
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->notifyFindDialogDismissed()V

    #@8
    .line 217
    iget-object v0, p0, Landroid/webkit/FindActionModeCallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@a
    invoke-virtual {v0}, Landroid/webkit/WebViewClassic;->closedFindDialog()Z

    #@d
    .line 218
    iget-object v0, p0, Landroid/webkit/FindActionModeCallback;->mInput:Landroid/view/inputmethod/InputMethodManager;

    #@f
    iget-object v1, p0, Landroid/webkit/FindActionModeCallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@11
    invoke-virtual {v1}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Landroid/webkit/WebView;->getWindowToken()Landroid/os/IBinder;

    #@18
    move-result-object v1

    #@19
    const/4 v2, 0x0

    #@1a
    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    #@1d
    .line 219
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .registers 4
    .parameter "mode"
    .parameter "menu"

    #@0
    .prologue
    .line 223
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter "s"
    .parameter "start"
    .parameter "before"
    .parameter "count"

    #@0
    .prologue
    .line 261
    invoke-virtual {p0}, Landroid/webkit/FindActionModeCallback;->findAll()V

    #@3
    .line 262
    return-void
.end method

.method setText(Ljava/lang/String;)V
    .registers 6
    .parameter "text"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 80
    iget-object v2, p0, Landroid/webkit/FindActionModeCallback;->mEditText:Landroid/widget/EditText;

    #@3
    invoke-virtual {v2, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    #@6
    .line 81
    iget-object v2, p0, Landroid/webkit/FindActionModeCallback;->mEditText:Landroid/widget/EditText;

    #@8
    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@b
    move-result-object v1

    #@c
    .line 82
    .local v1, span:Landroid/text/Spannable;
    invoke-interface {v1}, Landroid/text/Spannable;->length()I

    #@f
    move-result v0

    #@10
    .line 86
    .local v0, length:I
    invoke-static {v1, v0, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    #@13
    .line 89
    const/16 v2, 0x12

    #@15
    invoke-interface {v1, p0, v3, v0, v2}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@18
    .line 90
    iput-boolean v3, p0, Landroid/webkit/FindActionModeCallback;->mMatchesFound:Z

    #@1a
    .line 91
    return-void
.end method

.method setWebView(Landroid/webkit/WebViewClassic;)V
    .registers 4
    .parameter "webView"

    #@0
    .prologue
    .line 98
    if-nez p1, :cond_a

    #@2
    .line 99
    new-instance v0, Ljava/lang/AssertionError;

    #@4
    const-string v1, "WebView supplied to FindActionModeCallback cannot be null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    #@9
    throw v0

    #@a
    .line 102
    :cond_a
    iput-object p1, p0, Landroid/webkit/FindActionModeCallback;->mWebView:Landroid/webkit/WebViewClassic;

    #@c
    .line 103
    return-void
.end method

.method public showSoftInput()V
    .registers 4

    #@0
    .prologue
    .line 151
    iget-object v0, p0, Landroid/webkit/FindActionModeCallback;->mInput:Landroid/view/inputmethod/InputMethodManager;

    #@2
    iget-object v1, p0, Landroid/webkit/FindActionModeCallback;->mEditText:Landroid/widget/EditText;

    #@4
    invoke-virtual {v1}, Landroid/widget/EditText;->getRootView()Landroid/view/View;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->startGettingWindowFocus(Landroid/view/View;)V

    #@b
    .line 152
    iget-object v0, p0, Landroid/webkit/FindActionModeCallback;->mInput:Landroid/view/inputmethod/InputMethodManager;

    #@d
    iget-object v1, p0, Landroid/webkit/FindActionModeCallback;->mEditText:Landroid/widget/EditText;

    #@f
    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->focusIn(Landroid/view/View;)V

    #@12
    .line 153
    iget-object v0, p0, Landroid/webkit/FindActionModeCallback;->mInput:Landroid/view/inputmethod/InputMethodManager;

    #@14
    iget-object v1, p0, Landroid/webkit/FindActionModeCallback;->mEditText:Landroid/widget/EditText;

    #@16
    const/4 v2, 0x0

    #@17
    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    #@1a
    .line 154
    return-void
.end method

.method public updateMatchCount(IIZ)V
    .registers 6
    .parameter "matchIndex"
    .parameter "matchCount"
    .parameter "isEmptyFind"

    #@0
    .prologue
    .line 157
    if-nez p3, :cond_a

    #@2
    .line 158
    iput p2, p0, Landroid/webkit/FindActionModeCallback;->mNumberOfMatches:I

    #@4
    .line 159
    iput p1, p0, Landroid/webkit/FindActionModeCallback;->mActiveMatchIndex:I

    #@6
    .line 160
    invoke-direct {p0}, Landroid/webkit/FindActionModeCallback;->updateMatchesString()V

    #@9
    .line 165
    :goto_9
    return-void

    #@a
    .line 162
    :cond_a
    iget-object v0, p0, Landroid/webkit/FindActionModeCallback;->mMatches:Landroid/widget/TextView;

    #@c
    const/4 v1, 0x4

    #@d
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    #@10
    .line 163
    const/4 v0, 0x0

    #@11
    iput v0, p0, Landroid/webkit/FindActionModeCallback;->mNumberOfMatches:I

    #@13
    goto :goto_9
.end method
