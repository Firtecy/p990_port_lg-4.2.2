.class Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;
.super Landroid/widget/FrameLayout;
.source "ZoomControlExternal.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/ZoomControlExternal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ExtendedZoomControls"
.end annotation


# instance fields
.field private mPlusMinusZoomControls:Landroid/widget/ZoomControls;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 121
    const/4 v1, 0x0

    #@1
    invoke-direct {p0, p1, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 122
    const-string/jumbo v1, "layout_inflater"

    #@7
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@a
    move-result-object v0

    #@b
    check-cast v0, Landroid/view/LayoutInflater;

    #@d
    .line 124
    .local v0, inflater:Landroid/view/LayoutInflater;
    const v1, 0x10900f1

    #@10
    const/4 v2, 0x1

    #@11
    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@14
    .line 125
    const v1, 0x10203c5

    #@17
    invoke-virtual {p0, v1}, Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;->findViewById(I)Landroid/view/View;

    #@1a
    move-result-object v1

    #@1b
    check-cast v1, Landroid/widget/ZoomControls;

    #@1d
    iput-object v1, p0, Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;->mPlusMinusZoomControls:Landroid/widget/ZoomControls;

    #@1f
    .line 127
    const v1, 0x10203c8

    #@22
    invoke-virtual {p0, v1}, Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;->findViewById(I)Landroid/view/View;

    #@25
    move-result-object v1

    #@26
    const/16 v2, 0x8

    #@28
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    #@2b
    .line 129
    return-void
.end method

.method private fade(IFF)V
    .registers 7
    .parameter "visibility"
    .parameter "startAlpha"
    .parameter "endAlpha"

    #@0
    .prologue
    .line 141
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    #@2
    invoke-direct {v0, p2, p3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    #@5
    .line 142
    .local v0, anim:Landroid/view/animation/AlphaAnimation;
    const-wide/16 v1, 0x1f4

    #@7
    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    #@a
    .line 143
    invoke-virtual {p0, v0}, Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;->startAnimation(Landroid/view/animation/Animation;)V

    #@d
    .line 144
    invoke-virtual {p0, p1}, Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;->setVisibility(I)V

    #@10
    .line 145
    return-void
.end method


# virtual methods
.method public hasFocus()Z
    .registers 2

    #@0
    .prologue
    .line 148
    iget-object v0, p0, Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;->mPlusMinusZoomControls:Landroid/widget/ZoomControls;

    #@2
    invoke-virtual {v0}, Landroid/widget/ZoomControls;->hasFocus()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public hide()V
    .registers 4

    #@0
    .prologue
    .line 137
    const/16 v0, 0x8

    #@2
    const/high16 v1, 0x3f80

    #@4
    const/4 v2, 0x0

    #@5
    invoke-direct {p0, v0, v1, v2}, Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;->fade(IFF)V

    #@8
    .line 138
    return-void
.end method

.method public setOnZoomInClickListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 152
    iget-object v0, p0, Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;->mPlusMinusZoomControls:Landroid/widget/ZoomControls;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/ZoomControls;->setOnZoomInClickListener(Landroid/view/View$OnClickListener;)V

    #@5
    .line 153
    return-void
.end method

.method public setOnZoomOutClickListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 156
    iget-object v0, p0, Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;->mPlusMinusZoomControls:Landroid/widget/ZoomControls;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/ZoomControls;->setOnZoomOutClickListener(Landroid/view/View$OnClickListener;)V

    #@5
    .line 157
    return-void
.end method

.method public show(Z)V
    .registers 5
    .parameter "showZoom"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 132
    iget-object v2, p0, Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;->mPlusMinusZoomControls:Landroid/widget/ZoomControls;

    #@3
    if-eqz p1, :cond_10

    #@5
    move v0, v1

    #@6
    :goto_6
    invoke-virtual {v2, v0}, Landroid/widget/ZoomControls;->setVisibility(I)V

    #@9
    .line 133
    const/4 v0, 0x0

    #@a
    const/high16 v2, 0x3f80

    #@c
    invoke-direct {p0, v1, v0, v2}, Landroid/webkit/ZoomControlExternal$ExtendedZoomControls;->fade(IFF)V

    #@f
    .line 134
    return-void

    #@10
    .line 132
    :cond_10
    const/16 v0, 0x8

    #@12
    goto :goto_6
.end method
