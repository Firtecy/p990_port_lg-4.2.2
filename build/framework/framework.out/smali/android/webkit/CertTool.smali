.class final Landroid/webkit/CertTool;
.super Ljava/lang/Object;
.source "CertTool.java"


# static fields
.field private static final LOGTAG:Ljava/lang/String; = "CertTool"

.field private static final MD5_WITH_RSA:Lcom/android/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

.field private static sCertificateTypeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 37
    new-instance v0, Lcom/android/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    #@2
    sget-object v1, Lcom/android/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->md5WithRSAEncryption:Lcom/android/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    #@4
    invoke-direct {v0, v1}, Lcom/android/org/bouncycastle/asn1/x509/AlgorithmIdentifier;-><init>(Lcom/android/org/bouncycastle/asn1/ASN1ObjectIdentifier;)V

    #@7
    sput-object v0, Landroid/webkit/CertTool;->MD5_WITH_RSA:Lcom/android/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    #@9
    .line 42
    new-instance v0, Ljava/util/HashMap;

    #@b
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@e
    sput-object v0, Landroid/webkit/CertTool;->sCertificateTypeMap:Ljava/util/HashMap;

    #@10
    .line 43
    sget-object v0, Landroid/webkit/CertTool;->sCertificateTypeMap:Ljava/util/HashMap;

    #@12
    const-string v1, "application/x-x509-ca-cert"

    #@14
    const-string v2, "CERT"

    #@16
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@19
    .line 44
    sget-object v0, Landroid/webkit/CertTool;->sCertificateTypeMap:Ljava/util/HashMap;

    #@1b
    const-string v1, "application/x-x509-user-cert"

    #@1d
    const-string v2, "CERT"

    #@1f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    .line 45
    sget-object v0, Landroid/webkit/CertTool;->sCertificateTypeMap:Ljava/util/HashMap;

    #@24
    const-string v1, "application/x-pkcs12"

    #@26
    const-string v2, "PKCS12"

    #@28
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2b
    .line 46
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 79
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method static addCertificate(Landroid/content/Context;Ljava/lang/String;[B)V
    .registers 4
    .parameter "context"
    .parameter "type"
    .parameter "value"

    #@0
    .prologue
    .line 72
    invoke-static {}, Landroid/security/Credentials;->getInstance()Landroid/security/Credentials;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p0, p1, p2}, Landroid/security/Credentials;->install(Landroid/content/Context;Ljava/lang/String;[B)V

    #@7
    .line 73
    return-void
.end method

.method static getCertType(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "mimeType"

    #@0
    .prologue
    .line 76
    sget-object v0, Landroid/webkit/CertTool;->sCertificateTypeMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/lang/String;

    #@8
    return-object v0
.end method

.method static getKeyStrengthList()[Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 49
    const/4 v0, 0x2

    #@1
    new-array v0, v0, [Ljava/lang/String;

    #@3
    const/4 v1, 0x0

    #@4
    const-string v2, "High Grade"

    #@6
    aput-object v2, v0, v1

    #@8
    const/4 v1, 0x1

    #@9
    const-string v2, "Medium Grade"

    #@b
    aput-object v2, v0, v1

    #@d
    return-object v0
.end method

.method static getSignedPublicKey(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;
    .registers 10
    .parameter "context"
    .parameter "index"
    .parameter "challenge"

    #@0
    .prologue
    .line 54
    :try_start_0
    const-string v5, "RSA"

    #@2
    invoke-static {v5}, Ljava/security/KeyPairGenerator;->getInstance(Ljava/lang/String;)Ljava/security/KeyPairGenerator;

    #@5
    move-result-object v1

    #@6
    .line 55
    .local v1, generator:Ljava/security/KeyPairGenerator;
    if-nez p1, :cond_3e

    #@8
    const/16 v5, 0x800

    #@a
    :goto_a
    invoke-virtual {v1, v5}, Ljava/security/KeyPairGenerator;->initialize(I)V

    #@d
    .line 56
    invoke-virtual {v1}, Ljava/security/KeyPairGenerator;->genKeyPair()Ljava/security/KeyPair;

    #@10
    move-result-object v2

    #@11
    .line 58
    .local v2, pair:Ljava/security/KeyPair;
    new-instance v3, Lcom/android/org/bouncycastle/jce/netscape/NetscapeCertRequest;

    #@13
    sget-object v5, Landroid/webkit/CertTool;->MD5_WITH_RSA:Lcom/android/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    #@15
    invoke-virtual {v2}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    #@18
    move-result-object v6

    #@19
    invoke-direct {v3, p2, v5, v6}, Lcom/android/org/bouncycastle/jce/netscape/NetscapeCertRequest;-><init>(Ljava/lang/String;Lcom/android/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Ljava/security/PublicKey;)V

    #@1c
    .line 60
    .local v3, request:Lcom/android/org/bouncycastle/jce/netscape/NetscapeCertRequest;
    invoke-virtual {v2}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    #@1f
    move-result-object v5

    #@20
    invoke-virtual {v3, v5}, Lcom/android/org/bouncycastle/jce/netscape/NetscapeCertRequest;->sign(Ljava/security/PrivateKey;)V

    #@23
    .line 61
    invoke-virtual {v3}, Lcom/android/org/bouncycastle/jce/netscape/NetscapeCertRequest;->toASN1Primitive()Lcom/android/org/bouncycastle/asn1/ASN1Primitive;

    #@26
    move-result-object v5

    #@27
    const-string v6, "DER"

    #@29
    invoke-virtual {v5, v6}, Lcom/android/org/bouncycastle/asn1/ASN1Primitive;->getEncoded(Ljava/lang/String;)[B

    #@2c
    move-result-object v4

    #@2d
    .line 63
    .local v4, signed:[B
    invoke-static {}, Landroid/security/Credentials;->getInstance()Landroid/security/Credentials;

    #@30
    move-result-object v5

    #@31
    invoke-virtual {v5, p0, v2}, Landroid/security/Credentials;->install(Landroid/content/Context;Ljava/security/KeyPair;)V

    #@34
    .line 64
    new-instance v5, Ljava/lang/String;

    #@36
    invoke-static {v4}, Lcom/android/org/bouncycastle/util/encoders/Base64;->encode([B)[B

    #@39
    move-result-object v6

    #@3a
    invoke-direct {v5, v6}, Ljava/lang/String;-><init>([B)V
    :try_end_3d
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_3d} :catch_41

    #@3d
    .line 68
    .end local v1           #generator:Ljava/security/KeyPairGenerator;
    .end local v2           #pair:Ljava/security/KeyPair;
    .end local v3           #request:Lcom/android/org/bouncycastle/jce/netscape/NetscapeCertRequest;
    .end local v4           #signed:[B
    :goto_3d
    return-object v5

    #@3e
    .line 55
    .restart local v1       #generator:Ljava/security/KeyPairGenerator;
    :cond_3e
    const/16 v5, 0x400

    #@40
    goto :goto_a

    #@41
    .line 65
    .end local v1           #generator:Ljava/security/KeyPairGenerator;
    :catch_41
    move-exception v0

    #@42
    .line 66
    .local v0, e:Ljava/lang/Exception;
    const-string v5, "CertTool"

    #@44
    invoke-static {v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@47
    .line 68
    const/4 v5, 0x0

    #@48
    goto :goto_3d
.end method
