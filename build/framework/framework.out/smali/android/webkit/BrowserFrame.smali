.class Landroid/webkit/BrowserFrame;
.super Landroid/os/Handler;
.source "BrowserFrame.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/BrowserFrame$ConfigCallback;,
        Landroid/webkit/BrowserFrame$JSObject;
    }
.end annotation


# static fields
#the value of this static final field might be set in the static constructor
.field static final synthetic $assertionsDisabled:Z = false

.field static final DRAWABLEDIR:I = 0x3

.field private static final FILE_UPLOAD_LABEL:I = 0x4

.field private static final FILE_UPLOAD_NO_FILE_CHOSEN:I = 0x7

.field static final FRAME_COMPLETED:I = 0x3e9

.field static final FRAME_LOADTYPE_BACK:I = 0x1

.field static final FRAME_LOADTYPE_FORWARD:I = 0x2

.field static final FRAME_LOADTYPE_INDEXEDBACKFORWARD:I = 0x3

.field static final FRAME_LOADTYPE_REDIRECT:I = 0x7

.field static final FRAME_LOADTYPE_RELOAD:I = 0x4

.field static final FRAME_LOADTYPE_RELOADALLOWINGSTALEDATA:I = 0x5

.field static final FRAME_LOADTYPE_REPLACE:I = 0x8

.field static final FRAME_LOADTYPE_SAME:I = 0x6

.field static final FRAME_LOADTYPE_STANDARD:I = 0x0

.field private static final LOADERROR:I = 0x2

.field private static final LOGTAG:Ljava/lang/String; = "webkit"

.field private static final MAX_OUTSTANDING_REQUESTS:I = 0x12c

.field private static final NODOMAIN:I = 0x1

.field static final ORIENTATION_CHANGED:I = 0x3ea

.field static final POLICY_FUNCTION:I = 0x3eb

.field static final POLICY_IGNORE:I = 0x2

.field static final POLICY_USE:I = 0x0

.field private static final RESET_LABEL:I = 0x5

.field private static final SCHEME_HOST_DELIMITER:Ljava/lang/String; = "://"

.field private static final SUBMIT_LABEL:I = 0x6

.field private static final TRANSITION_SWITCH_THRESHOLD:I = 0x4b

.field static sConfigCallback:Landroid/webkit/BrowserFrame$ConfigCallback;

.field static sJavaBridge:Landroid/webkit/JWebCoreJavaBridge;


# instance fields
.field private mBlockMessages:Z

.field private final mCallbackProxy:Landroid/webkit/CallbackProxy;

.field private mCommitted:Z

.field private final mContext:Landroid/content/Context;

.field private final mDatabase:Landroid/webkit/WebViewDatabaseClassic;

.field private mFirstLayoutDone:Z

.field private mIsMainFrame:Z

.field private mJavaScriptObjects:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/webkit/BrowserFrame$JSObject;",
            ">;"
        }
    .end annotation
.end field

.field private mKeyStoreHandler:Landroid/webkit/KeyStoreHandler;

.field mLoadInitFromJava:Z

.field private mLoadType:I

.field mNativeFrame:I

.field private mOrientation:I

.field private mRemovedJavaScriptObjects:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final mSettings:Landroid/webkit/WebSettingsClassic;

.field private final mWebViewCore:Landroid/webkit/WebViewCore;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 62
    const-class v0, Landroid/webkit/BrowserFrame;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_c

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    sput-boolean v0, Landroid/webkit/BrowserFrame;->$assertionsDisabled:Z

    #@b
    return-void

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_9
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/webkit/WebViewCore;Landroid/webkit/CallbackProxy;Landroid/webkit/WebSettingsClassic;Ljava/util/Map;)V
    .registers 10
    .parameter "context"
    .parameter "w"
    .parameter "proxy"
    .parameter "settings"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/webkit/WebViewCore;",
            "Landroid/webkit/CallbackProxy;",
            "Landroid/webkit/WebSettingsClassic;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p5, javascriptInterfaces:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v2, 0x1

    #@1
    .line 214
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@4
    .line 82
    iput-boolean v2, p0, Landroid/webkit/BrowserFrame;->mFirstLayoutDone:Z

    #@6
    .line 83
    iput-boolean v2, p0, Landroid/webkit/BrowserFrame;->mCommitted:Z

    #@8
    .line 87
    const/4 v2, 0x0

    #@9
    iput-boolean v2, p0, Landroid/webkit/BrowserFrame;->mBlockMessages:Z

    #@b
    .line 88
    const/4 v2, -0x1

    #@c
    iput v2, p0, Landroid/webkit/BrowserFrame;->mOrientation:I

    #@e
    .line 109
    const/4 v2, 0x0

    #@f
    iput-object v2, p0, Landroid/webkit/BrowserFrame;->mKeyStoreHandler:Landroid/webkit/KeyStoreHandler;

    #@11
    .line 216
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@14
    move-result-object v1

    #@15
    .line 220
    .local v1, appContext:Landroid/content/Context;
    sget-object v2, Landroid/webkit/BrowserFrame;->sJavaBridge:Landroid/webkit/JWebCoreJavaBridge;

    #@17
    if-nez v2, :cond_40

    #@19
    .line 221
    new-instance v2, Landroid/webkit/JWebCoreJavaBridge;

    #@1b
    invoke-direct {v2}, Landroid/webkit/JWebCoreJavaBridge;-><init>()V

    #@1e
    sput-object v2, Landroid/webkit/BrowserFrame;->sJavaBridge:Landroid/webkit/JWebCoreJavaBridge;

    #@20
    .line 223
    const-string v2, "activity"

    #@22
    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@25
    move-result-object v0

    #@26
    check-cast v0, Landroid/app/ActivityManager;

    #@28
    .line 225
    .local v0, am:Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    #@2b
    move-result v2

    #@2c
    const/16 v3, 0x10

    #@2e
    if-le v2, v3, :cond_89

    #@30
    .line 226
    sget-object v2, Landroid/webkit/BrowserFrame;->sJavaBridge:Landroid/webkit/JWebCoreJavaBridge;

    #@32
    const/high16 v3, 0x80

    #@34
    invoke-virtual {v2, v3}, Landroid/webkit/JWebCoreJavaBridge;->setCacheSize(I)V

    #@37
    .line 231
    :goto_37
    invoke-static {v1}, Landroid/webkit/CacheManager;->init(Landroid/content/Context;)V

    #@3a
    .line 233
    invoke-static {v1}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    #@3d
    .line 235
    invoke-static {v1}, Landroid/webkit/PluginManager;->getInstance(Landroid/content/Context;)Landroid/webkit/PluginManager;

    #@40
    .line 238
    .end local v0           #am:Landroid/app/ActivityManager;
    :cond_40
    sget-object v2, Landroid/webkit/BrowserFrame;->sConfigCallback:Landroid/webkit/BrowserFrame$ConfigCallback;

    #@42
    if-nez v2, :cond_59

    #@44
    .line 239
    new-instance v3, Landroid/webkit/BrowserFrame$ConfigCallback;

    #@46
    const-string/jumbo v2, "window"

    #@49
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@4c
    move-result-object v2

    #@4d
    check-cast v2, Landroid/view/WindowManager;

    #@4f
    invoke-direct {v3, v2}, Landroid/webkit/BrowserFrame$ConfigCallback;-><init>(Landroid/view/WindowManager;)V

    #@52
    sput-object v3, Landroid/webkit/BrowserFrame;->sConfigCallback:Landroid/webkit/BrowserFrame$ConfigCallback;

    #@54
    .line 242
    sget-object v2, Landroid/webkit/BrowserFrame;->sConfigCallback:Landroid/webkit/BrowserFrame$ConfigCallback;

    #@56
    invoke-static {v2}, Landroid/view/ViewRootImpl;->addConfigCallback(Landroid/content/ComponentCallbacks;)V

    #@59
    .line 244
    :cond_59
    sget-object v2, Landroid/webkit/BrowserFrame;->sConfigCallback:Landroid/webkit/BrowserFrame$ConfigCallback;

    #@5b
    invoke-virtual {v2, p0}, Landroid/webkit/BrowserFrame$ConfigCallback;->addHandler(Landroid/os/Handler;)V

    #@5e
    .line 246
    new-instance v2, Ljava/util/HashMap;

    #@60
    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    #@63
    iput-object v2, p0, Landroid/webkit/BrowserFrame;->mJavaScriptObjects:Ljava/util/Map;

    #@65
    .line 247
    invoke-direct {p0, p5}, Landroid/webkit/BrowserFrame;->addJavaScriptObjects(Ljava/util/Map;)V

    #@68
    .line 248
    new-instance v2, Ljava/util/HashSet;

    #@6a
    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    #@6d
    iput-object v2, p0, Landroid/webkit/BrowserFrame;->mRemovedJavaScriptObjects:Ljava/util/Set;

    #@6f
    .line 250
    iput-object p4, p0, Landroid/webkit/BrowserFrame;->mSettings:Landroid/webkit/WebSettingsClassic;

    #@71
    .line 251
    iput-object p1, p0, Landroid/webkit/BrowserFrame;->mContext:Landroid/content/Context;

    #@73
    .line 252
    iput-object p3, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@75
    .line 253
    invoke-static {v1}, Landroid/webkit/WebViewDatabaseClassic;->getInstance(Landroid/content/Context;)Landroid/webkit/WebViewDatabaseClassic;

    #@78
    move-result-object v2

    #@79
    iput-object v2, p0, Landroid/webkit/BrowserFrame;->mDatabase:Landroid/webkit/WebViewDatabaseClassic;

    #@7b
    .line 254
    iput-object p2, p0, Landroid/webkit/BrowserFrame;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@7d
    .line 256
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    #@80
    move-result-object v0

    #@81
    .line 257
    .local v0, am:Landroid/content/res/AssetManager;
    invoke-virtual {p3}, Landroid/webkit/CallbackProxy;->getBackForwardList()Landroid/webkit/WebBackForwardListClassic;

    #@84
    move-result-object v2

    #@85
    invoke-direct {p0, p2, v0, v2}, Landroid/webkit/BrowserFrame;->nativeCreateFrame(Landroid/webkit/WebViewCore;Landroid/content/res/AssetManager;Landroid/webkit/WebBackForwardList;)V

    #@88
    .line 262
    return-void

    #@89
    .line 228
    .local v0, am:Landroid/app/ActivityManager;
    :cond_89
    sget-object v2, Landroid/webkit/BrowserFrame;->sJavaBridge:Landroid/webkit/JWebCoreJavaBridge;

    #@8b
    const/high16 v3, 0x40

    #@8d
    invoke-virtual {v2, v3}, Landroid/webkit/JWebCoreJavaBridge;->setCacheSize(I)V

    #@90
    goto :goto_37
.end method

.method static synthetic access$000(Landroid/webkit/BrowserFrame;ILjava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 62
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/BrowserFrame;->nativeAuthenticationProceed(ILjava/lang/String;Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$100(Landroid/webkit/BrowserFrame;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    invoke-direct {p0, p1}, Landroid/webkit/BrowserFrame;->nativeAuthenticationCancel(I)V

    #@3
    return-void
.end method

.method static synthetic access$200(Landroid/webkit/BrowserFrame;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    invoke-direct {p0, p1}, Landroid/webkit/BrowserFrame;->nativeSslCertErrorProceed(I)V

    #@3
    return-void
.end method

.method static synthetic access$300(Landroid/webkit/BrowserFrame;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Landroid/webkit/BrowserFrame;->nativeSslCertErrorCancel(II)V

    #@3
    return-void
.end method

.method private addJavaScriptObjects(Ljava/util/Map;)V
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 622
    .local p1, javascriptInterfaces:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-nez p1, :cond_3

    #@2
    .line 631
    :cond_2
    return-void

    #@3
    .line 623
    :cond_3
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    #@6
    move-result-object v3

    #@7
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v1

    #@b
    .line 624
    .local v1, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_b
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_2

    #@11
    .line 625
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Ljava/lang/String;

    #@17
    .line 626
    .local v0, interfaceName:Ljava/lang/String;
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1a
    move-result-object v2

    #@1b
    .line 627
    .local v2, object:Ljava/lang/Object;
    if-eqz v2, :cond_b

    #@1d
    .line 628
    iget-object v3, p0, Landroid/webkit/BrowserFrame;->mJavaScriptObjects:Ljava/util/Map;

    #@1f
    new-instance v4, Landroid/webkit/BrowserFrame$JSObject;

    #@21
    const/4 v5, 0x0

    #@22
    invoke-direct {v4, p0, v2, v5}, Landroid/webkit/BrowserFrame$JSObject;-><init>(Landroid/webkit/BrowserFrame;Ljava/lang/Object;Z)V

    #@25
    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@28
    goto :goto_b
.end method

.method private autoLogin(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "realm"
    .parameter "account"
    .parameter "args"

    #@0
    .prologue
    .line 1243
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/webkit/CallbackProxy;->onReceivedLoginRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 1244
    return-void
.end method

.method private native childFramesAsText()Ljava/lang/String;
.end method

.method private closeWindow(Landroid/webkit/WebViewCore;)V
    .registers 4
    .parameter "w"

    #@0
    .prologue
    .line 951
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {p1}, Landroid/webkit/WebViewCore;->getWebViewClassic()Landroid/webkit/WebViewClassic;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1}, Landroid/webkit/CallbackProxy;->onCloseWindow(Landroid/webkit/WebViewClassic;)V

    #@9
    .line 952
    return-void
.end method

.method private createWindow(ZZ)Landroid/webkit/BrowserFrame;
    .registers 4
    .parameter "dialog"
    .parameter "userGesture"

    #@0
    .prologue
    .line 937
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/webkit/CallbackProxy;->createWindow(ZZ)Landroid/webkit/BrowserFrame;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method private decidePolicyForFormResubmission(I)V
    .registers 6
    .parameter "policyFunction"

    #@0
    .prologue
    const/16 v3, 0x3eb

    #@2
    .line 959
    const/4 v2, 0x2

    #@3
    invoke-virtual {p0, v3, p1, v2}, Landroid/webkit/BrowserFrame;->obtainMessage(III)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 961
    .local v0, dontResend:Landroid/os/Message;
    const/4 v2, 0x0

    #@8
    invoke-virtual {p0, v3, p1, v2}, Landroid/webkit/BrowserFrame;->obtainMessage(III)Landroid/os/Message;

    #@b
    move-result-object v1

    #@c
    .line 963
    .local v1, resend:Landroid/os/Message;
    iget-object v2, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@e
    invoke-virtual {v2, v0, v1}, Landroid/webkit/CallbackProxy;->onFormResubmission(Landroid/os/Message;Landroid/os/Message;)V

    #@11
    .line 964
    return-void
.end method

.method private density()F
    .registers 2

    #@0
    .prologue
    .line 1050
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v0}, Landroid/webkit/WebViewCore;->getFixedDisplayDensity(Landroid/content/Context;)F

    #@5
    move-result v0

    #@6
    return v0
.end method

.method private didFinishLoading()V
    .registers 3

    #@0
    .prologue
    .line 1218
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mKeyStoreHandler:Landroid/webkit/KeyStoreHandler;

    #@2
    if-eqz v0, :cond_e

    #@4
    .line 1219
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mKeyStoreHandler:Landroid/webkit/KeyStoreHandler;

    #@6
    iget-object v1, p0, Landroid/webkit/BrowserFrame;->mContext:Landroid/content/Context;

    #@8
    invoke-virtual {v0, v1}, Landroid/webkit/KeyStoreHandler;->installCert(Landroid/content/Context;)V

    #@b
    .line 1220
    const/4 v0, 0x0

    #@c
    iput-object v0, p0, Landroid/webkit/BrowserFrame;->mKeyStoreHandler:Landroid/webkit/KeyStoreHandler;

    #@e
    .line 1222
    :cond_e
    return-void
.end method

.method private didReceiveAuthenticationChallenge(ILjava/lang/String;Ljava/lang/String;ZZ)V
    .registers 8
    .parameter "handle"
    .parameter "host"
    .parameter "realm"
    .parameter "useCachedCredentials"
    .parameter "suppressDialog"

    #@0
    .prologue
    .line 1068
    new-instance v0, Landroid/webkit/BrowserFrame$1;

    #@2
    invoke-direct {v0, p0, p4, p1, p5}, Landroid/webkit/BrowserFrame$1;-><init>(Landroid/webkit/BrowserFrame;ZIZ)V

    #@5
    .line 1090
    .local v0, handler:Landroid/webkit/HttpAuthHandler;
    iget-object v1, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@7
    invoke-virtual {v1, v0, p2, p3}, Landroid/webkit/CallbackProxy;->onReceivedHttpAuthRequest(Landroid/webkit/HttpAuthHandler;Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 1091
    return-void
.end method

.method private didReceiveData([BI)V
    .registers 4
    .parameter "data"
    .parameter "size"

    #@0
    .prologue
    .line 1214
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mKeyStoreHandler:Landroid/webkit/KeyStoreHandler;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mKeyStoreHandler:Landroid/webkit/KeyStoreHandler;

    #@6
    invoke-virtual {v0, p1, p2}, Landroid/webkit/KeyStoreHandler;->didReceiveData([BI)V

    #@9
    .line 1215
    :cond_9
    return-void
.end method

.method private didReceiveIcon(Landroid/graphics/Bitmap;)V
    .registers 3
    .parameter "icon"

    #@0
    .prologue
    .line 924
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0, p1}, Landroid/webkit/CallbackProxy;->onReceivedIcon(Landroid/graphics/Bitmap;)V

    #@5
    .line 925
    return-void
.end method

.method private didReceiveTouchIconUrl(Ljava/lang/String;Z)V
    .registers 4
    .parameter "url"
    .parameter "precomposed"

    #@0
    .prologue
    .line 929
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/webkit/CallbackProxy;->onReceivedTouchIconUrl(Ljava/lang/String;Z)V

    #@5
    .line 930
    return-void
.end method

.method private native documentAsText()Ljava/lang/String;
.end method

.method private downloadStart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .registers 19
    .parameter "url"
    .parameter "userAgent"
    .parameter "contentDisposition"
    .parameter "mimeType"
    .parameter "referer"
    .parameter "suggestedName"
    .parameter "contentLength"

    #@0
    .prologue
    .line 1187
    invoke-virtual {p4}, Ljava/lang/String;->isEmpty()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_1a

    #@6
    .line 1189
    const/16 v0, 0x2e

    #@8
    :try_start_8
    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(I)I

    #@b
    move-result v0

    #@c
    add-int/lit8 v0, v0, 0x1

    #@e
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@11
    move-result-object v9

    #@12
    .line 1190
    .local v9, extension:Ljava/lang/String;
    invoke-static {v9}, Llibcore/net/MimeUtils;->guessMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    #@15
    move-result-object p4

    #@16
    .line 1192
    if-nez p4, :cond_1a

    #@18
    .line 1193
    const-string p4, ""
    :try_end_1a
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_8 .. :try_end_1a} :catch_3f

    #@1a
    .line 1198
    .end local v9           #extension:Ljava/lang/String;
    :cond_1a
    :goto_1a
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0, p4, p1, p3}, Landroid/webkit/MimeTypeMap;->remapGenericMimeType(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@21
    move-result-object p4

    #@22
    .line 1201
    invoke-static {p4}, Landroid/webkit/CertTool;->getCertType(Ljava/lang/String;)Ljava/lang/String;

    #@25
    move-result-object v0

    #@26
    if-eqz v0, :cond_30

    #@28
    .line 1202
    new-instance v0, Landroid/webkit/KeyStoreHandler;

    #@2a
    invoke-direct {v0, p4}, Landroid/webkit/KeyStoreHandler;-><init>(Ljava/lang/String;)V

    #@2d
    iput-object v0, p0, Landroid/webkit/BrowserFrame;->mKeyStoreHandler:Landroid/webkit/KeyStoreHandler;

    #@2f
    .line 1208
    :goto_2f
    return-void

    #@30
    .line 1205
    :cond_30
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@32
    move-object v1, p1

    #@33
    move-object v2, p2

    #@34
    move-object v3, p3

    #@35
    move-object v4, p4

    #@36
    move-object v5, p5

    #@37
    move-wide/from16 v6, p7

    #@39
    move-object/from16 v8, p6

    #@3b
    invoke-virtual/range {v0 .. v8}, Landroid/webkit/CallbackProxy;->onDownloadStart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)Z

    #@3e
    goto :goto_2f

    #@3f
    .line 1194
    :catch_3f
    move-exception v0

    #@40
    goto :goto_1a
.end method

.method private native externalRepresentation()Ljava/lang/String;
.end method

.method private getFile(Ljava/lang/String;[BII)I
    .registers 12
    .parameter "uri"
    .parameter "buffer"
    .parameter "offset"
    .parameter "expectedSize"

    #@0
    .prologue
    .line 701
    const/4 v2, 0x0

    #@1
    .line 703
    .local v2, size:I
    :try_start_1
    iget-object v4, p0, Landroid/webkit/BrowserFrame;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v4

    #@7
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@a
    move-result-object v5

    #@b
    invoke-virtual {v4, v5}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    #@e
    move-result-object v3

    #@f
    .line 705
    .local v3, stream:Ljava/io/InputStream;
    invoke-virtual {v3}, Ljava/io/InputStream;->available()I

    #@12
    move-result v2

    #@13
    .line 706
    if-gt v2, p4, :cond_22

    #@15
    if-eqz p2, :cond_22

    #@17
    array-length v4, p2

    #@18
    sub-int/2addr v4, p3

    #@19
    if-lt v4, v2, :cond_22

    #@1b
    .line 708
    invoke-virtual {v3, p2, p3, v2}, Ljava/io/InputStream;->read([BII)I

    #@1e
    .line 712
    :goto_1e
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_21
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_21} :catch_24
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_21} :catch_40

    #@21
    .line 720
    .end local v3           #stream:Ljava/io/InputStream;
    :goto_21
    return v2

    #@22
    .line 710
    .restart local v3       #stream:Ljava/io/InputStream;
    :cond_22
    const/4 v2, 0x0

    #@23
    goto :goto_1e

    #@24
    .line 713
    .end local v3           #stream:Ljava/io/InputStream;
    :catch_24
    move-exception v0

    #@25
    .line 714
    .local v0, e:Ljava/io/FileNotFoundException;
    const-string/jumbo v4, "webkit"

    #@28
    new-instance v5, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v6, "FileNotFoundException:"

    #@2f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v5

    #@33
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v5

    #@37
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v5

    #@3b
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 715
    const/4 v2, 0x0

    #@3f
    .line 719
    goto :goto_21

    #@40
    .line 716
    .end local v0           #e:Ljava/io/FileNotFoundException;
    :catch_40
    move-exception v1

    #@41
    .line 717
    .local v1, e2:Ljava/io/IOException;
    const-string/jumbo v4, "webkit"

    #@44
    new-instance v5, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v6, "IOException: "

    #@4b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v5

    #@4f
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v5

    #@53
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v5

    #@57
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    .line 718
    const/4 v2, 0x0

    #@5b
    goto :goto_21
.end method

.method private getFileSize(Ljava/lang/String;)I
    .registers 6
    .parameter "uri"

    #@0
    .prologue
    .line 680
    const/4 v0, 0x0

    #@1
    .line 682
    .local v0, size:I
    :try_start_1
    iget-object v2, p0, Landroid/webkit/BrowserFrame;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v2

    #@7
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@a
    move-result-object v3

    #@b
    invoke-virtual {v2, v3}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    #@e
    move-result-object v1

    #@f
    .line 684
    .local v1, stream:Ljava/io/InputStream;
    invoke-virtual {v1}, Ljava/io/InputStream;->available()I

    #@12
    move-result v0

    #@13
    .line 685
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_16} :catch_17

    #@16
    .line 687
    .end local v1           #stream:Ljava/io/InputStream;
    :goto_16
    return v0

    #@17
    .line 686
    :catch_17
    move-exception v2

    #@18
    goto :goto_16
.end method

.method private getRawResFilename(I)Ljava/lang/String;
    .registers 3
    .parameter "id"

    #@0
    .prologue
    .line 997
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mContext:Landroid/content/Context;

    #@2
    invoke-static {p1, v0}, Landroid/webkit/BrowserFrame;->getRawResFilename(ILandroid/content/Context;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method static getRawResFilename(ILandroid/content/Context;)Ljava/lang/String;
    .registers 8
    .parameter "id"
    .parameter "context"

    #@0
    .prologue
    .line 1001
    packed-switch p0, :pswitch_data_82

    #@3
    .line 1032
    const-string/jumbo v4, "webkit"

    #@6
    const-string v5, "getRawResFilename got incompatible resource ID"

    #@8
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 1033
    const-string v4, ""

    #@d
    .line 1046
    :goto_d
    return-object v4

    #@e
    .line 1003
    :pswitch_e
    const v2, 0x1100004

    #@11
    .line 1035
    .local v2, resid:I
    :goto_11
    new-instance v3, Landroid/util/TypedValue;

    #@13
    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    #@16
    .line 1036
    .local v3, value:Landroid/util/TypedValue;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@19
    move-result-object v4

    #@1a
    const/4 v5, 0x1

    #@1b
    invoke-virtual {v4, v2, v3, v5}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    #@1e
    .line 1037
    const/4 v4, 0x3

    #@1f
    if-ne p0, v4, :cond_7a

    #@21
    .line 1038
    iget-object v4, v3, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    #@23
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    .line 1039
    .local v1, path:Ljava/lang/String;
    const/16 v4, 0x2f

    #@29
    invoke-virtual {v1, v4}, Ljava/lang/String;->lastIndexOf(I)I

    #@2c
    move-result v0

    #@2d
    .line 1040
    .local v0, index:I
    if-gez v0, :cond_72

    #@2f
    .line 1041
    const-string/jumbo v4, "webkit"

    #@32
    const-string v5, "Can\'t find drawable directory."

    #@34
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 1042
    const-string v4, ""

    #@39
    goto :goto_d

    #@3a
    .line 1007
    .end local v0           #index:I
    .end local v1           #path:Ljava/lang/String;
    .end local v2           #resid:I
    .end local v3           #value:Landroid/util/TypedValue;
    :pswitch_3a
    const v2, 0x1100003

    #@3d
    .line 1008
    .restart local v2       #resid:I
    goto :goto_11

    #@3e
    .line 1012
    .end local v2           #resid:I
    :pswitch_3e
    const v2, 0x10800e1

    #@41
    .line 1013
    .restart local v2       #resid:I
    goto :goto_11

    #@42
    .line 1016
    .end local v2           #resid:I
    :pswitch_42
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@45
    move-result-object v4

    #@46
    const v5, 0x10404b7

    #@49
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@4c
    move-result-object v4

    #@4d
    goto :goto_d

    #@4e
    .line 1020
    :pswitch_4e
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@51
    move-result-object v4

    #@52
    const v5, 0x10404b9

    #@55
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@58
    move-result-object v4

    #@59
    goto :goto_d

    #@5a
    .line 1024
    :pswitch_5a
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5d
    move-result-object v4

    #@5e
    const v5, 0x10404ba

    #@61
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@64
    move-result-object v4

    #@65
    goto :goto_d

    #@66
    .line 1028
    :pswitch_66
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@69
    move-result-object v4

    #@6a
    const v5, 0x10404b8

    #@6d
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@70
    move-result-object v4

    #@71
    goto :goto_d

    #@72
    .line 1044
    .restart local v0       #index:I
    .restart local v1       #path:Ljava/lang/String;
    .restart local v2       #resid:I
    .restart local v3       #value:Landroid/util/TypedValue;
    :cond_72
    const/4 v4, 0x0

    #@73
    add-int/lit8 v5, v0, 0x1

    #@75
    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@78
    move-result-object v4

    #@79
    goto :goto_d

    #@7a
    .line 1046
    .end local v0           #index:I
    .end local v1           #path:Ljava/lang/String;
    :cond_7a
    iget-object v4, v3, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    #@7c
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@7f
    move-result-object v4

    #@80
    goto :goto_d

    #@81
    .line 1001
    nop

    #@82
    :pswitch_data_82
    .packed-switch 0x1
        :pswitch_e
        :pswitch_3a
        :pswitch_3e
        :pswitch_42
        :pswitch_4e
        :pswitch_5a
        :pswitch_66
    .end packed-switch
.end method

.method private native getUsernamePassword()[Ljava/lang/String;
.end method

.method private native hasPasswordField()Z
.end method

.method private inputStreamForAndroidResource(Ljava/lang/String;)Ljava/io/InputStream;
    .registers 25
    .parameter "url"

    #@0
    .prologue
    .line 733
    const-string v3, "file:///android_asset/"

    #@2
    .line 734
    .local v3, ANDROID_ASSET:Ljava/lang/String;
    const-string v5, "file:///android_res/"

    #@4
    .line 735
    .local v5, ANDROID_RESOURCE:Ljava/lang/String;
    const-string v4, "content:"

    #@6
    .line 737
    .local v4, ANDROID_CONTENT:Ljava/lang/String;
    const-string v19, "file:///android_res/"

    #@8
    move-object/from16 v0, p1

    #@a
    move-object/from16 v1, v19

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@f
    move-result v19

    #@10
    if-eqz v19, :cond_16f

    #@12
    .line 738
    const-string v19, "file:///android_res/"

    #@14
    const-string v20, ""

    #@16
    move-object/from16 v0, p1

    #@18
    move-object/from16 v1, v19

    #@1a
    move-object/from16 v2, v20

    #@1c
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1f
    move-result-object p1

    #@20
    .line 739
    if-eqz p1, :cond_28

    #@22
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    #@25
    move-result v19

    #@26
    if-nez v19, :cond_49

    #@28
    .line 740
    :cond_28
    const-string/jumbo v19, "webkit"

    #@2b
    new-instance v20, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string/jumbo v21, "url has length 0 "

    #@33
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v20

    #@37
    move-object/from16 v0, v20

    #@39
    move-object/from16 v1, p1

    #@3b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v20

    #@3f
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v20

    #@43
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 741
    const/16 v19, 0x0

    #@48
    .line 800
    :goto_48
    return-object v19

    #@49
    .line 743
    :cond_49
    const/16 v19, 0x2f

    #@4b
    move-object/from16 v0, p1

    #@4d
    move/from16 v1, v19

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    #@52
    move-result v15

    #@53
    .line 744
    .local v15, slash:I
    const/16 v19, 0x2e

    #@55
    move-object/from16 v0, p1

    #@57
    move/from16 v1, v19

    #@59
    invoke-virtual {v0, v1, v15}, Ljava/lang/String;->indexOf(II)I

    #@5c
    move-result v8

    #@5d
    .line 745
    .local v8, dot:I
    const/16 v19, -0x1

    #@5f
    move/from16 v0, v19

    #@61
    if-eq v15, v0, :cond_69

    #@63
    const/16 v19, -0x1

    #@65
    move/from16 v0, v19

    #@67
    if-ne v8, v0, :cond_89

    #@69
    .line 746
    :cond_69
    const-string/jumbo v19, "webkit"

    #@6c
    new-instance v20, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    const-string v21, "Incorrect res path: "

    #@73
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v20

    #@77
    move-object/from16 v0, v20

    #@79
    move-object/from16 v1, p1

    #@7b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v20

    #@7f
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v20

    #@83
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@86
    .line 747
    const/16 v19, 0x0

    #@88
    goto :goto_48

    #@89
    .line 749
    :cond_89
    const/16 v19, 0x0

    #@8b
    move-object/from16 v0, p1

    #@8d
    move/from16 v1, v19

    #@8f
    invoke-virtual {v0, v1, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@92
    move-result-object v16

    #@93
    .line 750
    .local v16, subClassName:Ljava/lang/String;
    add-int/lit8 v19, v15, 0x1

    #@95
    move-object/from16 v0, p1

    #@97
    move/from16 v1, v19

    #@99
    invoke-virtual {v0, v1, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@9c
    move-result-object v12

    #@9d
    .line 751
    .local v12, fieldName:Ljava/lang/String;
    const/4 v10, 0x0

    #@9e
    .line 753
    .local v10, errorMsg:Ljava/lang/String;
    :try_start_9e
    move-object/from16 v0, p0

    #@a0
    iget-object v0, v0, Landroid/webkit/BrowserFrame;->mContext:Landroid/content/Context;

    #@a2
    move-object/from16 v19, v0

    #@a4
    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@a7
    move-result-object v19

    #@a8
    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    #@ab
    move-result-object v19

    #@ac
    new-instance v20, Ljava/lang/StringBuilder;

    #@ae
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@b1
    move-object/from16 v0, p0

    #@b3
    iget-object v0, v0, Landroid/webkit/BrowserFrame;->mContext:Landroid/content/Context;

    #@b5
    move-object/from16 v21, v0

    #@b7
    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@ba
    move-result-object v21

    #@bb
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v20

    #@bf
    const-string v21, ".R$"

    #@c1
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v20

    #@c5
    move-object/from16 v0, v20

    #@c7
    move-object/from16 v1, v16

    #@c9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v20

    #@cd
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d0
    move-result-object v20

    #@d1
    invoke-virtual/range {v19 .. v20}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@d4
    move-result-object v7

    #@d5
    .line 757
    .local v7, d:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    invoke-virtual {v7, v12}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@d8
    move-result-object v11

    #@d9
    .line 758
    .local v11, field:Ljava/lang/reflect/Field;
    const/16 v19, 0x0

    #@db
    move-object/from16 v0, v19

    #@dd
    invoke-virtual {v11, v0}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    #@e0
    move-result v13

    #@e1
    .line 759
    .local v13, id:I
    new-instance v18, Landroid/util/TypedValue;

    #@e3
    invoke-direct/range {v18 .. v18}, Landroid/util/TypedValue;-><init>()V

    #@e6
    .line 760
    .local v18, value:Landroid/util/TypedValue;
    move-object/from16 v0, p0

    #@e8
    iget-object v0, v0, Landroid/webkit/BrowserFrame;->mContext:Landroid/content/Context;

    #@ea
    move-object/from16 v19, v0

    #@ec
    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@ef
    move-result-object v19

    #@f0
    const/16 v20, 0x1

    #@f2
    move-object/from16 v0, v19

    #@f4
    move-object/from16 v1, v18

    #@f6
    move/from16 v2, v20

    #@f8
    invoke-virtual {v0, v13, v1, v2}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    #@fb
    .line 761
    move-object/from16 v0, v18

    #@fd
    iget v0, v0, Landroid/util/TypedValue;->type:I

    #@ff
    move/from16 v19, v0

    #@101
    const/16 v20, 0x3

    #@103
    move/from16 v0, v19

    #@105
    move/from16 v1, v20

    #@107
    if-ne v0, v1, :cond_12b

    #@109
    .line 762
    move-object/from16 v0, p0

    #@10b
    iget-object v0, v0, Landroid/webkit/BrowserFrame;->mContext:Landroid/content/Context;

    #@10d
    move-object/from16 v19, v0

    #@10f
    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    #@112
    move-result-object v19

    #@113
    move-object/from16 v0, v18

    #@115
    iget v0, v0, Landroid/util/TypedValue;->assetCookie:I

    #@117
    move/from16 v20, v0

    #@119
    move-object/from16 v0, v18

    #@11b
    iget-object v0, v0, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    #@11d
    move-object/from16 v21, v0

    #@11f
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@122
    move-result-object v21

    #@123
    const/16 v22, 0x2

    #@125
    invoke-virtual/range {v19 .. v22}, Landroid/content/res/AssetManager;->openNonAsset(ILjava/lang/String;I)Ljava/io/InputStream;

    #@128
    move-result-object v19

    #@129
    goto/16 :goto_48

    #@12b
    .line 767
    :cond_12b
    const-string/jumbo v19, "webkit"

    #@12e
    new-instance v20, Ljava/lang/StringBuilder;

    #@130
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@133
    const-string/jumbo v21, "not of type string: "

    #@136
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@139
    move-result-object v20

    #@13a
    move-object/from16 v0, v20

    #@13c
    move-object/from16 v1, p1

    #@13e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@141
    move-result-object v20

    #@142
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@145
    move-result-object v20

    #@146
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_149
    .catch Ljava/lang/Exception; {:try_start_9e .. :try_end_149} :catch_14d

    #@149
    .line 768
    const/16 v19, 0x0

    #@14b
    goto/16 :goto_48

    #@14d
    .line 770
    .end local v7           #d:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    .end local v11           #field:Ljava/lang/reflect/Field;
    .end local v13           #id:I
    .end local v18           #value:Landroid/util/TypedValue;
    :catch_14d
    move-exception v9

    #@14e
    .line 771
    .local v9, e:Ljava/lang/Exception;
    const-string/jumbo v19, "webkit"

    #@151
    new-instance v20, Ljava/lang/StringBuilder;

    #@153
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@156
    const-string v21, "Exception: "

    #@158
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15b
    move-result-object v20

    #@15c
    move-object/from16 v0, v20

    #@15e
    move-object/from16 v1, p1

    #@160
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@163
    move-result-object v20

    #@164
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@167
    move-result-object v20

    #@168
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@16b
    .line 772
    const/16 v19, 0x0

    #@16d
    goto/16 :goto_48

    #@16f
    .line 774
    .end local v8           #dot:I
    .end local v9           #e:Ljava/lang/Exception;
    .end local v10           #errorMsg:Ljava/lang/String;
    .end local v12           #fieldName:Ljava/lang/String;
    .end local v15           #slash:I
    .end local v16           #subClassName:Ljava/lang/String;
    :cond_16f
    const-string v19, "file:///android_asset/"

    #@171
    move-object/from16 v0, p1

    #@173
    move-object/from16 v1, v19

    #@175
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@178
    move-result v19

    #@179
    if-eqz v19, :cond_1ac

    #@17b
    .line 775
    const-string v19, "file:///android_asset/"

    #@17d
    const-string v20, ""

    #@17f
    move-object/from16 v0, p1

    #@181
    move-object/from16 v1, v19

    #@183
    move-object/from16 v2, v20

    #@185
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@188
    move-result-object p1

    #@189
    .line 777
    :try_start_189
    move-object/from16 v0, p0

    #@18b
    iget-object v0, v0, Landroid/webkit/BrowserFrame;->mContext:Landroid/content/Context;

    #@18d
    move-object/from16 v19, v0

    #@18f
    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    #@192
    move-result-object v6

    #@193
    .line 778
    .local v6, assets:Landroid/content/res/AssetManager;
    invoke-static/range {p1 .. p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@196
    move-result-object v17

    #@197
    .line 779
    .local v17, uri:Landroid/net/Uri;
    invoke-virtual/range {v17 .. v17}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    #@19a
    move-result-object v19

    #@19b
    const/16 v20, 0x2

    #@19d
    move-object/from16 v0, v19

    #@19f
    move/from16 v1, v20

    #@1a1
    invoke-virtual {v6, v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;I)Ljava/io/InputStream;
    :try_end_1a4
    .catch Ljava/io/IOException; {:try_start_189 .. :try_end_1a4} :catch_1a7

    #@1a4
    move-result-object v19

    #@1a5
    goto/16 :goto_48

    #@1a7
    .line 780
    .end local v6           #assets:Landroid/content/res/AssetManager;
    .end local v17           #uri:Landroid/net/Uri;
    :catch_1a7
    move-exception v9

    #@1a8
    .line 781
    .local v9, e:Ljava/io/IOException;
    const/16 v19, 0x0

    #@1aa
    goto/16 :goto_48

    #@1ac
    .line 783
    .end local v9           #e:Ljava/io/IOException;
    :cond_1ac
    move-object/from16 v0, p0

    #@1ae
    iget-object v0, v0, Landroid/webkit/BrowserFrame;->mSettings:Landroid/webkit/WebSettingsClassic;

    #@1b0
    move-object/from16 v19, v0

    #@1b2
    invoke-virtual/range {v19 .. v19}, Landroid/webkit/WebSettingsClassic;->getAllowContentAccess()Z

    #@1b5
    move-result v19

    #@1b6
    if-eqz v19, :cond_218

    #@1b8
    const-string v19, "content:"

    #@1ba
    move-object/from16 v0, p1

    #@1bc
    move-object/from16 v1, v19

    #@1be
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@1c1
    move-result v19

    #@1c2
    if-eqz v19, :cond_218

    #@1c4
    .line 789
    const/16 v19, 0x3f

    #@1c6
    :try_start_1c6
    move-object/from16 v0, p1

    #@1c8
    move/from16 v1, v19

    #@1ca
    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    #@1cd
    move-result v14

    #@1ce
    .line 790
    .local v14, mimeIndex:I
    const/16 v19, -0x1

    #@1d0
    move/from16 v0, v19

    #@1d2
    if-eq v14, v0, :cond_1de

    #@1d4
    .line 791
    const/16 v19, 0x0

    #@1d6
    move-object/from16 v0, p1

    #@1d8
    move/from16 v1, v19

    #@1da
    invoke-virtual {v0, v1, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1dd
    move-result-object p1

    #@1de
    .line 793
    :cond_1de
    invoke-static/range {p1 .. p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@1e1
    move-result-object v17

    #@1e2
    .line 794
    .restart local v17       #uri:Landroid/net/Uri;
    move-object/from16 v0, p0

    #@1e4
    iget-object v0, v0, Landroid/webkit/BrowserFrame;->mContext:Landroid/content/Context;

    #@1e6
    move-object/from16 v19, v0

    #@1e8
    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1eb
    move-result-object v19

    #@1ec
    move-object/from16 v0, v19

    #@1ee
    move-object/from16 v1, v17

    #@1f0
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_1f3
    .catch Ljava/lang/Exception; {:try_start_1c6 .. :try_end_1f3} :catch_1f6

    #@1f3
    move-result-object v19

    #@1f4
    goto/16 :goto_48

    #@1f6
    .line 795
    .end local v14           #mimeIndex:I
    .end local v17           #uri:Landroid/net/Uri;
    :catch_1f6
    move-exception v9

    #@1f7
    .line 796
    .local v9, e:Ljava/lang/Exception;
    const-string/jumbo v19, "webkit"

    #@1fa
    new-instance v20, Ljava/lang/StringBuilder;

    #@1fc
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@1ff
    const-string v21, "Exception: "

    #@201
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@204
    move-result-object v20

    #@205
    move-object/from16 v0, v20

    #@207
    move-object/from16 v1, p1

    #@209
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20c
    move-result-object v20

    #@20d
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@210
    move-result-object v20

    #@211
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@214
    .line 797
    const/16 v19, 0x0

    #@216
    goto/16 :goto_48

    #@218
    .line 800
    .end local v9           #e:Ljava/lang/Exception;
    :cond_218
    const/16 v19, 0x0

    #@21a
    goto/16 :goto_48
.end method

.method private loadFinished(Ljava/lang/String;IZ)V
    .registers 5
    .parameter "url"
    .parameter "loadType"
    .parameter "isMainFrame"

    #@0
    .prologue
    .line 470
    if-nez p3, :cond_4

    #@2
    if-nez p2, :cond_13

    #@4
    .line 471
    :cond_4
    if-eqz p3, :cond_13

    #@6
    .line 472
    invoke-direct {p0}, Landroid/webkit/BrowserFrame;->resetLoadingStates()V

    #@9
    .line 473
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@b
    invoke-virtual {v0}, Landroid/webkit/CallbackProxy;->switchOutDrawHistory()V

    #@e
    .line 474
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@10
    invoke-virtual {v0, p1}, Landroid/webkit/CallbackProxy;->onPageFinished(Ljava/lang/String;)V

    #@13
    .line 477
    :cond_13
    return-void
.end method

.method private loadStarted(Ljava/lang/String;Landroid/graphics/Bitmap;IZ)V
    .registers 7
    .parameter "url"
    .parameter "favicon"
    .parameter "loadType"
    .parameter "isMainFrame"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 405
    iput-boolean p4, p0, Landroid/webkit/BrowserFrame;->mIsMainFrame:Z

    #@3
    .line 407
    if-nez p4, :cond_7

    #@5
    if-nez p3, :cond_20

    #@7
    .line 408
    :cond_7
    iput p3, p0, Landroid/webkit/BrowserFrame;->mLoadType:I

    #@9
    .line 410
    if-eqz p4, :cond_20

    #@b
    .line 412
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/webkit/CallbackProxy;->onPageStarted(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    #@10
    .line 415
    iput-boolean v1, p0, Landroid/webkit/BrowserFrame;->mFirstLayoutDone:Z

    #@12
    .line 416
    iput-boolean v1, p0, Landroid/webkit/BrowserFrame;->mCommitted:Z

    #@14
    .line 419
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@16
    invoke-virtual {v0}, Landroid/webkit/WebViewCore;->clearContent()V

    #@19
    .line 420
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@1b
    const/16 v1, 0x82

    #@1d
    invoke-virtual {v0, v1}, Landroid/webkit/WebViewCore;->removeMessages(I)V

    #@20
    .line 423
    :cond_20
    return-void
.end method

.method private maybeSavePassword([BLjava/lang/String;Ljava/lang/String;)V
    .registers 13
    .parameter "postData"
    .parameter "username"
    .parameter "password"

    #@0
    .prologue
    const/4 v8, 0x2

    #@1
    const/4 v7, 0x1

    #@2
    .line 817
    if-eqz p1, :cond_14

    #@4
    if-eqz p2, :cond_14

    #@6
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    #@9
    move-result v6

    #@a
    if-nez v6, :cond_14

    #@c
    if-eqz p3, :cond_14

    #@e
    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    #@11
    move-result v6

    #@12
    if-eqz v6, :cond_15

    #@14
    .line 870
    :cond_14
    :goto_14
    return-void

    #@15
    .line 823
    :cond_15
    iget-object v6, p0, Landroid/webkit/BrowserFrame;->mSettings:Landroid/webkit/WebSettingsClassic;

    #@17
    invoke-virtual {v6}, Landroid/webkit/WebSettingsClassic;->getSavePassword()Z

    #@1a
    move-result v6

    #@1b
    if-eqz v6, :cond_14

    #@1d
    .line 827
    const-string v6, ","

    #@1f
    invoke-virtual {p2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    .line 828
    .local v4, tokenizedUsername:[Ljava/lang/String;
    const-string v6, ","

    #@25
    invoke-virtual {p3, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    .line 830
    .local v3, tokenizedPassword:[Ljava/lang/String;
    array-length v6, v4

    #@2a
    if-ne v6, v8, :cond_14

    #@2c
    aget-object v6, v4, v7

    #@2e
    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    #@31
    move-result v6

    #@32
    if-nez v6, :cond_14

    #@34
    array-length v6, v3

    #@35
    if-ne v6, v8, :cond_14

    #@37
    aget-object v6, v3, v7

    #@39
    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    #@3c
    move-result v6

    #@3d
    if-nez v6, :cond_14

    #@3f
    .line 840
    :try_start_3f
    new-instance v5, Landroid/net/WebAddress;

    #@41
    iget-object v6, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@43
    invoke-virtual {v6}, Landroid/webkit/CallbackProxy;->getBackForwardList()Landroid/webkit/WebBackForwardListClassic;

    #@46
    move-result-object v6

    #@47
    invoke-virtual {v6}, Landroid/webkit/WebBackForwardListClassic;->getCurrentItem()Landroid/webkit/WebHistoryItemClassic;

    #@4a
    move-result-object v6

    #@4b
    invoke-virtual {v6}, Landroid/webkit/WebHistoryItemClassic;->getUrl()Ljava/lang/String;

    #@4e
    move-result-object v6

    #@4f
    invoke-direct {v5, v6}, Landroid/net/WebAddress;-><init>(Ljava/lang/String;)V

    #@52
    .line 842
    .local v5, uri:Landroid/net/WebAddress;
    new-instance v6, Ljava/lang/StringBuilder;

    #@54
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@57
    invoke-virtual {v5}, Landroid/net/WebAddress;->getScheme()Ljava/lang/String;

    #@5a
    move-result-object v7

    #@5b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v6

    #@5f
    const-string v7, "://"

    #@61
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v6

    #@65
    invoke-virtual {v5}, Landroid/net/WebAddress;->getHost()Ljava/lang/String;

    #@68
    move-result-object v7

    #@69
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v6

    #@6d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v2

    #@71
    .line 846
    .local v2, schemePlusHost:Ljava/lang/String;
    new-instance v0, Ljava/lang/String;

    #@73
    invoke-direct {v0, p1}, Ljava/lang/String;-><init>([B)V

    #@76
    .line 847
    .local v0, postString:Ljava/lang/String;
    const/4 v6, 0x1

    #@77
    aget-object v6, v4, v6

    #@79
    invoke-static {v6}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    #@7c
    move-result-object v6

    #@7d
    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@80
    move-result v6

    #@81
    if-eqz v6, :cond_14

    #@83
    const/4 v6, 0x1

    #@84
    aget-object v6, v3, v6

    #@86
    invoke-static {v6}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    #@89
    move-result-object v6

    #@8a
    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@8d
    move-result v6

    #@8e
    if-eqz v6, :cond_14

    #@90
    .line 849
    iget-object v6, p0, Landroid/webkit/BrowserFrame;->mDatabase:Landroid/webkit/WebViewDatabaseClassic;

    #@92
    invoke-virtual {v6, v2}, Landroid/webkit/WebViewDatabaseClassic;->getUsernamePassword(Ljava/lang/String;)[Ljava/lang/String;

    #@95
    move-result-object v1

    #@96
    .line 851
    .local v1, saved:[Ljava/lang/String;
    if-eqz v1, :cond_a7

    #@98
    .line 854
    const/4 v6, 0x0

    #@99
    aget-object v6, v1, v6

    #@9b
    if-eqz v6, :cond_14

    #@9d
    .line 858
    iget-object v6, p0, Landroid/webkit/BrowserFrame;->mDatabase:Landroid/webkit/WebViewDatabaseClassic;

    #@9f
    invoke-virtual {v6, v2, p2, p3}, Landroid/webkit/WebViewDatabaseClassic;->setUsernamePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@a2
    goto/16 :goto_14

    #@a4
    .line 867
    .end local v0           #postString:Ljava/lang/String;
    .end local v1           #saved:[Ljava/lang/String;
    .end local v2           #schemePlusHost:Ljava/lang/String;
    .end local v5           #uri:Landroid/net/WebAddress;
    :catch_a4
    move-exception v6

    #@a5
    goto/16 :goto_14

    #@a7
    .line 863
    .restart local v0       #postString:Ljava/lang/String;
    .restart local v1       #saved:[Ljava/lang/String;
    .restart local v2       #schemePlusHost:Ljava/lang/String;
    .restart local v5       #uri:Landroid/net/WebAddress;
    :cond_a7
    iget-object v6, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@a9
    const/4 v7, 0x0

    #@aa
    invoke-virtual {v6, v2, p2, p3, v7}, Landroid/webkit/CallbackProxy;->onSavePassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)Z
    :try_end_ad
    .catch Landroid/net/ParseException; {:try_start_3f .. :try_end_ad} :catch_a4

    #@ad
    goto/16 :goto_14
.end method

.method private native nativeAddJavascriptInterface(ILjava/lang/Object;Ljava/lang/String;Z)V
.end method

.method private native nativeAuthenticationCancel(I)V
.end method

.method private native nativeAuthenticationProceed(ILjava/lang/String;Ljava/lang/String;)V
.end method

.method private native nativeCallPolicyFunction(II)V
.end method

.method private native nativeCreateFrame(Landroid/webkit/WebViewCore;Landroid/content/res/AssetManager;Landroid/webkit/WebBackForwardList;)V
.end method

.method private native nativeGetShouldStartScrolledRight(I)Z
.end method

.method private native nativeGoBackOrForward(I)V
.end method

.method private native nativeLoadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private native nativeLoadUrl(Ljava/lang/String;Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method private native nativeOrientationChanged(I)V
.end method

.method private native nativePostUrl(Ljava/lang/String;[B)V
.end method

.method private native nativeSaveWebArchive(Ljava/lang/String;Z)Ljava/lang/String;
.end method

.method private native nativeSslCertErrorCancel(II)V
.end method

.method private native nativeSslCertErrorProceed(I)V
.end method

.method private native nativeStopLoading()V
.end method

.method private reportError(ILjava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "errorCode"
    .parameter "description"
    .parameter "failingUrl"

    #@0
    .prologue
    .line 365
    invoke-direct {p0}, Landroid/webkit/BrowserFrame;->resetLoadingStates()V

    #@3
    .line 366
    if-eqz p2, :cond_b

    #@5
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_11

    #@b
    .line 367
    :cond_b
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mContext:Landroid/content/Context;

    #@d
    invoke-static {p1, v0}, Landroid/net/http/ErrorStrings;->getString(ILandroid/content/Context;)Ljava/lang/String;

    #@10
    move-result-object p2

    #@11
    .line 369
    :cond_11
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@13
    invoke-virtual {v0, p1, p2, p3}, Landroid/webkit/CallbackProxy;->onReceivedError(ILjava/lang/String;Ljava/lang/String;)V

    #@16
    .line 370
    return-void
.end method

.method private reportSslCertError(II[BLjava/lang/String;)V
    .registers 12
    .parameter "handle"
    .parameter "certError"
    .parameter "certDER"
    .parameter "url"

    #@0
    .prologue
    .line 1104
    :try_start_0
    new-instance v0, Lorg/apache/harmony/security/provider/cert/X509CertImpl;

    #@2
    invoke-direct {v0, p3}, Lorg/apache/harmony/security/provider/cert/X509CertImpl;-><init>([B)V

    #@5
    .line 1105
    .local v0, cert:Ljava/security/cert/X509Certificate;
    new-instance v3, Landroid/net/http/SslCertificate;

    #@7
    invoke-direct {v3, v0}, Landroid/net/http/SslCertificate;-><init>(Ljava/security/cert/X509Certificate;)V

    #@a
    .line 1106
    .local v3, sslCert:Landroid/net/http/SslCertificate;
    invoke-static {p2, v3, p4}, Landroid/net/http/SslError;->SslErrorFromChromiumErrorCode(ILandroid/net/http/SslCertificate;Ljava/lang/String;)Landroid/net/http/SslError;
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_d} :catch_21

    #@d
    move-result-object v4

    #@e
    .line 1114
    .local v4, sslError:Landroid/net/http/SslError;
    invoke-static {}, Landroid/webkit/SslCertLookupTable;->getInstance()Landroid/webkit/SslCertLookupTable;

    #@11
    move-result-object v5

    #@12
    invoke-virtual {v5, v4}, Landroid/webkit/SslCertLookupTable;->isAllowed(Landroid/net/http/SslError;)Z

    #@15
    move-result v5

    #@16
    if-eqz v5, :cond_2e

    #@18
    .line 1115
    invoke-direct {p0, p1}, Landroid/webkit/BrowserFrame;->nativeSslCertErrorProceed(I)V

    #@1b
    .line 1116
    iget-object v5, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@1d
    invoke-virtual {v5, v4}, Landroid/webkit/CallbackProxy;->onProceededAfterSslError(Landroid/net/http/SslError;)V

    #@20
    .line 1140
    .end local v0           #cert:Ljava/security/cert/X509Certificate;
    .end local v3           #sslCert:Landroid/net/http/SslCertificate;
    .end local v4           #sslError:Landroid/net/http/SslError;
    :goto_20
    return-void

    #@21
    .line 1107
    :catch_21
    move-exception v1

    #@22
    .line 1109
    .local v1, e:Ljava/io/IOException;
    const-string/jumbo v5, "webkit"

    #@25
    const-string v6, "Can\'t get the certificate from WebKit, canceling"

    #@27
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 1110
    invoke-direct {p0, p1, p2}, Landroid/webkit/BrowserFrame;->nativeSslCertErrorCancel(II)V

    #@2d
    goto :goto_20

    #@2e
    .line 1120
    .end local v1           #e:Ljava/io/IOException;
    .restart local v0       #cert:Ljava/security/cert/X509Certificate;
    .restart local v3       #sslCert:Landroid/net/http/SslCertificate;
    .restart local v4       #sslError:Landroid/net/http/SslError;
    :cond_2e
    new-instance v2, Landroid/webkit/BrowserFrame$2;

    #@30
    invoke-direct {v2, p0, v4, p1, p2}, Landroid/webkit/BrowserFrame$2;-><init>(Landroid/webkit/BrowserFrame;Landroid/net/http/SslError;II)V

    #@33
    .line 1139
    .local v2, handler:Landroid/webkit/SslErrorHandler;
    iget-object v5, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@35
    invoke-virtual {v5, v2, v4}, Landroid/webkit/CallbackProxy;->onReceivedSslError(Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V

    #@38
    goto :goto_20
.end method

.method private requestClientCert(ILjava/lang/String;)V
    .registers 7
    .parameter "handle"
    .parameter "hostAndPort"

    #@0
    .prologue
    .line 1150
    invoke-static {}, Landroid/webkit/SslClientCertLookupTable;->getInstance()Landroid/webkit/SslClientCertLookupTable;

    #@3
    move-result-object v1

    #@4
    .line 1151
    .local v1, table:Landroid/webkit/SslClientCertLookupTable;
    invoke-virtual {v1, p2}, Landroid/webkit/SslClientCertLookupTable;->IsAllowed(Ljava/lang/String;)Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_3e

    #@a
    .line 1153
    invoke-virtual {v1, p2}, Landroid/webkit/SslClientCertLookupTable;->PrivateKey(Ljava/lang/String;)Ljava/security/PrivateKey;

    #@d
    move-result-object v0

    #@e
    .line 1154
    .local v0, pkey:Ljava/security/PrivateKey;
    instance-of v2, v0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPrivateKey;

    #@10
    if-eqz v2, :cond_20

    #@12
    .line 1155
    check-cast v0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPrivateKey;

    #@14
    .end local v0           #pkey:Ljava/security/PrivateKey;
    invoke-virtual {v0}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPrivateKey;->getPkeyContext()I

    #@17
    move-result v2

    #@18
    invoke-virtual {v1, p2}, Landroid/webkit/SslClientCertLookupTable;->CertificateChain(Ljava/lang/String;)[[B

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {p0, p1, v2, v3}, Landroid/webkit/BrowserFrame;->nativeSslClientCert(II[[B)V

    #@1f
    .line 1175
    :goto_1f
    return-void

    #@20
    .line 1158
    .restart local v0       #pkey:Ljava/security/PrivateKey;
    :cond_20
    instance-of v2, v0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAPrivateKey;

    #@22
    if-eqz v2, :cond_32

    #@24
    .line 1159
    check-cast v0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAPrivateKey;

    #@26
    .end local v0           #pkey:Ljava/security/PrivateKey;
    invoke-virtual {v0}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAPrivateKey;->getPkeyContext()I

    #@29
    move-result v2

    #@2a
    invoke-virtual {v1, p2}, Landroid/webkit/SslClientCertLookupTable;->CertificateChain(Ljava/lang/String;)[[B

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {p0, p1, v2, v3}, Landroid/webkit/BrowserFrame;->nativeSslClientCert(II[[B)V

    #@31
    goto :goto_1f

    #@32
    .line 1163
    .restart local v0       #pkey:Ljava/security/PrivateKey;
    :cond_32
    invoke-interface {v0}, Ljava/security/PrivateKey;->getEncoded()[B

    #@35
    move-result-object v2

    #@36
    invoke-virtual {v1, p2}, Landroid/webkit/SslClientCertLookupTable;->CertificateChain(Ljava/lang/String;)[[B

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {p0, p1, v2, v3}, Landroid/webkit/BrowserFrame;->nativeSslClientCert(I[B[[B)V

    #@3d
    goto :goto_1f

    #@3e
    .line 1167
    .end local v0           #pkey:Ljava/security/PrivateKey;
    :cond_3e
    invoke-virtual {v1, p2}, Landroid/webkit/SslClientCertLookupTable;->IsDenied(Ljava/lang/String;)Z

    #@41
    move-result v2

    #@42
    if-eqz v2, :cond_4c

    #@44
    .line 1169
    const/4 v3, 0x0

    #@45
    const/4 v2, 0x0

    #@46
    check-cast v2, [[B

    #@48
    invoke-virtual {p0, p1, v3, v2}, Landroid/webkit/BrowserFrame;->nativeSslClientCert(II[[B)V

    #@4b
    goto :goto_1f

    #@4c
    .line 1172
    :cond_4c
    iget-object v2, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@4e
    new-instance v3, Landroid/webkit/ClientCertRequestHandler;

    #@50
    invoke-direct {v3, p0, p1, p2, v1}, Landroid/webkit/ClientCertRequestHandler;-><init>(Landroid/webkit/BrowserFrame;ILjava/lang/String;Landroid/webkit/SslClientCertLookupTable;)V

    #@53
    invoke-virtual {v2, v3, p2}, Landroid/webkit/CallbackProxy;->onReceivedClientCertRequest(Landroid/webkit/ClientCertRequestHandler;Ljava/lang/String;)V

    #@56
    goto :goto_1f
.end method

.method private requestFocus()V
    .registers 2

    #@0
    .prologue
    .line 944
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0}, Landroid/webkit/CallbackProxy;->onRequestFocus()V

    #@5
    .line 945
    return-void
.end method

.method private resetLoadingStates()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 373
    iput-boolean v0, p0, Landroid/webkit/BrowserFrame;->mCommitted:Z

    #@3
    .line 374
    iput-boolean v0, p0, Landroid/webkit/BrowserFrame;->mFirstLayoutDone:Z

    #@5
    .line 375
    return-void
.end method

.method private saveFormData(Ljava/util/HashMap;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 427
    .local p1, data:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, p0, Landroid/webkit/BrowserFrame;->mSettings:Landroid/webkit/WebSettingsClassic;

    #@2
    invoke-virtual {v2}, Landroid/webkit/WebSettingsClassic;->getSaveFormData()Z

    #@5
    move-result v2

    #@6
    if-eqz v2, :cond_23

    #@8
    .line 428
    iget-object v2, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@a
    invoke-virtual {v2}, Landroid/webkit/CallbackProxy;->getBackForwardList()Landroid/webkit/WebBackForwardListClassic;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2}, Landroid/webkit/WebBackForwardListClassic;->getCurrentItem()Landroid/webkit/WebHistoryItemClassic;

    #@11
    move-result-object v0

    #@12
    .line 430
    .local v0, h:Landroid/webkit/WebHistoryItem;
    if-eqz v0, :cond_23

    #@14
    .line 431
    invoke-virtual {v0}, Landroid/webkit/WebHistoryItemClassic;->getUrl()Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    invoke-static {v2}, Landroid/webkit/WebTextView;->urlForAutoCompleteData(Ljava/lang/String;)Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    .line 432
    .local v1, url:Ljava/lang/String;
    if-eqz v1, :cond_23

    #@1e
    .line 433
    iget-object v2, p0, Landroid/webkit/BrowserFrame;->mDatabase:Landroid/webkit/WebViewDatabaseClassic;

    #@20
    invoke-virtual {v2, v1, p1}, Landroid/webkit/WebViewDatabaseClassic;->setFormData(Ljava/lang/String;Ljava/util/HashMap;)V

    #@23
    .line 437
    .end local v0           #h:Landroid/webkit/WebHistoryItem;
    .end local v1           #url:Ljava/lang/String;
    :cond_23
    return-void
.end method

.method private setCertificate([B)V
    .registers 6
    .parameter "cert_der"

    #@0
    .prologue
    .line 1230
    :try_start_0
    new-instance v0, Lorg/apache/harmony/security/provider/cert/X509CertImpl;

    #@2
    invoke-direct {v0, p1}, Lorg/apache/harmony/security/provider/cert/X509CertImpl;-><init>([B)V

    #@5
    .line 1231
    .local v0, cert:Ljava/security/cert/X509Certificate;
    iget-object v2, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@7
    new-instance v3, Landroid/net/http/SslCertificate;

    #@9
    invoke-direct {v3, v0}, Landroid/net/http/SslCertificate;-><init>(Ljava/security/cert/X509Certificate;)V

    #@c
    invoke-virtual {v2, v3}, Landroid/webkit/CallbackProxy;->onReceivedCertificate(Landroid/net/http/SslCertificate;)V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_f} :catch_10

    #@f
    .line 1237
    .end local v0           #cert:Ljava/security/cert/X509Certificate;
    :goto_f
    return-void

    #@10
    .line 1232
    :catch_10
    move-exception v1

    #@11
    .line 1234
    .local v1, e:Ljava/io/IOException;
    const-string/jumbo v2, "webkit"

    #@14
    const-string v3, "Can\'t get the certificate from WebKit, canceling"

    #@16
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    goto :goto_f
.end method

.method private setProgress(I)V
    .registers 5
    .parameter "newProgress"

    #@0
    .prologue
    .line 907
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0, p1}, Landroid/webkit/CallbackProxy;->onProgressChanged(I)V

    #@5
    .line 908
    const/16 v0, 0x64

    #@7
    if-ne p1, v0, :cond_14

    #@9
    .line 909
    const/16 v0, 0x3e9

    #@b
    invoke-virtual {p0, v0}, Landroid/webkit/BrowserFrame;->obtainMessage(I)Landroid/os/Message;

    #@e
    move-result-object v0

    #@f
    const-wide/16 v1, 0x64

    #@11
    invoke-virtual {p0, v0, v1, v2}, Landroid/webkit/BrowserFrame;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@14
    .line 914
    :cond_14
    iget-boolean v0, p0, Landroid/webkit/BrowserFrame;->mFirstLayoutDone:Z

    #@16
    if-eqz v0, :cond_21

    #@18
    const/16 v0, 0x4b

    #@1a
    if-le p1, v0, :cond_21

    #@1c
    .line 915
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@1e
    invoke-virtual {v0}, Landroid/webkit/CallbackProxy;->switchOutDrawHistory()V

    #@21
    .line 917
    :cond_21
    return-void
.end method

.method private setTitle(Ljava/lang/String;)V
    .registers 3
    .parameter "title"

    #@0
    .prologue
    .line 547
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0, p1}, Landroid/webkit/CallbackProxy;->onReceivedTitle(Ljava/lang/String;)V

    #@5
    .line 548
    return-void
.end method

.method private native setUsernamePassword(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private shouldInterceptRequest(Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .registers 10
    .parameter "url"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 874
    invoke-direct {p0, p1}, Landroid/webkit/BrowserFrame;->inputStreamForAndroidResource(Ljava/lang/String;)Ljava/io/InputStream;

    #@4
    move-result-object v0

    #@5
    .line 875
    .local v0, androidResource:Ljava/io/InputStream;
    if-eqz v0, :cond_d

    #@7
    .line 876
    new-instance v4, Landroid/webkit/WebResourceResponse;

    #@9
    invoke-direct {v4, v7, v7, v0}, Landroid/webkit/WebResourceResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)V

    #@c
    .line 897
    :cond_c
    :goto_c
    return-object v4

    #@d
    .line 881
    :cond_d
    iget-object v6, p0, Landroid/webkit/BrowserFrame;->mSettings:Landroid/webkit/WebSettingsClassic;

    #@f
    invoke-virtual {v6}, Landroid/webkit/WebSettingsClassic;->getAllowFileAccess()Z

    #@12
    move-result v6

    #@13
    if-nez v6, :cond_23

    #@15
    const-string v6, "file://"

    #@17
    invoke-virtual {p1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@1a
    move-result v6

    #@1b
    if-eqz v6, :cond_23

    #@1d
    .line 882
    new-instance v4, Landroid/webkit/WebResourceResponse;

    #@1f
    invoke-direct {v4, v7, v7, v7}, Landroid/webkit/WebResourceResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)V

    #@22
    goto :goto_c

    #@23
    .line 885
    :cond_23
    iget-object v6, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@25
    invoke-virtual {v6, p1}, Landroid/webkit/CallbackProxy;->shouldInterceptRequest(Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    #@28
    move-result-object v4

    #@29
    .line 886
    .local v4, response:Landroid/webkit/WebResourceResponse;
    if-nez v4, :cond_c

    #@2b
    const-string v6, "browser:incognito"

    #@2d
    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v6

    #@31
    if-eqz v6, :cond_c

    #@33
    .line 888
    :try_start_33
    iget-object v6, p0, Landroid/webkit/BrowserFrame;->mContext:Landroid/content/Context;

    #@35
    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@38
    move-result-object v3

    #@39
    .line 889
    .local v3, res:Landroid/content/res/Resources;
    const v6, 0x1100002

    #@3c
    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    #@3f
    move-result-object v2

    #@40
    .line 891
    .local v2, ins:Ljava/io/InputStream;
    new-instance v5, Landroid/webkit/WebResourceResponse;

    #@42
    const-string/jumbo v6, "text/html"

    #@45
    const-string/jumbo v7, "utf8"

    #@48
    invoke-direct {v5, v6, v7, v2}, Landroid/webkit/WebResourceResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)V
    :try_end_4b
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_33 .. :try_end_4b} :catch_4d

    #@4b
    .end local v4           #response:Landroid/webkit/WebResourceResponse;
    .local v5, response:Landroid/webkit/WebResourceResponse;
    move-object v4, v5

    #@4c
    .line 895
    .end local v5           #response:Landroid/webkit/WebResourceResponse;
    .restart local v4       #response:Landroid/webkit/WebResourceResponse;
    goto :goto_c

    #@4d
    .line 892
    .end local v2           #ins:Ljava/io/InputStream;
    .end local v3           #res:Landroid/content/res/Resources;
    :catch_4d
    move-exception v1

    #@4e
    .line 894
    .local v1, ex:Landroid/content/res/Resources$NotFoundException;
    const-string/jumbo v6, "webkit"

    #@51
    const-string v7, "Failed opening raw.incognito_mode_start_page"

    #@53
    invoke-static {v6, v7, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@56
    goto :goto_c
.end method

.method private shouldSaveFormData()Z
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 441
    iget-object v2, p0, Landroid/webkit/BrowserFrame;->mSettings:Landroid/webkit/WebSettingsClassic;

    #@3
    invoke-virtual {v2}, Landroid/webkit/WebSettingsClassic;->getSaveFormData()Z

    #@6
    move-result v2

    #@7
    if-eqz v2, :cond_1c

    #@9
    .line 442
    iget-object v2, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@b
    invoke-virtual {v2}, Landroid/webkit/CallbackProxy;->getBackForwardList()Landroid/webkit/WebBackForwardListClassic;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2}, Landroid/webkit/WebBackForwardListClassic;->getCurrentItem()Landroid/webkit/WebHistoryItemClassic;

    #@12
    move-result-object v0

    #@13
    .line 444
    .local v0, h:Landroid/webkit/WebHistoryItem;
    if-eqz v0, :cond_1c

    #@15
    invoke-virtual {v0}, Landroid/webkit/WebHistoryItemClassic;->getUrl()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    if-eqz v2, :cond_1c

    #@1b
    const/4 v1, 0x1

    #@1c
    .line 446
    .end local v0           #h:Landroid/webkit/WebHistoryItem;
    :cond_1c
    return v1
.end method

.method private transitionToCommitted(IZ)V
    .registers 4
    .parameter "loadType"
    .parameter "isMainFrame"

    #@0
    .prologue
    .line 455
    if-eqz p2, :cond_10

    #@2
    .line 456
    const/4 v0, 0x1

    #@3
    iput-boolean v0, p0, Landroid/webkit/BrowserFrame;->mCommitted:Z

    #@5
    .line 457
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@7
    invoke-virtual {v0}, Landroid/webkit/WebViewCore;->getWebViewClassic()Landroid/webkit/WebViewClassic;

    #@a
    move-result-object v0

    #@b
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mViewManager:Landroid/webkit/ViewManager;

    #@d
    invoke-virtual {v0}, Landroid/webkit/ViewManager;->postResetStateAll()V

    #@10
    .line 459
    :cond_10
    return-void
.end method

.method private updateVisitedHistory(Ljava/lang/String;Z)V
    .registers 4
    .parameter "url"
    .parameter "isReload"

    #@0
    .prologue
    .line 970
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/webkit/CallbackProxy;->doUpdateVisitedHistory(Ljava/lang/String;Z)V

    #@5
    .line 971
    return-void
.end method

.method private windowObjectCleared(I)V
    .registers 7
    .parameter "nativeFramePointer"

    #@0
    .prologue
    .line 600
    iget-object v3, p0, Landroid/webkit/BrowserFrame;->mJavaScriptObjects:Ljava/util/Map;

    #@2
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    #@5
    move-result-object v3

    #@6
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v1

    #@a
    .line 601
    .local v1, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_a
    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v3

    #@e
    if-eqz v3, :cond_2c

    #@10
    .line 602
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Ljava/lang/String;

    #@16
    .line 603
    .local v0, interfaceName:Ljava/lang/String;
    iget-object v3, p0, Landroid/webkit/BrowserFrame;->mJavaScriptObjects:Ljava/util/Map;

    #@18
    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1b
    move-result-object v2

    #@1c
    check-cast v2, Landroid/webkit/BrowserFrame$JSObject;

    #@1e
    .line 604
    .local v2, jsobject:Landroid/webkit/BrowserFrame$JSObject;
    if-eqz v2, :cond_a

    #@20
    iget-object v3, v2, Landroid/webkit/BrowserFrame$JSObject;->object:Ljava/lang/Object;

    #@22
    if-eqz v3, :cond_a

    #@24
    .line 605
    iget-object v3, v2, Landroid/webkit/BrowserFrame$JSObject;->object:Ljava/lang/Object;

    #@26
    iget-boolean v4, v2, Landroid/webkit/BrowserFrame$JSObject;->requireAnnotation:Z

    #@28
    invoke-direct {p0, p1, v3, v0, v4}, Landroid/webkit/BrowserFrame;->nativeAddJavascriptInterface(ILjava/lang/Object;Ljava/lang/String;Z)V

    #@2b
    goto :goto_a

    #@2c
    .line 609
    .end local v0           #interfaceName:Ljava/lang/String;
    .end local v2           #jsobject:Landroid/webkit/BrowserFrame$JSObject;
    :cond_2c
    iget-object v3, p0, Landroid/webkit/BrowserFrame;->mRemovedJavaScriptObjects:Ljava/util/Set;

    #@2e
    invoke-interface {v3}, Ljava/util/Set;->clear()V

    #@31
    .line 610
    return-void
.end method


# virtual methods
.method public addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;Z)V
    .registers 6
    .parameter "obj"
    .parameter "interfaceName"
    .parameter "requireAnnotation"

    #@0
    .prologue
    .line 660
    sget-boolean v0, Landroid/webkit/BrowserFrame;->$assertionsDisabled:Z

    #@2
    if-nez v0, :cond_c

    #@4
    if-nez p1, :cond_c

    #@6
    new-instance v0, Ljava/lang/AssertionError;

    #@8
    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    #@b
    throw v0

    #@c
    .line 661
    :cond_c
    invoke-virtual {p0, p2}, Landroid/webkit/BrowserFrame;->removeJavascriptInterface(Ljava/lang/String;)V

    #@f
    .line 662
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mJavaScriptObjects:Ljava/util/Map;

    #@11
    new-instance v1, Landroid/webkit/BrowserFrame$JSObject;

    #@13
    invoke-direct {v1, p0, p1, p3}, Landroid/webkit/BrowserFrame$JSObject;-><init>(Landroid/webkit/BrowserFrame;Ljava/lang/Object;Z)V

    #@16
    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@19
    .line 663
    return-void
.end method

.method public native clearCache()V
.end method

.method committed()Z
    .registers 2

    #@0
    .prologue
    .line 378
    iget-boolean v0, p0, Landroid/webkit/BrowserFrame;->mCommitted:Z

    #@2
    return v0
.end method

.method public destroy()V
    .registers 2

    #@0
    .prologue
    .line 483
    invoke-virtual {p0}, Landroid/webkit/BrowserFrame;->nativeDestroyFrame()V

    #@3
    .line 484
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/webkit/BrowserFrame;->mBlockMessages:Z

    #@6
    .line 485
    const/4 v0, 0x0

    #@7
    invoke-virtual {p0, v0}, Landroid/webkit/BrowserFrame;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    #@a
    .line 486
    return-void
.end method

.method didFirstLayout()V
    .registers 2

    #@0
    .prologue
    .line 390
    iget-boolean v0, p0, Landroid/webkit/BrowserFrame;->mFirstLayoutDone:Z

    #@2
    if-nez v0, :cond_c

    #@4
    .line 391
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Landroid/webkit/BrowserFrame;->mFirstLayoutDone:Z

    #@7
    .line 394
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mWebViewCore:Landroid/webkit/WebViewCore;

    #@9
    invoke-virtual {v0}, Landroid/webkit/WebViewCore;->contentDraw()V

    #@c
    .line 396
    :cond_c
    return-void
.end method

.method public documentAsText(Landroid/os/Message;)V
    .registers 4
    .parameter "callback"

    #@0
    .prologue
    .line 571
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 572
    .local v0, text:Ljava/lang/StringBuilder;
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@7
    if-eqz v1, :cond_10

    #@9
    .line 574
    invoke-direct {p0}, Landroid/webkit/BrowserFrame;->documentAsText()Ljava/lang/String;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    .line 576
    :cond_10
    iget v1, p1, Landroid/os/Message;->arg2:I

    #@12
    if-eqz v1, :cond_1b

    #@14
    .line 578
    invoke-direct {p0}, Landroid/webkit/BrowserFrame;->childFramesAsText()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    .line 580
    :cond_1b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    iput-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@21
    .line 581
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    #@24
    .line 582
    return-void
.end method

.method public native documentHasImages()Z
.end method

.method public externalRepresentation(Landroid/os/Message;)V
    .registers 3
    .parameter "callback"

    #@0
    .prologue
    .line 556
    invoke-direct {p0}, Landroid/webkit/BrowserFrame;->externalRepresentation()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@6
    .line 557
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    #@9
    .line 558
    return-void
.end method

.method firstLayoutDone()Z
    .registers 2

    #@0
    .prologue
    .line 382
    iget-boolean v0, p0, Landroid/webkit/BrowserFrame;->mFirstLayoutDone:Z

    #@2
    return v0
.end method

.method getCallbackProxy()Landroid/webkit/CallbackProxy;
    .registers 2

    #@0
    .prologue
    .line 977
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@2
    return-object v0
.end method

.method getShouldStartScrolledRight()Z
    .registers 2

    #@0
    .prologue
    .line 1369
    iget v0, p0, Landroid/webkit/BrowserFrame;->mNativeFrame:I

    #@2
    invoke-direct {p0, v0}, Landroid/webkit/BrowserFrame;->nativeGetShouldStartScrolledRight(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method getUserAgentString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 984
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mSettings:Landroid/webkit/WebSettingsClassic;

    #@2
    invoke-virtual {v0}, Landroid/webkit/WebSettingsClassic;->getUserAgentString()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public goBackOrForward(I)V
    .registers 3
    .parameter "steps"

    #@0
    .prologue
    .line 346
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/webkit/BrowserFrame;->mLoadInitFromJava:Z

    #@3
    .line 347
    invoke-direct {p0, p1}, Landroid/webkit/BrowserFrame;->nativeGoBackOrForward(I)V

    #@6
    .line 348
    const/4 v0, 0x0

    #@7
    iput-boolean v0, p0, Landroid/webkit/BrowserFrame;->mLoadInitFromJava:Z

    #@9
    .line 349
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 9
    .parameter "msg"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 494
    iget-boolean v4, p0, Landroid/webkit/BrowserFrame;->mBlockMessages:Z

    #@3
    if-eqz v4, :cond_6

    #@5
    .line 536
    :cond_5
    :goto_5
    return-void

    #@6
    .line 497
    :cond_6
    iget v4, p1, Landroid/os/Message;->what:I

    #@8
    packed-switch v4, :pswitch_data_a8

    #@b
    goto :goto_5

    #@c
    .line 499
    :pswitch_c
    iget-object v4, p0, Landroid/webkit/BrowserFrame;->mSettings:Landroid/webkit/WebSettingsClassic;

    #@e
    invoke-virtual {v4}, Landroid/webkit/WebSettingsClassic;->getSavePassword()Z

    #@11
    move-result v4

    #@12
    if-eqz v4, :cond_5

    #@14
    invoke-direct {p0}, Landroid/webkit/BrowserFrame;->hasPasswordField()Z

    #@17
    move-result v4

    #@18
    if-eqz v4, :cond_5

    #@1a
    .line 500
    iget-object v4, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@1c
    invoke-virtual {v4}, Landroid/webkit/CallbackProxy;->getBackForwardList()Landroid/webkit/WebBackForwardListClassic;

    #@1f
    move-result-object v4

    #@20
    invoke-virtual {v4}, Landroid/webkit/WebBackForwardListClassic;->getCurrentItem()Landroid/webkit/WebHistoryItemClassic;

    #@23
    move-result-object v0

    #@24
    .line 502
    .local v0, item:Landroid/webkit/WebHistoryItem;
    if-eqz v0, :cond_5

    #@26
    .line 503
    new-instance v3, Landroid/net/WebAddress;

    #@28
    invoke-virtual {v0}, Landroid/webkit/WebHistoryItemClassic;->getUrl()Ljava/lang/String;

    #@2b
    move-result-object v4

    #@2c
    invoke-direct {v3, v4}, Landroid/net/WebAddress;-><init>(Ljava/lang/String;)V

    #@2f
    .line 504
    .local v3, uri:Landroid/net/WebAddress;
    new-instance v4, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    invoke-virtual {v3}, Landroid/net/WebAddress;->getScheme()Ljava/lang/String;

    #@37
    move-result-object v5

    #@38
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v4

    #@3c
    const-string v5, "://"

    #@3e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v4

    #@42
    invoke-virtual {v3}, Landroid/net/WebAddress;->getHost()Ljava/lang/String;

    #@45
    move-result-object v5

    #@46
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v4

    #@4a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v1

    #@4e
    .line 505
    .local v1, schemePlusHost:Ljava/lang/String;
    iget-object v4, p0, Landroid/webkit/BrowserFrame;->mContext:Landroid/content/Context;

    #@50
    invoke-static {v4}, Landroid/webkit/WebViewDatabaseClassic;->getInstance(Landroid/content/Context;)Landroid/webkit/WebViewDatabaseClassic;

    #@53
    move-result-object v4

    #@54
    invoke-virtual {v4, v1}, Landroid/webkit/WebViewDatabaseClassic;->getUsernamePassword(Ljava/lang/String;)[Ljava/lang/String;

    #@57
    move-result-object v2

    #@58
    .line 508
    .local v2, up:[Ljava/lang/String;
    if-nez v2, :cond_7d

    #@5a
    .line 509
    new-instance v4, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    invoke-virtual {v3}, Landroid/net/WebAddress;->getScheme()Ljava/lang/String;

    #@62
    move-result-object v5

    #@63
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v4

    #@67
    invoke-virtual {v3}, Landroid/net/WebAddress;->getHost()Ljava/lang/String;

    #@6a
    move-result-object v5

    #@6b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v4

    #@6f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v1

    #@73
    .line 510
    iget-object v4, p0, Landroid/webkit/BrowserFrame;->mContext:Landroid/content/Context;

    #@75
    invoke-static {v4}, Landroid/webkit/WebViewDatabaseClassic;->getInstance(Landroid/content/Context;)Landroid/webkit/WebViewDatabaseClassic;

    #@78
    move-result-object v4

    #@79
    invoke-virtual {v4, v1}, Landroid/webkit/WebViewDatabaseClassic;->getUsernamePassword(Ljava/lang/String;)[Ljava/lang/String;

    #@7c
    move-result-object v2

    #@7d
    .line 512
    :cond_7d
    if-eqz v2, :cond_5

    #@7f
    aget-object v4, v2, v6

    #@81
    if-eqz v4, :cond_5

    #@83
    .line 513
    aget-object v4, v2, v6

    #@85
    const/4 v5, 0x1

    #@86
    aget-object v5, v2, v5

    #@88
    invoke-direct {p0, v4, v5}, Landroid/webkit/BrowserFrame;->setUsernamePassword(Ljava/lang/String;Ljava/lang/String;)V

    #@8b
    goto/16 :goto_5

    #@8d
    .line 521
    .end local v0           #item:Landroid/webkit/WebHistoryItem;
    .end local v1           #schemePlusHost:Ljava/lang/String;
    .end local v2           #up:[Ljava/lang/String;
    .end local v3           #uri:Landroid/net/WebAddress;
    :pswitch_8d
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@8f
    iget v5, p1, Landroid/os/Message;->arg2:I

    #@91
    invoke-direct {p0, v4, v5}, Landroid/webkit/BrowserFrame;->nativeCallPolicyFunction(II)V

    #@94
    goto/16 :goto_5

    #@96
    .line 526
    :pswitch_96
    iget v4, p0, Landroid/webkit/BrowserFrame;->mOrientation:I

    #@98
    iget v5, p1, Landroid/os/Message;->arg1:I

    #@9a
    if-eq v4, v5, :cond_5

    #@9c
    .line 527
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@9e
    iput v4, p0, Landroid/webkit/BrowserFrame;->mOrientation:I

    #@a0
    .line 528
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@a2
    invoke-direct {p0, v4}, Landroid/webkit/BrowserFrame;->nativeOrientationChanged(I)V

    #@a5
    goto/16 :goto_5

    #@a7
    .line 497
    nop

    #@a8
    :pswitch_data_a8
    .packed-switch 0x3e9
        :pswitch_c
        :pswitch_96
        :pswitch_8d
    .end packed-switch
.end method

.method public handleUrl(Ljava/lang/String;)Z
    .registers 5
    .parameter "url"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 638
    iget-boolean v2, p0, Landroid/webkit/BrowserFrame;->mLoadInitFromJava:Z

    #@4
    if-ne v2, v1, :cond_7

    #@6
    .line 654
    :cond_6
    :goto_6
    return v0

    #@7
    .line 643
    :cond_7
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@a
    move-result-object v2

    #@b
    if-eqz v2, :cond_19

    #@d
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@10
    move-result-object v2

    #@11
    invoke-interface {v2, p1}, Lcom/lge/cappuccino/IMdm;->checkWebView(Ljava/lang/String;)Z

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_19

    #@17
    move v0, v1

    #@18
    .line 645
    goto :goto_6

    #@19
    .line 649
    :cond_19
    iget-object v2, p0, Landroid/webkit/BrowserFrame;->mCallbackProxy:Landroid/webkit/CallbackProxy;

    #@1b
    invoke-virtual {v2, p1}, Landroid/webkit/CallbackProxy;->shouldOverrideUrlLoading(Ljava/lang/String;)Z

    #@1e
    move-result v2

    #@1f
    if-eqz v2, :cond_6

    #@21
    .line 651
    invoke-virtual {p0}, Landroid/webkit/BrowserFrame;->didFirstLayout()V

    #@24
    move v0, v1

    #@25
    .line 652
    goto :goto_6
.end method

.method public loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "baseUrl"
    .parameter "data"
    .parameter "mimeType"
    .parameter "encoding"
    .parameter "historyUrl"

    #@0
    .prologue
    .line 308
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/webkit/BrowserFrame;->mLoadInitFromJava:Z

    #@3
    .line 309
    if-eqz p5, :cond_b

    #@5
    invoke-virtual {p5}, Ljava/lang/String;->length()I

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_d

    #@b
    .line 310
    :cond_b
    const-string p5, "about:blank"

    #@d
    .line 312
    :cond_d
    if-nez p2, :cond_11

    #@f
    .line 313
    const-string p2, ""

    #@11
    .line 318
    :cond_11
    if-eqz p1, :cond_19

    #@13
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@16
    move-result v0

    #@17
    if-nez v0, :cond_1b

    #@19
    .line 319
    :cond_19
    const-string p1, "about:blank"

    #@1b
    .line 321
    :cond_1b
    if-eqz p3, :cond_23

    #@1d
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    #@20
    move-result v0

    #@21
    if-nez v0, :cond_26

    #@23
    .line 322
    :cond_23
    const-string/jumbo p3, "text/html"

    #@26
    .line 324
    :cond_26
    invoke-direct/range {p0 .. p5}, Landroid/webkit/BrowserFrame;->nativeLoadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@29
    .line 325
    const/4 v0, 0x0

    #@2a
    iput-boolean v0, p0, Landroid/webkit/BrowserFrame;->mLoadInitFromJava:Z

    #@2c
    .line 326
    return-void
.end method

.method loadType()I
    .registers 2

    #@0
    .prologue
    .line 386
    iget v0, p0, Landroid/webkit/BrowserFrame;->mLoadType:I

    #@2
    return v0
.end method

.method public loadUrl(Ljava/lang/String;Ljava/util/Map;)V
    .registers 4
    .parameter "url"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 274
    .local p2, extraHeaders:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/webkit/BrowserFrame;->mLoadInitFromJava:Z

    #@3
    .line 275
    invoke-static {p1}, Landroid/webkit/URLUtil;->isJavaScriptUrl(Ljava/lang/String;)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_1b

    #@9
    .line 277
    const-string/jumbo v0, "javascript:"

    #@c
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@f
    move-result v0

    #@10
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    invoke-virtual {p0, v0}, Landroid/webkit/BrowserFrame;->stringByEvaluatingJavaScriptFromString(Ljava/lang/String;)Ljava/lang/String;

    #@17
    .line 282
    :goto_17
    const/4 v0, 0x0

    #@18
    iput-boolean v0, p0, Landroid/webkit/BrowserFrame;->mLoadInitFromJava:Z

    #@1a
    .line 283
    return-void

    #@1b
    .line 280
    :cond_1b
    invoke-direct {p0, p1, p2}, Landroid/webkit/BrowserFrame;->nativeLoadUrl(Ljava/lang/String;Ljava/util/Map;)V

    #@1e
    goto :goto_17
.end method

.method public native nativeDestroyFrame()V
.end method

.method native nativeSslClientCert(II[[B)V
.end method

.method native nativeSslClientCert(I[B[[B)V
.end method

.method public postUrl(Ljava/lang/String;[B)V
    .registers 4
    .parameter "url"
    .parameter "data"

    #@0
    .prologue
    .line 291
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/webkit/BrowserFrame;->mLoadInitFromJava:Z

    #@3
    .line 292
    invoke-direct {p0, p1, p2}, Landroid/webkit/BrowserFrame;->nativePostUrl(Ljava/lang/String;[B)V

    #@6
    .line 293
    const/4 v0, 0x0

    #@7
    iput-boolean v0, p0, Landroid/webkit/BrowserFrame;->mLoadInitFromJava:Z

    #@9
    .line 294
    return-void
.end method

.method public native reload(Z)V
.end method

.method public removeJavascriptInterface(Ljava/lang/String;)V
    .registers 4
    .parameter "interfaceName"

    #@0
    .prologue
    .line 669
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mJavaScriptObjects:Ljava/util/Map;

    #@2
    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_13

    #@8
    .line 670
    iget-object v0, p0, Landroid/webkit/BrowserFrame;->mRemovedJavaScriptObjects:Ljava/util/Set;

    #@a
    iget-object v1, p0, Landroid/webkit/BrowserFrame;->mJavaScriptObjects:Ljava/util/Map;

    #@c
    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    #@13
    .line 672
    :cond_13
    return-void
.end method

.method saveWebArchive(Ljava/lang/String;Z)Ljava/lang/String;
    .registers 4
    .parameter "basename"
    .parameter "autoname"

    #@0
    .prologue
    .line 337
    invoke-direct {p0, p1, p2}, Landroid/webkit/BrowserFrame;->nativeSaveWebArchive(Ljava/lang/String;Z)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public stopLoading()V
    .registers 2

    #@0
    .prologue
    .line 1311
    iget-boolean v0, p0, Landroid/webkit/BrowserFrame;->mIsMainFrame:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 1312
    invoke-direct {p0}, Landroid/webkit/BrowserFrame;->resetLoadingStates()V

    #@7
    .line 1314
    :cond_7
    invoke-direct {p0}, Landroid/webkit/BrowserFrame;->nativeStopLoading()V

    #@a
    .line 1315
    return-void
.end method

.method public native stringByEvaluatingJavaScriptFromString(Ljava/lang/String;)Ljava/lang/String;
.end method
