.class Landroid/webkit/ViewManager;
.super Ljava/lang/Object;
.source "ViewManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/ViewManager$ChildView;
    }
.end annotation


# static fields
.field private static final MAX_SURFACE_DIMENSION:I = 0x800


# instance fields
.field private final MAX_SURFACE_AREA:I

.field private final mChildren:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/webkit/ViewManager$ChildView;",
            ">;"
        }
    .end annotation
.end field

.field private mHidden:Z

.field private mReadyToDraw:Z

.field private final mWebView:Landroid/webkit/WebViewClassic;

.field private mZoomInProgress:Z


# direct methods
.method constructor <init>(Landroid/webkit/WebViewClassic;)V
    .registers 8
    .parameter "w"

    #@0
    .prologue
    .line 102
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 29
    new-instance v2, Ljava/util/ArrayList;

    #@5
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v2, p0, Landroid/webkit/ViewManager;->mChildren:Ljava/util/ArrayList;

    #@a
    .line 32
    const/4 v2, 0x0

    #@b
    iput-boolean v2, p0, Landroid/webkit/ViewManager;->mZoomInProgress:Z

    #@d
    .line 103
    iput-object p1, p0, Landroid/webkit/ViewManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@f
    .line 104
    invoke-virtual {p1}, Landroid/webkit/WebViewClassic;->getWebView()Landroid/webkit/WebView;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2}, Landroid/webkit/WebView;->getResources()Landroid/content/res/Resources;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@1a
    move-result-object v0

    #@1b
    .line 105
    .local v0, metrics:Landroid/util/DisplayMetrics;
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    #@1d
    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    #@1f
    mul-int v1, v2, v3

    #@21
    .line 110
    .local v1, pixelArea:I
    int-to-double v2, v1

    #@22
    const-wide/high16 v4, 0x4006

    #@24
    mul-double/2addr v2, v4

    #@25
    double-to-int v2, v2

    #@26
    iput v2, p0, Landroid/webkit/ViewManager;->MAX_SURFACE_AREA:I

    #@28
    .line 111
    return-void
.end method

.method static synthetic access$000(Landroid/webkit/ViewManager;Landroid/webkit/ViewManager$ChildView;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 27
    invoke-direct {p0, p1}, Landroid/webkit/ViewManager;->requestLayout(Landroid/webkit/ViewManager$ChildView;)V

    #@3
    return-void
.end method

.method static synthetic access$200(Landroid/webkit/ViewManager;)Landroid/webkit/WebViewClassic;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 27
    iget-object v0, p0, Landroid/webkit/ViewManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/webkit/ViewManager;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 27
    iget-object v0, p0, Landroid/webkit/ViewManager;->mChildren:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Landroid/webkit/ViewManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 27
    iget-boolean v0, p0, Landroid/webkit/ViewManager;->mReadyToDraw:Z

    #@2
    return v0
.end method

.method static synthetic access$402(Landroid/webkit/ViewManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 27
    iput-boolean p1, p0, Landroid/webkit/ViewManager;->mReadyToDraw:Z

    #@2
    return p1
.end method

.method private requestLayout(Landroid/webkit/ViewManager$ChildView;)V
    .registers 15
    .parameter "v"

    #@0
    .prologue
    const/16 v12, 0x800

    #@2
    .line 122
    iget-object v10, p0, Landroid/webkit/ViewManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@4
    iget v11, p1, Landroid/webkit/ViewManager$ChildView;->width:I

    #@6
    invoke-virtual {v10, v11}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@9
    move-result v7

    #@a
    .line 123
    .local v7, width:I
    iget-object v10, p0, Landroid/webkit/ViewManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@c
    iget v11, p1, Landroid/webkit/ViewManager$ChildView;->height:I

    #@e
    invoke-virtual {v10, v11}, Landroid/webkit/WebViewClassic;->contentToViewDimension(I)I

    #@11
    move-result v3

    #@12
    .line 124
    .local v3, height:I
    iget-object v10, p0, Landroid/webkit/ViewManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@14
    iget v11, p1, Landroid/webkit/ViewManager$ChildView;->x:I

    #@16
    invoke-virtual {v10, v11}, Landroid/webkit/WebViewClassic;->contentToViewX(I)I

    #@19
    move-result v8

    #@1a
    .line 125
    .local v8, x:I
    iget-object v10, p0, Landroid/webkit/ViewManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@1c
    iget v11, p1, Landroid/webkit/ViewManager$ChildView;->y:I

    #@1e
    invoke-virtual {v10, v11}, Landroid/webkit/WebViewClassic;->contentToViewY(I)I

    #@21
    move-result v9

    #@22
    .line 128
    .local v9, y:I
    iget-object v10, p1, Landroid/webkit/ViewManager$ChildView;->mView:Landroid/view/View;

    #@24
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@27
    move-result-object v4

    #@28
    .line 130
    .local v4, layoutParams:Landroid/view/ViewGroup$LayoutParams;
    instance-of v10, v4, Landroid/widget/AbsoluteLayout$LayoutParams;

    #@2a
    if-eqz v10, :cond_51

    #@2c
    move-object v5, v4

    #@2d
    .line 131
    check-cast v5, Landroid/widget/AbsoluteLayout$LayoutParams;

    #@2f
    .line 132
    .local v5, lp:Landroid/widget/AbsoluteLayout$LayoutParams;
    iput v7, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@31
    .line 133
    iput v3, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@33
    .line 134
    iput v8, v5, Landroid/widget/AbsoluteLayout$LayoutParams;->x:I

    #@35
    .line 135
    iput v9, v5, Landroid/widget/AbsoluteLayout$LayoutParams;->y:I

    #@37
    .line 141
    :goto_37
    iget-object v10, p1, Landroid/webkit/ViewManager$ChildView;->mView:Landroid/view/View;

    #@39
    invoke-virtual {v10, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@3c
    .line 143
    iget-object v10, p1, Landroid/webkit/ViewManager$ChildView;->mView:Landroid/view/View;

    #@3e
    instance-of v10, v10, Landroid/view/SurfaceView;

    #@40
    if-eqz v10, :cond_50

    #@42
    .line 145
    iget-object v6, p1, Landroid/webkit/ViewManager$ChildView;->mView:Landroid/view/View;

    #@44
    check-cast v6, Landroid/view/SurfaceView;

    #@46
    .line 147
    .local v6, sView:Landroid/view/SurfaceView;
    invoke-virtual {v6}, Landroid/view/SurfaceView;->isFixedSize()Z

    #@49
    move-result v10

    #@4a
    if-eqz v10, :cond_57

    #@4c
    iget-boolean v10, p0, Landroid/webkit/ViewManager;->mZoomInProgress:Z

    #@4e
    if-eqz v10, :cond_57

    #@50
    .line 221
    .end local v6           #sView:Landroid/view/SurfaceView;
    :cond_50
    :goto_50
    return-void

    #@51
    .line 137
    .end local v5           #lp:Landroid/widget/AbsoluteLayout$LayoutParams;
    :cond_51
    new-instance v5, Landroid/widget/AbsoluteLayout$LayoutParams;

    #@53
    invoke-direct {v5, v7, v3, v8, v9}, Landroid/widget/AbsoluteLayout$LayoutParams;-><init>(IIII)V

    #@56
    .restart local v5       #lp:Landroid/widget/AbsoluteLayout$LayoutParams;
    goto :goto_37

    #@57
    .line 162
    .restart local v6       #sView:Landroid/view/SurfaceView;
    :cond_57
    move v2, v7

    #@58
    .line 163
    .local v2, fixedW:I
    move v1, v3

    #@59
    .line 164
    .local v1, fixedH:I
    if-gt v2, v12, :cond_5d

    #@5b
    if-le v1, v12, :cond_6d

    #@5d
    .line 165
    :cond_5d
    iget v10, p1, Landroid/webkit/ViewManager$ChildView;->width:I

    #@5f
    iget v11, p1, Landroid/webkit/ViewManager$ChildView;->height:I

    #@61
    if-le v10, v11, :cond_9d

    #@63
    .line 166
    const/16 v2, 0x800

    #@65
    .line 167
    iget v10, p1, Landroid/webkit/ViewManager$ChildView;->height:I

    #@67
    mul-int/lit16 v10, v10, 0x800

    #@69
    iget v11, p1, Landroid/webkit/ViewManager$ChildView;->width:I

    #@6b
    div-int v1, v10, v11

    #@6d
    .line 173
    :cond_6d
    :goto_6d
    mul-int v10, v2, v1

    #@6f
    iget v11, p0, Landroid/webkit/ViewManager;->MAX_SURFACE_AREA:I

    #@71
    if-le v10, v11, :cond_91

    #@73
    .line 174
    iget v10, p0, Landroid/webkit/ViewManager;->MAX_SURFACE_AREA:I

    #@75
    int-to-float v0, v10

    #@76
    .line 175
    .local v0, area:F
    iget v10, p1, Landroid/webkit/ViewManager$ChildView;->width:I

    #@78
    iget v11, p1, Landroid/webkit/ViewManager$ChildView;->height:I

    #@7a
    if-le v10, v11, :cond_a8

    #@7c
    .line 176
    iget v10, p1, Landroid/webkit/ViewManager$ChildView;->width:I

    #@7e
    int-to-float v10, v10

    #@7f
    mul-float/2addr v10, v0

    #@80
    iget v11, p1, Landroid/webkit/ViewManager$ChildView;->height:I

    #@82
    int-to-float v11, v11

    #@83
    div-float/2addr v10, v11

    #@84
    float-to-double v10, v10

    #@85
    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    #@88
    move-result-wide v10

    #@89
    double-to-int v2, v10

    #@8a
    .line 177
    iget v10, p1, Landroid/webkit/ViewManager$ChildView;->height:I

    #@8c
    mul-int/2addr v10, v2

    #@8d
    iget v11, p1, Landroid/webkit/ViewManager$ChildView;->width:I

    #@8f
    div-int v1, v10, v11

    #@91
    .line 184
    .end local v0           #area:F
    :cond_91
    :goto_91
    if-ne v2, v7, :cond_95

    #@93
    if-eq v1, v3, :cond_be

    #@95
    .line 187
    :cond_95
    invoke-virtual {v6}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    #@98
    move-result-object v10

    #@99
    invoke-interface {v10, v2, v1}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    #@9c
    goto :goto_50

    #@9d
    .line 169
    :cond_9d
    const/16 v1, 0x800

    #@9f
    .line 170
    iget v10, p1, Landroid/webkit/ViewManager$ChildView;->width:I

    #@a1
    mul-int/lit16 v10, v10, 0x800

    #@a3
    iget v11, p1, Landroid/webkit/ViewManager$ChildView;->height:I

    #@a5
    div-int v2, v10, v11

    #@a7
    goto :goto_6d

    #@a8
    .line 179
    .restart local v0       #area:F
    :cond_a8
    iget v10, p1, Landroid/webkit/ViewManager$ChildView;->height:I

    #@aa
    int-to-float v10, v10

    #@ab
    mul-float/2addr v10, v0

    #@ac
    iget v11, p1, Landroid/webkit/ViewManager$ChildView;->width:I

    #@ae
    int-to-float v11, v11

    #@af
    div-float/2addr v10, v11

    #@b0
    float-to-double v10, v10

    #@b1
    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    #@b4
    move-result-wide v10

    #@b5
    double-to-int v1, v10

    #@b6
    .line 180
    iget v10, p1, Landroid/webkit/ViewManager$ChildView;->width:I

    #@b8
    mul-int/2addr v10, v1

    #@b9
    iget v11, p1, Landroid/webkit/ViewManager$ChildView;->height:I

    #@bb
    div-int v2, v10, v11

    #@bd
    goto :goto_91

    #@be
    .line 188
    .end local v0           #area:F
    :cond_be
    invoke-virtual {v6}, Landroid/view/SurfaceView;->isFixedSize()Z

    #@c1
    move-result v10

    #@c2
    if-nez v10, :cond_d9

    #@c4
    iget-boolean v10, p0, Landroid/webkit/ViewManager;->mZoomInProgress:Z

    #@c6
    if-eqz v10, :cond_d9

    #@c8
    .line 191
    invoke-virtual {v6}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    #@cb
    move-result-object v10

    #@cc
    invoke-virtual {v6}, Landroid/view/SurfaceView;->getWidth()I

    #@cf
    move-result v11

    #@d0
    invoke-virtual {v6}, Landroid/view/SurfaceView;->getHeight()I

    #@d3
    move-result v12

    #@d4
    invoke-interface {v10, v11, v12}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    #@d7
    goto/16 :goto_50

    #@d9
    .line 193
    :cond_d9
    invoke-virtual {v6}, Landroid/view/SurfaceView;->isFixedSize()Z

    #@dc
    move-result v10

    #@dd
    if-eqz v10, :cond_50

    #@df
    iget-boolean v10, p0, Landroid/webkit/ViewManager;->mZoomInProgress:Z

    #@e1
    if-nez v10, :cond_50

    #@e3
    .line 205
    invoke-virtual {v6}, Landroid/view/SurfaceView;->getVisibility()I

    #@e6
    move-result v10

    #@e7
    if-nez v10, :cond_102

    #@e9
    .line 206
    const/4 v10, 0x4

    #@ea
    invoke-virtual {v6, v10}, Landroid/view/SurfaceView;->setVisibility(I)V

    #@ed
    .line 207
    invoke-virtual {v6}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    #@f0
    move-result-object v10

    #@f1
    invoke-interface {v10}, Landroid/view/SurfaceHolder;->setSizeFromLayout()V

    #@f4
    .line 211
    iget-object v10, p0, Landroid/webkit/ViewManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@f6
    iget-object v10, v10, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@f8
    new-instance v11, Landroid/webkit/ViewManager$1;

    #@fa
    invoke-direct {v11, p0, v6}, Landroid/webkit/ViewManager$1;-><init>(Landroid/webkit/ViewManager;Landroid/view/SurfaceView;)V

    #@fd
    invoke-virtual {v10, v11}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@100
    goto/16 :goto_50

    #@102
    .line 217
    :cond_102
    invoke-virtual {v6}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    #@105
    move-result-object v10

    #@106
    invoke-interface {v10}, Landroid/view/SurfaceHolder;->setSizeFromLayout()V

    #@109
    goto/16 :goto_50
.end method


# virtual methods
.method createView()Landroid/webkit/ViewManager$ChildView;
    .registers 2

    #@0
    .prologue
    .line 114
    new-instance v0, Landroid/webkit/ViewManager$ChildView;

    #@2
    invoke-direct {v0, p0}, Landroid/webkit/ViewManager$ChildView;-><init>(Landroid/webkit/ViewManager;)V

    #@5
    return-object v0
.end method

.method endZoom()V
    .registers 4

    #@0
    .prologue
    .line 231
    const/4 v2, 0x0

    #@1
    iput-boolean v2, p0, Landroid/webkit/ViewManager;->mZoomInProgress:Z

    #@3
    .line 232
    iget-object v2, p0, Landroid/webkit/ViewManager;->mChildren:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@8
    move-result-object v0

    #@9
    .local v0, i$:Ljava/util/Iterator;
    :goto_9
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_19

    #@f
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@12
    move-result-object v1

    #@13
    check-cast v1, Landroid/webkit/ViewManager$ChildView;

    #@15
    .line 233
    .local v1, v:Landroid/webkit/ViewManager$ChildView;
    invoke-direct {p0, v1}, Landroid/webkit/ViewManager;->requestLayout(Landroid/webkit/ViewManager$ChildView;)V

    #@18
    goto :goto_9

    #@19
    .line 235
    .end local v1           #v:Landroid/webkit/ViewManager$ChildView;
    :cond_19
    return-void
.end method

.method hideAll()V
    .registers 5

    #@0
    .prologue
    .line 244
    iget-boolean v2, p0, Landroid/webkit/ViewManager;->mHidden:Z

    #@2
    if-eqz v2, :cond_5

    #@4
    .line 251
    :goto_4
    return-void

    #@5
    .line 247
    :cond_5
    iget-object v2, p0, Landroid/webkit/ViewManager;->mChildren:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v0

    #@b
    .local v0, i$:Ljava/util/Iterator;
    :goto_b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v2

    #@f
    if-eqz v2, :cond_1f

    #@11
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v1

    #@15
    check-cast v1, Landroid/webkit/ViewManager$ChildView;

    #@17
    .line 248
    .local v1, v:Landroid/webkit/ViewManager$ChildView;
    iget-object v2, v1, Landroid/webkit/ViewManager$ChildView;->mView:Landroid/view/View;

    #@19
    const/16 v3, 0x8

    #@1b
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    #@1e
    goto :goto_b

    #@1f
    .line 250
    .end local v1           #v:Landroid/webkit/ViewManager$ChildView;
    :cond_1f
    const/4 v2, 0x1

    #@20
    iput-boolean v2, p0, Landroid/webkit/ViewManager;->mHidden:Z

    #@22
    goto :goto_4
.end method

.method hitTest(II)Landroid/webkit/ViewManager$ChildView;
    .registers 8
    .parameter "contentX"
    .parameter "contentY"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 283
    iget-boolean v3, p0, Landroid/webkit/ViewManager;->mHidden:Z

    #@3
    if-eqz v3, :cond_7

    #@5
    move-object v1, v2

    #@6
    .line 294
    :goto_6
    return-object v1

    #@7
    .line 286
    :cond_7
    iget-object v3, p0, Landroid/webkit/ViewManager;->mChildren:Ljava/util/ArrayList;

    #@9
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@c
    move-result-object v0

    #@d
    .local v0, i$:Ljava/util/Iterator;
    :cond_d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@10
    move-result v3

    #@11
    if-eqz v3, :cond_38

    #@13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@16
    move-result-object v1

    #@17
    check-cast v1, Landroid/webkit/ViewManager$ChildView;

    #@19
    .line 287
    .local v1, v:Landroid/webkit/ViewManager$ChildView;
    iget-object v3, v1, Landroid/webkit/ViewManager$ChildView;->mView:Landroid/view/View;

    #@1b
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    #@1e
    move-result v3

    #@1f
    if-nez v3, :cond_d

    #@21
    .line 288
    iget v3, v1, Landroid/webkit/ViewManager$ChildView;->x:I

    #@23
    if-lt p1, v3, :cond_d

    #@25
    iget v3, v1, Landroid/webkit/ViewManager$ChildView;->x:I

    #@27
    iget v4, v1, Landroid/webkit/ViewManager$ChildView;->width:I

    #@29
    add-int/2addr v3, v4

    #@2a
    if-ge p1, v3, :cond_d

    #@2c
    iget v3, v1, Landroid/webkit/ViewManager$ChildView;->y:I

    #@2e
    if-lt p2, v3, :cond_d

    #@30
    iget v3, v1, Landroid/webkit/ViewManager$ChildView;->y:I

    #@32
    iget v4, v1, Landroid/webkit/ViewManager$ChildView;->height:I

    #@34
    add-int/2addr v3, v4

    #@35
    if-ge p2, v3, :cond_d

    #@37
    goto :goto_6

    #@38
    .end local v1           #v:Landroid/webkit/ViewManager$ChildView;
    :cond_38
    move-object v1, v2

    #@39
    .line 294
    goto :goto_6
.end method

.method postReadyToDrawAll()V
    .registers 3

    #@0
    .prologue
    .line 272
    iget-object v0, p0, Landroid/webkit/ViewManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@2
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@4
    new-instance v1, Landroid/webkit/ViewManager$3;

    #@6
    invoke-direct {v1, p0}, Landroid/webkit/ViewManager$3;-><init>(Landroid/webkit/ViewManager;)V

    #@9
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@c
    .line 280
    return-void
.end method

.method postResetStateAll()V
    .registers 3

    #@0
    .prologue
    .line 264
    iget-object v0, p0, Landroid/webkit/ViewManager;->mWebView:Landroid/webkit/WebViewClassic;

    #@2
    iget-object v0, v0, Landroid/webkit/WebViewClassic;->mPrivateHandler:Landroid/os/Handler;

    #@4
    new-instance v1, Landroid/webkit/ViewManager$2;

    #@6
    invoke-direct {v1, p0}, Landroid/webkit/ViewManager$2;-><init>(Landroid/webkit/ViewManager;)V

    #@9
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@c
    .line 269
    return-void
.end method

.method scaleAll()V
    .registers 4

    #@0
    .prologue
    .line 238
    iget-object v2, p0, Landroid/webkit/ViewManager;->mChildren:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v0

    #@6
    .local v0, i$:Ljava/util/Iterator;
    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_16

    #@c
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Landroid/webkit/ViewManager$ChildView;

    #@12
    .line 239
    .local v1, v:Landroid/webkit/ViewManager$ChildView;
    invoke-direct {p0, v1}, Landroid/webkit/ViewManager;->requestLayout(Landroid/webkit/ViewManager$ChildView;)V

    #@15
    goto :goto_6

    #@16
    .line 241
    .end local v1           #v:Landroid/webkit/ViewManager$ChildView;
    :cond_16
    return-void
.end method

.method showAll()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 254
    iget-boolean v2, p0, Landroid/webkit/ViewManager;->mHidden:Z

    #@3
    if-nez v2, :cond_6

    #@5
    .line 261
    :goto_5
    return-void

    #@6
    .line 257
    :cond_6
    iget-object v2, p0, Landroid/webkit/ViewManager;->mChildren:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@b
    move-result-object v0

    #@c
    .local v0, i$:Ljava/util/Iterator;
    :goto_c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@f
    move-result v2

    #@10
    if-eqz v2, :cond_1e

    #@12
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@15
    move-result-object v1

    #@16
    check-cast v1, Landroid/webkit/ViewManager$ChildView;

    #@18
    .line 258
    .local v1, v:Landroid/webkit/ViewManager$ChildView;
    iget-object v2, v1, Landroid/webkit/ViewManager$ChildView;->mView:Landroid/view/View;

    #@1a
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    #@1d
    goto :goto_c

    #@1e
    .line 260
    .end local v1           #v:Landroid/webkit/ViewManager$ChildView;
    :cond_1e
    iput-boolean v3, p0, Landroid/webkit/ViewManager;->mHidden:Z

    #@20
    goto :goto_5
.end method

.method startZoom()V
    .registers 4

    #@0
    .prologue
    .line 224
    const/4 v2, 0x1

    #@1
    iput-boolean v2, p0, Landroid/webkit/ViewManager;->mZoomInProgress:Z

    #@3
    .line 225
    iget-object v2, p0, Landroid/webkit/ViewManager;->mChildren:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@8
    move-result-object v0

    #@9
    .local v0, i$:Ljava/util/Iterator;
    :goto_9
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_19

    #@f
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@12
    move-result-object v1

    #@13
    check-cast v1, Landroid/webkit/ViewManager$ChildView;

    #@15
    .line 226
    .local v1, v:Landroid/webkit/ViewManager$ChildView;
    invoke-direct {p0, v1}, Landroid/webkit/ViewManager;->requestLayout(Landroid/webkit/ViewManager$ChildView;)V

    #@18
    goto :goto_9

    #@19
    .line 228
    .end local v1           #v:Landroid/webkit/ViewManager$ChildView;
    :cond_19
    return-void
.end method
