.class public Landroid/webkit/WebStorage$Origin;
.super Ljava/lang/Object;
.source "WebStorage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/WebStorage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Origin"
.end annotation


# instance fields
.field private mOrigin:Ljava/lang/String;

.field private mQuota:J

.field private mUsage:J


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .registers 5
    .parameter "origin"

    #@0
    .prologue
    const-wide/16 v1, 0x0

    #@2
    .line 84
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 66
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Landroid/webkit/WebStorage$Origin;->mOrigin:Ljava/lang/String;

    #@8
    .line 67
    iput-wide v1, p0, Landroid/webkit/WebStorage$Origin;->mQuota:J

    #@a
    .line 68
    iput-wide v1, p0, Landroid/webkit/WebStorage$Origin;->mUsage:J

    #@c
    .line 85
    iput-object p1, p0, Landroid/webkit/WebStorage$Origin;->mOrigin:Ljava/lang/String;

    #@e
    .line 86
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;J)V
    .registers 7
    .parameter "origin"
    .parameter "quota"

    #@0
    .prologue
    const-wide/16 v1, 0x0

    #@2
    .line 78
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 66
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Landroid/webkit/WebStorage$Origin;->mOrigin:Ljava/lang/String;

    #@8
    .line 67
    iput-wide v1, p0, Landroid/webkit/WebStorage$Origin;->mQuota:J

    #@a
    .line 68
    iput-wide v1, p0, Landroid/webkit/WebStorage$Origin;->mUsage:J

    #@c
    .line 79
    iput-object p1, p0, Landroid/webkit/WebStorage$Origin;->mOrigin:Ljava/lang/String;

    #@e
    .line 80
    iput-wide p2, p0, Landroid/webkit/WebStorage$Origin;->mQuota:J

    #@10
    .line 81
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;JJ)V
    .registers 9
    .parameter "origin"
    .parameter "quota"
    .parameter "usage"

    #@0
    .prologue
    const-wide/16 v1, 0x0

    #@2
    .line 71
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 66
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Landroid/webkit/WebStorage$Origin;->mOrigin:Ljava/lang/String;

    #@8
    .line 67
    iput-wide v1, p0, Landroid/webkit/WebStorage$Origin;->mQuota:J

    #@a
    .line 68
    iput-wide v1, p0, Landroid/webkit/WebStorage$Origin;->mUsage:J

    #@c
    .line 72
    iput-object p1, p0, Landroid/webkit/WebStorage$Origin;->mOrigin:Ljava/lang/String;

    #@e
    .line 73
    iput-wide p2, p0, Landroid/webkit/WebStorage$Origin;->mQuota:J

    #@10
    .line 74
    iput-wide p4, p0, Landroid/webkit/WebStorage$Origin;->mUsage:J

    #@12
    .line 75
    return-void
.end method


# virtual methods
.method public getOrigin()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 99
    iget-object v0, p0, Landroid/webkit/WebStorage$Origin;->mOrigin:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getQuota()J
    .registers 3

    #@0
    .prologue
    .line 110
    iget-wide v0, p0, Landroid/webkit/WebStorage$Origin;->mQuota:J

    #@2
    return-wide v0
.end method

.method public getUsage()J
    .registers 3

    #@0
    .prologue
    .line 120
    iget-wide v0, p0, Landroid/webkit/WebStorage$Origin;->mUsage:J

    #@2
    return-wide v0
.end method
