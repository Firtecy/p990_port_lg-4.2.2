.class Landroid/webkit/LGScrapManager$1;
.super Ljava/lang/Object;
.source "LGScrapManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/webkit/LGScrapManager;->saveBitmapToFileCache(Landroid/graphics/Bitmap;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/webkit/LGScrapManager;

.field final synthetic val$bitmap_copy:Landroid/graphics/Bitmap;

.field final synthetic val$fileCacheItem:Ljava/io/File;

.field final synthetic val$fileName_copy:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/webkit/LGScrapManager;Ljava/io/File;Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 131
    iput-object p1, p0, Landroid/webkit/LGScrapManager$1;->this$0:Landroid/webkit/LGScrapManager;

    #@2
    iput-object p2, p0, Landroid/webkit/LGScrapManager$1;->val$fileCacheItem:Ljava/io/File;

    #@4
    iput-object p3, p0, Landroid/webkit/LGScrapManager$1;->val$bitmap_copy:Landroid/graphics/Bitmap;

    #@6
    iput-object p4, p0, Landroid/webkit/LGScrapManager$1;->val$fileName_copy:Ljava/lang/String;

    #@8
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@b
    return-void
.end method


# virtual methods
.method public run()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 133
    const/4 v1, 0x0

    #@2
    .line 134
    .local v1, out:Ljava/io/OutputStream;
    const/4 v3, 0x1

    #@3
    .line 136
    .local v3, send:Z
    :try_start_3
    new-instance v2, Ljava/io/FileOutputStream;

    #@5
    iget-object v4, p0, Landroid/webkit/LGScrapManager$1;->val$fileCacheItem:Ljava/io/File;

    #@7
    invoke-direct {v2, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_3a
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_a} :catch_26

    #@a
    .line 137
    .end local v1           #out:Ljava/io/OutputStream;
    .local v2, out:Ljava/io/OutputStream;
    :try_start_a
    iget-object v4, p0, Landroid/webkit/LGScrapManager$1;->val$bitmap_copy:Landroid/graphics/Bitmap;

    #@c
    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    #@e
    const/16 v6, 0x64

    #@10
    invoke-virtual {v4, v5, v6, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    #@13
    .line 138
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V
    :try_end_16
    .catchall {:try_start_a .. :try_end_16} :catchall_8f
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_16} :catch_92

    #@16
    .line 143
    if-eqz v2, :cond_24

    #@18
    .line 145
    :try_start_18
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_1b
    .catchall {:try_start_18 .. :try_end_1b} :catchall_84
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_1b} :catch_78

    #@1b
    .line 151
    if-ne v3, v7, :cond_24

    #@1d
    .line 152
    iget-object v4, p0, Landroid/webkit/LGScrapManager$1;->this$0:Landroid/webkit/LGScrapManager;

    #@1f
    iget-object v5, p0, Landroid/webkit/LGScrapManager$1;->val$fileName_copy:Ljava/lang/String;

    #@21
    :goto_21
    invoke-static {v4, v5}, Landroid/webkit/LGScrapManager;->access$100(Landroid/webkit/LGScrapManager;Ljava/lang/String;)V

    #@24
    :cond_24
    move-object v1, v2

    #@25
    .line 157
    .end local v2           #out:Ljava/io/OutputStream;
    .restart local v1       #out:Ljava/io/OutputStream;
    :cond_25
    :goto_25
    return-void

    #@26
    .line 139
    :catch_26
    move-exception v0

    #@27
    .line 140
    .local v0, e:Ljava/lang/Exception;
    :goto_27
    :try_start_27
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2a
    .catchall {:try_start_27 .. :try_end_2a} :catchall_3a

    #@2a
    .line 141
    const/4 v3, 0x0

    #@2b
    .line 143
    if-eqz v1, :cond_25

    #@2d
    .line 145
    :try_start_2d
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_30
    .catchall {:try_start_2d .. :try_end_30} :catchall_6d
    .catch Ljava/lang/Exception; {:try_start_2d .. :try_end_30} :catch_61

    #@30
    .line 151
    if-ne v3, v7, :cond_25

    #@32
    .line 152
    iget-object v4, p0, Landroid/webkit/LGScrapManager$1;->this$0:Landroid/webkit/LGScrapManager;

    #@34
    iget-object v5, p0, Landroid/webkit/LGScrapManager$1;->val$fileName_copy:Ljava/lang/String;

    #@36
    :goto_36
    invoke-static {v4, v5}, Landroid/webkit/LGScrapManager;->access$100(Landroid/webkit/LGScrapManager;Ljava/lang/String;)V

    #@39
    goto :goto_25

    #@3a
    .line 143
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_3a
    move-exception v4

    #@3b
    :goto_3b
    if-eqz v1, :cond_49

    #@3d
    .line 145
    :try_start_3d
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_40
    .catchall {:try_start_3d .. :try_end_40} :catchall_56
    .catch Ljava/lang/Exception; {:try_start_3d .. :try_end_40} :catch_4a

    #@40
    .line 151
    if-ne v3, v7, :cond_49

    #@42
    .line 152
    iget-object v5, p0, Landroid/webkit/LGScrapManager$1;->this$0:Landroid/webkit/LGScrapManager;

    #@44
    iget-object v6, p0, Landroid/webkit/LGScrapManager$1;->val$fileName_copy:Ljava/lang/String;

    #@46
    :goto_46
    invoke-static {v5, v6}, Landroid/webkit/LGScrapManager;->access$100(Landroid/webkit/LGScrapManager;Ljava/lang/String;)V

    #@49
    .line 143
    :cond_49
    throw v4

    #@4a
    .line 146
    :catch_4a
    move-exception v0

    #@4b
    .line 148
    .restart local v0       #e:Ljava/lang/Exception;
    :try_start_4b
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4e
    .catchall {:try_start_4b .. :try_end_4e} :catchall_56

    #@4e
    .line 149
    const/4 v3, 0x0

    #@4f
    .line 151
    if-ne v3, v7, :cond_49

    #@51
    .line 152
    iget-object v5, p0, Landroid/webkit/LGScrapManager$1;->this$0:Landroid/webkit/LGScrapManager;

    #@53
    iget-object v6, p0, Landroid/webkit/LGScrapManager$1;->val$fileName_copy:Ljava/lang/String;

    #@55
    goto :goto_46

    #@56
    .line 151
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_56
    move-exception v4

    #@57
    if-ne v3, v7, :cond_60

    #@59
    .line 152
    iget-object v5, p0, Landroid/webkit/LGScrapManager$1;->this$0:Landroid/webkit/LGScrapManager;

    #@5b
    iget-object v6, p0, Landroid/webkit/LGScrapManager$1;->val$fileName_copy:Ljava/lang/String;

    #@5d
    invoke-static {v5, v6}, Landroid/webkit/LGScrapManager;->access$100(Landroid/webkit/LGScrapManager;Ljava/lang/String;)V

    #@60
    .line 151
    :cond_60
    throw v4

    #@61
    .line 146
    .restart local v0       #e:Ljava/lang/Exception;
    :catch_61
    move-exception v0

    #@62
    .line 148
    :try_start_62
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_65
    .catchall {:try_start_62 .. :try_end_65} :catchall_6d

    #@65
    .line 149
    const/4 v3, 0x0

    #@66
    .line 151
    if-ne v3, v7, :cond_25

    #@68
    .line 152
    iget-object v4, p0, Landroid/webkit/LGScrapManager$1;->this$0:Landroid/webkit/LGScrapManager;

    #@6a
    iget-object v5, p0, Landroid/webkit/LGScrapManager$1;->val$fileName_copy:Ljava/lang/String;

    #@6c
    goto :goto_36

    #@6d
    .line 151
    :catchall_6d
    move-exception v4

    #@6e
    if-ne v3, v7, :cond_77

    #@70
    .line 152
    iget-object v5, p0, Landroid/webkit/LGScrapManager$1;->this$0:Landroid/webkit/LGScrapManager;

    #@72
    iget-object v6, p0, Landroid/webkit/LGScrapManager$1;->val$fileName_copy:Ljava/lang/String;

    #@74
    invoke-static {v5, v6}, Landroid/webkit/LGScrapManager;->access$100(Landroid/webkit/LGScrapManager;Ljava/lang/String;)V

    #@77
    .line 151
    :cond_77
    throw v4

    #@78
    .line 146
    .end local v0           #e:Ljava/lang/Exception;
    .end local v1           #out:Ljava/io/OutputStream;
    .restart local v2       #out:Ljava/io/OutputStream;
    :catch_78
    move-exception v0

    #@79
    .line 148
    .restart local v0       #e:Ljava/lang/Exception;
    :try_start_79
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_7c
    .catchall {:try_start_79 .. :try_end_7c} :catchall_84

    #@7c
    .line 149
    const/4 v3, 0x0

    #@7d
    .line 151
    if-ne v3, v7, :cond_24

    #@7f
    .line 152
    iget-object v4, p0, Landroid/webkit/LGScrapManager$1;->this$0:Landroid/webkit/LGScrapManager;

    #@81
    iget-object v5, p0, Landroid/webkit/LGScrapManager$1;->val$fileName_copy:Ljava/lang/String;

    #@83
    goto :goto_21

    #@84
    .line 151
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_84
    move-exception v4

    #@85
    if-ne v3, v7, :cond_8e

    #@87
    .line 152
    iget-object v5, p0, Landroid/webkit/LGScrapManager$1;->this$0:Landroid/webkit/LGScrapManager;

    #@89
    iget-object v6, p0, Landroid/webkit/LGScrapManager$1;->val$fileName_copy:Ljava/lang/String;

    #@8b
    invoke-static {v5, v6}, Landroid/webkit/LGScrapManager;->access$100(Landroid/webkit/LGScrapManager;Ljava/lang/String;)V

    #@8e
    .line 151
    :cond_8e
    throw v4

    #@8f
    .line 143
    :catchall_8f
    move-exception v4

    #@90
    move-object v1, v2

    #@91
    .end local v2           #out:Ljava/io/OutputStream;
    .restart local v1       #out:Ljava/io/OutputStream;
    goto :goto_3b

    #@92
    .line 139
    .end local v1           #out:Ljava/io/OutputStream;
    .restart local v2       #out:Ljava/io/OutputStream;
    :catch_92
    move-exception v0

    #@93
    move-object v1, v2

    #@94
    .end local v2           #out:Ljava/io/OutputStream;
    .restart local v1       #out:Ljava/io/OutputStream;
    goto :goto_27
.end method
