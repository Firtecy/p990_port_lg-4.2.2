.class synthetic Landroid/webkit/WebSocket$2;
.super Ljava/lang/Object;
.source "WebSocket.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/WebSocket;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$javax$net$ssl$SSLEngineResult$HandshakeStatus:[I

.field static final synthetic $SwitchMap$javax$net$ssl$SSLEngineResult$Status:[I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 623
    invoke-static {}, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->values()[Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    #@3
    move-result-object v0

    #@4
    array-length v0, v0

    #@5
    new-array v0, v0, [I

    #@7
    sput-object v0, Landroid/webkit/WebSocket$2;->$SwitchMap$javax$net$ssl$SSLEngineResult$HandshakeStatus:[I

    #@9
    :try_start_9
    sget-object v0, Landroid/webkit/WebSocket$2;->$SwitchMap$javax$net$ssl$SSLEngineResult$HandshakeStatus:[I

    #@b
    sget-object v1, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NOT_HANDSHAKING:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    #@d
    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->ordinal()I

    #@10
    move-result v1

    #@11
    const/4 v2, 0x1

    #@12
    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_14} :catch_86

    #@14
    :goto_14
    :try_start_14
    sget-object v0, Landroid/webkit/WebSocket$2;->$SwitchMap$javax$net$ssl$SSLEngineResult$HandshakeStatus:[I

    #@16
    sget-object v1, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_TASK:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    #@18
    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->ordinal()I

    #@1b
    move-result v1

    #@1c
    const/4 v2, 0x2

    #@1d
    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_1f} :catch_84

    #@1f
    :goto_1f
    :try_start_1f
    sget-object v0, Landroid/webkit/WebSocket$2;->$SwitchMap$javax$net$ssl$SSLEngineResult$HandshakeStatus:[I

    #@21
    sget-object v1, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_WRAP:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    #@23
    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->ordinal()I

    #@26
    move-result v1

    #@27
    const/4 v2, 0x3

    #@28
    aput v2, v0, v1
    :try_end_2a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_2a} :catch_82

    #@2a
    :goto_2a
    :try_start_2a
    sget-object v0, Landroid/webkit/WebSocket$2;->$SwitchMap$javax$net$ssl$SSLEngineResult$HandshakeStatus:[I

    #@2c
    sget-object v1, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_UNWRAP:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    #@2e
    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->ordinal()I

    #@31
    move-result v1

    #@32
    const/4 v2, 0x4

    #@33
    aput v2, v0, v1
    :try_end_35
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2a .. :try_end_35} :catch_80

    #@35
    :goto_35
    :try_start_35
    sget-object v0, Landroid/webkit/WebSocket$2;->$SwitchMap$javax$net$ssl$SSLEngineResult$HandshakeStatus:[I

    #@37
    sget-object v1, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->FINISHED:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    #@39
    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->ordinal()I

    #@3c
    move-result v1

    #@3d
    const/4 v2, 0x5

    #@3e
    aput v2, v0, v1
    :try_end_40
    .catch Ljava/lang/NoSuchFieldError; {:try_start_35 .. :try_end_40} :catch_7e

    #@40
    .line 494
    :goto_40
    invoke-static {}, Ljavax/net/ssl/SSLEngineResult$Status;->values()[Ljavax/net/ssl/SSLEngineResult$Status;

    #@43
    move-result-object v0

    #@44
    array-length v0, v0

    #@45
    new-array v0, v0, [I

    #@47
    sput-object v0, Landroid/webkit/WebSocket$2;->$SwitchMap$javax$net$ssl$SSLEngineResult$Status:[I

    #@49
    :try_start_49
    sget-object v0, Landroid/webkit/WebSocket$2;->$SwitchMap$javax$net$ssl$SSLEngineResult$Status:[I

    #@4b
    sget-object v1, Ljavax/net/ssl/SSLEngineResult$Status;->BUFFER_UNDERFLOW:Ljavax/net/ssl/SSLEngineResult$Status;

    #@4d
    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngineResult$Status;->ordinal()I

    #@50
    move-result v1

    #@51
    const/4 v2, 0x1

    #@52
    aput v2, v0, v1
    :try_end_54
    .catch Ljava/lang/NoSuchFieldError; {:try_start_49 .. :try_end_54} :catch_7c

    #@54
    :goto_54
    :try_start_54
    sget-object v0, Landroid/webkit/WebSocket$2;->$SwitchMap$javax$net$ssl$SSLEngineResult$Status:[I

    #@56
    sget-object v1, Ljavax/net/ssl/SSLEngineResult$Status;->BUFFER_OVERFLOW:Ljavax/net/ssl/SSLEngineResult$Status;

    #@58
    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngineResult$Status;->ordinal()I

    #@5b
    move-result v1

    #@5c
    const/4 v2, 0x2

    #@5d
    aput v2, v0, v1
    :try_end_5f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_54 .. :try_end_5f} :catch_7a

    #@5f
    :goto_5f
    :try_start_5f
    sget-object v0, Landroid/webkit/WebSocket$2;->$SwitchMap$javax$net$ssl$SSLEngineResult$Status:[I

    #@61
    sget-object v1, Ljavax/net/ssl/SSLEngineResult$Status;->CLOSED:Ljavax/net/ssl/SSLEngineResult$Status;

    #@63
    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngineResult$Status;->ordinal()I

    #@66
    move-result v1

    #@67
    const/4 v2, 0x3

    #@68
    aput v2, v0, v1
    :try_end_6a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5f .. :try_end_6a} :catch_78

    #@6a
    :goto_6a
    :try_start_6a
    sget-object v0, Landroid/webkit/WebSocket$2;->$SwitchMap$javax$net$ssl$SSLEngineResult$Status:[I

    #@6c
    sget-object v1, Ljavax/net/ssl/SSLEngineResult$Status;->OK:Ljavax/net/ssl/SSLEngineResult$Status;

    #@6e
    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngineResult$Status;->ordinal()I

    #@71
    move-result v1

    #@72
    const/4 v2, 0x4

    #@73
    aput v2, v0, v1
    :try_end_75
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6a .. :try_end_75} :catch_76

    #@75
    :goto_75
    return-void

    #@76
    :catch_76
    move-exception v0

    #@77
    goto :goto_75

    #@78
    :catch_78
    move-exception v0

    #@79
    goto :goto_6a

    #@7a
    :catch_7a
    move-exception v0

    #@7b
    goto :goto_5f

    #@7c
    :catch_7c
    move-exception v0

    #@7d
    goto :goto_54

    #@7e
    .line 623
    :catch_7e
    move-exception v0

    #@7f
    goto :goto_40

    #@80
    :catch_80
    move-exception v0

    #@81
    goto :goto_35

    #@82
    :catch_82
    move-exception v0

    #@83
    goto :goto_2a

    #@84
    :catch_84
    move-exception v0

    #@85
    goto :goto_1f

    #@86
    :catch_86
    move-exception v0

    #@87
    goto :goto_14
.end method
