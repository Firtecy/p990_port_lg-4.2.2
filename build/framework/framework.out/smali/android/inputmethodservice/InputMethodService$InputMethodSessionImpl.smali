.class public Landroid/inputmethodservice/InputMethodService$InputMethodSessionImpl;
.super Landroid/inputmethodservice/AbstractInputMethodService$AbstractInputMethodSessionImpl;
.source "InputMethodService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/inputmethodservice/InputMethodService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "InputMethodSessionImpl"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/inputmethodservice/InputMethodService;


# direct methods
.method public constructor <init>(Landroid/inputmethodservice/InputMethodService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 454
    iput-object p1, p0, Landroid/inputmethodservice/InputMethodService$InputMethodSessionImpl;->this$0:Landroid/inputmethodservice/InputMethodService;

    #@2
    invoke-direct {p0, p1}, Landroid/inputmethodservice/AbstractInputMethodService$AbstractInputMethodSessionImpl;-><init>(Landroid/inputmethodservice/AbstractInputMethodService;)V

    #@5
    return-void
.end method


# virtual methods
.method public appPrivateCommand(Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 4
    .parameter "action"
    .parameter "data"

    #@0
    .prologue
    .line 533
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService$InputMethodSessionImpl;->isEnabled()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 537
    :goto_6
    return-void

    #@7
    .line 536
    :cond_7
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService$InputMethodSessionImpl;->this$0:Landroid/inputmethodservice/InputMethodService;

    #@9
    invoke-virtual {v0, p1, p2}, Landroid/inputmethodservice/InputMethodService;->onAppPrivateCommand(Ljava/lang/String;Landroid/os/Bundle;)V

    #@c
    goto :goto_6
.end method

.method public displayCompletions([Landroid/view/inputmethod/CompletionInfo;)V
    .registers 3
    .parameter "completions"

    #@0
    .prologue
    .line 468
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService$InputMethodSessionImpl;->isEnabled()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 473
    :goto_6
    return-void

    #@7
    .line 471
    :cond_7
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService$InputMethodSessionImpl;->this$0:Landroid/inputmethodservice/InputMethodService;

    #@9
    iput-object p1, v0, Landroid/inputmethodservice/InputMethodService;->mCurCompletions:[Landroid/view/inputmethod/CompletionInfo;

    #@b
    .line 472
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService$InputMethodSessionImpl;->this$0:Landroid/inputmethodservice/InputMethodService;

    #@d
    invoke-virtual {v0, p1}, Landroid/inputmethodservice/InputMethodService;->onDisplayCompletions([Landroid/view/inputmethod/CompletionInfo;)V

    #@10
    goto :goto_6
.end method

.method public finishInput()V
    .registers 2

    #@0
    .prologue
    .line 456
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService$InputMethodSessionImpl;->isEnabled()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 461
    :goto_6
    return-void

    #@7
    .line 460
    :cond_7
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService$InputMethodSessionImpl;->this$0:Landroid/inputmethodservice/InputMethodService;

    #@9
    invoke-virtual {v0}, Landroid/inputmethodservice/InputMethodService;->doFinishInput()V

    #@c
    goto :goto_6
.end method

.method public toggleSoftInput(II)V
    .registers 4
    .parameter "showFlags"
    .parameter "hideFlags"

    #@0
    .prologue
    .line 543
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService$InputMethodSessionImpl;->this$0:Landroid/inputmethodservice/InputMethodService;

    #@2
    invoke-static {v0, p1, p2}, Landroid/inputmethodservice/InputMethodService;->access$100(Landroid/inputmethodservice/InputMethodService;II)V

    #@5
    .line 544
    return-void
.end method

.method public updateCursor(Landroid/graphics/Rect;)V
    .registers 3
    .parameter "newCursor"

    #@0
    .prologue
    .line 522
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService$InputMethodSessionImpl;->isEnabled()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 526
    :goto_6
    return-void

    #@7
    .line 525
    :cond_7
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService$InputMethodSessionImpl;->this$0:Landroid/inputmethodservice/InputMethodService;

    #@9
    invoke-virtual {v0, p1}, Landroid/inputmethodservice/InputMethodService;->onUpdateCursor(Landroid/graphics/Rect;)V

    #@c
    goto :goto_6
.end method

.method public updateExtractedText(ILandroid/view/inputmethod/ExtractedText;)V
    .registers 4
    .parameter "token"
    .parameter "text"

    #@0
    .prologue
    .line 480
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService$InputMethodSessionImpl;->isEnabled()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 484
    :goto_6
    return-void

    #@7
    .line 483
    :cond_7
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService$InputMethodSessionImpl;->this$0:Landroid/inputmethodservice/InputMethodService;

    #@9
    invoke-virtual {v0, p1, p2}, Landroid/inputmethodservice/InputMethodService;->onUpdateExtractedText(ILandroid/view/inputmethod/ExtractedText;)V

    #@c
    goto :goto_6
.end method

.method public updateSelection(IIIIII)V
    .registers 15
    .parameter "oldSelStart"
    .parameter "oldSelEnd"
    .parameter "newSelStart"
    .parameter "newSelEnd"
    .parameter "candidatesStart"
    .parameter "candidatesEnd"

    #@0
    .prologue
    .line 493
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService$InputMethodSessionImpl;->isEnabled()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 507
    :cond_6
    :goto_6
    return-void

    #@7
    .line 496
    :cond_7
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService$InputMethodSessionImpl;->this$0:Landroid/inputmethodservice/InputMethodService;

    #@9
    move v1, p1

    #@a
    move v2, p2

    #@b
    move v3, p3

    #@c
    move v4, p4

    #@d
    move v5, p5

    #@e
    move v6, p6

    #@f
    invoke-virtual/range {v0 .. v6}, Landroid/inputmethodservice/InputMethodService;->onUpdateSelection(IIIIII)V

    #@12
    .line 499
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@14
    if-eqz v0, :cond_6

    #@16
    .line 500
    if-ne p3, p4, :cond_1d

    #@18
    const/4 v0, 0x0

    #@19
    invoke-static {v0}, Landroid/widget/BubblePopupHelper;->setShowingAnyBubblePopup(Z)V

    #@1c
    goto :goto_6

    #@1d
    .line 501
    :cond_1d
    invoke-static {}, Landroid/widget/BubblePopupHelper;->isShowingAnyBubblePopup()Z

    #@20
    move-result v0

    #@21
    if-eqz v0, :cond_6

    #@23
    .line 502
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService$InputMethodSessionImpl;->this$0:Landroid/inputmethodservice/InputMethodService;

    #@25
    invoke-virtual {v0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    #@28
    move-result-object v7

    #@29
    .line 503
    .local v7, ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v7, :cond_6

    #@2b
    .line 504
    const-string v0, "ACTION_SHOWING_BUBBLE_POPUP"

    #@2d
    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    #@2f
    invoke-interface {v7, v0, v1}, Landroid/view/inputmethod/InputConnection;->performPrivateCommand(Ljava/lang/String;Landroid/os/Bundle;)Z

    #@32
    goto :goto_6
.end method

.method public viewClicked(Z)V
    .registers 3
    .parameter "focusChanged"

    #@0
    .prologue
    .line 511
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService$InputMethodSessionImpl;->isEnabled()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 515
    :goto_6
    return-void

    #@7
    .line 514
    :cond_7
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService$InputMethodSessionImpl;->this$0:Landroid/inputmethodservice/InputMethodService;

    #@9
    invoke-virtual {v0, p1}, Landroid/inputmethodservice/InputMethodService;->onViewClicked(Z)V

    #@c
    goto :goto_6
.end method
