.class Landroid/inputmethodservice/IInputMethodSessionWrapper$InputMethodEventCallbackWrapper;
.super Ljava/lang/Object;
.source "IInputMethodSessionWrapper.java"

# interfaces
.implements Landroid/view/inputmethod/InputMethodSession$EventCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/inputmethodservice/IInputMethodSessionWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "InputMethodEventCallbackWrapper"
.end annotation


# instance fields
.field final mCb:Lcom/android/internal/view/IInputMethodCallback;


# direct methods
.method constructor <init>(Lcom/android/internal/view/IInputMethodCallback;)V
    .registers 2
    .parameter "cb"

    #@0
    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 61
    iput-object p1, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper$InputMethodEventCallbackWrapper;->mCb:Lcom/android/internal/view/IInputMethodCallback;

    #@5
    .line 62
    return-void
.end method


# virtual methods
.method public finishedEvent(IZ)V
    .registers 4
    .parameter "seq"
    .parameter "handled"

    #@0
    .prologue
    .line 65
    :try_start_0
    iget-object v0, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper$InputMethodEventCallbackWrapper;->mCb:Lcom/android/internal/view/IInputMethodCallback;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/view/IInputMethodCallback;->finishedEvent(IZ)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 68
    :goto_5
    return-void

    #@6
    .line 66
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method
