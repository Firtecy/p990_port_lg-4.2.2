.class public Landroid/inputmethodservice/InputMethodService;
.super Landroid/inputmethodservice/AbstractInputMethodService;
.source "InputMethodService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/inputmethodservice/InputMethodService$Insets;,
        Landroid/inputmethodservice/InputMethodService$InputMethodSessionImpl;,
        Landroid/inputmethodservice/InputMethodService$InputMethodImpl;
    }
.end annotation


# static fields
.field public static final BACK_DISPOSITION_DEFAULT:I = 0x0

.field public static final BACK_DISPOSITION_WILL_DISMISS:I = 0x2

.field public static final BACK_DISPOSITION_WILL_NOT_DISMISS:I = 0x1

.field static final DEBUG:Z = false

.field public static final IME_ACTIVE:I = 0x1

.field public static final IME_VISIBLE:I = 0x2

.field static final MOVEMENT_DOWN:I = -0x1

.field static final MOVEMENT_UP:I = -0x2

.field static final TAG:Ljava/lang/String; = "InputMethodService"


# instance fields
.field final mActionClickListener:Landroid/view/View$OnClickListener;

.field mBackDisposition:I

.field mCandidatesFrame:Landroid/widget/FrameLayout;

.field mCandidatesViewStarted:Z

.field mCandidatesVisibility:I

.field mCurCompletions:[Landroid/view/inputmethod/CompletionInfo;

.field mExtractAccessories:Landroid/view/ViewGroup;

.field mExtractAction:Landroid/widget/Button;

.field mExtractEditText:Landroid/inputmethodservice/ExtractEditText;

.field mExtractFrame:Landroid/widget/FrameLayout;

.field mExtractView:Landroid/view/View;

.field mExtractViewHidden:Z

.field mExtractedText:Landroid/view/inputmethod/ExtractedText;

.field mExtractedToken:I

.field mFullscreenApplied:Z

.field mFullscreenArea:Landroid/view/ViewGroup;

.field mHardwareAccelerated:Z

.field mImm:Landroid/view/inputmethod/InputMethodManager;

.field mInShowWindow:Z

.field mInflater:Landroid/view/LayoutInflater;

.field mInitialized:Z

.field mInputBinding:Landroid/view/inputmethod/InputBinding;

.field mInputConnection:Landroid/view/inputmethod/InputConnection;

.field mInputEditorInfo:Landroid/view/inputmethod/EditorInfo;

.field mInputFrame:Landroid/widget/FrameLayout;

.field mInputStarted:Z

.field mInputView:Landroid/view/View;

.field mInputViewStarted:Z

.field final mInsetsComputer:Landroid/view/ViewTreeObserver$OnComputeInternalInsetsListener;

.field mIsFullscreen:Z

.field mIsInputViewShown:Z

.field mLastShowInputRequested:Z

.field mRootView:Landroid/view/View;

.field mShowInputFlags:I

.field mShowInputForced:Z

.field mShowInputRequested:Z

.field mStartedInputConnection:Landroid/view/inputmethod/InputConnection;

.field mStatusIcon:I

.field mTheme:I

.field mThemeAttrs:Landroid/content/res/TypedArray;

.field final mTmpInsets:Landroid/inputmethodservice/InputMethodService$Insets;

.field final mTmpLocation:[I

.field mToken:Landroid/os/IBinder;

.field mWindow:Landroid/inputmethodservice/SoftInputWindow;

.field mWindowAdded:Z

.field mWindowCreated:Z

.field mWindowVisible:Z

.field mWindowWasVisible:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 220
    invoke-direct {p0}, Landroid/inputmethodservice/AbstractInputMethodService;-><init>()V

    #@4
    .line 253
    iput v0, p0, Landroid/inputmethodservice/InputMethodService;->mTheme:I

    #@6
    .line 254
    iput-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mHardwareAccelerated:Z

    #@8
    .line 305
    new-instance v0, Landroid/inputmethodservice/InputMethodService$Insets;

    #@a
    invoke-direct {v0}, Landroid/inputmethodservice/InputMethodService$Insets;-><init>()V

    #@d
    iput-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mTmpInsets:Landroid/inputmethodservice/InputMethodService$Insets;

    #@f
    .line 306
    const/4 v0, 0x2

    #@10
    new-array v0, v0, [I

    #@12
    iput-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mTmpLocation:[I

    #@14
    .line 308
    new-instance v0, Landroid/inputmethodservice/InputMethodService$1;

    #@16
    invoke-direct {v0, p0}, Landroid/inputmethodservice/InputMethodService$1;-><init>(Landroid/inputmethodservice/InputMethodService;)V

    #@19
    iput-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mInsetsComputer:Landroid/view/ViewTreeObserver$OnComputeInternalInsetsListener;

    #@1b
    .line 329
    new-instance v0, Landroid/inputmethodservice/InputMethodService$2;

    #@1d
    invoke-direct {v0, p0}, Landroid/inputmethodservice/InputMethodService$2;-><init>(Landroid/inputmethodservice/InputMethodService;)V

    #@20
    iput-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mActionClickListener:Landroid/view/View$OnClickListener;

    #@22
    .line 550
    return-void
.end method

.method static synthetic access$000(Landroid/inputmethodservice/InputMethodService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 220
    invoke-direct {p0}, Landroid/inputmethodservice/InputMethodService;->doHideWindow()V

    #@3
    return-void
.end method

.method static synthetic access$100(Landroid/inputmethodservice/InputMethodService;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 220
    invoke-direct {p0, p1, p2}, Landroid/inputmethodservice/InputMethodService;->onToggleSoftInput(II)V

    #@3
    return-void
.end method

.method private doHideWindow()V
    .registers 5

    #@0
    .prologue
    .line 1496
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mImm:Landroid/view/inputmethod/InputMethodManager;

    #@2
    iget-object v1, p0, Landroid/inputmethodservice/InputMethodService;->mToken:Landroid/os/IBinder;

    #@4
    const/4 v2, 0x0

    #@5
    iget v3, p0, Landroid/inputmethodservice/InputMethodService;->mBackDisposition:I

    #@7
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->setImeWindowStatus(Landroid/os/IBinder;II)V

    #@a
    .line 1498
    new-instance v0, Landroid/content/Intent;

    #@c
    const-string v1, "com.lge.intent.action.SOFT_INPUT_HIDDEN"

    #@e
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@11
    invoke-virtual {p0, v0}, Landroid/inputmethodservice/InputMethodService;->sendBroadcast(Landroid/content/Intent;)V

    #@14
    .line 1499
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->hideWindow()V

    #@17
    .line 1500
    return-void
.end method

.method private finishViews()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1484
    iget-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mInputViewStarted:Z

    #@3
    if-eqz v0, :cond_d

    #@5
    .line 1486
    invoke-virtual {p0, v1}, Landroid/inputmethodservice/InputMethodService;->onFinishInputView(Z)V

    #@8
    .line 1491
    :cond_8
    :goto_8
    iput-boolean v1, p0, Landroid/inputmethodservice/InputMethodService;->mInputViewStarted:Z

    #@a
    .line 1492
    iput-boolean v1, p0, Landroid/inputmethodservice/InputMethodService;->mCandidatesViewStarted:Z

    #@c
    .line 1493
    return-void

    #@d
    .line 1487
    :cond_d
    iget-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mCandidatesViewStarted:Z

    #@f
    if-eqz v0, :cond_8

    #@11
    .line 1489
    invoke-virtual {p0, v1}, Landroid/inputmethodservice/InputMethodService;->onFinishCandidatesView(Z)V

    #@14
    goto :goto_8
.end method

.method private handleBack(Z)Z
    .registers 6
    .parameter "doIt"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1731
    iget-boolean v3, p0, Landroid/inputmethodservice/InputMethodService;->mShowInputRequested:Z

    #@4
    if-eqz v3, :cond_28

    #@6
    .line 1732
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->isExtractViewShown()Z

    #@9
    move-result v3

    #@a
    if-eqz v3, :cond_22

    #@c
    iget-object v3, p0, Landroid/inputmethodservice/InputMethodService;->mExtractView:Landroid/view/View;

    #@e
    instance-of v3, v3, Landroid/inputmethodservice/ExtractEditLayout;

    #@10
    if-eqz v3, :cond_22

    #@12
    .line 1733
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mExtractView:Landroid/view/View;

    #@14
    check-cast v0, Landroid/inputmethodservice/ExtractEditLayout;

    #@16
    .line 1734
    .local v0, extractEditLayout:Landroid/inputmethodservice/ExtractEditLayout;
    invoke-virtual {v0}, Landroid/inputmethodservice/ExtractEditLayout;->isActionModeStarted()Z

    #@19
    move-result v3

    #@1a
    if-eqz v3, :cond_22

    #@1c
    .line 1735
    if-eqz p1, :cond_21

    #@1e
    invoke-virtual {v0}, Landroid/inputmethodservice/ExtractEditLayout;->finishActionMode()V

    #@21
    .line 1756
    .end local v0           #extractEditLayout:Landroid/inputmethodservice/ExtractEditLayout;
    :cond_21
    :goto_21
    return v1

    #@22
    .line 1741
    :cond_22
    if-eqz p1, :cond_21

    #@24
    invoke-virtual {p0, v2}, Landroid/inputmethodservice/InputMethodService;->requestHideSelf(I)V

    #@27
    goto :goto_21

    #@28
    .line 1743
    :cond_28
    iget-boolean v3, p0, Landroid/inputmethodservice/InputMethodService;->mWindowVisible:Z

    #@2a
    if-eqz v3, :cond_3c

    #@2c
    .line 1744
    iget v3, p0, Landroid/inputmethodservice/InputMethodService;->mCandidatesVisibility:I

    #@2e
    if-nez v3, :cond_36

    #@30
    .line 1747
    if-eqz p1, :cond_21

    #@32
    invoke-virtual {p0, v2}, Landroid/inputmethodservice/InputMethodService;->setCandidatesViewShown(Z)V

    #@35
    goto :goto_21

    #@36
    .line 1752
    :cond_36
    if-eqz p1, :cond_21

    #@38
    invoke-direct {p0}, Landroid/inputmethodservice/InputMethodService;->doHideWindow()V

    #@3b
    goto :goto_21

    #@3c
    :cond_3c
    move v1, v2

    #@3d
    .line 1756
    goto :goto_21
.end method

.method private onToggleSoftInput(II)V
    .registers 4
    .parameter "showFlags"
    .parameter "hideFlags"

    #@0
    .prologue
    .line 1890
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->isInputViewShown()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_a

    #@6
    .line 1891
    invoke-virtual {p0, p2}, Landroid/inputmethodservice/InputMethodService;->requestHideSelf(I)V

    #@9
    .line 1895
    :goto_9
    return-void

    #@a
    .line 1893
    :cond_a
    invoke-direct {p0, p1}, Landroid/inputmethodservice/InputMethodService;->requestShowSelf(I)V

    #@d
    goto :goto_9
.end method

.method private requestShowSelf(I)V
    .registers 4
    .parameter "flags"

    #@0
    .prologue
    .line 1727
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mImm:Landroid/view/inputmethod/InputMethodManager;

    #@2
    iget-object v1, p0, Landroid/inputmethodservice/InputMethodService;->mToken:Landroid/os/IBinder;

    #@4
    invoke-virtual {v0, v1, p1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInputFromInputMethod(Landroid/os/IBinder;I)V

    #@7
    .line 1728
    return-void
.end method


# virtual methods
.method doFinishInput()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    const/4 v1, 0x0

    #@3
    .line 1567
    iget-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mInputViewStarted:Z

    #@5
    if-eqz v0, :cond_1c

    #@7
    .line 1569
    invoke-virtual {p0, v2}, Landroid/inputmethodservice/InputMethodService;->onFinishInputView(Z)V

    #@a
    .line 1574
    :cond_a
    :goto_a
    iput-boolean v1, p0, Landroid/inputmethodservice/InputMethodService;->mInputViewStarted:Z

    #@c
    .line 1575
    iput-boolean v1, p0, Landroid/inputmethodservice/InputMethodService;->mCandidatesViewStarted:Z

    #@e
    .line 1576
    iget-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mInputStarted:Z

    #@10
    if-eqz v0, :cond_15

    #@12
    .line 1578
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->onFinishInput()V

    #@15
    .line 1580
    :cond_15
    iput-boolean v1, p0, Landroid/inputmethodservice/InputMethodService;->mInputStarted:Z

    #@17
    .line 1581
    iput-object v3, p0, Landroid/inputmethodservice/InputMethodService;->mStartedInputConnection:Landroid/view/inputmethod/InputConnection;

    #@19
    .line 1582
    iput-object v3, p0, Landroid/inputmethodservice/InputMethodService;->mCurCompletions:[Landroid/view/inputmethod/CompletionInfo;

    #@1b
    .line 1583
    return-void

    #@1c
    .line 1570
    :cond_1c
    iget-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mCandidatesViewStarted:Z

    #@1e
    if-eqz v0, :cond_a

    #@20
    .line 1572
    invoke-virtual {p0, v2}, Landroid/inputmethodservice/InputMethodService;->onFinishCandidatesView(Z)V

    #@23
    goto :goto_a
.end method

.method doMovementKey(ILandroid/view/KeyEvent;I)Z
    .registers 12
    .parameter "keyCode"
    .parameter "event"
    .parameter "count"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 1920
    iget-object v1, p0, Landroid/inputmethodservice/InputMethodService;->mExtractEditText:Landroid/inputmethodservice/ExtractEditText;

    #@4
    .line 1921
    .local v1, eet:Landroid/inputmethodservice/ExtractEditText;
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->isExtractViewShown()Z

    #@7
    move-result v7

    #@8
    if-eqz v7, :cond_3f

    #@a
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->isInputViewShown()Z

    #@d
    move-result v7

    #@e
    if-eqz v7, :cond_3f

    #@10
    if-eqz v1, :cond_3f

    #@12
    .line 1925
    invoke-virtual {v1}, Landroid/inputmethodservice/ExtractEditText;->getMovementMethod()Landroid/text/method/MovementMethod;

    #@15
    move-result-object v3

    #@16
    .line 1926
    .local v3, movement:Landroid/text/method/MovementMethod;
    invoke-virtual {v1}, Landroid/inputmethodservice/ExtractEditText;->getLayout()Landroid/text/Layout;

    #@19
    move-result-object v2

    #@1a
    .line 1927
    .local v2, layout:Landroid/text/Layout;
    if-eqz v3, :cond_3c

    #@1c
    if-eqz v2, :cond_3c

    #@1e
    .line 1930
    const/4 v7, -0x1

    #@1f
    if-ne p3, v7, :cond_2f

    #@21
    .line 1931
    invoke-virtual {v1}, Landroid/inputmethodservice/ExtractEditText;->getText()Landroid/text/Editable;

    #@24
    move-result-object v7

    #@25
    invoke-interface {v3, v1, v7, p1, p2}, Landroid/text/method/MovementMethod;->onKeyDown(Landroid/widget/TextView;Landroid/text/Spannable;ILandroid/view/KeyEvent;)Z

    #@28
    move-result v7

    #@29
    if-eqz v7, :cond_3c

    #@2b
    .line 1933
    invoke-virtual {p0, p1, v5}, Landroid/inputmethodservice/InputMethodService;->reportExtractedMovement(II)V

    #@2e
    .line 1973
    .end local v2           #layout:Landroid/text/Layout;
    .end local v3           #movement:Landroid/text/method/MovementMethod;
    :cond_2e
    :goto_2e
    :pswitch_2e
    return v5

    #@2f
    .line 1936
    .restart local v2       #layout:Landroid/text/Layout;
    .restart local v3       #movement:Landroid/text/method/MovementMethod;
    :cond_2f
    const/4 v7, -0x2

    #@30
    if-ne p3, v7, :cond_41

    #@32
    .line 1937
    invoke-virtual {v1}, Landroid/inputmethodservice/ExtractEditText;->getText()Landroid/text/Editable;

    #@35
    move-result-object v7

    #@36
    invoke-interface {v3, v1, v7, p1, p2}, Landroid/text/method/MovementMethod;->onKeyUp(Landroid/widget/TextView;Landroid/text/Spannable;ILandroid/view/KeyEvent;)Z

    #@39
    move-result v7

    #@3a
    if-nez v7, :cond_2e

    #@3c
    .line 1964
    :cond_3c
    :goto_3c
    packed-switch p1, :pswitch_data_80

    #@3f
    .end local v2           #layout:Landroid/text/Layout;
    .end local v3           #movement:Landroid/text/method/MovementMethod;
    :cond_3f
    move v5, v6

    #@40
    .line 1973
    goto :goto_2e

    #@41
    .line 1942
    .restart local v2       #layout:Landroid/text/Layout;
    .restart local v3       #movement:Landroid/text/method/MovementMethod;
    :cond_41
    invoke-virtual {v1}, Landroid/inputmethodservice/ExtractEditText;->getText()Landroid/text/Editable;

    #@44
    move-result-object v7

    #@45
    invoke-interface {v3, v1, v7, p2}, Landroid/text/method/MovementMethod;->onKeyOther(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/KeyEvent;)Z

    #@48
    move-result v7

    #@49
    if-eqz v7, :cond_4f

    #@4b
    .line 1943
    invoke-virtual {p0, p1, p3}, Landroid/inputmethodservice/InputMethodService;->reportExtractedMovement(II)V

    #@4e
    goto :goto_3c

    #@4f
    .line 1945
    :cond_4f
    invoke-static {p2, v6}, Landroid/view/KeyEvent;->changeAction(Landroid/view/KeyEvent;I)Landroid/view/KeyEvent;

    #@52
    move-result-object v0

    #@53
    .line 1946
    .local v0, down:Landroid/view/KeyEvent;
    invoke-virtual {v1}, Landroid/inputmethodservice/ExtractEditText;->getText()Landroid/text/Editable;

    #@56
    move-result-object v7

    #@57
    invoke-interface {v3, v1, v7, p1, v0}, Landroid/text/method/MovementMethod;->onKeyDown(Landroid/widget/TextView;Landroid/text/Spannable;ILandroid/view/KeyEvent;)Z

    #@5a
    move-result v7

    #@5b
    if-eqz v7, :cond_3c

    #@5d
    .line 1948
    invoke-static {p2, v5}, Landroid/view/KeyEvent;->changeAction(Landroid/view/KeyEvent;I)Landroid/view/KeyEvent;

    #@60
    move-result-object v4

    #@61
    .line 1949
    .local v4, up:Landroid/view/KeyEvent;
    invoke-virtual {v1}, Landroid/inputmethodservice/ExtractEditText;->getText()Landroid/text/Editable;

    #@64
    move-result-object v7

    #@65
    invoke-interface {v3, v1, v7, p1, v4}, Landroid/text/method/MovementMethod;->onKeyUp(Landroid/widget/TextView;Landroid/text/Spannable;ILandroid/view/KeyEvent;)Z

    #@68
    .line 1951
    :goto_68
    add-int/lit8 p3, p3, -0x1

    #@6a
    if-lez p3, :cond_7b

    #@6c
    .line 1952
    invoke-virtual {v1}, Landroid/inputmethodservice/ExtractEditText;->getText()Landroid/text/Editable;

    #@6f
    move-result-object v7

    #@70
    invoke-interface {v3, v1, v7, p1, v0}, Landroid/text/method/MovementMethod;->onKeyDown(Landroid/widget/TextView;Landroid/text/Spannable;ILandroid/view/KeyEvent;)Z

    #@73
    .line 1954
    invoke-virtual {v1}, Landroid/inputmethodservice/ExtractEditText;->getText()Landroid/text/Editable;

    #@76
    move-result-object v7

    #@77
    invoke-interface {v3, v1, v7, p1, v4}, Landroid/text/method/MovementMethod;->onKeyUp(Landroid/widget/TextView;Landroid/text/Spannable;ILandroid/view/KeyEvent;)Z

    #@7a
    goto :goto_68

    #@7b
    .line 1957
    :cond_7b
    invoke-virtual {p0, p1, p3}, Landroid/inputmethodservice/InputMethodService;->reportExtractedMovement(II)V

    #@7e
    goto :goto_3c

    #@7f
    .line 1964
    nop

    #@80
    :pswitch_data_80
    .packed-switch 0x13
        :pswitch_2e
        :pswitch_2e
        :pswitch_2e
        :pswitch_2e
    .end packed-switch
.end method

.method doStartInput(Landroid/view/inputmethod/InputConnection;Landroid/view/inputmethod/EditorInfo;Z)V
    .registers 6
    .parameter "ic"
    .parameter "attribute"
    .parameter "restarting"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1586
    if-nez p3, :cond_6

    #@3
    .line 1587
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->doFinishInput()V

    #@6
    .line 1589
    :cond_6
    iput-boolean v1, p0, Landroid/inputmethodservice/InputMethodService;->mInputStarted:Z

    #@8
    .line 1590
    iput-object p1, p0, Landroid/inputmethodservice/InputMethodService;->mStartedInputConnection:Landroid/view/inputmethod/InputConnection;

    #@a
    .line 1591
    iput-object p2, p0, Landroid/inputmethodservice/InputMethodService;->mInputEditorInfo:Landroid/view/inputmethod/EditorInfo;

    #@c
    .line 1592
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->initialize()V

    #@f
    .line 1594
    invoke-virtual {p0, p2, p3}, Landroid/inputmethodservice/InputMethodService;->onStartInput(Landroid/view/inputmethod/EditorInfo;Z)V

    #@12
    .line 1595
    iget-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mWindowVisible:Z

    #@14
    if-eqz v0, :cond_24

    #@16
    .line 1596
    iget-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mShowInputRequested:Z

    #@18
    if-eqz v0, :cond_25

    #@1a
    .line 1598
    iput-boolean v1, p0, Landroid/inputmethodservice/InputMethodService;->mInputViewStarted:Z

    #@1c
    .line 1599
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mInputEditorInfo:Landroid/view/inputmethod/EditorInfo;

    #@1e
    invoke-virtual {p0, v0, p3}, Landroid/inputmethodservice/InputMethodService;->onStartInputView(Landroid/view/inputmethod/EditorInfo;Z)V

    #@21
    .line 1600
    invoke-virtual {p0, v1}, Landroid/inputmethodservice/InputMethodService;->startExtractingText(Z)V

    #@24
    .line 1607
    :cond_24
    :goto_24
    return-void

    #@25
    .line 1601
    :cond_25
    iget v0, p0, Landroid/inputmethodservice/InputMethodService;->mCandidatesVisibility:I

    #@27
    if-nez v0, :cond_24

    #@29
    .line 1603
    iput-boolean v1, p0, Landroid/inputmethodservice/InputMethodService;->mCandidatesViewStarted:Z

    #@2b
    .line 1604
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mInputEditorInfo:Landroid/view/inputmethod/EditorInfo;

    #@2d
    invoke-virtual {p0, v0, p3}, Landroid/inputmethodservice/InputMethodService;->onStartCandidatesView(Landroid/view/inputmethod/EditorInfo;Z)V

    #@30
    goto :goto_24
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 7
    .parameter "fd"
    .parameter "fout"
    .parameter "args"

    #@0
    .prologue
    .line 2379
    new-instance v0, Landroid/util/PrintWriterPrinter;

    #@2
    invoke-direct {v0, p2}, Landroid/util/PrintWriterPrinter;-><init>(Ljava/io/PrintWriter;)V

    #@5
    .line 2380
    .local v0, p:Landroid/util/Printer;
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "Input method service state for "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    const-string v2, ":"

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@21
    .line 2381
    new-instance v1, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v2, "  mWindowCreated="

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    iget-boolean v2, p0, Landroid/inputmethodservice/InputMethodService;->mWindowCreated:Z

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    const-string v2, " mWindowAdded="

    #@34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    iget-boolean v2, p0, Landroid/inputmethodservice/InputMethodService;->mWindowAdded:Z

    #@3a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v1

    #@3e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v1

    #@42
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@45
    .line 2383
    new-instance v1, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    const-string v2, "  mWindowVisible="

    #@4c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v1

    #@50
    iget-boolean v2, p0, Landroid/inputmethodservice/InputMethodService;->mWindowVisible:Z

    #@52
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@55
    move-result-object v1

    #@56
    const-string v2, " mWindowWasVisible="

    #@58
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v1

    #@5c
    iget-boolean v2, p0, Landroid/inputmethodservice/InputMethodService;->mWindowWasVisible:Z

    #@5e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@61
    move-result-object v1

    #@62
    const-string v2, " mInShowWindow="

    #@64
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v1

    #@68
    iget-boolean v2, p0, Landroid/inputmethodservice/InputMethodService;->mInShowWindow:Z

    #@6a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v1

    #@6e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v1

    #@72
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@75
    .line 2386
    new-instance v1, Ljava/lang/StringBuilder;

    #@77
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7a
    const-string v2, "  Configuration="

    #@7c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v1

    #@80
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getResources()Landroid/content/res/Resources;

    #@83
    move-result-object v2

    #@84
    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@87
    move-result-object v2

    #@88
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v1

    #@8c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8f
    move-result-object v1

    #@90
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@93
    .line 2387
    new-instance v1, Ljava/lang/StringBuilder;

    #@95
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@98
    const-string v2, "  mToken="

    #@9a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v1

    #@9e
    iget-object v2, p0, Landroid/inputmethodservice/InputMethodService;->mToken:Landroid/os/IBinder;

    #@a0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v1

    #@a4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a7
    move-result-object v1

    #@a8
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@ab
    .line 2388
    new-instance v1, Ljava/lang/StringBuilder;

    #@ad
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b0
    const-string v2, "  mInputBinding="

    #@b2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v1

    #@b6
    iget-object v2, p0, Landroid/inputmethodservice/InputMethodService;->mInputBinding:Landroid/view/inputmethod/InputBinding;

    #@b8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v1

    #@bc
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bf
    move-result-object v1

    #@c0
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@c3
    .line 2389
    new-instance v1, Ljava/lang/StringBuilder;

    #@c5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c8
    const-string v2, "  mInputConnection="

    #@ca
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v1

    #@ce
    iget-object v2, p0, Landroid/inputmethodservice/InputMethodService;->mInputConnection:Landroid/view/inputmethod/InputConnection;

    #@d0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v1

    #@d4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d7
    move-result-object v1

    #@d8
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@db
    .line 2390
    new-instance v1, Ljava/lang/StringBuilder;

    #@dd
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e0
    const-string v2, "  mStartedInputConnection="

    #@e2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v1

    #@e6
    iget-object v2, p0, Landroid/inputmethodservice/InputMethodService;->mStartedInputConnection:Landroid/view/inputmethod/InputConnection;

    #@e8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v1

    #@ec
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ef
    move-result-object v1

    #@f0
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@f3
    .line 2391
    new-instance v1, Ljava/lang/StringBuilder;

    #@f5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f8
    const-string v2, "  mInputStarted="

    #@fa
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fd
    move-result-object v1

    #@fe
    iget-boolean v2, p0, Landroid/inputmethodservice/InputMethodService;->mInputStarted:Z

    #@100
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@103
    move-result-object v1

    #@104
    const-string v2, " mInputViewStarted="

    #@106
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@109
    move-result-object v1

    #@10a
    iget-boolean v2, p0, Landroid/inputmethodservice/InputMethodService;->mInputViewStarted:Z

    #@10c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10f
    move-result-object v1

    #@110
    const-string v2, " mCandidatesViewStarted="

    #@112
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@115
    move-result-object v1

    #@116
    iget-boolean v2, p0, Landroid/inputmethodservice/InputMethodService;->mCandidatesViewStarted:Z

    #@118
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v1

    #@11c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11f
    move-result-object v1

    #@120
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@123
    .line 2395
    iget-object v1, p0, Landroid/inputmethodservice/InputMethodService;->mInputEditorInfo:Landroid/view/inputmethod/EditorInfo;

    #@125
    if-eqz v1, :cond_2aa

    #@127
    .line 2396
    const-string v1, "  mInputEditorInfo:"

    #@129
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@12c
    .line 2397
    iget-object v1, p0, Landroid/inputmethodservice/InputMethodService;->mInputEditorInfo:Landroid/view/inputmethod/EditorInfo;

    #@12e
    const-string v2, "    "

    #@130
    invoke-virtual {v1, v0, v2}, Landroid/view/inputmethod/EditorInfo;->dump(Landroid/util/Printer;Ljava/lang/String;)V

    #@133
    .line 2402
    :goto_133
    new-instance v1, Ljava/lang/StringBuilder;

    #@135
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@138
    const-string v2, "  mShowInputRequested="

    #@13a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v1

    #@13e
    iget-boolean v2, p0, Landroid/inputmethodservice/InputMethodService;->mShowInputRequested:Z

    #@140
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@143
    move-result-object v1

    #@144
    const-string v2, " mLastShowInputRequested="

    #@146
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@149
    move-result-object v1

    #@14a
    iget-boolean v2, p0, Landroid/inputmethodservice/InputMethodService;->mLastShowInputRequested:Z

    #@14c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@14f
    move-result-object v1

    #@150
    const-string v2, " mShowInputForced="

    #@152
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@155
    move-result-object v1

    #@156
    iget-boolean v2, p0, Landroid/inputmethodservice/InputMethodService;->mShowInputForced:Z

    #@158
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@15b
    move-result-object v1

    #@15c
    const-string v2, " mShowInputFlags=0x"

    #@15e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@161
    move-result-object v1

    #@162
    iget v2, p0, Landroid/inputmethodservice/InputMethodService;->mShowInputFlags:I

    #@164
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@167
    move-result-object v2

    #@168
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16b
    move-result-object v1

    #@16c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16f
    move-result-object v1

    #@170
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@173
    .line 2406
    new-instance v1, Ljava/lang/StringBuilder;

    #@175
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@178
    const-string v2, "  mCandidatesVisibility="

    #@17a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17d
    move-result-object v1

    #@17e
    iget v2, p0, Landroid/inputmethodservice/InputMethodService;->mCandidatesVisibility:I

    #@180
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@183
    move-result-object v1

    #@184
    const-string v2, " mFullscreenApplied="

    #@186
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@189
    move-result-object v1

    #@18a
    iget-boolean v2, p0, Landroid/inputmethodservice/InputMethodService;->mFullscreenApplied:Z

    #@18c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@18f
    move-result-object v1

    #@190
    const-string v2, " mIsFullscreen="

    #@192
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@195
    move-result-object v1

    #@196
    iget-boolean v2, p0, Landroid/inputmethodservice/InputMethodService;->mIsFullscreen:Z

    #@198
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@19b
    move-result-object v1

    #@19c
    const-string v2, " mExtractViewHidden="

    #@19e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a1
    move-result-object v1

    #@1a2
    iget-boolean v2, p0, Landroid/inputmethodservice/InputMethodService;->mExtractViewHidden:Z

    #@1a4
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1a7
    move-result-object v1

    #@1a8
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ab
    move-result-object v1

    #@1ac
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@1af
    .line 2411
    iget-object v1, p0, Landroid/inputmethodservice/InputMethodService;->mExtractedText:Landroid/view/inputmethod/ExtractedText;

    #@1b1
    if-eqz v1, :cond_2b1

    #@1b3
    .line 2412
    const-string v1, "  mExtractedText:"

    #@1b5
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@1b8
    .line 2413
    new-instance v1, Ljava/lang/StringBuilder;

    #@1ba
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1bd
    const-string v2, "    text="

    #@1bf
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c2
    move-result-object v1

    #@1c3
    iget-object v2, p0, Landroid/inputmethodservice/InputMethodService;->mExtractedText:Landroid/view/inputmethod/ExtractedText;

    #@1c5
    iget-object v2, v2, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    #@1c7
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    #@1ca
    move-result v2

    #@1cb
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1ce
    move-result-object v1

    #@1cf
    const-string v2, " chars"

    #@1d1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d4
    move-result-object v1

    #@1d5
    const-string v2, " startOffset="

    #@1d7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1da
    move-result-object v1

    #@1db
    iget-object v2, p0, Landroid/inputmethodservice/InputMethodService;->mExtractedText:Landroid/view/inputmethod/ExtractedText;

    #@1dd
    iget v2, v2, Landroid/view/inputmethod/ExtractedText;->startOffset:I

    #@1df
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e2
    move-result-object v1

    #@1e3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e6
    move-result-object v1

    #@1e7
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@1ea
    .line 2415
    new-instance v1, Ljava/lang/StringBuilder;

    #@1ec
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1ef
    const-string v2, "    selectionStart="

    #@1f1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f4
    move-result-object v1

    #@1f5
    iget-object v2, p0, Landroid/inputmethodservice/InputMethodService;->mExtractedText:Landroid/view/inputmethod/ExtractedText;

    #@1f7
    iget v2, v2, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    #@1f9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1fc
    move-result-object v1

    #@1fd
    const-string v2, " selectionEnd="

    #@1ff
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@202
    move-result-object v1

    #@203
    iget-object v2, p0, Landroid/inputmethodservice/InputMethodService;->mExtractedText:Landroid/view/inputmethod/ExtractedText;

    #@205
    iget v2, v2, Landroid/view/inputmethod/ExtractedText;->selectionEnd:I

    #@207
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20a
    move-result-object v1

    #@20b
    const-string v2, " flags=0x"

    #@20d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@210
    move-result-object v1

    #@211
    iget-object v2, p0, Landroid/inputmethodservice/InputMethodService;->mExtractedText:Landroid/view/inputmethod/ExtractedText;

    #@213
    iget v2, v2, Landroid/view/inputmethod/ExtractedText;->flags:I

    #@215
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@218
    move-result-object v2

    #@219
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21c
    move-result-object v1

    #@21d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@220
    move-result-object v1

    #@221
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@224
    .line 2421
    :goto_224
    new-instance v1, Ljava/lang/StringBuilder;

    #@226
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@229
    const-string v2, "  mExtractedToken="

    #@22b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22e
    move-result-object v1

    #@22f
    iget v2, p0, Landroid/inputmethodservice/InputMethodService;->mExtractedToken:I

    #@231
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@234
    move-result-object v1

    #@235
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@238
    move-result-object v1

    #@239
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@23c
    .line 2422
    new-instance v1, Ljava/lang/StringBuilder;

    #@23e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@241
    const-string v2, "  mIsInputViewShown="

    #@243
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@246
    move-result-object v1

    #@247
    iget-boolean v2, p0, Landroid/inputmethodservice/InputMethodService;->mIsInputViewShown:Z

    #@249
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@24c
    move-result-object v1

    #@24d
    const-string v2, " mStatusIcon="

    #@24f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@252
    move-result-object v1

    #@253
    iget v2, p0, Landroid/inputmethodservice/InputMethodService;->mStatusIcon:I

    #@255
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@258
    move-result-object v1

    #@259
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25c
    move-result-object v1

    #@25d
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@260
    .line 2424
    const-string v1, "Last computed insets:"

    #@262
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@265
    .line 2425
    new-instance v1, Ljava/lang/StringBuilder;

    #@267
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@26a
    const-string v2, "  contentTopInsets="

    #@26c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26f
    move-result-object v1

    #@270
    iget-object v2, p0, Landroid/inputmethodservice/InputMethodService;->mTmpInsets:Landroid/inputmethodservice/InputMethodService$Insets;

    #@272
    iget v2, v2, Landroid/inputmethodservice/InputMethodService$Insets;->contentTopInsets:I

    #@274
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@277
    move-result-object v1

    #@278
    const-string v2, " visibleTopInsets="

    #@27a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27d
    move-result-object v1

    #@27e
    iget-object v2, p0, Landroid/inputmethodservice/InputMethodService;->mTmpInsets:Landroid/inputmethodservice/InputMethodService$Insets;

    #@280
    iget v2, v2, Landroid/inputmethodservice/InputMethodService$Insets;->visibleTopInsets:I

    #@282
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@285
    move-result-object v1

    #@286
    const-string v2, " touchableInsets="

    #@288
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28b
    move-result-object v1

    #@28c
    iget-object v2, p0, Landroid/inputmethodservice/InputMethodService;->mTmpInsets:Landroid/inputmethodservice/InputMethodService$Insets;

    #@28e
    iget v2, v2, Landroid/inputmethodservice/InputMethodService$Insets;->touchableInsets:I

    #@290
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@293
    move-result-object v1

    #@294
    const-string v2, " touchableRegion="

    #@296
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@299
    move-result-object v1

    #@29a
    iget-object v2, p0, Landroid/inputmethodservice/InputMethodService;->mTmpInsets:Landroid/inputmethodservice/InputMethodService$Insets;

    #@29c
    iget-object v2, v2, Landroid/inputmethodservice/InputMethodService$Insets;->touchableRegion:Landroid/graphics/Region;

    #@29e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2a1
    move-result-object v1

    #@2a2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a5
    move-result-object v1

    #@2a6
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@2a9
    .line 2429
    return-void

    #@2aa
    .line 2399
    :cond_2aa
    const-string v1, "  mInputEditorInfo: null"

    #@2ac
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@2af
    goto/16 :goto_133

    #@2b1
    .line 2419
    :cond_2b1
    const-string v1, "  mExtractedText: null"

    #@2b3
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@2b6
    goto/16 :goto_224
.end method

.method public enableHardwareAcceleration()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 644
    iget-object v1, p0, Landroid/inputmethodservice/InputMethodService;->mWindow:Landroid/inputmethodservice/SoftInputWindow;

    #@3
    if-eqz v1, :cond_d

    #@5
    .line 645
    new-instance v0, Ljava/lang/IllegalStateException;

    #@7
    const-string v1, "Must be called before onCreate()"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 647
    :cond_d
    invoke-static {}, Landroid/app/ActivityManager;->isHighEndGfx()Z

    #@10
    move-result v1

    #@11
    if-eqz v1, :cond_16

    #@13
    .line 648
    iput-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mHardwareAccelerated:Z

    #@15
    .line 651
    :goto_15
    return v0

    #@16
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_15
.end method

.method public getBackDisposition()I
    .registers 2

    #@0
    .prologue
    .line 828
    iget v0, p0, Landroid/inputmethodservice/InputMethodService;->mBackDisposition:I

    #@2
    return v0
.end method

.method public getCandidatesHiddenVisibility()I
    .registers 2

    #@0
    .prologue
    .line 1166
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->isExtractViewShown()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_9

    #@6
    const/16 v0, 0x8

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x4

    #@a
    goto :goto_8
.end method

.method public getCurrentInputBinding()Landroid/view/inputmethod/InputBinding;
    .registers 2

    #@0
    .prologue
    .line 857
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mInputBinding:Landroid/view/inputmethod/InputBinding;

    #@2
    return-object v0
.end method

.method public getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;
    .registers 2

    #@0
    .prologue
    .line 865
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mStartedInputConnection:Landroid/view/inputmethod/InputConnection;

    #@2
    .line 866
    .local v0, ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v0, :cond_5

    #@4
    .line 869
    .end local v0           #ic:Landroid/view/inputmethod/InputConnection;
    :goto_4
    return-object v0

    #@5
    .restart local v0       #ic:Landroid/view/inputmethod/InputConnection;
    :cond_5
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mInputConnection:Landroid/view/inputmethod/InputConnection;

    #@7
    goto :goto_4
.end method

.method public getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;
    .registers 2

    #@0
    .prologue
    .line 877
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mInputEditorInfo:Landroid/view/inputmethod/EditorInfo;

    #@2
    return-object v0
.end method

.method public getCurrentInputStarted()Z
    .registers 2

    #@0
    .prologue
    .line 873
    iget-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mInputStarted:Z

    #@2
    return v0
.end method

.method public getLayoutInflater()Landroid/view/LayoutInflater;
    .registers 2

    #@0
    .prologue
    .line 816
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mInflater:Landroid/view/LayoutInflater;

    #@2
    return-object v0
.end method

.method public getMaxWidth()I
    .registers 3

    #@0
    .prologue
    .line 848
    const-string/jumbo v1, "window"

    #@3
    invoke-virtual {p0, v1}, Landroid/inputmethodservice/InputMethodService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Landroid/view/WindowManager;

    #@9
    .line 849
    .local v0, wm:Landroid/view/WindowManager;
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    #@10
    move-result v1

    #@11
    return v1
.end method

.method public getTextForImeAction(I)Ljava/lang/CharSequence;
    .registers 3
    .parameter "imeOptions"

    #@0
    .prologue
    .line 2192
    and-int/lit16 v0, p1, 0xff

    #@2
    packed-switch v0, :pswitch_data_40

    #@5
    .line 2208
    const v0, 0x104049f

    #@8
    invoke-virtual {p0, v0}, Landroid/inputmethodservice/InputMethodService;->getText(I)Ljava/lang/CharSequence;

    #@b
    move-result-object v0

    #@c
    :goto_c
    return-object v0

    #@d
    .line 2194
    :pswitch_d
    const/4 v0, 0x0

    #@e
    goto :goto_c

    #@f
    .line 2196
    :pswitch_f
    const v0, 0x1040499

    #@12
    invoke-virtual {p0, v0}, Landroid/inputmethodservice/InputMethodService;->getText(I)Ljava/lang/CharSequence;

    #@15
    move-result-object v0

    #@16
    goto :goto_c

    #@17
    .line 2198
    :pswitch_17
    const v0, 0x104049a

    #@1a
    invoke-virtual {p0, v0}, Landroid/inputmethodservice/InputMethodService;->getText(I)Ljava/lang/CharSequence;

    #@1d
    move-result-object v0

    #@1e
    goto :goto_c

    #@1f
    .line 2200
    :pswitch_1f
    const v0, 0x104049b

    #@22
    invoke-virtual {p0, v0}, Landroid/inputmethodservice/InputMethodService;->getText(I)Ljava/lang/CharSequence;

    #@25
    move-result-object v0

    #@26
    goto :goto_c

    #@27
    .line 2202
    :pswitch_27
    const v0, 0x104049c

    #@2a
    invoke-virtual {p0, v0}, Landroid/inputmethodservice/InputMethodService;->getText(I)Ljava/lang/CharSequence;

    #@2d
    move-result-object v0

    #@2e
    goto :goto_c

    #@2f
    .line 2204
    :pswitch_2f
    const v0, 0x104049d

    #@32
    invoke-virtual {p0, v0}, Landroid/inputmethodservice/InputMethodService;->getText(I)Ljava/lang/CharSequence;

    #@35
    move-result-object v0

    #@36
    goto :goto_c

    #@37
    .line 2206
    :pswitch_37
    const v0, 0x104049e

    #@3a
    invoke-virtual {p0, v0}, Landroid/inputmethodservice/InputMethodService;->getText(I)Ljava/lang/CharSequence;

    #@3d
    move-result-object v0

    #@3e
    goto :goto_c

    #@3f
    .line 2192
    nop

    #@40
    :pswitch_data_40
    .packed-switch 0x1
        :pswitch_d
        :pswitch_f
        :pswitch_17
        :pswitch_1f
        :pswitch_27
        :pswitch_2f
        :pswitch_37
    .end packed-switch
.end method

.method public getWindow()Landroid/app/Dialog;
    .registers 2

    #@0
    .prologue
    .line 820
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mWindow:Landroid/inputmethodservice/SoftInputWindow;

    #@2
    return-object v0
.end method

.method public hideStatusIcon()V
    .registers 3

    #@0
    .prologue
    .line 1175
    const/4 v0, 0x0

    #@1
    iput v0, p0, Landroid/inputmethodservice/InputMethodService;->mStatusIcon:I

    #@3
    .line 1176
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mImm:Landroid/view/inputmethod/InputMethodManager;

    #@5
    iget-object v1, p0, Landroid/inputmethodservice/InputMethodService;->mToken:Landroid/os/IBinder;

    #@7
    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->hideStatusIcon(Landroid/os/IBinder;)V

    #@a
    .line 1177
    return-void
.end method

.method public hideWindow()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1503
    invoke-direct {p0}, Landroid/inputmethodservice/InputMethodService;->finishViews()V

    #@4
    .line 1504
    iget-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mWindowVisible:Z

    #@6
    if-eqz v0, :cond_14

    #@8
    .line 1505
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mWindow:Landroid/inputmethodservice/SoftInputWindow;

    #@a
    invoke-virtual {v0}, Landroid/inputmethodservice/SoftInputWindow;->hide()V

    #@d
    .line 1506
    iput-boolean v1, p0, Landroid/inputmethodservice/InputMethodService;->mWindowVisible:Z

    #@f
    .line 1507
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->onWindowHidden()V

    #@12
    .line 1508
    iput-boolean v1, p0, Landroid/inputmethodservice/InputMethodService;->mWindowWasVisible:Z

    #@14
    .line 1510
    :cond_14
    return-void
.end method

.method initViews()V
    .registers 6

    #@0
    .prologue
    const/16 v4, 0x8

    #@2
    const/4 v3, 0x0

    #@3
    const/4 v2, 0x0

    #@4
    .line 691
    iput-boolean v2, p0, Landroid/inputmethodservice/InputMethodService;->mInitialized:Z

    #@6
    .line 692
    iput-boolean v2, p0, Landroid/inputmethodservice/InputMethodService;->mWindowCreated:Z

    #@8
    .line 693
    iput-boolean v2, p0, Landroid/inputmethodservice/InputMethodService;->mShowInputRequested:Z

    #@a
    .line 694
    iput-boolean v2, p0, Landroid/inputmethodservice/InputMethodService;->mShowInputForced:Z

    #@c
    .line 696
    sget-object v0, Landroid/R$styleable;->InputMethodService:[I

    #@e
    invoke-virtual {p0, v0}, Landroid/inputmethodservice/InputMethodService;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    #@11
    move-result-object v0

    #@12
    iput-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mThemeAttrs:Landroid/content/res/TypedArray;

    #@14
    .line 697
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mInflater:Landroid/view/LayoutInflater;

    #@16
    const v1, 0x109004a

    #@19
    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@1c
    move-result-object v0

    #@1d
    iput-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mRootView:Landroid/view/View;

    #@1f
    .line 699
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mWindow:Landroid/inputmethodservice/SoftInputWindow;

    #@21
    iget-object v1, p0, Landroid/inputmethodservice/InputMethodService;->mRootView:Landroid/view/View;

    #@23
    invoke-virtual {v0, v1}, Landroid/inputmethodservice/SoftInputWindow;->setContentView(Landroid/view/View;)V

    #@26
    .line 700
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mRootView:Landroid/view/View;

    #@28
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@2b
    move-result-object v0

    #@2c
    iget-object v1, p0, Landroid/inputmethodservice/InputMethodService;->mInsetsComputer:Landroid/view/ViewTreeObserver$OnComputeInternalInsetsListener;

    #@2e
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnComputeInternalInsetsListener(Landroid/view/ViewTreeObserver$OnComputeInternalInsetsListener;)V

    #@31
    .line 701
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getContentResolver()Landroid/content/ContentResolver;

    #@34
    move-result-object v0

    #@35
    const-string v1, "fancy_ime_animations"

    #@37
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@3a
    move-result v0

    #@3b
    if-eqz v0, :cond_49

    #@3d
    .line 703
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mWindow:Landroid/inputmethodservice/SoftInputWindow;

    #@3f
    invoke-virtual {v0}, Landroid/inputmethodservice/SoftInputWindow;->getWindow()Landroid/view/Window;

    #@42
    move-result-object v0

    #@43
    const v1, 0x10301e9

    #@46
    invoke-virtual {v0, v1}, Landroid/view/Window;->setWindowAnimations(I)V

    #@49
    .line 706
    :cond_49
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mRootView:Landroid/view/View;

    #@4b
    const v1, 0x10202a8

    #@4e
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@51
    move-result-object v0

    #@52
    check-cast v0, Landroid/view/ViewGroup;

    #@54
    iput-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mFullscreenArea:Landroid/view/ViewGroup;

    #@56
    .line 707
    iput-boolean v2, p0, Landroid/inputmethodservice/InputMethodService;->mExtractViewHidden:Z

    #@58
    .line 708
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mRootView:Landroid/view/View;

    #@5a
    const v1, 0x102001c

    #@5d
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@60
    move-result-object v0

    #@61
    check-cast v0, Landroid/widget/FrameLayout;

    #@63
    iput-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mExtractFrame:Landroid/widget/FrameLayout;

    #@65
    .line 709
    iput-object v3, p0, Landroid/inputmethodservice/InputMethodService;->mExtractView:Landroid/view/View;

    #@67
    .line 710
    iput-object v3, p0, Landroid/inputmethodservice/InputMethodService;->mExtractEditText:Landroid/inputmethodservice/ExtractEditText;

    #@69
    .line 711
    iput-object v3, p0, Landroid/inputmethodservice/InputMethodService;->mExtractAccessories:Landroid/view/ViewGroup;

    #@6b
    .line 712
    iput-object v3, p0, Landroid/inputmethodservice/InputMethodService;->mExtractAction:Landroid/widget/Button;

    #@6d
    .line 713
    iput-boolean v2, p0, Landroid/inputmethodservice/InputMethodService;->mFullscreenApplied:Z

    #@6f
    .line 715
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mRootView:Landroid/view/View;

    #@71
    const v1, 0x102001d

    #@74
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@77
    move-result-object v0

    #@78
    check-cast v0, Landroid/widget/FrameLayout;

    #@7a
    iput-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mCandidatesFrame:Landroid/widget/FrameLayout;

    #@7c
    .line 716
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mRootView:Landroid/view/View;

    #@7e
    const v1, 0x102001e

    #@81
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@84
    move-result-object v0

    #@85
    check-cast v0, Landroid/widget/FrameLayout;

    #@87
    iput-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mInputFrame:Landroid/widget/FrameLayout;

    #@89
    .line 717
    iput-object v3, p0, Landroid/inputmethodservice/InputMethodService;->mInputView:Landroid/view/View;

    #@8b
    .line 718
    iput-boolean v2, p0, Landroid/inputmethodservice/InputMethodService;->mIsInputViewShown:Z

    #@8d
    .line 720
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mExtractFrame:Landroid/widget/FrameLayout;

    #@8f
    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    #@92
    .line 721
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getCandidatesHiddenVisibility()I

    #@95
    move-result v0

    #@96
    iput v0, p0, Landroid/inputmethodservice/InputMethodService;->mCandidatesVisibility:I

    #@98
    .line 722
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mCandidatesFrame:Landroid/widget/FrameLayout;

    #@9a
    iget v1, p0, Landroid/inputmethodservice/InputMethodService;->mCandidatesVisibility:I

    #@9c
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    #@9f
    .line 723
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mInputFrame:Landroid/widget/FrameLayout;

    #@a1
    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    #@a4
    .line 724
    return-void
.end method

.method initialize()V
    .registers 2

    #@0
    .prologue
    .line 684
    iget-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mInitialized:Z

    #@2
    if-nez v0, :cond_a

    #@4
    .line 685
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mInitialized:Z

    #@7
    .line 686
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->onInitializeInterface()V

    #@a
    .line 688
    :cond_a
    return-void
.end method

.method public isExtractViewShown()Z
    .registers 2

    #@0
    .prologue
    .line 1013
    iget-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mIsFullscreen:Z

    #@2
    if-eqz v0, :cond_a

    #@4
    iget-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mExtractViewHidden:Z

    #@6
    if-nez v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public isFullscreenMode()Z
    .registers 2

    #@0
    .prologue
    .line 963
    iget-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mIsFullscreen:Z

    #@2
    return v0
.end method

.method public isInputViewShown()Z
    .registers 2

    #@0
    .prologue
    .line 1112
    iget-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mIsInputViewShown:Z

    #@2
    if-eqz v0, :cond_a

    #@4
    iget-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mWindowVisible:Z

    #@6
    if-eqz v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public isShowInputRequested()Z
    .registers 2

    #@0
    .prologue
    .line 1103
    iget-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mShowInputRequested:Z

    #@2
    return v0
.end method

.method public onAppPrivateCommand(Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 3
    .parameter "action"
    .parameter "data"

    #@0
    .prologue
    .line 1883
    return-void
.end method

.method public onBindInput()V
    .registers 1

    #@0
    .prologue
    .line 1538
    return-void
.end method

.method public onComputeInsets(Landroid/inputmethodservice/InputMethodService$Insets;)V
    .registers 6
    .parameter "outInsets"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1054
    iget-object v1, p0, Landroid/inputmethodservice/InputMethodService;->mTmpLocation:[I

    #@3
    .line 1055
    .local v1, loc:[I
    iget-object v2, p0, Landroid/inputmethodservice/InputMethodService;->mInputFrame:Landroid/widget/FrameLayout;

    #@5
    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getVisibility()I

    #@8
    move-result v2

    #@9
    if-nez v2, :cond_42

    #@b
    .line 1056
    iget-object v2, p0, Landroid/inputmethodservice/InputMethodService;->mInputFrame:Landroid/widget/FrameLayout;

    #@d
    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->getLocationInWindow([I)V

    #@10
    .line 1061
    :goto_10
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->isFullscreenMode()Z

    #@13
    move-result v2

    #@14
    if-eqz v2, :cond_55

    #@16
    .line 1063
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getWindow()Landroid/app/Dialog;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@21
    move-result-object v0

    #@22
    .line 1064
    .local v0, decor:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    #@25
    move-result v2

    #@26
    iput v2, p1, Landroid/inputmethodservice/InputMethodService$Insets;->contentTopInsets:I

    #@28
    .line 1068
    .end local v0           #decor:Landroid/view/View;
    :goto_28
    iget-object v2, p0, Landroid/inputmethodservice/InputMethodService;->mCandidatesFrame:Landroid/widget/FrameLayout;

    #@2a
    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getVisibility()I

    #@2d
    move-result v2

    #@2e
    if-nez v2, :cond_35

    #@30
    .line 1069
    iget-object v2, p0, Landroid/inputmethodservice/InputMethodService;->mCandidatesFrame:Landroid/widget/FrameLayout;

    #@32
    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->getLocationInWindow([I)V

    #@35
    .line 1071
    :cond_35
    aget v2, v1, v3

    #@37
    iput v2, p1, Landroid/inputmethodservice/InputMethodService$Insets;->visibleTopInsets:I

    #@39
    .line 1072
    const/4 v2, 0x2

    #@3a
    iput v2, p1, Landroid/inputmethodservice/InputMethodService$Insets;->touchableInsets:I

    #@3c
    .line 1073
    iget-object v2, p1, Landroid/inputmethodservice/InputMethodService$Insets;->touchableRegion:Landroid/graphics/Region;

    #@3e
    invoke-virtual {v2}, Landroid/graphics/Region;->setEmpty()V

    #@41
    .line 1074
    return-void

    #@42
    .line 1058
    :cond_42
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getWindow()Landroid/app/Dialog;

    #@45
    move-result-object v2

    #@46
    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    #@49
    move-result-object v2

    #@4a
    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@4d
    move-result-object v0

    #@4e
    .line 1059
    .restart local v0       #decor:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    #@51
    move-result v2

    #@52
    aput v2, v1, v3

    #@54
    goto :goto_10

    #@55
    .line 1066
    .end local v0           #decor:Landroid/view/View;
    :cond_55
    aget v2, v1, v3

    #@57
    iput v2, p1, Landroid/inputmethodservice/InputMethodService$Insets;->contentTopInsets:I

    #@59
    goto :goto_28
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 11
    .parameter "newConfig"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 757
    invoke-super {p0, p1}, Landroid/inputmethodservice/AbstractInputMethodService;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    #@5
    .line 759
    iget-boolean v4, p0, Landroid/inputmethodservice/InputMethodService;->mWindowVisible:Z

    #@7
    .line 760
    .local v4, visible:Z
    iget v1, p0, Landroid/inputmethodservice/InputMethodService;->mShowInputFlags:I

    #@9
    .line 761
    .local v1, showFlags:I
    iget-boolean v3, p0, Landroid/inputmethodservice/InputMethodService;->mShowInputRequested:Z

    #@b
    .line 762
    .local v3, showingInput:Z
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mCurCompletions:[Landroid/view/inputmethod/CompletionInfo;

    #@d
    .line 763
    .local v0, completions:[Landroid/view/inputmethod/CompletionInfo;
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->initViews()V

    #@10
    .line 764
    iput-boolean v5, p0, Landroid/inputmethodservice/InputMethodService;->mInputViewStarted:Z

    #@12
    .line 765
    iput-boolean v5, p0, Landroid/inputmethodservice/InputMethodService;->mCandidatesViewStarted:Z

    #@14
    .line 766
    iget-boolean v6, p0, Landroid/inputmethodservice/InputMethodService;->mInputStarted:Z

    #@16
    if-eqz v6, :cond_23

    #@18
    .line 767
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    #@1b
    move-result-object v6

    #@1c
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;

    #@1f
    move-result-object v7

    #@20
    invoke-virtual {p0, v6, v7, v8}, Landroid/inputmethodservice/InputMethodService;->doStartInput(Landroid/view/inputmethod/InputConnection;Landroid/view/inputmethod/EditorInfo;Z)V

    #@23
    .line 770
    :cond_23
    if-eqz v4, :cond_49

    #@25
    .line 771
    if-eqz v3, :cond_4e

    #@27
    .line 773
    invoke-virtual {p0, v1, v8}, Landroid/inputmethodservice/InputMethodService;->onShowInputRequested(IZ)Z

    #@2a
    move-result v6

    #@2b
    if-eqz v6, :cond_4a

    #@2d
    .line 774
    invoke-virtual {p0, v8}, Landroid/inputmethodservice/InputMethodService;->showWindow(Z)V

    #@30
    .line 775
    if-eqz v0, :cond_37

    #@32
    .line 776
    iput-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mCurCompletions:[Landroid/view/inputmethod/CompletionInfo;

    #@34
    .line 777
    invoke-virtual {p0, v0}, Landroid/inputmethodservice/InputMethodService;->onDisplayCompletions([Landroid/view/inputmethod/CompletionInfo;)V

    #@37
    .line 791
    :cond_37
    :goto_37
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->onEvaluateInputViewShown()Z

    #@3a
    move-result v2

    #@3b
    .line 792
    .local v2, showing:Z
    iget-object v6, p0, Landroid/inputmethodservice/InputMethodService;->mImm:Landroid/view/inputmethod/InputMethodManager;

    #@3d
    iget-object v7, p0, Landroid/inputmethodservice/InputMethodService;->mToken:Landroid/os/IBinder;

    #@3f
    if-eqz v2, :cond_42

    #@41
    const/4 v5, 0x2

    #@42
    :cond_42
    or-int/lit8 v5, v5, 0x1

    #@44
    iget v8, p0, Landroid/inputmethodservice/InputMethodService;->mBackDisposition:I

    #@46
    invoke-virtual {v6, v7, v5, v8}, Landroid/view/inputmethod/InputMethodManager;->setImeWindowStatus(Landroid/os/IBinder;II)V

    #@49
    .line 795
    .end local v2           #showing:Z
    :cond_49
    return-void

    #@4a
    .line 780
    :cond_4a
    invoke-direct {p0}, Landroid/inputmethodservice/InputMethodService;->doHideWindow()V

    #@4d
    goto :goto_37

    #@4e
    .line 782
    :cond_4e
    iget v6, p0, Landroid/inputmethodservice/InputMethodService;->mCandidatesVisibility:I

    #@50
    if-nez v6, :cond_56

    #@52
    .line 785
    invoke-virtual {p0, v5}, Landroid/inputmethodservice/InputMethodService;->showWindow(Z)V

    #@55
    goto :goto_37

    #@56
    .line 788
    :cond_56
    invoke-direct {p0}, Landroid/inputmethodservice/InputMethodService;->doHideWindow()V

    #@59
    goto :goto_37
.end method

.method public onConfigureWindow(Landroid/view/Window;ZZ)V
    .registers 10
    .parameter "win"
    .parameter "isFullscreen"
    .parameter "isCandidatesOnly"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 948
    iget-object v3, p0, Landroid/inputmethodservice/InputMethodService;->mWindow:Landroid/inputmethodservice/SoftInputWindow;

    #@3
    invoke-virtual {v3}, Landroid/inputmethodservice/SoftInputWindow;->getWindow()Landroid/view/Window;

    #@6
    move-result-object v3

    #@7
    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@a
    move-result-object v3

    #@b
    iget v0, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@d
    .line 949
    .local v0, currentHeight:I
    if-eqz p2, :cond_42

    #@f
    move v1, v2

    #@10
    .line 950
    .local v1, newHeight:I
    :goto_10
    iget-boolean v3, p0, Landroid/inputmethodservice/InputMethodService;->mIsInputViewShown:Z

    #@12
    if-eqz v3, :cond_38

    #@14
    if-eq v0, v1, :cond_38

    #@16
    .line 951
    const-string v3, "InputMethodService"

    #@18
    new-instance v4, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v5, "Window size has been changed. This may cause jankiness of resizing window: "

    #@1f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v4

    #@27
    const-string v5, " -> "

    #@29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v4

    #@35
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 954
    :cond_38
    iget-object v3, p0, Landroid/inputmethodservice/InputMethodService;->mWindow:Landroid/inputmethodservice/SoftInputWindow;

    #@3a
    invoke-virtual {v3}, Landroid/inputmethodservice/SoftInputWindow;->getWindow()Landroid/view/Window;

    #@3d
    move-result-object v3

    #@3e
    invoke-virtual {v3, v2, v1}, Landroid/view/Window;->setLayout(II)V

    #@41
    .line 955
    return-void

    #@42
    .line 949
    .end local v1           #newHeight:I
    :cond_42
    const/4 v1, -0x2

    #@43
    goto :goto_10
.end method

.method public onCreate()V
    .registers 6

    #@0
    .prologue
    .line 655
    iget v0, p0, Landroid/inputmethodservice/InputMethodService;->mTheme:I

    #@2
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@5
    move-result-object v1

    #@6
    iget v1, v1, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@8
    const v2, 0x1030054

    #@b
    const v3, 0x103007f

    #@e
    const v4, 0x103013e

    #@11
    invoke-static {v0, v1, v2, v3, v4}, Landroid/content/res/Resources;->selectSystemTheme(IIIII)I

    #@14
    move-result v0

    #@15
    iput v0, p0, Landroid/inputmethodservice/InputMethodService;->mTheme:I

    #@17
    .line 660
    iget v0, p0, Landroid/inputmethodservice/InputMethodService;->mTheme:I

    #@19
    invoke-super {p0, v0}, Landroid/inputmethodservice/AbstractInputMethodService;->setTheme(I)V

    #@1c
    .line 661
    invoke-super {p0}, Landroid/inputmethodservice/AbstractInputMethodService;->onCreate()V

    #@1f
    .line 662
    const-string v0, "input_method"

    #@21
    invoke-virtual {p0, v0}, Landroid/inputmethodservice/InputMethodService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@24
    move-result-object v0

    #@25
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    #@27
    iput-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mImm:Landroid/view/inputmethod/InputMethodManager;

    #@29
    .line 663
    const-string/jumbo v0, "layout_inflater"

    #@2c
    invoke-virtual {p0, v0}, Landroid/inputmethodservice/InputMethodService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2f
    move-result-object v0

    #@30
    check-cast v0, Landroid/view/LayoutInflater;

    #@32
    iput-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mInflater:Landroid/view/LayoutInflater;

    #@34
    .line 665
    new-instance v0, Landroid/inputmethodservice/SoftInputWindow;

    #@36
    iget v1, p0, Landroid/inputmethodservice/InputMethodService;->mTheme:I

    #@38
    iget-object v2, p0, Landroid/inputmethodservice/AbstractInputMethodService;->mDispatcherState:Landroid/view/KeyEvent$DispatcherState;

    #@3a
    invoke-direct {v0, p0, v1, v2}, Landroid/inputmethodservice/SoftInputWindow;-><init>(Landroid/content/Context;ILandroid/view/KeyEvent$DispatcherState;)V

    #@3d
    iput-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mWindow:Landroid/inputmethodservice/SoftInputWindow;

    #@3f
    .line 666
    iget-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mHardwareAccelerated:Z

    #@41
    if-eqz v0, :cond_4e

    #@43
    .line 667
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mWindow:Landroid/inputmethodservice/SoftInputWindow;

    #@45
    invoke-virtual {v0}, Landroid/inputmethodservice/SoftInputWindow;->getWindow()Landroid/view/Window;

    #@48
    move-result-object v0

    #@49
    const/high16 v1, 0x100

    #@4b
    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    #@4e
    .line 669
    :cond_4e
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->initViews()V

    #@51
    .line 670
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mWindow:Landroid/inputmethodservice/SoftInputWindow;

    #@53
    invoke-virtual {v0}, Landroid/inputmethodservice/SoftInputWindow;->getWindow()Landroid/view/Window;

    #@56
    move-result-object v0

    #@57
    const/4 v1, -0x1

    #@58
    const/4 v2, -0x2

    #@59
    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setLayout(II)V

    #@5c
    .line 671
    return-void
.end method

.method public onCreateCandidatesView()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 1263
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public onCreateExtractTextView()Landroid/view/View;
    .registers 4

    #@0
    .prologue
    .line 1248
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mInflater:Landroid/view/LayoutInflater;

    #@2
    const v1, 0x109004b

    #@5
    const/4 v2, 0x0

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public onCreateInputMethodInterface()Landroid/inputmethodservice/AbstractInputMethodService$AbstractInputMethodImpl;
    .registers 2

    #@0
    .prologue
    .line 803
    new-instance v0, Landroid/inputmethodservice/InputMethodService$InputMethodImpl;

    #@2
    invoke-direct {v0, p0}, Landroid/inputmethodservice/InputMethodService$InputMethodImpl;-><init>(Landroid/inputmethodservice/InputMethodService;)V

    #@5
    return-object v0
.end method

.method public onCreateInputMethodSessionInterface()Landroid/inputmethodservice/AbstractInputMethodService$AbstractInputMethodSessionImpl;
    .registers 2

    #@0
    .prologue
    .line 812
    new-instance v0, Landroid/inputmethodservice/InputMethodService$InputMethodSessionImpl;

    #@2
    invoke-direct {v0, p0}, Landroid/inputmethodservice/InputMethodService$InputMethodSessionImpl;-><init>(Landroid/inputmethodservice/InputMethodService;)V

    #@5
    return-object v0
.end method

.method public onCreateInputView()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 1278
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method protected onCurrentInputMethodSubtypeChanged(Landroid/view/inputmethod/InputMethodSubtype;)V
    .registers 2
    .parameter "newSubtype"

    #@0
    .prologue
    .line 2372
    return-void
.end method

.method public onDestroy()V
    .registers 3

    #@0
    .prologue
    .line 727
    invoke-super {p0}, Landroid/inputmethodservice/AbstractInputMethodService;->onDestroy()V

    #@3
    .line 728
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mRootView:Landroid/view/View;

    #@5
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@8
    move-result-object v0

    #@9
    iget-object v1, p0, Landroid/inputmethodservice/InputMethodService;->mInsetsComputer:Landroid/view/ViewTreeObserver$OnComputeInternalInsetsListener;

    #@b
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnComputeInternalInsetsListener(Landroid/view/ViewTreeObserver$OnComputeInternalInsetsListener;)V

    #@e
    .line 730
    invoke-direct {p0}, Landroid/inputmethodservice/InputMethodService;->finishViews()V

    #@11
    .line 731
    iget-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mWindowAdded:Z

    #@13
    if-eqz v0, :cond_24

    #@15
    .line 735
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mWindow:Landroid/inputmethodservice/SoftInputWindow;

    #@17
    invoke-virtual {v0}, Landroid/inputmethodservice/SoftInputWindow;->getWindow()Landroid/view/Window;

    #@1a
    move-result-object v0

    #@1b
    const/4 v1, 0x0

    #@1c
    invoke-virtual {v0, v1}, Landroid/view/Window;->setWindowAnimations(I)V

    #@1f
    .line 736
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mWindow:Landroid/inputmethodservice/SoftInputWindow;

    #@21
    invoke-virtual {v0}, Landroid/inputmethodservice/SoftInputWindow;->dismiss()V

    #@24
    .line 738
    :cond_24
    return-void
.end method

.method public onDisplayCompletions([Landroid/view/inputmethod/CompletionInfo;)V
    .registers 2
    .parameter "completions"

    #@0
    .prologue
    .line 1639
    return-void
.end method

.method public onEvaluateFullscreenMode()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 975
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getResources()Landroid/content/res/Resources;

    #@4
    move-result-object v2

    #@5
    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@8
    move-result-object v0

    #@9
    .line 976
    .local v0, config:Landroid/content/res/Configuration;
    iget v2, v0, Landroid/content/res/Configuration;->orientation:I

    #@b
    const/4 v3, 0x2

    #@c
    if-eq v2, v3, :cond_f

    #@e
    .line 983
    :cond_e
    :goto_e
    return v1

    #@f
    .line 979
    :cond_f
    iget-object v2, p0, Landroid/inputmethodservice/InputMethodService;->mInputEditorInfo:Landroid/view/inputmethod/EditorInfo;

    #@11
    if-eqz v2, :cond_1c

    #@13
    iget-object v2, p0, Landroid/inputmethodservice/InputMethodService;->mInputEditorInfo:Landroid/view/inputmethod/EditorInfo;

    #@15
    iget v2, v2, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    #@17
    const/high16 v3, 0x200

    #@19
    and-int/2addr v2, v3

    #@1a
    if-nez v2, :cond_e

    #@1c
    .line 983
    :cond_1c
    const/4 v1, 0x1

    #@1d
    goto :goto_e
.end method

.method public onEvaluateInputViewShown()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1124
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getResources()Landroid/content/res/Resources;

    #@4
    move-result-object v2

    #@5
    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@8
    move-result-object v0

    #@9
    .line 1125
    .local v0, config:Landroid/content/res/Configuration;
    iget v2, v0, Landroid/content/res/Configuration;->keyboard:I

    #@b
    if-eq v2, v1, :cond_12

    #@d
    iget v2, v0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    #@f
    const/4 v3, 0x2

    #@10
    if-ne v2, v3, :cond_13

    #@12
    :cond_12
    :goto_12
    return v1

    #@13
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_12
.end method

.method public onExtractTextContextMenuItem(I)Z
    .registers 4
    .parameter "id"

    #@0
    .prologue
    .line 2173
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    #@3
    move-result-object v0

    #@4
    .line 2174
    .local v0, ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v0, :cond_9

    #@6
    .line 2175
    invoke-interface {v0, p1}, Landroid/view/inputmethod/InputConnection;->performContextMenuAction(I)Z

    #@9
    .line 2177
    :cond_9
    const/4 v1, 0x1

    #@a
    return v1
.end method

.method public onExtractedCursorMovement(II)V
    .registers 4
    .parameter "dx"
    .parameter "dy"

    #@0
    .prologue
    .line 2156
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mExtractEditText:Landroid/inputmethodservice/ExtractEditText;

    #@2
    if-eqz v0, :cond_6

    #@4
    if-nez p2, :cond_7

    #@6
    .line 2162
    :cond_6
    :goto_6
    return-void

    #@7
    .line 2159
    :cond_7
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mExtractEditText:Landroid/inputmethodservice/ExtractEditText;

    #@9
    invoke-virtual {v0}, Landroid/inputmethodservice/ExtractEditText;->hasVerticalScrollBar()Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_6

    #@f
    .line 2160
    const/4 v0, 0x0

    #@10
    invoke-virtual {p0, v0}, Landroid/inputmethodservice/InputMethodService;->setCandidatesViewShown(Z)V

    #@13
    goto :goto_6
.end method

.method public onExtractedDeleteText(II)V
    .registers 6
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 2095
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    #@3
    move-result-object v0

    #@4
    .line 2096
    .local v0, conn:Landroid/view/inputmethod/InputConnection;
    if-eqz v0, :cond_f

    #@6
    .line 2097
    invoke-interface {v0, p1, p1}, Landroid/view/inputmethod/InputConnection;->setSelection(II)Z

    #@9
    .line 2098
    const/4 v1, 0x0

    #@a
    sub-int v2, p2, p1

    #@c
    invoke-interface {v0, v1, v2}, Landroid/view/inputmethod/InputConnection;->deleteSurroundingText(II)Z

    #@f
    .line 2100
    :cond_f
    return-void
.end method

.method public onExtractedReplaceText(IILjava/lang/CharSequence;)V
    .registers 6
    .parameter "start"
    .parameter "end"
    .parameter "text"

    #@0
    .prologue
    .line 2106
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    #@3
    move-result-object v0

    #@4
    .line 2107
    .local v0, conn:Landroid/view/inputmethod/InputConnection;
    if-eqz v0, :cond_d

    #@6
    .line 2108
    invoke-interface {v0, p1, p2}, Landroid/view/inputmethod/InputConnection;->setComposingRegion(II)Z

    #@9
    .line 2109
    const/4 v1, 0x1

    #@a
    invoke-interface {v0, p3, v1}, Landroid/view/inputmethod/InputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    #@d
    .line 2111
    :cond_d
    return-void
.end method

.method public onExtractedSelectionChanged(II)V
    .registers 4
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 2085
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    #@3
    move-result-object v0

    #@4
    .line 2086
    .local v0, conn:Landroid/view/inputmethod/InputConnection;
    if-eqz v0, :cond_9

    #@6
    .line 2087
    invoke-interface {v0, p1, p2}, Landroid/view/inputmethod/InputConnection;->setSelection(II)Z

    #@9
    .line 2089
    :cond_9
    return-void
.end method

.method public onExtractedSetSpan(Ljava/lang/Object;III)V
    .registers 11
    .parameter "span"
    .parameter "start"
    .parameter "end"
    .parameter "flags"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 2117
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    #@4
    move-result-object v0

    #@5
    .line 2118
    .local v0, conn:Landroid/view/inputmethod/InputConnection;
    if-eqz v0, :cond_d

    #@7
    .line 2119
    invoke-interface {v0, p2, p3}, Landroid/view/inputmethod/InputConnection;->setSelection(II)Z

    #@a
    move-result v2

    #@b
    if-nez v2, :cond_e

    #@d
    .line 2127
    :cond_d
    :goto_d
    return-void

    #@e
    .line 2120
    :cond_e
    invoke-interface {v0, v5}, Landroid/view/inputmethod/InputConnection;->getSelectedText(I)Ljava/lang/CharSequence;

    #@11
    move-result-object v1

    #@12
    .line 2121
    .local v1, text:Ljava/lang/CharSequence;
    instance-of v2, v1, Landroid/text/Spannable;

    #@14
    if-eqz v2, :cond_d

    #@16
    move-object v2, v1

    #@17
    .line 2122
    check-cast v2, Landroid/text/Spannable;

    #@19
    const/4 v3, 0x0

    #@1a
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    #@1d
    move-result v4

    #@1e
    invoke-interface {v2, p1, v3, v4, p4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@21
    .line 2123
    invoke-interface {v0, p2, p3}, Landroid/view/inputmethod/InputConnection;->setComposingRegion(II)Z

    #@24
    .line 2124
    invoke-interface {v0, v1, v5}, Landroid/view/inputmethod/InputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    #@27
    goto :goto_d
.end method

.method public onExtractedTextClicked()V
    .registers 2

    #@0
    .prologue
    .line 2137
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mExtractEditText:Landroid/inputmethodservice/ExtractEditText;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 2143
    :cond_4
    :goto_4
    return-void

    #@5
    .line 2140
    :cond_5
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mExtractEditText:Landroid/inputmethodservice/ExtractEditText;

    #@7
    invoke-virtual {v0}, Landroid/inputmethodservice/ExtractEditText;->hasVerticalScrollBar()Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_4

    #@d
    .line 2141
    const/4 v0, 0x0

    #@e
    invoke-virtual {p0, v0}, Landroid/inputmethodservice/InputMethodService;->setCandidatesViewShown(Z)V

    #@11
    goto :goto_4
.end method

.method public onExtractingInputChanged(Landroid/view/inputmethod/EditorInfo;)V
    .registers 3
    .parameter "ei"

    #@0
    .prologue
    .line 2284
    iget v0, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    #@2
    if-nez v0, :cond_8

    #@4
    .line 2285
    const/4 v0, 0x2

    #@5
    invoke-virtual {p0, v0}, Landroid/inputmethodservice/InputMethodService;->requestHideSelf(I)V

    #@8
    .line 2287
    :cond_8
    return-void
.end method

.method public onFinishCandidatesView(Z)V
    .registers 3
    .parameter "finishingInput"

    #@0
    .prologue
    .line 1355
    if-nez p1, :cond_b

    #@2
    .line 1356
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    #@5
    move-result-object v0

    #@6
    .line 1357
    .local v0, ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v0, :cond_b

    #@8
    .line 1358
    invoke-interface {v0}, Landroid/view/inputmethod/InputConnection;->finishComposingText()Z

    #@b
    .line 1361
    .end local v0           #ic:Landroid/view/inputmethod/InputConnection;
    :cond_b
    return-void
.end method

.method public onFinishInput()V
    .registers 2

    #@0
    .prologue
    .line 1622
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    #@3
    move-result-object v0

    #@4
    .line 1623
    .local v0, ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v0, :cond_9

    #@6
    .line 1624
    invoke-interface {v0}, Landroid/view/inputmethod/InputConnection;->finishComposingText()Z

    #@9
    .line 1626
    :cond_9
    return-void
.end method

.method public onFinishInputView(Z)V
    .registers 3
    .parameter "finishingInput"

    #@0
    .prologue
    .line 1310
    if-nez p1, :cond_b

    #@2
    .line 1311
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    #@5
    move-result-object v0

    #@6
    .line 1312
    .local v0, ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v0, :cond_b

    #@8
    .line 1313
    invoke-interface {v0}, Landroid/view/inputmethod/InputConnection;->finishComposingText()Z

    #@b
    .line 1316
    .end local v0           #ic:Landroid/view/inputmethod/InputConnection;
    :cond_b
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 1879
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onInitializeInterface()V
    .registers 1

    #@0
    .prologue
    .line 681
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 6
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1773
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    #@4
    move-result v1

    #@5
    const/4 v2, 0x4

    #@6
    if-ne v1, v2, :cond_13

    #@8
    .line 1774
    invoke-direct {p0, v0}, Landroid/inputmethodservice/InputMethodService;->handleBack(Z)Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_12

    #@e
    .line 1775
    invoke-virtual {p2}, Landroid/view/KeyEvent;->startTracking()V

    #@11
    .line 1776
    const/4 v0, 0x1

    #@12
    .line 1780
    :cond_12
    :goto_12
    return v0

    #@13
    :cond_13
    const/4 v0, -0x1

    #@14
    invoke-virtual {p0, p1, p2, v0}, Landroid/inputmethodservice/InputMethodService;->doMovementKey(ILandroid/view/KeyEvent;I)Z

    #@17
    move-result v0

    #@18
    goto :goto_12
.end method

.method public onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 1789
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .registers 10
    .parameter "keyCode"
    .parameter "count"
    .parameter "event"

    #@0
    .prologue
    const/4 v5, 0x5

    #@1
    const/4 v3, 0x1

    #@2
    .line 1806
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_OSP:Z

    #@4
    if-eqz v4, :cond_43

    #@6
    if-nez p1, :cond_43

    #@8
    if-eqz p3, :cond_43

    #@a
    .line 1808
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getCharacters()Ljava/lang/String;

    #@d
    move-result-object v2

    #@e
    .line 1809
    .local v2, text:Ljava/lang/String;
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    #@11
    move-result-object v1

    #@12
    .line 1810
    .local v1, ic:Landroid/view/inputmethod/InputConnection;
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;

    #@15
    move-result-object v0

    #@16
    .line 1812
    .local v0, ei:Landroid/view/inputmethod/EditorInfo;
    if-eqz v2, :cond_43

    #@18
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@1b
    move-result v4

    #@1c
    if-lez v4, :cond_43

    #@1e
    if-eqz v1, :cond_43

    #@20
    if-eqz v0, :cond_43

    #@22
    .line 1813
    invoke-interface {v1}, Landroid/view/inputmethod/InputConnection;->finishComposingText()Z

    #@25
    .line 1814
    const/4 v4, 0x0

    #@26
    invoke-virtual {p0, v4}, Landroid/inputmethodservice/InputMethodService;->requestHideSelf(I)V

    #@29
    .line 1816
    const-string v4, "\t"

    #@2b
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v4

    #@2f
    if-eqz v4, :cond_3f

    #@31
    .line 1817
    iget v4, v0, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    #@33
    and-int/lit16 v4, v4, 0xff

    #@35
    if-ne v4, v5, :cond_3b

    #@37
    .line 1819
    invoke-interface {v1, v5}, Landroid/view/inputmethod/InputConnection;->performEditorAction(I)Z

    #@3a
    .line 1832
    .end local v0           #ei:Landroid/view/inputmethod/EditorInfo;
    .end local v1           #ic:Landroid/view/inputmethod/InputConnection;
    .end local v2           #text:Ljava/lang/String;
    :goto_3a
    return v3

    #@3b
    .line 1821
    .restart local v0       #ei:Landroid/view/inputmethod/EditorInfo;
    .restart local v1       #ic:Landroid/view/inputmethod/InputConnection;
    .restart local v2       #text:Ljava/lang/String;
    :cond_3b
    invoke-interface {v1, v2, v3}, Landroid/view/inputmethod/InputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    #@3e
    goto :goto_3a

    #@3f
    .line 1824
    :cond_3f
    invoke-interface {v1, v2, v3}, Landroid/view/inputmethod/InputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    #@42
    goto :goto_3a

    #@43
    .line 1832
    .end local v0           #ei:Landroid/view/inputmethod/EditorInfo;
    .end local v1           #ic:Landroid/view/inputmethod/InputConnection;
    .end local v2           #text:Ljava/lang/String;
    :cond_43
    invoke-virtual {p0, p1, p3, p2}, Landroid/inputmethodservice/InputMethodService;->doMovementKey(ILandroid/view/KeyEvent;I)Z

    #@46
    move-result v3

    #@47
    goto :goto_3a
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 1848
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    #@3
    move-result v0

    #@4
    const/4 v1, 0x4

    #@5
    if-ne v0, v1, :cond_19

    #@7
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_19

    #@d
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    #@10
    move-result v0

    #@11
    if-nez v0, :cond_19

    #@13
    .line 1850
    const/4 v0, 0x1

    #@14
    invoke-direct {p0, v0}, Landroid/inputmethodservice/InputMethodService;->handleBack(Z)Z

    #@17
    move-result v0

    #@18
    .line 1853
    :goto_18
    return v0

    #@19
    :cond_19
    const/4 v0, -0x2

    #@1a
    invoke-virtual {p0, p1, p2, v0}, Landroid/inputmethodservice/InputMethodService;->doMovementKey(ILandroid/view/KeyEvent;I)Z

    #@1d
    move-result v0

    #@1e
    goto :goto_18
.end method

.method public onShowInputRequested(IZ)Z
    .registers 7
    .parameter "flags"
    .parameter "configChange"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1379
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->onEvaluateInputViewShown()Z

    #@5
    move-result v3

    #@6
    if-nez v3, :cond_9

    #@8
    .line 1402
    :cond_8
    :goto_8
    return v1

    #@9
    .line 1382
    :cond_9
    and-int/lit8 v3, p1, 0x1

    #@b
    if-nez v3, :cond_21

    #@d
    .line 1383
    if-nez p2, :cond_15

    #@f
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->onEvaluateFullscreenMode()Z

    #@12
    move-result v3

    #@13
    if-nez v3, :cond_8

    #@15
    .line 1391
    :cond_15
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getResources()Landroid/content/res/Resources;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@1c
    move-result-object v0

    #@1d
    .line 1392
    .local v0, config:Landroid/content/res/Configuration;
    iget v3, v0, Landroid/content/res/Configuration;->keyboard:I

    #@1f
    if-ne v3, v2, :cond_8

    #@21
    .line 1399
    .end local v0           #config:Landroid/content/res/Configuration;
    :cond_21
    and-int/lit8 v1, p1, 0x2

    #@23
    if-eqz v1, :cond_27

    #@25
    .line 1400
    iput-boolean v2, p0, Landroid/inputmethodservice/InputMethodService;->mShowInputForced:Z

    #@27
    :cond_27
    move v1, v2

    #@28
    .line 1402
    goto :goto_8
.end method

.method public onStartCandidatesView(Landroid/view/inputmethod/EditorInfo;Z)V
    .registers 3
    .parameter "info"
    .parameter "restarting"

    #@0
    .prologue
    .line 1339
    return-void
.end method

.method public onStartInput(Landroid/view/inputmethod/EditorInfo;Z)V
    .registers 3
    .parameter "attribute"
    .parameter "restarting"

    #@0
    .prologue
    .line 1564
    return-void
.end method

.method public onStartInputView(Landroid/view/inputmethod/EditorInfo;Z)V
    .registers 3
    .parameter "info"
    .parameter "restarting"

    #@0
    .prologue
    .line 1294
    return-void
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 1866
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onUnbindInput()V
    .registers 1

    #@0
    .prologue
    .line 1548
    return-void
.end method

.method public onUpdateCursor(Landroid/graphics/Rect;)V
    .registers 2
    .parameter "newCursor"

    #@0
    .prologue
    .line 1705
    return-void
.end method

.method public onUpdateExtractedText(ILandroid/view/inputmethod/ExtractedText;)V
    .registers 4
    .parameter "token"
    .parameter "text"

    #@0
    .prologue
    .line 1648
    iget v0, p0, Landroid/inputmethodservice/InputMethodService;->mExtractedToken:I

    #@2
    if-eq v0, p1, :cond_5

    #@4
    .line 1657
    :cond_4
    :goto_4
    return-void

    #@5
    .line 1651
    :cond_5
    if-eqz p2, :cond_4

    #@7
    .line 1652
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mExtractEditText:Landroid/inputmethodservice/ExtractEditText;

    #@9
    if-eqz v0, :cond_4

    #@b
    .line 1653
    iput-object p2, p0, Landroid/inputmethodservice/InputMethodService;->mExtractedText:Landroid/view/inputmethod/ExtractedText;

    #@d
    .line 1654
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mExtractEditText:Landroid/inputmethodservice/ExtractEditText;

    #@f
    invoke-virtual {v0, p2}, Landroid/inputmethodservice/ExtractEditText;->setExtractedText(Landroid/view/inputmethod/ExtractedText;)V

    #@12
    goto :goto_4
.end method

.method public onUpdateExtractingViews(Landroid/view/inputmethod/EditorInfo;)V
    .registers 6
    .parameter "ei"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 2248
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->isExtractViewShown()Z

    #@5
    move-result v2

    #@6
    if-nez v2, :cond_9

    #@8
    .line 2275
    :cond_8
    :goto_8
    return-void

    #@9
    .line 2252
    :cond_9
    iget-object v2, p0, Landroid/inputmethodservice/InputMethodService;->mExtractAccessories:Landroid/view/ViewGroup;

    #@b
    if-eqz v2, :cond_8

    #@d
    .line 2255
    iget-object v2, p1, Landroid/view/inputmethod/EditorInfo;->actionLabel:Ljava/lang/CharSequence;

    #@f
    if-nez v2, :cond_22

    #@11
    iget v2, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    #@13
    and-int/lit16 v2, v2, 0xff

    #@15
    if-eq v2, v0, :cond_40

    #@17
    iget v2, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    #@19
    const/high16 v3, 0x2000

    #@1b
    and-int/2addr v2, v3

    #@1c
    if-nez v2, :cond_40

    #@1e
    iget v2, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    #@20
    if-eqz v2, :cond_40

    #@22
    .line 2259
    .local v0, hasAction:Z
    :cond_22
    :goto_22
    if-eqz v0, :cond_4e

    #@24
    .line 2260
    iget-object v2, p0, Landroid/inputmethodservice/InputMethodService;->mExtractAccessories:Landroid/view/ViewGroup;

    #@26
    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    #@29
    .line 2261
    iget-object v1, p0, Landroid/inputmethodservice/InputMethodService;->mExtractAction:Landroid/widget/Button;

    #@2b
    if-eqz v1, :cond_8

    #@2d
    .line 2262
    iget-object v1, p1, Landroid/view/inputmethod/EditorInfo;->actionLabel:Ljava/lang/CharSequence;

    #@2f
    if-eqz v1, :cond_42

    #@31
    .line 2263
    iget-object v1, p0, Landroid/inputmethodservice/InputMethodService;->mExtractAction:Landroid/widget/Button;

    #@33
    iget-object v2, p1, Landroid/view/inputmethod/EditorInfo;->actionLabel:Ljava/lang/CharSequence;

    #@35
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    #@38
    .line 2267
    :goto_38
    iget-object v1, p0, Landroid/inputmethodservice/InputMethodService;->mExtractAction:Landroid/widget/Button;

    #@3a
    iget-object v2, p0, Landroid/inputmethodservice/InputMethodService;->mActionClickListener:Landroid/view/View$OnClickListener;

    #@3c
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@3f
    goto :goto_8

    #@40
    .end local v0           #hasAction:Z
    :cond_40
    move v0, v1

    #@41
    .line 2255
    goto :goto_22

    #@42
    .line 2265
    .restart local v0       #hasAction:Z
    :cond_42
    iget-object v1, p0, Landroid/inputmethodservice/InputMethodService;->mExtractAction:Landroid/widget/Button;

    #@44
    iget v2, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    #@46
    invoke-virtual {p0, v2}, Landroid/inputmethodservice/InputMethodService;->getTextForImeAction(I)Ljava/lang/CharSequence;

    #@49
    move-result-object v2

    #@4a
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    #@4d
    goto :goto_38

    #@4e
    .line 2270
    :cond_4e
    iget-object v1, p0, Landroid/inputmethodservice/InputMethodService;->mExtractAccessories:Landroid/view/ViewGroup;

    #@50
    const/16 v2, 0x8

    #@52
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    #@55
    .line 2271
    iget-object v1, p0, Landroid/inputmethodservice/InputMethodService;->mExtractAction:Landroid/widget/Button;

    #@57
    if-eqz v1, :cond_8

    #@59
    .line 2272
    iget-object v1, p0, Landroid/inputmethodservice/InputMethodService;->mExtractAction:Landroid/widget/Button;

    #@5b
    const/4 v2, 0x0

    #@5c
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@5f
    goto :goto_8
.end method

.method public onUpdateExtractingVisibility(Landroid/view/inputmethod/EditorInfo;)V
    .registers 4
    .parameter "ei"

    #@0
    .prologue
    .line 2222
    iget v0, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    #@2
    if-eqz v0, :cond_b

    #@4
    iget v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    #@6
    const/high16 v1, 0x1000

    #@8
    and-int/2addr v0, v1

    #@9
    if-eqz v0, :cond_10

    #@b
    .line 2225
    :cond_b
    const/4 v0, 0x0

    #@c
    invoke-virtual {p0, v0}, Landroid/inputmethodservice/InputMethodService;->setExtractViewShown(Z)V

    #@f
    .line 2230
    :goto_f
    return-void

    #@10
    .line 2229
    :cond_10
    const/4 v0, 0x1

    #@11
    invoke-virtual {p0, v0}, Landroid/inputmethodservice/InputMethodService;->setExtractViewShown(Z)V

    #@14
    goto :goto_f
.end method

.method public onUpdateSelection(IIIIII)V
    .registers 11
    .parameter "oldSelStart"
    .parameter "oldSelEnd"
    .parameter "newSelStart"
    .parameter "newSelEnd"
    .parameter "candidatesStart"
    .parameter "candidatesEnd"

    #@0
    .prologue
    .line 1671
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mExtractEditText:Landroid/inputmethodservice/ExtractEditText;

    #@2
    .line 1672
    .local v0, eet:Landroid/inputmethodservice/ExtractEditText;
    if-eqz v0, :cond_2b

    #@4
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->isFullscreenMode()Z

    #@7
    move-result v3

    #@8
    if-eqz v3, :cond_2b

    #@a
    iget-object v3, p0, Landroid/inputmethodservice/InputMethodService;->mExtractedText:Landroid/view/inputmethod/ExtractedText;

    #@c
    if-eqz v3, :cond_2b

    #@e
    .line 1673
    iget-object v3, p0, Landroid/inputmethodservice/InputMethodService;->mExtractedText:Landroid/view/inputmethod/ExtractedText;

    #@10
    iget v2, v3, Landroid/view/inputmethod/ExtractedText;->startOffset:I

    #@12
    .line 1674
    .local v2, off:I
    invoke-virtual {v0}, Landroid/inputmethodservice/ExtractEditText;->startInternalChanges()V

    #@15
    .line 1675
    sub-int/2addr p3, v2

    #@16
    .line 1676
    sub-int/2addr p4, v2

    #@17
    .line 1677
    invoke-virtual {v0}, Landroid/inputmethodservice/ExtractEditText;->getText()Landroid/text/Editable;

    #@1a
    move-result-object v3

    #@1b
    invoke-interface {v3}, Landroid/text/Editable;->length()I

    #@1e
    move-result v1

    #@1f
    .line 1678
    .local v1, len:I
    if-gez p3, :cond_2c

    #@21
    const/4 p3, 0x0

    #@22
    .line 1680
    :cond_22
    :goto_22
    if-gez p4, :cond_30

    #@24
    const/4 p4, 0x0

    #@25
    .line 1682
    :cond_25
    :goto_25
    invoke-virtual {v0, p3, p4}, Landroid/inputmethodservice/ExtractEditText;->setSelection(II)V

    #@28
    .line 1683
    invoke-virtual {v0}, Landroid/inputmethodservice/ExtractEditText;->finishInternalChanges()V

    #@2b
    .line 1685
    .end local v1           #len:I
    .end local v2           #off:I
    :cond_2b
    return-void

    #@2c
    .line 1679
    .restart local v1       #len:I
    .restart local v2       #off:I
    :cond_2c
    if-le p3, v1, :cond_22

    #@2e
    move p3, v1

    #@2f
    goto :goto_22

    #@30
    .line 1681
    :cond_30
    if-le p4, v1, :cond_25

    #@32
    move p4, v1

    #@33
    goto :goto_25
.end method

.method public onViewClicked(Z)V
    .registers 2
    .parameter "focusChanged"

    #@0
    .prologue
    .line 1696
    return-void
.end method

.method public onWindowHidden()V
    .registers 1

    #@0
    .prologue
    .line 1527
    return-void
.end method

.method public onWindowShown()V
    .registers 1

    #@0
    .prologue
    .line 1519
    return-void
.end method

.method reportExtractedMovement(II)V
    .registers 5
    .parameter "keyCode"
    .parameter "count"

    #@0
    .prologue
    .line 1901
    const/4 v0, 0x0

    #@1
    .local v0, dx:I
    const/4 v1, 0x0

    #@2
    .line 1902
    .local v1, dy:I
    packed-switch p1, :pswitch_data_12

    #@5
    .line 1916
    :goto_5
    invoke-virtual {p0, v0, v1}, Landroid/inputmethodservice/InputMethodService;->onExtractedCursorMovement(II)V

    #@8
    .line 1917
    return-void

    #@9
    .line 1904
    :pswitch_9
    neg-int v0, p2

    #@a
    .line 1905
    goto :goto_5

    #@b
    .line 1907
    :pswitch_b
    move v0, p2

    #@c
    .line 1908
    goto :goto_5

    #@d
    .line 1910
    :pswitch_d
    neg-int v1, p2

    #@e
    .line 1911
    goto :goto_5

    #@f
    .line 1913
    :pswitch_f
    move v1, p2

    #@10
    goto :goto_5

    #@11
    .line 1902
    nop

    #@12
    :pswitch_data_12
    .packed-switch 0x13
        :pswitch_d
        :pswitch_f
        :pswitch_9
        :pswitch_b
    .end packed-switch
.end method

.method public requestHideSelf(I)V
    .registers 4
    .parameter "flags"

    #@0
    .prologue
    .line 1716
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mImm:Landroid/view/inputmethod/InputMethodManager;

    #@2
    iget-object v1, p0, Landroid/inputmethodservice/InputMethodService;->mToken:Landroid/os/IBinder;

    #@4
    invoke-virtual {v0, v1, p1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromInputMethod(Landroid/os/IBinder;I)V

    #@7
    .line 1717
    return-void
.end method

.method public sendDefaultEditorAction(Z)Z
    .registers 7
    .parameter "fromEnterKey"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 2024
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;

    #@4
    move-result-object v0

    #@5
    .line 2025
    .local v0, ei:Landroid/view/inputmethod/EditorInfo;
    if-eqz v0, :cond_24

    #@7
    if-eqz p1, :cond_10

    #@9
    iget v3, v0, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    #@b
    const/high16 v4, 0x4000

    #@d
    and-int/2addr v3, v4

    #@e
    if-nez v3, :cond_24

    #@10
    :cond_10
    iget v3, v0, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    #@12
    and-int/lit16 v3, v3, 0xff

    #@14
    if-eq v3, v2, :cond_24

    #@16
    .line 2033
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    #@19
    move-result-object v1

    #@1a
    .line 2034
    .local v1, ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v1, :cond_23

    #@1c
    .line 2035
    iget v3, v0, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    #@1e
    and-int/lit16 v3, v3, 0xff

    #@20
    invoke-interface {v1, v3}, Landroid/view/inputmethod/InputConnection;->performEditorAction(I)Z

    #@23
    .line 2040
    .end local v1           #ic:Landroid/view/inputmethod/InputConnection;
    :cond_23
    :goto_23
    return v2

    #@24
    :cond_24
    const/4 v2, 0x0

    #@25
    goto :goto_23
.end method

.method public sendDownUpKeyEvents(I)V
    .registers 18
    .parameter "keyEventCode"

    #@0
    .prologue
    .line 1996
    invoke-virtual/range {p0 .. p0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    #@3
    move-result-object v15

    #@4
    .line 1997
    .local v15, ic:Landroid/view/inputmethod/InputConnection;
    if-nez v15, :cond_7

    #@6
    .line 2005
    :goto_6
    return-void

    #@7
    .line 1998
    :cond_7
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@a
    move-result-wide v1

    #@b
    .line 1999
    .local v1, eventTime:J
    new-instance v0, Landroid/view/KeyEvent;

    #@d
    const/4 v5, 0x0

    #@e
    const/4 v7, 0x0

    #@f
    const/4 v8, 0x0

    #@10
    const/4 v9, -0x1

    #@11
    const/4 v10, 0x0

    #@12
    const/4 v11, 0x6

    #@13
    move-wide v3, v1

    #@14
    move/from16 v6, p1

    #@16
    invoke-direct/range {v0 .. v11}, Landroid/view/KeyEvent;-><init>(JJIIIIIII)V

    #@19
    invoke-interface {v15, v0}, Landroid/view/inputmethod/InputConnection;->sendKeyEvent(Landroid/view/KeyEvent;)Z

    #@1c
    .line 2002
    new-instance v3, Landroid/view/KeyEvent;

    #@1e
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@21
    move-result-wide v4

    #@22
    const/4 v8, 0x1

    #@23
    const/4 v10, 0x0

    #@24
    const/4 v11, 0x0

    #@25
    const/4 v12, -0x1

    #@26
    const/4 v13, 0x0

    #@27
    const/4 v14, 0x6

    #@28
    move-wide v6, v1

    #@29
    move/from16 v9, p1

    #@2b
    invoke-direct/range {v3 .. v14}, Landroid/view/KeyEvent;-><init>(JJIIIIIII)V

    #@2e
    invoke-interface {v15, v3}, Landroid/view/inputmethod/InputConnection;->sendKeyEvent(Landroid/view/KeyEvent;)Z

    #@31
    goto :goto_6
.end method

.method public sendKeyChar(C)V
    .registers 5
    .parameter "charCode"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 2058
    packed-switch p1, :pswitch_data_2e

    #@4
    .line 2066
    const/16 v1, 0x30

    #@6
    if-lt p1, v1, :cond_20

    #@8
    const/16 v1, 0x39

    #@a
    if-gt p1, v1, :cond_20

    #@c
    .line 2067
    add-int/lit8 v1, p1, -0x30

    #@e
    add-int/lit8 v1, v1, 0x7

    #@10
    invoke-virtual {p0, v1}, Landroid/inputmethodservice/InputMethodService;->sendDownUpKeyEvents(I)V

    #@13
    .line 2076
    :cond_13
    :goto_13
    return-void

    #@14
    .line 2060
    :pswitch_14
    invoke-virtual {p0, v2}, Landroid/inputmethodservice/InputMethodService;->sendDefaultEditorAction(Z)Z

    #@17
    move-result v1

    #@18
    if-nez v1, :cond_13

    #@1a
    .line 2061
    const/16 v1, 0x42

    #@1c
    invoke-virtual {p0, v1}, Landroid/inputmethodservice/InputMethodService;->sendDownUpKeyEvents(I)V

    #@1f
    goto :goto_13

    #@20
    .line 2069
    :cond_20
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    #@23
    move-result-object v0

    #@24
    .line 2070
    .local v0, ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v0, :cond_13

    #@26
    .line 2071
    invoke-static {p1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    invoke-interface {v0, v1, v2}, Landroid/view/inputmethod/InputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    #@2d
    goto :goto_13

    #@2e
    .line 2058
    :pswitch_data_2e
    .packed-switch 0xa
        :pswitch_14
    .end packed-switch
.end method

.method public setBackDisposition(I)V
    .registers 2
    .parameter "disposition"

    #@0
    .prologue
    .line 824
    iput p1, p0, Landroid/inputmethodservice/InputMethodService;->mBackDisposition:I

    #@2
    .line 825
    return-void
.end method

.method public setCandidatesView(Landroid/view/View;)V
    .registers 6
    .parameter "view"

    #@0
    .prologue
    .line 1221
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mCandidatesFrame:Landroid/widget/FrameLayout;

    #@2
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    #@5
    .line 1222
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mCandidatesFrame:Landroid/widget/FrameLayout;

    #@7
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    #@9
    const/4 v2, -0x1

    #@a
    const/4 v3, -0x2

    #@b
    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    #@e
    invoke-virtual {v0, p1, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@11
    .line 1225
    return-void
.end method

.method public setCandidatesViewShown(Z)V
    .registers 3
    .parameter "shown"

    #@0
    .prologue
    .line 1134
    invoke-virtual {p0, p1}, Landroid/inputmethodservice/InputMethodService;->updateCandidatesVisibility(Z)V

    #@3
    .line 1135
    iget-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mShowInputRequested:Z

    #@5
    if-nez v0, :cond_11

    #@7
    iget-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mWindowVisible:Z

    #@9
    if-eq v0, p1, :cond_11

    #@b
    .line 1139
    if-eqz p1, :cond_12

    #@d
    .line 1140
    const/4 v0, 0x0

    #@e
    invoke-virtual {p0, v0}, Landroid/inputmethodservice/InputMethodService;->showWindow(Z)V

    #@11
    .line 1145
    :cond_11
    :goto_11
    return-void

    #@12
    .line 1142
    :cond_12
    invoke-direct {p0}, Landroid/inputmethodservice/InputMethodService;->doHideWindow()V

    #@15
    goto :goto_11
.end method

.method public setExtractView(Landroid/view/View;)V
    .registers 6
    .parameter "view"

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1191
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mExtractFrame:Landroid/widget/FrameLayout;

    #@4
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    #@7
    .line 1192
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mExtractFrame:Landroid/widget/FrameLayout;

    #@9
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    #@b
    invoke-direct {v1, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    #@e
    invoke-virtual {v0, p1, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@11
    .line 1195
    iput-object p1, p0, Landroid/inputmethodservice/InputMethodService;->mExtractView:Landroid/view/View;

    #@13
    .line 1196
    if-eqz p1, :cond_44

    #@15
    .line 1197
    const v0, 0x1020025

    #@18
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@1b
    move-result-object v0

    #@1c
    check-cast v0, Landroid/inputmethodservice/ExtractEditText;

    #@1e
    iput-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mExtractEditText:Landroid/inputmethodservice/ExtractEditText;

    #@20
    .line 1199
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mExtractEditText:Landroid/inputmethodservice/ExtractEditText;

    #@22
    invoke-virtual {v0, p0}, Landroid/inputmethodservice/ExtractEditText;->setIME(Landroid/inputmethodservice/InputMethodService;)V

    #@25
    .line 1200
    const v0, 0x10202aa

    #@28
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@2b
    move-result-object v0

    #@2c
    check-cast v0, Landroid/widget/Button;

    #@2e
    iput-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mExtractAction:Landroid/widget/Button;

    #@30
    .line 1202
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mExtractAction:Landroid/widget/Button;

    #@32
    if-eqz v0, :cond_3f

    #@34
    .line 1203
    const v0, 0x10202a9

    #@37
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@3a
    move-result-object v0

    #@3b
    check-cast v0, Landroid/view/ViewGroup;

    #@3d
    iput-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mExtractAccessories:Landroid/view/ViewGroup;

    #@3f
    .line 1206
    :cond_3f
    const/4 v0, 0x0

    #@40
    invoke-virtual {p0, v0}, Landroid/inputmethodservice/InputMethodService;->startExtractingText(Z)V

    #@43
    .line 1212
    :goto_43
    return-void

    #@44
    .line 1208
    :cond_44
    iput-object v2, p0, Landroid/inputmethodservice/InputMethodService;->mExtractEditText:Landroid/inputmethodservice/ExtractEditText;

    #@46
    .line 1209
    iput-object v2, p0, Landroid/inputmethodservice/InputMethodService;->mExtractAccessories:Landroid/view/ViewGroup;

    #@48
    .line 1210
    iput-object v2, p0, Landroid/inputmethodservice/InputMethodService;->mExtractAction:Landroid/widget/Button;

    #@4a
    goto :goto_43
.end method

.method public setExtractViewShown(Z)V
    .registers 3
    .parameter "shown"

    #@0
    .prologue
    .line 996
    iget-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mExtractViewHidden:Z

    #@2
    if-ne v0, p1, :cond_c

    #@4
    .line 997
    if-nez p1, :cond_d

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    iput-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mExtractViewHidden:Z

    #@9
    .line 998
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->updateExtractFrameVisibility()V

    #@c
    .line 1000
    :cond_c
    return-void

    #@d
    .line 997
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_7
.end method

.method public setInputView(Landroid/view/View;)V
    .registers 6
    .parameter "view"

    #@0
    .prologue
    .line 1234
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mInputFrame:Landroid/widget/FrameLayout;

    #@2
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    #@5
    .line 1235
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mInputFrame:Landroid/widget/FrameLayout;

    #@7
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    #@9
    const/4 v2, -0x1

    #@a
    const/4 v3, -0x2

    #@b
    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    #@e
    invoke-virtual {v0, p1, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@11
    .line 1238
    iput-object p1, p0, Landroid/inputmethodservice/InputMethodService;->mInputView:Landroid/view/View;

    #@13
    .line 1239
    return-void
.end method

.method public setTheme(I)V
    .registers 4
    .parameter "theme"

    #@0
    .prologue
    .line 628
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mWindow:Landroid/inputmethodservice/SoftInputWindow;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 629
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "Must be called before onCreate()"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 631
    :cond_c
    iput p1, p0, Landroid/inputmethodservice/InputMethodService;->mTheme:I

    #@e
    .line 632
    return-void
.end method

.method public showStatusIcon(I)V
    .registers 5
    .parameter "iconResId"

    #@0
    .prologue
    .line 1170
    iput p1, p0, Landroid/inputmethodservice/InputMethodService;->mStatusIcon:I

    #@2
    .line 1171
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mImm:Landroid/view/inputmethod/InputMethodManager;

    #@4
    iget-object v1, p0, Landroid/inputmethodservice/InputMethodService;->mToken:Landroid/os/IBinder;

    #@6
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getPackageName()Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    invoke-virtual {v0, v1, v2, p1}, Landroid/view/inputmethod/InputMethodManager;->showStatusIcon(Landroid/os/IBinder;Ljava/lang/String;I)V

    #@d
    .line 1172
    return-void
.end method

.method public showWindow(Z)V
    .registers 5
    .parameter "showInput"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 1413
    iget-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mInShowWindow:Z

    #@4
    if-eqz v0, :cond_e

    #@6
    .line 1414
    const-string v0, "InputMethodService"

    #@8
    const-string v1, "Re-entrance in to showWindow"

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 1426
    :goto_d
    return-void

    #@e
    .line 1419
    :cond_e
    :try_start_e
    iget-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mWindowVisible:Z

    #@10
    iput-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mWindowWasVisible:Z

    #@12
    .line 1420
    const/4 v0, 0x1

    #@13
    iput-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mInShowWindow:Z

    #@15
    .line 1421
    invoke-virtual {p0, p1}, Landroid/inputmethodservice/InputMethodService;->showWindowInner(Z)V
    :try_end_18
    .catchall {:try_start_e .. :try_end_18} :catchall_1d

    #@18
    .line 1423
    iput-boolean v1, p0, Landroid/inputmethodservice/InputMethodService;->mWindowWasVisible:Z

    #@1a
    .line 1424
    iput-boolean v2, p0, Landroid/inputmethodservice/InputMethodService;->mInShowWindow:Z

    #@1c
    goto :goto_d

    #@1d
    .line 1423
    :catchall_1d
    move-exception v0

    #@1e
    iput-boolean v1, p0, Landroid/inputmethodservice/InputMethodService;->mWindowWasVisible:Z

    #@20
    .line 1424
    iput-boolean v2, p0, Landroid/inputmethodservice/InputMethodService;->mInShowWindow:Z

    #@22
    .line 1423
    throw v0
.end method

.method showWindowInner(Z)V
    .registers 9
    .parameter "showInput"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 1429
    const/4 v0, 0x0

    #@3
    .line 1430
    .local v0, doShowInput:Z
    iget-boolean v2, p0, Landroid/inputmethodservice/InputMethodService;->mWindowVisible:Z

    #@5
    .line 1431
    .local v2, wasVisible:Z
    iput-boolean v6, p0, Landroid/inputmethodservice/InputMethodService;->mWindowVisible:Z

    #@7
    .line 1432
    iget-boolean v3, p0, Landroid/inputmethodservice/InputMethodService;->mShowInputRequested:Z

    #@9
    if-nez v3, :cond_5d

    #@b
    .line 1433
    iget-boolean v3, p0, Landroid/inputmethodservice/InputMethodService;->mInputStarted:Z

    #@d
    if-eqz v3, :cond_14

    #@f
    .line 1434
    if-eqz p1, :cond_14

    #@11
    .line 1435
    const/4 v0, 0x1

    #@12
    .line 1436
    iput-boolean v6, p0, Landroid/inputmethodservice/InputMethodService;->mShowInputRequested:Z

    #@14
    .line 1444
    :cond_14
    :goto_14
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->initialize()V

    #@17
    .line 1445
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->updateFullscreenMode()V

    #@1a
    .line 1446
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->updateInputViewShown()V

    #@1d
    .line 1448
    iget-boolean v3, p0, Landroid/inputmethodservice/InputMethodService;->mWindowAdded:Z

    #@1f
    if-eqz v3, :cond_25

    #@21
    iget-boolean v3, p0, Landroid/inputmethodservice/InputMethodService;->mWindowCreated:Z

    #@23
    if-nez v3, :cond_35

    #@25
    .line 1449
    :cond_25
    iput-boolean v6, p0, Landroid/inputmethodservice/InputMethodService;->mWindowAdded:Z

    #@27
    .line 1450
    iput-boolean v6, p0, Landroid/inputmethodservice/InputMethodService;->mWindowCreated:Z

    #@29
    .line 1451
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->initialize()V

    #@2c
    .line 1453
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->onCreateCandidatesView()Landroid/view/View;

    #@2f
    move-result-object v1

    #@30
    .line 1455
    .local v1, v:Landroid/view/View;
    if-eqz v1, :cond_35

    #@32
    .line 1456
    invoke-virtual {p0, v1}, Landroid/inputmethodservice/InputMethodService;->setCandidatesView(Landroid/view/View;)V

    #@35
    .line 1459
    .end local v1           #v:Landroid/view/View;
    :cond_35
    iget-boolean v3, p0, Landroid/inputmethodservice/InputMethodService;->mShowInputRequested:Z

    #@37
    if-eqz v3, :cond_5f

    #@39
    .line 1460
    iget-boolean v3, p0, Landroid/inputmethodservice/InputMethodService;->mInputViewStarted:Z

    #@3b
    if-nez v3, :cond_44

    #@3d
    .line 1462
    iput-boolean v6, p0, Landroid/inputmethodservice/InputMethodService;->mInputViewStarted:Z

    #@3f
    .line 1463
    iget-object v3, p0, Landroid/inputmethodservice/InputMethodService;->mInputEditorInfo:Landroid/view/inputmethod/EditorInfo;

    #@41
    invoke-virtual {p0, v3, v4}, Landroid/inputmethodservice/InputMethodService;->onStartInputView(Landroid/view/inputmethod/EditorInfo;Z)V

    #@44
    .line 1471
    :cond_44
    :goto_44
    if-eqz v0, :cond_49

    #@46
    .line 1472
    invoke-virtual {p0, v4}, Landroid/inputmethodservice/InputMethodService;->startExtractingText(Z)V

    #@49
    .line 1475
    :cond_49
    if-nez v2, :cond_5c

    #@4b
    .line 1477
    iget-object v3, p0, Landroid/inputmethodservice/InputMethodService;->mImm:Landroid/view/inputmethod/InputMethodManager;

    #@4d
    iget-object v4, p0, Landroid/inputmethodservice/InputMethodService;->mToken:Landroid/os/IBinder;

    #@4f
    iget v5, p0, Landroid/inputmethodservice/InputMethodService;->mBackDisposition:I

    #@51
    invoke-virtual {v3, v4, v6, v5}, Landroid/view/inputmethod/InputMethodManager;->setImeWindowStatus(Landroid/os/IBinder;II)V

    #@54
    .line 1478
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->onWindowShown()V

    #@57
    .line 1479
    iget-object v3, p0, Landroid/inputmethodservice/InputMethodService;->mWindow:Landroid/inputmethodservice/SoftInputWindow;

    #@59
    invoke-virtual {v3}, Landroid/inputmethodservice/SoftInputWindow;->show()V

    #@5c
    .line 1481
    :cond_5c
    return-void

    #@5d
    .line 1440
    :cond_5d
    const/4 p1, 0x1

    #@5e
    goto :goto_14

    #@5f
    .line 1465
    :cond_5f
    iget-boolean v3, p0, Landroid/inputmethodservice/InputMethodService;->mCandidatesViewStarted:Z

    #@61
    if-nez v3, :cond_44

    #@63
    .line 1467
    iput-boolean v6, p0, Landroid/inputmethodservice/InputMethodService;->mCandidatesViewStarted:Z

    #@65
    .line 1468
    iget-object v3, p0, Landroid/inputmethodservice/InputMethodService;->mInputEditorInfo:Landroid/view/inputmethod/EditorInfo;

    #@67
    invoke-virtual {p0, v3, v4}, Landroid/inputmethodservice/InputMethodService;->onStartCandidatesView(Landroid/view/inputmethod/EditorInfo;Z)V

    #@6a
    goto :goto_44
.end method

.method startExtractingText(Z)V
    .registers 12
    .parameter "inputChanged"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    .line 2290
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mExtractEditText:Landroid/inputmethodservice/ExtractEditText;

    #@3
    .line 2291
    .local v0, eet:Landroid/inputmethodservice/ExtractEditText;
    if-eqz v0, :cond_bf

    #@5
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputStarted()Z

    #@8
    move-result v6

    #@9
    if-eqz v6, :cond_bf

    #@b
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->isFullscreenMode()Z

    #@e
    move-result v6

    #@f
    if-eqz v6, :cond_bf

    #@11
    .line 2293
    iget v6, p0, Landroid/inputmethodservice/InputMethodService;->mExtractedToken:I

    #@13
    add-int/lit8 v6, v6, 0x1

    #@15
    iput v6, p0, Landroid/inputmethodservice/InputMethodService;->mExtractedToken:I

    #@17
    .line 2294
    new-instance v4, Landroid/view/inputmethod/ExtractedTextRequest;

    #@19
    invoke-direct {v4}, Landroid/view/inputmethod/ExtractedTextRequest;-><init>()V

    #@1c
    .line 2295
    .local v4, req:Landroid/view/inputmethod/ExtractedTextRequest;
    iget v6, p0, Landroid/inputmethodservice/InputMethodService;->mExtractedToken:I

    #@1e
    iput v6, v4, Landroid/view/inputmethod/ExtractedTextRequest;->token:I

    #@20
    .line 2296
    iput v9, v4, Landroid/view/inputmethod/ExtractedTextRequest;->flags:I

    #@22
    .line 2297
    const/16 v6, 0xa

    #@24
    iput v6, v4, Landroid/view/inputmethod/ExtractedTextRequest;->hintMaxLines:I

    #@26
    .line 2298
    const/16 v6, 0x2710

    #@28
    iput v6, v4, Landroid/view/inputmethod/ExtractedTextRequest;->hintMaxChars:I

    #@2a
    .line 2299
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    #@2d
    move-result-object v2

    #@2e
    .line 2300
    .local v2, ic:Landroid/view/inputmethod/InputConnection;
    if-nez v2, :cond_c0

    #@30
    const/4 v6, 0x0

    #@31
    :goto_31
    iput-object v6, p0, Landroid/inputmethodservice/InputMethodService;->mExtractedText:Landroid/view/inputmethod/ExtractedText;

    #@33
    .line 2302
    iget-object v6, p0, Landroid/inputmethodservice/InputMethodService;->mExtractedText:Landroid/view/inputmethod/ExtractedText;

    #@35
    if-eqz v6, :cond_39

    #@37
    if-nez v2, :cond_5d

    #@39
    .line 2303
    :cond_39
    const-string v6, "InputMethodService"

    #@3b
    new-instance v7, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v8, "Unexpected null in startExtractingText : mExtractedText = "

    #@42
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v7

    #@46
    iget-object v8, p0, Landroid/inputmethodservice/InputMethodService;->mExtractedText:Landroid/view/inputmethod/ExtractedText;

    #@48
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v7

    #@4c
    const-string v8, ", input connection = "

    #@4e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v7

    #@52
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v7

    #@56
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v7

    #@5a
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    .line 2306
    :cond_5d
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;

    #@60
    move-result-object v1

    #@61
    .line 2309
    .local v1, ei:Landroid/view/inputmethod/EditorInfo;
    :try_start_61
    invoke-virtual {v0}, Landroid/inputmethodservice/ExtractEditText;->startInternalChanges()V

    #@64
    .line 2310
    invoke-virtual {p0, v1}, Landroid/inputmethodservice/InputMethodService;->onUpdateExtractingVisibility(Landroid/view/inputmethod/EditorInfo;)V

    #@67
    .line 2311
    invoke-virtual {p0, v1}, Landroid/inputmethodservice/InputMethodService;->onUpdateExtractingViews(Landroid/view/inputmethod/EditorInfo;)V

    #@6a
    .line 2312
    iget v3, v1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    #@6c
    .line 2313
    .local v3, inputType:I
    and-int/lit8 v6, v3, 0xf

    #@6e
    if-ne v6, v9, :cond_78

    #@70
    .line 2315
    const/high16 v6, 0x4

    #@72
    and-int/2addr v6, v3

    #@73
    if-eqz v6, :cond_78

    #@75
    .line 2316
    const/high16 v6, 0x2

    #@77
    or-int/2addr v3, v6

    #@78
    .line 2319
    :cond_78
    invoke-virtual {v0, v3}, Landroid/inputmethodservice/ExtractEditText;->setInputType(I)V

    #@7b
    .line 2320
    iget-object v6, v1, Landroid/view/inputmethod/EditorInfo;->hintText:Ljava/lang/CharSequence;

    #@7d
    invoke-virtual {v0, v6}, Landroid/inputmethodservice/ExtractEditText;->setHint(Ljava/lang/CharSequence;)V

    #@80
    .line 2321
    iget-object v6, p0, Landroid/inputmethodservice/InputMethodService;->mExtractedText:Landroid/view/inputmethod/ExtractedText;

    #@82
    if-eqz v6, :cond_d7

    #@84
    .line 2322
    const/4 v6, 0x1

    #@85
    invoke-virtual {v0, v6}, Landroid/inputmethodservice/ExtractEditText;->setEnabled(Z)V

    #@88
    .line 2323
    iget-object v6, p0, Landroid/inputmethodservice/InputMethodService;->mExtractedText:Landroid/view/inputmethod/ExtractedText;

    #@8a
    invoke-virtual {v0, v6}, Landroid/inputmethodservice/ExtractEditText;->setExtractedText(Landroid/view/inputmethod/ExtractedText;)V

    #@8d
    .line 2324
    sget-boolean v6, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@8f
    if-eqz v6, :cond_b7

    #@91
    instance-of v6, v0, Landroid/widget/TextView;

    #@93
    if-eqz v6, :cond_b7

    #@95
    .line 2326
    move-object v5, v0

    #@96
    .line 2327
    .local v5, view:Landroid/widget/TextView;
    iget-object v6, p0, Landroid/inputmethodservice/InputMethodService;->mExtractedText:Landroid/view/inputmethod/ExtractedText;

    #@98
    iget v6, v6, Landroid/view/inputmethod/ExtractedText;->flags:I

    #@9a
    and-int/lit8 v6, v6, 0x4

    #@9c
    if-eqz v6, :cond_c6

    #@9e
    .line 2328
    const/4 v6, 0x0

    #@9f
    iput-boolean v6, v5, Landroid/widget/TextView;->mCanCreateBubblePopup:Z

    #@a1
    .line 2332
    :goto_a1
    iget-object v6, p0, Landroid/inputmethodservice/InputMethodService;->mExtractedText:Landroid/view/inputmethod/ExtractedText;

    #@a3
    iget v6, v6, Landroid/view/inputmethod/ExtractedText;->flags:I

    #@a5
    and-int/lit8 v6, v6, 0x8

    #@a7
    if-eqz v6, :cond_cf

    #@a9
    .line 2333
    const/4 v6, 0x0

    #@aa
    iput-boolean v6, v5, Landroid/widget/TextView;->mCanPasteBubblePopup:Z

    #@ac
    .line 2337
    :goto_ac
    iget-object v6, p0, Landroid/inputmethodservice/InputMethodService;->mExtractedText:Landroid/view/inputmethod/ExtractedText;

    #@ae
    iget v6, v6, Landroid/view/inputmethod/ExtractedText;->flags:I

    #@b0
    and-int/lit8 v6, v6, 0x10

    #@b2
    if-eqz v6, :cond_d3

    #@b4
    .line 2338
    const/4 v6, 0x0

    #@b5
    iput-boolean v6, v5, Landroid/widget/TextView;->mCanCutBubblePopup:Z
    :try_end_b7
    .catchall {:try_start_61 .. :try_end_b7} :catchall_ca

    #@b7
    .line 2348
    .end local v5           #view:Landroid/widget/TextView;
    :cond_b7
    :goto_b7
    invoke-virtual {v0}, Landroid/inputmethodservice/ExtractEditText;->finishInternalChanges()V

    #@ba
    .line 2351
    if-eqz p1, :cond_bf

    #@bc
    .line 2352
    invoke-virtual {p0, v1}, Landroid/inputmethodservice/InputMethodService;->onExtractingInputChanged(Landroid/view/inputmethod/EditorInfo;)V

    #@bf
    .line 2355
    .end local v1           #ei:Landroid/view/inputmethod/EditorInfo;
    .end local v2           #ic:Landroid/view/inputmethod/InputConnection;
    .end local v3           #inputType:I
    .end local v4           #req:Landroid/view/inputmethod/ExtractedTextRequest;
    :cond_bf
    return-void

    #@c0
    .line 2300
    .restart local v2       #ic:Landroid/view/inputmethod/InputConnection;
    .restart local v4       #req:Landroid/view/inputmethod/ExtractedTextRequest;
    :cond_c0
    invoke-interface {v2, v4, v9}, Landroid/view/inputmethod/InputConnection;->getExtractedText(Landroid/view/inputmethod/ExtractedTextRequest;I)Landroid/view/inputmethod/ExtractedText;

    #@c3
    move-result-object v6

    #@c4
    goto/16 :goto_31

    #@c6
    .line 2330
    .restart local v1       #ei:Landroid/view/inputmethod/EditorInfo;
    .restart local v3       #inputType:I
    .restart local v5       #view:Landroid/widget/TextView;
    :cond_c6
    const/4 v6, 0x1

    #@c7
    :try_start_c7
    iput-boolean v6, v5, Landroid/widget/TextView;->mCanCreateBubblePopup:Z
    :try_end_c9
    .catchall {:try_start_c7 .. :try_end_c9} :catchall_ca

    #@c9
    goto :goto_a1

    #@ca
    .line 2348
    .end local v3           #inputType:I
    .end local v5           #view:Landroid/widget/TextView;
    :catchall_ca
    move-exception v6

    #@cb
    invoke-virtual {v0}, Landroid/inputmethodservice/ExtractEditText;->finishInternalChanges()V

    #@ce
    throw v6

    #@cf
    .line 2335
    .restart local v3       #inputType:I
    .restart local v5       #view:Landroid/widget/TextView;
    :cond_cf
    const/4 v6, 0x1

    #@d0
    :try_start_d0
    iput-boolean v6, v5, Landroid/widget/TextView;->mCanPasteBubblePopup:Z

    #@d2
    goto :goto_ac

    #@d3
    .line 2340
    :cond_d3
    const/4 v6, 0x1

    #@d4
    iput-boolean v6, v5, Landroid/widget/TextView;->mCanCutBubblePopup:Z

    #@d6
    goto :goto_b7

    #@d7
    .line 2344
    .end local v5           #view:Landroid/widget/TextView;
    :cond_d7
    const/4 v6, 0x0

    #@d8
    invoke-virtual {v0, v6}, Landroid/inputmethodservice/ExtractEditText;->setEnabled(Z)V

    #@db
    .line 2345
    const-string v6, ""

    #@dd
    invoke-virtual {v0, v6}, Landroid/inputmethodservice/ExtractEditText;->setText(Ljava/lang/CharSequence;)V
    :try_end_e0
    .catchall {:try_start_d0 .. :try_end_e0} :catchall_ca

    #@e0
    goto :goto_b7
.end method

.method public switchInputMethod(Ljava/lang/String;)V
    .registers 4
    .parameter "id"

    #@0
    .prologue
    .line 1187
    iget-object v0, p0, Landroid/inputmethodservice/InputMethodService;->mImm:Landroid/view/inputmethod/InputMethodManager;

    #@2
    iget-object v1, p0, Landroid/inputmethodservice/InputMethodService;->mToken:Landroid/os/IBinder;

    #@4
    invoke-virtual {v0, v1, p1}, Landroid/view/inputmethod/InputMethodManager;->setInputMethod(Landroid/os/IBinder;Ljava/lang/String;)V

    #@7
    .line 1188
    return-void
.end method

.method updateCandidatesVisibility(Z)V
    .registers 4
    .parameter "shown"

    #@0
    .prologue
    .line 1148
    if-eqz p1, :cond_f

    #@2
    const/4 v0, 0x0

    #@3
    .line 1149
    .local v0, vis:I
    :goto_3
    iget v1, p0, Landroid/inputmethodservice/InputMethodService;->mCandidatesVisibility:I

    #@5
    if-eq v1, v0, :cond_e

    #@7
    .line 1150
    iget-object v1, p0, Landroid/inputmethodservice/InputMethodService;->mCandidatesFrame:Landroid/widget/FrameLayout;

    #@9
    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    #@c
    .line 1151
    iput v0, p0, Landroid/inputmethodservice/InputMethodService;->mCandidatesVisibility:I

    #@e
    .line 1153
    :cond_e
    return-void

    #@f
    .line 1148
    .end local v0           #vis:I
    :cond_f
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getCandidatesHiddenVisibility()I

    #@12
    move-result v0

    #@13
    goto :goto_3
.end method

.method updateExtractFrameVisibility()V
    .registers 7

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1018
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->isFullscreenMode()Z

    #@5
    move-result v3

    #@6
    if-eqz v3, :cond_41

    #@8
    .line 1019
    iget-boolean v3, p0, Landroid/inputmethodservice/InputMethodService;->mExtractViewHidden:Z

    #@a
    if-eqz v3, :cond_3f

    #@c
    const/4 v1, 0x4

    #@d
    .line 1021
    .local v1, vis:I
    :goto_d
    iget-object v3, p0, Landroid/inputmethodservice/InputMethodService;->mExtractFrame:Landroid/widget/FrameLayout;

    #@f
    invoke-virtual {v3, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    #@12
    .line 1026
    :goto_12
    iget v3, p0, Landroid/inputmethodservice/InputMethodService;->mCandidatesVisibility:I

    #@14
    if-nez v3, :cond_4a

    #@16
    move v3, v4

    #@17
    :goto_17
    invoke-virtual {p0, v3}, Landroid/inputmethodservice/InputMethodService;->updateCandidatesVisibility(Z)V

    #@1a
    .line 1027
    iget-boolean v3, p0, Landroid/inputmethodservice/InputMethodService;->mWindowWasVisible:Z

    #@1c
    if-eqz v3, :cond_39

    #@1e
    iget-object v3, p0, Landroid/inputmethodservice/InputMethodService;->mFullscreenArea:Landroid/view/ViewGroup;

    #@20
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getVisibility()I

    #@23
    move-result v3

    #@24
    if-eq v3, v1, :cond_39

    #@26
    .line 1028
    iget-object v3, p0, Landroid/inputmethodservice/InputMethodService;->mThemeAttrs:Landroid/content/res/TypedArray;

    #@28
    if-nez v1, :cond_4c

    #@2a
    :goto_2a
    invoke-virtual {v3, v4, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@2d
    move-result v0

    #@2e
    .line 1032
    .local v0, animRes:I
    if-eqz v0, :cond_39

    #@30
    .line 1033
    iget-object v2, p0, Landroid/inputmethodservice/InputMethodService;->mFullscreenArea:Landroid/view/ViewGroup;

    #@32
    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    #@39
    .line 1037
    .end local v0           #animRes:I
    :cond_39
    iget-object v2, p0, Landroid/inputmethodservice/InputMethodService;->mFullscreenArea:Landroid/view/ViewGroup;

    #@3b
    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    #@3e
    .line 1038
    return-void

    #@3f
    .end local v1           #vis:I
    :cond_3f
    move v1, v2

    #@40
    .line 1019
    goto :goto_d

    #@41
    .line 1023
    :cond_41
    const/4 v1, 0x0

    #@42
    .line 1024
    .restart local v1       #vis:I
    iget-object v3, p0, Landroid/inputmethodservice/InputMethodService;->mExtractFrame:Landroid/widget/FrameLayout;

    #@44
    const/16 v5, 0x8

    #@46
    invoke-virtual {v3, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    #@49
    goto :goto_12

    #@4a
    :cond_4a
    move v3, v2

    #@4b
    .line 1026
    goto :goto_17

    #@4c
    .line 1028
    :cond_4c
    const/4 v4, 0x2

    #@4d
    goto :goto_2a
.end method

.method public updateFullscreenMode()V
    .registers 10

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 889
    iget-boolean v5, p0, Landroid/inputmethodservice/InputMethodService;->mShowInputRequested:Z

    #@4
    if-eqz v5, :cond_7e

    #@6
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->onEvaluateFullscreenMode()Z

    #@9
    move-result v5

    #@a
    if-eqz v5, :cond_7e

    #@c
    move v2, v6

    #@d
    .line 890
    .local v2, isFullscreen:Z
    :goto_d
    iget-boolean v5, p0, Landroid/inputmethodservice/InputMethodService;->mLastShowInputRequested:Z

    #@f
    iget-boolean v8, p0, Landroid/inputmethodservice/InputMethodService;->mShowInputRequested:Z

    #@11
    if-eq v5, v8, :cond_80

    #@13
    move v0, v6

    #@14
    .line 891
    .local v0, changed:Z
    :goto_14
    iget-boolean v5, p0, Landroid/inputmethodservice/InputMethodService;->mIsFullscreen:Z

    #@16
    if-ne v5, v2, :cond_1c

    #@18
    iget-boolean v5, p0, Landroid/inputmethodservice/InputMethodService;->mFullscreenApplied:Z

    #@1a
    if-nez v5, :cond_6a

    #@1c
    .line 892
    :cond_1c
    const/4 v0, 0x1

    #@1d
    .line 893
    iput-boolean v2, p0, Landroid/inputmethodservice/InputMethodService;->mIsFullscreen:Z

    #@1f
    .line 894
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    #@22
    move-result-object v1

    #@23
    .line 895
    .local v1, ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v1, :cond_28

    #@25
    invoke-interface {v1, v2}, Landroid/view/inputmethod/InputConnection;->reportFullscreenMode(Z)Z

    #@28
    .line 896
    :cond_28
    iput-boolean v6, p0, Landroid/inputmethodservice/InputMethodService;->mFullscreenApplied:Z

    #@2a
    .line 897
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->initialize()V

    #@2d
    .line 898
    iget-object v5, p0, Landroid/inputmethodservice/InputMethodService;->mFullscreenArea:Landroid/view/ViewGroup;

    #@2f
    invoke-virtual {v5}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@32
    move-result-object v3

    #@33
    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    #@35
    .line 900
    .local v3, lp:Landroid/widget/LinearLayout$LayoutParams;
    if-eqz v2, :cond_82

    #@37
    .line 901
    iget-object v5, p0, Landroid/inputmethodservice/InputMethodService;->mFullscreenArea:Landroid/view/ViewGroup;

    #@39
    iget-object v8, p0, Landroid/inputmethodservice/InputMethodService;->mThemeAttrs:Landroid/content/res/TypedArray;

    #@3b
    invoke-virtual {v8, v7}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@3e
    move-result-object v8

    #@3f
    invoke-virtual {v5, v8}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@42
    .line 903
    iput v7, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@44
    .line 904
    const/high16 v5, 0x3f80

    #@46
    iput v5, v3, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    #@48
    .line 910
    :goto_48
    iget-object v5, p0, Landroid/inputmethodservice/InputMethodService;->mFullscreenArea:Landroid/view/ViewGroup;

    #@4a
    invoke-virtual {v5}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    #@4d
    move-result-object v5

    #@4e
    check-cast v5, Landroid/view/ViewGroup;

    #@50
    iget-object v8, p0, Landroid/inputmethodservice/InputMethodService;->mFullscreenArea:Landroid/view/ViewGroup;

    #@52
    invoke-virtual {v5, v8, v3}, Landroid/view/ViewGroup;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@55
    .line 912
    if-eqz v2, :cond_67

    #@57
    .line 913
    iget-object v5, p0, Landroid/inputmethodservice/InputMethodService;->mExtractView:Landroid/view/View;

    #@59
    if-nez v5, :cond_64

    #@5b
    .line 914
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->onCreateExtractTextView()Landroid/view/View;

    #@5e
    move-result-object v4

    #@5f
    .line 915
    .local v4, v:Landroid/view/View;
    if-eqz v4, :cond_64

    #@61
    .line 916
    invoke-virtual {p0, v4}, Landroid/inputmethodservice/InputMethodService;->setExtractView(Landroid/view/View;)V

    #@64
    .line 919
    .end local v4           #v:Landroid/view/View;
    :cond_64
    invoke-virtual {p0, v7}, Landroid/inputmethodservice/InputMethodService;->startExtractingText(Z)V

    #@67
    .line 921
    :cond_67
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->updateExtractFrameVisibility()V

    #@6a
    .line 924
    .end local v1           #ic:Landroid/view/inputmethod/InputConnection;
    .end local v3           #lp:Landroid/widget/LinearLayout$LayoutParams;
    :cond_6a
    if-eqz v0, :cond_7d

    #@6c
    .line 925
    iget-object v5, p0, Landroid/inputmethodservice/InputMethodService;->mWindow:Landroid/inputmethodservice/SoftInputWindow;

    #@6e
    invoke-virtual {v5}, Landroid/inputmethodservice/SoftInputWindow;->getWindow()Landroid/view/Window;

    #@71
    move-result-object v5

    #@72
    iget-boolean v8, p0, Landroid/inputmethodservice/InputMethodService;->mShowInputRequested:Z

    #@74
    if-nez v8, :cond_8f

    #@76
    :goto_76
    invoke-virtual {p0, v5, v2, v6}, Landroid/inputmethodservice/InputMethodService;->onConfigureWindow(Landroid/view/Window;ZZ)V

    #@79
    .line 926
    iget-boolean v5, p0, Landroid/inputmethodservice/InputMethodService;->mShowInputRequested:Z

    #@7b
    iput-boolean v5, p0, Landroid/inputmethodservice/InputMethodService;->mLastShowInputRequested:Z

    #@7d
    .line 928
    :cond_7d
    return-void

    #@7e
    .end local v0           #changed:Z
    .end local v2           #isFullscreen:Z
    :cond_7e
    move v2, v7

    #@7f
    .line 889
    goto :goto_d

    #@80
    .restart local v2       #isFullscreen:Z
    :cond_80
    move v0, v7

    #@81
    .line 890
    goto :goto_14

    #@82
    .line 906
    .restart local v0       #changed:Z
    .restart local v1       #ic:Landroid/view/inputmethod/InputConnection;
    .restart local v3       #lp:Landroid/widget/LinearLayout$LayoutParams;
    :cond_82
    iget-object v5, p0, Landroid/inputmethodservice/InputMethodService;->mFullscreenArea:Landroid/view/ViewGroup;

    #@84
    const/4 v8, 0x0

    #@85
    invoke-virtual {v5, v8}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@88
    .line 907
    const/4 v5, -0x2

    #@89
    iput v5, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@8b
    .line 908
    const/4 v5, 0x0

    #@8c
    iput v5, v3, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    #@8e
    goto :goto_48

    #@8f
    .end local v1           #ic:Landroid/view/inputmethod/InputConnection;
    .end local v3           #lp:Landroid/widget/LinearLayout$LayoutParams;
    :cond_8f
    move v6, v7

    #@90
    .line 925
    goto :goto_76
.end method

.method public updateInputViewShown()V
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1085
    iget-boolean v3, p0, Landroid/inputmethodservice/InputMethodService;->mShowInputRequested:Z

    #@3
    if-eqz v3, :cond_2e

    #@5
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->onEvaluateInputViewShown()Z

    #@8
    move-result v3

    #@9
    if-eqz v3, :cond_2e

    #@b
    const/4 v0, 0x1

    #@c
    .line 1086
    .local v0, isShown:Z
    :goto_c
    iget-boolean v3, p0, Landroid/inputmethodservice/InputMethodService;->mIsInputViewShown:Z

    #@e
    if-eq v3, v0, :cond_2d

    #@10
    iget-boolean v3, p0, Landroid/inputmethodservice/InputMethodService;->mWindowVisible:Z

    #@12
    if-eqz v3, :cond_2d

    #@14
    .line 1087
    iput-boolean v0, p0, Landroid/inputmethodservice/InputMethodService;->mIsInputViewShown:Z

    #@16
    .line 1088
    iget-object v3, p0, Landroid/inputmethodservice/InputMethodService;->mInputFrame:Landroid/widget/FrameLayout;

    #@18
    if-eqz v0, :cond_30

    #@1a
    :goto_1a
    invoke-virtual {v3, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    #@1d
    .line 1089
    iget-object v2, p0, Landroid/inputmethodservice/InputMethodService;->mInputView:Landroid/view/View;

    #@1f
    if-nez v2, :cond_2d

    #@21
    .line 1090
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->initialize()V

    #@24
    .line 1091
    invoke-virtual {p0}, Landroid/inputmethodservice/InputMethodService;->onCreateInputView()Landroid/view/View;

    #@27
    move-result-object v1

    #@28
    .line 1092
    .local v1, v:Landroid/view/View;
    if-eqz v1, :cond_2d

    #@2a
    .line 1093
    invoke-virtual {p0, v1}, Landroid/inputmethodservice/InputMethodService;->setInputView(Landroid/view/View;)V

    #@2d
    .line 1097
    .end local v1           #v:Landroid/view/View;
    :cond_2d
    return-void

    #@2e
    .end local v0           #isShown:Z
    :cond_2e
    move v0, v2

    #@2f
    .line 1085
    goto :goto_c

    #@30
    .line 1088
    .restart local v0       #isShown:Z
    :cond_30
    const/16 v2, 0x8

    #@32
    goto :goto_1a
.end method
