.class Landroid/inputmethodservice/KeyboardView$2;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "KeyboardView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/inputmethodservice/KeyboardView;->initGestureDetector()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/inputmethodservice/KeyboardView;


# direct methods
.method constructor <init>(Landroid/inputmethodservice/KeyboardView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 384
    iput-object p1, p0, Landroid/inputmethodservice/KeyboardView$2;->this$0:Landroid/inputmethodservice/KeyboardView;

    #@2
    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .registers 20
    .parameter "me1"
    .parameter "me2"
    .parameter "velocityX"
    .parameter "velocityY"

    #@0
    .prologue
    .line 388
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView$2;->this$0:Landroid/inputmethodservice/KeyboardView;

    #@2
    invoke-static {v0}, Landroid/inputmethodservice/KeyboardView;->access$500(Landroid/inputmethodservice/KeyboardView;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_a

    #@8
    const/4 v0, 0x0

    #@9
    .line 432
    :goto_9
    return v0

    #@a
    .line 389
    :cond_a
    invoke-static/range {p3 .. p3}, Ljava/lang/Math;->abs(F)F

    #@d
    move-result v6

    #@e
    .line 390
    .local v6, absX:F
    invoke-static/range {p4 .. p4}, Ljava/lang/Math;->abs(F)F

    #@11
    move-result v7

    #@12
    .line 391
    .local v7, absY:F
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    #@15
    move-result v0

    #@16
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    #@19
    move-result v1

    #@1a
    sub-float v8, v0, v1

    #@1c
    .line 392
    .local v8, deltaX:F
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    #@1f
    move-result v0

    #@20
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    #@23
    move-result v1

    #@24
    sub-float v9, v0, v1

    #@26
    .line 393
    .local v9, deltaY:F
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView$2;->this$0:Landroid/inputmethodservice/KeyboardView;

    #@28
    invoke-virtual {v0}, Landroid/inputmethodservice/KeyboardView;->getWidth()I

    #@2b
    move-result v0

    #@2c
    div-int/lit8 v13, v0, 0x2

    #@2e
    .line 394
    .local v13, travelX:I
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView$2;->this$0:Landroid/inputmethodservice/KeyboardView;

    #@30
    invoke-virtual {v0}, Landroid/inputmethodservice/KeyboardView;->getHeight()I

    #@33
    move-result v0

    #@34
    div-int/lit8 v14, v0, 0x2

    #@36
    .line 395
    .local v14, travelY:I
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView$2;->this$0:Landroid/inputmethodservice/KeyboardView;

    #@38
    invoke-static {v0}, Landroid/inputmethodservice/KeyboardView;->access$600(Landroid/inputmethodservice/KeyboardView;)Landroid/inputmethodservice/KeyboardView$SwipeTracker;

    #@3b
    move-result-object v0

    #@3c
    const/16 v1, 0x3e8

    #@3e
    invoke-virtual {v0, v1}, Landroid/inputmethodservice/KeyboardView$SwipeTracker;->computeCurrentVelocity(I)V

    #@41
    .line 396
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView$2;->this$0:Landroid/inputmethodservice/KeyboardView;

    #@43
    invoke-static {v0}, Landroid/inputmethodservice/KeyboardView;->access$600(Landroid/inputmethodservice/KeyboardView;)Landroid/inputmethodservice/KeyboardView$SwipeTracker;

    #@46
    move-result-object v0

    #@47
    invoke-virtual {v0}, Landroid/inputmethodservice/KeyboardView$SwipeTracker;->getXVelocity()F

    #@4a
    move-result v10

    #@4b
    .line 397
    .local v10, endingVelocityX:F
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView$2;->this$0:Landroid/inputmethodservice/KeyboardView;

    #@4d
    invoke-static {v0}, Landroid/inputmethodservice/KeyboardView;->access$600(Landroid/inputmethodservice/KeyboardView;)Landroid/inputmethodservice/KeyboardView$SwipeTracker;

    #@50
    move-result-object v0

    #@51
    invoke-virtual {v0}, Landroid/inputmethodservice/KeyboardView$SwipeTracker;->getYVelocity()F

    #@54
    move-result v11

    #@55
    .line 398
    .local v11, endingVelocityY:F
    const/4 v12, 0x0

    #@56
    .line 399
    .local v12, sendDownKey:Z
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView$2;->this$0:Landroid/inputmethodservice/KeyboardView;

    #@58
    invoke-static {v0}, Landroid/inputmethodservice/KeyboardView;->access$700(Landroid/inputmethodservice/KeyboardView;)I

    #@5b
    move-result v0

    #@5c
    int-to-float v0, v0

    #@5d
    cmpl-float v0, p3, v0

    #@5f
    if-lez v0, :cond_a3

    #@61
    cmpg-float v0, v7, v6

    #@63
    if-gez v0, :cond_a3

    #@65
    int-to-float v0, v13

    #@66
    cmpl-float v0, v8, v0

    #@68
    if-lez v0, :cond_a3

    #@6a
    .line 400
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView$2;->this$0:Landroid/inputmethodservice/KeyboardView;

    #@6c
    invoke-static {v0}, Landroid/inputmethodservice/KeyboardView;->access$800(Landroid/inputmethodservice/KeyboardView;)Z

    #@6f
    move-result v0

    #@70
    if-eqz v0, :cond_9b

    #@72
    const/high16 v0, 0x4080

    #@74
    div-float v0, p3, v0

    #@76
    cmpg-float v0, v10, v0

    #@78
    if-gez v0, :cond_9b

    #@7a
    .line 401
    const/4 v12, 0x1

    #@7b
    .line 429
    :cond_7b
    :goto_7b
    if-eqz v12, :cond_98

    #@7d
    .line 430
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView$2;->this$0:Landroid/inputmethodservice/KeyboardView;

    #@7f
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView$2;->this$0:Landroid/inputmethodservice/KeyboardView;

    #@81
    invoke-static {v1}, Landroid/inputmethodservice/KeyboardView;->access$900(Landroid/inputmethodservice/KeyboardView;)I

    #@84
    move-result v1

    #@85
    iget-object v2, p0, Landroid/inputmethodservice/KeyboardView$2;->this$0:Landroid/inputmethodservice/KeyboardView;

    #@87
    invoke-static {v2}, Landroid/inputmethodservice/KeyboardView;->access$1000(Landroid/inputmethodservice/KeyboardView;)I

    #@8a
    move-result v2

    #@8b
    iget-object v3, p0, Landroid/inputmethodservice/KeyboardView$2;->this$0:Landroid/inputmethodservice/KeyboardView;

    #@8d
    invoke-static {v3}, Landroid/inputmethodservice/KeyboardView;->access$1100(Landroid/inputmethodservice/KeyboardView;)I

    #@90
    move-result v3

    #@91
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    #@94
    move-result-wide v4

    #@95
    invoke-static/range {v0 .. v5}, Landroid/inputmethodservice/KeyboardView;->access$1200(Landroid/inputmethodservice/KeyboardView;IIIJ)V

    #@98
    .line 432
    :cond_98
    const/4 v0, 0x0

    #@99
    goto/16 :goto_9

    #@9b
    .line 403
    :cond_9b
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView$2;->this$0:Landroid/inputmethodservice/KeyboardView;

    #@9d
    invoke-virtual {v0}, Landroid/inputmethodservice/KeyboardView;->swipeRight()V

    #@a0
    .line 404
    const/4 v0, 0x1

    #@a1
    goto/16 :goto_9

    #@a3
    .line 406
    :cond_a3
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView$2;->this$0:Landroid/inputmethodservice/KeyboardView;

    #@a5
    invoke-static {v0}, Landroid/inputmethodservice/KeyboardView;->access$700(Landroid/inputmethodservice/KeyboardView;)I

    #@a8
    move-result v0

    #@a9
    neg-int v0, v0

    #@aa
    int-to-float v0, v0

    #@ab
    cmpg-float v0, p3, v0

    #@ad
    if-gez v0, :cond_d3

    #@af
    cmpg-float v0, v7, v6

    #@b1
    if-gez v0, :cond_d3

    #@b3
    neg-int v0, v13

    #@b4
    int-to-float v0, v0

    #@b5
    cmpg-float v0, v8, v0

    #@b7
    if-gez v0, :cond_d3

    #@b9
    .line 407
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView$2;->this$0:Landroid/inputmethodservice/KeyboardView;

    #@bb
    invoke-static {v0}, Landroid/inputmethodservice/KeyboardView;->access$800(Landroid/inputmethodservice/KeyboardView;)Z

    #@be
    move-result v0

    #@bf
    if-eqz v0, :cond_cb

    #@c1
    const/high16 v0, 0x4080

    #@c3
    div-float v0, p3, v0

    #@c5
    cmpl-float v0, v10, v0

    #@c7
    if-lez v0, :cond_cb

    #@c9
    .line 408
    const/4 v12, 0x1

    #@ca
    goto :goto_7b

    #@cb
    .line 410
    :cond_cb
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView$2;->this$0:Landroid/inputmethodservice/KeyboardView;

    #@cd
    invoke-virtual {v0}, Landroid/inputmethodservice/KeyboardView;->swipeLeft()V

    #@d0
    .line 411
    const/4 v0, 0x1

    #@d1
    goto/16 :goto_9

    #@d3
    .line 413
    :cond_d3
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView$2;->this$0:Landroid/inputmethodservice/KeyboardView;

    #@d5
    invoke-static {v0}, Landroid/inputmethodservice/KeyboardView;->access$700(Landroid/inputmethodservice/KeyboardView;)I

    #@d8
    move-result v0

    #@d9
    neg-int v0, v0

    #@da
    int-to-float v0, v0

    #@db
    cmpg-float v0, p4, v0

    #@dd
    if-gez v0, :cond_103

    #@df
    cmpg-float v0, v6, v7

    #@e1
    if-gez v0, :cond_103

    #@e3
    neg-int v0, v14

    #@e4
    int-to-float v0, v0

    #@e5
    cmpg-float v0, v9, v0

    #@e7
    if-gez v0, :cond_103

    #@e9
    .line 414
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView$2;->this$0:Landroid/inputmethodservice/KeyboardView;

    #@eb
    invoke-static {v0}, Landroid/inputmethodservice/KeyboardView;->access$800(Landroid/inputmethodservice/KeyboardView;)Z

    #@ee
    move-result v0

    #@ef
    if-eqz v0, :cond_fb

    #@f1
    const/high16 v0, 0x4080

    #@f3
    div-float v0, p4, v0

    #@f5
    cmpl-float v0, v11, v0

    #@f7
    if-lez v0, :cond_fb

    #@f9
    .line 415
    const/4 v12, 0x1

    #@fa
    goto :goto_7b

    #@fb
    .line 417
    :cond_fb
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView$2;->this$0:Landroid/inputmethodservice/KeyboardView;

    #@fd
    invoke-virtual {v0}, Landroid/inputmethodservice/KeyboardView;->swipeUp()V

    #@100
    .line 418
    const/4 v0, 0x1

    #@101
    goto/16 :goto_9

    #@103
    .line 420
    :cond_103
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView$2;->this$0:Landroid/inputmethodservice/KeyboardView;

    #@105
    invoke-static {v0}, Landroid/inputmethodservice/KeyboardView;->access$700(Landroid/inputmethodservice/KeyboardView;)I

    #@108
    move-result v0

    #@109
    int-to-float v0, v0

    #@10a
    cmpl-float v0, p4, v0

    #@10c
    if-lez v0, :cond_7b

    #@10e
    const/high16 v0, 0x4000

    #@110
    div-float v0, v7, v0

    #@112
    cmpg-float v0, v6, v0

    #@114
    if-gez v0, :cond_7b

    #@116
    int-to-float v0, v14

    #@117
    cmpl-float v0, v9, v0

    #@119
    if-lez v0, :cond_7b

    #@11b
    .line 421
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView$2;->this$0:Landroid/inputmethodservice/KeyboardView;

    #@11d
    invoke-static {v0}, Landroid/inputmethodservice/KeyboardView;->access$800(Landroid/inputmethodservice/KeyboardView;)Z

    #@120
    move-result v0

    #@121
    if-eqz v0, :cond_12e

    #@123
    const/high16 v0, 0x4080

    #@125
    div-float v0, p4, v0

    #@127
    cmpg-float v0, v11, v0

    #@129
    if-gez v0, :cond_12e

    #@12b
    .line 422
    const/4 v12, 0x1

    #@12c
    goto/16 :goto_7b

    #@12e
    .line 424
    :cond_12e
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView$2;->this$0:Landroid/inputmethodservice/KeyboardView;

    #@130
    invoke-virtual {v0}, Landroid/inputmethodservice/KeyboardView;->swipeDown()V

    #@133
    .line 425
    const/4 v0, 0x1

    #@134
    goto/16 :goto_9
.end method
