.class public Landroid/inputmethodservice/ExtractEditText;
.super Landroid/widget/EditText;
.source "ExtractEditText.java"


# instance fields
.field private mIME:Landroid/inputmethodservice/InputMethodService;

.field private mSettingExtractedText:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 34
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 38
    const v0, 0x101006e

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@3
    .line 43
    return-void
.end method


# virtual methods
.method protected deleteText_internal(II)V
    .registers 4
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 171
    iget-object v0, p0, Landroid/inputmethodservice/ExtractEditText;->mIME:Landroid/inputmethodservice/InputMethodService;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/inputmethodservice/InputMethodService;->onExtractedDeleteText(II)V

    #@5
    .line 172
    return-void
.end method

.method public finishInternalChanges()V
    .registers 2

    #@0
    .prologue
    .line 64
    iget v0, p0, Landroid/inputmethodservice/ExtractEditText;->mSettingExtractedText:I

    #@2
    add-int/lit8 v0, v0, -0x1

    #@4
    iput v0, p0, Landroid/inputmethodservice/ExtractEditText;->mSettingExtractedText:I

    #@6
    .line 65
    return-void
.end method

.method public hasFocus()Z
    .registers 2

    #@0
    .prologue
    .line 147
    invoke-virtual {p0}, Landroid/inputmethodservice/ExtractEditText;->isEnabled()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public hasVerticalScrollBar()Z
    .registers 3

    #@0
    .prologue
    .line 123
    invoke-virtual {p0}, Landroid/inputmethodservice/ExtractEditText;->computeVerticalScrollRange()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0}, Landroid/inputmethodservice/ExtractEditText;->computeVerticalScrollExtent()I

    #@7
    move-result v1

    #@8
    if-le v0, v1, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public hasWindowFocus()Z
    .registers 2

    #@0
    .prologue
    .line 131
    invoke-virtual {p0}, Landroid/inputmethodservice/ExtractEditText;->isEnabled()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public isFocused()Z
    .registers 2

    #@0
    .prologue
    .line 139
    invoke-virtual {p0}, Landroid/inputmethodservice/ExtractEditText;->isEnabled()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public isInputMethodTarget()Z
    .registers 2

    #@0
    .prologue
    .line 116
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method protected onSelectionChanged(II)V
    .registers 4
    .parameter "selStart"
    .parameter "selEnd"

    #@0
    .prologue
    .line 84
    iget v0, p0, Landroid/inputmethodservice/ExtractEditText;->mSettingExtractedText:I

    #@2
    if-nez v0, :cond_11

    #@4
    iget-object v0, p0, Landroid/inputmethodservice/ExtractEditText;->mIME:Landroid/inputmethodservice/InputMethodService;

    #@6
    if-eqz v0, :cond_11

    #@8
    if-ltz p1, :cond_11

    #@a
    if-ltz p2, :cond_11

    #@c
    .line 85
    iget-object v0, p0, Landroid/inputmethodservice/ExtractEditText;->mIME:Landroid/inputmethodservice/InputMethodService;

    #@e
    invoke-virtual {v0, p1, p2}, Landroid/inputmethodservice/InputMethodService;->onExtractedSelectionChanged(II)V

    #@11
    .line 87
    :cond_11
    return-void
.end method

.method public onTextContextMenuItem(I)Z
    .registers 3
    .parameter "id"

    #@0
    .prologue
    .line 102
    iget-object v0, p0, Landroid/inputmethodservice/ExtractEditText;->mIME:Landroid/inputmethodservice/InputMethodService;

    #@2
    if-eqz v0, :cond_16

    #@4
    iget-object v0, p0, Landroid/inputmethodservice/ExtractEditText;->mIME:Landroid/inputmethodservice/InputMethodService;

    #@6
    invoke-virtual {v0, p1}, Landroid/inputmethodservice/InputMethodService;->onExtractTextContextMenuItem(I)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_16

    #@c
    .line 105
    const v0, 0x1020021

    #@f
    if-ne p1, v0, :cond_14

    #@11
    invoke-virtual {p0}, Landroid/inputmethodservice/ExtractEditText;->stopSelectionActionMode()V

    #@14
    .line 106
    :cond_14
    const/4 v0, 0x1

    #@15
    .line 108
    :goto_15
    return v0

    #@16
    :cond_16
    invoke-super {p0, p1}, Landroid/widget/EditText;->onTextContextMenuItem(I)Z

    #@19
    move-result v0

    #@1a
    goto :goto_15
.end method

.method public performClick()Z
    .registers 2

    #@0
    .prologue
    .line 94
    invoke-super {p0}, Landroid/widget/EditText;->performClick()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_11

    #@6
    iget-object v0, p0, Landroid/inputmethodservice/ExtractEditText;->mIME:Landroid/inputmethodservice/InputMethodService;

    #@8
    if-eqz v0, :cond_11

    #@a
    .line 95
    iget-object v0, p0, Landroid/inputmethodservice/ExtractEditText;->mIME:Landroid/inputmethodservice/InputMethodService;

    #@c
    invoke-virtual {v0}, Landroid/inputmethodservice/InputMethodService;->onExtractedTextClicked()V

    #@f
    .line 96
    const/4 v0, 0x1

    #@10
    .line 98
    :goto_10
    return v0

    #@11
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_10
.end method

.method protected replaceText_internal(IILjava/lang/CharSequence;)V
    .registers 5
    .parameter "start"
    .parameter "end"
    .parameter "text"

    #@0
    .prologue
    .line 182
    iget-object v0, p0, Landroid/inputmethodservice/ExtractEditText;->mIME:Landroid/inputmethodservice/InputMethodService;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/inputmethodservice/InputMethodService;->onExtractedReplaceText(IILjava/lang/CharSequence;)V

    #@5
    .line 183
    return-void
.end method

.method protected setCursorPosition_internal(II)V
    .registers 4
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 204
    iget-object v0, p0, Landroid/inputmethodservice/ExtractEditText;->mIME:Landroid/inputmethodservice/InputMethodService;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/inputmethodservice/InputMethodService;->onExtractedSelectionChanged(II)V

    #@5
    .line 205
    return-void
.end method

.method public setExtractedText(Landroid/view/inputmethod/ExtractedText;)V
    .registers 4
    .parameter "text"

    #@0
    .prologue
    .line 73
    :try_start_0
    iget v0, p0, Landroid/inputmethodservice/ExtractEditText;->mSettingExtractedText:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Landroid/inputmethodservice/ExtractEditText;->mSettingExtractedText:I

    #@6
    .line 74
    invoke-super {p0, p1}, Landroid/widget/EditText;->setExtractedText(Landroid/view/inputmethod/ExtractedText;)V
    :try_end_9
    .catchall {:try_start_0 .. :try_end_9} :catchall_10

    #@9
    .line 76
    iget v0, p0, Landroid/inputmethodservice/ExtractEditText;->mSettingExtractedText:I

    #@b
    add-int/lit8 v0, v0, -0x1

    #@d
    iput v0, p0, Landroid/inputmethodservice/ExtractEditText;->mSettingExtractedText:I

    #@f
    .line 78
    return-void

    #@10
    .line 76
    :catchall_10
    move-exception v0

    #@11
    iget v1, p0, Landroid/inputmethodservice/ExtractEditText;->mSettingExtractedText:I

    #@13
    add-int/lit8 v1, v1, -0x1

    #@15
    iput v1, p0, Landroid/inputmethodservice/ExtractEditText;->mSettingExtractedText:I

    #@17
    throw v0
.end method

.method setIME(Landroid/inputmethodservice/InputMethodService;)V
    .registers 2
    .parameter "ime"

    #@0
    .prologue
    .line 46
    iput-object p1, p0, Landroid/inputmethodservice/ExtractEditText;->mIME:Landroid/inputmethodservice/InputMethodService;

    #@2
    .line 47
    return-void
.end method

.method protected setSpan_internal(Ljava/lang/Object;III)V
    .registers 6
    .parameter "span"
    .parameter "start"
    .parameter "end"
    .parameter "flags"

    #@0
    .prologue
    .line 193
    iget-object v0, p0, Landroid/inputmethodservice/ExtractEditText;->mIME:Landroid/inputmethodservice/InputMethodService;

    #@2
    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/inputmethodservice/InputMethodService;->onExtractedSetSpan(Ljava/lang/Object;III)V

    #@5
    .line 194
    return-void
.end method

.method public startInternalChanges()V
    .registers 2

    #@0
    .prologue
    .line 55
    iget v0, p0, Landroid/inputmethodservice/ExtractEditText;->mSettingExtractedText:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Landroid/inputmethodservice/ExtractEditText;->mSettingExtractedText:I

    #@6
    .line 56
    return-void
.end method

.method protected viewClicked(Landroid/view/inputmethod/InputMethodManager;)V
    .registers 4
    .parameter "imm"

    #@0
    .prologue
    .line 158
    iget-object v0, p0, Landroid/inputmethodservice/ExtractEditText;->mIME:Landroid/inputmethodservice/InputMethodService;

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 159
    iget-object v0, p0, Landroid/inputmethodservice/ExtractEditText;->mIME:Landroid/inputmethodservice/InputMethodService;

    #@6
    const/4 v1, 0x0

    #@7
    invoke-virtual {v0, v1}, Landroid/inputmethodservice/InputMethodService;->onViewClicked(Z)V

    #@a
    .line 161
    :cond_a
    return-void
.end method
