.class Landroid/inputmethodservice/IInputMethodSessionWrapper;
.super Lcom/android/internal/view/IInputMethodSession$Stub;
.source "IInputMethodSessionWrapper.java"

# interfaces
.implements Lcom/android/internal/os/HandlerCaller$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/inputmethodservice/IInputMethodSessionWrapper$InputMethodEventCallbackWrapper;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final DO_APP_PRIVATE_COMMAND:I = 0x64

.field private static final DO_DISPATCH_GENERIC_MOTION_EVENT:I = 0x55

.field private static final DO_DISPATCH_KEY_EVENT:I = 0x46

.field private static final DO_DISPATCH_TRACKBALL_EVENT:I = 0x50

.field private static final DO_DISPLAY_COMPLETIONS:I = 0x41

.field private static final DO_FINISH_INPUT:I = 0x3c

.field private static final DO_FINISH_SESSION:I = 0x6e

.field private static final DO_TOGGLE_SOFT_INPUT:I = 0x69

.field private static final DO_UPDATE_CURSOR:I = 0x5f

.field private static final DO_UPDATE_EXTRACTED_TEXT:I = 0x43

.field private static final DO_UPDATE_SELECTION:I = 0x5a

.field private static final DO_VIEW_CLICKED:I = 0x73

.field private static final TAG:Ljava/lang/String; = "InputMethodWrapper"


# instance fields
.field mCaller:Lcom/android/internal/os/HandlerCaller;

.field mInputMethodSession:Landroid/view/inputmethod/InputMethodSession;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/inputmethod/InputMethodSession;)V
    .registers 4
    .parameter "context"
    .parameter "inputMethodSession"

    #@0
    .prologue
    .line 72
    invoke-direct {p0}, Lcom/android/internal/view/IInputMethodSession$Stub;-><init>()V

    #@3
    .line 73
    new-instance v0, Lcom/android/internal/os/HandlerCaller;

    #@5
    invoke-direct {v0, p1, p0}, Lcom/android/internal/os/HandlerCaller;-><init>(Landroid/content/Context;Lcom/android/internal/os/HandlerCaller$Callback;)V

    #@8
    iput-object v0, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@a
    .line 74
    iput-object p2, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mInputMethodSession:Landroid/view/inputmethod/InputMethodSession;

    #@c
    .line 75
    return-void
.end method


# virtual methods
.method public appPrivateCommand(Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 6
    .parameter "action"
    .parameter "data"

    #@0
    .prologue
    .line 209
    iget-object v0, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2
    iget-object v1, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@4
    const/16 v2, 0x64

    #@6
    invoke-virtual {v1, v2, p1, p2}, Lcom/android/internal/os/HandlerCaller;->obtainMessageOO(ILjava/lang/Object;Ljava/lang/Object;)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V

    #@d
    .line 210
    return-void
.end method

.method public dispatchGenericMotionEvent(ILandroid/view/MotionEvent;Lcom/android/internal/view/IInputMethodCallback;)V
    .registers 7
    .parameter "seq"
    .parameter "event"
    .parameter "callback"

    #@0
    .prologue
    .line 188
    iget-object v0, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2
    iget-object v1, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@4
    const/16 v2, 0x55

    #@6
    invoke-virtual {v1, v2, p1, p2, p3}, Lcom/android/internal/os/HandlerCaller;->obtainMessageIOO(IILjava/lang/Object;Ljava/lang/Object;)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V

    #@d
    .line 190
    return-void
.end method

.method public dispatchKeyEvent(ILandroid/view/KeyEvent;Lcom/android/internal/view/IInputMethodCallback;)V
    .registers 7
    .parameter "seq"
    .parameter "event"
    .parameter "callback"

    #@0
    .prologue
    .line 177
    iget-object v0, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2
    iget-object v1, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@4
    const/16 v2, 0x46

    #@6
    invoke-virtual {v1, v2, p1, p2, p3}, Lcom/android/internal/os/HandlerCaller;->obtainMessageIOO(IILjava/lang/Object;Ljava/lang/Object;)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V

    #@d
    .line 179
    return-void
.end method

.method public dispatchTrackballEvent(ILandroid/view/MotionEvent;Lcom/android/internal/view/IInputMethodCallback;)V
    .registers 7
    .parameter "seq"
    .parameter "event"
    .parameter "callback"

    #@0
    .prologue
    .line 182
    iget-object v0, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2
    iget-object v1, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@4
    const/16 v2, 0x50

    #@6
    invoke-virtual {v1, v2, p1, p2, p3}, Lcom/android/internal/os/HandlerCaller;->obtainMessageIOO(IILjava/lang/Object;Ljava/lang/Object;)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V

    #@d
    .line 184
    return-void
.end method

.method public displayCompletions([Landroid/view/inputmethod/CompletionInfo;)V
    .registers 5
    .parameter "completions"

    #@0
    .prologue
    .line 167
    iget-object v0, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2
    iget-object v1, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@4
    const/16 v2, 0x41

    #@6
    invoke-virtual {v1, v2, p1}, Lcom/android/internal/os/HandlerCaller;->obtainMessageO(ILjava/lang/Object;)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V

    #@d
    .line 169
    return-void
.end method

.method public executeMessage(Landroid/os/Message;)V
    .registers 10
    .parameter "msg"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 82
    iget-object v1, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mInputMethodSession:Landroid/view/inputmethod/InputMethodSession;

    #@3
    if-nez v1, :cond_6

    #@5
    .line 160
    :goto_5
    return-void

    #@6
    .line 84
    :cond_6
    iget v1, p1, Landroid/os/Message;->what:I

    #@8
    sparse-switch v1, :sswitch_data_104

    #@b
    .line 159
    const-string v0, "InputMethodWrapper"

    #@d
    new-instance v1, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v2, "Unhandled message code: "

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    iget v2, p1, Landroid/os/Message;->what:I

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    goto :goto_5

    #@26
    .line 86
    :sswitch_26
    iget-object v0, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mInputMethodSession:Landroid/view/inputmethod/InputMethodSession;

    #@28
    invoke-interface {v0}, Landroid/view/inputmethod/InputMethodSession;->finishInput()V

    #@2b
    goto :goto_5

    #@2c
    .line 89
    :sswitch_2c
    iget-object v1, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mInputMethodSession:Landroid/view/inputmethod/InputMethodSession;

    #@2e
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@30
    check-cast v0, [Landroid/view/inputmethod/CompletionInfo;

    #@32
    check-cast v0, [Landroid/view/inputmethod/CompletionInfo;

    #@34
    invoke-interface {v1, v0}, Landroid/view/inputmethod/InputMethodSession;->displayCompletions([Landroid/view/inputmethod/CompletionInfo;)V

    #@37
    goto :goto_5

    #@38
    .line 92
    :sswitch_38
    iget-object v1, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mInputMethodSession:Landroid/view/inputmethod/InputMethodSession;

    #@3a
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@3c
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3e
    check-cast v0, Landroid/view/inputmethod/ExtractedText;

    #@40
    invoke-interface {v1, v2, v0}, Landroid/view/inputmethod/InputMethodSession;->updateExtractedText(ILandroid/view/inputmethod/ExtractedText;)V

    #@43
    goto :goto_5

    #@44
    .line 96
    :sswitch_44
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@46
    check-cast v7, Lcom/android/internal/os/SomeArgs;

    #@48
    .line 97
    .local v7, args:Lcom/android/internal/os/SomeArgs;
    iget-object v2, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mInputMethodSession:Landroid/view/inputmethod/InputMethodSession;

    #@4a
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@4c
    iget-object v0, v7, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@4e
    check-cast v0, Landroid/view/KeyEvent;

    #@50
    new-instance v4, Landroid/inputmethodservice/IInputMethodSessionWrapper$InputMethodEventCallbackWrapper;

    #@52
    iget-object v1, v7, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    #@54
    check-cast v1, Lcom/android/internal/view/IInputMethodCallback;

    #@56
    invoke-direct {v4, v1}, Landroid/inputmethodservice/IInputMethodSessionWrapper$InputMethodEventCallbackWrapper;-><init>(Lcom/android/internal/view/IInputMethodCallback;)V

    #@59
    invoke-interface {v2, v3, v0, v4}, Landroid/view/inputmethod/InputMethodSession;->dispatchKeyEvent(ILandroid/view/KeyEvent;Landroid/view/inputmethod/InputMethodSession$EventCallback;)V

    #@5c
    .line 101
    invoke-virtual {v7}, Lcom/android/internal/os/SomeArgs;->recycle()V

    #@5f
    goto :goto_5

    #@60
    .line 105
    .end local v7           #args:Lcom/android/internal/os/SomeArgs;
    :sswitch_60
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@62
    check-cast v7, Lcom/android/internal/os/SomeArgs;

    #@64
    .line 106
    .restart local v7       #args:Lcom/android/internal/os/SomeArgs;
    iget-object v2, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mInputMethodSession:Landroid/view/inputmethod/InputMethodSession;

    #@66
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@68
    iget-object v0, v7, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@6a
    check-cast v0, Landroid/view/MotionEvent;

    #@6c
    new-instance v4, Landroid/inputmethodservice/IInputMethodSessionWrapper$InputMethodEventCallbackWrapper;

    #@6e
    iget-object v1, v7, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    #@70
    check-cast v1, Lcom/android/internal/view/IInputMethodCallback;

    #@72
    invoke-direct {v4, v1}, Landroid/inputmethodservice/IInputMethodSessionWrapper$InputMethodEventCallbackWrapper;-><init>(Lcom/android/internal/view/IInputMethodCallback;)V

    #@75
    invoke-interface {v2, v3, v0, v4}, Landroid/view/inputmethod/InputMethodSession;->dispatchTrackballEvent(ILandroid/view/MotionEvent;Landroid/view/inputmethod/InputMethodSession$EventCallback;)V

    #@78
    .line 110
    invoke-virtual {v7}, Lcom/android/internal/os/SomeArgs;->recycle()V

    #@7b
    goto :goto_5

    #@7c
    .line 114
    .end local v7           #args:Lcom/android/internal/os/SomeArgs;
    :sswitch_7c
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@7e
    check-cast v7, Lcom/android/internal/os/SomeArgs;

    #@80
    .line 115
    .restart local v7       #args:Lcom/android/internal/os/SomeArgs;
    iget-object v2, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mInputMethodSession:Landroid/view/inputmethod/InputMethodSession;

    #@82
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@84
    iget-object v0, v7, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@86
    check-cast v0, Landroid/view/MotionEvent;

    #@88
    new-instance v4, Landroid/inputmethodservice/IInputMethodSessionWrapper$InputMethodEventCallbackWrapper;

    #@8a
    iget-object v1, v7, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    #@8c
    check-cast v1, Lcom/android/internal/view/IInputMethodCallback;

    #@8e
    invoke-direct {v4, v1}, Landroid/inputmethodservice/IInputMethodSessionWrapper$InputMethodEventCallbackWrapper;-><init>(Lcom/android/internal/view/IInputMethodCallback;)V

    #@91
    invoke-interface {v2, v3, v0, v4}, Landroid/view/inputmethod/InputMethodSession;->dispatchGenericMotionEvent(ILandroid/view/MotionEvent;Landroid/view/inputmethod/InputMethodSession$EventCallback;)V

    #@94
    .line 119
    invoke-virtual {v7}, Lcom/android/internal/os/SomeArgs;->recycle()V

    #@97
    goto/16 :goto_5

    #@99
    .line 123
    .end local v7           #args:Lcom/android/internal/os/SomeArgs;
    :sswitch_99
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9b
    check-cast v7, Lcom/android/internal/os/SomeArgs;

    #@9d
    .line 124
    .restart local v7       #args:Lcom/android/internal/os/SomeArgs;
    iget-object v0, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mInputMethodSession:Landroid/view/inputmethod/InputMethodSession;

    #@9f
    iget v1, v7, Lcom/android/internal/os/SomeArgs;->argi1:I

    #@a1
    iget v2, v7, Lcom/android/internal/os/SomeArgs;->argi2:I

    #@a3
    iget v3, v7, Lcom/android/internal/os/SomeArgs;->argi3:I

    #@a5
    iget v4, v7, Lcom/android/internal/os/SomeArgs;->argi4:I

    #@a7
    iget v5, v7, Lcom/android/internal/os/SomeArgs;->argi5:I

    #@a9
    iget v6, v7, Lcom/android/internal/os/SomeArgs;->argi6:I

    #@ab
    invoke-interface/range {v0 .. v6}, Landroid/view/inputmethod/InputMethodSession;->updateSelection(IIIIII)V

    #@ae
    .line 126
    invoke-virtual {v7}, Lcom/android/internal/os/SomeArgs;->recycle()V

    #@b1
    goto/16 :goto_5

    #@b3
    .line 130
    .end local v7           #args:Lcom/android/internal/os/SomeArgs;
    :sswitch_b3
    iget-object v1, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mInputMethodSession:Landroid/view/inputmethod/InputMethodSession;

    #@b5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@b7
    check-cast v0, Landroid/graphics/Rect;

    #@b9
    invoke-interface {v1, v0}, Landroid/view/inputmethod/InputMethodSession;->updateCursor(Landroid/graphics/Rect;)V

    #@bc
    goto/16 :goto_5

    #@be
    .line 134
    :sswitch_be
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@c0
    check-cast v7, Lcom/android/internal/os/SomeArgs;

    #@c2
    .line 135
    .restart local v7       #args:Lcom/android/internal/os/SomeArgs;
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@c4
    if-eqz v1, :cond_d5

    #@c6
    .line 136
    iget-object v1, v7, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@c8
    const-string v2, "ACTION_SHOWING_BUBBLE_POPUP"

    #@ca
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@cd
    move-result v1

    #@ce
    if-eqz v1, :cond_d5

    #@d0
    .line 137
    invoke-static {v0}, Landroid/widget/BubblePopupHelper;->setShowingAnyBubblePopup(Z)V

    #@d3
    goto/16 :goto_5

    #@d5
    .line 141
    :cond_d5
    iget-object v2, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mInputMethodSession:Landroid/view/inputmethod/InputMethodSession;

    #@d7
    iget-object v0, v7, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@d9
    check-cast v0, Ljava/lang/String;

    #@db
    iget-object v1, v7, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    #@dd
    check-cast v1, Landroid/os/Bundle;

    #@df
    invoke-interface {v2, v0, v1}, Landroid/view/inputmethod/InputMethodSession;->appPrivateCommand(Ljava/lang/String;Landroid/os/Bundle;)V

    #@e2
    .line 143
    invoke-virtual {v7}, Lcom/android/internal/os/SomeArgs;->recycle()V

    #@e5
    goto/16 :goto_5

    #@e7
    .line 147
    .end local v7           #args:Lcom/android/internal/os/SomeArgs;
    :sswitch_e7
    iget-object v0, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mInputMethodSession:Landroid/view/inputmethod/InputMethodSession;

    #@e9
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@eb
    iget v2, p1, Landroid/os/Message;->arg2:I

    #@ed
    invoke-interface {v0, v1, v2}, Landroid/view/inputmethod/InputMethodSession;->toggleSoftInput(II)V

    #@f0
    goto/16 :goto_5

    #@f2
    .line 151
    :sswitch_f2
    const/4 v0, 0x0

    #@f3
    iput-object v0, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mInputMethodSession:Landroid/view/inputmethod/InputMethodSession;

    #@f5
    goto/16 :goto_5

    #@f7
    .line 155
    :sswitch_f7
    iget-object v1, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mInputMethodSession:Landroid/view/inputmethod/InputMethodSession;

    #@f9
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@fb
    if-ne v2, v0, :cond_102

    #@fd
    :goto_fd
    invoke-interface {v1, v0}, Landroid/view/inputmethod/InputMethodSession;->viewClicked(Z)V

    #@100
    goto/16 :goto_5

    #@102
    :cond_102
    const/4 v0, 0x0

    #@103
    goto :goto_fd

    #@104
    .line 84
    :sswitch_data_104
    .sparse-switch
        0x3c -> :sswitch_26
        0x41 -> :sswitch_2c
        0x43 -> :sswitch_38
        0x46 -> :sswitch_44
        0x50 -> :sswitch_60
        0x55 -> :sswitch_7c
        0x5a -> :sswitch_99
        0x5f -> :sswitch_b3
        0x64 -> :sswitch_be
        0x69 -> :sswitch_e7
        0x6e -> :sswitch_f2
        0x73 -> :sswitch_f7
    .end sparse-switch
.end method

.method public finishInput()V
    .registers 4

    #@0
    .prologue
    .line 163
    iget-object v0, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2
    iget-object v1, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@4
    const/16 v2, 0x3c

    #@6
    invoke-virtual {v1, v2}, Lcom/android/internal/os/HandlerCaller;->obtainMessage(I)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V

    #@d
    .line 164
    return-void
.end method

.method public finishSession()V
    .registers 4

    #@0
    .prologue
    .line 217
    iget-object v0, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2
    iget-object v1, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@4
    const/16 v2, 0x6e

    #@6
    invoke-virtual {v1, v2}, Lcom/android/internal/os/HandlerCaller;->obtainMessage(I)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V

    #@d
    .line 218
    return-void
.end method

.method public getInternalInputMethodSession()Landroid/view/inputmethod/InputMethodSession;
    .registers 2

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mInputMethodSession:Landroid/view/inputmethod/InputMethodSession;

    #@2
    return-object v0
.end method

.method public toggleSoftInput(II)V
    .registers 6
    .parameter "showFlags"
    .parameter "hideFlags"

    #@0
    .prologue
    .line 213
    iget-object v0, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2
    iget-object v1, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@4
    const/16 v2, 0x69

    #@6
    invoke-virtual {v1, v2, p1, p2}, Lcom/android/internal/os/HandlerCaller;->obtainMessageII(III)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V

    #@d
    .line 214
    return-void
.end method

.method public updateCursor(Landroid/graphics/Rect;)V
    .registers 5
    .parameter "newCursor"

    #@0
    .prologue
    .line 204
    iget-object v0, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2
    iget-object v1, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@4
    const/16 v2, 0x5f

    #@6
    invoke-virtual {v1, v2, p1}, Lcom/android/internal/os/HandlerCaller;->obtainMessageO(ILjava/lang/Object;)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V

    #@d
    .line 206
    return-void
.end method

.method public updateExtractedText(ILandroid/view/inputmethod/ExtractedText;)V
    .registers 6
    .parameter "token"
    .parameter "text"

    #@0
    .prologue
    .line 172
    iget-object v0, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2
    iget-object v1, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@4
    const/16 v2, 0x43

    #@6
    invoke-virtual {v1, v2, p1, p2}, Lcom/android/internal/os/HandlerCaller;->obtainMessageIO(IILjava/lang/Object;)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V

    #@d
    .line 174
    return-void
.end method

.method public updateSelection(IIIIII)V
    .registers 16
    .parameter "oldSelStart"
    .parameter "oldSelEnd"
    .parameter "newSelStart"
    .parameter "newSelEnd"
    .parameter "candidatesStart"
    .parameter "candidatesEnd"

    #@0
    .prologue
    .line 194
    iget-object v8, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2
    iget-object v0, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@4
    const/16 v1, 0x5a

    #@6
    move v2, p1

    #@7
    move v3, p2

    #@8
    move v4, p3

    #@9
    move v5, p4

    #@a
    move v6, p5

    #@b
    move v7, p6

    #@c
    invoke-virtual/range {v0 .. v7}, Lcom/android/internal/os/HandlerCaller;->obtainMessageIIIIII(IIIIIII)Landroid/os/Message;

    #@f
    move-result-object v0

    #@10
    invoke-virtual {v8, v0}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V

    #@13
    .line 197
    return-void
.end method

.method public viewClicked(Z)V
    .registers 6
    .parameter "focusChanged"

    #@0
    .prologue
    .line 200
    iget-object v1, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2
    iget-object v2, p0, Landroid/inputmethodservice/IInputMethodSessionWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@4
    const/16 v3, 0x73

    #@6
    if-eqz p1, :cond_11

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    invoke-virtual {v2, v3, v0}, Lcom/android/internal/os/HandlerCaller;->obtainMessageI(II)Landroid/os/Message;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {v1, v0}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V

    #@10
    .line 201
    return-void

    #@11
    .line 200
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_9
.end method
