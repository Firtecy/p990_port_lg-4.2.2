.class public Landroid/inputmethodservice/Keyboard$Key;
.super Ljava/lang/Object;
.source "Keyboard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/inputmethodservice/Keyboard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Key"
.end annotation


# static fields
.field private static final KEY_STATE_NORMAL:[I

.field private static final KEY_STATE_NORMAL_OFF:[I

.field private static final KEY_STATE_NORMAL_ON:[I

.field private static final KEY_STATE_PRESSED:[I

.field private static final KEY_STATE_PRESSED_OFF:[I

.field private static final KEY_STATE_PRESSED_ON:[I


# instance fields
.field public codes:[I

.field public edgeFlags:I

.field public gap:I

.field public height:I

.field public icon:Landroid/graphics/drawable/Drawable;

.field public iconPreview:Landroid/graphics/drawable/Drawable;

.field private keyboard:Landroid/inputmethodservice/Keyboard;

.field public label:Ljava/lang/CharSequence;

.field public modifier:Z

.field public on:Z

.field public popupCharacters:Ljava/lang/CharSequence;

.field public popupResId:I

.field public pressed:Z

.field public repeatable:Z

.field public sticky:Z

.field public text:Ljava/lang/CharSequence;

.field public width:I

.field public x:I

.field public y:I


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 285
    new-array v0, v4, [I

    #@5
    fill-array-data v0, :array_30

    #@8
    sput-object v0, Landroid/inputmethodservice/Keyboard$Key;->KEY_STATE_NORMAL_ON:[I

    #@a
    .line 290
    const/4 v0, 0x3

    #@b
    new-array v0, v0, [I

    #@d
    fill-array-data v0, :array_38

    #@10
    sput-object v0, Landroid/inputmethodservice/Keyboard$Key;->KEY_STATE_PRESSED_ON:[I

    #@12
    .line 296
    new-array v0, v3, [I

    #@14
    const v1, 0x101009f

    #@17
    aput v1, v0, v2

    #@19
    sput-object v0, Landroid/inputmethodservice/Keyboard$Key;->KEY_STATE_NORMAL_OFF:[I

    #@1b
    .line 300
    new-array v0, v4, [I

    #@1d
    fill-array-data v0, :array_42

    #@20
    sput-object v0, Landroid/inputmethodservice/Keyboard$Key;->KEY_STATE_PRESSED_OFF:[I

    #@22
    .line 305
    new-array v0, v2, [I

    #@24
    sput-object v0, Landroid/inputmethodservice/Keyboard$Key;->KEY_STATE_NORMAL:[I

    #@26
    .line 308
    new-array v0, v3, [I

    #@28
    const v1, 0x10100a7

    #@2b
    aput v1, v0, v2

    #@2d
    sput-object v0, Landroid/inputmethodservice/Keyboard$Key;->KEY_STATE_PRESSED:[I

    #@2f
    return-void

    #@30
    .line 285
    :array_30
    .array-data 0x4
        0x9ft 0x0t 0x1t 0x1t
        0xa0t 0x0t 0x1t 0x1t
    .end array-data

    #@38
    .line 290
    :array_38
    .array-data 0x4
        0xa7t 0x0t 0x1t 0x1t
        0x9ft 0x0t 0x1t 0x1t
        0xa0t 0x0t 0x1t 0x1t
    .end array-data

    #@42
    .line 300
    :array_42
    .array-data 0x4
        0xa7t 0x0t 0x1t 0x1t
        0x9ft 0x0t 0x1t 0x1t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/res/Resources;Landroid/inputmethodservice/Keyboard$Row;IILandroid/content/res/XmlResourceParser;)V
    .registers 15
    .parameter "res"
    .parameter "parent"
    .parameter "x"
    .parameter "y"
    .parameter "parser"

    #@0
    .prologue
    const/4 v8, 0x3

    #@1
    const/4 v7, 0x2

    #@2
    const/4 v6, 0x1

    #@3
    const/4 v5, 0x0

    #@4
    .line 331
    invoke-direct {p0, p2}, Landroid/inputmethodservice/Keyboard$Key;-><init>(Landroid/inputmethodservice/Keyboard$Row;)V

    #@7
    .line 333
    iput p3, p0, Landroid/inputmethodservice/Keyboard$Key;->x:I

    #@9
    .line 334
    iput p4, p0, Landroid/inputmethodservice/Keyboard$Key;->y:I

    #@b
    .line 336
    invoke-static {p5}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    #@e
    move-result-object v2

    #@f
    sget-object v3, Lcom/android/internal/R$styleable;->Keyboard:[I

    #@11
    invoke-virtual {p1, v2, v3}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@14
    move-result-object v0

    #@15
    .line 339
    .local v0, a:Landroid/content/res/TypedArray;
    iget-object v2, p0, Landroid/inputmethodservice/Keyboard$Key;->keyboard:Landroid/inputmethodservice/Keyboard;

    #@17
    invoke-static {v2}, Landroid/inputmethodservice/Keyboard;->access$000(Landroid/inputmethodservice/Keyboard;)I

    #@1a
    move-result v2

    #@1b
    iget v3, p2, Landroid/inputmethodservice/Keyboard$Row;->defaultWidth:I

    #@1d
    invoke-static {v0, v5, v2, v3}, Landroid/inputmethodservice/Keyboard;->getDimensionOrFraction(Landroid/content/res/TypedArray;III)I

    #@20
    move-result v2

    #@21
    iput v2, p0, Landroid/inputmethodservice/Keyboard$Key;->width:I

    #@23
    .line 342
    iget-object v2, p0, Landroid/inputmethodservice/Keyboard$Key;->keyboard:Landroid/inputmethodservice/Keyboard;

    #@25
    invoke-static {v2}, Landroid/inputmethodservice/Keyboard;->access$200(Landroid/inputmethodservice/Keyboard;)I

    #@28
    move-result v2

    #@29
    iget v3, p2, Landroid/inputmethodservice/Keyboard$Row;->defaultHeight:I

    #@2b
    invoke-static {v0, v6, v2, v3}, Landroid/inputmethodservice/Keyboard;->getDimensionOrFraction(Landroid/content/res/TypedArray;III)I

    #@2e
    move-result v2

    #@2f
    iput v2, p0, Landroid/inputmethodservice/Keyboard$Key;->height:I

    #@31
    .line 345
    iget-object v2, p0, Landroid/inputmethodservice/Keyboard$Key;->keyboard:Landroid/inputmethodservice/Keyboard;

    #@33
    invoke-static {v2}, Landroid/inputmethodservice/Keyboard;->access$000(Landroid/inputmethodservice/Keyboard;)I

    #@36
    move-result v2

    #@37
    iget v3, p2, Landroid/inputmethodservice/Keyboard$Row;->defaultHorizontalGap:I

    #@39
    invoke-static {v0, v7, v2, v3}, Landroid/inputmethodservice/Keyboard;->getDimensionOrFraction(Landroid/content/res/TypedArray;III)I

    #@3c
    move-result v2

    #@3d
    iput v2, p0, Landroid/inputmethodservice/Keyboard$Key;->gap:I

    #@3f
    .line 348
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@42
    .line 349
    invoke-static {p5}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    #@45
    move-result-object v2

    #@46
    sget-object v3, Lcom/android/internal/R$styleable;->Keyboard_Key:[I

    #@48
    invoke-virtual {p1, v2, v3}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@4b
    move-result-object v0

    #@4c
    .line 351
    iget v2, p0, Landroid/inputmethodservice/Keyboard$Key;->x:I

    #@4e
    iget v3, p0, Landroid/inputmethodservice/Keyboard$Key;->gap:I

    #@50
    add-int/2addr v2, v3

    #@51
    iput v2, p0, Landroid/inputmethodservice/Keyboard$Key;->x:I

    #@53
    .line 352
    new-instance v1, Landroid/util/TypedValue;

    #@55
    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    #@58
    .line 353
    .local v1, codesValue:Landroid/util/TypedValue;
    invoke-virtual {v0, v5, v1}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    #@5b
    .line 355
    iget v2, v1, Landroid/util/TypedValue;->type:I

    #@5d
    const/16 v3, 0x10

    #@5f
    if-eq v2, v3, :cond_67

    #@61
    iget v2, v1, Landroid/util/TypedValue;->type:I

    #@63
    const/16 v3, 0x11

    #@65
    if-ne v2, v3, :cond_102

    #@67
    .line 357
    :cond_67
    new-array v2, v6, [I

    #@69
    iget v3, v1, Landroid/util/TypedValue;->data:I

    #@6b
    aput v3, v2, v5

    #@6d
    iput-object v2, p0, Landroid/inputmethodservice/Keyboard$Key;->codes:[I

    #@6f
    .line 362
    :cond_6f
    :goto_6f
    const/4 v2, 0x7

    #@70
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@73
    move-result-object v2

    #@74
    iput-object v2, p0, Landroid/inputmethodservice/Keyboard$Key;->iconPreview:Landroid/graphics/drawable/Drawable;

    #@76
    .line 363
    iget-object v2, p0, Landroid/inputmethodservice/Keyboard$Key;->iconPreview:Landroid/graphics/drawable/Drawable;

    #@78
    if-eqz v2, :cond_8b

    #@7a
    .line 364
    iget-object v2, p0, Landroid/inputmethodservice/Keyboard$Key;->iconPreview:Landroid/graphics/drawable/Drawable;

    #@7c
    iget-object v3, p0, Landroid/inputmethodservice/Keyboard$Key;->iconPreview:Landroid/graphics/drawable/Drawable;

    #@7e
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@81
    move-result v3

    #@82
    iget-object v4, p0, Landroid/inputmethodservice/Keyboard$Key;->iconPreview:Landroid/graphics/drawable/Drawable;

    #@84
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@87
    move-result v4

    #@88
    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@8b
    .line 367
    :cond_8b
    invoke-virtual {v0, v7}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    #@8e
    move-result-object v2

    #@8f
    iput-object v2, p0, Landroid/inputmethodservice/Keyboard$Key;->popupCharacters:Ljava/lang/CharSequence;

    #@91
    .line 369
    invoke-virtual {v0, v6, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@94
    move-result v2

    #@95
    iput v2, p0, Landroid/inputmethodservice/Keyboard$Key;->popupResId:I

    #@97
    .line 371
    const/4 v2, 0x6

    #@98
    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@9b
    move-result v2

    #@9c
    iput-boolean v2, p0, Landroid/inputmethodservice/Keyboard$Key;->repeatable:Z

    #@9e
    .line 373
    const/4 v2, 0x4

    #@9f
    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@a2
    move-result v2

    #@a3
    iput-boolean v2, p0, Landroid/inputmethodservice/Keyboard$Key;->modifier:Z

    #@a5
    .line 375
    const/4 v2, 0x5

    #@a6
    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@a9
    move-result v2

    #@aa
    iput-boolean v2, p0, Landroid/inputmethodservice/Keyboard$Key;->sticky:Z

    #@ac
    .line 377
    invoke-virtual {v0, v8, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    #@af
    move-result v2

    #@b0
    iput v2, p0, Landroid/inputmethodservice/Keyboard$Key;->edgeFlags:I

    #@b2
    .line 378
    iget v2, p0, Landroid/inputmethodservice/Keyboard$Key;->edgeFlags:I

    #@b4
    iget v3, p2, Landroid/inputmethodservice/Keyboard$Row;->rowEdgeFlags:I

    #@b6
    or-int/2addr v2, v3

    #@b7
    iput v2, p0, Landroid/inputmethodservice/Keyboard$Key;->edgeFlags:I

    #@b9
    .line 380
    const/16 v2, 0xa

    #@bb
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@be
    move-result-object v2

    #@bf
    iput-object v2, p0, Landroid/inputmethodservice/Keyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    #@c1
    .line 382
    iget-object v2, p0, Landroid/inputmethodservice/Keyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    #@c3
    if-eqz v2, :cond_d6

    #@c5
    .line 383
    iget-object v2, p0, Landroid/inputmethodservice/Keyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    #@c7
    iget-object v3, p0, Landroid/inputmethodservice/Keyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    #@c9
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@cc
    move-result v3

    #@cd
    iget-object v4, p0, Landroid/inputmethodservice/Keyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    #@cf
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@d2
    move-result v4

    #@d3
    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@d6
    .line 385
    :cond_d6
    const/16 v2, 0x9

    #@d8
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    #@db
    move-result-object v2

    #@dc
    iput-object v2, p0, Landroid/inputmethodservice/Keyboard$Key;->label:Ljava/lang/CharSequence;

    #@de
    .line 386
    const/16 v2, 0x8

    #@e0
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    #@e3
    move-result-object v2

    #@e4
    iput-object v2, p0, Landroid/inputmethodservice/Keyboard$Key;->text:Ljava/lang/CharSequence;

    #@e6
    .line 388
    iget-object v2, p0, Landroid/inputmethodservice/Keyboard$Key;->codes:[I

    #@e8
    if-nez v2, :cond_fe

    #@ea
    iget-object v2, p0, Landroid/inputmethodservice/Keyboard$Key;->label:Ljava/lang/CharSequence;

    #@ec
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@ef
    move-result v2

    #@f0
    if-nez v2, :cond_fe

    #@f2
    .line 389
    new-array v2, v6, [I

    #@f4
    iget-object v3, p0, Landroid/inputmethodservice/Keyboard$Key;->label:Ljava/lang/CharSequence;

    #@f6
    invoke-interface {v3, v5}, Ljava/lang/CharSequence;->charAt(I)C

    #@f9
    move-result v3

    #@fa
    aput v3, v2, v5

    #@fc
    iput-object v2, p0, Landroid/inputmethodservice/Keyboard$Key;->codes:[I

    #@fe
    .line 391
    :cond_fe
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@101
    .line 392
    return-void

    #@102
    .line 358
    :cond_102
    iget v2, v1, Landroid/util/TypedValue;->type:I

    #@104
    if-ne v2, v8, :cond_6f

    #@106
    .line 359
    iget-object v2, v1, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    #@108
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@10b
    move-result-object v2

    #@10c
    invoke-virtual {p0, v2}, Landroid/inputmethodservice/Keyboard$Key;->parseCSV(Ljava/lang/String;)[I

    #@10f
    move-result-object v2

    #@110
    iput-object v2, p0, Landroid/inputmethodservice/Keyboard$Key;->codes:[I

    #@112
    goto/16 :goto_6f
.end method

.method public constructor <init>(Landroid/inputmethodservice/Keyboard$Row;)V
    .registers 3
    .parameter "parent"

    #@0
    .prologue
    .line 313
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 314
    invoke-static {p1}, Landroid/inputmethodservice/Keyboard$Row;->access$600(Landroid/inputmethodservice/Keyboard$Row;)Landroid/inputmethodservice/Keyboard;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/inputmethodservice/Keyboard$Key;->keyboard:Landroid/inputmethodservice/Keyboard;

    #@9
    .line 315
    iget v0, p1, Landroid/inputmethodservice/Keyboard$Row;->defaultHeight:I

    #@b
    iput v0, p0, Landroid/inputmethodservice/Keyboard$Key;->height:I

    #@d
    .line 316
    iget v0, p1, Landroid/inputmethodservice/Keyboard$Row;->defaultWidth:I

    #@f
    iput v0, p0, Landroid/inputmethodservice/Keyboard$Key;->width:I

    #@11
    .line 317
    iget v0, p1, Landroid/inputmethodservice/Keyboard$Row;->defaultHorizontalGap:I

    #@13
    iput v0, p0, Landroid/inputmethodservice/Keyboard$Key;->gap:I

    #@15
    .line 318
    iget v0, p1, Landroid/inputmethodservice/Keyboard$Row;->rowEdgeFlags:I

    #@17
    iput v0, p0, Landroid/inputmethodservice/Keyboard$Key;->edgeFlags:I

    #@19
    .line 319
    return-void
.end method


# virtual methods
.method public getCurrentDrawableState()[I
    .registers 3

    #@0
    .prologue
    .line 479
    sget-object v0, Landroid/inputmethodservice/Keyboard$Key;->KEY_STATE_NORMAL:[I

    #@2
    .line 481
    .local v0, states:[I
    iget-boolean v1, p0, Landroid/inputmethodservice/Keyboard$Key;->on:Z

    #@4
    if-eqz v1, :cond_10

    #@6
    .line 482
    iget-boolean v1, p0, Landroid/inputmethodservice/Keyboard$Key;->pressed:Z

    #@8
    if-eqz v1, :cond_d

    #@a
    .line 483
    sget-object v0, Landroid/inputmethodservice/Keyboard$Key;->KEY_STATE_PRESSED_ON:[I

    #@c
    .line 500
    :cond_c
    :goto_c
    return-object v0

    #@d
    .line 485
    :cond_d
    sget-object v0, Landroid/inputmethodservice/Keyboard$Key;->KEY_STATE_NORMAL_ON:[I

    #@f
    goto :goto_c

    #@10
    .line 488
    :cond_10
    iget-boolean v1, p0, Landroid/inputmethodservice/Keyboard$Key;->sticky:Z

    #@12
    if-eqz v1, :cond_1e

    #@14
    .line 489
    iget-boolean v1, p0, Landroid/inputmethodservice/Keyboard$Key;->pressed:Z

    #@16
    if-eqz v1, :cond_1b

    #@18
    .line 490
    sget-object v0, Landroid/inputmethodservice/Keyboard$Key;->KEY_STATE_PRESSED_OFF:[I

    #@1a
    goto :goto_c

    #@1b
    .line 492
    :cond_1b
    sget-object v0, Landroid/inputmethodservice/Keyboard$Key;->KEY_STATE_NORMAL_OFF:[I

    #@1d
    goto :goto_c

    #@1e
    .line 495
    :cond_1e
    iget-boolean v1, p0, Landroid/inputmethodservice/Keyboard$Key;->pressed:Z

    #@20
    if-eqz v1, :cond_c

    #@22
    .line 496
    sget-object v0, Landroid/inputmethodservice/Keyboard$Key;->KEY_STATE_PRESSED:[I

    #@24
    goto :goto_c
.end method

.method public isInside(II)Z
    .registers 11
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 447
    iget v6, p0, Landroid/inputmethodservice/Keyboard$Key;->edgeFlags:I

    #@4
    and-int/lit8 v6, v6, 0x1

    #@6
    if-lez v6, :cond_53

    #@8
    move v1, v4

    #@9
    .line 448
    .local v1, leftEdge:Z
    :goto_9
    iget v6, p0, Landroid/inputmethodservice/Keyboard$Key;->edgeFlags:I

    #@b
    and-int/lit8 v6, v6, 0x2

    #@d
    if-lez v6, :cond_55

    #@f
    move v2, v4

    #@10
    .line 449
    .local v2, rightEdge:Z
    :goto_10
    iget v6, p0, Landroid/inputmethodservice/Keyboard$Key;->edgeFlags:I

    #@12
    and-int/lit8 v6, v6, 0x4

    #@14
    if-lez v6, :cond_57

    #@16
    move v3, v4

    #@17
    .line 450
    .local v3, topEdge:Z
    :goto_17
    iget v6, p0, Landroid/inputmethodservice/Keyboard$Key;->edgeFlags:I

    #@19
    and-int/lit8 v6, v6, 0x8

    #@1b
    if-lez v6, :cond_59

    #@1d
    move v0, v4

    #@1e
    .line 451
    .local v0, bottomEdge:Z
    :goto_1e
    iget v6, p0, Landroid/inputmethodservice/Keyboard$Key;->x:I

    #@20
    if-ge p1, v6, :cond_2b

    #@22
    if-eqz v1, :cond_5b

    #@24
    iget v6, p0, Landroid/inputmethodservice/Keyboard$Key;->x:I

    #@26
    iget v7, p0, Landroid/inputmethodservice/Keyboard$Key;->width:I

    #@28
    add-int/2addr v6, v7

    #@29
    if-gt p1, v6, :cond_5b

    #@2b
    :cond_2b
    iget v6, p0, Landroid/inputmethodservice/Keyboard$Key;->x:I

    #@2d
    iget v7, p0, Landroid/inputmethodservice/Keyboard$Key;->width:I

    #@2f
    add-int/2addr v6, v7

    #@30
    if-lt p1, v6, :cond_38

    #@32
    if-eqz v2, :cond_5b

    #@34
    iget v6, p0, Landroid/inputmethodservice/Keyboard$Key;->x:I

    #@36
    if-lt p1, v6, :cond_5b

    #@38
    :cond_38
    iget v6, p0, Landroid/inputmethodservice/Keyboard$Key;->y:I

    #@3a
    if-ge p2, v6, :cond_45

    #@3c
    if-eqz v3, :cond_5b

    #@3e
    iget v6, p0, Landroid/inputmethodservice/Keyboard$Key;->y:I

    #@40
    iget v7, p0, Landroid/inputmethodservice/Keyboard$Key;->height:I

    #@42
    add-int/2addr v6, v7

    #@43
    if-gt p2, v6, :cond_5b

    #@45
    :cond_45
    iget v6, p0, Landroid/inputmethodservice/Keyboard$Key;->y:I

    #@47
    iget v7, p0, Landroid/inputmethodservice/Keyboard$Key;->height:I

    #@49
    add-int/2addr v6, v7

    #@4a
    if-lt p2, v6, :cond_52

    #@4c
    if-eqz v0, :cond_5b

    #@4e
    iget v6, p0, Landroid/inputmethodservice/Keyboard$Key;->y:I

    #@50
    if-lt p2, v6, :cond_5b

    #@52
    .line 457
    :cond_52
    :goto_52
    return v4

    #@53
    .end local v0           #bottomEdge:Z
    .end local v1           #leftEdge:Z
    .end local v2           #rightEdge:Z
    .end local v3           #topEdge:Z
    :cond_53
    move v1, v5

    #@54
    .line 447
    goto :goto_9

    #@55
    .restart local v1       #leftEdge:Z
    :cond_55
    move v2, v5

    #@56
    .line 448
    goto :goto_10

    #@57
    .restart local v2       #rightEdge:Z
    :cond_57
    move v3, v5

    #@58
    .line 449
    goto :goto_17

    #@59
    .restart local v3       #topEdge:Z
    :cond_59
    move v0, v5

    #@5a
    .line 450
    goto :goto_1e

    #@5b
    .restart local v0       #bottomEdge:Z
    :cond_5b
    move v4, v5

    #@5c
    .line 457
    goto :goto_52
.end method

.method public onPressed()V
    .registers 2

    #@0
    .prologue
    .line 400
    iget-boolean v0, p0, Landroid/inputmethodservice/Keyboard$Key;->pressed:Z

    #@2
    if-nez v0, :cond_8

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    iput-boolean v0, p0, Landroid/inputmethodservice/Keyboard$Key;->pressed:Z

    #@7
    .line 401
    return-void

    #@8
    .line 400
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_5
.end method

.method public onReleased(Z)V
    .registers 5
    .parameter "inside"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 410
    iget-boolean v0, p0, Landroid/inputmethodservice/Keyboard$Key;->pressed:Z

    #@4
    if-nez v0, :cond_14

    #@6
    move v0, v1

    #@7
    :goto_7
    iput-boolean v0, p0, Landroid/inputmethodservice/Keyboard$Key;->pressed:Z

    #@9
    .line 411
    iget-boolean v0, p0, Landroid/inputmethodservice/Keyboard$Key;->sticky:Z

    #@b
    if-eqz v0, :cond_13

    #@d
    .line 412
    iget-boolean v0, p0, Landroid/inputmethodservice/Keyboard$Key;->on:Z

    #@f
    if-nez v0, :cond_16

    #@11
    :goto_11
    iput-boolean v1, p0, Landroid/inputmethodservice/Keyboard$Key;->on:Z

    #@13
    .line 414
    :cond_13
    return-void

    #@14
    :cond_14
    move v0, v2

    #@15
    .line 410
    goto :goto_7

    #@16
    :cond_16
    move v1, v2

    #@17
    .line 412
    goto :goto_11
.end method

.method parseCSV(Ljava/lang/String;)[I
    .registers 11
    .parameter "value"

    #@0
    .prologue
    .line 417
    const/4 v0, 0x0

    #@1
    .line 418
    .local v0, count:I
    const/4 v2, 0x0

    #@2
    .line 419
    .local v2, lastIndex:I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@5
    move-result v6

    #@6
    if-lez v6, :cond_17

    #@8
    .line 420
    add-int/lit8 v0, v0, 0x1

    #@a
    .line 421
    :goto_a
    const-string v6, ","

    #@c
    add-int/lit8 v7, v2, 0x1

    #@e
    invoke-virtual {p1, v6, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    #@11
    move-result v2

    #@12
    if-lez v2, :cond_17

    #@14
    .line 422
    add-int/lit8 v0, v0, 0x1

    #@16
    goto :goto_a

    #@17
    .line 425
    :cond_17
    new-array v5, v0, [I

    #@19
    .line 426
    .local v5, values:[I
    const/4 v0, 0x0

    #@1a
    .line 427
    new-instance v4, Ljava/util/StringTokenizer;

    #@1c
    const-string v6, ","

    #@1e
    invoke-direct {v4, p1, v6}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@21
    .line 428
    .local v4, st:Ljava/util/StringTokenizer;
    :goto_21
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    #@24
    move-result v6

    #@25
    if-eqz v6, :cond_50

    #@27
    .line 430
    add-int/lit8 v1, v0, 0x1

    #@29
    .end local v0           #count:I
    .local v1, count:I
    :try_start_29
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@2c
    move-result-object v6

    #@2d
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@30
    move-result v6

    #@31
    aput v6, v5, v0
    :try_end_33
    .catch Ljava/lang/NumberFormatException; {:try_start_29 .. :try_end_33} :catch_35

    #@33
    move v0, v1

    #@34
    .line 433
    .end local v1           #count:I
    .restart local v0       #count:I
    goto :goto_21

    #@35
    .line 431
    .end local v0           #count:I
    .restart local v1       #count:I
    :catch_35
    move-exception v3

    #@36
    .line 432
    .local v3, nfe:Ljava/lang/NumberFormatException;
    const-string v6, "Keyboard"

    #@38
    new-instance v7, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v8, "Error parsing keycodes "

    #@3f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v7

    #@43
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v7

    #@47
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v7

    #@4b
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    move v0, v1

    #@4f
    .line 433
    .end local v1           #count:I
    .restart local v0       #count:I
    goto :goto_21

    #@50
    .line 435
    .end local v3           #nfe:Ljava/lang/NumberFormatException;
    :cond_50
    return-object v5
.end method

.method public squaredDistanceFrom(II)I
    .registers 7
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 468
    iget v2, p0, Landroid/inputmethodservice/Keyboard$Key;->x:I

    #@2
    iget v3, p0, Landroid/inputmethodservice/Keyboard$Key;->width:I

    #@4
    div-int/lit8 v3, v3, 0x2

    #@6
    add-int/2addr v2, v3

    #@7
    sub-int v0, v2, p1

    #@9
    .line 469
    .local v0, xDist:I
    iget v2, p0, Landroid/inputmethodservice/Keyboard$Key;->y:I

    #@b
    iget v3, p0, Landroid/inputmethodservice/Keyboard$Key;->height:I

    #@d
    div-int/lit8 v3, v3, 0x2

    #@f
    add-int/2addr v2, v3

    #@10
    sub-int v1, v2, p2

    #@12
    .line 470
    .local v1, yDist:I
    mul-int v2, v0, v0

    #@14
    mul-int v3, v1, v1

    #@16
    add-int/2addr v2, v3

    #@17
    return v2
.end method
