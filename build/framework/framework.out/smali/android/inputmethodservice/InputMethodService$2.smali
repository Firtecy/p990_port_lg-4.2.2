.class Landroid/inputmethodservice/InputMethodService$2;
.super Ljava/lang/Object;
.source "InputMethodService.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/inputmethodservice/InputMethodService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/inputmethodservice/InputMethodService;


# direct methods
.method constructor <init>(Landroid/inputmethodservice/InputMethodService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 329
    iput-object p1, p0, Landroid/inputmethodservice/InputMethodService$2;->this$0:Landroid/inputmethodservice/InputMethodService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 6
    .parameter "v"

    #@0
    .prologue
    .line 331
    iget-object v2, p0, Landroid/inputmethodservice/InputMethodService$2;->this$0:Landroid/inputmethodservice/InputMethodService;

    #@2
    invoke-virtual {v2}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;

    #@5
    move-result-object v0

    #@6
    .line 332
    .local v0, ei:Landroid/view/inputmethod/EditorInfo;
    iget-object v2, p0, Landroid/inputmethodservice/InputMethodService$2;->this$0:Landroid/inputmethodservice/InputMethodService;

    #@8
    invoke-virtual {v2}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    #@b
    move-result-object v1

    #@c
    .line 333
    .local v1, ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v0, :cond_19

    #@e
    if-eqz v1, :cond_19

    #@10
    .line 334
    iget v2, v0, Landroid/view/inputmethod/EditorInfo;->actionId:I

    #@12
    if-eqz v2, :cond_1a

    #@14
    .line 335
    iget v2, v0, Landroid/view/inputmethod/EditorInfo;->actionId:I

    #@16
    invoke-interface {v1, v2}, Landroid/view/inputmethod/InputConnection;->performEditorAction(I)Z

    #@19
    .line 341
    :cond_19
    :goto_19
    return-void

    #@1a
    .line 336
    :cond_1a
    iget v2, v0, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    #@1c
    and-int/lit16 v2, v2, 0xff

    #@1e
    const/4 v3, 0x1

    #@1f
    if-eq v2, v3, :cond_19

    #@21
    .line 338
    iget v2, v0, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    #@23
    and-int/lit16 v2, v2, 0xff

    #@25
    invoke-interface {v1, v2}, Landroid/view/inputmethod/InputConnection;->performEditorAction(I)Z

    #@28
    goto :goto_19
.end method
