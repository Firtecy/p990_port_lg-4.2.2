.class Landroid/inputmethodservice/IInputMethodWrapper;
.super Lcom/android/internal/view/IInputMethod$Stub;
.source "IInputMethodWrapper.java"

# interfaces
.implements Lcom/android/internal/os/HandlerCaller$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/inputmethodservice/IInputMethodWrapper$InputMethodSessionCallbackWrapper;,
        Landroid/inputmethodservice/IInputMethodWrapper$Notifier;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final DO_ATTACH_TOKEN:I = 0xa

.field private static final DO_CHANGE_INPUTMETHOD_SUBTYPE:I = 0x50

.field private static final DO_CREATE_SESSION:I = 0x28

.field private static final DO_DUMP:I = 0x1

.field private static final DO_HIDE_SOFT_INPUT:I = 0x46

.field private static final DO_RESTART_INPUT:I = 0x22

.field private static final DO_REVOKE_SESSION:I = 0x32

.field private static final DO_SET_INPUT_CONTEXT:I = 0x14

.field private static final DO_SET_SESSION_ENABLED:I = 0x2d

.field private static final DO_SHOW_SOFT_INPUT:I = 0x3c

.field private static final DO_START_INPUT:I = 0x20

.field private static final DO_UNSET_INPUT_CONTEXT:I = 0x1e

.field private static final TAG:Ljava/lang/String; = "InputMethodWrapper"


# instance fields
.field final mCaller:Lcom/android/internal/os/HandlerCaller;

.field final mInputMethod:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/inputmethod/InputMethod;",
            ">;"
        }
    .end annotation
.end field

.field final mTarget:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/inputmethodservice/AbstractInputMethodService;",
            ">;"
        }
    .end annotation
.end field

.field final mTargetSdkVersion:I


# direct methods
.method public constructor <init>(Landroid/inputmethodservice/AbstractInputMethodService;Landroid/view/inputmethod/InputMethod;)V
    .registers 5
    .parameter "context"
    .parameter "inputMethod"

    #@0
    .prologue
    .line 103
    invoke-direct {p0}, Lcom/android/internal/view/IInputMethod$Stub;-><init>()V

    #@3
    .line 104
    new-instance v0, Ljava/lang/ref/WeakReference;

    #@5
    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@8
    iput-object v0, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mTarget:Ljava/lang/ref/WeakReference;

    #@a
    .line 105
    new-instance v0, Lcom/android/internal/os/HandlerCaller;

    #@c
    invoke-virtual {p1}, Landroid/inputmethodservice/AbstractInputMethodService;->getApplicationContext()Landroid/content/Context;

    #@f
    move-result-object v1

    #@10
    invoke-direct {v0, v1, p0}, Lcom/android/internal/os/HandlerCaller;-><init>(Landroid/content/Context;Lcom/android/internal/os/HandlerCaller$Callback;)V

    #@13
    iput-object v0, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@15
    .line 106
    new-instance v0, Ljava/lang/ref/WeakReference;

    #@17
    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@1a
    iput-object v0, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mInputMethod:Ljava/lang/ref/WeakReference;

    #@1c
    .line 107
    invoke-virtual {p1}, Landroid/inputmethodservice/AbstractInputMethodService;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@1f
    move-result-object v0

    #@20
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@22
    iput v0, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mTargetSdkVersion:I

    #@24
    .line 108
    return-void
.end method


# virtual methods
.method public attachToken(Landroid/os/IBinder;)V
    .registers 5
    .parameter "token"

    #@0
    .prologue
    .line 227
    iget-object v0, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2
    iget-object v1, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@4
    const/16 v2, 0xa

    #@6
    invoke-virtual {v1, v2, p1}, Lcom/android/internal/os/HandlerCaller;->obtainMessageO(ILjava/lang/Object;)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V

    #@d
    .line 228
    return-void
.end method

.method public bindInput(Landroid/view/inputmethod/InputBinding;)V
    .registers 7
    .parameter "binding"

    #@0
    .prologue
    .line 231
    new-instance v0, Lcom/android/internal/view/InputConnectionWrapper;

    #@2
    invoke-virtual {p1}, Landroid/view/inputmethod/InputBinding;->getConnectionToken()Landroid/os/IBinder;

    #@5
    move-result-object v2

    #@6
    invoke-static {v2}, Lcom/android/internal/view/IInputContext$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputContext;

    #@9
    move-result-object v2

    #@a
    invoke-direct {v0, v2}, Lcom/android/internal/view/InputConnectionWrapper;-><init>(Lcom/android/internal/view/IInputContext;)V

    #@d
    .line 233
    .local v0, ic:Landroid/view/inputmethod/InputConnection;
    new-instance v1, Landroid/view/inputmethod/InputBinding;

    #@f
    invoke-direct {v1, v0, p1}, Landroid/view/inputmethod/InputBinding;-><init>(Landroid/view/inputmethod/InputConnection;Landroid/view/inputmethod/InputBinding;)V

    #@12
    .line 234
    .local v1, nu:Landroid/view/inputmethod/InputBinding;
    iget-object v2, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@14
    iget-object v3, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@16
    const/16 v4, 0x14

    #@18
    invoke-virtual {v3, v4, v1}, Lcom/android/internal/os/HandlerCaller;->obtainMessageO(ILjava/lang/Object;)Landroid/os/Message;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v2, v3}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V

    #@1f
    .line 235
    return-void
.end method

.method public changeInputMethodSubtype(Landroid/view/inputmethod/InputMethodSubtype;)V
    .registers 5
    .parameter "subtype"

    #@0
    .prologue
    .line 287
    iget-object v0, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2
    iget-object v1, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@4
    const/16 v2, 0x50

    #@6
    invoke-virtual {v1, v2, p1}, Lcom/android/internal/os/HandlerCaller;->obtainMessageO(ILjava/lang/Object;)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V

    #@d
    .line 289
    return-void
.end method

.method public createSession(Lcom/android/internal/view/IInputMethodCallback;)V
    .registers 5
    .parameter "callback"

    #@0
    .prologue
    .line 252
    iget-object v0, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2
    iget-object v1, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@4
    const/16 v2, 0x28

    #@6
    invoke-virtual {v1, v2, p1}, Lcom/android/internal/os/HandlerCaller;->obtainMessageO(ILjava/lang/Object;)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V

    #@d
    .line 253
    return-void
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 13
    .parameter "fd"
    .parameter "fout"
    .parameter "args"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 201
    iget-object v0, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mTarget:Ljava/lang/ref/WeakReference;

    #@3
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@6
    move-result-object v7

    #@7
    check-cast v7, Landroid/inputmethodservice/AbstractInputMethodService;

    #@9
    .line 202
    .local v7, target:Landroid/inputmethodservice/AbstractInputMethodService;
    if-nez v7, :cond_c

    #@b
    .line 224
    :cond_b
    :goto_b
    return-void

    #@c
    .line 205
    :cond_c
    const-string v0, "android.permission.DUMP"

    #@e
    invoke-virtual {v7, v0}, Landroid/inputmethodservice/AbstractInputMethodService;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_3d

    #@14
    .line 208
    new-instance v0, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v1, "Permission Denial: can\'t dump InputMethodManager from from pid="

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@22
    move-result v1

    #@23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v0

    #@27
    const-string v1, ", uid="

    #@29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v0

    #@2d
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@30
    move-result v1

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v0

    #@39
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3c
    goto :goto_b

    #@3d
    .line 214
    :cond_3d
    new-instance v5, Ljava/util/concurrent/CountDownLatch;

    #@3f
    invoke-direct {v5, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    #@42
    .line 215
    .local v5, latch:Ljava/util/concurrent/CountDownLatch;
    iget-object v8, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@44
    iget-object v0, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@46
    move-object v2, p1

    #@47
    move-object v3, p2

    #@48
    move-object v4, p3

    #@49
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/os/HandlerCaller;->obtainMessageOOOO(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Landroid/os/Message;

    #@4c
    move-result-object v0

    #@4d
    invoke-virtual {v8, v0}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V

    #@50
    .line 218
    const-wide/16 v0, 0x5

    #@52
    :try_start_52
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    #@54
    invoke-virtual {v5, v0, v1, v2}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    #@57
    move-result v0

    #@58
    if-nez v0, :cond_b

    #@5a
    .line 219
    const-string v0, "Timeout waiting for dump"

    #@5c
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_5f
    .catch Ljava/lang/InterruptedException; {:try_start_52 .. :try_end_5f} :catch_60

    #@5f
    goto :goto_b

    #@60
    .line 221
    :catch_60
    move-exception v6

    #@61
    .line 222
    .local v6, e:Ljava/lang/InterruptedException;
    const-string v0, "Interrupted waiting for dump"

    #@63
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@66
    goto :goto_b
.end method

.method public executeMessage(Landroid/os/Message;)V
    .registers 12
    .parameter "msg"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v8, 0x1

    #@2
    .line 115
    iget-object v7, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mInputMethod:Ljava/lang/ref/WeakReference;

    #@4
    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@7
    move-result-object v5

    #@8
    check-cast v5, Landroid/view/inputmethod/InputMethod;

    #@a
    .line 117
    .local v5, inputMethod:Landroid/view/inputmethod/InputMethod;
    if-nez v5, :cond_2b

    #@c
    iget v7, p1, Landroid/os/Message;->what:I

    #@e
    if-eq v7, v8, :cond_2b

    #@10
    .line 118
    const-string v7, "InputMethodWrapper"

    #@12
    new-instance v8, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v9, "Input method reference was null, ignoring message: "

    #@19
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v8

    #@1d
    iget v9, p1, Landroid/os/Message;->what:I

    #@1f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v8

    #@23
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v8

    #@27
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 198
    :cond_2a
    :goto_2a
    return-void

    #@2b
    .line 122
    :cond_2b
    iget v7, p1, Landroid/os/Message;->what:I

    #@2d
    sparse-switch v7, :sswitch_data_136

    #@30
    .line 197
    const-string v7, "InputMethodWrapper"

    #@32
    new-instance v8, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v9, "Unhandled message code: "

    #@39
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v8

    #@3d
    iget v9, p1, Landroid/os/Message;->what:I

    #@3f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    move-result-object v8

    #@43
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v8

    #@47
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    goto :goto_2a

    #@4b
    .line 124
    :sswitch_4b
    iget-object v7, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mTarget:Ljava/lang/ref/WeakReference;

    #@4d
    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@50
    move-result-object v6

    #@51
    check-cast v6, Landroid/inputmethodservice/AbstractInputMethodService;

    #@53
    .line 125
    .local v6, target:Landroid/inputmethodservice/AbstractInputMethodService;
    if-eqz v6, :cond_2a

    #@55
    .line 128
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@57
    check-cast v0, Lcom/android/internal/os/SomeArgs;

    #@59
    .line 130
    .local v0, args:Lcom/android/internal/os/SomeArgs;
    :try_start_59
    iget-object v7, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@5b
    check-cast v7, Ljava/io/FileDescriptor;

    #@5d
    iget-object v8, v0, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    #@5f
    check-cast v8, Ljava/io/PrintWriter;

    #@61
    iget-object v9, v0, Lcom/android/internal/os/SomeArgs;->arg3:Ljava/lang/Object;

    #@63
    check-cast v9, [Ljava/lang/String;

    #@65
    check-cast v9, [Ljava/lang/String;

    #@67
    invoke-virtual {v6, v7, v8, v9}, Landroid/inputmethodservice/AbstractInputMethodService;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_6a
    .catch Ljava/lang/RuntimeException; {:try_start_59 .. :try_end_6a} :catch_79

    #@6a
    .line 135
    :goto_6a
    iget-object v8, v0, Lcom/android/internal/os/SomeArgs;->arg4:Ljava/lang/Object;

    #@6c
    monitor-enter v8

    #@6d
    .line 136
    :try_start_6d
    iget-object v7, v0, Lcom/android/internal/os/SomeArgs;->arg4:Ljava/lang/Object;

    #@6f
    check-cast v7, Ljava/util/concurrent/CountDownLatch;

    #@71
    invoke-virtual {v7}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    #@74
    .line 137
    monitor-exit v8
    :try_end_75
    .catchall {:try_start_6d .. :try_end_75} :catchall_95

    #@75
    .line 138
    invoke-virtual {v0}, Lcom/android/internal/os/SomeArgs;->recycle()V

    #@78
    goto :goto_2a

    #@79
    .line 132
    :catch_79
    move-exception v1

    #@7a
    .line 133
    .local v1, e:Ljava/lang/RuntimeException;
    iget-object v7, v0, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    #@7c
    check-cast v7, Ljava/io/PrintWriter;

    #@7e
    new-instance v8, Ljava/lang/StringBuilder;

    #@80
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@83
    const-string v9, "Exception: "

    #@85
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v8

    #@89
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v8

    #@8d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@90
    move-result-object v8

    #@91
    invoke-virtual {v7, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@94
    goto :goto_6a

    #@95
    .line 137
    .end local v1           #e:Ljava/lang/RuntimeException;
    :catchall_95
    move-exception v7

    #@96
    :try_start_96
    monitor-exit v8
    :try_end_97
    .catchall {:try_start_96 .. :try_end_97} :catchall_95

    #@97
    throw v7

    #@98
    .line 143
    .end local v0           #args:Lcom/android/internal/os/SomeArgs;
    .end local v6           #target:Landroid/inputmethodservice/AbstractInputMethodService;
    :sswitch_98
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9a
    check-cast v7, Landroid/os/IBinder;

    #@9c
    invoke-interface {v5, v7}, Landroid/view/inputmethod/InputMethod;->attachToken(Landroid/os/IBinder;)V

    #@9f
    goto :goto_2a

    #@a0
    .line 147
    :sswitch_a0
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a2
    check-cast v7, Landroid/view/inputmethod/InputBinding;

    #@a4
    invoke-interface {v5, v7}, Landroid/view/inputmethod/InputMethod;->bindInput(Landroid/view/inputmethod/InputBinding;)V

    #@a7
    goto :goto_2a

    #@a8
    .line 151
    :sswitch_a8
    invoke-interface {v5}, Landroid/view/inputmethod/InputMethod;->unbindInput()V

    #@ab
    goto/16 :goto_2a

    #@ad
    .line 154
    :sswitch_ad
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@af
    check-cast v0, Lcom/android/internal/os/SomeArgs;

    #@b1
    .line 155
    .restart local v0       #args:Lcom/android/internal/os/SomeArgs;
    iget-object v4, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@b3
    check-cast v4, Lcom/android/internal/view/IInputContext;

    #@b5
    .line 156
    .local v4, inputContext:Lcom/android/internal/view/IInputContext;
    if-eqz v4, :cond_bc

    #@b7
    new-instance v2, Lcom/android/internal/view/InputConnectionWrapper;

    #@b9
    invoke-direct {v2, v4}, Lcom/android/internal/view/InputConnectionWrapper;-><init>(Lcom/android/internal/view/IInputContext;)V

    #@bc
    .line 158
    .local v2, ic:Landroid/view/inputmethod/InputConnection;
    :cond_bc
    iget-object v3, v0, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    #@be
    check-cast v3, Landroid/view/inputmethod/EditorInfo;

    #@c0
    .line 159
    .local v3, info:Landroid/view/inputmethod/EditorInfo;
    iget v7, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mTargetSdkVersion:I

    #@c2
    invoke-virtual {v3, v7}, Landroid/view/inputmethod/EditorInfo;->makeCompatible(I)V

    #@c5
    .line 160
    invoke-interface {v5, v2, v3}, Landroid/view/inputmethod/InputMethod;->startInput(Landroid/view/inputmethod/InputConnection;Landroid/view/inputmethod/EditorInfo;)V

    #@c8
    .line 161
    invoke-virtual {v0}, Lcom/android/internal/os/SomeArgs;->recycle()V

    #@cb
    goto/16 :goto_2a

    #@cd
    .line 165
    .end local v0           #args:Lcom/android/internal/os/SomeArgs;
    .end local v2           #ic:Landroid/view/inputmethod/InputConnection;
    .end local v3           #info:Landroid/view/inputmethod/EditorInfo;
    .end local v4           #inputContext:Lcom/android/internal/view/IInputContext;
    :sswitch_cd
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@cf
    check-cast v0, Lcom/android/internal/os/SomeArgs;

    #@d1
    .line 166
    .restart local v0       #args:Lcom/android/internal/os/SomeArgs;
    iget-object v4, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@d3
    check-cast v4, Lcom/android/internal/view/IInputContext;

    #@d5
    .line 167
    .restart local v4       #inputContext:Lcom/android/internal/view/IInputContext;
    if-eqz v4, :cond_dc

    #@d7
    new-instance v2, Lcom/android/internal/view/InputConnectionWrapper;

    #@d9
    invoke-direct {v2, v4}, Lcom/android/internal/view/InputConnectionWrapper;-><init>(Lcom/android/internal/view/IInputContext;)V

    #@dc
    .line 169
    .restart local v2       #ic:Landroid/view/inputmethod/InputConnection;
    :cond_dc
    iget-object v3, v0, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    #@de
    check-cast v3, Landroid/view/inputmethod/EditorInfo;

    #@e0
    .line 170
    .restart local v3       #info:Landroid/view/inputmethod/EditorInfo;
    iget v7, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mTargetSdkVersion:I

    #@e2
    invoke-virtual {v3, v7}, Landroid/view/inputmethod/EditorInfo;->makeCompatible(I)V

    #@e5
    .line 171
    invoke-interface {v5, v2, v3}, Landroid/view/inputmethod/InputMethod;->restartInput(Landroid/view/inputmethod/InputConnection;Landroid/view/inputmethod/EditorInfo;)V

    #@e8
    .line 172
    invoke-virtual {v0}, Lcom/android/internal/os/SomeArgs;->recycle()V

    #@eb
    goto/16 :goto_2a

    #@ed
    .line 176
    .end local v0           #args:Lcom/android/internal/os/SomeArgs;
    .end local v2           #ic:Landroid/view/inputmethod/InputConnection;
    .end local v3           #info:Landroid/view/inputmethod/EditorInfo;
    .end local v4           #inputContext:Lcom/android/internal/view/IInputContext;
    :sswitch_ed
    new-instance v8, Landroid/inputmethodservice/IInputMethodWrapper$InputMethodSessionCallbackWrapper;

    #@ef
    iget-object v7, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@f1
    iget-object v9, v7, Lcom/android/internal/os/HandlerCaller;->mContext:Landroid/content/Context;

    #@f3
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@f5
    check-cast v7, Lcom/android/internal/view/IInputMethodCallback;

    #@f7
    invoke-direct {v8, v9, v7}, Landroid/inputmethodservice/IInputMethodWrapper$InputMethodSessionCallbackWrapper;-><init>(Landroid/content/Context;Lcom/android/internal/view/IInputMethodCallback;)V

    #@fa
    invoke-interface {v5, v8}, Landroid/view/inputmethod/InputMethod;->createSession(Landroid/view/inputmethod/InputMethod$SessionCallback;)V

    #@fd
    goto/16 :goto_2a

    #@ff
    .line 181
    :sswitch_ff
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@101
    check-cast v7, Landroid/view/inputmethod/InputMethodSession;

    #@103
    iget v9, p1, Landroid/os/Message;->arg1:I

    #@105
    if-eqz v9, :cond_10c

    #@107
    :goto_107
    invoke-interface {v5, v7, v8}, Landroid/view/inputmethod/InputMethod;->setSessionEnabled(Landroid/view/inputmethod/InputMethodSession;Z)V

    #@10a
    goto/16 :goto_2a

    #@10c
    :cond_10c
    const/4 v8, 0x0

    #@10d
    goto :goto_107

    #@10e
    .line 185
    :sswitch_10e
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@110
    check-cast v7, Landroid/view/inputmethod/InputMethodSession;

    #@112
    invoke-interface {v5, v7}, Landroid/view/inputmethod/InputMethod;->revokeSession(Landroid/view/inputmethod/InputMethodSession;)V

    #@115
    goto/16 :goto_2a

    #@117
    .line 188
    :sswitch_117
    iget v8, p1, Landroid/os/Message;->arg1:I

    #@119
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@11b
    check-cast v7, Landroid/os/ResultReceiver;

    #@11d
    invoke-interface {v5, v8, v7}, Landroid/view/inputmethod/InputMethod;->showSoftInput(ILandroid/os/ResultReceiver;)V

    #@120
    goto/16 :goto_2a

    #@122
    .line 191
    :sswitch_122
    iget v8, p1, Landroid/os/Message;->arg1:I

    #@124
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@126
    check-cast v7, Landroid/os/ResultReceiver;

    #@128
    invoke-interface {v5, v8, v7}, Landroid/view/inputmethod/InputMethod;->hideSoftInput(ILandroid/os/ResultReceiver;)V

    #@12b
    goto/16 :goto_2a

    #@12d
    .line 194
    :sswitch_12d
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@12f
    check-cast v7, Landroid/view/inputmethod/InputMethodSubtype;

    #@131
    invoke-interface {v5, v7}, Landroid/view/inputmethod/InputMethod;->changeInputMethodSubtype(Landroid/view/inputmethod/InputMethodSubtype;)V

    #@134
    goto/16 :goto_2a

    #@136
    .line 122
    :sswitch_data_136
    .sparse-switch
        0x1 -> :sswitch_4b
        0xa -> :sswitch_98
        0x14 -> :sswitch_a0
        0x1e -> :sswitch_a8
        0x20 -> :sswitch_ad
        0x22 -> :sswitch_cd
        0x28 -> :sswitch_ed
        0x2d -> :sswitch_ff
        0x32 -> :sswitch_10e
        0x3c -> :sswitch_117
        0x46 -> :sswitch_122
        0x50 -> :sswitch_12d
    .end sparse-switch
.end method

.method public getInternalInputMethod()Landroid/view/inputmethod/InputMethod;
    .registers 2

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mInputMethod:Ljava/lang/ref/WeakReference;

    #@2
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/view/inputmethod/InputMethod;

    #@8
    return-object v0
.end method

.method public hideSoftInput(ILandroid/os/ResultReceiver;)V
    .registers 6
    .parameter "flags"
    .parameter "resultReceiver"

    #@0
    .prologue
    .line 282
    iget-object v0, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2
    iget-object v1, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@4
    const/16 v2, 0x46

    #@6
    invoke-virtual {v1, v2, p1, p2}, Lcom/android/internal/os/HandlerCaller;->obtainMessageIO(IILjava/lang/Object;)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V

    #@d
    .line 284
    return-void
.end method

.method public restartInput(Lcom/android/internal/view/IInputContext;Landroid/view/inputmethod/EditorInfo;)V
    .registers 6
    .parameter "inputContext"
    .parameter "attribute"

    #@0
    .prologue
    .line 247
    iget-object v0, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2
    iget-object v1, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@4
    const/16 v2, 0x22

    #@6
    invoke-virtual {v1, v2, p1, p2}, Lcom/android/internal/os/HandlerCaller;->obtainMessageOO(ILjava/lang/Object;Ljava/lang/Object;)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V

    #@d
    .line 249
    return-void
.end method

.method public revokeSession(Lcom/android/internal/view/IInputMethodSession;)V
    .registers 8
    .parameter "session"

    #@0
    .prologue
    .line 268
    :try_start_0
    move-object v0, p1

    #@1
    check-cast v0, Landroid/inputmethodservice/IInputMethodSessionWrapper;

    #@3
    move-object v3, v0

    #@4
    invoke-virtual {v3}, Landroid/inputmethodservice/IInputMethodSessionWrapper;->getInternalInputMethodSession()Landroid/view/inputmethod/InputMethodSession;

    #@7
    move-result-object v2

    #@8
    .line 270
    .local v2, ls:Landroid/view/inputmethod/InputMethodSession;
    iget-object v3, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@a
    iget-object v4, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@c
    const/16 v5, 0x32

    #@e
    invoke-virtual {v4, v5, v2}, Lcom/android/internal/os/HandlerCaller;->obtainMessageO(ILjava/lang/Object;)Landroid/os/Message;

    #@11
    move-result-object v4

    #@12
    invoke-virtual {v3, v4}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V
    :try_end_15
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_15} :catch_16

    #@15
    .line 274
    .end local v2           #ls:Landroid/view/inputmethod/InputMethodSession;
    :goto_15
    return-void

    #@16
    .line 271
    :catch_16
    move-exception v1

    #@17
    .line 272
    .local v1, e:Ljava/lang/ClassCastException;
    const-string v3, "InputMethodWrapper"

    #@19
    new-instance v4, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v5, "Incoming session not of correct type: "

    #@20
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v4

    #@24
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v4

    #@28
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v4

    #@2c
    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2f
    goto :goto_15
.end method

.method public setSessionEnabled(Lcom/android/internal/view/IInputMethodSession;Z)V
    .registers 10
    .parameter "session"
    .parameter "enabled"

    #@0
    .prologue
    .line 257
    :try_start_0
    move-object v0, p1

    #@1
    check-cast v0, Landroid/inputmethodservice/IInputMethodSessionWrapper;

    #@3
    move-object v3, v0

    #@4
    invoke-virtual {v3}, Landroid/inputmethodservice/IInputMethodSessionWrapper;->getInternalInputMethodSession()Landroid/view/inputmethod/InputMethodSession;

    #@7
    move-result-object v2

    #@8
    .line 259
    .local v2, ls:Landroid/view/inputmethod/InputMethodSession;
    iget-object v4, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@a
    iget-object v5, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@c
    const/16 v6, 0x2d

    #@e
    if-eqz p2, :cond_19

    #@10
    const/4 v3, 0x1

    #@11
    :goto_11
    invoke-virtual {v5, v6, v3, v2}, Lcom/android/internal/os/HandlerCaller;->obtainMessageIO(IILjava/lang/Object;)Landroid/os/Message;

    #@14
    move-result-object v3

    #@15
    invoke-virtual {v4, v3}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V
    :try_end_18
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_18} :catch_1b

    #@18
    .line 264
    .end local v2           #ls:Landroid/view/inputmethod/InputMethodSession;
    :goto_18
    return-void

    #@19
    .line 259
    .restart local v2       #ls:Landroid/view/inputmethod/InputMethodSession;
    :cond_19
    const/4 v3, 0x0

    #@1a
    goto :goto_11

    #@1b
    .line 261
    .end local v2           #ls:Landroid/view/inputmethod/InputMethodSession;
    :catch_1b
    move-exception v1

    #@1c
    .line 262
    .local v1, e:Ljava/lang/ClassCastException;
    const-string v3, "InputMethodWrapper"

    #@1e
    new-instance v4, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v5, "Incoming session not of correct type: "

    #@25
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v4

    #@31
    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@34
    goto :goto_18
.end method

.method public showSoftInput(ILandroid/os/ResultReceiver;)V
    .registers 6
    .parameter "flags"
    .parameter "resultReceiver"

    #@0
    .prologue
    .line 277
    iget-object v0, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2
    iget-object v1, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@4
    const/16 v2, 0x3c

    #@6
    invoke-virtual {v1, v2, p1, p2}, Lcom/android/internal/os/HandlerCaller;->obtainMessageIO(IILjava/lang/Object;)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V

    #@d
    .line 279
    return-void
.end method

.method public startInput(Lcom/android/internal/view/IInputContext;Landroid/view/inputmethod/EditorInfo;)V
    .registers 6
    .parameter "inputContext"
    .parameter "attribute"

    #@0
    .prologue
    .line 242
    iget-object v0, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2
    iget-object v1, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@4
    const/16 v2, 0x20

    #@6
    invoke-virtual {v1, v2, p1, p2}, Lcom/android/internal/os/HandlerCaller;->obtainMessageOO(ILjava/lang/Object;Ljava/lang/Object;)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V

    #@d
    .line 244
    return-void
.end method

.method public unbindInput()V
    .registers 4

    #@0
    .prologue
    .line 238
    iget-object v0, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2
    iget-object v1, p0, Landroid/inputmethodservice/IInputMethodWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@4
    const/16 v2, 0x1e

    #@6
    invoke-virtual {v1, v2}, Lcom/android/internal/os/HandlerCaller;->obtainMessage(I)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1}, Lcom/android/internal/os/HandlerCaller;->executeOrSendMessage(Landroid/os/Message;)V

    #@d
    .line 239
    return-void
.end method
