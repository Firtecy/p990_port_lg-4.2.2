.class public Landroid/inputmethodservice/KeyboardView;
.super Landroid/view/View;
.source "KeyboardView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/inputmethodservice/KeyboardView$SwipeTracker;,
        Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;
    }
.end annotation


# static fields
.field private static final DEBOUNCE_TIME:I = 0x46

.field private static final DEBUG:Z = false

.field private static final DELAY_AFTER_PREVIEW:I = 0x46

.field private static final DELAY_BEFORE_PREVIEW:I = 0x0

.field private static final KEY_DELETE:[I = null

#the value of this static final field might be set in the static constructor
.field private static final LONGPRESS_TIMEOUT:I = 0x0

.field private static final LONG_PRESSABLE_STATE_SET:[I = null

.field private static MAX_NEARBY_KEYS:I = 0x0

.field private static final MSG_LONGPRESS:I = 0x4

.field private static final MSG_REMOVE_PREVIEW:I = 0x2

.field private static final MSG_REPEAT:I = 0x3

.field private static final MSG_SHOW_PREVIEW:I = 0x1

.field private static final MULTITAP_INTERVAL:I = 0x320

.field private static final NOT_A_KEY:I = -0x1

.field private static final REPEAT_INTERVAL:I = 0x32

.field private static final REPEAT_START_DELAY:I = 0x190


# instance fields
.field private mAbortKey:Z

.field private mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mBackgroundDimAmount:F

.field private mBuffer:Landroid/graphics/Bitmap;

.field private mCanvas:Landroid/graphics/Canvas;

.field private mClipRegion:Landroid/graphics/Rect;

.field private final mCoordinates:[I

.field private mCurrentKey:I

.field private mCurrentKeyIndex:I

.field private mCurrentKeyTime:J

.field private mDirtyRect:Landroid/graphics/Rect;

.field private mDisambiguateSwipe:Z

.field private mDistances:[I

.field private mDownKey:I

.field private mDownTime:J

.field private mDrawPending:Z

.field private mGestureDetector:Landroid/view/GestureDetector;

.field mHandler:Landroid/os/Handler;

.field private mHeadsetRequiredToHearPasswordsAnnounced:Z

.field private mInMultiTap:Z

.field private mInvalidatedKey:Landroid/inputmethodservice/Keyboard$Key;

.field private mKeyBackground:Landroid/graphics/drawable/Drawable;

.field private mKeyIndices:[I

.field private mKeyTextColor:I

.field private mKeyTextSize:I

.field private mKeyboard:Landroid/inputmethodservice/Keyboard;

.field private mKeyboardActionListener:Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;

.field private mKeyboardChanged:Z

.field private mKeys:[Landroid/inputmethodservice/Keyboard$Key;

.field private mLabelTextSize:I

.field private mLastCodeX:I

.field private mLastCodeY:I

.field private mLastKey:I

.field private mLastKeyTime:J

.field private mLastMoveTime:J

.field private mLastSentIndex:I

.field private mLastTapTime:J

.field private mLastX:I

.field private mLastY:I

.field private mMiniKeyboard:Landroid/inputmethodservice/KeyboardView;

.field private mMiniKeyboardCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/inputmethodservice/Keyboard$Key;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mMiniKeyboardContainer:Landroid/view/View;

.field private mMiniKeyboardOffsetX:I

.field private mMiniKeyboardOffsetY:I

.field private mMiniKeyboardOnScreen:Z

.field private mOldPointerCount:I

.field private mOldPointerX:F

.field private mOldPointerY:F

.field private mPadding:Landroid/graphics/Rect;

.field private mPaint:Landroid/graphics/Paint;

.field private mPopupKeyboard:Landroid/widget/PopupWindow;

.field private mPopupLayout:I

.field private mPopupParent:Landroid/view/View;

.field private mPopupPreviewX:I

.field private mPopupPreviewY:I

.field private mPopupX:I

.field private mPopupY:I

.field private mPossiblePoly:Z

.field private mPreviewCentered:Z

.field private mPreviewHeight:I

.field private mPreviewLabel:Ljava/lang/StringBuilder;

.field private mPreviewOffset:I

.field private mPreviewPopup:Landroid/widget/PopupWindow;

.field private mPreviewText:Landroid/widget/TextView;

.field private mPreviewTextSizeLarge:I

.field private mProximityCorrectOn:Z

.field private mProximityThreshold:I

.field private mRepeatKeyIndex:I

.field private mShadowColor:I

.field private mShadowRadius:F

.field private mShowPreview:Z

.field private mShowTouchPoints:Z

.field private mStartX:I

.field private mStartY:I

.field private mSwipeThreshold:I

.field private mSwipeTracker:Landroid/inputmethodservice/KeyboardView$SwipeTracker;

.field private mTapCount:I

.field private mVerticalCorrection:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 132
    new-array v0, v3, [I

    #@4
    const/4 v1, -0x5

    #@5
    aput v1, v0, v2

    #@7
    sput-object v0, Landroid/inputmethodservice/KeyboardView;->KEY_DELETE:[I

    #@9
    .line 133
    new-array v0, v3, [I

    #@b
    const v1, 0x101023c

    #@e
    aput v1, v0, v2

    #@10
    sput-object v0, Landroid/inputmethodservice/KeyboardView;->LONG_PRESSABLE_STATE_SET:[I

    #@12
    .line 225
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    #@15
    move-result v0

    #@16
    sput v0, Landroid/inputmethodservice/KeyboardView;->LONGPRESS_TIMEOUT:I

    #@18
    .line 227
    const/16 v0, 0xc

    #@1a
    sput v0, Landroid/inputmethodservice/KeyboardView;->MAX_NEARBY_KEYS:I

    #@1c
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 279
    const v0, 0x1010428

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/inputmethodservice/KeyboardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 280
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 16
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 283
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@3
    .line 136
    const/4 v7, -0x1

    #@4
    iput v7, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKeyIndex:I

    #@6
    .line 150
    const/4 v7, 0x2

    #@7
    new-array v7, v7, [I

    #@9
    iput-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mCoordinates:[I

    #@b
    .line 177
    const/4 v7, 0x0

    #@c
    iput-boolean v7, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewCentered:Z

    #@e
    .line 178
    const/4 v7, 0x1

    #@f
    iput-boolean v7, p0, Landroid/inputmethodservice/KeyboardView;->mShowPreview:Z

    #@11
    .line 179
    const/4 v7, 0x1

    #@12
    iput-boolean v7, p0, Landroid/inputmethodservice/KeyboardView;->mShowTouchPoints:Z

    #@14
    .line 198
    const/4 v7, -0x1

    #@15
    iput v7, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKey:I

    #@17
    .line 199
    const/4 v7, -0x1

    #@18
    iput v7, p0, Landroid/inputmethodservice/KeyboardView;->mDownKey:I

    #@1a
    .line 202
    const/16 v7, 0xc

    #@1c
    new-array v7, v7, [I

    #@1e
    iput-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mKeyIndices:[I

    #@20
    .line 206
    const/4 v7, -0x1

    #@21
    iput v7, p0, Landroid/inputmethodservice/KeyboardView;->mRepeatKeyIndex:I

    #@23
    .line 210
    new-instance v7, Landroid/graphics/Rect;

    #@25
    const/4 v8, 0x0

    #@26
    const/4 v9, 0x0

    #@27
    const/4 v10, 0x0

    #@28
    const/4 v11, 0x0

    #@29
    invoke-direct {v7, v8, v9, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    #@2c
    iput-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mClipRegion:Landroid/graphics/Rect;

    #@2e
    .line 212
    new-instance v7, Landroid/inputmethodservice/KeyboardView$SwipeTracker;

    #@30
    const/4 v8, 0x0

    #@31
    invoke-direct {v7, v8}, Landroid/inputmethodservice/KeyboardView$SwipeTracker;-><init>(Landroid/inputmethodservice/KeyboardView$1;)V

    #@34
    iput-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mSwipeTracker:Landroid/inputmethodservice/KeyboardView$SwipeTracker;

    #@36
    .line 217
    const/4 v7, 0x1

    #@37
    iput v7, p0, Landroid/inputmethodservice/KeyboardView;->mOldPointerCount:I

    #@39
    .line 228
    sget v7, Landroid/inputmethodservice/KeyboardView;->MAX_NEARBY_KEYS:I

    #@3b
    new-array v7, v7, [I

    #@3d
    iput-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mDistances:[I

    #@3f
    .line 236
    new-instance v7, Ljava/lang/StringBuilder;

    #@41
    const/4 v8, 0x1

    #@42
    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    #@45
    iput-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewLabel:Ljava/lang/StringBuilder;

    #@47
    .line 241
    new-instance v7, Landroid/graphics/Rect;

    #@49
    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    #@4c
    iput-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mDirtyRect:Landroid/graphics/Rect;

    #@4e
    .line 255
    new-instance v7, Landroid/inputmethodservice/KeyboardView$1;

    #@50
    invoke-direct {v7, p0}, Landroid/inputmethodservice/KeyboardView$1;-><init>(Landroid/inputmethodservice/KeyboardView;)V

    #@53
    iput-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mHandler:Landroid/os/Handler;

    #@55
    .line 285
    sget-object v7, Landroid/R$styleable;->KeyboardView:[I

    #@57
    const/4 v8, 0x0

    #@58
    invoke-virtual {p1, p2, v7, p3, v8}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@5b
    move-result-object v0

    #@5c
    .line 289
    .local v0, a:Landroid/content/res/TypedArray;
    const-string/jumbo v7, "layout_inflater"

    #@5f
    invoke-virtual {p1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@62
    move-result-object v3

    #@63
    check-cast v3, Landroid/view/LayoutInflater;

    #@65
    .line 293
    .local v3, inflate:Landroid/view/LayoutInflater;
    const/4 v6, 0x0

    #@66
    .line 294
    .local v6, previewLayout:I
    const/4 v4, 0x0

    #@67
    .line 296
    .local v4, keyTextSize:I
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->getIndexCount()I

    #@6a
    move-result v5

    #@6b
    .line 298
    .local v5, n:I
    const/4 v2, 0x0

    #@6c
    .local v2, i:I
    :goto_6c
    if-ge v2, v5, :cond_d1

    #@6e
    .line 299
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getIndex(I)I

    #@71
    move-result v1

    #@72
    .line 301
    .local v1, attr:I
    packed-switch v1, :pswitch_data_192

    #@75
    .line 298
    :goto_75
    add-int/lit8 v2, v2, 0x1

    #@77
    goto :goto_6c

    #@78
    .line 303
    :pswitch_78
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@7b
    move-result-object v7

    #@7c
    iput-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mKeyBackground:Landroid/graphics/drawable/Drawable;

    #@7e
    goto :goto_75

    #@7f
    .line 306
    :pswitch_7f
    const/4 v7, 0x0

    #@80
    invoke-virtual {v0, v1, v7}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@83
    move-result v7

    #@84
    iput v7, p0, Landroid/inputmethodservice/KeyboardView;->mVerticalCorrection:I

    #@86
    goto :goto_75

    #@87
    .line 309
    :pswitch_87
    const/4 v7, 0x0

    #@88
    invoke-virtual {v0, v1, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@8b
    move-result v6

    #@8c
    .line 310
    goto :goto_75

    #@8d
    .line 312
    :pswitch_8d
    const/4 v7, 0x0

    #@8e
    invoke-virtual {v0, v1, v7}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@91
    move-result v7

    #@92
    iput v7, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewOffset:I

    #@94
    goto :goto_75

    #@95
    .line 315
    :pswitch_95
    const/16 v7, 0x50

    #@97
    invoke-virtual {v0, v1, v7}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@9a
    move-result v7

    #@9b
    iput v7, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewHeight:I

    #@9d
    goto :goto_75

    #@9e
    .line 318
    :pswitch_9e
    const/16 v7, 0x12

    #@a0
    invoke-virtual {v0, v1, v7}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@a3
    move-result v7

    #@a4
    iput v7, p0, Landroid/inputmethodservice/KeyboardView;->mKeyTextSize:I

    #@a6
    goto :goto_75

    #@a7
    .line 321
    :pswitch_a7
    const/high16 v7, -0x100

    #@a9
    invoke-virtual {v0, v1, v7}, Landroid/content/res/TypedArray;->getColor(II)I

    #@ac
    move-result v7

    #@ad
    iput v7, p0, Landroid/inputmethodservice/KeyboardView;->mKeyTextColor:I

    #@af
    goto :goto_75

    #@b0
    .line 324
    :pswitch_b0
    const/16 v7, 0xe

    #@b2
    invoke-virtual {v0, v1, v7}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@b5
    move-result v7

    #@b6
    iput v7, p0, Landroid/inputmethodservice/KeyboardView;->mLabelTextSize:I

    #@b8
    goto :goto_75

    #@b9
    .line 327
    :pswitch_b9
    const/4 v7, 0x0

    #@ba
    invoke-virtual {v0, v1, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@bd
    move-result v7

    #@be
    iput v7, p0, Landroid/inputmethodservice/KeyboardView;->mPopupLayout:I

    #@c0
    goto :goto_75

    #@c1
    .line 330
    :pswitch_c1
    const/4 v7, 0x0

    #@c2
    invoke-virtual {v0, v1, v7}, Landroid/content/res/TypedArray;->getColor(II)I

    #@c5
    move-result v7

    #@c6
    iput v7, p0, Landroid/inputmethodservice/KeyboardView;->mShadowColor:I

    #@c8
    goto :goto_75

    #@c9
    .line 333
    :pswitch_c9
    const/4 v7, 0x0

    #@ca
    invoke-virtual {v0, v1, v7}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@cd
    move-result v7

    #@ce
    iput v7, p0, Landroid/inputmethodservice/KeyboardView;->mShadowRadius:F

    #@d0
    goto :goto_75

    #@d1
    .line 338
    .end local v1           #attr:I
    :cond_d1
    iget-object v7, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@d3
    sget-object v8, Lcom/android/internal/R$styleable;->Theme:[I

    #@d5
    invoke-virtual {v7, v8}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    #@d8
    move-result-object v0

    #@d9
    .line 340
    const/4 v7, 0x2

    #@da
    const/high16 v8, 0x3f00

    #@dc
    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@df
    move-result v7

    #@e0
    iput v7, p0, Landroid/inputmethodservice/KeyboardView;->mBackgroundDimAmount:F

    #@e2
    .line 342
    new-instance v7, Landroid/widget/PopupWindow;

    #@e4
    invoke-direct {v7, p1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    #@e7
    iput-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewPopup:Landroid/widget/PopupWindow;

    #@e9
    .line 343
    if-eqz v6, :cond_18d

    #@eb
    .line 344
    const/4 v7, 0x0

    #@ec
    invoke-virtual {v3, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@ef
    move-result-object v7

    #@f0
    check-cast v7, Landroid/widget/TextView;

    #@f2
    iput-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewText:Landroid/widget/TextView;

    #@f4
    .line 345
    iget-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewText:Landroid/widget/TextView;

    #@f6
    invoke-virtual {v7}, Landroid/widget/TextView;->getTextSize()F

    #@f9
    move-result v7

    #@fa
    float-to-int v7, v7

    #@fb
    iput v7, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewTextSizeLarge:I

    #@fd
    .line 346
    iget-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewPopup:Landroid/widget/PopupWindow;

    #@ff
    iget-object v8, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewText:Landroid/widget/TextView;

    #@101
    invoke-virtual {v7, v8}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    #@104
    .line 347
    iget-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewPopup:Landroid/widget/PopupWindow;

    #@106
    const/4 v8, 0x0

    #@107
    invoke-virtual {v7, v8}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@10a
    .line 352
    :goto_10a
    iget-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewPopup:Landroid/widget/PopupWindow;

    #@10c
    const/4 v8, 0x0

    #@10d
    invoke-virtual {v7, v8}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    #@110
    .line 354
    new-instance v7, Landroid/widget/PopupWindow;

    #@112
    invoke-direct {v7, p1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    #@115
    iput-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mPopupKeyboard:Landroid/widget/PopupWindow;

    #@117
    .line 355
    iget-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mPopupKeyboard:Landroid/widget/PopupWindow;

    #@119
    const/4 v8, 0x0

    #@11a
    invoke-virtual {v7, v8}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@11d
    .line 358
    iput-object p0, p0, Landroid/inputmethodservice/KeyboardView;->mPopupParent:Landroid/view/View;

    #@11f
    .line 361
    new-instance v7, Landroid/graphics/Paint;

    #@121
    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    #@124
    iput-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mPaint:Landroid/graphics/Paint;

    #@126
    .line 362
    iget-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mPaint:Landroid/graphics/Paint;

    #@128
    const/4 v8, 0x1

    #@129
    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    #@12c
    .line 363
    iget-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mPaint:Landroid/graphics/Paint;

    #@12e
    int-to-float v8, v4

    #@12f
    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->setTextSize(F)V

    #@132
    .line 364
    iget-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mPaint:Landroid/graphics/Paint;

    #@134
    sget-object v8, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    #@136
    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    #@139
    .line 365
    iget-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mPaint:Landroid/graphics/Paint;

    #@13b
    const/16 v8, 0xff

    #@13d
    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->setAlpha(I)V

    #@140
    .line 367
    new-instance v7, Landroid/graphics/Rect;

    #@142
    const/4 v8, 0x0

    #@143
    const/4 v9, 0x0

    #@144
    const/4 v10, 0x0

    #@145
    const/4 v11, 0x0

    #@146
    invoke-direct {v7, v8, v9, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    #@149
    iput-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mPadding:Landroid/graphics/Rect;

    #@14b
    .line 368
    new-instance v7, Ljava/util/HashMap;

    #@14d
    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    #@150
    iput-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboardCache:Ljava/util/Map;

    #@152
    .line 369
    iget-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mKeyBackground:Landroid/graphics/drawable/Drawable;

    #@154
    iget-object v8, p0, Landroid/inputmethodservice/KeyboardView;->mPadding:Landroid/graphics/Rect;

    #@156
    invoke-virtual {v7, v8}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@159
    .line 371
    const/high16 v7, 0x43fa

    #@15b
    invoke-virtual {p0}, Landroid/inputmethodservice/KeyboardView;->getResources()Landroid/content/res/Resources;

    #@15e
    move-result-object v8

    #@15f
    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@162
    move-result-object v8

    #@163
    iget v8, v8, Landroid/util/DisplayMetrics;->density:F

    #@165
    mul-float/2addr v7, v8

    #@166
    float-to-int v7, v7

    #@167
    iput v7, p0, Landroid/inputmethodservice/KeyboardView;->mSwipeThreshold:I

    #@169
    .line 372
    invoke-virtual {p0}, Landroid/inputmethodservice/KeyboardView;->getResources()Landroid/content/res/Resources;

    #@16c
    move-result-object v7

    #@16d
    const v8, 0x111002a

    #@170
    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@173
    move-result v7

    #@174
    iput-boolean v7, p0, Landroid/inputmethodservice/KeyboardView;->mDisambiguateSwipe:Z

    #@176
    .line 375
    invoke-static {p1}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@179
    move-result-object v7

    #@17a
    iput-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    #@17c
    .line 376
    const-string v7, "audio"

    #@17e
    invoke-virtual {p1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@181
    move-result-object v7

    #@182
    check-cast v7, Landroid/media/AudioManager;

    #@184
    iput-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mAudioManager:Landroid/media/AudioManager;

    #@186
    .line 378
    invoke-direct {p0}, Landroid/inputmethodservice/KeyboardView;->resetMultiTap()V

    #@189
    .line 379
    invoke-direct {p0}, Landroid/inputmethodservice/KeyboardView;->initGestureDetector()V

    #@18c
    .line 380
    return-void

    #@18d
    .line 349
    :cond_18d
    const/4 v7, 0x0

    #@18e
    iput-boolean v7, p0, Landroid/inputmethodservice/KeyboardView;->mShowPreview:Z

    #@190
    goto/16 :goto_10a

    #@192
    .line 301
    :pswitch_data_192
    .packed-switch 0x0
        :pswitch_c1
        :pswitch_c9
        :pswitch_78
        :pswitch_9e
        :pswitch_b0
        :pswitch_a7
        :pswitch_87
        :pswitch_8d
        :pswitch_95
        :pswitch_7f
        :pswitch_b9
    .end packed-switch
.end method

.method static synthetic access$100(Landroid/inputmethodservice/KeyboardView;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 69
    invoke-direct {p0, p1}, Landroid/inputmethodservice/KeyboardView;->showKey(I)V

    #@3
    return-void
.end method

.method static synthetic access$1000(Landroid/inputmethodservice/KeyboardView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget v0, p0, Landroid/inputmethodservice/KeyboardView;->mStartX:I

    #@2
    return v0
.end method

.method static synthetic access$1100(Landroid/inputmethodservice/KeyboardView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget v0, p0, Landroid/inputmethodservice/KeyboardView;->mStartY:I

    #@2
    return v0
.end method

.method static synthetic access$1200(Landroid/inputmethodservice/KeyboardView;IIIJ)V
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 69
    invoke-direct/range {p0 .. p5}, Landroid/inputmethodservice/KeyboardView;->detectAndSendKey(IIIJ)V

    #@3
    return-void
.end method

.method static synthetic access$1300(Landroid/inputmethodservice/KeyboardView;)Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mKeyboardActionListener:Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;

    #@2
    return-object v0
.end method

.method static synthetic access$1400(Landroid/inputmethodservice/KeyboardView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 69
    invoke-direct {p0}, Landroid/inputmethodservice/KeyboardView;->dismissPopupKeyboard()V

    #@3
    return-void
.end method

.method static synthetic access$200(Landroid/inputmethodservice/KeyboardView;)Landroid/widget/TextView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewText:Landroid/widget/TextView;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/inputmethodservice/KeyboardView;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    invoke-direct {p0}, Landroid/inputmethodservice/KeyboardView;->repeatKey()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$400(Landroid/inputmethodservice/KeyboardView;Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 69
    invoke-direct {p0, p1}, Landroid/inputmethodservice/KeyboardView;->openPopupIfRequired(Landroid/view/MotionEvent;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$500(Landroid/inputmethodservice/KeyboardView;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-boolean v0, p0, Landroid/inputmethodservice/KeyboardView;->mPossiblePoly:Z

    #@2
    return v0
.end method

.method static synthetic access$600(Landroid/inputmethodservice/KeyboardView;)Landroid/inputmethodservice/KeyboardView$SwipeTracker;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mSwipeTracker:Landroid/inputmethodservice/KeyboardView$SwipeTracker;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Landroid/inputmethodservice/KeyboardView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget v0, p0, Landroid/inputmethodservice/KeyboardView;->mSwipeThreshold:I

    #@2
    return v0
.end method

.method static synthetic access$800(Landroid/inputmethodservice/KeyboardView;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-boolean v0, p0, Landroid/inputmethodservice/KeyboardView;->mDisambiguateSwipe:Z

    #@2
    return v0
.end method

.method static synthetic access$900(Landroid/inputmethodservice/KeyboardView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget v0, p0, Landroid/inputmethodservice/KeyboardView;->mDownKey:I

    #@2
    return v0
.end method

.method private adjustCase(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .registers 4
    .parameter "label"

    #@0
    .prologue
    .line 577
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mKeyboard:Landroid/inputmethodservice/Keyboard;

    #@2
    invoke-virtual {v0}, Landroid/inputmethodservice/Keyboard;->isShifted()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_24

    #@8
    if-eqz p1, :cond_24

    #@a
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@d
    move-result v0

    #@e
    const/4 v1, 0x3

    #@f
    if-ge v0, v1, :cond_24

    #@11
    const/4 v0, 0x0

    #@12
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    #@15
    move-result v0

    #@16
    invoke-static {v0}, Ljava/lang/Character;->isLowerCase(C)Z

    #@19
    move-result v0

    #@1a
    if-eqz v0, :cond_24

    #@1c
    .line 579
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@23
    move-result-object p1

    #@24
    .line 581
    :cond_24
    return-object p1
.end method

.method private checkMultiTap(JI)V
    .registers 10
    .parameter "eventTime"
    .parameter "keyIndex"

    #@0
    .prologue
    const-wide/16 v4, 0x320

    #@2
    const/4 v2, 0x1

    #@3
    const/4 v3, -0x1

    #@4
    .line 1434
    if-ne p3, v3, :cond_7

    #@6
    .line 1450
    :cond_6
    :goto_6
    return-void

    #@7
    .line 1435
    :cond_7
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@9
    aget-object v0, v1, p3

    #@b
    .line 1436
    .local v0, key:Landroid/inputmethodservice/Keyboard$Key;
    iget-object v1, v0, Landroid/inputmethodservice/Keyboard$Key;->codes:[I

    #@d
    array-length v1, v1

    #@e
    if-le v1, v2, :cond_2b

    #@10
    .line 1437
    iput-boolean v2, p0, Landroid/inputmethodservice/KeyboardView;->mInMultiTap:Z

    #@12
    .line 1438
    iget-wide v1, p0, Landroid/inputmethodservice/KeyboardView;->mLastTapTime:J

    #@14
    add-long/2addr v1, v4

    #@15
    cmp-long v1, p1, v1

    #@17
    if-gez v1, :cond_28

    #@19
    iget v1, p0, Landroid/inputmethodservice/KeyboardView;->mLastSentIndex:I

    #@1b
    if-ne p3, v1, :cond_28

    #@1d
    .line 1440
    iget v1, p0, Landroid/inputmethodservice/KeyboardView;->mTapCount:I

    #@1f
    add-int/lit8 v1, v1, 0x1

    #@21
    iget-object v2, v0, Landroid/inputmethodservice/Keyboard$Key;->codes:[I

    #@23
    array-length v2, v2

    #@24
    rem-int/2addr v1, v2

    #@25
    iput v1, p0, Landroid/inputmethodservice/KeyboardView;->mTapCount:I

    #@27
    goto :goto_6

    #@28
    .line 1443
    :cond_28
    iput v3, p0, Landroid/inputmethodservice/KeyboardView;->mTapCount:I

    #@2a
    goto :goto_6

    #@2b
    .line 1447
    :cond_2b
    iget-wide v1, p0, Landroid/inputmethodservice/KeyboardView;->mLastTapTime:J

    #@2d
    add-long/2addr v1, v4

    #@2e
    cmp-long v1, p1, v1

    #@30
    if-gtz v1, :cond_36

    #@32
    iget v1, p0, Landroid/inputmethodservice/KeyboardView;->mLastSentIndex:I

    #@34
    if-eq p3, v1, :cond_6

    #@36
    .line 1448
    :cond_36
    invoke-direct {p0}, Landroid/inputmethodservice/KeyboardView;->resetMultiTap()V

    #@39
    goto :goto_6
.end method

.method private computeProximityThreshold(Landroid/inputmethodservice/Keyboard;)V
    .registers 9
    .parameter "keyboard"

    #@0
    .prologue
    .line 605
    if-nez p1, :cond_3

    #@2
    .line 617
    :cond_2
    :goto_2
    return-void

    #@3
    .line 606
    :cond_3
    iget-object v3, p0, Landroid/inputmethodservice/KeyboardView;->mKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@5
    .line 607
    .local v3, keys:[Landroid/inputmethodservice/Keyboard$Key;
    if-eqz v3, :cond_2

    #@7
    .line 608
    array-length v4, v3

    #@8
    .line 609
    .local v4, length:I
    const/4 v0, 0x0

    #@9
    .line 610
    .local v0, dimensionSum:I
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    :goto_a
    if-ge v1, v4, :cond_1d

    #@c
    .line 611
    aget-object v2, v3, v1

    #@e
    .line 612
    .local v2, key:Landroid/inputmethodservice/Keyboard$Key;
    iget v5, v2, Landroid/inputmethodservice/Keyboard$Key;->width:I

    #@10
    iget v6, v2, Landroid/inputmethodservice/Keyboard$Key;->height:I

    #@12
    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    #@15
    move-result v5

    #@16
    iget v6, v2, Landroid/inputmethodservice/Keyboard$Key;->gap:I

    #@18
    add-int/2addr v5, v6

    #@19
    add-int/2addr v0, v5

    #@1a
    .line 610
    add-int/lit8 v1, v1, 0x1

    #@1c
    goto :goto_a

    #@1d
    .line 614
    .end local v2           #key:Landroid/inputmethodservice/Keyboard$Key;
    :cond_1d
    if-ltz v0, :cond_2

    #@1f
    if-eqz v4, :cond_2

    #@21
    .line 615
    int-to-float v5, v0

    #@22
    const v6, 0x3fb33333

    #@25
    mul-float/2addr v5, v6

    #@26
    int-to-float v6, v4

    #@27
    div-float/2addr v5, v6

    #@28
    float-to-int v5, v5

    #@29
    iput v5, p0, Landroid/inputmethodservice/KeyboardView;->mProximityThreshold:I

    #@2b
    .line 616
    iget v5, p0, Landroid/inputmethodservice/KeyboardView;->mProximityThreshold:I

    #@2d
    iget v6, p0, Landroid/inputmethodservice/KeyboardView;->mProximityThreshold:I

    #@2f
    mul-int/2addr v5, v6

    #@30
    iput v5, p0, Landroid/inputmethodservice/KeyboardView;->mProximityThreshold:I

    #@32
    goto :goto_2
.end method

.method private detectAndSendKey(IIIJ)V
    .registers 12
    .parameter "index"
    .parameter "x"
    .parameter "y"
    .parameter "eventTime"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v5, -0x1

    #@2
    .line 804
    if-eq p1, v5, :cond_21

    #@4
    iget-object v3, p0, Landroid/inputmethodservice/KeyboardView;->mKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@6
    array-length v3, v3

    #@7
    if-ge p1, v3, :cond_21

    #@9
    .line 805
    iget-object v3, p0, Landroid/inputmethodservice/KeyboardView;->mKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@b
    aget-object v2, v3, p1

    #@d
    .line 806
    .local v2, key:Landroid/inputmethodservice/Keyboard$Key;
    iget-object v3, v2, Landroid/inputmethodservice/Keyboard$Key;->text:Ljava/lang/CharSequence;

    #@f
    if-eqz v3, :cond_22

    #@11
    .line 807
    iget-object v3, p0, Landroid/inputmethodservice/KeyboardView;->mKeyboardActionListener:Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;

    #@13
    iget-object v4, v2, Landroid/inputmethodservice/Keyboard$Key;->text:Ljava/lang/CharSequence;

    #@15
    invoke-interface {v3, v4}, Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;->onText(Ljava/lang/CharSequence;)V

    #@18
    .line 808
    iget-object v3, p0, Landroid/inputmethodservice/KeyboardView;->mKeyboardActionListener:Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;

    #@1a
    invoke-interface {v3, v5}, Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;->onRelease(I)V

    #@1d
    .line 827
    :goto_1d
    iput p1, p0, Landroid/inputmethodservice/KeyboardView;->mLastSentIndex:I

    #@1f
    .line 828
    iput-wide p4, p0, Landroid/inputmethodservice/KeyboardView;->mLastTapTime:J

    #@21
    .line 830
    .end local v2           #key:Landroid/inputmethodservice/Keyboard$Key;
    :cond_21
    return-void

    #@22
    .line 810
    .restart local v2       #key:Landroid/inputmethodservice/Keyboard$Key;
    :cond_22
    iget-object v3, v2, Landroid/inputmethodservice/Keyboard$Key;->codes:[I

    #@24
    aget v0, v3, v4

    #@26
    .line 812
    .local v0, code:I
    sget v3, Landroid/inputmethodservice/KeyboardView;->MAX_NEARBY_KEYS:I

    #@28
    new-array v1, v3, [I

    #@2a
    .line 813
    .local v1, codes:[I
    invoke-static {v1, v5}, Ljava/util/Arrays;->fill([II)V

    #@2d
    .line 814
    invoke-direct {p0, p2, p3, v1}, Landroid/inputmethodservice/KeyboardView;->getKeyIndices(II[I)I

    #@30
    .line 816
    iget-boolean v3, p0, Landroid/inputmethodservice/KeyboardView;->mInMultiTap:Z

    #@32
    if-eqz v3, :cond_46

    #@34
    .line 817
    iget v3, p0, Landroid/inputmethodservice/KeyboardView;->mTapCount:I

    #@36
    if-eq v3, v5, :cond_51

    #@38
    .line 818
    iget-object v3, p0, Landroid/inputmethodservice/KeyboardView;->mKeyboardActionListener:Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;

    #@3a
    const/4 v4, -0x5

    #@3b
    sget-object v5, Landroid/inputmethodservice/KeyboardView;->KEY_DELETE:[I

    #@3d
    invoke-interface {v3, v4, v5}, Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;->onKey(I[I)V

    #@40
    .line 822
    :goto_40
    iget-object v3, v2, Landroid/inputmethodservice/Keyboard$Key;->codes:[I

    #@42
    iget v4, p0, Landroid/inputmethodservice/KeyboardView;->mTapCount:I

    #@44
    aget v0, v3, v4

    #@46
    .line 824
    :cond_46
    iget-object v3, p0, Landroid/inputmethodservice/KeyboardView;->mKeyboardActionListener:Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;

    #@48
    invoke-interface {v3, v0, v1}, Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;->onKey(I[I)V

    #@4b
    .line 825
    iget-object v3, p0, Landroid/inputmethodservice/KeyboardView;->mKeyboardActionListener:Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;

    #@4d
    invoke-interface {v3, v0}, Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;->onRelease(I)V

    #@50
    goto :goto_1d

    #@51
    .line 820
    :cond_51
    iput v4, p0, Landroid/inputmethodservice/KeyboardView;->mTapCount:I

    #@53
    goto :goto_40
.end method

.method private dismissPopupKeyboard()V
    .registers 2

    #@0
    .prologue
    .line 1411
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mPopupKeyboard:Landroid/widget/PopupWindow;

    #@2
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_13

    #@8
    .line 1412
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mPopupKeyboard:Landroid/widget/PopupWindow;

    #@a
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    #@d
    .line 1413
    const/4 v0, 0x0

    #@e
    iput-boolean v0, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboardOnScreen:Z

    #@10
    .line 1414
    invoke-virtual {p0}, Landroid/inputmethodservice/KeyboardView;->invalidateAllKeys()V

    #@13
    .line 1416
    :cond_13
    return-void
.end method

.method private getKeyIndices(II[I)I
    .registers 25
    .parameter "x"
    .parameter "y"
    .parameter "allKeys"

    #@0
    .prologue
    .line 753
    move-object/from16 v0, p0

    #@2
    iget-object v13, v0, Landroid/inputmethodservice/KeyboardView;->mKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@4
    .line 754
    .local v13, keys:[Landroid/inputmethodservice/Keyboard$Key;
    const/16 v16, -0x1

    #@6
    .line 755
    .local v16, primaryIndex:I
    const/4 v5, -0x1

    #@7
    .line 756
    .local v5, closestKey:I
    move-object/from16 v0, p0

    #@9
    iget v0, v0, Landroid/inputmethodservice/KeyboardView;->mProximityThreshold:I

    #@b
    move/from16 v17, v0

    #@d
    add-int/lit8 v6, v17, 0x1

    #@f
    .line 757
    .local v6, closestKeyDist:I
    move-object/from16 v0, p0

    #@11
    iget-object v0, v0, Landroid/inputmethodservice/KeyboardView;->mDistances:[I

    #@13
    move-object/from16 v17, v0

    #@15
    const v18, 0x7fffffff

    #@18
    invoke-static/range {v17 .. v18}, Ljava/util/Arrays;->fill([II)V

    #@1b
    .line 758
    move-object/from16 v0, p0

    #@1d
    iget-object v0, v0, Landroid/inputmethodservice/KeyboardView;->mKeyboard:Landroid/inputmethodservice/Keyboard;

    #@1f
    move-object/from16 v17, v0

    #@21
    move-object/from16 v0, v17

    #@23
    move/from16 v1, p1

    #@25
    move/from16 v2, p2

    #@27
    invoke-virtual {v0, v1, v2}, Landroid/inputmethodservice/Keyboard;->getNearestKeys(II)[I

    #@2a
    move-result-object v15

    #@2b
    .line 759
    .local v15, nearestKeyIndices:[I
    array-length v12, v15

    #@2c
    .line 760
    .local v12, keyCount:I
    const/4 v8, 0x0

    #@2d
    .local v8, i:I
    :goto_2d
    if-ge v8, v12, :cond_f4

    #@2f
    .line 761
    aget v17, v15, v8

    #@31
    aget-object v11, v13, v17

    #@33
    .line 762
    .local v11, key:Landroid/inputmethodservice/Keyboard$Key;
    const/4 v7, 0x0

    #@34
    .line 763
    .local v7, dist:I
    move/from16 v0, p1

    #@36
    move/from16 v1, p2

    #@38
    invoke-virtual {v11, v0, v1}, Landroid/inputmethodservice/Keyboard$Key;->isInside(II)Z

    #@3b
    move-result v9

    #@3c
    .line 764
    .local v9, isInside:Z
    if-eqz v9, :cond_40

    #@3e
    .line 765
    aget v16, v15, v8

    #@40
    .line 768
    :cond_40
    move-object/from16 v0, p0

    #@42
    iget-boolean v0, v0, Landroid/inputmethodservice/KeyboardView;->mProximityCorrectOn:Z

    #@44
    move/from16 v17, v0

    #@46
    if-eqz v17, :cond_5a

    #@48
    move/from16 v0, p1

    #@4a
    move/from16 v1, p2

    #@4c
    invoke-virtual {v11, v0, v1}, Landroid/inputmethodservice/Keyboard$Key;->squaredDistanceFrom(II)I

    #@4f
    move-result v7

    #@50
    move-object/from16 v0, p0

    #@52
    iget v0, v0, Landroid/inputmethodservice/KeyboardView;->mProximityThreshold:I

    #@54
    move/from16 v17, v0

    #@56
    move/from16 v0, v17

    #@58
    if-lt v7, v0, :cond_5c

    #@5a
    :cond_5a
    if-eqz v9, :cond_7a

    #@5c
    :cond_5c
    iget-object v0, v11, Landroid/inputmethodservice/Keyboard$Key;->codes:[I

    #@5e
    move-object/from16 v17, v0

    #@60
    const/16 v18, 0x0

    #@62
    aget v17, v17, v18

    #@64
    const/16 v18, 0x20

    #@66
    move/from16 v0, v17

    #@68
    move/from16 v1, v18

    #@6a
    if-le v0, v1, :cond_7a

    #@6c
    .line 773
    iget-object v0, v11, Landroid/inputmethodservice/Keyboard$Key;->codes:[I

    #@6e
    move-object/from16 v17, v0

    #@70
    move-object/from16 v0, v17

    #@72
    array-length v14, v0

    #@73
    .line 774
    .local v14, nCodes:I
    if-ge v7, v6, :cond_78

    #@75
    .line 775
    move v6, v7

    #@76
    .line 776
    aget v5, v15, v8

    #@78
    .line 779
    :cond_78
    if-nez p3, :cond_7d

    #@7a
    .line 760
    .end local v14           #nCodes:I
    :cond_7a
    add-int/lit8 v8, v8, 0x1

    #@7c
    goto :goto_2d

    #@7d
    .line 781
    .restart local v14       #nCodes:I
    :cond_7d
    const/4 v10, 0x0

    #@7e
    .local v10, j:I
    :goto_7e
    move-object/from16 v0, p0

    #@80
    iget-object v0, v0, Landroid/inputmethodservice/KeyboardView;->mDistances:[I

    #@82
    move-object/from16 v17, v0

    #@84
    move-object/from16 v0, v17

    #@86
    array-length v0, v0

    #@87
    move/from16 v17, v0

    #@89
    move/from16 v0, v17

    #@8b
    if-ge v10, v0, :cond_7a

    #@8d
    .line 782
    move-object/from16 v0, p0

    #@8f
    iget-object v0, v0, Landroid/inputmethodservice/KeyboardView;->mDistances:[I

    #@91
    move-object/from16 v17, v0

    #@93
    aget v17, v17, v10

    #@95
    move/from16 v0, v17

    #@97
    if-le v0, v7, :cond_f1

    #@99
    .line 784
    move-object/from16 v0, p0

    #@9b
    iget-object v0, v0, Landroid/inputmethodservice/KeyboardView;->mDistances:[I

    #@9d
    move-object/from16 v17, v0

    #@9f
    move-object/from16 v0, p0

    #@a1
    iget-object v0, v0, Landroid/inputmethodservice/KeyboardView;->mDistances:[I

    #@a3
    move-object/from16 v18, v0

    #@a5
    add-int v19, v10, v14

    #@a7
    move-object/from16 v0, p0

    #@a9
    iget-object v0, v0, Landroid/inputmethodservice/KeyboardView;->mDistances:[I

    #@ab
    move-object/from16 v20, v0

    #@ad
    move-object/from16 v0, v20

    #@af
    array-length v0, v0

    #@b0
    move/from16 v20, v0

    #@b2
    sub-int v20, v20, v10

    #@b4
    sub-int v20, v20, v14

    #@b6
    move-object/from16 v0, v17

    #@b8
    move-object/from16 v1, v18

    #@ba
    move/from16 v2, v19

    #@bc
    move/from16 v3, v20

    #@be
    invoke-static {v0, v10, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@c1
    .line 786
    add-int v17, v10, v14

    #@c3
    move-object/from16 v0, p3

    #@c5
    array-length v0, v0

    #@c6
    move/from16 v18, v0

    #@c8
    sub-int v18, v18, v10

    #@ca
    sub-int v18, v18, v14

    #@cc
    move-object/from16 v0, p3

    #@ce
    move-object/from16 v1, p3

    #@d0
    move/from16 v2, v17

    #@d2
    move/from16 v3, v18

    #@d4
    invoke-static {v0, v10, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@d7
    .line 788
    const/4 v4, 0x0

    #@d8
    .local v4, c:I
    :goto_d8
    if-ge v4, v14, :cond_7a

    #@da
    .line 789
    add-int v17, v10, v4

    #@dc
    iget-object v0, v11, Landroid/inputmethodservice/Keyboard$Key;->codes:[I

    #@de
    move-object/from16 v18, v0

    #@e0
    aget v18, v18, v4

    #@e2
    aput v18, p3, v17

    #@e4
    .line 790
    move-object/from16 v0, p0

    #@e6
    iget-object v0, v0, Landroid/inputmethodservice/KeyboardView;->mDistances:[I

    #@e8
    move-object/from16 v17, v0

    #@ea
    add-int v18, v10, v4

    #@ec
    aput v7, v17, v18

    #@ee
    .line 788
    add-int/lit8 v4, v4, 0x1

    #@f0
    goto :goto_d8

    #@f1
    .line 781
    .end local v4           #c:I
    :cond_f1
    add-int/lit8 v10, v10, 0x1

    #@f3
    goto :goto_7e

    #@f4
    .line 797
    .end local v7           #dist:I
    .end local v9           #isInside:Z
    .end local v10           #j:I
    .end local v11           #key:Landroid/inputmethodservice/Keyboard$Key;
    .end local v14           #nCodes:I
    :cond_f4
    const/16 v17, -0x1

    #@f6
    move/from16 v0, v16

    #@f8
    move/from16 v1, v17

    #@fa
    if-ne v0, v1, :cond_fe

    #@fc
    .line 798
    move/from16 v16, v5

    #@fe
    .line 800
    :cond_fe
    return v16
.end method

.method private getPreviewText(Landroid/inputmethodservice/Keyboard$Key;)Ljava/lang/CharSequence;
    .registers 6
    .parameter "key"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 836
    iget-boolean v1, p0, Landroid/inputmethodservice/KeyboardView;->mInMultiTap:Z

    #@3
    if-eqz v1, :cond_22

    #@5
    .line 838
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewLabel:Ljava/lang/StringBuilder;

    #@7
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    #@a
    .line 839
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewLabel:Ljava/lang/StringBuilder;

    #@c
    iget-object v2, p1, Landroid/inputmethodservice/Keyboard$Key;->codes:[I

    #@e
    iget v3, p0, Landroid/inputmethodservice/KeyboardView;->mTapCount:I

    #@10
    if-gez v3, :cond_1f

    #@12
    :goto_12
    aget v0, v2, v0

    #@14
    int-to-char v0, v0

    #@15
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@18
    .line 840
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewLabel:Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {p0, v0}, Landroid/inputmethodservice/KeyboardView;->adjustCase(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@1d
    move-result-object v0

    #@1e
    .line 842
    :goto_1e
    return-object v0

    #@1f
    .line 839
    :cond_1f
    iget v0, p0, Landroid/inputmethodservice/KeyboardView;->mTapCount:I

    #@21
    goto :goto_12

    #@22
    .line 842
    :cond_22
    iget-object v0, p1, Landroid/inputmethodservice/Keyboard$Key;->label:Ljava/lang/CharSequence;

    #@24
    invoke-direct {p0, v0}, Landroid/inputmethodservice/KeyboardView;->adjustCase(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@27
    move-result-object v0

    #@28
    goto :goto_1e
.end method

.method private initGestureDetector()V
    .registers 4

    #@0
    .prologue
    .line 384
    new-instance v0, Landroid/view/GestureDetector;

    #@2
    invoke-virtual {p0}, Landroid/inputmethodservice/KeyboardView;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    new-instance v2, Landroid/inputmethodservice/KeyboardView$2;

    #@8
    invoke-direct {v2, p0}, Landroid/inputmethodservice/KeyboardView$2;-><init>(Landroid/inputmethodservice/KeyboardView;)V

    #@b
    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    #@e
    iput-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mGestureDetector:Landroid/view/GestureDetector;

    #@10
    .line 436
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mGestureDetector:Landroid/view/GestureDetector;

    #@12
    const/4 v1, 0x0

    #@13
    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    #@16
    .line 437
    return-void
.end method

.method private onBufferDraw()V
    .registers 27

    #@0
    .prologue
    .line 639
    move-object/from16 v0, p0

    #@2
    iget-object v2, v0, Landroid/inputmethodservice/KeyboardView;->mBuffer:Landroid/graphics/Bitmap;

    #@4
    if-eqz v2, :cond_c

    #@6
    move-object/from16 v0, p0

    #@8
    iget-boolean v2, v0, Landroid/inputmethodservice/KeyboardView;->mKeyboardChanged:Z

    #@a
    if-eqz v2, :cond_67

    #@c
    .line 640
    :cond_c
    move-object/from16 v0, p0

    #@e
    iget-object v2, v0, Landroid/inputmethodservice/KeyboardView;->mBuffer:Landroid/graphics/Bitmap;

    #@10
    if-eqz v2, :cond_34

    #@12
    move-object/from16 v0, p0

    #@14
    iget-boolean v2, v0, Landroid/inputmethodservice/KeyboardView;->mKeyboardChanged:Z

    #@16
    if-eqz v2, :cond_5f

    #@18
    move-object/from16 v0, p0

    #@1a
    iget-object v2, v0, Landroid/inputmethodservice/KeyboardView;->mBuffer:Landroid/graphics/Bitmap;

    #@1c
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    #@1f
    move-result v2

    #@20
    invoke-virtual/range {p0 .. p0}, Landroid/inputmethodservice/KeyboardView;->getWidth()I

    #@23
    move-result v3

    #@24
    if-ne v2, v3, :cond_34

    #@26
    move-object/from16 v0, p0

    #@28
    iget-object v2, v0, Landroid/inputmethodservice/KeyboardView;->mBuffer:Landroid/graphics/Bitmap;

    #@2a
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    #@2d
    move-result v2

    #@2e
    invoke-virtual/range {p0 .. p0}, Landroid/inputmethodservice/KeyboardView;->getHeight()I

    #@31
    move-result v3

    #@32
    if-eq v2, v3, :cond_5f

    #@34
    .line 643
    :cond_34
    const/4 v2, 0x1

    #@35
    invoke-virtual/range {p0 .. p0}, Landroid/inputmethodservice/KeyboardView;->getWidth()I

    #@38
    move-result v3

    #@39
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    #@3c
    move-result v24

    #@3d
    .line 644
    .local v24, width:I
    const/4 v2, 0x1

    #@3e
    invoke-virtual/range {p0 .. p0}, Landroid/inputmethodservice/KeyboardView;->getHeight()I

    #@41
    move-result v3

    #@42
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    #@45
    move-result v13

    #@46
    .line 645
    .local v13, height:I
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@48
    move/from16 v0, v24

    #@4a
    invoke-static {v0, v13, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    #@4d
    move-result-object v2

    #@4e
    move-object/from16 v0, p0

    #@50
    iput-object v2, v0, Landroid/inputmethodservice/KeyboardView;->mBuffer:Landroid/graphics/Bitmap;

    #@52
    .line 646
    new-instance v2, Landroid/graphics/Canvas;

    #@54
    move-object/from16 v0, p0

    #@56
    iget-object v3, v0, Landroid/inputmethodservice/KeyboardView;->mBuffer:Landroid/graphics/Bitmap;

    #@58
    invoke-direct {v2, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    #@5b
    move-object/from16 v0, p0

    #@5d
    iput-object v2, v0, Landroid/inputmethodservice/KeyboardView;->mCanvas:Landroid/graphics/Canvas;

    #@5f
    .line 648
    .end local v13           #height:I
    .end local v24           #width:I
    :cond_5f
    invoke-virtual/range {p0 .. p0}, Landroid/inputmethodservice/KeyboardView;->invalidateAllKeys()V

    #@62
    .line 649
    const/4 v2, 0x0

    #@63
    move-object/from16 v0, p0

    #@65
    iput-boolean v2, v0, Landroid/inputmethodservice/KeyboardView;->mKeyboardChanged:Z

    #@67
    .line 651
    :cond_67
    move-object/from16 v0, p0

    #@69
    iget-object v1, v0, Landroid/inputmethodservice/KeyboardView;->mCanvas:Landroid/graphics/Canvas;

    #@6b
    .line 652
    .local v1, canvas:Landroid/graphics/Canvas;
    move-object/from16 v0, p0

    #@6d
    iget-object v2, v0, Landroid/inputmethodservice/KeyboardView;->mDirtyRect:Landroid/graphics/Rect;

    #@6f
    sget-object v3, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    #@71
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    #@74
    .line 654
    move-object/from16 v0, p0

    #@76
    iget-object v2, v0, Landroid/inputmethodservice/KeyboardView;->mKeyboard:Landroid/inputmethodservice/Keyboard;

    #@78
    if-nez v2, :cond_7b

    #@7a
    .line 750
    :goto_7a
    return-void

    #@7b
    .line 656
    :cond_7b
    move-object/from16 v0, p0

    #@7d
    iget-object v6, v0, Landroid/inputmethodservice/KeyboardView;->mPaint:Landroid/graphics/Paint;

    #@7f
    .line 657
    .local v6, paint:Landroid/graphics/Paint;
    move-object/from16 v0, p0

    #@81
    iget-object v0, v0, Landroid/inputmethodservice/KeyboardView;->mKeyBackground:Landroid/graphics/drawable/Drawable;

    #@83
    move-object/from16 v19, v0

    #@85
    .line 658
    .local v19, keyBackground:Landroid/graphics/drawable/Drawable;
    move-object/from16 v0, p0

    #@87
    iget-object v8, v0, Landroid/inputmethodservice/KeyboardView;->mClipRegion:Landroid/graphics/Rect;

    #@89
    .line 659
    .local v8, clipRegion:Landroid/graphics/Rect;
    move-object/from16 v0, p0

    #@8b
    iget-object v0, v0, Landroid/inputmethodservice/KeyboardView;->mPadding:Landroid/graphics/Rect;

    #@8d
    move-object/from16 v23, v0

    #@8f
    .line 660
    .local v23, padding:Landroid/graphics/Rect;
    move-object/from16 v0, p0

    #@91
    iget v0, v0, Landroid/view/View;->mPaddingLeft:I

    #@93
    move/from16 v16, v0

    #@95
    .line 661
    .local v16, kbdPaddingLeft:I
    move-object/from16 v0, p0

    #@97
    iget v0, v0, Landroid/view/View;->mPaddingTop:I

    #@99
    move/from16 v17, v0

    #@9b
    .line 662
    .local v17, kbdPaddingTop:I
    move-object/from16 v0, p0

    #@9d
    iget-object v0, v0, Landroid/inputmethodservice/KeyboardView;->mKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@9f
    move-object/from16 v21, v0

    #@a1
    .line 663
    .local v21, keys:[Landroid/inputmethodservice/Keyboard$Key;
    move-object/from16 v0, p0

    #@a3
    iget-object v15, v0, Landroid/inputmethodservice/KeyboardView;->mInvalidatedKey:Landroid/inputmethodservice/Keyboard$Key;

    #@a5
    .line 665
    .local v15, invalidKey:Landroid/inputmethodservice/Keyboard$Key;
    move-object/from16 v0, p0

    #@a7
    iget v2, v0, Landroid/inputmethodservice/KeyboardView;->mKeyTextColor:I

    #@a9
    invoke-virtual {v6, v2}, Landroid/graphics/Paint;->setColor(I)V

    #@ac
    .line 666
    const/4 v9, 0x0

    #@ad
    .line 667
    .local v9, drawSingleKey:Z
    if-eqz v15, :cond_e4

    #@af
    invoke-virtual {v1, v8}, Landroid/graphics/Canvas;->getClipBounds(Landroid/graphics/Rect;)Z

    #@b2
    move-result v2

    #@b3
    if-eqz v2, :cond_e4

    #@b5
    .line 669
    iget v2, v15, Landroid/inputmethodservice/Keyboard$Key;->x:I

    #@b7
    add-int v2, v2, v16

    #@b9
    add-int/lit8 v2, v2, -0x1

    #@bb
    iget v3, v8, Landroid/graphics/Rect;->left:I

    #@bd
    if-gt v2, v3, :cond_e4

    #@bf
    iget v2, v15, Landroid/inputmethodservice/Keyboard$Key;->y:I

    #@c1
    add-int v2, v2, v17

    #@c3
    add-int/lit8 v2, v2, -0x1

    #@c5
    iget v3, v8, Landroid/graphics/Rect;->top:I

    #@c7
    if-gt v2, v3, :cond_e4

    #@c9
    iget v2, v15, Landroid/inputmethodservice/Keyboard$Key;->x:I

    #@cb
    iget v3, v15, Landroid/inputmethodservice/Keyboard$Key;->width:I

    #@cd
    add-int/2addr v2, v3

    #@ce
    add-int v2, v2, v16

    #@d0
    add-int/lit8 v2, v2, 0x1

    #@d2
    iget v3, v8, Landroid/graphics/Rect;->right:I

    #@d4
    if-lt v2, v3, :cond_e4

    #@d6
    iget v2, v15, Landroid/inputmethodservice/Keyboard$Key;->y:I

    #@d8
    iget v3, v15, Landroid/inputmethodservice/Keyboard$Key;->height:I

    #@da
    add-int/2addr v2, v3

    #@db
    add-int v2, v2, v17

    #@dd
    add-int/lit8 v2, v2, 0x1

    #@df
    iget v3, v8, Landroid/graphics/Rect;->bottom:I

    #@e1
    if-lt v2, v3, :cond_e4

    #@e3
    .line 673
    const/4 v9, 0x1

    #@e4
    .line 676
    :cond_e4
    const/4 v2, 0x0

    #@e5
    sget-object v3, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    #@e7
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    #@ea
    .line 677
    move-object/from16 v0, v21

    #@ec
    array-length v0, v0

    #@ed
    move/from16 v20, v0

    #@ef
    .line 678
    .local v20, keyCount:I
    const/4 v14, 0x0

    #@f0
    .local v14, i:I
    :goto_f0
    move/from16 v0, v20

    #@f2
    if-ge v14, v0, :cond_264

    #@f4
    .line 679
    aget-object v18, v21, v14

    #@f6
    .line 680
    .local v18, key:Landroid/inputmethodservice/Keyboard$Key;
    if-eqz v9, :cond_ff

    #@f8
    move-object/from16 v0, v18

    #@fa
    if-eq v15, v0, :cond_ff

    #@fc
    .line 678
    :goto_fc
    add-int/lit8 v14, v14, 0x1

    #@fe
    goto :goto_f0

    #@ff
    .line 683
    :cond_ff
    invoke-virtual/range {v18 .. v18}, Landroid/inputmethodservice/Keyboard$Key;->getCurrentDrawableState()[I

    #@102
    move-result-object v10

    #@103
    .line 684
    .local v10, drawableState:[I
    move-object/from16 v0, v19

    #@105
    invoke-virtual {v0, v10}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@108
    .line 687
    move-object/from16 v0, v18

    #@10a
    iget-object v2, v0, Landroid/inputmethodservice/Keyboard$Key;->label:Ljava/lang/CharSequence;

    #@10c
    if-nez v2, :cond_1cf

    #@10e
    const/16 v22, 0x0

    #@110
    .line 689
    .local v22, label:Ljava/lang/String;
    :goto_110
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    #@113
    move-result-object v7

    #@114
    .line 690
    .local v7, bounds:Landroid/graphics/Rect;
    move-object/from16 v0, v18

    #@116
    iget v2, v0, Landroid/inputmethodservice/Keyboard$Key;->width:I

    #@118
    iget v3, v7, Landroid/graphics/Rect;->right:I

    #@11a
    if-ne v2, v3, :cond_124

    #@11c
    move-object/from16 v0, v18

    #@11e
    iget v2, v0, Landroid/inputmethodservice/Keyboard$Key;->height:I

    #@120
    iget v3, v7, Landroid/graphics/Rect;->bottom:I

    #@122
    if-eq v2, v3, :cond_133

    #@124
    .line 692
    :cond_124
    const/4 v2, 0x0

    #@125
    const/4 v3, 0x0

    #@126
    move-object/from16 v0, v18

    #@128
    iget v4, v0, Landroid/inputmethodservice/Keyboard$Key;->width:I

    #@12a
    move-object/from16 v0, v18

    #@12c
    iget v5, v0, Landroid/inputmethodservice/Keyboard$Key;->height:I

    #@12e
    move-object/from16 v0, v19

    #@130
    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@133
    .line 694
    :cond_133
    move-object/from16 v0, v18

    #@135
    iget v2, v0, Landroid/inputmethodservice/Keyboard$Key;->x:I

    #@137
    add-int v2, v2, v16

    #@139
    int-to-float v2, v2

    #@13a
    move-object/from16 v0, v18

    #@13c
    iget v3, v0, Landroid/inputmethodservice/Keyboard$Key;->y:I

    #@13e
    add-int v3, v3, v17

    #@140
    int-to-float v3, v3

    #@141
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    #@144
    .line 695
    move-object/from16 v0, v19

    #@146
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@149
    .line 697
    if-eqz v22, :cond_1ee

    #@14b
    .line 699
    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    #@14e
    move-result v2

    #@14f
    const/4 v3, 0x1

    #@150
    if-le v2, v3, :cond_1df

    #@152
    move-object/from16 v0, v18

    #@154
    iget-object v2, v0, Landroid/inputmethodservice/Keyboard$Key;->codes:[I

    #@156
    array-length v2, v2

    #@157
    const/4 v3, 0x2

    #@158
    if-ge v2, v3, :cond_1df

    #@15a
    .line 700
    move-object/from16 v0, p0

    #@15c
    iget v2, v0, Landroid/inputmethodservice/KeyboardView;->mLabelTextSize:I

    #@15e
    int-to-float v2, v2

    #@15f
    invoke-virtual {v6, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    #@162
    .line 701
    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    #@164
    invoke-virtual {v6, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    #@167
    .line 707
    :goto_167
    move-object/from16 v0, p0

    #@169
    iget v2, v0, Landroid/inputmethodservice/KeyboardView;->mShadowRadius:F

    #@16b
    const/4 v3, 0x0

    #@16c
    const/4 v4, 0x0

    #@16d
    move-object/from16 v0, p0

    #@16f
    iget v5, v0, Landroid/inputmethodservice/KeyboardView;->mShadowColor:I

    #@171
    invoke-virtual {v6, v2, v3, v4, v5}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    #@174
    .line 709
    move-object/from16 v0, v18

    #@176
    iget v2, v0, Landroid/inputmethodservice/Keyboard$Key;->width:I

    #@178
    move-object/from16 v0, v23

    #@17a
    iget v3, v0, Landroid/graphics/Rect;->left:I

    #@17c
    sub-int/2addr v2, v3

    #@17d
    move-object/from16 v0, v23

    #@17f
    iget v3, v0, Landroid/graphics/Rect;->right:I

    #@181
    sub-int/2addr v2, v3

    #@182
    div-int/lit8 v2, v2, 0x2

    #@184
    move-object/from16 v0, v23

    #@186
    iget v3, v0, Landroid/graphics/Rect;->left:I

    #@188
    add-int/2addr v2, v3

    #@189
    int-to-float v2, v2

    #@18a
    move-object/from16 v0, v18

    #@18c
    iget v3, v0, Landroid/inputmethodservice/Keyboard$Key;->height:I

    #@18e
    move-object/from16 v0, v23

    #@190
    iget v4, v0, Landroid/graphics/Rect;->top:I

    #@192
    sub-int/2addr v3, v4

    #@193
    move-object/from16 v0, v23

    #@195
    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    #@197
    sub-int/2addr v3, v4

    #@198
    div-int/lit8 v3, v3, 0x2

    #@19a
    int-to-float v3, v3

    #@19b
    invoke-virtual {v6}, Landroid/graphics/Paint;->getTextSize()F

    #@19e
    move-result v4

    #@19f
    invoke-virtual {v6}, Landroid/graphics/Paint;->descent()F

    #@1a2
    move-result v5

    #@1a3
    sub-float/2addr v4, v5

    #@1a4
    const/high16 v5, 0x4000

    #@1a6
    div-float/2addr v4, v5

    #@1a7
    add-float/2addr v3, v4

    #@1a8
    move-object/from16 v0, v23

    #@1aa
    iget v4, v0, Landroid/graphics/Rect;->top:I

    #@1ac
    int-to-float v4, v4

    #@1ad
    add-float/2addr v3, v4

    #@1ae
    move-object/from16 v0, v22

    #@1b0
    invoke-virtual {v1, v0, v2, v3, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    #@1b3
    .line 716
    const/4 v2, 0x0

    #@1b4
    const/4 v3, 0x0

    #@1b5
    const/4 v4, 0x0

    #@1b6
    const/4 v5, 0x0

    #@1b7
    invoke-virtual {v6, v2, v3, v4, v5}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    #@1ba
    .line 728
    :cond_1ba
    :goto_1ba
    move-object/from16 v0, v18

    #@1bc
    iget v2, v0, Landroid/inputmethodservice/Keyboard$Key;->x:I

    #@1be
    neg-int v2, v2

    #@1bf
    sub-int v2, v2, v16

    #@1c1
    int-to-float v2, v2

    #@1c2
    move-object/from16 v0, v18

    #@1c4
    iget v3, v0, Landroid/inputmethodservice/Keyboard$Key;->y:I

    #@1c6
    neg-int v3, v3

    #@1c7
    sub-int v3, v3, v17

    #@1c9
    int-to-float v3, v3

    #@1ca
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    #@1cd
    goto/16 :goto_fc

    #@1cf
    .line 687
    .end local v7           #bounds:Landroid/graphics/Rect;
    .end local v22           #label:Ljava/lang/String;
    :cond_1cf
    move-object/from16 v0, v18

    #@1d1
    iget-object v2, v0, Landroid/inputmethodservice/Keyboard$Key;->label:Ljava/lang/CharSequence;

    #@1d3
    move-object/from16 v0, p0

    #@1d5
    invoke-direct {v0, v2}, Landroid/inputmethodservice/KeyboardView;->adjustCase(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@1d8
    move-result-object v2

    #@1d9
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1dc
    move-result-object v22

    #@1dd
    goto/16 :goto_110

    #@1df
    .line 703
    .restart local v7       #bounds:Landroid/graphics/Rect;
    .restart local v22       #label:Ljava/lang/String;
    :cond_1df
    move-object/from16 v0, p0

    #@1e1
    iget v2, v0, Landroid/inputmethodservice/KeyboardView;->mKeyTextSize:I

    #@1e3
    int-to-float v2, v2

    #@1e4
    invoke-virtual {v6, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    #@1e7
    .line 704
    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    #@1e9
    invoke-virtual {v6, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    #@1ec
    goto/16 :goto_167

    #@1ee
    .line 717
    :cond_1ee
    move-object/from16 v0, v18

    #@1f0
    iget-object v2, v0, Landroid/inputmethodservice/Keyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    #@1f2
    if-eqz v2, :cond_1ba

    #@1f4
    .line 718
    move-object/from16 v0, v18

    #@1f6
    iget v2, v0, Landroid/inputmethodservice/Keyboard$Key;->width:I

    #@1f8
    move-object/from16 v0, v23

    #@1fa
    iget v3, v0, Landroid/graphics/Rect;->left:I

    #@1fc
    sub-int/2addr v2, v3

    #@1fd
    move-object/from16 v0, v23

    #@1ff
    iget v3, v0, Landroid/graphics/Rect;->right:I

    #@201
    sub-int/2addr v2, v3

    #@202
    move-object/from16 v0, v18

    #@204
    iget-object v3, v0, Landroid/inputmethodservice/Keyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    #@206
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@209
    move-result v3

    #@20a
    sub-int/2addr v2, v3

    #@20b
    div-int/lit8 v2, v2, 0x2

    #@20d
    move-object/from16 v0, v23

    #@20f
    iget v3, v0, Landroid/graphics/Rect;->left:I

    #@211
    add-int v11, v2, v3

    #@213
    .line 720
    .local v11, drawableX:I
    move-object/from16 v0, v18

    #@215
    iget v2, v0, Landroid/inputmethodservice/Keyboard$Key;->height:I

    #@217
    move-object/from16 v0, v23

    #@219
    iget v3, v0, Landroid/graphics/Rect;->top:I

    #@21b
    sub-int/2addr v2, v3

    #@21c
    move-object/from16 v0, v23

    #@21e
    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    #@220
    sub-int/2addr v2, v3

    #@221
    move-object/from16 v0, v18

    #@223
    iget-object v3, v0, Landroid/inputmethodservice/Keyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    #@225
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@228
    move-result v3

    #@229
    sub-int/2addr v2, v3

    #@22a
    div-int/lit8 v2, v2, 0x2

    #@22c
    move-object/from16 v0, v23

    #@22e
    iget v3, v0, Landroid/graphics/Rect;->top:I

    #@230
    add-int v12, v2, v3

    #@232
    .line 722
    .local v12, drawableY:I
    int-to-float v2, v11

    #@233
    int-to-float v3, v12

    #@234
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    #@237
    .line 723
    move-object/from16 v0, v18

    #@239
    iget-object v2, v0, Landroid/inputmethodservice/Keyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    #@23b
    const/4 v3, 0x0

    #@23c
    const/4 v4, 0x0

    #@23d
    move-object/from16 v0, v18

    #@23f
    iget-object v5, v0, Landroid/inputmethodservice/Keyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    #@241
    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@244
    move-result v5

    #@245
    move-object/from16 v0, v18

    #@247
    iget-object v0, v0, Landroid/inputmethodservice/Keyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    #@249
    move-object/from16 v25, v0

    #@24b
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@24e
    move-result v25

    #@24f
    move/from16 v0, v25

    #@251
    invoke-virtual {v2, v3, v4, v5, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@254
    .line 725
    move-object/from16 v0, v18

    #@256
    iget-object v2, v0, Landroid/inputmethodservice/Keyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    #@258
    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@25b
    .line 726
    neg-int v2, v11

    #@25c
    int-to-float v2, v2

    #@25d
    neg-int v3, v12

    #@25e
    int-to-float v3, v3

    #@25f
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    #@262
    goto/16 :goto_1ba

    #@264
    .line 730
    .end local v7           #bounds:Landroid/graphics/Rect;
    .end local v10           #drawableState:[I
    .end local v11           #drawableX:I
    .end local v12           #drawableY:I
    .end local v18           #key:Landroid/inputmethodservice/Keyboard$Key;
    .end local v22           #label:Ljava/lang/String;
    :cond_264
    const/4 v2, 0x0

    #@265
    move-object/from16 v0, p0

    #@267
    iput-object v2, v0, Landroid/inputmethodservice/KeyboardView;->mInvalidatedKey:Landroid/inputmethodservice/Keyboard$Key;

    #@269
    .line 732
    move-object/from16 v0, p0

    #@26b
    iget-boolean v2, v0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboardOnScreen:Z

    #@26d
    if-eqz v2, :cond_28b

    #@26f
    .line 733
    move-object/from16 v0, p0

    #@271
    iget v2, v0, Landroid/inputmethodservice/KeyboardView;->mBackgroundDimAmount:F

    #@273
    const/high16 v3, 0x437f

    #@275
    mul-float/2addr v2, v3

    #@276
    float-to-int v2, v2

    #@277
    shl-int/lit8 v2, v2, 0x18

    #@279
    invoke-virtual {v6, v2}, Landroid/graphics/Paint;->setColor(I)V

    #@27c
    .line 734
    const/4 v2, 0x0

    #@27d
    const/4 v3, 0x0

    #@27e
    invoke-virtual/range {p0 .. p0}, Landroid/inputmethodservice/KeyboardView;->getWidth()I

    #@281
    move-result v4

    #@282
    int-to-float v4, v4

    #@283
    invoke-virtual/range {p0 .. p0}, Landroid/inputmethodservice/KeyboardView;->getHeight()I

    #@286
    move-result v5

    #@287
    int-to-float v5, v5

    #@288
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    #@28b
    .line 748
    :cond_28b
    const/4 v2, 0x0

    #@28c
    move-object/from16 v0, p0

    #@28e
    iput-boolean v2, v0, Landroid/inputmethodservice/KeyboardView;->mDrawPending:Z

    #@290
    .line 749
    move-object/from16 v0, p0

    #@292
    iget-object v2, v0, Landroid/inputmethodservice/KeyboardView;->mDirtyRect:Landroid/graphics/Rect;

    #@294
    invoke-virtual {v2}, Landroid/graphics/Rect;->setEmpty()V

    #@297
    goto/16 :goto_7a
.end method

.method private onModifiedTouchEvent(Landroid/view/MotionEvent;Z)Z
    .registers 15
    .parameter "me"
    .parameter "possiblePoly"

    #@0
    .prologue
    .line 1223
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@3
    move-result v0

    #@4
    float-to-int v0, v0

    #@5
    iget v1, p0, Landroid/view/View;->mPaddingLeft:I

    #@7
    sub-int v2, v0, v1

    #@9
    .line 1224
    .local v2, touchX:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@c
    move-result v0

    #@d
    float-to-int v0, v0

    #@e
    iget v1, p0, Landroid/view/View;->mPaddingTop:I

    #@10
    sub-int v3, v0, v1

    #@12
    .line 1225
    .local v3, touchY:I
    iget v0, p0, Landroid/inputmethodservice/KeyboardView;->mVerticalCorrection:I

    #@14
    neg-int v0, v0

    #@15
    if-lt v3, v0, :cond_1a

    #@17
    .line 1226
    iget v0, p0, Landroid/inputmethodservice/KeyboardView;->mVerticalCorrection:I

    #@19
    add-int/2addr v3, v0

    #@1a
    .line 1227
    :cond_1a
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@1d
    move-result v6

    #@1e
    .line 1228
    .local v6, action:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    #@21
    move-result-wide v4

    #@22
    .line 1229
    .local v4, eventTime:J
    const/4 v0, 0x0

    #@23
    invoke-direct {p0, v2, v3, v0}, Landroid/inputmethodservice/KeyboardView;->getKeyIndices(II[I)I

    #@26
    move-result v8

    #@27
    .line 1230
    .local v8, keyIndex:I
    iput-boolean p2, p0, Landroid/inputmethodservice/KeyboardView;->mPossiblePoly:Z

    #@29
    .line 1233
    if-nez v6, :cond_30

    #@2b
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mSwipeTracker:Landroid/inputmethodservice/KeyboardView$SwipeTracker;

    #@2d
    invoke-virtual {v0}, Landroid/inputmethodservice/KeyboardView$SwipeTracker;->clear()V

    #@30
    .line 1234
    :cond_30
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mSwipeTracker:Landroid/inputmethodservice/KeyboardView$SwipeTracker;

    #@32
    invoke-virtual {v0, p1}, Landroid/inputmethodservice/KeyboardView$SwipeTracker;->addMovement(Landroid/view/MotionEvent;)V

    #@35
    .line 1237
    iget-boolean v0, p0, Landroid/inputmethodservice/KeyboardView;->mAbortKey:Z

    #@37
    if-eqz v0, :cond_40

    #@39
    if-eqz v6, :cond_40

    #@3b
    const/4 v0, 0x3

    #@3c
    if-eq v6, v0, :cond_40

    #@3e
    .line 1239
    const/4 v0, 0x1

    #@3f
    .line 1361
    :goto_3f
    return v0

    #@40
    .line 1242
    :cond_40
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mGestureDetector:Landroid/view/GestureDetector;

    #@42
    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@45
    move-result v0

    #@46
    if-eqz v0, :cond_5a

    #@48
    .line 1243
    const/4 v0, -0x1

    #@49
    invoke-direct {p0, v0}, Landroid/inputmethodservice/KeyboardView;->showPreview(I)V

    #@4c
    .line 1244
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mHandler:Landroid/os/Handler;

    #@4e
    const/4 v1, 0x3

    #@4f
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@52
    .line 1245
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mHandler:Landroid/os/Handler;

    #@54
    const/4 v1, 0x4

    #@55
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@58
    .line 1246
    const/4 v0, 0x1

    #@59
    goto :goto_3f

    #@5a
    .line 1251
    :cond_5a
    iget-boolean v0, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboardOnScreen:Z

    #@5c
    if-eqz v0, :cond_63

    #@5e
    const/4 v0, 0x3

    #@5f
    if-eq v6, v0, :cond_63

    #@61
    .line 1252
    const/4 v0, 0x1

    #@62
    goto :goto_3f

    #@63
    .line 1255
    :cond_63
    packed-switch v6, :pswitch_data_1cc

    #@66
    .line 1359
    :goto_66
    iput v2, p0, Landroid/inputmethodservice/KeyboardView;->mLastX:I

    #@68
    .line 1360
    iput v3, p0, Landroid/inputmethodservice/KeyboardView;->mLastY:I

    #@6a
    .line 1361
    const/4 v0, 0x1

    #@6b
    goto :goto_3f

    #@6c
    .line 1257
    :pswitch_6c
    const/4 v0, 0x0

    #@6d
    iput-boolean v0, p0, Landroid/inputmethodservice/KeyboardView;->mAbortKey:Z

    #@6f
    .line 1258
    iput v2, p0, Landroid/inputmethodservice/KeyboardView;->mStartX:I

    #@71
    .line 1259
    iput v3, p0, Landroid/inputmethodservice/KeyboardView;->mStartY:I

    #@73
    .line 1260
    iput v2, p0, Landroid/inputmethodservice/KeyboardView;->mLastCodeX:I

    #@75
    .line 1261
    iput v3, p0, Landroid/inputmethodservice/KeyboardView;->mLastCodeY:I

    #@77
    .line 1262
    const-wide/16 v0, 0x0

    #@79
    iput-wide v0, p0, Landroid/inputmethodservice/KeyboardView;->mLastKeyTime:J

    #@7b
    .line 1263
    const-wide/16 v0, 0x0

    #@7d
    iput-wide v0, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKeyTime:J

    #@7f
    .line 1264
    const/4 v0, -0x1

    #@80
    iput v0, p0, Landroid/inputmethodservice/KeyboardView;->mLastKey:I

    #@82
    .line 1265
    iput v8, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKey:I

    #@84
    .line 1266
    iput v8, p0, Landroid/inputmethodservice/KeyboardView;->mDownKey:I

    #@86
    .line 1267
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    #@89
    move-result-wide v0

    #@8a
    iput-wide v0, p0, Landroid/inputmethodservice/KeyboardView;->mDownTime:J

    #@8c
    .line 1268
    iget-wide v0, p0, Landroid/inputmethodservice/KeyboardView;->mDownTime:J

    #@8e
    iput-wide v0, p0, Landroid/inputmethodservice/KeyboardView;->mLastMoveTime:J

    #@90
    .line 1269
    invoke-direct {p0, v4, v5, v8}, Landroid/inputmethodservice/KeyboardView;->checkMultiTap(JI)V

    #@93
    .line 1270
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mKeyboardActionListener:Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;

    #@95
    const/4 v0, -0x1

    #@96
    if-eq v8, v0, :cond_cf

    #@98
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@9a
    aget-object v0, v0, v8

    #@9c
    iget-object v0, v0, Landroid/inputmethodservice/Keyboard$Key;->codes:[I

    #@9e
    const/4 v10, 0x0

    #@9f
    aget v0, v0, v10

    #@a1
    :goto_a1
    invoke-interface {v1, v0}, Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;->onPress(I)V

    #@a4
    .line 1272
    iget v0, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKey:I

    #@a6
    if-ltz v0, :cond_d1

    #@a8
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@aa
    iget v1, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKey:I

    #@ac
    aget-object v0, v0, v1

    #@ae
    iget-boolean v0, v0, Landroid/inputmethodservice/Keyboard$Key;->repeatable:Z

    #@b0
    if-eqz v0, :cond_d1

    #@b2
    .line 1273
    iget v0, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKey:I

    #@b4
    iput v0, p0, Landroid/inputmethodservice/KeyboardView;->mRepeatKeyIndex:I

    #@b6
    .line 1274
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mHandler:Landroid/os/Handler;

    #@b8
    const/4 v1, 0x3

    #@b9
    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@bc
    move-result-object v9

    #@bd
    .line 1275
    .local v9, msg:Landroid/os/Message;
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mHandler:Landroid/os/Handler;

    #@bf
    const-wide/16 v10, 0x190

    #@c1
    invoke-virtual {v0, v9, v10, v11}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@c4
    .line 1276
    invoke-direct {p0}, Landroid/inputmethodservice/KeyboardView;->repeatKey()Z

    #@c7
    .line 1278
    iget-boolean v0, p0, Landroid/inputmethodservice/KeyboardView;->mAbortKey:Z

    #@c9
    if-eqz v0, :cond_d1

    #@cb
    .line 1279
    const/4 v0, -0x1

    #@cc
    iput v0, p0, Landroid/inputmethodservice/KeyboardView;->mRepeatKeyIndex:I

    #@ce
    goto :goto_66

    #@cf
    .line 1270
    .end local v9           #msg:Landroid/os/Message;
    :cond_cf
    const/4 v0, 0x0

    #@d0
    goto :goto_a1

    #@d1
    .line 1283
    :cond_d1
    iget v0, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKey:I

    #@d3
    const/4 v1, -0x1

    #@d4
    if-eq v0, v1, :cond_e5

    #@d6
    .line 1284
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mHandler:Landroid/os/Handler;

    #@d8
    const/4 v1, 0x4

    #@d9
    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@dc
    move-result-object v9

    #@dd
    .line 1285
    .restart local v9       #msg:Landroid/os/Message;
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mHandler:Landroid/os/Handler;

    #@df
    sget v1, Landroid/inputmethodservice/KeyboardView;->LONGPRESS_TIMEOUT:I

    #@e1
    int-to-long v10, v1

    #@e2
    invoke-virtual {v0, v9, v10, v11}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@e5
    .line 1287
    .end local v9           #msg:Landroid/os/Message;
    :cond_e5
    invoke-direct {p0, v8}, Landroid/inputmethodservice/KeyboardView;->showPreview(I)V

    #@e8
    goto/16 :goto_66

    #@ea
    .line 1291
    :pswitch_ea
    const/4 v7, 0x0

    #@eb
    .line 1292
    .local v7, continueLongPress:Z
    const/4 v0, -0x1

    #@ec
    if-eq v8, v0, :cond_fb

    #@ee
    .line 1293
    iget v0, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKey:I

    #@f0
    const/4 v1, -0x1

    #@f1
    if-ne v0, v1, :cond_11e

    #@f3
    .line 1294
    iput v8, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKey:I

    #@f5
    .line 1295
    iget-wide v0, p0, Landroid/inputmethodservice/KeyboardView;->mDownTime:J

    #@f7
    sub-long v0, v4, v0

    #@f9
    iput-wide v0, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKeyTime:J

    #@fb
    .line 1312
    :cond_fb
    :goto_fb
    if-nez v7, :cond_115

    #@fd
    .line 1314
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mHandler:Landroid/os/Handler;

    #@ff
    const/4 v1, 0x4

    #@100
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@103
    .line 1316
    const/4 v0, -0x1

    #@104
    if-eq v8, v0, :cond_115

    #@106
    .line 1317
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mHandler:Landroid/os/Handler;

    #@108
    const/4 v1, 0x4

    #@109
    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@10c
    move-result-object v9

    #@10d
    .line 1318
    .restart local v9       #msg:Landroid/os/Message;
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mHandler:Landroid/os/Handler;

    #@10f
    sget v1, Landroid/inputmethodservice/KeyboardView;->LONGPRESS_TIMEOUT:I

    #@111
    int-to-long v10, v1

    #@112
    invoke-virtual {v0, v9, v10, v11}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@115
    .line 1321
    .end local v9           #msg:Landroid/os/Message;
    :cond_115
    iget v0, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKey:I

    #@117
    invoke-direct {p0, v0}, Landroid/inputmethodservice/KeyboardView;->showPreview(I)V

    #@11a
    .line 1322
    iput-wide v4, p0, Landroid/inputmethodservice/KeyboardView;->mLastMoveTime:J

    #@11c
    goto/16 :goto_66

    #@11e
    .line 1297
    :cond_11e
    iget v0, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKey:I

    #@120
    if-ne v8, v0, :cond_12d

    #@122
    .line 1298
    iget-wide v0, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKeyTime:J

    #@124
    iget-wide v10, p0, Landroid/inputmethodservice/KeyboardView;->mLastMoveTime:J

    #@126
    sub-long v10, v4, v10

    #@128
    add-long/2addr v0, v10

    #@129
    iput-wide v0, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKeyTime:J

    #@12b
    .line 1299
    const/4 v7, 0x1

    #@12c
    goto :goto_fb

    #@12d
    .line 1300
    :cond_12d
    iget v0, p0, Landroid/inputmethodservice/KeyboardView;->mRepeatKeyIndex:I

    #@12f
    const/4 v1, -0x1

    #@130
    if-ne v0, v1, :cond_fb

    #@132
    .line 1301
    invoke-direct {p0}, Landroid/inputmethodservice/KeyboardView;->resetMultiTap()V

    #@135
    .line 1302
    iget v0, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKey:I

    #@137
    iput v0, p0, Landroid/inputmethodservice/KeyboardView;->mLastKey:I

    #@139
    .line 1303
    iget v0, p0, Landroid/inputmethodservice/KeyboardView;->mLastX:I

    #@13b
    iput v0, p0, Landroid/inputmethodservice/KeyboardView;->mLastCodeX:I

    #@13d
    .line 1304
    iget v0, p0, Landroid/inputmethodservice/KeyboardView;->mLastY:I

    #@13f
    iput v0, p0, Landroid/inputmethodservice/KeyboardView;->mLastCodeY:I

    #@141
    .line 1305
    iget-wide v0, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKeyTime:J

    #@143
    add-long/2addr v0, v4

    #@144
    iget-wide v10, p0, Landroid/inputmethodservice/KeyboardView;->mLastMoveTime:J

    #@146
    sub-long/2addr v0, v10

    #@147
    iput-wide v0, p0, Landroid/inputmethodservice/KeyboardView;->mLastKeyTime:J

    #@149
    .line 1307
    iput v8, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKey:I

    #@14b
    .line 1308
    const-wide/16 v0, 0x0

    #@14d
    iput-wide v0, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKeyTime:J

    #@14f
    goto :goto_fb

    #@150
    .line 1326
    .end local v7           #continueLongPress:Z
    :pswitch_150
    invoke-direct {p0}, Landroid/inputmethodservice/KeyboardView;->removeMessages()V

    #@153
    .line 1327
    iget v0, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKey:I

    #@155
    if-ne v8, v0, :cond_1a2

    #@157
    .line 1328
    iget-wide v0, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKeyTime:J

    #@159
    iget-wide v10, p0, Landroid/inputmethodservice/KeyboardView;->mLastMoveTime:J

    #@15b
    sub-long v10, v4, v10

    #@15d
    add-long/2addr v0, v10

    #@15e
    iput-wide v0, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKeyTime:J

    #@160
    .line 1336
    :goto_160
    iget-wide v0, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKeyTime:J

    #@162
    iget-wide v10, p0, Landroid/inputmethodservice/KeyboardView;->mLastKeyTime:J

    #@164
    cmp-long v0, v0, v10

    #@166
    if-gez v0, :cond_17d

    #@168
    iget-wide v0, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKeyTime:J

    #@16a
    const-wide/16 v10, 0x46

    #@16c
    cmp-long v0, v0, v10

    #@16e
    if-gez v0, :cond_17d

    #@170
    iget v0, p0, Landroid/inputmethodservice/KeyboardView;->mLastKey:I

    #@172
    const/4 v1, -0x1

    #@173
    if-eq v0, v1, :cond_17d

    #@175
    .line 1338
    iget v0, p0, Landroid/inputmethodservice/KeyboardView;->mLastKey:I

    #@177
    iput v0, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKey:I

    #@179
    .line 1339
    iget v2, p0, Landroid/inputmethodservice/KeyboardView;->mLastCodeX:I

    #@17b
    .line 1340
    iget v3, p0, Landroid/inputmethodservice/KeyboardView;->mLastCodeY:I

    #@17d
    .line 1342
    :cond_17d
    const/4 v0, -0x1

    #@17e
    invoke-direct {p0, v0}, Landroid/inputmethodservice/KeyboardView;->showPreview(I)V

    #@181
    .line 1343
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mKeyIndices:[I

    #@183
    const/4 v1, -0x1

    #@184
    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    #@187
    .line 1345
    iget v0, p0, Landroid/inputmethodservice/KeyboardView;->mRepeatKeyIndex:I

    #@189
    const/4 v1, -0x1

    #@18a
    if-ne v0, v1, :cond_19a

    #@18c
    iget-boolean v0, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboardOnScreen:Z

    #@18e
    if-nez v0, :cond_19a

    #@190
    iget-boolean v0, p0, Landroid/inputmethodservice/KeyboardView;->mAbortKey:Z

    #@192
    if-nez v0, :cond_19a

    #@194
    .line 1346
    iget v1, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKey:I

    #@196
    move-object v0, p0

    #@197
    invoke-direct/range {v0 .. v5}, Landroid/inputmethodservice/KeyboardView;->detectAndSendKey(IIIJ)V

    #@19a
    .line 1348
    :cond_19a
    invoke-virtual {p0, v8}, Landroid/inputmethodservice/KeyboardView;->invalidateKey(I)V

    #@19d
    .line 1349
    const/4 v0, -0x1

    #@19e
    iput v0, p0, Landroid/inputmethodservice/KeyboardView;->mRepeatKeyIndex:I

    #@1a0
    goto/16 :goto_66

    #@1a2
    .line 1330
    :cond_1a2
    invoke-direct {p0}, Landroid/inputmethodservice/KeyboardView;->resetMultiTap()V

    #@1a5
    .line 1331
    iget v0, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKey:I

    #@1a7
    iput v0, p0, Landroid/inputmethodservice/KeyboardView;->mLastKey:I

    #@1a9
    .line 1332
    iget-wide v0, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKeyTime:J

    #@1ab
    add-long/2addr v0, v4

    #@1ac
    iget-wide v10, p0, Landroid/inputmethodservice/KeyboardView;->mLastMoveTime:J

    #@1ae
    sub-long/2addr v0, v10

    #@1af
    iput-wide v0, p0, Landroid/inputmethodservice/KeyboardView;->mLastKeyTime:J

    #@1b1
    .line 1333
    iput v8, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKey:I

    #@1b3
    .line 1334
    const-wide/16 v0, 0x0

    #@1b5
    iput-wide v0, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKeyTime:J

    #@1b7
    goto :goto_160

    #@1b8
    .line 1352
    :pswitch_1b8
    invoke-direct {p0}, Landroid/inputmethodservice/KeyboardView;->removeMessages()V

    #@1bb
    .line 1353
    invoke-direct {p0}, Landroid/inputmethodservice/KeyboardView;->dismissPopupKeyboard()V

    #@1be
    .line 1354
    const/4 v0, 0x1

    #@1bf
    iput-boolean v0, p0, Landroid/inputmethodservice/KeyboardView;->mAbortKey:Z

    #@1c1
    .line 1355
    const/4 v0, -0x1

    #@1c2
    invoke-direct {p0, v0}, Landroid/inputmethodservice/KeyboardView;->showPreview(I)V

    #@1c5
    .line 1356
    iget v0, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKey:I

    #@1c7
    invoke-virtual {p0, v0}, Landroid/inputmethodservice/KeyboardView;->invalidateKey(I)V

    #@1ca
    goto/16 :goto_66

    #@1cc
    .line 1255
    :pswitch_data_1cc
    .packed-switch 0x0
        :pswitch_6c
        :pswitch_150
        :pswitch_ea
        :pswitch_1b8
    .end packed-switch
.end method

.method private openPopupIfRequired(Landroid/view/MotionEvent;)Z
    .registers 6
    .parameter "me"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1061
    iget v2, p0, Landroid/inputmethodservice/KeyboardView;->mPopupLayout:I

    #@3
    if-nez v2, :cond_6

    #@5
    .line 1074
    :cond_5
    :goto_5
    return v1

    #@6
    .line 1064
    :cond_6
    iget v2, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKey:I

    #@8
    if-ltz v2, :cond_5

    #@a
    iget v2, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKey:I

    #@c
    iget-object v3, p0, Landroid/inputmethodservice/KeyboardView;->mKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@e
    array-length v3, v3

    #@f
    if-ge v2, v3, :cond_5

    #@11
    .line 1068
    iget-object v2, p0, Landroid/inputmethodservice/KeyboardView;->mKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@13
    iget v3, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKey:I

    #@15
    aget-object v0, v2, v3

    #@17
    .line 1069
    .local v0, popupKey:Landroid/inputmethodservice/Keyboard$Key;
    invoke-virtual {p0, v0}, Landroid/inputmethodservice/KeyboardView;->onLongPress(Landroid/inputmethodservice/Keyboard$Key;)Z

    #@1a
    move-result v1

    #@1b
    .line 1070
    .local v1, result:Z
    if-eqz v1, :cond_5

    #@1d
    .line 1071
    const/4 v2, 0x1

    #@1e
    iput-boolean v2, p0, Landroid/inputmethodservice/KeyboardView;->mAbortKey:Z

    #@20
    .line 1072
    const/4 v2, -0x1

    #@21
    invoke-direct {p0, v2}, Landroid/inputmethodservice/KeyboardView;->showPreview(I)V

    #@24
    goto :goto_5
.end method

.method private removeMessages()V
    .registers 3

    #@0
    .prologue
    .line 1399
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mHandler:Landroid/os/Handler;

    #@2
    const/4 v1, 0x3

    #@3
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@6
    .line 1400
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mHandler:Landroid/os/Handler;

    #@8
    const/4 v1, 0x4

    #@9
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@c
    .line 1401
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mHandler:Landroid/os/Handler;

    #@e
    const/4 v1, 0x1

    #@f
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@12
    .line 1402
    return-void
.end method

.method private repeatKey()Z
    .registers 8

    #@0
    .prologue
    .line 1365
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@2
    iget v1, p0, Landroid/inputmethodservice/KeyboardView;->mRepeatKeyIndex:I

    #@4
    aget-object v6, v0, v1

    #@6
    .line 1366
    .local v6, key:Landroid/inputmethodservice/Keyboard$Key;
    iget v1, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKey:I

    #@8
    iget v2, v6, Landroid/inputmethodservice/Keyboard$Key;->x:I

    #@a
    iget v3, v6, Landroid/inputmethodservice/Keyboard$Key;->y:I

    #@c
    iget-wide v4, p0, Landroid/inputmethodservice/KeyboardView;->mLastTapTime:J

    #@e
    move-object v0, p0

    #@f
    invoke-direct/range {v0 .. v5}, Landroid/inputmethodservice/KeyboardView;->detectAndSendKey(IIIJ)V

    #@12
    .line 1367
    const/4 v0, 0x1

    #@13
    return v0
.end method

.method private resetMultiTap()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1427
    const/4 v0, -0x1

    #@2
    iput v0, p0, Landroid/inputmethodservice/KeyboardView;->mLastSentIndex:I

    #@4
    .line 1428
    iput v2, p0, Landroid/inputmethodservice/KeyboardView;->mTapCount:I

    #@6
    .line 1429
    const-wide/16 v0, -0x1

    #@8
    iput-wide v0, p0, Landroid/inputmethodservice/KeyboardView;->mLastTapTime:J

    #@a
    .line 1430
    iput-boolean v2, p0, Landroid/inputmethodservice/KeyboardView;->mInMultiTap:Z

    #@c
    .line 1431
    return-void
.end method

.method private sendAccessibilityEventForUnicodeCharacter(II)V
    .registers 9
    .parameter "eventType"
    .parameter "code"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 975
    iget-object v4, p0, Landroid/inputmethodservice/KeyboardView;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    #@4
    invoke-virtual {v4}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@7
    move-result v4

    #@8
    if-eqz v4, :cond_48

    #@a
    .line 976
    invoke-static {p1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    #@d
    move-result-object v0

    #@e
    .line 977
    .local v0, event:Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {p0, v0}, Landroid/inputmethodservice/KeyboardView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@11
    .line 978
    const/4 v2, 0x0

    #@12
    .line 980
    .local v2, text:Ljava/lang/String;
    iget-object v4, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@14
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@17
    move-result-object v4

    #@18
    const-string/jumbo v5, "speak_password"

    #@1b
    invoke-static {v4, v5, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@1e
    move-result v4

    #@1f
    if-eqz v4, :cond_22

    #@21
    move v1, v3

    #@22
    .line 984
    .local v1, speakPassword:Z
    :cond_22
    if-nez v1, :cond_34

    #@24
    iget-object v4, p0, Landroid/inputmethodservice/KeyboardView;->mAudioManager:Landroid/media/AudioManager;

    #@26
    invoke-virtual {v4}, Landroid/media/AudioManager;->isBluetoothA2dpOn()Z

    #@29
    move-result v4

    #@2a
    if-nez v4, :cond_34

    #@2c
    iget-object v4, p0, Landroid/inputmethodservice/KeyboardView;->mAudioManager:Landroid/media/AudioManager;

    #@2e
    invoke-virtual {v4}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    #@31
    move-result v4

    #@32
    if-eqz v4, :cond_8f

    #@34
    .line 986
    :cond_34
    sparse-switch p2, :sswitch_data_ae

    #@37
    .line 1009
    int-to-char v3, p2

    #@38
    invoke-static {v3}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    #@3b
    move-result-object v2

    #@3c
    .line 1021
    :goto_3c
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    #@3f
    move-result-object v3

    #@40
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@43
    .line 1022
    iget-object v3, p0, Landroid/inputmethodservice/KeyboardView;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    #@45
    invoke-virtual {v3, v0}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@48
    .line 1024
    .end local v0           #event:Landroid/view/accessibility/AccessibilityEvent;
    .end local v1           #speakPassword:Z
    .end local v2           #text:Ljava/lang/String;
    :cond_48
    return-void

    #@49
    .line 988
    .restart local v0       #event:Landroid/view/accessibility/AccessibilityEvent;
    .restart local v1       #speakPassword:Z
    .restart local v2       #text:Ljava/lang/String;
    :sswitch_49
    iget-object v3, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@4b
    const v4, 0x10404f3

    #@4e
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@51
    move-result-object v2

    #@52
    .line 989
    goto :goto_3c

    #@53
    .line 991
    :sswitch_53
    iget-object v3, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@55
    const v4, 0x10404f4

    #@58
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@5b
    move-result-object v2

    #@5c
    .line 992
    goto :goto_3c

    #@5d
    .line 994
    :sswitch_5d
    iget-object v3, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@5f
    const v4, 0x10404f5

    #@62
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@65
    move-result-object v2

    #@66
    .line 995
    goto :goto_3c

    #@67
    .line 997
    :sswitch_67
    iget-object v3, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@69
    const v4, 0x10404f6

    #@6c
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@6f
    move-result-object v2

    #@70
    .line 998
    goto :goto_3c

    #@71
    .line 1000
    :sswitch_71
    iget-object v3, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@73
    const v4, 0x10404f7

    #@76
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@79
    move-result-object v2

    #@7a
    .line 1001
    goto :goto_3c

    #@7b
    .line 1003
    :sswitch_7b
    iget-object v3, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@7d
    const v4, 0x10404f8

    #@80
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@83
    move-result-object v2

    #@84
    .line 1004
    goto :goto_3c

    #@85
    .line 1006
    :sswitch_85
    iget-object v3, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@87
    const v4, 0x10404f9

    #@8a
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@8d
    move-result-object v2

    #@8e
    .line 1007
    goto :goto_3c

    #@8f
    .line 1011
    :cond_8f
    iget-boolean v4, p0, Landroid/inputmethodservice/KeyboardView;->mHeadsetRequiredToHearPasswordsAnnounced:Z

    #@91
    if-nez v4, :cond_a3

    #@93
    .line 1014
    const/16 v4, 0x100

    #@95
    if-ne p1, v4, :cond_99

    #@97
    .line 1015
    iput-boolean v3, p0, Landroid/inputmethodservice/KeyboardView;->mHeadsetRequiredToHearPasswordsAnnounced:Z

    #@99
    .line 1017
    :cond_99
    iget-object v3, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@9b
    const v4, 0x1040508

    #@9e
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@a1
    move-result-object v2

    #@a2
    goto :goto_3c

    #@a3
    .line 1019
    :cond_a3
    iget-object v3, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@a5
    const v4, 0x1040509

    #@a8
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@ab
    move-result-object v2

    #@ac
    goto :goto_3c

    #@ad
    .line 986
    nop

    #@ae
    :sswitch_data_ae
    .sparse-switch
        -0x6 -> :sswitch_49
        -0x5 -> :sswitch_5d
        -0x4 -> :sswitch_67
        -0x3 -> :sswitch_53
        -0x2 -> :sswitch_71
        -0x1 -> :sswitch_7b
        0xa -> :sswitch_85
    .end sparse-switch
.end method

.method private showKey(I)V
    .registers 16
    .parameter "keyIndex"

    #@0
    .prologue
    const/4 v13, 0x2

    #@1
    const-wide/high16 v11, 0x4004

    #@3
    const/4 v10, 0x1

    #@4
    const/4 v8, 0x0

    #@5
    const/4 v9, 0x0

    #@6
    .line 901
    iget-object v5, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewPopup:Landroid/widget/PopupWindow;

    #@8
    .line 902
    .local v5, previewPopup:Landroid/widget/PopupWindow;
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@a
    .line 903
    .local v1, keys:[Landroid/inputmethodservice/Keyboard$Key;
    if-ltz p1, :cond_11

    #@c
    iget-object v6, p0, Landroid/inputmethodservice/KeyboardView;->mKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@e
    array-length v6, v6

    #@f
    if-lt p1, v6, :cond_12

    #@11
    .line 972
    :cond_11
    :goto_11
    return-void

    #@12
    .line 904
    :cond_12
    aget-object v0, v1, p1

    #@14
    .line 905
    .local v0, key:Landroid/inputmethodservice/Keyboard$Key;
    iget-object v6, v0, Landroid/inputmethodservice/Keyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    #@16
    if-eqz v6, :cond_f6

    #@18
    .line 906
    iget-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewText:Landroid/widget/TextView;

    #@1a
    iget-object v6, v0, Landroid/inputmethodservice/Keyboard$Key;->iconPreview:Landroid/graphics/drawable/Drawable;

    #@1c
    if-eqz v6, :cond_f2

    #@1e
    iget-object v6, v0, Landroid/inputmethodservice/Keyboard$Key;->iconPreview:Landroid/graphics/drawable/Drawable;

    #@20
    :goto_20
    invoke-virtual {v7, v8, v8, v8, v6}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    #@23
    .line 908
    iget-object v6, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewText:Landroid/widget/TextView;

    #@25
    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@28
    .line 920
    :goto_28
    iget-object v6, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewText:Landroid/widget/TextView;

    #@2a
    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@2d
    move-result v7

    #@2e
    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@31
    move-result v8

    #@32
    invoke-virtual {v6, v7, v8}, Landroid/widget/TextView;->measure(II)V

    #@35
    .line 922
    iget-object v6, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewText:Landroid/widget/TextView;

    #@37
    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredWidth()I

    #@3a
    move-result v6

    #@3b
    iget v7, v0, Landroid/inputmethodservice/Keyboard$Key;->width:I

    #@3d
    iget-object v8, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewText:Landroid/widget/TextView;

    #@3f
    invoke-virtual {v8}, Landroid/widget/TextView;->getPaddingLeft()I

    #@42
    move-result v8

    #@43
    add-int/2addr v7, v8

    #@44
    iget-object v8, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewText:Landroid/widget/TextView;

    #@46
    invoke-virtual {v8}, Landroid/widget/TextView;->getPaddingRight()I

    #@49
    move-result v8

    #@4a
    add-int/2addr v7, v8

    #@4b
    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    #@4e
    move-result v4

    #@4f
    .line 924
    .local v4, popupWidth:I
    iget v3, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewHeight:I

    #@51
    .line 925
    .local v3, popupHeight:I
    iget-object v6, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewText:Landroid/widget/TextView;

    #@53
    invoke-virtual {v6}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@56
    move-result-object v2

    #@57
    .line 926
    .local v2, lp:Landroid/view/ViewGroup$LayoutParams;
    if-eqz v2, :cond_5d

    #@59
    .line 927
    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@5b
    .line 928
    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@5d
    .line 930
    :cond_5d
    iget-boolean v6, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewCentered:Z

    #@5f
    if-nez v6, :cond_133

    #@61
    .line 931
    iget v6, v0, Landroid/inputmethodservice/Keyboard$Key;->x:I

    #@63
    iget-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewText:Landroid/widget/TextView;

    #@65
    invoke-virtual {v7}, Landroid/widget/TextView;->getPaddingLeft()I

    #@68
    move-result v7

    #@69
    sub-int/2addr v6, v7

    #@6a
    iget v7, p0, Landroid/view/View;->mPaddingLeft:I

    #@6c
    add-int/2addr v6, v7

    #@6d
    iput v6, p0, Landroid/inputmethodservice/KeyboardView;->mPopupPreviewX:I

    #@6f
    .line 932
    iget v6, v0, Landroid/inputmethodservice/Keyboard$Key;->y:I

    #@71
    sub-int/2addr v6, v3

    #@72
    iget v7, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewOffset:I

    #@74
    add-int/2addr v6, v7

    #@75
    iput v6, p0, Landroid/inputmethodservice/KeyboardView;->mPopupPreviewY:I

    #@77
    .line 938
    :goto_77
    iget-object v6, p0, Landroid/inputmethodservice/KeyboardView;->mHandler:Landroid/os/Handler;

    #@79
    invoke-virtual {v6, v13}, Landroid/os/Handler;->removeMessages(I)V

    #@7c
    .line 939
    iget-object v6, p0, Landroid/inputmethodservice/KeyboardView;->mCoordinates:[I

    #@7e
    invoke-virtual {p0, v6}, Landroid/inputmethodservice/KeyboardView;->getLocationInWindow([I)V

    #@81
    .line 940
    iget-object v6, p0, Landroid/inputmethodservice/KeyboardView;->mCoordinates:[I

    #@83
    aget v7, v6, v9

    #@85
    iget v8, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboardOffsetX:I

    #@87
    add-int/2addr v7, v8

    #@88
    aput v7, v6, v9

    #@8a
    .line 941
    iget-object v6, p0, Landroid/inputmethodservice/KeyboardView;->mCoordinates:[I

    #@8c
    aget v7, v6, v10

    #@8e
    iget v8, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboardOffsetY:I

    #@90
    add-int/2addr v7, v8

    #@91
    aput v7, v6, v10

    #@93
    .line 944
    iget-object v6, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewText:Landroid/widget/TextView;

    #@95
    invoke-virtual {v6}, Landroid/widget/TextView;->getBackground()Landroid/graphics/drawable/Drawable;

    #@98
    move-result-object v7

    #@99
    iget v6, v0, Landroid/inputmethodservice/Keyboard$Key;->popupResId:I

    #@9b
    if-eqz v6, :cond_14a

    #@9d
    sget-object v6, Landroid/inputmethodservice/KeyboardView;->LONG_PRESSABLE_STATE_SET:[I

    #@9f
    :goto_9f
    invoke-virtual {v7, v6}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@a2
    .line 946
    iget v6, p0, Landroid/inputmethodservice/KeyboardView;->mPopupPreviewX:I

    #@a4
    iget-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mCoordinates:[I

    #@a6
    aget v7, v7, v9

    #@a8
    add-int/2addr v6, v7

    #@a9
    iput v6, p0, Landroid/inputmethodservice/KeyboardView;->mPopupPreviewX:I

    #@ab
    .line 947
    iget v6, p0, Landroid/inputmethodservice/KeyboardView;->mPopupPreviewY:I

    #@ad
    iget-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mCoordinates:[I

    #@af
    aget v7, v7, v10

    #@b1
    add-int/2addr v6, v7

    #@b2
    iput v6, p0, Landroid/inputmethodservice/KeyboardView;->mPopupPreviewY:I

    #@b4
    .line 950
    iget-object v6, p0, Landroid/inputmethodservice/KeyboardView;->mCoordinates:[I

    #@b6
    invoke-virtual {p0, v6}, Landroid/inputmethodservice/KeyboardView;->getLocationOnScreen([I)V

    #@b9
    .line 951
    iget v6, p0, Landroid/inputmethodservice/KeyboardView;->mPopupPreviewY:I

    #@bb
    iget-object v7, p0, Landroid/inputmethodservice/KeyboardView;->mCoordinates:[I

    #@bd
    aget v7, v7, v10

    #@bf
    add-int/2addr v6, v7

    #@c0
    if-gez v6, :cond_de

    #@c2
    .line 954
    iget v6, v0, Landroid/inputmethodservice/Keyboard$Key;->x:I

    #@c4
    iget v7, v0, Landroid/inputmethodservice/Keyboard$Key;->width:I

    #@c6
    add-int/2addr v6, v7

    #@c7
    invoke-virtual {p0}, Landroid/inputmethodservice/KeyboardView;->getWidth()I

    #@ca
    move-result v7

    #@cb
    div-int/lit8 v7, v7, 0x2

    #@cd
    if-gt v6, v7, :cond_14e

    #@cf
    .line 955
    iget v6, p0, Landroid/inputmethodservice/KeyboardView;->mPopupPreviewX:I

    #@d1
    iget v7, v0, Landroid/inputmethodservice/Keyboard$Key;->width:I

    #@d3
    int-to-double v7, v7

    #@d4
    mul-double/2addr v7, v11

    #@d5
    double-to-int v7, v7

    #@d6
    add-int/2addr v6, v7

    #@d7
    iput v6, p0, Landroid/inputmethodservice/KeyboardView;->mPopupPreviewX:I

    #@d9
    .line 959
    :goto_d9
    iget v6, p0, Landroid/inputmethodservice/KeyboardView;->mPopupPreviewY:I

    #@db
    add-int/2addr v6, v3

    #@dc
    iput v6, p0, Landroid/inputmethodservice/KeyboardView;->mPopupPreviewY:I

    #@de
    .line 962
    :cond_de
    invoke-virtual {v5}, Landroid/widget/PopupWindow;->isShowing()Z

    #@e1
    move-result v6

    #@e2
    if-eqz v6, :cond_159

    #@e4
    .line 963
    iget v6, p0, Landroid/inputmethodservice/KeyboardView;->mPopupPreviewX:I

    #@e6
    iget v7, p0, Landroid/inputmethodservice/KeyboardView;->mPopupPreviewY:I

    #@e8
    invoke-virtual {v5, v6, v7, v4, v3}, Landroid/widget/PopupWindow;->update(IIII)V

    #@eb
    .line 971
    :goto_eb
    iget-object v6, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewText:Landroid/widget/TextView;

    #@ed
    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setVisibility(I)V

    #@f0
    goto/16 :goto_11

    #@f2
    .line 906
    .end local v2           #lp:Landroid/view/ViewGroup$LayoutParams;
    .end local v3           #popupHeight:I
    .end local v4           #popupWidth:I
    :cond_f2
    iget-object v6, v0, Landroid/inputmethodservice/Keyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    #@f4
    goto/16 :goto_20

    #@f6
    .line 910
    :cond_f6
    iget-object v6, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewText:Landroid/widget/TextView;

    #@f8
    invoke-virtual {v6, v8, v8, v8, v8}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    #@fb
    .line 911
    iget-object v6, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewText:Landroid/widget/TextView;

    #@fd
    invoke-direct {p0, v0}, Landroid/inputmethodservice/KeyboardView;->getPreviewText(Landroid/inputmethodservice/Keyboard$Key;)Ljava/lang/CharSequence;

    #@100
    move-result-object v7

    #@101
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@104
    .line 912
    iget-object v6, v0, Landroid/inputmethodservice/Keyboard$Key;->label:Ljava/lang/CharSequence;

    #@106
    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    #@109
    move-result v6

    #@10a
    if-le v6, v10, :cond_122

    #@10c
    iget-object v6, v0, Landroid/inputmethodservice/Keyboard$Key;->codes:[I

    #@10e
    array-length v6, v6

    #@10f
    if-ge v6, v13, :cond_122

    #@111
    .line 913
    iget-object v6, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewText:Landroid/widget/TextView;

    #@113
    iget v7, p0, Landroid/inputmethodservice/KeyboardView;->mKeyTextSize:I

    #@115
    int-to-float v7, v7

    #@116
    invoke-virtual {v6, v9, v7}, Landroid/widget/TextView;->setTextSize(IF)V

    #@119
    .line 914
    iget-object v6, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewText:Landroid/widget/TextView;

    #@11b
    sget-object v7, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    #@11d
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    #@120
    goto/16 :goto_28

    #@122
    .line 916
    :cond_122
    iget-object v6, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewText:Landroid/widget/TextView;

    #@124
    iget v7, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewTextSizeLarge:I

    #@126
    int-to-float v7, v7

    #@127
    invoke-virtual {v6, v9, v7}, Landroid/widget/TextView;->setTextSize(IF)V

    #@12a
    .line 917
    iget-object v6, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewText:Landroid/widget/TextView;

    #@12c
    sget-object v7, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    #@12e
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    #@131
    goto/16 :goto_28

    #@133
    .line 935
    .restart local v2       #lp:Landroid/view/ViewGroup$LayoutParams;
    .restart local v3       #popupHeight:I
    .restart local v4       #popupWidth:I
    :cond_133
    iget-object v6, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewText:Landroid/widget/TextView;

    #@135
    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredWidth()I

    #@138
    move-result v6

    #@139
    div-int/lit8 v6, v6, 0x2

    #@13b
    rsub-int v6, v6, 0xa0

    #@13d
    iput v6, p0, Landroid/inputmethodservice/KeyboardView;->mPopupPreviewX:I

    #@13f
    .line 936
    iget-object v6, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewText:Landroid/widget/TextView;

    #@141
    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    #@144
    move-result v6

    #@145
    neg-int v6, v6

    #@146
    iput v6, p0, Landroid/inputmethodservice/KeyboardView;->mPopupPreviewY:I

    #@148
    goto/16 :goto_77

    #@14a
    .line 944
    :cond_14a
    sget-object v6, Landroid/inputmethodservice/KeyboardView;->EMPTY_STATE_SET:[I

    #@14c
    goto/16 :goto_9f

    #@14e
    .line 957
    :cond_14e
    iget v6, p0, Landroid/inputmethodservice/KeyboardView;->mPopupPreviewX:I

    #@150
    iget v7, v0, Landroid/inputmethodservice/Keyboard$Key;->width:I

    #@152
    int-to-double v7, v7

    #@153
    mul-double/2addr v7, v11

    #@154
    double-to-int v7, v7

    #@155
    sub-int/2addr v6, v7

    #@156
    iput v6, p0, Landroid/inputmethodservice/KeyboardView;->mPopupPreviewX:I

    #@158
    goto :goto_d9

    #@159
    .line 966
    :cond_159
    invoke-virtual {v5, v4}, Landroid/widget/PopupWindow;->setWidth(I)V

    #@15c
    .line 967
    invoke-virtual {v5, v3}, Landroid/widget/PopupWindow;->setHeight(I)V

    #@15f
    .line 968
    iget-object v6, p0, Landroid/inputmethodservice/KeyboardView;->mPopupParent:Landroid/view/View;

    #@161
    iget v7, p0, Landroid/inputmethodservice/KeyboardView;->mPopupPreviewX:I

    #@163
    iget v8, p0, Landroid/inputmethodservice/KeyboardView;->mPopupPreviewY:I

    #@165
    invoke-virtual {v5, v6, v9, v7, v8}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    #@168
    goto :goto_eb
.end method

.method private showPreview(I)V
    .registers 15
    .parameter "keyIndex"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    const/4 v12, -0x1

    #@3
    .line 847
    iget v4, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKeyIndex:I

    #@5
    .line 848
    .local v4, oldKeyIndex:I
    iget-object v5, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewPopup:Landroid/widget/PopupWindow;

    #@7
    .line 850
    .local v5, previewPopup:Landroid/widget/PopupWindow;
    iput p1, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKeyIndex:I

    #@9
    .line 852
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@b
    .line 853
    .local v1, keys:[Landroid/inputmethodservice/Keyboard$Key;
    iget v6, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKeyIndex:I

    #@d
    if-eq v4, v6, :cond_53

    #@f
    .line 854
    if-eq v4, v12, :cond_2f

    #@11
    array-length v6, v1

    #@12
    if-le v6, v4, :cond_2f

    #@14
    .line 855
    aget-object v3, v1, v4

    #@16
    .line 856
    .local v3, oldKey:Landroid/inputmethodservice/Keyboard$Key;
    iget v6, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKeyIndex:I

    #@18
    if-ne v6, v12, :cond_8a

    #@1a
    move v6, v7

    #@1b
    :goto_1b
    invoke-virtual {v3, v6}, Landroid/inputmethodservice/Keyboard$Key;->onReleased(Z)V

    #@1e
    .line 857
    invoke-virtual {p0, v4}, Landroid/inputmethodservice/KeyboardView;->invalidateKey(I)V

    #@21
    .line 858
    iget-object v6, v3, Landroid/inputmethodservice/Keyboard$Key;->codes:[I

    #@23
    aget v0, v6, v8

    #@25
    .line 859
    .local v0, keyCode:I
    const/16 v6, 0x100

    #@27
    invoke-direct {p0, v6, v0}, Landroid/inputmethodservice/KeyboardView;->sendAccessibilityEventForUnicodeCharacter(II)V

    #@2a
    .line 862
    const/high16 v6, 0x1

    #@2c
    invoke-direct {p0, v6, v0}, Landroid/inputmethodservice/KeyboardView;->sendAccessibilityEventForUnicodeCharacter(II)V

    #@2f
    .line 865
    .end local v0           #keyCode:I
    .end local v3           #oldKey:Landroid/inputmethodservice/Keyboard$Key;
    :cond_2f
    iget v6, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKeyIndex:I

    #@31
    if-eq v6, v12, :cond_53

    #@33
    array-length v6, v1

    #@34
    iget v9, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKeyIndex:I

    #@36
    if-le v6, v9, :cond_53

    #@38
    .line 866
    iget v6, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKeyIndex:I

    #@3a
    aget-object v2, v1, v6

    #@3c
    .line 867
    .local v2, newKey:Landroid/inputmethodservice/Keyboard$Key;
    invoke-virtual {v2}, Landroid/inputmethodservice/Keyboard$Key;->onPressed()V

    #@3f
    .line 868
    iget v6, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKeyIndex:I

    #@41
    invoke-virtual {p0, v6}, Landroid/inputmethodservice/KeyboardView;->invalidateKey(I)V

    #@44
    .line 869
    iget-object v6, v2, Landroid/inputmethodservice/Keyboard$Key;->codes:[I

    #@46
    aget v0, v6, v8

    #@48
    .line 870
    .restart local v0       #keyCode:I
    const/16 v6, 0x80

    #@4a
    invoke-direct {p0, v6, v0}, Landroid/inputmethodservice/KeyboardView;->sendAccessibilityEventForUnicodeCharacter(II)V

    #@4d
    .line 873
    const v6, 0x8000

    #@50
    invoke-direct {p0, v6, v0}, Landroid/inputmethodservice/KeyboardView;->sendAccessibilityEventForUnicodeCharacter(II)V

    #@53
    .line 878
    .end local v0           #keyCode:I
    .end local v2           #newKey:Landroid/inputmethodservice/Keyboard$Key;
    :cond_53
    iget v6, p0, Landroid/inputmethodservice/KeyboardView;->mCurrentKeyIndex:I

    #@55
    if-eq v4, v6, :cond_89

    #@57
    iget-boolean v6, p0, Landroid/inputmethodservice/KeyboardView;->mShowPreview:Z

    #@59
    if-eqz v6, :cond_89

    #@5b
    .line 879
    iget-object v6, p0, Landroid/inputmethodservice/KeyboardView;->mHandler:Landroid/os/Handler;

    #@5d
    invoke-virtual {v6, v7}, Landroid/os/Handler;->removeMessages(I)V

    #@60
    .line 880
    invoke-virtual {v5}, Landroid/widget/PopupWindow;->isShowing()Z

    #@63
    move-result v6

    #@64
    if-eqz v6, :cond_76

    #@66
    .line 881
    if-ne p1, v12, :cond_76

    #@68
    .line 882
    iget-object v6, p0, Landroid/inputmethodservice/KeyboardView;->mHandler:Landroid/os/Handler;

    #@6a
    iget-object v9, p0, Landroid/inputmethodservice/KeyboardView;->mHandler:Landroid/os/Handler;

    #@6c
    const/4 v10, 0x2

    #@6d
    invoke-virtual {v9, v10}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@70
    move-result-object v9

    #@71
    const-wide/16 v10, 0x46

    #@73
    invoke-virtual {v6, v9, v10, v11}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@76
    .line 887
    :cond_76
    if-eq p1, v12, :cond_89

    #@78
    .line 888
    invoke-virtual {v5}, Landroid/widget/PopupWindow;->isShowing()Z

    #@7b
    move-result v6

    #@7c
    if-eqz v6, :cond_8c

    #@7e
    iget-object v6, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewText:Landroid/widget/TextView;

    #@80
    invoke-virtual {v6}, Landroid/widget/TextView;->getVisibility()I

    #@83
    move-result v6

    #@84
    if-nez v6, :cond_8c

    #@86
    .line 890
    invoke-direct {p0, p1}, Landroid/inputmethodservice/KeyboardView;->showKey(I)V

    #@89
    .line 898
    :cond_89
    :goto_89
    return-void

    #@8a
    .restart local v3       #oldKey:Landroid/inputmethodservice/Keyboard$Key;
    :cond_8a
    move v6, v8

    #@8b
    .line 856
    goto :goto_1b

    #@8c
    .line 892
    .end local v3           #oldKey:Landroid/inputmethodservice/Keyboard$Key;
    :cond_8c
    iget-object v6, p0, Landroid/inputmethodservice/KeyboardView;->mHandler:Landroid/os/Handler;

    #@8e
    iget-object v9, p0, Landroid/inputmethodservice/KeyboardView;->mHandler:Landroid/os/Handler;

    #@90
    invoke-virtual {v9, v7, p1, v8}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@93
    move-result-object v7

    #@94
    const-wide/16 v8, 0x0

    #@96
    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@99
    goto :goto_89
.end method


# virtual methods
.method public closing()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1387
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewPopup:Landroid/widget/PopupWindow;

    #@3
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 1388
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewPopup:Landroid/widget/PopupWindow;

    #@b
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    #@e
    .line 1390
    :cond_e
    invoke-direct {p0}, Landroid/inputmethodservice/KeyboardView;->removeMessages()V

    #@11
    .line 1392
    invoke-direct {p0}, Landroid/inputmethodservice/KeyboardView;->dismissPopupKeyboard()V

    #@14
    .line 1393
    iput-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mBuffer:Landroid/graphics/Bitmap;

    #@16
    .line 1394
    iput-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mCanvas:Landroid/graphics/Canvas;

    #@18
    .line 1395
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboardCache:Ljava/util/Map;

    #@1a
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    #@1d
    .line 1396
    return-void
.end method

.method public getKeyboard()Landroid/inputmethodservice/Keyboard;
    .registers 2

    #@0
    .prologue
    .line 484
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mKeyboard:Landroid/inputmethodservice/Keyboard;

    #@2
    return-object v0
.end method

.method protected getOnKeyboardActionListener()Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;
    .registers 2

    #@0
    .prologue
    .line 448
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mKeyboardActionListener:Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;

    #@2
    return-object v0
.end method

.method public handleBack()Z
    .registers 2

    #@0
    .prologue
    .line 1419
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mPopupKeyboard:Landroid/widget/PopupWindow;

    #@2
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_d

    #@8
    .line 1420
    invoke-direct {p0}, Landroid/inputmethodservice/KeyboardView;->dismissPopupKeyboard()V

    #@b
    .line 1421
    const/4 v0, 0x1

    #@c
    .line 1423
    :goto_c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method public invalidateAllKeys()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1033
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mDirtyRect:Landroid/graphics/Rect;

    #@3
    invoke-virtual {p0}, Landroid/inputmethodservice/KeyboardView;->getWidth()I

    #@6
    move-result v1

    #@7
    invoke-virtual {p0}, Landroid/inputmethodservice/KeyboardView;->getHeight()I

    #@a
    move-result v2

    #@b
    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->union(IIII)V

    #@e
    .line 1034
    const/4 v0, 0x1

    #@f
    iput-boolean v0, p0, Landroid/inputmethodservice/KeyboardView;->mDrawPending:Z

    #@11
    .line 1035
    invoke-virtual {p0}, Landroid/inputmethodservice/KeyboardView;->invalidate()V

    #@14
    .line 1036
    return-void
.end method

.method public invalidateKey(I)V
    .registers 9
    .parameter "keyIndex"

    #@0
    .prologue
    .line 1046
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 1057
    :cond_4
    :goto_4
    return-void

    #@5
    .line 1047
    :cond_5
    if-ltz p1, :cond_4

    #@7
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@9
    array-length v1, v1

    #@a
    if-ge p1, v1, :cond_4

    #@c
    .line 1050
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@e
    aget-object v0, v1, p1

    #@10
    .line 1051
    .local v0, key:Landroid/inputmethodservice/Keyboard$Key;
    iput-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mInvalidatedKey:Landroid/inputmethodservice/Keyboard$Key;

    #@12
    .line 1052
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mDirtyRect:Landroid/graphics/Rect;

    #@14
    iget v2, v0, Landroid/inputmethodservice/Keyboard$Key;->x:I

    #@16
    iget v3, p0, Landroid/view/View;->mPaddingLeft:I

    #@18
    add-int/2addr v2, v3

    #@19
    iget v3, v0, Landroid/inputmethodservice/Keyboard$Key;->y:I

    #@1b
    iget v4, p0, Landroid/view/View;->mPaddingTop:I

    #@1d
    add-int/2addr v3, v4

    #@1e
    iget v4, v0, Landroid/inputmethodservice/Keyboard$Key;->x:I

    #@20
    iget v5, v0, Landroid/inputmethodservice/Keyboard$Key;->width:I

    #@22
    add-int/2addr v4, v5

    #@23
    iget v5, p0, Landroid/view/View;->mPaddingLeft:I

    #@25
    add-int/2addr v4, v5

    #@26
    iget v5, v0, Landroid/inputmethodservice/Keyboard$Key;->y:I

    #@28
    iget v6, v0, Landroid/inputmethodservice/Keyboard$Key;->height:I

    #@2a
    add-int/2addr v5, v6

    #@2b
    iget v6, p0, Landroid/view/View;->mPaddingTop:I

    #@2d
    add-int/2addr v5, v6

    #@2e
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->union(IIII)V

    #@31
    .line 1054
    invoke-direct {p0}, Landroid/inputmethodservice/KeyboardView;->onBufferDraw()V

    #@34
    .line 1055
    iget v1, v0, Landroid/inputmethodservice/Keyboard$Key;->x:I

    #@36
    iget v2, p0, Landroid/view/View;->mPaddingLeft:I

    #@38
    add-int/2addr v1, v2

    #@39
    iget v2, v0, Landroid/inputmethodservice/Keyboard$Key;->y:I

    #@3b
    iget v3, p0, Landroid/view/View;->mPaddingTop:I

    #@3d
    add-int/2addr v2, v3

    #@3e
    iget v3, v0, Landroid/inputmethodservice/Keyboard$Key;->x:I

    #@40
    iget v4, v0, Landroid/inputmethodservice/Keyboard$Key;->width:I

    #@42
    add-int/2addr v3, v4

    #@43
    iget v4, p0, Landroid/view/View;->mPaddingLeft:I

    #@45
    add-int/2addr v3, v4

    #@46
    iget v4, v0, Landroid/inputmethodservice/Keyboard$Key;->y:I

    #@48
    iget v5, v0, Landroid/inputmethodservice/Keyboard$Key;->height:I

    #@4a
    add-int/2addr v4, v5

    #@4b
    iget v5, p0, Landroid/view/View;->mPaddingTop:I

    #@4d
    add-int/2addr v4, v5

    #@4e
    invoke-virtual {p0, v1, v2, v3, v4}, Landroid/inputmethodservice/KeyboardView;->invalidate(IIII)V

    #@51
    goto :goto_4
.end method

.method public isPreviewEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 533
    iget-boolean v0, p0, Landroid/inputmethodservice/KeyboardView;->mShowPreview:Z

    #@2
    return v0
.end method

.method public isProximityCorrectionEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 565
    iget-boolean v0, p0, Landroid/inputmethodservice/KeyboardView;->mProximityCorrectOn:Z

    #@2
    return v0
.end method

.method public isShifted()Z
    .registers 2

    #@0
    .prologue
    .line 511
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mKeyboard:Landroid/inputmethodservice/Keyboard;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 512
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mKeyboard:Landroid/inputmethodservice/Keyboard;

    #@6
    invoke-virtual {v0}, Landroid/inputmethodservice/Keyboard;->isShifted()Z

    #@9
    move-result v0

    #@a
    .line 514
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public onClick(Landroid/view/View;)V
    .registers 2
    .parameter "v"

    #@0
    .prologue
    .line 573
    invoke-direct {p0}, Landroid/inputmethodservice/KeyboardView;->dismissPopupKeyboard()V

    #@3
    .line 574
    return-void
.end method

.method public onDetachedFromWindow()V
    .registers 1

    #@0
    .prologue
    .line 1406
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    #@3
    .line 1407
    invoke-virtual {p0}, Landroid/inputmethodservice/KeyboardView;->closing()V

    #@6
    .line 1408
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .registers 5
    .parameter "canvas"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 631
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    #@4
    .line 632
    iget-boolean v0, p0, Landroid/inputmethodservice/KeyboardView;->mDrawPending:Z

    #@6
    if-nez v0, :cond_10

    #@8
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mBuffer:Landroid/graphics/Bitmap;

    #@a
    if-eqz v0, :cond_10

    #@c
    iget-boolean v0, p0, Landroid/inputmethodservice/KeyboardView;->mKeyboardChanged:Z

    #@e
    if-eqz v0, :cond_13

    #@10
    .line 633
    :cond_10
    invoke-direct {p0}, Landroid/inputmethodservice/KeyboardView;->onBufferDraw()V

    #@13
    .line 635
    :cond_13
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mBuffer:Landroid/graphics/Bitmap;

    #@15
    const/4 v1, 0x0

    #@16
    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    #@19
    .line 636
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .registers 5
    .parameter "event"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1162
    iget-object v2, p0, Landroid/inputmethodservice/KeyboardView;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    #@3
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    #@6
    move-result v2

    #@7
    if-eqz v2, :cond_1a

    #@9
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@c
    move-result v2

    #@d
    if-ne v2, v1, :cond_1a

    #@f
    .line 1163
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@12
    move-result v0

    #@13
    .line 1164
    .local v0, action:I
    packed-switch v0, :pswitch_data_2a

    #@16
    .line 1175
    :goto_16
    :pswitch_16
    invoke-virtual {p0, p1}, Landroid/inputmethodservice/KeyboardView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@19
    move-result v1

    #@1a
    .line 1177
    .end local v0           #action:I
    :cond_1a
    return v1

    #@1b
    .line 1166
    .restart local v0       #action:I
    :pswitch_1b
    const/4 v1, 0x0

    #@1c
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    #@1f
    goto :goto_16

    #@20
    .line 1169
    :pswitch_20
    const/4 v1, 0x2

    #@21
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    #@24
    goto :goto_16

    #@25
    .line 1172
    :pswitch_25
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    #@28
    goto :goto_16

    #@29
    .line 1164
    nop

    #@2a
    :pswitch_data_2a
    .packed-switch 0x7
        :pswitch_20
        :pswitch_16
        :pswitch_1b
        :pswitch_25
    .end packed-switch
.end method

.method protected onLongPress(Landroid/inputmethodservice/Keyboard$Key;)Z
    .registers 16
    .parameter "popupKey"

    #@0
    .prologue
    const v4, 0x1020026

    #@3
    const/high16 v13, -0x8000

    #@5
    const/4 v11, 0x1

    #@6
    const/4 v10, 0x0

    #@7
    .line 1085
    iget v2, p1, Landroid/inputmethodservice/Keyboard$Key;->popupResId:I

    #@9
    .line 1087
    .local v2, popupKeyboardId:I
    if-eqz v2, :cond_113

    #@b
    .line 1088
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboardCache:Ljava/util/Map;

    #@d
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@10
    move-result-object v1

    #@11
    check-cast v1, Landroid/view/View;

    #@13
    iput-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboardContainer:Landroid/view/View;

    #@15
    .line 1089
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboardContainer:Landroid/view/View;

    #@17
    if-nez v1, :cond_11f

    #@19
    .line 1090
    invoke-virtual {p0}, Landroid/inputmethodservice/KeyboardView;->getContext()Landroid/content/Context;

    #@1c
    move-result-object v1

    #@1d
    const-string/jumbo v3, "layout_inflater"

    #@20
    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@23
    move-result-object v7

    #@24
    check-cast v7, Landroid/view/LayoutInflater;

    #@26
    .line 1092
    .local v7, inflater:Landroid/view/LayoutInflater;
    iget v1, p0, Landroid/inputmethodservice/KeyboardView;->mPopupLayout:I

    #@28
    const/4 v3, 0x0

    #@29
    invoke-virtual {v7, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@2c
    move-result-object v1

    #@2d
    iput-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboardContainer:Landroid/view/View;

    #@2f
    .line 1093
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboardContainer:Landroid/view/View;

    #@31
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@34
    move-result-object v1

    #@35
    check-cast v1, Landroid/inputmethodservice/KeyboardView;

    #@37
    iput-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboard:Landroid/inputmethodservice/KeyboardView;

    #@39
    .line 1095
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboardContainer:Landroid/view/View;

    #@3b
    const v3, 0x1020027

    #@3e
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@41
    move-result-object v6

    #@42
    .line 1097
    .local v6, closeButton:Landroid/view/View;
    if-eqz v6, :cond_47

    #@44
    invoke-virtual {v6, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@47
    .line 1098
    :cond_47
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboard:Landroid/inputmethodservice/KeyboardView;

    #@49
    new-instance v3, Landroid/inputmethodservice/KeyboardView$3;

    #@4b
    invoke-direct {v3, p0}, Landroid/inputmethodservice/KeyboardView$3;-><init>(Landroid/inputmethodservice/KeyboardView;)V

    #@4e
    invoke-virtual {v1, v3}, Landroid/inputmethodservice/KeyboardView;->setOnKeyboardActionListener(Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;)V

    #@51
    .line 1122
    iget-object v1, p1, Landroid/inputmethodservice/Keyboard$Key;->popupCharacters:Ljava/lang/CharSequence;

    #@53
    if-eqz v1, :cond_114

    #@55
    .line 1123
    new-instance v0, Landroid/inputmethodservice/Keyboard;

    #@57
    invoke-virtual {p0}, Landroid/inputmethodservice/KeyboardView;->getContext()Landroid/content/Context;

    #@5a
    move-result-object v1

    #@5b
    iget-object v3, p1, Landroid/inputmethodservice/Keyboard$Key;->popupCharacters:Ljava/lang/CharSequence;

    #@5d
    const/4 v4, -0x1

    #@5e
    invoke-virtual {p0}, Landroid/inputmethodservice/KeyboardView;->getPaddingLeft()I

    #@61
    move-result v5

    #@62
    invoke-virtual {p0}, Landroid/inputmethodservice/KeyboardView;->getPaddingRight()I

    #@65
    move-result v12

    #@66
    add-int/2addr v5, v12

    #@67
    invoke-direct/range {v0 .. v5}, Landroid/inputmethodservice/Keyboard;-><init>(Landroid/content/Context;ILjava/lang/CharSequence;II)V

    #@6a
    .line 1128
    .local v0, keyboard:Landroid/inputmethodservice/Keyboard;
    :goto_6a
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboard:Landroid/inputmethodservice/KeyboardView;

    #@6c
    invoke-virtual {v1, v0}, Landroid/inputmethodservice/KeyboardView;->setKeyboard(Landroid/inputmethodservice/Keyboard;)V

    #@6f
    .line 1129
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboard:Landroid/inputmethodservice/KeyboardView;

    #@71
    invoke-virtual {v1, p0}, Landroid/inputmethodservice/KeyboardView;->setPopupParent(Landroid/view/View;)V

    #@74
    .line 1130
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboardContainer:Landroid/view/View;

    #@76
    invoke-virtual {p0}, Landroid/inputmethodservice/KeyboardView;->getWidth()I

    #@79
    move-result v3

    #@7a
    invoke-static {v3, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@7d
    move-result v3

    #@7e
    invoke-virtual {p0}, Landroid/inputmethodservice/KeyboardView;->getHeight()I

    #@81
    move-result v4

    #@82
    invoke-static {v4, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@85
    move-result v4

    #@86
    invoke-virtual {v1, v3, v4}, Landroid/view/View;->measure(II)V

    #@89
    .line 1134
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboardCache:Ljava/util/Map;

    #@8b
    iget-object v3, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboardContainer:Landroid/view/View;

    #@8d
    invoke-interface {v1, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@90
    .line 1139
    .end local v0           #keyboard:Landroid/inputmethodservice/Keyboard;
    .end local v6           #closeButton:Landroid/view/View;
    .end local v7           #inflater:Landroid/view/LayoutInflater;
    :goto_90
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mCoordinates:[I

    #@92
    invoke-virtual {p0, v1}, Landroid/inputmethodservice/KeyboardView;->getLocationInWindow([I)V

    #@95
    .line 1140
    iget v1, p1, Landroid/inputmethodservice/Keyboard$Key;->x:I

    #@97
    iget v3, p0, Landroid/view/View;->mPaddingLeft:I

    #@99
    add-int/2addr v1, v3

    #@9a
    iput v1, p0, Landroid/inputmethodservice/KeyboardView;->mPopupX:I

    #@9c
    .line 1141
    iget v1, p1, Landroid/inputmethodservice/Keyboard$Key;->y:I

    #@9e
    iget v3, p0, Landroid/view/View;->mPaddingTop:I

    #@a0
    add-int/2addr v1, v3

    #@a1
    iput v1, p0, Landroid/inputmethodservice/KeyboardView;->mPopupY:I

    #@a3
    .line 1142
    iget v1, p0, Landroid/inputmethodservice/KeyboardView;->mPopupX:I

    #@a5
    iget v3, p1, Landroid/inputmethodservice/Keyboard$Key;->width:I

    #@a7
    add-int/2addr v1, v3

    #@a8
    iget-object v3, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboardContainer:Landroid/view/View;

    #@aa
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    #@ad
    move-result v3

    #@ae
    sub-int/2addr v1, v3

    #@af
    iput v1, p0, Landroid/inputmethodservice/KeyboardView;->mPopupX:I

    #@b1
    .line 1143
    iget v1, p0, Landroid/inputmethodservice/KeyboardView;->mPopupY:I

    #@b3
    iget-object v3, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboardContainer:Landroid/view/View;

    #@b5
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    #@b8
    move-result v3

    #@b9
    sub-int/2addr v1, v3

    #@ba
    iput v1, p0, Landroid/inputmethodservice/KeyboardView;->mPopupY:I

    #@bc
    .line 1144
    iget v1, p0, Landroid/inputmethodservice/KeyboardView;->mPopupX:I

    #@be
    iget-object v3, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboardContainer:Landroid/view/View;

    #@c0
    invoke-virtual {v3}, Landroid/view/View;->getPaddingRight()I

    #@c3
    move-result v3

    #@c4
    add-int/2addr v1, v3

    #@c5
    iget-object v3, p0, Landroid/inputmethodservice/KeyboardView;->mCoordinates:[I

    #@c7
    aget v3, v3, v10

    #@c9
    add-int v8, v1, v3

    #@cb
    .line 1145
    .local v8, x:I
    iget v1, p0, Landroid/inputmethodservice/KeyboardView;->mPopupY:I

    #@cd
    iget-object v3, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboardContainer:Landroid/view/View;

    #@cf
    invoke-virtual {v3}, Landroid/view/View;->getPaddingBottom()I

    #@d2
    move-result v3

    #@d3
    add-int/2addr v1, v3

    #@d4
    iget-object v3, p0, Landroid/inputmethodservice/KeyboardView;->mCoordinates:[I

    #@d6
    aget v3, v3, v11

    #@d8
    add-int v9, v1, v3

    #@da
    .line 1146
    .local v9, y:I
    iget-object v3, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboard:Landroid/inputmethodservice/KeyboardView;

    #@dc
    if-gez v8, :cond_12b

    #@de
    move v1, v10

    #@df
    :goto_df
    invoke-virtual {v3, v1, v9}, Landroid/inputmethodservice/KeyboardView;->setPopupOffset(II)V

    #@e2
    .line 1147
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboard:Landroid/inputmethodservice/KeyboardView;

    #@e4
    invoke-virtual {p0}, Landroid/inputmethodservice/KeyboardView;->isShifted()Z

    #@e7
    move-result v3

    #@e8
    invoke-virtual {v1, v3}, Landroid/inputmethodservice/KeyboardView;->setShifted(Z)Z

    #@eb
    .line 1148
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mPopupKeyboard:Landroid/widget/PopupWindow;

    #@ed
    iget-object v3, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboardContainer:Landroid/view/View;

    #@ef
    invoke-virtual {v1, v3}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    #@f2
    .line 1149
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mPopupKeyboard:Landroid/widget/PopupWindow;

    #@f4
    iget-object v3, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboardContainer:Landroid/view/View;

    #@f6
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    #@f9
    move-result v3

    #@fa
    invoke-virtual {v1, v3}, Landroid/widget/PopupWindow;->setWidth(I)V

    #@fd
    .line 1150
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mPopupKeyboard:Landroid/widget/PopupWindow;

    #@ff
    iget-object v3, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboardContainer:Landroid/view/View;

    #@101
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    #@104
    move-result v3

    #@105
    invoke-virtual {v1, v3}, Landroid/widget/PopupWindow;->setHeight(I)V

    #@108
    .line 1151
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mPopupKeyboard:Landroid/widget/PopupWindow;

    #@10a
    invoke-virtual {v1, p0, v10, v8, v9}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    #@10d
    .line 1152
    iput-boolean v11, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboardOnScreen:Z

    #@10f
    .line 1154
    invoke-virtual {p0}, Landroid/inputmethodservice/KeyboardView;->invalidateAllKeys()V

    #@112
    move v10, v11

    #@113
    .line 1157
    .end local v8           #x:I
    .end local v9           #y:I
    :cond_113
    return v10

    #@114
    .line 1126
    .restart local v6       #closeButton:Landroid/view/View;
    .restart local v7       #inflater:Landroid/view/LayoutInflater;
    :cond_114
    new-instance v0, Landroid/inputmethodservice/Keyboard;

    #@116
    invoke-virtual {p0}, Landroid/inputmethodservice/KeyboardView;->getContext()Landroid/content/Context;

    #@119
    move-result-object v1

    #@11a
    invoke-direct {v0, v1, v2}, Landroid/inputmethodservice/Keyboard;-><init>(Landroid/content/Context;I)V

    #@11d
    .restart local v0       #keyboard:Landroid/inputmethodservice/Keyboard;
    goto/16 :goto_6a

    #@11f
    .line 1136
    .end local v0           #keyboard:Landroid/inputmethodservice/Keyboard;
    .end local v6           #closeButton:Landroid/view/View;
    .end local v7           #inflater:Landroid/view/LayoutInflater;
    :cond_11f
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboardContainer:Landroid/view/View;

    #@121
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@124
    move-result-object v1

    #@125
    check-cast v1, Landroid/inputmethodservice/KeyboardView;

    #@127
    iput-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboard:Landroid/inputmethodservice/KeyboardView;

    #@129
    goto/16 :goto_90

    #@12b
    .restart local v8       #x:I
    .restart local v9       #y:I
    :cond_12b
    move v1, v8

    #@12c
    .line 1146
    goto :goto_df
.end method

.method public onMeasure(II)V
    .registers 7
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 587
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mKeyboard:Landroid/inputmethodservice/Keyboard;

    #@2
    if-nez v1, :cond_12

    #@4
    .line 588
    iget v1, p0, Landroid/view/View;->mPaddingLeft:I

    #@6
    iget v2, p0, Landroid/view/View;->mPaddingRight:I

    #@8
    add-int/2addr v1, v2

    #@9
    iget v2, p0, Landroid/view/View;->mPaddingTop:I

    #@b
    iget v3, p0, Landroid/view/View;->mPaddingBottom:I

    #@d
    add-int/2addr v2, v3

    #@e
    invoke-virtual {p0, v1, v2}, Landroid/inputmethodservice/KeyboardView;->setMeasuredDimension(II)V

    #@11
    .line 596
    :goto_11
    return-void

    #@12
    .line 590
    :cond_12
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mKeyboard:Landroid/inputmethodservice/Keyboard;

    #@14
    invoke-virtual {v1}, Landroid/inputmethodservice/Keyboard;->getMinWidth()I

    #@17
    move-result v1

    #@18
    iget v2, p0, Landroid/view/View;->mPaddingLeft:I

    #@1a
    add-int/2addr v1, v2

    #@1b
    iget v2, p0, Landroid/view/View;->mPaddingRight:I

    #@1d
    add-int v0, v1, v2

    #@1f
    .line 591
    .local v0, width:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@22
    move-result v1

    #@23
    add-int/lit8 v2, v0, 0xa

    #@25
    if-ge v1, v2, :cond_2b

    #@27
    .line 592
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@2a
    move-result v0

    #@2b
    .line 594
    :cond_2b
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mKeyboard:Landroid/inputmethodservice/Keyboard;

    #@2d
    invoke-virtual {v1}, Landroid/inputmethodservice/Keyboard;->getHeight()I

    #@30
    move-result v1

    #@31
    iget v2, p0, Landroid/view/View;->mPaddingTop:I

    #@33
    add-int/2addr v1, v2

    #@34
    iget v2, p0, Landroid/view/View;->mPaddingBottom:I

    #@36
    add-int/2addr v1, v2

    #@37
    invoke-virtual {p0, v0, v1}, Landroid/inputmethodservice/KeyboardView;->setMeasuredDimension(II)V

    #@3a
    goto :goto_11
.end method

.method public onSizeChanged(IIII)V
    .registers 6
    .parameter "w"
    .parameter "h"
    .parameter "oldw"
    .parameter "oldh"

    #@0
    .prologue
    .line 621
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    #@3
    .line 622
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mKeyboard:Landroid/inputmethodservice/Keyboard;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 623
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mKeyboard:Landroid/inputmethodservice/Keyboard;

    #@9
    invoke-virtual {v0, p1, p2}, Landroid/inputmethodservice/Keyboard;->resize(II)V

    #@c
    .line 626
    :cond_c
    const/4 v0, 0x0

    #@d
    iput-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mBuffer:Landroid/graphics/Bitmap;

    #@f
    .line 627
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 16
    .parameter "me"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v13, 0x1

    #@2
    .line 1184
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@5
    move-result v10

    #@6
    .line 1185
    .local v10, pointerCount:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@9
    move-result v8

    #@a
    .line 1186
    .local v8, action:I
    const/4 v11, 0x0

    #@b
    .line 1187
    .local v11, result:Z
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    #@e
    move-result-wide v0

    #@f
    .line 1189
    .local v0, now:J
    iget v2, p0, Landroid/inputmethodservice/KeyboardView;->mOldPointerCount:I

    #@11
    if-eq v10, v2, :cond_4c

    #@13
    .line 1190
    if-ne v10, v13, :cond_36

    #@15
    .line 1192
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@18
    move-result v5

    #@19
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@1c
    move-result v6

    #@1d
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    #@20
    move-result v7

    #@21
    move-wide v2, v0

    #@22
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    #@25
    move-result-object v9

    #@26
    .line 1194
    .local v9, down:Landroid/view/MotionEvent;
    invoke-direct {p0, v9, v4}, Landroid/inputmethodservice/KeyboardView;->onModifiedTouchEvent(Landroid/view/MotionEvent;Z)Z

    #@29
    move-result v11

    #@2a
    .line 1195
    invoke-virtual {v9}, Landroid/view/MotionEvent;->recycle()V

    #@2d
    .line 1197
    if-ne v8, v13, :cond_33

    #@2f
    .line 1198
    invoke-direct {p0, p1, v13}, Landroid/inputmethodservice/KeyboardView;->onModifiedTouchEvent(Landroid/view/MotionEvent;Z)Z

    #@32
    move-result v11

    #@33
    .line 1217
    .end local v9           #down:Landroid/view/MotionEvent;
    :cond_33
    :goto_33
    iput v10, p0, Landroid/inputmethodservice/KeyboardView;->mOldPointerCount:I

    #@35
    .line 1219
    return v11

    #@36
    .line 1202
    :cond_36
    iget v5, p0, Landroid/inputmethodservice/KeyboardView;->mOldPointerX:F

    #@38
    iget v6, p0, Landroid/inputmethodservice/KeyboardView;->mOldPointerY:F

    #@3a
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    #@3d
    move-result v7

    #@3e
    move-wide v2, v0

    #@3f
    move v4, v13

    #@40
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    #@43
    move-result-object v12

    #@44
    .line 1204
    .local v12, up:Landroid/view/MotionEvent;
    invoke-direct {p0, v12, v13}, Landroid/inputmethodservice/KeyboardView;->onModifiedTouchEvent(Landroid/view/MotionEvent;Z)Z

    #@47
    move-result v11

    #@48
    .line 1205
    invoke-virtual {v12}, Landroid/view/MotionEvent;->recycle()V

    #@4b
    goto :goto_33

    #@4c
    .line 1208
    .end local v12           #up:Landroid/view/MotionEvent;
    :cond_4c
    if-ne v10, v13, :cond_5f

    #@4e
    .line 1209
    invoke-direct {p0, p1, v4}, Landroid/inputmethodservice/KeyboardView;->onModifiedTouchEvent(Landroid/view/MotionEvent;Z)Z

    #@51
    move-result v11

    #@52
    .line 1210
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@55
    move-result v2

    #@56
    iput v2, p0, Landroid/inputmethodservice/KeyboardView;->mOldPointerX:F

    #@58
    .line 1211
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@5b
    move-result v2

    #@5c
    iput v2, p0, Landroid/inputmethodservice/KeyboardView;->mOldPointerY:F

    #@5e
    goto :goto_33

    #@5f
    .line 1214
    :cond_5f
    const/4 v11, 0x1

    #@60
    goto :goto_33
.end method

.method public setKeyboard(Landroid/inputmethodservice/Keyboard;)V
    .registers 5
    .parameter "keyboard"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 459
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mKeyboard:Landroid/inputmethodservice/Keyboard;

    #@3
    if-eqz v1, :cond_9

    #@5
    .line 460
    const/4 v1, -0x1

    #@6
    invoke-direct {p0, v1}, Landroid/inputmethodservice/KeyboardView;->showPreview(I)V

    #@9
    .line 463
    :cond_9
    invoke-direct {p0}, Landroid/inputmethodservice/KeyboardView;->removeMessages()V

    #@c
    .line 464
    iput-object p1, p0, Landroid/inputmethodservice/KeyboardView;->mKeyboard:Landroid/inputmethodservice/Keyboard;

    #@e
    .line 465
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mKeyboard:Landroid/inputmethodservice/Keyboard;

    #@10
    invoke-virtual {v1}, Landroid/inputmethodservice/Keyboard;->getKeys()Ljava/util/List;

    #@13
    move-result-object v0

    #@14
    .line 466
    .local v0, keys:Ljava/util/List;,"Ljava/util/List<Landroid/inputmethodservice/Keyboard$Key;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@17
    move-result v1

    #@18
    new-array v1, v1, [Landroid/inputmethodservice/Keyboard$Key;

    #@1a
    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@1d
    move-result-object v1

    #@1e
    check-cast v1, [Landroid/inputmethodservice/Keyboard$Key;

    #@20
    iput-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@22
    .line 467
    invoke-virtual {p0}, Landroid/inputmethodservice/KeyboardView;->requestLayout()V

    #@25
    .line 469
    iput-boolean v2, p0, Landroid/inputmethodservice/KeyboardView;->mKeyboardChanged:Z

    #@27
    .line 470
    invoke-virtual {p0}, Landroid/inputmethodservice/KeyboardView;->invalidateAllKeys()V

    #@2a
    .line 471
    invoke-direct {p0, p1}, Landroid/inputmethodservice/KeyboardView;->computeProximityThreshold(Landroid/inputmethodservice/Keyboard;)V

    #@2d
    .line 472
    iget-object v1, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboardCache:Ljava/util/Map;

    #@2f
    invoke-interface {v1}, Ljava/util/Map;->clear()V

    #@32
    .line 475
    iput-boolean v2, p0, Landroid/inputmethodservice/KeyboardView;->mAbortKey:Z

    #@34
    .line 476
    return-void
.end method

.method public setOnKeyboardActionListener(Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 440
    iput-object p1, p0, Landroid/inputmethodservice/KeyboardView;->mKeyboardActionListener:Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;

    #@2
    .line 441
    return-void
.end method

.method public setPopupOffset(II)V
    .registers 4
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 544
    iput p1, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboardOffsetX:I

    #@2
    .line 545
    iput p2, p0, Landroid/inputmethodservice/KeyboardView;->mMiniKeyboardOffsetY:I

    #@4
    .line 546
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewPopup:Landroid/widget/PopupWindow;

    #@6
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_11

    #@c
    .line 547
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mPreviewPopup:Landroid/widget/PopupWindow;

    #@e
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    #@11
    .line 549
    :cond_11
    return-void
.end method

.method public setPopupParent(Landroid/view/View;)V
    .registers 2
    .parameter "v"

    #@0
    .prologue
    .line 540
    iput-object p1, p0, Landroid/inputmethodservice/KeyboardView;->mPopupParent:Landroid/view/View;

    #@2
    .line 541
    return-void
.end method

.method public setPreviewEnabled(Z)V
    .registers 2
    .parameter "previewEnabled"

    #@0
    .prologue
    .line 524
    iput-boolean p1, p0, Landroid/inputmethodservice/KeyboardView;->mShowPreview:Z

    #@2
    .line 525
    return-void
.end method

.method public setProximityCorrectionEnabled(Z)V
    .registers 2
    .parameter "enabled"

    #@0
    .prologue
    .line 558
    iput-boolean p1, p0, Landroid/inputmethodservice/KeyboardView;->mProximityCorrectOn:Z

    #@2
    .line 559
    return-void
.end method

.method public setShifted(Z)Z
    .registers 3
    .parameter "shifted"

    #@0
    .prologue
    .line 494
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mKeyboard:Landroid/inputmethodservice/Keyboard;

    #@2
    if-eqz v0, :cond_11

    #@4
    .line 495
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mKeyboard:Landroid/inputmethodservice/Keyboard;

    #@6
    invoke-virtual {v0, p1}, Landroid/inputmethodservice/Keyboard;->setShifted(Z)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_11

    #@c
    .line 497
    invoke-virtual {p0}, Landroid/inputmethodservice/KeyboardView;->invalidateAllKeys()V

    #@f
    .line 498
    const/4 v0, 0x1

    #@10
    .line 501
    :goto_10
    return v0

    #@11
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_10
.end method

.method public setVerticalCorrection(I)V
    .registers 2
    .parameter "verticalOffset"

    #@0
    .prologue
    .line 538
    return-void
.end method

.method protected swipeDown()V
    .registers 2

    #@0
    .prologue
    .line 1383
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mKeyboardActionListener:Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;

    #@2
    invoke-interface {v0}, Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;->swipeDown()V

    #@5
    .line 1384
    return-void
.end method

.method protected swipeLeft()V
    .registers 2

    #@0
    .prologue
    .line 1375
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mKeyboardActionListener:Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;

    #@2
    invoke-interface {v0}, Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;->swipeLeft()V

    #@5
    .line 1376
    return-void
.end method

.method protected swipeRight()V
    .registers 2

    #@0
    .prologue
    .line 1371
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mKeyboardActionListener:Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;

    #@2
    invoke-interface {v0}, Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;->swipeRight()V

    #@5
    .line 1372
    return-void
.end method

.method protected swipeUp()V
    .registers 2

    #@0
    .prologue
    .line 1379
    iget-object v0, p0, Landroid/inputmethodservice/KeyboardView;->mKeyboardActionListener:Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;

    #@2
    invoke-interface {v0}, Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;->swipeUp()V

    #@5
    .line 1380
    return-void
.end method
