.class Landroid/inputmethodservice/SoftInputWindow;
.super Landroid/app/Dialog;
.source "SoftInputWindow.java"


# instance fields
.field private final mBounds:Landroid/graphics/Rect;

.field final mDispatcherState:Landroid/view/KeyEvent$DispatcherState;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/view/KeyEvent$DispatcherState;)V
    .registers 5
    .parameter "context"
    .parameter "theme"
    .parameter "dispatcherState"

    #@0
    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    #@3
    .line 36
    new-instance v0, Landroid/graphics/Rect;

    #@5
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@8
    iput-object v0, p0, Landroid/inputmethodservice/SoftInputWindow;->mBounds:Landroid/graphics/Rect;

    #@a
    .line 59
    iput-object p3, p0, Landroid/inputmethodservice/SoftInputWindow;->mDispatcherState:Landroid/view/KeyEvent$DispatcherState;

    #@c
    .line 60
    invoke-direct {p0}, Landroid/inputmethodservice/SoftInputWindow;->initDockWindow()V

    #@f
    .line 61
    return-void
.end method

.method private initDockWindow()V
    .registers 5

    #@0
    .prologue
    .line 152
    invoke-virtual {p0}, Landroid/inputmethodservice/SoftInputWindow;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@7
    move-result-object v0

    #@8
    .line 154
    .local v0, lp:Landroid/view/WindowManager$LayoutParams;
    const/16 v1, 0x7db

    #@a
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@c
    .line 155
    const-string v1, "InputMethod"

    #@e
    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    #@11
    .line 157
    const/16 v1, 0x50

    #@13
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@15
    .line 158
    const/4 v1, -0x1

    #@16
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@18
    .line 163
    invoke-virtual {p0}, Landroid/inputmethodservice/SoftInputWindow;->getWindow()Landroid/view/Window;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    #@1f
    .line 164
    invoke-virtual {p0}, Landroid/inputmethodservice/SoftInputWindow;->getWindow()Landroid/view/Window;

    #@22
    move-result-object v1

    #@23
    const/16 v2, 0x108

    #@25
    const/16 v3, 0x10a

    #@27
    invoke-virtual {v1, v2, v3}, Landroid/view/Window;->setFlags(II)V

    #@2a
    .line 170
    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 8
    .parameter "ev"

    #@0
    .prologue
    .line 71
    invoke-virtual {p0}, Landroid/inputmethodservice/SoftInputWindow;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v2

    #@4
    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@7
    move-result-object v2

    #@8
    iget-object v3, p0, Landroid/inputmethodservice/SoftInputWindow;->mBounds:Landroid/graphics/Rect;

    #@a
    invoke-virtual {v2, v3}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    #@d
    .line 73
    iget-object v2, p0, Landroid/inputmethodservice/SoftInputWindow;->mBounds:Landroid/graphics/Rect;

    #@f
    iget v2, v2, Landroid/graphics/Rect;->left:I

    #@11
    int-to-float v2, v2

    #@12
    iget-object v3, p0, Landroid/inputmethodservice/SoftInputWindow;->mBounds:Landroid/graphics/Rect;

    #@14
    iget v3, v3, Landroid/graphics/Rect;->top:I

    #@16
    int-to-float v3, v3

    #@17
    iget-object v4, p0, Landroid/inputmethodservice/SoftInputWindow;->mBounds:Landroid/graphics/Rect;

    #@19
    iget v4, v4, Landroid/graphics/Rect;->right:I

    #@1b
    add-int/lit8 v4, v4, -0x1

    #@1d
    int-to-float v4, v4

    #@1e
    iget-object v5, p0, Landroid/inputmethodservice/SoftInputWindow;->mBounds:Landroid/graphics/Rect;

    #@20
    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    #@22
    add-int/lit8 v5, v5, -0x1

    #@24
    int-to-float v5, v5

    #@25
    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/view/MotionEvent;->isWithinBoundsNoHistory(FFFF)Z

    #@28
    move-result v2

    #@29
    if-eqz v2, :cond_30

    #@2b
    .line 75
    invoke-super {p0, p1}, Landroid/app/Dialog;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    #@2e
    move-result v0

    #@2f
    .line 81
    :goto_2f
    return v0

    #@30
    .line 77
    :cond_30
    iget-object v2, p0, Landroid/inputmethodservice/SoftInputWindow;->mBounds:Landroid/graphics/Rect;

    #@32
    iget v2, v2, Landroid/graphics/Rect;->left:I

    #@34
    int-to-float v2, v2

    #@35
    iget-object v3, p0, Landroid/inputmethodservice/SoftInputWindow;->mBounds:Landroid/graphics/Rect;

    #@37
    iget v3, v3, Landroid/graphics/Rect;->top:I

    #@39
    int-to-float v3, v3

    #@3a
    iget-object v4, p0, Landroid/inputmethodservice/SoftInputWindow;->mBounds:Landroid/graphics/Rect;

    #@3c
    iget v4, v4, Landroid/graphics/Rect;->right:I

    #@3e
    add-int/lit8 v4, v4, -0x1

    #@40
    int-to-float v4, v4

    #@41
    iget-object v5, p0, Landroid/inputmethodservice/SoftInputWindow;->mBounds:Landroid/graphics/Rect;

    #@43
    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    #@45
    add-int/lit8 v5, v5, -0x1

    #@47
    int-to-float v5, v5

    #@48
    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/view/MotionEvent;->clampNoHistory(FFFF)Landroid/view/MotionEvent;

    #@4b
    move-result-object v1

    #@4c
    .line 79
    .local v1, temp:Landroid/view/MotionEvent;
    invoke-super {p0, v1}, Landroid/app/Dialog;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    #@4f
    move-result v0

    #@50
    .line 80
    .local v0, handled:Z
    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    #@53
    goto :goto_2f
.end method

.method public getSize()I
    .registers 4

    #@0
    .prologue
    .line 95
    invoke-virtual {p0}, Landroid/inputmethodservice/SoftInputWindow;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@7
    move-result-object v0

    #@8
    .line 97
    .local v0, lp:Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@a
    const/16 v2, 0x30

    #@c
    if-eq v1, v2, :cond_14

    #@e
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@10
    const/16 v2, 0x50

    #@12
    if-ne v1, v2, :cond_17

    #@14
    .line 98
    :cond_14
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@16
    .line 100
    :goto_16
    return v1

    #@17
    :cond_17
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@19
    goto :goto_16
.end method

.method public onWindowFocusChanged(Z)V
    .registers 3
    .parameter "hasFocus"

    #@0
    .prologue
    .line 65
    invoke-super {p0, p1}, Landroid/app/Dialog;->onWindowFocusChanged(Z)V

    #@3
    .line 66
    iget-object v0, p0, Landroid/inputmethodservice/SoftInputWindow;->mDispatcherState:Landroid/view/KeyEvent$DispatcherState;

    #@5
    invoke-virtual {v0}, Landroid/view/KeyEvent$DispatcherState;->reset()V

    #@8
    .line 67
    return-void
.end method

.method public setGravity(I)V
    .registers 11
    .parameter "gravity"

    #@0
    .prologue
    const/16 v8, 0x50

    #@2
    const/16 v7, 0x30

    #@4
    const/4 v5, 0x1

    #@5
    const/4 v4, 0x0

    #@6
    .line 135
    invoke-virtual {p0}, Landroid/inputmethodservice/SoftInputWindow;->getWindow()Landroid/view/Window;

    #@9
    move-result-object v6

    #@a
    invoke-virtual {v6}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@d
    move-result-object v0

    #@e
    .line 137
    .local v0, lp:Landroid/view/WindowManager$LayoutParams;
    iget v6, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@10
    if-eq v6, v7, :cond_16

    #@12
    iget v6, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@14
    if-ne v6, v8, :cond_34

    #@16
    :cond_16
    move v2, v5

    #@17
    .line 139
    .local v2, oldIsVertical:Z
    :goto_17
    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@19
    .line 141
    iget v6, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@1b
    if-eq v6, v7, :cond_21

    #@1d
    iget v6, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@1f
    if-ne v6, v8, :cond_36

    #@21
    :cond_21
    move v1, v5

    #@22
    .line 143
    .local v1, newIsVertical:Z
    :goto_22
    if-eq v2, v1, :cond_33

    #@24
    .line 144
    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@26
    .line 145
    .local v3, tmp:I
    iget v4, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@28
    iput v4, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@2a
    .line 146
    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@2c
    .line 147
    invoke-virtual {p0}, Landroid/inputmethodservice/SoftInputWindow;->getWindow()Landroid/view/Window;

    #@2f
    move-result-object v4

    #@30
    invoke-virtual {v4, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    #@33
    .line 149
    .end local v3           #tmp:I
    :cond_33
    return-void

    #@34
    .end local v1           #newIsVertical:Z
    .end local v2           #oldIsVertical:Z
    :cond_34
    move v2, v4

    #@35
    .line 137
    goto :goto_17

    #@36
    .restart local v2       #oldIsVertical:Z
    :cond_36
    move v1, v4

    #@37
    .line 141
    goto :goto_22
.end method

.method public setSize(I)V
    .registers 6
    .parameter "size"

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    .line 114
    invoke-virtual {p0}, Landroid/inputmethodservice/SoftInputWindow;->getWindow()Landroid/view/Window;

    #@4
    move-result-object v1

    #@5
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@8
    move-result-object v0

    #@9
    .line 116
    .local v0, lp:Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@b
    const/16 v2, 0x30

    #@d
    if-eq v1, v2, :cond_15

    #@f
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@11
    const/16 v2, 0x50

    #@13
    if-ne v1, v2, :cond_21

    #@15
    .line 117
    :cond_15
    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@17
    .line 118
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@19
    .line 123
    :goto_19
    invoke-virtual {p0}, Landroid/inputmethodservice/SoftInputWindow;->getWindow()Landroid/view/Window;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    #@20
    .line 124
    return-void

    #@21
    .line 120
    :cond_21
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@23
    .line 121
    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@25
    goto :goto_19
.end method

.method public setToken(Landroid/os/IBinder;)V
    .registers 4
    .parameter "token"

    #@0
    .prologue
    .line 39
    invoke-virtual {p0}, Landroid/inputmethodservice/SoftInputWindow;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@7
    move-result-object v0

    #@8
    .line 40
    .local v0, lp:Landroid/view/WindowManager$LayoutParams;
    iput-object p1, v0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@a
    .line 41
    invoke-virtual {p0}, Landroid/inputmethodservice/SoftInputWindow;->getWindow()Landroid/view/Window;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    #@11
    .line 42
    return-void
.end method
