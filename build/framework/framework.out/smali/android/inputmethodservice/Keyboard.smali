.class public Landroid/inputmethodservice/Keyboard;
.super Ljava/lang/Object;
.source "Keyboard.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/inputmethodservice/Keyboard$Key;,
        Landroid/inputmethodservice/Keyboard$Row;
    }
.end annotation


# static fields
.field public static final EDGE_BOTTOM:I = 0x8

.field public static final EDGE_LEFT:I = 0x1

.field public static final EDGE_RIGHT:I = 0x2

.field public static final EDGE_TOP:I = 0x4

.field private static final GRID_HEIGHT:I = 0x5

.field private static final GRID_SIZE:I = 0x32

.field private static final GRID_WIDTH:I = 0xa

.field public static final KEYCODE_ALT:I = -0x6

.field public static final KEYCODE_CANCEL:I = -0x3

.field public static final KEYCODE_DELETE:I = -0x5

.field public static final KEYCODE_DONE:I = -0x4

.field public static final KEYCODE_MODE_CHANGE:I = -0x2

.field public static final KEYCODE_SHIFT:I = -0x1

.field private static SEARCH_DISTANCE:F = 0.0f

.field static final TAG:Ljava/lang/String; = "Keyboard"

.field private static final TAG_KEY:Ljava/lang/String; = "Key"

.field private static final TAG_KEYBOARD:Ljava/lang/String; = "Keyboard"

.field private static final TAG_ROW:Ljava/lang/String; = "Row"


# instance fields
.field private mCellHeight:I

.field private mCellWidth:I

.field private mDefaultHeight:I

.field private mDefaultHorizontalGap:I

.field private mDefaultVerticalGap:I

.field private mDefaultWidth:I

.field private mDisplayHeight:I

.field private mDisplayWidth:I

.field private mGridNeighbors:[[I

.field private mKeyHeight:I

.field private mKeyWidth:I

.field private mKeyboardMode:I

.field private mKeys:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/inputmethodservice/Keyboard$Key;",
            ">;"
        }
    .end annotation
.end field

.field private mLabel:Ljava/lang/CharSequence;

.field private mModifierKeys:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/inputmethodservice/Keyboard$Key;",
            ">;"
        }
    .end annotation
.end field

.field private mProximityThreshold:I

.field private mShiftKeyIndices:[I

.field private mShiftKeys:[Landroid/inputmethodservice/Keyboard$Key;

.field private mShifted:Z

.field private mTotalHeight:I

.field private mTotalWidth:I

.field private rows:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/inputmethodservice/Keyboard$Row;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 145
    const v0, 0x3fe66666

    #@3
    sput v0, Landroid/inputmethodservice/Keyboard;->SEARCH_DISTANCE:F

    #@5
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .registers 4
    .parameter "context"
    .parameter "xmlLayoutResId"

    #@0
    .prologue
    .line 510
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/inputmethodservice/Keyboard;-><init>(Landroid/content/Context;II)V

    #@4
    .line 511
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .registers 10
    .parameter "context"
    .parameter "xmlLayoutResId"
    .parameter "modeId"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x2

    #@2
    const/4 v3, 0x0

    #@3
    .line 543
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 100
    new-array v1, v4, [Landroid/inputmethodservice/Keyboard$Key;

    #@8
    aput-object v5, v1, v3

    #@a
    const/4 v2, 0x1

    #@b
    aput-object v5, v1, v2

    #@d
    iput-object v1, p0, Landroid/inputmethodservice/Keyboard;->mShiftKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@f
    .line 103
    new-array v1, v4, [I

    #@11
    fill-array-data v1, :array_58

    #@14
    iput-object v1, p0, Landroid/inputmethodservice/Keyboard;->mShiftKeyIndices:[I

    #@16
    .line 147
    new-instance v1, Ljava/util/ArrayList;

    #@18
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@1b
    iput-object v1, p0, Landroid/inputmethodservice/Keyboard;->rows:Ljava/util/ArrayList;

    #@1d
    .line 544
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@24
    move-result-object v0

    #@25
    .line 545
    .local v0, dm:Landroid/util/DisplayMetrics;
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    #@27
    iput v1, p0, Landroid/inputmethodservice/Keyboard;->mDisplayWidth:I

    #@29
    .line 546
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    #@2b
    iput v1, p0, Landroid/inputmethodservice/Keyboard;->mDisplayHeight:I

    #@2d
    .line 549
    iput v3, p0, Landroid/inputmethodservice/Keyboard;->mDefaultHorizontalGap:I

    #@2f
    .line 550
    iget v1, p0, Landroid/inputmethodservice/Keyboard;->mDisplayWidth:I

    #@31
    div-int/lit8 v1, v1, 0xa

    #@33
    iput v1, p0, Landroid/inputmethodservice/Keyboard;->mDefaultWidth:I

    #@35
    .line 551
    iput v3, p0, Landroid/inputmethodservice/Keyboard;->mDefaultVerticalGap:I

    #@37
    .line 552
    iget v1, p0, Landroid/inputmethodservice/Keyboard;->mDefaultWidth:I

    #@39
    iput v1, p0, Landroid/inputmethodservice/Keyboard;->mDefaultHeight:I

    #@3b
    .line 553
    new-instance v1, Ljava/util/ArrayList;

    #@3d
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@40
    iput-object v1, p0, Landroid/inputmethodservice/Keyboard;->mKeys:Ljava/util/List;

    #@42
    .line 554
    new-instance v1, Ljava/util/ArrayList;

    #@44
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@47
    iput-object v1, p0, Landroid/inputmethodservice/Keyboard;->mModifierKeys:Ljava/util/List;

    #@49
    .line 555
    iput p3, p0, Landroid/inputmethodservice/Keyboard;->mKeyboardMode:I

    #@4b
    .line 556
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@4e
    move-result-object v1

    #@4f
    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    #@52
    move-result-object v1

    #@53
    invoke-direct {p0, p1, v1}, Landroid/inputmethodservice/Keyboard;->loadKeyboard(Landroid/content/Context;Landroid/content/res/XmlResourceParser;)V

    #@56
    .line 557
    return-void

    #@57
    .line 103
    nop

    #@58
    :array_58
    .array-data 0x4
        0xfft 0xfft 0xfft 0xfft
        0xfft 0xfft 0xfft 0xfft
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;IIII)V
    .registers 11
    .parameter "context"
    .parameter "xmlLayoutResId"
    .parameter "modeId"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x2

    #@2
    const/4 v2, 0x0

    #@3
    .line 522
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 100
    new-array v0, v3, [Landroid/inputmethodservice/Keyboard$Key;

    #@8
    aput-object v4, v0, v2

    #@a
    const/4 v1, 0x1

    #@b
    aput-object v4, v0, v1

    #@d
    iput-object v0, p0, Landroid/inputmethodservice/Keyboard;->mShiftKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@f
    .line 103
    new-array v0, v3, [I

    #@11
    fill-array-data v0, :array_4c

    #@14
    iput-object v0, p0, Landroid/inputmethodservice/Keyboard;->mShiftKeyIndices:[I

    #@16
    .line 147
    new-instance v0, Ljava/util/ArrayList;

    #@18
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@1b
    iput-object v0, p0, Landroid/inputmethodservice/Keyboard;->rows:Ljava/util/ArrayList;

    #@1d
    .line 523
    iput p4, p0, Landroid/inputmethodservice/Keyboard;->mDisplayWidth:I

    #@1f
    .line 524
    iput p5, p0, Landroid/inputmethodservice/Keyboard;->mDisplayHeight:I

    #@21
    .line 526
    iput v2, p0, Landroid/inputmethodservice/Keyboard;->mDefaultHorizontalGap:I

    #@23
    .line 527
    iget v0, p0, Landroid/inputmethodservice/Keyboard;->mDisplayWidth:I

    #@25
    div-int/lit8 v0, v0, 0xa

    #@27
    iput v0, p0, Landroid/inputmethodservice/Keyboard;->mDefaultWidth:I

    #@29
    .line 528
    iput v2, p0, Landroid/inputmethodservice/Keyboard;->mDefaultVerticalGap:I

    #@2b
    .line 529
    iget v0, p0, Landroid/inputmethodservice/Keyboard;->mDefaultWidth:I

    #@2d
    iput v0, p0, Landroid/inputmethodservice/Keyboard;->mDefaultHeight:I

    #@2f
    .line 530
    new-instance v0, Ljava/util/ArrayList;

    #@31
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@34
    iput-object v0, p0, Landroid/inputmethodservice/Keyboard;->mKeys:Ljava/util/List;

    #@36
    .line 531
    new-instance v0, Ljava/util/ArrayList;

    #@38
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@3b
    iput-object v0, p0, Landroid/inputmethodservice/Keyboard;->mModifierKeys:Ljava/util/List;

    #@3d
    .line 532
    iput p3, p0, Landroid/inputmethodservice/Keyboard;->mKeyboardMode:I

    #@3f
    .line 533
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@42
    move-result-object v0

    #@43
    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    #@46
    move-result-object v0

    #@47
    invoke-direct {p0, p1, v0}, Landroid/inputmethodservice/Keyboard;->loadKeyboard(Landroid/content/Context;Landroid/content/res/XmlResourceParser;)V

    #@4a
    .line 534
    return-void

    #@4b
    .line 103
    nop

    #@4c
    :array_4c
    .array-data 0x4
        0xfft 0xfft 0xfft 0xfft
        0xfft 0xfft 0xfft 0xfft
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/CharSequence;II)V
    .registers 16
    .parameter "context"
    .parameter "layoutTemplateResId"
    .parameter "characters"
    .parameter "columns"
    .parameter "horizontalPadding"

    #@0
    .prologue
    .line 575
    invoke-direct {p0, p1, p2}, Landroid/inputmethodservice/Keyboard;-><init>(Landroid/content/Context;I)V

    #@3
    .line 576
    const/4 v6, 0x0

    #@4
    .line 577
    .local v6, x:I
    const/4 v7, 0x0

    #@5
    .line 578
    .local v7, y:I
    const/4 v1, 0x0

    #@6
    .line 579
    .local v1, column:I
    const/4 v8, 0x0

    #@7
    iput v8, p0, Landroid/inputmethodservice/Keyboard;->mTotalWidth:I

    #@9
    .line 581
    new-instance v5, Landroid/inputmethodservice/Keyboard$Row;

    #@b
    invoke-direct {v5, p0}, Landroid/inputmethodservice/Keyboard$Row;-><init>(Landroid/inputmethodservice/Keyboard;)V

    #@e
    .line 582
    .local v5, row:Landroid/inputmethodservice/Keyboard$Row;
    iget v8, p0, Landroid/inputmethodservice/Keyboard;->mDefaultHeight:I

    #@10
    iput v8, v5, Landroid/inputmethodservice/Keyboard$Row;->defaultHeight:I

    #@12
    .line 583
    iget v8, p0, Landroid/inputmethodservice/Keyboard;->mDefaultWidth:I

    #@14
    iput v8, v5, Landroid/inputmethodservice/Keyboard$Row;->defaultWidth:I

    #@16
    .line 584
    iget v8, p0, Landroid/inputmethodservice/Keyboard;->mDefaultHorizontalGap:I

    #@18
    iput v8, v5, Landroid/inputmethodservice/Keyboard$Row;->defaultHorizontalGap:I

    #@1a
    .line 585
    iget v8, p0, Landroid/inputmethodservice/Keyboard;->mDefaultVerticalGap:I

    #@1c
    iput v8, v5, Landroid/inputmethodservice/Keyboard$Row;->verticalGap:I

    #@1e
    .line 586
    const/16 v8, 0xc

    #@20
    iput v8, v5, Landroid/inputmethodservice/Keyboard$Row;->rowEdgeFlags:I

    #@22
    .line 587
    const/4 v8, -0x1

    #@23
    if-ne p4, v8, :cond_77

    #@25
    const v4, 0x7fffffff

    #@28
    .line 588
    .local v4, maxColumns:I
    :goto_28
    const/4 v2, 0x0

    #@29
    .local v2, i:I
    :goto_29
    invoke-interface {p3}, Ljava/lang/CharSequence;->length()I

    #@2c
    move-result v8

    #@2d
    if-ge v2, v8, :cond_79

    #@2f
    .line 589
    invoke-interface {p3, v2}, Ljava/lang/CharSequence;->charAt(I)C

    #@32
    move-result v0

    #@33
    .line 590
    .local v0, c:C
    if-ge v1, v4, :cond_3d

    #@35
    iget v8, p0, Landroid/inputmethodservice/Keyboard;->mDefaultWidth:I

    #@37
    add-int/2addr v8, v6

    #@38
    add-int/2addr v8, p5

    #@39
    iget v9, p0, Landroid/inputmethodservice/Keyboard;->mDisplayWidth:I

    #@3b
    if-le v8, v9, :cond_45

    #@3d
    .line 592
    :cond_3d
    const/4 v6, 0x0

    #@3e
    .line 593
    iget v8, p0, Landroid/inputmethodservice/Keyboard;->mDefaultVerticalGap:I

    #@40
    iget v9, p0, Landroid/inputmethodservice/Keyboard;->mDefaultHeight:I

    #@42
    add-int/2addr v8, v9

    #@43
    add-int/2addr v7, v8

    #@44
    .line 594
    const/4 v1, 0x0

    #@45
    .line 596
    :cond_45
    new-instance v3, Landroid/inputmethodservice/Keyboard$Key;

    #@47
    invoke-direct {v3, v5}, Landroid/inputmethodservice/Keyboard$Key;-><init>(Landroid/inputmethodservice/Keyboard$Row;)V

    #@4a
    .line 597
    .local v3, key:Landroid/inputmethodservice/Keyboard$Key;
    iput v6, v3, Landroid/inputmethodservice/Keyboard$Key;->x:I

    #@4c
    .line 598
    iput v7, v3, Landroid/inputmethodservice/Keyboard$Key;->y:I

    #@4e
    .line 599
    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    #@51
    move-result-object v8

    #@52
    iput-object v8, v3, Landroid/inputmethodservice/Keyboard$Key;->label:Ljava/lang/CharSequence;

    #@54
    .line 600
    const/4 v8, 0x1

    #@55
    new-array v8, v8, [I

    #@57
    const/4 v9, 0x0

    #@58
    aput v0, v8, v9

    #@5a
    iput-object v8, v3, Landroid/inputmethodservice/Keyboard$Key;->codes:[I

    #@5c
    .line 601
    add-int/lit8 v1, v1, 0x1

    #@5e
    .line 602
    iget v8, v3, Landroid/inputmethodservice/Keyboard$Key;->width:I

    #@60
    iget v9, v3, Landroid/inputmethodservice/Keyboard$Key;->gap:I

    #@62
    add-int/2addr v8, v9

    #@63
    add-int/2addr v6, v8

    #@64
    .line 603
    iget-object v8, p0, Landroid/inputmethodservice/Keyboard;->mKeys:Ljava/util/List;

    #@66
    invoke-interface {v8, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@69
    .line 604
    iget-object v8, v5, Landroid/inputmethodservice/Keyboard$Row;->mKeys:Ljava/util/ArrayList;

    #@6b
    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@6e
    .line 605
    iget v8, p0, Landroid/inputmethodservice/Keyboard;->mTotalWidth:I

    #@70
    if-le v6, v8, :cond_74

    #@72
    .line 606
    iput v6, p0, Landroid/inputmethodservice/Keyboard;->mTotalWidth:I

    #@74
    .line 588
    :cond_74
    add-int/lit8 v2, v2, 0x1

    #@76
    goto :goto_29

    #@77
    .end local v0           #c:C
    .end local v2           #i:I
    .end local v3           #key:Landroid/inputmethodservice/Keyboard$Key;
    .end local v4           #maxColumns:I
    :cond_77
    move v4, p4

    #@78
    .line 587
    goto :goto_28

    #@79
    .line 609
    .restart local v2       #i:I
    .restart local v4       #maxColumns:I
    :cond_79
    iget v8, p0, Landroid/inputmethodservice/Keyboard;->mDefaultHeight:I

    #@7b
    add-int/2addr v8, v7

    #@7c
    iput v8, p0, Landroid/inputmethodservice/Keyboard;->mTotalHeight:I

    #@7e
    .line 610
    iget-object v8, p0, Landroid/inputmethodservice/Keyboard;->rows:Ljava/util/ArrayList;

    #@80
    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@83
    .line 611
    return-void
.end method

.method static synthetic access$000(Landroid/inputmethodservice/Keyboard;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget v0, p0, Landroid/inputmethodservice/Keyboard;->mDisplayWidth:I

    #@2
    return v0
.end method

.method static synthetic access$100(Landroid/inputmethodservice/Keyboard;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget v0, p0, Landroid/inputmethodservice/Keyboard;->mDefaultWidth:I

    #@2
    return v0
.end method

.method static synthetic access$200(Landroid/inputmethodservice/Keyboard;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget v0, p0, Landroid/inputmethodservice/Keyboard;->mDisplayHeight:I

    #@2
    return v0
.end method

.method static synthetic access$300(Landroid/inputmethodservice/Keyboard;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget v0, p0, Landroid/inputmethodservice/Keyboard;->mDefaultHeight:I

    #@2
    return v0
.end method

.method static synthetic access$400(Landroid/inputmethodservice/Keyboard;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget v0, p0, Landroid/inputmethodservice/Keyboard;->mDefaultHorizontalGap:I

    #@2
    return v0
.end method

.method static synthetic access$500(Landroid/inputmethodservice/Keyboard;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget v0, p0, Landroid/inputmethodservice/Keyboard;->mDefaultVerticalGap:I

    #@2
    return v0
.end method

.method private computeNearestNeighbors()V
    .registers 15

    #@0
    .prologue
    const/4 v13, 0x0

    #@1
    .line 726
    invoke-virtual {p0}, Landroid/inputmethodservice/Keyboard;->getMinWidth()I

    #@4
    move-result v10

    #@5
    add-int/lit8 v10, v10, 0xa

    #@7
    add-int/lit8 v10, v10, -0x1

    #@9
    div-int/lit8 v10, v10, 0xa

    #@b
    iput v10, p0, Landroid/inputmethodservice/Keyboard;->mCellWidth:I

    #@d
    .line 727
    invoke-virtual {p0}, Landroid/inputmethodservice/Keyboard;->getHeight()I

    #@10
    move-result v10

    #@11
    add-int/lit8 v10, v10, 0x5

    #@13
    add-int/lit8 v10, v10, -0x1

    #@15
    div-int/lit8 v10, v10, 0x5

    #@17
    iput v10, p0, Landroid/inputmethodservice/Keyboard;->mCellHeight:I

    #@19
    .line 728
    const/16 v10, 0x32

    #@1b
    new-array v10, v10, [[I

    #@1d
    iput-object v10, p0, Landroid/inputmethodservice/Keyboard;->mGridNeighbors:[[I

    #@1f
    .line 729
    iget-object v10, p0, Landroid/inputmethodservice/Keyboard;->mKeys:Ljava/util/List;

    #@21
    invoke-interface {v10}, Ljava/util/List;->size()I

    #@24
    move-result v10

    #@25
    new-array v6, v10, [I

    #@27
    .line 730
    .local v6, indices:[I
    iget v10, p0, Landroid/inputmethodservice/Keyboard;->mCellWidth:I

    #@29
    mul-int/lit8 v4, v10, 0xa

    #@2b
    .line 731
    .local v4, gridWidth:I
    iget v10, p0, Landroid/inputmethodservice/Keyboard;->mCellHeight:I

    #@2d
    mul-int/lit8 v3, v10, 0x5

    #@2f
    .line 732
    .local v3, gridHeight:I
    const/4 v8, 0x0

    #@30
    .local v8, x:I
    :goto_30
    if-ge v8, v4, :cond_9f

    #@32
    .line 733
    const/4 v9, 0x0

    #@33
    .local v9, y:I
    :goto_33
    if-ge v9, v3, :cond_9b

    #@35
    .line 734
    const/4 v1, 0x0

    #@36
    .line 735
    .local v1, count:I
    const/4 v5, 0x0

    #@37
    .local v5, i:I
    :goto_37
    iget-object v10, p0, Landroid/inputmethodservice/Keyboard;->mKeys:Ljava/util/List;

    #@39
    invoke-interface {v10}, Ljava/util/List;->size()I

    #@3c
    move-result v10

    #@3d
    if-ge v5, v10, :cond_83

    #@3f
    .line 736
    iget-object v10, p0, Landroid/inputmethodservice/Keyboard;->mKeys:Ljava/util/List;

    #@41
    invoke-interface {v10, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@44
    move-result-object v7

    #@45
    check-cast v7, Landroid/inputmethodservice/Keyboard$Key;

    #@47
    .line 737
    .local v7, key:Landroid/inputmethodservice/Keyboard$Key;
    invoke-virtual {v7, v8, v9}, Landroid/inputmethodservice/Keyboard$Key;->squaredDistanceFrom(II)I

    #@4a
    move-result v10

    #@4b
    iget v11, p0, Landroid/inputmethodservice/Keyboard;->mProximityThreshold:I

    #@4d
    if-lt v10, v11, :cond_7b

    #@4f
    iget v10, p0, Landroid/inputmethodservice/Keyboard;->mCellWidth:I

    #@51
    add-int/2addr v10, v8

    #@52
    add-int/lit8 v10, v10, -0x1

    #@54
    invoke-virtual {v7, v10, v9}, Landroid/inputmethodservice/Keyboard$Key;->squaredDistanceFrom(II)I

    #@57
    move-result v10

    #@58
    iget v11, p0, Landroid/inputmethodservice/Keyboard;->mProximityThreshold:I

    #@5a
    if-lt v10, v11, :cond_7b

    #@5c
    iget v10, p0, Landroid/inputmethodservice/Keyboard;->mCellWidth:I

    #@5e
    add-int/2addr v10, v8

    #@5f
    add-int/lit8 v10, v10, -0x1

    #@61
    iget v11, p0, Landroid/inputmethodservice/Keyboard;->mCellHeight:I

    #@63
    add-int/2addr v11, v9

    #@64
    add-int/lit8 v11, v11, -0x1

    #@66
    invoke-virtual {v7, v10, v11}, Landroid/inputmethodservice/Keyboard$Key;->squaredDistanceFrom(II)I

    #@69
    move-result v10

    #@6a
    iget v11, p0, Landroid/inputmethodservice/Keyboard;->mProximityThreshold:I

    #@6c
    if-lt v10, v11, :cond_7b

    #@6e
    iget v10, p0, Landroid/inputmethodservice/Keyboard;->mCellHeight:I

    #@70
    add-int/2addr v10, v9

    #@71
    add-int/lit8 v10, v10, -0x1

    #@73
    invoke-virtual {v7, v8, v10}, Landroid/inputmethodservice/Keyboard$Key;->squaredDistanceFrom(II)I

    #@76
    move-result v10

    #@77
    iget v11, p0, Landroid/inputmethodservice/Keyboard;->mProximityThreshold:I

    #@79
    if-ge v10, v11, :cond_80

    #@7b
    .line 742
    :cond_7b
    add-int/lit8 v2, v1, 0x1

    #@7d
    .end local v1           #count:I
    .local v2, count:I
    aput v5, v6, v1

    #@7f
    move v1, v2

    #@80
    .line 735
    .end local v2           #count:I
    .restart local v1       #count:I
    :cond_80
    add-int/lit8 v5, v5, 0x1

    #@82
    goto :goto_37

    #@83
    .line 745
    .end local v7           #key:Landroid/inputmethodservice/Keyboard$Key;
    :cond_83
    new-array v0, v1, [I

    #@85
    .line 746
    .local v0, cell:[I
    invoke-static {v6, v13, v0, v13, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@88
    .line 747
    iget-object v10, p0, Landroid/inputmethodservice/Keyboard;->mGridNeighbors:[[I

    #@8a
    iget v11, p0, Landroid/inputmethodservice/Keyboard;->mCellHeight:I

    #@8c
    div-int v11, v9, v11

    #@8e
    mul-int/lit8 v11, v11, 0xa

    #@90
    iget v12, p0, Landroid/inputmethodservice/Keyboard;->mCellWidth:I

    #@92
    div-int v12, v8, v12

    #@94
    add-int/2addr v11, v12

    #@95
    aput-object v0, v10, v11

    #@97
    .line 733
    iget v10, p0, Landroid/inputmethodservice/Keyboard;->mCellHeight:I

    #@99
    add-int/2addr v9, v10

    #@9a
    goto :goto_33

    #@9b
    .line 732
    .end local v0           #cell:[I
    .end local v1           #count:I
    .end local v5           #i:I
    :cond_9b
    iget v10, p0, Landroid/inputmethodservice/Keyboard;->mCellWidth:I

    #@9d
    add-int/2addr v8, v10

    #@9e
    goto :goto_30

    #@9f
    .line 750
    .end local v9           #y:I
    :cond_9f
    return-void
.end method

.method static getDimensionOrFraction(Landroid/content/res/TypedArray;III)I
    .registers 7
    .parameter "a"
    .parameter "index"
    .parameter "base"
    .parameter "defValue"

    #@0
    .prologue
    .line 884
    invoke-virtual {p0, p1}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@3
    move-result-object v0

    #@4
    .line 885
    .local v0, value:Landroid/util/TypedValue;
    if-nez v0, :cond_7

    #@6
    .line 892
    .end local p3
    :cond_6
    :goto_6
    return p3

    #@7
    .line 886
    .restart local p3
    :cond_7
    iget v1, v0, Landroid/util/TypedValue;->type:I

    #@9
    const/4 v2, 0x5

    #@a
    if-ne v1, v2, :cond_11

    #@c
    .line 887
    invoke-virtual {p0, p1, p3}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@f
    move-result p3

    #@10
    goto :goto_6

    #@11
    .line 888
    :cond_11
    iget v1, v0, Landroid/util/TypedValue;->type:I

    #@13
    const/4 v2, 0x6

    #@14
    if-ne v1, v2, :cond_6

    #@16
    .line 890
    int-to-float v1, p3

    #@17
    invoke-virtual {p0, p1, p2, p2, v1}, Landroid/content/res/TypedArray;->getFraction(IIIF)F

    #@1a
    move-result v1

    #@1b
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    #@1e
    move-result p3

    #@1f
    goto :goto_6
.end method

.method private loadKeyboard(Landroid/content/Context;Landroid/content/res/XmlResourceParser;)V
    .registers 22
    .parameter "context"
    .parameter "parser"

    #@0
    .prologue
    .line 780
    const/4 v11, 0x0

    #@1
    .line 781
    .local v11, inKey:Z
    const/4 v12, 0x0

    #@2
    .line 782
    .local v12, inRow:Z
    const/4 v14, 0x0

    #@3
    .line 783
    .local v14, leftMostKey:Z
    const/4 v15, 0x0

    #@4
    .line 784
    .local v15, row:I
    const/4 v5, 0x0

    #@5
    .line 785
    .local v5, x:I
    const/4 v6, 0x0

    #@6
    .line 786
    .local v6, y:I
    const/4 v13, 0x0

    #@7
    .line 787
    .local v13, key:Landroid/inputmethodservice/Keyboard$Key;
    const/4 v4, 0x0

    #@8
    .line 788
    .local v4, currentRow:Landroid/inputmethodservice/Keyboard$Row;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@b
    move-result-object v3

    #@c
    .line 789
    .local v3, res:Landroid/content/res/Resources;
    const/16 v16, 0x0

    #@e
    .line 793
    .local v16, skipRow:Z
    :cond_e
    :goto_e
    :try_start_e
    invoke-interface/range {p2 .. p2}, Landroid/content/res/XmlResourceParser;->next()I

    #@11
    move-result v9

    #@12
    .local v9, event:I
    const/4 v2, 0x1

    #@13
    if-eq v9, v2, :cond_c7

    #@15
    .line 794
    const/4 v2, 0x2

    #@16
    if-ne v9, v2, :cond_f8

    #@18
    .line 795
    invoke-interface/range {p2 .. p2}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    #@1b
    move-result-object v17

    #@1c
    .line 796
    .local v17, tag:Ljava/lang/String;
    const-string v2, "Row"

    #@1e
    move-object/from16 v0, v17

    #@20
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v2

    #@24
    if-eqz v2, :cond_53

    #@26
    .line 797
    const/4 v12, 0x1

    #@27
    .line 798
    const/4 v5, 0x0

    #@28
    .line 799
    move-object/from16 v0, p0

    #@2a
    move-object/from16 v1, p2

    #@2c
    invoke-virtual {v0, v3, v1}, Landroid/inputmethodservice/Keyboard;->createRowFromXml(Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;)Landroid/inputmethodservice/Keyboard$Row;

    #@2f
    move-result-object v4

    #@30
    .line 800
    move-object/from16 v0, p0

    #@32
    iget-object v2, v0, Landroid/inputmethodservice/Keyboard;->rows:Ljava/util/ArrayList;

    #@34
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@37
    .line 801
    iget v2, v4, Landroid/inputmethodservice/Keyboard$Row;->mode:I

    #@39
    if-eqz v2, :cond_50

    #@3b
    iget v2, v4, Landroid/inputmethodservice/Keyboard$Row;->mode:I

    #@3d
    move-object/from16 v0, p0

    #@3f
    iget v7, v0, Landroid/inputmethodservice/Keyboard;->mKeyboardMode:I

    #@41
    if-eq v2, v7, :cond_50

    #@43
    const/16 v16, 0x1

    #@45
    .line 802
    :goto_45
    if-eqz v16, :cond_e

    #@47
    .line 803
    move-object/from16 v0, p0

    #@49
    move-object/from16 v1, p2

    #@4b
    invoke-direct {v0, v1}, Landroid/inputmethodservice/Keyboard;->skipToEndOfRow(Landroid/content/res/XmlResourceParser;)V

    #@4e
    .line 804
    const/4 v12, 0x0

    #@4f
    goto :goto_e

    #@50
    .line 801
    :cond_50
    const/16 v16, 0x0

    #@52
    goto :goto_45

    #@53
    .line 806
    :cond_53
    const-string v2, "Key"

    #@55
    move-object/from16 v0, v17

    #@57
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5a
    move-result v2

    #@5b
    if-eqz v2, :cond_e5

    #@5d
    .line 807
    const/4 v11, 0x1

    #@5e
    move-object/from16 v2, p0

    #@60
    move-object/from16 v7, p2

    #@62
    .line 808
    invoke-virtual/range {v2 .. v7}, Landroid/inputmethodservice/Keyboard;->createKeyFromXml(Landroid/content/res/Resources;Landroid/inputmethodservice/Keyboard$Row;IILandroid/content/res/XmlResourceParser;)Landroid/inputmethodservice/Keyboard$Key;

    #@65
    move-result-object v13

    #@66
    .line 809
    move-object/from16 v0, p0

    #@68
    iget-object v2, v0, Landroid/inputmethodservice/Keyboard;->mKeys:Ljava/util/List;

    #@6a
    invoke-interface {v2, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@6d
    .line 810
    iget-object v2, v13, Landroid/inputmethodservice/Keyboard$Key;->codes:[I

    #@6f
    const/4 v7, 0x0

    #@70
    aget v2, v2, v7

    #@72
    const/4 v7, -0x1

    #@73
    if-ne v2, v7, :cond_d5

    #@75
    .line 812
    const/4 v10, 0x0

    #@76
    .local v10, i:I
    :goto_76
    move-object/from16 v0, p0

    #@78
    iget-object v2, v0, Landroid/inputmethodservice/Keyboard;->mShiftKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@7a
    array-length v2, v2

    #@7b
    if-ge v10, v2, :cond_9b

    #@7d
    .line 813
    move-object/from16 v0, p0

    #@7f
    iget-object v2, v0, Landroid/inputmethodservice/Keyboard;->mShiftKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@81
    aget-object v2, v2, v10

    #@83
    if-nez v2, :cond_d2

    #@85
    .line 814
    move-object/from16 v0, p0

    #@87
    iget-object v2, v0, Landroid/inputmethodservice/Keyboard;->mShiftKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@89
    aput-object v13, v2, v10

    #@8b
    .line 815
    move-object/from16 v0, p0

    #@8d
    iget-object v2, v0, Landroid/inputmethodservice/Keyboard;->mShiftKeyIndices:[I

    #@8f
    move-object/from16 v0, p0

    #@91
    iget-object v7, v0, Landroid/inputmethodservice/Keyboard;->mKeys:Ljava/util/List;

    #@93
    invoke-interface {v7}, Ljava/util/List;->size()I

    #@96
    move-result v7

    #@97
    add-int/lit8 v7, v7, -0x1

    #@99
    aput v7, v2, v10

    #@9b
    .line 819
    :cond_9b
    move-object/from16 v0, p0

    #@9d
    iget-object v2, v0, Landroid/inputmethodservice/Keyboard;->mModifierKeys:Ljava/util/List;

    #@9f
    invoke-interface {v2, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@a2
    .line 823
    .end local v10           #i:I
    :cond_a2
    :goto_a2
    iget-object v2, v4, Landroid/inputmethodservice/Keyboard$Row;->mKeys:Ljava/util/ArrayList;

    #@a4
    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_a7
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_a7} :catch_a9

    #@a7
    goto/16 :goto_e

    #@a9
    .line 844
    .end local v9           #event:I
    .end local v17           #tag:Ljava/lang/String;
    :catch_a9
    move-exception v8

    #@aa
    .line 845
    .local v8, e:Ljava/lang/Exception;
    const-string v2, "Keyboard"

    #@ac
    new-instance v7, Ljava/lang/StringBuilder;

    #@ae
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@b1
    const-string v18, "Parse error:"

    #@b3
    move-object/from16 v0, v18

    #@b5
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v7

    #@b9
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v7

    #@bd
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c0
    move-result-object v7

    #@c1
    invoke-static {v2, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c4
    .line 846
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    #@c7
    .line 848
    .end local v8           #e:Ljava/lang/Exception;
    :cond_c7
    move-object/from16 v0, p0

    #@c9
    iget v2, v0, Landroid/inputmethodservice/Keyboard;->mDefaultVerticalGap:I

    #@cb
    sub-int v2, v6, v2

    #@cd
    move-object/from16 v0, p0

    #@cf
    iput v2, v0, Landroid/inputmethodservice/Keyboard;->mTotalHeight:I

    #@d1
    .line 849
    return-void

    #@d2
    .line 812
    .restart local v9       #event:I
    .restart local v10       #i:I
    .restart local v17       #tag:Ljava/lang/String;
    :cond_d2
    add-int/lit8 v10, v10, 0x1

    #@d4
    goto :goto_76

    #@d5
    .line 820
    .end local v10           #i:I
    :cond_d5
    :try_start_d5
    iget-object v2, v13, Landroid/inputmethodservice/Keyboard$Key;->codes:[I

    #@d7
    const/4 v7, 0x0

    #@d8
    aget v2, v2, v7

    #@da
    const/4 v7, -0x6

    #@db
    if-ne v2, v7, :cond_a2

    #@dd
    .line 821
    move-object/from16 v0, p0

    #@df
    iget-object v2, v0, Landroid/inputmethodservice/Keyboard;->mModifierKeys:Ljava/util/List;

    #@e1
    invoke-interface {v2, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@e4
    goto :goto_a2

    #@e5
    .line 824
    :cond_e5
    const-string v2, "Keyboard"

    #@e7
    move-object/from16 v0, v17

    #@e9
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ec
    move-result v2

    #@ed
    if-eqz v2, :cond_e

    #@ef
    .line 825
    move-object/from16 v0, p0

    #@f1
    move-object/from16 v1, p2

    #@f3
    invoke-direct {v0, v3, v1}, Landroid/inputmethodservice/Keyboard;->parseKeyboardAttributes(Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;)V

    #@f6
    goto/16 :goto_e

    #@f8
    .line 827
    .end local v17           #tag:Ljava/lang/String;
    :cond_f8
    const/4 v2, 0x3

    #@f9
    if-ne v9, v2, :cond_e

    #@fb
    .line 828
    if-eqz v11, :cond_110

    #@fd
    .line 829
    const/4 v11, 0x0

    #@fe
    .line 830
    iget v2, v13, Landroid/inputmethodservice/Keyboard$Key;->gap:I

    #@100
    iget v7, v13, Landroid/inputmethodservice/Keyboard$Key;->width:I

    #@102
    add-int/2addr v2, v7

    #@103
    add-int/2addr v5, v2

    #@104
    .line 831
    move-object/from16 v0, p0

    #@106
    iget v2, v0, Landroid/inputmethodservice/Keyboard;->mTotalWidth:I

    #@108
    if-le v5, v2, :cond_e

    #@10a
    .line 832
    move-object/from16 v0, p0

    #@10c
    iput v5, v0, Landroid/inputmethodservice/Keyboard;->mTotalWidth:I

    #@10e
    goto/16 :goto_e

    #@110
    .line 834
    :cond_110
    if-eqz v12, :cond_e

    #@112
    .line 835
    const/4 v12, 0x0

    #@113
    .line 836
    iget v2, v4, Landroid/inputmethodservice/Keyboard$Row;->verticalGap:I

    #@115
    add-int/2addr v6, v2

    #@116
    .line 837
    iget v2, v4, Landroid/inputmethodservice/Keyboard$Row;->defaultHeight:I
    :try_end_118
    .catch Ljava/lang/Exception; {:try_start_d5 .. :try_end_118} :catch_a9

    #@118
    add-int/2addr v6, v2

    #@119
    .line 838
    add-int/lit8 v15, v15, 0x1

    #@11b
    goto/16 :goto_e
.end method

.method private parseKeyboardAttributes(Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;)V
    .registers 8
    .parameter "res"
    .parameter "parser"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 863
    invoke-static {p2}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    #@4
    move-result-object v1

    #@5
    sget-object v2, Lcom/android/internal/R$styleable;->Keyboard:[I

    #@7
    invoke-virtual {p1, v1, v2}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@a
    move-result-object v0

    #@b
    .line 866
    .local v0, a:Landroid/content/res/TypedArray;
    iget v1, p0, Landroid/inputmethodservice/Keyboard;->mDisplayWidth:I

    #@d
    iget v2, p0, Landroid/inputmethodservice/Keyboard;->mDisplayWidth:I

    #@f
    div-int/lit8 v2, v2, 0xa

    #@11
    invoke-static {v0, v4, v1, v2}, Landroid/inputmethodservice/Keyboard;->getDimensionOrFraction(Landroid/content/res/TypedArray;III)I

    #@14
    move-result v1

    #@15
    iput v1, p0, Landroid/inputmethodservice/Keyboard;->mDefaultWidth:I

    #@17
    .line 869
    const/4 v1, 0x1

    #@18
    iget v2, p0, Landroid/inputmethodservice/Keyboard;->mDisplayHeight:I

    #@1a
    const/16 v3, 0x32

    #@1c
    invoke-static {v0, v1, v2, v3}, Landroid/inputmethodservice/Keyboard;->getDimensionOrFraction(Landroid/content/res/TypedArray;III)I

    #@1f
    move-result v1

    #@20
    iput v1, p0, Landroid/inputmethodservice/Keyboard;->mDefaultHeight:I

    #@22
    .line 872
    const/4 v1, 0x2

    #@23
    iget v2, p0, Landroid/inputmethodservice/Keyboard;->mDisplayWidth:I

    #@25
    invoke-static {v0, v1, v2, v4}, Landroid/inputmethodservice/Keyboard;->getDimensionOrFraction(Landroid/content/res/TypedArray;III)I

    #@28
    move-result v1

    #@29
    iput v1, p0, Landroid/inputmethodservice/Keyboard;->mDefaultHorizontalGap:I

    #@2b
    .line 875
    const/4 v1, 0x3

    #@2c
    iget v2, p0, Landroid/inputmethodservice/Keyboard;->mDisplayHeight:I

    #@2e
    invoke-static {v0, v1, v2, v4}, Landroid/inputmethodservice/Keyboard;->getDimensionOrFraction(Landroid/content/res/TypedArray;III)I

    #@31
    move-result v1

    #@32
    iput v1, p0, Landroid/inputmethodservice/Keyboard;->mDefaultVerticalGap:I

    #@34
    .line 878
    iget v1, p0, Landroid/inputmethodservice/Keyboard;->mDefaultWidth:I

    #@36
    int-to-float v1, v1

    #@37
    sget v2, Landroid/inputmethodservice/Keyboard;->SEARCH_DISTANCE:F

    #@39
    mul-float/2addr v1, v2

    #@3a
    float-to-int v1, v1

    #@3b
    iput v1, p0, Landroid/inputmethodservice/Keyboard;->mProximityThreshold:I

    #@3d
    .line 879
    iget v1, p0, Landroid/inputmethodservice/Keyboard;->mProximityThreshold:I

    #@3f
    iget v2, p0, Landroid/inputmethodservice/Keyboard;->mProximityThreshold:I

    #@41
    mul-int/2addr v1, v2

    #@42
    iput v1, p0, Landroid/inputmethodservice/Keyboard;->mProximityThreshold:I

    #@44
    .line 880
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@47
    .line 881
    return-void
.end method

.method private skipToEndOfRow(Landroid/content/res/XmlResourceParser;)V
    .registers 5
    .parameter "parser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 854
    :cond_0
    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->next()I

    #@3
    move-result v0

    #@4
    .local v0, event:I
    const/4 v1, 0x1

    #@5
    if-eq v0, v1, :cond_16

    #@7
    .line 855
    const/4 v1, 0x3

    #@8
    if-ne v0, v1, :cond_0

    #@a
    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    const-string v2, "Row"

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_0

    #@16
    .line 860
    :cond_16
    return-void
.end method


# virtual methods
.method protected createKeyFromXml(Landroid/content/res/Resources;Landroid/inputmethodservice/Keyboard$Row;IILandroid/content/res/XmlResourceParser;)Landroid/inputmethodservice/Keyboard$Key;
    .registers 12
    .parameter "res"
    .parameter "parent"
    .parameter "x"
    .parameter "y"
    .parameter "parser"

    #@0
    .prologue
    .line 776
    new-instance v0, Landroid/inputmethodservice/Keyboard$Key;

    #@2
    move-object v1, p1

    #@3
    move-object v2, p2

    #@4
    move v3, p3

    #@5
    move v4, p4

    #@6
    move-object v5, p5

    #@7
    invoke-direct/range {v0 .. v5}, Landroid/inputmethodservice/Keyboard$Key;-><init>(Landroid/content/res/Resources;Landroid/inputmethodservice/Keyboard$Row;IILandroid/content/res/XmlResourceParser;)V

    #@a
    return-object v0
.end method

.method protected createRowFromXml(Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;)Landroid/inputmethodservice/Keyboard$Row;
    .registers 4
    .parameter "res"
    .parameter "parser"

    #@0
    .prologue
    .line 771
    new-instance v0, Landroid/inputmethodservice/Keyboard$Row;

    #@2
    invoke-direct {v0, p1, p0, p2}, Landroid/inputmethodservice/Keyboard$Row;-><init>(Landroid/content/res/Resources;Landroid/inputmethodservice/Keyboard;Landroid/content/res/XmlResourceParser;)V

    #@5
    return-object v0
.end method

.method public getHeight()I
    .registers 2

    #@0
    .prologue
    .line 689
    iget v0, p0, Landroid/inputmethodservice/Keyboard;->mTotalHeight:I

    #@2
    return v0
.end method

.method protected getHorizontalGap()I
    .registers 2

    #@0
    .prologue
    .line 653
    iget v0, p0, Landroid/inputmethodservice/Keyboard;->mDefaultHorizontalGap:I

    #@2
    return v0
.end method

.method protected getKeyHeight()I
    .registers 2

    #@0
    .prologue
    .line 669
    iget v0, p0, Landroid/inputmethodservice/Keyboard;->mDefaultHeight:I

    #@2
    return v0
.end method

.method protected getKeyWidth()I
    .registers 2

    #@0
    .prologue
    .line 677
    iget v0, p0, Landroid/inputmethodservice/Keyboard;->mDefaultWidth:I

    #@2
    return v0
.end method

.method public getKeys()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/inputmethodservice/Keyboard$Key;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 645
    iget-object v0, p0, Landroid/inputmethodservice/Keyboard;->mKeys:Ljava/util/List;

    #@2
    return-object v0
.end method

.method public getMinWidth()I
    .registers 2

    #@0
    .prologue
    .line 693
    iget v0, p0, Landroid/inputmethodservice/Keyboard;->mTotalWidth:I

    #@2
    return v0
.end method

.method public getModifierKeys()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/inputmethodservice/Keyboard$Key;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 649
    iget-object v0, p0, Landroid/inputmethodservice/Keyboard;->mModifierKeys:Ljava/util/List;

    #@2
    return-object v0
.end method

.method public getNearestKeys(II)[I
    .registers 6
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 760
    iget-object v1, p0, Landroid/inputmethodservice/Keyboard;->mGridNeighbors:[[I

    #@2
    if-nez v1, :cond_7

    #@4
    invoke-direct {p0}, Landroid/inputmethodservice/Keyboard;->computeNearestNeighbors()V

    #@7
    .line 761
    :cond_7
    if-ltz p1, :cond_2c

    #@9
    invoke-virtual {p0}, Landroid/inputmethodservice/Keyboard;->getMinWidth()I

    #@c
    move-result v1

    #@d
    if-ge p1, v1, :cond_2c

    #@f
    if-ltz p2, :cond_2c

    #@11
    invoke-virtual {p0}, Landroid/inputmethodservice/Keyboard;->getHeight()I

    #@14
    move-result v1

    #@15
    if-ge p2, v1, :cond_2c

    #@17
    .line 762
    iget v1, p0, Landroid/inputmethodservice/Keyboard;->mCellHeight:I

    #@19
    div-int v1, p2, v1

    #@1b
    mul-int/lit8 v1, v1, 0xa

    #@1d
    iget v2, p0, Landroid/inputmethodservice/Keyboard;->mCellWidth:I

    #@1f
    div-int v2, p1, v2

    #@21
    add-int v0, v1, v2

    #@23
    .line 763
    .local v0, index:I
    const/16 v1, 0x32

    #@25
    if-ge v0, v1, :cond_2c

    #@27
    .line 764
    iget-object v1, p0, Landroid/inputmethodservice/Keyboard;->mGridNeighbors:[[I

    #@29
    aget-object v1, v1, v0

    #@2b
    .line 767
    .end local v0           #index:I
    :goto_2b
    return-object v1

    #@2c
    :cond_2c
    const/4 v1, 0x0

    #@2d
    new-array v1, v1, [I

    #@2f
    goto :goto_2b
.end method

.method public getShiftKeyIndex()I
    .registers 3

    #@0
    .prologue
    .line 721
    iget-object v0, p0, Landroid/inputmethodservice/Keyboard;->mShiftKeyIndices:[I

    #@2
    const/4 v1, 0x0

    #@3
    aget v0, v0, v1

    #@5
    return v0
.end method

.method public getShiftKeyIndices()[I
    .registers 2

    #@0
    .prologue
    .line 717
    iget-object v0, p0, Landroid/inputmethodservice/Keyboard;->mShiftKeyIndices:[I

    #@2
    return-object v0
.end method

.method protected getVerticalGap()I
    .registers 2

    #@0
    .prologue
    .line 661
    iget v0, p0, Landroid/inputmethodservice/Keyboard;->mDefaultVerticalGap:I

    #@2
    return v0
.end method

.method public isShifted()Z
    .registers 2

    #@0
    .prologue
    .line 710
    iget-boolean v0, p0, Landroid/inputmethodservice/Keyboard;->mShifted:Z

    #@2
    return v0
.end method

.method final resize(II)V
    .registers 15
    .parameter "newWidth"
    .parameter "newHeight"

    #@0
    .prologue
    .line 614
    iget-object v10, p0, Landroid/inputmethodservice/Keyboard;->rows:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v3

    #@6
    .line 615
    .local v3, numRows:I
    const/4 v5, 0x0

    #@7
    .local v5, rowIndex:I
    :goto_7
    if-ge v5, v3, :cond_5a

    #@9
    .line 616
    iget-object v10, p0, Landroid/inputmethodservice/Keyboard;->rows:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v4

    #@f
    check-cast v4, Landroid/inputmethodservice/Keyboard$Row;

    #@11
    .line 617
    .local v4, row:Landroid/inputmethodservice/Keyboard$Row;
    iget-object v10, v4, Landroid/inputmethodservice/Keyboard$Row;->mKeys:Ljava/util/ArrayList;

    #@13
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@16
    move-result v2

    #@17
    .line 618
    .local v2, numKeys:I
    const/4 v7, 0x0

    #@18
    .line 619
    .local v7, totalGap:I
    const/4 v8, 0x0

    #@19
    .line 620
    .local v8, totalWidth:I
    const/4 v1, 0x0

    #@1a
    .local v1, keyIndex:I
    :goto_1a
    if-ge v1, v2, :cond_2f

    #@1c
    .line 621
    iget-object v10, v4, Landroid/inputmethodservice/Keyboard$Row;->mKeys:Ljava/util/ArrayList;

    #@1e
    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@21
    move-result-object v0

    #@22
    check-cast v0, Landroid/inputmethodservice/Keyboard$Key;

    #@24
    .line 622
    .local v0, key:Landroid/inputmethodservice/Keyboard$Key;
    if-lez v1, :cond_29

    #@26
    .line 623
    iget v10, v0, Landroid/inputmethodservice/Keyboard$Key;->gap:I

    #@28
    add-int/2addr v7, v10

    #@29
    .line 625
    :cond_29
    iget v10, v0, Landroid/inputmethodservice/Keyboard$Key;->width:I

    #@2b
    add-int/2addr v8, v10

    #@2c
    .line 620
    add-int/lit8 v1, v1, 0x1

    #@2e
    goto :goto_1a

    #@2f
    .line 627
    .end local v0           #key:Landroid/inputmethodservice/Keyboard$Key;
    :cond_2f
    add-int v10, v7, v8

    #@31
    if-le v10, p1, :cond_57

    #@33
    .line 628
    const/4 v9, 0x0

    #@34
    .line 629
    .local v9, x:I
    sub-int v10, p1, v7

    #@36
    int-to-float v10, v10

    #@37
    int-to-float v11, v8

    #@38
    div-float v6, v10, v11

    #@3a
    .line 630
    .local v6, scaleFactor:F
    const/4 v1, 0x0

    #@3b
    :goto_3b
    if-ge v1, v2, :cond_57

    #@3d
    .line 631
    iget-object v10, v4, Landroid/inputmethodservice/Keyboard$Row;->mKeys:Ljava/util/ArrayList;

    #@3f
    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@42
    move-result-object v0

    #@43
    check-cast v0, Landroid/inputmethodservice/Keyboard$Key;

    #@45
    .line 632
    .restart local v0       #key:Landroid/inputmethodservice/Keyboard$Key;
    iget v10, v0, Landroid/inputmethodservice/Keyboard$Key;->width:I

    #@47
    int-to-float v10, v10

    #@48
    mul-float/2addr v10, v6

    #@49
    float-to-int v10, v10

    #@4a
    iput v10, v0, Landroid/inputmethodservice/Keyboard$Key;->width:I

    #@4c
    .line 633
    iput v9, v0, Landroid/inputmethodservice/Keyboard$Key;->x:I

    #@4e
    .line 634
    iget v10, v0, Landroid/inputmethodservice/Keyboard$Key;->width:I

    #@50
    iget v11, v0, Landroid/inputmethodservice/Keyboard$Key;->gap:I

    #@52
    add-int/2addr v10, v11

    #@53
    add-int/2addr v9, v10

    #@54
    .line 630
    add-int/lit8 v1, v1, 0x1

    #@56
    goto :goto_3b

    #@57
    .line 615
    .end local v0           #key:Landroid/inputmethodservice/Keyboard$Key;
    .end local v6           #scaleFactor:F
    .end local v9           #x:I
    :cond_57
    add-int/lit8 v5, v5, 0x1

    #@59
    goto :goto_7

    #@5a
    .line 638
    .end local v1           #keyIndex:I
    .end local v2           #numKeys:I
    .end local v4           #row:Landroid/inputmethodservice/Keyboard$Row;
    .end local v7           #totalGap:I
    .end local v8           #totalWidth:I
    :cond_5a
    iput p1, p0, Landroid/inputmethodservice/Keyboard;->mTotalWidth:I

    #@5c
    .line 642
    return-void
.end method

.method protected setHorizontalGap(I)V
    .registers 2
    .parameter "gap"

    #@0
    .prologue
    .line 657
    iput p1, p0, Landroid/inputmethodservice/Keyboard;->mDefaultHorizontalGap:I

    #@2
    .line 658
    return-void
.end method

.method protected setKeyHeight(I)V
    .registers 2
    .parameter "height"

    #@0
    .prologue
    .line 673
    iput p1, p0, Landroid/inputmethodservice/Keyboard;->mDefaultHeight:I

    #@2
    .line 674
    return-void
.end method

.method protected setKeyWidth(I)V
    .registers 2
    .parameter "width"

    #@0
    .prologue
    .line 681
    iput p1, p0, Landroid/inputmethodservice/Keyboard;->mDefaultWidth:I

    #@2
    .line 682
    return-void
.end method

.method public setShifted(Z)Z
    .registers 7
    .parameter "shiftState"

    #@0
    .prologue
    .line 697
    iget-object v0, p0, Landroid/inputmethodservice/Keyboard;->mShiftKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@2
    .local v0, arr$:[Landroid/inputmethodservice/Keyboard$Key;
    array-length v2, v0

    #@3
    .local v2, len$:I
    const/4 v1, 0x0

    #@4
    .local v1, i$:I
    :goto_4
    if-ge v1, v2, :cond_f

    #@6
    aget-object v3, v0, v1

    #@8
    .line 698
    .local v3, shiftKey:Landroid/inputmethodservice/Keyboard$Key;
    if-eqz v3, :cond_c

    #@a
    .line 699
    iput-boolean p1, v3, Landroid/inputmethodservice/Keyboard$Key;->on:Z

    #@c
    .line 697
    :cond_c
    add-int/lit8 v1, v1, 0x1

    #@e
    goto :goto_4

    #@f
    .line 702
    .end local v3           #shiftKey:Landroid/inputmethodservice/Keyboard$Key;
    :cond_f
    iget-boolean v4, p0, Landroid/inputmethodservice/Keyboard;->mShifted:Z

    #@11
    if-eq v4, p1, :cond_17

    #@13
    .line 703
    iput-boolean p1, p0, Landroid/inputmethodservice/Keyboard;->mShifted:Z

    #@15
    .line 704
    const/4 v4, 0x1

    #@16
    .line 706
    :goto_16
    return v4

    #@17
    :cond_17
    const/4 v4, 0x0

    #@18
    goto :goto_16
.end method

.method protected setVerticalGap(I)V
    .registers 2
    .parameter "gap"

    #@0
    .prologue
    .line 665
    iput p1, p0, Landroid/inputmethodservice/Keyboard;->mDefaultVerticalGap:I

    #@2
    .line 666
    return-void
.end method
