.class public final Landroid/media/MediaCodecInfo;
.super Ljava/lang/Object;
.source "MediaCodecInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/MediaCodecInfo$CodecProfileLevel;,
        Landroid/media/MediaCodecInfo$CodecCapabilities;
    }
.end annotation


# instance fields
.field private mIndex:I


# direct methods
.method constructor <init>(I)V
    .registers 2
    .parameter "index"

    #@0
    .prologue
    .line 22
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 23
    iput p1, p0, Landroid/media/MediaCodecInfo;->mIndex:I

    #@5
    .line 24
    return-void
.end method


# virtual methods
.method public final getCapabilitiesForType(Ljava/lang/String;)Landroid/media/MediaCodecInfo$CodecCapabilities;
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 211
    iget v0, p0, Landroid/media/MediaCodecInfo;->mIndex:I

    #@2
    invoke-static {v0, p1}, Landroid/media/MediaCodecList;->getCodecCapabilities(ILjava/lang/String;)Landroid/media/MediaCodecInfo$CodecCapabilities;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 30
    iget v0, p0, Landroid/media/MediaCodecInfo;->mIndex:I

    #@2
    invoke-static {v0}, Landroid/media/MediaCodecList;->getCodecName(I)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public final getSupportedTypes()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 44
    iget v0, p0, Landroid/media/MediaCodecInfo;->mIndex:I

    #@2
    invoke-static {v0}, Landroid/media/MediaCodecList;->getSupportedTypes(I)[Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public final isEncoder()Z
    .registers 2

    #@0
    .prologue
    .line 37
    iget v0, p0, Landroid/media/MediaCodecInfo;->mIndex:I

    #@2
    invoke-static {v0}, Landroid/media/MediaCodecList;->isEncoder(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method
