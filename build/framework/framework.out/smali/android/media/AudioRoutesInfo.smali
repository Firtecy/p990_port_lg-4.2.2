.class public Landroid/media/AudioRoutesInfo;
.super Ljava/lang/Object;
.source "AudioRoutesInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/media/AudioRoutesInfo;",
            ">;"
        }
    .end annotation
.end field

.field static final MAIN_DOCK_SPEAKERS:I = 0x4

.field static final MAIN_HDMI:I = 0x8

.field static final MAIN_HEADPHONES:I = 0x2

.field static final MAIN_HEADSET:I = 0x1

.field static final MAIN_SPEAKER:I


# instance fields
.field mBluetoothName:Ljava/lang/CharSequence;

.field mMainType:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 61
    new-instance v0, Landroid/media/AudioRoutesInfo$1;

    #@2
    invoke-direct {v0}, Landroid/media/AudioRoutesInfo$1;-><init>()V

    #@5
    sput-object v0, Landroid/media/AudioRoutesInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 37
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 35
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/media/AudioRoutesInfo;->mMainType:I

    #@6
    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/media/AudioRoutesInfo;)V
    .registers 3
    .parameter "o"

    #@0
    .prologue
    .line 40
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 35
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/media/AudioRoutesInfo;->mMainType:I

    #@6
    .line 41
    iget-object v0, p1, Landroid/media/AudioRoutesInfo;->mBluetoothName:Ljava/lang/CharSequence;

    #@8
    iput-object v0, p0, Landroid/media/AudioRoutesInfo;->mBluetoothName:Ljava/lang/CharSequence;

    #@a
    .line 42
    iget v0, p1, Landroid/media/AudioRoutesInfo;->mMainType:I

    #@c
    iput v0, p0, Landroid/media/AudioRoutesInfo;->mMainType:I

    #@e
    .line 43
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "src"

    #@0
    .prologue
    .line 45
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 35
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/media/AudioRoutesInfo;->mMainType:I

    #@6
    .line 46
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@8
    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Ljava/lang/CharSequence;

    #@e
    iput-object v0, p0, Landroid/media/AudioRoutesInfo;->mBluetoothName:Ljava/lang/CharSequence;

    #@10
    .line 47
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@13
    move-result v0

    #@14
    iput v0, p0, Landroid/media/AudioRoutesInfo;->mMainType:I

    #@16
    .line 48
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 52
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 57
    iget-object v0, p0, Landroid/media/AudioRoutesInfo;->mBluetoothName:Ljava/lang/CharSequence;

    #@2
    invoke-static {v0, p1, p2}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    #@5
    .line 58
    iget v0, p0, Landroid/media/AudioRoutesInfo;->mMainType:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 59
    return-void
.end method
