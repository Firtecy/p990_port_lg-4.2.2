.class public Landroid/media/RemoteControlClient$MetadataEditor;
.super Ljava/lang/Object;
.source "RemoteControlClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/RemoteControlClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MetadataEditor"
.end annotation


# static fields
.field public static final BITMAP_KEY_ARTWORK:I = 0x64

.field public static final METADATA_KEY_ARTWORK:I = 0x64


# instance fields
.field private mApplied:Z

.field protected mArtworkChanged:Z

.field protected mEditorArtwork:Landroid/graphics/Bitmap;

.field protected mEditorMetadata:Landroid/os/Bundle;

.field protected mMetadataChanged:Z

.field final synthetic this$0:Landroid/media/RemoteControlClient;


# direct methods
.method private constructor <init>(Landroid/media/RemoteControlClient;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 400
    iput-object p1, p0, Landroid/media/RemoteControlClient$MetadataEditor;->this$0:Landroid/media/RemoteControlClient;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 397
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Landroid/media/RemoteControlClient$MetadataEditor;->mApplied:Z

    #@8
    .line 400
    return-void
.end method

.method synthetic constructor <init>(Landroid/media/RemoteControlClient;Landroid/media/RemoteControlClient$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 380
    invoke-direct {p0, p1}, Landroid/media/RemoteControlClient$MetadataEditor;-><init>(Landroid/media/RemoteControlClient;)V

    #@3
    return-void
.end method


# virtual methods
.method public declared-synchronized apply()V
    .registers 5

    #@0
    .prologue
    .line 532
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/media/RemoteControlClient$MetadataEditor;->mApplied:Z

    #@3
    if-eqz v0, :cond_e

    #@5
    .line 533
    const-string v0, "RemoteControlClient"

    #@7
    const-string v1, "Can\'t apply a previously applied MetadataEditor"

    #@9
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_5e

    #@c
    .line 556
    :goto_c
    monitor-exit p0

    #@d
    return-void

    #@e
    .line 536
    :cond_e
    :try_start_e
    iget-object v0, p0, Landroid/media/RemoteControlClient$MetadataEditor;->this$0:Landroid/media/RemoteControlClient;

    #@10
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$600(Landroid/media/RemoteControlClient;)Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    monitor-enter v1
    :try_end_15
    .catchall {:try_start_e .. :try_end_15} :catchall_5e

    #@15
    .line 538
    :try_start_15
    iget-object v0, p0, Landroid/media/RemoteControlClient$MetadataEditor;->this$0:Landroid/media/RemoteControlClient;

    #@17
    new-instance v2, Landroid/os/Bundle;

    #@19
    iget-object v3, p0, Landroid/media/RemoteControlClient$MetadataEditor;->mEditorMetadata:Landroid/os/Bundle;

    #@1b
    invoke-direct {v2, v3}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@1e
    invoke-static {v0, v2}, Landroid/media/RemoteControlClient;->access$702(Landroid/media/RemoteControlClient;Landroid/os/Bundle;)Landroid/os/Bundle;

    #@21
    .line 539
    iget-object v0, p0, Landroid/media/RemoteControlClient$MetadataEditor;->this$0:Landroid/media/RemoteControlClient;

    #@23
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$800(Landroid/media/RemoteControlClient;)Landroid/graphics/Bitmap;

    #@26
    move-result-object v0

    #@27
    if-eqz v0, :cond_40

    #@29
    iget-object v0, p0, Landroid/media/RemoteControlClient$MetadataEditor;->this$0:Landroid/media/RemoteControlClient;

    #@2b
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$800(Landroid/media/RemoteControlClient;)Landroid/graphics/Bitmap;

    #@2e
    move-result-object v0

    #@2f
    iget-object v2, p0, Landroid/media/RemoteControlClient$MetadataEditor;->mEditorArtwork:Landroid/graphics/Bitmap;

    #@31
    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@34
    move-result v0

    #@35
    if-nez v0, :cond_40

    #@37
    .line 540
    iget-object v0, p0, Landroid/media/RemoteControlClient$MetadataEditor;->this$0:Landroid/media/RemoteControlClient;

    #@39
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$800(Landroid/media/RemoteControlClient;)Landroid/graphics/Bitmap;

    #@3c
    move-result-object v0

    #@3d
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    #@40
    .line 542
    :cond_40
    iget-object v0, p0, Landroid/media/RemoteControlClient$MetadataEditor;->this$0:Landroid/media/RemoteControlClient;

    #@42
    iget-object v2, p0, Landroid/media/RemoteControlClient$MetadataEditor;->mEditorArtwork:Landroid/graphics/Bitmap;

    #@44
    invoke-static {v0, v2}, Landroid/media/RemoteControlClient;->access$802(Landroid/media/RemoteControlClient;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    #@47
    .line 543
    const/4 v0, 0x0

    #@48
    iput-object v0, p0, Landroid/media/RemoteControlClient$MetadataEditor;->mEditorArtwork:Landroid/graphics/Bitmap;

    #@4a
    .line 544
    iget-boolean v0, p0, Landroid/media/RemoteControlClient$MetadataEditor;->mMetadataChanged:Z

    #@4c
    iget-boolean v2, p0, Landroid/media/RemoteControlClient$MetadataEditor;->mArtworkChanged:Z

    #@4e
    and-int/2addr v0, v2

    #@4f
    if-eqz v0, :cond_61

    #@51
    .line 546
    iget-object v0, p0, Landroid/media/RemoteControlClient$MetadataEditor;->this$0:Landroid/media/RemoteControlClient;

    #@53
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$900(Landroid/media/RemoteControlClient;)V

    #@56
    .line 554
    :cond_56
    :goto_56
    const/4 v0, 0x1

    #@57
    iput-boolean v0, p0, Landroid/media/RemoteControlClient$MetadataEditor;->mApplied:Z

    #@59
    .line 555
    monitor-exit v1

    #@5a
    goto :goto_c

    #@5b
    :catchall_5b
    move-exception v0

    #@5c
    monitor-exit v1
    :try_end_5d
    .catchall {:try_start_15 .. :try_end_5d} :catchall_5b

    #@5d
    :try_start_5d
    throw v0
    :try_end_5e
    .catchall {:try_start_5d .. :try_end_5e} :catchall_5e

    #@5e
    .line 532
    :catchall_5e
    move-exception v0

    #@5f
    monitor-exit p0

    #@60
    throw v0

    #@61
    .line 547
    :cond_61
    :try_start_61
    iget-boolean v0, p0, Landroid/media/RemoteControlClient$MetadataEditor;->mMetadataChanged:Z

    #@63
    if-eqz v0, :cond_6b

    #@65
    .line 549
    iget-object v0, p0, Landroid/media/RemoteControlClient$MetadataEditor;->this$0:Landroid/media/RemoteControlClient;

    #@67
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$1000(Landroid/media/RemoteControlClient;)V

    #@6a
    goto :goto_56

    #@6b
    .line 550
    :cond_6b
    iget-boolean v0, p0, Landroid/media/RemoteControlClient$MetadataEditor;->mArtworkChanged:Z

    #@6d
    if-eqz v0, :cond_56

    #@6f
    .line 552
    iget-object v0, p0, Landroid/media/RemoteControlClient$MetadataEditor;->this$0:Landroid/media/RemoteControlClient;

    #@71
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$1100(Landroid/media/RemoteControlClient;)V
    :try_end_74
    .catchall {:try_start_61 .. :try_end_74} :catchall_5b

    #@74
    goto :goto_56
.end method

.method public declared-synchronized clear()V
    .registers 3

    #@0
    .prologue
    .line 517
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/media/RemoteControlClient$MetadataEditor;->mApplied:Z

    #@3
    if-eqz v0, :cond_e

    #@5
    .line 518
    const-string v0, "RemoteControlClient"

    #@7
    const-string v1, "Can\'t clear a previously applied MetadataEditor"

    #@9
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_17

    #@c
    .line 523
    :goto_c
    monitor-exit p0

    #@d
    return-void

    #@e
    .line 521
    :cond_e
    :try_start_e
    iget-object v0, p0, Landroid/media/RemoteControlClient$MetadataEditor;->mEditorMetadata:Landroid/os/Bundle;

    #@10
    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    #@13
    .line 522
    const/4 v0, 0x0

    #@14
    iput-object v0, p0, Landroid/media/RemoteControlClient$MetadataEditor;->mEditorArtwork:Landroid/graphics/Bitmap;
    :try_end_16
    .catchall {:try_start_e .. :try_end_16} :catchall_17

    #@16
    goto :goto_c

    #@17
    .line 517
    :catchall_17
    move-exception v0

    #@18
    monitor-exit p0

    #@19
    throw v0
.end method

.method public clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 405
    new-instance v0, Ljava/lang/CloneNotSupportedException;

    #@2
    invoke-direct {v0}, Ljava/lang/CloneNotSupportedException;-><init>()V

    #@5
    throw v0
.end method

.method public declared-synchronized putBitmap(ILandroid/graphics/Bitmap;)Landroid/media/RemoteControlClient$MetadataEditor;
    .registers 7
    .parameter "key"
    .parameter "bitmap"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 494
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v1, p0, Landroid/media/RemoteControlClient$MetadataEditor;->mApplied:Z

    #@3
    if-eqz v1, :cond_f

    #@5
    .line 495
    const-string v1, "RemoteControlClient"

    #@7
    const-string v2, "Can\'t edit a previously applied MetadataEditor"

    #@9
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_2c

    #@c
    move-object v0, p0

    #@d
    .line 509
    .end local p0
    .local v0, this:Landroid/media/RemoteControlClient$MetadataEditor;
    :goto_d
    monitor-exit p0

    #@e
    return-object v0

    #@f
    .line 498
    .end local v0           #this:Landroid/media/RemoteControlClient$MetadataEditor;
    .restart local p0
    :cond_f
    const/16 v1, 0x64

    #@11
    if-eq p1, v1, :cond_2f

    #@13
    .line 499
    :try_start_13
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@15
    new-instance v2, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v3, "Invalid type \'Bitmap\' for key "

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2b
    throw v1
    :try_end_2c
    .catchall {:try_start_13 .. :try_end_2c} :catchall_2c

    #@2c
    .line 494
    :catchall_2c
    move-exception v1

    #@2d
    monitor-exit p0

    #@2e
    throw v1

    #@2f
    .line 501
    :cond_2f
    :try_start_2f
    iget-object v1, p0, Landroid/media/RemoteControlClient$MetadataEditor;->this$0:Landroid/media/RemoteControlClient;

    #@31
    invoke-static {v1}, Landroid/media/RemoteControlClient;->access$300(Landroid/media/RemoteControlClient;)I

    #@34
    move-result v1

    #@35
    if-lez v1, :cond_58

    #@37
    iget-object v1, p0, Landroid/media/RemoteControlClient$MetadataEditor;->this$0:Landroid/media/RemoteControlClient;

    #@39
    invoke-static {v1}, Landroid/media/RemoteControlClient;->access$400(Landroid/media/RemoteControlClient;)I

    #@3c
    move-result v1

    #@3d
    if-lez v1, :cond_58

    #@3f
    .line 502
    iget-object v1, p0, Landroid/media/RemoteControlClient$MetadataEditor;->this$0:Landroid/media/RemoteControlClient;

    #@41
    iget-object v2, p0, Landroid/media/RemoteControlClient$MetadataEditor;->this$0:Landroid/media/RemoteControlClient;

    #@43
    invoke-static {v2}, Landroid/media/RemoteControlClient;->access$300(Landroid/media/RemoteControlClient;)I

    #@46
    move-result v2

    #@47
    iget-object v3, p0, Landroid/media/RemoteControlClient$MetadataEditor;->this$0:Landroid/media/RemoteControlClient;

    #@49
    invoke-static {v3}, Landroid/media/RemoteControlClient;->access$400(Landroid/media/RemoteControlClient;)I

    #@4c
    move-result v3

    #@4d
    invoke-static {v1, p2, v2, v3}, Landroid/media/RemoteControlClient;->access$500(Landroid/media/RemoteControlClient;Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    #@50
    move-result-object v1

    #@51
    iput-object v1, p0, Landroid/media/RemoteControlClient$MetadataEditor;->mEditorArtwork:Landroid/graphics/Bitmap;

    #@53
    .line 508
    :goto_53
    const/4 v1, 0x1

    #@54
    iput-boolean v1, p0, Landroid/media/RemoteControlClient$MetadataEditor;->mArtworkChanged:Z

    #@56
    move-object v0, p0

    #@57
    .line 509
    .end local p0
    .restart local v0       #this:Landroid/media/RemoteControlClient$MetadataEditor;
    goto :goto_d

    #@58
    .line 506
    .end local v0           #this:Landroid/media/RemoteControlClient$MetadataEditor;
    .restart local p0
    :cond_58
    iput-object p2, p0, Landroid/media/RemoteControlClient$MetadataEditor;->mEditorArtwork:Landroid/graphics/Bitmap;
    :try_end_5a
    .catchall {:try_start_2f .. :try_end_5a} :catchall_2c

    #@5a
    goto :goto_53
.end method

.method public declared-synchronized putLong(IJ)Landroid/media/RemoteControlClient$MetadataEditor;
    .registers 8
    .parameter "key"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 470
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v1, p0, Landroid/media/RemoteControlClient$MetadataEditor;->mApplied:Z

    #@3
    if-eqz v1, :cond_f

    #@5
    .line 471
    const-string v1, "RemoteControlClient"

    #@7
    const-string v2, "Can\'t edit a previously applied MetadataEditor"

    #@9
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_32

    #@c
    move-object v0, p0

    #@d
    .line 479
    .end local p0
    .local v0, this:Landroid/media/RemoteControlClient$MetadataEditor;
    :goto_d
    monitor-exit p0

    #@e
    return-object v0

    #@f
    .line 474
    .end local v0           #this:Landroid/media/RemoteControlClient$MetadataEditor;
    .restart local p0
    :cond_f
    :try_start_f
    invoke-static {}, Landroid/media/RemoteControlClient;->access$200()[I

    #@12
    move-result-object v1

    #@13
    invoke-static {p1, v1}, Landroid/media/RemoteControlClient;->access$100(I[I)Z

    #@16
    move-result v1

    #@17
    if-nez v1, :cond_35

    #@19
    .line 475
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@1b
    new-instance v2, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v3, "Invalid type \'long\' for key "

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@31
    throw v1
    :try_end_32
    .catchall {:try_start_f .. :try_end_32} :catchall_32

    #@32
    .line 470
    :catchall_32
    move-exception v1

    #@33
    monitor-exit p0

    #@34
    throw v1

    #@35
    .line 477
    :cond_35
    :try_start_35
    iget-object v1, p0, Landroid/media/RemoteControlClient$MetadataEditor;->mEditorMetadata:Landroid/os/Bundle;

    #@37
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v1, v2, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@3e
    .line 478
    const/4 v1, 0x1

    #@3f
    iput-boolean v1, p0, Landroid/media/RemoteControlClient$MetadataEditor;->mMetadataChanged:Z
    :try_end_41
    .catchall {:try_start_35 .. :try_end_41} :catchall_32

    #@41
    move-object v0, p0

    #@42
    .line 479
    .end local p0
    .restart local v0       #this:Landroid/media/RemoteControlClient$MetadataEditor;
    goto :goto_d
.end method

.method public declared-synchronized putString(ILjava/lang/String;)Landroid/media/RemoteControlClient$MetadataEditor;
    .registers 7
    .parameter "key"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 441
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v1, p0, Landroid/media/RemoteControlClient$MetadataEditor;->mApplied:Z

    #@3
    if-eqz v1, :cond_f

    #@5
    .line 442
    const-string v1, "RemoteControlClient"

    #@7
    const-string v2, "Can\'t edit a previously applied MetadataEditor"

    #@9
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_32

    #@c
    move-object v0, p0

    #@d
    .line 450
    .end local p0
    .local v0, this:Landroid/media/RemoteControlClient$MetadataEditor;
    :goto_d
    monitor-exit p0

    #@e
    return-object v0

    #@f
    .line 445
    .end local v0           #this:Landroid/media/RemoteControlClient$MetadataEditor;
    .restart local p0
    :cond_f
    :try_start_f
    invoke-static {}, Landroid/media/RemoteControlClient;->access$000()[I

    #@12
    move-result-object v1

    #@13
    invoke-static {p1, v1}, Landroid/media/RemoteControlClient;->access$100(I[I)Z

    #@16
    move-result v1

    #@17
    if-nez v1, :cond_35

    #@19
    .line 446
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@1b
    new-instance v2, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v3, "Invalid type \'String\' for key "

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@31
    throw v1
    :try_end_32
    .catchall {:try_start_f .. :try_end_32} :catchall_32

    #@32
    .line 441
    :catchall_32
    move-exception v1

    #@33
    monitor-exit p0

    #@34
    throw v1

    #@35
    .line 448
    :cond_35
    :try_start_35
    iget-object v1, p0, Landroid/media/RemoteControlClient$MetadataEditor;->mEditorMetadata:Landroid/os/Bundle;

    #@37
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@3e
    .line 449
    const/4 v1, 0x1

    #@3f
    iput-boolean v1, p0, Landroid/media/RemoteControlClient$MetadataEditor;->mMetadataChanged:Z
    :try_end_41
    .catchall {:try_start_35 .. :try_end_41} :catchall_32

    #@41
    move-object v0, p0

    #@42
    .line 450
    .end local p0
    .restart local v0       #this:Landroid/media/RemoteControlClient$MetadataEditor;
    goto :goto_d
.end method
