.class public Landroid/media/videoeditor/VideoEditorFactory;
.super Ljava/lang/Object;
.source "VideoEditorFactory.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 32
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static create(Ljava/lang/String;)Landroid/media/videoeditor/VideoEditor;
    .registers 5
    .parameter "projectPath"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 49
    new-instance v0, Ljava/io/File;

    #@2
    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@5
    .line 50
    .local v0, dir:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@8
    move-result v1

    #@9
    if-nez v1, :cond_3f

    #@b
    .line 51
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    #@e
    move-result v1

    #@f
    if-nez v1, :cond_2a

    #@11
    .line 52
    new-instance v1, Ljava/io/FileNotFoundException;

    #@13
    new-instance v2, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v3, "Cannot create project path: "

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    invoke-direct {v1, v2}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@29
    throw v1

    #@2a
    .line 59
    :cond_2a
    new-instance v1, Ljava/io/File;

    #@2c
    const-string v2, ".nomedia"

    #@2e
    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@31
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    #@34
    move-result v1

    #@35
    if-nez v1, :cond_3f

    #@37
    .line 60
    new-instance v1, Ljava/io/FileNotFoundException;

    #@39
    const-string v2, "Cannot create file .nomedia"

    #@3b
    invoke-direct {v1, v2}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    #@3e
    throw v1

    #@3f
    .line 65
    :cond_3f
    new-instance v1, Landroid/media/videoeditor/VideoEditorImpl;

    #@41
    invoke-direct {v1, p0}, Landroid/media/videoeditor/VideoEditorImpl;-><init>(Ljava/lang/String;)V

    #@44
    return-object v1
.end method

.method public static load(Ljava/lang/String;Z)Landroid/media/videoeditor/VideoEditor;
    .registers 4
    .parameter "projectPath"
    .parameter "generatePreview"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 89
    new-instance v0, Landroid/media/videoeditor/VideoEditorImpl;

    #@2
    invoke-direct {v0, p0}, Landroid/media/videoeditor/VideoEditorImpl;-><init>(Ljava/lang/String;)V

    #@5
    .line 90
    .local v0, videoEditor:Landroid/media/videoeditor/VideoEditor;
    if-eqz p1, :cond_b

    #@7
    .line 91
    const/4 v1, 0x0

    #@8
    invoke-interface {v0, v1}, Landroid/media/videoeditor/VideoEditor;->generatePreview(Landroid/media/videoeditor/VideoEditor$MediaProcessingProgressListener;)V

    #@b
    .line 93
    :cond_b
    return-object v0
.end method
