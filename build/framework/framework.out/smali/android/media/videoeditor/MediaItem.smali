.class public abstract Landroid/media/videoeditor/MediaItem;
.super Ljava/lang/Object;
.source "MediaItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/videoeditor/MediaItem$GetThumbnailListCallback;
    }
.end annotation


# static fields
.field public static final END_OF_FILE:I = -0x1

.field public static final RENDERING_MODE_BLACK_BORDER:I = 0x0

.field public static final RENDERING_MODE_CROPPING:I = 0x2

.field public static final RENDERING_MODE_STRETCH:I = 0x1


# instance fields
.field protected mBeginTransition:Landroid/media/videoeditor/Transition;

.field private mBlankFrameFilename:Ljava/lang/String;

.field private mBlankFrameGenerated:Z

.field private final mEffects:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/media/videoeditor/Effect;",
            ">;"
        }
    .end annotation
.end field

.field protected mEndTransition:Landroid/media/videoeditor/Transition;

.field protected final mFilename:Ljava/lang/String;

.field protected mGeneratedImageClip:Ljava/lang/String;

.field private final mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

.field private final mOverlays:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/media/videoeditor/Overlay;",
            ">;"
        }
    .end annotation
.end field

.field private final mProjectPath:Ljava/lang/String;

.field protected mRegenerateClip:Z

.field private mRenderingMode:I

.field private final mUniqueId:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;I)V
    .registers 12
    .parameter "editor"
    .parameter "mediaItemId"
    .parameter "filename"
    .parameter "renderingMode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x0

    #@2
    .line 130
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 113
    iput-boolean v6, p0, Landroid/media/videoeditor/MediaItem;->mBlankFrameGenerated:Z

    #@7
    .line 115
    iput-object v5, p0, Landroid/media/videoeditor/MediaItem;->mBlankFrameFilename:Ljava/lang/String;

    #@9
    .line 131
    if-nez p3, :cond_13

    #@b
    .line 132
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@d
    const-string v2, "MediaItem : filename is null"

    #@f
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@12
    throw v1

    #@13
    .line 134
    :cond_13
    new-instance v0, Ljava/io/File;

    #@15
    invoke-direct {v0, p3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@18
    .line 135
    .local v0, file:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@1b
    move-result v1

    #@1c
    if-nez v1, :cond_37

    #@1e
    .line 136
    new-instance v1, Ljava/io/IOException;

    #@20
    new-instance v2, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    const-string v3, " not found ! "

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v2

    #@33
    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@36
    throw v1

    #@37
    .line 140
    :cond_37
    const-wide v1, 0x80000000L

    #@3c
    invoke-virtual {v0}, Ljava/io/File;->length()J

    #@3f
    move-result-wide v3

    #@40
    cmp-long v1, v1, v3

    #@42
    if-gtz v1, :cond_4c

    #@44
    .line 141
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@46
    const-string v2, "File size is more than 2GB"

    #@48
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@4b
    throw v1

    #@4c
    .line 143
    :cond_4c
    iput-object p2, p0, Landroid/media/videoeditor/MediaItem;->mUniqueId:Ljava/lang/String;

    #@4e
    .line 144
    iput-object p3, p0, Landroid/media/videoeditor/MediaItem;->mFilename:Ljava/lang/String;

    #@50
    .line 145
    iput p4, p0, Landroid/media/videoeditor/MediaItem;->mRenderingMode:I

    #@52
    .line 146
    new-instance v1, Ljava/util/ArrayList;

    #@54
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@57
    iput-object v1, p0, Landroid/media/videoeditor/MediaItem;->mEffects:Ljava/util/List;

    #@59
    .line 147
    new-instance v1, Ljava/util/ArrayList;

    #@5b
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@5e
    iput-object v1, p0, Landroid/media/videoeditor/MediaItem;->mOverlays:Ljava/util/List;

    #@60
    .line 148
    iput-object v5, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@62
    .line 149
    iput-object v5, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@64
    move-object v1, p1

    #@65
    .line 150
    check-cast v1, Landroid/media/videoeditor/VideoEditorImpl;

    #@67
    invoke-virtual {v1}, Landroid/media/videoeditor/VideoEditorImpl;->getNativeContext()Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@6a
    move-result-object v1

    #@6b
    iput-object v1, p0, Landroid/media/videoeditor/MediaItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@6d
    .line 151
    invoke-interface {p1}, Landroid/media/videoeditor/VideoEditor;->getPath()Ljava/lang/String;

    #@70
    move-result-object v1

    #@71
    iput-object v1, p0, Landroid/media/videoeditor/MediaItem;->mProjectPath:Ljava/lang/String;

    #@73
    .line 152
    iput-boolean v6, p0, Landroid/media/videoeditor/MediaItem;->mRegenerateClip:Z

    #@75
    .line 153
    iput-object v5, p0, Landroid/media/videoeditor/MediaItem;->mGeneratedImageClip:Ljava/lang/String;

    #@77
    .line 154
    return-void
.end method


# virtual methods
.method public addEffect(Landroid/media/videoeditor/Effect;)V
    .registers 7
    .parameter "effect"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 294
    if-nez p1, :cond_b

    #@3
    .line 295
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@5
    const-string v1, "NULL effect cannot be applied"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 298
    :cond_b
    invoke-virtual {p1}, Landroid/media/videoeditor/Effect;->getMediaItem()Landroid/media/videoeditor/MediaItem;

    #@e
    move-result-object v0

    #@f
    if-eq v0, p0, :cond_19

    #@11
    .line 299
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@13
    const-string v1, "Media item mismatch"

    #@15
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@18
    throw v0

    #@19
    .line 302
    :cond_19
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mEffects:Ljava/util/List;

    #@1b
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@1e
    move-result v0

    #@1f
    if-eqz v0, :cond_3e

    #@21
    .line 303
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@23
    new-instance v1, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v2, "Effect already exists: "

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {p1}, Landroid/media/videoeditor/Effect;->getId()Ljava/lang/String;

    #@31
    move-result-object v2

    #@32
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v1

    #@36
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v1

    #@3a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@3d
    throw v0

    #@3e
    .line 306
    :cond_3e
    invoke-virtual {p1}, Landroid/media/videoeditor/Effect;->getStartTime()J

    #@41
    move-result-wide v0

    #@42
    invoke-virtual {p1}, Landroid/media/videoeditor/Effect;->getDuration()J

    #@45
    move-result-wide v2

    #@46
    add-long/2addr v0, v2

    #@47
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaItem;->getDuration()J

    #@4a
    move-result-wide v2

    #@4b
    cmp-long v0, v0, v2

    #@4d
    if-lez v0, :cond_57

    #@4f
    .line 307
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@51
    const-string v1, "Effect start time + effect duration > media clip duration"

    #@53
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@56
    throw v0

    #@57
    .line 311
    :cond_57
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@59
    invoke-virtual {v0, v4}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@5c
    .line 313
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mEffects:Ljava/util/List;

    #@5e
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@61
    .line 315
    invoke-virtual {p1}, Landroid/media/videoeditor/Effect;->getStartTime()J

    #@64
    move-result-wide v0

    #@65
    invoke-virtual {p1}, Landroid/media/videoeditor/Effect;->getDuration()J

    #@68
    move-result-wide v2

    #@69
    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/media/videoeditor/MediaItem;->invalidateTransitions(JJ)V

    #@6c
    .line 317
    instance-of v0, p1, Landroid/media/videoeditor/EffectKenBurns;

    #@6e
    if-eqz v0, :cond_72

    #@70
    .line 318
    iput-boolean v4, p0, Landroid/media/videoeditor/MediaItem;->mRegenerateClip:Z

    #@72
    .line 320
    :cond_72
    return-void
.end method

.method public addOverlay(Landroid/media/videoeditor/Overlay;)V
    .registers 10
    .parameter "overlay"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 417
    if-nez p1, :cond_a

    #@2
    .line 418
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v5, "NULL Overlay cannot be applied"

    #@6
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v4

    #@a
    .line 421
    :cond_a
    invoke-virtual {p1}, Landroid/media/videoeditor/Overlay;->getMediaItem()Landroid/media/videoeditor/MediaItem;

    #@d
    move-result-object v4

    #@e
    if-eq v4, p0, :cond_18

    #@10
    .line 422
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@12
    const-string v5, "Media item mismatch"

    #@14
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v4

    #@18
    .line 425
    :cond_18
    iget-object v4, p0, Landroid/media/videoeditor/MediaItem;->mOverlays:Ljava/util/List;

    #@1a
    invoke-interface {v4, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@1d
    move-result v4

    #@1e
    if-eqz v4, :cond_3d

    #@20
    .line 426
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@22
    new-instance v5, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v6, "Overlay already exists: "

    #@29
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v5

    #@2d
    invoke-virtual {p1}, Landroid/media/videoeditor/Overlay;->getId()Ljava/lang/String;

    #@30
    move-result-object v6

    #@31
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v5

    #@35
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v5

    #@39
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@3c
    throw v4

    #@3d
    .line 429
    :cond_3d
    invoke-virtual {p1}, Landroid/media/videoeditor/Overlay;->getStartTime()J

    #@40
    move-result-wide v4

    #@41
    invoke-virtual {p1}, Landroid/media/videoeditor/Overlay;->getDuration()J

    #@44
    move-result-wide v6

    #@45
    add-long/2addr v4, v6

    #@46
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaItem;->getDuration()J

    #@49
    move-result-wide v6

    #@4a
    cmp-long v4, v4, v6

    #@4c
    if-lez v4, :cond_56

    #@4e
    .line 430
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@50
    const-string v5, "Overlay start time + overlay duration > media clip duration"

    #@52
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@55
    throw v4

    #@56
    .line 434
    :cond_56
    instance-of v4, p1, Landroid/media/videoeditor/OverlayFrame;

    #@58
    if-eqz v4, :cond_b9

    #@5a
    move-object v1, p1

    #@5b
    .line 435
    check-cast v1, Landroid/media/videoeditor/OverlayFrame;

    #@5d
    .line 436
    .local v1, frame:Landroid/media/videoeditor/OverlayFrame;
    invoke-virtual {v1}, Landroid/media/videoeditor/OverlayFrame;->getBitmap()Landroid/graphics/Bitmap;

    #@60
    move-result-object v0

    #@61
    .line 437
    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-nez v0, :cond_6b

    #@63
    .line 438
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@65
    const-string v5, "Overlay bitmap not specified"

    #@67
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@6a
    throw v4

    #@6b
    .line 442
    :cond_6b
    instance-of v4, p0, Landroid/media/videoeditor/MediaVideoItem;

    #@6d
    if-eqz v4, :cond_8b

    #@6f
    .line 443
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaItem;->getWidth()I

    #@72
    move-result v3

    #@73
    .line 444
    .local v3, scaledWidth:I
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaItem;->getHeight()I

    #@76
    move-result v2

    #@77
    .line 454
    .local v2, scaledHeight:I
    :goto_77
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    #@7a
    move-result v4

    #@7b
    if-ne v4, v3, :cond_83

    #@7d
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    #@80
    move-result v4

    #@81
    if-eq v4, v2, :cond_9a

    #@83
    .line 455
    :cond_83
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@85
    const-string v5, "Bitmap dimensions must match media item dimensions"

    #@87
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@8a
    throw v4

    #@8b
    .end local v2           #scaledHeight:I
    .end local v3           #scaledWidth:I
    :cond_8b
    move-object v4, p0

    #@8c
    .line 446
    check-cast v4, Landroid/media/videoeditor/MediaImageItem;

    #@8e
    invoke-virtual {v4}, Landroid/media/videoeditor/MediaImageItem;->getScaledWidth()I

    #@91
    move-result v3

    #@92
    .restart local v3       #scaledWidth:I
    move-object v4, p0

    #@93
    .line 447
    check-cast v4, Landroid/media/videoeditor/MediaImageItem;

    #@95
    invoke-virtual {v4}, Landroid/media/videoeditor/MediaImageItem;->getScaledHeight()I

    #@98
    move-result v2

    #@99
    .restart local v2       #scaledHeight:I
    goto :goto_77

    #@9a
    .line 459
    :cond_9a
    iget-object v4, p0, Landroid/media/videoeditor/MediaItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@9c
    const/4 v5, 0x1

    #@9d
    invoke-virtual {v4, v5}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@a0
    move-object v4, p1

    #@a1
    .line 460
    check-cast v4, Landroid/media/videoeditor/OverlayFrame;

    #@a3
    iget-object v5, p0, Landroid/media/videoeditor/MediaItem;->mProjectPath:Ljava/lang/String;

    #@a5
    invoke-virtual {v4, v5}, Landroid/media/videoeditor/OverlayFrame;->save(Ljava/lang/String;)Ljava/lang/String;

    #@a8
    .line 462
    iget-object v4, p0, Landroid/media/videoeditor/MediaItem;->mOverlays:Ljava/util/List;

    #@aa
    invoke-interface {v4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@ad
    .line 463
    invoke-virtual {p1}, Landroid/media/videoeditor/Overlay;->getStartTime()J

    #@b0
    move-result-wide v4

    #@b1
    invoke-virtual {p1}, Landroid/media/videoeditor/Overlay;->getDuration()J

    #@b4
    move-result-wide v6

    #@b5
    invoke-virtual {p0, v4, v5, v6, v7}, Landroid/media/videoeditor/MediaItem;->invalidateTransitions(JJ)V

    #@b8
    .line 468
    return-void

    #@b9
    .line 466
    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    .end local v1           #frame:Landroid/media/videoeditor/OverlayFrame;
    .end local v2           #scaledHeight:I
    .end local v3           #scaledWidth:I
    :cond_b9
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@bb
    const-string v5, "Overlay not supported"

    #@bd
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@c0
    throw v4
.end method

.method protected adjustTransitions()V
    .registers 5

    #@0
    .prologue
    .line 670
    iget-object v2, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@2
    if-eqz v2, :cond_19

    #@4
    .line 671
    iget-object v2, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@6
    invoke-virtual {v2}, Landroid/media/videoeditor/Transition;->getMaximumDuration()J

    #@9
    move-result-wide v0

    #@a
    .line 672
    .local v0, maxDurationMs:J
    iget-object v2, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@c
    invoke-virtual {v2}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@f
    move-result-wide v2

    #@10
    cmp-long v2, v2, v0

    #@12
    if-lez v2, :cond_19

    #@14
    .line 673
    iget-object v2, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@16
    invoke-virtual {v2, v0, v1}, Landroid/media/videoeditor/Transition;->setDuration(J)V

    #@19
    .line 677
    .end local v0           #maxDurationMs:J
    :cond_19
    iget-object v2, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@1b
    if-eqz v2, :cond_32

    #@1d
    .line 678
    iget-object v2, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@1f
    invoke-virtual {v2}, Landroid/media/videoeditor/Transition;->getMaximumDuration()J

    #@22
    move-result-wide v0

    #@23
    .line 679
    .restart local v0       #maxDurationMs:J
    iget-object v2, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@25
    invoke-virtual {v2}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@28
    move-result-wide v2

    #@29
    cmp-long v2, v2, v0

    #@2b
    if-lez v2, :cond_32

    #@2d
    .line 680
    iget-object v2, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@2f
    invoke-virtual {v2, v0, v1}, Landroid/media/videoeditor/Transition;->setDuration(J)V

    #@32
    .line 683
    .end local v0           #maxDurationMs:J
    :cond_32
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .parameter "object"

    #@0
    .prologue
    .line 608
    instance-of v0, p1, Landroid/media/videoeditor/MediaItem;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 609
    const/4 v0, 0x0

    #@5
    .line 611
    .end local p1
    :goto_5
    return v0

    #@6
    .restart local p1
    :cond_6
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mUniqueId:Ljava/lang/String;

    #@8
    check-cast p1, Landroid/media/videoeditor/MediaItem;

    #@a
    .end local p1
    iget-object v1, p1, Landroid/media/videoeditor/MediaItem;->mUniqueId:Ljava/lang/String;

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    goto :goto_5
.end method

.method generateBlankFrame(Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;)V
    .registers 16
    .parameter "clipSettings"

    #@0
    .prologue
    const/16 v13, 0x40

    #@2
    const/4 v12, 0x0

    #@3
    .line 754
    iget-boolean v10, p0, Landroid/media/videoeditor/MediaItem;->mBlankFrameGenerated:Z

    #@5
    if-nez v10, :cond_5f

    #@7
    .line 755
    const/16 v8, 0x40

    #@9
    .line 756
    .local v8, mWidth:I
    const/16 v7, 0x40

    #@b
    .line 757
    .local v7, mHeight:I
    new-instance v10, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    iget-object v11, p0, Landroid/media/videoeditor/MediaItem;->mProjectPath:Ljava/lang/String;

    #@12
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v10

    #@16
    const-string v11, "/"

    #@18
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v10

    #@1c
    const-string v11, "ghost.rgb"

    #@1e
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v10

    #@22
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v10

    #@26
    new-array v11, v12, [Ljava/lang/Object;

    #@28
    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2b
    move-result-object v10

    #@2c
    iput-object v10, p0, Landroid/media/videoeditor/MediaItem;->mBlankFrameFilename:Ljava/lang/String;

    #@2e
    .line 758
    const/4 v3, 0x0

    #@2f
    .line 760
    .local v3, fl:Ljava/io/FileOutputStream;
    :try_start_2f
    new-instance v4, Ljava/io/FileOutputStream;

    #@31
    iget-object v10, p0, Landroid/media/videoeditor/MediaItem;->mBlankFrameFilename:Ljava/lang/String;

    #@33
    invoke-direct {v4, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_36
    .catch Ljava/io/IOException; {:try_start_2f .. :try_end_36} :catch_75

    #@36
    .end local v3           #fl:Ljava/io/FileOutputStream;
    .local v4, fl:Ljava/io/FileOutputStream;
    move-object v3, v4

    #@37
    .line 764
    .end local v4           #fl:Ljava/io/FileOutputStream;
    .restart local v3       #fl:Ljava/io/FileOutputStream;
    :goto_37
    new-instance v2, Ljava/io/DataOutputStream;

    #@39
    invoke-direct {v2, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@3c
    .line 766
    .local v2, dos:Ljava/io/DataOutputStream;
    new-array v5, v8, [I

    #@3e
    .line 768
    .local v5, framingBuffer:[I
    array-length v10, v5

    #@3f
    mul-int/lit8 v10, v10, 0x4

    #@41
    invoke-static {v10}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@44
    move-result-object v1

    #@45
    .line 771
    .local v1, byteBuffer:Ljava/nio/ByteBuffer;
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    #@48
    move-result-object v0

    #@49
    .line 772
    .local v0, array:[B
    const/4 v9, 0x0

    #@4a
    .line 773
    .local v9, tmp:I
    :goto_4a
    if-ge v9, v7, :cond_59

    #@4c
    .line 774
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    #@4f
    move-result-object v6

    #@50
    .line 775
    .local v6, intBuffer:Ljava/nio/IntBuffer;
    invoke-virtual {v6, v5, v12, v8}, Ljava/nio/IntBuffer;->put([III)Ljava/nio/IntBuffer;

    #@53
    .line 777
    :try_start_53
    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_56
    .catch Ljava/io/IOException; {:try_start_53 .. :try_end_56} :catch_71

    #@56
    .line 781
    :goto_56
    add-int/lit8 v9, v9, 0x1

    #@58
    goto :goto_4a

    #@59
    .line 785
    .end local v6           #intBuffer:Ljava/nio/IntBuffer;
    :cond_59
    :try_start_59
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_5c
    .catch Ljava/io/IOException; {:try_start_59 .. :try_end_5c} :catch_73

    #@5c
    .line 789
    :goto_5c
    const/4 v10, 0x1

    #@5d
    iput-boolean v10, p0, Landroid/media/videoeditor/MediaItem;->mBlankFrameGenerated:Z

    #@5f
    .line 792
    .end local v0           #array:[B
    .end local v1           #byteBuffer:Ljava/nio/ByteBuffer;
    .end local v2           #dos:Ljava/io/DataOutputStream;
    .end local v3           #fl:Ljava/io/FileOutputStream;
    .end local v5           #framingBuffer:[I
    .end local v7           #mHeight:I
    .end local v8           #mWidth:I
    .end local v9           #tmp:I
    :cond_5f
    iget-object v10, p0, Landroid/media/videoeditor/MediaItem;->mBlankFrameFilename:Ljava/lang/String;

    #@61
    iput-object v10, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipPath:Ljava/lang/String;

    #@63
    .line 793
    const/4 v10, 0x5

    #@64
    iput v10, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->fileType:I

    #@66
    .line 794
    iput v12, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->beginCutTime:I

    #@68
    .line 795
    iput v12, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutTime:I

    #@6a
    .line 796
    iput v12, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->mediaRendering:I

    #@6c
    .line 798
    iput v13, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->rgbWidth:I

    #@6e
    .line 799
    iput v13, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->rgbHeight:I

    #@70
    .line 800
    return-void

    #@71
    .line 778
    .restart local v0       #array:[B
    .restart local v1       #byteBuffer:Ljava/nio/ByteBuffer;
    .restart local v2       #dos:Ljava/io/DataOutputStream;
    .restart local v3       #fl:Ljava/io/FileOutputStream;
    .restart local v5       #framingBuffer:[I
    .restart local v6       #intBuffer:Ljava/nio/IntBuffer;
    .restart local v7       #mHeight:I
    .restart local v8       #mWidth:I
    .restart local v9       #tmp:I
    :catch_71
    move-exception v10

    #@72
    goto :goto_56

    #@73
    .line 786
    .end local v6           #intBuffer:Ljava/nio/IntBuffer;
    :catch_73
    move-exception v10

    #@74
    goto :goto_5c

    #@75
    .line 761
    .end local v0           #array:[B
    .end local v1           #byteBuffer:Ljava/nio/ByteBuffer;
    .end local v2           #dos:Ljava/io/DataOutputStream;
    .end local v5           #framingBuffer:[I
    .end local v9           #tmp:I
    :catch_75
    move-exception v10

    #@76
    goto :goto_37
.end method

.method public getAllEffects()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/media/videoeditor/Effect;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 401
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mEffects:Ljava/util/List;

    #@2
    return-object v0
.end method

.method public getAllOverlays()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/media/videoeditor/Overlay;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 540
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mOverlays:Ljava/util/List;

    #@2
    return-object v0
.end method

.method public abstract getAspectRatio()I
.end method

.method public getBeginTransition()Landroid/media/videoeditor/Transition;
    .registers 2

    #@0
    .prologue
    .line 223
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@2
    return-object v0
.end method

.method getClipSettings()Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;
    .registers 6

    #@0
    .prologue
    .line 724
    const/4 v2, 0x0

    #@1
    .line 725
    .local v2, mVI:Landroid/media/videoeditor/MediaVideoItem;
    const/4 v1, 0x0

    #@2
    .line 726
    .local v1, mII:Landroid/media/videoeditor/MediaImageItem;
    new-instance v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@4
    invoke-direct {v0}, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;-><init>()V

    #@7
    .line 727
    .local v0, clipSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;
    invoke-virtual {p0, v0}, Landroid/media/videoeditor/MediaItem;->initClipSettings(Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;)V

    #@a
    .line 728
    instance-of v3, p0, Landroid/media/videoeditor/MediaVideoItem;

    #@c
    if-eqz v3, :cond_3e

    #@e
    move-object v2, p0

    #@f
    .line 729
    check-cast v2, Landroid/media/videoeditor/MediaVideoItem;

    #@11
    .line 730
    invoke-virtual {v2}, Landroid/media/videoeditor/MediaVideoItem;->getFilename()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    iput-object v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipPath:Ljava/lang/String;

    #@17
    .line 731
    iget-object v3, p0, Landroid/media/videoeditor/MediaItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@19
    invoke-virtual {v2}, Landroid/media/videoeditor/MediaVideoItem;->getFileType()I

    #@1c
    move-result v4

    #@1d
    invoke-virtual {v3, v4}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getMediaItemFileType(I)I

    #@20
    move-result v3

    #@21
    iput v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->fileType:I

    #@23
    .line 733
    invoke-virtual {v2}, Landroid/media/videoeditor/MediaVideoItem;->getBoundaryBeginTime()J

    #@26
    move-result-wide v3

    #@27
    long-to-int v3, v3

    #@28
    iput v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->beginCutTime:I

    #@2a
    .line 734
    invoke-virtual {v2}, Landroid/media/videoeditor/MediaVideoItem;->getBoundaryEndTime()J

    #@2d
    move-result-wide v3

    #@2e
    long-to-int v3, v3

    #@2f
    iput v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutTime:I

    #@31
    .line 735
    iget-object v3, p0, Landroid/media/videoeditor/MediaItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@33
    invoke-virtual {v2}, Landroid/media/videoeditor/MediaVideoItem;->getRenderingMode()I

    #@36
    move-result v4

    #@37
    invoke-virtual {v3, v4}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getMediaItemRenderingMode(I)I

    #@3a
    move-result v3

    #@3b
    iput v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->mediaRendering:I

    #@3d
    .line 742
    :cond_3d
    :goto_3d
    return-object v0

    #@3e
    .line 738
    :cond_3e
    instance-of v3, p0, Landroid/media/videoeditor/MediaImageItem;

    #@40
    if-eqz v3, :cond_3d

    #@42
    move-object v1, p0

    #@43
    .line 739
    check-cast v1, Landroid/media/videoeditor/MediaImageItem;

    #@45
    .line 740
    invoke-virtual {v1}, Landroid/media/videoeditor/MediaImageItem;->getImageClipProperties()Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@48
    move-result-object v0

    #@49
    goto :goto_3d
.end method

.method public abstract getDuration()J
.end method

.method public getEffect(Ljava/lang/String;)Landroid/media/videoeditor/Effect;
    .registers 5
    .parameter "effectId"

    #@0
    .prologue
    .line 386
    iget-object v2, p0, Landroid/media/videoeditor/MediaItem;->mEffects:Ljava/util/List;

    #@2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .local v1, i$:Ljava/util/Iterator;
    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_1d

    #@c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/media/videoeditor/Effect;

    #@12
    .line 387
    .local v0, effect:Landroid/media/videoeditor/Effect;
    invoke-virtual {v0}, Landroid/media/videoeditor/Effect;->getId()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v2

    #@1a
    if-eqz v2, :cond_6

    #@1c
    .line 391
    .end local v0           #effect:Landroid/media/videoeditor/Effect;
    :goto_1c
    return-object v0

    #@1d
    :cond_1d
    const/4 v0, 0x0

    #@1e
    goto :goto_1c
.end method

.method public getEndTransition()Landroid/media/videoeditor/Transition;
    .registers 2

    #@0
    .prologue
    .line 237
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@2
    return-object v0
.end method

.method public abstract getFileType()I
.end method

.method public getFilename()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 167
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mFilename:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method getGeneratedImageClip()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 376
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mGeneratedImageClip:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public abstract getHeight()I
.end method

.method public getId()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 160
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mUniqueId:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method getNativeContext()Landroid/media/videoeditor/MediaArtistNativeHelper;
    .registers 2

    #@0
    .prologue
    .line 689
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@2
    return-object v0
.end method

.method public getOverlay(Ljava/lang/String;)Landroid/media/videoeditor/Overlay;
    .registers 5
    .parameter "overlayId"

    #@0
    .prologue
    .line 521
    iget-object v2, p0, Landroid/media/videoeditor/MediaItem;->mOverlays:Ljava/util/List;

    #@2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v0

    #@6
    .local v0, i$:Ljava/util/Iterator;
    :cond_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_1d

    #@c
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Landroid/media/videoeditor/Overlay;

    #@12
    .line 522
    .local v1, overlay:Landroid/media/videoeditor/Overlay;
    invoke-virtual {v1}, Landroid/media/videoeditor/Overlay;->getId()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v2

    #@1a
    if-eqz v2, :cond_6

    #@1c
    .line 527
    .end local v1           #overlay:Landroid/media/videoeditor/Overlay;
    :goto_1c
    return-object v1

    #@1d
    :cond_1d
    const/4 v1, 0x0

    #@1e
    goto :goto_1c
.end method

.method getRegenerateClip()Z
    .registers 2

    #@0
    .prologue
    .line 483
    iget-boolean v0, p0, Landroid/media/videoeditor/MediaItem;->mRegenerateClip:Z

    #@2
    return v0
.end method

.method public getRenderingMode()I
    .registers 2

    #@0
    .prologue
    .line 209
    iget v0, p0, Landroid/media/videoeditor/MediaItem;->mRenderingMode:I

    #@2
    return v0
.end method

.method public abstract getThumbnail(IIJ)Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getThumbnailList(IIJJI[ILandroid/media/videoeditor/MediaItem$GetThumbnailListCallback;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public getThumbnailList(IIJJI)[Landroid/graphics/Bitmap;
    .registers 21
    .parameter "width"
    .parameter "height"
    .parameter "startMs"
    .parameter "endMs"
    .parameter "thumbnailCount"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 588
    move/from16 v0, p7

    #@2
    new-array v11, v0, [Landroid/graphics/Bitmap;

    #@4
    .line 589
    .local v11, bitmaps:[Landroid/graphics/Bitmap;
    move/from16 v0, p7

    #@6
    new-array v9, v0, [I

    #@8
    .line 590
    .local v9, indices:[I
    const/4 v12, 0x0

    #@9
    .local v12, i:I
    :goto_9
    move/from16 v0, p7

    #@b
    if-ge v12, v0, :cond_12

    #@d
    .line 591
    aput v12, v9, v12

    #@f
    .line 590
    add-int/lit8 v12, v12, 0x1

    #@11
    goto :goto_9

    #@12
    .line 593
    :cond_12
    new-instance v10, Landroid/media/videoeditor/MediaItem$1;

    #@14
    invoke-direct {v10, p0, v11}, Landroid/media/videoeditor/MediaItem$1;-><init>(Landroid/media/videoeditor/MediaItem;[Landroid/graphics/Bitmap;)V

    #@17
    move-object v1, p0

    #@18
    move v2, p1

    #@19
    move v3, p2

    #@1a
    move-wide/from16 v4, p3

    #@1c
    move-wide/from16 v6, p5

    #@1e
    move/from16 v8, p7

    #@20
    invoke-virtual/range {v1 .. v10}, Landroid/media/videoeditor/MediaItem;->getThumbnailList(IIJJI[ILandroid/media/videoeditor/MediaItem$GetThumbnailListCallback;)V

    #@23
    .line 600
    return-object v11
.end method

.method public abstract getTimelineDuration()J
.end method

.method public abstract getWidth()I
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 619
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mUniqueId:Ljava/lang/String;

    #@2
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method initClipSettings(Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;)V
    .registers 4
    .parameter "clipSettings"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 699
    iput-object v1, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipPath:Ljava/lang/String;

    #@4
    .line 700
    iput-object v1, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipDecodedPath:Ljava/lang/String;

    #@6
    .line 701
    iput-object v1, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipOriginalPath:Ljava/lang/String;

    #@8
    .line 702
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->fileType:I

    #@a
    .line 703
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutTime:I

    #@c
    .line 704
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->beginCutTime:I

    #@e
    .line 705
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->beginCutPercent:I

    #@10
    .line 706
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutPercent:I

    #@12
    .line 707
    iput-boolean v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomEnabled:Z

    #@14
    .line 708
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomPercentStart:I

    #@16
    .line 709
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomTopLeftXStart:I

    #@18
    .line 710
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomTopLeftYStart:I

    #@1a
    .line 711
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomPercentEnd:I

    #@1c
    .line 712
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomTopLeftXEnd:I

    #@1e
    .line 713
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomTopLeftYEnd:I

    #@20
    .line 714
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->mediaRendering:I

    #@22
    .line 715
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->rgbWidth:I

    #@24
    .line 716
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->rgbHeight:I

    #@26
    .line 717
    return-void
.end method

.method invalidateBlankFrame()V
    .registers 3

    #@0
    .prologue
    .line 806
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mBlankFrameFilename:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_1e

    #@4
    .line 807
    new-instance v0, Ljava/io/File;

    #@6
    iget-object v1, p0, Landroid/media/videoeditor/MediaItem;->mBlankFrameFilename:Ljava/lang/String;

    #@8
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@b
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_1e

    #@11
    .line 808
    new-instance v0, Ljava/io/File;

    #@13
    iget-object v1, p0, Landroid/media/videoeditor/MediaItem;->mBlankFrameFilename:Ljava/lang/String;

    #@15
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@18
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@1b
    .line 809
    const/4 v0, 0x0

    #@1c
    iput-object v0, p0, Landroid/media/videoeditor/MediaItem;->mBlankFrameFilename:Ljava/lang/String;

    #@1e
    .line 812
    :cond_1e
    return-void
.end method

.method abstract invalidateTransitions(JJ)V
.end method

.method abstract invalidateTransitions(JJJJ)V
.end method

.method protected isOverlapping(JJJJ)Z
    .registers 12
    .parameter "startTimeMs1"
    .parameter "durationMs1"
    .parameter "startTimeMs2"
    .parameter "durationMs2"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 654
    add-long v1, p1, p3

    #@3
    cmp-long v1, v1, p5

    #@5
    if-gtz v1, :cond_8

    #@7
    .line 660
    :cond_7
    :goto_7
    return v0

    #@8
    .line 656
    :cond_8
    add-long v1, p5, p7

    #@a
    cmp-long v1, p1, v1

    #@c
    if-gez v1, :cond_7

    #@e
    .line 660
    const/4 v0, 0x1

    #@f
    goto :goto_7
.end method

.method public removeEffect(Ljava/lang/String;)Landroid/media/videoeditor/Effect;
    .registers 9
    .parameter "effectId"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 334
    iget-object v3, p0, Landroid/media/videoeditor/MediaItem;->mEffects:Ljava/util/List;

    #@3
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@6
    move-result-object v1

    #@7
    .local v1, i$:Ljava/util/Iterator;
    :cond_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@a
    move-result v3

    #@b
    if-eqz v3, :cond_4b

    #@d
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@10
    move-result-object v0

    #@11
    check-cast v0, Landroid/media/videoeditor/Effect;

    #@13
    .line 335
    .local v0, effect:Landroid/media/videoeditor/Effect;
    invoke-virtual {v0}, Landroid/media/videoeditor/Effect;->getId()Ljava/lang/String;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a
    move-result v3

    #@1b
    if-eqz v3, :cond_7

    #@1d
    .line 336
    iget-object v3, p0, Landroid/media/videoeditor/MediaItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@1f
    const/4 v4, 0x1

    #@20
    invoke-virtual {v3, v4}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@23
    .line 338
    iget-object v3, p0, Landroid/media/videoeditor/MediaItem;->mEffects:Ljava/util/List;

    #@25
    invoke-interface {v3, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@28
    .line 340
    invoke-virtual {v0}, Landroid/media/videoeditor/Effect;->getStartTime()J

    #@2b
    move-result-wide v3

    #@2c
    invoke-virtual {v0}, Landroid/media/videoeditor/Effect;->getDuration()J

    #@2f
    move-result-wide v5

    #@30
    invoke-virtual {p0, v3, v4, v5, v6}, Landroid/media/videoeditor/MediaItem;->invalidateTransitions(JJ)V

    #@33
    .line 341
    instance-of v3, v0, Landroid/media/videoeditor/EffectKenBurns;

    #@35
    if-eqz v3, :cond_4a

    #@37
    .line 342
    iget-object v3, p0, Landroid/media/videoeditor/MediaItem;->mGeneratedImageClip:Ljava/lang/String;

    #@39
    if-eqz v3, :cond_47

    #@3b
    .line 346
    new-instance v3, Ljava/io/File;

    #@3d
    iget-object v4, p0, Landroid/media/videoeditor/MediaItem;->mGeneratedImageClip:Ljava/lang/String;

    #@3f
    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@42
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    #@45
    .line 350
    iput-object v2, p0, Landroid/media/videoeditor/MediaItem;->mGeneratedImageClip:Ljava/lang/String;

    #@47
    .line 352
    :cond_47
    const/4 v2, 0x0

    #@48
    iput-boolean v2, p0, Landroid/media/videoeditor/MediaItem;->mRegenerateClip:Z

    #@4a
    .line 357
    .end local v0           #effect:Landroid/media/videoeditor/Effect;
    :cond_4a
    :goto_4a
    return-object v0

    #@4b
    :cond_4b
    move-object v0, v2

    #@4c
    goto :goto_4a
.end method

.method public removeOverlay(Ljava/lang/String;)Landroid/media/videoeditor/Overlay;
    .registers 8
    .parameter "overlayId"

    #@0
    .prologue
    .line 498
    iget-object v2, p0, Landroid/media/videoeditor/MediaItem;->mOverlays:Ljava/util/List;

    #@2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v0

    #@6
    .local v0, i$:Ljava/util/Iterator;
    :cond_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_3d

    #@c
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Landroid/media/videoeditor/Overlay;

    #@12
    .line 499
    .local v1, overlay:Landroid/media/videoeditor/Overlay;
    invoke-virtual {v1}, Landroid/media/videoeditor/Overlay;->getId()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v2

    #@1a
    if-eqz v2, :cond_6

    #@1c
    .line 500
    iget-object v2, p0, Landroid/media/videoeditor/MediaItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@1e
    const/4 v3, 0x1

    #@1f
    invoke-virtual {v2, v3}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@22
    .line 502
    iget-object v2, p0, Landroid/media/videoeditor/MediaItem;->mOverlays:Ljava/util/List;

    #@24
    invoke-interface {v2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@27
    .line 503
    instance-of v2, v1, Landroid/media/videoeditor/OverlayFrame;

    #@29
    if-eqz v2, :cond_31

    #@2b
    move-object v2, v1

    #@2c
    .line 504
    check-cast v2, Landroid/media/videoeditor/OverlayFrame;

    #@2e
    invoke-virtual {v2}, Landroid/media/videoeditor/OverlayFrame;->invalidate()V

    #@31
    .line 506
    :cond_31
    invoke-virtual {v1}, Landroid/media/videoeditor/Overlay;->getStartTime()J

    #@34
    move-result-wide v2

    #@35
    invoke-virtual {v1}, Landroid/media/videoeditor/Overlay;->getDuration()J

    #@38
    move-result-wide v4

    #@39
    invoke-virtual {p0, v2, v3, v4, v5}, Landroid/media/videoeditor/MediaItem;->invalidateTransitions(JJ)V

    #@3c
    .line 510
    .end local v1           #overlay:Landroid/media/videoeditor/Overlay;
    :goto_3c
    return-object v1

    #@3d
    :cond_3d
    const/4 v1, 0x0

    #@3e
    goto :goto_3c
.end method

.method setBeginTransition(Landroid/media/videoeditor/Transition;)V
    .registers 2
    .parameter "transition"

    #@0
    .prologue
    .line 216
    iput-object p1, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@2
    .line 217
    return-void
.end method

.method setEndTransition(Landroid/media/videoeditor/Transition;)V
    .registers 2
    .parameter "transition"

    #@0
    .prologue
    .line 230
    iput-object p1, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@2
    .line 231
    return-void
.end method

.method setGeneratedImageClip(Ljava/lang/String;)V
    .registers 2
    .parameter "generatedFilePath"

    #@0
    .prologue
    .line 366
    iput-object p1, p0, Landroid/media/videoeditor/MediaItem;->mGeneratedImageClip:Ljava/lang/String;

    #@2
    .line 367
    return-void
.end method

.method setRegenerateClip(Z)V
    .registers 2
    .parameter "flag"

    #@0
    .prologue
    .line 475
    iput-boolean p1, p0, Landroid/media/videoeditor/MediaItem;->mRegenerateClip:Z

    #@2
    .line 476
    return-void
.end method

.method public setRenderingMode(I)V
    .registers 6
    .parameter "renderingMode"

    #@0
    .prologue
    .line 179
    packed-switch p1, :pswitch_data_3e

    #@3
    .line 186
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@5
    const-string v3, "Invalid Rendering Mode"

    #@7
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v2

    #@b
    .line 189
    :pswitch_b
    iget-object v2, p0, Landroid/media/videoeditor/MediaItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@d
    const/4 v3, 0x1

    #@e
    invoke-virtual {v2, v3}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@11
    .line 191
    iput p1, p0, Landroid/media/videoeditor/MediaItem;->mRenderingMode:I

    #@13
    .line 192
    iget-object v2, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@15
    if-eqz v2, :cond_1c

    #@17
    .line 193
    iget-object v2, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@19
    invoke-virtual {v2}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@1c
    .line 196
    :cond_1c
    iget-object v2, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@1e
    if-eqz v2, :cond_25

    #@20
    .line 197
    iget-object v2, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@22
    invoke-virtual {v2}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@25
    .line 200
    :cond_25
    iget-object v2, p0, Landroid/media/videoeditor/MediaItem;->mOverlays:Ljava/util/List;

    #@27
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@2a
    move-result-object v0

    #@2b
    .local v0, i$:Ljava/util/Iterator;
    :goto_2b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@2e
    move-result v2

    #@2f
    if-eqz v2, :cond_3d

    #@31
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@34
    move-result-object v1

    #@35
    check-cast v1, Landroid/media/videoeditor/Overlay;

    #@37
    .line 201
    .local v1, overlay:Landroid/media/videoeditor/Overlay;
    check-cast v1, Landroid/media/videoeditor/OverlayFrame;

    #@39
    .end local v1           #overlay:Landroid/media/videoeditor/Overlay;
    invoke-virtual {v1}, Landroid/media/videoeditor/OverlayFrame;->invalidateGeneratedFiles()V

    #@3c
    goto :goto_2b

    #@3d
    .line 203
    :cond_3d
    return-void

    #@3e
    .line 179
    :pswitch_data_3e
    .packed-switch 0x0
        :pswitch_b
        :pswitch_b
        :pswitch_b
    .end packed-switch
.end method
