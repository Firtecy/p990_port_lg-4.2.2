.class public Landroid/media/videoeditor/MediaVideoItem;
.super Landroid/media/videoeditor/MediaItem;
.source "MediaVideoItem.java"


# instance fields
.field private final mAspectRatio:I

.field private final mAudioBitrate:I

.field private final mAudioChannels:I

.field private final mAudioSamplingFrequency:I

.field private final mAudioType:I

.field private mAudioWaveformFilename:Ljava/lang/String;

.field private mBeginBoundaryTimeMs:J

.field private final mDurationMs:J

.field private mEndBoundaryTimeMs:J

.field private final mFileType:I

.field private final mFps:I

.field private final mHeight:I

.field private mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

.field private mMuted:Z

.field private final mVideoBitrate:I

.field private mVideoEditor:Landroid/media/videoeditor/VideoEditorImpl;

.field private final mVideoLevel:I

.field private final mVideoProfile:I

.field private final mVideoRotationDegree:I

.field private final mVideoType:I

.field private mVolumePercentage:I

.field private mWaveformData:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/media/videoeditor/WaveformData;",
            ">;"
        }
    .end annotation
.end field

.field private final mWidth:I


# direct methods
.method private constructor <init>()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 71
    const/4 v0, 0x0

    #@2
    invoke-direct {p0, v1, v1, v1, v0}, Landroid/media/videoeditor/MediaVideoItem;-><init>(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;I)V

    #@5
    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;I)V
    .registers 17
    .parameter "editor"
    .parameter "mediaItemId"
    .parameter "filename"
    .parameter "renderingMode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 86
    const-wide/16 v5, 0x0

    #@2
    const-wide/16 v7, -0x1

    #@4
    const/16 v9, 0x64

    #@6
    const/4 v10, 0x0

    #@7
    const/4 v11, 0x0

    #@8
    move-object v0, p0

    #@9
    move-object v1, p1

    #@a
    move-object v2, p2

    #@b
    move-object v3, p3

    #@c
    move/from16 v4, p4

    #@e
    invoke-direct/range {v0 .. v11}, Landroid/media/videoeditor/MediaVideoItem;-><init>(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;IJJIZLjava/lang/String;)V

    #@11
    .line 87
    return-void
.end method

.method constructor <init>(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;IJJIZLjava/lang/String;)V
    .registers 22
    .parameter "editor"
    .parameter "mediaItemId"
    .parameter "filename"
    .parameter "renderingMode"
    .parameter "beginMs"
    .parameter "endMs"
    .parameter "volumePercent"
    .parameter "muted"
    .parameter "audioWaveformFilename"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 110
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/media/videoeditor/MediaItem;-><init>(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;I)V

    #@3
    .line 112
    instance-of v7, p1, Landroid/media/videoeditor/VideoEditorImpl;

    #@5
    if-eqz v7, :cond_14

    #@7
    move-object v7, p1

    #@8
    .line 113
    check-cast v7, Landroid/media/videoeditor/VideoEditorImpl;

    #@a
    invoke-virtual {v7}, Landroid/media/videoeditor/VideoEditorImpl;->getNativeContext()Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@d
    move-result-object v7

    #@e
    iput-object v7, p0, Landroid/media/videoeditor/MediaVideoItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@10
    .line 114
    check-cast p1, Landroid/media/videoeditor/VideoEditorImpl;

    #@12
    .end local p1
    iput-object p1, p0, Landroid/media/videoeditor/MediaVideoItem;->mVideoEditor:Landroid/media/videoeditor/VideoEditorImpl;

    #@14
    .line 119
    :cond_14
    :try_start_14
    iget-object v7, p0, Landroid/media/videoeditor/MediaVideoItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@16
    invoke-virtual {v7, p3}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getMediaProperties(Ljava/lang/String;)Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_19} :catch_28

    #@19
    move-result-object v5

    #@1a
    .line 125
    .local v5, properties:Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
    invoke-static {}, Landroid/media/videoeditor/VideoEditorProfile;->get()Landroid/media/videoeditor/VideoEditorProfile;

    #@1d
    move-result-object v6

    #@1e
    .line 126
    .local v6, veProfile:Landroid/media/videoeditor/VideoEditorProfile;
    if-nez v6, :cond_4a

    #@20
    .line 127
    new-instance v7, Ljava/lang/RuntimeException;

    #@22
    const-string v8, "Can\'t get the video editor profile"

    #@24
    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@27
    throw v7

    #@28
    .line 120
    .end local v5           #properties:Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
    .end local v6           #veProfile:Landroid/media/videoeditor/VideoEditorProfile;
    :catch_28
    move-exception v2

    #@29
    .line 121
    .local v2, e:Ljava/lang/Exception;
    new-instance v7, Ljava/lang/IllegalArgumentException;

    #@2b
    new-instance v8, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@33
    move-result-object v9

    #@34
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v8

    #@38
    const-string v9, " : "

    #@3a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v8

    #@3e
    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v8

    #@42
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v8

    #@46
    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@49
    throw v7

    #@4a
    .line 129
    .end local v2           #e:Ljava/lang/Exception;
    .restart local v5       #properties:Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
    .restart local v6       #veProfile:Landroid/media/videoeditor/VideoEditorProfile;
    :cond_4a
    iget v4, v6, Landroid/media/videoeditor/VideoEditorProfile;->maxInputVideoFrameWidth:I

    #@4c
    .line 130
    .local v4, maxInputWidth:I
    iget v3, v6, Landroid/media/videoeditor/VideoEditorProfile;->maxInputVideoFrameHeight:I

    #@4e
    .line 131
    .local v3, maxInputHeight:I
    iget v7, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->width:I

    #@50
    if-gt v7, v4, :cond_56

    #@52
    iget v7, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->height:I

    #@54
    if-le v7, v3, :cond_91

    #@56
    .line 133
    :cond_56
    new-instance v7, Ljava/lang/IllegalArgumentException;

    #@58
    new-instance v8, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string v9, "Unsupported import resolution. Supported maximum width:"

    #@5f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v8

    #@63
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@66
    move-result-object v8

    #@67
    const-string v9, " height:"

    #@69
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v8

    #@6d
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@70
    move-result-object v8

    #@71
    const-string v9, ", current width:"

    #@73
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v8

    #@77
    iget v9, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->width:I

    #@79
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v8

    #@7d
    const-string v9, " height:"

    #@7f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v8

    #@83
    iget v9, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->height:I

    #@85
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@88
    move-result-object v8

    #@89
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8c
    move-result-object v8

    #@8d
    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@90
    throw v7

    #@91
    .line 140
    :cond_91
    iget-boolean v7, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->profileSupported:Z

    #@93
    if-nez v7, :cond_b0

    #@95
    .line 141
    new-instance v7, Ljava/lang/IllegalArgumentException;

    #@97
    new-instance v8, Ljava/lang/StringBuilder;

    #@99
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@9c
    const-string v9, "Unsupported video profile "

    #@9e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v8

    #@a2
    iget v9, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->profile:I

    #@a4
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v8

    #@a8
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ab
    move-result-object v8

    #@ac
    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@af
    throw v7

    #@b0
    .line 144
    :cond_b0
    iget-boolean v7, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->levelSupported:Z

    #@b2
    if-nez v7, :cond_cf

    #@b4
    .line 145
    new-instance v7, Ljava/lang/IllegalArgumentException;

    #@b6
    new-instance v8, Ljava/lang/StringBuilder;

    #@b8
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@bb
    const-string v9, "Unsupported video level "

    #@bd
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v8

    #@c1
    iget v9, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->level:I

    #@c3
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v8

    #@c7
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ca
    move-result-object v8

    #@cb
    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@ce
    throw v7

    #@cf
    .line 148
    :cond_cf
    iget-object v7, p0, Landroid/media/videoeditor/MediaVideoItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@d1
    iget v8, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->fileType:I

    #@d3
    invoke-virtual {v7, v8}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getFileType(I)I

    #@d6
    move-result v7

    #@d7
    sparse-switch v7, :sswitch_data_17e

    #@da
    .line 155
    new-instance v7, Ljava/lang/IllegalArgumentException;

    #@dc
    const-string v8, "Unsupported Input File Type"

    #@de
    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e1
    throw v7

    #@e2
    .line 158
    :sswitch_e2
    iget-object v7, p0, Landroid/media/videoeditor/MediaVideoItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@e4
    iget v8, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->videoFormat:I

    #@e6
    invoke-virtual {v7, v8}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getVideoCodecType(I)I

    #@e9
    move-result v7

    #@ea
    packed-switch v7, :pswitch_data_18c

    #@ed
    .line 165
    new-instance v7, Ljava/lang/IllegalArgumentException;

    #@ef
    const-string v8, "Unsupported Video Codec Format in Input File"

    #@f1
    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f4
    throw v7

    #@f5
    .line 168
    :pswitch_f5
    iget v7, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->width:I

    #@f7
    iput v7, p0, Landroid/media/videoeditor/MediaVideoItem;->mWidth:I

    #@f9
    .line 169
    iget v7, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->height:I

    #@fb
    iput v7, p0, Landroid/media/videoeditor/MediaVideoItem;->mHeight:I

    #@fd
    .line 170
    iget-object v7, p0, Landroid/media/videoeditor/MediaVideoItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@ff
    iget v8, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->width:I

    #@101
    iget v9, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->height:I

    #@103
    invoke-virtual {v7, v8, v9}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getAspectRatio(II)I

    #@106
    move-result v7

    #@107
    iput v7, p0, Landroid/media/videoeditor/MediaVideoItem;->mAspectRatio:I

    #@109
    .line 172
    iget-object v7, p0, Landroid/media/videoeditor/MediaVideoItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@10b
    iget v8, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->fileType:I

    #@10d
    invoke-virtual {v7, v8}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getFileType(I)I

    #@110
    move-result v7

    #@111
    iput v7, p0, Landroid/media/videoeditor/MediaVideoItem;->mFileType:I

    #@113
    .line 173
    iget-object v7, p0, Landroid/media/videoeditor/MediaVideoItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@115
    iget v8, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->videoFormat:I

    #@117
    invoke-virtual {v7, v8}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getVideoCodecType(I)I

    #@11a
    move-result v7

    #@11b
    iput v7, p0, Landroid/media/videoeditor/MediaVideoItem;->mVideoType:I

    #@11d
    .line 174
    iget v7, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->profile:I

    #@11f
    iput v7, p0, Landroid/media/videoeditor/MediaVideoItem;->mVideoProfile:I

    #@121
    .line 175
    iget v7, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->level:I

    #@123
    iput v7, p0, Landroid/media/videoeditor/MediaVideoItem;->mVideoLevel:I

    #@125
    .line 176
    iget v7, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->videoDuration:I

    #@127
    int-to-long v7, v7

    #@128
    iput-wide v7, p0, Landroid/media/videoeditor/MediaVideoItem;->mDurationMs:J

    #@12a
    .line 177
    iget v7, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->videoBitrate:I

    #@12c
    iput v7, p0, Landroid/media/videoeditor/MediaVideoItem;->mVideoBitrate:I

    #@12e
    .line 178
    iget v7, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->audioBitrate:I

    #@130
    iput v7, p0, Landroid/media/videoeditor/MediaVideoItem;->mAudioBitrate:I

    #@132
    .line 179
    iget v7, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->averageFrameRate:F

    #@134
    float-to-int v7, v7

    #@135
    iput v7, p0, Landroid/media/videoeditor/MediaVideoItem;->mFps:I

    #@137
    .line 180
    iget-object v7, p0, Landroid/media/videoeditor/MediaVideoItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@139
    iget v8, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->audioFormat:I

    #@13b
    invoke-virtual {v7, v8}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    #@13e
    move-result v7

    #@13f
    iput v7, p0, Landroid/media/videoeditor/MediaVideoItem;->mAudioType:I

    #@141
    .line 181
    iget v7, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->audioChannels:I

    #@143
    iput v7, p0, Landroid/media/videoeditor/MediaVideoItem;->mAudioChannels:I

    #@145
    .line 182
    iget v7, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->audioSamplingFrequency:I

    #@147
    iput v7, p0, Landroid/media/videoeditor/MediaVideoItem;->mAudioSamplingFrequency:I

    #@149
    .line 183
    iput-wide p5, p0, Landroid/media/videoeditor/MediaVideoItem;->mBeginBoundaryTimeMs:J

    #@14b
    .line 184
    const-wide/16 v7, -0x1

    #@14d
    cmp-long v7, p7, v7

    #@14f
    if-nez v7, :cond_155

    #@151
    iget-wide v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mDurationMs:J

    #@153
    move-wide/from16 p7, v0

    #@155
    .end local p7
    :cond_155
    move-wide/from16 v0, p7

    #@157
    iput-wide v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mEndBoundaryTimeMs:J

    #@159
    .line 185
    move/from16 v0, p9

    #@15b
    iput v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mVolumePercentage:I

    #@15d
    .line 186
    move/from16 v0, p10

    #@15f
    iput-boolean v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mMuted:Z

    #@161
    .line 187
    move-object/from16 v0, p11

    #@163
    iput-object v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mAudioWaveformFilename:Ljava/lang/String;

    #@165
    .line 188
    if-eqz p11, :cond_17a

    #@167
    .line 189
    new-instance v7, Ljava/lang/ref/SoftReference;

    #@169
    new-instance v8, Landroid/media/videoeditor/WaveformData;

    #@16b
    move-object/from16 v0, p11

    #@16d
    invoke-direct {v8, v0}, Landroid/media/videoeditor/WaveformData;-><init>(Ljava/lang/String;)V

    #@170
    invoke-direct {v7, v8}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    #@173
    iput-object v7, p0, Landroid/media/videoeditor/MediaVideoItem;->mWaveformData:Ljava/lang/ref/SoftReference;

    #@175
    .line 194
    :goto_175
    iget v7, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->videoRotation:I

    #@177
    iput v7, p0, Landroid/media/videoeditor/MediaVideoItem;->mVideoRotationDegree:I

    #@179
    .line 195
    return-void

    #@17a
    .line 192
    :cond_17a
    const/4 v7, 0x0

    #@17b
    iput-object v7, p0, Landroid/media/videoeditor/MediaVideoItem;->mWaveformData:Ljava/lang/ref/SoftReference;

    #@17d
    goto :goto_175

    #@17e
    .line 148
    :sswitch_data_17e
    .sparse-switch
        0x0 -> :sswitch_e2
        0x1 -> :sswitch_e2
        0xa -> :sswitch_e2
    .end sparse-switch

    #@18c
    .line 158
    :pswitch_data_18c
    .packed-switch 0x1
        :pswitch_f5
        :pswitch_f5
        :pswitch_f5
    .end packed-switch
.end method


# virtual methods
.method public addEffect(Landroid/media/videoeditor/Effect;)V
    .registers 4
    .parameter "effect"

    #@0
    .prologue
    .line 273
    instance-of v0, p1, Landroid/media/videoeditor/EffectKenBurns;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 274
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@6
    const-string v1, "Ken Burns effects cannot be applied to MediaVideoItem"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 276
    :cond_c
    invoke-super {p0, p1}, Landroid/media/videoeditor/MediaItem;->addEffect(Landroid/media/videoeditor/Effect;)V

    #@f
    .line 277
    return-void
.end method

.method public extractAudioWaveform(Landroid/media/videoeditor/ExtractAudioWaveformProgressListener;)V
    .registers 12
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    const/4 v8, 0x1

    #@2
    .line 533
    const/4 v4, 0x0

    #@3
    .line 534
    .local v4, frameDuration:I
    const/4 v6, 0x0

    #@4
    .line 535
    .local v6, sampleCount:I
    iget-object v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@6
    invoke-virtual {v0}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getProjectPath()Ljava/lang/String;

    #@9
    move-result-object v9

    #@a
    .line 539
    .local v9, projectPath:Ljava/lang/String;
    iget-object v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mAudioWaveformFilename:Ljava/lang/String;

    #@c
    if-nez v0, :cond_58

    #@e
    .line 543
    const/4 v3, 0x0

    #@f
    .line 545
    .local v3, mAudioWaveFileName:Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    const-string v1, "/"

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    const-string v1, "audioWaveformFile-"

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v0

    #@24
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaVideoItem;->getId()Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v0

    #@2c
    const-string v1, ".dat"

    #@2e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v0

    #@32
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v0

    #@36
    const/4 v1, 0x0

    #@37
    new-array v1, v1, [Ljava/lang/Object;

    #@39
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@3c
    move-result-object v3

    #@3d
    .line 551
    iget-object v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@3f
    iget v1, p0, Landroid/media/videoeditor/MediaVideoItem;->mAudioType:I

    #@41
    invoke-virtual {v0, v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    #@44
    move-result v0

    #@45
    if-ne v0, v8, :cond_67

    #@47
    .line 553
    const/4 v4, 0x5

    #@48
    .line 555
    const/16 v6, 0xa0

    #@4a
    .line 568
    :cond_4a
    :goto_4a
    iget-object v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@4c
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaVideoItem;->getId()Ljava/lang/String;

    #@4f
    move-result-object v1

    #@50
    iget-object v2, p0, Landroid/media/videoeditor/MediaItem;->mFilename:Ljava/lang/String;

    #@52
    move-object v7, p1

    #@53
    invoke-virtual/range {v0 .. v8}, Landroid/media/videoeditor/MediaArtistNativeHelper;->generateAudioGraph(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILandroid/media/videoeditor/ExtractAudioWaveformProgressListener;Z)V

    #@56
    .line 579
    iput-object v3, p0, Landroid/media/videoeditor/MediaVideoItem;->mAudioWaveformFilename:Ljava/lang/String;

    #@58
    .line 581
    .end local v3           #mAudioWaveFileName:Ljava/lang/String;
    :cond_58
    new-instance v0, Ljava/lang/ref/SoftReference;

    #@5a
    new-instance v1, Landroid/media/videoeditor/WaveformData;

    #@5c
    iget-object v2, p0, Landroid/media/videoeditor/MediaVideoItem;->mAudioWaveformFilename:Ljava/lang/String;

    #@5e
    invoke-direct {v1, v2}, Landroid/media/videoeditor/WaveformData;-><init>(Ljava/lang/String;)V

    #@61
    invoke-direct {v0, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    #@64
    iput-object v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mWaveformData:Ljava/lang/ref/SoftReference;

    #@66
    .line 583
    return-void

    #@67
    .line 556
    .restart local v3       #mAudioWaveFileName:Ljava/lang/String;
    :cond_67
    iget-object v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@69
    iget v1, p0, Landroid/media/videoeditor/MediaVideoItem;->mAudioType:I

    #@6b
    invoke-virtual {v0, v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    #@6e
    move-result v0

    #@6f
    const/16 v1, 0x8

    #@71
    if-ne v0, v1, :cond_78

    #@73
    .line 558
    const/16 v4, 0xa

    #@75
    .line 560
    const/16 v6, 0x140

    #@77
    goto :goto_4a

    #@78
    .line 561
    :cond_78
    iget-object v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@7a
    iget v1, p0, Landroid/media/videoeditor/MediaVideoItem;->mAudioType:I

    #@7c
    invoke-virtual {v0, v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    #@7f
    move-result v0

    #@80
    if-ne v0, v5, :cond_4a

    #@82
    .line 563
    const/16 v4, 0x20

    #@84
    .line 565
    const/16 v6, 0x400

    #@86
    goto :goto_4a
.end method

.method public getAspectRatio()I
    .registers 2

    #@0
    .prologue
    .line 428
    iget v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mAspectRatio:I

    #@2
    return v0
.end method

.method public getAudioBitrate()I
    .registers 2

    #@0
    .prologue
    .line 712
    iget v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mAudioBitrate:I

    #@2
    return v0
.end method

.method public getAudioChannels()I
    .registers 2

    #@0
    .prologue
    .line 733
    iget v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mAudioChannels:I

    #@2
    return v0
.end method

.method public getAudioSamplingFrequency()I
    .registers 2

    #@0
    .prologue
    .line 740
    iget v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mAudioSamplingFrequency:I

    #@2
    return v0
.end method

.method public getAudioType()I
    .registers 2

    #@0
    .prologue
    .line 726
    iget v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mAudioType:I

    #@2
    return v0
.end method

.method getAudioWaveformFilename()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 597
    iget-object v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mAudioWaveformFilename:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getBoundaryBeginTime()J
    .registers 3

    #@0
    .prologue
    .line 258
    iget-wide v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mBeginBoundaryTimeMs:J

    #@2
    return-wide v0
.end method

.method public getBoundaryEndTime()J
    .registers 3

    #@0
    .prologue
    .line 265
    iget-wide v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mEndBoundaryTimeMs:J

    #@2
    return-wide v0
.end method

.method public getDuration()J
    .registers 3

    #@0
    .prologue
    .line 470
    iget-wide v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mDurationMs:J

    #@2
    return-wide v0
.end method

.method public getFileType()I
    .registers 2

    #@0
    .prologue
    .line 436
    iget v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mFileType:I

    #@2
    return v0
.end method

.method public getFps()I
    .registers 2

    #@0
    .prologue
    .line 719
    iget v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mFps:I

    #@2
    return v0
.end method

.method public getHeight()I
    .registers 3

    #@0
    .prologue
    .line 457
    iget v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mVideoRotationDegree:I

    #@2
    const/16 v1, 0x5a

    #@4
    if-eq v0, v1, :cond_c

    #@6
    iget v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mVideoRotationDegree:I

    #@8
    const/16 v1, 0x10e

    #@a
    if-ne v0, v1, :cond_f

    #@c
    .line 459
    :cond_c
    iget v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mWidth:I

    #@e
    .line 461
    :goto_e
    return v0

    #@f
    :cond_f
    iget v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mHeight:I

    #@11
    goto :goto_e
.end method

.method public getThumbnail(IIJ)Landroid/graphics/Bitmap;
    .registers 13
    .parameter "width"
    .parameter "height"
    .parameter "timeMs"

    #@0
    .prologue
    .line 284
    iget-wide v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mDurationMs:J

    #@2
    cmp-long v0, p3, v0

    #@4
    if-lez v0, :cond_e

    #@6
    .line 285
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v1, "Time Exceeds duration"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 288
    :cond_e
    const-wide/16 v0, 0x0

    #@10
    cmp-long v0, p3, v0

    #@12
    if-gez v0, :cond_1c

    #@14
    .line 289
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@16
    const-string v1, "Invalid Time duration"

    #@18
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 292
    :cond_1c
    if-lez p1, :cond_20

    #@1e
    if-gtz p2, :cond_28

    #@20
    .line 293
    :cond_20
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@22
    const-string v1, "Invalid Dimensions"

    #@24
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@27
    throw v0

    #@28
    .line 296
    :cond_28
    iget v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mVideoRotationDegree:I

    #@2a
    const/16 v1, 0x5a

    #@2c
    if-eq v0, v1, :cond_34

    #@2e
    iget v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mVideoRotationDegree:I

    #@30
    const/16 v1, 0x10e

    #@32
    if-ne v0, v1, :cond_37

    #@34
    .line 297
    :cond_34
    move v7, p1

    #@35
    .line 298
    .local v7, temp:I
    move p1, p2

    #@36
    .line 299
    move p2, v7

    #@37
    .line 302
    .end local v7           #temp:I
    :cond_37
    iget-object v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@39
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaVideoItem;->getFilename()Ljava/lang/String;

    #@3c
    move-result-object v1

    #@3d
    iget v6, p0, Landroid/media/videoeditor/MediaVideoItem;->mVideoRotationDegree:I

    #@3f
    move v2, p1

    #@40
    move v3, p2

    #@41
    move-wide v4, p3

    #@42
    invoke-virtual/range {v0 .. v6}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getPixels(Ljava/lang/String;IIJI)Landroid/graphics/Bitmap;

    #@45
    move-result-object v0

    #@46
    return-object v0
.end method

.method public getThumbnailList(IIJJI[ILandroid/media/videoeditor/MediaItem$GetThumbnailListCallback;)V
    .registers 23
    .parameter "width"
    .parameter "height"
    .parameter "startMs"
    .parameter "endMs"
    .parameter "thumbnailCount"
    .parameter "indices"
    .parameter "callback"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 316
    cmp-long v0, p3, p5

    #@2
    if-lez v0, :cond_c

    #@4
    .line 317
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@6
    const-string v1, "Start time is greater than end time"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 320
    :cond_c
    iget-wide v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mDurationMs:J

    #@e
    cmp-long v0, p5, v0

    #@10
    if-lez v0, :cond_1a

    #@12
    .line 321
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@14
    const-string v1, "End time is greater than file duration"

    #@16
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@19
    throw v0

    #@1a
    .line 324
    :cond_1a
    if-lez p2, :cond_1e

    #@1c
    if-gtz p1, :cond_26

    #@1e
    .line 325
    :cond_1e
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@20
    const-string v1, "Invalid dimension"

    #@22
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@25
    throw v0

    #@26
    .line 328
    :cond_26
    iget v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mVideoRotationDegree:I

    #@28
    const/16 v1, 0x5a

    #@2a
    if-eq v0, v1, :cond_32

    #@2c
    iget v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mVideoRotationDegree:I

    #@2e
    const/16 v1, 0x10e

    #@30
    if-ne v0, v1, :cond_35

    #@32
    .line 329
    :cond_32
    move v12, p1

    #@33
    .line 330
    .local v12, temp:I
    move p1, p2

    #@34
    .line 331
    move p2, v12

    #@35
    .line 334
    .end local v12           #temp:I
    :cond_35
    iget-object v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@37
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaVideoItem;->getFilename()Ljava/lang/String;

    #@3a
    move-result-object v1

    #@3b
    iget v11, p0, Landroid/media/videoeditor/MediaVideoItem;->mVideoRotationDegree:I

    #@3d
    move v2, p1

    #@3e
    move v3, p2

    #@3f
    move-wide/from16 v4, p3

    #@41
    move-wide/from16 v6, p5

    #@43
    move/from16 v8, p7

    #@45
    move-object/from16 v9, p8

    #@47
    move-object/from16 v10, p9

    #@49
    invoke-virtual/range {v0 .. v11}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getPixelsList(Ljava/lang/String;IIJJI[ILandroid/media/videoeditor/MediaItem$GetThumbnailListCallback;I)V

    #@4c
    .line 337
    return-void
.end method

.method public getTimelineDuration()J
    .registers 5

    #@0
    .prologue
    .line 478
    iget-wide v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mEndBoundaryTimeMs:J

    #@2
    iget-wide v2, p0, Landroid/media/videoeditor/MediaVideoItem;->mBeginBoundaryTimeMs:J

    #@4
    sub-long/2addr v0, v2

    #@5
    return-wide v0
.end method

.method public getVideoBitrate()I
    .registers 2

    #@0
    .prologue
    .line 705
    iget v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mVideoBitrate:I

    #@2
    return v0
.end method

.method getVideoClipProperties()Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;
    .registers 4

    #@0
    .prologue
    .line 748
    new-instance v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@2
    invoke-direct {v0}, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;-><init>()V

    #@5
    .line 749
    .local v0, clipSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaVideoItem;->getFilename()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    iput-object v1, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipPath:Ljava/lang/String;

    #@b
    .line 750
    iget-object v1, p0, Landroid/media/videoeditor/MediaVideoItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@d
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaVideoItem;->getFileType()I

    #@10
    move-result v2

    #@11
    invoke-virtual {v1, v2}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getMediaItemFileType(I)I

    #@14
    move-result v1

    #@15
    iput v1, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->fileType:I

    #@17
    .line 751
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaVideoItem;->getBoundaryBeginTime()J

    #@1a
    move-result-wide v1

    #@1b
    long-to-int v1, v1

    #@1c
    iput v1, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->beginCutTime:I

    #@1e
    .line 752
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaVideoItem;->getBoundaryEndTime()J

    #@21
    move-result-wide v1

    #@22
    long-to-int v1, v1

    #@23
    iput v1, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutTime:I

    #@25
    .line 753
    iget-object v1, p0, Landroid/media/videoeditor/MediaVideoItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@27
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaVideoItem;->getRenderingMode()I

    #@2a
    move-result v2

    #@2b
    invoke-virtual {v1, v2}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getMediaItemRenderingMode(I)I

    #@2e
    move-result v1

    #@2f
    iput v1, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->mediaRendering:I

    #@31
    .line 754
    iget v1, p0, Landroid/media/videoeditor/MediaVideoItem;->mVideoRotationDegree:I

    #@33
    iput v1, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->rotationDegree:I

    #@35
    .line 756
    return-object v0
.end method

.method public getVideoLevel()I
    .registers 2

    #@0
    .prologue
    .line 698
    iget v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mVideoLevel:I

    #@2
    return v0
.end method

.method public getVideoProfile()I
    .registers 2

    #@0
    .prologue
    .line 691
    iget v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mVideoProfile:I

    #@2
    return v0
.end method

.method public getVideoType()I
    .registers 2

    #@0
    .prologue
    .line 684
    iget v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mVideoType:I

    #@2
    return v0
.end method

.method public getVolume()I
    .registers 2

    #@0
    .prologue
    .line 656
    iget v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mVolumePercentage:I

    #@2
    return v0
.end method

.method public getWaveformData()Landroid/media/videoeditor/WaveformData;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 614
    iget-object v3, p0, Landroid/media/videoeditor/MediaVideoItem;->mWaveformData:Ljava/lang/ref/SoftReference;

    #@3
    if-nez v3, :cond_7

    #@5
    move-object v1, v2

    #@6
    .line 630
    :cond_6
    :goto_6
    return-object v1

    #@7
    .line 618
    :cond_7
    iget-object v3, p0, Landroid/media/videoeditor/MediaVideoItem;->mWaveformData:Ljava/lang/ref/SoftReference;

    #@9
    invoke-virtual {v3}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    #@c
    move-result-object v1

    #@d
    check-cast v1, Landroid/media/videoeditor/WaveformData;

    #@f
    .line 619
    .local v1, waveformData:Landroid/media/videoeditor/WaveformData;
    if-nez v1, :cond_6

    #@11
    .line 621
    iget-object v3, p0, Landroid/media/videoeditor/MediaVideoItem;->mAudioWaveformFilename:Ljava/lang/String;

    #@13
    if-eqz v3, :cond_26

    #@15
    .line 623
    :try_start_15
    new-instance v1, Landroid/media/videoeditor/WaveformData;

    #@17
    .end local v1           #waveformData:Landroid/media/videoeditor/WaveformData;
    iget-object v2, p0, Landroid/media/videoeditor/MediaVideoItem;->mAudioWaveformFilename:Ljava/lang/String;

    #@19
    invoke-direct {v1, v2}, Landroid/media/videoeditor/WaveformData;-><init>(Ljava/lang/String;)V
    :try_end_1c
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_1c} :catch_24

    #@1c
    .line 627
    .restart local v1       #waveformData:Landroid/media/videoeditor/WaveformData;
    new-instance v2, Ljava/lang/ref/SoftReference;

    #@1e
    invoke-direct {v2, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    #@21
    iput-object v2, p0, Landroid/media/videoeditor/MediaVideoItem;->mWaveformData:Ljava/lang/ref/SoftReference;

    #@23
    goto :goto_6

    #@24
    .line 624
    .end local v1           #waveformData:Landroid/media/videoeditor/WaveformData;
    :catch_24
    move-exception v0

    #@25
    .line 625
    .local v0, e:Ljava/io/IOException;
    throw v0

    #@26
    .end local v0           #e:Ljava/io/IOException;
    .restart local v1       #waveformData:Landroid/media/videoeditor/WaveformData;
    :cond_26
    move-object v1, v2

    #@27
    .line 630
    goto :goto_6
.end method

.method public getWidth()I
    .registers 3

    #@0
    .prologue
    .line 444
    iget v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mVideoRotationDegree:I

    #@2
    const/16 v1, 0x5a

    #@4
    if-eq v0, v1, :cond_c

    #@6
    iget v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mVideoRotationDegree:I

    #@8
    const/16 v1, 0x10e

    #@a
    if-ne v0, v1, :cond_f

    #@c
    .line 446
    :cond_c
    iget v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mHeight:I

    #@e
    .line 448
    :goto_e
    return v0

    #@f
    :cond_f
    iget v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mWidth:I

    #@11
    goto :goto_e
.end method

.method invalidate()V
    .registers 3

    #@0
    .prologue
    .line 604
    iget-object v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mAudioWaveformFilename:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_11

    #@4
    .line 605
    new-instance v0, Ljava/io/File;

    #@6
    iget-object v1, p0, Landroid/media/videoeditor/MediaVideoItem;->mAudioWaveformFilename:Ljava/lang/String;

    #@8
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@b
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@e
    .line 606
    const/4 v0, 0x0

    #@f
    iput-object v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mAudioWaveformFilename:Ljava/lang/String;

    #@11
    .line 608
    :cond_11
    return-void
.end method

.method invalidateTransitions(JJ)V
    .registers 14
    .parameter "startTimeMs"
    .parameter "durationMs"

    #@0
    .prologue
    .line 347
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@2
    if-eqz v0, :cond_1a

    #@4
    .line 348
    iget-wide v5, p0, Landroid/media/videoeditor/MediaVideoItem;->mBeginBoundaryTimeMs:J

    #@6
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@8
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@b
    move-result-wide v7

    #@c
    move-object v0, p0

    #@d
    move-wide v1, p1

    #@e
    move-wide v3, p3

    #@f
    invoke-virtual/range {v0 .. v8}, Landroid/media/videoeditor/MediaVideoItem;->isOverlapping(JJJJ)Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_1a

    #@15
    .line 350
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@17
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@1a
    .line 354
    :cond_1a
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@1c
    if-eqz v0, :cond_36

    #@1e
    .line 355
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@20
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@23
    move-result-wide v7

    #@24
    .line 356
    .local v7, transitionDurationMs:J
    iget-wide v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mEndBoundaryTimeMs:J

    #@26
    sub-long v5, v0, v7

    #@28
    move-object v0, p0

    #@29
    move-wide v1, p1

    #@2a
    move-wide v3, p3

    #@2b
    invoke-virtual/range {v0 .. v8}, Landroid/media/videoeditor/MediaVideoItem;->isOverlapping(JJJJ)Z

    #@2e
    move-result v0

    #@2f
    if-eqz v0, :cond_36

    #@31
    .line 358
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@33
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@36
    .line 361
    .end local v7           #transitionDurationMs:J
    :cond_36
    return-void
.end method

.method invalidateTransitions(JJJJ)V
    .registers 20
    .parameter "oldStartTimeMs"
    .parameter "oldDurationMs"
    .parameter "newStartTimeMs"
    .parameter "newDurationMs"

    #@0
    .prologue
    .line 372
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@2
    if-eqz v0, :cond_25

    #@4
    .line 373
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@6
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@9
    move-result-wide v7

    #@a
    .line 374
    .local v7, transitionDurationMs:J
    iget-wide v5, p0, Landroid/media/videoeditor/MediaVideoItem;->mBeginBoundaryTimeMs:J

    #@c
    move-object v0, p0

    #@d
    move-wide v1, p1

    #@e
    move-wide v3, p3

    #@f
    invoke-virtual/range {v0 .. v8}, Landroid/media/videoeditor/MediaVideoItem;->isOverlapping(JJJJ)Z

    #@12
    move-result v10

    #@13
    .line 376
    .local v10, oldOverlap:Z
    iget-wide v5, p0, Landroid/media/videoeditor/MediaVideoItem;->mBeginBoundaryTimeMs:J

    #@15
    move-object v0, p0

    #@16
    move-wide/from16 v1, p5

    #@18
    move-wide/from16 v3, p7

    #@1a
    invoke-virtual/range {v0 .. v8}, Landroid/media/videoeditor/MediaVideoItem;->isOverlapping(JJJJ)Z

    #@1d
    move-result v9

    #@1e
    .line 386
    .local v9, newOverlap:Z
    if-eq v9, v10, :cond_4f

    #@20
    .line 387
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@22
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@25
    .line 397
    .end local v7           #transitionDurationMs:J
    .end local v9           #newOverlap:Z
    .end local v10           #oldOverlap:Z
    :cond_25
    :goto_25
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@27
    if-eqz v0, :cond_4e

    #@29
    .line 398
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@2b
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@2e
    move-result-wide v7

    #@2f
    .line 399
    .restart local v7       #transitionDurationMs:J
    iget-wide v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mEndBoundaryTimeMs:J

    #@31
    sub-long v5, v0, v7

    #@33
    move-object v0, p0

    #@34
    move-wide v1, p1

    #@35
    move-wide v3, p3

    #@36
    invoke-virtual/range {v0 .. v8}, Landroid/media/videoeditor/MediaVideoItem;->isOverlapping(JJJJ)Z

    #@39
    move-result v10

    #@3a
    .line 401
    .restart local v10       #oldOverlap:Z
    iget-wide v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mEndBoundaryTimeMs:J

    #@3c
    sub-long v5, v0, v7

    #@3e
    move-object v0, p0

    #@3f
    move-wide/from16 v1, p5

    #@41
    move-wide/from16 v3, p7

    #@43
    invoke-virtual/range {v0 .. v8}, Landroid/media/videoeditor/MediaVideoItem;->isOverlapping(JJJJ)Z

    #@46
    move-result v9

    #@47
    .line 411
    .restart local v9       #newOverlap:Z
    if-eq v9, v10, :cond_67

    #@49
    .line 412
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@4b
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@4e
    .line 421
    .end local v7           #transitionDurationMs:J
    .end local v9           #newOverlap:Z
    .end local v10           #oldOverlap:Z
    :cond_4e
    :goto_4e
    return-void

    #@4f
    .line 388
    .restart local v7       #transitionDurationMs:J
    .restart local v9       #newOverlap:Z
    .restart local v10       #oldOverlap:Z
    :cond_4f
    if-eqz v9, :cond_25

    #@51
    .line 389
    cmp-long v0, p1, p5

    #@53
    if-nez v0, :cond_61

    #@55
    add-long v0, p1, p3

    #@57
    cmp-long v0, v0, v7

    #@59
    if-lez v0, :cond_61

    #@5b
    add-long v0, p5, p7

    #@5d
    cmp-long v0, v0, v7

    #@5f
    if-gtz v0, :cond_25

    #@61
    .line 392
    :cond_61
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@63
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@66
    goto :goto_25

    #@67
    .line 413
    :cond_67
    if-eqz v9, :cond_4e

    #@69
    .line 414
    add-long v0, p1, p3

    #@6b
    add-long v2, p5, p7

    #@6d
    cmp-long v0, v0, v2

    #@6f
    if-nez v0, :cond_7f

    #@71
    iget-wide v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mEndBoundaryTimeMs:J

    #@73
    sub-long/2addr v0, v7

    #@74
    cmp-long v0, p1, v0

    #@76
    if-gtz v0, :cond_7f

    #@78
    iget-wide v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mEndBoundaryTimeMs:J

    #@7a
    sub-long/2addr v0, v7

    #@7b
    cmp-long v0, p5, v0

    #@7d
    if-lez v0, :cond_4e

    #@7f
    .line 417
    :cond_7f
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@81
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@84
    goto :goto_4e
.end method

.method public isMuted()Z
    .registers 2

    #@0
    .prologue
    .line 677
    iget-boolean v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mMuted:Z

    #@2
    return v0
.end method

.method public renderFrame(Landroid/view/SurfaceHolder;J)J
    .registers 11
    .parameter "surfaceHolder"
    .parameter "timeMs"

    #@0
    .prologue
    const-wide/16 v2, 0x0

    #@2
    .line 497
    if-nez p1, :cond_c

    #@4
    .line 498
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@6
    const-string v2, "Surface Holder is null"

    #@8
    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 501
    :cond_c
    iget-wide v4, p0, Landroid/media/videoeditor/MediaVideoItem;->mDurationMs:J

    #@e
    cmp-long v0, p2, v4

    #@10
    if-gtz v0, :cond_16

    #@12
    cmp-long v0, p2, v2

    #@14
    if-gez v0, :cond_1f

    #@16
    .line 502
    :cond_16
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@18
    const-string/jumbo v2, "requested time not correct"

    #@1b
    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v0

    #@1f
    .line 505
    :cond_1f
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    #@22
    move-result-object v1

    #@23
    .line 506
    .local v1, surface:Landroid/view/Surface;
    if-nez v1, :cond_2d

    #@25
    .line 507
    new-instance v0, Ljava/lang/RuntimeException;

    #@27
    const-string v2, "Surface could not be retrieved from Surface holder"

    #@29
    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v0

    #@2d
    .line 510
    :cond_2d
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mFilename:Ljava/lang/String;

    #@2f
    if-eqz v0, :cond_3e

    #@31
    .line 511
    iget-object v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@33
    iget-object v2, p0, Landroid/media/videoeditor/MediaItem;->mFilename:Ljava/lang/String;

    #@35
    iget v5, p0, Landroid/media/videoeditor/MediaVideoItem;->mWidth:I

    #@37
    iget v6, p0, Landroid/media/videoeditor/MediaVideoItem;->mHeight:I

    #@39
    move-wide v3, p2

    #@3a
    invoke-virtual/range {v0 .. v6}, Landroid/media/videoeditor/MediaArtistNativeHelper;->renderMediaItemPreviewFrame(Landroid/view/Surface;Ljava/lang/String;JII)J

    #@3d
    move-result-wide v2

    #@3e
    .line 514
    :cond_3e
    return-wide v2
.end method

.method public setExtractBoundaries(JJ)V
    .registers 11
    .parameter "beginMs"
    .parameter "endMs"

    #@0
    .prologue
    const-wide/16 v4, 0x0

    #@2
    const-wide/16 v2, -0x1

    #@4
    .line 213
    iget-wide v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mDurationMs:J

    #@6
    cmp-long v0, p1, v0

    #@8
    if-lez v0, :cond_13

    #@a
    .line 214
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@c
    const-string/jumbo v1, "setExtractBoundaries: Invalid start time"

    #@f
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@12
    throw v0

    #@13
    .line 217
    :cond_13
    iget-wide v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mDurationMs:J

    #@15
    cmp-long v0, p3, v0

    #@17
    if-lez v0, :cond_22

    #@19
    .line 218
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1b
    const-string/jumbo v1, "setExtractBoundaries: Invalid end time"

    #@1e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@21
    throw v0

    #@22
    .line 221
    :cond_22
    cmp-long v0, p3, v2

    #@24
    if-eqz v0, :cond_33

    #@26
    cmp-long v0, p1, p3

    #@28
    if-ltz v0, :cond_33

    #@2a
    .line 222
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@2c
    const-string/jumbo v1, "setExtractBoundaries: Start time is greater than end time"

    #@2f
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@32
    throw v0

    #@33
    .line 225
    :cond_33
    cmp-long v0, p1, v4

    #@35
    if-ltz v0, :cond_3f

    #@37
    cmp-long v0, p3, v2

    #@39
    if-eqz v0, :cond_48

    #@3b
    cmp-long v0, p3, v4

    #@3d
    if-gez v0, :cond_48

    #@3f
    .line 226
    :cond_3f
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@41
    const-string/jumbo v1, "setExtractBoundaries: Start time or end time is negative"

    #@44
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@47
    throw v0

    #@48
    .line 229
    :cond_48
    iget-object v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@4a
    const/4 v1, 0x1

    #@4b
    invoke-virtual {v0, v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@4e
    .line 231
    iget-wide v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mBeginBoundaryTimeMs:J

    #@50
    cmp-long v0, p1, v0

    #@52
    if-eqz v0, :cond_5d

    #@54
    .line 232
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@56
    if-eqz v0, :cond_5d

    #@58
    .line 233
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@5a
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@5d
    .line 237
    :cond_5d
    iget-wide v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mEndBoundaryTimeMs:J

    #@5f
    cmp-long v0, p3, v0

    #@61
    if-eqz v0, :cond_6c

    #@63
    .line 238
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@65
    if-eqz v0, :cond_6c

    #@67
    .line 239
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@69
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@6c
    .line 243
    :cond_6c
    iput-wide p1, p0, Landroid/media/videoeditor/MediaVideoItem;->mBeginBoundaryTimeMs:J

    #@6e
    .line 244
    iput-wide p3, p0, Landroid/media/videoeditor/MediaVideoItem;->mEndBoundaryTimeMs:J

    #@70
    .line 245
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaVideoItem;->adjustTransitions()V

    #@73
    .line 246
    iget-object v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mVideoEditor:Landroid/media/videoeditor/VideoEditorImpl;

    #@75
    invoke-virtual {v0}, Landroid/media/videoeditor/VideoEditorImpl;->updateTimelineDuration()V

    #@78
    .line 252
    return-void
.end method

.method public setMute(Z)V
    .registers 4
    .parameter "muted"

    #@0
    .prologue
    .line 663
    iget-object v0, p0, Landroid/media/videoeditor/MediaVideoItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {v0, v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@6
    .line 664
    iput-boolean p1, p0, Landroid/media/videoeditor/MediaVideoItem;->mMuted:Z

    #@8
    .line 665
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@a
    if-eqz v0, :cond_11

    #@c
    .line 666
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@e
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@11
    .line 668
    :cond_11
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@13
    if-eqz v0, :cond_1a

    #@15
    .line 669
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@17
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@1a
    .line 671
    :cond_1a
    return-void
.end method

.method public setVolume(I)V
    .registers 4
    .parameter "volumePercent"

    #@0
    .prologue
    .line 642
    if-ltz p1, :cond_6

    #@2
    const/16 v0, 0x64

    #@4
    if-le p1, v0, :cond_e

    #@6
    .line 643
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v1, "Invalid volume"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 646
    :cond_e
    iput p1, p0, Landroid/media/videoeditor/MediaVideoItem;->mVolumePercentage:I

    #@10
    .line 647
    return-void
.end method
