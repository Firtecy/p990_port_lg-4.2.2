.class public Landroid/media/videoeditor/OverlayFrame;
.super Landroid/media/videoeditor/Overlay;
.source "OverlayFrame.java"


# static fields
.field private static final sResizePaint:Landroid/graphics/Paint;


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mBitmapFileName:Ljava/lang/String;

.field private mFilename:Ljava/lang/String;

.field private mOFHeight:I

.field private mOFWidth:I

.field private mResizedRGBHeight:I

.field private mResizedRGBWidth:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 61
    new-instance v0, Landroid/graphics/Paint;

    #@2
    const/4 v1, 0x2

    #@3
    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    #@6
    sput-object v0, Landroid/media/videoeditor/OverlayFrame;->sResizePaint:Landroid/graphics/Paint;

    #@8
    return-void
.end method

.method private constructor <init>()V
    .registers 9

    #@0
    .prologue
    const-wide/16 v4, 0x0

    #@2
    const/4 v1, 0x0

    #@3
    .line 69
    move-object v3, v1

    #@4
    check-cast v3, Ljava/lang/String;

    #@6
    move-object v0, p0

    #@7
    move-object v2, v1

    #@8
    move-wide v6, v4

    #@9
    invoke-direct/range {v0 .. v7}, Landroid/media/videoeditor/OverlayFrame;-><init>(Landroid/media/videoeditor/MediaItem;Ljava/lang/String;Ljava/lang/String;JJ)V

    #@c
    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/media/videoeditor/MediaItem;Ljava/lang/String;Landroid/graphics/Bitmap;JJ)V
    .registers 15
    .parameter "mediaItem"
    .parameter "overlayId"
    .parameter "bitmap"
    .parameter "startTimeMs"
    .parameter "durationMs"

    #@0
    .prologue
    .line 88
    move-object v0, p0

    #@1
    move-object v1, p1

    #@2
    move-object v2, p2

    #@3
    move-wide v3, p4

    #@4
    move-wide v5, p6

    #@5
    invoke-direct/range {v0 .. v6}, Landroid/media/videoeditor/Overlay;-><init>(Landroid/media/videoeditor/MediaItem;Ljava/lang/String;JJ)V

    #@8
    .line 89
    iput-object p3, p0, Landroid/media/videoeditor/OverlayFrame;->mBitmap:Landroid/graphics/Bitmap;

    #@a
    .line 90
    const/4 v0, 0x0

    #@b
    iput-object v0, p0, Landroid/media/videoeditor/OverlayFrame;->mFilename:Ljava/lang/String;

    #@d
    .line 91
    const/4 v0, 0x0

    #@e
    iput-object v0, p0, Landroid/media/videoeditor/OverlayFrame;->mBitmapFileName:Ljava/lang/String;

    #@10
    .line 92
    const/4 v0, 0x0

    #@11
    iput v0, p0, Landroid/media/videoeditor/OverlayFrame;->mResizedRGBWidth:I

    #@13
    .line 93
    const/4 v0, 0x0

    #@14
    iput v0, p0, Landroid/media/videoeditor/OverlayFrame;->mResizedRGBHeight:I

    #@16
    .line 94
    return-void
.end method

.method constructor <init>(Landroid/media/videoeditor/MediaItem;Ljava/lang/String;Ljava/lang/String;JJ)V
    .registers 16
    .parameter "mediaItem"
    .parameter "overlayId"
    .parameter "filename"
    .parameter "startTimeMs"
    .parameter "durationMs"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 111
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move-object v2, p2

    #@4
    move-wide v3, p4

    #@5
    move-wide v5, p6

    #@6
    invoke-direct/range {v0 .. v6}, Landroid/media/videoeditor/Overlay;-><init>(Landroid/media/videoeditor/MediaItem;Ljava/lang/String;JJ)V

    #@9
    .line 112
    iput-object p3, p0, Landroid/media/videoeditor/OverlayFrame;->mBitmapFileName:Ljava/lang/String;

    #@b
    .line 113
    iget-object v0, p0, Landroid/media/videoeditor/OverlayFrame;->mBitmapFileName:Ljava/lang/String;

    #@d
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    #@10
    move-result-object v0

    #@11
    iput-object v0, p0, Landroid/media/videoeditor/OverlayFrame;->mBitmap:Landroid/graphics/Bitmap;

    #@13
    .line 114
    const/4 v0, 0x0

    #@14
    iput-object v0, p0, Landroid/media/videoeditor/OverlayFrame;->mFilename:Ljava/lang/String;

    #@16
    .line 115
    iput v7, p0, Landroid/media/videoeditor/OverlayFrame;->mResizedRGBWidth:I

    #@18
    .line 116
    iput v7, p0, Landroid/media/videoeditor/OverlayFrame;->mResizedRGBHeight:I

    #@1a
    .line 117
    return-void
.end method


# virtual methods
.method generateOverlayWithRenderingMode(Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/OverlayFrame;II)V
    .registers 39
    .parameter "mediaItemsList"
    .parameter "overlay"
    .parameter "height"
    .parameter "width"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 313
    move-object/from16 v32, p1

    #@2
    .line 316
    .local v32, t:Landroid/media/videoeditor/MediaItem;
    invoke-virtual/range {v32 .. v32}, Landroid/media/videoeditor/MediaItem;->getRenderingMode()I

    #@5
    move-result v27

    #@6
    .line 318
    .local v27, renderMode:I
    invoke-virtual/range {p2 .. p2}, Landroid/media/videoeditor/OverlayFrame;->getBitmap()Landroid/graphics/Bitmap;

    #@9
    move-result-object v25

    #@a
    .line 324
    .local v25, overlayBitmap:Landroid/graphics/Bitmap;
    invoke-virtual/range {p2 .. p2}, Landroid/media/videoeditor/OverlayFrame;->getResizedRGBSizeHeight()I

    #@d
    move-result v28

    #@e
    .line 325
    .local v28, resizedRGBFileHeight:I
    invoke-virtual/range {p2 .. p2}, Landroid/media/videoeditor/OverlayFrame;->getResizedRGBSizeWidth()I

    #@11
    move-result v29

    #@12
    .line 328
    .local v29, resizedRGBFileWidth:I
    if-nez v29, :cond_18

    #@14
    .line 329
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Bitmap;->getWidth()I

    #@17
    move-result v29

    #@18
    .line 332
    :cond_18
    if-nez v28, :cond_1e

    #@1a
    .line 333
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Bitmap;->getHeight()I

    #@1d
    move-result v28

    #@1e
    .line 336
    :cond_1e
    move/from16 v0, v29

    #@20
    move/from16 v1, p4

    #@22
    if-ne v0, v1, :cond_39

    #@24
    move/from16 v0, v28

    #@26
    move/from16 v1, p3

    #@28
    if-ne v0, v1, :cond_39

    #@2a
    new-instance v6, Ljava/io/File;

    #@2c
    invoke-virtual/range {p2 .. p2}, Landroid/media/videoeditor/OverlayFrame;->getFilename()Ljava/lang/String;

    #@2f
    move-result-object v7

    #@30
    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@33
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    #@36
    move-result v6

    #@37
    if-nez v6, :cond_1f2

    #@39
    .line 341
    :cond_39
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@3b
    move/from16 v0, p4

    #@3d
    move/from16 v1, p3

    #@3f
    invoke-static {v0, v1, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    #@42
    move-result-object v4

    #@43
    .line 344
    .local v4, destBitmap:Landroid/graphics/Bitmap;
    new-instance v26, Landroid/graphics/Canvas;

    #@45
    move-object/from16 v0, v26

    #@47
    invoke-direct {v0, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    #@4a
    .line 348
    .local v26, overlayCanvas:Landroid/graphics/Canvas;
    packed-switch v27, :pswitch_data_1f4

    #@4d
    .line 418
    new-instance v6, Ljava/lang/IllegalStateException;

    #@4f
    new-instance v7, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    const-string v8, "Rendering mode: "

    #@56
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v7

    #@5a
    move/from16 v0, v27

    #@5c
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v7

    #@60
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v7

    #@64
    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@67
    throw v6

    #@68
    .line 350
    :pswitch_68
    new-instance v17, Landroid/graphics/Rect;

    #@6a
    const/4 v6, 0x0

    #@6b
    const/4 v7, 0x0

    #@6c
    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Canvas;->getWidth()I

    #@6f
    move-result v8

    #@70
    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Canvas;->getHeight()I

    #@73
    move-result v10

    #@74
    move-object/from16 v0, v17

    #@76
    invoke-direct {v0, v6, v7, v8, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    #@79
    .line 352
    .local v17, destRect:Landroid/graphics/Rect;
    new-instance v31, Landroid/graphics/Rect;

    #@7b
    const/4 v6, 0x0

    #@7c
    const/4 v7, 0x0

    #@7d
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Bitmap;->getWidth()I

    #@80
    move-result v8

    #@81
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Bitmap;->getHeight()I

    #@84
    move-result v10

    #@85
    move-object/from16 v0, v31

    #@87
    invoke-direct {v0, v6, v7, v8, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    #@8a
    .line 422
    .local v31, srcRect:Landroid/graphics/Rect;
    :goto_8a
    sget-object v6, Landroid/media/videoeditor/OverlayFrame;->sResizePaint:Landroid/graphics/Paint;

    #@8c
    move-object/from16 v0, v26

    #@8e
    move-object/from16 v1, v25

    #@90
    move-object/from16 v2, v31

    #@92
    move-object/from16 v3, v17

    #@94
    invoke-virtual {v0, v1, v2, v3, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    #@97
    .line 423
    const/4 v6, 0x0

    #@98
    move-object/from16 v0, v26

    #@9a
    invoke-virtual {v0, v6}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    #@9d
    .line 428
    invoke-virtual/range {p2 .. p2}, Landroid/media/videoeditor/OverlayFrame;->getFilename()Ljava/lang/String;

    #@a0
    move-result-object v24

    #@a1
    .line 433
    .local v24, outFileName:Ljava/lang/String;
    if-eqz v24, :cond_ad

    #@a3
    .line 434
    new-instance v6, Ljava/io/File;

    #@a5
    move-object/from16 v0, v24

    #@a7
    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@aa
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    #@ad
    .line 437
    :cond_ad
    new-instance v19, Ljava/io/FileOutputStream;

    #@af
    move-object/from16 v0, v19

    #@b1
    move-object/from16 v1, v24

    #@b3
    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    #@b6
    .line 438
    .local v19, fl:Ljava/io/FileOutputStream;
    new-instance v18, Ljava/io/DataOutputStream;

    #@b8
    invoke-direct/range {v18 .. v19}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@bb
    .line 443
    .local v18, dos:Ljava/io/DataOutputStream;
    move/from16 v0, p4

    #@bd
    new-array v5, v0, [I

    #@bf
    .line 444
    .local v5, framingBuffer:[I
    array-length v6, v5

    #@c0
    mul-int/lit8 v6, v6, 0x4

    #@c2
    invoke-static {v6}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@c5
    move-result-object v16

    #@c6
    .line 447
    .local v16, byteBuffer:Ljava/nio/ByteBuffer;
    invoke-virtual/range {v16 .. v16}, Ljava/nio/ByteBuffer;->array()[B

    #@c9
    move-result-object v14

    #@ca
    .line 448
    .local v14, array:[B
    const/4 v9, 0x0

    #@cb
    .line 449
    .local v9, tmp:I
    :goto_cb
    move/from16 v0, p3

    #@cd
    if-ge v9, v0, :cond_1e3

    #@cf
    .line 450
    const/4 v6, 0x0

    #@d0
    const/4 v8, 0x0

    #@d1
    const/4 v11, 0x1

    #@d2
    move/from16 v7, p4

    #@d4
    move/from16 v10, p4

    #@d6
    invoke-virtual/range {v4 .. v11}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    #@d9
    .line 451
    invoke-virtual/range {v16 .. v16}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    #@dc
    move-result-object v20

    #@dd
    .line 452
    .local v20, intBuffer:Ljava/nio/IntBuffer;
    const/4 v6, 0x0

    #@de
    move-object/from16 v0, v20

    #@e0
    move/from16 v1, p4

    #@e2
    invoke-virtual {v0, v5, v6, v1}, Ljava/nio/IntBuffer;->put([III)Ljava/nio/IntBuffer;

    #@e5
    .line 453
    move-object/from16 v0, v18

    #@e7
    invoke-virtual {v0, v14}, Ljava/io/DataOutputStream;->write([B)V

    #@ea
    .line 454
    add-int/lit8 v9, v9, 0x1

    #@ec
    goto :goto_cb

    #@ed
    .line 360
    .end local v5           #framingBuffer:[I
    .end local v9           #tmp:I
    .end local v14           #array:[B
    .end local v16           #byteBuffer:Ljava/nio/ByteBuffer;
    .end local v17           #destRect:Landroid/graphics/Rect;
    .end local v18           #dos:Ljava/io/DataOutputStream;
    .end local v19           #fl:Ljava/io/FileOutputStream;
    .end local v20           #intBuffer:Ljava/nio/IntBuffer;
    .end local v24           #outFileName:Ljava/lang/String;
    .end local v31           #srcRect:Landroid/graphics/Rect;
    :pswitch_ed
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Bitmap;->getWidth()I

    #@f0
    move-result v6

    #@f1
    int-to-float v6, v6

    #@f2
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Bitmap;->getHeight()I

    #@f5
    move-result v7

    #@f6
    int-to-float v7, v7

    #@f7
    div-float v13, v6, v7

    #@f9
    .line 363
    .local v13, aROverlayImage:F
    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Canvas;->getWidth()I

    #@fc
    move-result v6

    #@fd
    int-to-float v6, v6

    #@fe
    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Canvas;->getHeight()I

    #@101
    move-result v7

    #@102
    int-to-float v7, v7

    #@103
    div-float v12, v6, v7

    #@105
    .line 366
    .local v12, aRCanvas:F
    cmpl-float v6, v13, v12

    #@107
    if-lez v6, :cond_148

    #@109
    .line 367
    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Canvas;->getWidth()I

    #@10c
    move-result v6

    #@10d
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Bitmap;->getHeight()I

    #@110
    move-result v7

    #@111
    mul-int/2addr v6, v7

    #@112
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Bitmap;->getWidth()I

    #@115
    move-result v7

    #@116
    div-int v22, v6, v7

    #@118
    .line 369
    .local v22, newHeight:I
    const/16 v21, 0x0

    #@11a
    .line 370
    .local v21, left:I
    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Canvas;->getHeight()I

    #@11d
    move-result v6

    #@11e
    sub-int v6, v6, v22

    #@120
    div-int/lit8 v33, v6, 0x2

    #@122
    .line 371
    .local v33, top:I
    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Canvas;->getWidth()I

    #@125
    move-result v30

    #@126
    .line 372
    .local v30, right:I
    add-int v15, v33, v22

    #@128
    .line 382
    .end local v22           #newHeight:I
    .local v15, bottom:I
    :goto_128
    new-instance v17, Landroid/graphics/Rect;

    #@12a
    move-object/from16 v0, v17

    #@12c
    move/from16 v1, v21

    #@12e
    move/from16 v2, v33

    #@130
    move/from16 v3, v30

    #@132
    invoke-direct {v0, v1, v2, v3, v15}, Landroid/graphics/Rect;-><init>(IIII)V

    #@135
    .line 383
    .restart local v17       #destRect:Landroid/graphics/Rect;
    new-instance v31, Landroid/graphics/Rect;

    #@137
    const/4 v6, 0x0

    #@138
    const/4 v7, 0x0

    #@139
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Bitmap;->getWidth()I

    #@13c
    move-result v8

    #@13d
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Bitmap;->getHeight()I

    #@140
    move-result v10

    #@141
    move-object/from16 v0, v31

    #@143
    invoke-direct {v0, v6, v7, v8, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    #@146
    .line 384
    .restart local v31       #srcRect:Landroid/graphics/Rect;
    goto/16 :goto_8a

    #@148
    .line 374
    .end local v15           #bottom:I
    .end local v17           #destRect:Landroid/graphics/Rect;
    .end local v21           #left:I
    .end local v30           #right:I
    .end local v31           #srcRect:Landroid/graphics/Rect;
    .end local v33           #top:I
    :cond_148
    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Canvas;->getHeight()I

    #@14b
    move-result v6

    #@14c
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Bitmap;->getWidth()I

    #@14f
    move-result v7

    #@150
    mul-int/2addr v6, v7

    #@151
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Bitmap;->getHeight()I

    #@154
    move-result v7

    #@155
    div-int v23, v6, v7

    #@157
    .line 376
    .local v23, newWidth:I
    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Canvas;->getWidth()I

    #@15a
    move-result v6

    #@15b
    sub-int v6, v6, v23

    #@15d
    div-int/lit8 v21, v6, 0x2

    #@15f
    .line 377
    .restart local v21       #left:I
    const/16 v33, 0x0

    #@161
    .line 378
    .restart local v33       #top:I
    add-int v30, v21, v23

    #@163
    .line 379
    .restart local v30       #right:I
    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Canvas;->getHeight()I

    #@166
    move-result v15

    #@167
    .restart local v15       #bottom:I
    goto :goto_128

    #@168
    .line 391
    .end local v12           #aRCanvas:F
    .end local v13           #aROverlayImage:F
    .end local v15           #bottom:I
    .end local v21           #left:I
    .end local v23           #newWidth:I
    .end local v30           #right:I
    .end local v33           #top:I
    :pswitch_168
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Bitmap;->getWidth()I

    #@16b
    move-result v6

    #@16c
    int-to-float v6, v6

    #@16d
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Bitmap;->getHeight()I

    #@170
    move-result v7

    #@171
    int-to-float v7, v7

    #@172
    div-float v13, v6, v7

    #@174
    .line 393
    .restart local v13       #aROverlayImage:F
    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Canvas;->getWidth()I

    #@177
    move-result v6

    #@178
    int-to-float v6, v6

    #@179
    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Canvas;->getHeight()I

    #@17c
    move-result v7

    #@17d
    int-to-float v7, v7

    #@17e
    div-float v12, v6, v7

    #@180
    .line 395
    .restart local v12       #aRCanvas:F
    cmpg-float v6, v13, v12

    #@182
    if-gez v6, :cond_1c3

    #@184
    .line 396
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Bitmap;->getWidth()I

    #@187
    move-result v6

    #@188
    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Canvas;->getHeight()I

    #@18b
    move-result v7

    #@18c
    mul-int/2addr v6, v7

    #@18d
    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Canvas;->getWidth()I

    #@190
    move-result v7

    #@191
    div-int v22, v6, v7

    #@193
    .line 399
    .restart local v22       #newHeight:I
    const/16 v21, 0x0

    #@195
    .line 400
    .restart local v21       #left:I
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Bitmap;->getHeight()I

    #@198
    move-result v6

    #@199
    sub-int v6, v6, v22

    #@19b
    div-int/lit8 v33, v6, 0x2

    #@19d
    .line 401
    .restart local v33       #top:I
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Bitmap;->getWidth()I

    #@1a0
    move-result v30

    #@1a1
    .line 402
    .restart local v30       #right:I
    add-int v15, v33, v22

    #@1a3
    .line 412
    .end local v22           #newHeight:I
    .restart local v15       #bottom:I
    :goto_1a3
    new-instance v31, Landroid/graphics/Rect;

    #@1a5
    move-object/from16 v0, v31

    #@1a7
    move/from16 v1, v21

    #@1a9
    move/from16 v2, v33

    #@1ab
    move/from16 v3, v30

    #@1ad
    invoke-direct {v0, v1, v2, v3, v15}, Landroid/graphics/Rect;-><init>(IIII)V

    #@1b0
    .line 413
    .restart local v31       #srcRect:Landroid/graphics/Rect;
    new-instance v17, Landroid/graphics/Rect;

    #@1b2
    const/4 v6, 0x0

    #@1b3
    const/4 v7, 0x0

    #@1b4
    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Canvas;->getWidth()I

    #@1b7
    move-result v8

    #@1b8
    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Canvas;->getHeight()I

    #@1bb
    move-result v10

    #@1bc
    move-object/from16 v0, v17

    #@1be
    invoke-direct {v0, v6, v7, v8, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    #@1c1
    .line 414
    .restart local v17       #destRect:Landroid/graphics/Rect;
    goto/16 :goto_8a

    #@1c3
    .line 404
    .end local v15           #bottom:I
    .end local v17           #destRect:Landroid/graphics/Rect;
    .end local v21           #left:I
    .end local v30           #right:I
    .end local v31           #srcRect:Landroid/graphics/Rect;
    .end local v33           #top:I
    :cond_1c3
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Bitmap;->getHeight()I

    #@1c6
    move-result v6

    #@1c7
    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Canvas;->getWidth()I

    #@1ca
    move-result v7

    #@1cb
    mul-int/2addr v6, v7

    #@1cc
    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Canvas;->getHeight()I

    #@1cf
    move-result v7

    #@1d0
    div-int v23, v6, v7

    #@1d2
    .line 406
    .restart local v23       #newWidth:I
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Bitmap;->getWidth()I

    #@1d5
    move-result v6

    #@1d6
    sub-int v6, v6, v23

    #@1d8
    div-int/lit8 v21, v6, 0x2

    #@1da
    .line 407
    .restart local v21       #left:I
    const/16 v33, 0x0

    #@1dc
    .line 408
    .restart local v33       #top:I
    add-int v30, v21, v23

    #@1de
    .line 409
    .restart local v30       #right:I
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Bitmap;->getHeight()I

    #@1e1
    move-result v15

    #@1e2
    .restart local v15       #bottom:I
    goto :goto_1a3

    #@1e3
    .line 456
    .end local v12           #aRCanvas:F
    .end local v13           #aROverlayImage:F
    .end local v15           #bottom:I
    .end local v21           #left:I
    .end local v23           #newWidth:I
    .end local v30           #right:I
    .end local v33           #top:I
    .restart local v5       #framingBuffer:[I
    .restart local v9       #tmp:I
    .restart local v14       #array:[B
    .restart local v16       #byteBuffer:Ljava/nio/ByteBuffer;
    .restart local v17       #destRect:Landroid/graphics/Rect;
    .restart local v18       #dos:Ljava/io/DataOutputStream;
    .restart local v19       #fl:Ljava/io/FileOutputStream;
    .restart local v24       #outFileName:Ljava/lang/String;
    .restart local v31       #srcRect:Landroid/graphics/Rect;
    :cond_1e3
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->flush()V

    #@1e6
    .line 457
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V

    #@1e9
    .line 462
    move-object/from16 v0, p2

    #@1eb
    move/from16 v1, p4

    #@1ed
    move/from16 v2, p3

    #@1ef
    invoke-virtual {v0, v1, v2}, Landroid/media/videoeditor/OverlayFrame;->setResizedRGBSize(II)V

    #@1f2
    .line 464
    .end local v4           #destBitmap:Landroid/graphics/Bitmap;
    .end local v5           #framingBuffer:[I
    .end local v9           #tmp:I
    .end local v14           #array:[B
    .end local v16           #byteBuffer:Ljava/nio/ByteBuffer;
    .end local v17           #destRect:Landroid/graphics/Rect;
    .end local v18           #dos:Ljava/io/DataOutputStream;
    .end local v19           #fl:Ljava/io/FileOutputStream;
    .end local v24           #outFileName:Ljava/lang/String;
    .end local v26           #overlayCanvas:Landroid/graphics/Canvas;
    .end local v31           #srcRect:Landroid/graphics/Rect;
    :cond_1f2
    return-void

    #@1f3
    .line 348
    nop

    #@1f4
    :pswitch_data_1f4
    .packed-switch 0x0
        :pswitch_ed
        :pswitch_68
        :pswitch_168
    .end packed-switch
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .registers 2

    #@0
    .prologue
    .line 125
    iget-object v0, p0, Landroid/media/videoeditor/OverlayFrame;->mBitmap:Landroid/graphics/Bitmap;

    #@2
    return-object v0
.end method

.method getBitmapImageFileName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 134
    iget-object v0, p0, Landroid/media/videoeditor/OverlayFrame;->mBitmapFileName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method getFilename()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 168
    iget-object v0, p0, Landroid/media/videoeditor/OverlayFrame;->mFilename:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method getOverlayFrameHeight()I
    .registers 2

    #@0
    .prologue
    .line 228
    iget v0, p0, Landroid/media/videoeditor/OverlayFrame;->mOFHeight:I

    #@2
    return v0
.end method

.method getOverlayFrameWidth()I
    .registers 2

    #@0
    .prologue
    .line 235
    iget v0, p0, Landroid/media/videoeditor/OverlayFrame;->mOFWidth:I

    #@2
    return v0
.end method

.method getResizedRGBSizeHeight()I
    .registers 2

    #@0
    .prologue
    .line 264
    iget v0, p0, Landroid/media/videoeditor/OverlayFrame;->mResizedRGBHeight:I

    #@2
    return v0
.end method

.method getResizedRGBSizeWidth()I
    .registers 2

    #@0
    .prologue
    .line 271
    iget v0, p0, Landroid/media/videoeditor/OverlayFrame;->mResizedRGBWidth:I

    #@2
    return v0
.end method

.method invalidate()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 279
    iget-object v0, p0, Landroid/media/videoeditor/OverlayFrame;->mBitmap:Landroid/graphics/Bitmap;

    #@3
    if-eqz v0, :cond_c

    #@5
    .line 280
    iget-object v0, p0, Landroid/media/videoeditor/OverlayFrame;->mBitmap:Landroid/graphics/Bitmap;

    #@7
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    #@a
    .line 281
    iput-object v2, p0, Landroid/media/videoeditor/OverlayFrame;->mBitmap:Landroid/graphics/Bitmap;

    #@c
    .line 284
    :cond_c
    iget-object v0, p0, Landroid/media/videoeditor/OverlayFrame;->mFilename:Ljava/lang/String;

    #@e
    if-eqz v0, :cond_1c

    #@10
    .line 285
    new-instance v0, Ljava/io/File;

    #@12
    iget-object v1, p0, Landroid/media/videoeditor/OverlayFrame;->mFilename:Ljava/lang/String;

    #@14
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@17
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@1a
    .line 286
    iput-object v2, p0, Landroid/media/videoeditor/OverlayFrame;->mFilename:Ljava/lang/String;

    #@1c
    .line 289
    :cond_1c
    iget-object v0, p0, Landroid/media/videoeditor/OverlayFrame;->mBitmapFileName:Ljava/lang/String;

    #@1e
    if-eqz v0, :cond_2c

    #@20
    .line 290
    new-instance v0, Ljava/io/File;

    #@22
    iget-object v1, p0, Landroid/media/videoeditor/OverlayFrame;->mBitmapFileName:Ljava/lang/String;

    #@24
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@27
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@2a
    .line 291
    iput-object v2, p0, Landroid/media/videoeditor/OverlayFrame;->mBitmapFileName:Ljava/lang/String;

    #@2c
    .line 293
    :cond_2c
    return-void
.end method

.method invalidateGeneratedFiles()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 299
    iget-object v0, p0, Landroid/media/videoeditor/OverlayFrame;->mFilename:Ljava/lang/String;

    #@3
    if-eqz v0, :cond_11

    #@5
    .line 300
    new-instance v0, Ljava/io/File;

    #@7
    iget-object v1, p0, Landroid/media/videoeditor/OverlayFrame;->mFilename:Ljava/lang/String;

    #@9
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@c
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@f
    .line 301
    iput-object v2, p0, Landroid/media/videoeditor/OverlayFrame;->mFilename:Ljava/lang/String;

    #@11
    .line 304
    :cond_11
    iget-object v0, p0, Landroid/media/videoeditor/OverlayFrame;->mBitmapFileName:Ljava/lang/String;

    #@13
    if-eqz v0, :cond_21

    #@15
    .line 305
    new-instance v0, Ljava/io/File;

    #@17
    iget-object v1, p0, Landroid/media/videoeditor/OverlayFrame;->mBitmapFileName:Ljava/lang/String;

    #@19
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@1c
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@1f
    .line 306
    iput-object v2, p0, Landroid/media/videoeditor/OverlayFrame;->mBitmapFileName:Ljava/lang/String;

    #@21
    .line 308
    :cond_21
    return-void
.end method

.method save(Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter "path"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 187
    iget-object v4, p0, Landroid/media/videoeditor/OverlayFrame;->mFilename:Ljava/lang/String;

    #@2
    if-eqz v4, :cond_7

    #@4
    .line 188
    iget-object v4, p0, Landroid/media/videoeditor/OverlayFrame;->mFilename:Ljava/lang/String;

    #@6
    .line 221
    :goto_6
    return-object v4

    #@7
    .line 192
    :cond_7
    new-instance v4, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v4

    #@10
    const-string v5, "/"

    #@12
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v4

    #@16
    const-string v5, "Overlay"

    #@18
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {p0}, Landroid/media/videoeditor/OverlayFrame;->getId()Ljava/lang/String;

    #@1f
    move-result-object v5

    #@20
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v4

    #@24
    const-string v5, ".png"

    #@26
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v4

    #@2a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v4

    #@2e
    iput-object v4, p0, Landroid/media/videoeditor/OverlayFrame;->mBitmapFileName:Ljava/lang/String;

    #@30
    .line 193
    new-instance v4, Ljava/io/File;

    #@32
    iget-object v5, p0, Landroid/media/videoeditor/OverlayFrame;->mBitmapFileName:Ljava/lang/String;

    #@34
    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@37
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    #@3a
    move-result v4

    #@3b
    if-nez v4, :cond_53

    #@3d
    .line 194
    new-instance v2, Ljava/io/FileOutputStream;

    #@3f
    iget-object v4, p0, Landroid/media/videoeditor/OverlayFrame;->mBitmapFileName:Ljava/lang/String;

    #@41
    invoke-direct {v2, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    #@44
    .line 195
    .local v2, out:Ljava/io/FileOutputStream;
    iget-object v4, p0, Landroid/media/videoeditor/OverlayFrame;->mBitmap:Landroid/graphics/Bitmap;

    #@46
    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    #@48
    const/16 v6, 0x64

    #@4a
    invoke-virtual {v4, v5, v6, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    #@4d
    .line 196
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V

    #@50
    .line 197
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    #@53
    .line 200
    .end local v2           #out:Ljava/io/FileOutputStream;
    :cond_53
    iget-object v4, p0, Landroid/media/videoeditor/OverlayFrame;->mBitmap:Landroid/graphics/Bitmap;

    #@55
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    #@58
    move-result v4

    #@59
    iput v4, p0, Landroid/media/videoeditor/OverlayFrame;->mOFWidth:I

    #@5b
    .line 201
    iget-object v4, p0, Landroid/media/videoeditor/OverlayFrame;->mBitmap:Landroid/graphics/Bitmap;

    #@5d
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    #@60
    move-result v4

    #@61
    iput v4, p0, Landroid/media/videoeditor/OverlayFrame;->mOFHeight:I

    #@63
    .line 203
    new-instance v4, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v4

    #@6c
    const-string v5, "/"

    #@6e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v4

    #@72
    const-string v5, "Overlay"

    #@74
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v4

    #@78
    invoke-virtual {p0}, Landroid/media/videoeditor/OverlayFrame;->getId()Ljava/lang/String;

    #@7b
    move-result-object v5

    #@7c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v4

    #@80
    const-string v5, ".rgb"

    #@82
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v4

    #@86
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v4

    #@8a
    iput-object v4, p0, Landroid/media/videoeditor/OverlayFrame;->mFilename:Ljava/lang/String;

    #@8c
    .line 206
    invoke-super {p0}, Landroid/media/videoeditor/Overlay;->getMediaItem()Landroid/media/videoeditor/MediaItem;

    #@8f
    move-result-object v4

    #@90
    invoke-virtual {v4}, Landroid/media/videoeditor/MediaItem;->getNativeContext()Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@93
    move-result-object v1

    #@94
    .line 211
    .local v1, nativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;
    invoke-virtual {v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->nativeHelperGetAspectRatio()I

    #@97
    move-result v4

    #@98
    invoke-static {v4}, Landroid/media/videoeditor/MediaProperties;->getSupportedResolutions(I)[Landroid/util/Pair;

    #@9b
    move-result-object v3

    #@9c
    .line 214
    .local v3, resolutions:[Landroid/util/Pair;,"[Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    array-length v4, v3

    #@9d
    add-int/lit8 v4, v4, -0x1

    #@9f
    aget-object v0, v3, v4

    #@a1
    .line 217
    .local v0, maxResolution:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-super {p0}, Landroid/media/videoeditor/Overlay;->getMediaItem()Landroid/media/videoeditor/MediaItem;

    #@a4
    move-result-object v5

    #@a5
    iget-object v4, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@a7
    check-cast v4, Ljava/lang/Integer;

    #@a9
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    #@ac
    move-result v6

    #@ad
    iget-object v4, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@af
    check-cast v4, Ljava/lang/Integer;

    #@b1
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    #@b4
    move-result v4

    #@b5
    invoke-virtual {p0, v5, p0, v6, v4}, Landroid/media/videoeditor/OverlayFrame;->generateOverlayWithRenderingMode(Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/OverlayFrame;II)V

    #@b8
    .line 221
    iget-object v4, p0, Landroid/media/videoeditor/OverlayFrame;->mFilename:Ljava/lang/String;

    #@ba
    goto/16 :goto_6
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .registers 7
    .parameter "bitmap"

    #@0
    .prologue
    .line 142
    invoke-virtual {p0}, Landroid/media/videoeditor/OverlayFrame;->getMediaItem()Landroid/media/videoeditor/MediaItem;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/media/videoeditor/MediaItem;->getNativeContext()Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@7
    move-result-object v0

    #@8
    const/4 v1, 0x1

    #@9
    invoke-virtual {v0, v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@c
    .line 144
    invoke-virtual {p0}, Landroid/media/videoeditor/OverlayFrame;->invalidate()V

    #@f
    .line 146
    iput-object p1, p0, Landroid/media/videoeditor/OverlayFrame;->mBitmap:Landroid/graphics/Bitmap;

    #@11
    .line 147
    iget-object v0, p0, Landroid/media/videoeditor/OverlayFrame;->mFilename:Ljava/lang/String;

    #@13
    if-eqz v0, :cond_22

    #@15
    .line 151
    new-instance v0, Ljava/io/File;

    #@17
    iget-object v1, p0, Landroid/media/videoeditor/OverlayFrame;->mFilename:Ljava/lang/String;

    #@19
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@1c
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@1f
    .line 155
    const/4 v0, 0x0

    #@20
    iput-object v0, p0, Landroid/media/videoeditor/OverlayFrame;->mFilename:Ljava/lang/String;

    #@22
    .line 161
    :cond_22
    invoke-virtual {p0}, Landroid/media/videoeditor/OverlayFrame;->getMediaItem()Landroid/media/videoeditor/MediaItem;

    #@25
    move-result-object v0

    #@26
    iget-wide v1, p0, Landroid/media/videoeditor/Overlay;->mStartTimeMs:J

    #@28
    iget-wide v3, p0, Landroid/media/videoeditor/Overlay;->mDurationMs:J

    #@2a
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/media/videoeditor/MediaItem;->invalidateTransitions(JJ)V

    #@2d
    .line 162
    return-void
.end method

.method setFilename(Ljava/lang/String;)V
    .registers 2
    .parameter "filename"

    #@0
    .prologue
    .line 175
    iput-object p1, p0, Landroid/media/videoeditor/OverlayFrame;->mFilename:Ljava/lang/String;

    #@2
    .line 176
    return-void
.end method

.method setOverlayFrameHeight(I)V
    .registers 2
    .parameter "height"

    #@0
    .prologue
    .line 242
    iput p1, p0, Landroid/media/videoeditor/OverlayFrame;->mOFHeight:I

    #@2
    .line 243
    return-void
.end method

.method setOverlayFrameWidth(I)V
    .registers 2
    .parameter "width"

    #@0
    .prologue
    .line 249
    iput p1, p0, Landroid/media/videoeditor/OverlayFrame;->mOFWidth:I

    #@2
    .line 250
    return-void
.end method

.method setResizedRGBSize(II)V
    .registers 3
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 256
    iput p1, p0, Landroid/media/videoeditor/OverlayFrame;->mResizedRGBWidth:I

    #@2
    .line 257
    iput p2, p0, Landroid/media/videoeditor/OverlayFrame;->mResizedRGBHeight:I

    #@4
    .line 258
    return-void
.end method
