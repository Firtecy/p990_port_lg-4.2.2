.class public Landroid/media/videoeditor/WaveformData;
.super Ljava/lang/Object;
.source "WaveformData.java"


# instance fields
.field private final mFrameDurationMs:I

.field private final mFramesCount:I

.field private final mGains:[S


# direct methods
.method private constructor <init>()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 43
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 44
    iput v0, p0, Landroid/media/videoeditor/WaveformData;->mFrameDurationMs:I

    #@6
    .line 45
    iput v0, p0, Landroid/media/videoeditor/WaveformData;->mFramesCount:I

    #@8
    .line 46
    const/4 v0, 0x0

    #@9
    iput-object v0, p0, Landroid/media/videoeditor/WaveformData;->mGains:[S

    #@b
    .line 47
    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .registers 13
    .parameter "audioWaveformFilename"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v10, 0x4

    #@1
    .line 66
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 68
    if-nez p1, :cond_e

    #@6
    .line 69
    new-instance v8, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v9, "WaveformData : filename is null"

    #@a
    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v8

    #@e
    .line 72
    :cond_e
    const/4 v1, 0x0

    #@f
    .line 75
    .local v1, audioGraphFileReadHandle:Ljava/io/FileInputStream;
    :try_start_f
    new-instance v0, Ljava/io/File;

    #@11
    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@14
    .line 77
    .local v0, audioGraphFileContext:Ljava/io/File;
    new-instance v2, Ljava/io/FileInputStream;

    #@16
    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_19
    .catchall {:try_start_f .. :try_end_19} :catchall_66

    #@19
    .line 81
    .end local v1           #audioGraphFileReadHandle:Ljava/io/FileInputStream;
    .local v2, audioGraphFileReadHandle:Ljava/io/FileInputStream;
    const/4 v8, 0x4

    #@1a
    :try_start_1a
    new-array v4, v8, [B

    #@1c
    .line 83
    .local v4, tempFrameDuration:[B
    const/4 v8, 0x0

    #@1d
    const/4 v9, 0x4

    #@1e
    invoke-virtual {v2, v4, v8, v9}, Ljava/io/FileInputStream;->read([BII)I

    #@21
    .line 85
    const/4 v5, 0x0

    #@22
    .line 86
    .local v5, tempFrameDurationMs:I
    const/4 v7, 0x0

    #@23
    .line 87
    .local v7, tempFramesCounter:I
    const/4 v3, 0x0

    #@24
    .local v3, i:I
    :goto_24
    if-ge v3, v10, :cond_30

    #@26
    .line 88
    shl-int/lit8 v5, v5, 0x8

    #@28
    .line 89
    aget-byte v8, v4, v3

    #@2a
    and-int/lit16 v8, v8, 0xff

    #@2c
    or-int/2addr v5, v8

    #@2d
    .line 87
    add-int/lit8 v3, v3, 0x1

    #@2f
    goto :goto_24

    #@30
    .line 91
    :cond_30
    iput v5, p0, Landroid/media/videoeditor/WaveformData;->mFrameDurationMs:I

    #@32
    .line 96
    const/4 v8, 0x4

    #@33
    new-array v6, v8, [B

    #@35
    .line 98
    .local v6, tempFramesCount:[B
    const/4 v8, 0x0

    #@36
    const/4 v9, 0x4

    #@37
    invoke-virtual {v2, v6, v8, v9}, Ljava/io/FileInputStream;->read([BII)I

    #@3a
    .line 99
    const/4 v3, 0x0

    #@3b
    :goto_3b
    if-ge v3, v10, :cond_47

    #@3d
    .line 100
    shl-int/lit8 v7, v7, 0x8

    #@3f
    .line 101
    aget-byte v8, v6, v3

    #@41
    and-int/lit16 v8, v8, 0xff

    #@43
    or-int/2addr v7, v8

    #@44
    .line 99
    add-int/lit8 v3, v3, 0x1

    #@46
    goto :goto_3b

    #@47
    .line 103
    :cond_47
    iput v7, p0, Landroid/media/videoeditor/WaveformData;->mFramesCount:I

    #@49
    .line 108
    iget v8, p0, Landroid/media/videoeditor/WaveformData;->mFramesCount:I

    #@4b
    new-array v8, v8, [S

    #@4d
    iput-object v8, p0, Landroid/media/videoeditor/WaveformData;->mGains:[S

    #@4f
    .line 110
    const/4 v3, 0x0

    #@50
    :goto_50
    iget v8, p0, Landroid/media/videoeditor/WaveformData;->mFramesCount:I

    #@52
    if-ge v3, v8, :cond_60

    #@54
    .line 111
    iget-object v8, p0, Landroid/media/videoeditor/WaveformData;->mGains:[S

    #@56
    invoke-virtual {v2}, Ljava/io/FileInputStream;->read()I

    #@59
    move-result v9

    #@5a
    int-to-short v9, v9

    #@5b
    aput-short v9, v8, v3
    :try_end_5d
    .catchall {:try_start_1a .. :try_end_5d} :catchall_6d

    #@5d
    .line 110
    add-int/lit8 v3, v3, 0x1

    #@5f
    goto :goto_50

    #@60
    .line 114
    :cond_60
    if-eqz v2, :cond_65

    #@62
    .line 115
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    #@65
    .line 118
    :cond_65
    return-void

    #@66
    .line 114
    .end local v0           #audioGraphFileContext:Ljava/io/File;
    .end local v2           #audioGraphFileReadHandle:Ljava/io/FileInputStream;
    .end local v3           #i:I
    .end local v4           #tempFrameDuration:[B
    .end local v5           #tempFrameDurationMs:I
    .end local v6           #tempFramesCount:[B
    .end local v7           #tempFramesCounter:I
    .restart local v1       #audioGraphFileReadHandle:Ljava/io/FileInputStream;
    :catchall_66
    move-exception v8

    #@67
    :goto_67
    if-eqz v1, :cond_6c

    #@69
    .line 115
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    #@6c
    :cond_6c
    throw v8

    #@6d
    .line 114
    .end local v1           #audioGraphFileReadHandle:Ljava/io/FileInputStream;
    .restart local v0       #audioGraphFileContext:Ljava/io/File;
    .restart local v2       #audioGraphFileReadHandle:Ljava/io/FileInputStream;
    :catchall_6d
    move-exception v8

    #@6e
    move-object v1, v2

    #@6f
    .end local v2           #audioGraphFileReadHandle:Ljava/io/FileInputStream;
    .restart local v1       #audioGraphFileReadHandle:Ljava/io/FileInputStream;
    goto :goto_67
.end method


# virtual methods
.method public getFrameDuration()I
    .registers 2

    #@0
    .prologue
    .line 124
    iget v0, p0, Landroid/media/videoeditor/WaveformData;->mFrameDurationMs:I

    #@2
    return v0
.end method

.method public getFrameGains()[S
    .registers 2

    #@0
    .prologue
    .line 139
    iget-object v0, p0, Landroid/media/videoeditor/WaveformData;->mGains:[S

    #@2
    return-object v0
.end method

.method public getFramesCount()I
    .registers 2

    #@0
    .prologue
    .line 131
    iget v0, p0, Landroid/media/videoeditor/WaveformData;->mFramesCount:I

    #@2
    return v0
.end method
