.class public abstract Landroid/media/videoeditor/Overlay;
.super Ljava/lang/Object;
.source "Overlay.java"


# instance fields
.field protected mDurationMs:J

.field private final mMediaItem:Landroid/media/videoeditor/MediaItem;

.field protected mStartTimeMs:J

.field private final mUniqueId:Ljava/lang/String;

.field private final mUserAttributes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .registers 8

    #@0
    .prologue
    const-wide/16 v3, 0x0

    #@2
    const/4 v1, 0x0

    #@3
    .line 49
    move-object v0, p0

    #@4
    move-object v2, v1

    #@5
    move-wide v5, v3

    #@6
    invoke-direct/range {v0 .. v6}, Landroid/media/videoeditor/Overlay;-><init>(Landroid/media/videoeditor/MediaItem;Ljava/lang/String;JJ)V

    #@9
    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/media/videoeditor/MediaItem;Ljava/lang/String;JJ)V
    .registers 11
    .parameter "mediaItem"
    .parameter "overlayId"
    .parameter "startTimeMs"
    .parameter "durationMs"

    #@0
    .prologue
    const-wide/16 v1, 0x0

    #@2
    .line 64
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 65
    if-nez p1, :cond_f

    #@7
    .line 66
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@9
    const-string v1, "Media item cannot be null"

    #@b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 69
    :cond_f
    cmp-long v0, p3, v1

    #@11
    if-ltz v0, :cond_17

    #@13
    cmp-long v0, p5, v1

    #@15
    if-gez v0, :cond_1f

    #@17
    .line 70
    :cond_17
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@19
    const-string v1, "Invalid start time and/OR duration"

    #@1b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v0

    #@1f
    .line 73
    :cond_1f
    add-long v0, p3, p5

    #@21
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getDuration()J

    #@24
    move-result-wide v2

    #@25
    cmp-long v0, v0, v2

    #@27
    if-lez v0, :cond_31

    #@29
    .line 74
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@2b
    const-string v1, "Invalid start time and duration"

    #@2d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@30
    throw v0

    #@31
    .line 77
    :cond_31
    iput-object p1, p0, Landroid/media/videoeditor/Overlay;->mMediaItem:Landroid/media/videoeditor/MediaItem;

    #@33
    .line 78
    iput-object p2, p0, Landroid/media/videoeditor/Overlay;->mUniqueId:Ljava/lang/String;

    #@35
    .line 79
    iput-wide p3, p0, Landroid/media/videoeditor/Overlay;->mStartTimeMs:J

    #@37
    .line 80
    iput-wide p5, p0, Landroid/media/videoeditor/Overlay;->mDurationMs:J

    #@39
    .line 81
    new-instance v0, Ljava/util/HashMap;

    #@3b
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@3e
    iput-object v0, p0, Landroid/media/videoeditor/Overlay;->mUserAttributes:Ljava/util/Map;

    #@40
    .line 82
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .parameter "object"

    #@0
    .prologue
    .line 209
    instance-of v0, p1, Landroid/media/videoeditor/Overlay;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 210
    const/4 v0, 0x0

    #@5
    .line 212
    .end local p1
    :goto_5
    return v0

    #@6
    .restart local p1
    :cond_6
    iget-object v0, p0, Landroid/media/videoeditor/Overlay;->mUniqueId:Ljava/lang/String;

    #@8
    check-cast p1, Landroid/media/videoeditor/Overlay;

    #@a
    .end local p1
    iget-object v1, p1, Landroid/media/videoeditor/Overlay;->mUniqueId:Ljava/lang/String;

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    goto :goto_5
.end method

.method public getDuration()J
    .registers 3

    #@0
    .prologue
    .line 99
    iget-wide v0, p0, Landroid/media/videoeditor/Overlay;->mDurationMs:J

    #@2
    return-wide v0
.end method

.method public getId()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 90
    iget-object v0, p0, Landroid/media/videoeditor/Overlay;->mUniqueId:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getMediaItem()Landroid/media/videoeditor/MediaItem;
    .registers 2

    #@0
    .prologue
    .line 182
    iget-object v0, p0, Landroid/media/videoeditor/Overlay;->mMediaItem:Landroid/media/videoeditor/MediaItem;

    #@2
    return-object v0
.end method

.method public getStartTime()J
    .registers 3

    #@0
    .prologue
    .line 131
    iget-wide v0, p0, Landroid/media/videoeditor/Overlay;->mStartTimeMs:J

    #@2
    return-wide v0
.end method

.method public getUserAttributes()Ljava/util/Map;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 201
    iget-object v0, p0, Landroid/media/videoeditor/Overlay;->mUserAttributes:Ljava/util/Map;

    #@2
    return-object v0
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 220
    iget-object v0, p0, Landroid/media/videoeditor/Overlay;->mUniqueId:Ljava/lang/String;

    #@2
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public setDuration(J)V
    .registers 12
    .parameter "durationMs"

    #@0
    .prologue
    .line 109
    const-wide/16 v0, 0x0

    #@2
    cmp-long v0, p1, v0

    #@4
    if-gez v0, :cond_e

    #@6
    .line 110
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v1, "Invalid duration"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 113
    :cond_e
    iget-wide v0, p0, Landroid/media/videoeditor/Overlay;->mStartTimeMs:J

    #@10
    add-long/2addr v0, p1

    #@11
    iget-object v2, p0, Landroid/media/videoeditor/Overlay;->mMediaItem:Landroid/media/videoeditor/MediaItem;

    #@13
    invoke-virtual {v2}, Landroid/media/videoeditor/MediaItem;->getDuration()J

    #@16
    move-result-wide v5

    #@17
    cmp-long v0, v0, v5

    #@19
    if-lez v0, :cond_23

    #@1b
    .line 114
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1d
    const-string v1, "Duration is too large"

    #@1f
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@22
    throw v0

    #@23
    .line 117
    :cond_23
    invoke-virtual {p0}, Landroid/media/videoeditor/Overlay;->getMediaItem()Landroid/media/videoeditor/MediaItem;

    #@26
    move-result-object v0

    #@27
    invoke-virtual {v0}, Landroid/media/videoeditor/MediaItem;->getNativeContext()Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@2a
    move-result-object v0

    #@2b
    const/4 v1, 0x1

    #@2c
    invoke-virtual {v0, v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@2f
    .line 119
    iget-wide v3, p0, Landroid/media/videoeditor/Overlay;->mDurationMs:J

    #@31
    .line 120
    .local v3, oldDurationMs:J
    iput-wide p1, p0, Landroid/media/videoeditor/Overlay;->mDurationMs:J

    #@33
    .line 122
    iget-object v0, p0, Landroid/media/videoeditor/Overlay;->mMediaItem:Landroid/media/videoeditor/MediaItem;

    #@35
    iget-wide v1, p0, Landroid/media/videoeditor/Overlay;->mStartTimeMs:J

    #@37
    iget-wide v5, p0, Landroid/media/videoeditor/Overlay;->mStartTimeMs:J

    #@39
    iget-wide v7, p0, Landroid/media/videoeditor/Overlay;->mDurationMs:J

    #@3b
    invoke-virtual/range {v0 .. v8}, Landroid/media/videoeditor/MediaItem;->invalidateTransitions(JJJJ)V

    #@3e
    .line 123
    return-void
.end method

.method public setStartTime(J)V
    .registers 12
    .parameter "startTimeMs"

    #@0
    .prologue
    .line 142
    iget-wide v3, p0, Landroid/media/videoeditor/Overlay;->mDurationMs:J

    #@2
    add-long/2addr v3, p1

    #@3
    iget-object v0, p0, Landroid/media/videoeditor/Overlay;->mMediaItem:Landroid/media/videoeditor/MediaItem;

    #@5
    invoke-virtual {v0}, Landroid/media/videoeditor/MediaItem;->getDuration()J

    #@8
    move-result-wide v5

    #@9
    cmp-long v0, v3, v5

    #@b
    if-lez v0, :cond_15

    #@d
    .line 143
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@f
    const-string v3, "Start time is too large"

    #@11
    invoke-direct {v0, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@14
    throw v0

    #@15
    .line 146
    :cond_15
    invoke-virtual {p0}, Landroid/media/videoeditor/Overlay;->getMediaItem()Landroid/media/videoeditor/MediaItem;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Landroid/media/videoeditor/MediaItem;->getNativeContext()Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@1c
    move-result-object v0

    #@1d
    const/4 v3, 0x1

    #@1e
    invoke-virtual {v0, v3}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@21
    .line 148
    iget-wide v1, p0, Landroid/media/videoeditor/Overlay;->mStartTimeMs:J

    #@23
    .line 149
    .local v1, oldStartTimeMs:J
    iput-wide p1, p0, Landroid/media/videoeditor/Overlay;->mStartTimeMs:J

    #@25
    .line 151
    iget-object v0, p0, Landroid/media/videoeditor/Overlay;->mMediaItem:Landroid/media/videoeditor/MediaItem;

    #@27
    iget-wide v3, p0, Landroid/media/videoeditor/Overlay;->mDurationMs:J

    #@29
    iget-wide v5, p0, Landroid/media/videoeditor/Overlay;->mStartTimeMs:J

    #@2b
    iget-wide v7, p0, Landroid/media/videoeditor/Overlay;->mDurationMs:J

    #@2d
    invoke-virtual/range {v0 .. v8}, Landroid/media/videoeditor/MediaItem;->invalidateTransitions(JJJJ)V

    #@30
    .line 152
    return-void
.end method

.method public setStartTimeAndDuration(JJ)V
    .registers 14
    .parameter "startTimeMs"
    .parameter "durationMs"

    #@0
    .prologue
    .line 161
    add-long v5, p1, p3

    #@2
    iget-object v0, p0, Landroid/media/videoeditor/Overlay;->mMediaItem:Landroid/media/videoeditor/MediaItem;

    #@4
    invoke-virtual {v0}, Landroid/media/videoeditor/MediaItem;->getDuration()J

    #@7
    move-result-wide v7

    #@8
    cmp-long v0, v5, v7

    #@a
    if-lez v0, :cond_14

    #@c
    .line 162
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@e
    const-string v5, "Invalid start time or duration"

    #@10
    invoke-direct {v0, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 165
    :cond_14
    invoke-virtual {p0}, Landroid/media/videoeditor/Overlay;->getMediaItem()Landroid/media/videoeditor/MediaItem;

    #@17
    move-result-object v0

    #@18
    invoke-virtual {v0}, Landroid/media/videoeditor/MediaItem;->getNativeContext()Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@1b
    move-result-object v0

    #@1c
    const/4 v5, 0x1

    #@1d
    invoke-virtual {v0, v5}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@20
    .line 167
    iget-wide v1, p0, Landroid/media/videoeditor/Overlay;->mStartTimeMs:J

    #@22
    .line 168
    .local v1, oldStartTimeMs:J
    iget-wide v3, p0, Landroid/media/videoeditor/Overlay;->mDurationMs:J

    #@24
    .line 170
    .local v3, oldDurationMs:J
    iput-wide p1, p0, Landroid/media/videoeditor/Overlay;->mStartTimeMs:J

    #@26
    .line 171
    iput-wide p3, p0, Landroid/media/videoeditor/Overlay;->mDurationMs:J

    #@28
    .line 173
    iget-object v0, p0, Landroid/media/videoeditor/Overlay;->mMediaItem:Landroid/media/videoeditor/MediaItem;

    #@2a
    iget-wide v5, p0, Landroid/media/videoeditor/Overlay;->mStartTimeMs:J

    #@2c
    iget-wide v7, p0, Landroid/media/videoeditor/Overlay;->mDurationMs:J

    #@2e
    invoke-virtual/range {v0 .. v8}, Landroid/media/videoeditor/MediaItem;->invalidateTransitions(JJJJ)V

    #@31
    .line 174
    return-void
.end method

.method public setUserAttribute(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 192
    iget-object v0, p0, Landroid/media/videoeditor/Overlay;->mUserAttributes:Ljava/util/Map;

    #@2
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    .line 193
    return-void
.end method
