.class public Landroid/media/videoeditor/VideoEditorProfile;
.super Ljava/lang/Object;
.source "VideoEditorProfile.java"


# instance fields
.field public maxInputVideoFrameHeight:I

.field public maxInputVideoFrameWidth:I

.field public maxOutputVideoFrameHeight:I

.field public maxOutputVideoFrameWidth:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 35
    const-string/jumbo v0, "media_jni"

    #@3
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@6
    .line 36
    invoke-static {}, Landroid/media/videoeditor/VideoEditorProfile;->native_init()V

    #@9
    .line 37
    return-void
.end method

.method private constructor <init>(IIII)V
    .registers 5
    .parameter "inputWidth"
    .parameter "inputHeight"
    .parameter "outputWidth"
    .parameter "outputHeight"

    #@0
    .prologue
    .line 107
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 109
    iput p1, p0, Landroid/media/videoeditor/VideoEditorProfile;->maxInputVideoFrameWidth:I

    #@5
    .line 110
    iput p2, p0, Landroid/media/videoeditor/VideoEditorProfile;->maxInputVideoFrameHeight:I

    #@7
    .line 111
    iput p3, p0, Landroid/media/videoeditor/VideoEditorProfile;->maxOutputVideoFrameWidth:I

    #@9
    .line 112
    iput p4, p0, Landroid/media/videoeditor/VideoEditorProfile;->maxOutputVideoFrameHeight:I

    #@b
    .line 113
    return-void
.end method

.method public static get()Landroid/media/videoeditor/VideoEditorProfile;
    .registers 1

    #@0
    .prologue
    .line 62
    invoke-static {}, Landroid/media/videoeditor/VideoEditorProfile;->native_get_videoeditor_profile()Landroid/media/videoeditor/VideoEditorProfile;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static getExportLevel(I)I
    .registers 5
    .parameter "vidCodec"

    #@0
    .prologue
    .line 88
    const/4 v0, -0x1

    #@1
    .line 90
    .local v0, level:I
    packed-switch p0, :pswitch_data_22

    #@4
    .line 97
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "Unsupported video codec"

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1c
    throw v1

    #@1d
    .line 94
    :pswitch_1d
    invoke-static {p0}, Landroid/media/videoeditor/VideoEditorProfile;->native_get_videoeditor_export_level(I)I

    #@20
    move-result v0

    #@21
    .line 100
    return v0

    #@22
    .line 90
    :pswitch_data_22
    .packed-switch 0x1
        :pswitch_1d
        :pswitch_1d
        :pswitch_1d
    .end packed-switch
.end method

.method public static getExportProfile(I)I
    .registers 5
    .parameter "vidCodec"

    #@0
    .prologue
    .line 69
    const/4 v0, -0x1

    #@1
    .line 71
    .local v0, profile:I
    packed-switch p0, :pswitch_data_22

    #@4
    .line 78
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "Unsupported video codec"

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1c
    throw v1

    #@1d
    .line 75
    :pswitch_1d
    invoke-static {p0}, Landroid/media/videoeditor/VideoEditorProfile;->native_get_videoeditor_export_profile(I)I

    #@20
    move-result v0

    #@21
    .line 81
    return v0

    #@22
    .line 71
    :pswitch_data_22
    .packed-switch 0x1
        :pswitch_1d
        :pswitch_1d
        :pswitch_1d
    .end packed-switch
.end method

.method private static final native native_get_videoeditor_export_level(I)I
.end method

.method private static final native native_get_videoeditor_export_profile(I)I
.end method

.method private static final native native_get_videoeditor_profile()Landroid/media/videoeditor/VideoEditorProfile;
.end method

.method private static final native native_init()V
.end method
