.class public Landroid/media/videoeditor/TransitionCrossfade;
.super Landroid/media/videoeditor/Transition;
.source "TransitionCrossfade.java"


# direct methods
.method private constructor <init>()V
    .registers 8

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 33
    const-wide/16 v4, 0x0

    #@3
    const/4 v6, 0x0

    #@4
    move-object v0, p0

    #@5
    move-object v2, v1

    #@6
    move-object v3, v1

    #@7
    invoke-direct/range {v0 .. v6}, Landroid/media/videoeditor/TransitionCrossfade;-><init>(Ljava/lang/String;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;JI)V

    #@a
    .line 34
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;JI)V
    .registers 7
    .parameter "transitionId"
    .parameter "afterMediaItem"
    .parameter "beforeMediaItem"
    .parameter "durationMs"
    .parameter "behavior"

    #@0
    .prologue
    .line 52
    invoke-direct/range {p0 .. p6}, Landroid/media/videoeditor/Transition;-><init>(Ljava/lang/String;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;JI)V

    #@3
    .line 53
    return-void
.end method


# virtual methods
.method generate()V
    .registers 1

    #@0
    .prologue
    .line 60
    invoke-super {p0}, Landroid/media/videoeditor/Transition;->generate()V

    #@3
    .line 61
    return-void
.end method
