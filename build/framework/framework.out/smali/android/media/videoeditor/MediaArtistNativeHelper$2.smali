.class Landroid/media/videoeditor/MediaArtistNativeHelper$2;
.super Ljava/lang/Object;
.source "MediaArtistNativeHelper.java"

# interfaces
.implements Landroid/media/videoeditor/MediaArtistNativeHelper$NativeGetPixelsListCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/media/videoeditor/MediaArtistNativeHelper;->getPixelsList(Ljava/lang/String;IIJJI[ILandroid/media/videoeditor/MediaItem$GetThumbnailListCallback;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/media/videoeditor/MediaArtistNativeHelper;

.field final synthetic val$callback:Landroid/media/videoeditor/MediaItem$GetThumbnailListCallback;

.field final synthetic val$decArray:[I

.field final synthetic val$decBuffer:Ljava/nio/IntBuffer;

.field final synthetic val$decHeight:I

.field final synthetic val$decWidth:I

.field final synthetic val$needToMassage:Z

.field final synthetic val$outHeight:I

.field final synthetic val$outWidth:I

.field final synthetic val$thumbnailSize:I

.field final synthetic val$tmpBitmap:Landroid/graphics/Bitmap;

.field final synthetic val$videoRotation:I


# direct methods
.method constructor <init>(Landroid/media/videoeditor/MediaArtistNativeHelper;IILjava/nio/IntBuffer;[IIZLandroid/graphics/Bitmap;IIILandroid/media/videoeditor/MediaItem$GetThumbnailListCallback;)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 3827
    iput-object p1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->this$0:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@2
    iput p2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->val$outWidth:I

    #@4
    iput p3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->val$outHeight:I

    #@6
    iput-object p4, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->val$decBuffer:Ljava/nio/IntBuffer;

    #@8
    iput-object p5, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->val$decArray:[I

    #@a
    iput p6, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->val$thumbnailSize:I

    #@c
    iput-boolean p7, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->val$needToMassage:Z

    #@e
    iput-object p8, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->val$tmpBitmap:Landroid/graphics/Bitmap;

    #@10
    iput p9, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->val$decWidth:I

    #@12
    iput p10, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->val$decHeight:I

    #@14
    iput p11, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->val$videoRotation:I

    #@16
    iput-object p12, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->val$callback:Landroid/media/videoeditor/MediaItem$GetThumbnailListCallback;

    #@18
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@1b
    return-void
.end method


# virtual methods
.method public onThumbnail(I)V
    .registers 13
    .parameter "index"

    #@0
    .prologue
    const/high16 v10, 0x3f80

    #@2
    const/high16 v9, 0x3f00

    #@4
    .line 3830
    iget v5, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->val$outWidth:I

    #@6
    iget v6, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->val$outHeight:I

    #@8
    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@a
    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    #@d
    move-result-object v2

    #@e
    .line 3834
    .local v2, outBitmap:Landroid/graphics/Bitmap;
    iget-object v5, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->val$decBuffer:Ljava/nio/IntBuffer;

    #@10
    invoke-virtual {v5}, Ljava/nio/IntBuffer;->rewind()Ljava/nio/Buffer;

    #@13
    .line 3835
    iget-object v5, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->val$decBuffer:Ljava/nio/IntBuffer;

    #@15
    iget-object v6, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->val$decArray:[I

    #@17
    const/4 v7, 0x0

    #@18
    iget v8, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->val$thumbnailSize:I

    #@1a
    invoke-virtual {v5, v6, v7, v8}, Ljava/nio/IntBuffer;->put([III)Ljava/nio/IntBuffer;

    #@1d
    .line 3836
    iget-object v5, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->val$decBuffer:Ljava/nio/IntBuffer;

    #@1f
    invoke-virtual {v5}, Ljava/nio/IntBuffer;->rewind()Ljava/nio/Buffer;

    #@22
    .line 3838
    iget-boolean v5, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->val$needToMassage:Z

    #@24
    if-nez v5, :cond_31

    #@26
    .line 3840
    iget-object v5, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->val$decBuffer:Ljava/nio/IntBuffer;

    #@28
    invoke-virtual {v2, v5}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    #@2b
    .line 3858
    :goto_2b
    iget-object v5, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->val$callback:Landroid/media/videoeditor/MediaItem$GetThumbnailListCallback;

    #@2d
    invoke-interface {v5, v2, p1}, Landroid/media/videoeditor/MediaItem$GetThumbnailListCallback;->onThumbnail(Landroid/graphics/Bitmap;I)V

    #@30
    .line 3859
    return-void

    #@31
    .line 3843
    :cond_31
    iget-object v5, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->val$tmpBitmap:Landroid/graphics/Bitmap;

    #@33
    iget-object v6, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->val$decBuffer:Ljava/nio/IntBuffer;

    #@35
    invoke-virtual {v5, v6}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    #@38
    .line 3849
    new-instance v0, Landroid/graphics/Canvas;

    #@3a
    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    #@3d
    .line 3850
    .local v0, canvas:Landroid/graphics/Canvas;
    new-instance v1, Landroid/graphics/Matrix;

    #@3f
    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    #@42
    .line 3851
    .local v1, m:Landroid/graphics/Matrix;
    iget v5, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->val$decWidth:I

    #@44
    int-to-float v5, v5

    #@45
    div-float v3, v10, v5

    #@47
    .line 3852
    .local v3, sx:F
    iget v5, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->val$decHeight:I

    #@49
    int-to-float v5, v5

    #@4a
    div-float v4, v10, v5

    #@4c
    .line 3853
    .local v4, sy:F
    invoke-virtual {v1, v3, v4}, Landroid/graphics/Matrix;->postScale(FF)Z

    #@4f
    .line 3854
    iget v5, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->val$videoRotation:I

    #@51
    int-to-float v5, v5

    #@52
    invoke-virtual {v1, v5, v9, v9}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    #@55
    .line 3855
    iget v5, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->val$outWidth:I

    #@57
    int-to-float v5, v5

    #@58
    iget v6, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->val$outHeight:I

    #@5a
    int-to-float v6, v6

    #@5b
    invoke-virtual {v1, v5, v6}, Landroid/graphics/Matrix;->postScale(FF)Z

    #@5e
    .line 3856
    iget-object v5, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$2;->val$tmpBitmap:Landroid/graphics/Bitmap;

    #@60
    invoke-static {}, Landroid/media/videoeditor/MediaArtistNativeHelper;->access$000()Landroid/graphics/Paint;

    #@63
    move-result-object v6

    #@64
    invoke-virtual {v0, v5, v1, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    #@67
    goto :goto_2b
.end method
