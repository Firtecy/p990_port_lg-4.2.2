.class public abstract Landroid/media/videoeditor/Transition;
.super Ljava/lang/Object;
.source "Transition.java"


# static fields
.field public static final BEHAVIOR_LINEAR:I = 0x2

.field private static final BEHAVIOR_MAX_VALUE:I = 0x4

.field public static final BEHAVIOR_MIDDLE_FAST:I = 0x4

.field public static final BEHAVIOR_MIDDLE_SLOW:I = 0x3

.field private static final BEHAVIOR_MIN_VALUE:I = 0x0

.field public static final BEHAVIOR_SPEED_DOWN:I = 0x1

.field public static final BEHAVIOR_SPEED_UP:I


# instance fields
.field private final mAfterMediaItem:Landroid/media/videoeditor/MediaItem;

.field private final mBeforeMediaItem:Landroid/media/videoeditor/MediaItem;

.field protected final mBehavior:I

.field protected mDurationMs:J

.field protected mFilename:Ljava/lang/String;

.field protected mNativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

.field private final mUniqueId:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .registers 8

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 102
    const-wide/16 v4, 0x0

    #@3
    const/4 v6, 0x0

    #@4
    move-object v0, p0

    #@5
    move-object v2, v1

    #@6
    move-object v3, v1

    #@7
    invoke-direct/range {v0 .. v6}, Landroid/media/videoeditor/Transition;-><init>(Ljava/lang/String;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;JI)V

    #@a
    .line 103
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;JI)V
    .registers 10
    .parameter "transitionId"
    .parameter "afterMediaItem"
    .parameter "beforeMediaItem"
    .parameter "durationMs"
    .parameter "behavior"

    #@0
    .prologue
    .line 118
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 119
    if-ltz p6, :cond_8

    #@5
    const/4 v0, 0x4

    #@6
    if-le p6, v0, :cond_21

    #@8
    .line 120
    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "Invalid behavior: "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@20
    throw v0

    #@21
    .line 122
    :cond_21
    if-nez p2, :cond_2d

    #@23
    if-nez p3, :cond_2d

    #@25
    .line 123
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@27
    const-string v1, "Null media items"

    #@29
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v0

    #@2d
    .line 125
    :cond_2d
    iput-object p1, p0, Landroid/media/videoeditor/Transition;->mUniqueId:Ljava/lang/String;

    #@2f
    .line 126
    iput-object p2, p0, Landroid/media/videoeditor/Transition;->mAfterMediaItem:Landroid/media/videoeditor/MediaItem;

    #@31
    .line 127
    iput-object p3, p0, Landroid/media/videoeditor/Transition;->mBeforeMediaItem:Landroid/media/videoeditor/MediaItem;

    #@33
    .line 128
    iput-wide p4, p0, Landroid/media/videoeditor/Transition;->mDurationMs:J

    #@35
    .line 129
    iput p6, p0, Landroid/media/videoeditor/Transition;->mBehavior:I

    #@37
    .line 130
    const/4 v0, 0x0

    #@38
    iput-object v0, p0, Landroid/media/videoeditor/Transition;->mNativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@3a
    .line 131
    invoke-virtual {p0}, Landroid/media/videoeditor/Transition;->getMaximumDuration()J

    #@3d
    move-result-wide v0

    #@3e
    cmp-long v0, p4, v0

    #@40
    if-lez v0, :cond_4a

    #@42
    .line 132
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@44
    const-string v1, "The duration is too large"

    #@46
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@49
    throw v0

    #@4a
    .line 134
    :cond_4a
    if-eqz p2, :cond_53

    #@4c
    .line 135
    invoke-virtual {p2}, Landroid/media/videoeditor/MediaItem;->getNativeContext()Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@4f
    move-result-object v0

    #@50
    iput-object v0, p0, Landroid/media/videoeditor/Transition;->mNativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@52
    .line 139
    :goto_52
    return-void

    #@53
    .line 137
    :cond_53
    invoke-virtual {p3}, Landroid/media/videoeditor/MediaItem;->getNativeContext()Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@56
    move-result-object v0

    #@57
    iput-object v0, p0, Landroid/media/videoeditor/Transition;->mNativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@59
    goto :goto_52
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .parameter "object"

    #@0
    .prologue
    .line 476
    instance-of v0, p1, Landroid/media/videoeditor/Transition;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 477
    const/4 v0, 0x0

    #@5
    .line 479
    .end local p1
    :goto_5
    return v0

    #@6
    .restart local p1
    :cond_6
    iget-object v0, p0, Landroid/media/videoeditor/Transition;->mUniqueId:Ljava/lang/String;

    #@8
    check-cast p1, Landroid/media/videoeditor/Transition;

    #@a
    .end local p1
    iget-object v1, p1, Landroid/media/videoeditor/Transition;->mUniqueId:Ljava/lang/String;

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    goto :goto_5
.end method

.method generate()V
    .registers 22

    #@0
    .prologue
    .line 333
    invoke-virtual/range {p0 .. p0}, Landroid/media/videoeditor/Transition;->getAfterMediaItem()Landroid/media/videoeditor/MediaItem;

    #@3
    move-result-object v5

    #@4
    .line 334
    .local v5, m1:Landroid/media/videoeditor/MediaItem;
    invoke-virtual/range {p0 .. p0}, Landroid/media/videoeditor/Transition;->getBeforeMediaItem()Landroid/media/videoeditor/MediaItem;

    #@7
    move-result-object v6

    #@8
    .line 335
    .local v6, m2:Landroid/media/videoeditor/MediaItem;
    new-instance v8, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@a
    invoke-direct {v8}, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;-><init>()V

    #@d
    .line 336
    .local v8, clipSettings1:Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;
    new-instance v9, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@f
    invoke-direct {v9}, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;-><init>()V

    #@12
    .line 337
    .local v9, clipSettings2:Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;
    const/16 v16, 0x0

    #@14
    .line 338
    .local v16, transitionSetting:Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;
    new-instance v3, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@16
    invoke-direct {v3}, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;-><init>()V

    #@19
    .line 342
    .local v3, editSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;
    const/4 v15, 0x0

    #@1a
    .line 344
    .local v15, output:Ljava/lang/String;
    move-object/from16 v0, p0

    #@1c
    iget-object v2, v0, Landroid/media/videoeditor/Transition;->mNativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@1e
    if-nez v2, :cond_2a

    #@20
    .line 345
    if-eqz v5, :cond_8e

    #@22
    .line 346
    invoke-virtual {v5}, Landroid/media/videoeditor/MediaItem;->getNativeContext()Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@25
    move-result-object v2

    #@26
    move-object/from16 v0, p0

    #@28
    iput-object v2, v0, Landroid/media/videoeditor/Transition;->mNativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@2a
    .line 350
    :cond_2a
    :goto_2a
    invoke-virtual/range {p0 .. p0}, Landroid/media/videoeditor/Transition;->getTransitionSettings()Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;

    #@2d
    move-result-object v16

    #@2e
    .line 351
    if-eqz v5, :cond_d3

    #@30
    if-eqz v6, :cond_d3

    #@32
    .line 353
    invoke-virtual {v5}, Landroid/media/videoeditor/MediaItem;->getClipSettings()Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@35
    move-result-object v8

    #@36
    .line 354
    invoke-virtual {v6}, Landroid/media/videoeditor/MediaItem;->getClipSettings()Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@39
    move-result-object v9

    #@3a
    .line 355
    iget v2, v8, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutTime:I

    #@3c
    int-to-long v0, v2

    #@3d
    move-wide/from16 v17, v0

    #@3f
    move-object/from16 v0, p0

    #@41
    iget-wide v0, v0, Landroid/media/videoeditor/Transition;->mDurationMs:J

    #@43
    move-wide/from16 v19, v0

    #@45
    sub-long v17, v17, v19

    #@47
    move-wide/from16 v0, v17

    #@49
    long-to-int v2, v0

    #@4a
    iput v2, v8, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->beginCutTime:I

    #@4c
    .line 357
    iget v2, v9, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->beginCutTime:I

    #@4e
    int-to-long v0, v2

    #@4f
    move-wide/from16 v17, v0

    #@51
    move-object/from16 v0, p0

    #@53
    iget-wide v0, v0, Landroid/media/videoeditor/Transition;->mDurationMs:J

    #@55
    move-wide/from16 v19, v0

    #@57
    add-long v17, v17, v19

    #@59
    move-wide/from16 v0, v17

    #@5b
    long-to-int v2, v0

    #@5c
    iput v2, v9, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutTime:I

    #@5e
    .line 363
    const/4 v2, 0x1

    #@5f
    move-object/from16 v0, p0

    #@61
    invoke-virtual {v0, v5, v8, v2}, Landroid/media/videoeditor/Transition;->isEffectandOverlayOverlapping(Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;I)Ljava/util/List;

    #@64
    move-result-object v10

    #@65
    .line 364
    .local v10, effectSettings_clip1:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;>;"
    const/4 v2, 0x2

    #@66
    move-object/from16 v0, p0

    #@68
    invoke-virtual {v0, v6, v9, v2}, Landroid/media/videoeditor/Transition;->isEffectandOverlayOverlapping(Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;I)Ljava/util/List;

    #@6b
    move-result-object v11

    #@6c
    .line 365
    .local v11, effectSettings_clip2:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;>;"
    const/4 v13, 0x0

    #@6d
    .local v13, index:I
    :goto_6d
    invoke-interface {v11}, Ljava/util/List;->size()I

    #@70
    move-result v2

    #@71
    if-ge v13, v2, :cond_99

    #@73
    .line 366
    invoke-interface {v11, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@76
    move-result-object v2

    #@77
    check-cast v2, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;

    #@79
    iget v4, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@7b
    int-to-long v0, v4

    #@7c
    move-wide/from16 v17, v0

    #@7e
    move-object/from16 v0, p0

    #@80
    iget-wide v0, v0, Landroid/media/videoeditor/Transition;->mDurationMs:J

    #@82
    move-wide/from16 v19, v0

    #@84
    add-long v17, v17, v19

    #@86
    move-wide/from16 v0, v17

    #@88
    long-to-int v4, v0

    #@89
    iput v4, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@8b
    .line 365
    add-int/lit8 v13, v13, 0x1

    #@8d
    goto :goto_6d

    #@8e
    .line 347
    .end local v10           #effectSettings_clip1:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;>;"
    .end local v11           #effectSettings_clip2:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;>;"
    .end local v13           #index:I
    :cond_8e
    if-eqz v6, :cond_2a

    #@90
    .line 348
    invoke-virtual {v6}, Landroid/media/videoeditor/MediaItem;->getNativeContext()Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@93
    move-result-object v2

    #@94
    move-object/from16 v0, p0

    #@96
    iput-object v2, v0, Landroid/media/videoeditor/Transition;->mNativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@98
    goto :goto_2a

    #@99
    .line 368
    .restart local v10       #effectSettings_clip1:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;>;"
    .restart local v11       #effectSettings_clip2:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;>;"
    .restart local v13       #index:I
    :cond_99
    invoke-interface {v10}, Ljava/util/List;->size()I

    #@9c
    move-result v2

    #@9d
    invoke-interface {v11}, Ljava/util/List;->size()I

    #@a0
    move-result v4

    #@a1
    add-int/2addr v2, v4

    #@a2
    new-array v2, v2, [Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;

    #@a4
    iput-object v2, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->effectSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;

    #@a6
    .line 371
    const/4 v12, 0x0

    #@a7
    .local v12, i:I
    const/4 v14, 0x0

    #@a8
    .line 372
    .local v14, j:I
    :goto_a8
    invoke-interface {v10}, Ljava/util/List;->size()I

    #@ab
    move-result v2

    #@ac
    if-ge v12, v2, :cond_bd

    #@ae
    .line 373
    iget-object v4, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->effectSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;

    #@b0
    invoke-interface {v10, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@b3
    move-result-object v2

    #@b4
    check-cast v2, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;

    #@b6
    aput-object v2, v4, v14

    #@b8
    .line 374
    add-int/lit8 v12, v12, 0x1

    #@ba
    .line 375
    add-int/lit8 v14, v14, 0x1

    #@bc
    goto :goto_a8

    #@bd
    .line 377
    :cond_bd
    const/4 v12, 0x0

    #@be
    .line 378
    :goto_be
    invoke-interface {v11}, Ljava/util/List;->size()I

    #@c1
    move-result v2

    #@c2
    if-ge v12, v2, :cond_199

    #@c4
    .line 379
    iget-object v4, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->effectSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;

    #@c6
    invoke-interface {v11, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@c9
    move-result-object v2

    #@ca
    check-cast v2, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;

    #@cc
    aput-object v2, v4, v14

    #@ce
    .line 380
    add-int/lit8 v12, v12, 0x1

    #@d0
    .line 381
    add-int/lit8 v14, v14, 0x1

    #@d2
    goto :goto_be

    #@d3
    .line 383
    .end local v10           #effectSettings_clip1:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;>;"
    .end local v11           #effectSettings_clip2:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;>;"
    .end local v12           #i:I
    .end local v13           #index:I
    .end local v14           #j:I
    :cond_d3
    if-nez v5, :cond_147

    #@d5
    if-eqz v6, :cond_147

    #@d7
    .line 385
    invoke-virtual {v6, v8}, Landroid/media/videoeditor/MediaItem;->generateBlankFrame(Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;)V

    #@da
    .line 386
    invoke-virtual {v6}, Landroid/media/videoeditor/MediaItem;->getClipSettings()Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@dd
    move-result-object v9

    #@de
    .line 387
    move-object/from16 v0, p0

    #@e0
    iget-wide v0, v0, Landroid/media/videoeditor/Transition;->mDurationMs:J

    #@e2
    move-wide/from16 v17, v0

    #@e4
    const-wide/16 v19, 0x32

    #@e6
    add-long v17, v17, v19

    #@e8
    move-wide/from16 v0, v17

    #@ea
    long-to-int v2, v0

    #@eb
    iput v2, v8, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutTime:I

    #@ed
    .line 388
    iget v2, v9, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->beginCutTime:I

    #@ef
    int-to-long v0, v2

    #@f0
    move-wide/from16 v17, v0

    #@f2
    move-object/from16 v0, p0

    #@f4
    iget-wide v0, v0, Landroid/media/videoeditor/Transition;->mDurationMs:J

    #@f6
    move-wide/from16 v19, v0

    #@f8
    add-long v17, v17, v19

    #@fa
    move-wide/from16 v0, v17

    #@fc
    long-to-int v2, v0

    #@fd
    iput v2, v9, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutTime:I

    #@ff
    .line 394
    const/4 v2, 0x2

    #@100
    move-object/from16 v0, p0

    #@102
    invoke-virtual {v0, v6, v9, v2}, Landroid/media/videoeditor/Transition;->isEffectandOverlayOverlapping(Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;I)Ljava/util/List;

    #@105
    move-result-object v11

    #@106
    .line 395
    .restart local v11       #effectSettings_clip2:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;>;"
    const/4 v13, 0x0

    #@107
    .restart local v13       #index:I
    :goto_107
    invoke-interface {v11}, Ljava/util/List;->size()I

    #@10a
    move-result v2

    #@10b
    if-ge v13, v2, :cond_128

    #@10d
    .line 396
    invoke-interface {v11, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@110
    move-result-object v2

    #@111
    check-cast v2, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;

    #@113
    iget v4, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@115
    int-to-long v0, v4

    #@116
    move-wide/from16 v17, v0

    #@118
    move-object/from16 v0, p0

    #@11a
    iget-wide v0, v0, Landroid/media/videoeditor/Transition;->mDurationMs:J

    #@11c
    move-wide/from16 v19, v0

    #@11e
    add-long v17, v17, v19

    #@120
    move-wide/from16 v0, v17

    #@122
    long-to-int v4, v0

    #@123
    iput v4, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@125
    .line 395
    add-int/lit8 v13, v13, 0x1

    #@127
    goto :goto_107

    #@128
    .line 398
    :cond_128
    invoke-interface {v11}, Ljava/util/List;->size()I

    #@12b
    move-result v2

    #@12c
    new-array v2, v2, [Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;

    #@12e
    iput-object v2, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->effectSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;

    #@130
    .line 399
    const/4 v12, 0x0

    #@131
    .restart local v12       #i:I
    const/4 v14, 0x0

    #@132
    .line 400
    .restart local v14       #j:I
    :goto_132
    invoke-interface {v11}, Ljava/util/List;->size()I

    #@135
    move-result v2

    #@136
    if-ge v12, v2, :cond_199

    #@138
    .line 401
    iget-object v4, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->effectSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;

    #@13a
    invoke-interface {v11, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@13d
    move-result-object v2

    #@13e
    check-cast v2, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;

    #@140
    aput-object v2, v4, v14

    #@142
    .line 402
    add-int/lit8 v12, v12, 0x1

    #@144
    .line 403
    add-int/lit8 v14, v14, 0x1

    #@146
    goto :goto_132

    #@147
    .line 405
    .end local v11           #effectSettings_clip2:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;>;"
    .end local v12           #i:I
    .end local v13           #index:I
    .end local v14           #j:I
    :cond_147
    if-eqz v5, :cond_199

    #@149
    if-nez v6, :cond_199

    #@14b
    .line 407
    invoke-virtual {v5}, Landroid/media/videoeditor/MediaItem;->getClipSettings()Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@14e
    move-result-object v8

    #@14f
    .line 408
    invoke-virtual {v5, v9}, Landroid/media/videoeditor/MediaItem;->generateBlankFrame(Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;)V

    #@152
    .line 409
    iget v2, v8, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutTime:I

    #@154
    int-to-long v0, v2

    #@155
    move-wide/from16 v17, v0

    #@157
    move-object/from16 v0, p0

    #@159
    iget-wide v0, v0, Landroid/media/videoeditor/Transition;->mDurationMs:J

    #@15b
    move-wide/from16 v19, v0

    #@15d
    sub-long v17, v17, v19

    #@15f
    move-wide/from16 v0, v17

    #@161
    long-to-int v2, v0

    #@162
    iput v2, v8, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->beginCutTime:I

    #@164
    .line 411
    move-object/from16 v0, p0

    #@166
    iget-wide v0, v0, Landroid/media/videoeditor/Transition;->mDurationMs:J

    #@168
    move-wide/from16 v17, v0

    #@16a
    const-wide/16 v19, 0x32

    #@16c
    add-long v17, v17, v19

    #@16e
    move-wide/from16 v0, v17

    #@170
    long-to-int v2, v0

    #@171
    iput v2, v9, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutTime:I

    #@173
    .line 416
    const/4 v2, 0x1

    #@174
    move-object/from16 v0, p0

    #@176
    invoke-virtual {v0, v5, v8, v2}, Landroid/media/videoeditor/Transition;->isEffectandOverlayOverlapping(Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;I)Ljava/util/List;

    #@179
    move-result-object v10

    #@17a
    .line 417
    .restart local v10       #effectSettings_clip1:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;>;"
    invoke-interface {v10}, Ljava/util/List;->size()I

    #@17d
    move-result v2

    #@17e
    new-array v2, v2, [Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;

    #@180
    iput-object v2, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->effectSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;

    #@182
    .line 418
    const/4 v12, 0x0

    #@183
    .restart local v12       #i:I
    const/4 v14, 0x0

    #@184
    .line 419
    .restart local v14       #j:I
    :goto_184
    invoke-interface {v10}, Ljava/util/List;->size()I

    #@187
    move-result v2

    #@188
    if-ge v12, v2, :cond_199

    #@18a
    .line 420
    iget-object v4, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->effectSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;

    #@18c
    invoke-interface {v10, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@18f
    move-result-object v2

    #@190
    check-cast v2, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;

    #@192
    aput-object v2, v4, v14

    #@194
    .line 421
    add-int/lit8 v12, v12, 0x1

    #@196
    .line 422
    add-int/lit8 v14, v14, 0x1

    #@198
    goto :goto_184

    #@199
    .line 426
    .end local v10           #effectSettings_clip1:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;>;"
    .end local v12           #i:I
    .end local v14           #j:I
    :cond_199
    const/4 v2, 0x2

    #@19a
    new-array v2, v2, [Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@19c
    iput-object v2, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@19e
    .line 427
    iget-object v2, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@1a0
    const/4 v4, 0x0

    #@1a1
    aput-object v8, v2, v4

    #@1a3
    .line 428
    iget-object v2, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@1a5
    const/4 v4, 0x1

    #@1a6
    aput-object v9, v2, v4

    #@1a8
    .line 429
    const/4 v2, 0x0

    #@1a9
    iput-object v2, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->backgroundMusicSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;

    #@1ab
    .line 430
    const/4 v2, 0x1

    #@1ac
    new-array v2, v2, [Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;

    #@1ae
    iput-object v2, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->transitionSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;

    #@1b0
    .line 431
    iget-object v2, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->transitionSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;

    #@1b2
    const/4 v4, 0x0

    #@1b3
    aput-object v16, v2, v4

    #@1b5
    .line 432
    move-object/from16 v0, p0

    #@1b7
    iget-object v2, v0, Landroid/media/videoeditor/Transition;->mNativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@1b9
    move-object/from16 v0, p0

    #@1bb
    iget-object v4, v0, Landroid/media/videoeditor/Transition;->mUniqueId:Ljava/lang/String;

    #@1bd
    move-object/from16 v7, p0

    #@1bf
    invoke-virtual/range {v2 .. v7}, Landroid/media/videoeditor/MediaArtistNativeHelper;->generateTransitionClip(Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;Ljava/lang/String;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/Transition;)Ljava/lang/String;

    #@1c2
    move-result-object v15

    #@1c3
    .line 434
    move-object/from16 v0, p0

    #@1c5
    invoke-virtual {v0, v15}, Landroid/media/videoeditor/Transition;->setFilename(Ljava/lang/String;)V

    #@1c8
    .line 435
    return-void
.end method

.method public getAfterMediaItem()Landroid/media/videoeditor/MediaItem;
    .registers 2

    #@0
    .prologue
    .line 156
    iget-object v0, p0, Landroid/media/videoeditor/Transition;->mAfterMediaItem:Landroid/media/videoeditor/MediaItem;

    #@2
    return-object v0
.end method

.method public getBeforeMediaItem()Landroid/media/videoeditor/MediaItem;
    .registers 2

    #@0
    .prologue
    .line 166
    iget-object v0, p0, Landroid/media/videoeditor/Transition;->mBeforeMediaItem:Landroid/media/videoeditor/MediaItem;

    #@2
    return-object v0
.end method

.method public getBehavior()I
    .registers 2

    #@0
    .prologue
    .line 216
    iget v0, p0, Landroid/media/videoeditor/Transition;->mBehavior:I

    #@2
    return v0
.end method

.method public getDuration()J
    .registers 3

    #@0
    .prologue
    .line 190
    iget-wide v0, p0, Landroid/media/videoeditor/Transition;->mDurationMs:J

    #@2
    return-wide v0
.end method

.method getFilename()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 449
    iget-object v0, p0, Landroid/media/videoeditor/Transition;->mFilename:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 147
    iget-object v0, p0, Landroid/media/videoeditor/Transition;->mUniqueId:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getMaximumDuration()J
    .registers 7

    #@0
    .prologue
    const-wide/16 v4, 0x2

    #@2
    .line 200
    iget-object v0, p0, Landroid/media/videoeditor/Transition;->mAfterMediaItem:Landroid/media/videoeditor/MediaItem;

    #@4
    if-nez v0, :cond_e

    #@6
    .line 201
    iget-object v0, p0, Landroid/media/videoeditor/Transition;->mBeforeMediaItem:Landroid/media/videoeditor/MediaItem;

    #@8
    invoke-virtual {v0}, Landroid/media/videoeditor/MediaItem;->getTimelineDuration()J

    #@b
    move-result-wide v0

    #@c
    div-long/2addr v0, v4

    #@d
    .line 205
    :goto_d
    return-wide v0

    #@e
    .line 202
    :cond_e
    iget-object v0, p0, Landroid/media/videoeditor/Transition;->mBeforeMediaItem:Landroid/media/videoeditor/MediaItem;

    #@10
    if-nez v0, :cond_1a

    #@12
    .line 203
    iget-object v0, p0, Landroid/media/videoeditor/Transition;->mAfterMediaItem:Landroid/media/videoeditor/MediaItem;

    #@14
    invoke-virtual {v0}, Landroid/media/videoeditor/MediaItem;->getTimelineDuration()J

    #@17
    move-result-wide v0

    #@18
    div-long/2addr v0, v4

    #@19
    goto :goto_d

    #@1a
    .line 205
    :cond_1a
    iget-object v0, p0, Landroid/media/videoeditor/Transition;->mAfterMediaItem:Landroid/media/videoeditor/MediaItem;

    #@1c
    invoke-virtual {v0}, Landroid/media/videoeditor/MediaItem;->getTimelineDuration()J

    #@1f
    move-result-wide v0

    #@20
    iget-object v2, p0, Landroid/media/videoeditor/Transition;->mBeforeMediaItem:Landroid/media/videoeditor/MediaItem;

    #@22
    invoke-virtual {v2}, Landroid/media/videoeditor/MediaItem;->getTimelineDuration()J

    #@25
    move-result-wide v2

    #@26
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    #@29
    move-result-wide v0

    #@2a
    div-long/2addr v0, v4

    #@2b
    goto :goto_d
.end method

.method getTransitionSettings()Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 226
    const/4 v0, 0x0

    #@3
    .line 227
    .local v0, transitionAlpha:Landroid/media/videoeditor/TransitionAlpha;
    const/4 v4, 0x0

    #@4
    .line 228
    .local v4, transitionSliding:Landroid/media/videoeditor/TransitionSliding;
    const/4 v1, 0x0

    #@5
    .line 229
    .local v1, transitionCrossfade:Landroid/media/videoeditor/TransitionCrossfade;
    const/4 v2, 0x0

    #@6
    .line 230
    .local v2, transitionFadeBlack:Landroid/media/videoeditor/TransitionFadeBlack;
    const/4 v3, 0x0

    #@7
    .line 231
    .local v3, transitionSetting:Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;
    new-instance v3, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;

    #@9
    .end local v3           #transitionSetting:Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;
    invoke-direct {v3}, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;-><init>()V

    #@c
    .line 232
    .restart local v3       #transitionSetting:Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;
    invoke-virtual {p0}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@f
    move-result-wide v5

    #@10
    long-to-int v5, v5

    #@11
    iput v5, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;->duration:I

    #@13
    .line 233
    instance-of v5, p0, Landroid/media/videoeditor/TransitionAlpha;

    #@15
    if-eqz v5, :cond_5e

    #@17
    move-object v0, p0

    #@18
    .line 234
    check-cast v0, Landroid/media/videoeditor/TransitionAlpha;

    #@1a
    .line 235
    const/16 v5, 0x101

    #@1c
    iput v5, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;->videoTransitionType:I

    #@1e
    .line 236
    iput v8, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;->audioTransitionType:I

    #@20
    .line 237
    iget-object v5, p0, Landroid/media/videoeditor/Transition;->mNativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@22
    invoke-virtual {v0}, Landroid/media/videoeditor/TransitionAlpha;->getBehavior()I

    #@25
    move-result v6

    #@26
    invoke-virtual {v5, v6}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getVideoTransitionBehaviour(I)I

    #@29
    move-result v5

    #@2a
    iput v5, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;->transitionBehaviour:I

    #@2c
    .line 239
    new-instance v5, Landroid/media/videoeditor/MediaArtistNativeHelper$AlphaMagicSettings;

    #@2e
    invoke-direct {v5}, Landroid/media/videoeditor/MediaArtistNativeHelper$AlphaMagicSettings;-><init>()V

    #@31
    iput-object v5, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;->alphaSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AlphaMagicSettings;

    #@33
    .line 240
    iput-object v7, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;->slideSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$SlideTransitionSettings;

    #@35
    .line 241
    iget-object v5, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;->alphaSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AlphaMagicSettings;

    #@37
    invoke-virtual {v0}, Landroid/media/videoeditor/TransitionAlpha;->getPNGMaskFilename()Ljava/lang/String;

    #@3a
    move-result-object v6

    #@3b
    iput-object v6, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$AlphaMagicSettings;->file:Ljava/lang/String;

    #@3d
    .line 242
    iget-object v5, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;->alphaSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AlphaMagicSettings;

    #@3f
    invoke-virtual {v0}, Landroid/media/videoeditor/TransitionAlpha;->getBlendingPercent()I

    #@42
    move-result v6

    #@43
    iput v6, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$AlphaMagicSettings;->blendingPercent:I

    #@45
    .line 243
    iget-object v5, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;->alphaSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AlphaMagicSettings;

    #@47
    invoke-virtual {v0}, Landroid/media/videoeditor/TransitionAlpha;->isInvert()Z

    #@4a
    move-result v6

    #@4b
    iput-boolean v6, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$AlphaMagicSettings;->invertRotation:Z

    #@4d
    .line 244
    iget-object v5, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;->alphaSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AlphaMagicSettings;

    #@4f
    invoke-virtual {v0}, Landroid/media/videoeditor/TransitionAlpha;->getRGBFileWidth()I

    #@52
    move-result v6

    #@53
    iput v6, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$AlphaMagicSettings;->rgbWidth:I

    #@55
    .line 245
    iget-object v5, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;->alphaSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AlphaMagicSettings;

    #@57
    invoke-virtual {v0}, Landroid/media/videoeditor/TransitionAlpha;->getRGBFileHeight()I

    #@5a
    move-result v6

    #@5b
    iput v6, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$AlphaMagicSettings;->rgbHeight:I

    #@5d
    .line 275
    :cond_5d
    :goto_5d
    return-object v3

    #@5e
    .line 247
    :cond_5e
    instance-of v5, p0, Landroid/media/videoeditor/TransitionSliding;

    #@60
    if-eqz v5, :cond_8f

    #@62
    move-object v4, p0

    #@63
    .line 248
    check-cast v4, Landroid/media/videoeditor/TransitionSliding;

    #@65
    .line 249
    const/16 v5, 0x102

    #@67
    iput v5, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;->videoTransitionType:I

    #@69
    .line 250
    iput v8, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;->audioTransitionType:I

    #@6b
    .line 251
    iget-object v5, p0, Landroid/media/videoeditor/Transition;->mNativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@6d
    invoke-virtual {v4}, Landroid/media/videoeditor/TransitionSliding;->getBehavior()I

    #@70
    move-result v6

    #@71
    invoke-virtual {v5, v6}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getVideoTransitionBehaviour(I)I

    #@74
    move-result v5

    #@75
    iput v5, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;->transitionBehaviour:I

    #@77
    .line 253
    iput-object v7, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;->alphaSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AlphaMagicSettings;

    #@79
    .line 254
    new-instance v5, Landroid/media/videoeditor/MediaArtistNativeHelper$SlideTransitionSettings;

    #@7b
    invoke-direct {v5}, Landroid/media/videoeditor/MediaArtistNativeHelper$SlideTransitionSettings;-><init>()V

    #@7e
    iput-object v5, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;->slideSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$SlideTransitionSettings;

    #@80
    .line 255
    iget-object v5, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;->slideSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$SlideTransitionSettings;

    #@82
    iget-object v6, p0, Landroid/media/videoeditor/Transition;->mNativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@84
    invoke-virtual {v4}, Landroid/media/videoeditor/TransitionSliding;->getDirection()I

    #@87
    move-result v7

    #@88
    invoke-virtual {v6, v7}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getSlideSettingsDirection(I)I

    #@8b
    move-result v6

    #@8c
    iput v6, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$SlideTransitionSettings;->direction:I

    #@8e
    goto :goto_5d

    #@8f
    .line 257
    :cond_8f
    instance-of v5, p0, Landroid/media/videoeditor/TransitionCrossfade;

    #@91
    if-eqz v5, :cond_ab

    #@93
    move-object v1, p0

    #@94
    .line 258
    check-cast v1, Landroid/media/videoeditor/TransitionCrossfade;

    #@96
    .line 259
    iput v8, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;->videoTransitionType:I

    #@98
    .line 260
    iput v8, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;->audioTransitionType:I

    #@9a
    .line 261
    iget-object v5, p0, Landroid/media/videoeditor/Transition;->mNativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@9c
    invoke-virtual {v1}, Landroid/media/videoeditor/TransitionCrossfade;->getBehavior()I

    #@9f
    move-result v6

    #@a0
    invoke-virtual {v5, v6}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getVideoTransitionBehaviour(I)I

    #@a3
    move-result v5

    #@a4
    iput v5, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;->transitionBehaviour:I

    #@a6
    .line 263
    iput-object v7, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;->alphaSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AlphaMagicSettings;

    #@a8
    .line 264
    iput-object v7, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;->slideSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$SlideTransitionSettings;

    #@aa
    goto :goto_5d

    #@ab
    .line 265
    :cond_ab
    instance-of v5, p0, Landroid/media/videoeditor/TransitionFadeBlack;

    #@ad
    if-eqz v5, :cond_5d

    #@af
    move-object v2, p0

    #@b0
    .line 266
    check-cast v2, Landroid/media/videoeditor/TransitionFadeBlack;

    #@b2
    .line 267
    const/16 v5, 0x103

    #@b4
    iput v5, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;->videoTransitionType:I

    #@b6
    .line 268
    iput v8, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;->audioTransitionType:I

    #@b8
    .line 269
    iget-object v5, p0, Landroid/media/videoeditor/Transition;->mNativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@ba
    invoke-virtual {v2}, Landroid/media/videoeditor/TransitionFadeBlack;->getBehavior()I

    #@bd
    move-result v6

    #@be
    invoke-virtual {v5, v6}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getVideoTransitionBehaviour(I)I

    #@c1
    move-result v5

    #@c2
    iput v5, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;->transitionBehaviour:I

    #@c4
    .line 271
    iput-object v7, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;->alphaSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AlphaMagicSettings;

    #@c6
    .line 272
    iput-object v7, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;->slideSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$SlideTransitionSettings;

    #@c8
    goto :goto_5d
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 487
    iget-object v0, p0, Landroid/media/videoeditor/Transition;->mUniqueId:Ljava/lang/String;

    #@2
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method invalidate()V
    .registers 3

    #@0
    .prologue
    .line 456
    iget-object v0, p0, Landroid/media/videoeditor/Transition;->mFilename:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_11

    #@4
    .line 457
    new-instance v0, Ljava/io/File;

    #@6
    iget-object v1, p0, Landroid/media/videoeditor/Transition;->mFilename:Ljava/lang/String;

    #@8
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@b
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@e
    .line 458
    const/4 v0, 0x0

    #@f
    iput-object v0, p0, Landroid/media/videoeditor/Transition;->mFilename:Ljava/lang/String;

    #@11
    .line 460
    :cond_11
    return-void
.end method

.method isEffectandOverlayOverlapping(Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;I)Ljava/util/List;
    .registers 14
    .parameter "m"
    .parameter "clipSettings"
    .parameter "clipNo"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/media/videoeditor/MediaItem;",
            "Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;",
            "I)",
            "Ljava/util/List",
            "<",
            "Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 294
    new-instance v1, Ljava/util/ArrayList;

    #@2
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 297
    .local v1, effectSettings:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;>;"
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getAllOverlays()Ljava/util/List;

    #@8
    move-result-object v5

    #@9
    .line 298
    .local v5, overlays:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Overlay;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@c
    move-result-object v3

    #@d
    .local v3, i$:Ljava/util/Iterator;
    :cond_d
    :goto_d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@10
    move-result v7

    #@11
    if-eqz v7, :cond_32

    #@13
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@16
    move-result-object v4

    #@17
    check-cast v4, Landroid/media/videoeditor/Overlay;

    #@19
    .line 299
    .local v4, overlay:Landroid/media/videoeditor/Overlay;
    iget-object v7, p0, Landroid/media/videoeditor/Transition;->mNativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@1b
    check-cast v4, Landroid/media/videoeditor/OverlayFrame;

    #@1d
    .end local v4           #overlay:Landroid/media/videoeditor/Overlay;
    invoke-virtual {v7, v4}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getOverlaySettings(Landroid/media/videoeditor/OverlayFrame;)Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;

    #@20
    move-result-object v6

    #@21
    .line 300
    .local v6, tmpEffectSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;
    iget-object v7, p0, Landroid/media/videoeditor/Transition;->mNativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@23
    iget v8, p2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->beginCutTime:I

    #@25
    iget v9, p2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutTime:I

    #@27
    invoke-virtual {v7, v6, v8, v9}, Landroid/media/videoeditor/MediaArtistNativeHelper;->adjustEffectsStartTimeAndDuration(Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;II)V

    #@2a
    .line 302
    iget v7, v6, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->duration:I

    #@2c
    if-eqz v7, :cond_d

    #@2e
    .line 303
    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@31
    goto :goto_d

    #@32
    .line 307
    .end local v6           #tmpEffectSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;
    :cond_32
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getAllEffects()Ljava/util/List;

    #@35
    move-result-object v2

    #@36
    .line 308
    .local v2, effects:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Effect;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@39
    move-result-object v3

    #@3a
    :cond_3a
    :goto_3a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@3d
    move-result v7

    #@3e
    if-eqz v7, :cond_76

    #@40
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@43
    move-result-object v0

    #@44
    check-cast v0, Landroid/media/videoeditor/Effect;

    #@46
    .line 309
    .local v0, effect:Landroid/media/videoeditor/Effect;
    instance-of v7, v0, Landroid/media/videoeditor/EffectColor;

    #@48
    if-eqz v7, :cond_3a

    #@4a
    .line 310
    iget-object v7, p0, Landroid/media/videoeditor/Transition;->mNativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@4c
    check-cast v0, Landroid/media/videoeditor/EffectColor;

    #@4e
    .end local v0           #effect:Landroid/media/videoeditor/Effect;
    invoke-virtual {v7, v0}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getEffectSettings(Landroid/media/videoeditor/EffectColor;)Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;

    #@51
    move-result-object v6

    #@52
    .line 311
    .restart local v6       #tmpEffectSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;
    iget-object v7, p0, Landroid/media/videoeditor/Transition;->mNativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@54
    iget v8, p2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->beginCutTime:I

    #@56
    iget v9, p2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutTime:I

    #@58
    invoke-virtual {v7, v6, v8, v9}, Landroid/media/videoeditor/MediaArtistNativeHelper;->adjustEffectsStartTimeAndDuration(Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;II)V

    #@5b
    .line 313
    iget v7, v6, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->duration:I

    #@5d
    if-eqz v7, :cond_3a

    #@5f
    .line 314
    instance-of v7, p1, Landroid/media/videoeditor/MediaVideoItem;

    #@61
    if-eqz v7, :cond_72

    #@63
    .line 315
    iget-object v8, p0, Landroid/media/videoeditor/Transition;->mNativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@65
    move-object v7, p1

    #@66
    check-cast v7, Landroid/media/videoeditor/MediaVideoItem;

    #@68
    invoke-virtual {v7}, Landroid/media/videoeditor/MediaVideoItem;->getFps()I

    #@6b
    move-result v7

    #@6c
    invoke-virtual {v8, v7}, Landroid/media/videoeditor/MediaArtistNativeHelper;->GetClosestVideoFrameRate(I)I

    #@6f
    move-result v7

    #@70
    iput v7, v6, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->fiftiesFrameRate:I

    #@72
    .line 318
    :cond_72
    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@75
    goto :goto_3a

    #@76
    .line 323
    .end local v6           #tmpEffectSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;
    :cond_76
    return-object v1
.end method

.method isGenerated()Z
    .registers 2

    #@0
    .prologue
    .line 468
    iget-object v0, p0, Landroid/media/videoeditor/Transition;->mFilename:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public setDuration(J)V
    .registers 5
    .parameter "durationMs"

    #@0
    .prologue
    .line 175
    invoke-virtual {p0}, Landroid/media/videoeditor/Transition;->getMaximumDuration()J

    #@3
    move-result-wide v0

    #@4
    cmp-long v0, p1, v0

    #@6
    if-lez v0, :cond_10

    #@8
    .line 176
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@a
    const-string v1, "The duration is too large"

    #@c
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 179
    :cond_10
    iput-wide p1, p0, Landroid/media/videoeditor/Transition;->mDurationMs:J

    #@12
    .line 180
    invoke-virtual {p0}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@15
    .line 181
    iget-object v0, p0, Landroid/media/videoeditor/Transition;->mNativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@17
    const/4 v1, 0x1

    #@18
    invoke-virtual {v0, v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@1b
    .line 182
    return-void
.end method

.method setFilename(Ljava/lang/String;)V
    .registers 2
    .parameter "filename"

    #@0
    .prologue
    .line 442
    iput-object p1, p0, Landroid/media/videoeditor/Transition;->mFilename:Ljava/lang/String;

    #@2
    .line 443
    return-void
.end method
