.class public final Landroid/media/videoeditor/VideoEditor$OverlayData;
.super Ljava/lang/Object;
.source "VideoEditor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/videoeditor/VideoEditor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OverlayData"
.end annotation


# static fields
.field private static final sResizePaint:Landroid/graphics/Paint;


# instance fields
.field private mClear:Z

.field private mOverlayBitmap:Landroid/graphics/Bitmap;

.field private mRenderingMode:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 173
    new-instance v0, Landroid/graphics/Paint;

    #@2
    const/4 v1, 0x2

    #@3
    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    #@6
    sput-object v0, Landroid/media/videoeditor/VideoEditor$OverlayData;->sResizePaint:Landroid/graphics/Paint;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 178
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 179
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mOverlayBitmap:Landroid/graphics/Bitmap;

    #@6
    .line 180
    const/4 v0, 0x2

    #@7
    iput v0, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mRenderingMode:I

    #@9
    .line 181
    const/4 v0, 0x0

    #@a
    iput-boolean v0, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mClear:Z

    #@c
    .line 182
    return-void
.end method


# virtual methods
.method public needsRendering()Z
    .registers 2

    #@0
    .prologue
    .line 200
    iget-boolean v0, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mClear:Z

    #@2
    if-nez v0, :cond_8

    #@4
    iget-object v0, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mOverlayBitmap:Landroid/graphics/Bitmap;

    #@6
    if-eqz v0, :cond_a

    #@8
    :cond_8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public release()V
    .registers 2

    #@0
    .prologue
    .line 188
    iget-object v0, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mOverlayBitmap:Landroid/graphics/Bitmap;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 189
    iget-object v0, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mOverlayBitmap:Landroid/graphics/Bitmap;

    #@6
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    #@9
    .line 190
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mOverlayBitmap:Landroid/graphics/Bitmap;

    #@c
    .line 192
    :cond_c
    return-void
.end method

.method public renderOverlay(Landroid/graphics/Bitmap;)V
    .registers 16
    .parameter "destBitmap"

    #@0
    .prologue
    const/4 v13, 0x0

    #@1
    .line 230
    iget-boolean v11, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mClear:Z

    #@3
    if-eqz v11, :cond_9

    #@5
    .line 231
    invoke-virtual {p1, v13}, Landroid/graphics/Bitmap;->eraseColor(I)V

    #@8
    .line 315
    :cond_8
    :goto_8
    return-void

    #@9
    .line 232
    :cond_9
    iget-object v11, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mOverlayBitmap:Landroid/graphics/Bitmap;

    #@b
    if-eqz v11, :cond_8

    #@d
    .line 233
    new-instance v7, Landroid/graphics/Canvas;

    #@f
    invoke-direct {v7, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    #@12
    .line 236
    .local v7, overlayCanvas:Landroid/graphics/Canvas;
    iget v11, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mRenderingMode:I

    #@14
    packed-switch v11, :pswitch_data_156

    #@17
    .line 306
    new-instance v11, Ljava/lang/IllegalStateException;

    #@19
    new-instance v12, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v13, "Rendering mode: "

    #@20
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v12

    #@24
    iget v13, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mRenderingMode:I

    #@26
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v12

    #@2a
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v12

    #@2e
    invoke-direct {v11, v12}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@31
    throw v11

    #@32
    .line 238
    :pswitch_32
    new-instance v3, Landroid/graphics/Rect;

    #@34
    invoke-virtual {v7}, Landroid/graphics/Canvas;->getWidth()I

    #@37
    move-result v11

    #@38
    invoke-virtual {v7}, Landroid/graphics/Canvas;->getHeight()I

    #@3b
    move-result v12

    #@3c
    invoke-direct {v3, v13, v13, v11, v12}, Landroid/graphics/Rect;-><init>(IIII)V

    #@3f
    .line 240
    .local v3, destRect:Landroid/graphics/Rect;
    new-instance v9, Landroid/graphics/Rect;

    #@41
    iget-object v11, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mOverlayBitmap:Landroid/graphics/Bitmap;

    #@43
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    #@46
    move-result v11

    #@47
    iget-object v12, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mOverlayBitmap:Landroid/graphics/Bitmap;

    #@49
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    #@4c
    move-result v12

    #@4d
    invoke-direct {v9, v13, v13, v11, v12}, Landroid/graphics/Rect;-><init>(IIII)V

    #@50
    .line 310
    .local v9, srcRect:Landroid/graphics/Rect;
    :goto_50
    invoke-virtual {p1, v13}, Landroid/graphics/Bitmap;->eraseColor(I)V

    #@53
    .line 311
    iget-object v11, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mOverlayBitmap:Landroid/graphics/Bitmap;

    #@55
    sget-object v12, Landroid/media/videoeditor/VideoEditor$OverlayData;->sResizePaint:Landroid/graphics/Paint;

    #@57
    invoke-virtual {v7, v11, v9, v3, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    #@5a
    .line 313
    iget-object v11, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mOverlayBitmap:Landroid/graphics/Bitmap;

    #@5c
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->recycle()V

    #@5f
    goto :goto_8

    #@60
    .line 248
    .end local v3           #destRect:Landroid/graphics/Rect;
    .end local v9           #srcRect:Landroid/graphics/Rect;
    :pswitch_60
    iget-object v11, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mOverlayBitmap:Landroid/graphics/Bitmap;

    #@62
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    #@65
    move-result v11

    #@66
    int-to-float v11, v11

    #@67
    iget-object v12, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mOverlayBitmap:Landroid/graphics/Bitmap;

    #@69
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    #@6c
    move-result v12

    #@6d
    int-to-float v12, v12

    #@6e
    div-float v1, v11, v12

    #@70
    .line 251
    .local v1, aROverlayImage:F
    invoke-virtual {v7}, Landroid/graphics/Canvas;->getWidth()I

    #@73
    move-result v11

    #@74
    int-to-float v11, v11

    #@75
    invoke-virtual {v7}, Landroid/graphics/Canvas;->getHeight()I

    #@78
    move-result v12

    #@79
    int-to-float v12, v12

    #@7a
    div-float v0, v11, v12

    #@7c
    .line 254
    .local v0, aRCanvas:F
    cmpl-float v11, v1, v0

    #@7e
    if-lez v11, :cond_b8

    #@80
    .line 255
    invoke-virtual {v7}, Landroid/graphics/Canvas;->getWidth()I

    #@83
    move-result v11

    #@84
    iget-object v12, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mOverlayBitmap:Landroid/graphics/Bitmap;

    #@86
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    #@89
    move-result v12

    #@8a
    mul-int/2addr v11, v12

    #@8b
    iget-object v12, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mOverlayBitmap:Landroid/graphics/Bitmap;

    #@8d
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    #@90
    move-result v12

    #@91
    div-int v5, v11, v12

    #@93
    .line 257
    .local v5, newHeight:I
    const/4 v4, 0x0

    #@94
    .line 258
    .local v4, left:I
    invoke-virtual {v7}, Landroid/graphics/Canvas;->getHeight()I

    #@97
    move-result v11

    #@98
    sub-int/2addr v11, v5

    #@99
    div-int/lit8 v10, v11, 0x2

    #@9b
    .line 259
    .local v10, top:I
    invoke-virtual {v7}, Landroid/graphics/Canvas;->getWidth()I

    #@9e
    move-result v8

    #@9f
    .line 260
    .local v8, right:I
    add-int v2, v10, v5

    #@a1
    .line 270
    .end local v5           #newHeight:I
    .local v2, bottom:I
    :goto_a1
    new-instance v3, Landroid/graphics/Rect;

    #@a3
    invoke-direct {v3, v4, v10, v8, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    #@a6
    .line 271
    .restart local v3       #destRect:Landroid/graphics/Rect;
    new-instance v9, Landroid/graphics/Rect;

    #@a8
    iget-object v11, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mOverlayBitmap:Landroid/graphics/Bitmap;

    #@aa
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    #@ad
    move-result v11

    #@ae
    iget-object v12, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mOverlayBitmap:Landroid/graphics/Bitmap;

    #@b0
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    #@b3
    move-result v12

    #@b4
    invoke-direct {v9, v13, v13, v11, v12}, Landroid/graphics/Rect;-><init>(IIII)V

    #@b7
    .line 272
    .restart local v9       #srcRect:Landroid/graphics/Rect;
    goto :goto_50

    #@b8
    .line 262
    .end local v2           #bottom:I
    .end local v3           #destRect:Landroid/graphics/Rect;
    .end local v4           #left:I
    .end local v8           #right:I
    .end local v9           #srcRect:Landroid/graphics/Rect;
    .end local v10           #top:I
    :cond_b8
    invoke-virtual {v7}, Landroid/graphics/Canvas;->getHeight()I

    #@bb
    move-result v11

    #@bc
    iget-object v12, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mOverlayBitmap:Landroid/graphics/Bitmap;

    #@be
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    #@c1
    move-result v12

    #@c2
    mul-int/2addr v11, v12

    #@c3
    iget-object v12, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mOverlayBitmap:Landroid/graphics/Bitmap;

    #@c5
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    #@c8
    move-result v12

    #@c9
    div-int v6, v11, v12

    #@cb
    .line 264
    .local v6, newWidth:I
    invoke-virtual {v7}, Landroid/graphics/Canvas;->getWidth()I

    #@ce
    move-result v11

    #@cf
    sub-int/2addr v11, v6

    #@d0
    div-int/lit8 v4, v11, 0x2

    #@d2
    .line 265
    .restart local v4       #left:I
    const/4 v10, 0x0

    #@d3
    .line 266
    .restart local v10       #top:I
    add-int v8, v4, v6

    #@d5
    .line 267
    .restart local v8       #right:I
    invoke-virtual {v7}, Landroid/graphics/Canvas;->getHeight()I

    #@d8
    move-result v2

    #@d9
    .restart local v2       #bottom:I
    goto :goto_a1

    #@da
    .line 279
    .end local v0           #aRCanvas:F
    .end local v1           #aROverlayImage:F
    .end local v2           #bottom:I
    .end local v4           #left:I
    .end local v6           #newWidth:I
    .end local v8           #right:I
    .end local v10           #top:I
    :pswitch_da
    iget-object v11, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mOverlayBitmap:Landroid/graphics/Bitmap;

    #@dc
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    #@df
    move-result v11

    #@e0
    int-to-float v11, v11

    #@e1
    iget-object v12, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mOverlayBitmap:Landroid/graphics/Bitmap;

    #@e3
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    #@e6
    move-result v12

    #@e7
    int-to-float v12, v12

    #@e8
    div-float v1, v11, v12

    #@ea
    .line 281
    .restart local v1       #aROverlayImage:F
    invoke-virtual {v7}, Landroid/graphics/Canvas;->getWidth()I

    #@ed
    move-result v11

    #@ee
    int-to-float v11, v11

    #@ef
    invoke-virtual {v7}, Landroid/graphics/Canvas;->getHeight()I

    #@f2
    move-result v12

    #@f3
    int-to-float v12, v12

    #@f4
    div-float v0, v11, v12

    #@f6
    .line 283
    .restart local v0       #aRCanvas:F
    cmpg-float v11, v1, v0

    #@f8
    if-gez v11, :cond_131

    #@fa
    .line 284
    iget-object v11, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mOverlayBitmap:Landroid/graphics/Bitmap;

    #@fc
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    #@ff
    move-result v11

    #@100
    invoke-virtual {v7}, Landroid/graphics/Canvas;->getHeight()I

    #@103
    move-result v12

    #@104
    mul-int/2addr v11, v12

    #@105
    invoke-virtual {v7}, Landroid/graphics/Canvas;->getWidth()I

    #@108
    move-result v12

    #@109
    div-int v5, v11, v12

    #@10b
    .line 287
    .restart local v5       #newHeight:I
    const/4 v4, 0x0

    #@10c
    .line 288
    .restart local v4       #left:I
    iget-object v11, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mOverlayBitmap:Landroid/graphics/Bitmap;

    #@10e
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    #@111
    move-result v11

    #@112
    sub-int/2addr v11, v5

    #@113
    div-int/lit8 v10, v11, 0x2

    #@115
    .line 289
    .restart local v10       #top:I
    iget-object v11, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mOverlayBitmap:Landroid/graphics/Bitmap;

    #@117
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    #@11a
    move-result v8

    #@11b
    .line 290
    .restart local v8       #right:I
    add-int v2, v10, v5

    #@11d
    .line 300
    .end local v5           #newHeight:I
    .restart local v2       #bottom:I
    :goto_11d
    new-instance v9, Landroid/graphics/Rect;

    #@11f
    invoke-direct {v9, v4, v10, v8, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    #@122
    .line 301
    .restart local v9       #srcRect:Landroid/graphics/Rect;
    new-instance v3, Landroid/graphics/Rect;

    #@124
    invoke-virtual {v7}, Landroid/graphics/Canvas;->getWidth()I

    #@127
    move-result v11

    #@128
    invoke-virtual {v7}, Landroid/graphics/Canvas;->getHeight()I

    #@12b
    move-result v12

    #@12c
    invoke-direct {v3, v13, v13, v11, v12}, Landroid/graphics/Rect;-><init>(IIII)V

    #@12f
    .line 302
    .restart local v3       #destRect:Landroid/graphics/Rect;
    goto/16 :goto_50

    #@131
    .line 292
    .end local v2           #bottom:I
    .end local v3           #destRect:Landroid/graphics/Rect;
    .end local v4           #left:I
    .end local v8           #right:I
    .end local v9           #srcRect:Landroid/graphics/Rect;
    .end local v10           #top:I
    :cond_131
    iget-object v11, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mOverlayBitmap:Landroid/graphics/Bitmap;

    #@133
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    #@136
    move-result v11

    #@137
    invoke-virtual {v7}, Landroid/graphics/Canvas;->getWidth()I

    #@13a
    move-result v12

    #@13b
    mul-int/2addr v11, v12

    #@13c
    invoke-virtual {v7}, Landroid/graphics/Canvas;->getHeight()I

    #@13f
    move-result v12

    #@140
    div-int v6, v11, v12

    #@142
    .line 294
    .restart local v6       #newWidth:I
    iget-object v11, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mOverlayBitmap:Landroid/graphics/Bitmap;

    #@144
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    #@147
    move-result v11

    #@148
    sub-int/2addr v11, v6

    #@149
    div-int/lit8 v4, v11, 0x2

    #@14b
    .line 295
    .restart local v4       #left:I
    const/4 v10, 0x0

    #@14c
    .line 296
    .restart local v10       #top:I
    add-int v8, v4, v6

    #@14e
    .line 297
    .restart local v8       #right:I
    iget-object v11, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mOverlayBitmap:Landroid/graphics/Bitmap;

    #@150
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    #@153
    move-result v2

    #@154
    .restart local v2       #bottom:I
    goto :goto_11d

    #@155
    .line 236
    nop

    #@156
    :pswitch_data_156
    .packed-switch 0x0
        :pswitch_32
        :pswitch_da
        :pswitch_60
    .end packed-switch
.end method

.method set(Landroid/graphics/Bitmap;I)V
    .registers 4
    .parameter "overlayBitmap"
    .parameter "renderingMode"

    #@0
    .prologue
    .line 210
    iput-object p1, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mOverlayBitmap:Landroid/graphics/Bitmap;

    #@2
    .line 211
    iput p2, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mRenderingMode:I

    #@4
    .line 212
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mClear:Z

    #@7
    .line 213
    return-void
.end method

.method setClear()V
    .registers 2

    #@0
    .prologue
    .line 219
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/media/videoeditor/VideoEditor$OverlayData;->mClear:Z

    #@3
    .line 220
    return-void
.end method
