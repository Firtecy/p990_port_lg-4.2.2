.class public Landroid/media/videoeditor/TransitionAlpha;
.super Landroid/media/videoeditor/Transition;
.source "TransitionAlpha.java"


# instance fields
.field private final mBlendingPercent:I

.field private mHeight:I

.field private final mIsInvert:Z

.field private final mMaskFilename:Ljava/lang/String;

.field private mRGBMaskFile:Ljava/lang/String;

.field private mWidth:I


# direct methods
.method private constructor <init>()V
    .registers 11

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 67
    const-wide/16 v4, 0x0

    #@4
    move-object v0, p0

    #@5
    move-object v2, v1

    #@6
    move-object v3, v1

    #@7
    move-object v7, v1

    #@8
    move v8, v6

    #@9
    move v9, v6

    #@a
    invoke-direct/range {v0 .. v9}, Landroid/media/videoeditor/TransitionAlpha;-><init>(Ljava/lang/String;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;JILjava/lang/String;IZ)V

    #@d
    .line 68
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;JILjava/lang/String;IZ)V
    .registers 27
    .parameter "transitionId"
    .parameter "afterMediaItem"
    .parameter "beforeMediaItem"
    .parameter "durationMs"
    .parameter "behavior"
    .parameter "maskFilename"
    .parameter "blendingPercent"
    .parameter "invert"

    #@0
    .prologue
    .line 93
    invoke-direct/range {p0 .. p6}, Landroid/media/videoeditor/Transition;-><init>(Ljava/lang/String;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;JI)V

    #@3
    .line 98
    new-instance v12, Landroid/graphics/BitmapFactory$Options;

    #@5
    invoke-direct {v12}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    #@8
    .line 99
    .local v12, dbo:Landroid/graphics/BitmapFactory$Options;
    const/4 v4, 0x1

    #@9
    iput-boolean v4, v12, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    #@b
    .line 100
    new-instance v4, Ljava/io/File;

    #@d
    move-object/from16 v0, p7

    #@f
    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@12
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    #@15
    move-result v4

    #@16
    if-nez v4, :cond_33

    #@18
    .line 101
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@1a
    new-instance v5, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v6, "File not Found "

    #@21
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v5

    #@25
    move-object/from16 v0, p7

    #@27
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v5

    #@2b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v5

    #@2f
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@32
    throw v4

    #@33
    .line 102
    :cond_33
    move-object/from16 v0, p7

    #@35
    invoke-static {v0, v12}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@38
    .line 104
    iget v4, v12, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    #@3a
    move-object/from16 v0, p0

    #@3c
    iput v4, v0, Landroid/media/videoeditor/TransitionAlpha;->mWidth:I

    #@3e
    .line 105
    iget v4, v12, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    #@40
    move-object/from16 v0, p0

    #@42
    iput v4, v0, Landroid/media/videoeditor/TransitionAlpha;->mHeight:I

    #@44
    .line 107
    new-instance v4, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    move-object/from16 v0, p0

    #@4b
    iget-object v5, v0, Landroid/media/videoeditor/Transition;->mNativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@4d
    invoke-virtual {v5}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getProjectPath()Ljava/lang/String;

    #@50
    move-result-object v5

    #@51
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v4

    #@55
    const-string v5, "/"

    #@57
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v4

    #@5b
    const-string/jumbo v5, "mask"

    #@5e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v4

    #@62
    move-object/from16 v0, p1

    #@64
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v4

    #@68
    const-string v5, ".rgb"

    #@6a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v4

    #@6e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v4

    #@72
    const/4 v5, 0x0

    #@73
    new-array v5, v5, [Ljava/lang/Object;

    #@75
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@78
    move-result-object v4

    #@79
    move-object/from16 v0, p0

    #@7b
    iput-object v4, v0, Landroid/media/videoeditor/TransitionAlpha;->mRGBMaskFile:Ljava/lang/String;

    #@7d
    .line 111
    const/4 v14, 0x0

    #@7e
    .line 114
    .local v14, fl:Ljava/io/FileOutputStream;
    :try_start_7e
    new-instance v15, Ljava/io/FileOutputStream;

    #@80
    move-object/from16 v0, p0

    #@82
    iget-object v4, v0, Landroid/media/videoeditor/TransitionAlpha;->mRGBMaskFile:Ljava/lang/String;

    #@84
    invoke-direct {v15, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_87
    .catch Ljava/io/IOException; {:try_start_7e .. :try_end_87} :catch_ea

    #@87
    .end local v14           #fl:Ljava/io/FileOutputStream;
    .local v15, fl:Ljava/io/FileOutputStream;
    move-object v14, v15

    #@88
    .line 118
    .end local v15           #fl:Ljava/io/FileOutputStream;
    .restart local v14       #fl:Ljava/io/FileOutputStream;
    :goto_88
    new-instance v13, Ljava/io/DataOutputStream;

    #@8a
    invoke-direct {v13, v14}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@8d
    .line 120
    .local v13, dos:Ljava/io/DataOutputStream;
    if-eqz v14, :cond_d3

    #@8f
    .line 124
    invoke-static/range {p7 .. p7}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    #@92
    move-result-object v2

    #@93
    .line 125
    .local v2, imageBitmap:Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    #@95
    iget v4, v0, Landroid/media/videoeditor/TransitionAlpha;->mWidth:I

    #@97
    new-array v3, v4, [I

    #@99
    .line 126
    .local v3, framingBuffer:[I
    array-length v4, v3

    #@9a
    mul-int/lit8 v4, v4, 0x4

    #@9c
    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@9f
    move-result-object v11

    #@a0
    .line 129
    .local v11, byteBuffer:Ljava/nio/ByteBuffer;
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->array()[B

    #@a3
    move-result-object v10

    #@a4
    .line 130
    .local v10, array:[B
    const/4 v7, 0x0

    #@a5
    .line 131
    .local v7, tmp:I
    :goto_a5
    move-object/from16 v0, p0

    #@a7
    iget v4, v0, Landroid/media/videoeditor/TransitionAlpha;->mHeight:I

    #@a9
    if-ge v7, v4, :cond_cd

    #@ab
    .line 132
    const/4 v4, 0x0

    #@ac
    move-object/from16 v0, p0

    #@ae
    iget v5, v0, Landroid/media/videoeditor/TransitionAlpha;->mWidth:I

    #@b0
    const/4 v6, 0x0

    #@b1
    move-object/from16 v0, p0

    #@b3
    iget v8, v0, Landroid/media/videoeditor/TransitionAlpha;->mWidth:I

    #@b5
    const/4 v9, 0x1

    #@b6
    invoke-virtual/range {v2 .. v9}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    #@b9
    .line 133
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    #@bc
    move-result-object v16

    #@bd
    .line 134
    .local v16, intBuffer:Ljava/nio/IntBuffer;
    const/4 v4, 0x0

    #@be
    move-object/from16 v0, p0

    #@c0
    iget v5, v0, Landroid/media/videoeditor/TransitionAlpha;->mWidth:I

    #@c2
    move-object/from16 v0, v16

    #@c4
    invoke-virtual {v0, v3, v4, v5}, Ljava/nio/IntBuffer;->put([III)Ljava/nio/IntBuffer;

    #@c7
    .line 136
    :try_start_c7
    invoke-virtual {v13, v10}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_ca
    .catch Ljava/io/IOException; {:try_start_c7 .. :try_end_ca} :catch_e6

    #@ca
    .line 140
    :goto_ca
    add-int/lit8 v7, v7, 0x1

    #@cc
    goto :goto_a5

    #@cd
    .line 143
    .end local v16           #intBuffer:Ljava/nio/IntBuffer;
    :cond_cd
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    #@d0
    .line 145
    :try_start_d0
    invoke-virtual {v14}, Ljava/io/FileOutputStream;->close()V
    :try_end_d3
    .catch Ljava/io/IOException; {:try_start_d0 .. :try_end_d3} :catch_e8

    #@d3
    .line 154
    .end local v2           #imageBitmap:Landroid/graphics/Bitmap;
    .end local v3           #framingBuffer:[I
    .end local v7           #tmp:I
    .end local v10           #array:[B
    .end local v11           #byteBuffer:Ljava/nio/ByteBuffer;
    :cond_d3
    :goto_d3
    move-object/from16 v0, p7

    #@d5
    move-object/from16 v1, p0

    #@d7
    iput-object v0, v1, Landroid/media/videoeditor/TransitionAlpha;->mMaskFilename:Ljava/lang/String;

    #@d9
    .line 155
    move/from16 v0, p8

    #@db
    move-object/from16 v1, p0

    #@dd
    iput v0, v1, Landroid/media/videoeditor/TransitionAlpha;->mBlendingPercent:I

    #@df
    .line 156
    move/from16 v0, p9

    #@e1
    move-object/from16 v1, p0

    #@e3
    iput-boolean v0, v1, Landroid/media/videoeditor/TransitionAlpha;->mIsInvert:Z

    #@e5
    .line 157
    return-void

    #@e6
    .line 137
    .restart local v2       #imageBitmap:Landroid/graphics/Bitmap;
    .restart local v3       #framingBuffer:[I
    .restart local v7       #tmp:I
    .restart local v10       #array:[B
    .restart local v11       #byteBuffer:Ljava/nio/ByteBuffer;
    .restart local v16       #intBuffer:Ljava/nio/IntBuffer;
    :catch_e6
    move-exception v4

    #@e7
    goto :goto_ca

    #@e8
    .line 146
    .end local v16           #intBuffer:Ljava/nio/IntBuffer;
    :catch_e8
    move-exception v4

    #@e9
    goto :goto_d3

    #@ea
    .line 115
    .end local v2           #imageBitmap:Landroid/graphics/Bitmap;
    .end local v3           #framingBuffer:[I
    .end local v7           #tmp:I
    .end local v10           #array:[B
    .end local v11           #byteBuffer:Ljava/nio/ByteBuffer;
    .end local v13           #dos:Ljava/io/DataOutputStream;
    :catch_ea
    move-exception v4

    #@eb
    goto :goto_88
.end method


# virtual methods
.method public generate()V
    .registers 1

    #@0
    .prologue
    .line 203
    invoke-super {p0}, Landroid/media/videoeditor/Transition;->generate()V

    #@3
    .line 204
    return-void
.end method

.method public getBlendingPercent()I
    .registers 2

    #@0
    .prologue
    .line 177
    iget v0, p0, Landroid/media/videoeditor/TransitionAlpha;->mBlendingPercent:I

    #@2
    return v0
.end method

.method public getMaskFilename()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 186
    iget-object v0, p0, Landroid/media/videoeditor/TransitionAlpha;->mMaskFilename:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getPNGMaskFilename()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 168
    iget-object v0, p0, Landroid/media/videoeditor/TransitionAlpha;->mRGBMaskFile:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getRGBFileHeight()I
    .registers 2

    #@0
    .prologue
    .line 164
    iget v0, p0, Landroid/media/videoeditor/TransitionAlpha;->mHeight:I

    #@2
    return v0
.end method

.method public getRGBFileWidth()I
    .registers 2

    #@0
    .prologue
    .line 160
    iget v0, p0, Landroid/media/videoeditor/TransitionAlpha;->mWidth:I

    #@2
    return v0
.end method

.method public isInvert()Z
    .registers 2

    #@0
    .prologue
    .line 195
    iget-boolean v0, p0, Landroid/media/videoeditor/TransitionAlpha;->mIsInvert:Z

    #@2
    return v0
.end method
