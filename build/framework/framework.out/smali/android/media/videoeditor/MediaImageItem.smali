.class public Landroid/media/videoeditor/MediaImageItem;
.super Landroid/media/videoeditor/MediaItem;
.source "MediaImageItem.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MediaImageItem"

.field private static final sResizePaint:Landroid/graphics/Paint;


# instance fields
.field private final mAspectRatio:I

.field private mDecodedFilename:Ljava/lang/String;

.field private mDurationMs:J

.field private mFileName:Ljava/lang/String;

.field private mGeneratedClipHeight:I

.field private mGeneratedClipWidth:I

.field private final mHeight:I

.field private final mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

.field private mScaledFilename:Ljava/lang/String;

.field private mScaledHeight:I

.field private mScaledWidth:I

.field private final mVideoEditor:Landroid/media/videoeditor/VideoEditorImpl;

.field private final mWidth:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 59
    new-instance v0, Landroid/graphics/Paint;

    #@2
    const/4 v1, 0x2

    #@3
    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    #@6
    sput-object v0, Landroid/media/videoeditor/MediaImageItem;->sResizePaint:Landroid/graphics/Paint;

    #@8
    return-void
.end method

.method private constructor <init>()V
    .registers 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 83
    const-wide/16 v4, 0x0

    #@3
    const/4 v6, 0x0

    #@4
    move-object v0, p0

    #@5
    move-object v2, v1

    #@6
    move-object v3, v1

    #@7
    invoke-direct/range {v0 .. v6}, Landroid/media/videoeditor/MediaImageItem;-><init>(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;JI)V

    #@a
    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;JI)V
    .registers 32
    .parameter "editor"
    .parameter "mediaItemId"
    .parameter "filename"
    .parameter "durationMs"
    .parameter "renderingMode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 100
    move-object/from16 v0, p0

    #@2
    move-object/from16 v1, p1

    #@4
    move-object/from16 v2, p2

    #@6
    move-object/from16 v3, p3

    #@8
    move/from16 v4, p6

    #@a
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/media/videoeditor/MediaItem;-><init>(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;I)V

    #@d
    move-object/from16 v7, p1

    #@f
    .line 102
    check-cast v7, Landroid/media/videoeditor/VideoEditorImpl;

    #@11
    invoke-virtual {v7}, Landroid/media/videoeditor/VideoEditorImpl;->getNativeContext()Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@14
    move-result-object v7

    #@15
    move-object/from16 v0, p0

    #@17
    iput-object v7, v0, Landroid/media/videoeditor/MediaImageItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@19
    .line 103
    check-cast p1, Landroid/media/videoeditor/VideoEditorImpl;

    #@1b
    .end local p1
    move-object/from16 v0, p1

    #@1d
    move-object/from16 v1, p0

    #@1f
    iput-object v0, v1, Landroid/media/videoeditor/MediaImageItem;->mVideoEditor:Landroid/media/videoeditor/VideoEditorImpl;

    #@21
    .line 105
    :try_start_21
    move-object/from16 v0, p0

    #@23
    iget-object v7, v0, Landroid/media/videoeditor/MediaImageItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@25
    move-object/from16 v0, p3

    #@27
    invoke-virtual {v7, v0}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getMediaProperties(Ljava/lang/String;)Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@2a
    move-result-object v23

    #@2b
    .line 107
    .local v23, properties:Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
    move-object/from16 v0, p0

    #@2d
    iget-object v7, v0, Landroid/media/videoeditor/MediaImageItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@2f
    move-object/from16 v0, v23

    #@31
    iget v8, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->fileType:I

    #@33
    invoke-virtual {v7, v8}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getFileType(I)I

    #@36
    move-result v7

    #@37
    packed-switch v7, :pswitch_data_238

    #@3a
    .line 114
    :pswitch_3a
    new-instance v7, Ljava/lang/IllegalArgumentException;

    #@3c
    const-string v8, "Unsupported Input File Type"

    #@3e
    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@41
    throw v7
    :try_end_42
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_42} :catch_42

    #@42
    .line 117
    .end local v23           #properties:Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
    :catch_42
    move-exception v17

    #@43
    .line 118
    .local v17, e:Ljava/lang/Exception;
    new-instance v7, Ljava/lang/IllegalArgumentException;

    #@45
    new-instance v8, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    const-string v9, "Unsupported file or file not found: "

    #@4c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v8

    #@50
    move-object/from16 v0, p3

    #@52
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v8

    #@56
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v8

    #@5a
    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@5d
    throw v7

    #@5e
    .line 120
    .end local v17           #e:Ljava/lang/Exception;
    .restart local v23       #properties:Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
    :pswitch_5e
    move-object/from16 v0, p3

    #@60
    move-object/from16 v1, p0

    #@62
    iput-object v0, v1, Landroid/media/videoeditor/MediaImageItem;->mFileName:Ljava/lang/String;

    #@64
    .line 124
    new-instance v15, Landroid/graphics/BitmapFactory$Options;

    #@66
    invoke-direct {v15}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    #@69
    .line 125
    .local v15, dbo:Landroid/graphics/BitmapFactory$Options;
    const/4 v7, 0x1

    #@6a
    iput-boolean v7, v15, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    #@6c
    .line 126
    move-object/from16 v0, p3

    #@6e
    invoke-static {v0, v15}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@71
    .line 128
    iget v7, v15, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    #@73
    move-object/from16 v0, p0

    #@75
    iput v7, v0, Landroid/media/videoeditor/MediaImageItem;->mWidth:I

    #@77
    .line 129
    iget v7, v15, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    #@79
    move-object/from16 v0, p0

    #@7b
    iput v7, v0, Landroid/media/videoeditor/MediaImageItem;->mHeight:I

    #@7d
    .line 130
    move-wide/from16 v0, p4

    #@7f
    move-object/from16 v2, p0

    #@81
    iput-wide v0, v2, Landroid/media/videoeditor/MediaImageItem;->mDurationMs:J

    #@83
    .line 131
    new-instance v7, Ljava/lang/StringBuilder;

    #@85
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@88
    move-object/from16 v0, p0

    #@8a
    iget-object v8, v0, Landroid/media/videoeditor/MediaImageItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@8c
    invoke-virtual {v8}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getProjectPath()Ljava/lang/String;

    #@8f
    move-result-object v8

    #@90
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v7

    #@94
    const-string v8, "/"

    #@96
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v7

    #@9a
    const-string v8, "decoded"

    #@9c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v7

    #@a0
    invoke-virtual/range {p0 .. p0}, Landroid/media/videoeditor/MediaImageItem;->getId()Ljava/lang/String;

    #@a3
    move-result-object v8

    #@a4
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v7

    #@a8
    const-string v8, ".rgb"

    #@aa
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v7

    #@ae
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b1
    move-result-object v7

    #@b2
    const/4 v8, 0x0

    #@b3
    new-array v8, v8, [Ljava/lang/Object;

    #@b5
    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@b8
    move-result-object v7

    #@b9
    move-object/from16 v0, p0

    #@bb
    iput-object v7, v0, Landroid/media/videoeditor/MediaImageItem;->mDecodedFilename:Ljava/lang/String;

    #@bd
    .line 135
    :try_start_bd
    move-object/from16 v0, p0

    #@bf
    iget-object v7, v0, Landroid/media/videoeditor/MediaImageItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@c1
    move-object/from16 v0, p0

    #@c3
    iget v8, v0, Landroid/media/videoeditor/MediaImageItem;->mWidth:I

    #@c5
    move-object/from16 v0, p0

    #@c7
    iget v9, v0, Landroid/media/videoeditor/MediaImageItem;->mHeight:I

    #@c9
    invoke-virtual {v7, v8, v9}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getAspectRatio(II)I

    #@cc
    move-result v7

    #@cd
    move-object/from16 v0, p0

    #@cf
    iput v7, v0, Landroid/media/videoeditor/MediaImageItem;->mAspectRatio:I
    :try_end_d1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_bd .. :try_end_d1} :catch_1ff

    #@d1
    .line 140
    const/4 v7, 0x0

    #@d2
    move-object/from16 v0, p0

    #@d4
    iput v7, v0, Landroid/media/videoeditor/MediaImageItem;->mGeneratedClipHeight:I

    #@d6
    .line 141
    const/4 v7, 0x0

    #@d7
    move-object/from16 v0, p0

    #@d9
    iput v7, v0, Landroid/media/videoeditor/MediaImageItem;->mGeneratedClipWidth:I

    #@db
    .line 147
    move-object/from16 v0, p0

    #@dd
    iget v7, v0, Landroid/media/videoeditor/MediaImageItem;->mAspectRatio:I

    #@df
    invoke-static {v7}, Landroid/media/videoeditor/MediaProperties;->getSupportedResolutions(I)[Landroid/util/Pair;

    #@e2
    move-result-object v24

    #@e3
    .line 153
    .local v24, resolutions:[Landroid/util/Pair;,"[Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    move-object/from16 v0, v24

    #@e5
    array-length v7, v0

    #@e6
    add-int/lit8 v7, v7, -0x1

    #@e8
    aget-object v21, v24, v7

    #@ea
    .line 157
    .local v21, maxResolution:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    move-object/from16 v0, p0

    #@ec
    iget v8, v0, Landroid/media/videoeditor/MediaImageItem;->mWidth:I

    #@ee
    move-object/from16 v0, v21

    #@f0
    iget-object v7, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@f2
    check-cast v7, Ljava/lang/Integer;

    #@f4
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    #@f7
    move-result v7

    #@f8
    if-gt v8, v7, :cond_10a

    #@fa
    move-object/from16 v0, p0

    #@fc
    iget v8, v0, Landroid/media/videoeditor/MediaImageItem;->mHeight:I

    #@fe
    move-object/from16 v0, v21

    #@100
    iget-object v7, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@102
    check-cast v7, Ljava/lang/Integer;

    #@104
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    #@107
    move-result v7

    #@108
    if-le v8, v7, :cond_208

    #@10a
    .line 161
    :cond_10a
    move-object/from16 v0, v21

    #@10c
    iget-object v7, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@10e
    check-cast v7, Ljava/lang/Integer;

    #@110
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    #@113
    move-result v8

    #@114
    move-object/from16 v0, v21

    #@116
    iget-object v7, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@118
    check-cast v7, Ljava/lang/Integer;

    #@11a
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    #@11d
    move-result v7

    #@11e
    move-object/from16 v0, p0

    #@120
    move-object/from16 v1, p3

    #@122
    invoke-direct {v0, v1, v8, v7}, Landroid/media/videoeditor/MediaImageItem;->scaleImage(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    #@125
    move-result-object v5

    #@126
    .line 163
    .local v5, imageBitmap:Landroid/graphics/Bitmap;
    new-instance v7, Ljava/lang/StringBuilder;

    #@128
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@12b
    move-object/from16 v0, p0

    #@12d
    iget-object v8, v0, Landroid/media/videoeditor/MediaImageItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@12f
    invoke-virtual {v8}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getProjectPath()Ljava/lang/String;

    #@132
    move-result-object v8

    #@133
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@136
    move-result-object v7

    #@137
    const-string v8, "/"

    #@139
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13c
    move-result-object v7

    #@13d
    const-string/jumbo v8, "scaled"

    #@140
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@143
    move-result-object v7

    #@144
    invoke-virtual/range {p0 .. p0}, Landroid/media/videoeditor/MediaImageItem;->getId()Ljava/lang/String;

    #@147
    move-result-object v8

    #@148
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14b
    move-result-object v7

    #@14c
    const-string v8, ".JPG"

    #@14e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@151
    move-result-object v7

    #@152
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@155
    move-result-object v7

    #@156
    const/4 v8, 0x0

    #@157
    new-array v8, v8, [Ljava/lang/Object;

    #@159
    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@15c
    move-result-object v7

    #@15d
    move-object/from16 v0, p0

    #@15f
    iput-object v7, v0, Landroid/media/videoeditor/MediaImageItem;->mScaledFilename:Ljava/lang/String;

    #@161
    .line 165
    new-instance v7, Ljava/io/File;

    #@163
    move-object/from16 v0, p0

    #@165
    iget-object v8, v0, Landroid/media/videoeditor/MediaImageItem;->mScaledFilename:Ljava/lang/String;

    #@167
    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@16a
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    #@16d
    move-result v7

    #@16e
    if-nez v7, :cond_18c

    #@170
    .line 166
    const/4 v7, 0x1

    #@171
    move-object/from16 v0, p0

    #@173
    iput-boolean v7, v0, Landroid/media/videoeditor/MediaItem;->mRegenerateClip:Z

    #@175
    .line 167
    new-instance v18, Ljava/io/FileOutputStream;

    #@177
    move-object/from16 v0, p0

    #@179
    iget-object v7, v0, Landroid/media/videoeditor/MediaImageItem;->mScaledFilename:Ljava/lang/String;

    #@17b
    move-object/from16 v0, v18

    #@17d
    invoke-direct {v0, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    #@180
    .line 168
    .local v18, f1:Ljava/io/FileOutputStream;
    sget-object v7, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    #@182
    const/16 v8, 0x32

    #@184
    move-object/from16 v0, v18

    #@186
    invoke-virtual {v5, v7, v8, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    #@189
    .line 169
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileOutputStream;->close()V

    #@18c
    .line 171
    .end local v18           #f1:Ljava/io/FileOutputStream;
    :cond_18c
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    #@18f
    move-result v7

    #@190
    shr-int/lit8 v7, v7, 0x1

    #@192
    shl-int/lit8 v7, v7, 0x1

    #@194
    move-object/from16 v0, p0

    #@196
    iput v7, v0, Landroid/media/videoeditor/MediaImageItem;->mScaledWidth:I

    #@198
    .line 172
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    #@19b
    move-result v7

    #@19c
    shr-int/lit8 v7, v7, 0x1

    #@19e
    shl-int/lit8 v7, v7, 0x1

    #@1a0
    move-object/from16 v0, p0

    #@1a2
    iput v7, v0, Landroid/media/videoeditor/MediaImageItem;->mScaledHeight:I

    #@1a4
    .line 179
    :goto_1a4
    move-object/from16 v0, p0

    #@1a6
    iget v11, v0, Landroid/media/videoeditor/MediaImageItem;->mScaledWidth:I

    #@1a8
    .line 180
    .local v11, newWidth:I
    move-object/from16 v0, p0

    #@1aa
    iget v0, v0, Landroid/media/videoeditor/MediaImageItem;->mScaledHeight:I

    #@1ac
    move/from16 v22, v0

    #@1ae
    .line 181
    .local v22, newHeight:I
    new-instance v7, Ljava/io/File;

    #@1b0
    move-object/from16 v0, p0

    #@1b2
    iget-object v8, v0, Landroid/media/videoeditor/MediaImageItem;->mDecodedFilename:Ljava/lang/String;

    #@1b4
    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@1b7
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    #@1ba
    move-result v7

    #@1bb
    if-nez v7, :cond_233

    #@1bd
    .line 182
    new-instance v19, Ljava/io/FileOutputStream;

    #@1bf
    move-object/from16 v0, p0

    #@1c1
    iget-object v7, v0, Landroid/media/videoeditor/MediaImageItem;->mDecodedFilename:Ljava/lang/String;

    #@1c3
    move-object/from16 v0, v19

    #@1c5
    invoke-direct {v0, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    #@1c8
    .line 183
    .local v19, fl:Ljava/io/FileOutputStream;
    new-instance v16, Ljava/io/DataOutputStream;

    #@1ca
    move-object/from16 v0, v16

    #@1cc
    move-object/from16 v1, v19

    #@1ce
    invoke-direct {v0, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@1d1
    .line 184
    .local v16, dos:Ljava/io/DataOutputStream;
    new-array v6, v11, [I

    #@1d3
    .line 185
    .local v6, framingBuffer:[I
    array-length v7, v6

    #@1d4
    mul-int/lit8 v7, v7, 0x4

    #@1d6
    invoke-static {v7}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@1d9
    move-result-object v14

    #@1da
    .line 187
    .local v14, byteBuffer:Ljava/nio/ByteBuffer;
    invoke-virtual {v14}, Ljava/nio/ByteBuffer;->array()[B

    #@1dd
    move-result-object v13

    #@1de
    .line 188
    .local v13, array:[B
    const/4 v10, 0x0

    #@1df
    .line 189
    .local v10, tmp:I
    :goto_1df
    move/from16 v0, v22

    #@1e1
    if-ge v10, v0, :cond_230

    #@1e3
    .line 190
    const/4 v7, 0x0

    #@1e4
    move-object/from16 v0, p0

    #@1e6
    iget v8, v0, Landroid/media/videoeditor/MediaImageItem;->mScaledWidth:I

    #@1e8
    const/4 v9, 0x0

    #@1e9
    const/4 v12, 0x1

    #@1ea
    invoke-virtual/range {v5 .. v12}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    #@1ed
    .line 192
    invoke-virtual {v14}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    #@1f0
    move-result-object v20

    #@1f1
    .line 193
    .local v20, intBuffer:Ljava/nio/IntBuffer;
    const/4 v7, 0x0

    #@1f2
    move-object/from16 v0, v20

    #@1f4
    invoke-virtual {v0, v6, v7, v11}, Ljava/nio/IntBuffer;->put([III)Ljava/nio/IntBuffer;

    #@1f7
    .line 194
    move-object/from16 v0, v16

    #@1f9
    invoke-virtual {v0, v13}, Ljava/io/DataOutputStream;->write([B)V

    #@1fc
    .line 195
    add-int/lit8 v10, v10, 0x1

    #@1fe
    goto :goto_1df

    #@1ff
    .line 136
    .end local v5           #imageBitmap:Landroid/graphics/Bitmap;
    .end local v6           #framingBuffer:[I
    .end local v10           #tmp:I
    .end local v11           #newWidth:I
    .end local v13           #array:[B
    .end local v14           #byteBuffer:Ljava/nio/ByteBuffer;
    .end local v16           #dos:Ljava/io/DataOutputStream;
    .end local v19           #fl:Ljava/io/FileOutputStream;
    .end local v20           #intBuffer:Ljava/nio/IntBuffer;
    .end local v21           #maxResolution:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .end local v22           #newHeight:I
    .end local v24           #resolutions:[Landroid/util/Pair;,"[Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :catch_1ff
    move-exception v17

    #@200
    .line 137
    .local v17, e:Ljava/lang/IllegalArgumentException;
    new-instance v7, Ljava/lang/IllegalArgumentException;

    #@202
    const-string v8, "Null width and height"

    #@204
    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@207
    throw v7

    #@208
    .line 174
    .end local v17           #e:Ljava/lang/IllegalArgumentException;
    .restart local v21       #maxResolution:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .restart local v24       #resolutions:[Landroid/util/Pair;,"[Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_208
    move-object/from16 v0, p3

    #@20a
    move-object/from16 v1, p0

    #@20c
    iput-object v0, v1, Landroid/media/videoeditor/MediaImageItem;->mScaledFilename:Ljava/lang/String;

    #@20e
    .line 175
    move-object/from16 v0, p0

    #@210
    iget v7, v0, Landroid/media/videoeditor/MediaImageItem;->mWidth:I

    #@212
    shr-int/lit8 v7, v7, 0x1

    #@214
    shl-int/lit8 v7, v7, 0x1

    #@216
    move-object/from16 v0, p0

    #@218
    iput v7, v0, Landroid/media/videoeditor/MediaImageItem;->mScaledWidth:I

    #@21a
    .line 176
    move-object/from16 v0, p0

    #@21c
    iget v7, v0, Landroid/media/videoeditor/MediaImageItem;->mHeight:I

    #@21e
    shr-int/lit8 v7, v7, 0x1

    #@220
    shl-int/lit8 v7, v7, 0x1

    #@222
    move-object/from16 v0, p0

    #@224
    iput v7, v0, Landroid/media/videoeditor/MediaImageItem;->mScaledHeight:I

    #@226
    .line 177
    move-object/from16 v0, p0

    #@228
    iget-object v7, v0, Landroid/media/videoeditor/MediaImageItem;->mScaledFilename:Ljava/lang/String;

    #@22a
    invoke-static {v7}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    #@22d
    move-result-object v5

    #@22e
    .restart local v5       #imageBitmap:Landroid/graphics/Bitmap;
    goto/16 :goto_1a4

    #@230
    .line 197
    .restart local v6       #framingBuffer:[I
    .restart local v10       #tmp:I
    .restart local v11       #newWidth:I
    .restart local v13       #array:[B
    .restart local v14       #byteBuffer:Ljava/nio/ByteBuffer;
    .restart local v16       #dos:Ljava/io/DataOutputStream;
    .restart local v19       #fl:Ljava/io/FileOutputStream;
    .restart local v22       #newHeight:I
    :cond_230
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V

    #@233
    .line 199
    .end local v6           #framingBuffer:[I
    .end local v10           #tmp:I
    .end local v13           #array:[B
    .end local v14           #byteBuffer:Ljava/nio/ByteBuffer;
    .end local v16           #dos:Ljava/io/DataOutputStream;
    .end local v19           #fl:Ljava/io/FileOutputStream;
    :cond_233
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    #@236
    .line 200
    return-void

    #@237
    .line 107
    nop

    #@238
    :pswitch_data_238
    .packed-switch 0x5
        :pswitch_5e
        :pswitch_3a
        :pswitch_3a
        :pswitch_5e
    .end packed-switch
.end method

.method private adjustEffects()Ljava/util/List;
    .registers 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/media/videoeditor/Effect;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 435
    new-instance v0, Ljava/util/ArrayList;

    #@2
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 436
    .local v0, adjustedEffects:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Effect;>;"
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getAllEffects()Ljava/util/List;

    #@8
    move-result-object v6

    #@9
    .line 437
    .local v6, effects:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Effect;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@c
    move-result-object v7

    #@d
    .local v7, i$:Ljava/util/Iterator;
    :cond_d
    :goto_d
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@10
    move-result v8

    #@11
    if-eqz v8, :cond_5b

    #@13
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@16
    move-result-object v1

    #@17
    check-cast v1, Landroid/media/videoeditor/Effect;

    #@19
    .line 442
    .local v1, effect:Landroid/media/videoeditor/Effect;
    invoke-virtual {v1}, Landroid/media/videoeditor/Effect;->getStartTime()J

    #@1c
    move-result-wide v8

    #@1d
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getDuration()J

    #@20
    move-result-wide v10

    #@21
    cmp-long v8, v8, v10

    #@23
    if-lez v8, :cond_51

    #@25
    .line 443
    const-wide/16 v4, 0x0

    #@27
    .line 452
    .local v4, effectStartTimeMs:J
    :goto_27
    invoke-virtual {v1}, Landroid/media/videoeditor/Effect;->getDuration()J

    #@2a
    move-result-wide v8

    #@2b
    add-long/2addr v8, v4

    #@2c
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getDuration()J

    #@2f
    move-result-wide v10

    #@30
    cmp-long v8, v8, v10

    #@32
    if-lez v8, :cond_56

    #@34
    .line 453
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getDuration()J

    #@37
    move-result-wide v8

    #@38
    sub-long v2, v8, v4

    #@3a
    .line 458
    .local v2, effectDurationMs:J
    :goto_3a
    invoke-virtual {v1}, Landroid/media/videoeditor/Effect;->getStartTime()J

    #@3d
    move-result-wide v8

    #@3e
    cmp-long v8, v4, v8

    #@40
    if-nez v8, :cond_4a

    #@42
    invoke-virtual {v1}, Landroid/media/videoeditor/Effect;->getDuration()J

    #@45
    move-result-wide v8

    #@46
    cmp-long v8, v2, v8

    #@48
    if-eqz v8, :cond_d

    #@4a
    .line 460
    :cond_4a
    invoke-virtual {v1, v4, v5, v2, v3}, Landroid/media/videoeditor/Effect;->setStartTimeAndDuration(JJ)V

    #@4d
    .line 461
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@50
    goto :goto_d

    #@51
    .line 445
    .end local v2           #effectDurationMs:J
    .end local v4           #effectStartTimeMs:J
    :cond_51
    invoke-virtual {v1}, Landroid/media/videoeditor/Effect;->getStartTime()J

    #@54
    move-result-wide v4

    #@55
    .restart local v4       #effectStartTimeMs:J
    goto :goto_27

    #@56
    .line 455
    :cond_56
    invoke-virtual {v1}, Landroid/media/videoeditor/Effect;->getDuration()J

    #@59
    move-result-wide v2

    #@5a
    .restart local v2       #effectDurationMs:J
    goto :goto_3a

    #@5b
    .line 465
    .end local v1           #effect:Landroid/media/videoeditor/Effect;
    .end local v2           #effectDurationMs:J
    .end local v4           #effectStartTimeMs:J
    :cond_5b
    return-object v0
.end method

.method private adjustOverlays()Ljava/util/List;
    .registers 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/media/videoeditor/Overlay;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 474
    new-instance v0, Ljava/util/ArrayList;

    #@2
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 475
    .local v0, adjustedOverlays:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Overlay;>;"
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getAllOverlays()Ljava/util/List;

    #@8
    move-result-object v7

    #@9
    .line 476
    .local v7, overlays:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Overlay;>;"
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@c
    move-result-object v1

    #@d
    .local v1, i$:Ljava/util/Iterator;
    :cond_d
    :goto_d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@10
    move-result v8

    #@11
    if-eqz v8, :cond_5b

    #@13
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@16
    move-result-object v2

    #@17
    check-cast v2, Landroid/media/videoeditor/Overlay;

    #@19
    .line 481
    .local v2, overlay:Landroid/media/videoeditor/Overlay;
    invoke-virtual {v2}, Landroid/media/videoeditor/Overlay;->getStartTime()J

    #@1c
    move-result-wide v8

    #@1d
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getDuration()J

    #@20
    move-result-wide v10

    #@21
    cmp-long v8, v8, v10

    #@23
    if-lez v8, :cond_51

    #@25
    .line 482
    const-wide/16 v5, 0x0

    #@27
    .line 491
    .local v5, overlayStartTimeMs:J
    :goto_27
    invoke-virtual {v2}, Landroid/media/videoeditor/Overlay;->getDuration()J

    #@2a
    move-result-wide v8

    #@2b
    add-long/2addr v8, v5

    #@2c
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getDuration()J

    #@2f
    move-result-wide v10

    #@30
    cmp-long v8, v8, v10

    #@32
    if-lez v8, :cond_56

    #@34
    .line 492
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getDuration()J

    #@37
    move-result-wide v8

    #@38
    sub-long v3, v8, v5

    #@3a
    .line 497
    .local v3, overlayDurationMs:J
    :goto_3a
    invoke-virtual {v2}, Landroid/media/videoeditor/Overlay;->getStartTime()J

    #@3d
    move-result-wide v8

    #@3e
    cmp-long v8, v5, v8

    #@40
    if-nez v8, :cond_4a

    #@42
    invoke-virtual {v2}, Landroid/media/videoeditor/Overlay;->getDuration()J

    #@45
    move-result-wide v8

    #@46
    cmp-long v8, v3, v8

    #@48
    if-eqz v8, :cond_d

    #@4a
    .line 499
    :cond_4a
    invoke-virtual {v2, v5, v6, v3, v4}, Landroid/media/videoeditor/Overlay;->setStartTimeAndDuration(JJ)V

    #@4d
    .line 500
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@50
    goto :goto_d

    #@51
    .line 484
    .end local v3           #overlayDurationMs:J
    .end local v5           #overlayStartTimeMs:J
    :cond_51
    invoke-virtual {v2}, Landroid/media/videoeditor/Overlay;->getStartTime()J

    #@54
    move-result-wide v5

    #@55
    .restart local v5       #overlayStartTimeMs:J
    goto :goto_27

    #@56
    .line 494
    :cond_56
    invoke-virtual {v2}, Landroid/media/videoeditor/Overlay;->getDuration()J

    #@59
    move-result-wide v3

    #@5a
    .restart local v3       #overlayDurationMs:J
    goto :goto_3a

    #@5b
    .line 504
    .end local v2           #overlay:Landroid/media/videoeditor/Overlay;
    .end local v3           #overlayDurationMs:J
    .end local v5           #overlayStartTimeMs:J
    :cond_5b
    return-object v0
.end method

.method private getKenBurns(Landroid/media/videoeditor/EffectKenBurns;)Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;
    .registers 12
    .parameter "effectKB"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 760
    const/4 v6, 0x0

    #@2
    .local v6, width:I
    const/4 v4, 0x0

    #@3
    .line 761
    .local v4, height:I
    new-instance v5, Landroid/graphics/Rect;

    #@5
    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    #@8
    .line 762
    .local v5, start:Landroid/graphics/Rect;
    new-instance v3, Landroid/graphics/Rect;

    #@a
    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    #@d
    .line 763
    .local v3, end:Landroid/graphics/Rect;
    const/4 v2, 0x0

    #@e
    .line 764
    .local v2, clipSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;
    new-instance v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@10
    .end local v2           #clipSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;
    invoke-direct {v2}, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;-><init>()V

    #@13
    .line 780
    .restart local v2       #clipSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;
    invoke-virtual {p1, v5, v3}, Landroid/media/videoeditor/EffectKenBurns;->getKenBurnsSettings(Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    #@16
    .line 781
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getWidth()I

    #@19
    move-result v6

    #@1a
    .line 782
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getHeight()I

    #@1d
    move-result v4

    #@1e
    .line 783
    iget v7, v5, Landroid/graphics/Rect;->left:I

    #@20
    if-ltz v7, :cond_5e

    #@22
    iget v7, v5, Landroid/graphics/Rect;->left:I

    #@24
    if-gt v7, v6, :cond_5e

    #@26
    iget v7, v5, Landroid/graphics/Rect;->right:I

    #@28
    if-ltz v7, :cond_5e

    #@2a
    iget v7, v5, Landroid/graphics/Rect;->right:I

    #@2c
    if-gt v7, v6, :cond_5e

    #@2e
    iget v7, v5, Landroid/graphics/Rect;->top:I

    #@30
    if-ltz v7, :cond_5e

    #@32
    iget v7, v5, Landroid/graphics/Rect;->top:I

    #@34
    if-gt v7, v4, :cond_5e

    #@36
    iget v7, v5, Landroid/graphics/Rect;->bottom:I

    #@38
    if-ltz v7, :cond_5e

    #@3a
    iget v7, v5, Landroid/graphics/Rect;->bottom:I

    #@3c
    if-gt v7, v4, :cond_5e

    #@3e
    iget v7, v3, Landroid/graphics/Rect;->left:I

    #@40
    if-ltz v7, :cond_5e

    #@42
    iget v7, v3, Landroid/graphics/Rect;->left:I

    #@44
    if-gt v7, v6, :cond_5e

    #@46
    iget v7, v3, Landroid/graphics/Rect;->right:I

    #@48
    if-ltz v7, :cond_5e

    #@4a
    iget v7, v3, Landroid/graphics/Rect;->right:I

    #@4c
    if-gt v7, v6, :cond_5e

    #@4e
    iget v7, v3, Landroid/graphics/Rect;->top:I

    #@50
    if-ltz v7, :cond_5e

    #@52
    iget v7, v3, Landroid/graphics/Rect;->top:I

    #@54
    if-gt v7, v4, :cond_5e

    #@56
    iget v7, v3, Landroid/graphics/Rect;->bottom:I

    #@58
    if-ltz v7, :cond_5e

    #@5a
    iget v7, v3, Landroid/graphics/Rect;->bottom:I

    #@5c
    if-le v7, v4, :cond_66

    #@5e
    .line 788
    :cond_5e
    new-instance v7, Ljava/lang/IllegalArgumentException;

    #@60
    const-string v8, "Illegal arguments for KebBurns"

    #@62
    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@65
    throw v7

    #@66
    .line 791
    :cond_66
    iget v7, v5, Landroid/graphics/Rect;->right:I

    #@68
    iget v8, v5, Landroid/graphics/Rect;->left:I

    #@6a
    sub-int/2addr v7, v8

    #@6b
    sub-int v7, v6, v7

    #@6d
    if-eqz v7, :cond_78

    #@6f
    iget v7, v5, Landroid/graphics/Rect;->bottom:I

    #@71
    iget v8, v5, Landroid/graphics/Rect;->top:I

    #@73
    sub-int/2addr v7, v8

    #@74
    sub-int v7, v4, v7

    #@76
    if-nez v7, :cond_ca

    #@78
    :cond_78
    iget v7, v3, Landroid/graphics/Rect;->right:I

    #@7a
    iget v8, v3, Landroid/graphics/Rect;->left:I

    #@7c
    sub-int/2addr v7, v8

    #@7d
    sub-int v7, v6, v7

    #@7f
    if-eqz v7, :cond_8a

    #@81
    iget v7, v3, Landroid/graphics/Rect;->bottom:I

    #@83
    iget v8, v3, Landroid/graphics/Rect;->top:I

    #@85
    sub-int/2addr v7, v8

    #@86
    sub-int v7, v4, v7

    #@88
    if-nez v7, :cond_ca

    #@8a
    .line 793
    :cond_8a
    invoke-virtual {p0, v9}, Landroid/media/videoeditor/MediaImageItem;->setRegenerateClip(Z)V

    #@8d
    .line 794
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getDecodedImageFileName()Ljava/lang/String;

    #@90
    move-result-object v7

    #@91
    iput-object v7, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipPath:Ljava/lang/String;

    #@93
    .line 795
    const/4 v7, 0x5

    #@94
    iput v7, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->fileType:I

    #@96
    .line 796
    iput v9, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->beginCutTime:I

    #@98
    .line 797
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getTimelineDuration()J

    #@9b
    move-result-wide v7

    #@9c
    long-to-int v7, v7

    #@9d
    iput v7, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutTime:I

    #@9f
    .line 798
    iput v9, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->beginCutPercent:I

    #@a1
    .line 799
    iput v9, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutPercent:I

    #@a3
    .line 800
    iput-boolean v9, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomEnabled:Z

    #@a5
    .line 801
    iput v9, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomPercentStart:I

    #@a7
    .line 802
    iput v9, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomTopLeftXStart:I

    #@a9
    .line 803
    iput v9, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomTopLeftYStart:I

    #@ab
    .line 804
    iput v9, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomPercentEnd:I

    #@ad
    .line 805
    iput v9, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomTopLeftXEnd:I

    #@af
    .line 806
    iput v9, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomTopLeftYEnd:I

    #@b1
    .line 807
    iget-object v7, p0, Landroid/media/videoeditor/MediaImageItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@b3
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getRenderingMode()I

    #@b6
    move-result v8

    #@b7
    invoke-virtual {v7, v8}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getMediaItemRenderingMode(I)I

    #@ba
    move-result v7

    #@bb
    iput v7, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->mediaRendering:I

    #@bd
    .line 810
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getScaledWidth()I

    #@c0
    move-result v7

    #@c1
    iput v7, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->rgbWidth:I

    #@c3
    .line 811
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getScaledHeight()I

    #@c6
    move-result v7

    #@c7
    iput v7, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->rgbHeight:I

    #@c9
    .line 838
    :goto_c9
    return-object v2

    #@ca
    .line 816
    :cond_ca
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    #@cd
    move-result v7

    #@ce
    mul-int/lit16 v7, v7, 0x3e8

    #@d0
    div-int v0, v7, v6

    #@d2
    .line 817
    .local v0, PanZoomXa:I
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    #@d5
    move-result v7

    #@d6
    mul-int/lit16 v7, v7, 0x3e8

    #@d8
    div-int v1, v7, v6

    #@da
    .line 819
    .local v1, PanZoomXb:I
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getDecodedImageFileName()Ljava/lang/String;

    #@dd
    move-result-object v7

    #@de
    iput-object v7, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipPath:Ljava/lang/String;

    #@e0
    .line 820
    iget-object v7, p0, Landroid/media/videoeditor/MediaImageItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@e2
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getFileType()I

    #@e5
    move-result v8

    #@e6
    invoke-virtual {v7, v8}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getMediaItemFileType(I)I

    #@e9
    move-result v7

    #@ea
    iput v7, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->fileType:I

    #@ec
    .line 821
    iput v9, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->beginCutTime:I

    #@ee
    .line 822
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getTimelineDuration()J

    #@f1
    move-result-wide v7

    #@f2
    long-to-int v7, v7

    #@f3
    iput v7, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutTime:I

    #@f5
    .line 823
    iput v9, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->beginCutPercent:I

    #@f7
    .line 824
    iput v9, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutPercent:I

    #@f9
    .line 825
    const/4 v7, 0x1

    #@fa
    iput-boolean v7, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomEnabled:Z

    #@fc
    .line 826
    iput v0, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomPercentStart:I

    #@fe
    .line 827
    iget v7, v5, Landroid/graphics/Rect;->left:I

    #@100
    mul-int/lit16 v7, v7, 0x3e8

    #@102
    div-int/2addr v7, v6

    #@103
    iput v7, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomTopLeftXStart:I

    #@105
    .line 828
    iget v7, v5, Landroid/graphics/Rect;->top:I

    #@107
    mul-int/lit16 v7, v7, 0x3e8

    #@109
    div-int/2addr v7, v4

    #@10a
    iput v7, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomTopLeftYStart:I

    #@10c
    .line 829
    iput v1, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomPercentEnd:I

    #@10e
    .line 830
    iget v7, v3, Landroid/graphics/Rect;->left:I

    #@110
    mul-int/lit16 v7, v7, 0x3e8

    #@112
    div-int/2addr v7, v6

    #@113
    iput v7, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomTopLeftXEnd:I

    #@115
    .line 831
    iget v7, v3, Landroid/graphics/Rect;->top:I

    #@117
    mul-int/lit16 v7, v7, 0x3e8

    #@119
    div-int/2addr v7, v4

    #@11a
    iput v7, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomTopLeftYEnd:I

    #@11c
    .line 832
    iget-object v7, p0, Landroid/media/videoeditor/MediaImageItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@11e
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getRenderingMode()I

    #@121
    move-result v8

    #@122
    invoke-virtual {v7, v8}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getMediaItemRenderingMode(I)I

    #@125
    move-result v7

    #@126
    iput v7, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->mediaRendering:I

    #@128
    .line 835
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getScaledWidth()I

    #@12b
    move-result v7

    #@12c
    iput v7, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->rgbWidth:I

    #@12e
    .line 836
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getScaledHeight()I

    #@131
    move-result v7

    #@132
    iput v7, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->rgbHeight:I

    #@134
    goto :goto_c9
.end method

.method private getWidthByAspectRatioAndHeight(II)I
    .registers 7
    .parameter "aspectRatio"
    .parameter "height"

    #@0
    .prologue
    const/16 v3, 0x2d0

    #@2
    const/16 v2, 0x1e0

    #@4
    .line 514
    const/4 v0, 0x0

    #@5
    .line 516
    .local v0, width:I
    packed-switch p1, :pswitch_data_48

    #@8
    .line 553
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@a
    const-string v2, "Illegal arguments for aspectRatio"

    #@c
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v1

    #@10
    .line 518
    :pswitch_10
    if-ne p2, v2, :cond_15

    #@12
    .line 519
    const/16 v0, 0x2d0

    #@14
    .line 558
    :cond_14
    :goto_14
    return v0

    #@15
    .line 520
    :cond_15
    if-ne p2, v3, :cond_14

    #@17
    .line 521
    const/16 v0, 0x438

    #@19
    goto :goto_14

    #@1a
    .line 525
    :pswitch_1a
    const/16 v1, 0x168

    #@1c
    if-ne p2, v1, :cond_21

    #@1e
    .line 526
    const/16 v0, 0x280

    #@20
    goto :goto_14

    #@21
    .line 527
    :cond_21
    if-ne p2, v2, :cond_26

    #@23
    .line 528
    const/16 v0, 0x356

    #@25
    goto :goto_14

    #@26
    .line 529
    :cond_26
    if-ne p2, v3, :cond_2b

    #@28
    .line 530
    const/16 v0, 0x500

    #@2a
    goto :goto_14

    #@2b
    .line 531
    :cond_2b
    const/16 v1, 0x438

    #@2d
    if-ne p2, v1, :cond_14

    #@2f
    .line 532
    const/16 v0, 0x780

    #@31
    goto :goto_14

    #@32
    .line 536
    :pswitch_32
    if-ne p2, v2, :cond_36

    #@34
    .line 537
    const/16 v0, 0x280

    #@36
    .line 538
    :cond_36
    if-ne p2, v3, :cond_14

    #@38
    .line 539
    const/16 v0, 0x3c0

    #@3a
    goto :goto_14

    #@3b
    .line 543
    :pswitch_3b
    if-ne p2, v2, :cond_14

    #@3d
    .line 544
    const/16 v0, 0x320

    #@3f
    goto :goto_14

    #@40
    .line 548
    :pswitch_40
    const/16 v1, 0x90

    #@42
    if-ne p2, v1, :cond_14

    #@44
    .line 549
    const/16 v0, 0xb0

    #@46
    goto :goto_14

    #@47
    .line 516
    nop

    #@48
    :pswitch_data_48
    .packed-switch 0x1
        :pswitch_10
        :pswitch_1a
        :pswitch_32
        :pswitch_3b
        :pswitch_40
    .end packed-switch
.end method

.method private invalidateBeginTransition(Ljava/util/List;Ljava/util/List;)V
    .registers 10
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/media/videoeditor/Effect;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/media/videoeditor/Overlay;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 350
    .local p1, effects:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Effect;>;"
    .local p2, overlays:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Overlay;>;"
    iget-object v5, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@2
    if-eqz v5, :cond_54

    #@4
    iget-object v5, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@6
    invoke-virtual {v5}, Landroid/media/videoeditor/Transition;->isGenerated()Z

    #@9
    move-result v5

    #@a
    if-eqz v5, :cond_54

    #@c
    .line 351
    iget-object v5, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@e
    invoke-virtual {v5}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@11
    move-result-wide v3

    #@12
    .line 357
    .local v3, transitionDurationMs:J
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@15
    move-result-object v1

    #@16
    .local v1, i$:Ljava/util/Iterator;
    :cond_16
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@19
    move-result v5

    #@1a
    if-eqz v5, :cond_2f

    #@1c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Landroid/media/videoeditor/Effect;

    #@22
    .line 361
    .local v0, effect:Landroid/media/videoeditor/Effect;
    invoke-virtual {v0}, Landroid/media/videoeditor/Effect;->getStartTime()J

    #@25
    move-result-wide v5

    #@26
    cmp-long v5, v5, v3

    #@28
    if-gez v5, :cond_16

    #@2a
    .line 362
    iget-object v5, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@2c
    invoke-virtual {v5}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@2f
    .line 367
    .end local v0           #effect:Landroid/media/videoeditor/Effect;
    :cond_2f
    iget-object v5, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@31
    invoke-virtual {v5}, Landroid/media/videoeditor/Transition;->isGenerated()Z

    #@34
    move-result v5

    #@35
    if-eqz v5, :cond_54

    #@37
    .line 372
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@3a
    move-result-object v1

    #@3b
    :cond_3b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@3e
    move-result v5

    #@3f
    if-eqz v5, :cond_54

    #@41
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@44
    move-result-object v2

    #@45
    check-cast v2, Landroid/media/videoeditor/Overlay;

    #@47
    .line 376
    .local v2, overlay:Landroid/media/videoeditor/Overlay;
    invoke-virtual {v2}, Landroid/media/videoeditor/Overlay;->getStartTime()J

    #@4a
    move-result-wide v5

    #@4b
    cmp-long v5, v5, v3

    #@4d
    if-gez v5, :cond_3b

    #@4f
    .line 377
    iget-object v5, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@51
    invoke-virtual {v5}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@54
    .line 383
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #overlay:Landroid/media/videoeditor/Overlay;
    .end local v3           #transitionDurationMs:J
    :cond_54
    return-void
.end method

.method private invalidateEndTransition()V
    .registers 12

    #@0
    .prologue
    .line 390
    iget-object v7, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@2
    if-eqz v7, :cond_6c

    #@4
    iget-object v7, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@6
    invoke-virtual {v7}, Landroid/media/videoeditor/Transition;->isGenerated()Z

    #@9
    move-result v7

    #@a
    if-eqz v7, :cond_6c

    #@c
    .line 391
    iget-object v7, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@e
    invoke-virtual {v7}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@11
    move-result-wide v5

    #@12
    .line 397
    .local v5, transitionDurationMs:J
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getAllEffects()Ljava/util/List;

    #@15
    move-result-object v1

    #@16
    .line 398
    .local v1, effects:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Effect;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@19
    move-result-object v2

    #@1a
    .local v2, i$:Ljava/util/Iterator;
    :cond_1a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@1d
    move-result v7

    #@1e
    if-eqz v7, :cond_3b

    #@20
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@23
    move-result-object v0

    #@24
    check-cast v0, Landroid/media/videoeditor/Effect;

    #@26
    .line 402
    .local v0, effect:Landroid/media/videoeditor/Effect;
    invoke-virtual {v0}, Landroid/media/videoeditor/Effect;->getStartTime()J

    #@29
    move-result-wide v7

    #@2a
    invoke-virtual {v0}, Landroid/media/videoeditor/Effect;->getDuration()J

    #@2d
    move-result-wide v9

    #@2e
    add-long/2addr v7, v9

    #@2f
    iget-wide v9, p0, Landroid/media/videoeditor/MediaImageItem;->mDurationMs:J

    #@31
    sub-long/2addr v9, v5

    #@32
    cmp-long v7, v7, v9

    #@34
    if-lez v7, :cond_1a

    #@36
    .line 404
    iget-object v7, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@38
    invoke-virtual {v7}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@3b
    .line 409
    .end local v0           #effect:Landroid/media/videoeditor/Effect;
    :cond_3b
    iget-object v7, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@3d
    invoke-virtual {v7}, Landroid/media/videoeditor/Transition;->isGenerated()Z

    #@40
    move-result v7

    #@41
    if-eqz v7, :cond_6c

    #@43
    .line 414
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getAllOverlays()Ljava/util/List;

    #@46
    move-result-object v4

    #@47
    .line 415
    .local v4, overlays:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Overlay;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@4a
    move-result-object v2

    #@4b
    :cond_4b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@4e
    move-result v7

    #@4f
    if-eqz v7, :cond_6c

    #@51
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@54
    move-result-object v3

    #@55
    check-cast v3, Landroid/media/videoeditor/Overlay;

    #@57
    .line 419
    .local v3, overlay:Landroid/media/videoeditor/Overlay;
    invoke-virtual {v3}, Landroid/media/videoeditor/Overlay;->getStartTime()J

    #@5a
    move-result-wide v7

    #@5b
    invoke-virtual {v3}, Landroid/media/videoeditor/Overlay;->getDuration()J

    #@5e
    move-result-wide v9

    #@5f
    add-long/2addr v7, v9

    #@60
    iget-wide v9, p0, Landroid/media/videoeditor/MediaImageItem;->mDurationMs:J

    #@62
    sub-long/2addr v9, v5

    #@63
    cmp-long v7, v7, v9

    #@65
    if-lez v7, :cond_4b

    #@67
    .line 421
    iget-object v7, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@69
    invoke-virtual {v7}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@6c
    .line 427
    .end local v1           #effects:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Effect;>;"
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #overlay:Landroid/media/videoeditor/Overlay;
    .end local v4           #overlays:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Overlay;>;"
    .end local v5           #transitionDurationMs:J
    :cond_6c
    return-void
.end method

.method public static nextPowerOf2(I)I
    .registers 2
    .parameter "n"

    #@0
    .prologue
    .line 1013
    add-int/lit8 p0, p0, -0x1

    #@2
    .line 1014
    ushr-int/lit8 v0, p0, 0x10

    #@4
    or-int/2addr p0, v0

    #@5
    .line 1015
    ushr-int/lit8 v0, p0, 0x8

    #@7
    or-int/2addr p0, v0

    #@8
    .line 1016
    ushr-int/lit8 v0, p0, 0x4

    #@a
    or-int/2addr p0, v0

    #@b
    .line 1017
    ushr-int/lit8 v0, p0, 0x2

    #@d
    or-int/2addr p0, v0

    #@e
    .line 1018
    ushr-int/lit8 v0, p0, 0x1

    #@10
    or-int/2addr p0, v0

    #@11
    .line 1019
    add-int/lit8 v0, p0, 0x1

    #@13
    return v0
.end method

.method private scaleImage(Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .registers 23
    .parameter "filename"
    .parameter "width"
    .parameter "height"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 935
    new-instance v5, Landroid/graphics/BitmapFactory$Options;

    #@2
    invoke-direct {v5}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    #@5
    .line 936
    .local v5, dbo:Landroid/graphics/BitmapFactory$Options;
    const/4 v13, 0x1

    #@6
    iput-boolean v13, v5, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    #@8
    .line 937
    move-object/from16 v0, p1

    #@a
    invoke-static {v0, v5}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@d
    .line 939
    iget v9, v5, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    #@f
    .line 940
    .local v9, nativeWidth:I
    iget v8, v5, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    #@11
    .line 941
    .local v8, nativeHeight:I
    const-string v13, "MediaImageItem"

    #@13
    const/4 v14, 0x3

    #@14
    invoke-static {v13, v14}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@17
    move-result v13

    #@18
    if-eqz v13, :cond_56

    #@1a
    .line 942
    const-string v13, "MediaImageItem"

    #@1c
    new-instance v14, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v15, "generateThumbnail: Input: "

    #@23
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v14

    #@27
    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v14

    #@2b
    const-string/jumbo v15, "x"

    #@2e
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v14

    #@32
    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v14

    #@36
    const-string v15, ", resize to: "

    #@38
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v14

    #@3c
    move/from16 v0, p2

    #@3e
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v14

    #@42
    const-string/jumbo v15, "x"

    #@45
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v14

    #@49
    move/from16 v0, p3

    #@4b
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v14

    #@4f
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v14

    #@53
    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 948
    :cond_56
    move/from16 v0, p2

    #@58
    if-gt v9, v0, :cond_5e

    #@5a
    move/from16 v0, p3

    #@5c
    if-le v8, v0, :cond_ee

    #@5e
    .line 949
    :cond_5e
    int-to-float v13, v9

    #@5f
    move/from16 v0, p2

    #@61
    int-to-float v14, v0

    #@62
    div-float v6, v13, v14

    #@64
    .line 950
    .local v6, dx:F
    int-to-float v13, v8

    #@65
    move/from16 v0, p3

    #@67
    int-to-float v14, v0

    #@68
    div-float v7, v13, v14

    #@6a
    .line 952
    .local v7, dy:F
    cmpl-float v13, v6, v7

    #@6c
    if-lez v13, :cond_d0

    #@6e
    .line 953
    move/from16 v0, p2

    #@70
    int-to-float v3, v0

    #@71
    .line 955
    .local v3, bitmapWidth:F
    int-to-float v13, v8

    #@72
    div-float/2addr v13, v6

    #@73
    move/from16 v0, p3

    #@75
    int-to-float v14, v0

    #@76
    cmpg-float v13, v13, v14

    #@78
    if-gez v13, :cond_c7

    #@7a
    .line 956
    int-to-float v13, v8

    #@7b
    div-float/2addr v13, v6

    #@7c
    float-to-double v13, v13

    #@7d
    invoke-static {v13, v14}, Ljava/lang/Math;->ceil(D)D

    #@80
    move-result-wide v13

    #@81
    double-to-float v2, v13

    #@82
    .line 974
    .local v2, bitmapHeight:F
    :goto_82
    int-to-float v13, v9

    #@83
    div-float/2addr v13, v3

    #@84
    int-to-float v14, v8

    #@85
    div-float/2addr v14, v2

    #@86
    invoke-static {v13, v14}, Ljava/lang/Math;->max(FF)F

    #@89
    move-result v13

    #@8a
    float-to-double v13, v13

    #@8b
    invoke-static {v13, v14}, Ljava/lang/Math;->ceil(D)D

    #@8e
    move-result-wide v13

    #@8f
    double-to-int v11, v13

    #@90
    .line 977
    .local v11, sampleSize:I
    invoke-static {v11}, Landroid/media/videoeditor/MediaImageItem;->nextPowerOf2(I)I

    #@93
    move-result v11

    #@94
    .line 978
    new-instance v10, Landroid/graphics/BitmapFactory$Options;

    #@96
    invoke-direct {v10}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    #@99
    .line 979
    .local v10, options:Landroid/graphics/BitmapFactory$Options;
    iput v11, v10, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    #@9b
    .line 980
    move-object/from16 v0, p1

    #@9d
    invoke-static {v0, v10}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@a0
    move-result-object v12

    #@a1
    .line 988
    .end local v6           #dx:F
    .end local v7           #dy:F
    .end local v10           #options:Landroid/graphics/BitmapFactory$Options;
    .end local v11           #sampleSize:I
    .local v12, srcBitmap:Landroid/graphics/Bitmap;
    :goto_a1
    if-nez v12, :cond_f9

    #@a3
    .line 989
    const-string v13, "MediaImageItem"

    #@a5
    const-string v14, "generateThumbnail: Cannot decode image bytes"

    #@a7
    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@aa
    .line 990
    new-instance v13, Ljava/io/IOException;

    #@ac
    new-instance v14, Ljava/lang/StringBuilder;

    #@ae
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@b1
    const-string v15, "Cannot decode file: "

    #@b3
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v14

    #@b7
    move-object/from16 v0, p0

    #@b9
    iget-object v15, v0, Landroid/media/videoeditor/MediaItem;->mFilename:Ljava/lang/String;

    #@bb
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v14

    #@bf
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c2
    move-result-object v14

    #@c3
    invoke-direct {v13, v14}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@c6
    throw v13

    #@c7
    .line 958
    .end local v2           #bitmapHeight:F
    .end local v12           #srcBitmap:Landroid/graphics/Bitmap;
    .restart local v6       #dx:F
    .restart local v7       #dy:F
    :cond_c7
    int-to-float v13, v8

    #@c8
    div-float/2addr v13, v6

    #@c9
    float-to-double v13, v13

    #@ca
    invoke-static {v13, v14}, Ljava/lang/Math;->floor(D)D

    #@cd
    move-result-wide v13

    #@ce
    double-to-float v2, v13

    #@cf
    .restart local v2       #bitmapHeight:F
    goto :goto_82

    #@d0
    .line 962
    .end local v2           #bitmapHeight:F
    .end local v3           #bitmapWidth:F
    :cond_d0
    int-to-float v13, v9

    #@d1
    div-float/2addr v13, v7

    #@d2
    move/from16 v0, p2

    #@d4
    int-to-float v14, v0

    #@d5
    cmpl-float v13, v13, v14

    #@d7
    if-lez v13, :cond_e5

    #@d9
    .line 963
    int-to-float v13, v9

    #@da
    div-float/2addr v13, v7

    #@db
    float-to-double v13, v13

    #@dc
    invoke-static {v13, v14}, Ljava/lang/Math;->floor(D)D

    #@df
    move-result-wide v13

    #@e0
    double-to-float v3, v13

    #@e1
    .line 968
    .restart local v3       #bitmapWidth:F
    :goto_e1
    move/from16 v0, p3

    #@e3
    int-to-float v2, v0

    #@e4
    .restart local v2       #bitmapHeight:F
    goto :goto_82

    #@e5
    .line 965
    .end local v2           #bitmapHeight:F
    .end local v3           #bitmapWidth:F
    :cond_e5
    int-to-float v13, v9

    #@e6
    div-float/2addr v13, v7

    #@e7
    float-to-double v13, v13

    #@e8
    invoke-static {v13, v14}, Ljava/lang/Math;->ceil(D)D

    #@eb
    move-result-wide v13

    #@ec
    double-to-float v3, v13

    #@ed
    .restart local v3       #bitmapWidth:F
    goto :goto_e1

    #@ee
    .line 982
    .end local v3           #bitmapWidth:F
    .end local v6           #dx:F
    .end local v7           #dy:F
    :cond_ee
    move/from16 v0, p2

    #@f0
    int-to-float v3, v0

    #@f1
    .line 983
    .restart local v3       #bitmapWidth:F
    move/from16 v0, p3

    #@f3
    int-to-float v2, v0

    #@f4
    .line 984
    .restart local v2       #bitmapHeight:F
    invoke-static/range {p1 .. p1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    #@f7
    move-result-object v12

    #@f8
    .restart local v12       #srcBitmap:Landroid/graphics/Bitmap;
    goto :goto_a1

    #@f9
    .line 996
    :cond_f9
    float-to-int v13, v3

    #@fa
    float-to-int v14, v2

    #@fb
    sget-object v15, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@fd
    invoke-static {v13, v14, v15}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    #@100
    move-result-object v1

    #@101
    .line 999
    .local v1, bitmap:Landroid/graphics/Bitmap;
    new-instance v4, Landroid/graphics/Canvas;

    #@103
    invoke-direct {v4, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    #@106
    .line 1000
    .local v4, canvas:Landroid/graphics/Canvas;
    new-instance v13, Landroid/graphics/Rect;

    #@108
    const/4 v14, 0x0

    #@109
    const/4 v15, 0x0

    #@10a
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    #@10d
    move-result v16

    #@10e
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    #@111
    move-result v17

    #@112
    invoke-direct/range {v13 .. v17}, Landroid/graphics/Rect;-><init>(IIII)V

    #@115
    new-instance v14, Landroid/graphics/Rect;

    #@117
    const/4 v15, 0x0

    #@118
    const/16 v16, 0x0

    #@11a
    float-to-int v0, v3

    #@11b
    move/from16 v17, v0

    #@11d
    float-to-int v0, v2

    #@11e
    move/from16 v18, v0

    #@120
    invoke-direct/range {v14 .. v18}, Landroid/graphics/Rect;-><init>(IIII)V

    #@123
    sget-object v15, Landroid/media/videoeditor/MediaImageItem;->sResizePaint:Landroid/graphics/Paint;

    #@125
    invoke-virtual {v4, v12, v13, v14, v15}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    #@128
    .line 1004
    const/4 v13, 0x0

    #@129
    invoke-virtual {v4, v13}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    #@12c
    .line 1008
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->recycle()V

    #@12f
    .line 1009
    return-object v1
.end method


# virtual methods
.method generateKenburnsClip(Landroid/media/videoeditor/EffectKenBurns;)Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;
    .registers 8
    .parameter "effectKB"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 848
    new-instance v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@3
    invoke-direct {v1}, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;-><init>()V

    #@6
    .line 849
    .local v1, editSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;
    const/4 v3, 0x1

    #@7
    new-array v3, v3, [Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@9
    iput-object v3, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@b
    .line 850
    const/4 v2, 0x0

    #@c
    .line 851
    .local v2, output:Ljava/lang/String;
    new-instance v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@e
    invoke-direct {v0}, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;-><init>()V

    #@11
    .line 852
    .local v0, clipSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;
    invoke-virtual {p0, v0}, Landroid/media/videoeditor/MediaImageItem;->initClipSettings(Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;)V

    #@14
    .line 853
    iget-object v3, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@16
    invoke-direct {p0, p1}, Landroid/media/videoeditor/MediaImageItem;->getKenBurns(Landroid/media/videoeditor/EffectKenBurns;)Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@19
    move-result-object v4

    #@1a
    aput-object v4, v3, v5

    #@1c
    .line 854
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getGeneratedImageClip()Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    if-nez v3, :cond_62

    #@22
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getRegenerateClip()Z

    #@25
    move-result v3

    #@26
    if-eqz v3, :cond_62

    #@28
    .line 855
    iget-object v3, p0, Landroid/media/videoeditor/MediaImageItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@2a
    invoke-virtual {v3, v1, p0}, Landroid/media/videoeditor/MediaArtistNativeHelper;->generateKenBurnsClip(Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;Landroid/media/videoeditor/MediaImageItem;)Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    .line 856
    invoke-virtual {p0, v2}, Landroid/media/videoeditor/MediaImageItem;->setGeneratedImageClip(Ljava/lang/String;)V

    #@31
    .line 857
    invoke-virtual {p0, v5}, Landroid/media/videoeditor/MediaImageItem;->setRegenerateClip(Z)V

    #@34
    .line 858
    iput-object v2, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipPath:Ljava/lang/String;

    #@36
    .line 859
    iput v5, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->fileType:I

    #@38
    .line 861
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getScaledHeight()I

    #@3b
    move-result v3

    #@3c
    iput v3, p0, Landroid/media/videoeditor/MediaImageItem;->mGeneratedClipHeight:I

    #@3e
    .line 862
    iget-object v3, p0, Landroid/media/videoeditor/MediaImageItem;->mVideoEditor:Landroid/media/videoeditor/VideoEditorImpl;

    #@40
    invoke-virtual {v3}, Landroid/media/videoeditor/VideoEditorImpl;->getAspectRatio()I

    #@43
    move-result v3

    #@44
    iget v4, p0, Landroid/media/videoeditor/MediaImageItem;->mGeneratedClipHeight:I

    #@46
    invoke-direct {p0, v3, v4}, Landroid/media/videoeditor/MediaImageItem;->getWidthByAspectRatioAndHeight(II)I

    #@49
    move-result v3

    #@4a
    iput v3, p0, Landroid/media/videoeditor/MediaImageItem;->mGeneratedClipWidth:I

    #@4c
    .line 877
    :goto_4c
    iget-object v3, p0, Landroid/media/videoeditor/MediaImageItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@4e
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getRenderingMode()I

    #@51
    move-result v4

    #@52
    invoke-virtual {v3, v4}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getMediaItemRenderingMode(I)I

    #@55
    move-result v3

    #@56
    iput v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->mediaRendering:I

    #@58
    .line 878
    iput v5, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->beginCutTime:I

    #@5a
    .line 879
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getTimelineDuration()J

    #@5d
    move-result-wide v3

    #@5e
    long-to-int v3, v3

    #@5f
    iput v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutTime:I

    #@61
    .line 881
    return-object v0

    #@62
    .line 865
    :cond_62
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getGeneratedImageClip()Ljava/lang/String;

    #@65
    move-result-object v3

    #@66
    if-nez v3, :cond_7e

    #@68
    .line 866
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getDecodedImageFileName()Ljava/lang/String;

    #@6b
    move-result-object v3

    #@6c
    iput-object v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipPath:Ljava/lang/String;

    #@6e
    .line 867
    const/4 v3, 0x5

    #@6f
    iput v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->fileType:I

    #@71
    .line 869
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getScaledWidth()I

    #@74
    move-result v3

    #@75
    iput v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->rgbWidth:I

    #@77
    .line 870
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getScaledHeight()I

    #@7a
    move-result v3

    #@7b
    iput v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->rgbHeight:I

    #@7d
    goto :goto_4c

    #@7e
    .line 873
    :cond_7e
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getGeneratedImageClip()Ljava/lang/String;

    #@81
    move-result-object v3

    #@82
    iput-object v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipPath:Ljava/lang/String;

    #@84
    .line 874
    iput v5, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->fileType:I

    #@86
    goto :goto_4c
.end method

.method public getAspectRatio()I
    .registers 2

    #@0
    .prologue
    .line 281
    iget v0, p0, Landroid/media/videoeditor/MediaImageItem;->mAspectRatio:I

    #@2
    return v0
.end method

.method getDecodedImageFileName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 243
    iget-object v0, p0, Landroid/media/videoeditor/MediaImageItem;->mDecodedFilename:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getDuration()J
    .registers 3

    #@0
    .prologue
    .line 591
    iget-wide v0, p0, Landroid/media/videoeditor/MediaImageItem;->mDurationMs:J

    #@2
    return-wide v0
.end method

.method public getFileType()I
    .registers 3

    #@0
    .prologue
    .line 207
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mFilename:Ljava/lang/String;

    #@2
    const-string v1, ".jpg"

    #@4
    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_28

    #@a
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mFilename:Ljava/lang/String;

    #@c
    const-string v1, ".jpeg"

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@11
    move-result v0

    #@12
    if-nez v0, :cond_28

    #@14
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mFilename:Ljava/lang/String;

    #@16
    const-string v1, ".JPG"

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@1b
    move-result v0

    #@1c
    if-nez v0, :cond_28

    #@1e
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mFilename:Ljava/lang/String;

    #@20
    const-string v1, ".JPEG"

    #@22
    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@25
    move-result v0

    #@26
    if-eqz v0, :cond_2a

    #@28
    .line 209
    :cond_28
    const/4 v0, 0x5

    #@29
    .line 213
    :goto_29
    return v0

    #@2a
    .line 210
    :cond_2a
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mFilename:Ljava/lang/String;

    #@2c
    const-string v1, ".png"

    #@2e
    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@31
    move-result v0

    #@32
    if-nez v0, :cond_3e

    #@34
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mFilename:Ljava/lang/String;

    #@36
    const-string v1, ".PNG"

    #@38
    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@3b
    move-result v0

    #@3c
    if-eqz v0, :cond_41

    #@3e
    .line 211
    :cond_3e
    const/16 v0, 0x8

    #@40
    goto :goto_29

    #@41
    .line 213
    :cond_41
    const/16 v0, 0xff

    #@43
    goto :goto_29
.end method

.method getGeneratedClipHeight()I
    .registers 2

    #@0
    .prologue
    .line 228
    iget v0, p0, Landroid/media/videoeditor/MediaImageItem;->mGeneratedClipHeight:I

    #@2
    return v0
.end method

.method getGeneratedClipWidth()I
    .registers 2

    #@0
    .prologue
    .line 235
    iget v0, p0, Landroid/media/videoeditor/MediaImageItem;->mGeneratedClipWidth:I

    #@2
    return v0
.end method

.method getGeneratedImageClip()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 583
    invoke-super {p0}, Landroid/media/videoeditor/MediaItem;->getGeneratedImageClip()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getHeight()I
    .registers 2

    #@0
    .prologue
    .line 259
    iget v0, p0, Landroid/media/videoeditor/MediaImageItem;->mHeight:I

    #@2
    return v0
.end method

.method getImageClipProperties()Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;
    .registers 9

    #@0
    .prologue
    .line 890
    new-instance v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@2
    invoke-direct {v0}, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;-><init>()V

    #@5
    .line 891
    .local v0, clipSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;
    const/4 v4, 0x0

    #@6
    .line 892
    .local v4, effects:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Effect;>;"
    const/4 v2, 0x0

    #@7
    .line 893
    .local v2, effectKB:Landroid/media/videoeditor/EffectKenBurns;
    const/4 v3, 0x0

    #@8
    .line 895
    .local v3, effectKBPresent:Z
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getAllEffects()Ljava/util/List;

    #@b
    move-result-object v4

    #@c
    .line 896
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@f
    move-result-object v5

    #@10
    .local v5, i$:Ljava/util/Iterator;
    :cond_10
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@13
    move-result v6

    #@14
    if-eqz v6, :cond_24

    #@16
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@19
    move-result-object v1

    #@1a
    check-cast v1, Landroid/media/videoeditor/Effect;

    #@1c
    .line 897
    .local v1, effect:Landroid/media/videoeditor/Effect;
    instance-of v6, v1, Landroid/media/videoeditor/EffectKenBurns;

    #@1e
    if-eqz v6, :cond_10

    #@20
    move-object v2, v1

    #@21
    .line 898
    check-cast v2, Landroid/media/videoeditor/EffectKenBurns;

    #@23
    .line 899
    const/4 v3, 0x1

    #@24
    .line 904
    .end local v1           #effect:Landroid/media/videoeditor/Effect;
    :cond_24
    if-eqz v3, :cond_2b

    #@26
    .line 905
    invoke-virtual {p0, v2}, Landroid/media/videoeditor/MediaImageItem;->generateKenburnsClip(Landroid/media/videoeditor/EffectKenBurns;)Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@29
    move-result-object v0

    #@2a
    .line 921
    :goto_2a
    return-object v0

    #@2b
    .line 910
    :cond_2b
    invoke-virtual {p0, v0}, Landroid/media/videoeditor/MediaImageItem;->initClipSettings(Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;)V

    #@2e
    .line 911
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getDecodedImageFileName()Ljava/lang/String;

    #@31
    move-result-object v6

    #@32
    iput-object v6, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipPath:Ljava/lang/String;

    #@34
    .line 912
    const/4 v6, 0x5

    #@35
    iput v6, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->fileType:I

    #@37
    .line 913
    const/4 v6, 0x0

    #@38
    iput v6, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->beginCutTime:I

    #@3a
    .line 914
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getTimelineDuration()J

    #@3d
    move-result-wide v6

    #@3e
    long-to-int v6, v6

    #@3f
    iput v6, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutTime:I

    #@41
    .line 915
    iget-object v6, p0, Landroid/media/videoeditor/MediaImageItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@43
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getRenderingMode()I

    #@46
    move-result v7

    #@47
    invoke-virtual {v6, v7}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getMediaItemRenderingMode(I)I

    #@4a
    move-result v6

    #@4b
    iput v6, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->mediaRendering:I

    #@4d
    .line 917
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getScaledWidth()I

    #@50
    move-result v6

    #@51
    iput v6, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->rgbWidth:I

    #@53
    .line 918
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getScaledHeight()I

    #@56
    move-result v6

    #@57
    iput v6, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->rgbHeight:I

    #@59
    goto :goto_2a
.end method

.method public getScaledHeight()I
    .registers 2

    #@0
    .prologue
    .line 273
    iget v0, p0, Landroid/media/videoeditor/MediaImageItem;->mScaledHeight:I

    #@2
    return v0
.end method

.method getScaledImageFileName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 221
    iget-object v0, p0, Landroid/media/videoeditor/MediaImageItem;->mScaledFilename:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getScaledWidth()I
    .registers 2

    #@0
    .prologue
    .line 266
    iget v0, p0, Landroid/media/videoeditor/MediaImageItem;->mScaledWidth:I

    #@2
    return v0
.end method

.method public getThumbnail(IIJ)Landroid/graphics/Bitmap;
    .registers 12
    .parameter "width"
    .parameter "height"
    .parameter "timeMs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 607
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getGeneratedImageClip()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    if-eqz v0, :cond_15

    #@6
    .line 608
    iget-object v0, p0, Landroid/media/videoeditor/MediaImageItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@8
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getGeneratedImageClip()Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    const/4 v6, 0x0

    #@d
    move v2, p1

    #@e
    move v3, p2

    #@f
    move-wide v4, p3

    #@10
    invoke-virtual/range {v0 .. v6}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getPixels(Ljava/lang/String;IIJI)Landroid/graphics/Bitmap;

    #@13
    move-result-object v0

    #@14
    .line 611
    :goto_14
    return-object v0

    #@15
    :cond_15
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mFilename:Ljava/lang/String;

    #@17
    invoke-direct {p0, v0, p1, p2}, Landroid/media/videoeditor/MediaImageItem;->scaleImage(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    #@1a
    move-result-object v0

    #@1b
    goto :goto_14
.end method

.method public getThumbnailList(IIJJI[ILandroid/media/videoeditor/MediaItem$GetThumbnailListCallback;)V
    .registers 27
    .parameter "width"
    .parameter "height"
    .parameter "startMs"
    .parameter "endMs"
    .parameter "thumbnailCount"
    .parameter "indices"
    .parameter "callback"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 626
    invoke-virtual/range {p0 .. p0}, Landroid/media/videoeditor/MediaImageItem;->getGeneratedImageClip()Ljava/lang/String;

    #@3
    move-result-object v3

    #@4
    if-nez v3, :cond_26

    #@6
    .line 627
    move-object/from16 v0, p0

    #@8
    iget-object v3, v0, Landroid/media/videoeditor/MediaItem;->mFilename:Ljava/lang/String;

    #@a
    move-object/from16 v0, p0

    #@c
    move/from16 v1, p1

    #@e
    move/from16 v2, p2

    #@10
    invoke-direct {v0, v3, v1, v2}, Landroid/media/videoeditor/MediaImageItem;->scaleImage(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    #@13
    move-result-object v16

    #@14
    .line 628
    .local v16, thumbnail:Landroid/graphics/Bitmap;
    const/4 v15, 0x0

    #@15
    .local v15, i:I
    :goto_15
    move-object/from16 v0, p8

    #@17
    array-length v3, v0

    #@18
    if-ge v15, v3, :cond_5c

    #@1a
    .line 629
    aget v3, p8, v15

    #@1c
    move-object/from16 v0, p9

    #@1e
    move-object/from16 v1, v16

    #@20
    invoke-interface {v0, v1, v3}, Landroid/media/videoeditor/MediaItem$GetThumbnailListCallback;->onThumbnail(Landroid/graphics/Bitmap;I)V

    #@23
    .line 628
    add-int/lit8 v15, v15, 0x1

    #@25
    goto :goto_15

    #@26
    .line 632
    .end local v15           #i:I
    .end local v16           #thumbnail:Landroid/graphics/Bitmap;
    :cond_26
    cmp-long v3, p3, p5

    #@28
    if-lez v3, :cond_32

    #@2a
    .line 633
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@2c
    const-string v4, "Start time is greater than end time"

    #@2e
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@31
    throw v3

    #@32
    .line 636
    :cond_32
    move-object/from16 v0, p0

    #@34
    iget-wide v3, v0, Landroid/media/videoeditor/MediaImageItem;->mDurationMs:J

    #@36
    cmp-long v3, p5, v3

    #@38
    if-lez v3, :cond_42

    #@3a
    .line 637
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@3c
    const-string v4, "End time is greater than file duration"

    #@3e
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@41
    throw v3

    #@42
    .line 640
    :cond_42
    move-object/from16 v0, p0

    #@44
    iget-object v3, v0, Landroid/media/videoeditor/MediaImageItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@46
    invoke-virtual/range {p0 .. p0}, Landroid/media/videoeditor/MediaImageItem;->getGeneratedImageClip()Ljava/lang/String;

    #@49
    move-result-object v4

    #@4a
    const/4 v14, 0x0

    #@4b
    move/from16 v5, p1

    #@4d
    move/from16 v6, p2

    #@4f
    move-wide/from16 v7, p3

    #@51
    move-wide/from16 v9, p5

    #@53
    move/from16 v11, p7

    #@55
    move-object/from16 v12, p8

    #@57
    move-object/from16 v13, p9

    #@59
    invoke-virtual/range {v3 .. v14}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getPixelsList(Ljava/lang/String;IIJJI[ILandroid/media/videoeditor/MediaItem$GetThumbnailListCallback;I)V

    #@5c
    .line 643
    :cond_5c
    return-void
.end method

.method public getTimelineDuration()J
    .registers 3

    #@0
    .prologue
    .line 599
    iget-wide v0, p0, Landroid/media/videoeditor/MediaImageItem;->mDurationMs:J

    #@2
    return-wide v0
.end method

.method public getWidth()I
    .registers 2

    #@0
    .prologue
    .line 251
    iget v0, p0, Landroid/media/videoeditor/MediaImageItem;->mWidth:I

    #@2
    return v0
.end method

.method invalidate()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 733
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getGeneratedImageClip()Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    if-eqz v0, :cond_1a

    #@7
    .line 734
    new-instance v0, Ljava/io/File;

    #@9
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getGeneratedImageClip()Ljava/lang/String;

    #@c
    move-result-object v1

    #@d
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@10
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@13
    .line 735
    invoke-virtual {p0, v2}, Landroid/media/videoeditor/MediaImageItem;->setGeneratedImageClip(Ljava/lang/String;)V

    #@16
    .line 736
    const/4 v0, 0x1

    #@17
    invoke-virtual {p0, v0}, Landroid/media/videoeditor/MediaImageItem;->setRegenerateClip(Z)V

    #@1a
    .line 739
    :cond_1a
    iget-object v0, p0, Landroid/media/videoeditor/MediaImageItem;->mScaledFilename:Ljava/lang/String;

    #@1c
    if-eqz v0, :cond_30

    #@1e
    .line 740
    iget-object v0, p0, Landroid/media/videoeditor/MediaImageItem;->mFileName:Ljava/lang/String;

    #@20
    iget-object v1, p0, Landroid/media/videoeditor/MediaImageItem;->mScaledFilename:Ljava/lang/String;

    #@22
    if-eq v0, v1, :cond_2e

    #@24
    .line 741
    new-instance v0, Ljava/io/File;

    #@26
    iget-object v1, p0, Landroid/media/videoeditor/MediaImageItem;->mScaledFilename:Ljava/lang/String;

    #@28
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@2b
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@2e
    .line 743
    :cond_2e
    iput-object v2, p0, Landroid/media/videoeditor/MediaImageItem;->mScaledFilename:Ljava/lang/String;

    #@30
    .line 746
    :cond_30
    iget-object v0, p0, Landroid/media/videoeditor/MediaImageItem;->mDecodedFilename:Ljava/lang/String;

    #@32
    if-eqz v0, :cond_40

    #@34
    .line 747
    new-instance v0, Ljava/io/File;

    #@36
    iget-object v1, p0, Landroid/media/videoeditor/MediaImageItem;->mDecodedFilename:Ljava/lang/String;

    #@38
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@3b
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@3e
    .line 748
    iput-object v2, p0, Landroid/media/videoeditor/MediaImageItem;->mDecodedFilename:Ljava/lang/String;

    #@40
    .line 750
    :cond_40
    return-void
.end method

.method invalidateTransitions(JJ)V
    .registers 14
    .parameter "startTimeMs"
    .parameter "durationMs"

    #@0
    .prologue
    .line 653
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@2
    if-eqz v0, :cond_1a

    #@4
    .line 654
    const-wide/16 v5, 0x0

    #@6
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@8
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@b
    move-result-wide v7

    #@c
    move-object v0, p0

    #@d
    move-wide v1, p1

    #@e
    move-wide v3, p3

    #@f
    invoke-virtual/range {v0 .. v8}, Landroid/media/videoeditor/MediaImageItem;->isOverlapping(JJJJ)Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_1a

    #@15
    .line 655
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@17
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@1a
    .line 659
    :cond_1a
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@1c
    if-eqz v0, :cond_38

    #@1e
    .line 660
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@20
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@23
    move-result-wide v7

    #@24
    .line 661
    .local v7, transitionDurationMs:J
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getDuration()J

    #@27
    move-result-wide v0

    #@28
    sub-long v5, v0, v7

    #@2a
    move-object v0, p0

    #@2b
    move-wide v1, p1

    #@2c
    move-wide v3, p3

    #@2d
    invoke-virtual/range {v0 .. v8}, Landroid/media/videoeditor/MediaImageItem;->isOverlapping(JJJJ)Z

    #@30
    move-result v0

    #@31
    if-eqz v0, :cond_38

    #@33
    .line 663
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@35
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@38
    .line 666
    .end local v7           #transitionDurationMs:J
    :cond_38
    return-void
.end method

.method invalidateTransitions(JJJJ)V
    .registers 20
    .parameter "oldStartTimeMs"
    .parameter "oldDurationMs"
    .parameter "newStartTimeMs"
    .parameter "newDurationMs"

    #@0
    .prologue
    .line 677
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@2
    if-eqz v0, :cond_25

    #@4
    .line 678
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@6
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@9
    move-result-wide v7

    #@a
    .line 679
    .local v7, transitionDurationMs:J
    const-wide/16 v5, 0x0

    #@c
    move-object v0, p0

    #@d
    move-wide v1, p1

    #@e
    move-wide v3, p3

    #@f
    invoke-virtual/range {v0 .. v8}, Landroid/media/videoeditor/MediaImageItem;->isOverlapping(JJJJ)Z

    #@12
    move-result v10

    #@13
    .line 681
    .local v10, oldOverlap:Z
    const-wide/16 v5, 0x0

    #@15
    move-object v0, p0

    #@16
    move-wide/from16 v1, p5

    #@18
    move-wide/from16 v3, p7

    #@1a
    invoke-virtual/range {v0 .. v8}, Landroid/media/videoeditor/MediaImageItem;->isOverlapping(JJJJ)Z

    #@1d
    move-result v9

    #@1e
    .line 691
    .local v9, newOverlap:Z
    if-eq v9, v10, :cond_4f

    #@20
    .line 692
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@22
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@25
    .line 702
    .end local v7           #transitionDurationMs:J
    .end local v9           #newOverlap:Z
    .end local v10           #oldOverlap:Z
    :cond_25
    :goto_25
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@27
    if-eqz v0, :cond_4e

    #@29
    .line 703
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@2b
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@2e
    move-result-wide v7

    #@2f
    .line 704
    .restart local v7       #transitionDurationMs:J
    iget-wide v0, p0, Landroid/media/videoeditor/MediaImageItem;->mDurationMs:J

    #@31
    sub-long v5, v0, v7

    #@33
    move-object v0, p0

    #@34
    move-wide v1, p1

    #@35
    move-wide v3, p3

    #@36
    invoke-virtual/range {v0 .. v8}, Landroid/media/videoeditor/MediaImageItem;->isOverlapping(JJJJ)Z

    #@39
    move-result v10

    #@3a
    .line 706
    .restart local v10       #oldOverlap:Z
    iget-wide v0, p0, Landroid/media/videoeditor/MediaImageItem;->mDurationMs:J

    #@3c
    sub-long v5, v0, v7

    #@3e
    move-object v0, p0

    #@3f
    move-wide/from16 v1, p5

    #@41
    move-wide/from16 v3, p7

    #@43
    invoke-virtual/range {v0 .. v8}, Landroid/media/videoeditor/MediaImageItem;->isOverlapping(JJJJ)Z

    #@46
    move-result v9

    #@47
    .line 716
    .restart local v9       #newOverlap:Z
    if-eq v9, v10, :cond_67

    #@49
    .line 717
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@4b
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@4e
    .line 726
    .end local v7           #transitionDurationMs:J
    .end local v9           #newOverlap:Z
    .end local v10           #oldOverlap:Z
    :cond_4e
    :goto_4e
    return-void

    #@4f
    .line 693
    .restart local v7       #transitionDurationMs:J
    .restart local v9       #newOverlap:Z
    .restart local v10       #oldOverlap:Z
    :cond_4f
    if-eqz v9, :cond_25

    #@51
    .line 694
    cmp-long v0, p1, p5

    #@53
    if-nez v0, :cond_61

    #@55
    add-long v0, p1, p3

    #@57
    cmp-long v0, v0, v7

    #@59
    if-lez v0, :cond_61

    #@5b
    add-long v0, p5, p7

    #@5d
    cmp-long v0, v0, v7

    #@5f
    if-gtz v0, :cond_25

    #@61
    .line 697
    :cond_61
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mBeginTransition:Landroid/media/videoeditor/Transition;

    #@63
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@66
    goto :goto_25

    #@67
    .line 718
    :cond_67
    if-eqz v9, :cond_4e

    #@69
    .line 719
    add-long v0, p1, p3

    #@6b
    add-long v2, p5, p7

    #@6d
    cmp-long v0, v0, v2

    #@6f
    if-nez v0, :cond_7f

    #@71
    iget-wide v0, p0, Landroid/media/videoeditor/MediaImageItem;->mDurationMs:J

    #@73
    sub-long/2addr v0, v7

    #@74
    cmp-long v0, p1, v0

    #@76
    if-gtz v0, :cond_7f

    #@78
    iget-wide v0, p0, Landroid/media/videoeditor/MediaImageItem;->mDurationMs:J

    #@7a
    sub-long/2addr v0, v7

    #@7b
    cmp-long v0, p5, v0

    #@7d
    if-lez v0, :cond_4e

    #@7f
    .line 722
    :cond_7f
    iget-object v0, p0, Landroid/media/videoeditor/MediaItem;->mEndTransition:Landroid/media/videoeditor/Transition;

    #@81
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@84
    goto :goto_4e
.end method

.method public setDuration(J)V
    .registers 8
    .parameter "durationMs"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 292
    iget-wide v2, p0, Landroid/media/videoeditor/MediaImageItem;->mDurationMs:J

    #@3
    cmp-long v2, p1, v2

    #@5
    if-nez v2, :cond_8

    #@7
    .line 340
    :goto_7
    return-void

    #@8
    .line 296
    :cond_8
    iget-object v2, p0, Landroid/media/videoeditor/MediaImageItem;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@a
    invoke-virtual {v2, v4}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@d
    .line 310
    invoke-direct {p0}, Landroid/media/videoeditor/MediaImageItem;->invalidateEndTransition()V

    #@10
    .line 312
    iput-wide p1, p0, Landroid/media/videoeditor/MediaImageItem;->mDurationMs:J

    #@12
    .line 314
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->adjustTransitions()V

    #@15
    .line 315
    invoke-direct {p0}, Landroid/media/videoeditor/MediaImageItem;->adjustOverlays()Ljava/util/List;

    #@18
    move-result-object v1

    #@19
    .line 316
    .local v1, adjustedOverlays:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Overlay;>;"
    invoke-direct {p0}, Landroid/media/videoeditor/MediaImageItem;->adjustEffects()Ljava/util/List;

    #@1c
    move-result-object v0

    #@1d
    .line 326
    .local v0, adjustedEffects:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Effect;>;"
    invoke-direct {p0, v0, v1}, Landroid/media/videoeditor/MediaImageItem;->invalidateBeginTransition(Ljava/util/List;Ljava/util/List;)V

    #@20
    .line 327
    invoke-direct {p0}, Landroid/media/videoeditor/MediaImageItem;->invalidateEndTransition()V

    #@23
    .line 328
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getGeneratedImageClip()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    if-eqz v2, :cond_3c

    #@29
    .line 332
    new-instance v2, Ljava/io/File;

    #@2b
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getGeneratedImageClip()Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@32
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    #@35
    .line 336
    const/4 v2, 0x0

    #@36
    invoke-virtual {p0, v2}, Landroid/media/videoeditor/MediaImageItem;->setGeneratedImageClip(Ljava/lang/String;)V

    #@39
    .line 337
    invoke-super {p0, v4}, Landroid/media/videoeditor/MediaItem;->setRegenerateClip(Z)V

    #@3c
    .line 339
    :cond_3c
    iget-object v2, p0, Landroid/media/videoeditor/MediaImageItem;->mVideoEditor:Landroid/media/videoeditor/VideoEditorImpl;

    #@3e
    invoke-virtual {v2}, Landroid/media/videoeditor/VideoEditorImpl;->updateTimelineDuration()V

    #@41
    goto :goto_7
.end method

.method setGeneratedImageClip(Ljava/lang/String;)V
    .registers 4
    .parameter "generatedFilePath"

    #@0
    .prologue
    .line 569
    invoke-super {p0, p1}, Landroid/media/videoeditor/MediaItem;->setGeneratedImageClip(Ljava/lang/String;)V

    #@3
    .line 572
    invoke-virtual {p0}, Landroid/media/videoeditor/MediaImageItem;->getScaledHeight()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/media/videoeditor/MediaImageItem;->mGeneratedClipHeight:I

    #@9
    .line 573
    iget-object v0, p0, Landroid/media/videoeditor/MediaImageItem;->mVideoEditor:Landroid/media/videoeditor/VideoEditorImpl;

    #@b
    invoke-virtual {v0}, Landroid/media/videoeditor/VideoEditorImpl;->getAspectRatio()I

    #@e
    move-result v0

    #@f
    iget v1, p0, Landroid/media/videoeditor/MediaImageItem;->mGeneratedClipHeight:I

    #@11
    invoke-direct {p0, v0, v1}, Landroid/media/videoeditor/MediaImageItem;->getWidthByAspectRatioAndHeight(II)I

    #@14
    move-result v0

    #@15
    iput v0, p0, Landroid/media/videoeditor/MediaImageItem;->mGeneratedClipWidth:I

    #@17
    .line 575
    return-void
.end method
