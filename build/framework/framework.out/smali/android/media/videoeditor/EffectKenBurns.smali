.class public Landroid/media/videoeditor/EffectKenBurns;
.super Landroid/media/videoeditor/Effect;
.source "EffectKenBurns.java"


# instance fields
.field private mEndRect:Landroid/graphics/Rect;

.field private mStartRect:Landroid/graphics/Rect;


# direct methods
.method private constructor <init>()V
    .registers 10

    #@0
    .prologue
    const-wide/16 v5, 0x0

    #@2
    const/4 v1, 0x0

    #@3
    .line 39
    move-object v0, p0

    #@4
    move-object v2, v1

    #@5
    move-object v3, v1

    #@6
    move-object v4, v1

    #@7
    move-wide v7, v5

    #@8
    invoke-direct/range {v0 .. v8}, Landroid/media/videoeditor/EffectKenBurns;-><init>(Landroid/media/videoeditor/MediaItem;Ljava/lang/String;Landroid/graphics/Rect;Landroid/graphics/Rect;JJ)V

    #@b
    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/media/videoeditor/MediaItem;Ljava/lang/String;Landroid/graphics/Rect;Landroid/graphics/Rect;JJ)V
    .registers 16
    .parameter "mediaItem"
    .parameter "effectId"
    .parameter "startRect"
    .parameter "endRect"
    .parameter "startTimeMs"
    .parameter "durationMs"

    #@0
    .prologue
    .line 54
    move-object v0, p0

    #@1
    move-object v1, p1

    #@2
    move-object v2, p2

    #@3
    move-wide v3, p5

    #@4
    move-wide v5, p7

    #@5
    invoke-direct/range {v0 .. v6}, Landroid/media/videoeditor/Effect;-><init>(Landroid/media/videoeditor/MediaItem;Ljava/lang/String;JJ)V

    #@8
    .line 56
    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    #@b
    move-result v0

    #@c
    if-lez v0, :cond_14

    #@e
    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    #@11
    move-result v0

    #@12
    if-gtz v0, :cond_1c

    #@14
    .line 57
    :cond_14
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@16
    const-string v1, "Invalid Start rectangle"

    #@18
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 59
    :cond_1c
    invoke-virtual {p4}, Landroid/graphics/Rect;->width()I

    #@1f
    move-result v0

    #@20
    if-lez v0, :cond_28

    #@22
    invoke-virtual {p4}, Landroid/graphics/Rect;->height()I

    #@25
    move-result v0

    #@26
    if-gtz v0, :cond_30

    #@28
    .line 60
    :cond_28
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@2a
    const-string v1, "Invalid End rectangle"

    #@2c
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2f
    throw v0

    #@30
    .line 63
    :cond_30
    iput-object p3, p0, Landroid/media/videoeditor/EffectKenBurns;->mStartRect:Landroid/graphics/Rect;

    #@32
    .line 64
    iput-object p4, p0, Landroid/media/videoeditor/EffectKenBurns;->mEndRect:Landroid/graphics/Rect;

    #@34
    .line 65
    return-void
.end method


# virtual methods
.method public getEndRect()Landroid/graphics/Rect;
    .registers 2

    #@0
    .prologue
    .line 84
    iget-object v0, p0, Landroid/media/videoeditor/EffectKenBurns;->mEndRect:Landroid/graphics/Rect;

    #@2
    return-object v0
.end method

.method getKenBurnsSettings(Landroid/graphics/Rect;Landroid/graphics/Rect;)V
    .registers 4
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 96
    invoke-virtual {p0}, Landroid/media/videoeditor/EffectKenBurns;->getStartRect()Landroid/graphics/Rect;

    #@3
    move-result-object v0

    #@4
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@6
    iput v0, p1, Landroid/graphics/Rect;->left:I

    #@8
    .line 97
    invoke-virtual {p0}, Landroid/media/videoeditor/EffectKenBurns;->getStartRect()Landroid/graphics/Rect;

    #@b
    move-result-object v0

    #@c
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@e
    iput v0, p1, Landroid/graphics/Rect;->top:I

    #@10
    .line 98
    invoke-virtual {p0}, Landroid/media/videoeditor/EffectKenBurns;->getStartRect()Landroid/graphics/Rect;

    #@13
    move-result-object v0

    #@14
    iget v0, v0, Landroid/graphics/Rect;->right:I

    #@16
    iput v0, p1, Landroid/graphics/Rect;->right:I

    #@18
    .line 99
    invoke-virtual {p0}, Landroid/media/videoeditor/EffectKenBurns;->getStartRect()Landroid/graphics/Rect;

    #@1b
    move-result-object v0

    #@1c
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    #@1e
    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    #@20
    .line 100
    invoke-virtual {p0}, Landroid/media/videoeditor/EffectKenBurns;->getEndRect()Landroid/graphics/Rect;

    #@23
    move-result-object v0

    #@24
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@26
    iput v0, p2, Landroid/graphics/Rect;->left:I

    #@28
    .line 101
    invoke-virtual {p0}, Landroid/media/videoeditor/EffectKenBurns;->getEndRect()Landroid/graphics/Rect;

    #@2b
    move-result-object v0

    #@2c
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@2e
    iput v0, p2, Landroid/graphics/Rect;->top:I

    #@30
    .line 102
    invoke-virtual {p0}, Landroid/media/videoeditor/EffectKenBurns;->getEndRect()Landroid/graphics/Rect;

    #@33
    move-result-object v0

    #@34
    iget v0, v0, Landroid/graphics/Rect;->right:I

    #@36
    iput v0, p2, Landroid/graphics/Rect;->right:I

    #@38
    .line 103
    invoke-virtual {p0}, Landroid/media/videoeditor/EffectKenBurns;->getEndRect()Landroid/graphics/Rect;

    #@3b
    move-result-object v0

    #@3c
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    #@3e
    iput v0, p2, Landroid/graphics/Rect;->bottom:I

    #@40
    .line 104
    return-void
.end method

.method public getStartRect()Landroid/graphics/Rect;
    .registers 2

    #@0
    .prologue
    .line 74
    iget-object v0, p0, Landroid/media/videoeditor/EffectKenBurns;->mStartRect:Landroid/graphics/Rect;

    #@2
    return-object v0
.end method
