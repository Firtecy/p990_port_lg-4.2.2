.class public Landroid/media/videoeditor/EffectColor;
.super Landroid/media/videoeditor/Effect;
.source "EffectColor.java"


# static fields
.field public static final GRAY:I = 0x7f7f7f

.field public static final GREEN:I = 0xff00

.field public static final PINK:I = 0xff66cc

.field public static final TYPE_COLOR:I = 0x1

.field public static final TYPE_FIFTIES:I = 0x5

.field public static final TYPE_GRADIENT:I = 0x2

.field public static final TYPE_NEGATIVE:I = 0x4

.field public static final TYPE_SEPIA:I = 0x3


# instance fields
.field private final mColor:I

.field private final mType:I


# direct methods
.method private constructor <init>()V
    .registers 10

    #@0
    .prologue
    const-wide/16 v3, 0x0

    #@2
    const/4 v1, 0x0

    #@3
    const/4 v7, 0x0

    #@4
    .line 76
    move-object v0, p0

    #@5
    move-object v2, v1

    #@6
    move-wide v5, v3

    #@7
    move v8, v7

    #@8
    invoke-direct/range {v0 .. v8}, Landroid/media/videoeditor/EffectColor;-><init>(Landroid/media/videoeditor/MediaItem;Ljava/lang/String;JJII)V

    #@b
    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/media/videoeditor/MediaItem;Ljava/lang/String;JJII)V
    .registers 12
    .parameter "mediaItem"
    .parameter "effectId"
    .parameter "startTimeMs"
    .parameter "durationMs"
    .parameter "type"
    .parameter "color"

    #@0
    .prologue
    .line 95
    invoke-direct/range {p0 .. p6}, Landroid/media/videoeditor/Effect;-><init>(Landroid/media/videoeditor/MediaItem;Ljava/lang/String;JJ)V

    #@3
    .line 96
    packed-switch p7, :pswitch_data_44

    #@6
    .line 119
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Invalid type: "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v0

    #@1f
    .line 99
    :pswitch_1f
    sparse-switch p8, :sswitch_data_52

    #@22
    .line 107
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@24
    new-instance v1, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v2, "Invalid Color: "

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1, p8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v1

    #@37
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@3a
    throw v0

    #@3b
    .line 103
    :sswitch_3b
    iput p8, p0, Landroid/media/videoeditor/EffectColor;->mColor:I

    #@3d
    .line 122
    :goto_3d
    iput p7, p0, Landroid/media/videoeditor/EffectColor;->mType:I

    #@3f
    .line 123
    return-void

    #@40
    .line 114
    :pswitch_40
    const/4 v0, -0x1

    #@41
    iput v0, p0, Landroid/media/videoeditor/EffectColor;->mColor:I

    #@43
    goto :goto_3d

    #@44
    .line 96
    :pswitch_data_44
    .packed-switch 0x1
        :pswitch_1f
        :pswitch_1f
        :pswitch_40
        :pswitch_40
        :pswitch_40
    .end packed-switch

    #@52
    .line 99
    :sswitch_data_52
    .sparse-switch
        0xff00 -> :sswitch_3b
        0x7f7f7f -> :sswitch_3b
        0xff66cc -> :sswitch_3b
    .end sparse-switch
.end method


# virtual methods
.method public getColor()I
    .registers 2

    #@0
    .prologue
    .line 140
    iget v0, p0, Landroid/media/videoeditor/EffectColor;->mColor:I

    #@2
    return v0
.end method

.method public getType()I
    .registers 2

    #@0
    .prologue
    .line 131
    iget v0, p0, Landroid/media/videoeditor/EffectColor;->mType:I

    #@2
    return v0
.end method
