.class public Landroid/media/videoeditor/VideoEditorImpl;
.super Ljava/lang/Object;
.source "VideoEditorImpl.java"

# interfaces
.implements Landroid/media/videoeditor/VideoEditor;


# static fields
.field private static final ATTR_AFTER_MEDIA_ITEM_ID:Ljava/lang/String; = "after_media_item"

.field private static final ATTR_ASPECT_RATIO:Ljava/lang/String; = "aspect_ratio"

.field private static final ATTR_AUDIO_WAVEFORM_FILENAME:Ljava/lang/String; = "waveform"

.field private static final ATTR_BEFORE_MEDIA_ITEM_ID:Ljava/lang/String; = "before_media_item"

.field private static final ATTR_BEGIN_TIME:Ljava/lang/String; = "begin_time"

.field private static final ATTR_BEHAVIOR:Ljava/lang/String; = "behavior"

.field private static final ATTR_BLENDING:Ljava/lang/String; = "blending"

.field private static final ATTR_COLOR_EFFECT_TYPE:Ljava/lang/String; = "color_type"

.field private static final ATTR_COLOR_EFFECT_VALUE:Ljava/lang/String; = "color_value"

.field private static final ATTR_DIRECTION:Ljava/lang/String; = "direction"

.field private static final ATTR_DUCKED_TRACK_VOLUME:Ljava/lang/String; = "ducking_volume"

.field private static final ATTR_DUCK_ENABLED:Ljava/lang/String; = "ducking_enabled"

.field private static final ATTR_DUCK_THRESHOLD:Ljava/lang/String; = "ducking_threshold"

.field private static final ATTR_DURATION:Ljava/lang/String; = "duration"

.field private static final ATTR_END_RECT_BOTTOM:Ljava/lang/String; = "end_b"

.field private static final ATTR_END_RECT_LEFT:Ljava/lang/String; = "end_l"

.field private static final ATTR_END_RECT_RIGHT:Ljava/lang/String; = "end_r"

.field private static final ATTR_END_RECT_TOP:Ljava/lang/String; = "end_t"

.field private static final ATTR_END_TIME:Ljava/lang/String; = "end_time"

.field private static final ATTR_FILENAME:Ljava/lang/String; = "filename"

.field private static final ATTR_GENERATED_IMAGE_CLIP:Ljava/lang/String; = "generated_image_clip"

.field private static final ATTR_GENERATED_TRANSITION_CLIP:Ljava/lang/String; = "generated_transition_clip"

.field private static final ATTR_ID:Ljava/lang/String; = "id"

.field private static final ATTR_INVERT:Ljava/lang/String; = "invert"

.field private static final ATTR_IS_IMAGE_CLIP_GENERATED:Ljava/lang/String; = "is_image_clip_generated"

.field private static final ATTR_IS_TRANSITION_GENERATED:Ljava/lang/String; = "is_transition_generated"

.field private static final ATTR_LOOP:Ljava/lang/String; = "loop"

.field private static final ATTR_MASK:Ljava/lang/String; = "mask"

.field private static final ATTR_MUTED:Ljava/lang/String; = "muted"

.field private static final ATTR_OVERLAY_FRAME_HEIGHT:Ljava/lang/String; = "overlay_frame_height"

.field private static final ATTR_OVERLAY_FRAME_WIDTH:Ljava/lang/String; = "overlay_frame_width"

.field private static final ATTR_OVERLAY_RESIZED_RGB_FRAME_HEIGHT:Ljava/lang/String; = "resized_RGBframe_height"

.field private static final ATTR_OVERLAY_RESIZED_RGB_FRAME_WIDTH:Ljava/lang/String; = "resized_RGBframe_width"

.field private static final ATTR_OVERLAY_RGB_FILENAME:Ljava/lang/String; = "overlay_rgb_filename"

.field private static final ATTR_REGENERATE_PCM:Ljava/lang/String; = "regeneratePCMFlag"

.field private static final ATTR_RENDERING_MODE:Ljava/lang/String; = "rendering_mode"

.field private static final ATTR_START_RECT_BOTTOM:Ljava/lang/String; = "start_b"

.field private static final ATTR_START_RECT_LEFT:Ljava/lang/String; = "start_l"

.field private static final ATTR_START_RECT_RIGHT:Ljava/lang/String; = "start_r"

.field private static final ATTR_START_RECT_TOP:Ljava/lang/String; = "start_t"

.field private static final ATTR_START_TIME:Ljava/lang/String; = "start_time"

.field private static final ATTR_TYPE:Ljava/lang/String; = "type"

.field private static final ATTR_VOLUME:Ljava/lang/String; = "volume"

.field private static final ENGINE_ACCESS_MAX_TIMEOUT_MS:I = 0x1f4

.field private static final PROJECT_FILENAME:Ljava/lang/String; = "videoeditor.xml"

.field private static final TAG:Ljava/lang/String; = "VideoEditorImpl"

.field private static final TAG_AUDIO_TRACK:Ljava/lang/String; = "audio_track"

.field private static final TAG_AUDIO_TRACKS:Ljava/lang/String; = "audio_tracks"

.field private static final TAG_EFFECT:Ljava/lang/String; = "effect"

.field private static final TAG_EFFECTS:Ljava/lang/String; = "effects"

.field private static final TAG_MEDIA_ITEM:Ljava/lang/String; = "media_item"

.field private static final TAG_MEDIA_ITEMS:Ljava/lang/String; = "media_items"

.field private static final TAG_OVERLAY:Ljava/lang/String; = "overlay"

.field private static final TAG_OVERLAYS:Ljava/lang/String; = "overlays"

.field private static final TAG_OVERLAY_USER_ATTRIBUTES:Ljava/lang/String; = "overlay_user_attributes"

.field private static final TAG_PROJECT:Ljava/lang/String; = "project"

.field private static final TAG_TRANSITION:Ljava/lang/String; = "transition"

.field private static final TAG_TRANSITIONS:Ljava/lang/String; = "transitions"


# instance fields
.field private mAspectRatio:I

.field private final mAudioTracks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/media/videoeditor/AudioTrack;",
            ">;"
        }
    .end annotation
.end field

.field private mDurationMs:J

.field private final mLock:Ljava/util/concurrent/Semaphore;

.field private mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

.field private final mMallocDebug:Z

.field private final mMediaItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/media/videoeditor/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private mPreviewInProgress:Z

.field private final mProjectPath:Ljava/lang/String;

.field private final mTransitions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/media/videoeditor/Transition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 8
    .parameter "projectPath"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 148
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 129
    new-instance v3, Ljava/util/ArrayList;

    #@7
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@a
    iput-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@c
    .line 130
    new-instance v3, Ljava/util/ArrayList;

    #@e
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@11
    iput-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mAudioTracks:Ljava/util/List;

    #@13
    .line 131
    new-instance v3, Ljava/util/ArrayList;

    #@15
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@18
    iput-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mTransitions:Ljava/util/List;

    #@1a
    .line 139
    iput-boolean v4, p0, Landroid/media/videoeditor/VideoEditorImpl;->mPreviewInProgress:Z

    #@1c
    .line 150
    const-string/jumbo v3, "libc.debug.malloc"

    #@1f
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    .line 151
    .local v2, s:Ljava/lang/String;
    const-string v3, "1"

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v3

    #@29
    if-eqz v3, :cond_5f

    #@2b
    .line 152
    iput-boolean v5, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMallocDebug:Z

    #@2d
    .line 154
    :try_start_2d
    const-string v3, "HeapAtStart"

    #@2f
    invoke-static {v3}, Landroid/media/videoeditor/VideoEditorImpl;->dumpHeap(Ljava/lang/String;)V
    :try_end_32
    .catch Ljava/lang/Exception; {:try_start_2d .. :try_end_32} :catch_56

    #@32
    .line 161
    :goto_32
    new-instance v3, Ljava/util/concurrent/Semaphore;

    #@34
    invoke-direct {v3, v5, v5}, Ljava/util/concurrent/Semaphore;-><init>(IZ)V

    #@37
    iput-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mLock:Ljava/util/concurrent/Semaphore;

    #@39
    .line 162
    new-instance v3, Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@3b
    iget-object v4, p0, Landroid/media/videoeditor/VideoEditorImpl;->mLock:Ljava/util/concurrent/Semaphore;

    #@3d
    invoke-direct {v3, p1, v4, p0}, Landroid/media/videoeditor/MediaArtistNativeHelper;-><init>(Ljava/lang/String;Ljava/util/concurrent/Semaphore;Landroid/media/videoeditor/VideoEditor;)V

    #@40
    iput-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@42
    .line 163
    iput-object p1, p0, Landroid/media/videoeditor/VideoEditorImpl;->mProjectPath:Ljava/lang/String;

    #@44
    .line 164
    new-instance v1, Ljava/io/File;

    #@46
    const-string/jumbo v3, "videoeditor.xml"

    #@49
    invoke-direct {v1, p1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@4c
    .line 165
    .local v1, projectXml:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@4f
    move-result v3

    #@50
    if-eqz v3, :cond_70

    #@52
    .line 167
    :try_start_52
    invoke-direct {p0}, Landroid/media/videoeditor/VideoEditorImpl;->load()V
    :try_end_55
    .catch Ljava/lang/Exception; {:try_start_52 .. :try_end_55} :catch_62

    #@55
    .line 176
    :goto_55
    return-void

    #@56
    .line 155
    .end local v1           #projectXml:Ljava/io/File;
    :catch_56
    move-exception v0

    #@57
    .line 156
    .local v0, ex:Ljava/lang/Exception;
    const-string v3, "VideoEditorImpl"

    #@59
    const-string v4, "dumpHeap returned error in constructor"

    #@5b
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    goto :goto_32

    #@5f
    .line 159
    .end local v0           #ex:Ljava/lang/Exception;
    :cond_5f
    iput-boolean v4, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMallocDebug:Z

    #@61
    goto :goto_32

    #@62
    .line 168
    .restart local v1       #projectXml:Ljava/io/File;
    :catch_62
    move-exception v0

    #@63
    .line 169
    .restart local v0       #ex:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@66
    .line 170
    new-instance v3, Ljava/io/IOException;

    #@68
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@6b
    move-result-object v4

    #@6c
    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@6f
    throw v3

    #@70
    .line 173
    .end local v0           #ex:Ljava/lang/Exception;
    :cond_70
    const/4 v3, 0x2

    #@71
    iput v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mAspectRatio:I

    #@73
    .line 174
    const-wide/16 v3, 0x0

    #@75
    iput-wide v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mDurationMs:J

    #@77
    goto :goto_55
.end method

.method private computeTimelineDuration()V
    .registers 8

    #@0
    .prologue
    .line 1799
    const-wide/16 v3, 0x0

    #@2
    iput-wide v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mDurationMs:J

    #@4
    .line 1800
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@6
    invoke-interface {v3}, Ljava/util/List;->size()I

    #@9
    move-result v2

    #@a
    .line 1801
    .local v2, mediaItemsCount:I
    const/4 v0, 0x0

    #@b
    .local v0, i:I
    :goto_b
    if-ge v0, v2, :cond_38

    #@d
    .line 1802
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@f
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v1

    #@13
    check-cast v1, Landroid/media/videoeditor/MediaItem;

    #@15
    .line 1803
    .local v1, mediaItem:Landroid/media/videoeditor/MediaItem;
    iget-wide v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mDurationMs:J

    #@17
    invoke-virtual {v1}, Landroid/media/videoeditor/MediaItem;->getTimelineDuration()J

    #@1a
    move-result-wide v5

    #@1b
    add-long/2addr v3, v5

    #@1c
    iput-wide v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mDurationMs:J

    #@1e
    .line 1804
    invoke-virtual {v1}, Landroid/media/videoeditor/MediaItem;->getEndTransition()Landroid/media/videoeditor/Transition;

    #@21
    move-result-object v3

    #@22
    if-eqz v3, :cond_35

    #@24
    .line 1805
    add-int/lit8 v3, v2, -0x1

    #@26
    if-ge v0, v3, :cond_35

    #@28
    .line 1806
    iget-wide v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mDurationMs:J

    #@2a
    invoke-virtual {v1}, Landroid/media/videoeditor/MediaItem;->getEndTransition()Landroid/media/videoeditor/Transition;

    #@2d
    move-result-object v5

    #@2e
    invoke-virtual {v5}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@31
    move-result-wide v5

    #@32
    sub-long/2addr v3, v5

    #@33
    iput-wide v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mDurationMs:J

    #@35
    .line 1801
    :cond_35
    add-int/lit8 v0, v0, 0x1

    #@37
    goto :goto_b

    #@38
    .line 1810
    .end local v1           #mediaItem:Landroid/media/videoeditor/MediaItem;
    :cond_38
    return-void
.end method

.method private static dumpHeap(Ljava/lang/String;)V
    .registers 7
    .parameter "filename"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    #@0
    .prologue
    .line 1955
    invoke-static {}, Ljava/lang/System;->gc()V

    #@3
    .line 1956
    invoke-static {}, Ljava/lang/System;->runFinalization()V

    #@6
    .line 1957
    const-wide/16 v3, 0x3e8

    #@8
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V

    #@b
    .line 1958
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    #@e
    move-result-object v2

    #@f
    .line 1959
    .local v2, state:Ljava/lang/String;
    const-string/jumbo v3, "mounted"

    #@12
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v3

    #@16
    if-eqz v3, :cond_99

    #@18
    .line 1960
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3}, Ljava/io/File;->toString()Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    .line 1965
    .local v0, extDir:Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    #@22
    new-instance v4, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    const-string v5, "/"

    #@2d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    const-string v5, ".dump"

    #@37
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v4

    #@3f
    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@42
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    #@45
    move-result v3

    #@46
    if-eqz v3, :cond_6d

    #@48
    .line 1966
    new-instance v3, Ljava/io/File;

    #@4a
    new-instance v4, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v4

    #@53
    const-string v5, "/"

    #@55
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v4

    #@59
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v4

    #@5d
    const-string v5, ".dump"

    #@5f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v4

    #@63
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v4

    #@67
    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@6a
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    #@6d
    .line 1970
    :cond_6d
    new-instance v1, Ljava/io/FileOutputStream;

    #@6f
    new-instance v3, Ljava/lang/StringBuilder;

    #@71
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@74
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v3

    #@78
    const-string v4, "/"

    #@7a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v3

    #@7e
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v3

    #@82
    const-string v4, ".dump"

    #@84
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v3

    #@88
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8b
    move-result-object v3

    #@8c
    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    #@8f
    .line 1972
    .local v1, ost:Ljava/io/FileOutputStream;
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    #@92
    move-result-object v3

    #@93
    invoke-static {v3}, Landroid/os/Debug;->dumpNativeHeap(Ljava/io/FileDescriptor;)V

    #@96
    .line 1973
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    #@99
    .line 1975
    .end local v0           #extDir:Ljava/lang/String;
    .end local v1           #ost:Ljava/io/FileOutputStream;
    :cond_99
    return-void
.end method

.method private generateProjectThumbnail()V
    .registers 14

    #@0
    .prologue
    .line 1819
    new-instance v10, Ljava/io/File;

    #@2
    new-instance v11, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    iget-object v12, p0, Landroid/media/videoeditor/VideoEditorImpl;->mProjectPath:Ljava/lang/String;

    #@9
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v11

    #@d
    const-string v12, "/"

    #@f
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v11

    #@13
    const-string/jumbo v12, "thumbnail.jpg"

    #@16
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v11

    #@1a
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v11

    #@1e
    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@21
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    #@24
    move-result v10

    #@25
    if-eqz v10, :cond_4b

    #@27
    .line 1820
    new-instance v10, Ljava/io/File;

    #@29
    new-instance v11, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    iget-object v12, p0, Landroid/media/videoeditor/VideoEditorImpl;->mProjectPath:Ljava/lang/String;

    #@30
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v11

    #@34
    const-string v12, "/"

    #@36
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v11

    #@3a
    const-string/jumbo v12, "thumbnail.jpg"

    #@3d
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v11

    #@41
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v11

    #@45
    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@48
    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    #@4b
    .line 1825
    :cond_4b
    iget-object v10, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@4d
    invoke-interface {v10}, Ljava/util/List;->size()I

    #@50
    move-result v10

    #@51
    if-lez v10, :cond_d9

    #@53
    .line 1826
    iget-object v10, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@55
    const/4 v11, 0x0

    #@56
    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@59
    move-result-object v4

    #@5a
    check-cast v4, Landroid/media/videoeditor/MediaItem;

    #@5c
    .line 1830
    .local v4, mI:Landroid/media/videoeditor/MediaItem;
    const/16 v3, 0x1e0

    #@5e
    .line 1831
    .local v3, height:I
    invoke-virtual {v4}, Landroid/media/videoeditor/MediaItem;->getWidth()I

    #@61
    move-result v10

    #@62
    mul-int/2addr v10, v3

    #@63
    invoke-virtual {v4}, Landroid/media/videoeditor/MediaItem;->getHeight()I

    #@66
    move-result v11

    #@67
    div-int v9, v10, v11

    #@69
    .line 1833
    .local v9, width:I
    const/4 v6, 0x0

    #@6a
    .line 1834
    .local v6, projectBitmap:Landroid/graphics/Bitmap;
    invoke-virtual {v4}, Landroid/media/videoeditor/MediaItem;->getFilename()Ljava/lang/String;

    #@6d
    move-result-object v2

    #@6e
    .line 1835
    .local v2, filename:Ljava/lang/String;
    instance-of v10, v4, Landroid/media/videoeditor/MediaVideoItem;

    #@70
    if-eqz v10, :cond_da

    #@72
    .line 1836
    new-instance v7, Landroid/media/MediaMetadataRetriever;

    #@74
    invoke-direct {v7}, Landroid/media/MediaMetadataRetriever;-><init>()V

    #@77
    .line 1837
    .local v7, retriever:Landroid/media/MediaMetadataRetriever;
    invoke-virtual {v7, v2}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    #@7a
    .line 1838
    invoke-virtual {v7}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime()Landroid/graphics/Bitmap;

    #@7d
    move-result-object v0

    #@7e
    .line 1839
    .local v0, bitmap:Landroid/graphics/Bitmap;
    invoke-virtual {v7}, Landroid/media/MediaMetadataRetriever;->release()V

    #@81
    .line 1840
    const/4 v7, 0x0

    #@82
    .line 1841
    if-nez v0, :cond_a3

    #@84
    .line 1842
    new-instance v10, Ljava/lang/StringBuilder;

    #@86
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@89
    const-string v11, "Thumbnail extraction from "

    #@8b
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v10

    #@8f
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v10

    #@93
    const-string v11, " failed"

    #@95
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v10

    #@99
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9c
    move-result-object v5

    #@9d
    .line 1844
    .local v5, msg:Ljava/lang/String;
    new-instance v10, Ljava/lang/IllegalArgumentException;

    #@9f
    invoke-direct {v10, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a2
    throw v10

    #@a3
    .line 1847
    .end local v5           #msg:Ljava/lang/String;
    :cond_a3
    const/4 v10, 0x1

    #@a4
    invoke-static {v0, v9, v3, v10}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    #@a7
    move-result-object v6

    #@a8
    .line 1863
    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    .end local v7           #retriever:Landroid/media/MediaMetadataRetriever;
    :goto_a8
    :try_start_a8
    new-instance v8, Ljava/io/FileOutputStream;

    #@aa
    new-instance v10, Ljava/lang/StringBuilder;

    #@ac
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@af
    iget-object v11, p0, Landroid/media/videoeditor/VideoEditorImpl;->mProjectPath:Ljava/lang/String;

    #@b1
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v10

    #@b5
    const-string v11, "/"

    #@b7
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v10

    #@bb
    const-string/jumbo v11, "thumbnail.jpg"

    #@be
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v10

    #@c2
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c5
    move-result-object v10

    #@c6
    invoke-direct {v8, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    #@c9
    .line 1865
    .local v8, stream:Ljava/io/FileOutputStream;
    sget-object v10, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    #@cb
    const/16 v11, 0x64

    #@cd
    invoke-virtual {v6, v10, v11, v8}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    #@d0
    .line 1866
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->flush()V

    #@d3
    .line 1867
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_d6
    .catchall {:try_start_a8 .. :try_end_d6} :catchall_113
    .catch Ljava/io/IOException; {:try_start_a8 .. :try_end_d6} :catch_10a

    #@d6
    .line 1871
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V

    #@d9
    .line 1874
    .end local v2           #filename:Ljava/lang/String;
    .end local v3           #height:I
    .end local v4           #mI:Landroid/media/videoeditor/MediaItem;
    .end local v6           #projectBitmap:Landroid/graphics/Bitmap;
    .end local v8           #stream:Ljava/io/FileOutputStream;
    .end local v9           #width:I
    :cond_d9
    return-void

    #@da
    .line 1851
    .restart local v2       #filename:Ljava/lang/String;
    .restart local v3       #height:I
    .restart local v4       #mI:Landroid/media/videoeditor/MediaItem;
    .restart local v6       #projectBitmap:Landroid/graphics/Bitmap;
    .restart local v9       #width:I
    :cond_da
    const-wide/16 v10, 0x1f4

    #@dc
    :try_start_dc
    invoke-virtual {v4, v9, v3, v10, v11}, Landroid/media/videoeditor/MediaItem;->getThumbnail(IIJ)Landroid/graphics/Bitmap;
    :try_end_df
    .catch Ljava/lang/IllegalArgumentException; {:try_start_dc .. :try_end_df} :catch_e1
    .catch Ljava/io/IOException; {:try_start_dc .. :try_end_df} :catch_101

    #@df
    move-result-object v6

    #@e0
    goto :goto_a8

    #@e1
    .line 1852
    :catch_e1
    move-exception v1

    #@e2
    .line 1853
    .local v1, e:Ljava/lang/IllegalArgumentException;
    new-instance v10, Ljava/lang/StringBuilder;

    #@e4
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@e7
    const-string v11, "Project thumbnail extraction from "

    #@e9
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ec
    move-result-object v10

    #@ed
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v10

    #@f1
    const-string v11, " failed"

    #@f3
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v10

    #@f7
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fa
    move-result-object v5

    #@fb
    .line 1855
    .restart local v5       #msg:Ljava/lang/String;
    new-instance v10, Ljava/lang/IllegalArgumentException;

    #@fd
    invoke-direct {v10, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@100
    throw v10

    #@101
    .line 1856
    .end local v1           #e:Ljava/lang/IllegalArgumentException;
    .end local v5           #msg:Ljava/lang/String;
    :catch_101
    move-exception v1

    #@102
    .line 1857
    .local v1, e:Ljava/io/IOException;
    const-string v5, "IO Error creating project thumbnail"

    #@104
    .line 1858
    .restart local v5       #msg:Ljava/lang/String;
    new-instance v10, Ljava/lang/IllegalArgumentException;

    #@106
    invoke-direct {v10, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@109
    throw v10

    #@10a
    .line 1868
    .end local v1           #e:Ljava/io/IOException;
    .end local v5           #msg:Ljava/lang/String;
    :catch_10a
    move-exception v1

    #@10b
    .line 1869
    .restart local v1       #e:Ljava/io/IOException;
    :try_start_10b
    new-instance v10, Ljava/lang/IllegalArgumentException;

    #@10d
    const-string v11, "Error creating project thumbnail"

    #@10f
    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@112
    throw v10
    :try_end_113
    .catchall {:try_start_10b .. :try_end_113} :catchall_113

    #@113
    .line 1871
    .end local v1           #e:Ljava/io/IOException;
    :catchall_113
    move-exception v10

    #@114
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V

    #@117
    throw v10
.end method

.method private load()V
    .registers 26
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 966
    new-instance v10, Ljava/io/File;

    #@2
    move-object/from16 v0, p0

    #@4
    iget-object v0, v0, Landroid/media/videoeditor/VideoEditorImpl;->mProjectPath:Ljava/lang/String;

    #@6
    move-object/from16 v22, v0

    #@8
    const-string/jumbo v23, "videoeditor.xml"

    #@b
    move-object/from16 v0, v22

    #@d
    move-object/from16 v1, v23

    #@f
    invoke-direct {v10, v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@12
    .line 970
    .local v10, file:Ljava/io/File;
    new-instance v12, Ljava/io/FileInputStream;

    #@14
    invoke-direct {v12, v10}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    #@17
    .line 972
    .local v12, fis:Ljava/io/FileInputStream;
    :try_start_17
    new-instance v14, Ljava/util/ArrayList;

    #@19
    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    #@1c
    .line 974
    .local v14, ignoredMediaItems:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@1f
    move-result-object v19

    #@20
    .line 975
    .local v19, parser:Lorg/xmlpull/v1/XmlPullParser;
    const-string v22, "UTF-8"

    #@22
    move-object/from16 v0, v19

    #@24
    move-object/from16 v1, v22

    #@26
    invoke-interface {v0, v12, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    #@29
    .line 976
    invoke-interface/range {v19 .. v19}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    #@2c
    move-result v8

    #@2d
    .line 978
    .local v8, eventType:I
    const/4 v5, 0x0

    #@2e
    .line 979
    .local v5, currentMediaItem:Landroid/media/videoeditor/MediaItem;
    const/4 v6, 0x0

    #@2f
    .line 980
    .local v6, currentOverlay:Landroid/media/videoeditor/Overlay;
    const/16 v20, 0x0

    #@31
    .line 981
    .local v20, regenerateProjectThumbnail:Z
    :goto_31
    const/16 v22, 0x1

    #@33
    move/from16 v0, v22

    #@35
    if-eq v8, v0, :cond_273

    #@37
    .line 982
    packed-switch v8, :pswitch_data_282

    #@3a
    .line 1098
    :cond_3a
    :goto_3a
    invoke-interface/range {v19 .. v19}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@3d
    move-result v8

    #@3e
    goto :goto_31

    #@3f
    .line 984
    :pswitch_3f
    invoke-interface/range {v19 .. v19}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@42
    move-result-object v18

    #@43
    .line 985
    .local v18, name:Ljava/lang/String;
    const-string/jumbo v22, "project"

    #@46
    move-object/from16 v0, v22

    #@48
    move-object/from16 v1, v18

    #@4a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4d
    move-result v22

    #@4e
    if-eqz v22, :cond_90

    #@50
    .line 986
    const-string v22, ""

    #@52
    const-string v23, "aspect_ratio"

    #@54
    move-object/from16 v0, v19

    #@56
    move-object/from16 v1, v22

    #@58
    move-object/from16 v2, v23

    #@5a
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@5d
    move-result-object v22

    #@5e
    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@61
    move-result v22

    #@62
    move/from16 v0, v22

    #@64
    move-object/from16 v1, p0

    #@66
    iput v0, v1, Landroid/media/videoeditor/VideoEditorImpl;->mAspectRatio:I

    #@68
    .line 989
    const-string v22, ""

    #@6a
    const-string/jumbo v23, "regeneratePCMFlag"

    #@6d
    move-object/from16 v0, v19

    #@6f
    move-object/from16 v1, v22

    #@71
    move-object/from16 v2, v23

    #@73
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@76
    move-result-object v22

    #@77
    invoke-static/range {v22 .. v22}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@7a
    move-result v16

    #@7b
    .line 992
    .local v16, mRegenPCM:Z
    move-object/from16 v0, p0

    #@7d
    iget-object v0, v0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@7f
    move-object/from16 v22, v0

    #@81
    move-object/from16 v0, v22

    #@83
    move/from16 v1, v16

    #@85
    invoke-virtual {v0, v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setAudioflag(Z)V
    :try_end_88
    .catchall {:try_start_17 .. :try_end_88} :catchall_89

    #@88
    goto :goto_3a

    #@89
    .line 1107
    .end local v5           #currentMediaItem:Landroid/media/videoeditor/MediaItem;
    .end local v6           #currentOverlay:Landroid/media/videoeditor/Overlay;
    .end local v8           #eventType:I
    .end local v14           #ignoredMediaItems:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v16           #mRegenPCM:Z
    .end local v18           #name:Ljava/lang/String;
    .end local v19           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v20           #regenerateProjectThumbnail:Z
    :catchall_89
    move-exception v22

    #@8a
    if-eqz v12, :cond_8f

    #@8c
    .line 1108
    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V

    #@8f
    :cond_8f
    throw v22

    #@90
    .line 993
    .restart local v5       #currentMediaItem:Landroid/media/videoeditor/MediaItem;
    .restart local v6       #currentOverlay:Landroid/media/videoeditor/Overlay;
    .restart local v8       #eventType:I
    .restart local v14       #ignoredMediaItems:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .restart local v18       #name:Ljava/lang/String;
    .restart local v19       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v20       #regenerateProjectThumbnail:Z
    :cond_90
    :try_start_90
    const-string/jumbo v22, "media_item"

    #@93
    move-object/from16 v0, v22

    #@95
    move-object/from16 v1, v18

    #@97
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9a
    move-result v22

    #@9b
    if-eqz v22, :cond_f7

    #@9d
    .line 994
    const-string v22, ""

    #@9f
    const-string v23, "id"

    #@a1
    move-object/from16 v0, v19

    #@a3
    move-object/from16 v1, v22

    #@a5
    move-object/from16 v2, v23

    #@a7
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_aa
    .catchall {:try_start_90 .. :try_end_aa} :catchall_89

    #@aa
    move-result-object v17

    #@ab
    .line 996
    .local v17, mediaItemId:Ljava/lang/String;
    :try_start_ab
    move-object/from16 v0, p0

    #@ad
    move-object/from16 v1, v19

    #@af
    invoke-direct {v0, v1}, Landroid/media/videoeditor/VideoEditorImpl;->parseMediaItem(Lorg/xmlpull/v1/XmlPullParser;)Landroid/media/videoeditor/MediaItem;

    #@b2
    move-result-object v5

    #@b3
    .line 997
    move-object/from16 v0, p0

    #@b5
    iget-object v0, v0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@b7
    move-object/from16 v22, v0

    #@b9
    move-object/from16 v0, v22

    #@bb
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_be
    .catchall {:try_start_ab .. :try_end_be} :catchall_89
    .catch Ljava/lang/Exception; {:try_start_ab .. :try_end_be} :catch_c0

    #@be
    goto/16 :goto_3a

    #@c0
    .line 998
    :catch_c0
    move-exception v9

    #@c1
    .line 999
    .local v9, ex:Ljava/lang/Exception;
    :try_start_c1
    const-string v22, "VideoEditorImpl"

    #@c3
    new-instance v23, Ljava/lang/StringBuilder;

    #@c5
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@c8
    const-string v24, "Cannot load media item: "

    #@ca
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v23

    #@ce
    move-object/from16 v0, v23

    #@d0
    move-object/from16 v1, v17

    #@d2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v23

    #@d6
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d9
    move-result-object v23

    #@da
    move-object/from16 v0, v22

    #@dc
    move-object/from16 v1, v23

    #@de
    invoke-static {v0, v1, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e1
    .line 1000
    const/4 v5, 0x0

    #@e2
    .line 1003
    move-object/from16 v0, p0

    #@e4
    iget-object v0, v0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@e6
    move-object/from16 v22, v0

    #@e8
    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    #@eb
    move-result v22

    #@ec
    if-nez v22, :cond_f0

    #@ee
    .line 1004
    const/16 v20, 0x1

    #@f0
    .line 1007
    :cond_f0
    move-object/from16 v0, v17

    #@f2
    invoke-interface {v14, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@f5
    goto/16 :goto_3a

    #@f7
    .line 1009
    .end local v9           #ex:Ljava/lang/Exception;
    .end local v17           #mediaItemId:Ljava/lang/String;
    :cond_f7
    const-string/jumbo v22, "transition"

    #@fa
    move-object/from16 v0, v22

    #@fc
    move-object/from16 v1, v18

    #@fe
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_101
    .catchall {:try_start_c1 .. :try_end_101} :catchall_89

    #@101
    move-result v22

    #@102
    if-eqz v22, :cond_12b

    #@104
    .line 1011
    :try_start_104
    move-object/from16 v0, p0

    #@106
    move-object/from16 v1, v19

    #@108
    invoke-direct {v0, v1, v14}, Landroid/media/videoeditor/VideoEditorImpl;->parseTransition(Lorg/xmlpull/v1/XmlPullParser;Ljava/util/List;)Landroid/media/videoeditor/Transition;

    #@10b
    move-result-object v21

    #@10c
    .line 1015
    .local v21, transition:Landroid/media/videoeditor/Transition;
    if-eqz v21, :cond_3a

    #@10e
    .line 1016
    move-object/from16 v0, p0

    #@110
    iget-object v0, v0, Landroid/media/videoeditor/VideoEditorImpl;->mTransitions:Ljava/util/List;

    #@112
    move-object/from16 v22, v0

    #@114
    move-object/from16 v0, v22

    #@116
    move-object/from16 v1, v21

    #@118
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_11b
    .catchall {:try_start_104 .. :try_end_11b} :catchall_89
    .catch Ljava/lang/Exception; {:try_start_104 .. :try_end_11b} :catch_11d

    #@11b
    goto/16 :goto_3a

    #@11d
    .line 1018
    .end local v21           #transition:Landroid/media/videoeditor/Transition;
    :catch_11d
    move-exception v9

    #@11e
    .line 1019
    .restart local v9       #ex:Ljava/lang/Exception;
    :try_start_11e
    const-string v22, "VideoEditorImpl"

    #@120
    const-string v23, "Cannot load transition"

    #@122
    move-object/from16 v0, v22

    #@124
    move-object/from16 v1, v23

    #@126
    invoke-static {v0, v1, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@129
    goto/16 :goto_3a

    #@12b
    .line 1021
    .end local v9           #ex:Ljava/lang/Exception;
    :cond_12b
    const-string/jumbo v22, "overlay"

    #@12e
    move-object/from16 v0, v22

    #@130
    move-object/from16 v1, v18

    #@132
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_135
    .catchall {:try_start_11e .. :try_end_135} :catchall_89

    #@135
    move-result v22

    #@136
    if-eqz v22, :cond_155

    #@138
    .line 1022
    if-eqz v5, :cond_3a

    #@13a
    .line 1024
    :try_start_13a
    move-object/from16 v0, p0

    #@13c
    move-object/from16 v1, v19

    #@13e
    invoke-direct {v0, v1, v5}, Landroid/media/videoeditor/VideoEditorImpl;->parseOverlay(Lorg/xmlpull/v1/XmlPullParser;Landroid/media/videoeditor/MediaItem;)Landroid/media/videoeditor/Overlay;

    #@141
    move-result-object v6

    #@142
    .line 1025
    invoke-virtual {v5, v6}, Landroid/media/videoeditor/MediaItem;->addOverlay(Landroid/media/videoeditor/Overlay;)V
    :try_end_145
    .catchall {:try_start_13a .. :try_end_145} :catchall_89
    .catch Ljava/lang/Exception; {:try_start_13a .. :try_end_145} :catch_147

    #@145
    goto/16 :goto_3a

    #@147
    .line 1026
    :catch_147
    move-exception v9

    #@148
    .line 1027
    .restart local v9       #ex:Ljava/lang/Exception;
    :try_start_148
    const-string v22, "VideoEditorImpl"

    #@14a
    const-string v23, "Cannot load overlay"

    #@14c
    move-object/from16 v0, v22

    #@14e
    move-object/from16 v1, v23

    #@150
    invoke-static {v0, v1, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@153
    goto/16 :goto_3a

    #@155
    .line 1030
    .end local v9           #ex:Ljava/lang/Exception;
    :cond_155
    const-string/jumbo v22, "overlay_user_attributes"

    #@158
    move-object/from16 v0, v22

    #@15a
    move-object/from16 v1, v18

    #@15c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15f
    move-result v22

    #@160
    if-eqz v22, :cond_181

    #@162
    .line 1031
    if-eqz v6, :cond_3a

    #@164
    .line 1032
    invoke-interface/range {v19 .. v19}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    #@167
    move-result v3

    #@168
    .line 1033
    .local v3, attributesCount:I
    const/4 v13, 0x0

    #@169
    .local v13, i:I
    :goto_169
    if-ge v13, v3, :cond_3a

    #@16b
    .line 1034
    move-object/from16 v0, v19

    #@16d
    invoke-interface {v0, v13}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    #@170
    move-result-object v22

    #@171
    move-object/from16 v0, v19

    #@173
    invoke-interface {v0, v13}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    #@176
    move-result-object v23

    #@177
    move-object/from16 v0, v22

    #@179
    move-object/from16 v1, v23

    #@17b
    invoke-virtual {v6, v0, v1}, Landroid/media/videoeditor/Overlay;->setUserAttribute(Ljava/lang/String;Ljava/lang/String;)V

    #@17e
    .line 1033
    add-int/lit8 v13, v13, 0x1

    #@180
    goto :goto_169

    #@181
    .line 1038
    .end local v3           #attributesCount:I
    .end local v13           #i:I
    :cond_181
    const-string v22, "effect"

    #@183
    move-object/from16 v0, v22

    #@185
    move-object/from16 v1, v18

    #@187
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_18a
    .catchall {:try_start_148 .. :try_end_18a} :catchall_89

    #@18a
    move-result v22

    #@18b
    if-eqz v22, :cond_226

    #@18d
    .line 1039
    if-eqz v5, :cond_3a

    #@18f
    .line 1041
    :try_start_18f
    move-object/from16 v0, p0

    #@191
    move-object/from16 v1, v19

    #@193
    invoke-direct {v0, v1, v5}, Landroid/media/videoeditor/VideoEditorImpl;->parseEffect(Lorg/xmlpull/v1/XmlPullParser;Landroid/media/videoeditor/MediaItem;)Landroid/media/videoeditor/Effect;

    #@196
    move-result-object v7

    #@197
    .line 1042
    .local v7, effect:Landroid/media/videoeditor/Effect;
    invoke-virtual {v5, v7}, Landroid/media/videoeditor/MediaItem;->addEffect(Landroid/media/videoeditor/Effect;)V

    #@19a
    .line 1044
    instance-of v0, v7, Landroid/media/videoeditor/EffectKenBurns;

    #@19c
    move/from16 v22, v0

    #@19e
    if-eqz v22, :cond_3a

    #@1a0
    .line 1045
    const-string v22, ""

    #@1a2
    const-string/jumbo v23, "is_image_clip_generated"

    #@1a5
    move-object/from16 v0, v19

    #@1a7
    move-object/from16 v1, v22

    #@1a9
    move-object/from16 v2, v23

    #@1ab
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1ae
    move-result-object v22

    #@1af
    invoke-static/range {v22 .. v22}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@1b2
    move-result v15

    #@1b3
    .line 1048
    .local v15, isImageClipGenerated:Z
    if-eqz v15, :cond_210

    #@1b5
    .line 1049
    const-string v22, ""

    #@1b7
    const-string v23, "generated_image_clip"

    #@1b9
    move-object/from16 v0, v19

    #@1bb
    move-object/from16 v1, v22

    #@1bd
    move-object/from16 v2, v23

    #@1bf
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1c2
    move-result-object v11

    #@1c3
    .line 1051
    .local v11, filename:Ljava/lang/String;
    new-instance v22, Ljava/io/File;

    #@1c5
    move-object/from16 v0, v22

    #@1c7
    invoke-direct {v0, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@1ca
    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->exists()Z

    #@1cd
    move-result v22

    #@1ce
    const/16 v23, 0x1

    #@1d0
    move/from16 v0, v22

    #@1d2
    move/from16 v1, v23

    #@1d4
    if-ne v0, v1, :cond_1fa

    #@1d6
    .line 1052
    move-object v0, v5

    #@1d7
    check-cast v0, Landroid/media/videoeditor/MediaImageItem;

    #@1d9
    move-object/from16 v22, v0

    #@1db
    move-object/from16 v0, v22

    #@1dd
    invoke-virtual {v0, v11}, Landroid/media/videoeditor/MediaImageItem;->setGeneratedImageClip(Ljava/lang/String;)V

    #@1e0
    .line 1054
    move-object v0, v5

    #@1e1
    check-cast v0, Landroid/media/videoeditor/MediaImageItem;

    #@1e3
    move-object/from16 v22, v0

    #@1e5
    const/16 v23, 0x0

    #@1e7
    invoke-virtual/range {v22 .. v23}, Landroid/media/videoeditor/MediaImageItem;->setRegenerateClip(Z)V
    :try_end_1ea
    .catchall {:try_start_18f .. :try_end_1ea} :catchall_89
    .catch Ljava/lang/Exception; {:try_start_18f .. :try_end_1ea} :catch_1ec

    #@1ea
    goto/16 :goto_3a

    #@1ec
    .line 1069
    .end local v7           #effect:Landroid/media/videoeditor/Effect;
    .end local v11           #filename:Ljava/lang/String;
    .end local v15           #isImageClipGenerated:Z
    :catch_1ec
    move-exception v9

    #@1ed
    .line 1070
    .restart local v9       #ex:Ljava/lang/Exception;
    :try_start_1ed
    const-string v22, "VideoEditorImpl"

    #@1ef
    const-string v23, "Cannot load effect"

    #@1f1
    move-object/from16 v0, v22

    #@1f3
    move-object/from16 v1, v23

    #@1f5
    invoke-static {v0, v1, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1f8
    .catchall {:try_start_1ed .. :try_end_1f8} :catchall_89

    #@1f8
    goto/16 :goto_3a

    #@1fa
    .line 1057
    .end local v9           #ex:Ljava/lang/Exception;
    .restart local v7       #effect:Landroid/media/videoeditor/Effect;
    .restart local v11       #filename:Ljava/lang/String;
    .restart local v15       #isImageClipGenerated:Z
    :cond_1fa
    :try_start_1fa
    move-object v0, v5

    #@1fb
    check-cast v0, Landroid/media/videoeditor/MediaImageItem;

    #@1fd
    move-object/from16 v22, v0

    #@1ff
    const/16 v23, 0x0

    #@201
    invoke-virtual/range {v22 .. v23}, Landroid/media/videoeditor/MediaImageItem;->setGeneratedImageClip(Ljava/lang/String;)V

    #@204
    .line 1059
    move-object v0, v5

    #@205
    check-cast v0, Landroid/media/videoeditor/MediaImageItem;

    #@207
    move-object/from16 v22, v0

    #@209
    const/16 v23, 0x1

    #@20b
    invoke-virtual/range {v22 .. v23}, Landroid/media/videoeditor/MediaImageItem;->setRegenerateClip(Z)V

    #@20e
    goto/16 :goto_3a

    #@210
    .line 1063
    .end local v11           #filename:Ljava/lang/String;
    :cond_210
    move-object v0, v5

    #@211
    check-cast v0, Landroid/media/videoeditor/MediaImageItem;

    #@213
    move-object/from16 v22, v0

    #@215
    const/16 v23, 0x0

    #@217
    invoke-virtual/range {v22 .. v23}, Landroid/media/videoeditor/MediaImageItem;->setGeneratedImageClip(Ljava/lang/String;)V

    #@21a
    .line 1065
    move-object v0, v5

    #@21b
    check-cast v0, Landroid/media/videoeditor/MediaImageItem;

    #@21d
    move-object/from16 v22, v0

    #@21f
    const/16 v23, 0x1

    #@221
    invoke-virtual/range {v22 .. v23}, Landroid/media/videoeditor/MediaImageItem;->setRegenerateClip(Z)V
    :try_end_224
    .catchall {:try_start_1fa .. :try_end_224} :catchall_89
    .catch Ljava/lang/Exception; {:try_start_1fa .. :try_end_224} :catch_1ec

    #@224
    goto/16 :goto_3a

    #@226
    .line 1073
    .end local v7           #effect:Landroid/media/videoeditor/Effect;
    .end local v15           #isImageClipGenerated:Z
    :cond_226
    :try_start_226
    const-string v22, "audio_track"

    #@228
    move-object/from16 v0, v22

    #@22a
    move-object/from16 v1, v18

    #@22c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_22f
    .catchall {:try_start_226 .. :try_end_22f} :catchall_89

    #@22f
    move-result v22

    #@230
    if-eqz v22, :cond_3a

    #@232
    .line 1075
    :try_start_232
    move-object/from16 v0, p0

    #@234
    move-object/from16 v1, v19

    #@236
    invoke-direct {v0, v1}, Landroid/media/videoeditor/VideoEditorImpl;->parseAudioTrack(Lorg/xmlpull/v1/XmlPullParser;)Landroid/media/videoeditor/AudioTrack;

    #@239
    move-result-object v4

    #@23a
    .line 1076
    .local v4, audioTrack:Landroid/media/videoeditor/AudioTrack;
    move-object/from16 v0, p0

    #@23c
    invoke-virtual {v0, v4}, Landroid/media/videoeditor/VideoEditorImpl;->addAudioTrack(Landroid/media/videoeditor/AudioTrack;)V
    :try_end_23f
    .catchall {:try_start_232 .. :try_end_23f} :catchall_89
    .catch Ljava/lang/Exception; {:try_start_232 .. :try_end_23f} :catch_241

    #@23f
    goto/16 :goto_3a

    #@241
    .line 1077
    .end local v4           #audioTrack:Landroid/media/videoeditor/AudioTrack;
    :catch_241
    move-exception v9

    #@242
    .line 1078
    .restart local v9       #ex:Ljava/lang/Exception;
    :try_start_242
    const-string v22, "VideoEditorImpl"

    #@244
    const-string v23, "Cannot load audio track"

    #@246
    move-object/from16 v0, v22

    #@248
    move-object/from16 v1, v23

    #@24a
    invoke-static {v0, v1, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@24d
    goto/16 :goto_3a

    #@24f
    .line 1085
    .end local v9           #ex:Ljava/lang/Exception;
    .end local v18           #name:Ljava/lang/String;
    :pswitch_24f
    invoke-interface/range {v19 .. v19}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@252
    move-result-object v18

    #@253
    .line 1086
    .restart local v18       #name:Ljava/lang/String;
    const-string/jumbo v22, "media_item"

    #@256
    move-object/from16 v0, v22

    #@258
    move-object/from16 v1, v18

    #@25a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25d
    move-result v22

    #@25e
    if-eqz v22, :cond_263

    #@260
    .line 1087
    const/4 v5, 0x0

    #@261
    goto/16 :goto_3a

    #@263
    .line 1088
    :cond_263
    const-string/jumbo v22, "overlay"

    #@266
    move-object/from16 v0, v22

    #@268
    move-object/from16 v1, v18

    #@26a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26d
    move-result v22

    #@26e
    if-eqz v22, :cond_3a

    #@270
    .line 1089
    const/4 v6, 0x0

    #@271
    goto/16 :goto_3a

    #@273
    .line 1100
    .end local v18           #name:Ljava/lang/String;
    :cond_273
    invoke-direct/range {p0 .. p0}, Landroid/media/videoeditor/VideoEditorImpl;->computeTimelineDuration()V

    #@276
    .line 1102
    if-eqz v20, :cond_27b

    #@278
    .line 1103
    invoke-direct/range {p0 .. p0}, Landroid/media/videoeditor/VideoEditorImpl;->generateProjectThumbnail()V
    :try_end_27b
    .catchall {:try_start_242 .. :try_end_27b} :catchall_89

    #@27b
    .line 1107
    :cond_27b
    if-eqz v12, :cond_280

    #@27d
    .line 1108
    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V

    #@280
    .line 1111
    :cond_280
    return-void

    #@281
    .line 982
    nop

    #@282
    :pswitch_data_282
    .packed-switch 0x2
        :pswitch_3f
        :pswitch_24f
    .end packed-switch
.end method

.method private lock()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x3

    #@1
    .line 1909
    const-string v0, "VideoEditorImpl"

    #@3
    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_16

    #@9
    .line 1910
    const-string v0, "VideoEditorImpl"

    #@b
    const-string/jumbo v1, "lock: grabbing semaphore"

    #@e
    new-instance v2, Ljava/lang/Throwable;

    #@10
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@13
    invoke-static {v0, v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16
    .line 1912
    :cond_16
    iget-object v0, p0, Landroid/media/videoeditor/VideoEditorImpl;->mLock:Ljava/util/concurrent/Semaphore;

    #@18
    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquire()V

    #@1b
    .line 1913
    const-string v0, "VideoEditorImpl"

    #@1d
    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@20
    move-result v0

    #@21
    if-eqz v0, :cond_2b

    #@23
    .line 1914
    const-string v0, "VideoEditorImpl"

    #@25
    const-string/jumbo v1, "lock: grabbed semaphore"

    #@28
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 1916
    :cond_2b
    return-void
.end method

.method private lock(J)Z
    .registers 8
    .parameter "timeoutMs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x3

    #@1
    .line 1927
    const-string v1, "VideoEditorImpl"

    #@3
    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_27

    #@9
    .line 1928
    const-string v1, "VideoEditorImpl"

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string/jumbo v3, "lock: grabbing semaphore with timeout "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    new-instance v3, Ljava/lang/Throwable;

    #@21
    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    #@24
    invoke-static {v1, v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@27
    .line 1931
    :cond_27
    iget-object v1, p0, Landroid/media/videoeditor/VideoEditorImpl;->mLock:Ljava/util/concurrent/Semaphore;

    #@29
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    #@2b
    invoke-virtual {v1, p1, p2, v2}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z

    #@2e
    move-result v0

    #@2f
    .line 1932
    .local v0, acquireSem:Z
    const-string v1, "VideoEditorImpl"

    #@31
    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@34
    move-result v1

    #@35
    if-eqz v1, :cond_50

    #@37
    .line 1933
    const-string v1, "VideoEditorImpl"

    #@39
    new-instance v2, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string/jumbo v3, "lock: grabbed semaphore status "

    #@41
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v2

    #@45
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v2

    #@4d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    .line 1936
    :cond_50
    return v0
.end method

.method private parseAudioTrack(Lorg/xmlpull/v1/XmlPullParser;)Landroid/media/videoeditor/AudioTrack;
    .registers 22
    .parameter "parser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 1340
    const-string v3, ""

    #@2
    const-string v19, "id"

    #@4
    move-object/from16 v0, p1

    #@6
    move-object/from16 v1, v19

    #@8
    invoke-interface {v0, v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v4

    #@c
    .line 1341
    .local v4, audioTrackId:Ljava/lang/String;
    const-string v3, ""

    #@e
    const-string v19, "filename"

    #@10
    move-object/from16 v0, p1

    #@12
    move-object/from16 v1, v19

    #@14
    invoke-interface {v0, v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@17
    move-result-object v5

    #@18
    .line 1342
    .local v5, filename:Ljava/lang/String;
    const-string v3, ""

    #@1a
    const-string/jumbo v19, "start_time"

    #@1d
    move-object/from16 v0, p1

    #@1f
    move-object/from16 v1, v19

    #@21
    invoke-interface {v0, v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@28
    move-result-wide v6

    #@29
    .line 1343
    .local v6, startTimeMs:J
    const-string v3, ""

    #@2b
    const-string v19, "begin_time"

    #@2d
    move-object/from16 v0, p1

    #@2f
    move-object/from16 v1, v19

    #@31
    invoke-interface {v0, v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@34
    move-result-object v3

    #@35
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@38
    move-result-wide v8

    #@39
    .line 1344
    .local v8, beginMs:J
    const-string v3, ""

    #@3b
    const-string v19, "end_time"

    #@3d
    move-object/from16 v0, p1

    #@3f
    move-object/from16 v1, v19

    #@41
    invoke-interface {v0, v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@44
    move-result-object v3

    #@45
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@48
    move-result-wide v10

    #@49
    .line 1345
    .local v10, endMs:J
    const-string v3, ""

    #@4b
    const-string/jumbo v19, "volume"

    #@4e
    move-object/from16 v0, p1

    #@50
    move-object/from16 v1, v19

    #@52
    invoke-interface {v0, v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@55
    move-result-object v3

    #@56
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@59
    move-result v13

    #@5a
    .line 1346
    .local v13, volume:I
    const-string v3, ""

    #@5c
    const-string/jumbo v19, "muted"

    #@5f
    move-object/from16 v0, p1

    #@61
    move-object/from16 v1, v19

    #@63
    invoke-interface {v0, v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@66
    move-result-object v3

    #@67
    invoke-static {v3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@6a
    move-result v14

    #@6b
    .line 1347
    .local v14, muted:Z
    const-string v3, ""

    #@6d
    const-string/jumbo v19, "loop"

    #@70
    move-object/from16 v0, p1

    #@72
    move-object/from16 v1, v19

    #@74
    invoke-interface {v0, v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@77
    move-result-object v3

    #@78
    invoke-static {v3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@7b
    move-result v12

    #@7c
    .line 1348
    .local v12, loop:Z
    const-string v3, ""

    #@7e
    const-string v19, "ducking_enabled"

    #@80
    move-object/from16 v0, p1

    #@82
    move-object/from16 v1, v19

    #@84
    invoke-interface {v0, v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@87
    move-result-object v3

    #@88
    invoke-static {v3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@8b
    move-result v15

    #@8c
    .line 1350
    .local v15, duckingEnabled:Z
    const-string v3, ""

    #@8e
    const-string v19, "ducking_threshold"

    #@90
    move-object/from16 v0, p1

    #@92
    move-object/from16 v1, v19

    #@94
    invoke-interface {v0, v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@97
    move-result-object v3

    #@98
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@9b
    move-result v16

    #@9c
    .line 1352
    .local v16, duckThreshold:I
    const-string v3, ""

    #@9e
    const-string v19, "ducking_volume"

    #@a0
    move-object/from16 v0, p1

    #@a2
    move-object/from16 v1, v19

    #@a4
    invoke-interface {v0, v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@a7
    move-result-object v3

    #@a8
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@ab
    move-result v17

    #@ac
    .line 1355
    .local v17, duckedTrackVolume:I
    const-string v3, ""

    #@ae
    const-string/jumbo v19, "waveform"

    #@b1
    move-object/from16 v0, p1

    #@b3
    move-object/from16 v1, v19

    #@b5
    invoke-interface {v0, v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@b8
    move-result-object v18

    #@b9
    .line 1356
    .local v18, waveformFilename:Ljava/lang/String;
    new-instance v2, Landroid/media/videoeditor/AudioTrack;

    #@bb
    move-object/from16 v3, p0

    #@bd
    invoke-direct/range {v2 .. v18}, Landroid/media/videoeditor/AudioTrack;-><init>(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;JJJZIZZIILjava/lang/String;)V

    #@c0
    .line 1365
    .local v2, audioTrack:Landroid/media/videoeditor/AudioTrack;
    return-object v2
.end method

.method private parseEffect(Lorg/xmlpull/v1/XmlPullParser;Landroid/media/videoeditor/MediaItem;)Landroid/media/videoeditor/Effect;
    .registers 23
    .parameter "parser"
    .parameter "mediaItem"

    #@0
    .prologue
    .line 1294
    const-string v2, ""

    #@2
    const-string v10, "id"

    #@4
    move-object/from16 v0, p1

    #@6
    invoke-interface {v0, v2, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v3

    #@a
    .line 1295
    .local v3, effectId:Ljava/lang/String;
    const-string v2, ""

    #@c
    const-string/jumbo v10, "type"

    #@f
    move-object/from16 v0, p1

    #@11
    invoke-interface {v0, v2, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@14
    move-result-object v19

    #@15
    .line 1296
    .local v19, type:Ljava/lang/String;
    const-string v2, ""

    #@17
    const-string v10, "duration"

    #@19
    move-object/from16 v0, p1

    #@1b
    invoke-interface {v0, v2, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@22
    move-result-wide v6

    #@23
    .line 1297
    .local v6, durationMs:J
    const-string v2, ""

    #@25
    const-string v10, "begin_time"

    #@27
    move-object/from16 v0, p1

    #@29
    invoke-interface {v0, v2, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@30
    move-result-wide v4

    #@31
    .line 1300
    .local v4, startTimeMs:J
    const-class v2, Landroid/media/videoeditor/EffectColor;

    #@33
    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    move-object/from16 v0, v19

    #@39
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3c
    move-result v2

    #@3d
    if-eqz v2, :cond_6b

    #@3f
    .line 1301
    const-string v2, ""

    #@41
    const-string v10, "color_type"

    #@43
    move-object/from16 v0, p1

    #@45
    invoke-interface {v0, v2, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@48
    move-result-object v2

    #@49
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@4c
    move-result v8

    #@4d
    .line 1304
    .local v8, colorEffectType:I
    const/4 v2, 0x1

    #@4e
    if-eq v8, v2, :cond_53

    #@50
    const/4 v2, 0x2

    #@51
    if-ne v8, v2, :cond_69

    #@53
    .line 1306
    :cond_53
    const-string v2, ""

    #@55
    const-string v10, "color_value"

    #@57
    move-object/from16 v0, p1

    #@59
    invoke-interface {v0, v2, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@5c
    move-result-object v2

    #@5d
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@60
    move-result v9

    #@61
    .line 1310
    .local v9, color:I
    :goto_61
    new-instance v1, Landroid/media/videoeditor/EffectColor;

    #@63
    move-object/from16 v2, p2

    #@65
    invoke-direct/range {v1 .. v9}, Landroid/media/videoeditor/EffectColor;-><init>(Landroid/media/videoeditor/MediaItem;Ljava/lang/String;JJII)V

    #@68
    .line 1329
    .end local v8           #colorEffectType:I
    .end local v9           #color:I
    .local v1, effect:Landroid/media/videoeditor/Effect;
    :goto_68
    return-object v1

    #@69
    .line 1308
    .end local v1           #effect:Landroid/media/videoeditor/Effect;
    .restart local v8       #colorEffectType:I
    :cond_69
    const/4 v9, 0x0

    #@6a
    .restart local v9       #color:I
    goto :goto_61

    #@6b
    .line 1312
    .end local v8           #colorEffectType:I
    .end local v9           #color:I
    :cond_6b
    const-class v2, Landroid/media/videoeditor/EffectKenBurns;

    #@6d
    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@70
    move-result-object v2

    #@71
    move-object/from16 v0, v19

    #@73
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@76
    move-result v2

    #@77
    if-eqz v2, :cond_105

    #@79
    .line 1313
    new-instance v13, Landroid/graphics/Rect;

    #@7b
    const-string v2, ""

    #@7d
    const-string/jumbo v10, "start_l"

    #@80
    move-object/from16 v0, p1

    #@82
    invoke-interface {v0, v2, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@85
    move-result-object v2

    #@86
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@89
    move-result v2

    #@8a
    const-string v10, ""

    #@8c
    const-string/jumbo v11, "start_t"

    #@8f
    move-object/from16 v0, p1

    #@91
    invoke-interface {v0, v10, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@94
    move-result-object v10

    #@95
    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@98
    move-result v10

    #@99
    const-string v11, ""

    #@9b
    const-string/jumbo v12, "start_r"

    #@9e
    move-object/from16 v0, p1

    #@a0
    invoke-interface {v0, v11, v12}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@a3
    move-result-object v11

    #@a4
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@a7
    move-result v11

    #@a8
    const-string v12, ""

    #@aa
    const-string/jumbo v15, "start_b"

    #@ad
    move-object/from16 v0, p1

    #@af
    invoke-interface {v0, v12, v15}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@b2
    move-result-object v12

    #@b3
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@b6
    move-result v12

    #@b7
    invoke-direct {v13, v2, v10, v11, v12}, Landroid/graphics/Rect;-><init>(IIII)V

    #@ba
    .line 1318
    .local v13, startRect:Landroid/graphics/Rect;
    new-instance v14, Landroid/graphics/Rect;

    #@bc
    const-string v2, ""

    #@be
    const-string v10, "end_l"

    #@c0
    move-object/from16 v0, p1

    #@c2
    invoke-interface {v0, v2, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@c5
    move-result-object v2

    #@c6
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@c9
    move-result v2

    #@ca
    const-string v10, ""

    #@cc
    const-string v11, "end_t"

    #@ce
    move-object/from16 v0, p1

    #@d0
    invoke-interface {v0, v10, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@d3
    move-result-object v10

    #@d4
    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@d7
    move-result v10

    #@d8
    const-string v11, ""

    #@da
    const-string v12, "end_r"

    #@dc
    move-object/from16 v0, p1

    #@de
    invoke-interface {v0, v11, v12}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@e1
    move-result-object v11

    #@e2
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@e5
    move-result v11

    #@e6
    const-string v12, ""

    #@e8
    const-string v15, "end_b"

    #@ea
    move-object/from16 v0, p1

    #@ec
    invoke-interface {v0, v12, v15}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@ef
    move-result-object v12

    #@f0
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@f3
    move-result v12

    #@f4
    invoke-direct {v14, v2, v10, v11, v12}, Landroid/graphics/Rect;-><init>(IIII)V

    #@f7
    .line 1323
    .local v14, endRect:Landroid/graphics/Rect;
    new-instance v1, Landroid/media/videoeditor/EffectKenBurns;

    #@f9
    move-object v10, v1

    #@fa
    move-object/from16 v11, p2

    #@fc
    move-object v12, v3

    #@fd
    move-wide v15, v4

    #@fe
    move-wide/from16 v17, v6

    #@100
    invoke-direct/range {v10 .. v18}, Landroid/media/videoeditor/EffectKenBurns;-><init>(Landroid/media/videoeditor/MediaItem;Ljava/lang/String;Landroid/graphics/Rect;Landroid/graphics/Rect;JJ)V

    #@103
    .line 1325
    .restart local v1       #effect:Landroid/media/videoeditor/Effect;
    goto/16 :goto_68

    #@105
    .line 1326
    .end local v1           #effect:Landroid/media/videoeditor/Effect;
    .end local v13           #startRect:Landroid/graphics/Rect;
    .end local v14           #endRect:Landroid/graphics/Rect;
    :cond_105
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@107
    new-instance v10, Ljava/lang/StringBuilder;

    #@109
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@10c
    const-string v11, "Invalid effect type: "

    #@10e
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@111
    move-result-object v10

    #@112
    move-object/from16 v0, v19

    #@114
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@117
    move-result-object v10

    #@118
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11b
    move-result-object v10

    #@11c
    invoke-direct {v2, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@11f
    throw v2
.end method

.method private parseMediaItem(Lorg/xmlpull/v1/XmlPullParser;)Landroid/media/videoeditor/MediaItem;
    .registers 31
    .parameter "parser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 1120
    const-string v5, ""

    #@2
    const-string v11, "id"

    #@4
    move-object/from16 v0, p1

    #@6
    invoke-interface {v0, v5, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v6

    #@a
    .line 1121
    .local v6, mediaItemId:Ljava/lang/String;
    const-string v5, ""

    #@c
    const-string/jumbo v11, "type"

    #@f
    move-object/from16 v0, p1

    #@11
    invoke-interface {v0, v5, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@14
    move-result-object v27

    #@15
    .line 1122
    .local v27, type:Ljava/lang/String;
    const-string v5, ""

    #@17
    const-string v11, "filename"

    #@19
    move-object/from16 v0, p1

    #@1b
    invoke-interface {v0, v5, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1e
    move-result-object v7

    #@1f
    .line 1123
    .local v7, filename:Ljava/lang/String;
    const-string v5, ""

    #@21
    const-string/jumbo v11, "rendering_mode"

    #@24
    move-object/from16 v0, p1

    #@26
    invoke-interface {v0, v5, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@29
    move-result-object v5

    #@2a
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@2d
    move-result v10

    #@2e
    .line 1127
    .local v10, renderingMode:I
    const-class v5, Landroid/media/videoeditor/MediaImageItem;

    #@30
    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@33
    move-result-object v5

    #@34
    move-object/from16 v0, v27

    #@36
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39
    move-result v5

    #@3a
    if-eqz v5, :cond_52

    #@3c
    .line 1128
    const-string v5, ""

    #@3e
    const-string v11, "duration"

    #@40
    move-object/from16 v0, p1

    #@42
    invoke-interface {v0, v5, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@45
    move-result-object v5

    #@46
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@49
    move-result-wide v8

    #@4a
    .line 1129
    .local v8, durationMs:J
    new-instance v4, Landroid/media/videoeditor/MediaImageItem;

    #@4c
    move-object/from16 v5, p0

    #@4e
    invoke-direct/range {v4 .. v10}, Landroid/media/videoeditor/MediaImageItem;-><init>(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;JI)V

    #@51
    .line 1151
    .end local v8           #durationMs:J
    .local v4, currentMediaItem:Landroid/media/videoeditor/MediaItem;
    :goto_51
    return-object v4

    #@52
    .line 1131
    .end local v4           #currentMediaItem:Landroid/media/videoeditor/MediaItem;
    :cond_52
    const-class v5, Landroid/media/videoeditor/MediaVideoItem;

    #@54
    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@57
    move-result-object v5

    #@58
    move-object/from16 v0, v27

    #@5a
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5d
    move-result v5

    #@5e
    if-eqz v5, :cond_ef

    #@60
    .line 1132
    const-string v5, ""

    #@62
    const-string v11, "begin_time"

    #@64
    move-object/from16 v0, p1

    #@66
    invoke-interface {v0, v5, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@69
    move-result-object v5

    #@6a
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@6d
    move-result-wide v16

    #@6e
    .line 1133
    .local v16, beginMs:J
    const-string v5, ""

    #@70
    const-string v11, "end_time"

    #@72
    move-object/from16 v0, p1

    #@74
    invoke-interface {v0, v5, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@77
    move-result-object v5

    #@78
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@7b
    move-result-wide v18

    #@7c
    .line 1134
    .local v18, endMs:J
    const-string v5, ""

    #@7e
    const-string/jumbo v11, "volume"

    #@81
    move-object/from16 v0, p1

    #@83
    invoke-interface {v0, v5, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@86
    move-result-object v5

    #@87
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@8a
    move-result v20

    #@8b
    .line 1135
    .local v20, volume:I
    const-string v5, ""

    #@8d
    const-string/jumbo v11, "muted"

    #@90
    move-object/from16 v0, p1

    #@92
    invoke-interface {v0, v5, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@95
    move-result-object v5

    #@96
    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@99
    move-result v21

    #@9a
    .line 1136
    .local v21, muted:Z
    const-string v5, ""

    #@9c
    const-string/jumbo v11, "waveform"

    #@9f
    move-object/from16 v0, p1

    #@a1
    invoke-interface {v0, v5, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@a4
    move-result-object v22

    #@a5
    .line 1138
    .local v22, audioWaveformFilename:Ljava/lang/String;
    new-instance v4, Landroid/media/videoeditor/MediaVideoItem;

    #@a7
    move-object v11, v4

    #@a8
    move-object/from16 v12, p0

    #@aa
    move-object v13, v6

    #@ab
    move-object v14, v7

    #@ac
    move v15, v10

    #@ad
    invoke-direct/range {v11 .. v22}, Landroid/media/videoeditor/MediaVideoItem;-><init>(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;IJJIZLjava/lang/String;)V

    #@b0
    .line 1141
    .restart local v4       #currentMediaItem:Landroid/media/videoeditor/MediaItem;
    const-string v5, ""

    #@b2
    const-string v11, "begin_time"

    #@b4
    move-object/from16 v0, p1

    #@b6
    invoke-interface {v0, v5, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@b9
    move-result-object v5

    #@ba
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@bd
    move-result-wide v23

    #@be
    .line 1142
    .local v23, beginTimeMs:J
    const-string v5, ""

    #@c0
    const-string v11, "end_time"

    #@c2
    move-object/from16 v0, p1

    #@c4
    invoke-interface {v0, v5, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@c7
    move-result-object v5

    #@c8
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@cb
    move-result-wide v25

    #@cc
    .local v25, endTimeMs:J
    move-object v5, v4

    #@cd
    .line 1143
    check-cast v5, Landroid/media/videoeditor/MediaVideoItem;

    #@cf
    move-wide/from16 v0, v23

    #@d1
    move-wide/from16 v2, v25

    #@d3
    invoke-virtual {v5, v0, v1, v2, v3}, Landroid/media/videoeditor/MediaVideoItem;->setExtractBoundaries(JJ)V

    #@d6
    .line 1145
    const-string v5, ""

    #@d8
    const-string/jumbo v11, "volume"

    #@db
    move-object/from16 v0, p1

    #@dd
    invoke-interface {v0, v5, v11}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@e0
    move-result-object v5

    #@e1
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@e4
    move-result v28

    #@e5
    .local v28, volumePercent:I
    move-object v5, v4

    #@e6
    .line 1146
    check-cast v5, Landroid/media/videoeditor/MediaVideoItem;

    #@e8
    move/from16 v0, v28

    #@ea
    invoke-virtual {v5, v0}, Landroid/media/videoeditor/MediaVideoItem;->setVolume(I)V

    #@ed
    goto/16 :goto_51

    #@ef
    .line 1148
    .end local v4           #currentMediaItem:Landroid/media/videoeditor/MediaItem;
    .end local v16           #beginMs:J
    .end local v18           #endMs:J
    .end local v20           #volume:I
    .end local v21           #muted:Z
    .end local v22           #audioWaveformFilename:Ljava/lang/String;
    .end local v23           #beginTimeMs:J
    .end local v25           #endTimeMs:J
    .end local v28           #volumePercent:I
    :cond_ef
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@f1
    new-instance v11, Ljava/lang/StringBuilder;

    #@f3
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@f6
    const-string v12, "Unknown media item type: "

    #@f8
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v11

    #@fc
    move-object/from16 v0, v27

    #@fe
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@101
    move-result-object v11

    #@102
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@105
    move-result-object v11

    #@106
    invoke-direct {v5, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@109
    throw v5
.end method

.method private parseOverlay(Lorg/xmlpull/v1/XmlPullParser;Landroid/media/videoeditor/MediaItem;)Landroid/media/videoeditor/Overlay;
    .registers 20
    .parameter "parser"
    .parameter "mediaItem"

    #@0
    .prologue
    .line 1249
    const-string v2, ""

    #@2
    const-string v15, "id"

    #@4
    move-object/from16 v0, p1

    #@6
    invoke-interface {v0, v2, v15}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v3

    #@a
    .line 1250
    .local v3, overlayId:Ljava/lang/String;
    const-string v2, ""

    #@c
    const-string/jumbo v15, "type"

    #@f
    move-object/from16 v0, p1

    #@11
    invoke-interface {v0, v2, v15}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@14
    move-result-object v14

    #@15
    .line 1251
    .local v14, type:Ljava/lang/String;
    const-string v2, ""

    #@17
    const-string v15, "duration"

    #@19
    move-object/from16 v0, p1

    #@1b
    invoke-interface {v0, v2, v15}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@22
    move-result-wide v7

    #@23
    .line 1252
    .local v7, durationMs:J
    const-string v2, ""

    #@25
    const-string v15, "begin_time"

    #@27
    move-object/from16 v0, p1

    #@29
    invoke-interface {v0, v2, v15}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@30
    move-result-wide v5

    #@31
    .line 1255
    .local v5, startTimeMs:J
    const-class v2, Landroid/media/videoeditor/OverlayFrame;

    #@33
    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3a
    move-result v2

    #@3b
    if-eqz v2, :cond_b0

    #@3d
    .line 1256
    const-string v2, ""

    #@3f
    const-string v15, "filename"

    #@41
    move-object/from16 v0, p1

    #@43
    invoke-interface {v0, v2, v15}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@46
    move-result-object v4

    #@47
    .line 1257
    .local v4, filename:Ljava/lang/String;
    new-instance v1, Landroid/media/videoeditor/OverlayFrame;

    #@49
    move-object/from16 v2, p2

    #@4b
    invoke-direct/range {v1 .. v8}, Landroid/media/videoeditor/OverlayFrame;-><init>(Landroid/media/videoeditor/MediaItem;Ljava/lang/String;Ljava/lang/String;JJ)V

    #@4e
    .line 1262
    .local v1, overlay:Landroid/media/videoeditor/Overlay;
    const-string v2, ""

    #@50
    const-string/jumbo v15, "overlay_rgb_filename"

    #@53
    move-object/from16 v0, p1

    #@55
    invoke-interface {v0, v2, v15}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@58
    move-result-object v11

    #@59
    .line 1263
    .local v11, overlayRgbFileName:Ljava/lang/String;
    if-eqz v11, :cond_af

    #@5b
    move-object v2, v1

    #@5c
    .line 1264
    check-cast v2, Landroid/media/videoeditor/OverlayFrame;

    #@5e
    invoke-virtual {v2, v11}, Landroid/media/videoeditor/OverlayFrame;->setFilename(Ljava/lang/String;)V

    #@61
    .line 1266
    const-string v2, ""

    #@63
    const-string/jumbo v15, "overlay_frame_width"

    #@66
    move-object/from16 v0, p1

    #@68
    invoke-interface {v0, v2, v15}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@6b
    move-result-object v2

    #@6c
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@6f
    move-result v10

    #@70
    .line 1268
    .local v10, overlayFrameWidth:I
    const-string v2, ""

    #@72
    const-string/jumbo v15, "overlay_frame_height"

    #@75
    move-object/from16 v0, p1

    #@77
    invoke-interface {v0, v2, v15}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7a
    move-result-object v2

    #@7b
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@7e
    move-result v9

    #@7f
    .local v9, overlayFrameHeight:I
    move-object v2, v1

    #@80
    .line 1271
    check-cast v2, Landroid/media/videoeditor/OverlayFrame;

    #@82
    invoke-virtual {v2, v10}, Landroid/media/videoeditor/OverlayFrame;->setOverlayFrameWidth(I)V

    #@85
    move-object v2, v1

    #@86
    .line 1272
    check-cast v2, Landroid/media/videoeditor/OverlayFrame;

    #@88
    invoke-virtual {v2, v9}, Landroid/media/videoeditor/OverlayFrame;->setOverlayFrameHeight(I)V

    #@8b
    .line 1274
    const-string v2, ""

    #@8d
    const-string/jumbo v15, "resized_RGBframe_width"

    #@90
    move-object/from16 v0, p1

    #@92
    invoke-interface {v0, v2, v15}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@95
    move-result-object v2

    #@96
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@99
    move-result v13

    #@9a
    .line 1276
    .local v13, resizedRGBFrameWidth:I
    const-string v2, ""

    #@9c
    const-string/jumbo v15, "resized_RGBframe_height"

    #@9f
    move-object/from16 v0, p1

    #@a1
    invoke-interface {v0, v2, v15}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@a4
    move-result-object v2

    #@a5
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@a8
    move-result v12

    #@a9
    .local v12, resizedRGBFrameHeight:I
    move-object v2, v1

    #@aa
    .line 1279
    check-cast v2, Landroid/media/videoeditor/OverlayFrame;

    #@ac
    invoke-virtual {v2, v13, v12}, Landroid/media/videoeditor/OverlayFrame;->setResizedRGBSize(II)V

    #@af
    .line 1282
    .end local v9           #overlayFrameHeight:I
    .end local v10           #overlayFrameWidth:I
    .end local v12           #resizedRGBFrameHeight:I
    .end local v13           #resizedRGBFrameWidth:I
    :cond_af
    return-object v1

    #@b0
    .line 1259
    .end local v1           #overlay:Landroid/media/videoeditor/Overlay;
    .end local v4           #filename:Ljava/lang/String;
    .end local v11           #overlayRgbFileName:Ljava/lang/String;
    :cond_b0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@b2
    new-instance v15, Ljava/lang/StringBuilder;

    #@b4
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@b7
    const-string v16, "Invalid overlay type: "

    #@b9
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v15

    #@bd
    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v15

    #@c1
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c4
    move-result-object v15

    #@c5
    invoke-direct {v2, v15}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@c8
    throw v2
.end method

.method private parseTransition(Lorg/xmlpull/v1/XmlPullParser;Ljava/util/List;)Landroid/media/videoeditor/Transition;
    .registers 28
    .parameter "parser"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/xmlpull/v1/XmlPullParser;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/media/videoeditor/Transition;"
        }
    .end annotation

    #@0
    .prologue
    .line 1163
    .local p2, ignoredMediaItems:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const-string v12, ""

    #@2
    const-string v13, "id"

    #@4
    move-object/from16 v0, p1

    #@6
    invoke-interface {v0, v12, v13}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v3

    #@a
    .line 1164
    .local v3, transitionId:Ljava/lang/String;
    const-string v12, ""

    #@c
    const-string/jumbo v13, "type"

    #@f
    move-object/from16 v0, p1

    #@11
    invoke-interface {v0, v12, v13}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@14
    move-result-object v24

    #@15
    .line 1165
    .local v24, type:Ljava/lang/String;
    const-string v12, ""

    #@17
    const-string v13, "duration"

    #@19
    move-object/from16 v0, p1

    #@1b
    invoke-interface {v0, v12, v13}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1e
    move-result-object v12

    #@1f
    invoke-static {v12}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@22
    move-result-wide v6

    #@23
    .line 1166
    .local v6, durationMs:J
    const-string v12, ""

    #@25
    const-string v13, "behavior"

    #@27
    move-object/from16 v0, p1

    #@29
    invoke-interface {v0, v12, v13}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object v12

    #@2d
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@30
    move-result v8

    #@31
    .line 1168
    .local v8, behavior:I
    const-string v12, ""

    #@33
    const-string v13, "before_media_item"

    #@35
    move-object/from16 v0, p1

    #@37
    invoke-interface {v0, v12, v13}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3a
    move-result-object v21

    #@3b
    .line 1170
    .local v21, beforeMediaItemId:Ljava/lang/String;
    if-eqz v21, :cond_69

    #@3d
    .line 1171
    move-object/from16 v0, p2

    #@3f
    move-object/from16 v1, v21

    #@41
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@44
    move-result v12

    #@45
    if-eqz v12, :cond_49

    #@47
    .line 1173
    const/4 v2, 0x0

    #@48
    .line 1237
    :cond_48
    :goto_48
    return-object v2

    #@49
    .line 1176
    :cond_49
    move-object/from16 v0, p0

    #@4b
    move-object/from16 v1, v21

    #@4d
    invoke-virtual {v0, v1}, Landroid/media/videoeditor/VideoEditorImpl;->getMediaItem(Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;

    #@50
    move-result-object v5

    #@51
    .line 1181
    .local v5, beforeMediaItem:Landroid/media/videoeditor/MediaItem;
    :goto_51
    const-string v12, ""

    #@53
    const-string v13, "after_media_item"

    #@55
    move-object/from16 v0, p1

    #@57
    invoke-interface {v0, v12, v13}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@5a
    move-result-object v20

    #@5b
    .line 1183
    .local v20, afterMediaItemId:Ljava/lang/String;
    if-eqz v20, :cond_e9

    #@5d
    .line 1184
    move-object/from16 v0, p2

    #@5f
    move-object/from16 v1, v20

    #@61
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@64
    move-result v12

    #@65
    if-eqz v12, :cond_6b

    #@67
    .line 1186
    const/4 v2, 0x0

    #@68
    goto :goto_48

    #@69
    .line 1178
    .end local v5           #beforeMediaItem:Landroid/media/videoeditor/MediaItem;
    .end local v20           #afterMediaItemId:Ljava/lang/String;
    :cond_69
    const/4 v5, 0x0

    #@6a
    .restart local v5       #beforeMediaItem:Landroid/media/videoeditor/MediaItem;
    goto :goto_51

    #@6b
    .line 1189
    .restart local v20       #afterMediaItemId:Ljava/lang/String;
    :cond_6b
    move-object/from16 v0, p0

    #@6d
    move-object/from16 v1, v20

    #@6f
    invoke-virtual {v0, v1}, Landroid/media/videoeditor/VideoEditorImpl;->getMediaItem(Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;

    #@72
    move-result-object v4

    #@73
    .line 1195
    .local v4, afterMediaItem:Landroid/media/videoeditor/MediaItem;
    :goto_73
    const-class v12, Landroid/media/videoeditor/TransitionAlpha;

    #@75
    invoke-virtual {v12}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@78
    move-result-object v12

    #@79
    move-object/from16 v0, v24

    #@7b
    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7e
    move-result v12

    #@7f
    if-eqz v12, :cond_eb

    #@81
    .line 1196
    const-string v12, ""

    #@83
    const-string v13, "blending"

    #@85
    move-object/from16 v0, p1

    #@87
    invoke-interface {v0, v12, v13}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@8a
    move-result-object v12

    #@8b
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@8e
    move-result v10

    #@8f
    .line 1197
    .local v10, blending:I
    const-string v12, ""

    #@91
    const-string/jumbo v13, "mask"

    #@94
    move-object/from16 v0, p1

    #@96
    invoke-interface {v0, v12, v13}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@99
    move-result-object v9

    #@9a
    .line 1198
    .local v9, maskFilename:Ljava/lang/String;
    const-string v12, ""

    #@9c
    const-string v13, "invert"

    #@9e
    move-object/from16 v0, p1

    #@a0
    invoke-interface {v0, v12, v13}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@a3
    move-result-object v12

    #@a4
    invoke-static {v12}, Ljava/lang/Boolean;->getBoolean(Ljava/lang/String;)Z

    #@a7
    move-result v11

    #@a8
    .line 1199
    .local v11, invert:Z
    new-instance v2, Landroid/media/videoeditor/TransitionAlpha;

    #@aa
    invoke-direct/range {v2 .. v11}, Landroid/media/videoeditor/TransitionAlpha;-><init>(Ljava/lang/String;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;JILjava/lang/String;IZ)V

    #@ad
    .line 1215
    .end local v9           #maskFilename:Ljava/lang/String;
    .end local v10           #blending:I
    .end local v11           #invert:Z
    .local v2, transition:Landroid/media/videoeditor/Transition;
    :goto_ad
    const-string v12, ""

    #@af
    const-string/jumbo v13, "is_transition_generated"

    #@b2
    move-object/from16 v0, p1

    #@b4
    invoke-interface {v0, v12, v13}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@b7
    move-result-object v12

    #@b8
    invoke-static {v12}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@bb
    move-result v22

    #@bc
    .line 1217
    .local v22, isTransitionGenerated:Z
    const/4 v12, 0x1

    #@bd
    move/from16 v0, v22

    #@bf
    if-ne v0, v12, :cond_dd

    #@c1
    .line 1218
    const-string v12, ""

    #@c3
    const-string v13, "generated_transition_clip"

    #@c5
    move-object/from16 v0, p1

    #@c7
    invoke-interface {v0, v12, v13}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@ca
    move-result-object v23

    #@cb
    .line 1221
    .local v23, transitionFile:Ljava/lang/String;
    new-instance v12, Ljava/io/File;

    #@cd
    move-object/from16 v0, v23

    #@cf
    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@d2
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    #@d5
    move-result v12

    #@d6
    if-eqz v12, :cond_159

    #@d8
    .line 1222
    move-object/from16 v0, v23

    #@da
    invoke-virtual {v2, v0}, Landroid/media/videoeditor/Transition;->setFilename(Ljava/lang/String;)V

    #@dd
    .line 1229
    .end local v23           #transitionFile:Ljava/lang/String;
    :cond_dd
    :goto_dd
    if-eqz v5, :cond_e2

    #@df
    .line 1230
    invoke-virtual {v5, v2}, Landroid/media/videoeditor/MediaItem;->setBeginTransition(Landroid/media/videoeditor/Transition;)V

    #@e2
    .line 1233
    :cond_e2
    if-eqz v4, :cond_48

    #@e4
    .line 1234
    invoke-virtual {v4, v2}, Landroid/media/videoeditor/MediaItem;->setEndTransition(Landroid/media/videoeditor/Transition;)V

    #@e7
    goto/16 :goto_48

    #@e9
    .line 1191
    .end local v2           #transition:Landroid/media/videoeditor/Transition;
    .end local v4           #afterMediaItem:Landroid/media/videoeditor/MediaItem;
    .end local v22           #isTransitionGenerated:Z
    :cond_e9
    const/4 v4, 0x0

    #@ea
    .restart local v4       #afterMediaItem:Landroid/media/videoeditor/MediaItem;
    goto :goto_73

    #@eb
    .line 1201
    :cond_eb
    const-class v12, Landroid/media/videoeditor/TransitionCrossfade;

    #@ed
    invoke-virtual {v12}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@f0
    move-result-object v12

    #@f1
    move-object/from16 v0, v24

    #@f3
    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f6
    move-result v12

    #@f7
    if-eqz v12, :cond_ff

    #@f9
    .line 1202
    new-instance v2, Landroid/media/videoeditor/TransitionCrossfade;

    #@fb
    invoke-direct/range {v2 .. v8}, Landroid/media/videoeditor/TransitionCrossfade;-><init>(Ljava/lang/String;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;JI)V

    #@fe
    .restart local v2       #transition:Landroid/media/videoeditor/Transition;
    goto :goto_ad

    #@ff
    .line 1204
    .end local v2           #transition:Landroid/media/videoeditor/Transition;
    :cond_ff
    const-class v12, Landroid/media/videoeditor/TransitionSliding;

    #@101
    invoke-virtual {v12}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@104
    move-result-object v12

    #@105
    move-object/from16 v0, v24

    #@107
    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10a
    move-result v12

    #@10b
    if-eqz v12, :cond_129

    #@10d
    .line 1205
    const-string v12, ""

    #@10f
    const-string v13, "direction"

    #@111
    move-object/from16 v0, p1

    #@113
    invoke-interface {v0, v12, v13}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@116
    move-result-object v12

    #@117
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@11a
    move-result v19

    #@11b
    .line 1206
    .local v19, direction:I
    new-instance v2, Landroid/media/videoeditor/TransitionSliding;

    #@11d
    move-object v12, v2

    #@11e
    move-object v13, v3

    #@11f
    move-object v14, v4

    #@120
    move-object v15, v5

    #@121
    move-wide/from16 v16, v6

    #@123
    move/from16 v18, v8

    #@125
    invoke-direct/range {v12 .. v19}, Landroid/media/videoeditor/TransitionSliding;-><init>(Ljava/lang/String;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;JII)V

    #@128
    .line 1208
    .restart local v2       #transition:Landroid/media/videoeditor/Transition;
    goto :goto_ad

    #@129
    .end local v2           #transition:Landroid/media/videoeditor/Transition;
    .end local v19           #direction:I
    :cond_129
    const-class v12, Landroid/media/videoeditor/TransitionFadeBlack;

    #@12b
    invoke-virtual {v12}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@12e
    move-result-object v12

    #@12f
    move-object/from16 v0, v24

    #@131
    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@134
    move-result v12

    #@135
    if-eqz v12, :cond_13e

    #@137
    .line 1209
    new-instance v2, Landroid/media/videoeditor/TransitionFadeBlack;

    #@139
    invoke-direct/range {v2 .. v8}, Landroid/media/videoeditor/TransitionFadeBlack;-><init>(Ljava/lang/String;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;JI)V

    #@13c
    .restart local v2       #transition:Landroid/media/videoeditor/Transition;
    goto/16 :goto_ad

    #@13e
    .line 1212
    .end local v2           #transition:Landroid/media/videoeditor/Transition;
    :cond_13e
    new-instance v12, Ljava/lang/IllegalArgumentException;

    #@140
    new-instance v13, Ljava/lang/StringBuilder;

    #@142
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@145
    const-string v14, "Invalid transition type: "

    #@147
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14a
    move-result-object v13

    #@14b
    move-object/from16 v0, v24

    #@14d
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@150
    move-result-object v13

    #@151
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@154
    move-result-object v13

    #@155
    invoke-direct {v12, v13}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@158
    throw v12

    #@159
    .line 1224
    .restart local v2       #transition:Landroid/media/videoeditor/Transition;
    .restart local v22       #isTransitionGenerated:Z
    .restart local v23       #transitionFile:Ljava/lang/String;
    :cond_159
    const/4 v12, 0x0

    #@15a
    invoke-virtual {v2, v12}, Landroid/media/videoeditor/Transition;->setFilename(Ljava/lang/String;)V

    #@15d
    goto :goto_dd
.end method

.method private removeAdjacentTransitions(Landroid/media/videoeditor/MediaItem;)V
    .registers 6
    .parameter "mediaItem"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1724
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getBeginTransition()Landroid/media/videoeditor/Transition;

    #@4
    move-result-object v0

    #@5
    .line 1725
    .local v0, beginTransition:Landroid/media/videoeditor/Transition;
    if-eqz v0, :cond_1c

    #@7
    .line 1726
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->getAfterMediaItem()Landroid/media/videoeditor/MediaItem;

    #@a
    move-result-object v2

    #@b
    if-eqz v2, :cond_14

    #@d
    .line 1727
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->getAfterMediaItem()Landroid/media/videoeditor/MediaItem;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, v3}, Landroid/media/videoeditor/MediaItem;->setEndTransition(Landroid/media/videoeditor/Transition;)V

    #@14
    .line 1729
    :cond_14
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@17
    .line 1730
    iget-object v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mTransitions:Ljava/util/List;

    #@19
    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@1c
    .line 1733
    :cond_1c
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getEndTransition()Landroid/media/videoeditor/Transition;

    #@1f
    move-result-object v1

    #@20
    .line 1734
    .local v1, endTransition:Landroid/media/videoeditor/Transition;
    if-eqz v1, :cond_37

    #@22
    .line 1735
    invoke-virtual {v1}, Landroid/media/videoeditor/Transition;->getBeforeMediaItem()Landroid/media/videoeditor/MediaItem;

    #@25
    move-result-object v2

    #@26
    if-eqz v2, :cond_2f

    #@28
    .line 1736
    invoke-virtual {v1}, Landroid/media/videoeditor/Transition;->getBeforeMediaItem()Landroid/media/videoeditor/MediaItem;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v2, v3}, Landroid/media/videoeditor/MediaItem;->setBeginTransition(Landroid/media/videoeditor/Transition;)V

    #@2f
    .line 1738
    :cond_2f
    invoke-virtual {v1}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@32
    .line 1739
    iget-object v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mTransitions:Ljava/util/List;

    #@34
    invoke-interface {v2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@37
    .line 1742
    :cond_37
    invoke-virtual {p1, v3}, Landroid/media/videoeditor/MediaItem;->setBeginTransition(Landroid/media/videoeditor/Transition;)V

    #@3a
    .line 1743
    invoke-virtual {p1, v3}, Landroid/media/videoeditor/MediaItem;->setEndTransition(Landroid/media/videoeditor/Transition;)V

    #@3d
    .line 1744
    return-void
.end method

.method private declared-synchronized removeMediaItem(Ljava/lang/String;Z)Landroid/media/videoeditor/MediaItem;
    .registers 7
    .parameter "mediaItemId"
    .parameter "flag"

    #@0
    .prologue
    .line 852
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@3
    const/4 v3, 0x0

    #@4
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@7
    move-result-object v2

    #@8
    check-cast v2, Landroid/media/videoeditor/MediaItem;

    #@a
    invoke-virtual {v2}, Landroid/media/videoeditor/MediaItem;->getId()Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    .line 854
    .local v0, firstItemString:Ljava/lang/String;
    invoke-virtual {p0, p1}, Landroid/media/videoeditor/VideoEditorImpl;->getMediaItem(Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;

    #@11
    move-result-object v1

    #@12
    .line 855
    .local v1, mediaItem:Landroid/media/videoeditor/MediaItem;
    if-eqz v1, :cond_25

    #@14
    .line 856
    iget-object v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@16
    const/4 v3, 0x1

    #@17
    invoke-virtual {v2, v3}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@1a
    .line 860
    iget-object v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@1c
    invoke-interface {v2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@1f
    .line 864
    invoke-direct {p0, v1}, Landroid/media/videoeditor/VideoEditorImpl;->removeAdjacentTransitions(Landroid/media/videoeditor/MediaItem;)V

    #@22
    .line 865
    invoke-direct {p0}, Landroid/media/videoeditor/VideoEditorImpl;->computeTimelineDuration()V

    #@25
    .line 872
    :cond_25
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v2

    #@29
    if-eqz v2, :cond_2e

    #@2b
    .line 873
    invoke-direct {p0}, Landroid/media/videoeditor/VideoEditorImpl;->generateProjectThumbnail()V
    :try_end_2e
    .catchall {:try_start_1 .. :try_end_2e} :catchall_30

    #@2e
    .line 875
    :cond_2e
    monitor-exit p0

    #@2f
    return-object v1

    #@30
    .line 852
    .end local v0           #firstItemString:Ljava/lang/String;
    .end local v1           #mediaItem:Landroid/media/videoeditor/MediaItem;
    :catchall_30
    move-exception v2

    #@31
    monitor-exit p0

    #@32
    throw v2
.end method

.method private removeTransitionAfter(I)V
    .registers 8
    .parameter "index"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1775
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@3
    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@6
    move-result-object v1

    #@7
    check-cast v1, Landroid/media/videoeditor/MediaItem;

    #@9
    .line 1776
    .local v1, mediaItem:Landroid/media/videoeditor/MediaItem;
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mTransitions:Ljava/util/List;

    #@b
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@e
    move-result-object v0

    #@f
    .line 1777
    .local v0, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/videoeditor/Transition;>;"
    :cond_f
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@12
    move-result v3

    #@13
    if-eqz v3, :cond_47

    #@15
    .line 1778
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@18
    move-result-object v2

    #@19
    check-cast v2, Landroid/media/videoeditor/Transition;

    #@1b
    .line 1779
    .local v2, t:Landroid/media/videoeditor/Transition;
    invoke-virtual {v2}, Landroid/media/videoeditor/Transition;->getAfterMediaItem()Landroid/media/videoeditor/MediaItem;

    #@1e
    move-result-object v3

    #@1f
    if-ne v3, v1, :cond_f

    #@21
    .line 1780
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@23
    const/4 v4, 0x1

    #@24
    invoke-virtual {v3, v4}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@27
    .line 1781
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    #@2a
    .line 1782
    invoke-virtual {v2}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@2d
    .line 1783
    invoke-virtual {v1, v5}, Landroid/media/videoeditor/MediaItem;->setEndTransition(Landroid/media/videoeditor/Transition;)V

    #@30
    .line 1787
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@32
    invoke-interface {v3}, Ljava/util/List;->size()I

    #@35
    move-result v3

    #@36
    add-int/lit8 v3, v3, -0x1

    #@38
    if-ge p1, v3, :cond_47

    #@3a
    .line 1788
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@3c
    add-int/lit8 v4, p1, 0x1

    #@3e
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@41
    move-result-object v3

    #@42
    check-cast v3, Landroid/media/videoeditor/MediaItem;

    #@44
    invoke-virtual {v3, v5}, Landroid/media/videoeditor/MediaItem;->setBeginTransition(Landroid/media/videoeditor/Transition;)V

    #@47
    .line 1793
    .end local v2           #t:Landroid/media/videoeditor/Transition;
    :cond_47
    return-void
.end method

.method private removeTransitionBefore(I)V
    .registers 8
    .parameter "index"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1752
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@3
    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@6
    move-result-object v1

    #@7
    check-cast v1, Landroid/media/videoeditor/MediaItem;

    #@9
    .line 1753
    .local v1, mediaItem:Landroid/media/videoeditor/MediaItem;
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mTransitions:Ljava/util/List;

    #@b
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@e
    move-result-object v0

    #@f
    .line 1754
    .local v0, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/videoeditor/Transition;>;"
    :cond_f
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@12
    move-result v3

    #@13
    if-eqz v3, :cond_3f

    #@15
    .line 1755
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@18
    move-result-object v2

    #@19
    check-cast v2, Landroid/media/videoeditor/Transition;

    #@1b
    .line 1756
    .local v2, t:Landroid/media/videoeditor/Transition;
    invoke-virtual {v2}, Landroid/media/videoeditor/Transition;->getBeforeMediaItem()Landroid/media/videoeditor/MediaItem;

    #@1e
    move-result-object v3

    #@1f
    if-ne v3, v1, :cond_f

    #@21
    .line 1757
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@23
    const/4 v4, 0x1

    #@24
    invoke-virtual {v3, v4}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@27
    .line 1758
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    #@2a
    .line 1759
    invoke-virtual {v2}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@2d
    .line 1760
    invoke-virtual {v1, v5}, Landroid/media/videoeditor/MediaItem;->setBeginTransition(Landroid/media/videoeditor/Transition;)V

    #@30
    .line 1761
    if-lez p1, :cond_3f

    #@32
    .line 1762
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@34
    add-int/lit8 v4, p1, -0x1

    #@36
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@39
    move-result-object v3

    #@3a
    check-cast v3, Landroid/media/videoeditor/MediaItem;

    #@3c
    invoke-virtual {v3, v5}, Landroid/media/videoeditor/MediaItem;->setEndTransition(Landroid/media/videoeditor/Transition;)V

    #@3f
    .line 1767
    .end local v2           #t:Landroid/media/videoeditor/Transition;
    :cond_3f
    return-void
.end method

.method private unlock()V
    .registers 3

    #@0
    .prologue
    .line 1943
    const-string v0, "VideoEditorImpl"

    #@2
    const/4 v1, 0x3

    #@3
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_11

    #@9
    .line 1944
    const-string v0, "VideoEditorImpl"

    #@b
    const-string/jumbo v1, "unlock: releasing semaphore"

    #@e
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 1946
    :cond_11
    iget-object v0, p0, Landroid/media/videoeditor/VideoEditorImpl;->mLock:Ljava/util/concurrent/Semaphore;

    #@13
    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    #@16
    .line 1947
    return-void
.end method


# virtual methods
.method public declared-synchronized addAudioTrack(Landroid/media/videoeditor/AudioTrack;)V
    .registers 5
    .parameter "audioTrack"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 189
    monitor-enter p0

    #@2
    if-nez p1, :cond_f

    #@4
    .line 190
    :try_start_4
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@6
    const-string v2, "Audio Track is null"

    #@8
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v1
    :try_end_c
    .catchall {:try_start_4 .. :try_end_c} :catchall_c

    #@c
    .line 189
    :catchall_c
    move-exception v1

    #@d
    monitor-exit p0

    #@e
    throw v1

    #@f
    .line 193
    :cond_f
    :try_start_f
    iget-object v1, p0, Landroid/media/videoeditor/VideoEditorImpl;->mAudioTracks:Ljava/util/List;

    #@11
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@14
    move-result v1

    #@15
    if-ne v1, v2, :cond_1f

    #@17
    .line 194
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@19
    const-string v2, "No more tracks can be added"

    #@1b
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v1

    #@1f
    .line 197
    :cond_1f
    iget-object v1, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@21
    const/4 v2, 0x1

    #@22
    invoke-virtual {v1, v2}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@25
    .line 202
    iget-object v1, p0, Landroid/media/videoeditor/VideoEditorImpl;->mAudioTracks:Ljava/util/List;

    #@27
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@2a
    .line 207
    new-instance v1, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    iget-object v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mProjectPath:Ljava/lang/String;

    #@31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    const-string v2, "/"

    #@37
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v1

    #@3b
    const-string v2, "AudioPcm"

    #@3d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    invoke-virtual {p1}, Landroid/media/videoeditor/AudioTrack;->getId()Ljava/lang/String;

    #@44
    move-result-object v2

    #@45
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v1

    #@49
    const-string v2, ".pcm"

    #@4b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v1

    #@4f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v1

    #@53
    const/4 v2, 0x0

    #@54
    new-array v2, v2, [Ljava/lang/Object;

    #@56
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@59
    move-result-object v0

    #@5a
    .line 213
    .local v0, audioTrackPCMFilePath:Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    #@5c
    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@5f
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@62
    move-result v1

    #@63
    if-eqz v1, :cond_6b

    #@65
    .line 214
    iget-object v1, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@67
    const/4 v2, 0x0

    #@68
    invoke-virtual {v1, v2}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setAudioflag(Z)V
    :try_end_6b
    .catchall {:try_start_f .. :try_end_6b} :catchall_c

    #@6b
    .line 217
    :cond_6b
    monitor-exit p0

    #@6c
    return-void
.end method

.method public declared-synchronized addMediaItem(Landroid/media/videoeditor/MediaItem;)V
    .registers 6
    .parameter "mediaItem"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 226
    monitor-enter p0

    #@2
    if-nez p1, :cond_f

    #@4
    .line 227
    :try_start_4
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@6
    const-string v2, "Media item is null"

    #@8
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v1
    :try_end_c
    .catchall {:try_start_4 .. :try_end_c} :catchall_c

    #@c
    .line 226
    :catchall_c
    move-exception v1

    #@d
    monitor-exit p0

    #@e
    throw v1

    #@f
    .line 232
    :cond_f
    :try_start_f
    iget-object v1, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@11
    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@14
    move-result v1

    #@15
    if-eqz v1, :cond_34

    #@17
    .line 233
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@19
    new-instance v2, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v3, "Media item already exists: "

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getId()Ljava/lang/String;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v2

    #@30
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@33
    throw v1

    #@34
    .line 236
    :cond_34
    iget-object v1, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@36
    const/4 v2, 0x1

    #@37
    invoke-virtual {v1, v2}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@3a
    .line 241
    iget-object v1, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@3c
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@3f
    move-result v0

    #@40
    .line 242
    .local v0, mediaItemsCount:I
    if-lez v0, :cond_47

    #@42
    .line 243
    add-int/lit8 v1, v0, -0x1

    #@44
    invoke-direct {p0, v1}, Landroid/media/videoeditor/VideoEditorImpl;->removeTransitionAfter(I)V

    #@47
    .line 249
    :cond_47
    iget-object v1, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@49
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@4c
    .line 251
    invoke-direct {p0}, Landroid/media/videoeditor/VideoEditorImpl;->computeTimelineDuration()V

    #@4f
    .line 256
    iget-object v1, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@51
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@54
    move-result v1

    #@55
    if-ne v1, v3, :cond_5a

    #@57
    .line 257
    invoke-direct {p0}, Landroid/media/videoeditor/VideoEditorImpl;->generateProjectThumbnail()V
    :try_end_5a
    .catchall {:try_start_f .. :try_end_5a} :catchall_c

    #@5a
    .line 259
    :cond_5a
    monitor-exit p0

    #@5b
    return-void
.end method

.method public declared-synchronized addTransition(Landroid/media/videoeditor/Transition;)V
    .registers 8
    .parameter "transition"

    #@0
    .prologue
    const/4 v5, -0x1

    #@1
    .line 266
    monitor-enter p0

    #@2
    if-nez p1, :cond_f

    #@4
    .line 267
    :try_start_4
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@6
    const-string v5, "Null Transition"

    #@8
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v4
    :try_end_c
    .catchall {:try_start_4 .. :try_end_c} :catchall_c

    #@c
    .line 266
    :catchall_c
    move-exception v4

    #@d
    monitor-exit p0

    #@e
    throw v4

    #@f
    .line 270
    :cond_f
    :try_start_f
    invoke-virtual {p1}, Landroid/media/videoeditor/Transition;->getBeforeMediaItem()Landroid/media/videoeditor/MediaItem;

    #@12
    move-result-object v2

    #@13
    .line 271
    .local v2, beforeMediaItem:Landroid/media/videoeditor/MediaItem;
    invoke-virtual {p1}, Landroid/media/videoeditor/Transition;->getAfterMediaItem()Landroid/media/videoeditor/MediaItem;

    #@16
    move-result-object v0

    #@17
    .line 275
    .local v0, afterMediaItem:Landroid/media/videoeditor/MediaItem;
    iget-object v4, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@19
    if-nez v4, :cond_23

    #@1b
    .line 276
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@1d
    const-string v5, "No media items are added"

    #@1f
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@22
    throw v4

    #@23
    .line 279
    :cond_23
    if-eqz v0, :cond_4b

    #@25
    if-eqz v2, :cond_4b

    #@27
    .line 280
    iget-object v4, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@29
    invoke-interface {v4, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    #@2c
    move-result v1

    #@2d
    .line 281
    .local v1, afterMediaItemIndex:I
    iget-object v4, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@2f
    invoke-interface {v4, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    #@32
    move-result v3

    #@33
    .line 283
    .local v3, beforeMediaItemIndex:I
    if-eq v1, v5, :cond_37

    #@35
    if-ne v3, v5, :cond_3f

    #@37
    .line 284
    :cond_37
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@39
    const-string v5, "Either of the mediaItem is not found in the list"

    #@3b
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@3e
    throw v4

    #@3f
    .line 288
    :cond_3f
    add-int/lit8 v4, v3, -0x1

    #@41
    if-eq v1, v4, :cond_4b

    #@43
    .line 289
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@45
    const-string v5, "MediaItems are not in sequence"

    #@47
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@4a
    throw v4

    #@4b
    .line 293
    .end local v1           #afterMediaItemIndex:I
    .end local v3           #beforeMediaItemIndex:I
    :cond_4b
    iget-object v4, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@4d
    const/4 v5, 0x1

    #@4e
    invoke-virtual {v4, v5}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@51
    .line 295
    iget-object v4, p0, Landroid/media/videoeditor/VideoEditorImpl;->mTransitions:Ljava/util/List;

    #@53
    invoke-interface {v4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@56
    .line 299
    if-eqz v0, :cond_71

    #@58
    .line 304
    invoke-virtual {v0}, Landroid/media/videoeditor/MediaItem;->getEndTransition()Landroid/media/videoeditor/Transition;

    #@5b
    move-result-object v4

    #@5c
    if-eqz v4, :cond_6e

    #@5e
    .line 305
    invoke-virtual {v0}, Landroid/media/videoeditor/MediaItem;->getEndTransition()Landroid/media/videoeditor/Transition;

    #@61
    move-result-object v4

    #@62
    invoke-virtual {v4}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@65
    .line 306
    iget-object v4, p0, Landroid/media/videoeditor/VideoEditorImpl;->mTransitions:Ljava/util/List;

    #@67
    invoke-virtual {v0}, Landroid/media/videoeditor/MediaItem;->getEndTransition()Landroid/media/videoeditor/Transition;

    #@6a
    move-result-object v5

    #@6b
    invoke-interface {v4, v5}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@6e
    .line 308
    :cond_6e
    invoke-virtual {v0, p1}, Landroid/media/videoeditor/MediaItem;->setEndTransition(Landroid/media/videoeditor/Transition;)V

    #@71
    .line 311
    :cond_71
    if-eqz v2, :cond_8c

    #@73
    .line 316
    invoke-virtual {v2}, Landroid/media/videoeditor/MediaItem;->getBeginTransition()Landroid/media/videoeditor/Transition;

    #@76
    move-result-object v4

    #@77
    if-eqz v4, :cond_89

    #@79
    .line 317
    invoke-virtual {v2}, Landroid/media/videoeditor/MediaItem;->getBeginTransition()Landroid/media/videoeditor/Transition;

    #@7c
    move-result-object v4

    #@7d
    invoke-virtual {v4}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@80
    .line 318
    iget-object v4, p0, Landroid/media/videoeditor/VideoEditorImpl;->mTransitions:Ljava/util/List;

    #@82
    invoke-virtual {v2}, Landroid/media/videoeditor/MediaItem;->getBeginTransition()Landroid/media/videoeditor/Transition;

    #@85
    move-result-object v5

    #@86
    invoke-interface {v4, v5}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@89
    .line 320
    :cond_89
    invoke-virtual {v2, p1}, Landroid/media/videoeditor/MediaItem;->setBeginTransition(Landroid/media/videoeditor/Transition;)V

    #@8c
    .line 323
    :cond_8c
    invoke-direct {p0}, Landroid/media/videoeditor/VideoEditorImpl;->computeTimelineDuration()V
    :try_end_8f
    .catchall {:try_start_f .. :try_end_8f} :catchall_c

    #@8f
    .line 324
    monitor-exit p0

    #@90
    return-void
.end method

.method public cancelExport(Ljava/lang/String;)V
    .registers 3
    .parameter "filename"

    #@0
    .prologue
    .line 330
    iget-object v0, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@2
    if-eqz v0, :cond_b

    #@4
    if-eqz p1, :cond_b

    #@6
    .line 331
    iget-object v0, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@8
    invoke-virtual {v0, p1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->stop(Ljava/lang/String;)V

    #@b
    .line 333
    :cond_b
    return-void
.end method

.method public clearSurface(Landroid/view/SurfaceHolder;)V
    .registers 5
    .parameter "surfaceHolder"

    #@0
    .prologue
    .line 1883
    if-nez p1, :cond_a

    #@2
    .line 1884
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v2, "Invalid surface holder"

    #@6
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v1

    #@a
    .line 1887
    :cond_a
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    #@d
    move-result-object v0

    #@e
    .line 1888
    .local v0, surface:Landroid/view/Surface;
    if-nez v0, :cond_18

    #@10
    .line 1889
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@12
    const-string v2, "Surface could not be retrieved from surface holder"

    #@14
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v1

    #@18
    .line 1892
    :cond_18
    invoke-virtual {v0}, Landroid/view/Surface;->isValid()Z

    #@1b
    move-result v1

    #@1c
    if-nez v1, :cond_26

    #@1e
    .line 1893
    new-instance v1, Ljava/lang/IllegalStateException;

    #@20
    const-string v2, "Surface is not valid"

    #@22
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@25
    throw v1

    #@26
    .line 1896
    :cond_26
    iget-object v1, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@28
    if-eqz v1, :cond_30

    #@2a
    .line 1897
    iget-object v1, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@2c
    invoke-virtual {v1, v0}, Landroid/media/videoeditor/MediaArtistNativeHelper;->clearPreviewSurface(Landroid/view/Surface;)V

    #@2f
    .line 1901
    :goto_2f
    return-void

    #@30
    .line 1899
    :cond_30
    const-string v1, "VideoEditorImpl"

    #@32
    const-string v2, "Native helper was not ready!"

    #@34
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    goto :goto_2f
.end method

.method public export(Ljava/lang/String;IIIILandroid/media/videoeditor/VideoEditor$ExportProgressListener;)V
    .registers 28
    .parameter "filename"
    .parameter "height"
    .parameter "bitrate"
    .parameter "audioCodec"
    .parameter "videoCodec"
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 342
    const/4 v11, 0x0

    #@1
    .line 343
    .local v11, audcodec:I
    const/16 v20, 0x0

    #@3
    .line 344
    .local v20, vidcodec:I
    if-nez p1, :cond_d

    #@5
    .line 345
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@7
    const-string v3, "export: filename is null"

    #@9
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@c
    throw v2

    #@d
    .line 348
    :cond_d
    new-instance v19, Ljava/io/File;

    #@f
    move-object/from16 v0, v19

    #@11
    move-object/from16 v1, p1

    #@13
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@16
    .line 349
    .local v19, tempPathFile:Ljava/io/File;
    if-nez v19, :cond_33

    #@18
    .line 350
    new-instance v2, Ljava/io/IOException;

    #@1a
    new-instance v3, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    move-object/from16 v0, p1

    #@21
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    const-string v4, "can not be created"

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@32
    throw v2

    #@33
    .line 353
    :cond_33
    move-object/from16 v0, p0

    #@35
    iget-object v2, v0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@37
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@3a
    move-result v2

    #@3b
    if-nez v2, :cond_45

    #@3d
    .line 354
    new-instance v2, Ljava/lang/IllegalStateException;

    #@3f
    const-string v3, "No MediaItems added"

    #@41
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@44
    throw v2

    #@45
    .line 357
    :cond_45
    sparse-switch p2, :sswitch_data_156

    #@48
    .line 372
    new-instance v2, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v3, "Unsupported height value "

    #@4f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v2

    #@53
    move/from16 v0, p2

    #@55
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@58
    move-result-object v2

    #@59
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v17

    #@5d
    .line 373
    .local v17, message:Ljava/lang/String;
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@5f
    move-object/from16 v0, v17

    #@61
    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@64
    throw v2

    #@65
    .line 377
    .end local v17           #message:Ljava/lang/String;
    :sswitch_65
    sparse-switch p3, :sswitch_data_170

    #@68
    .line 406
    new-instance v2, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const-string v3, "Unsupported bitrate value "

    #@6f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v2

    #@73
    move/from16 v0, p3

    #@75
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@78
    move-result-object v2

    #@79
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v17

    #@7d
    .line 407
    .restart local v17       #message:Ljava/lang/String;
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@7f
    move-object/from16 v0, v17

    #@81
    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@84
    throw v2

    #@85
    .line 410
    .end local v17           #message:Ljava/lang/String;
    :sswitch_85
    invoke-direct/range {p0 .. p0}, Landroid/media/videoeditor/VideoEditorImpl;->computeTimelineDuration()V

    #@88
    .line 411
    const-wide/32 v12, 0x17700

    #@8b
    .line 412
    .local v12, audioBitrate:J
    move-object/from16 v0, p0

    #@8d
    iget-wide v2, v0, Landroid/media/videoeditor/VideoEditorImpl;->mDurationMs:J

    #@8f
    move/from16 v0, p3

    #@91
    int-to-long v4, v0

    #@92
    const-wide/32 v6, 0x17700

    #@95
    add-long/2addr v4, v6

    #@96
    mul-long/2addr v2, v4

    #@97
    const-wide/16 v4, 0x1f40

    #@99
    div-long v15, v2, v4

    #@9b
    .line 413
    .local v15, fileSize:J
    const-wide v2, 0x80000000L

    #@a0
    cmp-long v2, v2, v15

    #@a2
    if-gtz v2, :cond_ac

    #@a4
    .line 414
    new-instance v2, Ljava/lang/IllegalStateException;

    #@a6
    const-string v3, "Export Size is more than 2GB"

    #@a8
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@ab
    throw v2

    #@ac
    .line 416
    :cond_ac
    packed-switch p4, :pswitch_data_1a6

    #@af
    .line 425
    new-instance v2, Ljava/lang/StringBuilder;

    #@b1
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b4
    const-string v3, "Unsupported audio codec type "

    #@b6
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v2

    #@ba
    move/from16 v0, p4

    #@bc
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v2

    #@c0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c3
    move-result-object v17

    #@c4
    .line 426
    .restart local v17       #message:Ljava/lang/String;
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@c6
    move-object/from16 v0, v17

    #@c8
    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@cb
    throw v2

    #@cc
    .line 418
    .end local v17           #message:Ljava/lang/String;
    :pswitch_cc
    const/4 v11, 0x2

    #@cd
    .line 430
    :goto_cd
    packed-switch p5, :pswitch_data_1ae

    #@d0
    .line 442
    new-instance v2, Ljava/lang/StringBuilder;

    #@d2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d5
    const-string v3, "Unsupported video codec type "

    #@d7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@da
    move-result-object v2

    #@db
    move/from16 v0, p5

    #@dd
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e0
    move-result-object v2

    #@e1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e4
    move-result-object v17

    #@e5
    .line 443
    .restart local v17       #message:Ljava/lang/String;
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@e7
    move-object/from16 v0, v17

    #@e9
    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@ec
    throw v2

    #@ed
    .line 421
    .end local v17           #message:Ljava/lang/String;
    :pswitch_ed
    const/4 v11, 0x1

    #@ee
    .line 422
    goto :goto_cd

    #@ef
    .line 432
    :pswitch_ef
    const/16 v20, 0x1

    #@f1
    .line 447
    :goto_f1
    const/16 v18, 0x0

    #@f3
    .line 449
    .local v18, semAcquireDone:Z
    :try_start_f3
    invoke-direct/range {p0 .. p0}, Landroid/media/videoeditor/VideoEditorImpl;->lock()V

    #@f6
    .line 450
    const/16 v18, 0x1

    #@f8
    .line 452
    move-object/from16 v0, p0

    #@fa
    iget-object v2, v0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@fc
    if-nez v2, :cond_11a

    #@fe
    .line 453
    new-instance v2, Ljava/lang/IllegalStateException;

    #@100
    const-string v3, "The video editor is not initialized"

    #@102
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@105
    throw v2
    :try_end_106
    .catchall {:try_start_f3 .. :try_end_106} :catchall_14f
    .catch Ljava/lang/InterruptedException; {:try_start_f3 .. :try_end_106} :catch_106

    #@106
    .line 459
    :catch_106
    move-exception v14

    #@107
    .line 460
    .local v14, ex:Ljava/lang/InterruptedException;
    :try_start_107
    const-string v2, "VideoEditorImpl"

    #@109
    const-string v3, "Sem acquire NOT successful in export"

    #@10b
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_10e
    .catchall {:try_start_107 .. :try_end_10e} :catchall_14f

    #@10e
    .line 462
    if-eqz v18, :cond_113

    #@110
    .line 463
    invoke-direct/range {p0 .. p0}, Landroid/media/videoeditor/VideoEditorImpl;->unlock()V

    #@113
    .line 466
    .end local v14           #ex:Ljava/lang/InterruptedException;
    :cond_113
    :goto_113
    return-void

    #@114
    .line 435
    .end local v18           #semAcquireDone:Z
    :pswitch_114
    const/16 v20, 0x2

    #@116
    .line 436
    goto :goto_f1

    #@117
    .line 438
    :pswitch_117
    const/16 v20, 0x3

    #@119
    .line 439
    goto :goto_f1

    #@11a
    .line 455
    .restart local v18       #semAcquireDone:Z
    :cond_11a
    :try_start_11a
    move-object/from16 v0, p0

    #@11c
    iget-object v2, v0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@11e
    invoke-virtual {v2, v11}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setAudioCodec(I)V

    #@121
    .line 456
    move-object/from16 v0, p0

    #@123
    iget-object v2, v0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@125
    move/from16 v0, v20

    #@127
    invoke-virtual {v2, v0}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setVideoCodec(I)V

    #@12a
    .line 457
    move-object/from16 v0, p0

    #@12c
    iget-object v2, v0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@12e
    move-object/from16 v0, p0

    #@130
    iget-object v4, v0, Landroid/media/videoeditor/VideoEditorImpl;->mProjectPath:Ljava/lang/String;

    #@132
    move-object/from16 v0, p0

    #@134
    iget-object v7, v0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@136
    move-object/from16 v0, p0

    #@138
    iget-object v8, v0, Landroid/media/videoeditor/VideoEditorImpl;->mTransitions:Ljava/util/List;

    #@13a
    move-object/from16 v0, p0

    #@13c
    iget-object v9, v0, Landroid/media/videoeditor/VideoEditorImpl;->mAudioTracks:Ljava/util/List;

    #@13e
    move-object/from16 v3, p1

    #@140
    move/from16 v5, p2

    #@142
    move/from16 v6, p3

    #@144
    move-object/from16 v10, p6

    #@146
    invoke-virtual/range {v2 .. v10}, Landroid/media/videoeditor/MediaArtistNativeHelper;->export(Ljava/lang/String;Ljava/lang/String;IILjava/util/List;Ljava/util/List;Ljava/util/List;Landroid/media/videoeditor/VideoEditor$ExportProgressListener;)V
    :try_end_149
    .catchall {:try_start_11a .. :try_end_149} :catchall_14f
    .catch Ljava/lang/InterruptedException; {:try_start_11a .. :try_end_149} :catch_106

    #@149
    .line 462
    if-eqz v18, :cond_113

    #@14b
    .line 463
    invoke-direct/range {p0 .. p0}, Landroid/media/videoeditor/VideoEditorImpl;->unlock()V

    #@14e
    goto :goto_113

    #@14f
    .line 462
    :catchall_14f
    move-exception v2

    #@150
    if-eqz v18, :cond_155

    #@152
    .line 463
    invoke-direct/range {p0 .. p0}, Landroid/media/videoeditor/VideoEditorImpl;->unlock()V

    #@155
    :cond_155
    throw v2

    #@156
    .line 357
    :sswitch_data_156
    .sparse-switch
        0x90 -> :sswitch_65
        0x120 -> :sswitch_65
        0x168 -> :sswitch_65
        0x1e0 -> :sswitch_65
        0x2d0 -> :sswitch_65
        0x438 -> :sswitch_65
    .end sparse-switch

    #@170
    .line 377
    :sswitch_data_170
    .sparse-switch
        0x6d60 -> :sswitch_85
        0x9c40 -> :sswitch_85
        0xfa00 -> :sswitch_85
        0x17700 -> :sswitch_85
        0x1f400 -> :sswitch_85
        0x2ee00 -> :sswitch_85
        0x3e800 -> :sswitch_85
        0x5dc00 -> :sswitch_85
        0x7d000 -> :sswitch_85
        0xc3500 -> :sswitch_85
        0x1e8480 -> :sswitch_85
        0x4c4b40 -> :sswitch_85
        0x7a1200 -> :sswitch_85
    .end sparse-switch

    #@1a6
    .line 416
    :pswitch_data_1a6
    .packed-switch 0x1
        :pswitch_ed
        :pswitch_cc
    .end packed-switch

    #@1ae
    .line 430
    :pswitch_data_1ae
    .packed-switch 0x1
        :pswitch_ef
        :pswitch_114
        :pswitch_117
    .end packed-switch
.end method

.method public export(Ljava/lang/String;IILandroid/media/videoeditor/VideoEditor$ExportProgressListener;)V
    .registers 12
    .parameter "filename"
    .parameter "height"
    .parameter "bitrate"
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 474
    const/4 v4, 0x2

    #@1
    .line 475
    .local v4, defaultAudiocodec:I
    const/4 v5, 0x2

    #@2
    .local v5, defaultVideocodec:I
    move-object v0, p0

    #@3
    move-object v1, p1

    #@4
    move v2, p2

    #@5
    move v3, p3

    #@6
    move-object v6, p4

    #@7
    .line 477
    invoke-virtual/range {v0 .. v6}, Landroid/media/videoeditor/VideoEditorImpl;->export(Ljava/lang/String;IIIILandroid/media/videoeditor/VideoEditor$ExportProgressListener;)V

    #@a
    .line 479
    return-void
.end method

.method public generatePreview(Landroid/media/videoeditor/VideoEditor$MediaProcessingProgressListener;)V
    .registers 8
    .parameter "listener"

    #@0
    .prologue
    .line 485
    const/4 v1, 0x0

    #@1
    .line 487
    .local v1, semAcquireDone:Z
    :try_start_1
    invoke-direct {p0}, Landroid/media/videoeditor/VideoEditorImpl;->lock()V

    #@4
    .line 488
    const/4 v1, 0x1

    #@5
    .line 490
    iget-object v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@7
    if-nez v2, :cond_1f

    #@9
    .line 491
    new-instance v2, Ljava/lang/IllegalStateException;

    #@b
    const-string v3, "The video editor is not initialized"

    #@d
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@10
    throw v2
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_40
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_11} :catch_11

    #@11
    .line 498
    :catch_11
    move-exception v0

    #@12
    .line 499
    .local v0, ex:Ljava/lang/InterruptedException;
    :try_start_12
    const-string v2, "VideoEditorImpl"

    #@14
    const-string v3, "Sem acquire NOT successful in previewStoryBoard"

    #@16
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_19
    .catchall {:try_start_12 .. :try_end_19} :catchall_40

    #@19
    .line 501
    if-eqz v1, :cond_1e

    #@1b
    .line 502
    invoke-direct {p0}, Landroid/media/videoeditor/VideoEditorImpl;->unlock()V

    #@1e
    .line 505
    .end local v0           #ex:Ljava/lang/InterruptedException;
    :cond_1e
    :goto_1e
    return-void

    #@1f
    .line 494
    :cond_1f
    :try_start_1f
    iget-object v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@21
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@24
    move-result v2

    #@25
    if-gtz v2, :cond_2f

    #@27
    iget-object v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mAudioTracks:Ljava/util/List;

    #@29
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@2c
    move-result v2

    #@2d
    if-lez v2, :cond_3a

    #@2f
    .line 495
    :cond_2f
    iget-object v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@31
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@33
    iget-object v4, p0, Landroid/media/videoeditor/VideoEditorImpl;->mTransitions:Ljava/util/List;

    #@35
    iget-object v5, p0, Landroid/media/videoeditor/VideoEditorImpl;->mAudioTracks:Ljava/util/List;

    #@37
    invoke-virtual {v2, v3, v4, v5, p1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->previewStoryBoard(Ljava/util/List;Ljava/util/List;Ljava/util/List;Landroid/media/videoeditor/VideoEditor$MediaProcessingProgressListener;)V
    :try_end_3a
    .catchall {:try_start_1f .. :try_end_3a} :catchall_40
    .catch Ljava/lang/InterruptedException; {:try_start_1f .. :try_end_3a} :catch_11

    #@3a
    .line 501
    :cond_3a
    if-eqz v1, :cond_1e

    #@3c
    .line 502
    invoke-direct {p0}, Landroid/media/videoeditor/VideoEditorImpl;->unlock()V

    #@3f
    goto :goto_1e

    #@40
    .line 501
    :catchall_40
    move-exception v2

    #@41
    if-eqz v1, :cond_46

    #@43
    .line 502
    invoke-direct {p0}, Landroid/media/videoeditor/VideoEditorImpl;->unlock()V

    #@46
    :cond_46
    throw v2
.end method

.method public getAllAudioTracks()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/media/videoeditor/AudioTrack;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 511
    iget-object v0, p0, Landroid/media/videoeditor/VideoEditorImpl;->mAudioTracks:Ljava/util/List;

    #@2
    return-object v0
.end method

.method public getAllMediaItems()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/media/videoeditor/MediaItem;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 518
    iget-object v0, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@2
    return-object v0
.end method

.method public getAllTransitions()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/media/videoeditor/Transition;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 525
    iget-object v0, p0, Landroid/media/videoeditor/VideoEditorImpl;->mTransitions:Ljava/util/List;

    #@2
    return-object v0
.end method

.method public getAspectRatio()I
    .registers 2

    #@0
    .prologue
    .line 532
    iget v0, p0, Landroid/media/videoeditor/VideoEditorImpl;->mAspectRatio:I

    #@2
    return v0
.end method

.method public getAudioTrack(Ljava/lang/String;)Landroid/media/videoeditor/AudioTrack;
    .registers 5
    .parameter "audioTrackId"

    #@0
    .prologue
    .line 539
    iget-object v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mAudioTracks:Ljava/util/List;

    #@2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .local v1, i$:Ljava/util/Iterator;
    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_1d

    #@c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/media/videoeditor/AudioTrack;

    #@12
    .line 540
    .local v0, at:Landroid/media/videoeditor/AudioTrack;
    invoke-virtual {v0}, Landroid/media/videoeditor/AudioTrack;->getId()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v2

    #@1a
    if-eqz v2, :cond_6

    #@1c
    .line 544
    .end local v0           #at:Landroid/media/videoeditor/AudioTrack;
    :goto_1c
    return-object v0

    #@1d
    :cond_1d
    const/4 v0, 0x0

    #@1e
    goto :goto_1c
.end method

.method public getDuration()J
    .registers 3

    #@0
    .prologue
    .line 555
    invoke-direct {p0}, Landroid/media/videoeditor/VideoEditorImpl;->computeTimelineDuration()V

    #@3
    .line 556
    iget-wide v0, p0, Landroid/media/videoeditor/VideoEditorImpl;->mDurationMs:J

    #@5
    return-wide v0
.end method

.method public declared-synchronized getMediaItem(Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;
    .registers 5
    .parameter "mediaItemId"

    #@0
    .prologue
    .line 570
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@3
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@6
    move-result-object v0

    #@7
    .local v0, i$:Ljava/util/Iterator;
    :cond_7
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@a
    move-result v2

    #@b
    if-eqz v2, :cond_1f

    #@d
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@10
    move-result-object v1

    #@11
    check-cast v1, Landroid/media/videoeditor/MediaItem;

    #@13
    .line 571
    .local v1, mediaItem:Landroid/media/videoeditor/MediaItem;
    invoke-virtual {v1}, Landroid/media/videoeditor/MediaItem;->getId()Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1a
    .catchall {:try_start_1 .. :try_end_1a} :catchall_21

    #@1a
    move-result v2

    #@1b
    if-eqz v2, :cond_7

    #@1d
    .line 575
    .end local v1           #mediaItem:Landroid/media/videoeditor/MediaItem;
    :goto_1d
    monitor-exit p0

    #@1e
    return-object v1

    #@1f
    :cond_1f
    const/4 v1, 0x0

    #@20
    goto :goto_1d

    #@21
    .line 570
    .end local v0           #i$:Ljava/util/Iterator;
    :catchall_21
    move-exception v2

    #@22
    monitor-exit p0

    #@23
    throw v2
.end method

.method getNativeContext()Landroid/media/videoeditor/MediaArtistNativeHelper;
    .registers 2

    #@0
    .prologue
    .line 182
    iget-object v0, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@2
    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 582
    iget-object v0, p0, Landroid/media/videoeditor/VideoEditorImpl;->mProjectPath:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getTransition(Ljava/lang/String;)Landroid/media/videoeditor/Transition;
    .registers 5
    .parameter "transitionId"

    #@0
    .prologue
    .line 589
    iget-object v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mTransitions:Ljava/util/List;

    #@2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v0

    #@6
    .local v0, i$:Ljava/util/Iterator;
    :cond_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_1d

    #@c
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Landroid/media/videoeditor/Transition;

    #@12
    .line 590
    .local v1, transition:Landroid/media/videoeditor/Transition;
    invoke-virtual {v1}, Landroid/media/videoeditor/Transition;->getId()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v2

    #@1a
    if-eqz v2, :cond_6

    #@1c
    .line 594
    .end local v1           #transition:Landroid/media/videoeditor/Transition;
    :goto_1c
    return-object v1

    #@1d
    :cond_1d
    const/4 v1, 0x0

    #@1e
    goto :goto_1c
.end method

.method public declared-synchronized insertAudioTrack(Landroid/media/videoeditor/AudioTrack;Ljava/lang/String;)V
    .registers 9
    .parameter "audioTrack"
    .parameter "afterAudioTrackId"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 602
    monitor-enter p0

    #@2
    :try_start_2
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mAudioTracks:Ljava/util/List;

    #@4
    invoke-interface {v3}, Ljava/util/List;->size()I

    #@7
    move-result v3

    #@8
    if-ne v3, v4, :cond_15

    #@a
    .line 603
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@c
    const-string v4, "No more tracks can be added"

    #@e
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@11
    throw v3
    :try_end_12
    .catchall {:try_start_2 .. :try_end_12} :catchall_12

    #@12
    .line 602
    :catchall_12
    move-exception v3

    #@13
    monitor-exit p0

    #@14
    throw v3

    #@15
    .line 606
    :cond_15
    if-nez p2, :cond_25

    #@17
    .line 607
    :try_start_17
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@19
    const/4 v4, 0x1

    #@1a
    invoke-virtual {v3, v4}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@1d
    .line 608
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mAudioTracks:Ljava/util/List;

    #@1f
    const/4 v4, 0x0

    #@20
    invoke-interface {v3, v4, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V
    :try_end_23
    .catchall {:try_start_17 .. :try_end_23} :catchall_12

    #@23
    .line 622
    :goto_23
    monitor-exit p0

    #@24
    return-void

    #@25
    .line 610
    :cond_25
    :try_start_25
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mAudioTracks:Ljava/util/List;

    #@27
    invoke-interface {v3}, Ljava/util/List;->size()I

    #@2a
    move-result v1

    #@2b
    .line 611
    .local v1, audioTrackCount:I
    const/4 v2, 0x0

    #@2c
    .local v2, i:I
    :goto_2c
    if-ge v2, v1, :cond_51

    #@2e
    .line 612
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mAudioTracks:Ljava/util/List;

    #@30
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@33
    move-result-object v0

    #@34
    check-cast v0, Landroid/media/videoeditor/AudioTrack;

    #@36
    .line 613
    .local v0, at:Landroid/media/videoeditor/AudioTrack;
    invoke-virtual {v0}, Landroid/media/videoeditor/AudioTrack;->getId()Ljava/lang/String;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3d
    move-result v3

    #@3e
    if-eqz v3, :cond_4e

    #@40
    .line 614
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@42
    const/4 v4, 0x1

    #@43
    invoke-virtual {v3, v4}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@46
    .line 615
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mAudioTracks:Ljava/util/List;

    #@48
    add-int/lit8 v4, v2, 0x1

    #@4a
    invoke-interface {v3, v4, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    #@4d
    goto :goto_23

    #@4e
    .line 611
    :cond_4e
    add-int/lit8 v2, v2, 0x1

    #@50
    goto :goto_2c

    #@51
    .line 620
    .end local v0           #at:Landroid/media/videoeditor/AudioTrack;
    :cond_51
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@53
    new-instance v4, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v5, "AudioTrack not found: "

    #@5a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v4

    #@5e
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v4

    #@62
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v4

    #@66
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@69
    throw v3
    :try_end_6a
    .catchall {:try_start_25 .. :try_end_6a} :catchall_12
.end method

.method public declared-synchronized insertMediaItem(Landroid/media/videoeditor/MediaItem;Ljava/lang/String;)V
    .registers 9
    .parameter "mediaItem"
    .parameter "afterMediaItemId"

    #@0
    .prologue
    .line 628
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@3
    invoke-interface {v3, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@6
    move-result v3

    #@7
    if-eqz v3, :cond_29

    #@9
    .line 629
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@b
    new-instance v4, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v5, "Media item already exists: "

    #@12
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v4

    #@16
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getId()Ljava/lang/String;

    #@19
    move-result-object v5

    #@1a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v4

    #@1e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v4

    #@22
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@25
    throw v3
    :try_end_26
    .catchall {:try_start_1 .. :try_end_26} :catchall_26

    #@26
    .line 628
    :catchall_26
    move-exception v3

    #@27
    monitor-exit p0

    #@28
    throw v3

    #@29
    .line 632
    :cond_29
    if-nez p2, :cond_4b

    #@2b
    .line 633
    :try_start_2b
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@2d
    const/4 v4, 0x1

    #@2e
    invoke-virtual {v3, v4}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@31
    .line 634
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@33
    invoke-interface {v3}, Ljava/util/List;->size()I

    #@36
    move-result v3

    #@37
    if-lez v3, :cond_3d

    #@39
    .line 638
    const/4 v3, 0x0

    #@3a
    invoke-direct {p0, v3}, Landroid/media/videoeditor/VideoEditorImpl;->removeTransitionBefore(I)V

    #@3d
    .line 641
    :cond_3d
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@3f
    const/4 v4, 0x0

    #@40
    invoke-interface {v3, v4, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    #@43
    .line 642
    invoke-direct {p0}, Landroid/media/videoeditor/VideoEditorImpl;->computeTimelineDuration()V

    #@46
    .line 643
    invoke-direct {p0}, Landroid/media/videoeditor/VideoEditorImpl;->generateProjectThumbnail()V
    :try_end_49
    .catchall {:try_start_2b .. :try_end_49} :catchall_26

    #@49
    .line 665
    :goto_49
    monitor-exit p0

    #@4a
    return-void

    #@4b
    .line 645
    :cond_4b
    :try_start_4b
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@4d
    invoke-interface {v3}, Ljava/util/List;->size()I

    #@50
    move-result v1

    #@51
    .line 646
    .local v1, mediaItemCount:I
    const/4 v0, 0x0

    #@52
    .local v0, i:I
    :goto_52
    if-ge v0, v1, :cond_7d

    #@54
    .line 647
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@56
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@59
    move-result-object v2

    #@5a
    check-cast v2, Landroid/media/videoeditor/MediaItem;

    #@5c
    .line 648
    .local v2, mi:Landroid/media/videoeditor/MediaItem;
    invoke-virtual {v2}, Landroid/media/videoeditor/MediaItem;->getId()Ljava/lang/String;

    #@5f
    move-result-object v3

    #@60
    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@63
    move-result v3

    #@64
    if-eqz v3, :cond_7a

    #@66
    .line 649
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@68
    const/4 v4, 0x1

    #@69
    invoke-virtual {v3, v4}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@6c
    .line 653
    invoke-direct {p0, v0}, Landroid/media/videoeditor/VideoEditorImpl;->removeTransitionAfter(I)V

    #@6f
    .line 657
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@71
    add-int/lit8 v4, v0, 0x1

    #@73
    invoke-interface {v3, v4, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    #@76
    .line 658
    invoke-direct {p0}, Landroid/media/videoeditor/VideoEditorImpl;->computeTimelineDuration()V

    #@79
    goto :goto_49

    #@7a
    .line 646
    :cond_7a
    add-int/lit8 v0, v0, 0x1

    #@7c
    goto :goto_52

    #@7d
    .line 663
    .end local v2           #mi:Landroid/media/videoeditor/MediaItem;
    :cond_7d
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@7f
    new-instance v4, Ljava/lang/StringBuilder;

    #@81
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@84
    const-string v5, "MediaItem not found: "

    #@86
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v4

    #@8a
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v4

    #@8e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@91
    move-result-object v4

    #@92
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@95
    throw v3
    :try_end_96
    .catchall {:try_start_4b .. :try_end_96} :catchall_26
.end method

.method public declared-synchronized moveAudioTrack(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "audioTrackId"
    .parameter "afterAudioTrackId"

    #@0
    .prologue
    .line 671
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Ljava/lang/IllegalStateException;

    #@3
    const-string v1, "Not supported"

    #@5
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@8
    throw v0
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_9

    #@9
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method public declared-synchronized moveMediaItem(Ljava/lang/String;Ljava/lang/String;)V
    .registers 10
    .parameter "mediaItemId"
    .parameter "afterMediaItemId"

    #@0
    .prologue
    .line 678
    monitor-enter p0

    #@1
    const/4 v4, 0x1

    #@2
    :try_start_2
    invoke-direct {p0, p1, v4}, Landroid/media/videoeditor/VideoEditorImpl;->removeMediaItem(Ljava/lang/String;Z)Landroid/media/videoeditor/MediaItem;

    #@5
    move-result-object v3

    #@6
    .line 679
    .local v3, moveMediaItem:Landroid/media/videoeditor/MediaItem;
    if-nez v3, :cond_24

    #@8
    .line 680
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@a
    new-instance v5, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v6, "Target MediaItem not found: "

    #@11
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v5

    #@15
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v5

    #@19
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v5

    #@1d
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@20
    throw v4
    :try_end_21
    .catchall {:try_start_2 .. :try_end_21} :catchall_21

    #@21
    .line 678
    .end local v3           #moveMediaItem:Landroid/media/videoeditor/MediaItem;
    :catchall_21
    move-exception v4

    #@22
    monitor-exit p0

    #@23
    throw v4

    #@24
    .line 683
    .restart local v3       #moveMediaItem:Landroid/media/videoeditor/MediaItem;
    :cond_24
    if-nez p2, :cond_4e

    #@26
    .line 684
    :try_start_26
    iget-object v4, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@28
    invoke-interface {v4}, Ljava/util/List;->size()I

    #@2b
    move-result v4

    #@2c
    if-lez v4, :cond_46

    #@2e
    .line 685
    iget-object v4, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@30
    const/4 v5, 0x1

    #@31
    invoke-virtual {v4, v5}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@34
    .line 690
    const/4 v4, 0x0

    #@35
    invoke-direct {p0, v4}, Landroid/media/videoeditor/VideoEditorImpl;->removeTransitionBefore(I)V

    #@38
    .line 695
    iget-object v4, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@3a
    const/4 v5, 0x0

    #@3b
    invoke-interface {v4, v5, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    #@3e
    .line 696
    invoke-direct {p0}, Landroid/media/videoeditor/VideoEditorImpl;->computeTimelineDuration()V

    #@41
    .line 698
    invoke-direct {p0}, Landroid/media/videoeditor/VideoEditorImpl;->generateProjectThumbnail()V
    :try_end_44
    .catchall {:try_start_26 .. :try_end_44} :catchall_21

    #@44
    .line 723
    :goto_44
    monitor-exit p0

    #@45
    return-void

    #@46
    .line 700
    :cond_46
    :try_start_46
    new-instance v4, Ljava/lang/IllegalStateException;

    #@48
    const-string v5, "Cannot move media item (it is the only item)"

    #@4a
    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@4d
    throw v4

    #@4e
    .line 703
    :cond_4e
    iget-object v4, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@50
    invoke-interface {v4}, Ljava/util/List;->size()I

    #@53
    move-result v1

    #@54
    .line 704
    .local v1, mediaItemCount:I
    const/4 v0, 0x0

    #@55
    .local v0, i:I
    :goto_55
    if-ge v0, v1, :cond_80

    #@57
    .line 705
    iget-object v4, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@59
    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@5c
    move-result-object v2

    #@5d
    check-cast v2, Landroid/media/videoeditor/MediaItem;

    #@5f
    .line 706
    .local v2, mi:Landroid/media/videoeditor/MediaItem;
    invoke-virtual {v2}, Landroid/media/videoeditor/MediaItem;->getId()Ljava/lang/String;

    #@62
    move-result-object v4

    #@63
    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@66
    move-result v4

    #@67
    if-eqz v4, :cond_7d

    #@69
    .line 707
    iget-object v4, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@6b
    const/4 v5, 0x1

    #@6c
    invoke-virtual {v4, v5}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@6f
    .line 711
    invoke-direct {p0, v0}, Landroid/media/videoeditor/VideoEditorImpl;->removeTransitionAfter(I)V

    #@72
    .line 715
    iget-object v4, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@74
    add-int/lit8 v5, v0, 0x1

    #@76
    invoke-interface {v4, v5, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    #@79
    .line 716
    invoke-direct {p0}, Landroid/media/videoeditor/VideoEditorImpl;->computeTimelineDuration()V

    #@7c
    goto :goto_44

    #@7d
    .line 704
    :cond_7d
    add-int/lit8 v0, v0, 0x1

    #@7f
    goto :goto_55

    #@80
    .line 721
    .end local v2           #mi:Landroid/media/videoeditor/MediaItem;
    :cond_80
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@82
    new-instance v5, Ljava/lang/StringBuilder;

    #@84
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@87
    const-string v6, "MediaItem not found: "

    #@89
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v5

    #@8d
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v5

    #@91
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v5

    #@95
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@98
    throw v4
    :try_end_99
    .catchall {:try_start_46 .. :try_end_99} :catchall_21
.end method

.method public release()V
    .registers 5

    #@0
    .prologue
    .line 729
    invoke-virtual {p0}, Landroid/media/videoeditor/VideoEditorImpl;->stopPreview()J

    #@3
    .line 731
    const/4 v1, 0x0

    #@4
    .line 733
    .local v1, semAcquireDone:Z
    :try_start_4
    invoke-direct {p0}, Landroid/media/videoeditor/VideoEditorImpl;->lock()V

    #@7
    .line 734
    const/4 v1, 0x1

    #@8
    .line 736
    iget-object v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@a
    if-eqz v2, :cond_23

    #@c
    .line 737
    iget-object v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@e
    invoke-interface {v2}, Ljava/util/List;->clear()V

    #@11
    .line 738
    iget-object v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mAudioTracks:Ljava/util/List;

    #@13
    invoke-interface {v2}, Ljava/util/List;->clear()V

    #@16
    .line 739
    iget-object v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mTransitions:Ljava/util/List;

    #@18
    invoke-interface {v2}, Ljava/util/List;->clear()V

    #@1b
    .line 740
    iget-object v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@1d
    invoke-virtual {v2}, Landroid/media/videoeditor/MediaArtistNativeHelper;->releaseNativeHelper()V

    #@20
    .line 741
    const/4 v2, 0x0

    #@21
    iput-object v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;
    :try_end_23
    .catchall {:try_start_4 .. :try_end_23} :catchall_40
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_23} :catch_32

    #@23
    .line 746
    :cond_23
    if-eqz v1, :cond_28

    #@25
    .line 747
    invoke-direct {p0}, Landroid/media/videoeditor/VideoEditorImpl;->unlock()V

    #@28
    .line 750
    :cond_28
    :goto_28
    iget-boolean v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMallocDebug:Z

    #@2a
    if-eqz v2, :cond_31

    #@2c
    .line 752
    :try_start_2c
    const-string v2, "HeapAtEnd"

    #@2e
    invoke-static {v2}, Landroid/media/videoeditor/VideoEditorImpl;->dumpHeap(Ljava/lang/String;)V
    :try_end_31
    .catch Ljava/lang/Exception; {:try_start_2c .. :try_end_31} :catch_47

    #@31
    .line 757
    :cond_31
    :goto_31
    return-void

    #@32
    .line 743
    :catch_32
    move-exception v0

    #@33
    .line 744
    .local v0, ex:Ljava/lang/Exception;
    :try_start_33
    const-string v2, "VideoEditorImpl"

    #@35
    const-string v3, "Sem acquire NOT successful in export"

    #@37
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3a
    .catchall {:try_start_33 .. :try_end_3a} :catchall_40

    #@3a
    .line 746
    if-eqz v1, :cond_28

    #@3c
    .line 747
    invoke-direct {p0}, Landroid/media/videoeditor/VideoEditorImpl;->unlock()V

    #@3f
    goto :goto_28

    #@40
    .line 746
    .end local v0           #ex:Ljava/lang/Exception;
    :catchall_40
    move-exception v2

    #@41
    if-eqz v1, :cond_46

    #@43
    .line 747
    invoke-direct {p0}, Landroid/media/videoeditor/VideoEditorImpl;->unlock()V

    #@46
    :cond_46
    throw v2

    #@47
    .line 753
    :catch_47
    move-exception v0

    #@48
    .line 754
    .restart local v0       #ex:Ljava/lang/Exception;
    const-string v2, "VideoEditorImpl"

    #@4a
    const-string v3, "dumpHeap returned error in release"

    #@4c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    goto :goto_31
.end method

.method public declared-synchronized removeAllMediaItems()V
    .registers 6

    #@0
    .prologue
    .line 763
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@3
    const/4 v3, 0x1

    #@4
    invoke-virtual {v2, v3}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@7
    .line 765
    iget-object v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@9
    invoke-interface {v2}, Ljava/util/List;->clear()V

    #@c
    .line 770
    iget-object v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mTransitions:Ljava/util/List;

    #@e
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@11
    move-result-object v0

    #@12
    .local v0, i$:Ljava/util/Iterator;
    :goto_12
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@15
    move-result v2

    #@16
    if-eqz v2, :cond_25

    #@18
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1b
    move-result-object v1

    #@1c
    check-cast v1, Landroid/media/videoeditor/Transition;

    #@1e
    .line 771
    .local v1, transition:Landroid/media/videoeditor/Transition;
    invoke-virtual {v1}, Landroid/media/videoeditor/Transition;->invalidate()V
    :try_end_21
    .catchall {:try_start_1 .. :try_end_21} :catchall_22

    #@21
    goto :goto_12

    #@22
    .line 763
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #transition:Landroid/media/videoeditor/Transition;
    :catchall_22
    move-exception v2

    #@23
    monitor-exit p0

    #@24
    throw v2

    #@25
    .line 773
    .restart local v0       #i$:Ljava/util/Iterator;
    :cond_25
    :try_start_25
    iget-object v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mTransitions:Ljava/util/List;

    #@27
    invoke-interface {v2}, Ljava/util/List;->clear()V

    #@2a
    .line 775
    const-wide/16 v2, 0x0

    #@2c
    iput-wide v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mDurationMs:J

    #@2e
    .line 779
    new-instance v2, Ljava/io/File;

    #@30
    new-instance v3, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    iget-object v4, p0, Landroid/media/videoeditor/VideoEditorImpl;->mProjectPath:Ljava/lang/String;

    #@37
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    const-string v4, "/"

    #@3d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v3

    #@41
    const-string/jumbo v4, "thumbnail.jpg"

    #@44
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v3

    #@48
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v3

    #@4c
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@4f
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    #@52
    move-result v2

    #@53
    if-eqz v2, :cond_79

    #@55
    .line 780
    new-instance v2, Ljava/io/File;

    #@57
    new-instance v3, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    iget-object v4, p0, Landroid/media/videoeditor/VideoEditorImpl;->mProjectPath:Ljava/lang/String;

    #@5e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v3

    #@62
    const-string v4, "/"

    #@64
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v3

    #@68
    const-string/jumbo v4, "thumbnail.jpg"

    #@6b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v3

    #@6f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v3

    #@73
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@76
    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_79
    .catchall {:try_start_25 .. :try_end_79} :catchall_22

    #@79
    .line 783
    :cond_79
    monitor-exit p0

    #@7a
    return-void
.end method

.method public declared-synchronized removeAudioTrack(Ljava/lang/String;)Landroid/media/videoeditor/AudioTrack;
    .registers 5
    .parameter "audioTrackId"

    #@0
    .prologue
    .line 789
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p0, p1}, Landroid/media/videoeditor/VideoEditorImpl;->getAudioTrack(Ljava/lang/String;)Landroid/media/videoeditor/AudioTrack;

    #@4
    move-result-object v0

    #@5
    .line 790
    .local v0, audioTrack:Landroid/media/videoeditor/AudioTrack;
    if-eqz v0, :cond_22

    #@7
    .line 791
    iget-object v1, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@9
    const/4 v2, 0x1

    #@a
    invoke-virtual {v1, v2}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@d
    .line 792
    iget-object v1, p0, Landroid/media/videoeditor/VideoEditorImpl;->mAudioTracks:Ljava/util/List;

    #@f
    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@12
    .line 793
    invoke-virtual {v0}, Landroid/media/videoeditor/AudioTrack;->invalidate()V

    #@15
    .line 794
    iget-object v1, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@17
    invoke-virtual {v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->invalidatePcmFile()V

    #@1a
    .line 795
    iget-object v1, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@1c
    const/4 v2, 0x1

    #@1d
    invoke-virtual {v1, v2}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setAudioflag(Z)V
    :try_end_20
    .catchall {:try_start_1 .. :try_end_20} :catchall_2a

    #@20
    .line 799
    monitor-exit p0

    #@21
    return-object v0

    #@22
    .line 797
    :cond_22
    :try_start_22
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@24
    const-string v2, " No more audio tracks"

    #@26
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@29
    throw v1
    :try_end_2a
    .catchall {:try_start_22 .. :try_end_2a} :catchall_2a

    #@2a
    .line 789
    .end local v0           #audioTrack:Landroid/media/videoeditor/AudioTrack;
    :catchall_2a
    move-exception v1

    #@2b
    monitor-exit p0

    #@2c
    throw v1
.end method

.method public declared-synchronized removeMediaItem(Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;
    .registers 11
    .parameter "mediaItemId"

    #@0
    .prologue
    .line 806
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v7, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@3
    const/4 v8, 0x0

    #@4
    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@7
    move-result-object v7

    #@8
    check-cast v7, Landroid/media/videoeditor/MediaItem;

    #@a
    invoke-virtual {v7}, Landroid/media/videoeditor/MediaItem;->getId()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    .line 807
    .local v1, firstItemString:Ljava/lang/String;
    invoke-virtual {p0, p1}, Landroid/media/videoeditor/VideoEditorImpl;->getMediaItem(Ljava/lang/String;)Landroid/media/videoeditor/MediaItem;

    #@11
    move-result-object v3

    #@12
    .line 808
    .local v3, mediaItem:Landroid/media/videoeditor/MediaItem;
    if-eqz v3, :cond_59

    #@14
    .line 809
    iget-object v7, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@16
    const/4 v8, 0x1

    #@17
    invoke-virtual {v7, v8}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@1a
    .line 813
    iget-object v7, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@1c
    invoke-interface {v7, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@1f
    .line 814
    instance-of v7, v3, Landroid/media/videoeditor/MediaImageItem;

    #@21
    if-eqz v7, :cond_2a

    #@23
    .line 815
    move-object v0, v3

    #@24
    check-cast v0, Landroid/media/videoeditor/MediaImageItem;

    #@26
    move-object v7, v0

    #@27
    invoke-virtual {v7}, Landroid/media/videoeditor/MediaImageItem;->invalidate()V

    #@2a
    .line 817
    :cond_2a
    invoke-virtual {v3}, Landroid/media/videoeditor/MediaItem;->getAllOverlays()Ljava/util/List;

    #@2d
    move-result-object v6

    #@2e
    .line 818
    .local v6, overlays:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Overlay;>;"
    invoke-interface {v6}, Ljava/util/List;->size()I

    #@31
    move-result v7

    #@32
    if-lez v7, :cond_53

    #@34
    .line 819
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@37
    move-result-object v2

    #@38
    .local v2, i$:Ljava/util/Iterator;
    :cond_38
    :goto_38
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@3b
    move-result v7

    #@3c
    if-eqz v7, :cond_53

    #@3e
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@41
    move-result-object v4

    #@42
    check-cast v4, Landroid/media/videoeditor/Overlay;

    #@44
    .line 820
    .local v4, overlay:Landroid/media/videoeditor/Overlay;
    instance-of v7, v4, Landroid/media/videoeditor/OverlayFrame;

    #@46
    if-eqz v7, :cond_38

    #@48
    .line 821
    move-object v0, v4

    #@49
    check-cast v0, Landroid/media/videoeditor/OverlayFrame;

    #@4b
    move-object v5, v0

    #@4c
    .line 822
    .local v5, overlayFrame:Landroid/media/videoeditor/OverlayFrame;
    invoke-virtual {v5}, Landroid/media/videoeditor/OverlayFrame;->invalidate()V
    :try_end_4f
    .catchall {:try_start_1 .. :try_end_4f} :catchall_50

    #@4f
    goto :goto_38

    #@50
    .line 806
    .end local v1           #firstItemString:Ljava/lang/String;
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #mediaItem:Landroid/media/videoeditor/MediaItem;
    .end local v4           #overlay:Landroid/media/videoeditor/Overlay;
    .end local v5           #overlayFrame:Landroid/media/videoeditor/OverlayFrame;
    .end local v6           #overlays:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Overlay;>;"
    :catchall_50
    move-exception v7

    #@51
    monitor-exit p0

    #@52
    throw v7

    #@53
    .line 830
    .restart local v1       #firstItemString:Ljava/lang/String;
    .restart local v3       #mediaItem:Landroid/media/videoeditor/MediaItem;
    .restart local v6       #overlays:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Overlay;>;"
    :cond_53
    :try_start_53
    invoke-direct {p0, v3}, Landroid/media/videoeditor/VideoEditorImpl;->removeAdjacentTransitions(Landroid/media/videoeditor/MediaItem;)V

    #@56
    .line 831
    invoke-direct {p0}, Landroid/media/videoeditor/VideoEditorImpl;->computeTimelineDuration()V

    #@59
    .line 838
    .end local v6           #overlays:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Overlay;>;"
    :cond_59
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5c
    move-result v7

    #@5d
    if-eqz v7, :cond_62

    #@5f
    .line 839
    invoke-direct {p0}, Landroid/media/videoeditor/VideoEditorImpl;->generateProjectThumbnail()V

    #@62
    .line 842
    :cond_62
    instance-of v7, v3, Landroid/media/videoeditor/MediaVideoItem;

    #@64
    if-eqz v7, :cond_6d

    #@66
    .line 846
    move-object v0, v3

    #@67
    check-cast v0, Landroid/media/videoeditor/MediaVideoItem;

    #@69
    move-object v7, v0

    #@6a
    invoke-virtual {v7}, Landroid/media/videoeditor/MediaVideoItem;->invalidate()V
    :try_end_6d
    .catchall {:try_start_53 .. :try_end_6d} :catchall_50

    #@6d
    .line 848
    :cond_6d
    monitor-exit p0

    #@6e
    return-object v3
.end method

.method public declared-synchronized removeTransition(Ljava/lang/String;)Landroid/media/videoeditor/Transition;
    .registers 8
    .parameter "transitionId"

    #@0
    .prologue
    .line 882
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p0, p1}, Landroid/media/videoeditor/VideoEditorImpl;->getTransition(Ljava/lang/String;)Landroid/media/videoeditor/Transition;

    #@4
    move-result-object v2

    #@5
    .line 883
    .local v2, transition:Landroid/media/videoeditor/Transition;
    if-nez v2, :cond_23

    #@7
    .line 884
    new-instance v3, Ljava/lang/IllegalStateException;

    #@9
    new-instance v4, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v5, "Transition not found: "

    #@10
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v4

    #@14
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v4

    #@1c
    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v3
    :try_end_20
    .catchall {:try_start_1 .. :try_end_20} :catchall_20

    #@20
    .line 882
    .end local v2           #transition:Landroid/media/videoeditor/Transition;
    :catchall_20
    move-exception v3

    #@21
    monitor-exit p0

    #@22
    throw v3

    #@23
    .line 887
    .restart local v2       #transition:Landroid/media/videoeditor/Transition;
    :cond_23
    :try_start_23
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@25
    const/4 v4, 0x1

    #@26
    invoke-virtual {v3, v4}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@29
    .line 892
    invoke-virtual {v2}, Landroid/media/videoeditor/Transition;->getAfterMediaItem()Landroid/media/videoeditor/MediaItem;

    #@2c
    move-result-object v0

    #@2d
    .line 893
    .local v0, afterMediaItem:Landroid/media/videoeditor/MediaItem;
    if-eqz v0, :cond_33

    #@2f
    .line 894
    const/4 v3, 0x0

    #@30
    invoke-virtual {v0, v3}, Landroid/media/videoeditor/MediaItem;->setEndTransition(Landroid/media/videoeditor/Transition;)V

    #@33
    .line 897
    :cond_33
    invoke-virtual {v2}, Landroid/media/videoeditor/Transition;->getBeforeMediaItem()Landroid/media/videoeditor/MediaItem;

    #@36
    move-result-object v1

    #@37
    .line 898
    .local v1, beforeMediaItem:Landroid/media/videoeditor/MediaItem;
    if-eqz v1, :cond_3d

    #@39
    .line 899
    const/4 v3, 0x0

    #@3a
    invoke-virtual {v1, v3}, Landroid/media/videoeditor/MediaItem;->setBeginTransition(Landroid/media/videoeditor/Transition;)V

    #@3d
    .line 902
    :cond_3d
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mTransitions:Ljava/util/List;

    #@3f
    invoke-interface {v3, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@42
    .line 903
    invoke-virtual {v2}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@45
    .line 904
    invoke-direct {p0}, Landroid/media/videoeditor/VideoEditorImpl;->computeTimelineDuration()V
    :try_end_48
    .catchall {:try_start_23 .. :try_end_48} :catchall_20

    #@48
    .line 905
    monitor-exit p0

    #@49
    return-object v2
.end method

.method public renderPreviewFrame(Landroid/view/SurfaceHolder;JLandroid/media/videoeditor/VideoEditor$OverlayData;)J
    .registers 17
    .parameter "surfaceHolder"
    .parameter "timeMs"
    .parameter "overlayData"

    #@0
    .prologue
    .line 913
    if-nez p1, :cond_a

    #@2
    .line 914
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v2, "Surface Holder is null"

    #@6
    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 917
    :cond_a
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    #@d
    move-result-object v1

    #@e
    .line 918
    .local v1, surface:Landroid/view/Surface;
    if-nez v1, :cond_18

    #@10
    .line 919
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@12
    const-string v2, "Surface could not be retrieved from Surface holder"

    #@14
    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 922
    :cond_18
    invoke-virtual {v1}, Landroid/view/Surface;->isValid()Z

    #@1b
    move-result v0

    #@1c
    if-nez v0, :cond_26

    #@1e
    .line 923
    new-instance v0, Ljava/lang/IllegalStateException;

    #@20
    const-string v2, "Surface is not valid"

    #@22
    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@25
    throw v0

    #@26
    .line 926
    :cond_26
    const-wide/16 v2, 0x0

    #@28
    cmp-long v0, p2, v2

    #@2a
    if-gez v0, :cond_35

    #@2c
    .line 927
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@2e
    const-string/jumbo v2, "requested time not correct"

    #@31
    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@34
    throw v0

    #@35
    .line 928
    :cond_35
    iget-wide v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mDurationMs:J

    #@37
    cmp-long v0, p2, v2

    #@39
    if-lez v0, :cond_44

    #@3b
    .line 929
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@3d
    const-string/jumbo v2, "requested time more than duration"

    #@40
    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@43
    throw v0

    #@44
    .line 931
    :cond_44
    const-wide/16 v9, 0x0

    #@46
    .line 933
    .local v9, result:J
    const/4 v11, 0x0

    #@47
    .line 935
    .local v11, semAcquireDone:Z
    const-wide/16 v2, 0x1f4

    #@49
    :try_start_49
    invoke-direct {p0, v2, v3}, Landroid/media/videoeditor/VideoEditorImpl;->lock(J)Z

    #@4c
    move-result v11

    #@4d
    .line 936
    if-nez v11, :cond_73

    #@4f
    .line 937
    new-instance v0, Ljava/lang/IllegalStateException;

    #@51
    const-string v2, "Timeout waiting for semaphore"

    #@53
    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@56
    throw v0
    :try_end_57
    .catchall {:try_start_49 .. :try_end_57} :catchall_6c
    .catch Ljava/lang/InterruptedException; {:try_start_49 .. :try_end_57} :catch_57

    #@57
    .line 951
    :catch_57
    move-exception v7

    #@58
    .line 952
    .local v7, ex:Ljava/lang/InterruptedException;
    :try_start_58
    const-string v0, "VideoEditorImpl"

    #@5a
    const-string v2, "The thread was interrupted"

    #@5c
    new-instance v3, Ljava/lang/Throwable;

    #@5e
    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    #@61
    invoke-static {v0, v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@64
    .line 953
    new-instance v0, Ljava/lang/IllegalStateException;

    #@66
    const-string v2, "The thread was interrupted"

    #@68
    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@6b
    throw v0
    :try_end_6c
    .catchall {:try_start_58 .. :try_end_6c} :catchall_6c

    #@6c
    .line 955
    .end local v7           #ex:Ljava/lang/InterruptedException;
    :catchall_6c
    move-exception v0

    #@6d
    if-eqz v11, :cond_72

    #@6f
    .line 956
    invoke-direct {p0}, Landroid/media/videoeditor/VideoEditorImpl;->unlock()V

    #@72
    :cond_72
    throw v0

    #@73
    .line 940
    :cond_73
    :try_start_73
    iget-object v0, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@75
    if-nez v0, :cond_7f

    #@77
    .line 941
    new-instance v0, Ljava/lang/IllegalStateException;

    #@79
    const-string v2, "The video editor is not initialized"

    #@7b
    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@7e
    throw v0

    #@7f
    .line 944
    :cond_7f
    iget-object v0, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@81
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@84
    move-result v0

    #@85
    if-lez v0, :cond_a2

    #@87
    .line 945
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurfaceFrame()Landroid/graphics/Rect;

    #@8a
    move-result-object v8

    #@8b
    .line 946
    .local v8, frame:Landroid/graphics/Rect;
    iget-object v0, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@8d
    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    #@90
    move-result v4

    #@91
    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    #@94
    move-result v5

    #@95
    move-wide v2, p2

    #@96
    move-object/from16 v6, p4

    #@98
    invoke-virtual/range {v0 .. v6}, Landroid/media/videoeditor/MediaArtistNativeHelper;->renderPreviewFrame(Landroid/view/Surface;JIILandroid/media/videoeditor/VideoEditor$OverlayData;)J
    :try_end_9b
    .catchall {:try_start_73 .. :try_end_9b} :catchall_6c
    .catch Ljava/lang/InterruptedException; {:try_start_73 .. :try_end_9b} :catch_57

    #@9b
    move-result-wide v9

    #@9c
    .line 955
    .end local v8           #frame:Landroid/graphics/Rect;
    :goto_9c
    if-eqz v11, :cond_a1

    #@9e
    .line 956
    invoke-direct {p0}, Landroid/media/videoeditor/VideoEditorImpl;->unlock()V

    #@a1
    .line 959
    :cond_a1
    return-wide v9

    #@a2
    .line 949
    :cond_a2
    const-wide/16 v9, 0x0

    #@a4
    goto :goto_9c
.end method

.method public save()V
    .registers 34
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 1372
    invoke-static {}, Landroid/util/Xml;->newSerializer()Lorg/xmlpull/v1/XmlSerializer;

    #@3
    move-result-object v22

    #@4
    .line 1373
    .local v22, serializer:Lorg/xmlpull/v1/XmlSerializer;
    new-instance v28, Ljava/io/StringWriter;

    #@6
    invoke-direct/range {v28 .. v28}, Ljava/io/StringWriter;-><init>()V

    #@9
    .line 1374
    .local v28, writer:Ljava/io/StringWriter;
    move-object/from16 v0, v22

    #@b
    move-object/from16 v1, v28

    #@d
    invoke-interface {v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/Writer;)V

    #@10
    .line 1375
    const-string v29, "UTF-8"

    #@12
    const/16 v30, 0x1

    #@14
    invoke-static/range {v30 .. v30}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@17
    move-result-object v30

    #@18
    move-object/from16 v0, v22

    #@1a
    move-object/from16 v1, v29

    #@1c
    move-object/from16 v2, v30

    #@1e
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@21
    .line 1376
    const-string v29, ""

    #@23
    const-string/jumbo v30, "project"

    #@26
    move-object/from16 v0, v22

    #@28
    move-object/from16 v1, v29

    #@2a
    move-object/from16 v2, v30

    #@2c
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@2f
    .line 1377
    const-string v29, ""

    #@31
    const-string v30, "aspect_ratio"

    #@33
    move-object/from16 v0, p0

    #@35
    iget v0, v0, Landroid/media/videoeditor/VideoEditorImpl;->mAspectRatio:I

    #@37
    move/from16 v31, v0

    #@39
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@3c
    move-result-object v31

    #@3d
    move-object/from16 v0, v22

    #@3f
    move-object/from16 v1, v29

    #@41
    move-object/from16 v2, v30

    #@43
    move-object/from16 v3, v31

    #@45
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@48
    .line 1380
    const-string v29, ""

    #@4a
    const-string/jumbo v30, "regeneratePCMFlag"

    #@4d
    move-object/from16 v0, p0

    #@4f
    iget-object v0, v0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@51
    move-object/from16 v31, v0

    #@53
    invoke-virtual/range {v31 .. v31}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getAudioflag()Z

    #@56
    move-result v31

    #@57
    invoke-static/range {v31 .. v31}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    #@5a
    move-result-object v31

    #@5b
    move-object/from16 v0, v22

    #@5d
    move-object/from16 v1, v29

    #@5f
    move-object/from16 v2, v30

    #@61
    move-object/from16 v3, v31

    #@63
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@66
    .line 1383
    const-string v29, ""

    #@68
    const-string/jumbo v30, "media_items"

    #@6b
    move-object/from16 v0, v22

    #@6d
    move-object/from16 v1, v29

    #@6f
    move-object/from16 v2, v30

    #@71
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@74
    .line 1384
    move-object/from16 v0, p0

    #@76
    iget-object v0, v0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@78
    move-object/from16 v29, v0

    #@7a
    invoke-interface/range {v29 .. v29}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@7d
    move-result-object v11

    #@7e
    :goto_7e
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    #@81
    move-result v29

    #@82
    if-eqz v29, :cond_569

    #@84
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@87
    move-result-object v15

    #@88
    check-cast v15, Landroid/media/videoeditor/MediaItem;

    #@8a
    .line 1385
    .local v15, mediaItem:Landroid/media/videoeditor/MediaItem;
    const-string v29, ""

    #@8c
    const-string/jumbo v30, "media_item"

    #@8f
    move-object/from16 v0, v22

    #@91
    move-object/from16 v1, v29

    #@93
    move-object/from16 v2, v30

    #@95
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@98
    .line 1386
    const-string v29, ""

    #@9a
    const-string v30, "id"

    #@9c
    invoke-virtual {v15}, Landroid/media/videoeditor/MediaItem;->getId()Ljava/lang/String;

    #@9f
    move-result-object v31

    #@a0
    move-object/from16 v0, v22

    #@a2
    move-object/from16 v1, v29

    #@a4
    move-object/from16 v2, v30

    #@a6
    move-object/from16 v3, v31

    #@a8
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@ab
    .line 1387
    const-string v29, ""

    #@ad
    const-string/jumbo v30, "type"

    #@b0
    invoke-virtual {v15}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@b3
    move-result-object v31

    #@b4
    invoke-virtual/range {v31 .. v31}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@b7
    move-result-object v31

    #@b8
    move-object/from16 v0, v22

    #@ba
    move-object/from16 v1, v29

    #@bc
    move-object/from16 v2, v30

    #@be
    move-object/from16 v3, v31

    #@c0
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@c3
    .line 1389
    const-string v29, ""

    #@c5
    const-string v30, "filename"

    #@c7
    invoke-virtual {v15}, Landroid/media/videoeditor/MediaItem;->getFilename()Ljava/lang/String;

    #@ca
    move-result-object v31

    #@cb
    move-object/from16 v0, v22

    #@cd
    move-object/from16 v1, v29

    #@cf
    move-object/from16 v2, v30

    #@d1
    move-object/from16 v3, v31

    #@d3
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@d6
    .line 1390
    const-string v29, ""

    #@d8
    const-string/jumbo v30, "rendering_mode"

    #@db
    invoke-virtual {v15}, Landroid/media/videoeditor/MediaItem;->getRenderingMode()I

    #@de
    move-result v31

    #@df
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@e2
    move-result-object v31

    #@e3
    move-object/from16 v0, v22

    #@e5
    move-object/from16 v1, v29

    #@e7
    move-object/from16 v2, v30

    #@e9
    move-object/from16 v3, v31

    #@eb
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@ee
    .line 1392
    instance-of v0, v15, Landroid/media/videoeditor/MediaVideoItem;

    #@f0
    move/from16 v29, v0

    #@f2
    if-eqz v29, :cond_2e9

    #@f4
    move-object/from16 v16, v15

    #@f6
    .line 1393
    check-cast v16, Landroid/media/videoeditor/MediaVideoItem;

    #@f8
    .line 1394
    .local v16, mvi:Landroid/media/videoeditor/MediaVideoItem;
    const-string v29, ""

    #@fa
    const-string v30, "begin_time"

    #@fc
    invoke-virtual/range {v16 .. v16}, Landroid/media/videoeditor/MediaVideoItem;->getBoundaryBeginTime()J

    #@ff
    move-result-wide v31

    #@100
    invoke-static/range {v31 .. v32}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@103
    move-result-object v31

    #@104
    move-object/from16 v0, v22

    #@106
    move-object/from16 v1, v29

    #@108
    move-object/from16 v2, v30

    #@10a
    move-object/from16 v3, v31

    #@10c
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@10f
    .line 1397
    const-string v29, ""

    #@111
    const-string v30, "end_time"

    #@113
    invoke-virtual/range {v16 .. v16}, Landroid/media/videoeditor/MediaVideoItem;->getBoundaryEndTime()J

    #@116
    move-result-wide v31

    #@117
    invoke-static/range {v31 .. v32}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@11a
    move-result-object v31

    #@11b
    move-object/from16 v0, v22

    #@11d
    move-object/from16 v1, v29

    #@11f
    move-object/from16 v2, v30

    #@121
    move-object/from16 v3, v31

    #@123
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@126
    .line 1399
    const-string v29, ""

    #@128
    const-string/jumbo v30, "volume"

    #@12b
    invoke-virtual/range {v16 .. v16}, Landroid/media/videoeditor/MediaVideoItem;->getVolume()I

    #@12e
    move-result v31

    #@12f
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@132
    move-result-object v31

    #@133
    move-object/from16 v0, v22

    #@135
    move-object/from16 v1, v29

    #@137
    move-object/from16 v2, v30

    #@139
    move-object/from16 v3, v31

    #@13b
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@13e
    .line 1401
    const-string v29, ""

    #@140
    const-string/jumbo v30, "muted"

    #@143
    invoke-virtual/range {v16 .. v16}, Landroid/media/videoeditor/MediaVideoItem;->isMuted()Z

    #@146
    move-result v31

    #@147
    invoke-static/range {v31 .. v31}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    #@14a
    move-result-object v31

    #@14b
    move-object/from16 v0, v22

    #@14d
    move-object/from16 v1, v29

    #@14f
    move-object/from16 v2, v30

    #@151
    move-object/from16 v3, v31

    #@153
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@156
    .line 1403
    invoke-virtual/range {v16 .. v16}, Landroid/media/videoeditor/MediaVideoItem;->getAudioWaveformFilename()Ljava/lang/String;

    #@159
    move-result-object v29

    #@15a
    if-eqz v29, :cond_170

    #@15c
    .line 1404
    const-string v29, ""

    #@15e
    const-string/jumbo v30, "waveform"

    #@161
    invoke-virtual/range {v16 .. v16}, Landroid/media/videoeditor/MediaVideoItem;->getAudioWaveformFilename()Ljava/lang/String;

    #@164
    move-result-object v31

    #@165
    move-object/from16 v0, v22

    #@167
    move-object/from16 v1, v29

    #@169
    move-object/from16 v2, v30

    #@16b
    move-object/from16 v3, v31

    #@16d
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@170
    .line 1412
    .end local v16           #mvi:Landroid/media/videoeditor/MediaVideoItem;
    :cond_170
    :goto_170
    invoke-virtual {v15}, Landroid/media/videoeditor/MediaItem;->getAllOverlays()Ljava/util/List;

    #@173
    move-result-object v21

    #@174
    .line 1413
    .local v21, overlays:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Overlay;>;"
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    #@177
    move-result v29

    #@178
    if-lez v29, :cond_334

    #@17a
    .line 1414
    const-string v29, ""

    #@17c
    const-string/jumbo v30, "overlays"

    #@17f
    move-object/from16 v0, v22

    #@181
    move-object/from16 v1, v29

    #@183
    move-object/from16 v2, v30

    #@185
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@188
    .line 1415
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@18b
    move-result-object v12

    #@18c
    :goto_18c
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    #@18f
    move-result v29

    #@190
    if-eqz v29, :cond_326

    #@192
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@195
    move-result-object v19

    #@196
    check-cast v19, Landroid/media/videoeditor/Overlay;

    #@198
    .line 1416
    .local v19, overlay:Landroid/media/videoeditor/Overlay;
    const-string v29, ""

    #@19a
    const-string/jumbo v30, "overlay"

    #@19d
    move-object/from16 v0, v22

    #@19f
    move-object/from16 v1, v29

    #@1a1
    move-object/from16 v2, v30

    #@1a3
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@1a6
    .line 1417
    const-string v29, ""

    #@1a8
    const-string v30, "id"

    #@1aa
    invoke-virtual/range {v19 .. v19}, Landroid/media/videoeditor/Overlay;->getId()Ljava/lang/String;

    #@1ad
    move-result-object v31

    #@1ae
    move-object/from16 v0, v22

    #@1b0
    move-object/from16 v1, v29

    #@1b2
    move-object/from16 v2, v30

    #@1b4
    move-object/from16 v3, v31

    #@1b6
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@1b9
    .line 1418
    const-string v29, ""

    #@1bb
    const-string/jumbo v30, "type"

    #@1be
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@1c1
    move-result-object v31

    #@1c2
    invoke-virtual/range {v31 .. v31}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@1c5
    move-result-object v31

    #@1c6
    move-object/from16 v0, v22

    #@1c8
    move-object/from16 v1, v29

    #@1ca
    move-object/from16 v2, v30

    #@1cc
    move-object/from16 v3, v31

    #@1ce
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@1d1
    .line 1420
    const-string v29, ""

    #@1d3
    const-string v30, "begin_time"

    #@1d5
    invoke-virtual/range {v19 .. v19}, Landroid/media/videoeditor/Overlay;->getStartTime()J

    #@1d8
    move-result-wide v31

    #@1d9
    invoke-static/range {v31 .. v32}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@1dc
    move-result-object v31

    #@1dd
    move-object/from16 v0, v22

    #@1df
    move-object/from16 v1, v29

    #@1e1
    move-object/from16 v2, v30

    #@1e3
    move-object/from16 v3, v31

    #@1e5
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@1e8
    .line 1422
    const-string v29, ""

    #@1ea
    const-string v30, "duration"

    #@1ec
    invoke-virtual/range {v19 .. v19}, Landroid/media/videoeditor/Overlay;->getDuration()J

    #@1ef
    move-result-wide v31

    #@1f0
    invoke-static/range {v31 .. v32}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@1f3
    move-result-object v31

    #@1f4
    move-object/from16 v0, v22

    #@1f6
    move-object/from16 v1, v29

    #@1f8
    move-object/from16 v2, v30

    #@1fa
    move-object/from16 v3, v31

    #@1fc
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@1ff
    .line 1424
    move-object/from16 v0, v19

    #@201
    instance-of v0, v0, Landroid/media/videoeditor/OverlayFrame;

    #@203
    move/from16 v29, v0

    #@205
    if-eqz v29, :cond_2a9

    #@207
    move-object/from16 v20, v19

    #@209
    .line 1425
    check-cast v20, Landroid/media/videoeditor/OverlayFrame;

    #@20b
    .line 1426
    .local v20, overlayFrame:Landroid/media/videoeditor/OverlayFrame;
    invoke-virtual/range {p0 .. p0}, Landroid/media/videoeditor/VideoEditorImpl;->getPath()Ljava/lang/String;

    #@20e
    move-result-object v29

    #@20f
    move-object/from16 v0, v20

    #@211
    move-object/from16 v1, v29

    #@213
    invoke-virtual {v0, v1}, Landroid/media/videoeditor/OverlayFrame;->save(Ljava/lang/String;)Ljava/lang/String;

    #@216
    .line 1427
    invoke-virtual/range {v20 .. v20}, Landroid/media/videoeditor/OverlayFrame;->getBitmapImageFileName()Ljava/lang/String;

    #@219
    move-result-object v29

    #@21a
    if-eqz v29, :cond_22f

    #@21c
    .line 1428
    const-string v29, ""

    #@21e
    const-string v30, "filename"

    #@220
    invoke-virtual/range {v20 .. v20}, Landroid/media/videoeditor/OverlayFrame;->getBitmapImageFileName()Ljava/lang/String;

    #@223
    move-result-object v31

    #@224
    move-object/from16 v0, v22

    #@226
    move-object/from16 v1, v29

    #@228
    move-object/from16 v2, v30

    #@22a
    move-object/from16 v3, v31

    #@22c
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@22f
    .line 1432
    :cond_22f
    invoke-virtual/range {v20 .. v20}, Landroid/media/videoeditor/OverlayFrame;->getFilename()Ljava/lang/String;

    #@232
    move-result-object v29

    #@233
    if-eqz v29, :cond_2a9

    #@235
    .line 1433
    const-string v29, ""

    #@237
    const-string/jumbo v30, "overlay_rgb_filename"

    #@23a
    invoke-virtual/range {v20 .. v20}, Landroid/media/videoeditor/OverlayFrame;->getFilename()Ljava/lang/String;

    #@23d
    move-result-object v31

    #@23e
    move-object/from16 v0, v22

    #@240
    move-object/from16 v1, v29

    #@242
    move-object/from16 v2, v30

    #@244
    move-object/from16 v3, v31

    #@246
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@249
    .line 1436
    const-string v29, ""

    #@24b
    const-string/jumbo v30, "overlay_frame_width"

    #@24e
    invoke-virtual/range {v20 .. v20}, Landroid/media/videoeditor/OverlayFrame;->getOverlayFrameWidth()I

    #@251
    move-result v31

    #@252
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@255
    move-result-object v31

    #@256
    move-object/from16 v0, v22

    #@258
    move-object/from16 v1, v29

    #@25a
    move-object/from16 v2, v30

    #@25c
    move-object/from16 v3, v31

    #@25e
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@261
    .line 1438
    const-string v29, ""

    #@263
    const-string/jumbo v30, "overlay_frame_height"

    #@266
    invoke-virtual/range {v20 .. v20}, Landroid/media/videoeditor/OverlayFrame;->getOverlayFrameHeight()I

    #@269
    move-result v31

    #@26a
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@26d
    move-result-object v31

    #@26e
    move-object/from16 v0, v22

    #@270
    move-object/from16 v1, v29

    #@272
    move-object/from16 v2, v30

    #@274
    move-object/from16 v3, v31

    #@276
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@279
    .line 1440
    const-string v29, ""

    #@27b
    const-string/jumbo v30, "resized_RGBframe_width"

    #@27e
    invoke-virtual/range {v20 .. v20}, Landroid/media/videoeditor/OverlayFrame;->getResizedRGBSizeWidth()I

    #@281
    move-result v31

    #@282
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@285
    move-result-object v31

    #@286
    move-object/from16 v0, v22

    #@288
    move-object/from16 v1, v29

    #@28a
    move-object/from16 v2, v30

    #@28c
    move-object/from16 v3, v31

    #@28e
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@291
    .line 1442
    const-string v29, ""

    #@293
    const-string/jumbo v30, "resized_RGBframe_height"

    #@296
    invoke-virtual/range {v20 .. v20}, Landroid/media/videoeditor/OverlayFrame;->getResizedRGBSizeHeight()I

    #@299
    move-result v31

    #@29a
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@29d
    move-result-object v31

    #@29e
    move-object/from16 v0, v22

    #@2a0
    move-object/from16 v1, v29

    #@2a2
    move-object/from16 v2, v30

    #@2a4
    move-object/from16 v3, v31

    #@2a6
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@2a9
    .line 1452
    .end local v20           #overlayFrame:Landroid/media/videoeditor/OverlayFrame;
    :cond_2a9
    const-string v29, ""

    #@2ab
    const-string/jumbo v30, "overlay_user_attributes"

    #@2ae
    move-object/from16 v0, v22

    #@2b0
    move-object/from16 v1, v29

    #@2b2
    move-object/from16 v2, v30

    #@2b4
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@2b7
    .line 1453
    invoke-virtual/range {v19 .. v19}, Landroid/media/videoeditor/Overlay;->getUserAttributes()Ljava/util/Map;

    #@2ba
    move-result-object v26

    #@2bb
    .line 1454
    .local v26, userAttributes:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface/range {v26 .. v26}, Ljava/util/Map;->keySet()Ljava/util/Set;

    #@2be
    move-result-object v29

    #@2bf
    invoke-interface/range {v29 .. v29}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@2c2
    move-result-object v13

    #@2c3
    .local v13, i$:Ljava/util/Iterator;
    :cond_2c3
    :goto_2c3
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    #@2c6
    move-result v29

    #@2c7
    if-eqz v29, :cond_308

    #@2c9
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2cc
    move-result-object v17

    #@2cd
    check-cast v17, Ljava/lang/String;

    #@2cf
    .line 1455
    .local v17, name:Ljava/lang/String;
    move-object/from16 v0, v26

    #@2d1
    move-object/from16 v1, v17

    #@2d3
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2d6
    move-result-object v27

    #@2d7
    check-cast v27, Ljava/lang/String;

    #@2d9
    .line 1456
    .local v27, value:Ljava/lang/String;
    if-eqz v27, :cond_2c3

    #@2db
    .line 1457
    const-string v29, ""

    #@2dd
    move-object/from16 v0, v22

    #@2df
    move-object/from16 v1, v29

    #@2e1
    move-object/from16 v2, v17

    #@2e3
    move-object/from16 v3, v27

    #@2e5
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@2e8
    goto :goto_2c3

    #@2e9
    .line 1407
    .end local v13           #i$:Ljava/util/Iterator;
    .end local v17           #name:Ljava/lang/String;
    .end local v19           #overlay:Landroid/media/videoeditor/Overlay;
    .end local v21           #overlays:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Overlay;>;"
    .end local v26           #userAttributes:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v27           #value:Ljava/lang/String;
    :cond_2e9
    instance-of v0, v15, Landroid/media/videoeditor/MediaImageItem;

    #@2eb
    move/from16 v29, v0

    #@2ed
    if-eqz v29, :cond_170

    #@2ef
    .line 1408
    const-string v29, ""

    #@2f1
    const-string v30, "duration"

    #@2f3
    invoke-virtual {v15}, Landroid/media/videoeditor/MediaItem;->getTimelineDuration()J

    #@2f6
    move-result-wide v31

    #@2f7
    invoke-static/range {v31 .. v32}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@2fa
    move-result-object v31

    #@2fb
    move-object/from16 v0, v22

    #@2fd
    move-object/from16 v1, v29

    #@2ff
    move-object/from16 v2, v30

    #@301
    move-object/from16 v3, v31

    #@303
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@306
    goto/16 :goto_170

    #@308
    .line 1460
    .restart local v13       #i$:Ljava/util/Iterator;
    .restart local v19       #overlay:Landroid/media/videoeditor/Overlay;
    .restart local v21       #overlays:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Overlay;>;"
    .restart local v26       #userAttributes:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_308
    const-string v29, ""

    #@30a
    const-string/jumbo v30, "overlay_user_attributes"

    #@30d
    move-object/from16 v0, v22

    #@30f
    move-object/from16 v1, v29

    #@311
    move-object/from16 v2, v30

    #@313
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@316
    .line 1462
    const-string v29, ""

    #@318
    const-string/jumbo v30, "overlay"

    #@31b
    move-object/from16 v0, v22

    #@31d
    move-object/from16 v1, v29

    #@31f
    move-object/from16 v2, v30

    #@321
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@324
    goto/16 :goto_18c

    #@326
    .line 1464
    .end local v13           #i$:Ljava/util/Iterator;
    .end local v19           #overlay:Landroid/media/videoeditor/Overlay;
    .end local v26           #userAttributes:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_326
    const-string v29, ""

    #@328
    const-string/jumbo v30, "overlays"

    #@32b
    move-object/from16 v0, v22

    #@32d
    move-object/from16 v1, v29

    #@32f
    move-object/from16 v2, v30

    #@331
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@334
    .line 1467
    :cond_334
    invoke-virtual {v15}, Landroid/media/videoeditor/MediaItem;->getAllEffects()Ljava/util/List;

    #@337
    move-result-object v9

    #@338
    .line 1468
    .local v9, effects:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Effect;>;"
    invoke-interface {v9}, Ljava/util/List;->size()I

    #@33b
    move-result v29

    #@33c
    if-lez v29, :cond_559

    #@33e
    .line 1469
    const-string v29, ""

    #@340
    const-string v30, "effects"

    #@342
    move-object/from16 v0, v22

    #@344
    move-object/from16 v1, v29

    #@346
    move-object/from16 v2, v30

    #@348
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@34b
    .line 1470
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@34e
    move-result-object v12

    #@34f
    .local v12, i$:Ljava/util/Iterator;
    :goto_34f
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    #@352
    move-result v29

    #@353
    if-eqz v29, :cond_54c

    #@355
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@358
    move-result-object v8

    #@359
    check-cast v8, Landroid/media/videoeditor/Effect;

    #@35b
    .line 1471
    .local v8, effect:Landroid/media/videoeditor/Effect;
    const-string v29, ""

    #@35d
    const-string v30, "effect"

    #@35f
    move-object/from16 v0, v22

    #@361
    move-object/from16 v1, v29

    #@363
    move-object/from16 v2, v30

    #@365
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@368
    .line 1472
    const-string v29, ""

    #@36a
    const-string v30, "id"

    #@36c
    invoke-virtual {v8}, Landroid/media/videoeditor/Effect;->getId()Ljava/lang/String;

    #@36f
    move-result-object v31

    #@370
    move-object/from16 v0, v22

    #@372
    move-object/from16 v1, v29

    #@374
    move-object/from16 v2, v30

    #@376
    move-object/from16 v3, v31

    #@378
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@37b
    .line 1473
    const-string v29, ""

    #@37d
    const-string/jumbo v30, "type"

    #@380
    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@383
    move-result-object v31

    #@384
    invoke-virtual/range {v31 .. v31}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@387
    move-result-object v31

    #@388
    move-object/from16 v0, v22

    #@38a
    move-object/from16 v1, v29

    #@38c
    move-object/from16 v2, v30

    #@38e
    move-object/from16 v3, v31

    #@390
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@393
    .line 1475
    const-string v29, ""

    #@395
    const-string v30, "begin_time"

    #@397
    invoke-virtual {v8}, Landroid/media/videoeditor/Effect;->getStartTime()J

    #@39a
    move-result-wide v31

    #@39b
    invoke-static/range {v31 .. v32}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@39e
    move-result-object v31

    #@39f
    move-object/from16 v0, v22

    #@3a1
    move-object/from16 v1, v29

    #@3a3
    move-object/from16 v2, v30

    #@3a5
    move-object/from16 v3, v31

    #@3a7
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@3aa
    .line 1477
    const-string v29, ""

    #@3ac
    const-string v30, "duration"

    #@3ae
    invoke-virtual {v8}, Landroid/media/videoeditor/Effect;->getDuration()J

    #@3b1
    move-result-wide v31

    #@3b2
    invoke-static/range {v31 .. v32}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@3b5
    move-result-object v31

    #@3b6
    move-object/from16 v0, v22

    #@3b8
    move-object/from16 v1, v29

    #@3ba
    move-object/from16 v2, v30

    #@3bc
    move-object/from16 v3, v31

    #@3be
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@3c1
    .line 1479
    instance-of v0, v8, Landroid/media/videoeditor/EffectColor;

    #@3c3
    move/from16 v29, v0

    #@3c5
    if-eqz v29, :cond_41f

    #@3c7
    move-object v7, v8

    #@3c8
    .line 1480
    check-cast v7, Landroid/media/videoeditor/EffectColor;

    #@3ca
    .line 1481
    .local v7, colorEffect:Landroid/media/videoeditor/EffectColor;
    const-string v29, ""

    #@3cc
    const-string v30, "color_type"

    #@3ce
    invoke-virtual {v7}, Landroid/media/videoeditor/EffectColor;->getType()I

    #@3d1
    move-result v31

    #@3d2
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@3d5
    move-result-object v31

    #@3d6
    move-object/from16 v0, v22

    #@3d8
    move-object/from16 v1, v29

    #@3da
    move-object/from16 v2, v30

    #@3dc
    move-object/from16 v3, v31

    #@3de
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@3e1
    .line 1483
    invoke-virtual {v7}, Landroid/media/videoeditor/EffectColor;->getType()I

    #@3e4
    move-result v29

    #@3e5
    const/16 v30, 0x1

    #@3e7
    move/from16 v0, v29

    #@3e9
    move/from16 v1, v30

    #@3eb
    if-eq v0, v1, :cond_3f9

    #@3ed
    invoke-virtual {v7}, Landroid/media/videoeditor/EffectColor;->getType()I

    #@3f0
    move-result v29

    #@3f1
    const/16 v30, 0x2

    #@3f3
    move/from16 v0, v29

    #@3f5
    move/from16 v1, v30

    #@3f7
    if-ne v0, v1, :cond_410

    #@3f9
    .line 1485
    :cond_3f9
    const-string v29, ""

    #@3fb
    const-string v30, "color_value"

    #@3fd
    invoke-virtual {v7}, Landroid/media/videoeditor/EffectColor;->getColor()I

    #@400
    move-result v31

    #@401
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@404
    move-result-object v31

    #@405
    move-object/from16 v0, v22

    #@407
    move-object/from16 v1, v29

    #@409
    move-object/from16 v2, v30

    #@40b
    move-object/from16 v3, v31

    #@40d
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@410
    .line 1520
    .end local v7           #colorEffect:Landroid/media/videoeditor/EffectColor;
    :cond_410
    :goto_410
    const-string v29, ""

    #@412
    const-string v30, "effect"

    #@414
    move-object/from16 v0, v22

    #@416
    move-object/from16 v1, v29

    #@418
    move-object/from16 v2, v30

    #@41a
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@41d
    goto/16 :goto_34f

    #@41f
    .line 1488
    :cond_41f
    instance-of v0, v8, Landroid/media/videoeditor/EffectKenBurns;

    #@421
    move/from16 v29, v0

    #@423
    if-eqz v29, :cond_410

    #@425
    move-object/from16 v29, v8

    #@427
    .line 1489
    check-cast v29, Landroid/media/videoeditor/EffectKenBurns;

    #@429
    invoke-virtual/range {v29 .. v29}, Landroid/media/videoeditor/EffectKenBurns;->getStartRect()Landroid/graphics/Rect;

    #@42c
    move-result-object v23

    #@42d
    .line 1490
    .local v23, startRect:Landroid/graphics/Rect;
    const-string v29, ""

    #@42f
    const-string/jumbo v30, "start_l"

    #@432
    move-object/from16 v0, v23

    #@434
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@436
    move/from16 v31, v0

    #@438
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@43b
    move-result-object v31

    #@43c
    move-object/from16 v0, v22

    #@43e
    move-object/from16 v1, v29

    #@440
    move-object/from16 v2, v30

    #@442
    move-object/from16 v3, v31

    #@444
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@447
    .line 1492
    const-string v29, ""

    #@449
    const-string/jumbo v30, "start_t"

    #@44c
    move-object/from16 v0, v23

    #@44e
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@450
    move/from16 v31, v0

    #@452
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@455
    move-result-object v31

    #@456
    move-object/from16 v0, v22

    #@458
    move-object/from16 v1, v29

    #@45a
    move-object/from16 v2, v30

    #@45c
    move-object/from16 v3, v31

    #@45e
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@461
    .line 1494
    const-string v29, ""

    #@463
    const-string/jumbo v30, "start_r"

    #@466
    move-object/from16 v0, v23

    #@468
    iget v0, v0, Landroid/graphics/Rect;->right:I

    #@46a
    move/from16 v31, v0

    #@46c
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@46f
    move-result-object v31

    #@470
    move-object/from16 v0, v22

    #@472
    move-object/from16 v1, v29

    #@474
    move-object/from16 v2, v30

    #@476
    move-object/from16 v3, v31

    #@478
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@47b
    .line 1496
    const-string v29, ""

    #@47d
    const-string/jumbo v30, "start_b"

    #@480
    move-object/from16 v0, v23

    #@482
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    #@484
    move/from16 v31, v0

    #@486
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@489
    move-result-object v31

    #@48a
    move-object/from16 v0, v22

    #@48c
    move-object/from16 v1, v29

    #@48e
    move-object/from16 v2, v30

    #@490
    move-object/from16 v3, v31

    #@492
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@495
    move-object/from16 v29, v8

    #@497
    .line 1499
    check-cast v29, Landroid/media/videoeditor/EffectKenBurns;

    #@499
    invoke-virtual/range {v29 .. v29}, Landroid/media/videoeditor/EffectKenBurns;->getEndRect()Landroid/graphics/Rect;

    #@49c
    move-result-object v10

    #@49d
    .line 1500
    .local v10, endRect:Landroid/graphics/Rect;
    const-string v29, ""

    #@49f
    const-string v30, "end_l"

    #@4a1
    iget v0, v10, Landroid/graphics/Rect;->left:I

    #@4a3
    move/from16 v31, v0

    #@4a5
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@4a8
    move-result-object v31

    #@4a9
    move-object/from16 v0, v22

    #@4ab
    move-object/from16 v1, v29

    #@4ad
    move-object/from16 v2, v30

    #@4af
    move-object/from16 v3, v31

    #@4b1
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@4b4
    .line 1502
    const-string v29, ""

    #@4b6
    const-string v30, "end_t"

    #@4b8
    iget v0, v10, Landroid/graphics/Rect;->top:I

    #@4ba
    move/from16 v31, v0

    #@4bc
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@4bf
    move-result-object v31

    #@4c0
    move-object/from16 v0, v22

    #@4c2
    move-object/from16 v1, v29

    #@4c4
    move-object/from16 v2, v30

    #@4c6
    move-object/from16 v3, v31

    #@4c8
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@4cb
    .line 1504
    const-string v29, ""

    #@4cd
    const-string v30, "end_r"

    #@4cf
    iget v0, v10, Landroid/graphics/Rect;->right:I

    #@4d1
    move/from16 v31, v0

    #@4d3
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@4d6
    move-result-object v31

    #@4d7
    move-object/from16 v0, v22

    #@4d9
    move-object/from16 v1, v29

    #@4db
    move-object/from16 v2, v30

    #@4dd
    move-object/from16 v3, v31

    #@4df
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@4e2
    .line 1506
    const-string v29, ""

    #@4e4
    const-string v30, "end_b"

    #@4e6
    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    #@4e8
    move/from16 v31, v0

    #@4ea
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@4ed
    move-result-object v31

    #@4ee
    move-object/from16 v0, v22

    #@4f0
    move-object/from16 v1, v29

    #@4f2
    move-object/from16 v2, v30

    #@4f4
    move-object/from16 v3, v31

    #@4f6
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@4f9
    .line 1508
    invoke-virtual {v8}, Landroid/media/videoeditor/Effect;->getMediaItem()Landroid/media/videoeditor/MediaItem;

    #@4fc
    move-result-object v14

    #@4fd
    .local v14, mItem:Landroid/media/videoeditor/MediaItem;
    move-object/from16 v29, v14

    #@4ff
    .line 1509
    check-cast v29, Landroid/media/videoeditor/MediaImageItem;

    #@501
    invoke-virtual/range {v29 .. v29}, Landroid/media/videoeditor/MediaImageItem;->getGeneratedImageClip()Ljava/lang/String;

    #@504
    move-result-object v29

    #@505
    if-eqz v29, :cond_534

    #@507
    .line 1510
    const-string v29, ""

    #@509
    const-string/jumbo v30, "is_image_clip_generated"

    #@50c
    const/16 v31, 0x1

    #@50e
    invoke-static/range {v31 .. v31}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    #@511
    move-result-object v31

    #@512
    move-object/from16 v0, v22

    #@514
    move-object/from16 v1, v29

    #@516
    move-object/from16 v2, v30

    #@518
    move-object/from16 v3, v31

    #@51a
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@51d
    .line 1512
    const-string v29, ""

    #@51f
    const-string v30, "generated_image_clip"

    #@521
    check-cast v14, Landroid/media/videoeditor/MediaImageItem;

    #@523
    .end local v14           #mItem:Landroid/media/videoeditor/MediaItem;
    invoke-virtual {v14}, Landroid/media/videoeditor/MediaImageItem;->getGeneratedImageClip()Ljava/lang/String;

    #@526
    move-result-object v31

    #@527
    move-object/from16 v0, v22

    #@529
    move-object/from16 v1, v29

    #@52b
    move-object/from16 v2, v30

    #@52d
    move-object/from16 v3, v31

    #@52f
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@532
    goto/16 :goto_410

    #@534
    .line 1515
    .restart local v14       #mItem:Landroid/media/videoeditor/MediaItem;
    :cond_534
    const-string v29, ""

    #@536
    const-string/jumbo v30, "is_image_clip_generated"

    #@539
    const/16 v31, 0x0

    #@53b
    invoke-static/range {v31 .. v31}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    #@53e
    move-result-object v31

    #@53f
    move-object/from16 v0, v22

    #@541
    move-object/from16 v1, v29

    #@543
    move-object/from16 v2, v30

    #@545
    move-object/from16 v3, v31

    #@547
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@54a
    goto/16 :goto_410

    #@54c
    .line 1522
    .end local v8           #effect:Landroid/media/videoeditor/Effect;
    .end local v10           #endRect:Landroid/graphics/Rect;
    .end local v14           #mItem:Landroid/media/videoeditor/MediaItem;
    .end local v23           #startRect:Landroid/graphics/Rect;
    :cond_54c
    const-string v29, ""

    #@54e
    const-string v30, "effects"

    #@550
    move-object/from16 v0, v22

    #@552
    move-object/from16 v1, v29

    #@554
    move-object/from16 v2, v30

    #@556
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@559
    .line 1525
    .end local v12           #i$:Ljava/util/Iterator;
    :cond_559
    const-string v29, ""

    #@55b
    const-string/jumbo v30, "media_item"

    #@55e
    move-object/from16 v0, v22

    #@560
    move-object/from16 v1, v29

    #@562
    move-object/from16 v2, v30

    #@564
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@567
    goto/16 :goto_7e

    #@569
    .line 1527
    .end local v9           #effects:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Effect;>;"
    .end local v15           #mediaItem:Landroid/media/videoeditor/MediaItem;
    .end local v21           #overlays:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Overlay;>;"
    :cond_569
    const-string v29, ""

    #@56b
    const-string/jumbo v30, "media_items"

    #@56e
    move-object/from16 v0, v22

    #@570
    move-object/from16 v1, v29

    #@572
    move-object/from16 v2, v30

    #@574
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@577
    .line 1529
    const-string v29, ""

    #@579
    const-string/jumbo v30, "transitions"

    #@57c
    move-object/from16 v0, v22

    #@57e
    move-object/from16 v1, v29

    #@580
    move-object/from16 v2, v30

    #@582
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@585
    .line 1531
    move-object/from16 v0, p0

    #@587
    iget-object v0, v0, Landroid/media/videoeditor/VideoEditorImpl;->mTransitions:Ljava/util/List;

    #@589
    move-object/from16 v29, v0

    #@58b
    invoke-interface/range {v29 .. v29}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@58e
    move-result-object v11

    #@58f
    .local v11, i$:Ljava/util/Iterator;
    :goto_58f
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    #@592
    move-result v29

    #@593
    if-eqz v29, :cond_6f3

    #@595
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@598
    move-result-object v25

    #@599
    check-cast v25, Landroid/media/videoeditor/Transition;

    #@59b
    .line 1532
    .local v25, transition:Landroid/media/videoeditor/Transition;
    const-string v29, ""

    #@59d
    const-string/jumbo v30, "transition"

    #@5a0
    move-object/from16 v0, v22

    #@5a2
    move-object/from16 v1, v29

    #@5a4
    move-object/from16 v2, v30

    #@5a6
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@5a9
    .line 1533
    const-string v29, ""

    #@5ab
    const-string v30, "id"

    #@5ad
    invoke-virtual/range {v25 .. v25}, Landroid/media/videoeditor/Transition;->getId()Ljava/lang/String;

    #@5b0
    move-result-object v31

    #@5b1
    move-object/from16 v0, v22

    #@5b3
    move-object/from16 v1, v29

    #@5b5
    move-object/from16 v2, v30

    #@5b7
    move-object/from16 v3, v31

    #@5b9
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@5bc
    .line 1534
    const-string v29, ""

    #@5be
    const-string/jumbo v30, "type"

    #@5c1
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@5c4
    move-result-object v31

    #@5c5
    invoke-virtual/range {v31 .. v31}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5c8
    move-result-object v31

    #@5c9
    move-object/from16 v0, v22

    #@5cb
    move-object/from16 v1, v29

    #@5cd
    move-object/from16 v2, v30

    #@5cf
    move-object/from16 v3, v31

    #@5d1
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@5d4
    .line 1535
    const-string v29, ""

    #@5d6
    const-string v30, "duration"

    #@5d8
    invoke-virtual/range {v25 .. v25}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@5db
    move-result-wide v31

    #@5dc
    invoke-static/range {v31 .. v32}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@5df
    move-result-object v31

    #@5e0
    move-object/from16 v0, v22

    #@5e2
    move-object/from16 v1, v29

    #@5e4
    move-object/from16 v2, v30

    #@5e6
    move-object/from16 v3, v31

    #@5e8
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@5eb
    .line 1536
    const-string v29, ""

    #@5ed
    const-string v30, "behavior"

    #@5ef
    invoke-virtual/range {v25 .. v25}, Landroid/media/videoeditor/Transition;->getBehavior()I

    #@5f2
    move-result v31

    #@5f3
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@5f6
    move-result-object v31

    #@5f7
    move-object/from16 v0, v22

    #@5f9
    move-object/from16 v1, v29

    #@5fb
    move-object/from16 v2, v30

    #@5fd
    move-object/from16 v3, v31

    #@5ff
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@602
    .line 1537
    const-string v29, ""

    #@604
    const-string/jumbo v30, "is_transition_generated"

    #@607
    invoke-virtual/range {v25 .. v25}, Landroid/media/videoeditor/Transition;->isGenerated()Z

    #@60a
    move-result v31

    #@60b
    invoke-static/range {v31 .. v31}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    #@60e
    move-result-object v31

    #@60f
    move-object/from16 v0, v22

    #@611
    move-object/from16 v1, v29

    #@613
    move-object/from16 v2, v30

    #@615
    move-object/from16 v3, v31

    #@617
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@61a
    .line 1539
    invoke-virtual/range {v25 .. v25}, Landroid/media/videoeditor/Transition;->isGenerated()Z

    #@61d
    move-result v29

    #@61e
    const/16 v30, 0x1

    #@620
    move/from16 v0, v29

    #@622
    move/from16 v1, v30

    #@624
    if-ne v0, v1, :cond_63b

    #@626
    .line 1540
    const-string v29, ""

    #@628
    const-string v30, "generated_transition_clip"

    #@62a
    move-object/from16 v0, v25

    #@62c
    iget-object v0, v0, Landroid/media/videoeditor/Transition;->mFilename:Ljava/lang/String;

    #@62e
    move-object/from16 v31, v0

    #@630
    move-object/from16 v0, v22

    #@632
    move-object/from16 v1, v29

    #@634
    move-object/from16 v2, v30

    #@636
    move-object/from16 v3, v31

    #@638
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@63b
    .line 1542
    :cond_63b
    invoke-virtual/range {v25 .. v25}, Landroid/media/videoeditor/Transition;->getAfterMediaItem()Landroid/media/videoeditor/MediaItem;

    #@63e
    move-result-object v4

    #@63f
    .line 1543
    .local v4, afterMediaItem:Landroid/media/videoeditor/MediaItem;
    if-eqz v4, :cond_654

    #@641
    .line 1544
    const-string v29, ""

    #@643
    const-string v30, "after_media_item"

    #@645
    invoke-virtual {v4}, Landroid/media/videoeditor/MediaItem;->getId()Ljava/lang/String;

    #@648
    move-result-object v31

    #@649
    move-object/from16 v0, v22

    #@64b
    move-object/from16 v1, v29

    #@64d
    move-object/from16 v2, v30

    #@64f
    move-object/from16 v3, v31

    #@651
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@654
    .line 1547
    :cond_654
    invoke-virtual/range {v25 .. v25}, Landroid/media/videoeditor/Transition;->getBeforeMediaItem()Landroid/media/videoeditor/MediaItem;

    #@657
    move-result-object v6

    #@658
    .line 1548
    .local v6, beforeMediaItem:Landroid/media/videoeditor/MediaItem;
    if-eqz v6, :cond_66d

    #@65a
    .line 1549
    const-string v29, ""

    #@65c
    const-string v30, "before_media_item"

    #@65e
    invoke-virtual {v6}, Landroid/media/videoeditor/MediaItem;->getId()Ljava/lang/String;

    #@661
    move-result-object v31

    #@662
    move-object/from16 v0, v22

    #@664
    move-object/from16 v1, v29

    #@666
    move-object/from16 v2, v30

    #@668
    move-object/from16 v3, v31

    #@66a
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@66d
    .line 1552
    :cond_66d
    move-object/from16 v0, v25

    #@66f
    instance-of v0, v0, Landroid/media/videoeditor/TransitionSliding;

    #@671
    move/from16 v29, v0

    #@673
    if-eqz v29, :cond_69e

    #@675
    .line 1553
    const-string v29, ""

    #@677
    const-string v30, "direction"

    #@679
    check-cast v25, Landroid/media/videoeditor/TransitionSliding;

    #@67b
    .end local v25           #transition:Landroid/media/videoeditor/Transition;
    invoke-virtual/range {v25 .. v25}, Landroid/media/videoeditor/TransitionSliding;->getDirection()I

    #@67e
    move-result v31

    #@67f
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@682
    move-result-object v31

    #@683
    move-object/from16 v0, v22

    #@685
    move-object/from16 v1, v29

    #@687
    move-object/from16 v2, v30

    #@689
    move-object/from16 v3, v31

    #@68b
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@68e
    .line 1565
    :cond_68e
    :goto_68e
    const-string v29, ""

    #@690
    const-string/jumbo v30, "transition"

    #@693
    move-object/from16 v0, v22

    #@695
    move-object/from16 v1, v29

    #@697
    move-object/from16 v2, v30

    #@699
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@69c
    goto/16 :goto_58f

    #@69e
    .line 1555
    .restart local v25       #transition:Landroid/media/videoeditor/Transition;
    :cond_69e
    move-object/from16 v0, v25

    #@6a0
    instance-of v0, v0, Landroid/media/videoeditor/TransitionAlpha;

    #@6a2
    move/from16 v29, v0

    #@6a4
    if-eqz v29, :cond_68e

    #@6a6
    move-object/from16 v24, v25

    #@6a8
    .line 1556
    check-cast v24, Landroid/media/videoeditor/TransitionAlpha;

    #@6aa
    .line 1557
    .local v24, ta:Landroid/media/videoeditor/TransitionAlpha;
    const-string v29, ""

    #@6ac
    const-string v30, "blending"

    #@6ae
    invoke-virtual/range {v24 .. v24}, Landroid/media/videoeditor/TransitionAlpha;->getBlendingPercent()I

    #@6b1
    move-result v31

    #@6b2
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@6b5
    move-result-object v31

    #@6b6
    move-object/from16 v0, v22

    #@6b8
    move-object/from16 v1, v29

    #@6ba
    move-object/from16 v2, v30

    #@6bc
    move-object/from16 v3, v31

    #@6be
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@6c1
    .line 1559
    const-string v29, ""

    #@6c3
    const-string v30, "invert"

    #@6c5
    invoke-virtual/range {v24 .. v24}, Landroid/media/videoeditor/TransitionAlpha;->isInvert()Z

    #@6c8
    move-result v31

    #@6c9
    invoke-static/range {v31 .. v31}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    #@6cc
    move-result-object v31

    #@6cd
    move-object/from16 v0, v22

    #@6cf
    move-object/from16 v1, v29

    #@6d1
    move-object/from16 v2, v30

    #@6d3
    move-object/from16 v3, v31

    #@6d5
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@6d8
    .line 1561
    invoke-virtual/range {v24 .. v24}, Landroid/media/videoeditor/TransitionAlpha;->getMaskFilename()Ljava/lang/String;

    #@6db
    move-result-object v29

    #@6dc
    if-eqz v29, :cond_68e

    #@6de
    .line 1562
    const-string v29, ""

    #@6e0
    const-string/jumbo v30, "mask"

    #@6e3
    invoke-virtual/range {v24 .. v24}, Landroid/media/videoeditor/TransitionAlpha;->getMaskFilename()Ljava/lang/String;

    #@6e6
    move-result-object v31

    #@6e7
    move-object/from16 v0, v22

    #@6e9
    move-object/from16 v1, v29

    #@6eb
    move-object/from16 v2, v30

    #@6ed
    move-object/from16 v3, v31

    #@6ef
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@6f2
    goto :goto_68e

    #@6f3
    .line 1567
    .end local v4           #afterMediaItem:Landroid/media/videoeditor/MediaItem;
    .end local v6           #beforeMediaItem:Landroid/media/videoeditor/MediaItem;
    .end local v24           #ta:Landroid/media/videoeditor/TransitionAlpha;
    .end local v25           #transition:Landroid/media/videoeditor/Transition;
    :cond_6f3
    const-string v29, ""

    #@6f5
    const-string/jumbo v30, "transitions"

    #@6f8
    move-object/from16 v0, v22

    #@6fa
    move-object/from16 v1, v29

    #@6fc
    move-object/from16 v2, v30

    #@6fe
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@701
    .line 1568
    const-string v29, ""

    #@703
    const-string v30, "audio_tracks"

    #@705
    move-object/from16 v0, v22

    #@707
    move-object/from16 v1, v29

    #@709
    move-object/from16 v2, v30

    #@70b
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@70e
    .line 1569
    move-object/from16 v0, p0

    #@710
    iget-object v0, v0, Landroid/media/videoeditor/VideoEditorImpl;->mAudioTracks:Ljava/util/List;

    #@712
    move-object/from16 v29, v0

    #@714
    invoke-interface/range {v29 .. v29}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@717
    move-result-object v11

    #@718
    :goto_718
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    #@71b
    move-result v29

    #@71c
    if-eqz v29, :cond_853

    #@71e
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@721
    move-result-object v5

    #@722
    check-cast v5, Landroid/media/videoeditor/AudioTrack;

    #@724
    .line 1570
    .local v5, at:Landroid/media/videoeditor/AudioTrack;
    const-string v29, ""

    #@726
    const-string v30, "audio_track"

    #@728
    move-object/from16 v0, v22

    #@72a
    move-object/from16 v1, v29

    #@72c
    move-object/from16 v2, v30

    #@72e
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@731
    .line 1571
    const-string v29, ""

    #@733
    const-string v30, "id"

    #@735
    invoke-virtual {v5}, Landroid/media/videoeditor/AudioTrack;->getId()Ljava/lang/String;

    #@738
    move-result-object v31

    #@739
    move-object/from16 v0, v22

    #@73b
    move-object/from16 v1, v29

    #@73d
    move-object/from16 v2, v30

    #@73f
    move-object/from16 v3, v31

    #@741
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@744
    .line 1572
    const-string v29, ""

    #@746
    const-string v30, "filename"

    #@748
    invoke-virtual {v5}, Landroid/media/videoeditor/AudioTrack;->getFilename()Ljava/lang/String;

    #@74b
    move-result-object v31

    #@74c
    move-object/from16 v0, v22

    #@74e
    move-object/from16 v1, v29

    #@750
    move-object/from16 v2, v30

    #@752
    move-object/from16 v3, v31

    #@754
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@757
    .line 1573
    const-string v29, ""

    #@759
    const-string/jumbo v30, "start_time"

    #@75c
    invoke-virtual {v5}, Landroid/media/videoeditor/AudioTrack;->getStartTime()J

    #@75f
    move-result-wide v31

    #@760
    invoke-static/range {v31 .. v32}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@763
    move-result-object v31

    #@764
    move-object/from16 v0, v22

    #@766
    move-object/from16 v1, v29

    #@768
    move-object/from16 v2, v30

    #@76a
    move-object/from16 v3, v31

    #@76c
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@76f
    .line 1574
    const-string v29, ""

    #@771
    const-string v30, "begin_time"

    #@773
    invoke-virtual {v5}, Landroid/media/videoeditor/AudioTrack;->getBoundaryBeginTime()J

    #@776
    move-result-wide v31

    #@777
    invoke-static/range {v31 .. v32}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@77a
    move-result-object v31

    #@77b
    move-object/from16 v0, v22

    #@77d
    move-object/from16 v1, v29

    #@77f
    move-object/from16 v2, v30

    #@781
    move-object/from16 v3, v31

    #@783
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@786
    .line 1575
    const-string v29, ""

    #@788
    const-string v30, "end_time"

    #@78a
    invoke-virtual {v5}, Landroid/media/videoeditor/AudioTrack;->getBoundaryEndTime()J

    #@78d
    move-result-wide v31

    #@78e
    invoke-static/range {v31 .. v32}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@791
    move-result-object v31

    #@792
    move-object/from16 v0, v22

    #@794
    move-object/from16 v1, v29

    #@796
    move-object/from16 v2, v30

    #@798
    move-object/from16 v3, v31

    #@79a
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@79d
    .line 1576
    const-string v29, ""

    #@79f
    const-string/jumbo v30, "volume"

    #@7a2
    invoke-virtual {v5}, Landroid/media/videoeditor/AudioTrack;->getVolume()I

    #@7a5
    move-result v31

    #@7a6
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@7a9
    move-result-object v31

    #@7aa
    move-object/from16 v0, v22

    #@7ac
    move-object/from16 v1, v29

    #@7ae
    move-object/from16 v2, v30

    #@7b0
    move-object/from16 v3, v31

    #@7b2
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@7b5
    .line 1577
    const-string v29, ""

    #@7b7
    const-string v30, "ducking_enabled"

    #@7b9
    invoke-virtual {v5}, Landroid/media/videoeditor/AudioTrack;->isDuckingEnabled()Z

    #@7bc
    move-result v31

    #@7bd
    invoke-static/range {v31 .. v31}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    #@7c0
    move-result-object v31

    #@7c1
    move-object/from16 v0, v22

    #@7c3
    move-object/from16 v1, v29

    #@7c5
    move-object/from16 v2, v30

    #@7c7
    move-object/from16 v3, v31

    #@7c9
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@7cc
    .line 1579
    const-string v29, ""

    #@7ce
    const-string v30, "ducking_volume"

    #@7d0
    invoke-virtual {v5}, Landroid/media/videoeditor/AudioTrack;->getDuckedTrackVolume()I

    #@7d3
    move-result v31

    #@7d4
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@7d7
    move-result-object v31

    #@7d8
    move-object/from16 v0, v22

    #@7da
    move-object/from16 v1, v29

    #@7dc
    move-object/from16 v2, v30

    #@7de
    move-object/from16 v3, v31

    #@7e0
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@7e3
    .line 1581
    const-string v29, ""

    #@7e5
    const-string v30, "ducking_threshold"

    #@7e7
    invoke-virtual {v5}, Landroid/media/videoeditor/AudioTrack;->getDuckingThreshhold()I

    #@7ea
    move-result v31

    #@7eb
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@7ee
    move-result-object v31

    #@7ef
    move-object/from16 v0, v22

    #@7f1
    move-object/from16 v1, v29

    #@7f3
    move-object/from16 v2, v30

    #@7f5
    move-object/from16 v3, v31

    #@7f7
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@7fa
    .line 1583
    const-string v29, ""

    #@7fc
    const-string/jumbo v30, "muted"

    #@7ff
    invoke-virtual {v5}, Landroid/media/videoeditor/AudioTrack;->isMuted()Z

    #@802
    move-result v31

    #@803
    invoke-static/range {v31 .. v31}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    #@806
    move-result-object v31

    #@807
    move-object/from16 v0, v22

    #@809
    move-object/from16 v1, v29

    #@80b
    move-object/from16 v2, v30

    #@80d
    move-object/from16 v3, v31

    #@80f
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@812
    .line 1584
    const-string v29, ""

    #@814
    const-string/jumbo v30, "loop"

    #@817
    invoke-virtual {v5}, Landroid/media/videoeditor/AudioTrack;->isLooping()Z

    #@81a
    move-result v31

    #@81b
    invoke-static/range {v31 .. v31}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    #@81e
    move-result-object v31

    #@81f
    move-object/from16 v0, v22

    #@821
    move-object/from16 v1, v29

    #@823
    move-object/from16 v2, v30

    #@825
    move-object/from16 v3, v31

    #@827
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@82a
    .line 1585
    invoke-virtual {v5}, Landroid/media/videoeditor/AudioTrack;->getAudioWaveformFilename()Ljava/lang/String;

    #@82d
    move-result-object v29

    #@82e
    if-eqz v29, :cond_844

    #@830
    .line 1586
    const-string v29, ""

    #@832
    const-string/jumbo v30, "waveform"

    #@835
    invoke-virtual {v5}, Landroid/media/videoeditor/AudioTrack;->getAudioWaveformFilename()Ljava/lang/String;

    #@838
    move-result-object v31

    #@839
    move-object/from16 v0, v22

    #@83b
    move-object/from16 v1, v29

    #@83d
    move-object/from16 v2, v30

    #@83f
    move-object/from16 v3, v31

    #@841
    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@844
    .line 1590
    :cond_844
    const-string v29, ""

    #@846
    const-string v30, "audio_track"

    #@848
    move-object/from16 v0, v22

    #@84a
    move-object/from16 v1, v29

    #@84c
    move-object/from16 v2, v30

    #@84e
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@851
    goto/16 :goto_718

    #@853
    .line 1592
    .end local v5           #at:Landroid/media/videoeditor/AudioTrack;
    :cond_853
    const-string v29, ""

    #@855
    const-string v30, "audio_tracks"

    #@857
    move-object/from16 v0, v22

    #@859
    move-object/from16 v1, v29

    #@85b
    move-object/from16 v2, v30

    #@85d
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@860
    .line 1594
    const-string v29, ""

    #@862
    const-string/jumbo v30, "project"

    #@865
    move-object/from16 v0, v22

    #@867
    move-object/from16 v1, v29

    #@869
    move-object/from16 v2, v30

    #@86b
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@86e
    .line 1595
    invoke-interface/range {v22 .. v22}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    #@871
    .line 1600
    new-instance v18, Ljava/io/FileOutputStream;

    #@873
    new-instance v29, Ljava/io/File;

    #@875
    invoke-virtual/range {p0 .. p0}, Landroid/media/videoeditor/VideoEditorImpl;->getPath()Ljava/lang/String;

    #@878
    move-result-object v30

    #@879
    const-string/jumbo v31, "videoeditor.xml"

    #@87c
    invoke-direct/range {v29 .. v31}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@87f
    move-object/from16 v0, v18

    #@881
    move-object/from16 v1, v29

    #@883
    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    #@886
    .line 1602
    .local v18, out:Ljava/io/FileOutputStream;
    invoke-virtual/range {v28 .. v28}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    #@889
    move-result-object v29

    #@88a
    invoke-virtual/range {v29 .. v29}, Ljava/lang/String;->getBytes()[B

    #@88d
    move-result-object v29

    #@88e
    move-object/from16 v0, v18

    #@890
    move-object/from16 v1, v29

    #@892
    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    #@895
    .line 1603
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileOutputStream;->flush()V

    #@898
    .line 1604
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileOutputStream;->close()V

    #@89b
    .line 1605
    return-void
.end method

.method public setAspectRatio(I)V
    .registers 10
    .parameter "aspectRatio"

    #@0
    .prologue
    .line 1611
    iput p1, p0, Landroid/media/videoeditor/VideoEditorImpl;->mAspectRatio:I

    #@2
    .line 1615
    iget-object v6, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@4
    const/4 v7, 0x1

    #@5
    invoke-virtual {v6, v7}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@8
    .line 1617
    iget-object v6, p0, Landroid/media/videoeditor/VideoEditorImpl;->mTransitions:Ljava/util/List;

    #@a
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@d
    move-result-object v0

    #@e
    .local v0, i$:Ljava/util/Iterator;
    :goto_e
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@11
    move-result v6

    #@12
    if-eqz v6, :cond_1e

    #@14
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@17
    move-result-object v5

    #@18
    check-cast v5, Landroid/media/videoeditor/Transition;

    #@1a
    .line 1618
    .local v5, transition:Landroid/media/videoeditor/Transition;
    invoke-virtual {v5}, Landroid/media/videoeditor/Transition;->invalidate()V

    #@1d
    goto :goto_e

    #@1e
    .line 1621
    .end local v5           #transition:Landroid/media/videoeditor/Transition;
    :cond_1e
    iget-object v6, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@20
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@23
    move-result-object v1

    #@24
    .line 1623
    .local v1, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/videoeditor/MediaItem;>;"
    :cond_24
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@27
    move-result v6

    #@28
    if-eqz v6, :cond_4a

    #@2a
    .line 1624
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2d
    move-result-object v4

    #@2e
    check-cast v4, Landroid/media/videoeditor/MediaItem;

    #@30
    .line 1625
    .local v4, t:Landroid/media/videoeditor/MediaItem;
    invoke-virtual {v4}, Landroid/media/videoeditor/MediaItem;->getAllOverlays()Ljava/util/List;

    #@33
    move-result-object v3

    #@34
    .line 1626
    .local v3, overlayList:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Overlay;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@37
    move-result-object v0

    #@38
    :goto_38
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@3b
    move-result v6

    #@3c
    if-eqz v6, :cond_24

    #@3e
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@41
    move-result-object v2

    #@42
    check-cast v2, Landroid/media/videoeditor/Overlay;

    #@44
    .line 1628
    .local v2, overlay:Landroid/media/videoeditor/Overlay;
    check-cast v2, Landroid/media/videoeditor/OverlayFrame;

    #@46
    .end local v2           #overlay:Landroid/media/videoeditor/Overlay;
    invoke-virtual {v2}, Landroid/media/videoeditor/OverlayFrame;->invalidateGeneratedFiles()V

    #@49
    goto :goto_38

    #@4a
    .line 1631
    .end local v3           #overlayList:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Overlay;>;"
    .end local v4           #t:Landroid/media/videoeditor/MediaItem;
    :cond_4a
    return-void
.end method

.method public startPreview(Landroid/view/SurfaceHolder;JJZILandroid/media/videoeditor/VideoEditor$PreviewProgressListener;)V
    .registers 20
    .parameter "surfaceHolder"
    .parameter "fromMs"
    .parameter "toMs"
    .parameter "loop"
    .parameter "callbackAfterFrameCount"
    .parameter "listener"

    #@0
    .prologue
    .line 1640
    if-nez p1, :cond_8

    #@2
    .line 1641
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@7
    throw v0

    #@8
    .line 1644
    :cond_8
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    #@b
    move-result-object v1

    #@c
    .line 1645
    .local v1, surface:Landroid/view/Surface;
    if-nez v1, :cond_16

    #@e
    .line 1646
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@10
    const-string v2, "Surface could not be retrieved from surface holder"

    #@12
    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@15
    throw v0

    #@16
    .line 1649
    :cond_16
    invoke-virtual {v1}, Landroid/view/Surface;->isValid()Z

    #@19
    move-result v0

    #@1a
    if-nez v0, :cond_24

    #@1c
    .line 1650
    new-instance v0, Ljava/lang/IllegalStateException;

    #@1e
    const-string v2, "Surface is not valid"

    #@20
    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@23
    throw v0

    #@24
    .line 1653
    :cond_24
    if-nez p8, :cond_2c

    #@26
    .line 1654
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@28
    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@2b
    throw v0

    #@2c
    .line 1657
    :cond_2c
    iget-wide v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mDurationMs:J

    #@2e
    cmp-long v0, p2, v2

    #@30
    if-ltz v0, :cond_3a

    #@32
    .line 1658
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@34
    const-string v2, "Requested time not correct"

    #@36
    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@39
    throw v0

    #@3a
    .line 1661
    :cond_3a
    const-wide/16 v2, 0x0

    #@3c
    cmp-long v0, p2, v2

    #@3e
    if-gez v0, :cond_48

    #@40
    .line 1662
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@42
    const-string v2, "Requested time not correct"

    #@44
    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@47
    throw v0

    #@48
    .line 1665
    :cond_48
    const/4 v10, 0x0

    #@49
    .line 1666
    .local v10, semAcquireDone:Z
    iget-boolean v0, p0, Landroid/media/videoeditor/VideoEditorImpl;->mPreviewInProgress:Z

    #@4b
    if-nez v0, :cond_a3

    #@4d
    .line 1668
    const-wide/16 v2, 0x1f4

    #@4f
    :try_start_4f
    invoke-direct {p0, v2, v3}, Landroid/media/videoeditor/VideoEditorImpl;->lock(J)Z

    #@52
    move-result v10

    #@53
    .line 1669
    if-nez v10, :cond_72

    #@55
    .line 1670
    new-instance v0, Ljava/lang/IllegalStateException;

    #@57
    const-string v2, "Timeout waiting for semaphore"

    #@59
    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@5c
    throw v0
    :try_end_5d
    .catch Ljava/lang/InterruptedException; {:try_start_4f .. :try_end_5d} :catch_5d

    #@5d
    .line 1687
    :catch_5d
    move-exception v9

    #@5e
    .line 1688
    .local v9, ex:Ljava/lang/InterruptedException;
    const-string v0, "VideoEditorImpl"

    #@60
    const-string v2, "The thread was interrupted"

    #@62
    new-instance v3, Ljava/lang/Throwable;

    #@64
    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    #@67
    invoke-static {v0, v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@6a
    .line 1689
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6c
    const-string v2, "The thread was interrupted"

    #@6e
    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@71
    throw v0

    #@72
    .line 1673
    .end local v9           #ex:Ljava/lang/InterruptedException;
    :cond_72
    :try_start_72
    iget-object v0, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@74
    if-nez v0, :cond_7e

    #@76
    .line 1674
    new-instance v0, Ljava/lang/IllegalStateException;

    #@78
    const-string v2, "The video editor is not initialized"

    #@7a
    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@7d
    throw v0

    #@7e
    .line 1677
    :cond_7e
    iget-object v0, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@80
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@83
    move-result v0

    #@84
    if-lez v0, :cond_a2

    #@86
    .line 1678
    const/4 v0, 0x1

    #@87
    iput-boolean v0, p0, Landroid/media/videoeditor/VideoEditorImpl;->mPreviewInProgress:Z

    #@89
    .line 1679
    iget-object v0, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@8b
    iget-object v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    #@8d
    iget-object v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mTransitions:Ljava/util/List;

    #@8f
    iget-object v4, p0, Landroid/media/videoeditor/VideoEditorImpl;->mAudioTracks:Ljava/util/List;

    #@91
    const/4 v5, 0x0

    #@92
    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/media/videoeditor/MediaArtistNativeHelper;->previewStoryBoard(Ljava/util/List;Ljava/util/List;Ljava/util/List;Landroid/media/videoeditor/VideoEditor$MediaProcessingProgressListener;)V

    #@95
    .line 1681
    iget-object v0, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@97
    move-wide v2, p2

    #@98
    move-wide v4, p4

    #@99
    move/from16 v6, p6

    #@9b
    move/from16 v7, p7

    #@9d
    move-object/from16 v8, p8

    #@9f
    invoke-virtual/range {v0 .. v8}, Landroid/media/videoeditor/MediaArtistNativeHelper;->doPreview(Landroid/view/Surface;JJZILandroid/media/videoeditor/VideoEditor$PreviewProgressListener;)V
    :try_end_a2
    .catch Ljava/lang/InterruptedException; {:try_start_72 .. :try_end_a2} :catch_5d

    #@a2
    .line 1694
    :cond_a2
    return-void

    #@a3
    .line 1692
    :cond_a3
    new-instance v0, Ljava/lang/IllegalStateException;

    #@a5
    const-string v2, "Preview already in progress"

    #@a7
    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@aa
    throw v0
.end method

.method public stopPreview()J
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1700
    const-wide/16 v0, 0x0

    #@3
    .line 1701
    .local v0, result:J
    iget-boolean v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mPreviewInProgress:Z

    #@5
    if-eqz v2, :cond_1b

    #@7
    .line 1703
    :try_start_7
    iget-object v2, p0, Landroid/media/videoeditor/VideoEditorImpl;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@9
    invoke-virtual {v2}, Landroid/media/videoeditor/MediaArtistNativeHelper;->stopPreview()J
    :try_end_c
    .catchall {:try_start_7 .. :try_end_c} :catchall_14

    #@c
    move-result-wide v0

    #@d
    .line 1708
    iput-boolean v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mPreviewInProgress:Z

    #@f
    .line 1709
    invoke-direct {p0}, Landroid/media/videoeditor/VideoEditorImpl;->unlock()V

    #@12
    move-wide v2, v0

    #@13
    .line 1714
    :goto_13
    return-wide v2

    #@14
    .line 1708
    :catchall_14
    move-exception v2

    #@15
    iput-boolean v3, p0, Landroid/media/videoeditor/VideoEditorImpl;->mPreviewInProgress:Z

    #@17
    .line 1709
    invoke-direct {p0}, Landroid/media/videoeditor/VideoEditorImpl;->unlock()V

    #@1a
    throw v2

    #@1b
    .line 1714
    :cond_1b
    const-wide/16 v2, 0x0

    #@1d
    goto :goto_13
.end method

.method updateTimelineDuration()V
    .registers 1

    #@0
    .prologue
    .line 563
    invoke-direct {p0}, Landroid/media/videoeditor/VideoEditorImpl;->computeTimelineDuration()V

    #@3
    .line 564
    return-void
.end method
