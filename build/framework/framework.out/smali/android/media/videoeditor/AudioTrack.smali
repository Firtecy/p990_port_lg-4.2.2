.class public Landroid/media/videoeditor/AudioTrack;
.super Ljava/lang/Object;
.source "AudioTrack.java"


# instance fields
.field private final mAudioBitrate:I

.field private final mAudioChannels:I

.field private final mAudioSamplingFrequency:I

.field private final mAudioType:I

.field private mAudioWaveformFilename:Ljava/lang/String;

.field private mBeginBoundaryTimeMs:J

.field private mDuckedTrackVolume:I

.field private mDuckingThreshold:I

.field private final mDurationMs:J

.field private mEndBoundaryTimeMs:J

.field private final mFilename:Ljava/lang/String;

.field private mIsDuckingEnabled:Z

.field private mLoop:Z

.field private final mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

.field private mMuted:Z

.field private mStartTimeMs:J

.field private mTimelineDurationMs:J

.field private final mUniqueId:Ljava/lang/String;

.field private mVolumePercent:I

.field private mWaveformData:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/media/videoeditor/WaveformData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 75
    invoke-direct {p0, v0, v0, v0}, Landroid/media/videoeditor/AudioTrack;-><init>(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;)V

    #@4
    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;)V
    .registers 21
    .parameter "editor"
    .parameter "audioTrackId"
    .parameter "filename"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 91
    const-wide/16 v4, 0x0

    #@2
    const-wide/16 v6, 0x0

    #@4
    const-wide/16 v8, -0x1

    #@6
    const/4 v10, 0x0

    #@7
    const/16 v11, 0x64

    #@9
    const/4 v12, 0x0

    #@a
    const/4 v13, 0x0

    #@b
    const/4 v14, 0x0

    #@c
    const/4 v15, 0x0

    #@d
    const/16 v16, 0x0

    #@f
    move-object/from16 v0, p0

    #@11
    move-object/from16 v1, p1

    #@13
    move-object/from16 v2, p2

    #@15
    move-object/from16 v3, p3

    #@17
    invoke-direct/range {v0 .. v16}, Landroid/media/videoeditor/AudioTrack;-><init>(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;JJJZIZZIILjava/lang/String;)V

    #@1a
    .line 93
    return-void
.end method

.method constructor <init>(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;JJJZIZZIILjava/lang/String;)V
    .registers 27
    .parameter "editor"
    .parameter "audioTrackId"
    .parameter "filename"
    .parameter "startTimeMs"
    .parameter "beginMs"
    .parameter "endMs"
    .parameter "loop"
    .parameter "volume"
    .parameter "muted"
    .parameter "duckingEnabled"
    .parameter "duckThreshold"
    .parameter "duckedTrackVolume"
    .parameter "audioWaveformFilename"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 127
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 128
    const/4 v5, 0x0

    #@4
    .line 130
    .local v5, properties:Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
    new-instance v3, Ljava/io/File;

    #@6
    invoke-direct {v3, p3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@9
    .line 131
    .local v3, file:Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    #@c
    move-result v6

    #@d
    if-nez v6, :cond_28

    #@f
    .line 132
    new-instance v6, Ljava/io/IOException;

    #@11
    new-instance v7, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v7

    #@1a
    const-string v8, " not found ! "

    #@1c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v7

    #@20
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v7

    #@24
    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@27
    throw v6

    #@28
    .line 136
    :cond_28
    const-wide v6, 0x80000000L

    #@2d
    invoke-virtual {v3}, Ljava/io/File;->length()J

    #@30
    move-result-wide v8

    #@31
    cmp-long v6, v6, v8

    #@33
    if-gtz v6, :cond_3d

    #@35
    .line 137
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@37
    const-string v7, "File size is more than 2GB"

    #@39
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@3c
    throw v6

    #@3d
    .line 140
    :cond_3d
    instance-of v6, p1, Landroid/media/videoeditor/VideoEditorImpl;

    #@3f
    if-eqz v6, :cond_73

    #@41
    .line 141
    check-cast p1, Landroid/media/videoeditor/VideoEditorImpl;

    #@43
    .end local p1
    invoke-virtual {p1}, Landroid/media/videoeditor/VideoEditorImpl;->getNativeContext()Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@46
    move-result-object v6

    #@47
    iput-object v6, p0, Landroid/media/videoeditor/AudioTrack;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@49
    .line 146
    :try_start_49
    iget-object v6, p0, Landroid/media/videoeditor/AudioTrack;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@4b
    invoke-virtual {v6, p3}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getMediaProperties(Ljava/lang/String;)Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
    :try_end_4e
    .catch Ljava/lang/Exception; {:try_start_49 .. :try_end_4e} :catch_7b

    #@4e
    move-result-object v5

    #@4f
    .line 150
    iget-object v6, p0, Landroid/media/videoeditor/AudioTrack;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@51
    iget v7, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->fileType:I

    #@53
    invoke-virtual {v6, v7}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getFileType(I)I

    #@56
    move-result v4

    #@57
    .line 151
    .local v4, fileType:I
    packed-switch v4, :pswitch_data_114

    #@5a
    .line 159
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@5c
    new-instance v7, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    const-string v8, "Unsupported input file type: "

    #@63
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v7

    #@67
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v7

    #@6b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v7

    #@6f
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@72
    throw v6

    #@73
    .line 143
    .end local v4           #fileType:I
    .restart local p1
    :cond_73
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@75
    const-string v7, "editor is not of type VideoEditorImpl"

    #@77
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@7a
    throw v6

    #@7b
    .line 147
    .end local p1
    :catch_7b
    move-exception v2

    #@7c
    .line 148
    .local v2, e:Ljava/lang/Exception;
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@7e
    new-instance v7, Ljava/lang/StringBuilder;

    #@80
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@83
    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@86
    move-result-object v8

    #@87
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v7

    #@8b
    const-string v8, " : "

    #@8d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v7

    #@91
    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v7

    #@95
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v7

    #@99
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9c
    throw v6

    #@9d
    .line 162
    .end local v2           #e:Ljava/lang/Exception;
    .restart local v4       #fileType:I
    :pswitch_9d
    iget-object v6, p0, Landroid/media/videoeditor/AudioTrack;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@9f
    iget v7, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->audioFormat:I

    #@a1
    invoke-virtual {v6, v7}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    #@a4
    move-result v6

    #@a5
    packed-switch v6, :pswitch_data_120

    #@a8
    .line 169
    :pswitch_a8
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@aa
    const-string v7, "Unsupported Audio Codec Format in Input File"

    #@ac
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@af
    throw v6

    #@b0
    .line 172
    :pswitch_b0
    const-wide/16 v6, -0x1

    #@b2
    cmp-long v6, p8, v6

    #@b4
    if-nez v6, :cond_bb

    #@b6
    .line 173
    iget v6, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->audioDuration:I

    #@b8
    int-to-long v0, v6

    #@b9
    move-wide/from16 p8, v0

    #@bb
    .line 176
    :cond_bb
    iput-object p2, p0, Landroid/media/videoeditor/AudioTrack;->mUniqueId:Ljava/lang/String;

    #@bd
    .line 177
    iput-object p3, p0, Landroid/media/videoeditor/AudioTrack;->mFilename:Ljava/lang/String;

    #@bf
    .line 178
    iput-wide p4, p0, Landroid/media/videoeditor/AudioTrack;->mStartTimeMs:J

    #@c1
    .line 179
    iget v6, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->audioDuration:I

    #@c3
    int-to-long v6, v6

    #@c4
    iput-wide v6, p0, Landroid/media/videoeditor/AudioTrack;->mDurationMs:J

    #@c6
    .line 180
    iget v6, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->audioChannels:I

    #@c8
    iput v6, p0, Landroid/media/videoeditor/AudioTrack;->mAudioChannels:I

    #@ca
    .line 181
    iget v6, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->audioBitrate:I

    #@cc
    iput v6, p0, Landroid/media/videoeditor/AudioTrack;->mAudioBitrate:I

    #@ce
    .line 182
    iget v6, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->audioSamplingFrequency:I

    #@d0
    iput v6, p0, Landroid/media/videoeditor/AudioTrack;->mAudioSamplingFrequency:I

    #@d2
    .line 183
    iget v6, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->audioFormat:I

    #@d4
    iput v6, p0, Landroid/media/videoeditor/AudioTrack;->mAudioType:I

    #@d6
    .line 184
    sub-long v6, p8, p6

    #@d8
    iput-wide v6, p0, Landroid/media/videoeditor/AudioTrack;->mTimelineDurationMs:J

    #@da
    .line 185
    move/from16 v0, p11

    #@dc
    iput v0, p0, Landroid/media/videoeditor/AudioTrack;->mVolumePercent:I

    #@de
    .line 187
    move-wide/from16 v0, p6

    #@e0
    iput-wide v0, p0, Landroid/media/videoeditor/AudioTrack;->mBeginBoundaryTimeMs:J

    #@e2
    .line 188
    move-wide/from16 v0, p8

    #@e4
    iput-wide v0, p0, Landroid/media/videoeditor/AudioTrack;->mEndBoundaryTimeMs:J

    #@e6
    .line 190
    move/from16 v0, p10

    #@e8
    iput-boolean v0, p0, Landroid/media/videoeditor/AudioTrack;->mLoop:Z

    #@ea
    .line 191
    move/from16 v0, p12

    #@ec
    iput-boolean v0, p0, Landroid/media/videoeditor/AudioTrack;->mMuted:Z

    #@ee
    .line 192
    move/from16 v0, p13

    #@f0
    iput-boolean v0, p0, Landroid/media/videoeditor/AudioTrack;->mIsDuckingEnabled:Z

    #@f2
    .line 193
    move/from16 v0, p14

    #@f4
    iput v0, p0, Landroid/media/videoeditor/AudioTrack;->mDuckingThreshold:I

    #@f6
    .line 194
    move/from16 v0, p15

    #@f8
    iput v0, p0, Landroid/media/videoeditor/AudioTrack;->mDuckedTrackVolume:I

    #@fa
    .line 196
    move-object/from16 v0, p16

    #@fc
    iput-object v0, p0, Landroid/media/videoeditor/AudioTrack;->mAudioWaveformFilename:Ljava/lang/String;

    #@fe
    .line 197
    if-eqz p16, :cond_10f

    #@100
    .line 198
    new-instance v6, Ljava/lang/ref/SoftReference;

    #@102
    new-instance v7, Landroid/media/videoeditor/WaveformData;

    #@104
    move-object/from16 v0, p16

    #@106
    invoke-direct {v7, v0}, Landroid/media/videoeditor/WaveformData;-><init>(Ljava/lang/String;)V

    #@109
    invoke-direct {v6, v7}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    #@10c
    iput-object v6, p0, Landroid/media/videoeditor/AudioTrack;->mWaveformData:Ljava/lang/ref/SoftReference;

    #@10e
    .line 203
    :goto_10e
    return-void

    #@10f
    .line 201
    :cond_10f
    const/4 v6, 0x0

    #@110
    iput-object v6, p0, Landroid/media/videoeditor/AudioTrack;->mWaveformData:Ljava/lang/ref/SoftReference;

    #@112
    goto :goto_10e

    #@113
    .line 151
    nop

    #@114
    :pswitch_data_114
    .packed-switch 0x0
        :pswitch_9d
        :pswitch_9d
        :pswitch_9d
        :pswitch_9d
    .end packed-switch

    #@120
    .line 162
    :pswitch_data_120
    .packed-switch 0x1
        :pswitch_b0
        :pswitch_b0
        :pswitch_a8
        :pswitch_a8
        :pswitch_b0
        :pswitch_a8
        :pswitch_a8
        :pswitch_b0
    .end packed-switch
.end method


# virtual methods
.method public disableDucking()V
    .registers 3

    #@0
    .prologue
    .line 446
    iget-boolean v0, p0, Landroid/media/videoeditor/AudioTrack;->mIsDuckingEnabled:Z

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 450
    iget-object v0, p0, Landroid/media/videoeditor/AudioTrack;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@6
    const/4 v1, 0x1

    #@7
    invoke-virtual {v0, v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@a
    .line 451
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Landroid/media/videoeditor/AudioTrack;->mIsDuckingEnabled:Z

    #@d
    .line 453
    :cond_d
    return-void
.end method

.method public disableLoop()V
    .registers 3

    #@0
    .prologue
    .line 424
    iget-boolean v0, p0, Landroid/media/videoeditor/AudioTrack;->mLoop:Z

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 428
    iget-object v0, p0, Landroid/media/videoeditor/AudioTrack;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@6
    const/4 v1, 0x1

    #@7
    invoke-virtual {v0, v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@a
    .line 429
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Landroid/media/videoeditor/AudioTrack;->mLoop:Z

    #@d
    .line 431
    :cond_d
    return-void
.end method

.method public enableDucking(II)V
    .registers 6
    .parameter "threshold"
    .parameter "duckedTrackVolume"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 466
    if-ltz p1, :cond_7

    #@3
    const/16 v0, 0x5a

    #@5
    if-le p1, v0, :cond_20

    #@7
    .line 467
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v2, "Invalid threshold value: "

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0

    #@20
    .line 470
    :cond_20
    if-ltz p2, :cond_26

    #@22
    const/16 v0, 0x64

    #@24
    if-le p2, v0, :cond_3f

    #@26
    .line 471
    :cond_26
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@28
    new-instance v1, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v2, "Invalid duckedTrackVolume value: "

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v1

    #@3b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@3e
    throw v0

    #@3f
    .line 478
    :cond_3f
    iget-object v0, p0, Landroid/media/videoeditor/AudioTrack;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@41
    invoke-virtual {v0, v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@44
    .line 480
    iput p1, p0, Landroid/media/videoeditor/AudioTrack;->mDuckingThreshold:I

    #@46
    .line 481
    iput p2, p0, Landroid/media/videoeditor/AudioTrack;->mDuckedTrackVolume:I

    #@48
    .line 482
    iput-boolean v1, p0, Landroid/media/videoeditor/AudioTrack;->mIsDuckingEnabled:Z

    #@4a
    .line 483
    return-void
.end method

.method public enableLoop()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 411
    iget-boolean v0, p0, Landroid/media/videoeditor/AudioTrack;->mLoop:Z

    #@3
    if-nez v0, :cond_c

    #@5
    .line 415
    iget-object v0, p0, Landroid/media/videoeditor/AudioTrack;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@7
    invoke-virtual {v0, v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@a
    .line 416
    iput-boolean v1, p0, Landroid/media/videoeditor/AudioTrack;->mLoop:Z

    #@c
    .line 418
    :cond_c
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .parameter "object"

    #@0
    .prologue
    .line 645
    instance-of v0, p1, Landroid/media/videoeditor/AudioTrack;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 646
    const/4 v0, 0x0

    #@5
    .line 648
    .end local p1
    :goto_5
    return v0

    #@6
    .restart local p1
    :cond_6
    iget-object v0, p0, Landroid/media/videoeditor/AudioTrack;->mUniqueId:Ljava/lang/String;

    #@8
    check-cast p1, Landroid/media/videoeditor/AudioTrack;

    #@a
    .end local p1
    iget-object v1, p1, Landroid/media/videoeditor/AudioTrack;->mUniqueId:Ljava/lang/String;

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    goto :goto_5
.end method

.method public extractAudioWaveform(Landroid/media/videoeditor/ExtractAudioWaveformProgressListener;)V
    .registers 13
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 526
    iget-object v0, p0, Landroid/media/videoeditor/AudioTrack;->mAudioWaveformFilename:Ljava/lang/String;

    #@3
    if-nez v0, :cond_66

    #@5
    .line 530
    iget-object v0, p0, Landroid/media/videoeditor/AudioTrack;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@7
    invoke-virtual {v0}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getProjectPath()Ljava/lang/String;

    #@a
    move-result-object v10

    #@b
    .line 531
    .local v10, projectPath:Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v0

    #@14
    const-string v1, "/audioWaveformFile-"

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {p0}, Landroid/media/videoeditor/AudioTrack;->getId()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v0

    #@22
    const-string v1, ".dat"

    #@24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v0

    #@28
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v0

    #@2c
    new-array v1, v8, [Ljava/lang/Object;

    #@2e
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@31
    move-result-object v3

    #@32
    .line 540
    .local v3, audioWaveFilename:Ljava/lang/String;
    iget-object v0, p0, Landroid/media/videoeditor/AudioTrack;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@34
    iget v1, p0, Landroid/media/videoeditor/AudioTrack;->mAudioType:I

    #@36
    invoke-virtual {v0, v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getAudioCodecType(I)I

    #@39
    move-result v9

    #@3a
    .line 541
    .local v9, codecType:I
    packed-switch v9, :pswitch_data_84

    #@3d
    .line 571
    :pswitch_3d
    new-instance v0, Ljava/lang/IllegalStateException;

    #@3f
    new-instance v1, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v2, "Unsupported codec type: "

    #@46
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v1

    #@4e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v1

    #@52
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@55
    throw v0

    #@56
    .line 543
    :pswitch_56
    const/4 v4, 0x5

    #@57
    .line 545
    .local v4, frameDuration:I
    const/16 v6, 0xa0

    #@59
    .line 576
    .local v6, sampleCount:I
    :goto_59
    iget-object v0, p0, Landroid/media/videoeditor/AudioTrack;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@5b
    iget-object v1, p0, Landroid/media/videoeditor/AudioTrack;->mUniqueId:Ljava/lang/String;

    #@5d
    iget-object v2, p0, Landroid/media/videoeditor/AudioTrack;->mFilename:Ljava/lang/String;

    #@5f
    const/4 v5, 0x2

    #@60
    move-object v7, p1

    #@61
    invoke-virtual/range {v0 .. v8}, Landroid/media/videoeditor/MediaArtistNativeHelper;->generateAudioGraph(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILandroid/media/videoeditor/ExtractAudioWaveformProgressListener;Z)V

    #@64
    .line 587
    iput-object v3, p0, Landroid/media/videoeditor/AudioTrack;->mAudioWaveformFilename:Ljava/lang/String;

    #@66
    .line 589
    .end local v3           #audioWaveFilename:Ljava/lang/String;
    .end local v4           #frameDuration:I
    .end local v6           #sampleCount:I
    .end local v9           #codecType:I
    .end local v10           #projectPath:Ljava/lang/String;
    :cond_66
    new-instance v0, Ljava/lang/ref/SoftReference;

    #@68
    new-instance v1, Landroid/media/videoeditor/WaveformData;

    #@6a
    iget-object v2, p0, Landroid/media/videoeditor/AudioTrack;->mAudioWaveformFilename:Ljava/lang/String;

    #@6c
    invoke-direct {v1, v2}, Landroid/media/videoeditor/WaveformData;-><init>(Ljava/lang/String;)V

    #@6f
    invoke-direct {v0, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    #@72
    iput-object v0, p0, Landroid/media/videoeditor/AudioTrack;->mWaveformData:Ljava/lang/ref/SoftReference;

    #@74
    .line 590
    return-void

    #@75
    .line 550
    .restart local v3       #audioWaveFilename:Ljava/lang/String;
    .restart local v9       #codecType:I
    .restart local v10       #projectPath:Ljava/lang/String;
    :pswitch_75
    const/16 v4, 0xa

    #@77
    .line 552
    .restart local v4       #frameDuration:I
    const/16 v6, 0x140

    #@79
    .line 553
    .restart local v6       #sampleCount:I
    goto :goto_59

    #@7a
    .line 557
    .end local v4           #frameDuration:I
    .end local v6           #sampleCount:I
    :pswitch_7a
    const/16 v4, 0x20

    #@7c
    .line 559
    .restart local v4       #frameDuration:I
    const/16 v6, 0x400

    #@7e
    .line 560
    .restart local v6       #sampleCount:I
    goto :goto_59

    #@7f
    .line 564
    .end local v4           #frameDuration:I
    .end local v6           #sampleCount:I
    :pswitch_7f
    const/16 v4, 0x24

    #@81
    .line 566
    .restart local v4       #frameDuration:I
    const/16 v6, 0x480

    #@83
    .line 567
    .restart local v6       #sampleCount:I
    goto :goto_59

    #@84
    .line 541
    :pswitch_data_84
    .packed-switch 0x1
        :pswitch_56
        :pswitch_7a
        :pswitch_3d
        :pswitch_3d
        :pswitch_7f
        :pswitch_3d
        :pswitch_3d
        :pswitch_75
    .end packed-switch
.end method

.method public getAudioBitrate()I
    .registers 2

    #@0
    .prologue
    .line 257
    iget v0, p0, Landroid/media/videoeditor/AudioTrack;->mAudioBitrate:I

    #@2
    return v0
.end method

.method public getAudioChannels()I
    .registers 2

    #@0
    .prologue
    .line 229
    iget v0, p0, Landroid/media/videoeditor/AudioTrack;->mAudioChannels:I

    #@2
    return v0
.end method

.method public getAudioSamplingFrequency()I
    .registers 2

    #@0
    .prologue
    .line 248
    iget v0, p0, Landroid/media/videoeditor/AudioTrack;->mAudioSamplingFrequency:I

    #@2
    return v0
.end method

.method public getAudioType()I
    .registers 2

    #@0
    .prologue
    .line 239
    iget v0, p0, Landroid/media/videoeditor/AudioTrack;->mAudioType:I

    #@2
    return v0
.end method

.method getAudioWaveformFilename()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 598
    iget-object v0, p0, Landroid/media/videoeditor/AudioTrack;->mAudioWaveformFilename:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getBoundaryBeginTime()J
    .registers 3

    #@0
    .prologue
    .line 392
    iget-wide v0, p0, Landroid/media/videoeditor/AudioTrack;->mBeginBoundaryTimeMs:J

    #@2
    return-wide v0
.end method

.method public getBoundaryEndTime()J
    .registers 3

    #@0
    .prologue
    .line 401
    iget-wide v0, p0, Landroid/media/videoeditor/AudioTrack;->mEndBoundaryTimeMs:J

    #@2
    return-wide v0
.end method

.method public getDuckedTrackVolume()I
    .registers 2

    #@0
    .prologue
    .line 509
    iget v0, p0, Landroid/media/videoeditor/AudioTrack;->mDuckedTrackVolume:I

    #@2
    return v0
.end method

.method public getDuckingThreshhold()I
    .registers 2

    #@0
    .prologue
    .line 500
    iget v0, p0, Landroid/media/videoeditor/AudioTrack;->mDuckingThreshold:I

    #@2
    return v0
.end method

.method public getDuration()J
    .registers 3

    #@0
    .prologue
    .line 341
    iget-wide v0, p0, Landroid/media/videoeditor/AudioTrack;->mDurationMs:J

    #@2
    return-wide v0
.end method

.method public getFilename()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 220
    iget-object v0, p0, Landroid/media/videoeditor/AudioTrack;->mFilename:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 211
    iget-object v0, p0, Landroid/media/videoeditor/AudioTrack;->mUniqueId:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getStartTime()J
    .registers 3

    #@0
    .prologue
    .line 330
    iget-wide v0, p0, Landroid/media/videoeditor/AudioTrack;->mStartTimeMs:J

    #@2
    return-wide v0
.end method

.method public getTimelineDuration()J
    .registers 3

    #@0
    .prologue
    .line 350
    iget-wide v0, p0, Landroid/media/videoeditor/AudioTrack;->mTimelineDurationMs:J

    #@2
    return-wide v0
.end method

.method public getVolume()I
    .registers 2

    #@0
    .prologue
    .line 296
    iget v0, p0, Landroid/media/videoeditor/AudioTrack;->mVolumePercent:I

    #@2
    return v0
.end method

.method public getWaveformData()Landroid/media/videoeditor/WaveformData;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 620
    iget-object v3, p0, Landroid/media/videoeditor/AudioTrack;->mWaveformData:Ljava/lang/ref/SoftReference;

    #@3
    if-nez v3, :cond_7

    #@5
    move-object v1, v2

    #@6
    .line 636
    :cond_6
    :goto_6
    return-object v1

    #@7
    .line 624
    :cond_7
    iget-object v3, p0, Landroid/media/videoeditor/AudioTrack;->mWaveformData:Ljava/lang/ref/SoftReference;

    #@9
    invoke-virtual {v3}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    #@c
    move-result-object v1

    #@d
    check-cast v1, Landroid/media/videoeditor/WaveformData;

    #@f
    .line 625
    .local v1, waveformData:Landroid/media/videoeditor/WaveformData;
    if-nez v1, :cond_6

    #@11
    .line 627
    iget-object v3, p0, Landroid/media/videoeditor/AudioTrack;->mAudioWaveformFilename:Ljava/lang/String;

    #@13
    if-eqz v3, :cond_26

    #@15
    .line 629
    :try_start_15
    new-instance v1, Landroid/media/videoeditor/WaveformData;

    #@17
    .end local v1           #waveformData:Landroid/media/videoeditor/WaveformData;
    iget-object v2, p0, Landroid/media/videoeditor/AudioTrack;->mAudioWaveformFilename:Ljava/lang/String;

    #@19
    invoke-direct {v1, v2}, Landroid/media/videoeditor/WaveformData;-><init>(Ljava/lang/String;)V
    :try_end_1c
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_1c} :catch_24

    #@1c
    .line 633
    .restart local v1       #waveformData:Landroid/media/videoeditor/WaveformData;
    new-instance v2, Ljava/lang/ref/SoftReference;

    #@1e
    invoke-direct {v2, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    #@21
    iput-object v2, p0, Landroid/media/videoeditor/AudioTrack;->mWaveformData:Ljava/lang/ref/SoftReference;

    #@23
    goto :goto_6

    #@24
    .line 630
    .end local v1           #waveformData:Landroid/media/videoeditor/WaveformData;
    :catch_24
    move-exception v0

    #@25
    .line 631
    .local v0, e:Ljava/io/IOException;
    throw v0

    #@26
    .end local v0           #e:Ljava/io/IOException;
    .restart local v1       #waveformData:Landroid/media/videoeditor/WaveformData;
    :cond_26
    move-object v1, v2

    #@27
    .line 636
    goto :goto_6
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 656
    iget-object v0, p0, Landroid/media/videoeditor/AudioTrack;->mUniqueId:Ljava/lang/String;

    #@2
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method invalidate()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 605
    iget-object v0, p0, Landroid/media/videoeditor/AudioTrack;->mAudioWaveformFilename:Ljava/lang/String;

    #@3
    if-eqz v0, :cond_13

    #@5
    .line 606
    new-instance v0, Ljava/io/File;

    #@7
    iget-object v1, p0, Landroid/media/videoeditor/AudioTrack;->mAudioWaveformFilename:Ljava/lang/String;

    #@9
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@c
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@f
    .line 607
    iput-object v2, p0, Landroid/media/videoeditor/AudioTrack;->mAudioWaveformFilename:Ljava/lang/String;

    #@11
    .line 608
    iput-object v2, p0, Landroid/media/videoeditor/AudioTrack;->mWaveformData:Ljava/lang/ref/SoftReference;

    #@13
    .line 610
    :cond_13
    return-void
.end method

.method public isDuckingEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 491
    iget-boolean v0, p0, Landroid/media/videoeditor/AudioTrack;->mIsDuckingEnabled:Z

    #@2
    return v0
.end method

.method public isLooping()Z
    .registers 2

    #@0
    .prologue
    .line 439
    iget-boolean v0, p0, Landroid/media/videoeditor/AudioTrack;->mLoop:Z

    #@2
    return v0
.end method

.method public isMuted()Z
    .registers 2

    #@0
    .prologue
    .line 319
    iget-boolean v0, p0, Landroid/media/videoeditor/AudioTrack;->mMuted:Z

    #@2
    return v0
.end method

.method public setExtractBoundaries(JJ)V
    .registers 9
    .parameter "beginMs"
    .parameter "endMs"

    #@0
    .prologue
    const-wide/16 v2, 0x0

    #@2
    .line 362
    iget-wide v0, p0, Landroid/media/videoeditor/AudioTrack;->mDurationMs:J

    #@4
    cmp-long v0, p1, v0

    #@6
    if-lez v0, :cond_10

    #@8
    .line 363
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@a
    const-string v1, "Invalid start time"

    #@c
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 365
    :cond_10
    iget-wide v0, p0, Landroid/media/videoeditor/AudioTrack;->mDurationMs:J

    #@12
    cmp-long v0, p3, v0

    #@14
    if-lez v0, :cond_1e

    #@16
    .line 366
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@18
    const-string v1, "Invalid end time"

    #@1a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v0

    #@1e
    .line 368
    :cond_1e
    cmp-long v0, p1, v2

    #@20
    if-gez v0, :cond_2a

    #@22
    .line 369
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@24
    const-string v1, "Invalid start time; is < 0"

    #@26
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@29
    throw v0

    #@2a
    .line 371
    :cond_2a
    cmp-long v0, p3, v2

    #@2c
    if-gez v0, :cond_36

    #@2e
    .line 372
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@30
    const-string v1, "Invalid end time; is < 0"

    #@32
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@35
    throw v0

    #@36
    .line 378
    :cond_36
    iget-object v0, p0, Landroid/media/videoeditor/AudioTrack;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@38
    const/4 v1, 0x1

    #@39
    invoke-virtual {v0, v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@3c
    .line 380
    iput-wide p1, p0, Landroid/media/videoeditor/AudioTrack;->mBeginBoundaryTimeMs:J

    #@3e
    .line 381
    iput-wide p3, p0, Landroid/media/videoeditor/AudioTrack;->mEndBoundaryTimeMs:J

    #@40
    .line 383
    iget-wide v0, p0, Landroid/media/videoeditor/AudioTrack;->mEndBoundaryTimeMs:J

    #@42
    iget-wide v2, p0, Landroid/media/videoeditor/AudioTrack;->mBeginBoundaryTimeMs:J

    #@44
    sub-long/2addr v0, v2

    #@45
    iput-wide v0, p0, Landroid/media/videoeditor/AudioTrack;->mTimelineDurationMs:J

    #@47
    .line 384
    return-void
.end method

.method public setMute(Z)V
    .registers 4
    .parameter "muted"

    #@0
    .prologue
    .line 309
    iget-object v0, p0, Landroid/media/videoeditor/AudioTrack;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {v0, v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@6
    .line 310
    iput-boolean p1, p0, Landroid/media/videoeditor/AudioTrack;->mMuted:Z

    #@8
    .line 311
    return-void
.end method

.method public setVolume(I)V
    .registers 4
    .parameter "volumePercent"

    #@0
    .prologue
    .line 273
    const/16 v0, 0x64

    #@2
    if-le p1, v0, :cond_c

    #@4
    .line 274
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@6
    const-string v1, "Volume set exceeds maximum allowed value"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 277
    :cond_c
    if-gez p1, :cond_16

    #@e
    .line 278
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@10
    const-string v1, "Invalid Volume "

    #@12
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@15
    throw v0

    #@16
    .line 284
    :cond_16
    iget-object v0, p0, Landroid/media/videoeditor/AudioTrack;->mMANativeHelper:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@18
    const/4 v1, 0x1

    #@19
    invoke-virtual {v0, v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->setGeneratePreview(Z)V

    #@1c
    .line 286
    iput p1, p0, Landroid/media/videoeditor/AudioTrack;->mVolumePercent:I

    #@1e
    .line 287
    return-void
.end method
