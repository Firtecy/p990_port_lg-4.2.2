.class Landroid/media/videoeditor/MediaArtistNativeHelper;
.super Ljava/lang/Object;
.source "MediaArtistNativeHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/videoeditor/MediaArtistNativeHelper$NativeGetPixelsListCallback;,
        Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;,
        Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;,
        Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;,
        Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewSettings;,
        Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;,
        Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClips;,
        Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;,
        Landroid/media/videoeditor/MediaArtistNativeHelper$AudioEffect;,
        Landroid/media/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;,
        Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionBehaviour;,
        Landroid/media/videoeditor/MediaArtistNativeHelper$AudioTransition;,
        Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;,
        Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;,
        Landroid/media/videoeditor/MediaArtistNativeHelper$SlideTransitionSettings;,
        Landroid/media/videoeditor/MediaArtistNativeHelper$SlideDirection;,
        Landroid/media/videoeditor/MediaArtistNativeHelper$AlphaMagicSettings;,
        Landroid/media/videoeditor/MediaArtistNativeHelper$VideoTransition;,
        Landroid/media/videoeditor/MediaArtistNativeHelper$VideoEffect;,
        Landroid/media/videoeditor/MediaArtistNativeHelper$VideoFrameRate;,
        Landroid/media/videoeditor/MediaArtistNativeHelper$VideoFrameSize;,
        Landroid/media/videoeditor/MediaArtistNativeHelper$VideoFormat;,
        Landroid/media/videoeditor/MediaArtistNativeHelper$Result;,
        Landroid/media/videoeditor/MediaArtistNativeHelper$MediaRendering;,
        Landroid/media/videoeditor/MediaArtistNativeHelper$FileType;,
        Landroid/media/videoeditor/MediaArtistNativeHelper$Bitrate;,
        Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSamplingFrequency;,
        Landroid/media/videoeditor/MediaArtistNativeHelper$AudioFormat;,
        Landroid/media/videoeditor/MediaArtistNativeHelper$Version;,
        Landroid/media/videoeditor/MediaArtistNativeHelper$OnProgressUpdateListener;
    }
.end annotation


# static fields
.field private static final AUDIO_TRACK_PCM_FILE:Ljava/lang/String; = "AudioPcm.pcm"

.field private static final MAX_THUMBNAIL_PERMITTED:I = 0x8

.field public static final PROCESSING_AUDIO_PCM:I = 0x1

.field public static final PROCESSING_EXPORT:I = 0x14

.field public static final PROCESSING_INTERMEDIATE1:I = 0xb

.field public static final PROCESSING_INTERMEDIATE2:I = 0xc

.field public static final PROCESSING_INTERMEDIATE3:I = 0xd

.field public static final PROCESSING_KENBURNS:I = 0x3

.field public static final PROCESSING_NONE:I = 0x0

.field public static final PROCESSING_TRANSITION:I = 0x2

.field private static final TAG:Ljava/lang/String; = "MediaArtistNativeHelper"

.field public static final TASK_ENCODING:I = 0x2

.field public static final TASK_LOADING_SETTINGS:I = 0x1

.field private static final sResizePaint:Landroid/graphics/Paint;


# instance fields
.field private mAudioSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;

.field private mAudioTrack:Landroid/media/videoeditor/AudioTrack;

.field private mAudioTrackPCMFilePath:Ljava/lang/String;

.field private mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

.field private mErrorFlagSet:Z

.field private mExportAudioCodec:I

.field private mExportFilename:Ljava/lang/String;

.field private mExportProgressListener:Landroid/media/videoeditor/VideoEditor$ExportProgressListener;

.field private mExportVideoCodec:I

.field private mExtractAudioWaveformProgressListener:Landroid/media/videoeditor/ExtractAudioWaveformProgressListener;

.field private mInvalidatePreviewArray:Z

.field private mIsFirstProgress:Z

.field private final mLock:Ljava/util/concurrent/Semaphore;

.field private mManualEditContext:I

.field private mMediaProcessingProgressListener:Landroid/media/videoeditor/VideoEditor$MediaProcessingProgressListener;

.field private mOutputFilename:Ljava/lang/String;

.field private mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

.field private mPreviewProgress:J

.field private mPreviewProgressListener:Landroid/media/videoeditor/VideoEditor$PreviewProgressListener;

.field private mProcessingObject:Ljava/lang/Object;

.field private mProcessingState:I

.field private mProgressToApp:I

.field private final mProjectPath:Ljava/lang/String;

.field private mRegenerateAudio:Z

.field private mRenderPreviewOverlayFile:Ljava/lang/String;

.field private mRenderPreviewRenderingMode:I

.field private mStoryBoardSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

.field private mTotalClips:I

.field private final mVideoEditor:Landroid/media/videoeditor/VideoEditor;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 47
    const-string/jumbo v0, "videoeditor_jni"

    #@3
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@6
    .line 58
    new-instance v0, Landroid/graphics/Paint;

    #@8
    const/4 v1, 0x2

    #@9
    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    #@c
    sput-object v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->sResizePaint:Landroid/graphics/Paint;

    #@e
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/concurrent/Semaphore;Landroid/media/videoeditor/VideoEditor;)V
    .registers 7
    .parameter "projectPath"
    .parameter "lock"
    .parameter "veObj"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    const/4 v2, 0x0

    #@3
    .line 1724
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 70
    iput-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@8
    .line 74
    iput-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;

    #@a
    .line 76
    iput-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrack:Landroid/media/videoeditor/AudioTrack;

    #@c
    .line 78
    iput-boolean v1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mInvalidatePreviewArray:Z

    #@e
    .line 80
    iput-boolean v1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mRegenerateAudio:Z

    #@10
    .line 82
    iput-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mExportFilename:Ljava/lang/String;

    #@12
    .line 83
    iput v0, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mExportVideoCodec:I

    #@14
    .line 84
    iput v0, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mExportAudioCodec:I

    #@16
    .line 116
    iput v0, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mTotalClips:I

    #@18
    .line 118
    iput-boolean v0, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mErrorFlagSet:Z

    #@1a
    .line 1725
    iput-object p1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProjectPath:Ljava/lang/String;

    #@1c
    .line 1726
    if-eqz p3, :cond_38

    #@1e
    .line 1727
    iput-object p3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    #@20
    .line 1732
    iget-object v0, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mStoryBoardSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@22
    if-nez v0, :cond_2b

    #@24
    .line 1733
    new-instance v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@26
    invoke-direct {v0}, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;-><init>()V

    #@29
    iput-object v0, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mStoryBoardSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@2b
    .line 1736
    :cond_2b
    iput-object p2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mLock:Ljava/util/concurrent/Semaphore;

    #@2d
    .line 1738
    iget-object v0, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProjectPath:Ljava/lang/String;

    #@2f
    const-string/jumbo v1, "null"

    #@32
    invoke-direct {p0, v0, v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->_init(Ljava/lang/String;Ljava/lang/String;)V

    #@35
    .line 1739
    iput-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrackPCMFilePath:Ljava/lang/String;

    #@37
    .line 1740
    return-void

    #@38
    .line 1729
    :cond_38
    iput-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    #@3a
    .line 1730
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@3c
    const-string/jumbo v1, "video editor object is null"

    #@3f
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@42
    throw v0
.end method

.method private native _init(Ljava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method static synthetic access$000()Landroid/graphics/Paint;
    .registers 1

    #@0
    .prologue
    .line 43
    sget-object v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->sResizePaint:Landroid/graphics/Paint;

    #@2
    return-object v0
.end method

.method private adjustMediaItemBoundary(Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;Landroid/media/videoeditor/MediaItem;)V
    .registers 8
    .parameter "clipSettings"
    .parameter "clipProperties"
    .parameter "m"

    #@0
    .prologue
    const-wide/16 v2, 0x0

    #@2
    .line 2574
    invoke-virtual {p3}, Landroid/media/videoeditor/MediaItem;->getBeginTransition()Landroid/media/videoeditor/Transition;

    #@5
    move-result-object v0

    #@6
    if-eqz v0, :cond_62

    #@8
    invoke-virtual {p3}, Landroid/media/videoeditor/MediaItem;->getBeginTransition()Landroid/media/videoeditor/Transition;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@f
    move-result-wide v0

    #@10
    cmp-long v0, v0, v2

    #@12
    if-lez v0, :cond_62

    #@14
    invoke-virtual {p3}, Landroid/media/videoeditor/MediaItem;->getEndTransition()Landroid/media/videoeditor/Transition;

    #@17
    move-result-object v0

    #@18
    if-eqz v0, :cond_62

    #@1a
    invoke-virtual {p3}, Landroid/media/videoeditor/MediaItem;->getEndTransition()Landroid/media/videoeditor/Transition;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@21
    move-result-wide v0

    #@22
    cmp-long v0, v0, v2

    #@24
    if-lez v0, :cond_62

    #@26
    .line 2576
    iget v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->beginCutTime:I

    #@28
    int-to-long v0, v0

    #@29
    invoke-virtual {p3}, Landroid/media/videoeditor/MediaItem;->getBeginTransition()Landroid/media/videoeditor/Transition;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@30
    move-result-wide v2

    #@31
    add-long/2addr v0, v2

    #@32
    long-to-int v0, v0

    #@33
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->beginCutTime:I

    #@35
    .line 2577
    iget v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutTime:I

    #@37
    int-to-long v0, v0

    #@38
    invoke-virtual {p3}, Landroid/media/videoeditor/MediaItem;->getEndTransition()Landroid/media/videoeditor/Transition;

    #@3b
    move-result-object v2

    #@3c
    invoke-virtual {v2}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@3f
    move-result-wide v2

    #@40
    sub-long/2addr v0, v2

    #@41
    long-to-int v0, v0

    #@42
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutTime:I

    #@44
    .line 2586
    :cond_44
    :goto_44
    iget v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutTime:I

    #@46
    iget v1, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->beginCutTime:I

    #@48
    sub-int/2addr v0, v1

    #@49
    iput v0, p2, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->duration:I

    #@4b
    .line 2588
    iget v0, p2, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->videoDuration:I

    #@4d
    if-eqz v0, :cond_56

    #@4f
    .line 2589
    iget v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutTime:I

    #@51
    iget v1, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->beginCutTime:I

    #@53
    sub-int/2addr v0, v1

    #@54
    iput v0, p2, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->videoDuration:I

    #@56
    .line 2592
    :cond_56
    iget v0, p2, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->audioDuration:I

    #@58
    if-eqz v0, :cond_61

    #@5a
    .line 2593
    iget v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutTime:I

    #@5c
    iget v1, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->beginCutTime:I

    #@5e
    sub-int/2addr v0, v1

    #@5f
    iput v0, p2, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->audioDuration:I

    #@61
    .line 2595
    :cond_61
    return-void

    #@62
    .line 2578
    :cond_62
    invoke-virtual {p3}, Landroid/media/videoeditor/MediaItem;->getBeginTransition()Landroid/media/videoeditor/Transition;

    #@65
    move-result-object v0

    #@66
    if-nez v0, :cond_8a

    #@68
    invoke-virtual {p3}, Landroid/media/videoeditor/MediaItem;->getEndTransition()Landroid/media/videoeditor/Transition;

    #@6b
    move-result-object v0

    #@6c
    if-eqz v0, :cond_8a

    #@6e
    invoke-virtual {p3}, Landroid/media/videoeditor/MediaItem;->getEndTransition()Landroid/media/videoeditor/Transition;

    #@71
    move-result-object v0

    #@72
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@75
    move-result-wide v0

    #@76
    cmp-long v0, v0, v2

    #@78
    if-lez v0, :cond_8a

    #@7a
    .line 2580
    iget v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutTime:I

    #@7c
    int-to-long v0, v0

    #@7d
    invoke-virtual {p3}, Landroid/media/videoeditor/MediaItem;->getEndTransition()Landroid/media/videoeditor/Transition;

    #@80
    move-result-object v2

    #@81
    invoke-virtual {v2}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@84
    move-result-wide v2

    #@85
    sub-long/2addr v0, v2

    #@86
    long-to-int v0, v0

    #@87
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutTime:I

    #@89
    goto :goto_44

    #@8a
    .line 2581
    :cond_8a
    invoke-virtual {p3}, Landroid/media/videoeditor/MediaItem;->getEndTransition()Landroid/media/videoeditor/Transition;

    #@8d
    move-result-object v0

    #@8e
    if-nez v0, :cond_44

    #@90
    invoke-virtual {p3}, Landroid/media/videoeditor/MediaItem;->getBeginTransition()Landroid/media/videoeditor/Transition;

    #@93
    move-result-object v0

    #@94
    if-eqz v0, :cond_44

    #@96
    invoke-virtual {p3}, Landroid/media/videoeditor/MediaItem;->getBeginTransition()Landroid/media/videoeditor/Transition;

    #@99
    move-result-object v0

    #@9a
    invoke-virtual {v0}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@9d
    move-result-wide v0

    #@9e
    cmp-long v0, v0, v2

    #@a0
    if-lez v0, :cond_44

    #@a2
    .line 2583
    iget v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->beginCutTime:I

    #@a4
    int-to-long v0, v0

    #@a5
    invoke-virtual {p3}, Landroid/media/videoeditor/MediaItem;->getBeginTransition()Landroid/media/videoeditor/Transition;

    #@a8
    move-result-object v2

    #@a9
    invoke-virtual {v2}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@ac
    move-result-wide v2

    #@ad
    add-long/2addr v0, v2

    #@ae
    long-to-int v0, v0

    #@af
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->beginCutTime:I

    #@b1
    goto :goto_44
.end method

.method private adjustVolume(Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;I)V
    .registers 7
    .parameter "m"
    .parameter "clipProperties"
    .parameter "index"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2646
    instance-of v1, p1, Landroid/media/videoeditor/MediaVideoItem;

    #@3
    if-eqz v1, :cond_26

    #@5
    move-object v1, p1

    #@6
    .line 2647
    check-cast v1, Landroid/media/videoeditor/MediaVideoItem;

    #@8
    invoke-virtual {v1}, Landroid/media/videoeditor/MediaVideoItem;->isMuted()Z

    #@b
    move-result v0

    #@c
    .line 2648
    .local v0, videoMuted:Z
    if-nez v0, :cond_1d

    #@e
    .line 2649
    iget-object v1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@10
    iget-object v1, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@12
    aget-object v1, v1, p3

    #@14
    check-cast p1, Landroid/media/videoeditor/MediaVideoItem;

    #@16
    .end local p1
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaVideoItem;->getVolume()I

    #@19
    move-result v2

    #@1a
    iput v2, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->audioVolumeValue:I

    #@1c
    .line 2657
    .end local v0           #videoMuted:Z
    :cond_1c
    :goto_1c
    return-void

    #@1d
    .line 2652
    .restart local v0       #videoMuted:Z
    .restart local p1
    :cond_1d
    iget-object v1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@1f
    iget-object v1, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@21
    aget-object v1, v1, p3

    #@23
    iput v2, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->audioVolumeValue:I

    #@25
    goto :goto_1c

    #@26
    .line 2654
    .end local v0           #videoMuted:Z
    :cond_26
    instance-of v1, p1, Landroid/media/videoeditor/MediaImageItem;

    #@28
    if-eqz v1, :cond_1c

    #@2a
    .line 2655
    iget-object v1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@2c
    iget-object v1, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@2e
    aget-object v1, v1, p3

    #@30
    iput v2, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->audioVolumeValue:I

    #@32
    goto :goto_1c
.end method

.method private checkOddSizeImage(Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;I)V
    .registers 7
    .parameter "m"
    .parameter "clipProperties"
    .parameter "index"

    #@0
    .prologue
    .line 2667
    instance-of v2, p1, Landroid/media/videoeditor/MediaImageItem;

    #@2
    if-eqz v2, :cond_30

    #@4
    .line 2668
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@6
    iget-object v2, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@8
    aget-object v2, v2, p3

    #@a
    iget v1, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->width:I

    #@c
    .line 2669
    .local v1, width:I
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@e
    iget-object v2, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@10
    aget-object v2, v2, p3

    #@12
    iget v0, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->height:I

    #@14
    .line 2671
    .local v0, height:I
    rem-int/lit8 v2, v1, 0x2

    #@16
    if-eqz v2, :cond_1a

    #@18
    .line 2672
    add-int/lit8 v1, v1, -0x1

    #@1a
    .line 2674
    :cond_1a
    rem-int/lit8 v2, v0, 0x2

    #@1c
    if-eqz v2, :cond_20

    #@1e
    .line 2675
    add-int/lit8 v0, v0, -0x1

    #@20
    .line 2677
    :cond_20
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@22
    iget-object v2, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@24
    aget-object v2, v2, p3

    #@26
    iput v1, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->width:I

    #@28
    .line 2678
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@2a
    iget-object v2, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@2c
    aget-object v2, v2, p3

    #@2e
    iput v0, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->height:I

    #@30
    .line 2680
    .end local v0           #height:I
    .end local v1           #width:I
    :cond_30
    return-void
.end method

.method private findVideoBitrate(I)I
    .registers 3
    .parameter "videoFrameSize"

    #@0
    .prologue
    .line 3579
    packed-switch p1, :pswitch_data_18

    #@3
    .line 3599
    const v0, 0x7a1200

    #@6
    :goto_6
    return v0

    #@7
    .line 3583
    :pswitch_7
    const v0, 0x1f400

    #@a
    goto :goto_6

    #@b
    .line 3586
    :pswitch_b
    const v0, 0x5dc00

    #@e
    goto :goto_6

    #@f
    .line 3592
    :pswitch_f
    const v0, 0x1e8480

    #@12
    goto :goto_6

    #@13
    .line 3596
    :pswitch_13
    const v0, 0x4c4b40

    #@16
    goto :goto_6

    #@17
    .line 3579
    nop

    #@18
    :pswitch_data_18
    .packed-switch 0x0
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_b
        :pswitch_b
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_13
        :pswitch_13
        :pswitch_13
    .end packed-switch
.end method

.method private findVideoResolution(II)I
    .registers 8
    .parameter "aspectRatio"
    .parameter "height"

    #@0
    .prologue
    const/16 v4, 0x2d0

    #@2
    const/16 v3, 0x1e0

    #@4
    .line 3532
    const/4 v2, -0x1

    #@5
    .line 3533
    .local v2, retValue:I
    packed-switch p1, :pswitch_data_60

    #@8
    .line 3565
    :cond_8
    :goto_8
    const/4 v3, -0x1

    #@9
    if-ne v2, v3, :cond_2c

    #@b
    .line 3566
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    #@d
    invoke-interface {v3}, Landroid/media/videoeditor/VideoEditor;->getAspectRatio()I

    #@10
    move-result v3

    #@11
    invoke-static {v3}, Landroid/media/videoeditor/MediaProperties;->getSupportedResolutions(I)[Landroid/util/Pair;

    #@14
    move-result-object v1

    #@15
    .line 3568
    .local v1, resolutions:[Landroid/util/Pair;,"[Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    array-length v3, v1

    #@16
    add-int/lit8 v3, v3, -0x1

    #@18
    aget-object v0, v1, v3

    #@1a
    .line 3569
    .local v0, maxResolution:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    #@1c
    invoke-interface {v3}, Landroid/media/videoeditor/VideoEditor;->getAspectRatio()I

    #@1f
    move-result v4

    #@20
    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@22
    check-cast v3, Ljava/lang/Integer;

    #@24
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@27
    move-result v3

    #@28
    invoke-direct {p0, v4, v3}, Landroid/media/videoeditor/MediaArtistNativeHelper;->findVideoResolution(II)I

    #@2b
    move-result v2

    #@2c
    .line 3572
    .end local v0           #maxResolution:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .end local v1           #resolutions:[Landroid/util/Pair;,"[Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_2c
    return v2

    #@2d
    .line 3535
    :pswitch_2d
    if-ne p2, v3, :cond_31

    #@2f
    .line 3536
    const/4 v2, 0x7

    #@30
    goto :goto_8

    #@31
    .line 3537
    :cond_31
    if-ne p2, v4, :cond_8

    #@33
    .line 3538
    const/16 v2, 0xb

    #@35
    goto :goto_8

    #@36
    .line 3541
    :pswitch_36
    if-ne p2, v3, :cond_3b

    #@38
    .line 3542
    const/16 v2, 0x9

    #@3a
    goto :goto_8

    #@3b
    .line 3543
    :cond_3b
    if-ne p2, v4, :cond_40

    #@3d
    .line 3544
    const/16 v2, 0xa

    #@3f
    goto :goto_8

    #@40
    .line 3545
    :cond_40
    const/16 v3, 0x438

    #@42
    if-ne p2, v3, :cond_8

    #@44
    .line 3546
    const/16 v2, 0xd

    #@46
    goto :goto_8

    #@47
    .line 3549
    :pswitch_47
    if-ne p2, v3, :cond_4b

    #@49
    .line 3550
    const/4 v2, 0x5

    #@4a
    goto :goto_8

    #@4b
    .line 3551
    :cond_4b
    if-ne p2, v4, :cond_8

    #@4d
    .line 3552
    const/16 v2, 0xc

    #@4f
    goto :goto_8

    #@50
    .line 3555
    :pswitch_50
    if-ne p2, v3, :cond_8

    #@52
    .line 3556
    const/4 v2, 0x6

    #@53
    goto :goto_8

    #@54
    .line 3559
    :pswitch_54
    const/16 v3, 0x90

    #@56
    if-ne p2, v3, :cond_5a

    #@58
    .line 3560
    const/4 v2, 0x2

    #@59
    goto :goto_8

    #@5a
    .line 3561
    :cond_5a
    const/16 v3, 0x120

    #@5c
    if-ne p2, v3, :cond_8

    #@5e
    .line 3562
    const/4 v2, 0x4

    #@5f
    goto :goto_8

    #@60
    .line 3533
    :pswitch_data_60
    .packed-switch 0x1
        :pswitch_2d
        :pswitch_36
        :pswitch_47
        :pswitch_50
        :pswitch_54
    .end packed-switch
.end method

.method private generateTransition(Landroid/media/videoeditor/Transition;Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;I)V
    .registers 9
    .parameter "transition"
    .parameter "editSettings"
    .parameter "clipPropertiesArray"
    .parameter "index"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 2608
    invoke-virtual {p1}, Landroid/media/videoeditor/Transition;->isGenerated()Z

    #@4
    move-result v1

    #@5
    if-nez v1, :cond_a

    #@7
    .line 2609
    invoke-virtual {p1}, Landroid/media/videoeditor/Transition;->generate()V

    #@a
    .line 2611
    :cond_a
    iget-object v1, p2, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@c
    new-instance v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@e
    invoke-direct {v2}, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;-><init>()V

    #@11
    aput-object v2, v1, p4

    #@13
    .line 2612
    iget-object v1, p2, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@15
    aget-object v1, v1, p4

    #@17
    invoke-virtual {p1}, Landroid/media/videoeditor/Transition;->getFilename()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    iput-object v2, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipPath:Ljava/lang/String;

    #@1d
    .line 2613
    iget-object v1, p2, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@1f
    aget-object v1, v1, p4

    #@21
    iput v3, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->fileType:I

    #@23
    .line 2614
    iget-object v1, p2, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@25
    aget-object v1, v1, p4

    #@27
    iput v3, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->beginCutTime:I

    #@29
    .line 2615
    iget-object v1, p2, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@2b
    aget-object v1, v1, p4

    #@2d
    invoke-virtual {p1}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@30
    move-result-wide v2

    #@31
    long-to-int v2, v2

    #@32
    iput v2, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutTime:I

    #@34
    .line 2616
    iget-object v1, p2, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@36
    aget-object v1, v1, p4

    #@38
    const/4 v2, 0x2

    #@39
    iput v2, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->mediaRendering:I

    #@3b
    .line 2619
    :try_start_3b
    iget-object v1, p3, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@3d
    invoke-virtual {p1}, Landroid/media/videoeditor/Transition;->getFilename()Ljava/lang/String;

    #@40
    move-result-object v2

    #@41
    invoke-virtual {p0, v2}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getMediaProperties(Ljava/lang/String;)Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@44
    move-result-object v2

    #@45
    aput-object v2, v1, p4
    :try_end_47
    .catch Ljava/lang/Exception; {:try_start_3b .. :try_end_47} :catch_88

    #@47
    .line 2625
    iget-object v1, p3, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@49
    aget-object v1, v1, p4

    #@4b
    const/4 v2, 0x0

    #@4c
    iput-object v2, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->Id:Ljava/lang/String;

    #@4e
    .line 2626
    iget-object v1, p3, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@50
    aget-object v1, v1, p4

    #@52
    const/16 v2, 0x64

    #@54
    iput v2, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->audioVolumeValue:I

    #@56
    .line 2627
    iget-object v1, p3, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@58
    aget-object v1, v1, p4

    #@5a
    invoke-virtual {p1}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@5d
    move-result-wide v2

    #@5e
    long-to-int v2, v2

    #@5f
    iput v2, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->duration:I

    #@61
    .line 2628
    iget-object v1, p3, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@63
    aget-object v1, v1, p4

    #@65
    iget v1, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->videoDuration:I

    #@67
    if-eqz v1, :cond_74

    #@69
    .line 2629
    iget-object v1, p3, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@6b
    aget-object v1, v1, p4

    #@6d
    invoke-virtual {p1}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@70
    move-result-wide v2

    #@71
    long-to-int v2, v2

    #@72
    iput v2, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->videoDuration:I

    #@74
    .line 2632
    :cond_74
    iget-object v1, p3, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@76
    aget-object v1, v1, p4

    #@78
    iget v1, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->audioDuration:I

    #@7a
    if-eqz v1, :cond_87

    #@7c
    .line 2633
    iget-object v1, p3, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@7e
    aget-object v1, v1, p4

    #@80
    invoke-virtual {p1}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@83
    move-result-wide v2

    #@84
    long-to-int v2, v2

    #@85
    iput v2, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->audioDuration:I

    #@87
    .line 2635
    :cond_87
    return-void

    #@88
    .line 2621
    :catch_88
    move-exception v0

    #@89
    .line 2622
    .local v0, e:Ljava/lang/Exception;
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@8b
    const-string v2, "Unsupported file or file not found"

    #@8d
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@90
    throw v1
.end method

.method private getEffectColorType(Landroid/media/videoeditor/EffectColor;)I
    .registers 5
    .parameter "effect"

    #@0
    .prologue
    .line 3488
    const/4 v0, -0x1

    #@1
    .line 3489
    .local v0, retValue:I
    invoke-virtual {p1}, Landroid/media/videoeditor/EffectColor;->getType()I

    #@4
    move-result v1

    #@5
    packed-switch v1, :pswitch_data_3e

    #@8
    .line 3515
    const/4 v0, -0x1

    #@9
    .line 3517
    :goto_9
    return v0

    #@a
    .line 3491
    :pswitch_a
    invoke-virtual {p1}, Landroid/media/videoeditor/EffectColor;->getColor()I

    #@d
    move-result v1

    #@e
    const v2, 0xff00

    #@11
    if-ne v1, v2, :cond_16

    #@13
    .line 3492
    const/16 v0, 0x103

    #@15
    goto :goto_9

    #@16
    .line 3493
    :cond_16
    invoke-virtual {p1}, Landroid/media/videoeditor/EffectColor;->getColor()I

    #@19
    move-result v1

    #@1a
    const v2, 0xff66cc

    #@1d
    if-ne v1, v2, :cond_22

    #@1f
    .line 3494
    const/16 v0, 0x102

    #@21
    goto :goto_9

    #@22
    .line 3495
    :cond_22
    invoke-virtual {p1}, Landroid/media/videoeditor/EffectColor;->getColor()I

    #@25
    move-result v1

    #@26
    const v2, 0x7f7f7f

    #@29
    if-ne v1, v2, :cond_2e

    #@2b
    .line 3496
    const/16 v0, 0x101

    #@2d
    goto :goto_9

    #@2e
    .line 3498
    :cond_2e
    const/16 v0, 0x10b

    #@30
    .line 3500
    goto :goto_9

    #@31
    .line 3502
    :pswitch_31
    const/16 v0, 0x10c

    #@33
    .line 3503
    goto :goto_9

    #@34
    .line 3505
    :pswitch_34
    const/16 v0, 0x104

    #@36
    .line 3506
    goto :goto_9

    #@37
    .line 3508
    :pswitch_37
    const/16 v0, 0x105

    #@39
    .line 3509
    goto :goto_9

    #@3a
    .line 3511
    :pswitch_3a
    const/16 v0, 0x10a

    #@3c
    .line 3512
    goto :goto_9

    #@3d
    .line 3489
    nop

    #@3e
    :pswitch_data_3e
    .packed-switch 0x1
        :pswitch_a
        :pswitch_31
        :pswitch_34
        :pswitch_37
        :pswitch_3a
    .end packed-switch
.end method

.method private getTotalEffects(Ljava/util/List;)I
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/media/videoeditor/MediaItem;",
            ">;)I"
        }
    .end annotation

    #@0
    .prologue
    .line 2801
    .local p1, mediaItemsList:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/MediaItem;>;"
    const/4 v4, 0x0

    #@1
    .line 2802
    .local v4, totalEffects:I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@4
    move-result-object v2

    #@5
    .line 2803
    .local v2, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/videoeditor/MediaItem;>;"
    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@8
    move-result v5

    #@9
    if-eqz v5, :cond_3e

    #@b
    .line 2804
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@e
    move-result-object v3

    #@f
    check-cast v3, Landroid/media/videoeditor/MediaItem;

    #@11
    .line 2805
    .local v3, t:Landroid/media/videoeditor/MediaItem;
    invoke-virtual {v3}, Landroid/media/videoeditor/MediaItem;->getAllEffects()Ljava/util/List;

    #@14
    move-result-object v5

    #@15
    invoke-interface {v5}, Ljava/util/List;->size()I

    #@18
    move-result v5

    #@19
    add-int/2addr v4, v5

    #@1a
    .line 2806
    invoke-virtual {v3}, Landroid/media/videoeditor/MediaItem;->getAllOverlays()Ljava/util/List;

    #@1d
    move-result-object v5

    #@1e
    invoke-interface {v5}, Ljava/util/List;->size()I

    #@21
    move-result v5

    #@22
    add-int/2addr v4, v5

    #@23
    .line 2807
    invoke-virtual {v3}, Landroid/media/videoeditor/MediaItem;->getAllEffects()Ljava/util/List;

    #@26
    move-result-object v5

    #@27
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@2a
    move-result-object v1

    #@2b
    .line 2808
    .local v1, ef:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/videoeditor/Effect;>;"
    :cond_2b
    :goto_2b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@2e
    move-result v5

    #@2f
    if-eqz v5, :cond_5

    #@31
    .line 2809
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@34
    move-result-object v0

    #@35
    check-cast v0, Landroid/media/videoeditor/Effect;

    #@37
    .line 2810
    .local v0, e:Landroid/media/videoeditor/Effect;
    instance-of v5, v0, Landroid/media/videoeditor/EffectKenBurns;

    #@39
    if-eqz v5, :cond_2b

    #@3b
    .line 2811
    add-int/lit8 v4, v4, -0x1

    #@3d
    goto :goto_2b

    #@3e
    .line 2815
    .end local v0           #e:Landroid/media/videoeditor/Effect;
    .end local v1           #ef:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/videoeditor/Effect;>;"
    .end local v3           #t:Landroid/media/videoeditor/MediaItem;
    :cond_3e
    return v4
.end method

.method private getTransitionResolution(Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;)I
    .registers 7
    .parameter "m1"
    .parameter "m2"

    #@0
    .prologue
    .line 2426
    const/4 v0, 0x0

    #@1
    .line 2427
    .local v0, clip1Height:I
    const/4 v1, 0x0

    #@2
    .line 2428
    .local v1, clip2Height:I
    const/4 v2, 0x0

    #@3
    .line 2430
    .local v2, videoSize:I
    if-eqz p1, :cond_45

    #@5
    if-eqz p2, :cond_45

    #@7
    .line 2431
    instance-of v3, p1, Landroid/media/videoeditor/MediaVideoItem;

    #@9
    if-eqz v3, :cond_24

    #@b
    .line 2432
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getHeight()I

    #@e
    move-result v0

    #@f
    .line 2436
    .end local p1
    :cond_f
    :goto_f
    instance-of v3, p2, Landroid/media/videoeditor/MediaVideoItem;

    #@11
    if-eqz v3, :cond_2f

    #@13
    .line 2437
    invoke-virtual {p2}, Landroid/media/videoeditor/MediaItem;->getHeight()I

    #@16
    move-result v1

    #@17
    .line 2441
    .end local p2
    :cond_17
    :goto_17
    if-le v0, v1, :cond_3a

    #@19
    .line 2442
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    #@1b
    invoke-interface {v3}, Landroid/media/videoeditor/VideoEditor;->getAspectRatio()I

    #@1e
    move-result v3

    #@1f
    invoke-direct {p0, v3, v0}, Landroid/media/videoeditor/MediaArtistNativeHelper;->findVideoResolution(II)I

    #@22
    move-result v2

    #@23
    .line 2461
    :cond_23
    :goto_23
    return v2

    #@24
    .line 2433
    .restart local p1
    .restart local p2
    :cond_24
    instance-of v3, p1, Landroid/media/videoeditor/MediaImageItem;

    #@26
    if-eqz v3, :cond_f

    #@28
    .line 2434
    check-cast p1, Landroid/media/videoeditor/MediaImageItem;

    #@2a
    .end local p1
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaImageItem;->getScaledHeight()I

    #@2d
    move-result v0

    #@2e
    goto :goto_f

    #@2f
    .line 2438
    :cond_2f
    instance-of v3, p2, Landroid/media/videoeditor/MediaImageItem;

    #@31
    if-eqz v3, :cond_17

    #@33
    .line 2439
    check-cast p2, Landroid/media/videoeditor/MediaImageItem;

    #@35
    .end local p2
    invoke-virtual {p2}, Landroid/media/videoeditor/MediaImageItem;->getScaledHeight()I

    #@38
    move-result v1

    #@39
    goto :goto_17

    #@3a
    .line 2444
    :cond_3a
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    #@3c
    invoke-interface {v3}, Landroid/media/videoeditor/VideoEditor;->getAspectRatio()I

    #@3f
    move-result v3

    #@40
    invoke-direct {p0, v3, v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->findVideoResolution(II)I

    #@43
    move-result v2

    #@44
    goto :goto_23

    #@45
    .line 2446
    .restart local p1
    .restart local p2
    :cond_45
    if-nez p1, :cond_67

    #@47
    if-eqz p2, :cond_67

    #@49
    .line 2447
    instance-of v3, p2, Landroid/media/videoeditor/MediaVideoItem;

    #@4b
    if-eqz v3, :cond_5c

    #@4d
    .line 2448
    invoke-virtual {p2}, Landroid/media/videoeditor/MediaItem;->getHeight()I

    #@50
    move-result v1

    #@51
    .line 2452
    .end local p2
    :cond_51
    :goto_51
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    #@53
    invoke-interface {v3}, Landroid/media/videoeditor/VideoEditor;->getAspectRatio()I

    #@56
    move-result v3

    #@57
    invoke-direct {p0, v3, v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->findVideoResolution(II)I

    #@5a
    move-result v2

    #@5b
    goto :goto_23

    #@5c
    .line 2449
    .restart local p2
    :cond_5c
    instance-of v3, p2, Landroid/media/videoeditor/MediaImageItem;

    #@5e
    if-eqz v3, :cond_51

    #@60
    .line 2450
    check-cast p2, Landroid/media/videoeditor/MediaImageItem;

    #@62
    .end local p2
    invoke-virtual {p2}, Landroid/media/videoeditor/MediaImageItem;->getScaledHeight()I

    #@65
    move-result v1

    #@66
    goto :goto_51

    #@67
    .line 2453
    .restart local p2
    :cond_67
    if-eqz p1, :cond_23

    #@69
    if-nez p2, :cond_23

    #@6b
    .line 2454
    instance-of v3, p1, Landroid/media/videoeditor/MediaVideoItem;

    #@6d
    if-eqz v3, :cond_7e

    #@6f
    .line 2455
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getHeight()I

    #@72
    move-result v0

    #@73
    .line 2459
    .end local p1
    :cond_73
    :goto_73
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    #@75
    invoke-interface {v3}, Landroid/media/videoeditor/VideoEditor;->getAspectRatio()I

    #@78
    move-result v3

    #@79
    invoke-direct {p0, v3, v0}, Landroid/media/videoeditor/MediaArtistNativeHelper;->findVideoResolution(II)I

    #@7c
    move-result v2

    #@7d
    goto :goto_23

    #@7e
    .line 2456
    .restart local p1
    :cond_7e
    instance-of v3, p1, Landroid/media/videoeditor/MediaImageItem;

    #@80
    if-eqz v3, :cond_73

    #@82
    .line 2457
    check-cast p1, Landroid/media/videoeditor/MediaImageItem;

    #@84
    .end local p1
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaImageItem;->getScaledHeight()I

    #@87
    move-result v0

    #@88
    goto :goto_73
.end method

.method private static native getVersion()Landroid/media/videoeditor/MediaArtistNativeHelper$Version;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method private lock()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x3

    #@1
    .line 3929
    const-string v0, "MediaArtistNativeHelper"

    #@3
    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_16

    #@9
    .line 3930
    const-string v0, "MediaArtistNativeHelper"

    #@b
    const-string/jumbo v1, "lock: grabbing semaphore"

    #@e
    new-instance v2, Ljava/lang/Throwable;

    #@10
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@13
    invoke-static {v0, v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16
    .line 3932
    :cond_16
    iget-object v0, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mLock:Ljava/util/concurrent/Semaphore;

    #@18
    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquire()V

    #@1b
    .line 3933
    const-string v0, "MediaArtistNativeHelper"

    #@1d
    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@20
    move-result v0

    #@21
    if-eqz v0, :cond_2b

    #@23
    .line 3934
    const-string v0, "MediaArtistNativeHelper"

    #@25
    const-string/jumbo v1, "lock: grabbed semaphore"

    #@28
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 3936
    :cond_2b
    return-void
.end method

.method private native nativeClearSurface(Landroid/view/Surface;)V
.end method

.method private native nativeGenerateAudioGraph(Ljava/lang/String;Ljava/lang/String;III)I
.end method

.method private native nativeGenerateClip(Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method private native nativeGenerateRawAudio(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method private native nativeGetPixels(Ljava/lang/String;[IIIJ)I
.end method

.method private native nativeGetPixelsList(Ljava/lang/String;[IIIIJJ[ILandroid/media/videoeditor/MediaArtistNativeHelper$NativeGetPixelsListCallback;)I
.end method

.method private native nativePopulateSettings(Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method private native nativeRenderMediaItemPreviewFrame(Landroid/view/Surface;Ljava/lang/String;IIIIJ)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method private native nativeRenderPreviewFrame(Landroid/view/Surface;JII)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method private native nativeStartPreview(Landroid/view/Surface;JJIZ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method private native nativeStopPreview()I
.end method

.method private onAudioGraphExtractProgressUpdate(IZ)V
    .registers 4
    .parameter "progress"
    .parameter "isVideo"

    #@0
    .prologue
    .line 1893
    iget-object v0, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mExtractAudioWaveformProgressListener:Landroid/media/videoeditor/ExtractAudioWaveformProgressListener;

    #@2
    if-eqz v0, :cond_b

    #@4
    if-lez p1, :cond_b

    #@6
    .line 1894
    iget-object v0, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mExtractAudioWaveformProgressListener:Landroid/media/videoeditor/ExtractAudioWaveformProgressListener;

    #@8
    invoke-interface {v0, p1}, Landroid/media/videoeditor/ExtractAudioWaveformProgressListener;->onProgress(I)V

    #@b
    .line 1896
    :cond_b
    return-void
.end method

.method private onPreviewProgressUpdate(IZZLjava/lang/String;II)V
    .registers 12
    .parameter "progress"
    .parameter "isFinished"
    .parameter "updateOverlay"
    .parameter "filename"
    .parameter "renderingMode"
    .parameter "error"

    #@0
    .prologue
    .line 1849
    iget-object v1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewProgressListener:Landroid/media/videoeditor/VideoEditor$PreviewProgressListener;

    #@2
    if-eqz v1, :cond_30

    #@4
    .line 1850
    iget-boolean v1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mIsFirstProgress:Z

    #@6
    if-eqz v1, :cond_12

    #@8
    .line 1851
    iget-object v1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewProgressListener:Landroid/media/videoeditor/VideoEditor$PreviewProgressListener;

    #@a
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    #@c
    invoke-interface {v1, v2}, Landroid/media/videoeditor/VideoEditor$PreviewProgressListener;->onStart(Landroid/media/videoeditor/VideoEditor;)V

    #@f
    .line 1852
    const/4 v1, 0x0

    #@10
    iput-boolean v1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mIsFirstProgress:Z

    #@12
    .line 1856
    :cond_12
    if-eqz p3, :cond_35

    #@14
    .line 1857
    new-instance v0, Landroid/media/videoeditor/VideoEditor$OverlayData;

    #@16
    invoke-direct {v0}, Landroid/media/videoeditor/VideoEditor$OverlayData;-><init>()V

    #@19
    .line 1858
    .local v0, overlayData:Landroid/media/videoeditor/VideoEditor$OverlayData;
    if-eqz p4, :cond_31

    #@1b
    .line 1859
    invoke-static {p4}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v0, v1, p5}, Landroid/media/videoeditor/VideoEditor$OverlayData;->set(Landroid/graphics/Bitmap;I)V

    #@22
    .line 1867
    :goto_22
    if-eqz p1, :cond_27

    #@24
    .line 1868
    int-to-long v1, p1

    #@25
    iput-wide v1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewProgress:J

    #@27
    .line 1871
    :cond_27
    if-eqz p2, :cond_37

    #@29
    .line 1872
    iget-object v1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewProgressListener:Landroid/media/videoeditor/VideoEditor$PreviewProgressListener;

    #@2b
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    #@2d
    invoke-interface {v1, v2}, Landroid/media/videoeditor/VideoEditor$PreviewProgressListener;->onStop(Landroid/media/videoeditor/VideoEditor;)V

    #@30
    .line 1879
    .end local v0           #overlayData:Landroid/media/videoeditor/VideoEditor$OverlayData;
    :cond_30
    :goto_30
    return-void

    #@31
    .line 1861
    .restart local v0       #overlayData:Landroid/media/videoeditor/VideoEditor$OverlayData;
    :cond_31
    invoke-virtual {v0}, Landroid/media/videoeditor/VideoEditor$OverlayData;->setClear()V

    #@34
    goto :goto_22

    #@35
    .line 1864
    .end local v0           #overlayData:Landroid/media/videoeditor/VideoEditor$OverlayData;
    :cond_35
    const/4 v0, 0x0

    #@36
    .restart local v0       #overlayData:Landroid/media/videoeditor/VideoEditor$OverlayData;
    goto :goto_22

    #@37
    .line 1873
    :cond_37
    if-eqz p6, :cond_41

    #@39
    .line 1874
    iget-object v1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewProgressListener:Landroid/media/videoeditor/VideoEditor$PreviewProgressListener;

    #@3b
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    #@3d
    invoke-interface {v1, v2, p6}, Landroid/media/videoeditor/VideoEditor$PreviewProgressListener;->onError(Landroid/media/videoeditor/VideoEditor;I)V

    #@40
    goto :goto_30

    #@41
    .line 1876
    :cond_41
    iget-object v1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewProgressListener:Landroid/media/videoeditor/VideoEditor$PreviewProgressListener;

    #@43
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    #@45
    int-to-long v3, p1

    #@46
    invoke-interface {v1, v2, v3, v4, v0}, Landroid/media/videoeditor/VideoEditor$PreviewProgressListener;->onProgress(Landroid/media/videoeditor/VideoEditor;JLandroid/media/videoeditor/VideoEditor$OverlayData;)V

    #@49
    goto :goto_30
.end method

.method private onProgressUpdate(II)V
    .registers 8
    .parameter "taskId"
    .parameter "progress"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 1768
    iget v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProcessingState:I

    #@3
    const/16 v3, 0x14

    #@5
    if-ne v2, v3, :cond_1b

    #@7
    .line 1769
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mExportProgressListener:Landroid/media/videoeditor/VideoEditor$ExportProgressListener;

    #@9
    if-eqz v2, :cond_1a

    #@b
    .line 1770
    iget v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProgressToApp:I

    #@d
    if-ge v2, p2, :cond_1a

    #@f
    .line 1771
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mExportProgressListener:Landroid/media/videoeditor/VideoEditor$ExportProgressListener;

    #@11
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    #@13
    iget-object v4, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mOutputFilename:Ljava/lang/String;

    #@15
    invoke-interface {v2, v3, v4, p2}, Landroid/media/videoeditor/VideoEditor$ExportProgressListener;->onProgress(Landroid/media/videoeditor/VideoEditor;Ljava/lang/String;I)V

    #@18
    .line 1773
    iput p2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProgressToApp:I

    #@1a
    .line 1844
    :cond_1a
    :goto_1a
    return-void

    #@1b
    .line 1779
    :cond_1b
    const/4 v1, 0x0

    #@1c
    .line 1780
    .local v1, actualProgress:I
    const/4 v0, 0x0

    #@1d
    .line 1782
    .local v0, action:I
    iget v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProcessingState:I

    #@1f
    if-ne v2, v4, :cond_42

    #@21
    .line 1783
    const/4 v0, 0x2

    #@22
    .line 1788
    :goto_22
    iget v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProcessingState:I

    #@24
    packed-switch v2, :pswitch_data_96

    #@27
    .line 1819
    :pswitch_27
    const-string v2, "MediaArtistNativeHelper"

    #@29
    new-instance v3, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v4, "ERROR unexpected State="

    #@30
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    iget v4, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProcessingState:I

    #@36
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v3

    #@3e
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    goto :goto_1a

    #@42
    .line 1785
    :cond_42
    const/4 v0, 0x1

    #@43
    goto :goto_22

    #@44
    .line 1790
    :pswitch_44
    move v1, p2

    #@45
    .line 1822
    :cond_45
    :goto_45
    iget v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProgressToApp:I

    #@47
    if-eq v2, v1, :cond_58

    #@49
    if-eqz v1, :cond_58

    #@4b
    .line 1824
    iput v1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProgressToApp:I

    #@4d
    .line 1826
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mMediaProcessingProgressListener:Landroid/media/videoeditor/VideoEditor$MediaProcessingProgressListener;

    #@4f
    if-eqz v2, :cond_58

    #@51
    .line 1828
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mMediaProcessingProgressListener:Landroid/media/videoeditor/VideoEditor$MediaProcessingProgressListener;

    #@53
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProcessingObject:Ljava/lang/Object;

    #@55
    invoke-interface {v2, v3, v0, v1}, Landroid/media/videoeditor/VideoEditor$MediaProcessingProgressListener;->onProgress(Ljava/lang/Object;II)V

    #@58
    .line 1833
    :cond_58
    iget v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProgressToApp:I

    #@5a
    if-nez v2, :cond_1a

    #@5c
    .line 1834
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mMediaProcessingProgressListener:Landroid/media/videoeditor/VideoEditor$MediaProcessingProgressListener;

    #@5e
    if-eqz v2, :cond_67

    #@60
    .line 1838
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mMediaProcessingProgressListener:Landroid/media/videoeditor/VideoEditor$MediaProcessingProgressListener;

    #@62
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProcessingObject:Ljava/lang/Object;

    #@64
    invoke-interface {v2, v3, v0, v1}, Landroid/media/videoeditor/VideoEditor$MediaProcessingProgressListener;->onProgress(Ljava/lang/Object;II)V

    #@67
    .line 1841
    :cond_67
    iput v4, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProgressToApp:I

    #@69
    goto :goto_1a

    #@6a
    .line 1793
    :pswitch_6a
    move v1, p2

    #@6b
    .line 1794
    goto :goto_45

    #@6c
    .line 1796
    :pswitch_6c
    move v1, p2

    #@6d
    .line 1797
    goto :goto_45

    #@6e
    .line 1799
    :pswitch_6e
    if-nez p2, :cond_77

    #@70
    iget v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProgressToApp:I

    #@72
    if-eqz v2, :cond_77

    #@74
    .line 1800
    const/4 v2, 0x0

    #@75
    iput v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProgressToApp:I

    #@77
    .line 1802
    :cond_77
    if-nez p2, :cond_7d

    #@79
    iget v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProgressToApp:I

    #@7b
    if-eqz v2, :cond_45

    #@7d
    .line 1803
    :cond_7d
    div-int/lit8 v1, p2, 0x4

    #@7f
    goto :goto_45

    #@80
    .line 1807
    :pswitch_80
    if-nez p2, :cond_86

    #@82
    iget v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProgressToApp:I

    #@84
    if-eqz v2, :cond_45

    #@86
    .line 1808
    :cond_86
    div-int/lit8 v2, p2, 0x4

    #@88
    add-int/lit8 v1, v2, 0x19

    #@8a
    goto :goto_45

    #@8b
    .line 1812
    :pswitch_8b
    if-nez p2, :cond_91

    #@8d
    iget v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProgressToApp:I

    #@8f
    if-eqz v2, :cond_45

    #@91
    .line 1813
    :cond_91
    div-int/lit8 v2, p2, 0x2

    #@93
    add-int/lit8 v1, v2, 0x32

    #@95
    goto :goto_45

    #@96
    .line 1788
    :pswitch_data_96
    .packed-switch 0x1
        :pswitch_44
        :pswitch_6a
        :pswitch_6c
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_6e
        :pswitch_80
        :pswitch_8b
    .end packed-switch
.end method

.method private populateBackgroundMusicProperties(Ljava/util/List;)V
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/media/videoeditor/AudioTrack;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p1, mediaBGMList:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/AudioTrack;>;"
    const/4 v6, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    const/4 v5, 0x0

    #@3
    .line 2727
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@6
    move-result v2

    #@7
    if-ne v2, v6, :cond_15e

    #@9
    .line 2728
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v2

    #@d
    check-cast v2, Landroid/media/videoeditor/AudioTrack;

    #@f
    iput-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrack:Landroid/media/videoeditor/AudioTrack;

    #@11
    .line 2733
    :goto_11
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrack:Landroid/media/videoeditor/AudioTrack;

    #@13
    if-eqz v2, :cond_177

    #@15
    .line 2734
    new-instance v2, Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;

    #@17
    invoke-direct {v2}, Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;-><init>()V

    #@1a
    iput-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;

    #@1c
    .line 2735
    new-instance v1, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@1e
    invoke-direct {v1}, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;-><init>()V

    #@21
    .line 2736
    .local v1, mAudioProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;

    #@23
    iput-object v3, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;->pFile:Ljava/lang/String;

    #@25
    .line 2737
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;

    #@27
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrack:Landroid/media/videoeditor/AudioTrack;

    #@29
    invoke-virtual {v3}, Landroid/media/videoeditor/AudioTrack;->getId()Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    iput-object v3, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;->Id:Ljava/lang/String;

    #@2f
    .line 2739
    :try_start_2f
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrack:Landroid/media/videoeditor/AudioTrack;

    #@31
    invoke-virtual {v2}, Landroid/media/videoeditor/AudioTrack;->getFilename()Ljava/lang/String;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {p0, v2}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getMediaProperties(Ljava/lang/String;)Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
    :try_end_38
    .catch Ljava/lang/Exception; {:try_start_2f .. :try_end_38} :catch_162

    #@38
    move-result-object v1

    #@39
    .line 2743
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;

    #@3b
    iput-boolean v5, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;->bRemoveOriginal:Z

    #@3d
    .line 2744
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;

    #@3f
    iget v3, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->audioChannels:I

    #@41
    iput v3, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;->channels:I

    #@43
    .line 2745
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;

    #@45
    iget v3, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->audioSamplingFrequency:I

    #@47
    iput v3, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;->Fs:I

    #@49
    .line 2746
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;

    #@4b
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrack:Landroid/media/videoeditor/AudioTrack;

    #@4d
    invoke-virtual {v3}, Landroid/media/videoeditor/AudioTrack;->isLooping()Z

    #@50
    move-result v3

    #@51
    iput-boolean v3, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;->loop:Z

    #@53
    .line 2747
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;

    #@55
    iput v5, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;->ExtendedFs:I

    #@57
    .line 2748
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;

    #@59
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrack:Landroid/media/videoeditor/AudioTrack;

    #@5b
    invoke-virtual {v3}, Landroid/media/videoeditor/AudioTrack;->getFilename()Ljava/lang/String;

    #@5e
    move-result-object v3

    #@5f
    iput-object v3, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;->pFile:Ljava/lang/String;

    #@61
    .line 2749
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;

    #@63
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrack:Landroid/media/videoeditor/AudioTrack;

    #@65
    invoke-virtual {v3}, Landroid/media/videoeditor/AudioTrack;->getStartTime()J

    #@68
    move-result-wide v3

    #@69
    iput-wide v3, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;->startMs:J

    #@6b
    .line 2750
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;

    #@6d
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrack:Landroid/media/videoeditor/AudioTrack;

    #@6f
    invoke-virtual {v3}, Landroid/media/videoeditor/AudioTrack;->getBoundaryBeginTime()J

    #@72
    move-result-wide v3

    #@73
    iput-wide v3, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;->beginCutTime:J

    #@75
    .line 2751
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;

    #@77
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrack:Landroid/media/videoeditor/AudioTrack;

    #@79
    invoke-virtual {v3}, Landroid/media/videoeditor/AudioTrack;->getBoundaryEndTime()J

    #@7c
    move-result-wide v3

    #@7d
    iput-wide v3, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;->endCutTime:J

    #@7f
    .line 2752
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrack:Landroid/media/videoeditor/AudioTrack;

    #@81
    invoke-virtual {v2}, Landroid/media/videoeditor/AudioTrack;->isMuted()Z

    #@84
    move-result v2

    #@85
    if-eqz v2, :cond_16b

    #@87
    .line 2753
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;

    #@89
    iput v5, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;->volume:I

    #@8b
    .line 2757
    :goto_8b
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;

    #@8d
    iget v3, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->fileType:I

    #@8f
    iput v3, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;->fileType:I

    #@91
    .line 2758
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;

    #@93
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrack:Landroid/media/videoeditor/AudioTrack;

    #@95
    invoke-virtual {v3}, Landroid/media/videoeditor/AudioTrack;->getDuckedTrackVolume()I

    #@98
    move-result v3

    #@99
    iput v3, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;->ducking_lowVolume:I

    #@9b
    .line 2759
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;

    #@9d
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrack:Landroid/media/videoeditor/AudioTrack;

    #@9f
    invoke-virtual {v3}, Landroid/media/videoeditor/AudioTrack;->getDuckingThreshhold()I

    #@a2
    move-result v3

    #@a3
    iput v3, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;->ducking_threshold:I

    #@a5
    .line 2760
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;

    #@a7
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrack:Landroid/media/videoeditor/AudioTrack;

    #@a9
    invoke-virtual {v3}, Landroid/media/videoeditor/AudioTrack;->isDuckingEnabled()Z

    #@ac
    move-result v3

    #@ad
    iput-boolean v3, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;->bInDucking_enable:Z

    #@af
    .line 2761
    new-instance v2, Ljava/lang/StringBuilder;

    #@b1
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b4
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProjectPath:Ljava/lang/String;

    #@b6
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v2

    #@ba
    const-string v3, "/"

    #@bc
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v2

    #@c0
    const-string v3, "AudioPcm.pcm"

    #@c2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v2

    #@c6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c9
    move-result-object v2

    #@ca
    new-array v3, v5, [Ljava/lang/Object;

    #@cc
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@cf
    move-result-object v2

    #@d0
    iput-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrackPCMFilePath:Ljava/lang/String;

    #@d2
    .line 2762
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;

    #@d4
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrackPCMFilePath:Ljava/lang/String;

    #@d6
    iput-object v3, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;->pcmFilePath:Ljava/lang/String;

    #@d8
    .line 2764
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@da
    new-instance v3, Landroid/media/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;

    #@dc
    invoke-direct {v3}, Landroid/media/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;-><init>()V

    #@df
    iput-object v3, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->backgroundMusicSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;

    #@e1
    .line 2765
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@e3
    iget-object v2, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->backgroundMusicSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;

    #@e5
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrackPCMFilePath:Ljava/lang/String;

    #@e7
    iput-object v3, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;->file:Ljava/lang/String;

    #@e9
    .line 2766
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@eb
    iget-object v2, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->backgroundMusicSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;

    #@ed
    iget v3, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->fileType:I

    #@ef
    iput v3, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;->fileType:I

    #@f1
    .line 2767
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@f3
    iget-object v2, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->backgroundMusicSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;

    #@f5
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrack:Landroid/media/videoeditor/AudioTrack;

    #@f7
    invoke-virtual {v3}, Landroid/media/videoeditor/AudioTrack;->getStartTime()J

    #@fa
    move-result-wide v3

    #@fb
    iput-wide v3, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;->insertionTime:J

    #@fd
    .line 2769
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@ff
    iget-object v2, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->backgroundMusicSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;

    #@101
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrack:Landroid/media/videoeditor/AudioTrack;

    #@103
    invoke-virtual {v3}, Landroid/media/videoeditor/AudioTrack;->getVolume()I

    #@106
    move-result v3

    #@107
    iput v3, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;->volumePercent:I

    #@109
    .line 2770
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@10b
    iget-object v2, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->backgroundMusicSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;

    #@10d
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrack:Landroid/media/videoeditor/AudioTrack;

    #@10f
    invoke-virtual {v3}, Landroid/media/videoeditor/AudioTrack;->getBoundaryBeginTime()J

    #@112
    move-result-wide v3

    #@113
    iput-wide v3, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;->beginLoop:J

    #@115
    .line 2772
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@117
    iget-object v2, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->backgroundMusicSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;

    #@119
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrack:Landroid/media/videoeditor/AudioTrack;

    #@11b
    invoke-virtual {v3}, Landroid/media/videoeditor/AudioTrack;->getBoundaryEndTime()J

    #@11e
    move-result-wide v3

    #@11f
    iput-wide v3, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;->endLoop:J

    #@121
    .line 2774
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@123
    iget-object v2, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->backgroundMusicSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;

    #@125
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrack:Landroid/media/videoeditor/AudioTrack;

    #@127
    invoke-virtual {v3}, Landroid/media/videoeditor/AudioTrack;->isDuckingEnabled()Z

    #@12a
    move-result v3

    #@12b
    iput-boolean v3, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;->enableDucking:Z

    #@12d
    .line 2776
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@12f
    iget-object v2, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->backgroundMusicSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;

    #@131
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrack:Landroid/media/videoeditor/AudioTrack;

    #@133
    invoke-virtual {v3}, Landroid/media/videoeditor/AudioTrack;->getDuckingThreshhold()I

    #@136
    move-result v3

    #@137
    iput v3, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;->duckingThreshold:I

    #@139
    .line 2778
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@13b
    iget-object v2, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->backgroundMusicSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;

    #@13d
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrack:Landroid/media/videoeditor/AudioTrack;

    #@13f
    invoke-virtual {v3}, Landroid/media/videoeditor/AudioTrack;->getDuckedTrackVolume()I

    #@142
    move-result v3

    #@143
    iput v3, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;->lowVolume:I

    #@145
    .line 2780
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@147
    iget-object v2, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->backgroundMusicSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;

    #@149
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrack:Landroid/media/videoeditor/AudioTrack;

    #@14b
    invoke-virtual {v3}, Landroid/media/videoeditor/AudioTrack;->isLooping()Z

    #@14e
    move-result v3

    #@14f
    iput-boolean v3, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;->isLooping:Z

    #@151
    .line 2781
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@153
    const/16 v3, 0x64

    #@155
    iput v3, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->primaryTrackVolume:I

    #@157
    .line 2782
    iput v6, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProcessingState:I

    #@159
    .line 2783
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrack:Landroid/media/videoeditor/AudioTrack;

    #@15b
    iput-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProcessingObject:Ljava/lang/Object;

    #@15d
    .line 2789
    .end local v1           #mAudioProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
    :goto_15d
    return-void

    #@15e
    .line 2730
    :cond_15e
    iput-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrack:Landroid/media/videoeditor/AudioTrack;

    #@160
    goto/16 :goto_11

    #@162
    .line 2740
    .restart local v1       #mAudioProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
    :catch_162
    move-exception v0

    #@163
    .line 2741
    .local v0, e:Ljava/lang/Exception;
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@165
    const-string v3, "Unsupported file or file not found"

    #@167
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@16a
    throw v2

    #@16b
    .line 2755
    .end local v0           #e:Ljava/lang/Exception;
    :cond_16b
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;

    #@16d
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrack:Landroid/media/videoeditor/AudioTrack;

    #@16f
    invoke-virtual {v3}, Landroid/media/videoeditor/AudioTrack;->getVolume()I

    #@172
    move-result v3

    #@173
    iput v3, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;->volume:I

    #@175
    goto/16 :goto_8b

    #@177
    .line 2785
    .end local v1           #mAudioProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
    :cond_177
    iput-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;

    #@179
    .line 2786
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@17b
    iput-object v3, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->backgroundMusicSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;

    #@17d
    .line 2787
    iput-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrackPCMFilePath:Ljava/lang/String;

    #@17f
    goto :goto_15d
.end method

.method private populateEffects(Landroid/media/videoeditor/MediaItem;[Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;IIII)I
    .registers 16
    .parameter "m"
    .parameter "effectSettings"
    .parameter "i"
    .parameter "beginCutTime"
    .parameter "endCutTime"
    .parameter "storyBoardTime"

    #@0
    .prologue
    const-wide/16 v7, 0x0

    #@2
    .line 2531
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getBeginTransition()Landroid/media/videoeditor/Transition;

    #@5
    move-result-object v5

    #@6
    if-eqz v5, :cond_6b

    #@8
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getBeginTransition()Landroid/media/videoeditor/Transition;

    #@b
    move-result-object v5

    #@c
    invoke-virtual {v5}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@f
    move-result-wide v5

    #@10
    cmp-long v5, v5, v7

    #@12
    if-lez v5, :cond_6b

    #@14
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getEndTransition()Landroid/media/videoeditor/Transition;

    #@17
    move-result-object v5

    #@18
    if-eqz v5, :cond_6b

    #@1a
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getEndTransition()Landroid/media/videoeditor/Transition;

    #@1d
    move-result-object v5

    #@1e
    invoke-virtual {v5}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@21
    move-result-wide v5

    #@22
    cmp-long v5, v5, v7

    #@24
    if-lez v5, :cond_6b

    #@26
    .line 2533
    int-to-long v5, p4

    #@27
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getBeginTransition()Landroid/media/videoeditor/Transition;

    #@2a
    move-result-object v7

    #@2b
    invoke-virtual {v7}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@2e
    move-result-wide v7

    #@2f
    add-long/2addr v5, v7

    #@30
    long-to-int p4, v5

    #@31
    .line 2534
    int-to-long v5, p5

    #@32
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getEndTransition()Landroid/media/videoeditor/Transition;

    #@35
    move-result-object v7

    #@36
    invoke-virtual {v7}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@39
    move-result-wide v7

    #@3a
    sub-long/2addr v5, v7

    #@3b
    long-to-int p5, v5

    #@3c
    .line 2543
    :cond_3c
    :goto_3c
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getAllEffects()Ljava/util/List;

    #@3f
    move-result-object v1

    #@40
    .line 2544
    .local v1, effects:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Effect;>;"
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getAllOverlays()Ljava/util/List;

    #@43
    move-result-object v4

    #@44
    .line 2546
    .local v4, overlays:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Overlay;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@47
    move-result-object v2

    #@48
    .local v2, i$:Ljava/util/Iterator;
    :goto_48
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@4b
    move-result v5

    #@4c
    if-eqz v5, :cond_b3

    #@4e
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@51
    move-result-object v3

    #@52
    check-cast v3, Landroid/media/videoeditor/Overlay;

    #@54
    .line 2547
    .local v3, overlay:Landroid/media/videoeditor/Overlay;
    check-cast v3, Landroid/media/videoeditor/OverlayFrame;

    #@56
    .end local v3           #overlay:Landroid/media/videoeditor/Overlay;
    invoke-virtual {p0, v3}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getOverlaySettings(Landroid/media/videoeditor/OverlayFrame;)Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;

    #@59
    move-result-object v5

    #@5a
    aput-object v5, p2, p3

    #@5c
    .line 2548
    aget-object v5, p2, p3

    #@5e
    invoke-virtual {p0, v5, p4, p5}, Landroid/media/videoeditor/MediaArtistNativeHelper;->adjustEffectsStartTimeAndDuration(Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;II)V

    #@61
    .line 2549
    aget-object v5, p2, p3

    #@63
    iget v6, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@65
    add-int/2addr v6, p6

    #@66
    iput v6, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@68
    .line 2550
    add-int/lit8 p3, p3, 0x1

    #@6a
    goto :goto_48

    #@6b
    .line 2535
    .end local v1           #effects:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Effect;>;"
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v4           #overlays:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Overlay;>;"
    :cond_6b
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getBeginTransition()Landroid/media/videoeditor/Transition;

    #@6e
    move-result-object v5

    #@6f
    if-nez v5, :cond_8f

    #@71
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getEndTransition()Landroid/media/videoeditor/Transition;

    #@74
    move-result-object v5

    #@75
    if-eqz v5, :cond_8f

    #@77
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getEndTransition()Landroid/media/videoeditor/Transition;

    #@7a
    move-result-object v5

    #@7b
    invoke-virtual {v5}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@7e
    move-result-wide v5

    #@7f
    cmp-long v5, v5, v7

    #@81
    if-lez v5, :cond_8f

    #@83
    .line 2537
    int-to-long v5, p5

    #@84
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getEndTransition()Landroid/media/videoeditor/Transition;

    #@87
    move-result-object v7

    #@88
    invoke-virtual {v7}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@8b
    move-result-wide v7

    #@8c
    sub-long/2addr v5, v7

    #@8d
    long-to-int p5, v5

    #@8e
    goto :goto_3c

    #@8f
    .line 2538
    :cond_8f
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getEndTransition()Landroid/media/videoeditor/Transition;

    #@92
    move-result-object v5

    #@93
    if-nez v5, :cond_3c

    #@95
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getBeginTransition()Landroid/media/videoeditor/Transition;

    #@98
    move-result-object v5

    #@99
    if-eqz v5, :cond_3c

    #@9b
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getBeginTransition()Landroid/media/videoeditor/Transition;

    #@9e
    move-result-object v5

    #@9f
    invoke-virtual {v5}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@a2
    move-result-wide v5

    #@a3
    cmp-long v5, v5, v7

    #@a5
    if-lez v5, :cond_3c

    #@a7
    .line 2540
    int-to-long v5, p4

    #@a8
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getBeginTransition()Landroid/media/videoeditor/Transition;

    #@ab
    move-result-object v7

    #@ac
    invoke-virtual {v7}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@af
    move-result-wide v7

    #@b0
    add-long/2addr v5, v7

    #@b1
    long-to-int p4, v5

    #@b2
    goto :goto_3c

    #@b3
    .line 2553
    .restart local v1       #effects:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Effect;>;"
    .restart local v2       #i$:Ljava/util/Iterator;
    .restart local v4       #overlays:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Overlay;>;"
    :cond_b3
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@b6
    move-result-object v2

    #@b7
    :cond_b7
    :goto_b7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@ba
    move-result v5

    #@bb
    if-eqz v5, :cond_de

    #@bd
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@c0
    move-result-object v0

    #@c1
    check-cast v0, Landroid/media/videoeditor/Effect;

    #@c3
    .line 2554
    .local v0, effect:Landroid/media/videoeditor/Effect;
    instance-of v5, v0, Landroid/media/videoeditor/EffectColor;

    #@c5
    if-eqz v5, :cond_b7

    #@c7
    .line 2555
    check-cast v0, Landroid/media/videoeditor/EffectColor;

    #@c9
    .end local v0           #effect:Landroid/media/videoeditor/Effect;
    invoke-virtual {p0, v0}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getEffectSettings(Landroid/media/videoeditor/EffectColor;)Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;

    #@cc
    move-result-object v5

    #@cd
    aput-object v5, p2, p3

    #@cf
    .line 2556
    aget-object v5, p2, p3

    #@d1
    invoke-virtual {p0, v5, p4, p5}, Landroid/media/videoeditor/MediaArtistNativeHelper;->adjustEffectsStartTimeAndDuration(Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;II)V

    #@d4
    .line 2557
    aget-object v5, p2, p3

    #@d6
    iget v6, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@d8
    add-int/2addr v6, p6

    #@d9
    iput v6, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@db
    .line 2558
    add-int/lit8 p3, p3, 0x1

    #@dd
    goto :goto_b7

    #@de
    .line 2562
    :cond_de
    return p3
.end method

.method private populateMediaItemProperties(Landroid/media/videoeditor/MediaItem;II)I
    .registers 6
    .parameter "m"
    .parameter "index"
    .parameter "maxHeight"

    #@0
    .prologue
    .line 2694
    iget-object v0, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@2
    iget-object v0, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@4
    new-instance v1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@6
    invoke-direct {v1}, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;-><init>()V

    #@9
    aput-object v1, v0, p2

    #@b
    .line 2695
    instance-of v0, p1, Landroid/media/videoeditor/MediaVideoItem;

    #@d
    if-eqz v0, :cond_56

    #@f
    .line 2696
    iget-object v0, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@11
    iget-object v1, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@13
    move-object v0, p1

    #@14
    check-cast v0, Landroid/media/videoeditor/MediaVideoItem;

    #@16
    invoke-virtual {v0}, Landroid/media/videoeditor/MediaVideoItem;->getVideoClipProperties()Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@19
    move-result-object v0

    #@1a
    aput-object v0, v1, p2

    #@1c
    move-object v0, p1

    #@1d
    .line 2698
    check-cast v0, Landroid/media/videoeditor/MediaVideoItem;

    #@1f
    invoke-virtual {v0}, Landroid/media/videoeditor/MediaVideoItem;->getHeight()I

    #@22
    move-result v0

    #@23
    if-le v0, p3, :cond_2c

    #@25
    move-object v0, p1

    #@26
    .line 2699
    check-cast v0, Landroid/media/videoeditor/MediaVideoItem;

    #@28
    invoke-virtual {v0}, Landroid/media/videoeditor/MediaVideoItem;->getHeight()I

    #@2b
    move-result p3

    #@2c
    .line 2709
    :cond_2c
    :goto_2c
    iget-object v0, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@2e
    iget-object v0, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@30
    aget-object v0, v0, p2

    #@32
    iget v0, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->fileType:I

    #@34
    const/4 v1, 0x5

    #@35
    if-ne v0, v1, :cond_55

    #@37
    .line 2710
    iget-object v0, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@39
    iget-object v0, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@3b
    aget-object v0, v0, p2

    #@3d
    check-cast p1, Landroid/media/videoeditor/MediaImageItem;

    #@3f
    .end local p1
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaImageItem;->getDecodedImageFileName()Ljava/lang/String;

    #@42
    move-result-object v1

    #@43
    iput-object v1, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipDecodedPath:Ljava/lang/String;

    #@45
    .line 2713
    iget-object v0, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@47
    iget-object v0, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@49
    aget-object v0, v0, p2

    #@4b
    iget-object v1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@4d
    iget-object v1, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@4f
    aget-object v1, v1, p2

    #@51
    iget-object v1, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipPath:Ljava/lang/String;

    #@53
    iput-object v1, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipOriginalPath:Ljava/lang/String;

    #@55
    .line 2716
    :cond_55
    return p3

    #@56
    .line 2701
    .restart local p1
    :cond_56
    instance-of v0, p1, Landroid/media/videoeditor/MediaImageItem;

    #@58
    if-eqz v0, :cond_2c

    #@5a
    .line 2702
    iget-object v0, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@5c
    iget-object v1, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@5e
    move-object v0, p1

    #@5f
    check-cast v0, Landroid/media/videoeditor/MediaImageItem;

    #@61
    invoke-virtual {v0}, Landroid/media/videoeditor/MediaImageItem;->getImageClipProperties()Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@64
    move-result-object v0

    #@65
    aput-object v0, v1, p2

    #@67
    move-object v0, p1

    #@68
    .line 2704
    check-cast v0, Landroid/media/videoeditor/MediaImageItem;

    #@6a
    invoke-virtual {v0}, Landroid/media/videoeditor/MediaImageItem;->getScaledHeight()I

    #@6d
    move-result v0

    #@6e
    if-le v0, p3, :cond_2c

    #@70
    move-object v0, p1

    #@71
    .line 2705
    check-cast v0, Landroid/media/videoeditor/MediaImageItem;

    #@73
    invoke-virtual {v0}, Landroid/media/videoeditor/MediaImageItem;->getScaledHeight()I

    #@76
    move-result p3

    #@77
    goto :goto_2c
.end method

.method private previewFrameEditInfo(Ljava/lang/String;I)V
    .registers 3
    .parameter "filename"
    .parameter "renderingMode"

    #@0
    .prologue
    .line 3119
    iput-object p1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mRenderPreviewOverlayFile:Ljava/lang/String;

    #@2
    .line 3120
    iput p2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mRenderPreviewRenderingMode:I

    #@4
    .line 3121
    return-void
.end method

.method private native release()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method private native stopEncoding()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method private unlock()V
    .registers 3

    #@0
    .prologue
    .line 3942
    const-string v0, "MediaArtistNativeHelper"

    #@2
    const/4 v1, 0x3

    #@3
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_11

    #@9
    .line 3943
    const-string v0, "MediaArtistNativeHelper"

    #@b
    const-string/jumbo v1, "unlock: releasing semaphore"

    #@e
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 3945
    :cond_11
    iget-object v0, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mLock:Ljava/util/concurrent/Semaphore;

    #@13
    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    #@16
    .line 3946
    return-void
.end method


# virtual methods
.method GetClosestVideoFrameRate(I)I
    .registers 5
    .parameter "averageFrameRate"

    #@0
    .prologue
    const/4 v0, 0x7

    #@1
    const/4 v1, 0x5

    #@2
    .line 2113
    const/16 v2, 0x19

    #@4
    if-lt p1, v2, :cond_7

    #@6
    .line 2128
    :goto_6
    return v0

    #@7
    .line 2115
    :cond_7
    const/16 v2, 0x14

    #@9
    if-lt p1, v2, :cond_d

    #@b
    .line 2116
    const/4 v0, 0x6

    #@c
    goto :goto_6

    #@d
    .line 2117
    :cond_d
    const/16 v2, 0xf

    #@f
    if-lt p1, v2, :cond_13

    #@11
    move v0, v1

    #@12
    .line 2118
    goto :goto_6

    #@13
    .line 2119
    :cond_13
    const/16 v2, 0xc

    #@15
    if-lt p1, v2, :cond_19

    #@17
    .line 2120
    const/4 v0, 0x4

    #@18
    goto :goto_6

    #@19
    .line 2121
    :cond_19
    const/16 v2, 0xa

    #@1b
    if-lt p1, v2, :cond_1f

    #@1d
    .line 2122
    const/4 v0, 0x3

    #@1e
    goto :goto_6

    #@1f
    .line 2123
    :cond_1f
    if-lt p1, v0, :cond_23

    #@21
    .line 2124
    const/4 v0, 0x2

    #@22
    goto :goto_6

    #@23
    .line 2125
    :cond_23
    if-lt p1, v1, :cond_27

    #@25
    .line 2126
    const/4 v0, 0x1

    #@26
    goto :goto_6

    #@27
    .line 2128
    :cond_27
    const/4 v0, -0x1

    #@28
    goto :goto_6
.end method

.method public adjustEffectsStartTimeAndDuration(Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;II)V
    .registers 8
    .parameter "lEffect"
    .parameter "beginCutTime"
    .parameter "endCutTime"

    #@0
    .prologue
    .line 2139
    const/4 v1, 0x0

    #@1
    .line 2140
    .local v1, effectStartTime:I
    const/4 v0, 0x0

    #@2
    .line 2165
    .local v0, effectDuration:I
    iget v2, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@4
    if-gt v2, p3, :cond_d

    #@6
    iget v2, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@8
    iget v3, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->duration:I

    #@a
    add-int/2addr v2, v3

    #@b
    if-gt v2, p2, :cond_14

    #@d
    .line 2168
    :cond_d
    const/4 v1, 0x0

    #@e
    .line 2169
    const/4 v0, 0x0

    #@f
    .line 2171
    iput v1, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@11
    .line 2172
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->duration:I

    #@13
    .line 2218
    :cond_13
    :goto_13
    return-void

    #@14
    .line 2177
    :cond_14
    iget v2, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@16
    if-ge v2, p2, :cond_33

    #@18
    iget v2, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@1a
    iget v3, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->duration:I

    #@1c
    add-int/2addr v2, v3

    #@1d
    if-le v2, p2, :cond_33

    #@1f
    iget v2, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@21
    iget v3, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->duration:I

    #@23
    add-int/2addr v2, v3

    #@24
    if-gt v2, p3, :cond_33

    #@26
    .line 2180
    const/4 v1, 0x0

    #@27
    .line 2181
    iget v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->duration:I

    #@29
    .line 2183
    iget v2, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@2b
    sub-int v2, p2, v2

    #@2d
    sub-int/2addr v0, v2

    #@2e
    .line 2184
    iput v1, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@30
    .line 2185
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->duration:I

    #@32
    goto :goto_13

    #@33
    .line 2190
    :cond_33
    iget v2, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@35
    if-lt v2, p2, :cond_49

    #@37
    iget v2, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@39
    iget v3, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->duration:I

    #@3b
    add-int/2addr v2, v3

    #@3c
    if-gt v2, p3, :cond_49

    #@3e
    .line 2192
    iget v2, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@40
    sub-int v1, v2, p2

    #@42
    .line 2193
    iput v1, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@44
    .line 2194
    iget v2, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->duration:I

    #@46
    iput v2, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->duration:I

    #@48
    goto :goto_13

    #@49
    .line 2199
    :cond_49
    iget v2, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@4b
    if-lt v2, p2, :cond_61

    #@4d
    iget v2, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@4f
    iget v3, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->duration:I

    #@51
    add-int/2addr v2, v3

    #@52
    if-le v2, p3, :cond_61

    #@54
    .line 2201
    iget v2, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@56
    sub-int v1, v2, p2

    #@58
    .line 2202
    iget v2, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@5a
    sub-int v0, p3, v2

    #@5c
    .line 2203
    iput v1, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@5e
    .line 2204
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->duration:I

    #@60
    goto :goto_13

    #@61
    .line 2209
    :cond_61
    iget v2, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@63
    if-ge v2, p2, :cond_13

    #@65
    iget v2, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@67
    iget v3, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->duration:I

    #@69
    add-int/2addr v2, v3

    #@6a
    if-le v2, p3, :cond_13

    #@6c
    .line 2211
    const/4 v1, 0x0

    #@6d
    .line 2212
    sub-int v0, p3, p2

    #@6f
    .line 2213
    iput v1, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@71
    .line 2214
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->duration:I

    #@73
    goto :goto_13
.end method

.method clearPreviewSurface(Landroid/view/Surface;)V
    .registers 2
    .parameter "surface"

    #@0
    .prologue
    .line 3920
    invoke-direct {p0, p1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->nativeClearSurface(Landroid/view/Surface;)V

    #@3
    .line 3921
    return-void
.end method

.method doPreview(Landroid/view/Surface;JJZILandroid/media/videoeditor/VideoEditor$PreviewProgressListener;)V
    .registers 20
    .parameter "surface"
    .parameter "fromMs"
    .parameter "toMs"
    .parameter "loop"
    .parameter "callbackAfterFrameCount"
    .parameter "listener"

    #@0
    .prologue
    .line 3020
    iput-wide p2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewProgress:J

    #@2
    .line 3021
    const/4 v1, 0x1

    #@3
    iput-boolean v1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mIsFirstProgress:Z

    #@5
    .line 3022
    move-object/from16 v0, p8

    #@7
    iput-object v0, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewProgressListener:Landroid/media/videoeditor/VideoEditor$PreviewProgressListener;

    #@9
    .line 3024
    iget-boolean v1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mInvalidatePreviewArray:Z

    #@b
    if-nez v1, :cond_63

    #@d
    .line 3027
    const/4 v9, 0x0

    #@e
    .local v9, clipCnt:I
    :goto_e
    :try_start_e
    iget-object v1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@10
    iget-object v1, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@12
    array-length v1, v1

    #@13
    if-ge v9, v1, :cond_33

    #@15
    .line 3029
    iget-object v1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@17
    iget-object v1, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@19
    aget-object v1, v1, v9

    #@1b
    iget v1, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->fileType:I

    #@1d
    const/4 v2, 0x5

    #@1e
    if-ne v1, v2, :cond_30

    #@20
    .line 3030
    iget-object v1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@22
    iget-object v1, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@24
    aget-object v1, v1, v9

    #@26
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@28
    iget-object v2, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@2a
    aget-object v2, v2, v9

    #@2c
    iget-object v2, v2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipDecodedPath:Ljava/lang/String;

    #@2e
    iput-object v2, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipPath:Ljava/lang/String;

    #@30
    .line 3028
    :cond_30
    add-int/lit8 v9, v9, 0x1

    #@32
    goto :goto_e

    #@33
    .line 3034
    :cond_33
    iget-object v1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@35
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@37
    iget-object v3, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;

    #@39
    invoke-direct {p0, v1, v2, v3}, Landroid/media/videoeditor/MediaArtistNativeHelper;->nativePopulateSettings(Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;)V

    #@3c
    move-object v1, p0

    #@3d
    move-object v2, p1

    #@3e
    move-wide v3, p2

    #@3f
    move-wide v5, p4

    #@40
    move/from16 v7, p7

    #@42
    move/from16 v8, p6

    #@44
    .line 3035
    invoke-direct/range {v1 .. v8}, Landroid/media/videoeditor/MediaArtistNativeHelper;->nativeStartPreview(Landroid/view/Surface;JJIZ)V
    :try_end_47
    .catch Ljava/lang/IllegalArgumentException; {:try_start_e .. :try_end_47} :catch_48
    .catch Ljava/lang/IllegalStateException; {:try_start_e .. :try_end_47} :catch_51
    .catch Ljava/lang/RuntimeException; {:try_start_e .. :try_end_47} :catch_5a

    #@47
    .line 3049
    return-void

    #@48
    .line 3036
    :catch_48
    move-exception v10

    #@49
    .line 3037
    .local v10, ex:Ljava/lang/IllegalArgumentException;
    const-string v1, "MediaArtistNativeHelper"

    #@4b
    const-string v2, "Illegal argument exception in nativeStartPreview"

    #@4d
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    .line 3038
    throw v10

    #@51
    .line 3039
    .end local v10           #ex:Ljava/lang/IllegalArgumentException;
    :catch_51
    move-exception v10

    #@52
    .line 3040
    .local v10, ex:Ljava/lang/IllegalStateException;
    const-string v1, "MediaArtistNativeHelper"

    #@54
    const-string v2, "Illegal state exception in nativeStartPreview"

    #@56
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    .line 3041
    throw v10

    #@5a
    .line 3042
    .end local v10           #ex:Ljava/lang/IllegalStateException;
    :catch_5a
    move-exception v10

    #@5b
    .line 3043
    .local v10, ex:Ljava/lang/RuntimeException;
    const-string v1, "MediaArtistNativeHelper"

    #@5d
    const-string v2, "Runtime exception in nativeStartPreview"

    #@5f
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    .line 3044
    throw v10

    #@63
    .line 3047
    .end local v9           #clipCnt:I
    .end local v10           #ex:Ljava/lang/RuntimeException;
    :cond_63
    new-instance v1, Ljava/lang/IllegalStateException;

    #@65
    const-string v2, "generatePreview is in progress"

    #@67
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@6a
    throw v1
.end method

.method export(Ljava/lang/String;Ljava/lang/String;IILjava/util/List;Ljava/util/List;Ljava/util/List;Landroid/media/videoeditor/VideoEditor$ExportProgressListener;)V
    .registers 27
    .parameter "filePath"
    .parameter "projectDir"
    .parameter "height"
    .parameter "bitrate"
    .parameter
    .parameter
    .parameter
    .parameter "listener"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/List",
            "<",
            "Landroid/media/videoeditor/MediaItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/media/videoeditor/Transition;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/media/videoeditor/AudioTrack;",
            ">;",
            "Landroid/media/videoeditor/VideoEditor$ExportProgressListener;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 3620
    .local p5, mediaItemsList:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/MediaItem;>;"
    .local p6, mediaTransitionList:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Transition;>;"
    .local p7, mediaBGMList:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/AudioTrack;>;"
    const/4 v11, 0x0

    #@1
    .line 3621
    .local v11, outBitrate:I
    move-object/from16 v0, p1

    #@3
    move-object/from16 v1, p0

    #@5
    iput-object v0, v1, Landroid/media/videoeditor/MediaArtistNativeHelper;->mExportFilename:Ljava/lang/String;

    #@7
    .line 3622
    const/4 v15, 0x0

    #@8
    move-object/from16 v0, p0

    #@a
    move-object/from16 v1, p5

    #@c
    move-object/from16 v2, p6

    #@e
    move-object/from16 v3, p7

    #@10
    invoke-virtual {v0, v1, v2, v3, v15}, Landroid/media/videoeditor/MediaArtistNativeHelper;->previewStoryBoard(Ljava/util/List;Ljava/util/List;Ljava/util/List;Landroid/media/videoeditor/VideoEditor$MediaProcessingProgressListener;)V

    #@13
    .line 3623
    move-object/from16 v0, p8

    #@15
    move-object/from16 v1, p0

    #@17
    iput-object v0, v1, Landroid/media/videoeditor/MediaArtistNativeHelper;->mExportProgressListener:Landroid/media/videoeditor/VideoEditor$ExportProgressListener;

    #@19
    .line 3624
    const/4 v13, 0x0

    #@1a
    .line 3625
    .local v13, outVideoProfile:I
    const/4 v12, 0x0

    #@1b
    .line 3628
    .local v12, outVideoLevel:I
    invoke-static {}, Landroid/media/videoeditor/VideoEditorProfile;->get()Landroid/media/videoeditor/VideoEditorProfile;

    #@1e
    move-result-object v14

    #@1f
    .line 3629
    .local v14, veProfile:Landroid/media/videoeditor/VideoEditorProfile;
    if-nez v14, :cond_29

    #@21
    .line 3630
    new-instance v15, Ljava/lang/RuntimeException;

    #@23
    const-string v16, "Can\'t get the video editor profile"

    #@25
    invoke-direct/range {v15 .. v16}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@28
    throw v15

    #@29
    .line 3632
    :cond_29
    iget v9, v14, Landroid/media/videoeditor/VideoEditorProfile;->maxOutputVideoFrameHeight:I

    #@2b
    .line 3633
    .local v9, maxOutputHeight:I
    iget v10, v14, Landroid/media/videoeditor/VideoEditorProfile;->maxOutputVideoFrameWidth:I

    #@2d
    .line 3634
    .local v10, maxOutputWidth:I
    move/from16 v0, p3

    #@2f
    if-le v0, v9, :cond_66

    #@31
    .line 3635
    new-instance v15, Ljava/lang/IllegalArgumentException;

    #@33
    new-instance v16, Ljava/lang/StringBuilder;

    #@35
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v17, "Unsupported export resolution. Supported maximum width:"

    #@3a
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v16

    #@3e
    move-object/from16 v0, v16

    #@40
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@43
    move-result-object v16

    #@44
    const-string v17, " height:"

    #@46
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v16

    #@4a
    move-object/from16 v0, v16

    #@4c
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v16

    #@50
    const-string v17, " current height:"

    #@52
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v16

    #@56
    move-object/from16 v0, v16

    #@58
    move/from16 v1, p3

    #@5a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v16

    #@5e
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v16

    #@62
    invoke-direct/range {v15 .. v16}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@65
    throw v15

    #@66
    .line 3640
    :cond_66
    move-object/from16 v0, p0

    #@68
    iget v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mExportVideoCodec:I

    #@6a
    invoke-static {v15}, Landroid/media/videoeditor/VideoEditorProfile;->getExportProfile(I)I

    #@6d
    move-result v13

    #@6e
    .line 3641
    move-object/from16 v0, p0

    #@70
    iget v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mExportVideoCodec:I

    #@72
    invoke-static {v15}, Landroid/media/videoeditor/VideoEditorProfile;->getExportLevel(I)I

    #@75
    move-result v12

    #@76
    .line 3643
    const/4 v15, 0x0

    #@77
    move-object/from16 v0, p0

    #@79
    iput v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProgressToApp:I

    #@7b
    .line 3645
    sparse-switch p4, :sswitch_data_24c

    #@7e
    .line 3687
    new-instance v15, Ljava/lang/IllegalArgumentException;

    #@80
    const-string v16, "Argument Bitrate incorrect"

    #@82
    invoke-direct/range {v15 .. v16}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@85
    throw v15

    #@86
    .line 3647
    :sswitch_86
    const/16 v11, 0x7d00

    #@88
    .line 3689
    :goto_88
    move-object/from16 v0, p0

    #@8a
    iget-object v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@8c
    const/16 v16, 0x7

    #@8e
    move/from16 v0, v16

    #@90
    iput v0, v15, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameRate:I

    #@92
    .line 3690
    move-object/from16 v0, p0

    #@94
    iget-object v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@96
    move-object/from16 v0, p1

    #@98
    move-object/from16 v1, p0

    #@9a
    iput-object v0, v1, Landroid/media/videoeditor/MediaArtistNativeHelper;->mOutputFilename:Ljava/lang/String;

    #@9c
    move-object/from16 v0, p1

    #@9e
    iput-object v0, v15, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->outputFile:Ljava/lang/String;

    #@a0
    .line 3692
    move-object/from16 v0, p0

    #@a2
    iget-object v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    #@a4
    invoke-interface {v15}, Landroid/media/videoeditor/VideoEditor;->getAspectRatio()I

    #@a7
    move-result v4

    #@a8
    .line 3693
    .local v4, aspectRatio:I
    move-object/from16 v0, p0

    #@aa
    iget-object v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@ac
    move-object/from16 v0, p0

    #@ae
    move/from16 v1, p3

    #@b0
    invoke-direct {v0, v4, v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->findVideoResolution(II)I

    #@b3
    move-result v16

    #@b4
    move/from16 v0, v16

    #@b6
    iput v0, v15, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameSize:I

    #@b8
    .line 3694
    move-object/from16 v0, p0

    #@ba
    iget-object v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@bc
    move-object/from16 v0, p0

    #@be
    iget v0, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mExportVideoCodec:I

    #@c0
    move/from16 v16, v0

    #@c2
    move/from16 v0, v16

    #@c4
    iput v0, v15, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    #@c6
    .line 3695
    move-object/from16 v0, p0

    #@c8
    iget-object v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@ca
    move-object/from16 v0, p0

    #@cc
    iget v0, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mExportAudioCodec:I

    #@ce
    move/from16 v16, v0

    #@d0
    move/from16 v0, v16

    #@d2
    iput v0, v15, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->audioFormat:I

    #@d4
    .line 3696
    move-object/from16 v0, p0

    #@d6
    iget-object v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@d8
    iput v13, v15, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoProfile:I

    #@da
    .line 3697
    move-object/from16 v0, p0

    #@dc
    iget-object v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@de
    iput v12, v15, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoLevel:I

    #@e0
    .line 3698
    move-object/from16 v0, p0

    #@e2
    iget-object v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@e4
    const/16 v16, 0x7d00

    #@e6
    move/from16 v0, v16

    #@e8
    iput v0, v15, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->audioSamplingFreq:I

    #@ea
    .line 3699
    move-object/from16 v0, p0

    #@ec
    iget-object v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@ee
    const/16 v16, 0x0

    #@f0
    move/from16 v0, v16

    #@f2
    iput v0, v15, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->maxFileSize:I

    #@f4
    .line 3700
    move-object/from16 v0, p0

    #@f6
    iget-object v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@f8
    const/16 v16, 0x2

    #@fa
    move/from16 v0, v16

    #@fc
    iput v0, v15, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->audioChannels:I

    #@fe
    .line 3701
    move-object/from16 v0, p0

    #@100
    iget-object v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@102
    iput v11, v15, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoBitrate:I

    #@104
    .line 3702
    move-object/from16 v0, p0

    #@106
    iget-object v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@108
    const v16, 0x17700

    #@10b
    move/from16 v0, v16

    #@10d
    iput v0, v15, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->audioBitrate:I

    #@10f
    .line 3704
    move-object/from16 v0, p0

    #@111
    iget-object v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@113
    move-object/from16 v0, p0

    #@115
    iget v0, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mTotalClips:I

    #@117
    move/from16 v16, v0

    #@119
    add-int/lit8 v16, v16, -0x1

    #@11b
    move/from16 v0, v16

    #@11d
    new-array v0, v0, [Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;

    #@11f
    move-object/from16 v16, v0

    #@121
    move-object/from16 v0, v16

    #@123
    iput-object v0, v15, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->transitionSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;

    #@125
    .line 3705
    const/4 v8, 0x0

    #@126
    .local v8, index:I
    :goto_126
    move-object/from16 v0, p0

    #@128
    iget v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mTotalClips:I

    #@12a
    add-int/lit8 v15, v15, -0x1

    #@12c
    if-ge v8, v15, :cond_196

    #@12e
    .line 3706
    move-object/from16 v0, p0

    #@130
    iget-object v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@132
    iget-object v15, v15, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->transitionSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;

    #@134
    new-instance v16, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;

    #@136
    invoke-direct/range {v16 .. v16}, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;-><init>()V

    #@139
    aput-object v16, v15, v8

    #@13b
    .line 3707
    move-object/from16 v0, p0

    #@13d
    iget-object v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@13f
    iget-object v15, v15, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->transitionSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;

    #@141
    aget-object v15, v15, v8

    #@143
    const/16 v16, 0x0

    #@145
    move/from16 v0, v16

    #@147
    iput v0, v15, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;->videoTransitionType:I

    #@149
    .line 3709
    move-object/from16 v0, p0

    #@14b
    iget-object v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@14d
    iget-object v15, v15, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->transitionSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;

    #@14f
    aget-object v15, v15, v8

    #@151
    const/16 v16, 0x0

    #@153
    move/from16 v0, v16

    #@155
    iput v0, v15, Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;->audioTransitionType:I

    #@157
    .line 3705
    add-int/lit8 v8, v8, 0x1

    #@159
    goto :goto_126

    #@15a
    .line 3650
    .end local v4           #aspectRatio:I
    .end local v8           #index:I
    :sswitch_15a
    const v11, 0xbb80

    #@15d
    .line 3651
    goto/16 :goto_88

    #@15f
    .line 3653
    :sswitch_15f
    const v11, 0xfa00

    #@162
    .line 3654
    goto/16 :goto_88

    #@164
    .line 3656
    :sswitch_164
    const v11, 0x17700

    #@167
    .line 3657
    goto/16 :goto_88

    #@169
    .line 3659
    :sswitch_169
    const v11, 0x1f400

    #@16c
    .line 3660
    goto/16 :goto_88

    #@16e
    .line 3662
    :sswitch_16e
    const v11, 0x2ee00

    #@171
    .line 3663
    goto/16 :goto_88

    #@173
    .line 3665
    :sswitch_173
    const v11, 0x3e800

    #@176
    .line 3666
    goto/16 :goto_88

    #@178
    .line 3668
    :sswitch_178
    const v11, 0x5dc00

    #@17b
    .line 3669
    goto/16 :goto_88

    #@17d
    .line 3671
    :sswitch_17d
    const v11, 0x7d000

    #@180
    .line 3672
    goto/16 :goto_88

    #@182
    .line 3674
    :sswitch_182
    const v11, 0xc3500

    #@185
    .line 3675
    goto/16 :goto_88

    #@187
    .line 3677
    :sswitch_187
    const v11, 0x1e8480

    #@18a
    .line 3678
    goto/16 :goto_88

    #@18c
    .line 3680
    :sswitch_18c
    const v11, 0x4c4b40

    #@18f
    .line 3681
    goto/16 :goto_88

    #@191
    .line 3683
    :sswitch_191
    const v11, 0x7a1200

    #@194
    .line 3684
    goto/16 :goto_88

    #@196
    .line 3713
    .restart local v4       #aspectRatio:I
    .restart local v8       #index:I
    :cond_196
    const/4 v5, 0x0

    #@197
    .local v5, clipCnt:I
    :goto_197
    move-object/from16 v0, p0

    #@199
    iget-object v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@19b
    iget-object v15, v15, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@19d
    array-length v15, v15

    #@19e
    if-ge v5, v15, :cond_1d3

    #@1a0
    .line 3714
    move-object/from16 v0, p0

    #@1a2
    iget-object v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@1a4
    iget-object v15, v15, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@1a6
    aget-object v15, v15, v5

    #@1a8
    iget v15, v15, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->fileType:I

    #@1aa
    const/16 v16, 0x5

    #@1ac
    move/from16 v0, v16

    #@1ae
    if-ne v15, v0, :cond_1d0

    #@1b0
    .line 3715
    move-object/from16 v0, p0

    #@1b2
    iget-object v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@1b4
    iget-object v15, v15, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@1b6
    aget-object v15, v15, v5

    #@1b8
    move-object/from16 v0, p0

    #@1ba
    iget-object v0, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@1bc
    move-object/from16 v16, v0

    #@1be
    move-object/from16 v0, v16

    #@1c0
    iget-object v0, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@1c2
    move-object/from16 v16, v0

    #@1c4
    aget-object v16, v16, v5

    #@1c6
    move-object/from16 v0, v16

    #@1c8
    iget-object v0, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipOriginalPath:Ljava/lang/String;

    #@1ca
    move-object/from16 v16, v0

    #@1cc
    move-object/from16 v0, v16

    #@1ce
    iput-object v0, v15, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipPath:Ljava/lang/String;

    #@1d0
    .line 3713
    :cond_1d0
    add-int/lit8 v5, v5, 0x1

    #@1d2
    goto :goto_197

    #@1d3
    .line 3719
    :cond_1d3
    move-object/from16 v0, p0

    #@1d5
    iget-object v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@1d7
    move-object/from16 v0, p0

    #@1d9
    iget-object v0, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@1db
    move-object/from16 v16, v0

    #@1dd
    move-object/from16 v0, p0

    #@1df
    iget-object v0, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;

    #@1e1
    move-object/from16 v17, v0

    #@1e3
    move-object/from16 v0, p0

    #@1e5
    move-object/from16 v1, v16

    #@1e7
    move-object/from16 v2, v17

    #@1e9
    invoke-direct {v0, v15, v1, v2}, Landroid/media/videoeditor/MediaArtistNativeHelper;->nativePopulateSettings(Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;)V

    #@1ec
    .line 3721
    const/4 v6, 0x0

    #@1ed
    .line 3723
    .local v6, err:I
    const/16 v15, 0x14

    #@1ef
    :try_start_1ef
    move-object/from16 v0, p0

    #@1f1
    iput v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProcessingState:I

    #@1f3
    .line 3724
    const/4 v15, 0x0

    #@1f4
    move-object/from16 v0, p0

    #@1f6
    iput-object v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProcessingObject:Ljava/lang/Object;

    #@1f8
    .line 3725
    move-object/from16 v0, p0

    #@1fa
    iget-object v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@1fc
    move-object/from16 v0, p0

    #@1fe
    invoke-virtual {v0, v15}, Landroid/media/videoeditor/MediaArtistNativeHelper;->generateClip(Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;)I

    #@201
    move-result v6

    #@202
    .line 3726
    const/4 v15, 0x0

    #@203
    move-object/from16 v0, p0

    #@205
    iput v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProcessingState:I
    :try_end_207
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1ef .. :try_end_207} :catch_22b
    .catch Ljava/lang/IllegalStateException; {:try_start_1ef .. :try_end_207} :catch_234
    .catch Ljava/lang/RuntimeException; {:try_start_1ef .. :try_end_207} :catch_23d

    #@207
    .line 3738
    if-eqz v6, :cond_246

    #@209
    .line 3739
    const-string v15, "MediaArtistNativeHelper"

    #@20b
    const-string v16, "RuntimeException for generateClip"

    #@20d
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@210
    .line 3740
    new-instance v15, Ljava/lang/RuntimeException;

    #@212
    new-instance v16, Ljava/lang/StringBuilder;

    #@214
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@217
    const-string v17, "generateClip failed with error="

    #@219
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21c
    move-result-object v16

    #@21d
    move-object/from16 v0, v16

    #@21f
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@222
    move-result-object v16

    #@223
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@226
    move-result-object v16

    #@227
    invoke-direct/range {v15 .. v16}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@22a
    throw v15

    #@22b
    .line 3727
    :catch_22b
    move-exception v7

    #@22c
    .line 3728
    .local v7, ex:Ljava/lang/IllegalArgumentException;
    const-string v15, "MediaArtistNativeHelper"

    #@22e
    const-string v16, "IllegalArgument for generateClip"

    #@230
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@233
    .line 3729
    throw v7

    #@234
    .line 3730
    .end local v7           #ex:Ljava/lang/IllegalArgumentException;
    :catch_234
    move-exception v7

    #@235
    .line 3731
    .local v7, ex:Ljava/lang/IllegalStateException;
    const-string v15, "MediaArtistNativeHelper"

    #@237
    const-string v16, "IllegalStateExceptiont for generateClip"

    #@239
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23c
    .line 3732
    throw v7

    #@23d
    .line 3733
    .end local v7           #ex:Ljava/lang/IllegalStateException;
    :catch_23d
    move-exception v7

    #@23e
    .line 3734
    .local v7, ex:Ljava/lang/RuntimeException;
    const-string v15, "MediaArtistNativeHelper"

    #@240
    const-string v16, "RuntimeException for generateClip"

    #@242
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@245
    .line 3735
    throw v7

    #@246
    .line 3743
    .end local v7           #ex:Ljava/lang/RuntimeException;
    :cond_246
    const/4 v15, 0x0

    #@247
    move-object/from16 v0, p0

    #@249
    iput-object v15, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mExportProgressListener:Landroid/media/videoeditor/VideoEditor$ExportProgressListener;

    #@24b
    .line 3744
    return-void

    #@24c
    .line 3645
    :sswitch_data_24c
    .sparse-switch
        0x6d60 -> :sswitch_86
        0x9c40 -> :sswitch_15a
        0xfa00 -> :sswitch_15f
        0x17700 -> :sswitch_164
        0x1f400 -> :sswitch_169
        0x2ee00 -> :sswitch_16e
        0x3e800 -> :sswitch_173
        0x5dc00 -> :sswitch_178
        0x7d000 -> :sswitch_17d
        0xc3500 -> :sswitch_182
        0x1e8480 -> :sswitch_187
        0x4c4b40 -> :sswitch_18c
        0x7a1200 -> :sswitch_191
    .end sparse-switch
.end method

.method generateAudioGraph(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILandroid/media/videoeditor/ExtractAudioWaveformProgressListener;Z)V
    .registers 15
    .parameter "uniqueId"
    .parameter "inFileName"
    .parameter "OutAudiGraphFileName"
    .parameter "frameDuration"
    .parameter "audioChannels"
    .parameter "samplesCount"
    .parameter "listener"
    .parameter "isVideo"

    #@0
    .prologue
    .line 3889
    iput-object p7, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mExtractAudioWaveformProgressListener:Landroid/media/videoeditor/ExtractAudioWaveformProgressListener;

    #@2
    .line 3895
    if-eqz p8, :cond_42

    #@4
    .line 3896
    new-instance v0, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProjectPath:Ljava/lang/String;

    #@b
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v2, "/"

    #@11
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    const-string v2, ".pcm"

    #@1b
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    const/4 v2, 0x0

    #@24
    new-array v2, v2, [Ljava/lang/Object;

    #@26
    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    .line 3904
    .local v1, tempPCMFileName:Ljava/lang/String;
    :goto_2a
    if-eqz p8, :cond_2f

    #@2c
    .line 3905
    invoke-direct {p0, p2, v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->nativeGenerateRawAudio(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    :cond_2f
    move-object v0, p0

    #@30
    move-object v2, p3

    #@31
    move v3, p4

    #@32
    move v4, p5

    #@33
    move v5, p6

    #@34
    .line 3908
    invoke-direct/range {v0 .. v5}, Landroid/media/videoeditor/MediaArtistNativeHelper;->nativeGenerateAudioGraph(Ljava/lang/String;Ljava/lang/String;III)I

    #@37
    .line 3914
    if-eqz p8, :cond_41

    #@39
    .line 3915
    new-instance v0, Ljava/io/File;

    #@3b
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@3e
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@41
    .line 3917
    :cond_41
    return-void

    #@42
    .line 3898
    .end local v1           #tempPCMFileName:Ljava/lang/String;
    :cond_42
    iget-object v1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrackPCMFilePath:Ljava/lang/String;

    #@44
    .restart local v1       #tempPCMFileName:Ljava/lang/String;
    goto :goto_2a
.end method

.method public generateClip(Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;)I
    .registers 7
    .parameter "editSettings"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 2229
    const/4 v0, 0x0

    #@2
    .line 2232
    .local v0, err:I
    :try_start_2
    invoke-direct {p0, p1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->nativeGenerateClip(Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;)I
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_5} :catch_8
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_5} :catch_11
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_5} :catch_1a

    #@5
    move-result v0

    #@6
    move v2, v0

    #@7
    .line 2243
    :goto_7
    return v2

    #@8
    .line 2233
    :catch_8
    move-exception v1

    #@9
    .line 2234
    .local v1, ex:Ljava/lang/IllegalArgumentException;
    const-string v3, "MediaArtistNativeHelper"

    #@b
    const-string v4, "Illegal Argument exception in load settings"

    #@d
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    goto :goto_7

    #@11
    .line 2236
    .end local v1           #ex:Ljava/lang/IllegalArgumentException;
    :catch_11
    move-exception v1

    #@12
    .line 2237
    .local v1, ex:Ljava/lang/IllegalStateException;
    const-string v3, "MediaArtistNativeHelper"

    #@14
    const-string v4, "Illegal state exception in load settings"

    #@16
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    goto :goto_7

    #@1a
    .line 2239
    .end local v1           #ex:Ljava/lang/IllegalStateException;
    :catch_1a
    move-exception v1

    #@1b
    .line 2240
    .local v1, ex:Ljava/lang/RuntimeException;
    const-string v3, "MediaArtistNativeHelper"

    #@1d
    const-string v4, "Runtime exception in load settings"

    #@1f
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    goto :goto_7
.end method

.method generateEffectClip(Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;Ljava/lang/String;I)Ljava/lang/String;
    .registers 15
    .parameter "lMediaItem"
    .parameter "lclipSettings"
    .parameter "e"
    .parameter "uniqueId"
    .parameter "clipNo"

    #@0
    .prologue
    .line 2288
    const/4 v2, 0x0

    #@1
    .line 2289
    .local v2, err:I
    const/4 v1, 0x0

    #@2
    .line 2290
    .local v1, editSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;
    const/4 v0, 0x0

    #@3
    .line 2291
    .local v0, EffectClipPath:Ljava/lang/String;
    const/4 v5, 0x0

    #@4
    .line 2292
    .local v5, outVideoProfile:I
    const/4 v4, 0x0

    #@5
    .line 2293
    .local v4, outVideoLevel:I
    new-instance v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@7
    .end local v1           #editSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;
    invoke-direct {v1}, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;-><init>()V

    #@a
    .line 2295
    .restart local v1       #editSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;
    const/4 v7, 0x1

    #@b
    new-array v7, v7, [Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@d
    iput-object v7, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@f
    .line 2296
    iget-object v7, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@11
    const/4 v8, 0x0

    #@12
    aput-object p2, v7, v8

    #@14
    .line 2298
    const/4 v7, 0x0

    #@15
    iput-object v7, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->backgroundMusicSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;

    #@17
    .line 2299
    const/4 v7, 0x0

    #@18
    iput-object v7, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->transitionSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;

    #@1a
    .line 2300
    iget-object v7, p3, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->effectSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;

    #@1c
    iput-object v7, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->effectSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;

    #@1e
    .line 2302
    new-instance v7, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    iget-object v8, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProjectPath:Ljava/lang/String;

    #@25
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v7

    #@29
    const-string v8, "/"

    #@2b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v7

    #@2f
    const-string v8, "ClipEffectIntermediate"

    #@31
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v7

    #@35
    const-string v8, "_"

    #@37
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v7

    #@3b
    invoke-virtual {p1}, Landroid/media/videoeditor/MediaItem;->getId()Ljava/lang/String;

    #@3e
    move-result-object v8

    #@3f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v7

    #@43
    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v7

    #@47
    const-string v8, ".3gp"

    #@49
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v7

    #@4d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v7

    #@51
    const/4 v8, 0x0

    #@52
    new-array v8, v8, [Ljava/lang/Object;

    #@54
    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@57
    move-result-object v0

    #@58
    .line 2305
    new-instance v6, Ljava/io/File;

    #@5a
    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@5d
    .line 2306
    .local v6, tmpFile:Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    #@60
    move-result v7

    #@61
    if-eqz v7, :cond_66

    #@63
    .line 2307
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    #@66
    .line 2310
    :cond_66
    const/4 v7, 0x2

    #@67
    invoke-static {v7}, Landroid/media/videoeditor/VideoEditorProfile;->getExportProfile(I)I

    #@6a
    move-result v5

    #@6b
    .line 2311
    const/4 v7, 0x2

    #@6c
    invoke-static {v7}, Landroid/media/videoeditor/VideoEditorProfile;->getExportLevel(I)I

    #@6f
    move-result v4

    #@70
    .line 2312
    iput v5, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoProfile:I

    #@72
    .line 2313
    iput v4, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoLevel:I

    #@74
    .line 2315
    instance-of v7, p1, Landroid/media/videoeditor/MediaVideoItem;

    #@76
    if-eqz v7, :cond_c2

    #@78
    move-object v3, p1

    #@79
    .line 2316
    check-cast v3, Landroid/media/videoeditor/MediaVideoItem;

    #@7b
    .line 2318
    .local v3, m:Landroid/media/videoeditor/MediaVideoItem;
    const/4 v7, 0x2

    #@7c
    iput v7, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->audioFormat:I

    #@7e
    .line 2319
    const/4 v7, 0x2

    #@7f
    iput v7, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->audioChannels:I

    #@81
    .line 2320
    const v7, 0xfa00

    #@84
    iput v7, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->audioBitrate:I

    #@86
    .line 2321
    const/16 v7, 0x7d00

    #@88
    iput v7, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->audioSamplingFreq:I

    #@8a
    .line 2323
    const/4 v7, 0x2

    #@8b
    iput v7, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    #@8d
    .line 2324
    const/4 v7, 0x7

    #@8e
    iput v7, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameRate:I

    #@90
    .line 2325
    iget-object v7, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    #@92
    invoke-interface {v7}, Landroid/media/videoeditor/VideoEditor;->getAspectRatio()I

    #@95
    move-result v7

    #@96
    invoke-virtual {v3}, Landroid/media/videoeditor/MediaVideoItem;->getHeight()I

    #@99
    move-result v8

    #@9a
    invoke-direct {p0, v7, v8}, Landroid/media/videoeditor/MediaArtistNativeHelper;->findVideoResolution(II)I

    #@9d
    move-result v7

    #@9e
    iput v7, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameSize:I

    #@a0
    .line 2327
    iget v7, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameSize:I

    #@a2
    invoke-direct {p0, v7}, Landroid/media/videoeditor/MediaArtistNativeHelper;->findVideoBitrate(I)I

    #@a5
    move-result v7

    #@a6
    iput v7, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoBitrate:I

    #@a8
    .line 2342
    .end local v3           #m:Landroid/media/videoeditor/MediaVideoItem;
    :goto_a8
    iput-object v0, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->outputFile:Ljava/lang/String;

    #@aa
    .line 2344
    const/4 v7, 0x1

    #@ab
    if-ne p5, v7, :cond_f3

    #@ad
    .line 2345
    const/16 v7, 0xb

    #@af
    iput v7, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProcessingState:I

    #@b1
    .line 2349
    :cond_b1
    :goto_b1
    iput-object p1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProcessingObject:Ljava/lang/Object;

    #@b3
    .line 2350
    invoke-virtual {p0, v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->generateClip(Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;)I

    #@b6
    move-result v2

    #@b7
    .line 2351
    const/4 v7, 0x0

    #@b8
    iput v7, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProcessingState:I

    #@ba
    .line 2353
    if-nez v2, :cond_fb

    #@bc
    .line 2354
    iput-object v0, p2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipPath:Ljava/lang/String;

    #@be
    .line 2355
    const/4 v7, 0x0

    #@bf
    iput v7, p2, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->fileType:I

    #@c1
    .line 2356
    return-object v0

    #@c2
    :cond_c2
    move-object v3, p1

    #@c3
    .line 2329
    check-cast v3, Landroid/media/videoeditor/MediaImageItem;

    #@c5
    .line 2330
    .local v3, m:Landroid/media/videoeditor/MediaImageItem;
    const v7, 0xfa00

    #@c8
    iput v7, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->audioBitrate:I

    #@ca
    .line 2331
    const/4 v7, 0x2

    #@cb
    iput v7, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->audioChannels:I

    #@cd
    .line 2332
    const/4 v7, 0x2

    #@ce
    iput v7, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->audioFormat:I

    #@d0
    .line 2333
    const/16 v7, 0x7d00

    #@d2
    iput v7, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->audioSamplingFreq:I

    #@d4
    .line 2335
    const/4 v7, 0x2

    #@d5
    iput v7, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    #@d7
    .line 2336
    const/4 v7, 0x7

    #@d8
    iput v7, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameRate:I

    #@da
    .line 2337
    iget-object v7, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    #@dc
    invoke-interface {v7}, Landroid/media/videoeditor/VideoEditor;->getAspectRatio()I

    #@df
    move-result v7

    #@e0
    invoke-virtual {v3}, Landroid/media/videoeditor/MediaImageItem;->getScaledHeight()I

    #@e3
    move-result v8

    #@e4
    invoke-direct {p0, v7, v8}, Landroid/media/videoeditor/MediaArtistNativeHelper;->findVideoResolution(II)I

    #@e7
    move-result v7

    #@e8
    iput v7, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameSize:I

    #@ea
    .line 2339
    iget v7, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameSize:I

    #@ec
    invoke-direct {p0, v7}, Landroid/media/videoeditor/MediaArtistNativeHelper;->findVideoBitrate(I)I

    #@ef
    move-result v7

    #@f0
    iput v7, v1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoBitrate:I

    #@f2
    goto :goto_a8

    #@f3
    .line 2346
    .end local v3           #m:Landroid/media/videoeditor/MediaImageItem;
    :cond_f3
    const/4 v7, 0x2

    #@f4
    if-ne p5, v7, :cond_b1

    #@f6
    .line 2347
    const/16 v7, 0xc

    #@f8
    iput v7, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProcessingState:I

    #@fa
    goto :goto_b1

    #@fb
    .line 2358
    :cond_fb
    new-instance v7, Ljava/lang/RuntimeException;

    #@fd
    const-string/jumbo v8, "preview generation cannot be completed"

    #@100
    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@103
    throw v7
.end method

.method generateKenBurnsClip(Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;Landroid/media/videoeditor/MediaImageItem;)Ljava/lang/String;
    .registers 12
    .parameter "e"
    .parameter "m"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v5, 0x0

    #@2
    const/4 v7, 0x2

    #@3
    .line 2373
    const/4 v3, 0x0

    #@4
    .line 2374
    .local v3, output:Ljava/lang/String;
    const/4 v0, 0x0

    #@5
    .line 2375
    .local v0, err:I
    const/4 v2, 0x0

    #@6
    .line 2376
    .local v2, outVideoProfile:I
    const/4 v1, 0x0

    #@7
    .line 2378
    .local v1, outVideoLevel:I
    iput-object v5, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->backgroundMusicSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$BackgroundMusicSettings;

    #@9
    .line 2379
    iput-object v5, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->transitionSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$TransitionSettings;

    #@b
    .line 2380
    iput-object v5, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->effectSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;

    #@d
    .line 2381
    new-instance v5, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    iget-object v6, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProjectPath:Ljava/lang/String;

    #@14
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v5

    #@18
    const-string v6, "/"

    #@1a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v5

    #@1e
    const-string v6, "ImageClip-"

    #@20
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v5

    #@24
    invoke-virtual {p2}, Landroid/media/videoeditor/MediaImageItem;->getId()Ljava/lang/String;

    #@27
    move-result-object v6

    #@28
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v5

    #@2c
    const-string v6, ".3gp"

    #@2e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v5

    #@32
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v5

    #@36
    new-array v6, v8, [Ljava/lang/Object;

    #@38
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@3b
    move-result-object v3

    #@3c
    .line 2383
    new-instance v4, Ljava/io/File;

    #@3e
    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@41
    .line 2384
    .local v4, tmpFile:Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    #@44
    move-result v5

    #@45
    if-eqz v5, :cond_4a

    #@47
    .line 2385
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    #@4a
    .line 2388
    :cond_4a
    invoke-static {v7}, Landroid/media/videoeditor/VideoEditorProfile;->getExportProfile(I)I

    #@4d
    move-result v2

    #@4e
    .line 2389
    invoke-static {v7}, Landroid/media/videoeditor/VideoEditorProfile;->getExportLevel(I)I

    #@51
    move-result v1

    #@52
    .line 2390
    iput v2, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoProfile:I

    #@54
    .line 2391
    iput v1, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoLevel:I

    #@56
    .line 2393
    iput-object v3, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->outputFile:Ljava/lang/String;

    #@58
    .line 2394
    const v5, 0xfa00

    #@5b
    iput v5, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->audioBitrate:I

    #@5d
    .line 2395
    iput v7, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->audioChannels:I

    #@5f
    .line 2396
    iput v7, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->audioFormat:I

    #@61
    .line 2397
    const/16 v5, 0x7d00

    #@63
    iput v5, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->audioSamplingFreq:I

    #@65
    .line 2399
    iput v7, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    #@67
    .line 2400
    const/4 v5, 0x7

    #@68
    iput v5, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameRate:I

    #@6a
    .line 2401
    iget-object v5, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    #@6c
    invoke-interface {v5}, Landroid/media/videoeditor/VideoEditor;->getAspectRatio()I

    #@6f
    move-result v5

    #@70
    invoke-virtual {p2}, Landroid/media/videoeditor/MediaImageItem;->getScaledHeight()I

    #@73
    move-result v6

    #@74
    invoke-direct {p0, v5, v6}, Landroid/media/videoeditor/MediaArtistNativeHelper;->findVideoResolution(II)I

    #@77
    move-result v5

    #@78
    iput v5, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameSize:I

    #@7a
    .line 2403
    iget v5, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameSize:I

    #@7c
    invoke-direct {p0, v5}, Landroid/media/videoeditor/MediaArtistNativeHelper;->findVideoBitrate(I)I

    #@7f
    move-result v5

    #@80
    iput v5, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoBitrate:I

    #@82
    .line 2405
    const/4 v5, 0x3

    #@83
    iput v5, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProcessingState:I

    #@85
    .line 2406
    iput-object p2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProcessingObject:Ljava/lang/Object;

    #@87
    .line 2407
    invoke-virtual {p0, p1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->generateClip(Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;)I

    #@8a
    move-result v0

    #@8b
    .line 2409
    iput v8, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProcessingState:I

    #@8d
    .line 2410
    if-eqz v0, :cond_98

    #@8f
    .line 2411
    new-instance v5, Ljava/lang/RuntimeException;

    #@91
    const-string/jumbo v6, "preview generation cannot be completed"

    #@94
    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@97
    throw v5

    #@98
    .line 2413
    :cond_98
    return-object v3
.end method

.method generateTransitionClip(Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;Ljava/lang/String;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/Transition;)Ljava/lang/String;
    .registers 14
    .parameter "e"
    .parameter "uniqueId"
    .parameter "m1"
    .parameter "m2"
    .parameter "t"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x2

    #@2
    .line 2478
    const/4 v3, 0x0

    #@3
    .line 2479
    .local v3, outputFilename:Ljava/lang/String;
    const/4 v0, 0x0

    #@4
    .line 2480
    .local v0, err:I
    const/4 v2, 0x0

    #@5
    .line 2481
    .local v2, outVideoProfile:I
    const/4 v1, 0x0

    #@6
    .line 2482
    .local v1, outVideoLevel:I
    new-instance v4, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    iget-object v5, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProjectPath:Ljava/lang/String;

    #@d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    const-string v5, "/"

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    const-string v5, ".3gp"

    #@1d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v4

    #@25
    new-array v5, v7, [Ljava/lang/Object;

    #@27
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    .line 2484
    invoke-static {v6}, Landroid/media/videoeditor/VideoEditorProfile;->getExportProfile(I)I

    #@2e
    move-result v2

    #@2f
    .line 2485
    invoke-static {v6}, Landroid/media/videoeditor/VideoEditorProfile;->getExportLevel(I)I

    #@32
    move-result v1

    #@33
    .line 2486
    iput v2, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoProfile:I

    #@35
    .line 2487
    iput v1, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoLevel:I

    #@37
    .line 2489
    iput-object v3, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->outputFile:Ljava/lang/String;

    #@39
    .line 2490
    const v4, 0xfa00

    #@3c
    iput v4, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->audioBitrate:I

    #@3e
    .line 2491
    iput v6, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->audioChannels:I

    #@40
    .line 2492
    iput v6, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->audioFormat:I

    #@42
    .line 2493
    const/16 v4, 0x7d00

    #@44
    iput v4, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->audioSamplingFreq:I

    #@46
    .line 2495
    iput v6, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFormat:I

    #@48
    .line 2496
    const/4 v4, 0x7

    #@49
    iput v4, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameRate:I

    #@4b
    .line 2497
    invoke-direct {p0, p3, p4}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getTransitionResolution(Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaItem;)I

    #@4e
    move-result v4

    #@4f
    iput v4, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameSize:I

    #@51
    .line 2498
    iget v4, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameSize:I

    #@53
    invoke-direct {p0, v4}, Landroid/media/videoeditor/MediaArtistNativeHelper;->findVideoBitrate(I)I

    #@56
    move-result v4

    #@57
    iput v4, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoBitrate:I

    #@59
    .line 2500
    new-instance v4, Ljava/io/File;

    #@5b
    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@5e
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    #@61
    move-result v4

    #@62
    if-eqz v4, :cond_6c

    #@64
    .line 2501
    new-instance v4, Ljava/io/File;

    #@66
    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@69
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    #@6c
    .line 2503
    :cond_6c
    const/16 v4, 0xd

    #@6e
    iput v4, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProcessingState:I

    #@70
    .line 2504
    iput-object p5, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProcessingObject:Ljava/lang/Object;

    #@72
    .line 2505
    invoke-virtual {p0, p1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->generateClip(Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;)I

    #@75
    move-result v0

    #@76
    .line 2507
    iput v7, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProcessingState:I

    #@78
    .line 2508
    if-eqz v0, :cond_83

    #@7a
    .line 2509
    new-instance v4, Ljava/lang/RuntimeException;

    #@7c
    const-string/jumbo v5, "preview generation cannot be completed"

    #@7f
    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@82
    throw v4

    #@83
    .line 2511
    :cond_83
    return-object v3
.end method

.method getAspectRatio(II)I
    .registers 11
    .parameter "w"
    .parameter "h"

    #@0
    .prologue
    .line 3193
    int-to-double v4, p1

    #@1
    int-to-double v6, p2

    #@2
    div-double v0, v4, v6

    #@4
    .line 3194
    .local v0, apRatio:D
    new-instance v2, Ljava/math/BigDecimal;

    #@6
    invoke-direct {v2, v0, v1}, Ljava/math/BigDecimal;-><init>(D)V

    #@9
    .line 3195
    .local v2, bd:Ljava/math/BigDecimal;
    const/4 v4, 0x3

    #@a
    const/4 v5, 0x4

    #@b
    invoke-virtual {v2, v4, v5}, Ljava/math/BigDecimal;->setScale(II)Ljava/math/BigDecimal;

    #@e
    move-result-object v2

    #@f
    .line 3196
    invoke-virtual {v2}, Ljava/math/BigDecimal;->doubleValue()D

    #@12
    move-result-wide v0

    #@13
    .line 3197
    const/4 v3, 0x2

    #@14
    .line 3198
    .local v3, var:I
    const-wide v4, 0x3ffb333333333333L

    #@19
    cmpl-double v4, v0, v4

    #@1b
    if-ltz v4, :cond_1f

    #@1d
    .line 3199
    const/4 v3, 0x2

    #@1e
    .line 3209
    :cond_1e
    :goto_1e
    return v3

    #@1f
    .line 3200
    :cond_1f
    const-wide v4, 0x3ff999999999999aL

    #@24
    cmpl-double v4, v0, v4

    #@26
    if-ltz v4, :cond_2a

    #@28
    .line 3201
    const/4 v3, 0x4

    #@29
    goto :goto_1e

    #@2a
    .line 3202
    :cond_2a
    const-wide/high16 v4, 0x3ff8

    #@2c
    cmpl-double v4, v0, v4

    #@2e
    if-ltz v4, :cond_32

    #@30
    .line 3203
    const/4 v3, 0x1

    #@31
    goto :goto_1e

    #@32
    .line 3204
    :cond_32
    const-wide v4, 0x3ff4cccccccccccdL

    #@37
    cmpl-double v4, v0, v4

    #@39
    if-lez v4, :cond_3d

    #@3b
    .line 3205
    const/4 v3, 0x3

    #@3c
    goto :goto_1e

    #@3d
    .line 3206
    :cond_3d
    const-wide v4, 0x3ff3333333333333L

    #@42
    cmpl-double v4, v0, v4

    #@44
    if-ltz v4, :cond_1e

    #@46
    .line 3207
    const/4 v3, 0x5

    #@47
    goto :goto_1e
.end method

.method getAudioCodecType(I)I
    .registers 3
    .parameter "codecType"

    #@0
    .prologue
    .line 3291
    const/4 v0, -0x1

    #@1
    .line 3292
    .local v0, retValue:I
    packed-switch p1, :pswitch_data_c

    #@4
    .line 3304
    :pswitch_4
    const/4 v0, -0x1

    #@5
    .line 3306
    :goto_5
    return v0

    #@6
    .line 3294
    :pswitch_6
    const/4 v0, 0x1

    #@7
    .line 3295
    goto :goto_5

    #@8
    .line 3297
    :pswitch_8
    const/4 v0, 0x2

    #@9
    .line 3298
    goto :goto_5

    #@a
    .line 3300
    :pswitch_a
    const/4 v0, 0x5

    #@b
    .line 3301
    goto :goto_5

    #@c
    .line 3292
    :pswitch_data_c
    .packed-switch 0x1
        :pswitch_6
        :pswitch_8
        :pswitch_4
        :pswitch_4
        :pswitch_a
    .end packed-switch
.end method

.method getAudioflag()Z
    .registers 2

    #@0
    .prologue
    .line 2102
    iget-boolean v0, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mRegenerateAudio:Z

    #@2
    return v0
.end method

.method getEffectSettings(Landroid/media/videoeditor/EffectColor;)Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;
    .registers 7
    .parameter "effects"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    .line 1906
    new-instance v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;

    #@4
    invoke-direct {v0}, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;-><init>()V

    #@7
    .line 1907
    .local v0, effectSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;
    invoke-virtual {p1}, Landroid/media/videoeditor/EffectColor;->getStartTime()J

    #@a
    move-result-wide v1

    #@b
    long-to-int v1, v1

    #@c
    iput v1, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@e
    .line 1908
    invoke-virtual {p1}, Landroid/media/videoeditor/EffectColor;->getDuration()J

    #@11
    move-result-wide v1

    #@12
    long-to-int v1, v1

    #@13
    iput v1, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->duration:I

    #@15
    .line 1909
    invoke-direct {p0, p1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getEffectColorType(Landroid/media/videoeditor/EffectColor;)I

    #@18
    move-result v1

    #@19
    iput v1, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->videoEffectType:I

    #@1b
    .line 1910
    iput v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->audioEffectType:I

    #@1d
    .line 1911
    iput v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startPercent:I

    #@1f
    .line 1912
    iput v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->durationPercent:I

    #@21
    .line 1913
    iput-object v4, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->framingFile:Ljava/lang/String;

    #@23
    .line 1914
    iput v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->topLeftX:I

    #@25
    .line 1915
    iput v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->topLeftY:I

    #@27
    .line 1916
    iput-boolean v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->framingResize:Z

    #@29
    .line 1917
    iput-object v4, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->text:Ljava/lang/String;

    #@2b
    .line 1918
    iput-object v4, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->textRenderingData:Ljava/lang/String;

    #@2d
    .line 1919
    iput v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->textBufferWidth:I

    #@2f
    .line 1920
    iput v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->textBufferHeight:I

    #@31
    .line 1921
    invoke-virtual {p1}, Landroid/media/videoeditor/EffectColor;->getType()I

    #@34
    move-result v1

    #@35
    const/4 v2, 0x5

    #@36
    if-ne v1, v2, :cond_59

    #@38
    .line 1922
    const/16 v1, 0xf

    #@3a
    iput v1, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->fiftiesFrameRate:I

    #@3c
    .line 1927
    :goto_3c
    iget v1, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->videoEffectType:I

    #@3e
    const/16 v2, 0x10b

    #@40
    if-eq v1, v2, :cond_48

    #@42
    iget v1, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->videoEffectType:I

    #@44
    const/16 v2, 0x10c

    #@46
    if-ne v1, v2, :cond_4e

    #@48
    .line 1929
    :cond_48
    invoke-virtual {p1}, Landroid/media/videoeditor/EffectColor;->getColor()I

    #@4b
    move-result v1

    #@4c
    iput v1, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->rgb16InputColor:I

    #@4e
    .line 1932
    :cond_4e
    iput v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->alphaBlendingStartPercent:I

    #@50
    .line 1933
    iput v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->alphaBlendingMiddlePercent:I

    #@52
    .line 1934
    iput v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->alphaBlendingEndPercent:I

    #@54
    .line 1935
    iput v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->alphaBlendingFadeInTimePercent:I

    #@56
    .line 1936
    iput v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->alphaBlendingFadeOutTimePercent:I

    #@58
    .line 1937
    return-object v0

    #@59
    .line 1924
    :cond_59
    iput v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->fiftiesFrameRate:I

    #@5b
    goto :goto_3c
.end method

.method getFileType(I)I
    .registers 3
    .parameter "fileType"

    #@0
    .prologue
    .line 3221
    const/4 v0, -0x1

    #@1
    .line 3222
    .local v0, retValue:I
    sparse-switch p1, :sswitch_data_1a

    #@4
    .line 3249
    const/4 v0, -0x1

    #@5
    .line 3251
    :goto_5
    return v0

    #@6
    .line 3224
    :sswitch_6
    const/16 v0, 0xff

    #@8
    .line 3225
    goto :goto_5

    #@9
    .line 3227
    :sswitch_9
    const/4 v0, 0x0

    #@a
    .line 3228
    goto :goto_5

    #@b
    .line 3230
    :sswitch_b
    const/4 v0, 0x1

    #@c
    .line 3231
    goto :goto_5

    #@d
    .line 3233
    :sswitch_d
    const/4 v0, 0x5

    #@e
    .line 3234
    goto :goto_5

    #@f
    .line 3236
    :sswitch_f
    const/16 v0, 0x8

    #@11
    .line 3237
    goto :goto_5

    #@12
    .line 3239
    :sswitch_12
    const/4 v0, 0x3

    #@13
    .line 3240
    goto :goto_5

    #@14
    .line 3242
    :sswitch_14
    const/16 v0, 0xa

    #@16
    .line 3243
    goto :goto_5

    #@17
    .line 3245
    :sswitch_17
    const/4 v0, 0x2

    #@18
    .line 3246
    goto :goto_5

    #@19
    .line 3222
    nop

    #@1a
    :sswitch_data_1a
    .sparse-switch
        0x0 -> :sswitch_9
        0x1 -> :sswitch_b
        0x2 -> :sswitch_17
        0x3 -> :sswitch_12
        0x5 -> :sswitch_d
        0x8 -> :sswitch_f
        0xa -> :sswitch_14
        0xff -> :sswitch_6
    .end sparse-switch
.end method

.method getFrameRate(I)I
    .registers 3
    .parameter "fps"

    #@0
    .prologue
    .line 3317
    const/4 v0, -0x1

    #@1
    .line 3318
    .local v0, retValue:I
    packed-switch p1, :pswitch_data_1e

    #@4
    .line 3345
    const/4 v0, -0x1

    #@5
    .line 3347
    :goto_5
    return v0

    #@6
    .line 3320
    :pswitch_6
    const/4 v0, 0x5

    #@7
    .line 3321
    goto :goto_5

    #@8
    .line 3323
    :pswitch_8
    const/16 v0, 0x8

    #@a
    .line 3324
    goto :goto_5

    #@b
    .line 3326
    :pswitch_b
    const/16 v0, 0xa

    #@d
    .line 3327
    goto :goto_5

    #@e
    .line 3329
    :pswitch_e
    const/16 v0, 0xd

    #@10
    .line 3330
    goto :goto_5

    #@11
    .line 3332
    :pswitch_11
    const/16 v0, 0xf

    #@13
    .line 3333
    goto :goto_5

    #@14
    .line 3335
    :pswitch_14
    const/16 v0, 0x14

    #@16
    .line 3336
    goto :goto_5

    #@17
    .line 3338
    :pswitch_17
    const/16 v0, 0x19

    #@19
    .line 3339
    goto :goto_5

    #@1a
    .line 3341
    :pswitch_1a
    const/16 v0, 0x1e

    #@1c
    .line 3342
    goto :goto_5

    #@1d
    .line 3318
    nop

    #@1e
    :pswitch_data_1e
    .packed-switch 0x0
        :pswitch_6
        :pswitch_8
        :pswitch_b
        :pswitch_e
        :pswitch_11
        :pswitch_14
        :pswitch_17
        :pswitch_1a
    .end packed-switch
.end method

.method getGeneratePreview()Z
    .registers 2

    #@0
    .prologue
    .line 3181
    iget-boolean v0, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mInvalidatePreviewArray:Z

    #@2
    return v0
.end method

.method getMediaItemFileType(I)I
    .registers 3
    .parameter "fileType"

    #@0
    .prologue
    .line 3359
    const/4 v0, -0x1

    #@1
    .line 3361
    .local v0, retValue:I
    sparse-switch p1, :sswitch_data_16

    #@4
    .line 3382
    const/4 v0, -0x1

    #@5
    .line 3384
    :goto_5
    return v0

    #@6
    .line 3363
    :sswitch_6
    const/16 v0, 0xff

    #@8
    .line 3364
    goto :goto_5

    #@9
    .line 3366
    :sswitch_9
    const/4 v0, 0x0

    #@a
    .line 3367
    goto :goto_5

    #@b
    .line 3369
    :sswitch_b
    const/4 v0, 0x1

    #@c
    .line 3370
    goto :goto_5

    #@d
    .line 3372
    :sswitch_d
    const/4 v0, 0x5

    #@e
    .line 3373
    goto :goto_5

    #@f
    .line 3375
    :sswitch_f
    const/16 v0, 0x8

    #@11
    .line 3376
    goto :goto_5

    #@12
    .line 3378
    :sswitch_12
    const/16 v0, 0xa

    #@14
    .line 3379
    goto :goto_5

    #@15
    .line 3361
    nop

    #@16
    :sswitch_data_16
    .sparse-switch
        0x0 -> :sswitch_9
        0x1 -> :sswitch_b
        0x5 -> :sswitch_d
        0x8 -> :sswitch_f
        0xa -> :sswitch_12
        0xff -> :sswitch_6
    .end sparse-switch
.end method

.method getMediaItemRenderingMode(I)I
    .registers 3
    .parameter "renderingMode"

    #@0
    .prologue
    .line 3397
    const/4 v0, -0x1

    #@1
    .line 3398
    .local v0, retValue:I
    packed-switch p1, :pswitch_data_c

    #@4
    .line 3410
    const/4 v0, -0x1

    #@5
    .line 3412
    :goto_5
    return v0

    #@6
    .line 3400
    :pswitch_6
    const/4 v0, 0x2

    #@7
    .line 3401
    goto :goto_5

    #@8
    .line 3403
    :pswitch_8
    const/4 v0, 0x0

    #@9
    .line 3404
    goto :goto_5

    #@a
    .line 3406
    :pswitch_a
    const/4 v0, 0x1

    #@b
    .line 3407
    goto :goto_5

    #@c
    .line 3398
    :pswitch_data_c
    .packed-switch 0x0
        :pswitch_6
        :pswitch_8
        :pswitch_a
    .end packed-switch
.end method

.method native getMediaProperties(Ljava/lang/String;)Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Ljava/lang/RuntimeException;,
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method getOverlaySettings(Landroid/media/videoeditor/OverlayFrame;)Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;
    .registers 19
    .parameter "overlay"

    #@0
    .prologue
    .line 1948
    new-instance v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;

    #@2
    invoke-direct {v12}, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;-><init>()V

    #@5
    .line 1949
    .local v12, effectSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;
    const/4 v1, 0x0

    #@6
    .line 1951
    .local v1, bitmap:Landroid/graphics/Bitmap;
    invoke-virtual/range {p1 .. p1}, Landroid/media/videoeditor/OverlayFrame;->getStartTime()J

    #@9
    move-result-wide v2

    #@a
    long-to-int v2, v2

    #@b
    iput v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startTime:I

    #@d
    .line 1952
    invoke-virtual/range {p1 .. p1}, Landroid/media/videoeditor/OverlayFrame;->getDuration()J

    #@10
    move-result-wide v2

    #@11
    long-to-int v2, v2

    #@12
    iput v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->duration:I

    #@14
    .line 1953
    const/16 v2, 0x106

    #@16
    iput v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->videoEffectType:I

    #@18
    .line 1954
    const/4 v2, 0x0

    #@19
    iput v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->audioEffectType:I

    #@1b
    .line 1955
    const/4 v2, 0x0

    #@1c
    iput v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->startPercent:I

    #@1e
    .line 1956
    const/4 v2, 0x0

    #@1f
    iput v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->durationPercent:I

    #@21
    .line 1957
    const/4 v2, 0x0

    #@22
    iput-object v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->framingFile:Ljava/lang/String;

    #@24
    .line 1959
    invoke-virtual/range {p1 .. p1}, Landroid/media/videoeditor/OverlayFrame;->getBitmap()Landroid/graphics/Bitmap;

    #@27
    move-result-object v1

    #@28
    if-eqz v1, :cond_fe

    #@2a
    .line 1960
    invoke-virtual/range {p1 .. p1}, Landroid/media/videoeditor/OverlayFrame;->getFilename()Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    iput-object v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->framingFile:Ljava/lang/String;

    #@30
    .line 1962
    iget-object v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->framingFile:Ljava/lang/String;

    #@32
    if-nez v2, :cond_43

    #@34
    .line 1964
    :try_start_34
    move-object/from16 v0, p0

    #@36
    iget-object v2, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProjectPath:Ljava/lang/String;

    #@38
    move-object/from16 v0, p1

    #@3a
    invoke-virtual {v0, v2}, Landroid/media/videoeditor/OverlayFrame;->save(Ljava/lang/String;)Ljava/lang/String;
    :try_end_3d
    .catch Ljava/io/IOException; {:try_start_34 .. :try_end_3d} :catch_8f

    #@3d
    .line 1968
    :goto_3d
    invoke-virtual/range {p1 .. p1}, Landroid/media/videoeditor/OverlayFrame;->getFilename()Ljava/lang/String;

    #@40
    move-result-object v2

    #@41
    iput-object v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->framingFile:Ljava/lang/String;

    #@43
    .line 1970
    :cond_43
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    #@46
    move-result-object v2

    #@47
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@49
    if-ne v2, v3, :cond_98

    #@4b
    .line 1971
    const/4 v2, 0x6

    #@4c
    iput v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->bitmapType:I

    #@4e
    .line 1979
    :cond_4e
    :goto_4e
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    #@51
    move-result v2

    #@52
    iput v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->width:I

    #@54
    .line 1980
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    #@57
    move-result v2

    #@58
    iput v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->height:I

    #@5a
    .line 1981
    iget v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->width:I

    #@5c
    new-array v2, v2, [I

    #@5e
    iput-object v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->framingBuffer:[I

    #@60
    .line 1982
    const/4 v6, 0x0

    #@61
    .line 1983
    .local v6, tmp:I
    const/4 v14, 0x0

    #@62
    .line 1984
    .local v14, maxAlpha:S
    const/16 v16, 0xff

    #@64
    .line 1985
    .local v16, minAlpha:S
    const/4 v9, 0x0

    #@65
    .line 1986
    .local v9, alpha:S
    :goto_65
    iget v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->height:I

    #@67
    if-ge v6, v2, :cond_c3

    #@69
    .line 1987
    iget-object v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->framingBuffer:[I

    #@6b
    const/4 v3, 0x0

    #@6c
    iget v4, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->width:I

    #@6e
    const/4 v5, 0x0

    #@6f
    iget v7, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->width:I

    #@71
    const/4 v8, 0x1

    #@72
    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    #@75
    .line 1990
    const/4 v13, 0x0

    #@76
    .local v13, i:I
    :goto_76
    iget v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->width:I

    #@78
    if-ge v13, v2, :cond_c0

    #@7a
    .line 1991
    iget-object v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->framingBuffer:[I

    #@7c
    aget v2, v2, v13

    #@7e
    shr-int/lit8 v2, v2, 0x18

    #@80
    and-int/lit16 v2, v2, 0xff

    #@82
    int-to-short v9, v2

    #@83
    .line 1992
    if-le v9, v14, :cond_86

    #@85
    .line 1993
    move v14, v9

    #@86
    .line 1995
    :cond_86
    move/from16 v0, v16

    #@88
    if-ge v9, v0, :cond_8c

    #@8a
    .line 1996
    move/from16 v16, v9

    #@8c
    .line 1990
    :cond_8c
    add-int/lit8 v13, v13, 0x1

    #@8e
    goto :goto_76

    #@8f
    .line 1965
    .end local v6           #tmp:I
    .end local v9           #alpha:S
    .end local v13           #i:I
    .end local v14           #maxAlpha:S
    .end local v16           #minAlpha:S
    :catch_8f
    move-exception v11

    #@90
    .line 1966
    .local v11, e:Ljava/io/IOException;
    const-string v2, "MediaArtistNativeHelper"

    #@92
    const-string v3, "getOverlaySettings : File not found"

    #@94
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@97
    goto :goto_3d

    #@98
    .line 1972
    .end local v11           #e:Ljava/io/IOException;
    :cond_98
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    #@9b
    move-result-object v2

    #@9c
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    #@9e
    if-ne v2, v3, :cond_a4

    #@a0
    .line 1973
    const/4 v2, 0x5

    #@a1
    iput v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->bitmapType:I

    #@a3
    goto :goto_4e

    #@a4
    .line 1974
    :cond_a4
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    #@a7
    move-result-object v2

    #@a8
    sget-object v3, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    #@aa
    if-ne v2, v3, :cond_b0

    #@ac
    .line 1975
    const/4 v2, 0x4

    #@ad
    iput v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->bitmapType:I

    #@af
    goto :goto_4e

    #@b0
    .line 1976
    :cond_b0
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    #@b3
    move-result-object v2

    #@b4
    sget-object v3, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    #@b6
    if-ne v2, v3, :cond_4e

    #@b8
    .line 1977
    new-instance v2, Ljava/lang/RuntimeException;

    #@ba
    const-string v3, "Bitmap config not supported"

    #@bc
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@bf
    throw v2

    #@c0
    .line 1999
    .restart local v6       #tmp:I
    .restart local v9       #alpha:S
    .restart local v13       #i:I
    .restart local v14       #maxAlpha:S
    .restart local v16       #minAlpha:S
    :cond_c0
    add-int/lit8 v6, v6, 0x1

    #@c2
    goto :goto_65

    #@c3
    .line 2001
    .end local v13           #i:I
    :cond_c3
    add-int v2, v14, v16

    #@c5
    div-int/lit8 v2, v2, 0x2

    #@c7
    int-to-short v9, v2

    #@c8
    .line 2002
    mul-int/lit8 v2, v9, 0x64

    #@ca
    div-int/lit16 v2, v2, 0x100

    #@cc
    int-to-short v9, v2

    #@cd
    .line 2003
    iput v9, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->alphaBlendingEndPercent:I

    #@cf
    .line 2004
    iput v9, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->alphaBlendingMiddlePercent:I

    #@d1
    .line 2005
    iput v9, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->alphaBlendingStartPercent:I

    #@d3
    .line 2006
    const/16 v2, 0x64

    #@d5
    iput v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->alphaBlendingFadeInTimePercent:I

    #@d7
    .line 2007
    const/16 v2, 0x64

    #@d9
    iput v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->alphaBlendingFadeOutTimePercent:I

    #@db
    .line 2008
    const/4 v2, 0x0

    #@dc
    iput-object v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->framingBuffer:[I

    #@de
    .line 2013
    invoke-virtual/range {p1 .. p1}, Landroid/media/videoeditor/OverlayFrame;->getResizedRGBSizeWidth()I

    #@e1
    move-result v2

    #@e2
    iput v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->width:I

    #@e4
    .line 2014
    iget v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->width:I

    #@e6
    if-nez v2, :cond_ee

    #@e8
    .line 2015
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    #@eb
    move-result v2

    #@ec
    iput v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->width:I

    #@ee
    .line 2018
    :cond_ee
    invoke-virtual/range {p1 .. p1}, Landroid/media/videoeditor/OverlayFrame;->getResizedRGBSizeHeight()I

    #@f1
    move-result v2

    #@f2
    iput v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->height:I

    #@f4
    .line 2019
    iget v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->height:I

    #@f6
    if-nez v2, :cond_fe

    #@f8
    .line 2020
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    #@fb
    move-result v2

    #@fc
    iput v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->height:I

    #@fe
    .line 2025
    .end local v6           #tmp:I
    .end local v9           #alpha:S
    .end local v14           #maxAlpha:S
    .end local v16           #minAlpha:S
    :cond_fe
    const/4 v2, 0x0

    #@ff
    iput v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->topLeftX:I

    #@101
    .line 2026
    const/4 v2, 0x0

    #@102
    iput v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->topLeftY:I

    #@104
    .line 2028
    const/4 v2, 0x1

    #@105
    iput-boolean v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->framingResize:Z

    #@107
    .line 2029
    const/4 v2, 0x0

    #@108
    iput-object v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->text:Ljava/lang/String;

    #@10a
    .line 2030
    const/4 v2, 0x0

    #@10b
    iput-object v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->textRenderingData:Ljava/lang/String;

    #@10d
    .line 2031
    const/4 v2, 0x0

    #@10e
    iput v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->textBufferWidth:I

    #@110
    .line 2032
    const/4 v2, 0x0

    #@111
    iput v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->textBufferHeight:I

    #@113
    .line 2033
    const/4 v2, 0x0

    #@114
    iput v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->fiftiesFrameRate:I

    #@116
    .line 2034
    const/4 v2, 0x0

    #@117
    iput v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->rgb16InputColor:I

    #@119
    .line 2037
    invoke-virtual/range {p1 .. p1}, Landroid/media/videoeditor/OverlayFrame;->getMediaItem()Landroid/media/videoeditor/MediaItem;

    #@11c
    move-result-object v2

    #@11d
    instance-of v2, v2, Landroid/media/videoeditor/MediaImageItem;

    #@11f
    if-eqz v2, :cond_163

    #@121
    .line 2038
    invoke-virtual/range {p1 .. p1}, Landroid/media/videoeditor/OverlayFrame;->getMediaItem()Landroid/media/videoeditor/MediaItem;

    #@124
    move-result-object v2

    #@125
    check-cast v2, Landroid/media/videoeditor/MediaImageItem;

    #@127
    invoke-virtual {v2}, Landroid/media/videoeditor/MediaImageItem;->getGeneratedImageClip()Ljava/lang/String;

    #@12a
    move-result-object v2

    #@12b
    if-eqz v2, :cond_150

    #@12d
    .line 2040
    invoke-virtual/range {p1 .. p1}, Landroid/media/videoeditor/OverlayFrame;->getMediaItem()Landroid/media/videoeditor/MediaItem;

    #@130
    move-result-object v2

    #@131
    check-cast v2, Landroid/media/videoeditor/MediaImageItem;

    #@133
    invoke-virtual {v2}, Landroid/media/videoeditor/MediaImageItem;->getGeneratedClipHeight()I

    #@136
    move-result v15

    #@137
    .line 2041
    .local v15, mediaItemHeight:I
    invoke-virtual/range {p1 .. p1}, Landroid/media/videoeditor/OverlayFrame;->getMediaItem()Landroid/media/videoeditor/MediaItem;

    #@13a
    move-result-object v2

    #@13b
    check-cast v2, Landroid/media/videoeditor/MediaImageItem;

    #@13d
    invoke-virtual {v2}, Landroid/media/videoeditor/MediaImageItem;->getGeneratedClipWidth()I

    #@140
    move-result v2

    #@141
    move-object/from16 v0, p0

    #@143
    invoke-virtual {v0, v2, v15}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getAspectRatio(II)I

    #@146
    move-result v10

    #@147
    .line 2053
    .local v10, aspectRatio:I
    :goto_147
    move-object/from16 v0, p0

    #@149
    invoke-direct {v0, v10, v15}, Landroid/media/videoeditor/MediaArtistNativeHelper;->findVideoResolution(II)I

    #@14c
    move-result v2

    #@14d
    iput v2, v12, Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;->framingScaledSize:I

    #@14f
    .line 2054
    return-object v12

    #@150
    .line 2046
    .end local v10           #aspectRatio:I
    .end local v15           #mediaItemHeight:I
    :cond_150
    invoke-virtual/range {p1 .. p1}, Landroid/media/videoeditor/OverlayFrame;->getMediaItem()Landroid/media/videoeditor/MediaItem;

    #@153
    move-result-object v2

    #@154
    check-cast v2, Landroid/media/videoeditor/MediaImageItem;

    #@156
    invoke-virtual {v2}, Landroid/media/videoeditor/MediaImageItem;->getScaledHeight()I

    #@159
    move-result v15

    #@15a
    .line 2047
    .restart local v15       #mediaItemHeight:I
    invoke-virtual/range {p1 .. p1}, Landroid/media/videoeditor/OverlayFrame;->getMediaItem()Landroid/media/videoeditor/MediaItem;

    #@15d
    move-result-object v2

    #@15e
    invoke-virtual {v2}, Landroid/media/videoeditor/MediaItem;->getAspectRatio()I

    #@161
    move-result v10

    #@162
    .restart local v10       #aspectRatio:I
    goto :goto_147

    #@163
    .line 2050
    .end local v10           #aspectRatio:I
    .end local v15           #mediaItemHeight:I
    :cond_163
    invoke-virtual/range {p1 .. p1}, Landroid/media/videoeditor/OverlayFrame;->getMediaItem()Landroid/media/videoeditor/MediaItem;

    #@166
    move-result-object v2

    #@167
    invoke-virtual {v2}, Landroid/media/videoeditor/MediaItem;->getAspectRatio()I

    #@16a
    move-result v10

    #@16b
    .line 2051
    .restart local v10       #aspectRatio:I
    invoke-virtual/range {p1 .. p1}, Landroid/media/videoeditor/OverlayFrame;->getMediaItem()Landroid/media/videoeditor/MediaItem;

    #@16e
    move-result-object v2

    #@16f
    invoke-virtual {v2}, Landroid/media/videoeditor/MediaItem;->getHeight()I

    #@172
    move-result v15

    #@173
    .restart local v15       #mediaItemHeight:I
    goto :goto_147
.end method

.method getPixels(Ljava/lang/String;IIJI)Landroid/graphics/Bitmap;
    .registers 20
    .parameter "filename"
    .parameter "width"
    .parameter "height"
    .parameter "timeMs"
    .parameter "videoRotation"

    #@0
    .prologue
    .line 3770
    const/4 v0, 0x1

    #@1
    new-array v12, v0, [Landroid/graphics/Bitmap;

    #@3
    .line 3771
    .local v12, result:[Landroid/graphics/Bitmap;
    const/4 v8, 0x1

    #@4
    const/4 v0, 0x1

    #@5
    new-array v9, v0, [I

    #@7
    const/4 v0, 0x0

    #@8
    const/4 v1, 0x0

    #@9
    aput v1, v9, v0

    #@b
    new-instance v10, Landroid/media/videoeditor/MediaArtistNativeHelper$1;

    #@d
    invoke-direct {v10, p0, v12}, Landroid/media/videoeditor/MediaArtistNativeHelper$1;-><init>(Landroid/media/videoeditor/MediaArtistNativeHelper;[Landroid/graphics/Bitmap;)V

    #@10
    move-object v0, p0

    #@11
    move-object v1, p1

    #@12
    move v2, p2

    #@13
    move/from16 v3, p3

    #@15
    move-wide/from16 v4, p4

    #@17
    move-wide/from16 v6, p4

    #@19
    move/from16 v11, p6

    #@1b
    invoke-virtual/range {v0 .. v11}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getPixelsList(Ljava/lang/String;IIJJI[ILandroid/media/videoeditor/MediaItem$GetThumbnailListCallback;I)V

    #@1e
    .line 3777
    const/4 v0, 0x0

    #@1f
    aget-object v0, v12, v0

    #@21
    return-object v0
.end method

.method getPixelsList(Ljava/lang/String;IIJJI[ILandroid/media/videoeditor/MediaItem$GetThumbnailListCallback;I)V
    .registers 37
    .parameter "filename"
    .parameter "width"
    .parameter "height"
    .parameter "startMs"
    .parameter "endMs"
    .parameter "thumbnailCount"
    .parameter "indices"
    .parameter "callback"
    .parameter "videoRotation"

    #@0
    .prologue
    .line 3802
    add-int/lit8 v1, p2, 0x1

    #@2
    and-int/lit8 v10, v1, -0x2

    #@4
    .line 3803
    .local v10, decWidth:I
    add-int/lit8 v1, p3, 0x1

    #@6
    and-int/lit8 v11, v1, -0x2

    #@8
    .line 3804
    .local v11, decHeight:I
    mul-int v7, v10, v11

    #@a
    .line 3809
    .local v7, thumbnailSize:I
    new-array v6, v7, [I

    #@c
    .line 3810
    .local v6, decArray:[I
    invoke-static {v7}, Ljava/nio/IntBuffer;->allocate(I)Ljava/nio/IntBuffer;

    #@f
    move-result-object v5

    #@10
    .line 3814
    .local v5, decBuffer:Ljava/nio/IntBuffer;
    move/from16 v0, p2

    #@12
    if-ne v10, v0, :cond_1a

    #@14
    move/from16 v0, p3

    #@16
    if-ne v11, v0, :cond_1a

    #@18
    if-eqz p11, :cond_5f

    #@1a
    :cond_1a
    const/4 v8, 0x1

    #@1b
    .line 3816
    .local v8, needToMassage:Z
    :goto_1b
    if-eqz v8, :cond_61

    #@1d
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@1f
    invoke-static {v10, v11, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    #@22
    move-result-object v9

    #@23
    .line 3821
    .local v9, tmpBitmap:Landroid/graphics/Bitmap;
    :goto_23
    const/16 v1, 0x5a

    #@25
    move/from16 v0, p11

    #@27
    if-eq v0, v1, :cond_2f

    #@29
    const/16 v1, 0x10e

    #@2b
    move/from16 v0, p11

    #@2d
    if-ne v0, v1, :cond_63

    #@2f
    :cond_2f
    const/16 v24, 0x1

    #@31
    .line 3822
    .local v24, needToSwapWH:Z
    :goto_31
    if-eqz v24, :cond_66

    #@33
    move/from16 v3, p3

    #@35
    .line 3823
    .local v3, outWidth:I
    :goto_35
    if-eqz v24, :cond_69

    #@37
    move/from16 v4, p2

    #@39
    .line 3825
    .local v4, outHeight:I
    :goto_39
    new-instance v1, Landroid/media/videoeditor/MediaArtistNativeHelper$2;

    #@3b
    move-object/from16 v2, p0

    #@3d
    move/from16 v12, p11

    #@3f
    move-object/from16 v13, p10

    #@41
    invoke-direct/range {v1 .. v13}, Landroid/media/videoeditor/MediaArtistNativeHelper$2;-><init>(Landroid/media/videoeditor/MediaArtistNativeHelper;IILjava/nio/IntBuffer;[IIZLandroid/graphics/Bitmap;IIILandroid/media/videoeditor/MediaItem$GetThumbnailListCallback;)V

    #@44
    move-object/from16 v12, p0

    #@46
    move-object/from16 v13, p1

    #@48
    move-object v14, v6

    #@49
    move v15, v10

    #@4a
    move/from16 v16, v11

    #@4c
    move/from16 v17, p8

    #@4e
    move-wide/from16 v18, p4

    #@50
    move-wide/from16 v20, p6

    #@52
    move-object/from16 v22, p9

    #@54
    move-object/from16 v23, v1

    #@56
    invoke-direct/range {v12 .. v23}, Landroid/media/videoeditor/MediaArtistNativeHelper;->nativeGetPixelsList(Ljava/lang/String;[IIIIJJ[ILandroid/media/videoeditor/MediaArtistNativeHelper$NativeGetPixelsListCallback;)I

    #@59
    .line 3862
    if-eqz v9, :cond_5e

    #@5b
    .line 3863
    invoke-virtual {v9}, Landroid/graphics/Bitmap;->recycle()V

    #@5e
    .line 3865
    :cond_5e
    return-void

    #@5f
    .line 3814
    .end local v3           #outWidth:I
    .end local v4           #outHeight:I
    .end local v8           #needToMassage:Z
    .end local v9           #tmpBitmap:Landroid/graphics/Bitmap;
    .end local v24           #needToSwapWH:Z
    :cond_5f
    const/4 v8, 0x0

    #@60
    goto :goto_1b

    #@61
    .line 3816
    .restart local v8       #needToMassage:Z
    :cond_61
    const/4 v9, 0x0

    #@62
    goto :goto_23

    #@63
    .line 3821
    .restart local v9       #tmpBitmap:Landroid/graphics/Bitmap;
    :cond_63
    const/16 v24, 0x0

    #@65
    goto :goto_31

    #@66
    .restart local v24       #needToSwapWH:Z
    :cond_66
    move/from16 v3, p2

    #@68
    .line 3822
    goto :goto_35

    #@69
    .restart local v3       #outWidth:I
    :cond_69
    move/from16 v4, p3

    #@6b
    .line 3823
    goto :goto_39
.end method

.method getProjectAudioTrackPCMFilePath()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1753
    iget-object v0, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrackPCMFilePath:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method getProjectPath()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1746
    iget-object v0, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProjectPath:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method getSlideSettingsDirection(I)I
    .registers 3
    .parameter "slideDirection"

    #@0
    .prologue
    .line 3458
    const/4 v0, -0x1

    #@1
    .line 3459
    .local v0, retValue:I
    packed-switch p1, :pswitch_data_e

    #@4
    .line 3474
    const/4 v0, -0x1

    #@5
    .line 3476
    :goto_5
    return v0

    #@6
    .line 3461
    :pswitch_6
    const/4 v0, 0x0

    #@7
    .line 3462
    goto :goto_5

    #@8
    .line 3464
    :pswitch_8
    const/4 v0, 0x1

    #@9
    .line 3465
    goto :goto_5

    #@a
    .line 3467
    :pswitch_a
    const/4 v0, 0x2

    #@b
    .line 3468
    goto :goto_5

    #@c
    .line 3470
    :pswitch_c
    const/4 v0, 0x3

    #@d
    .line 3471
    goto :goto_5

    #@e
    .line 3459
    :pswitch_data_e
    .packed-switch 0x0
        :pswitch_6
        :pswitch_8
        :pswitch_a
        :pswitch_c
    .end packed-switch
.end method

.method getVideoCodecType(I)I
    .registers 3
    .parameter "codecType"

    #@0
    .prologue
    .line 3263
    const/4 v0, -0x1

    #@1
    .line 3264
    .local v0, retValue:I
    packed-switch p1, :pswitch_data_c

    #@4
    .line 3277
    const/4 v0, -0x1

    #@5
    .line 3279
    :goto_5
    return v0

    #@6
    .line 3266
    :pswitch_6
    const/4 v0, 0x1

    #@7
    .line 3267
    goto :goto_5

    #@8
    .line 3269
    :pswitch_8
    const/4 v0, 0x2

    #@9
    .line 3270
    goto :goto_5

    #@a
    .line 3272
    :pswitch_a
    const/4 v0, 0x3

    #@b
    .line 3273
    goto :goto_5

    #@c
    .line 3264
    :pswitch_data_c
    .packed-switch 0x1
        :pswitch_6
        :pswitch_8
        :pswitch_a
    .end packed-switch
.end method

.method getVideoTransitionBehaviour(I)I
    .registers 3
    .parameter "transitionType"

    #@0
    .prologue
    .line 3424
    const/4 v0, -0x1

    #@1
    .line 3425
    .local v0, retValue:I
    packed-switch p1, :pswitch_data_10

    #@4
    .line 3443
    const/4 v0, -0x1

    #@5
    .line 3445
    :goto_5
    return v0

    #@6
    .line 3427
    :pswitch_6
    const/4 v0, 0x0

    #@7
    .line 3428
    goto :goto_5

    #@8
    .line 3430
    :pswitch_8
    const/4 v0, 0x2

    #@9
    .line 3431
    goto :goto_5

    #@a
    .line 3433
    :pswitch_a
    const/4 v0, 0x1

    #@b
    .line 3434
    goto :goto_5

    #@c
    .line 3436
    :pswitch_c
    const/4 v0, 0x3

    #@d
    .line 3437
    goto :goto_5

    #@e
    .line 3439
    :pswitch_e
    const/4 v0, 0x4

    #@f
    .line 3440
    goto :goto_5

    #@10
    .line 3425
    :pswitch_data_10
    .packed-switch 0x0
        :pswitch_6
        :pswitch_8
        :pswitch_a
        :pswitch_c
        :pswitch_e
    .end packed-switch
.end method

.method initClipSettings(Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;)V
    .registers 4
    .parameter "lclipSettings"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 2253
    iput-object v1, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipPath:Ljava/lang/String;

    #@4
    .line 2254
    iput-object v1, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipDecodedPath:Ljava/lang/String;

    #@6
    .line 2255
    iput-object v1, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipOriginalPath:Ljava/lang/String;

    #@8
    .line 2256
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->fileType:I

    #@a
    .line 2257
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutTime:I

    #@c
    .line 2258
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->beginCutTime:I

    #@e
    .line 2259
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->beginCutPercent:I

    #@10
    .line 2260
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->endCutPercent:I

    #@12
    .line 2261
    iput-boolean v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomEnabled:Z

    #@14
    .line 2262
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomPercentStart:I

    #@16
    .line 2263
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomTopLeftXStart:I

    #@18
    .line 2264
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomTopLeftYStart:I

    #@1a
    .line 2265
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomPercentEnd:I

    #@1c
    .line 2266
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomTopLeftXEnd:I

    #@1e
    .line 2267
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->panZoomTopLeftYEnd:I

    #@20
    .line 2268
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->mediaRendering:I

    #@22
    .line 2269
    iput v0, p1, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->rotationDegree:I

    #@24
    .line 2270
    return-void
.end method

.method invalidatePcmFile()V
    .registers 3

    #@0
    .prologue
    .line 1760
    iget-object v0, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrackPCMFilePath:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_11

    #@4
    .line 1761
    new-instance v0, Ljava/io/File;

    #@6
    iget-object v1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrackPCMFilePath:Ljava/lang/String;

    #@8
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@b
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@e
    .line 1762
    const/4 v0, 0x0

    #@f
    iput-object v0, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioTrackPCMFilePath:Ljava/lang/String;

    #@11
    .line 1764
    :cond_11
    return-void
.end method

.method nativeHelperGetAspectRatio()I
    .registers 2

    #@0
    .prologue
    .line 2059
    iget-object v0, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    #@2
    invoke-interface {v0}, Landroid/media/videoeditor/VideoEditor;->getAspectRatio()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method previewStoryBoard(Ljava/util/List;Ljava/util/List;Ljava/util/List;Landroid/media/videoeditor/VideoEditor$MediaProcessingProgressListener;)V
    .registers 31
    .parameter
    .parameter
    .parameter
    .parameter "listener"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/media/videoeditor/MediaItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/media/videoeditor/Transition;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/media/videoeditor/AudioTrack;",
            ">;",
            "Landroid/media/videoeditor/VideoEditor$MediaProcessingProgressListener;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 2833
    .local p1, mediaItemsList:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/MediaItem;>;"
    .local p2, mediaTransitionList:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Transition;>;"
    .local p3, mediaBGMList:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/AudioTrack;>;"
    move-object/from16 v0, p0

    #@2
    iget-boolean v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mInvalidatePreviewArray:Z

    #@4
    if-eqz v3, :cond_2d7

    #@6
    .line 2834
    const/16 v18, 0x0

    #@8
    .line 2835
    .local v18, previewIndex:I
    const/16 v20, 0x0

    #@a
    .line 2836
    .local v20, totalEffects:I
    const/4 v9, 0x0

    #@b
    .line 2837
    .local v9, storyBoardTime:I
    const/16 v17, 0x0

    #@d
    .line 2838
    .local v17, maxHeight:I
    const/4 v7, 0x0

    #@e
    .line 2839
    .local v7, beginCutTime:I
    const/4 v8, 0x0

    #@f
    .line 2840
    .local v8, endCutTime:I
    const/4 v6, 0x0

    #@10
    .line 2841
    .local v6, effectIndex:I
    const/16 v16, 0x0

    #@12
    .line 2842
    .local v16, lTransition:Landroid/media/videoeditor/Transition;
    const/4 v4, 0x0

    #@13
    .line 2843
    .local v4, lMediaItem:Landroid/media/videoeditor/MediaItem;
    new-instance v3, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@15
    invoke-direct {v3}, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;-><init>()V

    #@18
    move-object/from16 v0, p0

    #@1a
    iput-object v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@1c
    .line 2844
    new-instance v3, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@1e
    invoke-direct {v3}, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;-><init>()V

    #@21
    move-object/from16 v0, p0

    #@23
    iput-object v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@25
    .line 2845
    const/4 v3, 0x0

    #@26
    move-object/from16 v0, p0

    #@28
    iput v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mTotalClips:I

    #@2a
    .line 2847
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    #@2d
    move-result v3

    #@2e
    move-object/from16 v0, p0

    #@30
    iput v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mTotalClips:I

    #@32
    .line 2848
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@35
    move-result-object v15

    #@36
    .local v15, i$:Ljava/util/Iterator;
    :cond_36
    :goto_36
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    #@39
    move-result v3

    #@3a
    if-eqz v3, :cond_57

    #@3c
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3f
    move-result-object v21

    #@40
    check-cast v21, Landroid/media/videoeditor/Transition;

    #@42
    .line 2849
    .local v21, transition:Landroid/media/videoeditor/Transition;
    invoke-virtual/range {v21 .. v21}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@45
    move-result-wide v22

    #@46
    const-wide/16 v24, 0x0

    #@48
    cmp-long v3, v22, v24

    #@4a
    if-lez v3, :cond_36

    #@4c
    .line 2850
    move-object/from16 v0, p0

    #@4e
    iget v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mTotalClips:I

    #@50
    add-int/lit8 v3, v3, 0x1

    #@52
    move-object/from16 v0, p0

    #@54
    iput v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mTotalClips:I

    #@56
    goto :goto_36

    #@57
    .line 2854
    .end local v21           #transition:Landroid/media/videoeditor/Transition;
    :cond_57
    invoke-direct/range {p0 .. p1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getTotalEffects(Ljava/util/List;)I

    #@5a
    move-result v20

    #@5b
    .line 2856
    move-object/from16 v0, p0

    #@5d
    iget-object v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@5f
    move-object/from16 v0, p0

    #@61
    iget v5, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mTotalClips:I

    #@63
    new-array v5, v5, [Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@65
    iput-object v5, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@67
    .line 2857
    move-object/from16 v0, p0

    #@69
    iget-object v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@6b
    move/from16 v0, v20

    #@6d
    new-array v5, v0, [Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;

    #@6f
    iput-object v5, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->effectSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;

    #@71
    .line 2858
    move-object/from16 v0, p0

    #@73
    iget-object v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@75
    move-object/from16 v0, p0

    #@77
    iget v5, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mTotalClips:I

    #@79
    new-array v5, v5, [Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@7b
    iput-object v5, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@7d
    .line 2861
    move-object/from16 v0, p4

    #@7f
    move-object/from16 v1, p0

    #@81
    iput-object v0, v1, Landroid/media/videoeditor/MediaArtistNativeHelper;->mMediaProcessingProgressListener:Landroid/media/videoeditor/VideoEditor$MediaProcessingProgressListener;

    #@83
    .line 2862
    const/4 v3, 0x0

    #@84
    move-object/from16 v0, p0

    #@86
    iput v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProgressToApp:I

    #@88
    .line 2864
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    #@8b
    move-result v3

    #@8c
    if-lez v3, :cond_1ea

    #@8e
    .line 2865
    const/4 v14, 0x0

    #@8f
    .local v14, i:I
    :goto_8f
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    #@92
    move-result v3

    #@93
    if-ge v14, v3, :cond_1a8

    #@95
    .line 2867
    move-object/from16 v0, p1

    #@97
    invoke-interface {v0, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@9a
    move-result-object v4

    #@9b
    .end local v4           #lMediaItem:Landroid/media/videoeditor/MediaItem;
    check-cast v4, Landroid/media/videoeditor/MediaItem;

    #@9d
    .line 2868
    .restart local v4       #lMediaItem:Landroid/media/videoeditor/MediaItem;
    instance-of v3, v4, Landroid/media/videoeditor/MediaVideoItem;

    #@9f
    if-eqz v3, :cond_1fe

    #@a1
    move-object v3, v4

    #@a2
    .line 2869
    check-cast v3, Landroid/media/videoeditor/MediaVideoItem;

    #@a4
    invoke-virtual {v3}, Landroid/media/videoeditor/MediaVideoItem;->getBoundaryBeginTime()J

    #@a7
    move-result-wide v22

    #@a8
    move-wide/from16 v0, v22

    #@aa
    long-to-int v7, v0

    #@ab
    move-object v3, v4

    #@ac
    .line 2870
    check-cast v3, Landroid/media/videoeditor/MediaVideoItem;

    #@ae
    invoke-virtual {v3}, Landroid/media/videoeditor/MediaVideoItem;->getBoundaryEndTime()J

    #@b1
    move-result-wide v22

    #@b2
    move-wide/from16 v0, v22

    #@b4
    long-to-int v8, v0

    #@b5
    .line 2876
    :cond_b5
    :goto_b5
    invoke-virtual {v4}, Landroid/media/videoeditor/MediaItem;->getBeginTransition()Landroid/media/videoeditor/Transition;

    #@b8
    move-result-object v16

    #@b9
    .line 2877
    if-eqz v16, :cond_e3

    #@bb
    invoke-virtual/range {v16 .. v16}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@be
    move-result-wide v22

    #@bf
    const-wide/16 v24, 0x0

    #@c1
    cmp-long v3, v22, v24

    #@c3
    if-lez v3, :cond_e3

    #@c5
    .line 2879
    move-object/from16 v0, p0

    #@c7
    iget-object v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@c9
    move-object/from16 v0, p0

    #@cb
    iget-object v5, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@cd
    move-object/from16 v0, p0

    #@cf
    move-object/from16 v1, v16

    #@d1
    move/from16 v2, v18

    #@d3
    invoke-direct {v0, v1, v3, v5, v2}, Landroid/media/videoeditor/MediaArtistNativeHelper;->generateTransition(Landroid/media/videoeditor/Transition;Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;I)V

    #@d6
    .line 2881
    move-object/from16 v0, p0

    #@d8
    iget-object v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@da
    iget-object v3, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@dc
    aget-object v3, v3, v18

    #@de
    iget v3, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->duration:I

    #@e0
    add-int/2addr v9, v3

    #@e1
    .line 2882
    add-int/lit8 v18, v18, 0x1

    #@e3
    .line 2885
    :cond_e3
    move-object/from16 v0, p0

    #@e5
    move/from16 v1, v18

    #@e7
    move/from16 v2, v17

    #@e9
    invoke-direct {v0, v4, v1, v2}, Landroid/media/videoeditor/MediaArtistNativeHelper;->populateMediaItemProperties(Landroid/media/videoeditor/MediaItem;II)I

    #@ec
    move-result v17

    #@ed
    .line 2887
    instance-of v3, v4, Landroid/media/videoeditor/MediaImageItem;

    #@ef
    if-eqz v3, :cond_29b

    #@f1
    .line 2888
    const/16 v19, 0x0

    #@f3
    .line 2889
    .local v19, tmpCnt:I
    const/4 v10, 0x0

    #@f4
    .line 2890
    .local v10, bEffectKbPresent:Z
    invoke-virtual {v4}, Landroid/media/videoeditor/MediaItem;->getAllEffects()Ljava/util/List;

    #@f7
    move-result-object v12

    #@f8
    .line 2894
    .local v12, effectList:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Effect;>;"
    :goto_f8
    invoke-interface {v12}, Ljava/util/List;->size()I

    #@fb
    move-result v3

    #@fc
    move/from16 v0, v19

    #@fe
    if-ge v0, v3, :cond_10b

    #@100
    .line 2895
    move/from16 v0, v19

    #@102
    invoke-interface {v12, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@105
    move-result-object v3

    #@106
    instance-of v3, v3, Landroid/media/videoeditor/EffectKenBurns;

    #@108
    if-eqz v3, :cond_20f

    #@10a
    .line 2896
    const/4 v10, 0x1

    #@10b
    .line 2902
    :cond_10b
    if-eqz v10, :cond_258

    #@10d
    .line 2904
    :try_start_10d
    move-object v0, v4

    #@10e
    check-cast v0, Landroid/media/videoeditor/MediaImageItem;

    #@110
    move-object v3, v0

    #@111
    invoke-virtual {v3}, Landroid/media/videoeditor/MediaImageItem;->getGeneratedImageClip()Ljava/lang/String;

    #@114
    move-result-object v3

    #@115
    if-eqz v3, :cond_213

    #@117
    .line 2905
    move-object/from16 v0, p0

    #@119
    iget-object v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@11b
    iget-object v5, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@11d
    move-object v0, v4

    #@11e
    check-cast v0, Landroid/media/videoeditor/MediaImageItem;

    #@120
    move-object v3, v0

    #@121
    invoke-virtual {v3}, Landroid/media/videoeditor/MediaImageItem;->getGeneratedImageClip()Ljava/lang/String;

    #@124
    move-result-object v3

    #@125
    move-object/from16 v0, p0

    #@127
    invoke-virtual {v0, v3}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getMediaProperties(Ljava/lang/String;)Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@12a
    move-result-object v3

    #@12b
    aput-object v3, v5, v18
    :try_end_12d
    .catch Ljava/lang/Exception; {:try_start_10d .. :try_end_12d} :catch_24f

    #@12d
    .line 2942
    .end local v10           #bEffectKbPresent:Z
    .end local v12           #effectList:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Effect;>;"
    .end local v19           #tmpCnt:I
    :goto_12d
    move-object/from16 v0, p0

    #@12f
    iget-object v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@131
    iget-object v3, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@133
    aget-object v3, v3, v18

    #@135
    invoke-virtual {v4}, Landroid/media/videoeditor/MediaItem;->getId()Ljava/lang/String;

    #@138
    move-result-object v5

    #@139
    iput-object v5, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->Id:Ljava/lang/String;

    #@13b
    .line 2943
    move-object/from16 v0, p0

    #@13d
    iget-object v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@13f
    move-object/from16 v0, p0

    #@141
    move/from16 v1, v18

    #@143
    invoke-direct {v0, v4, v3, v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->checkOddSizeImage(Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;I)V

    #@146
    .line 2944
    move-object/from16 v0, p0

    #@148
    iget-object v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@14a
    move-object/from16 v0, p0

    #@14c
    move/from16 v1, v18

    #@14e
    invoke-direct {v0, v4, v3, v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->adjustVolume(Landroid/media/videoeditor/MediaItem;Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;I)V

    #@151
    .line 2951
    move-object/from16 v0, p0

    #@153
    iget-object v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@155
    iget-object v3, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@157
    aget-object v3, v3, v18

    #@159
    move-object/from16 v0, p0

    #@15b
    iget-object v5, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@15d
    iget-object v5, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@15f
    aget-object v5, v5, v18

    #@161
    move-object/from16 v0, p0

    #@163
    invoke-direct {v0, v3, v5, v4}, Landroid/media/videoeditor/MediaArtistNativeHelper;->adjustMediaItemBoundary(Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;Landroid/media/videoeditor/MediaItem;)V

    #@166
    .line 2959
    move-object/from16 v0, p0

    #@168
    iget-object v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@16a
    iget-object v5, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->effectSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;

    #@16c
    move-object/from16 v3, p0

    #@16e
    invoke-direct/range {v3 .. v9}, Landroid/media/videoeditor/MediaArtistNativeHelper;->populateEffects(Landroid/media/videoeditor/MediaItem;[Landroid/media/videoeditor/MediaArtistNativeHelper$EffectSettings;IIII)I

    #@171
    move-result v6

    #@172
    .line 2962
    move-object/from16 v0, p0

    #@174
    iget-object v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@176
    iget-object v3, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@178
    aget-object v3, v3, v18

    #@17a
    iget v3, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->duration:I

    #@17c
    add-int/2addr v9, v3

    #@17d
    .line 2963
    add-int/lit8 v18, v18, 0x1

    #@17f
    .line 2967
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    #@182
    move-result v3

    #@183
    add-int/lit8 v3, v3, -0x1

    #@185
    if-ne v14, v3, :cond_2b8

    #@187
    .line 2968
    invoke-virtual {v4}, Landroid/media/videoeditor/MediaItem;->getEndTransition()Landroid/media/videoeditor/Transition;

    #@18a
    move-result-object v16

    #@18b
    .line 2969
    if-eqz v16, :cond_2b8

    #@18d
    invoke-virtual/range {v16 .. v16}, Landroid/media/videoeditor/Transition;->getDuration()J

    #@190
    move-result-wide v22

    #@191
    const-wide/16 v24, 0x0

    #@193
    cmp-long v3, v22, v24

    #@195
    if-lez v3, :cond_2b8

    #@197
    .line 2970
    move-object/from16 v0, p0

    #@199
    iget-object v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@19b
    move-object/from16 v0, p0

    #@19d
    iget-object v5, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@19f
    move-object/from16 v0, p0

    #@1a1
    move-object/from16 v1, v16

    #@1a3
    move/from16 v2, v18

    #@1a5
    invoke-direct {v0, v1, v3, v5, v2}, Landroid/media/videoeditor/MediaArtistNativeHelper;->generateTransition(Landroid/media/videoeditor/Transition;Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;I)V

    #@1a8
    .line 2977
    :cond_1a8
    move-object/from16 v0, p0

    #@1aa
    iget-boolean v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mErrorFlagSet:Z

    #@1ac
    if-nez v3, :cond_1ea

    #@1ae
    .line 2978
    move-object/from16 v0, p0

    #@1b0
    iget-object v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@1b2
    move-object/from16 v0, p0

    #@1b4
    iget-object v5, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mVideoEditor:Landroid/media/videoeditor/VideoEditor;

    #@1b6
    invoke-interface {v5}, Landroid/media/videoeditor/VideoEditor;->getAspectRatio()I

    #@1b9
    move-result v5

    #@1ba
    move-object/from16 v0, p0

    #@1bc
    move/from16 v1, v17

    #@1be
    invoke-direct {v0, v5, v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->findVideoResolution(II)I

    #@1c1
    move-result v5

    #@1c2
    iput v5, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->videoFrameSize:I

    #@1c4
    .line 2980
    move-object/from16 v0, p0

    #@1c6
    move-object/from16 v1, p3

    #@1c8
    invoke-direct {v0, v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->populateBackgroundMusicProperties(Ljava/util/List;)V

    #@1cb
    .line 2984
    :try_start_1cb
    move-object/from16 v0, p0

    #@1cd
    iget-object v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@1cf
    move-object/from16 v0, p0

    #@1d1
    iget-object v5, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@1d3
    move-object/from16 v0, p0

    #@1d5
    iget-object v0, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;

    #@1d7
    move-object/from16 v22, v0

    #@1d9
    move-object/from16 v0, p0

    #@1db
    move-object/from16 v1, v22

    #@1dd
    invoke-direct {v0, v3, v5, v1}, Landroid/media/videoeditor/MediaArtistNativeHelper;->nativePopulateSettings(Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;)V
    :try_end_1e0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1cb .. :try_end_1e0} :catch_2bc
    .catch Ljava/lang/IllegalStateException; {:try_start_1cb .. :try_end_1e0} :catch_2c5
    .catch Ljava/lang/RuntimeException; {:try_start_1cb .. :try_end_1e0} :catch_2ce

    #@1e0
    .line 2995
    const/4 v3, 0x0

    #@1e1
    move-object/from16 v0, p0

    #@1e3
    iput-boolean v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mInvalidatePreviewArray:Z

    #@1e5
    .line 2996
    const/4 v3, 0x0

    #@1e6
    move-object/from16 v0, p0

    #@1e8
    iput v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProcessingState:I

    #@1ea
    .line 2999
    .end local v14           #i:I
    :cond_1ea
    move-object/from16 v0, p0

    #@1ec
    iget-boolean v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mErrorFlagSet:Z

    #@1ee
    if-eqz v3, :cond_2d7

    #@1f0
    .line 3000
    const/4 v3, 0x0

    #@1f1
    move-object/from16 v0, p0

    #@1f3
    iput-boolean v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mErrorFlagSet:Z

    #@1f5
    .line 3001
    new-instance v3, Ljava/lang/RuntimeException;

    #@1f7
    const-string/jumbo v5, "preview generation cannot be completed"

    #@1fa
    invoke-direct {v3, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1fd
    throw v3

    #@1fe
    .line 2871
    .restart local v14       #i:I
    :cond_1fe
    instance-of v3, v4, Landroid/media/videoeditor/MediaImageItem;

    #@200
    if-eqz v3, :cond_b5

    #@202
    .line 2872
    const/4 v7, 0x0

    #@203
    move-object v3, v4

    #@204
    .line 2873
    check-cast v3, Landroid/media/videoeditor/MediaImageItem;

    #@206
    invoke-virtual {v3}, Landroid/media/videoeditor/MediaImageItem;->getTimelineDuration()J

    #@209
    move-result-wide v22

    #@20a
    move-wide/from16 v0, v22

    #@20c
    long-to-int v8, v0

    #@20d
    goto/16 :goto_b5

    #@20f
    .line 2899
    .restart local v10       #bEffectKbPresent:Z
    .restart local v12       #effectList:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Effect;>;"
    .restart local v19       #tmpCnt:I
    :cond_20f
    add-int/lit8 v19, v19, 0x1

    #@211
    goto/16 :goto_f8

    #@213
    .line 2910
    :cond_213
    :try_start_213
    move-object/from16 v0, p0

    #@215
    iget-object v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@217
    iget-object v5, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@219
    move-object v0, v4

    #@21a
    check-cast v0, Landroid/media/videoeditor/MediaImageItem;

    #@21c
    move-object v3, v0

    #@21d
    invoke-virtual {v3}, Landroid/media/videoeditor/MediaImageItem;->getScaledImageFileName()Ljava/lang/String;

    #@220
    move-result-object v3

    #@221
    move-object/from16 v0, p0

    #@223
    invoke-virtual {v0, v3}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getMediaProperties(Ljava/lang/String;)Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@226
    move-result-object v3

    #@227
    aput-object v3, v5, v18

    #@229
    .line 2913
    move-object/from16 v0, p0

    #@22b
    iget-object v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@22d
    iget-object v3, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@22f
    aget-object v5, v3, v18

    #@231
    move-object v0, v4

    #@232
    check-cast v0, Landroid/media/videoeditor/MediaImageItem;

    #@234
    move-object v3, v0

    #@235
    invoke-virtual {v3}, Landroid/media/videoeditor/MediaImageItem;->getScaledWidth()I

    #@238
    move-result v3

    #@239
    iput v3, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->width:I

    #@23b
    .line 2915
    move-object/from16 v0, p0

    #@23d
    iget-object v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@23f
    iget-object v3, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@241
    aget-object v5, v3, v18

    #@243
    move-object v0, v4

    #@244
    check-cast v0, Landroid/media/videoeditor/MediaImageItem;

    #@246
    move-object v3, v0

    #@247
    invoke-virtual {v3}, Landroid/media/videoeditor/MediaImageItem;->getScaledHeight()I

    #@24a
    move-result v3

    #@24b
    iput v3, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->height:I
    :try_end_24d
    .catch Ljava/lang/Exception; {:try_start_213 .. :try_end_24d} :catch_24f

    #@24d
    goto/16 :goto_12d

    #@24f
    .line 2918
    :catch_24f
    move-exception v11

    #@250
    .line 2919
    .local v11, e:Ljava/lang/Exception;
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@252
    const-string v5, "Unsupported file or file not found"

    #@254
    invoke-direct {v3, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@257
    throw v3

    #@258
    .line 2923
    .end local v11           #e:Ljava/lang/Exception;
    :cond_258
    :try_start_258
    move-object/from16 v0, p0

    #@25a
    iget-object v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@25c
    iget-object v5, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@25e
    move-object v0, v4

    #@25f
    check-cast v0, Landroid/media/videoeditor/MediaImageItem;

    #@261
    move-object v3, v0

    #@262
    invoke-virtual {v3}, Landroid/media/videoeditor/MediaImageItem;->getScaledImageFileName()Ljava/lang/String;

    #@265
    move-result-object v3

    #@266
    move-object/from16 v0, p0

    #@268
    invoke-virtual {v0, v3}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getMediaProperties(Ljava/lang/String;)Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@26b
    move-result-object v3

    #@26c
    aput-object v3, v5, v18
    :try_end_26e
    .catch Ljava/lang/Exception; {:try_start_258 .. :try_end_26e} :catch_292

    #@26e
    .line 2929
    move-object/from16 v0, p0

    #@270
    iget-object v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@272
    iget-object v3, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@274
    aget-object v5, v3, v18

    #@276
    move-object v3, v4

    #@277
    check-cast v3, Landroid/media/videoeditor/MediaImageItem;

    #@279
    invoke-virtual {v3}, Landroid/media/videoeditor/MediaImageItem;->getScaledWidth()I

    #@27c
    move-result v3

    #@27d
    iput v3, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->width:I

    #@27f
    .line 2931
    move-object/from16 v0, p0

    #@281
    iget-object v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@283
    iget-object v3, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@285
    aget-object v5, v3, v18

    #@287
    move-object v3, v4

    #@288
    check-cast v3, Landroid/media/videoeditor/MediaImageItem;

    #@28a
    invoke-virtual {v3}, Landroid/media/videoeditor/MediaImageItem;->getScaledHeight()I

    #@28d
    move-result v3

    #@28e
    iput v3, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;->height:I

    #@290
    goto/16 :goto_12d

    #@292
    .line 2926
    :catch_292
    move-exception v11

    #@293
    .line 2927
    .restart local v11       #e:Ljava/lang/Exception;
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@295
    const-string v5, "Unsupported file or file not found"

    #@297
    invoke-direct {v3, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@29a
    throw v3

    #@29b
    .line 2936
    .end local v10           #bEffectKbPresent:Z
    .end local v11           #e:Ljava/lang/Exception;
    .end local v12           #effectList:Ljava/util/List;,"Ljava/util/List<Landroid/media/videoeditor/Effect;>;"
    .end local v19           #tmpCnt:I
    :cond_29b
    :try_start_29b
    move-object/from16 v0, p0

    #@29d
    iget-object v3, v0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@29f
    iget-object v3, v3, Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;->clipProperties:[Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@2a1
    invoke-virtual {v4}, Landroid/media/videoeditor/MediaItem;->getFilename()Ljava/lang/String;

    #@2a4
    move-result-object v5

    #@2a5
    move-object/from16 v0, p0

    #@2a7
    invoke-virtual {v0, v5}, Landroid/media/videoeditor/MediaArtistNativeHelper;->getMediaProperties(Ljava/lang/String;)Landroid/media/videoeditor/MediaArtistNativeHelper$Properties;

    #@2aa
    move-result-object v5

    #@2ab
    aput-object v5, v3, v18
    :try_end_2ad
    .catch Ljava/lang/Exception; {:try_start_29b .. :try_end_2ad} :catch_2af

    #@2ad
    goto/16 :goto_12d

    #@2af
    .line 2938
    :catch_2af
    move-exception v11

    #@2b0
    .line 2939
    .restart local v11       #e:Ljava/lang/Exception;
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@2b2
    const-string v5, "Unsupported file or file not found"

    #@2b4
    invoke-direct {v3, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2b7
    throw v3

    #@2b8
    .line 2865
    .end local v11           #e:Ljava/lang/Exception;
    :cond_2b8
    add-int/lit8 v14, v14, 0x1

    #@2ba
    goto/16 :goto_8f

    #@2bc
    .line 2985
    :catch_2bc
    move-exception v13

    #@2bd
    .line 2986
    .local v13, ex:Ljava/lang/IllegalArgumentException;
    const-string v3, "MediaArtistNativeHelper"

    #@2bf
    const-string v5, "Illegal argument exception in nativePopulateSettings"

    #@2c1
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2c4
    .line 2987
    throw v13

    #@2c5
    .line 2988
    .end local v13           #ex:Ljava/lang/IllegalArgumentException;
    :catch_2c5
    move-exception v13

    #@2c6
    .line 2989
    .local v13, ex:Ljava/lang/IllegalStateException;
    const-string v3, "MediaArtistNativeHelper"

    #@2c8
    const-string v5, "Illegal state exception in nativePopulateSettings"

    #@2ca
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2cd
    .line 2990
    throw v13

    #@2ce
    .line 2991
    .end local v13           #ex:Ljava/lang/IllegalStateException;
    :catch_2ce
    move-exception v13

    #@2cf
    .line 2992
    .local v13, ex:Ljava/lang/RuntimeException;
    const-string v3, "MediaArtistNativeHelper"

    #@2d1
    const-string v5, "Runtime exception in nativePopulateSettings"

    #@2d3
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2d6
    .line 2993
    throw v13

    #@2d7
    .line 3004
    .end local v4           #lMediaItem:Landroid/media/videoeditor/MediaItem;
    .end local v6           #effectIndex:I
    .end local v7           #beginCutTime:I
    .end local v8           #endCutTime:I
    .end local v9           #storyBoardTime:I
    .end local v13           #ex:Ljava/lang/RuntimeException;
    .end local v14           #i:I
    .end local v15           #i$:Ljava/util/Iterator;
    .end local v16           #lTransition:Landroid/media/videoeditor/Transition;
    .end local v17           #maxHeight:I
    .end local v18           #previewIndex:I
    .end local v20           #totalEffects:I
    :cond_2d7
    return-void
.end method

.method releaseNativeHelper()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    #@0
    .prologue
    .line 1885
    invoke-direct {p0}, Landroid/media/videoeditor/MediaArtistNativeHelper;->release()V

    #@3
    .line 1886
    return-void
.end method

.method renderMediaItemPreviewFrame(Landroid/view/Surface;Ljava/lang/String;JII)J
    .registers 19
    .parameter "surface"
    .parameter "filepath"
    .parameter "time"
    .parameter "framewidth"
    .parameter "frameheight"

    #@0
    .prologue
    .line 3139
    const-wide/16 v10, 0x0

    #@2
    .line 3141
    .local v10, timeMs:J
    const/4 v5, 0x0

    #@3
    const/4 v6, 0x0

    #@4
    move-object v0, p0

    #@5
    move-object v1, p1

    #@6
    move-object v2, p2

    #@7
    move/from16 v3, p5

    #@9
    move/from16 v4, p6

    #@b
    move-wide v7, p3

    #@c
    :try_start_c
    invoke-direct/range {v0 .. v8}, Landroid/media/videoeditor/MediaArtistNativeHelper;->nativeRenderMediaItemPreviewFrame(Landroid/view/Surface;Ljava/lang/String;IIIIJ)I
    :try_end_f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_c .. :try_end_f} :catch_12
    .catch Ljava/lang/IllegalStateException; {:try_start_c .. :try_end_f} :catch_1b
    .catch Ljava/lang/RuntimeException; {:try_start_c .. :try_end_f} :catch_24

    #@f
    move-result v0

    #@10
    int-to-long v10, v0

    #@11
    .line 3154
    return-wide v10

    #@12
    .line 3143
    :catch_12
    move-exception v9

    #@13
    .line 3144
    .local v9, ex:Ljava/lang/IllegalArgumentException;
    const-string v0, "MediaArtistNativeHelper"

    #@15
    const-string v1, "Illegal Argument exception in renderMediaItemPreviewFrame"

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 3145
    throw v9

    #@1b
    .line 3146
    .end local v9           #ex:Ljava/lang/IllegalArgumentException;
    :catch_1b
    move-exception v9

    #@1c
    .line 3147
    .local v9, ex:Ljava/lang/IllegalStateException;
    const-string v0, "MediaArtistNativeHelper"

    #@1e
    const-string v1, "Illegal state exception in renderMediaItemPreviewFrame"

    #@20
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 3148
    throw v9

    #@24
    .line 3149
    .end local v9           #ex:Ljava/lang/IllegalStateException;
    :catch_24
    move-exception v9

    #@25
    .line 3150
    .local v9, ex:Ljava/lang/RuntimeException;
    const-string v0, "MediaArtistNativeHelper"

    #@27
    const-string v1, "Runtime exception in renderMediaItemPreviewFrame"

    #@29
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 3151
    throw v9
.end method

.method renderPreviewFrame(Landroid/view/Surface;JIILandroid/media/videoeditor/VideoEditor$OverlayData;)J
    .registers 14
    .parameter "surface"
    .parameter "time"
    .parameter "surfaceWidth"
    .parameter "surfaceHeight"
    .parameter "overlayData"

    #@0
    .prologue
    .line 3073
    iget-boolean v4, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mInvalidatePreviewArray:Z

    #@2
    if-eqz v4, :cond_1c

    #@4
    .line 3074
    const-string v4, "MediaArtistNativeHelper"

    #@6
    const/4 v5, 0x3

    #@7
    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@a
    move-result v4

    #@b
    if-eqz v4, :cond_14

    #@d
    .line 3075
    const-string v4, "MediaArtistNativeHelper"

    #@f
    const-string v5, "Call generate preview first"

    #@11
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 3077
    :cond_14
    new-instance v4, Ljava/lang/IllegalStateException;

    #@16
    const-string v5, "Call generate preview first"

    #@18
    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v4

    #@1c
    .line 3080
    :cond_1c
    const-wide/16 v2, 0x0

    #@1e
    .line 3082
    .local v2, timeMs:J
    const/4 v0, 0x0

    #@1f
    .local v0, clipCnt:I
    :goto_1f
    :try_start_1f
    iget-object v4, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@21
    iget-object v4, v4, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@23
    array-length v4, v4

    #@24
    if-ge v0, v4, :cond_44

    #@26
    .line 3084
    iget-object v4, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@28
    iget-object v4, v4, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@2a
    aget-object v4, v4, v0

    #@2c
    iget v4, v4, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->fileType:I

    #@2e
    const/4 v5, 0x5

    #@2f
    if-ne v4, v5, :cond_41

    #@31
    .line 3085
    iget-object v4, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@33
    iget-object v4, v4, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@35
    aget-object v4, v4, v0

    #@37
    iget-object v5, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@39
    iget-object v5, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;->clipSettingsArray:[Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;

    #@3b
    aget-object v5, v5, v0

    #@3d
    iget-object v5, v5, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipDecodedPath:Ljava/lang/String;

    #@3f
    iput-object v5, v4, Landroid/media/videoeditor/MediaArtistNativeHelper$ClipSettings;->clipPath:Ljava/lang/String;

    #@41
    .line 3083
    :cond_41
    add-int/lit8 v0, v0, 0x1

    #@43
    goto :goto_1f

    #@44
    .line 3091
    :cond_44
    const/4 v4, 0x0

    #@45
    iput-object v4, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mRenderPreviewOverlayFile:Ljava/lang/String;

    #@47
    .line 3092
    const/4 v4, 0x0

    #@48
    iput v4, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mRenderPreviewRenderingMode:I

    #@4a
    .line 3094
    iget-object v4, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mPreviewEditSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;

    #@4c
    iget-object v5, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mClipProperties:Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;

    #@4e
    iget-object v6, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mAudioSettings:Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;

    #@50
    invoke-direct {p0, v4, v5, v6}, Landroid/media/videoeditor/MediaArtistNativeHelper;->nativePopulateSettings(Landroid/media/videoeditor/MediaArtistNativeHelper$EditSettings;Landroid/media/videoeditor/MediaArtistNativeHelper$PreviewClipProperties;Landroid/media/videoeditor/MediaArtistNativeHelper$AudioSettings;)V

    #@53
    .line 3096
    invoke-direct/range {p0 .. p5}, Landroid/media/videoeditor/MediaArtistNativeHelper;->nativeRenderPreviewFrame(Landroid/view/Surface;JII)I

    #@56
    move-result v4

    #@57
    int-to-long v2, v4

    #@58
    .line 3098
    iget-object v4, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mRenderPreviewOverlayFile:Ljava/lang/String;

    #@5a
    if-eqz v4, :cond_68

    #@5c
    .line 3099
    iget-object v4, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mRenderPreviewOverlayFile:Ljava/lang/String;

    #@5e
    invoke-static {v4}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    #@61
    move-result-object v4

    #@62
    iget v5, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mRenderPreviewRenderingMode:I

    #@64
    invoke-virtual {p6, v4, v5}, Landroid/media/videoeditor/VideoEditor$OverlayData;->set(Landroid/graphics/Bitmap;I)V

    #@67
    .line 3115
    :goto_67
    return-wide v2

    #@68
    .line 3102
    :cond_68
    invoke-virtual {p6}, Landroid/media/videoeditor/VideoEditor$OverlayData;->setClear()V
    :try_end_6b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1f .. :try_end_6b} :catch_6c
    .catch Ljava/lang/IllegalStateException; {:try_start_1f .. :try_end_6b} :catch_75
    .catch Ljava/lang/RuntimeException; {:try_start_1f .. :try_end_6b} :catch_7e

    #@6b
    goto :goto_67

    #@6c
    .line 3104
    :catch_6c
    move-exception v1

    #@6d
    .line 3105
    .local v1, ex:Ljava/lang/IllegalArgumentException;
    const-string v4, "MediaArtistNativeHelper"

    #@6f
    const-string v5, "Illegal Argument exception in nativeRenderPreviewFrame"

    #@71
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@74
    .line 3106
    throw v1

    #@75
    .line 3107
    .end local v1           #ex:Ljava/lang/IllegalArgumentException;
    :catch_75
    move-exception v1

    #@76
    .line 3108
    .local v1, ex:Ljava/lang/IllegalStateException;
    const-string v4, "MediaArtistNativeHelper"

    #@78
    const-string v5, "Illegal state exception in nativeRenderPreviewFrame"

    #@7a
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7d
    .line 3109
    throw v1

    #@7e
    .line 3110
    .end local v1           #ex:Ljava/lang/IllegalStateException;
    :catch_7e
    move-exception v1

    #@7f
    .line 3111
    .local v1, ex:Ljava/lang/RuntimeException;
    const-string v4, "MediaArtistNativeHelper"

    #@81
    const-string v5, "Runtime exception in nativeRenderPreviewFrame"

    #@83
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@86
    .line 3112
    throw v1
.end method

.method setAudioCodec(I)V
    .registers 2
    .parameter "codec"

    #@0
    .prologue
    .line 2069
    iput p1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mExportAudioCodec:I

    #@2
    .line 2070
    return-void
.end method

.method setAudioflag(Z)V
    .registers 5
    .parameter "flag"

    #@0
    .prologue
    .line 2089
    new-instance v0, Ljava/io/File;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mProjectPath:Ljava/lang/String;

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    const-string v2, "/"

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, "AudioPcm.pcm"

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    const/4 v2, 0x0

    #@1e
    new-array v2, v2, [Ljava/lang/Object;

    #@20
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@27
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@2a
    move-result v0

    #@2b
    if-nez v0, :cond_2e

    #@2d
    .line 2090
    const/4 p1, 0x1

    #@2e
    .line 2092
    :cond_2e
    iput-boolean p1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mRegenerateAudio:Z

    #@30
    .line 2093
    return-void
.end method

.method setGeneratePreview(Z)V
    .registers 6
    .parameter "isRequired"

    #@0
    .prologue
    .line 3162
    const/4 v1, 0x0

    #@1
    .line 3164
    .local v1, semAcquiredDone:Z
    :try_start_1
    invoke-direct {p0}, Landroid/media/videoeditor/MediaArtistNativeHelper;->lock()V

    #@4
    .line 3165
    const/4 v1, 0x1

    #@5
    .line 3166
    iput-boolean p1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mInvalidatePreviewArray:Z
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_1b
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_7} :catch_d

    #@7
    .line 3170
    if-eqz v1, :cond_c

    #@9
    .line 3171
    invoke-direct {p0}, Landroid/media/videoeditor/MediaArtistNativeHelper;->unlock()V

    #@c
    .line 3174
    :cond_c
    :goto_c
    return-void

    #@d
    .line 3167
    :catch_d
    move-exception v0

    #@e
    .line 3168
    .local v0, ex:Ljava/lang/InterruptedException;
    :try_start_e
    const-string v2, "MediaArtistNativeHelper"

    #@10
    const-string v3, "Runtime exception in renderMediaItemPreviewFrame"

    #@12
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_15
    .catchall {:try_start_e .. :try_end_15} :catchall_1b

    #@15
    .line 3170
    if-eqz v1, :cond_c

    #@17
    .line 3171
    invoke-direct {p0}, Landroid/media/videoeditor/MediaArtistNativeHelper;->unlock()V

    #@1a
    goto :goto_c

    #@1b
    .line 3170
    .end local v0           #ex:Ljava/lang/InterruptedException;
    :catchall_1b
    move-exception v2

    #@1c
    if-eqz v1, :cond_21

    #@1e
    .line 3171
    invoke-direct {p0}, Landroid/media/videoeditor/MediaArtistNativeHelper;->unlock()V

    #@21
    :cond_21
    throw v2
.end method

.method setVideoCodec(I)V
    .registers 2
    .parameter "codec"

    #@0
    .prologue
    .line 2078
    iput p1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mExportVideoCodec:I

    #@2
    .line 2079
    return-void
.end method

.method stop(Ljava/lang/String;)V
    .registers 5
    .parameter "filename"

    #@0
    .prologue
    .line 3753
    :try_start_0
    invoke-direct {p0}, Landroid/media/videoeditor/MediaArtistNativeHelper;->stopEncoding()V

    #@3
    .line 3754
    new-instance v1, Ljava/io/File;

    #@5
    iget-object v2, p0, Landroid/media/videoeditor/MediaArtistNativeHelper;->mExportFilename:Ljava/lang/String;

    #@7
    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@a
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_d
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_d} :catch_e
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_d} :catch_17

    #@d
    .line 3762
    return-void

    #@e
    .line 3755
    :catch_e
    move-exception v0

    #@f
    .line 3756
    .local v0, ex:Ljava/lang/IllegalStateException;
    const-string v1, "MediaArtistNativeHelper"

    #@11
    const-string v2, "Illegal state exception in unload settings"

    #@13
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 3757
    throw v0

    #@17
    .line 3758
    .end local v0           #ex:Ljava/lang/IllegalStateException;
    :catch_17
    move-exception v0

    #@18
    .line 3759
    .local v0, ex:Ljava/lang/RuntimeException;
    const-string v1, "MediaArtistNativeHelper"

    #@1a
    const-string v2, "Runtime exception in unload settings"

    #@1c
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 3760
    throw v0
.end method

.method stopPreview()J
    .registers 3

    #@0
    .prologue
    .line 3055
    invoke-direct {p0}, Landroid/media/videoeditor/MediaArtistNativeHelper;->nativeStopPreview()I

    #@3
    move-result v0

    #@4
    int-to-long v0, v0

    #@5
    return-wide v0
.end method
