.class public final Landroid/media/videoeditor/MediaArtistNativeHelper$Version;
.super Ljava/lang/Object;
.source "MediaArtistNativeHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/videoeditor/MediaArtistNativeHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "Version"
.end annotation


# static fields
.field private static final VIDEOEDITOR_MAJOR_VERSION:I = 0x0

.field private static final VIDEOEDITOR_MINOR_VERSION:I = 0x0

.field private static final VIDEOEDITOR_REVISION_VERSION:I = 0x1


# instance fields
.field public major:I

.field public minor:I

.field public revision:I

.field final synthetic this$0:Landroid/media/videoeditor/MediaArtistNativeHelper;


# direct methods
.method public constructor <init>(Landroid/media/videoeditor/MediaArtistNativeHelper;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 141
    iput-object p1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$Version;->this$0:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public getVersion()Landroid/media/videoeditor/MediaArtistNativeHelper$Version;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 163
    new-instance v0, Landroid/media/videoeditor/MediaArtistNativeHelper$Version;

    #@3
    iget-object v1, p0, Landroid/media/videoeditor/MediaArtistNativeHelper$Version;->this$0:Landroid/media/videoeditor/MediaArtistNativeHelper;

    #@5
    invoke-direct {v0, v1}, Landroid/media/videoeditor/MediaArtistNativeHelper$Version;-><init>(Landroid/media/videoeditor/MediaArtistNativeHelper;)V

    #@8
    .line 165
    .local v0, version:Landroid/media/videoeditor/MediaArtistNativeHelper$Version;
    iput v2, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$Version;->major:I

    #@a
    .line 166
    iput v2, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$Version;->minor:I

    #@c
    .line 167
    const/4 v1, 0x1

    #@d
    iput v1, v0, Landroid/media/videoeditor/MediaArtistNativeHelper$Version;->revision:I

    #@f
    .line 169
    return-object v0
.end method
