.class public Landroid/media/FaceDetector;
.super Ljava/lang/Object;
.source "FaceDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/FaceDetector$1;,
        Landroid/media/FaceDetector$Face;
    }
.end annotation


# static fields
.field private static sInitialized:Z


# instance fields
.field private mBWBuffer:[B

.field private mDCR:I

.field private mFD:I

.field private mHeight:I

.field private mMaxFaces:I

.field private mSDK:I

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 179
    const/4 v1, 0x0

    #@1
    sput-boolean v1, Landroid/media/FaceDetector;->sInitialized:Z

    #@3
    .line 181
    :try_start_3
    const-string v1, "FFTEm"

    #@5
    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@8
    .line 182
    invoke-static {}, Landroid/media/FaceDetector;->nativeClassInit()V

    #@b
    .line 183
    const/4 v1, 0x1

    #@c
    sput-boolean v1, Landroid/media/FaceDetector;->sInitialized:Z
    :try_end_e
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_3 .. :try_end_e} :catch_f

    #@e
    .line 187
    .local v0, e:Ljava/lang/UnsatisfiedLinkError;
    :goto_e
    return-void

    #@f
    .line 184
    .end local v0           #e:Ljava/lang/UnsatisfiedLinkError;
    :catch_f
    move-exception v0

    #@10
    .line 185
    .restart local v0       #e:Ljava/lang/UnsatisfiedLinkError;
    const-string v1, "FFTEm"

    #@12
    const-string v2, "face detection library not found!"

    #@14
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    goto :goto_e
.end method

.method public constructor <init>(III)V
    .registers 5
    .parameter "width"
    .parameter "height"
    .parameter "maxFaces"

    #@0
    .prologue
    .line 113
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 114
    sget-boolean v0, Landroid/media/FaceDetector;->sInitialized:Z

    #@5
    if-nez v0, :cond_8

    #@7
    .line 122
    :goto_7
    return-void

    #@8
    .line 117
    :cond_8
    invoke-direct {p0, p1, p2, p3}, Landroid/media/FaceDetector;->fft_initialize(III)I

    #@b
    .line 118
    iput p1, p0, Landroid/media/FaceDetector;->mWidth:I

    #@d
    .line 119
    iput p2, p0, Landroid/media/FaceDetector;->mHeight:I

    #@f
    .line 120
    iput p3, p0, Landroid/media/FaceDetector;->mMaxFaces:I

    #@11
    .line 121
    mul-int v0, p1, p2

    #@13
    new-array v0, v0, [B

    #@15
    iput-object v0, p0, Landroid/media/FaceDetector;->mBWBuffer:[B

    #@17
    goto :goto_7
.end method

.method private native fft_destroy()V
.end method

.method private native fft_detect(Landroid/graphics/Bitmap;)I
.end method

.method private native fft_get_face(Landroid/media/FaceDetector$Face;I)V
.end method

.method private native fft_initialize(III)I
.end method

.method private static native nativeClassInit()V
.end method


# virtual methods
.method protected finalize()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 168
    invoke-direct {p0}, Landroid/media/FaceDetector;->fft_destroy()V

    #@3
    .line 169
    return-void
.end method

.method public findFaces(Landroid/graphics/Bitmap;[Landroid/media/FaceDetector$Face;)I
    .registers 7
    .parameter "bitmap"
    .parameter "faces"

    #@0
    .prologue
    .line 141
    sget-boolean v2, Landroid/media/FaceDetector;->sInitialized:Z

    #@2
    if-nez v2, :cond_6

    #@4
    .line 142
    const/4 v1, 0x0

    #@5
    .line 161
    :cond_5
    return v1

    #@6
    .line 144
    :cond_6
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    #@9
    move-result v2

    #@a
    iget v3, p0, Landroid/media/FaceDetector;->mWidth:I

    #@c
    if-ne v2, v3, :cond_16

    #@e
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    #@11
    move-result v2

    #@12
    iget v3, p0, Landroid/media/FaceDetector;->mHeight:I

    #@14
    if-eq v2, v3, :cond_1e

    #@16
    .line 145
    :cond_16
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@18
    const-string v3, "bitmap size doesn\'t match initialization"

    #@1a
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v2

    #@1e
    .line 148
    :cond_1e
    array-length v2, p2

    #@1f
    iget v3, p0, Landroid/media/FaceDetector;->mMaxFaces:I

    #@21
    if-ge v2, v3, :cond_2b

    #@23
    .line 149
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@25
    const-string v3, "faces[] smaller than maxFaces"

    #@27
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2a
    throw v2

    #@2b
    .line 153
    :cond_2b
    invoke-direct {p0, p1}, Landroid/media/FaceDetector;->fft_detect(Landroid/graphics/Bitmap;)I

    #@2e
    move-result v1

    #@2f
    .line 154
    .local v1, numFaces:I
    iget v2, p0, Landroid/media/FaceDetector;->mMaxFaces:I

    #@31
    if-lt v1, v2, :cond_35

    #@33
    .line 155
    iget v1, p0, Landroid/media/FaceDetector;->mMaxFaces:I

    #@35
    .line 156
    :cond_35
    const/4 v0, 0x0

    #@36
    .local v0, i:I
    :goto_36
    if-ge v0, v1, :cond_5

    #@38
    .line 157
    aget-object v2, p2, v0

    #@3a
    if-nez v2, :cond_44

    #@3c
    .line 158
    new-instance v2, Landroid/media/FaceDetector$Face;

    #@3e
    const/4 v3, 0x0

    #@3f
    invoke-direct {v2, p0, v3}, Landroid/media/FaceDetector$Face;-><init>(Landroid/media/FaceDetector;Landroid/media/FaceDetector$1;)V

    #@42
    aput-object v2, p2, v0

    #@44
    .line 159
    :cond_44
    aget-object v2, p2, v0

    #@46
    invoke-direct {p0, v2, v0}, Landroid/media/FaceDetector;->fft_get_face(Landroid/media/FaceDetector$Face;I)V

    #@49
    .line 156
    add-int/lit8 v0, v0, 0x1

    #@4b
    goto :goto_36
.end method
