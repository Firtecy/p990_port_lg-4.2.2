.class Landroid/media/AudioService$3;
.super Ljava/lang/Object;
.source "AudioService.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/AudioService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/media/AudioService;


# direct methods
.method constructor <init>(Landroid/media/AudioService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 2766
    iput-object p1, p0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .registers 21
    .parameter "profile"
    .parameter "proxy"

    #@0
    .prologue
    .line 2770
    packed-switch p1, :pswitch_data_13c

    #@3
    .line 2836
    .end local p2
    :cond_3
    :goto_3
    return-void

    #@4
    .restart local p2
    :pswitch_4
    move-object/from16 v15, p2

    #@6
    .line 2772
    check-cast v15, Landroid/bluetooth/BluetoothA2dp;

    #@8
    .line 2773
    .local v15, a2dp:Landroid/bluetooth/BluetoothA2dp;
    invoke-virtual {v15}, Landroid/bluetooth/BluetoothA2dp;->getConnectedDevices()Ljava/util/List;

    #@b
    move-result-object v16

    #@c
    .line 2774
    .local v16, deviceList:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    #@f
    move-result v1

    #@10
    if-lez v1, :cond_3

    #@12
    .line 2775
    const/4 v1, 0x0

    #@13
    move-object/from16 v0, v16

    #@15
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@18
    move-result-object v6

    #@19
    check-cast v6, Landroid/bluetooth/BluetoothDevice;

    #@1b
    .line 2776
    .local v6, btDevice:Landroid/bluetooth/BluetoothDevice;
    move-object/from16 v0, p0

    #@1d
    iget-object v1, v0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@1f
    invoke-static {v1}, Landroid/media/AudioService;->access$400(Landroid/media/AudioService;)Ljava/util/HashMap;

    #@22
    move-result-object v8

    #@23
    monitor-enter v8

    #@24
    .line 2777
    :try_start_24
    invoke-virtual {v15, v6}, Landroid/bluetooth/BluetoothA2dp;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@27
    move-result v4

    #@28
    .line 2778
    .local v4, state:I
    move-object/from16 v0, p0

    #@2a
    iget-object v2, v0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@2c
    const/16 v3, 0x80

    #@2e
    const/4 v1, 0x2

    #@2f
    if-ne v4, v1, :cond_4d

    #@31
    const/4 v1, 0x1

    #@32
    :goto_32
    invoke-static {v2, v3, v1}, Landroid/media/AudioService;->access$3000(Landroid/media/AudioService;II)I

    #@35
    move-result v7

    #@36
    .line 2781
    .local v7, delay:I
    move-object/from16 v0, p0

    #@38
    iget-object v1, v0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@3a
    move-object/from16 v0, p0

    #@3c
    iget-object v2, v0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@3e
    invoke-static {v2}, Landroid/media/AudioService;->access$100(Landroid/media/AudioService;)Landroid/media/AudioService$AudioHandler;

    #@41
    move-result-object v2

    #@42
    const/16 v3, 0x16

    #@44
    const/4 v5, 0x0

    #@45
    invoke-static/range {v1 .. v7}, Landroid/media/AudioService;->access$3100(Landroid/media/AudioService;Landroid/os/Handler;IIILjava/lang/Object;I)V

    #@48
    .line 2787
    monitor-exit v8

    #@49
    goto :goto_3

    #@4a
    .end local v4           #state:I
    .end local v7           #delay:I
    :catchall_4a
    move-exception v1

    #@4b
    monitor-exit v8
    :try_end_4c
    .catchall {:try_start_24 .. :try_end_4c} :catchall_4a

    #@4c
    throw v1

    #@4d
    .line 2778
    .restart local v4       #state:I
    :cond_4d
    const/4 v1, 0x0

    #@4e
    goto :goto_32

    #@4f
    .line 2792
    .end local v4           #state:I
    .end local v6           #btDevice:Landroid/bluetooth/BluetoothDevice;
    .end local v15           #a2dp:Landroid/bluetooth/BluetoothA2dp;
    .end local v16           #deviceList:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    :pswitch_4f
    move-object/from16 v0, p0

    #@51
    iget-object v1, v0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@53
    invoke-static {v1}, Landroid/media/AudioService;->access$2300(Landroid/media/AudioService;)Ljava/util/ArrayList;

    #@56
    move-result-object v2

    #@57
    monitor-enter v2

    #@58
    .line 2794
    :try_start_58
    move-object/from16 v0, p0

    #@5a
    iget-object v1, v0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@5c
    invoke-static {v1}, Landroid/media/AudioService;->access$100(Landroid/media/AudioService;)Landroid/media/AudioService$AudioHandler;

    #@5f
    move-result-object v1

    #@60
    const/16 v3, 0xb

    #@62
    invoke-virtual {v1, v3}, Landroid/media/AudioService$AudioHandler;->removeMessages(I)V

    #@65
    .line 2795
    move-object/from16 v0, p0

    #@67
    iget-object v1, v0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@69
    check-cast p2, Landroid/bluetooth/BluetoothHeadset;

    #@6b
    .end local p2
    move-object/from16 v0, p2

    #@6d
    invoke-static {v1, v0}, Landroid/media/AudioService;->access$2702(Landroid/media/AudioService;Landroid/bluetooth/BluetoothHeadset;)Landroid/bluetooth/BluetoothHeadset;

    #@70
    .line 2796
    move-object/from16 v0, p0

    #@72
    iget-object v1, v0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@74
    invoke-static {v1}, Landroid/media/AudioService;->access$2700(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothHeadset;

    #@77
    move-result-object v1

    #@78
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothHeadset;->getConnectedDevices()Ljava/util/List;

    #@7b
    move-result-object v16

    #@7c
    .line 2797
    .restart local v16       #deviceList:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    #@7f
    move-result v1

    #@80
    if-lez v1, :cond_eb

    #@82
    .line 2798
    move-object/from16 v0, p0

    #@84
    iget-object v3, v0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@86
    const/4 v1, 0x0

    #@87
    move-object/from16 v0, v16

    #@89
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@8c
    move-result-object v1

    #@8d
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@8f
    invoke-static {v3, v1}, Landroid/media/AudioService;->access$2802(Landroid/media/AudioService;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;

    #@92
    .line 2803
    :goto_92
    move-object/from16 v0, p0

    #@94
    iget-object v1, v0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@96
    invoke-static {v1}, Landroid/media/AudioService;->access$2400(Landroid/media/AudioService;)V

    #@99
    .line 2805
    move-object/from16 v0, p0

    #@9b
    iget-object v1, v0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@9d
    invoke-static {v1}, Landroid/media/AudioService;->access$2600(Landroid/media/AudioService;)I

    #@a0
    move-result v1

    #@a1
    const/4 v3, 0x1

    #@a2
    if-eq v1, v3, :cond_ba

    #@a4
    move-object/from16 v0, p0

    #@a6
    iget-object v1, v0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@a8
    invoke-static {v1}, Landroid/media/AudioService;->access$2600(Landroid/media/AudioService;)I

    #@ab
    move-result v1

    #@ac
    const/4 v3, 0x5

    #@ad
    if-eq v1, v3, :cond_ba

    #@af
    move-object/from16 v0, p0

    #@b1
    iget-object v1, v0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@b3
    invoke-static {v1}, Landroid/media/AudioService;->access$2600(Landroid/media/AudioService;)I

    #@b6
    move-result v1

    #@b7
    const/4 v3, 0x4

    #@b8
    if-ne v1, v3, :cond_e5

    #@ba
    .line 2808
    :cond_ba
    const/16 v17, 0x0

    #@bc
    .line 2809
    .local v17, status:Z
    move-object/from16 v0, p0

    #@be
    iget-object v1, v0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@c0
    invoke-static {v1}, Landroid/media/AudioService;->access$2800(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothDevice;

    #@c3
    move-result-object v1

    #@c4
    if-eqz v1, :cond_d1

    #@c6
    .line 2810
    move-object/from16 v0, p0

    #@c8
    iget-object v1, v0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@ca
    invoke-static {v1}, Landroid/media/AudioService;->access$2600(Landroid/media/AudioService;)I

    #@cd
    move-result v1

    #@ce
    packed-switch v1, :pswitch_data_144

    #@d1
    .line 2825
    :cond_d1
    :goto_d1
    :pswitch_d1
    if-nez v17, :cond_e5

    #@d3
    .line 2826
    move-object/from16 v0, p0

    #@d5
    iget-object v1, v0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@d7
    invoke-static {v1}, Landroid/media/AudioService;->access$100(Landroid/media/AudioService;)Landroid/media/AudioService$AudioHandler;

    #@da
    move-result-object v8

    #@db
    const/16 v9, 0xb

    #@dd
    const/4 v10, 0x0

    #@de
    const/4 v11, 0x0

    #@df
    const/4 v12, 0x0

    #@e0
    const/4 v13, 0x0

    #@e1
    const/4 v14, 0x0

    #@e2
    invoke-static/range {v8 .. v14}, Landroid/media/AudioService;->access$200(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@e5
    .line 2830
    .end local v17           #status:Z
    :cond_e5
    monitor-exit v2

    #@e6
    goto/16 :goto_3

    #@e8
    .end local v16           #deviceList:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    :catchall_e8
    move-exception v1

    #@e9
    monitor-exit v2
    :try_end_ea
    .catchall {:try_start_58 .. :try_end_ea} :catchall_e8

    #@ea
    throw v1

    #@eb
    .line 2800
    .restart local v16       #deviceList:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    :cond_eb
    :try_start_eb
    move-object/from16 v0, p0

    #@ed
    iget-object v1, v0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@ef
    const/4 v3, 0x0

    #@f0
    invoke-static {v1, v3}, Landroid/media/AudioService;->access$2802(Landroid/media/AudioService;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;

    #@f3
    goto :goto_92

    #@f4
    .line 2812
    .restart local v17       #status:Z
    :pswitch_f4
    move-object/from16 v0, p0

    #@f6
    iget-object v1, v0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@f8
    const/4 v3, 0x3

    #@f9
    invoke-static {v1, v3}, Landroid/media/AudioService;->access$2602(Landroid/media/AudioService;I)I

    #@fc
    .line 2813
    move-object/from16 v0, p0

    #@fe
    iget-object v1, v0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@100
    invoke-static {v1}, Landroid/media/AudioService;->access$2700(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothHeadset;

    #@103
    move-result-object v1

    #@104
    move-object/from16 v0, p0

    #@106
    iget-object v3, v0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@108
    invoke-static {v3}, Landroid/media/AudioService;->access$2800(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothDevice;

    #@10b
    move-result-object v3

    #@10c
    invoke-virtual {v1, v3}, Landroid/bluetooth/BluetoothHeadset;->startScoUsingVirtualVoiceCall(Landroid/bluetooth/BluetoothDevice;)Z

    #@10f
    move-result v17

    #@110
    .line 2815
    goto :goto_d1

    #@111
    .line 2817
    :pswitch_111
    move-object/from16 v0, p0

    #@113
    iget-object v1, v0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@115
    invoke-static {v1}, Landroid/media/AudioService;->access$2700(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothHeadset;

    #@118
    move-result-object v1

    #@119
    move-object/from16 v0, p0

    #@11b
    iget-object v3, v0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@11d
    invoke-static {v3}, Landroid/media/AudioService;->access$2800(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothDevice;

    #@120
    move-result-object v3

    #@121
    invoke-virtual {v1, v3}, Landroid/bluetooth/BluetoothHeadset;->stopScoUsingVirtualVoiceCall(Landroid/bluetooth/BluetoothDevice;)Z

    #@124
    move-result v17

    #@125
    .line 2819
    goto :goto_d1

    #@126
    .line 2821
    :pswitch_126
    move-object/from16 v0, p0

    #@128
    iget-object v1, v0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@12a
    invoke-static {v1}, Landroid/media/AudioService;->access$2700(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothHeadset;

    #@12d
    move-result-object v1

    #@12e
    move-object/from16 v0, p0

    #@130
    iget-object v3, v0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@132
    invoke-static {v3}, Landroid/media/AudioService;->access$2800(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothDevice;

    #@135
    move-result-object v3

    #@136
    invoke-virtual {v1, v3}, Landroid/bluetooth/BluetoothHeadset;->stopVoiceRecognition(Landroid/bluetooth/BluetoothDevice;)Z
    :try_end_139
    .catchall {:try_start_eb .. :try_end_139} :catchall_e8

    #@139
    move-result v17

    #@13a
    goto :goto_d1

    #@13b
    .line 2770
    nop

    #@13c
    :pswitch_data_13c
    .packed-switch 0x1
        :pswitch_4f
        :pswitch_4
    .end packed-switch

    #@144
    .line 2810
    :pswitch_data_144
    .packed-switch 0x1
        :pswitch_f4
        :pswitch_d1
        :pswitch_d1
        :pswitch_126
        :pswitch_111
    .end packed-switch
.end method

.method public onServiceDisconnected(I)V
    .registers 6
    .parameter "profile"

    #@0
    .prologue
    .line 2838
    packed-switch p1, :pswitch_data_4c

    #@3
    .line 2857
    :goto_3
    return-void

    #@4
    .line 2840
    :pswitch_4
    iget-object v0, p0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@6
    invoke-static {v0}, Landroid/media/AudioService;->access$400(Landroid/media/AudioService;)Ljava/util/HashMap;

    #@9
    move-result-object v1

    #@a
    monitor-enter v1

    #@b
    .line 2841
    :try_start_b
    iget-object v0, p0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@d
    invoke-static {v0}, Landroid/media/AudioService;->access$400(Landroid/media/AudioService;)Ljava/util/HashMap;

    #@10
    move-result-object v0

    #@11
    const/16 v2, 0x80

    #@13
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@1a
    move-result v0

    #@1b
    if-eqz v0, :cond_34

    #@1d
    .line 2842
    iget-object v2, p0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@1f
    iget-object v0, p0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@21
    invoke-static {v0}, Landroid/media/AudioService;->access$400(Landroid/media/AudioService;)Ljava/util/HashMap;

    #@24
    move-result-object v0

    #@25
    const/16 v3, 0x80

    #@27
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2e
    move-result-object v0

    #@2f
    check-cast v0, Ljava/lang/String;

    #@31
    invoke-static {v2, v0}, Landroid/media/AudioService;->access$3200(Landroid/media/AudioService;Ljava/lang/String;)V

    #@34
    .line 2845
    :cond_34
    monitor-exit v1

    #@35
    goto :goto_3

    #@36
    :catchall_36
    move-exception v0

    #@37
    monitor-exit v1
    :try_end_38
    .catchall {:try_start_b .. :try_end_38} :catchall_36

    #@38
    throw v0

    #@39
    .line 2849
    :pswitch_39
    iget-object v0, p0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@3b
    invoke-static {v0}, Landroid/media/AudioService;->access$2300(Landroid/media/AudioService;)Ljava/util/ArrayList;

    #@3e
    move-result-object v1

    #@3f
    monitor-enter v1

    #@40
    .line 2850
    :try_start_40
    iget-object v0, p0, Landroid/media/AudioService$3;->this$0:Landroid/media/AudioService;

    #@42
    const/4 v2, 0x0

    #@43
    invoke-static {v0, v2}, Landroid/media/AudioService;->access$2702(Landroid/media/AudioService;Landroid/bluetooth/BluetoothHeadset;)Landroid/bluetooth/BluetoothHeadset;

    #@46
    .line 2851
    monitor-exit v1

    #@47
    goto :goto_3

    #@48
    :catchall_48
    move-exception v0

    #@49
    monitor-exit v1
    :try_end_4a
    .catchall {:try_start_40 .. :try_end_4a} :catchall_48

    #@4a
    throw v0

    #@4b
    .line 2838
    nop

    #@4c
    :pswitch_data_4c
    .packed-switch 0x1
        :pswitch_39
        :pswitch_4
    .end packed-switch
.end method
