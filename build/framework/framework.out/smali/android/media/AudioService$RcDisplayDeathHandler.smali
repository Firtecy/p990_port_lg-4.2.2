.class Landroid/media/AudioService$RcDisplayDeathHandler;
.super Ljava/lang/Object;
.source "AudioService.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/AudioService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RcDisplayDeathHandler"
.end annotation


# instance fields
.field private mCb:Landroid/os/IBinder;

.field final synthetic this$0:Landroid/media/AudioService;


# direct methods
.method public constructor <init>(Landroid/media/AudioService;Landroid/os/IBinder;)V
    .registers 3
    .parameter
    .parameter "b"

    #@0
    .prologue
    .line 6255
    iput-object p1, p0, Landroid/media/AudioService$RcDisplayDeathHandler;->this$0:Landroid/media/AudioService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 6257
    iput-object p2, p0, Landroid/media/AudioService$RcDisplayDeathHandler;->mCb:Landroid/os/IBinder;

    #@7
    .line 6258
    return-void
.end method


# virtual methods
.method public binderDied()V
    .registers 4

    #@0
    .prologue
    .line 6261
    iget-object v0, p0, Landroid/media/AudioService$RcDisplayDeathHandler;->this$0:Landroid/media/AudioService;

    #@2
    invoke-static {v0}, Landroid/media/AudioService;->access$10100(Landroid/media/AudioService;)Ljava/util/Stack;

    #@5
    move-result-object v1

    #@6
    monitor-enter v1

    #@7
    .line 6262
    :try_start_7
    const-string v0, "AudioService"

    #@9
    const-string v2, "RemoteControl: display died"

    #@b
    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 6263
    iget-object v0, p0, Landroid/media/AudioService$RcDisplayDeathHandler;->this$0:Landroid/media/AudioService;

    #@10
    const/4 v2, 0x0

    #@11
    invoke-static {v0, v2}, Landroid/media/AudioService;->access$10202(Landroid/media/AudioService;Landroid/media/IRemoteControlDisplay;)Landroid/media/IRemoteControlDisplay;

    #@14
    .line 6264
    monitor-exit v1

    #@15
    .line 6265
    return-void

    #@16
    .line 6264
    :catchall_16
    move-exception v0

    #@17
    monitor-exit v1
    :try_end_18
    .catchall {:try_start_7 .. :try_end_18} :catchall_16

    #@18
    throw v0
.end method

.method public unlinkToRcDisplayDeath()V
    .registers 5

    #@0
    .prologue
    .line 6270
    :try_start_0
    iget-object v1, p0, Landroid/media/AudioService$RcDisplayDeathHandler;->mCb:Landroid/os/IBinder;

    #@2
    const/4 v2, 0x0

    #@3
    invoke-interface {v1, p0, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_6
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_6} :catch_7

    #@6
    .line 6276
    :goto_6
    return-void

    #@7
    .line 6271
    :catch_7
    move-exception v0

    #@8
    .line 6273
    .local v0, e:Ljava/util/NoSuchElementException;
    const-string v1, "AudioService"

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "Encountered "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    const-string v3, " in unlinkToRcDisplayDeath()"

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 6274
    invoke-virtual {v0}, Ljava/util/NoSuchElementException;->printStackTrace()V

    #@29
    goto :goto_6
.end method
