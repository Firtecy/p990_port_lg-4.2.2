.class public final Landroid/media/MediaCodec$CryptoException;
.super Ljava/lang/RuntimeException;
.source "MediaCodec.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/MediaCodec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CryptoException"
.end annotation


# instance fields
.field private mErrorCode:I


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .registers 3
    .parameter "errorCode"
    .parameter "detailMessage"

    #@0
    .prologue
    .line 286
    invoke-direct {p0, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@3
    .line 287
    iput p1, p0, Landroid/media/MediaCodec$CryptoException;->mErrorCode:I

    #@5
    .line 288
    return-void
.end method


# virtual methods
.method public getErrorCode()I
    .registers 2

    #@0
    .prologue
    .line 291
    iget v0, p0, Landroid/media/MediaCodec$CryptoException;->mErrorCode:I

    #@2
    return v0
.end method
