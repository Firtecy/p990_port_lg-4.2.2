.class public abstract Landroid/media/IAudioRoutesObserver$Stub;
.super Landroid/os/Binder;
.source "IAudioRoutesObserver.java"

# interfaces
.implements Landroid/media/IAudioRoutesObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/IAudioRoutesObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/IAudioRoutesObserver$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.media.IAudioRoutesObserver"

.field static final TRANSACTION_dispatchAudioRoutesChanged:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 19
    const-string v0, "android.media.IAudioRoutesObserver"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/media/IAudioRoutesObserver$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/media/IAudioRoutesObserver;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 27
    if-nez p0, :cond_4

    #@2
    .line 28
    const/4 v0, 0x0

    #@3
    .line 34
    :goto_3
    return-object v0

    #@4
    .line 30
    :cond_4
    const-string v1, "android.media.IAudioRoutesObserver"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 31
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/media/IAudioRoutesObserver;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 32
    check-cast v0, Landroid/media/IAudioRoutesObserver;

    #@12
    goto :goto_3

    #@13
    .line 34
    :cond_13
    new-instance v0, Landroid/media/IAudioRoutesObserver$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/media/IAudioRoutesObserver$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 8
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 42
    sparse-switch p1, :sswitch_data_28

    #@4
    .line 63
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v1

    #@8
    :goto_8
    return v1

    #@9
    .line 46
    :sswitch_9
    const-string v2, "android.media.IAudioRoutesObserver"

    #@b
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 51
    :sswitch_f
    const-string v2, "android.media.IAudioRoutesObserver"

    #@11
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 53
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v2

    #@18
    if-eqz v2, :cond_26

    #@1a
    .line 54
    sget-object v2, Landroid/media/AudioRoutesInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1c
    invoke-interface {v2, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Landroid/media/AudioRoutesInfo;

    #@22
    .line 59
    .local v0, _arg0:Landroid/media/AudioRoutesInfo;
    :goto_22
    invoke-virtual {p0, v0}, Landroid/media/IAudioRoutesObserver$Stub;->dispatchAudioRoutesChanged(Landroid/media/AudioRoutesInfo;)V

    #@25
    goto :goto_8

    #@26
    .line 57
    .end local v0           #_arg0:Landroid/media/AudioRoutesInfo;
    :cond_26
    const/4 v0, 0x0

    #@27
    .restart local v0       #_arg0:Landroid/media/AudioRoutesInfo;
    goto :goto_22

    #@28
    .line 42
    :sswitch_data_28
    .sparse-switch
        0x1 -> :sswitch_f
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
