.class Landroid/media/SmartRingtone$2;
.super Landroid/os/Handler;
.source "SmartRingtone.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/media/SmartRingtone;->onNoiseEstimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/media/SmartRingtone;


# direct methods
.method constructor <init>(Landroid/media/SmartRingtone;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 131
    iput-object p1, p0, Landroid/media/SmartRingtone$2;->this$0:Landroid/media/SmartRingtone;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public declared-synchronized handleMessage(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    .line 135
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v1, p0, Landroid/media/SmartRingtone$2;->this$0:Landroid/media/SmartRingtone;

    #@3
    invoke-static {v1}, Landroid/media/SmartRingtone;->access$100(Landroid/media/SmartRingtone;)Landroid/media/MediaPlayer;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_9a

    #@d
    iget-object v1, p0, Landroid/media/SmartRingtone$2;->this$0:Landroid/media/SmartRingtone;

    #@f
    invoke-static {v1}, Landroid/media/SmartRingtone;->access$200(Landroid/media/SmartRingtone;)I

    #@12
    move-result v1

    #@13
    if-lez v1, :cond_9a

    #@15
    .line 136
    iget-object v1, p0, Landroid/media/SmartRingtone$2;->this$0:Landroid/media/SmartRingtone;

    #@17
    iget-object v2, p0, Landroid/media/SmartRingtone$2;->this$0:Landroid/media/SmartRingtone;

    #@19
    invoke-static {v2}, Landroid/media/SmartRingtone;->access$500(Landroid/media/SmartRingtone;)Landroid/media/AudioManager;

    #@1c
    move-result-object v2

    #@1d
    iget-object v3, p0, Landroid/media/SmartRingtone$2;->this$0:Landroid/media/SmartRingtone;

    #@1f
    invoke-static {v3}, Landroid/media/SmartRingtone;->access$400(Landroid/media/SmartRingtone;)I

    #@22
    move-result v3

    #@23
    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    #@26
    move-result v2

    #@27
    invoke-static {v1, v2}, Landroid/media/SmartRingtone;->access$302(Landroid/media/SmartRingtone;I)I

    #@2a
    .line 139
    iget-object v1, p0, Landroid/media/SmartRingtone$2;->this$0:Landroid/media/SmartRingtone;

    #@2c
    invoke-static {v1}, Landroid/media/SmartRingtone;->access$300(Landroid/media/SmartRingtone;)I

    #@2f
    move-result v1

    #@30
    const/4 v2, 0x1

    #@31
    if-ne v1, v2, :cond_40

    #@33
    .line 140
    iget-object v1, p0, Landroid/media/SmartRingtone$2;->this$0:Landroid/media/SmartRingtone;

    #@35
    iget-object v2, p0, Landroid/media/SmartRingtone$2;->this$0:Landroid/media/SmartRingtone;

    #@37
    invoke-static {v2}, Landroid/media/SmartRingtone;->access$200(Landroid/media/SmartRingtone;)I

    #@3a
    move-result v2

    #@3b
    add-int/lit8 v2, v2, 0x1

    #@3d
    invoke-static {v1, v2}, Landroid/media/SmartRingtone;->access$202(Landroid/media/SmartRingtone;I)I

    #@40
    .line 143
    :cond_40
    iget-object v1, p0, Landroid/media/SmartRingtone$2;->this$0:Landroid/media/SmartRingtone;

    #@42
    invoke-static {v1}, Landroid/media/SmartRingtone;->access$600(Landroid/media/SmartRingtone;)V

    #@45
    .line 145
    iget-object v1, p0, Landroid/media/SmartRingtone$2;->this$0:Landroid/media/SmartRingtone;

    #@47
    invoke-static {v1}, Landroid/media/SmartRingtone;->access$300(Landroid/media/SmartRingtone;)I

    #@4a
    move-result v1

    #@4b
    if-lez v1, :cond_9a

    #@4d
    .line 146
    iget-object v1, p0, Landroid/media/SmartRingtone$2;->this$0:Landroid/media/SmartRingtone;

    #@4f
    invoke-static {v1}, Landroid/media/SmartRingtone;->access$500(Landroid/media/SmartRingtone;)Landroid/media/AudioManager;

    #@52
    move-result-object v1

    #@53
    iget-object v2, p0, Landroid/media/SmartRingtone$2;->this$0:Landroid/media/SmartRingtone;

    #@55
    invoke-static {v2}, Landroid/media/SmartRingtone;->access$400(Landroid/media/SmartRingtone;)I

    #@58
    move-result v2

    #@59
    iget-object v3, p0, Landroid/media/SmartRingtone$2;->this$0:Landroid/media/SmartRingtone;

    #@5b
    invoke-static {v3}, Landroid/media/SmartRingtone;->access$700(Landroid/media/SmartRingtone;)I

    #@5e
    move-result v3

    #@5f
    const/4 v4, 0x0

    #@60
    invoke-virtual {v1, v2, v3, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V

    #@63
    .line 147
    invoke-static {}, Landroid/media/SmartRingtone;->access$800()Z

    #@66
    move-result v1

    #@67
    if-eqz v1, :cond_9a

    #@69
    invoke-static {}, Landroid/media/SmartRingtone;->access$000()Ljava/lang/String;

    #@6c
    move-result-object v1

    #@6d
    new-instance v2, Ljava/lang/StringBuilder;

    #@6f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@72
    const-string/jumbo v3, "setStreamVolume:: prev="

    #@75
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v2

    #@79
    iget-object v3, p0, Landroid/media/SmartRingtone$2;->this$0:Landroid/media/SmartRingtone;

    #@7b
    invoke-static {v3}, Landroid/media/SmartRingtone;->access$300(Landroid/media/SmartRingtone;)I

    #@7e
    move-result v3

    #@7f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@82
    move-result-object v2

    #@83
    const-string v3, ", adjust="

    #@85
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v2

    #@89
    iget-object v3, p0, Landroid/media/SmartRingtone$2;->this$0:Landroid/media/SmartRingtone;

    #@8b
    invoke-static {v3}, Landroid/media/SmartRingtone;->access$700(Landroid/media/SmartRingtone;)I

    #@8e
    move-result v3

    #@8f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@92
    move-result-object v2

    #@93
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@96
    move-result-object v2

    #@97
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9a
    .catchall {:try_start_1 .. :try_end_9a} :catchall_b9
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_9a} :catch_9c

    #@9a
    .line 153
    :cond_9a
    :goto_9a
    monitor-exit p0

    #@9b
    return-void

    #@9c
    .line 150
    :catch_9c
    move-exception v0

    #@9d
    .line 151
    .local v0, e:Ljava/lang/Exception;
    :try_start_9d
    invoke-static {}, Landroid/media/SmartRingtone;->access$000()Ljava/lang/String;

    #@a0
    move-result-object v1

    #@a1
    new-instance v2, Ljava/lang/StringBuilder;

    #@a3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a6
    const-string/jumbo v3, "smart ringtone exception: "

    #@a9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v2

    #@ad
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v2

    #@b1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b4
    move-result-object v2

    #@b5
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b8
    .catchall {:try_start_9d .. :try_end_b8} :catchall_b9

    #@b8
    goto :goto_9a

    #@b9
    .line 135
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_b9
    move-exception v1

    #@ba
    monitor-exit p0

    #@bb
    throw v1
.end method
