.class public final Landroid/media/MediaCrypto;
.super Ljava/lang/Object;
.source "MediaCrypto.java"


# instance fields
.field private mNativeContext:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 87
    const-string/jumbo v0, "media_jni"

    #@3
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@6
    .line 88
    invoke-static {}, Landroid/media/MediaCrypto;->native_init()V

    #@9
    .line 89
    return-void
.end method

.method public constructor <init>(Ljava/util/UUID;[B)V
    .registers 4
    .parameter "uuid"
    .parameter "initData"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/media/MediaCryptoException;
        }
    .end annotation

    #@0
    .prologue
    .line 62
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 63
    invoke-static {p1}, Landroid/media/MediaCrypto;->getByteArrayFromUUID(Ljava/util/UUID;)[B

    #@6
    move-result-object v0

    #@7
    invoke-direct {p0, v0, p2}, Landroid/media/MediaCrypto;->native_setup([B[B)V

    #@a
    .line 64
    return-void
.end method

.method private static final getByteArrayFromUUID(Ljava/util/UUID;)[B
    .registers 10
    .parameter "uuid"

    #@0
    .prologue
    .line 42
    invoke-virtual {p0}, Ljava/util/UUID;->getMostSignificantBits()J

    #@3
    move-result-wide v3

    #@4
    .line 43
    .local v3, msb:J
    invoke-virtual {p0}, Ljava/util/UUID;->getLeastSignificantBits()J

    #@7
    move-result-wide v1

    #@8
    .line 45
    .local v1, lsb:J
    const/16 v6, 0x10

    #@a
    new-array v5, v6, [B

    #@c
    .line 46
    .local v5, uuidBytes:[B
    const/4 v0, 0x0

    #@d
    .local v0, i:I
    :goto_d
    const/16 v6, 0x8

    #@f
    if-ge v0, v6, :cond_2a

    #@11
    .line 47
    rsub-int/lit8 v6, v0, 0x7

    #@13
    mul-int/lit8 v6, v6, 0x8

    #@15
    ushr-long v6, v3, v6

    #@17
    long-to-int v6, v6

    #@18
    int-to-byte v6, v6

    #@19
    aput-byte v6, v5, v0

    #@1b
    .line 48
    add-int/lit8 v6, v0, 0x8

    #@1d
    rsub-int/lit8 v7, v0, 0x7

    #@1f
    mul-int/lit8 v7, v7, 0x8

    #@21
    ushr-long v7, v1, v7

    #@23
    long-to-int v7, v7

    #@24
    int-to-byte v7, v7

    #@25
    aput-byte v7, v5, v6

    #@27
    .line 46
    add-int/lit8 v0, v0, 0x1

    #@29
    goto :goto_d

    #@2a
    .line 51
    :cond_2a
    return-object v5
.end method

.method public static final isCryptoSchemeSupported(Ljava/util/UUID;)Z
    .registers 2
    .parameter "uuid"

    #@0
    .prologue
    .line 38
    invoke-static {p0}, Landroid/media/MediaCrypto;->getByteArrayFromUUID(Ljava/util/UUID;)[B

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Landroid/media/MediaCrypto;->isCryptoSchemeSupportedNative([B)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method private static final native isCryptoSchemeSupportedNative([B)Z
.end method

.method private final native native_finalize()V
.end method

.method private static final native native_init()V
.end method

.method private final native native_setup([B[B)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/media/MediaCryptoException;
        }
    .end annotation
.end method


# virtual methods
.method protected finalize()V
    .registers 1

    #@0
    .prologue
    .line 75
    invoke-direct {p0}, Landroid/media/MediaCrypto;->native_finalize()V

    #@3
    .line 76
    return-void
.end method

.method public final native release()V
.end method

.method public final native requiresSecureDecoderComponent(Ljava/lang/String;)Z
.end method
