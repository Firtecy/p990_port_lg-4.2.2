.class public final Landroid/media/MediaFormat;
.super Ljava/lang/Object;
.source "MediaFormat.java"


# static fields
.field public static final KEY_AAC_PROFILE:Ljava/lang/String; = "aac-profile"

.field public static final KEY_BIT_RATE:Ljava/lang/String; = "bitrate"

.field public static final KEY_CHANNEL_COUNT:Ljava/lang/String; = "channel-count"

.field public static final KEY_CHANNEL_MASK:Ljava/lang/String; = "channel-mask"

.field public static final KEY_COLOR_FORMAT:Ljava/lang/String; = "color-format"

.field public static final KEY_DURATION:Ljava/lang/String; = "durationUs"

.field public static final KEY_FLAC_COMPRESSION_LEVEL:Ljava/lang/String; = "flac-compression-level"

.field public static final KEY_FRAME_RATE:Ljava/lang/String; = "frame-rate"

.field public static final KEY_HEIGHT:Ljava/lang/String; = "height"

.field public static final KEY_IS_ADTS:Ljava/lang/String; = "is-adts"

.field public static final KEY_I_FRAME_INTERVAL:Ljava/lang/String; = "i-frame-interval"

.field public static final KEY_MAX_INPUT_SIZE:Ljava/lang/String; = "max-input-size"

.field public static final KEY_MIME:Ljava/lang/String; = "mime"

.field public static final KEY_SAMPLE_RATE:Ljava/lang/String; = "sample-rate"

.field public static final KEY_SLICE_HEIGHT:Ljava/lang/String; = "slice-height"

.field public static final KEY_STRIDE:Ljava/lang/String; = "stride"

.field public static final KEY_WIDTH:Ljava/lang/String; = "width"


# instance fields
.field private mMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 175
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 176
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Landroid/media/MediaFormat;->mMap:Ljava/util/Map;

    #@a
    .line 177
    return-void
.end method

.method constructor <init>(Ljava/util/Map;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 168
    .local p1, map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 169
    iput-object p1, p0, Landroid/media/MediaFormat;->mMap:Ljava/util/Map;

    #@5
    .line 170
    return-void
.end method

.method public static final createAudioFormat(Ljava/lang/String;II)Landroid/media/MediaFormat;
    .registers 5
    .parameter "mime"
    .parameter "sampleRate"
    .parameter "channelCount"

    #@0
    .prologue
    .line 270
    new-instance v0, Landroid/media/MediaFormat;

    #@2
    invoke-direct {v0}, Landroid/media/MediaFormat;-><init>()V

    #@5
    .line 271
    .local v0, format:Landroid/media/MediaFormat;
    const-string/jumbo v1, "mime"

    #@8
    invoke-virtual {v0, v1, p0}, Landroid/media/MediaFormat;->setString(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 272
    const-string/jumbo v1, "sample-rate"

    #@e
    invoke-virtual {v0, v1, p1}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    #@11
    .line 273
    const-string v1, "channel-count"

    #@13
    invoke-virtual {v0, v1, p2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    #@16
    .line 275
    return-object v0
.end method

.method public static final createVideoFormat(Ljava/lang/String;II)Landroid/media/MediaFormat;
    .registers 5
    .parameter "mime"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 288
    new-instance v0, Landroid/media/MediaFormat;

    #@2
    invoke-direct {v0}, Landroid/media/MediaFormat;-><init>()V

    #@5
    .line 289
    .local v0, format:Landroid/media/MediaFormat;
    const-string/jumbo v1, "mime"

    #@8
    invoke-virtual {v0, v1, p0}, Landroid/media/MediaFormat;->setString(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 290
    const-string/jumbo v1, "width"

    #@e
    invoke-virtual {v0, v1, p1}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    #@11
    .line 291
    const-string v1, "height"

    #@13
    invoke-virtual {v0, v1, p2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    #@16
    .line 293
    return-object v0
.end method


# virtual methods
.method public final containsKey(Ljava/lang/String;)Z
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 187
    iget-object v0, p0, Landroid/media/MediaFormat;->mMap:Ljava/util/Map;

    #@2
    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final getByteBuffer(Ljava/lang/String;)Ljava/nio/ByteBuffer;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 222
    iget-object v0, p0, Landroid/media/MediaFormat;->mMap:Ljava/util/Map;

    #@2
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/nio/ByteBuffer;

    #@8
    return-object v0
.end method

.method public final getFloat(Ljava/lang/String;)F
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 208
    iget-object v0, p0, Landroid/media/MediaFormat;->mMap:Ljava/util/Map;

    #@2
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/lang/Float;

    #@8
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public final getInteger(Ljava/lang/String;)I
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 194
    iget-object v0, p0, Landroid/media/MediaFormat;->mMap:Ljava/util/Map;

    #@2
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/lang/Integer;

    #@8
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public final getLong(Ljava/lang/String;)J
    .registers 4
    .parameter "name"

    #@0
    .prologue
    .line 201
    iget-object v0, p0, Landroid/media/MediaFormat;->mMap:Ljava/util/Map;

    #@2
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/lang/Long;

    #@8
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    #@b
    move-result-wide v0

    #@c
    return-wide v0
.end method

.method getMap()Ljava/util/Map;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 180
    iget-object v0, p0, Landroid/media/MediaFormat;->mMap:Ljava/util/Map;

    #@2
    return-object v0
.end method

.method public final getString(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 215
    iget-object v0, p0, Landroid/media/MediaFormat;->mMap:Ljava/util/Map;

    #@2
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/lang/String;

    #@8
    return-object v0
.end method

.method public final setByteBuffer(Ljava/lang/String;Ljava/nio/ByteBuffer;)V
    .registers 4
    .parameter "name"
    .parameter "bytes"

    #@0
    .prologue
    .line 257
    iget-object v0, p0, Landroid/media/MediaFormat;->mMap:Ljava/util/Map;

    #@2
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    .line 258
    return-void
.end method

.method public final setFloat(Ljava/lang/String;F)V
    .registers 5
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 243
    iget-object v0, p0, Landroid/media/MediaFormat;->mMap:Ljava/util/Map;

    #@2
    new-instance v1, Ljava/lang/Float;

    #@4
    invoke-direct {v1, p2}, Ljava/lang/Float;-><init>(F)V

    #@7
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    .line 244
    return-void
.end method

.method public final setInteger(Ljava/lang/String;I)V
    .registers 5
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 229
    iget-object v0, p0, Landroid/media/MediaFormat;->mMap:Ljava/util/Map;

    #@2
    new-instance v1, Ljava/lang/Integer;

    #@4
    invoke-direct {v1, p2}, Ljava/lang/Integer;-><init>(I)V

    #@7
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    .line 230
    return-void
.end method

.method public final setLong(Ljava/lang/String;J)V
    .registers 6
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 236
    iget-object v0, p0, Landroid/media/MediaFormat;->mMap:Ljava/util/Map;

    #@2
    new-instance v1, Ljava/lang/Long;

    #@4
    invoke-direct {v1, p2, p3}, Ljava/lang/Long;-><init>(J)V

    #@7
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    .line 237
    return-void
.end method

.method public final setString(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 250
    iget-object v0, p0, Landroid/media/MediaFormat;->mMap:Ljava/util/Map;

    #@2
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    .line 251
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 298
    iget-object v0, p0, Landroid/media/MediaFormat;->mMap:Ljava/util/Map;

    #@2
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method
