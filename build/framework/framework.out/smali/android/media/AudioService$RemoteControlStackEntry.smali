.class Landroid/media/AudioService$RemoteControlStackEntry;
.super Ljava/lang/Object;
.source "AudioService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/AudioService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RemoteControlStackEntry"
.end annotation


# instance fields
.field public mCallingPackageName:Ljava/lang/String;

.field public mCallingUid:I

.field public mMediaIntent:Landroid/app/PendingIntent;

.field public mPlaybackState:I

.field public mPlaybackStream:I

.field public mPlaybackType:I

.field public mPlaybackVolume:I

.field public mPlaybackVolumeHandling:I

.field public mPlaybackVolumeMax:I

.field public mRcClient:Landroid/media/IRemoteControlClient;

.field public mRcClientDeathHandler:Landroid/media/AudioService$RcClientDeathHandler;

.field public mRccId:I

.field public mReceiverComponent:Landroid/content/ComponentName;

.field public mRemoteVolumeObs:Landroid/media/IRemoteVolumeObserver;


# direct methods
.method public constructor <init>(Landroid/app/PendingIntent;Landroid/content/ComponentName;)V
    .registers 4
    .parameter "mediaIntent"
    .parameter "eventReceiver"

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 5649
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 5608
    iput v0, p0, Landroid/media/AudioService$RemoteControlStackEntry;->mRccId:I

    #@6
    .line 5650
    iput-object p1, p0, Landroid/media/AudioService$RemoteControlStackEntry;->mMediaIntent:Landroid/app/PendingIntent;

    #@8
    .line 5651
    iput-object p2, p0, Landroid/media/AudioService$RemoteControlStackEntry;->mReceiverComponent:Landroid/content/ComponentName;

    #@a
    .line 5652
    iput v0, p0, Landroid/media/AudioService$RemoteControlStackEntry;->mCallingUid:I

    #@c
    .line 5653
    const/4 v0, 0x0

    #@d
    iput-object v0, p0, Landroid/media/AudioService$RemoteControlStackEntry;->mRcClient:Landroid/media/IRemoteControlClient;

    #@f
    .line 5654
    invoke-static {}, Landroid/media/AudioService;->access$9904()I

    #@12
    move-result v0

    #@13
    iput v0, p0, Landroid/media/AudioService$RemoteControlStackEntry;->mRccId:I

    #@15
    .line 5656
    invoke-virtual {p0}, Landroid/media/AudioService$RemoteControlStackEntry;->resetPlaybackInfo()V

    #@18
    .line 5657
    return-void
.end method


# virtual methods
.method protected finalize()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 5674
    invoke-virtual {p0}, Landroid/media/AudioService$RemoteControlStackEntry;->unlinkToRcClientDeath()V

    #@3
    .line 5675
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@6
    .line 5676
    return-void
.end method

.method public resetPlaybackInfo()V
    .registers 4

    #@0
    .prologue
    const/16 v2, 0xf

    #@2
    const/4 v1, 0x1

    #@3
    .line 5639
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/media/AudioService$RemoteControlStackEntry;->mPlaybackType:I

    #@6
    .line 5640
    iput v2, p0, Landroid/media/AudioService$RemoteControlStackEntry;->mPlaybackVolume:I

    #@8
    .line 5641
    iput v2, p0, Landroid/media/AudioService$RemoteControlStackEntry;->mPlaybackVolumeMax:I

    #@a
    .line 5642
    iput v1, p0, Landroid/media/AudioService$RemoteControlStackEntry;->mPlaybackVolumeHandling:I

    #@c
    .line 5643
    const/4 v0, 0x3

    #@d
    iput v0, p0, Landroid/media/AudioService$RemoteControlStackEntry;->mPlaybackStream:I

    #@f
    .line 5644
    iput v1, p0, Landroid/media/AudioService$RemoteControlStackEntry;->mPlaybackState:I

    #@11
    .line 5645
    const/4 v0, 0x0

    #@12
    iput-object v0, p0, Landroid/media/AudioService$RemoteControlStackEntry;->mRemoteVolumeObs:Landroid/media/IRemoteVolumeObserver;

    #@14
    .line 5646
    return-void
.end method

.method public unlinkToRcClientDeath()V
    .registers 5

    #@0
    .prologue
    .line 5660
    iget-object v1, p0, Landroid/media/AudioService$RemoteControlStackEntry;->mRcClientDeathHandler:Landroid/media/AudioService$RcClientDeathHandler;

    #@2
    if-eqz v1, :cond_1b

    #@4
    iget-object v1, p0, Landroid/media/AudioService$RemoteControlStackEntry;->mRcClientDeathHandler:Landroid/media/AudioService$RcClientDeathHandler;

    #@6
    invoke-static {v1}, Landroid/media/AudioService$RcClientDeathHandler;->access$10000(Landroid/media/AudioService$RcClientDeathHandler;)Landroid/os/IBinder;

    #@9
    move-result-object v1

    #@a
    if-eqz v1, :cond_1b

    #@c
    .line 5662
    :try_start_c
    iget-object v1, p0, Landroid/media/AudioService$RemoteControlStackEntry;->mRcClientDeathHandler:Landroid/media/AudioService$RcClientDeathHandler;

    #@e
    invoke-static {v1}, Landroid/media/AudioService$RcClientDeathHandler;->access$10000(Landroid/media/AudioService$RcClientDeathHandler;)Landroid/os/IBinder;

    #@11
    move-result-object v1

    #@12
    iget-object v2, p0, Landroid/media/AudioService$RemoteControlStackEntry;->mRcClientDeathHandler:Landroid/media/AudioService$RcClientDeathHandler;

    #@14
    const/4 v3, 0x0

    #@15
    invoke-interface {v1, v2, v3}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    #@18
    .line 5663
    const/4 v1, 0x0

    #@19
    iput-object v1, p0, Landroid/media/AudioService$RemoteControlStackEntry;->mRcClientDeathHandler:Landroid/media/AudioService$RcClientDeathHandler;
    :try_end_1b
    .catch Ljava/util/NoSuchElementException; {:try_start_c .. :try_end_1b} :catch_1c

    #@1b
    .line 5670
    :cond_1b
    :goto_1b
    return-void

    #@1c
    .line 5664
    :catch_1c
    move-exception v0

    #@1d
    .line 5666
    .local v0, e:Ljava/util/NoSuchElementException;
    const-string v1, "AudioService"

    #@1f
    new-instance v2, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v3, "Encountered "

    #@26
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    const-string v3, " in unlinkToRcClientDeath()"

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v2

    #@38
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 5667
    invoke-virtual {v0}, Ljava/util/NoSuchElementException;->printStackTrace()V

    #@3e
    goto :goto_1b
.end method
