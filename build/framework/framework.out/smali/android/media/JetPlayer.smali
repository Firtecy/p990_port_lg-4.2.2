.class public Landroid/media/JetPlayer;
.super Ljava/lang/Object;
.source "JetPlayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/JetPlayer$OnJetEventListener;,
        Landroid/media/JetPlayer$NativeEventHandler;
    }
.end annotation


# static fields
.field private static final JET_EVENT:I = 0x1

.field private static final JET_EVENT_CHAN_MASK:I = 0x3c000

.field private static final JET_EVENT_CHAN_SHIFT:I = 0xe

.field private static final JET_EVENT_CTRL_MASK:I = 0x3f80

.field private static final JET_EVENT_CTRL_SHIFT:I = 0x7

.field private static final JET_EVENT_SEG_MASK:I = -0x1000000

.field private static final JET_EVENT_SEG_SHIFT:I = 0x18

.field private static final JET_EVENT_TRACK_MASK:I = 0xfc0000

.field private static final JET_EVENT_TRACK_SHIFT:I = 0x12

.field private static final JET_EVENT_VAL_MASK:I = 0x7f

.field private static final JET_NUMQUEUEDSEGMENT_UPDATE:I = 0x3

.field private static final JET_OUTPUT_CHANNEL_CONFIG:I = 0xc

.field private static final JET_OUTPUT_RATE:I = 0x5622

.field private static final JET_PAUSE_UPDATE:I = 0x4

.field private static final JET_USERID_UPDATE:I = 0x2

.field private static MAXTRACKS:I = 0x0

.field private static final TAG:Ljava/lang/String; = "JetPlayer-J"

.field private static singletonRef:Landroid/media/JetPlayer;


# instance fields
.field private mEventHandler:Landroid/media/JetPlayer$NativeEventHandler;

.field private final mEventListenerLock:Ljava/lang/Object;

.field private mInitializationLooper:Landroid/os/Looper;

.field private mJetEventListener:Landroid/media/JetPlayer$OnJetEventListener;

.field private mNativePlayerInJavaObj:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 72
    const/16 v0, 0x20

    #@2
    sput v0, Landroid/media/JetPlayer;->MAXTRACKS:I

    #@4
    return-void
.end method

.method private constructor <init>()V
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 157
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 106
    iput-object v2, p0, Landroid/media/JetPlayer;->mEventHandler:Landroid/media/JetPlayer$NativeEventHandler;

    #@6
    .line 111
    iput-object v2, p0, Landroid/media/JetPlayer;->mInitializationLooper:Landroid/os/Looper;

    #@8
    .line 116
    new-instance v1, Ljava/lang/Object;

    #@a
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@d
    iput-object v1, p0, Landroid/media/JetPlayer;->mEventListenerLock:Ljava/lang/Object;

    #@f
    .line 118
    iput-object v2, p0, Landroid/media/JetPlayer;->mJetEventListener:Landroid/media/JetPlayer$OnJetEventListener;

    #@11
    .line 160
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@14
    move-result-object v1

    #@15
    iput-object v1, p0, Landroid/media/JetPlayer;->mInitializationLooper:Landroid/os/Looper;

    #@17
    if-nez v1, :cond_1f

    #@19
    .line 161
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    #@1c
    move-result-object v1

    #@1d
    iput-object v1, p0, Landroid/media/JetPlayer;->mInitializationLooper:Landroid/os/Looper;

    #@1f
    .line 164
    :cond_1f
    const/16 v1, 0x5622

    #@21
    const/16 v2, 0xc

    #@23
    const/4 v3, 0x2

    #@24
    invoke-static {v1, v2, v3}, Landroid/media/AudioTrack;->getMinBufferSize(III)I

    #@27
    move-result v0

    #@28
    .line 167
    .local v0, buffSizeInBytes:I
    const/4 v1, -0x1

    #@29
    if-eq v0, v1, :cond_42

    #@2b
    const/4 v1, -0x2

    #@2c
    if-eq v0, v1, :cond_42

    #@2e
    .line 170
    new-instance v1, Ljava/lang/ref/WeakReference;

    #@30
    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@33
    invoke-static {}, Landroid/media/JetPlayer;->getMaxTracks()I

    #@36
    move-result v2

    #@37
    const/16 v3, 0x4b0

    #@39
    div-int/lit8 v4, v0, 0x4

    #@3b
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    #@3e
    move-result v3

    #@3f
    invoke-direct {p0, v1, v2, v3}, Landroid/media/JetPlayer;->native_setup(Ljava/lang/Object;II)Z

    #@42
    .line 176
    :cond_42
    return-void
.end method

.method static synthetic access$000(Landroid/media/JetPlayer;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Landroid/media/JetPlayer;->mEventListenerLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/media/JetPlayer;)Landroid/media/JetPlayer$OnJetEventListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Landroid/media/JetPlayer;->mJetEventListener:Landroid/media/JetPlayer$OnJetEventListener;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Ljava/lang/String;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 63
    invoke-static {p0}, Landroid/media/JetPlayer;->loge(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method public static getJetPlayer()Landroid/media/JetPlayer;
    .registers 1

    #@0
    .prologue
    .line 141
    sget-object v0, Landroid/media/JetPlayer;->singletonRef:Landroid/media/JetPlayer;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 142
    new-instance v0, Landroid/media/JetPlayer;

    #@6
    invoke-direct {v0}, Landroid/media/JetPlayer;-><init>()V

    #@9
    sput-object v0, Landroid/media/JetPlayer;->singletonRef:Landroid/media/JetPlayer;

    #@b
    .line 144
    :cond_b
    sget-object v0, Landroid/media/JetPlayer;->singletonRef:Landroid/media/JetPlayer;

    #@d
    return-object v0
.end method

.method public static getMaxTracks()I
    .registers 1

    #@0
    .prologue
    .line 202
    sget v0, Landroid/media/JetPlayer;->MAXTRACKS:I

    #@2
    return v0
.end method

.method private static logd(Ljava/lang/String;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 581
    const-string v0, "JetPlayer-J"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[ android.media.JetPlayer ] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 582
    return-void
.end method

.method private static loge(Ljava/lang/String;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 585
    const-string v0, "JetPlayer-J"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[ android.media.JetPlayer ] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 586
    return-void
.end method

.method private final native native_clearQueue()Z
.end method

.method private final native native_closeJetFile()Z
.end method

.method private final native native_finalize()V
.end method

.method private final native native_loadJetFromFile(Ljava/lang/String;)Z
.end method

.method private final native native_loadJetFromFileD(Ljava/io/FileDescriptor;JJ)Z
.end method

.method private final native native_pauseJet()Z
.end method

.method private final native native_playJet()Z
.end method

.method private final native native_queueJetSegment(IIIIIB)Z
.end method

.method private final native native_queueJetSegmentMuteArray(IIII[ZB)Z
.end method

.method private final native native_release()V
.end method

.method private final native native_setMuteArray([ZZ)Z
.end method

.method private final native native_setMuteFlag(IZZ)Z
.end method

.method private final native native_setMuteFlags(IZ)Z
.end method

.method private final native native_setup(Ljava/lang/Object;II)Z
.end method

.method private final native native_triggerClip(I)Z
.end method

.method private static postEventFromNative(Ljava/lang/Object;III)V
    .registers 8
    .parameter "jetplayer_ref"
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"

    #@0
    .prologue
    .line 564
    check-cast p0, Ljava/lang/ref/WeakReference;

    #@2
    .end local p0
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/media/JetPlayer;

    #@8
    .line 566
    .local v0, jet:Landroid/media/JetPlayer;
    if-eqz v0, :cond_1a

    #@a
    iget-object v2, v0, Landroid/media/JetPlayer;->mEventHandler:Landroid/media/JetPlayer$NativeEventHandler;

    #@c
    if-eqz v2, :cond_1a

    #@e
    .line 567
    iget-object v2, v0, Landroid/media/JetPlayer;->mEventHandler:Landroid/media/JetPlayer$NativeEventHandler;

    #@10
    const/4 v3, 0x0

    #@11
    invoke-virtual {v2, p1, p2, p3, v3}, Landroid/media/JetPlayer$NativeEventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@14
    move-result-object v1

    #@15
    .line 569
    .local v1, m:Landroid/os/Message;
    iget-object v2, v0, Landroid/media/JetPlayer;->mEventHandler:Landroid/media/JetPlayer$NativeEventHandler;

    #@17
    invoke-virtual {v2, v1}, Landroid/media/JetPlayer$NativeEventHandler;->sendMessage(Landroid/os/Message;)Z

    #@1a
    .line 572
    .end local v1           #m:Landroid/os/Message;
    :cond_1a
    return-void
.end method


# virtual methods
.method public clearQueue()Z
    .registers 2

    #@0
    .prologue
    .line 389
    invoke-direct {p0}, Landroid/media/JetPlayer;->native_clearQueue()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 153
    new-instance v0, Ljava/lang/CloneNotSupportedException;

    #@2
    invoke-direct {v0}, Ljava/lang/CloneNotSupportedException;-><init>()V

    #@5
    throw v0
.end method

.method public closeJetFile()Z
    .registers 2

    #@0
    .prologue
    .line 238
    invoke-direct {p0}, Landroid/media/JetPlayer;->native_closeJetFile()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method protected finalize()V
    .registers 1

    #@0
    .prologue
    .line 180
    invoke-direct {p0}, Landroid/media/JetPlayer;->native_finalize()V

    #@3
    .line 181
    return-void
.end method

.method public loadJetFile(Landroid/content/res/AssetFileDescriptor;)Z
    .registers 8
    .parameter "afd"

    #@0
    .prologue
    .line 225
    invoke-virtual {p1}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    #@3
    move-result-wide v4

    #@4
    .line 226
    .local v4, len:J
    const-wide/16 v0, 0x0

    #@6
    cmp-long v0, v4, v0

    #@8
    if-gez v0, :cond_13

    #@a
    .line 227
    new-instance v0, Landroid/util/AndroidRuntimeException;

    #@c
    const-string/jumbo v1, "no length for fd"

    #@f
    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    #@12
    throw v0

    #@13
    .line 229
    :cond_13
    invoke-virtual {p1}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {p1}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    #@1a
    move-result-wide v2

    #@1b
    move-object v0, p0

    #@1c
    invoke-direct/range {v0 .. v5}, Landroid/media/JetPlayer;->native_loadJetFromFileD(Ljava/io/FileDescriptor;JJ)Z

    #@1f
    move-result v0

    #@20
    return v0
.end method

.method public loadJetFile(Ljava/lang/String;)Z
    .registers 3
    .parameter "path"

    #@0
    .prologue
    .line 215
    invoke-direct {p0, p1}, Landroid/media/JetPlayer;->native_loadJetFromFile(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public pause()Z
    .registers 2

    #@0
    .prologue
    .line 256
    invoke-direct {p0}, Landroid/media/JetPlayer;->native_pauseJet()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public play()Z
    .registers 2

    #@0
    .prologue
    .line 247
    invoke-direct {p0}, Landroid/media/JetPlayer;->native_playJet()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public queueJetSegment(IIIIIB)Z
    .registers 8
    .parameter "segmentNum"
    .parameter "libNum"
    .parameter "repeatCount"
    .parameter "transpose"
    .parameter "muteFlags"
    .parameter "userID"

    #@0
    .prologue
    .line 283
    invoke-direct/range {p0 .. p6}, Landroid/media/JetPlayer;->native_queueJetSegment(IIIIIB)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public queueJetSegmentMuteArray(IIII[ZB)Z
    .registers 9
    .parameter "segmentNum"
    .parameter "libNum"
    .parameter "repeatCount"
    .parameter "transpose"
    .parameter "muteArray"
    .parameter "userID"

    #@0
    .prologue
    .line 312
    array-length v0, p5

    #@1
    invoke-static {}, Landroid/media/JetPlayer;->getMaxTracks()I

    #@4
    move-result v1

    #@5
    if-eq v0, v1, :cond_9

    #@7
    .line 313
    const/4 v0, 0x0

    #@8
    .line 315
    :goto_8
    return v0

    #@9
    :cond_9
    invoke-direct/range {p0 .. p6}, Landroid/media/JetPlayer;->native_queueJetSegmentMuteArray(IIII[ZB)Z

    #@c
    move-result v0

    #@d
    goto :goto_8
.end method

.method public release()V
    .registers 2

    #@0
    .prologue
    .line 190
    invoke-direct {p0}, Landroid/media/JetPlayer;->native_release()V

    #@3
    .line 191
    const/4 v0, 0x0

    #@4
    sput-object v0, Landroid/media/JetPlayer;->singletonRef:Landroid/media/JetPlayer;

    #@6
    .line 192
    return-void
.end method

.method public setEventListener(Landroid/media/JetPlayer$OnJetEventListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 461
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/media/JetPlayer;->setEventListener(Landroid/media/JetPlayer$OnJetEventListener;Landroid/os/Handler;)V

    #@4
    .line 462
    return-void
.end method

.method public setEventListener(Landroid/media/JetPlayer$OnJetEventListener;Landroid/os/Handler;)V
    .registers 6
    .parameter "listener"
    .parameter "handler"

    #@0
    .prologue
    .line 473
    iget-object v1, p0, Landroid/media/JetPlayer;->mEventListenerLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 475
    :try_start_3
    iput-object p1, p0, Landroid/media/JetPlayer;->mJetEventListener:Landroid/media/JetPlayer$OnJetEventListener;

    #@5
    .line 477
    if-eqz p1, :cond_23

    #@7
    .line 478
    if-eqz p2, :cond_16

    #@9
    .line 479
    new-instance v0, Landroid/media/JetPlayer$NativeEventHandler;

    #@b
    invoke-virtual {p2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@e
    move-result-object v2

    #@f
    invoke-direct {v0, p0, p0, v2}, Landroid/media/JetPlayer$NativeEventHandler;-><init>(Landroid/media/JetPlayer;Landroid/media/JetPlayer;Landroid/os/Looper;)V

    #@12
    iput-object v0, p0, Landroid/media/JetPlayer;->mEventHandler:Landroid/media/JetPlayer$NativeEventHandler;

    #@14
    .line 488
    :goto_14
    monitor-exit v1

    #@15
    .line 489
    return-void

    #@16
    .line 482
    :cond_16
    new-instance v0, Landroid/media/JetPlayer$NativeEventHandler;

    #@18
    iget-object v2, p0, Landroid/media/JetPlayer;->mInitializationLooper:Landroid/os/Looper;

    #@1a
    invoke-direct {v0, p0, p0, v2}, Landroid/media/JetPlayer$NativeEventHandler;-><init>(Landroid/media/JetPlayer;Landroid/media/JetPlayer;Landroid/os/Looper;)V

    #@1d
    iput-object v0, p0, Landroid/media/JetPlayer;->mEventHandler:Landroid/media/JetPlayer$NativeEventHandler;

    #@1f
    goto :goto_14

    #@20
    .line 488
    :catchall_20
    move-exception v0

    #@21
    monitor-exit v1
    :try_end_22
    .catchall {:try_start_3 .. :try_end_22} :catchall_20

    #@22
    throw v0

    #@23
    .line 485
    :cond_23
    const/4 v0, 0x0

    #@24
    :try_start_24
    iput-object v0, p0, Landroid/media/JetPlayer;->mEventHandler:Landroid/media/JetPlayer$NativeEventHandler;
    :try_end_26
    .catchall {:try_start_24 .. :try_end_26} :catchall_20

    #@26
    goto :goto_14
.end method

.method public setMuteArray([ZZ)Z
    .registers 5
    .parameter "muteArray"
    .parameter "sync"

    #@0
    .prologue
    .line 347
    array-length v0, p1

    #@1
    invoke-static {}, Landroid/media/JetPlayer;->getMaxTracks()I

    #@4
    move-result v1

    #@5
    if-eq v0, v1, :cond_9

    #@7
    .line 348
    const/4 v0, 0x0

    #@8
    .line 349
    :goto_8
    return v0

    #@9
    :cond_9
    invoke-direct {p0, p1, p2}, Landroid/media/JetPlayer;->native_setMuteArray([ZZ)Z

    #@c
    move-result v0

    #@d
    goto :goto_8
.end method

.method public setMuteFlag(IZZ)Z
    .registers 5
    .parameter "trackId"
    .parameter "muteFlag"
    .parameter "sync"

    #@0
    .prologue
    .line 364
    invoke-direct {p0, p1, p2, p3}, Landroid/media/JetPlayer;->native_setMuteFlag(IZZ)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public setMuteFlags(IZ)Z
    .registers 4
    .parameter "muteFlags"
    .parameter "sync"

    #@0
    .prologue
    .line 331
    invoke-direct {p0, p1, p2}, Landroid/media/JetPlayer;->native_setMuteFlags(IZ)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public triggerClip(I)Z
    .registers 3
    .parameter "clipId"

    #@0
    .prologue
    .line 380
    invoke-direct {p0, p1}, Landroid/media/JetPlayer;->native_triggerClip(I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method
