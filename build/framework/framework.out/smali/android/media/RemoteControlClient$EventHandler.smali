.class Landroid/media/RemoteControlClient$EventHandler;
.super Landroid/os/Handler;
.source "RemoteControlClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/RemoteControlClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventHandler"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/media/RemoteControlClient;


# direct methods
.method public constructor <init>(Landroid/media/RemoteControlClient;Landroid/media/RemoteControlClient;Landroid/os/Looper;)V
    .registers 4
    .parameter
    .parameter "rcc"
    .parameter "looper"

    #@0
    .prologue
    .line 920
    iput-object p1, p0, Landroid/media/RemoteControlClient$EventHandler;->this$0:Landroid/media/RemoteControlClient;

    #@2
    .line 921
    invoke-direct {p0, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 922
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    .line 926
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v0, :pswitch_data_94

    #@5
    .line 960
    const-string v0, "RemoteControlClient"

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "Unknown event "

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    iget v2, p1, Landroid/os/Message;->what:I

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    const-string v2, " in RemoteControlClient handler"

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 962
    :goto_25
    return-void

    #@26
    .line 928
    :pswitch_26
    iget-object v0, p0, Landroid/media/RemoteControlClient$EventHandler;->this$0:Landroid/media/RemoteControlClient;

    #@28
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$600(Landroid/media/RemoteControlClient;)Ljava/lang/Object;

    #@2b
    move-result-object v1

    #@2c
    monitor-enter v1

    #@2d
    .line 929
    :try_start_2d
    iget-object v0, p0, Landroid/media/RemoteControlClient$EventHandler;->this$0:Landroid/media/RemoteControlClient;

    #@2f
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$1400(Landroid/media/RemoteControlClient;)V

    #@32
    .line 930
    monitor-exit v1

    #@33
    goto :goto_25

    #@34
    :catchall_34
    move-exception v0

    #@35
    monitor-exit v1
    :try_end_36
    .catchall {:try_start_2d .. :try_end_36} :catchall_34

    #@36
    throw v0

    #@37
    .line 933
    :pswitch_37
    iget-object v0, p0, Landroid/media/RemoteControlClient$EventHandler;->this$0:Landroid/media/RemoteControlClient;

    #@39
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$600(Landroid/media/RemoteControlClient;)Ljava/lang/Object;

    #@3c
    move-result-object v1

    #@3d
    monitor-enter v1

    #@3e
    .line 934
    :try_start_3e
    iget-object v0, p0, Landroid/media/RemoteControlClient$EventHandler;->this$0:Landroid/media/RemoteControlClient;

    #@40
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$1000(Landroid/media/RemoteControlClient;)V

    #@43
    .line 935
    monitor-exit v1

    #@44
    goto :goto_25

    #@45
    :catchall_45
    move-exception v0

    #@46
    monitor-exit v1
    :try_end_47
    .catchall {:try_start_3e .. :try_end_47} :catchall_45

    #@47
    throw v0

    #@48
    .line 938
    :pswitch_48
    iget-object v0, p0, Landroid/media/RemoteControlClient$EventHandler;->this$0:Landroid/media/RemoteControlClient;

    #@4a
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$600(Landroid/media/RemoteControlClient;)Ljava/lang/Object;

    #@4d
    move-result-object v1

    #@4e
    monitor-enter v1

    #@4f
    .line 939
    :try_start_4f
    iget-object v0, p0, Landroid/media/RemoteControlClient$EventHandler;->this$0:Landroid/media/RemoteControlClient;

    #@51
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$1500(Landroid/media/RemoteControlClient;)V

    #@54
    .line 940
    monitor-exit v1

    #@55
    goto :goto_25

    #@56
    :catchall_56
    move-exception v0

    #@57
    monitor-exit v1
    :try_end_58
    .catchall {:try_start_4f .. :try_end_58} :catchall_56

    #@58
    throw v0

    #@59
    .line 943
    :pswitch_59
    iget-object v0, p0, Landroid/media/RemoteControlClient$EventHandler;->this$0:Landroid/media/RemoteControlClient;

    #@5b
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$600(Landroid/media/RemoteControlClient;)Ljava/lang/Object;

    #@5e
    move-result-object v1

    #@5f
    monitor-enter v1

    #@60
    .line 944
    :try_start_60
    iget-object v0, p0, Landroid/media/RemoteControlClient$EventHandler;->this$0:Landroid/media/RemoteControlClient;

    #@62
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$1100(Landroid/media/RemoteControlClient;)V

    #@65
    .line 945
    monitor-exit v1

    #@66
    goto :goto_25

    #@67
    :catchall_67
    move-exception v0

    #@68
    monitor-exit v1
    :try_end_69
    .catchall {:try_start_60 .. :try_end_69} :catchall_67

    #@69
    throw v0

    #@6a
    .line 948
    :pswitch_6a
    iget-object v1, p0, Landroid/media/RemoteControlClient$EventHandler;->this$0:Landroid/media/RemoteControlClient;

    #@6c
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@6e
    check-cast v0, Ljava/lang/Integer;

    #@70
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@72
    iget v3, p1, Landroid/os/Message;->arg2:I

    #@74
    invoke-static {v1, v0, v2, v3}, Landroid/media/RemoteControlClient;->access$1600(Landroid/media/RemoteControlClient;Ljava/lang/Integer;II)V

    #@77
    goto :goto_25

    #@78
    .line 951
    :pswitch_78
    iget-object v0, p0, Landroid/media/RemoteControlClient$EventHandler;->this$0:Landroid/media/RemoteControlClient;

    #@7a
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@7c
    invoke-static {v0, v1}, Landroid/media/RemoteControlClient;->access$1700(Landroid/media/RemoteControlClient;I)V

    #@7f
    goto :goto_25

    #@80
    .line 954
    :pswitch_80
    iget-object v1, p0, Landroid/media/RemoteControlClient$EventHandler;->this$0:Landroid/media/RemoteControlClient;

    #@82
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@84
    check-cast v0, Landroid/media/IRemoteControlDisplay;

    #@86
    invoke-static {v1, v0}, Landroid/media/RemoteControlClient;->access$1800(Landroid/media/RemoteControlClient;Landroid/media/IRemoteControlDisplay;)V

    #@89
    goto :goto_25

    #@8a
    .line 957
    :pswitch_8a
    iget-object v1, p0, Landroid/media/RemoteControlClient$EventHandler;->this$0:Landroid/media/RemoteControlClient;

    #@8c
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8e
    check-cast v0, Landroid/media/IRemoteControlDisplay;

    #@90
    invoke-static {v1, v0}, Landroid/media/RemoteControlClient;->access$1900(Landroid/media/RemoteControlClient;Landroid/media/IRemoteControlDisplay;)V

    #@93
    goto :goto_25

    #@94
    .line 926
    :pswitch_data_94
    .packed-switch 0x1
        :pswitch_26
        :pswitch_37
        :pswitch_48
        :pswitch_59
        :pswitch_6a
        :pswitch_78
        :pswitch_80
        :pswitch_8a
    .end packed-switch
.end method
