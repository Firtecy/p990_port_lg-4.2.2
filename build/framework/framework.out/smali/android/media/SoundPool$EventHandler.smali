.class Landroid/media/SoundPool$EventHandler;
.super Landroid/os/Handler;
.source "SoundPool.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/SoundPool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventHandler"
.end annotation


# instance fields
.field private mSoundPool:Landroid/media/SoundPool;

.field final synthetic this$0:Landroid/media/SoundPool;


# direct methods
.method public constructor <init>(Landroid/media/SoundPool;Landroid/media/SoundPool;Landroid/os/Looper;)V
    .registers 4
    .parameter
    .parameter "soundPool"
    .parameter "looper"

    #@0
    .prologue
    .line 438
    iput-object p1, p0, Landroid/media/SoundPool$EventHandler;->this$0:Landroid/media/SoundPool;

    #@2
    .line 439
    invoke-direct {p0, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 440
    iput-object p2, p0, Landroid/media/SoundPool$EventHandler;->mSoundPool:Landroid/media/SoundPool;

    #@7
    .line 441
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    .line 445
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v0, :pswitch_data_44

    #@5
    .line 455
    const-string v0, "SoundPool"

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "Unknown message type "

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    iget v2, p1, Landroid/os/Message;->what:I

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 458
    :goto_1f
    return-void

    #@20
    .line 448
    :pswitch_20
    iget-object v0, p0, Landroid/media/SoundPool$EventHandler;->this$0:Landroid/media/SoundPool;

    #@22
    invoke-static {v0}, Landroid/media/SoundPool;->access$000(Landroid/media/SoundPool;)Ljava/lang/Object;

    #@25
    move-result-object v1

    #@26
    monitor-enter v1

    #@27
    .line 449
    :try_start_27
    iget-object v0, p0, Landroid/media/SoundPool$EventHandler;->this$0:Landroid/media/SoundPool;

    #@29
    invoke-static {v0}, Landroid/media/SoundPool;->access$100(Landroid/media/SoundPool;)Landroid/media/SoundPool$OnLoadCompleteListener;

    #@2c
    move-result-object v0

    #@2d
    if-eqz v0, :cond_3e

    #@2f
    .line 450
    iget-object v0, p0, Landroid/media/SoundPool$EventHandler;->this$0:Landroid/media/SoundPool;

    #@31
    invoke-static {v0}, Landroid/media/SoundPool;->access$100(Landroid/media/SoundPool;)Landroid/media/SoundPool$OnLoadCompleteListener;

    #@34
    move-result-object v0

    #@35
    iget-object v2, p0, Landroid/media/SoundPool$EventHandler;->mSoundPool:Landroid/media/SoundPool;

    #@37
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@39
    iget v4, p1, Landroid/os/Message;->arg2:I

    #@3b
    invoke-interface {v0, v2, v3, v4}, Landroid/media/SoundPool$OnLoadCompleteListener;->onLoadComplete(Landroid/media/SoundPool;II)V

    #@3e
    .line 452
    :cond_3e
    monitor-exit v1

    #@3f
    goto :goto_1f

    #@40
    :catchall_40
    move-exception v0

    #@41
    monitor-exit v1
    :try_end_42
    .catchall {:try_start_27 .. :try_end_42} :catchall_40

    #@42
    throw v0

    #@43
    .line 445
    nop

    #@44
    :pswitch_data_44
    .packed-switch 0x1
        :pswitch_20
    .end packed-switch
.end method
