.class public Landroid/media/audiofx/Equalizer$Settings;
.super Ljava/lang/Object;
.source "Equalizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/audiofx/Equalizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Settings"
.end annotation


# instance fields
.field public bandLevels:[S

.field public curPreset:S

.field public numBands:S


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 459
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 456
    const/4 v0, 0x0

    #@4
    iput-short v0, p0, Landroid/media/audiofx/Equalizer$Settings;->numBands:S

    #@6
    .line 457
    const/4 v0, 0x0

    #@7
    iput-object v0, p0, Landroid/media/audiofx/Equalizer$Settings;->bandLevels:[S

    #@9
    .line 460
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 10
    .parameter "settings"

    #@0
    .prologue
    .line 467
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 456
    const/4 v5, 0x0

    #@4
    iput-short v5, p0, Landroid/media/audiofx/Equalizer$Settings;->numBands:S

    #@6
    .line 457
    const/4 v5, 0x0

    #@7
    iput-object v5, p0, Landroid/media/audiofx/Equalizer$Settings;->bandLevels:[S

    #@9
    .line 468
    new-instance v3, Ljava/util/StringTokenizer;

    #@b
    const-string v5, "=;"

    #@d
    invoke-direct {v3, p1, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@10
    .line 469
    .local v3, st:Ljava/util/StringTokenizer;
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->countTokens()I

    #@13
    move-result v4

    #@14
    .line 470
    .local v4, tokens:I
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->countTokens()I

    #@17
    move-result v5

    #@18
    const/4 v6, 0x5

    #@19
    if-ge v5, v6, :cond_35

    #@1b
    .line 471
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@1d
    new-instance v6, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string/jumbo v7, "settings: "

    #@25
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v6

    #@29
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v6

    #@2d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v6

    #@31
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@34
    throw v5

    #@35
    .line 473
    :cond_35
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@38
    move-result-object v1

    #@39
    .line 474
    .local v1, key:Ljava/lang/String;
    const-string v5, "Equalizer"

    #@3b
    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3e
    move-result v5

    #@3f
    if-nez v5, :cond_5a

    #@41
    .line 475
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@43
    new-instance v6, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v7, "invalid settings for Equalizer: "

    #@4a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v6

    #@4e
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v6

    #@52
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v6

    #@56
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@59
    throw v5

    #@5a
    .line 479
    :cond_5a
    :try_start_5a
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@5d
    move-result-object v1

    #@5e
    .line 480
    const-string v5, "curPreset"

    #@60
    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@63
    move-result v5

    #@64
    if-nez v5, :cond_99

    #@66
    .line 481
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@68
    new-instance v6, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const-string v7, "invalid key name: "

    #@6f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v6

    #@73
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v6

    #@77
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v6

    #@7b
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@7e
    throw v5
    :try_end_7f
    .catch Ljava/lang/NumberFormatException; {:try_start_5a .. :try_end_7f} :catch_7f

    #@7f
    .line 500
    :catch_7f
    move-exception v2

    #@80
    .line 501
    .local v2, nfe:Ljava/lang/NumberFormatException;
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@82
    new-instance v6, Ljava/lang/StringBuilder;

    #@84
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@87
    const-string v7, "invalid value for key: "

    #@89
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v6

    #@8d
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v6

    #@91
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v6

    #@95
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@98
    throw v5

    #@99
    .line 483
    .end local v2           #nfe:Ljava/lang/NumberFormatException;
    :cond_99
    :try_start_99
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@9c
    move-result-object v5

    #@9d
    invoke-static {v5}, Ljava/lang/Short;->parseShort(Ljava/lang/String;)S

    #@a0
    move-result v5

    #@a1
    iput-short v5, p0, Landroid/media/audiofx/Equalizer$Settings;->curPreset:S

    #@a3
    .line 484
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@a6
    move-result-object v1

    #@a7
    .line 485
    const-string/jumbo v5, "numBands"

    #@aa
    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ad
    move-result v5

    #@ae
    if-nez v5, :cond_c9

    #@b0
    .line 486
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@b2
    new-instance v6, Ljava/lang/StringBuilder;

    #@b4
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@b7
    const-string v7, "invalid key name: "

    #@b9
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v6

    #@bd
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v6

    #@c1
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c4
    move-result-object v6

    #@c5
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@c8
    throw v5

    #@c9
    .line 488
    :cond_c9
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@cc
    move-result-object v5

    #@cd
    invoke-static {v5}, Ljava/lang/Short;->parseShort(Ljava/lang/String;)S

    #@d0
    move-result v5

    #@d1
    iput-short v5, p0, Landroid/media/audiofx/Equalizer$Settings;->numBands:S

    #@d3
    .line 489
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->countTokens()I

    #@d6
    move-result v5

    #@d7
    iget-short v6, p0, Landroid/media/audiofx/Equalizer$Settings;->numBands:S

    #@d9
    mul-int/lit8 v6, v6, 0x2

    #@db
    if-eq v5, v6, :cond_f7

    #@dd
    .line 490
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@df
    new-instance v6, Ljava/lang/StringBuilder;

    #@e1
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@e4
    const-string/jumbo v7, "settings: "

    #@e7
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v6

    #@eb
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v6

    #@ef
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f2
    move-result-object v6

    #@f3
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f6
    throw v5

    #@f7
    .line 492
    :cond_f7
    iget-short v5, p0, Landroid/media/audiofx/Equalizer$Settings;->numBands:S

    #@f9
    new-array v5, v5, [S

    #@fb
    iput-object v5, p0, Landroid/media/audiofx/Equalizer$Settings;->bandLevels:[S

    #@fd
    .line 493
    const/4 v0, 0x0

    #@fe
    .local v0, i:I
    :goto_fe
    iget-short v5, p0, Landroid/media/audiofx/Equalizer$Settings;->numBands:S

    #@100
    if-ge v0, v5, :cond_14f

    #@102
    .line 494
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@105
    move-result-object v1

    #@106
    .line 495
    new-instance v5, Ljava/lang/StringBuilder;

    #@108
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@10b
    const-string v6, "band"

    #@10d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@110
    move-result-object v5

    #@111
    add-int/lit8 v6, v0, 0x1

    #@113
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@116
    move-result-object v5

    #@117
    const-string v6, "Level"

    #@119
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v5

    #@11d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@120
    move-result-object v5

    #@121
    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@124
    move-result v5

    #@125
    if-nez v5, :cond_140

    #@127
    .line 496
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@129
    new-instance v6, Ljava/lang/StringBuilder;

    #@12b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@12e
    const-string v7, "invalid key name: "

    #@130
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@133
    move-result-object v6

    #@134
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@137
    move-result-object v6

    #@138
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13b
    move-result-object v6

    #@13c
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@13f
    throw v5

    #@140
    .line 498
    :cond_140
    iget-object v5, p0, Landroid/media/audiofx/Equalizer$Settings;->bandLevels:[S

    #@142
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@145
    move-result-object v6

    #@146
    invoke-static {v6}, Ljava/lang/Short;->parseShort(Ljava/lang/String;)S

    #@149
    move-result v6

    #@14a
    aput-short v6, v5, v0
    :try_end_14c
    .catch Ljava/lang/NumberFormatException; {:try_start_99 .. :try_end_14c} :catch_7f

    #@14c
    .line 493
    add-int/lit8 v0, v0, 0x1

    #@14e
    goto :goto_fe

    #@14f
    .line 503
    :cond_14f
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 508
    new-instance v1, Ljava/lang/String;

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "Equalizer;curPreset="

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    iget-short v3, p0, Landroid/media/audiofx/Equalizer$Settings;->curPreset:S

    #@f
    invoke-static {v3}, Ljava/lang/Short;->toString(S)Ljava/lang/String;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    const-string v3, ";numBands="

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    iget-short v3, p0, Landroid/media/audiofx/Equalizer$Settings;->numBands:S

    #@1f
    invoke-static {v3}, Ljava/lang/Short;->toString(S)Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@2e
    .line 513
    .local v1, str:Ljava/lang/String;
    const/4 v0, 0x0

    #@2f
    .local v0, i:I
    :goto_2f
    iget-short v2, p0, Landroid/media/audiofx/Equalizer$Settings;->numBands:S

    #@31
    if-ge v0, v2, :cond_61

    #@33
    .line 514
    new-instance v2, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v3, ";band"

    #@3a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    add-int/lit8 v3, v0, 0x1

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    const-string v3, "Level="

    #@46
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v2

    #@4a
    iget-object v3, p0, Landroid/media/audiofx/Equalizer$Settings;->bandLevels:[S

    #@4c
    aget-short v3, v3, v0

    #@4e
    invoke-static {v3}, Ljava/lang/Short;->toString(S)Ljava/lang/String;

    #@51
    move-result-object v3

    #@52
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v2

    #@56
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v2

    #@5a
    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@5d
    move-result-object v1

    #@5e
    .line 513
    add-int/lit8 v0, v0, 0x1

    #@60
    goto :goto_2f

    #@61
    .line 516
    :cond_61
    return-object v1
.end method
