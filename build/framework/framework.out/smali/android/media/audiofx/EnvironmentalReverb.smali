.class public Landroid/media/audiofx/EnvironmentalReverb;
.super Landroid/media/audiofx/AudioEffect;
.source "EnvironmentalReverb.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/audiofx/EnvironmentalReverb$1;,
        Landroid/media/audiofx/EnvironmentalReverb$Settings;,
        Landroid/media/audiofx/EnvironmentalReverb$BaseParameterListener;,
        Landroid/media/audiofx/EnvironmentalReverb$OnParameterChangeListener;
    }
.end annotation


# static fields
.field public static final PARAM_DECAY_HF_RATIO:I = 0x3

.field public static final PARAM_DECAY_TIME:I = 0x2

.field public static final PARAM_DENSITY:I = 0x9

.field public static final PARAM_DIFFUSION:I = 0x8

.field private static final PARAM_PROPERTIES:I = 0xa

.field public static final PARAM_REFLECTIONS_DELAY:I = 0x5

.field public static final PARAM_REFLECTIONS_LEVEL:I = 0x4

.field public static final PARAM_REVERB_DELAY:I = 0x7

.field public static final PARAM_REVERB_LEVEL:I = 0x6

.field public static final PARAM_ROOM_HF_LEVEL:I = 0x1

.field public static final PARAM_ROOM_LEVEL:I = 0x0

.field private static PROPERTY_SIZE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "EnvironmentalReverb"


# instance fields
.field private mBaseParamListener:Landroid/media/audiofx/EnvironmentalReverb$BaseParameterListener;

.field private mParamListener:Landroid/media/audiofx/EnvironmentalReverb$OnParameterChangeListener;

.field private final mParamListenerLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 617
    const/16 v0, 0x1a

    #@2
    sput v0, Landroid/media/audiofx/EnvironmentalReverb;->PROPERTY_SIZE:I

    #@4
    return-void
.end method

.method public constructor <init>(II)V
    .registers 6
    .parameter "priority"
    .parameter "audioSession"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 146
    sget-object v0, Landroid/media/audiofx/EnvironmentalReverb;->EFFECT_TYPE_ENV_REVERB:Ljava/util/UUID;

    #@3
    sget-object v1, Landroid/media/audiofx/EnvironmentalReverb;->EFFECT_TYPE_NULL:Ljava/util/UUID;

    #@5
    invoke-direct {p0, v0, v1, p1, p2}, Landroid/media/audiofx/AudioEffect;-><init>(Ljava/util/UUID;Ljava/util/UUID;II)V

    #@8
    .line 115
    iput-object v2, p0, Landroid/media/audiofx/EnvironmentalReverb;->mParamListener:Landroid/media/audiofx/EnvironmentalReverb$OnParameterChangeListener;

    #@a
    .line 121
    iput-object v2, p0, Landroid/media/audiofx/EnvironmentalReverb;->mBaseParamListener:Landroid/media/audiofx/EnvironmentalReverb$BaseParameterListener;

    #@c
    .line 126
    new-instance v0, Ljava/lang/Object;

    #@e
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@11
    iput-object v0, p0, Landroid/media/audiofx/EnvironmentalReverb;->mParamListenerLock:Ljava/lang/Object;

    #@13
    .line 147
    return-void
.end method

.method static synthetic access$000(Landroid/media/audiofx/EnvironmentalReverb;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Landroid/media/audiofx/EnvironmentalReverb;->mParamListenerLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/media/audiofx/EnvironmentalReverb;)Landroid/media/audiofx/EnvironmentalReverb$OnParameterChangeListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Landroid/media/audiofx/EnvironmentalReverb;->mParamListener:Landroid/media/audiofx/EnvironmentalReverb$OnParameterChangeListener;

    #@2
    return-object v0
.end method


# virtual methods
.method public getDecayHFRatio()S
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    .line 256
    const/4 v1, 0x2

    #@1
    new-array v0, v1, [B

    #@3
    .line 257
    .local v0, param:[B
    const/4 v1, 0x3

    #@4
    invoke-virtual {p0, v1, v0}, Landroid/media/audiofx/EnvironmentalReverb;->getParameter(I[B)I

    #@7
    move-result v1

    #@8
    invoke-virtual {p0, v1}, Landroid/media/audiofx/EnvironmentalReverb;->checkStatus(I)V

    #@b
    .line 258
    invoke-virtual {p0, v0}, Landroid/media/audiofx/EnvironmentalReverb;->byteArrayToShort([B)S

    #@e
    move-result v1

    #@f
    return v1
.end method

.method public getDecayTime()I
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    .line 227
    const/4 v1, 0x4

    #@1
    new-array v0, v1, [B

    #@3
    .line 228
    .local v0, param:[B
    const/4 v1, 0x2

    #@4
    invoke-virtual {p0, v1, v0}, Landroid/media/audiofx/EnvironmentalReverb;->getParameter(I[B)I

    #@7
    move-result v1

    #@8
    invoke-virtual {p0, v1}, Landroid/media/audiofx/EnvironmentalReverb;->checkStatus(I)V

    #@b
    .line 229
    invoke-virtual {p0, v0}, Landroid/media/audiofx/EnvironmentalReverb;->byteArrayToInt([B)I

    #@e
    move-result v1

    #@f
    return v1
.end method

.method public getDensity()S
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    .line 432
    const/4 v1, 0x2

    #@1
    new-array v0, v1, [B

    #@3
    .line 433
    .local v0, param:[B
    const/16 v1, 0x9

    #@5
    invoke-virtual {p0, v1, v0}, Landroid/media/audiofx/EnvironmentalReverb;->getParameter(I[B)I

    #@8
    move-result v1

    #@9
    invoke-virtual {p0, v1}, Landroid/media/audiofx/EnvironmentalReverb;->checkStatus(I)V

    #@c
    .line 434
    invoke-virtual {p0, v0}, Landroid/media/audiofx/EnvironmentalReverb;->byteArrayToShort([B)S

    #@f
    move-result v1

    #@10
    return v1
.end method

.method public getDiffusion()S
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    .line 399
    const/4 v1, 0x2

    #@1
    new-array v0, v1, [B

    #@3
    .line 400
    .local v0, param:[B
    const/16 v1, 0x8

    #@5
    invoke-virtual {p0, v1, v0}, Landroid/media/audiofx/EnvironmentalReverb;->getParameter(I[B)I

    #@8
    move-result v1

    #@9
    invoke-virtual {p0, v1}, Landroid/media/audiofx/EnvironmentalReverb;->checkStatus(I)V

    #@c
    .line 401
    invoke-virtual {p0, v0}, Landroid/media/audiofx/EnvironmentalReverb;->byteArrayToShort([B)S

    #@f
    move-result v1

    #@10
    return v1
.end method

.method public getProperties()Landroid/media/audiofx/EnvironmentalReverb$Settings;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v3, 0xa

    #@2
    .line 629
    sget v2, Landroid/media/audiofx/EnvironmentalReverb;->PROPERTY_SIZE:I

    #@4
    new-array v0, v2, [B

    #@6
    .line 630
    .local v0, param:[B
    invoke-virtual {p0, v3, v0}, Landroid/media/audiofx/EnvironmentalReverb;->getParameter(I[B)I

    #@9
    move-result v2

    #@a
    invoke-virtual {p0, v2}, Landroid/media/audiofx/EnvironmentalReverb;->checkStatus(I)V

    #@d
    .line 631
    new-instance v1, Landroid/media/audiofx/EnvironmentalReverb$Settings;

    #@f
    invoke-direct {v1}, Landroid/media/audiofx/EnvironmentalReverb$Settings;-><init>()V

    #@12
    .line 632
    .local v1, settings:Landroid/media/audiofx/EnvironmentalReverb$Settings;
    const/4 v2, 0x0

    #@13
    invoke-virtual {p0, v0, v2}, Landroid/media/audiofx/EnvironmentalReverb;->byteArrayToShort([BI)S

    #@16
    move-result v2

    #@17
    iput-short v2, v1, Landroid/media/audiofx/EnvironmentalReverb$Settings;->roomLevel:S

    #@19
    .line 633
    const/4 v2, 0x2

    #@1a
    invoke-virtual {p0, v0, v2}, Landroid/media/audiofx/EnvironmentalReverb;->byteArrayToShort([BI)S

    #@1d
    move-result v2

    #@1e
    iput-short v2, v1, Landroid/media/audiofx/EnvironmentalReverb$Settings;->roomHFLevel:S

    #@20
    .line 634
    const/4 v2, 0x4

    #@21
    invoke-virtual {p0, v0, v2}, Landroid/media/audiofx/EnvironmentalReverb;->byteArrayToInt([BI)I

    #@24
    move-result v2

    #@25
    iput v2, v1, Landroid/media/audiofx/EnvironmentalReverb$Settings;->decayTime:I

    #@27
    .line 635
    const/16 v2, 0x8

    #@29
    invoke-virtual {p0, v0, v2}, Landroid/media/audiofx/EnvironmentalReverb;->byteArrayToShort([BI)S

    #@2c
    move-result v2

    #@2d
    iput-short v2, v1, Landroid/media/audiofx/EnvironmentalReverb$Settings;->decayHFRatio:S

    #@2f
    .line 636
    invoke-virtual {p0, v0, v3}, Landroid/media/audiofx/EnvironmentalReverb;->byteArrayToShort([BI)S

    #@32
    move-result v2

    #@33
    iput-short v2, v1, Landroid/media/audiofx/EnvironmentalReverb$Settings;->reflectionsLevel:S

    #@35
    .line 637
    const/16 v2, 0xc

    #@37
    invoke-virtual {p0, v0, v2}, Landroid/media/audiofx/EnvironmentalReverb;->byteArrayToInt([BI)I

    #@3a
    move-result v2

    #@3b
    iput v2, v1, Landroid/media/audiofx/EnvironmentalReverb$Settings;->reflectionsDelay:I

    #@3d
    .line 638
    const/16 v2, 0x10

    #@3f
    invoke-virtual {p0, v0, v2}, Landroid/media/audiofx/EnvironmentalReverb;->byteArrayToShort([BI)S

    #@42
    move-result v2

    #@43
    iput-short v2, v1, Landroid/media/audiofx/EnvironmentalReverb$Settings;->reverbLevel:S

    #@45
    .line 639
    const/16 v2, 0x12

    #@47
    invoke-virtual {p0, v0, v2}, Landroid/media/audiofx/EnvironmentalReverb;->byteArrayToInt([BI)I

    #@4a
    move-result v2

    #@4b
    iput v2, v1, Landroid/media/audiofx/EnvironmentalReverb$Settings;->reverbDelay:I

    #@4d
    .line 640
    const/16 v2, 0x16

    #@4f
    invoke-virtual {p0, v0, v2}, Landroid/media/audiofx/EnvironmentalReverb;->byteArrayToShort([BI)S

    #@52
    move-result v2

    #@53
    iput-short v2, v1, Landroid/media/audiofx/EnvironmentalReverb$Settings;->diffusion:S

    #@55
    .line 641
    const/16 v2, 0x18

    #@57
    invoke-virtual {p0, v0, v2}, Landroid/media/audiofx/EnvironmentalReverb;->byteArrayToShort([BI)S

    #@5a
    move-result v2

    #@5b
    iput-short v2, v1, Landroid/media/audiofx/EnvironmentalReverb$Settings;->density:S

    #@5d
    .line 642
    return-object v1
.end method

.method public getReflectionsDelay()I
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    .line 314
    const/4 v1, 0x4

    #@1
    new-array v0, v1, [B

    #@3
    .line 315
    .local v0, param:[B
    const/4 v1, 0x5

    #@4
    invoke-virtual {p0, v1, v0}, Landroid/media/audiofx/EnvironmentalReverb;->getParameter(I[B)I

    #@7
    move-result v1

    #@8
    invoke-virtual {p0, v1}, Landroid/media/audiofx/EnvironmentalReverb;->checkStatus(I)V

    #@b
    .line 316
    invoke-virtual {p0, v0}, Landroid/media/audiofx/EnvironmentalReverb;->byteArrayToInt([B)I

    #@e
    move-result v1

    #@f
    return v1
.end method

.method public getReflectionsLevel()S
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    .line 285
    const/4 v1, 0x2

    #@1
    new-array v0, v1, [B

    #@3
    .line 286
    .local v0, param:[B
    const/4 v1, 0x4

    #@4
    invoke-virtual {p0, v1, v0}, Landroid/media/audiofx/EnvironmentalReverb;->getParameter(I[B)I

    #@7
    move-result v1

    #@8
    invoke-virtual {p0, v1}, Landroid/media/audiofx/EnvironmentalReverb;->checkStatus(I)V

    #@b
    .line 287
    invoke-virtual {p0, v0}, Landroid/media/audiofx/EnvironmentalReverb;->byteArrayToShort([B)S

    #@e
    move-result v1

    #@f
    return v1
.end method

.method public getReverbDelay()I
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    .line 369
    const/4 v1, 0x4

    #@1
    new-array v0, v1, [B

    #@3
    .line 370
    .local v0, param:[B
    const/4 v1, 0x7

    #@4
    invoke-virtual {p0, v1, v0}, Landroid/media/audiofx/EnvironmentalReverb;->getParameter(I[B)I

    #@7
    move-result v1

    #@8
    invoke-virtual {p0, v1}, Landroid/media/audiofx/EnvironmentalReverb;->checkStatus(I)V

    #@b
    .line 371
    invoke-virtual {p0, v0}, Landroid/media/audiofx/EnvironmentalReverb;->byteArrayToInt([B)I

    #@e
    move-result v1

    #@f
    return v1
.end method

.method public getReverbLevel()S
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    .line 342
    const/4 v1, 0x2

    #@1
    new-array v0, v1, [B

    #@3
    .line 343
    .local v0, param:[B
    const/4 v1, 0x6

    #@4
    invoke-virtual {p0, v1, v0}, Landroid/media/audiofx/EnvironmentalReverb;->getParameter(I[B)I

    #@7
    move-result v1

    #@8
    invoke-virtual {p0, v1}, Landroid/media/audiofx/EnvironmentalReverb;->checkStatus(I)V

    #@b
    .line 344
    invoke-virtual {p0, v0}, Landroid/media/audiofx/EnvironmentalReverb;->byteArrayToShort([B)S

    #@e
    move-result v1

    #@f
    return v1
.end method

.method public getRoomHFLevel()S
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    .line 200
    const/4 v1, 0x2

    #@1
    new-array v0, v1, [B

    #@3
    .line 201
    .local v0, param:[B
    const/4 v1, 0x1

    #@4
    invoke-virtual {p0, v1, v0}, Landroid/media/audiofx/EnvironmentalReverb;->getParameter(I[B)I

    #@7
    move-result v1

    #@8
    invoke-virtual {p0, v1}, Landroid/media/audiofx/EnvironmentalReverb;->checkStatus(I)V

    #@b
    .line 202
    invoke-virtual {p0, v0}, Landroid/media/audiofx/EnvironmentalReverb;->byteArrayToShort([B)S

    #@e
    move-result v1

    #@f
    return v1
.end method

.method public getRoomLevel()S
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    .line 171
    const/4 v1, 0x2

    #@1
    new-array v0, v1, [B

    #@3
    .line 172
    .local v0, param:[B
    const/4 v1, 0x0

    #@4
    invoke-virtual {p0, v1, v0}, Landroid/media/audiofx/EnvironmentalReverb;->getParameter(I[B)I

    #@7
    move-result v1

    #@8
    invoke-virtual {p0, v1}, Landroid/media/audiofx/EnvironmentalReverb;->checkStatus(I)V

    #@b
    .line 173
    invoke-virtual {p0, v0}, Landroid/media/audiofx/EnvironmentalReverb;->byteArrayToShort([B)S

    #@e
    move-result v1

    #@f
    return v1
.end method

.method public setDecayHFRatio(S)V
    .registers 4
    .parameter "decayHFRatio"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    .line 243
    invoke-virtual {p0, p1}, Landroid/media/audiofx/EnvironmentalReverb;->shortToByteArray(S)[B

    #@3
    move-result-object v0

    #@4
    .line 244
    .local v0, param:[B
    const/4 v1, 0x3

    #@5
    invoke-virtual {p0, v1, v0}, Landroid/media/audiofx/EnvironmentalReverb;->setParameter(I[B)I

    #@8
    move-result v1

    #@9
    invoke-virtual {p0, v1}, Landroid/media/audiofx/EnvironmentalReverb;->checkStatus(I)V

    #@c
    .line 245
    return-void
.end method

.method public setDecayTime(I)V
    .registers 4
    .parameter "decayTime"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    .line 214
    invoke-virtual {p0, p1}, Landroid/media/audiofx/EnvironmentalReverb;->intToByteArray(I)[B

    #@3
    move-result-object v0

    #@4
    .line 215
    .local v0, param:[B
    const/4 v1, 0x2

    #@5
    invoke-virtual {p0, v1, v0}, Landroid/media/audiofx/EnvironmentalReverb;->setParameter(I[B)I

    #@8
    move-result v1

    #@9
    invoke-virtual {p0, v1}, Landroid/media/audiofx/EnvironmentalReverb;->checkStatus(I)V

    #@c
    .line 216
    return-void
.end method

.method public setDensity(S)V
    .registers 4
    .parameter "density"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    .line 419
    invoke-virtual {p0, p1}, Landroid/media/audiofx/EnvironmentalReverb;->shortToByteArray(S)[B

    #@3
    move-result-object v0

    #@4
    .line 420
    .local v0, param:[B
    const/16 v1, 0x9

    #@6
    invoke-virtual {p0, v1, v0}, Landroid/media/audiofx/EnvironmentalReverb;->setParameter(I[B)I

    #@9
    move-result v1

    #@a
    invoke-virtual {p0, v1}, Landroid/media/audiofx/EnvironmentalReverb;->checkStatus(I)V

    #@d
    .line 421
    return-void
.end method

.method public setDiffusion(S)V
    .registers 4
    .parameter "diffusion"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    .line 386
    invoke-virtual {p0, p1}, Landroid/media/audiofx/EnvironmentalReverb;->shortToByteArray(S)[B

    #@3
    move-result-object v0

    #@4
    .line 387
    .local v0, param:[B
    const/16 v1, 0x8

    #@6
    invoke-virtual {p0, v1, v0}, Landroid/media/audiofx/EnvironmentalReverb;->setParameter(I[B)I

    #@9
    move-result v1

    #@a
    invoke-virtual {p0, v1}, Landroid/media/audiofx/EnvironmentalReverb;->checkStatus(I)V

    #@d
    .line 388
    return-void
.end method

.method public setParameterListener(Landroid/media/audiofx/EnvironmentalReverb$OnParameterChangeListener;)V
    .registers 5
    .parameter "listener"

    #@0
    .prologue
    .line 495
    iget-object v1, p0, Landroid/media/audiofx/EnvironmentalReverb;->mParamListenerLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 496
    :try_start_3
    iget-object v0, p0, Landroid/media/audiofx/EnvironmentalReverb;->mParamListener:Landroid/media/audiofx/EnvironmentalReverb$OnParameterChangeListener;

    #@5
    if-nez v0, :cond_16

    #@7
    .line 497
    iput-object p1, p0, Landroid/media/audiofx/EnvironmentalReverb;->mParamListener:Landroid/media/audiofx/EnvironmentalReverb$OnParameterChangeListener;

    #@9
    .line 498
    new-instance v0, Landroid/media/audiofx/EnvironmentalReverb$BaseParameterListener;

    #@b
    const/4 v2, 0x0

    #@c
    invoke-direct {v0, p0, v2}, Landroid/media/audiofx/EnvironmentalReverb$BaseParameterListener;-><init>(Landroid/media/audiofx/EnvironmentalReverb;Landroid/media/audiofx/EnvironmentalReverb$1;)V

    #@f
    iput-object v0, p0, Landroid/media/audiofx/EnvironmentalReverb;->mBaseParamListener:Landroid/media/audiofx/EnvironmentalReverb$BaseParameterListener;

    #@11
    .line 499
    iget-object v0, p0, Landroid/media/audiofx/EnvironmentalReverb;->mBaseParamListener:Landroid/media/audiofx/EnvironmentalReverb$BaseParameterListener;

    #@13
    invoke-super {p0, v0}, Landroid/media/audiofx/AudioEffect;->setParameterListener(Landroid/media/audiofx/AudioEffect$OnParameterChangeListener;)V

    #@16
    .line 501
    :cond_16
    monitor-exit v1

    #@17
    .line 502
    return-void

    #@18
    .line 501
    :catchall_18
    move-exception v0

    #@19
    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_3 .. :try_end_1a} :catchall_18

    #@1a
    throw v0
.end method

.method public setProperties(Landroid/media/audiofx/EnvironmentalReverb$Settings;)V
    .registers 7
    .parameter "settings"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v4, 0xa

    #@2
    .line 656
    new-array v1, v4, [[B

    #@4
    const/4 v2, 0x0

    #@5
    iget-short v3, p1, Landroid/media/audiofx/EnvironmentalReverb$Settings;->roomLevel:S

    #@7
    invoke-virtual {p0, v3}, Landroid/media/audiofx/EnvironmentalReverb;->shortToByteArray(S)[B

    #@a
    move-result-object v3

    #@b
    aput-object v3, v1, v2

    #@d
    const/4 v2, 0x1

    #@e
    iget-short v3, p1, Landroid/media/audiofx/EnvironmentalReverb$Settings;->roomHFLevel:S

    #@10
    invoke-virtual {p0, v3}, Landroid/media/audiofx/EnvironmentalReverb;->shortToByteArray(S)[B

    #@13
    move-result-object v3

    #@14
    aput-object v3, v1, v2

    #@16
    const/4 v2, 0x2

    #@17
    iget v3, p1, Landroid/media/audiofx/EnvironmentalReverb$Settings;->decayTime:I

    #@19
    invoke-virtual {p0, v3}, Landroid/media/audiofx/EnvironmentalReverb;->intToByteArray(I)[B

    #@1c
    move-result-object v3

    #@1d
    aput-object v3, v1, v2

    #@1f
    const/4 v2, 0x3

    #@20
    iget-short v3, p1, Landroid/media/audiofx/EnvironmentalReverb$Settings;->decayHFRatio:S

    #@22
    invoke-virtual {p0, v3}, Landroid/media/audiofx/EnvironmentalReverb;->shortToByteArray(S)[B

    #@25
    move-result-object v3

    #@26
    aput-object v3, v1, v2

    #@28
    const/4 v2, 0x4

    #@29
    iget-short v3, p1, Landroid/media/audiofx/EnvironmentalReverb$Settings;->reflectionsLevel:S

    #@2b
    invoke-virtual {p0, v3}, Landroid/media/audiofx/EnvironmentalReverb;->shortToByteArray(S)[B

    #@2e
    move-result-object v3

    #@2f
    aput-object v3, v1, v2

    #@31
    const/4 v2, 0x5

    #@32
    iget v3, p1, Landroid/media/audiofx/EnvironmentalReverb$Settings;->reflectionsDelay:I

    #@34
    invoke-virtual {p0, v3}, Landroid/media/audiofx/EnvironmentalReverb;->intToByteArray(I)[B

    #@37
    move-result-object v3

    #@38
    aput-object v3, v1, v2

    #@3a
    const/4 v2, 0x6

    #@3b
    iget-short v3, p1, Landroid/media/audiofx/EnvironmentalReverb$Settings;->reverbLevel:S

    #@3d
    invoke-virtual {p0, v3}, Landroid/media/audiofx/EnvironmentalReverb;->shortToByteArray(S)[B

    #@40
    move-result-object v3

    #@41
    aput-object v3, v1, v2

    #@43
    const/4 v2, 0x7

    #@44
    iget v3, p1, Landroid/media/audiofx/EnvironmentalReverb$Settings;->reverbDelay:I

    #@46
    invoke-virtual {p0, v3}, Landroid/media/audiofx/EnvironmentalReverb;->intToByteArray(I)[B

    #@49
    move-result-object v3

    #@4a
    aput-object v3, v1, v2

    #@4c
    const/16 v2, 0x8

    #@4e
    iget-short v3, p1, Landroid/media/audiofx/EnvironmentalReverb$Settings;->diffusion:S

    #@50
    invoke-virtual {p0, v3}, Landroid/media/audiofx/EnvironmentalReverb;->shortToByteArray(S)[B

    #@53
    move-result-object v3

    #@54
    aput-object v3, v1, v2

    #@56
    const/16 v2, 0x9

    #@58
    iget-short v3, p1, Landroid/media/audiofx/EnvironmentalReverb$Settings;->density:S

    #@5a
    invoke-virtual {p0, v3}, Landroid/media/audiofx/EnvironmentalReverb;->shortToByteArray(S)[B

    #@5d
    move-result-object v3

    #@5e
    aput-object v3, v1, v2

    #@60
    invoke-virtual {p0, v1}, Landroid/media/audiofx/EnvironmentalReverb;->concatArrays([[B)[B

    #@63
    move-result-object v0

    #@64
    .line 667
    .local v0, param:[B
    invoke-virtual {p0, v4, v0}, Landroid/media/audiofx/EnvironmentalReverb;->setParameter(I[B)I

    #@67
    move-result v1

    #@68
    invoke-virtual {p0, v1}, Landroid/media/audiofx/EnvironmentalReverb;->checkStatus(I)V

    #@6b
    .line 668
    return-void
.end method

.method public setReflectionsDelay(I)V
    .registers 4
    .parameter "reflectionsDelay"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    .line 301
    invoke-virtual {p0, p1}, Landroid/media/audiofx/EnvironmentalReverb;->intToByteArray(I)[B

    #@3
    move-result-object v0

    #@4
    .line 302
    .local v0, param:[B
    const/4 v1, 0x5

    #@5
    invoke-virtual {p0, v1, v0}, Landroid/media/audiofx/EnvironmentalReverb;->setParameter(I[B)I

    #@8
    move-result v1

    #@9
    invoke-virtual {p0, v1}, Landroid/media/audiofx/EnvironmentalReverb;->checkStatus(I)V

    #@c
    .line 303
    return-void
.end method

.method public setReflectionsLevel(S)V
    .registers 4
    .parameter "reflectionsLevel"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    .line 272
    invoke-virtual {p0, p1}, Landroid/media/audiofx/EnvironmentalReverb;->shortToByteArray(S)[B

    #@3
    move-result-object v0

    #@4
    .line 273
    .local v0, param:[B
    const/4 v1, 0x4

    #@5
    invoke-virtual {p0, v1, v0}, Landroid/media/audiofx/EnvironmentalReverb;->setParameter(I[B)I

    #@8
    move-result v1

    #@9
    invoke-virtual {p0, v1}, Landroid/media/audiofx/EnvironmentalReverb;->checkStatus(I)V

    #@c
    .line 274
    return-void
.end method

.method public setReverbDelay(I)V
    .registers 4
    .parameter "reverbDelay"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    .line 356
    invoke-virtual {p0, p1}, Landroid/media/audiofx/EnvironmentalReverb;->intToByteArray(I)[B

    #@3
    move-result-object v0

    #@4
    .line 357
    .local v0, param:[B
    const/4 v1, 0x7

    #@5
    invoke-virtual {p0, v1, v0}, Landroid/media/audiofx/EnvironmentalReverb;->setParameter(I[B)I

    #@8
    move-result v1

    #@9
    invoke-virtual {p0, v1}, Landroid/media/audiofx/EnvironmentalReverb;->checkStatus(I)V

    #@c
    .line 358
    return-void
.end method

.method public setReverbLevel(S)V
    .registers 4
    .parameter "reverbLevel"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    .line 329
    invoke-virtual {p0, p1}, Landroid/media/audiofx/EnvironmentalReverb;->shortToByteArray(S)[B

    #@3
    move-result-object v0

    #@4
    .line 330
    .local v0, param:[B
    const/4 v1, 0x6

    #@5
    invoke-virtual {p0, v1, v0}, Landroid/media/audiofx/EnvironmentalReverb;->setParameter(I[B)I

    #@8
    move-result v1

    #@9
    invoke-virtual {p0, v1}, Landroid/media/audiofx/EnvironmentalReverb;->checkStatus(I)V

    #@c
    .line 331
    return-void
.end method

.method public setRoomHFLevel(S)V
    .registers 4
    .parameter "roomHF"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    .line 187
    invoke-virtual {p0, p1}, Landroid/media/audiofx/EnvironmentalReverb;->shortToByteArray(S)[B

    #@3
    move-result-object v0

    #@4
    .line 188
    .local v0, param:[B
    const/4 v1, 0x1

    #@5
    invoke-virtual {p0, v1, v0}, Landroid/media/audiofx/EnvironmentalReverb;->setParameter(I[B)I

    #@8
    move-result v1

    #@9
    invoke-virtual {p0, v1}, Landroid/media/audiofx/EnvironmentalReverb;->checkStatus(I)V

    #@c
    .line 189
    return-void
.end method

.method public setRoomLevel(S)V
    .registers 4
    .parameter "room"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    .line 158
    invoke-virtual {p0, p1}, Landroid/media/audiofx/EnvironmentalReverb;->shortToByteArray(S)[B

    #@3
    move-result-object v0

    #@4
    .line 159
    .local v0, param:[B
    const/4 v1, 0x0

    #@5
    invoke-virtual {p0, v1, v0}, Landroid/media/audiofx/EnvironmentalReverb;->setParameter(I[B)I

    #@8
    move-result v1

    #@9
    invoke-virtual {p0, v1}, Landroid/media/audiofx/EnvironmentalReverb;->checkStatus(I)V

    #@c
    .line 160
    return-void
.end method
