.class public Landroid/media/audiofx/Equalizer;
.super Landroid/media/audiofx/AudioEffect;
.source "Equalizer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/audiofx/Equalizer$1;,
        Landroid/media/audiofx/Equalizer$Settings;,
        Landroid/media/audiofx/Equalizer$BaseParameterListener;,
        Landroid/media/audiofx/Equalizer$OnParameterChangeListener;
    }
.end annotation


# static fields
.field public static final PARAM_BAND_FREQ_RANGE:I = 0x4

.field public static final PARAM_BAND_LEVEL:I = 0x2

.field public static final PARAM_CENTER_FREQ:I = 0x3

.field public static final PARAM_CURRENT_PRESET:I = 0x6

.field public static final PARAM_GET_BAND:I = 0x5

.field public static final PARAM_GET_NUM_OF_PRESETS:I = 0x7

.field public static final PARAM_GET_PRESET_NAME:I = 0x8

.field public static final PARAM_LEVEL_RANGE:I = 0x1

.field public static final PARAM_NUM_BANDS:I = 0x0

.field private static final PARAM_PROPERTIES:I = 0x9

.field public static final PARAM_STRING_SIZE_MAX:I = 0x20

.field private static final TAG:Ljava/lang/String; = "Equalizer"


# instance fields
.field private mBaseParamListener:Landroid/media/audiofx/Equalizer$BaseParameterListener;

.field private mNumBands:S

.field private mNumPresets:I

.field private mParamListener:Landroid/media/audiofx/Equalizer$OnParameterChangeListener;

.field private final mParamListenerLock:Ljava/lang/Object;

.field private mPresetNames:[Ljava/lang/String;


# direct methods
.method public constructor <init>(II)V
    .registers 12
    .parameter "priority"
    .parameter "audioSession"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v7, 0x0

    #@2
    .line 146
    sget-object v5, Landroid/media/audiofx/Equalizer;->EFFECT_TYPE_EQUALIZER:Ljava/util/UUID;

    #@4
    sget-object v6, Landroid/media/audiofx/Equalizer;->EFFECT_TYPE_NULL:Ljava/util/UUID;

    #@6
    invoke-direct {p0, v5, v6, p1, p2}, Landroid/media/audiofx/AudioEffect;-><init>(Ljava/util/UUID;Ljava/util/UUID;II)V

    #@9
    .line 103
    iput-short v7, p0, Landroid/media/audiofx/Equalizer;->mNumBands:S

    #@b
    .line 117
    iput-object v8, p0, Landroid/media/audiofx/Equalizer;->mParamListener:Landroid/media/audiofx/Equalizer$OnParameterChangeListener;

    #@d
    .line 122
    iput-object v8, p0, Landroid/media/audiofx/Equalizer;->mBaseParamListener:Landroid/media/audiofx/Equalizer$BaseParameterListener;

    #@f
    .line 127
    new-instance v5, Ljava/lang/Object;

    #@11
    invoke-direct/range {v5 .. v5}, Ljava/lang/Object;-><init>()V

    #@14
    iput-object v5, p0, Landroid/media/audiofx/Equalizer;->mParamListenerLock:Ljava/lang/Object;

    #@16
    .line 148
    if-nez p2, :cond_1f

    #@18
    .line 149
    const-string v5, "Equalizer"

    #@1a
    const-string v6, "WARNING: attaching an Equalizer to global output mix is deprecated!"

    #@1c
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 152
    :cond_1f
    invoke-virtual {p0}, Landroid/media/audiofx/Equalizer;->getNumberOfBands()S

    #@22
    .line 154
    invoke-virtual {p0}, Landroid/media/audiofx/Equalizer;->getNumberOfPresets()S

    #@25
    move-result v5

    #@26
    iput v5, p0, Landroid/media/audiofx/Equalizer;->mNumPresets:I

    #@28
    .line 156
    iget v5, p0, Landroid/media/audiofx/Equalizer;->mNumPresets:I

    #@2a
    if-eqz v5, :cond_6d

    #@2c
    .line 157
    iget v5, p0, Landroid/media/audiofx/Equalizer;->mNumPresets:I

    #@2e
    new-array v5, v5, [Ljava/lang/String;

    #@30
    iput-object v5, p0, Landroid/media/audiofx/Equalizer;->mPresetNames:[Ljava/lang/String;

    #@32
    .line 158
    const/16 v5, 0x20

    #@34
    new-array v4, v5, [B

    #@36
    .line 159
    .local v4, value:[B
    const/4 v5, 0x2

    #@37
    new-array v3, v5, [I

    #@39
    .line 160
    .local v3, param:[I
    const/16 v5, 0x8

    #@3b
    aput v5, v3, v7

    #@3d
    .line 161
    const/4 v1, 0x0

    #@3e
    .local v1, i:I
    :goto_3e
    iget v5, p0, Landroid/media/audiofx/Equalizer;->mNumPresets:I

    #@40
    if-ge v1, v5, :cond_6d

    #@42
    .line 162
    const/4 v5, 0x1

    #@43
    aput v1, v3, v5

    #@45
    .line 163
    invoke-virtual {p0, v3, v4}, Landroid/media/audiofx/Equalizer;->getParameter([I[B)I

    #@48
    move-result v5

    #@49
    invoke-virtual {p0, v5}, Landroid/media/audiofx/Equalizer;->checkStatus(I)V

    #@4c
    .line 164
    const/4 v2, 0x0

    #@4d
    .line 165
    .local v2, length:I
    :goto_4d
    aget-byte v5, v4, v2

    #@4f
    if-eqz v5, :cond_54

    #@51
    add-int/lit8 v2, v2, 0x1

    #@53
    goto :goto_4d

    #@54
    .line 167
    :cond_54
    :try_start_54
    iget-object v5, p0, Landroid/media/audiofx/Equalizer;->mPresetNames:[Ljava/lang/String;

    #@56
    new-instance v6, Ljava/lang/String;

    #@58
    const/4 v7, 0x0

    #@59
    const-string v8, "ISO-8859-1"

    #@5b
    invoke-direct {v6, v4, v7, v2, v8}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    #@5e
    aput-object v6, v5, v1
    :try_end_60
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_54 .. :try_end_60} :catch_63

    #@60
    .line 161
    :goto_60
    add-int/lit8 v1, v1, 0x1

    #@62
    goto :goto_3e

    #@63
    .line 168
    :catch_63
    move-exception v0

    #@64
    .line 169
    .local v0, e:Ljava/io/UnsupportedEncodingException;
    const-string v5, "Equalizer"

    #@66
    const-string/jumbo v6, "preset name decode error"

    #@69
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6c
    goto :goto_60

    #@6d
    .line 173
    .end local v0           #e:Ljava/io/UnsupportedEncodingException;
    .end local v1           #i:I
    .end local v2           #length:I
    .end local v3           #param:[I
    .end local v4           #value:[B
    :cond_6d
    return-void
.end method

.method static synthetic access$000(Landroid/media/audiofx/Equalizer;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget-object v0, p0, Landroid/media/audiofx/Equalizer;->mParamListenerLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/media/audiofx/Equalizer;)Landroid/media/audiofx/Equalizer$OnParameterChangeListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget-object v0, p0, Landroid/media/audiofx/Equalizer;->mParamListener:Landroid/media/audiofx/Equalizer$OnParameterChangeListener;

    #@2
    return-object v0
.end method


# virtual methods
.method public getBand(I)S
    .registers 7
    .parameter "frequency"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 307
    const/4 v2, 0x2

    #@3
    new-array v0, v2, [I

    #@5
    .line 308
    .local v0, param:[I
    new-array v1, v4, [S

    #@7
    .line 310
    .local v1, result:[S
    const/4 v2, 0x5

    #@8
    aput v2, v0, v3

    #@a
    .line 311
    aput p1, v0, v4

    #@c
    .line 312
    invoke-virtual {p0, v0, v1}, Landroid/media/audiofx/Equalizer;->getParameter([I[S)I

    #@f
    move-result v2

    #@10
    invoke-virtual {p0, v2}, Landroid/media/audiofx/Equalizer;->checkStatus(I)V

    #@13
    .line 314
    aget-short v2, v1, v3

    #@15
    return v2
.end method

.method public getBandFreqRange(S)[I
    .registers 6
    .parameter "band"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    .line 288
    new-array v0, v2, [I

    #@3
    .line 289
    .local v0, param:[I
    new-array v1, v2, [I

    #@5
    .line 290
    .local v1, result:[I
    const/4 v2, 0x0

    #@6
    const/4 v3, 0x4

    #@7
    aput v3, v0, v2

    #@9
    .line 291
    const/4 v2, 0x1

    #@a
    aput p1, v0, v2

    #@c
    .line 292
    invoke-virtual {p0, v0, v1}, Landroid/media/audiofx/Equalizer;->getParameter([I[I)I

    #@f
    move-result v2

    #@10
    invoke-virtual {p0, v2}, Landroid/media/audiofx/Equalizer;->checkStatus(I)V

    #@13
    .line 294
    return-object v1
.end method

.method public getBandLevel(S)S
    .registers 7
    .parameter "band"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v2, 0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 244
    new-array v0, v4, [I

    #@5
    .line 245
    .local v0, param:[I
    new-array v1, v2, [S

    #@7
    .line 247
    .local v1, result:[S
    aput v4, v0, v3

    #@9
    .line 248
    aput p1, v0, v2

    #@b
    .line 249
    invoke-virtual {p0, v0, v1}, Landroid/media/audiofx/Equalizer;->getParameter([I[S)I

    #@e
    move-result v2

    #@f
    invoke-virtual {p0, v2}, Landroid/media/audiofx/Equalizer;->checkStatus(I)V

    #@12
    .line 251
    aget-short v2, v1, v3

    #@14
    return v2
.end method

.method public getBandLevelRange()[S
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    .line 206
    const/4 v1, 0x2

    #@1
    new-array v0, v1, [S

    #@3
    .line 207
    .local v0, result:[S
    const/4 v1, 0x1

    #@4
    invoke-virtual {p0, v1, v0}, Landroid/media/audiofx/Equalizer;->getParameter(I[S)I

    #@7
    move-result v1

    #@8
    invoke-virtual {p0, v1}, Landroid/media/audiofx/Equalizer;->checkStatus(I)V

    #@b
    .line 208
    return-object v0
.end method

.method public getCenterFreq(S)I
    .registers 7
    .parameter "band"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 266
    const/4 v2, 0x2

    #@3
    new-array v0, v2, [I

    #@5
    .line 267
    .local v0, param:[I
    new-array v1, v4, [I

    #@7
    .line 269
    .local v1, result:[I
    const/4 v2, 0x3

    #@8
    aput v2, v0, v3

    #@a
    .line 270
    aput p1, v0, v4

    #@c
    .line 271
    invoke-virtual {p0, v0, v1}, Landroid/media/audiofx/Equalizer;->getParameter([I[I)I

    #@f
    move-result v2

    #@10
    invoke-virtual {p0, v2}, Landroid/media/audiofx/Equalizer;->checkStatus(I)V

    #@13
    .line 273
    aget v2, v1, v3

    #@15
    return v2
.end method

.method public getCurrentPreset()S
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    .line 326
    const/4 v1, 0x1

    #@1
    new-array v0, v1, [S

    #@3
    .line 327
    .local v0, result:[S
    const/4 v1, 0x6

    #@4
    invoke-virtual {p0, v1, v0}, Landroid/media/audiofx/Equalizer;->getParameter(I[S)I

    #@7
    move-result v1

    #@8
    invoke-virtual {p0, v1}, Landroid/media/audiofx/Equalizer;->checkStatus(I)V

    #@b
    .line 328
    const/4 v1, 0x0

    #@c
    aget-short v1, v0, v1

    #@e
    return v1
.end method

.method public getNumberOfBands()S
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 184
    iget-short v2, p0, Landroid/media/audiofx/Equalizer;->mNumBands:S

    #@4
    if-eqz v2, :cond_9

    #@6
    .line 185
    iget-short v2, p0, Landroid/media/audiofx/Equalizer;->mNumBands:S

    #@8
    .line 192
    :goto_8
    return v2

    #@9
    .line 187
    :cond_9
    new-array v0, v4, [I

    #@b
    .line 188
    .local v0, param:[I
    aput v3, v0, v3

    #@d
    .line 189
    new-array v1, v4, [S

    #@f
    .line 190
    .local v1, result:[S
    invoke-virtual {p0, v0, v1}, Landroid/media/audiofx/Equalizer;->getParameter([I[S)I

    #@12
    move-result v2

    #@13
    invoke-virtual {p0, v2}, Landroid/media/audiofx/Equalizer;->checkStatus(I)V

    #@16
    .line 191
    aget-short v2, v1, v3

    #@18
    iput-short v2, p0, Landroid/media/audiofx/Equalizer;->mNumBands:S

    #@1a
    .line 192
    iget-short v2, p0, Landroid/media/audiofx/Equalizer;->mNumBands:S

    #@1c
    goto :goto_8
.end method

.method public getNumberOfPresets()S
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    .line 355
    const/4 v1, 0x1

    #@1
    new-array v0, v1, [S

    #@3
    .line 356
    .local v0, result:[S
    const/4 v1, 0x7

    #@4
    invoke-virtual {p0, v1, v0}, Landroid/media/audiofx/Equalizer;->getParameter(I[S)I

    #@7
    move-result v1

    #@8
    invoke-virtual {p0, v1}, Landroid/media/audiofx/Equalizer;->checkStatus(I)V

    #@b
    .line 357
    const/4 v1, 0x0

    #@c
    aget-short v1, v0, v1

    #@e
    return v1
.end method

.method public getPresetName(S)Ljava/lang/String;
    .registers 3
    .parameter "preset"

    #@0
    .prologue
    .line 370
    if-ltz p1, :cond_b

    #@2
    iget v0, p0, Landroid/media/audiofx/Equalizer;->mNumPresets:I

    #@4
    if-ge p1, v0, :cond_b

    #@6
    .line 371
    iget-object v0, p0, Landroid/media/audiofx/Equalizer;->mPresetNames:[Ljava/lang/String;

    #@8
    aget-object v0, v0, p1

    #@a
    .line 373
    :goto_a
    return-object v0

    #@b
    :cond_b
    const-string v0, ""

    #@d
    goto :goto_a
.end method

.method public getProperties()Landroid/media/audiofx/Equalizer$Settings;
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    .line 531
    iget-short v3, p0, Landroid/media/audiofx/Equalizer;->mNumBands:S

    #@2
    mul-int/lit8 v3, v3, 0x2

    #@4
    add-int/lit8 v3, v3, 0x4

    #@6
    new-array v1, v3, [B

    #@8
    .line 532
    .local v1, param:[B
    const/16 v3, 0x9

    #@a
    invoke-virtual {p0, v3, v1}, Landroid/media/audiofx/Equalizer;->getParameter(I[B)I

    #@d
    move-result v3

    #@e
    invoke-virtual {p0, v3}, Landroid/media/audiofx/Equalizer;->checkStatus(I)V

    #@11
    .line 533
    new-instance v2, Landroid/media/audiofx/Equalizer$Settings;

    #@13
    invoke-direct {v2}, Landroid/media/audiofx/Equalizer$Settings;-><init>()V

    #@16
    .line 534
    .local v2, settings:Landroid/media/audiofx/Equalizer$Settings;
    const/4 v3, 0x0

    #@17
    invoke-virtual {p0, v1, v3}, Landroid/media/audiofx/Equalizer;->byteArrayToShort([BI)S

    #@1a
    move-result v3

    #@1b
    iput-short v3, v2, Landroid/media/audiofx/Equalizer$Settings;->curPreset:S

    #@1d
    .line 535
    const/4 v3, 0x2

    #@1e
    invoke-virtual {p0, v1, v3}, Landroid/media/audiofx/Equalizer;->byteArrayToShort([BI)S

    #@21
    move-result v3

    #@22
    iput-short v3, v2, Landroid/media/audiofx/Equalizer$Settings;->numBands:S

    #@24
    .line 536
    iget-short v3, p0, Landroid/media/audiofx/Equalizer;->mNumBands:S

    #@26
    new-array v3, v3, [S

    #@28
    iput-object v3, v2, Landroid/media/audiofx/Equalizer$Settings;->bandLevels:[S

    #@2a
    .line 537
    const/4 v0, 0x0

    #@2b
    .local v0, i:I
    :goto_2b
    iget-short v3, p0, Landroid/media/audiofx/Equalizer;->mNumBands:S

    #@2d
    if-ge v0, v3, :cond_3e

    #@2f
    .line 538
    iget-object v3, v2, Landroid/media/audiofx/Equalizer$Settings;->bandLevels:[S

    #@31
    mul-int/lit8 v4, v0, 0x2

    #@33
    add-int/lit8 v4, v4, 0x4

    #@35
    invoke-virtual {p0, v1, v4}, Landroid/media/audiofx/Equalizer;->byteArrayToShort([BI)S

    #@38
    move-result v4

    #@39
    aput-short v4, v3, v0

    #@3b
    .line 537
    add-int/lit8 v0, v0, 0x1

    #@3d
    goto :goto_2b

    #@3e
    .line 540
    :cond_3e
    return-object v2
.end method

.method public setBandLevel(SS)V
    .registers 8
    .parameter "band"
    .parameter "level"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 224
    new-array v0, v4, [I

    #@5
    .line 225
    .local v0, param:[I
    new-array v1, v3, [S

    #@7
    .line 227
    .local v1, value:[S
    aput v4, v0, v2

    #@9
    .line 228
    aput p1, v0, v3

    #@b
    .line 229
    aput-short p2, v1, v2

    #@d
    .line 230
    invoke-virtual {p0, v0, v1}, Landroid/media/audiofx/Equalizer;->setParameter([I[S)I

    #@10
    move-result v2

    #@11
    invoke-virtual {p0, v2}, Landroid/media/audiofx/Equalizer;->checkStatus(I)V

    #@14
    .line 231
    return-void
.end method

.method public setParameterListener(Landroid/media/audiofx/Equalizer$OnParameterChangeListener;)V
    .registers 5
    .parameter "listener"

    #@0
    .prologue
    .line 440
    iget-object v1, p0, Landroid/media/audiofx/Equalizer;->mParamListenerLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 441
    :try_start_3
    iget-object v0, p0, Landroid/media/audiofx/Equalizer;->mParamListener:Landroid/media/audiofx/Equalizer$OnParameterChangeListener;

    #@5
    if-nez v0, :cond_16

    #@7
    .line 442
    iput-object p1, p0, Landroid/media/audiofx/Equalizer;->mParamListener:Landroid/media/audiofx/Equalizer$OnParameterChangeListener;

    #@9
    .line 443
    new-instance v0, Landroid/media/audiofx/Equalizer$BaseParameterListener;

    #@b
    const/4 v2, 0x0

    #@c
    invoke-direct {v0, p0, v2}, Landroid/media/audiofx/Equalizer$BaseParameterListener;-><init>(Landroid/media/audiofx/Equalizer;Landroid/media/audiofx/Equalizer$1;)V

    #@f
    iput-object v0, p0, Landroid/media/audiofx/Equalizer;->mBaseParamListener:Landroid/media/audiofx/Equalizer$BaseParameterListener;

    #@11
    .line 444
    iget-object v0, p0, Landroid/media/audiofx/Equalizer;->mBaseParamListener:Landroid/media/audiofx/Equalizer$BaseParameterListener;

    #@13
    invoke-super {p0, v0}, Landroid/media/audiofx/AudioEffect;->setParameterListener(Landroid/media/audiofx/AudioEffect$OnParameterChangeListener;)V

    #@16
    .line 446
    :cond_16
    monitor-exit v1

    #@17
    .line 447
    return-void

    #@18
    .line 446
    :catchall_18
    move-exception v0

    #@19
    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_3 .. :try_end_1a} :catchall_18

    #@1a
    throw v0
.end method

.method public setProperties(Landroid/media/audiofx/Equalizer$Settings;)V
    .registers 9
    .parameter "settings"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x2

    #@1
    const/4 v5, 0x1

    #@2
    const/4 v4, 0x0

    #@3
    .line 553
    iget-short v2, p1, Landroid/media/audiofx/Equalizer$Settings;->numBands:S

    #@5
    iget-object v3, p1, Landroid/media/audiofx/Equalizer$Settings;->bandLevels:[S

    #@7
    array-length v3, v3

    #@8
    if-ne v2, v3, :cond_10

    #@a
    iget-short v2, p1, Landroid/media/audiofx/Equalizer$Settings;->numBands:S

    #@c
    iget-short v3, p0, Landroid/media/audiofx/Equalizer;->mNumBands:S

    #@e
    if-eq v2, v3, :cond_2c

    #@10
    .line 555
    :cond_10
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@12
    new-instance v3, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string/jumbo v4, "settings invalid band count: "

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    iget-short v4, p1, Landroid/media/audiofx/Equalizer$Settings;->numBands:S

    #@20
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v3

    #@28
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2b
    throw v2

    #@2c
    .line 558
    :cond_2c
    new-array v2, v6, [[B

    #@2e
    iget-short v3, p1, Landroid/media/audiofx/Equalizer$Settings;->curPreset:S

    #@30
    invoke-virtual {p0, v3}, Landroid/media/audiofx/Equalizer;->shortToByteArray(S)[B

    #@33
    move-result-object v3

    #@34
    aput-object v3, v2, v4

    #@36
    iget-short v3, p0, Landroid/media/audiofx/Equalizer;->mNumBands:S

    #@38
    invoke-virtual {p0, v3}, Landroid/media/audiofx/Equalizer;->shortToByteArray(S)[B

    #@3b
    move-result-object v3

    #@3c
    aput-object v3, v2, v5

    #@3e
    invoke-virtual {p0, v2}, Landroid/media/audiofx/Equalizer;->concatArrays([[B)[B

    #@41
    move-result-object v1

    #@42
    .line 560
    .local v1, param:[B
    const/4 v0, 0x0

    #@43
    .local v0, i:I
    :goto_43
    iget-short v2, p0, Landroid/media/audiofx/Equalizer;->mNumBands:S

    #@45
    if-ge v0, v2, :cond_5c

    #@47
    .line 561
    new-array v2, v6, [[B

    #@49
    aput-object v1, v2, v4

    #@4b
    iget-object v3, p1, Landroid/media/audiofx/Equalizer$Settings;->bandLevels:[S

    #@4d
    aget-short v3, v3, v0

    #@4f
    invoke-virtual {p0, v3}, Landroid/media/audiofx/Equalizer;->shortToByteArray(S)[B

    #@52
    move-result-object v3

    #@53
    aput-object v3, v2, v5

    #@55
    invoke-virtual {p0, v2}, Landroid/media/audiofx/Equalizer;->concatArrays([[B)[B

    #@58
    move-result-object v1

    #@59
    .line 560
    add-int/lit8 v0, v0, 0x1

    #@5b
    goto :goto_43

    #@5c
    .line 564
    :cond_5c
    const/16 v2, 0x9

    #@5e
    invoke-virtual {p0, v2, v1}, Landroid/media/audiofx/Equalizer;->setParameter(I[B)I

    #@61
    move-result v2

    #@62
    invoke-virtual {p0, v2}, Landroid/media/audiofx/Equalizer;->checkStatus(I)V

    #@65
    .line 565
    return-void
.end method

.method public usePreset(S)V
    .registers 3
    .parameter "preset"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    .line 342
    const/4 v0, 0x6

    #@1
    invoke-virtual {p0, v0, p1}, Landroid/media/audiofx/Equalizer;->setParameter(IS)I

    #@4
    move-result v0

    #@5
    invoke-virtual {p0, v0}, Landroid/media/audiofx/Equalizer;->checkStatus(I)V

    #@8
    .line 343
    return-void
.end method
