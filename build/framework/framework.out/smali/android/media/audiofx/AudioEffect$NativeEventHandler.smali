.class Landroid/media/audiofx/AudioEffect$NativeEventHandler;
.super Landroid/os/Handler;
.source "AudioEffect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/audiofx/AudioEffect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NativeEventHandler"
.end annotation


# instance fields
.field private mAudioEffect:Landroid/media/audiofx/AudioEffect;

.field final synthetic this$0:Landroid/media/audiofx/AudioEffect;


# direct methods
.method public constructor <init>(Landroid/media/audiofx/AudioEffect;Landroid/media/audiofx/AudioEffect;Landroid/os/Looper;)V
    .registers 4
    .parameter
    .parameter "ae"
    .parameter "looper"

    #@0
    .prologue
    .line 1105
    iput-object p1, p0, Landroid/media/audiofx/AudioEffect$NativeEventHandler;->this$0:Landroid/media/audiofx/AudioEffect;

    #@2
    .line 1106
    invoke-direct {p0, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 1107
    iput-object p2, p0, Landroid/media/audiofx/AudioEffect$NativeEventHandler;->mAudioEffect:Landroid/media/audiofx/AudioEffect;

    #@7
    .line 1108
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 16
    .parameter "msg"

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    const/4 v11, 0x0

    #@2
    .line 1112
    iget-object v12, p0, Landroid/media/audiofx/AudioEffect$NativeEventHandler;->mAudioEffect:Landroid/media/audiofx/AudioEffect;

    #@4
    if-nez v12, :cond_7

    #@6
    .line 1165
    :cond_6
    :goto_6
    return-void

    #@7
    .line 1115
    :cond_7
    iget v12, p1, Landroid/os/Message;->what:I

    #@9
    packed-switch v12, :pswitch_data_a6

    #@c
    .line 1162
    const-string v10, "AudioEffect-JAVA"

    #@e
    new-instance v11, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v12, "handleMessage() Unknown event type: "

    #@15
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v11

    #@19
    iget v12, p1, Landroid/os/Message;->what:I

    #@1b
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v11

    #@1f
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v11

    #@23
    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    goto :goto_6

    #@27
    .line 1117
    :pswitch_27
    const/4 v1, 0x0

    #@28
    .line 1118
    .local v1, enableStatusChangeListener:Landroid/media/audiofx/AudioEffect$OnEnableStatusChangeListener;
    iget-object v12, p0, Landroid/media/audiofx/AudioEffect$NativeEventHandler;->this$0:Landroid/media/audiofx/AudioEffect;

    #@2a
    iget-object v12, v12, Landroid/media/audiofx/AudioEffect;->mListenerLock:Ljava/lang/Object;

    #@2c
    monitor-enter v12

    #@2d
    .line 1119
    :try_start_2d
    iget-object v13, p0, Landroid/media/audiofx/AudioEffect$NativeEventHandler;->mAudioEffect:Landroid/media/audiofx/AudioEffect;

    #@2f
    invoke-static {v13}, Landroid/media/audiofx/AudioEffect;->access$000(Landroid/media/audiofx/AudioEffect;)Landroid/media/audiofx/AudioEffect$OnEnableStatusChangeListener;

    #@32
    move-result-object v1

    #@33
    .line 1120
    monitor-exit v12
    :try_end_34
    .catchall {:try_start_2d .. :try_end_34} :catchall_40

    #@34
    .line 1121
    if-eqz v1, :cond_6

    #@36
    .line 1122
    iget-object v12, p0, Landroid/media/audiofx/AudioEffect$NativeEventHandler;->mAudioEffect:Landroid/media/audiofx/AudioEffect;

    #@38
    iget v13, p1, Landroid/os/Message;->arg1:I

    #@3a
    if-eqz v13, :cond_43

    #@3c
    :goto_3c
    invoke-interface {v1, v12, v10}, Landroid/media/audiofx/AudioEffect$OnEnableStatusChangeListener;->onEnableStatusChange(Landroid/media/audiofx/AudioEffect;Z)V

    #@3f
    goto :goto_6

    #@40
    .line 1120
    :catchall_40
    move-exception v10

    #@41
    :try_start_41
    monitor-exit v12
    :try_end_42
    .catchall {:try_start_41 .. :try_end_42} :catchall_40

    #@42
    throw v10

    #@43
    :cond_43
    move v10, v11

    #@44
    .line 1122
    goto :goto_3c

    #@45
    .line 1127
    .end local v1           #enableStatusChangeListener:Landroid/media/audiofx/AudioEffect$OnEnableStatusChangeListener;
    :pswitch_45
    const/4 v0, 0x0

    #@46
    .line 1128
    .local v0, controlStatusChangeListener:Landroid/media/audiofx/AudioEffect$OnControlStatusChangeListener;
    iget-object v12, p0, Landroid/media/audiofx/AudioEffect$NativeEventHandler;->this$0:Landroid/media/audiofx/AudioEffect;

    #@48
    iget-object v12, v12, Landroid/media/audiofx/AudioEffect;->mListenerLock:Ljava/lang/Object;

    #@4a
    monitor-enter v12

    #@4b
    .line 1129
    :try_start_4b
    iget-object v13, p0, Landroid/media/audiofx/AudioEffect$NativeEventHandler;->mAudioEffect:Landroid/media/audiofx/AudioEffect;

    #@4d
    invoke-static {v13}, Landroid/media/audiofx/AudioEffect;->access$100(Landroid/media/audiofx/AudioEffect;)Landroid/media/audiofx/AudioEffect$OnControlStatusChangeListener;

    #@50
    move-result-object v0

    #@51
    .line 1130
    monitor-exit v12
    :try_end_52
    .catchall {:try_start_4b .. :try_end_52} :catchall_5e

    #@52
    .line 1131
    if-eqz v0, :cond_6

    #@54
    .line 1132
    iget-object v12, p0, Landroid/media/audiofx/AudioEffect$NativeEventHandler;->mAudioEffect:Landroid/media/audiofx/AudioEffect;

    #@56
    iget v13, p1, Landroid/os/Message;->arg1:I

    #@58
    if-eqz v13, :cond_61

    #@5a
    :goto_5a
    invoke-interface {v0, v12, v10}, Landroid/media/audiofx/AudioEffect$OnControlStatusChangeListener;->onControlStatusChange(Landroid/media/audiofx/AudioEffect;Z)V

    #@5d
    goto :goto_6

    #@5e
    .line 1130
    :catchall_5e
    move-exception v10

    #@5f
    :try_start_5f
    monitor-exit v12
    :try_end_60
    .catchall {:try_start_5f .. :try_end_60} :catchall_5e

    #@60
    throw v10

    #@61
    :cond_61
    move v10, v11

    #@62
    .line 1132
    goto :goto_5a

    #@63
    .line 1137
    .end local v0           #controlStatusChangeListener:Landroid/media/audiofx/AudioEffect$OnControlStatusChangeListener;
    :pswitch_63
    const/4 v4, 0x0

    #@64
    .line 1138
    .local v4, parameterChangeListener:Landroid/media/audiofx/AudioEffect$OnParameterChangeListener;
    iget-object v10, p0, Landroid/media/audiofx/AudioEffect$NativeEventHandler;->this$0:Landroid/media/audiofx/AudioEffect;

    #@66
    iget-object v12, v10, Landroid/media/audiofx/AudioEffect;->mListenerLock:Ljava/lang/Object;

    #@68
    monitor-enter v12

    #@69
    .line 1139
    :try_start_69
    iget-object v10, p0, Landroid/media/audiofx/AudioEffect$NativeEventHandler;->mAudioEffect:Landroid/media/audiofx/AudioEffect;

    #@6b
    invoke-static {v10}, Landroid/media/audiofx/AudioEffect;->access$200(Landroid/media/audiofx/AudioEffect;)Landroid/media/audiofx/AudioEffect$OnParameterChangeListener;

    #@6e
    move-result-object v4

    #@6f
    .line 1140
    monitor-exit v12
    :try_end_70
    .catchall {:try_start_69 .. :try_end_70} :catchall_a3

    #@70
    .line 1141
    if-eqz v4, :cond_6

    #@72
    .line 1144
    iget v7, p1, Landroid/os/Message;->arg1:I

    #@74
    .line 1145
    .local v7, vOffset:I
    iget-object v10, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@76
    check-cast v10, [B

    #@78
    move-object v2, v10

    #@79
    check-cast v2, [B

    #@7b
    .line 1148
    .local v2, p:[B
    iget-object v10, p0, Landroid/media/audiofx/AudioEffect$NativeEventHandler;->this$0:Landroid/media/audiofx/AudioEffect;

    #@7d
    invoke-virtual {v10, v2, v11}, Landroid/media/audiofx/AudioEffect;->byteArrayToInt([BI)I

    #@80
    move-result v6

    #@81
    .line 1149
    .local v6, status:I
    iget-object v10, p0, Landroid/media/audiofx/AudioEffect$NativeEventHandler;->this$0:Landroid/media/audiofx/AudioEffect;

    #@83
    const/4 v12, 0x4

    #@84
    invoke-virtual {v10, v2, v12}, Landroid/media/audiofx/AudioEffect;->byteArrayToInt([BI)I

    #@87
    move-result v5

    #@88
    .line 1150
    .local v5, psize:I
    iget-object v10, p0, Landroid/media/audiofx/AudioEffect$NativeEventHandler;->this$0:Landroid/media/audiofx/AudioEffect;

    #@8a
    const/16 v12, 0x8

    #@8c
    invoke-virtual {v10, v2, v12}, Landroid/media/audiofx/AudioEffect;->byteArrayToInt([BI)I

    #@8f
    move-result v9

    #@90
    .line 1151
    .local v9, vsize:I
    new-array v3, v5, [B

    #@92
    .line 1152
    .local v3, param:[B
    new-array v8, v9, [B

    #@94
    .line 1153
    .local v8, value:[B
    const/16 v10, 0xc

    #@96
    invoke-static {v2, v10, v3, v11, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@99
    .line 1154
    invoke-static {v2, v7, v8, v11, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@9c
    .line 1156
    iget-object v10, p0, Landroid/media/audiofx/AudioEffect$NativeEventHandler;->mAudioEffect:Landroid/media/audiofx/AudioEffect;

    #@9e
    invoke-interface {v4, v10, v6, v3, v8}, Landroid/media/audiofx/AudioEffect$OnParameterChangeListener;->onParameterChange(Landroid/media/audiofx/AudioEffect;I[B[B)V

    #@a1
    goto/16 :goto_6

    #@a3
    .line 1140
    .end local v2           #p:[B
    .end local v3           #param:[B
    .end local v5           #psize:I
    .end local v6           #status:I
    .end local v7           #vOffset:I
    .end local v8           #value:[B
    .end local v9           #vsize:I
    :catchall_a3
    move-exception v10

    #@a4
    :try_start_a4
    monitor-exit v12
    :try_end_a5
    .catchall {:try_start_a4 .. :try_end_a5} :catchall_a3

    #@a5
    throw v10

    #@a6
    .line 1115
    :pswitch_data_a6
    .packed-switch 0x0
        :pswitch_45
        :pswitch_27
        :pswitch_63
    .end packed-switch
.end method
