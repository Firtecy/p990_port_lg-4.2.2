.class public Landroid/media/audiofx/Visualizer;
.super Ljava/lang/Object;
.source "Visualizer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/audiofx/Visualizer$NativeEventHandler;,
        Landroid/media/audiofx/Visualizer$OnServerDiedListener;,
        Landroid/media/audiofx/Visualizer$OnDataCaptureListener;
    }
.end annotation


# static fields
.field public static final ALREADY_EXISTS:I = -0x2

.field public static final ERROR:I = -0x1

.field public static final ERROR_BAD_VALUE:I = -0x4

.field public static final ERROR_DEAD_OBJECT:I = -0x7

.field public static final ERROR_INVALID_OPERATION:I = -0x5

.field public static final ERROR_NO_INIT:I = -0x3

.field public static final ERROR_NO_MEMORY:I = -0x6

.field private static final NATIVE_EVENT_FFT_CAPTURE:I = 0x1

.field private static final NATIVE_EVENT_PCM_CAPTURE:I = 0x0

.field private static final NATIVE_EVENT_SERVER_DIED:I = 0x2

.field public static final SCALING_MODE_AS_PLAYED:I = 0x1

.field public static final SCALING_MODE_NORMALIZED:I = 0x0

.field public static final STATE_ENABLED:I = 0x2

.field public static final STATE_INITIALIZED:I = 0x1

.field public static final STATE_UNINITIALIZED:I = 0x0

.field public static final SUCCESS:I = 0x0

.field private static final TAG:Ljava/lang/String; = "Visualizer-JAVA"


# instance fields
.field private mCaptureListener:Landroid/media/audiofx/Visualizer$OnDataCaptureListener;

.field private mId:I

.field private mJniData:I

.field private final mListenerLock:Ljava/lang/Object;

.field private mNativeEventHandler:Landroid/media/audiofx/Visualizer$NativeEventHandler;

.field private mNativeVisualizer:I

.field private mServerDiedListener:Landroid/media/audiofx/Visualizer$OnServerDiedListener;

.field private mState:I

.field private final mStateLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 65
    const-string v0, "audioeffect_jni"

    #@2
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@5
    .line 66
    invoke-static {}, Landroid/media/audiofx/Visualizer;->native_init()V

    #@8
    .line 67
    return-void
.end method

.method public constructor <init>(I)V
    .registers 8
    .parameter "audioSession"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    const/4 v2, 0x0

    #@3
    .line 186
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 141
    iput v2, p0, Landroid/media/audiofx/Visualizer;->mState:I

    #@8
    .line 145
    new-instance v2, Ljava/lang/Object;

    #@a
    invoke-direct/range {v2 .. v2}, Ljava/lang/Object;-><init>()V

    #@d
    iput-object v2, p0, Landroid/media/audiofx/Visualizer;->mStateLock:Ljava/lang/Object;

    #@f
    .line 154
    new-instance v2, Ljava/lang/Object;

    #@11
    invoke-direct/range {v2 .. v2}, Ljava/lang/Object;-><init>()V

    #@14
    iput-object v2, p0, Landroid/media/audiofx/Visualizer;->mListenerLock:Ljava/lang/Object;

    #@16
    .line 158
    iput-object v3, p0, Landroid/media/audiofx/Visualizer;->mNativeEventHandler:Landroid/media/audiofx/Visualizer$NativeEventHandler;

    #@18
    .line 162
    iput-object v3, p0, Landroid/media/audiofx/Visualizer;->mCaptureListener:Landroid/media/audiofx/Visualizer$OnDataCaptureListener;

    #@1a
    .line 166
    iput-object v3, p0, Landroid/media/audiofx/Visualizer;->mServerDiedListener:Landroid/media/audiofx/Visualizer$OnServerDiedListener;

    #@1c
    .line 187
    new-array v0, v4, [I

    #@1e
    .line 189
    .local v0, id:[I
    iget-object v3, p0, Landroid/media/audiofx/Visualizer;->mStateLock:Ljava/lang/Object;

    #@20
    monitor-enter v3

    #@21
    .line 190
    const/4 v2, 0x0

    #@22
    :try_start_22
    iput v2, p0, Landroid/media/audiofx/Visualizer;->mState:I

    #@24
    .line 192
    new-instance v2, Ljava/lang/ref/WeakReference;

    #@26
    invoke-direct {v2, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@29
    invoke-direct {p0, v2, p1, v0}, Landroid/media/audiofx/Visualizer;->native_setup(Ljava/lang/Object;I[I)I

    #@2c
    move-result v1

    #@2d
    .line 193
    .local v1, result:I
    if-eqz v1, :cond_77

    #@2f
    const/4 v2, -0x2

    #@30
    if-eq v1, v2, :cond_77

    #@32
    .line 194
    const-string v2, "Visualizer-JAVA"

    #@34
    new-instance v4, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v5, "Error code "

    #@3b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v4

    #@3f
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    move-result-object v4

    #@43
    const-string v5, " when initializing Visualizer."

    #@45
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v4

    #@49
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v4

    #@4d
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    .line 195
    packed-switch v1, :pswitch_data_8c

    #@53
    .line 199
    new-instance v2, Ljava/lang/RuntimeException;

    #@55
    new-instance v4, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v5, "Cannot initialize Visualizer engine, error: "

    #@5c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v4

    #@60
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@63
    move-result-object v4

    #@64
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v4

    #@68
    invoke-direct {v2, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@6b
    throw v2

    #@6c
    .line 209
    .end local v1           #result:I
    :catchall_6c
    move-exception v2

    #@6d
    monitor-exit v3
    :try_end_6e
    .catchall {:try_start_22 .. :try_end_6e} :catchall_6c

    #@6e
    throw v2

    #@6f
    .line 197
    .restart local v1       #result:I
    :pswitch_6f
    :try_start_6f
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    #@71
    const-string v4, "Effect library not loaded"

    #@73
    invoke-direct {v2, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@76
    throw v2

    #@77
    .line 203
    :cond_77
    const/4 v2, 0x0

    #@78
    aget v2, v0, v2

    #@7a
    iput v2, p0, Landroid/media/audiofx/Visualizer;->mId:I

    #@7c
    .line 204
    invoke-direct {p0}, Landroid/media/audiofx/Visualizer;->native_getEnabled()Z

    #@7f
    move-result v2

    #@80
    if-eqz v2, :cond_87

    #@82
    .line 205
    const/4 v2, 0x2

    #@83
    iput v2, p0, Landroid/media/audiofx/Visualizer;->mState:I

    #@85
    .line 209
    :goto_85
    monitor-exit v3

    #@86
    .line 210
    return-void

    #@87
    .line 207
    :cond_87
    const/4 v2, 0x1

    #@88
    iput v2, p0, Landroid/media/audiofx/Visualizer;->mState:I
    :try_end_8a
    .catchall {:try_start_6f .. :try_end_8a} :catchall_6c

    #@8a
    goto :goto_85

    #@8b
    .line 195
    nop

    #@8c
    :pswitch_data_8c
    .packed-switch -0x5
        :pswitch_6f
    .end packed-switch
.end method

.method static synthetic access$000(Landroid/media/audiofx/Visualizer;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Landroid/media/audiofx/Visualizer;->mListenerLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/media/audiofx/Visualizer;)Landroid/media/audiofx/Visualizer$OnDataCaptureListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Landroid/media/audiofx/Visualizer;->mCaptureListener:Landroid/media/audiofx/Visualizer$OnDataCaptureListener;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/media/audiofx/Visualizer;)Landroid/media/audiofx/Visualizer$OnServerDiedListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Landroid/media/audiofx/Visualizer;->mServerDiedListener:Landroid/media/audiofx/Visualizer$OnServerDiedListener;

    #@2
    return-object v0
.end method

.method public static native getCaptureSizeRange()[I
.end method

.method public static native getMaxCaptureRate()I
.end method

.method private final native native_finalize()V
.end method

.method private final native native_getCaptureSize()I
.end method

.method private final native native_getEnabled()Z
.end method

.method private final native native_getFft([B)I
.end method

.method private final native native_getSamplingRate()I
.end method

.method private final native native_getScalingMode()I
.end method

.method private final native native_getWaveForm([B)I
.end method

.method private static final native native_init()V
.end method

.method private final native native_release()V
.end method

.method private final native native_setCaptureSize(I)I
.end method

.method private final native native_setEnabled(Z)I
.end method

.method private final native native_setPeriodicCapture(IZZ)I
.end method

.method private final native native_setScalingMode(I)I
.end method

.method private final native native_setup(Ljava/lang/Object;I[I)I
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .registers 8
    .parameter "effect_ref"
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"
    .parameter "obj"

    #@0
    .prologue
    .line 657
    check-cast p0, Ljava/lang/ref/WeakReference;

    #@2
    .end local p0
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Landroid/media/audiofx/Visualizer;

    #@8
    .line 658
    .local v1, visu:Landroid/media/audiofx/Visualizer;
    if-nez v1, :cond_b

    #@a
    .line 667
    :cond_a
    :goto_a
    return-void

    #@b
    .line 662
    :cond_b
    iget-object v2, v1, Landroid/media/audiofx/Visualizer;->mNativeEventHandler:Landroid/media/audiofx/Visualizer$NativeEventHandler;

    #@d
    if-eqz v2, :cond_a

    #@f
    .line 663
    iget-object v2, v1, Landroid/media/audiofx/Visualizer;->mNativeEventHandler:Landroid/media/audiofx/Visualizer$NativeEventHandler;

    #@11
    invoke-virtual {v2, p1, p2, p3, p4}, Landroid/media/audiofx/Visualizer$NativeEventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@14
    move-result-object v0

    #@15
    .line 664
    .local v0, m:Landroid/os/Message;
    iget-object v2, v1, Landroid/media/audiofx/Visualizer;->mNativeEventHandler:Landroid/media/audiofx/Visualizer$NativeEventHandler;

    #@17
    invoke-virtual {v2, v0}, Landroid/media/audiofx/Visualizer$NativeEventHandler;->sendMessage(Landroid/os/Message;)Z

    #@1a
    goto :goto_a
.end method


# virtual methods
.method protected finalize()V
    .registers 1

    #@0
    .prologue
    .line 225
    invoke-direct {p0}, Landroid/media/audiofx/Visualizer;->native_finalize()V

    #@3
    .line 226
    return-void
.end method

.method public getCaptureSize()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 308
    iget-object v1, p0, Landroid/media/audiofx/Visualizer;->mStateLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 309
    :try_start_3
    iget v0, p0, Landroid/media/audiofx/Visualizer;->mState:I

    #@5
    if-nez v0, :cond_25

    #@7
    .line 310
    new-instance v0, Ljava/lang/IllegalStateException;

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "getCaptureSize() called in wrong state: "

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    iget v3, p0, Landroid/media/audiofx/Visualizer;->mState:I

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@21
    throw v0

    #@22
    .line 313
    :catchall_22
    move-exception v0

    #@23
    monitor-exit v1
    :try_end_24
    .catchall {:try_start_3 .. :try_end_24} :catchall_22

    #@24
    throw v0

    #@25
    .line 312
    :cond_25
    :try_start_25
    invoke-direct {p0}, Landroid/media/audiofx/Visualizer;->native_getCaptureSize()I

    #@28
    move-result v0

    #@29
    monitor-exit v1
    :try_end_2a
    .catchall {:try_start_25 .. :try_end_2a} :catchall_22

    #@2a
    return v0
.end method

.method public getEnabled()Z
    .registers 5

    #@0
    .prologue
    .line 259
    iget-object v1, p0, Landroid/media/audiofx/Visualizer;->mStateLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 260
    :try_start_3
    iget v0, p0, Landroid/media/audiofx/Visualizer;->mState:I

    #@5
    if-nez v0, :cond_25

    #@7
    .line 261
    new-instance v0, Ljava/lang/IllegalStateException;

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "getEnabled() called in wrong state: "

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    iget v3, p0, Landroid/media/audiofx/Visualizer;->mState:I

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@21
    throw v0

    #@22
    .line 264
    :catchall_22
    move-exception v0

    #@23
    monitor-exit v1
    :try_end_24
    .catchall {:try_start_3 .. :try_end_24} :catchall_22

    #@24
    throw v0

    #@25
    .line 263
    :cond_25
    :try_start_25
    invoke-direct {p0}, Landroid/media/audiofx/Visualizer;->native_getEnabled()Z

    #@28
    move-result v0

    #@29
    monitor-exit v1
    :try_end_2a
    .catchall {:try_start_25 .. :try_end_2a} :catchall_22

    #@2a
    return v0
.end method

.method public getFft([B)I
    .registers 6
    .parameter "fft"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 432
    iget-object v1, p0, Landroid/media/audiofx/Visualizer;->mStateLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 433
    :try_start_3
    iget v0, p0, Landroid/media/audiofx/Visualizer;->mState:I

    #@5
    const/4 v2, 0x2

    #@6
    if-eq v0, v2, :cond_26

    #@8
    .line 434
    new-instance v0, Ljava/lang/IllegalStateException;

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "getFft() called in wrong state: "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    iget v3, p0, Landroid/media/audiofx/Visualizer;->mState:I

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@22
    throw v0

    #@23
    .line 437
    :catchall_23
    move-exception v0

    #@24
    monitor-exit v1
    :try_end_25
    .catchall {:try_start_3 .. :try_end_25} :catchall_23

    #@25
    throw v0

    #@26
    .line 436
    :cond_26
    :try_start_26
    invoke-direct {p0, p1}, Landroid/media/audiofx/Visualizer;->native_getFft([B)I

    #@29
    move-result v0

    #@2a
    monitor-exit v1
    :try_end_2b
    .catchall {:try_start_26 .. :try_end_2b} :catchall_23

    #@2b
    return v0
.end method

.method public getSamplingRate()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 358
    iget-object v1, p0, Landroid/media/audiofx/Visualizer;->mStateLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 359
    :try_start_3
    iget v0, p0, Landroid/media/audiofx/Visualizer;->mState:I

    #@5
    if-nez v0, :cond_25

    #@7
    .line 360
    new-instance v0, Ljava/lang/IllegalStateException;

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "getSamplingRate() called in wrong state: "

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    iget v3, p0, Landroid/media/audiofx/Visualizer;->mState:I

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@21
    throw v0

    #@22
    .line 363
    :catchall_22
    move-exception v0

    #@23
    monitor-exit v1
    :try_end_24
    .catchall {:try_start_3 .. :try_end_24} :catchall_22

    #@24
    throw v0

    #@25
    .line 362
    :cond_25
    :try_start_25
    invoke-direct {p0}, Landroid/media/audiofx/Visualizer;->native_getSamplingRate()I

    #@28
    move-result v0

    #@29
    monitor-exit v1
    :try_end_2a
    .catchall {:try_start_25 .. :try_end_2a} :catchall_22

    #@2a
    return v0
.end method

.method public getScalingMode()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 343
    iget-object v1, p0, Landroid/media/audiofx/Visualizer;->mStateLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 344
    :try_start_3
    iget v0, p0, Landroid/media/audiofx/Visualizer;->mState:I

    #@5
    if-nez v0, :cond_25

    #@7
    .line 345
    new-instance v0, Ljava/lang/IllegalStateException;

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "getScalingMode() called in wrong state: "

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    iget v3, p0, Landroid/media/audiofx/Visualizer;->mState:I

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@21
    throw v0

    #@22
    .line 349
    :catchall_22
    move-exception v0

    #@23
    monitor-exit v1
    :try_end_24
    .catchall {:try_start_3 .. :try_end_24} :catchall_22

    #@24
    throw v0

    #@25
    .line 348
    :cond_25
    :try_start_25
    invoke-direct {p0}, Landroid/media/audiofx/Visualizer;->native_getScalingMode()I

    #@28
    move-result v0

    #@29
    monitor-exit v1
    :try_end_2a
    .catchall {:try_start_25 .. :try_end_2a} :catchall_22

    #@2a
    return v0
.end method

.method public getWaveForm([B)I
    .registers 6
    .parameter "waveform"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 379
    iget-object v1, p0, Landroid/media/audiofx/Visualizer;->mStateLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 380
    :try_start_3
    iget v0, p0, Landroid/media/audiofx/Visualizer;->mState:I

    #@5
    const/4 v2, 0x2

    #@6
    if-eq v0, v2, :cond_26

    #@8
    .line 381
    new-instance v0, Ljava/lang/IllegalStateException;

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "getWaveForm() called in wrong state: "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    iget v3, p0, Landroid/media/audiofx/Visualizer;->mState:I

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@22
    throw v0

    #@23
    .line 384
    :catchall_23
    move-exception v0

    #@24
    monitor-exit v1
    :try_end_25
    .catchall {:try_start_3 .. :try_end_25} :catchall_23

    #@25
    throw v0

    #@26
    .line 383
    :cond_26
    :try_start_26
    invoke-direct {p0, p1}, Landroid/media/audiofx/Visualizer;->native_getWaveForm([B)I

    #@29
    move-result v0

    #@2a
    monitor-exit v1
    :try_end_2b
    .catchall {:try_start_26 .. :try_end_2b} :catchall_23

    #@2b
    return v0
.end method

.method public release()V
    .registers 3

    #@0
    .prologue
    .line 217
    iget-object v1, p0, Landroid/media/audiofx/Visualizer;->mStateLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 218
    :try_start_3
    invoke-direct {p0}, Landroid/media/audiofx/Visualizer;->native_release()V

    #@6
    .line 219
    const/4 v0, 0x0

    #@7
    iput v0, p0, Landroid/media/audiofx/Visualizer;->mState:I

    #@9
    .line 220
    monitor-exit v1

    #@a
    .line 221
    return-void

    #@b
    .line 220
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method public setCaptureSize(I)I
    .registers 6
    .parameter "size"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 294
    iget-object v1, p0, Landroid/media/audiofx/Visualizer;->mStateLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 295
    :try_start_3
    iget v0, p0, Landroid/media/audiofx/Visualizer;->mState:I

    #@5
    const/4 v2, 0x1

    #@6
    if-eq v0, v2, :cond_27

    #@8
    .line 296
    new-instance v0, Ljava/lang/IllegalStateException;

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string/jumbo v3, "setCaptureSize() called in wrong state: "

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    iget v3, p0, Landroid/media/audiofx/Visualizer;->mState:I

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@23
    throw v0

    #@24
    .line 299
    :catchall_24
    move-exception v0

    #@25
    monitor-exit v1
    :try_end_26
    .catchall {:try_start_3 .. :try_end_26} :catchall_24

    #@26
    throw v0

    #@27
    .line 298
    :cond_27
    :try_start_27
    invoke-direct {p0, p1}, Landroid/media/audiofx/Visualizer;->native_setCaptureSize(I)I

    #@2a
    move-result v0

    #@2b
    monitor-exit v1
    :try_end_2c
    .catchall {:try_start_27 .. :try_end_2c} :catchall_24

    #@2c
    return v0
.end method

.method public setDataCaptureListener(Landroid/media/audiofx/Visualizer$OnDataCaptureListener;IZZ)I
    .registers 9
    .parameter "listener"
    .parameter "rate"
    .parameter "waveform"
    .parameter "fft"

    #@0
    .prologue
    .line 488
    iget-object v3, p0, Landroid/media/audiofx/Visualizer;->mListenerLock:Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 489
    :try_start_3
    iput-object p1, p0, Landroid/media/audiofx/Visualizer;->mCaptureListener:Landroid/media/audiofx/Visualizer$OnDataCaptureListener;

    #@5
    .line 490
    monitor-exit v3
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_24

    #@6
    .line 491
    if-nez p1, :cond_a

    #@8
    .line 493
    const/4 p3, 0x0

    #@9
    .line 494
    const/4 p4, 0x0

    #@a
    .line 496
    :cond_a
    invoke-direct {p0, p2, p3, p4}, Landroid/media/audiofx/Visualizer;->native_setPeriodicCapture(IZZ)I

    #@d
    move-result v1

    #@e
    .line 497
    .local v1, status:I
    if-nez v1, :cond_23

    #@10
    .line 498
    if-eqz p1, :cond_23

    #@12
    iget-object v2, p0, Landroid/media/audiofx/Visualizer;->mNativeEventHandler:Landroid/media/audiofx/Visualizer$NativeEventHandler;

    #@14
    if-nez v2, :cond_23

    #@16
    .line 500
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@19
    move-result-object v0

    #@1a
    .local v0, looper:Landroid/os/Looper;
    if-eqz v0, :cond_27

    #@1c
    .line 501
    new-instance v2, Landroid/media/audiofx/Visualizer$NativeEventHandler;

    #@1e
    invoke-direct {v2, p0, p0, v0}, Landroid/media/audiofx/Visualizer$NativeEventHandler;-><init>(Landroid/media/audiofx/Visualizer;Landroid/media/audiofx/Visualizer;Landroid/os/Looper;)V

    #@21
    iput-object v2, p0, Landroid/media/audiofx/Visualizer;->mNativeEventHandler:Landroid/media/audiofx/Visualizer$NativeEventHandler;

    #@23
    .line 510
    .end local v0           #looper:Landroid/os/Looper;
    :cond_23
    :goto_23
    return v1

    #@24
    .line 490
    .end local v1           #status:I
    :catchall_24
    move-exception v2

    #@25
    :try_start_25
    monitor-exit v3
    :try_end_26
    .catchall {:try_start_25 .. :try_end_26} :catchall_24

    #@26
    throw v2

    #@27
    .line 502
    .restart local v0       #looper:Landroid/os/Looper;
    .restart local v1       #status:I
    :cond_27
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    #@2a
    move-result-object v0

    #@2b
    if-eqz v0, :cond_35

    #@2d
    .line 503
    new-instance v2, Landroid/media/audiofx/Visualizer$NativeEventHandler;

    #@2f
    invoke-direct {v2, p0, p0, v0}, Landroid/media/audiofx/Visualizer$NativeEventHandler;-><init>(Landroid/media/audiofx/Visualizer;Landroid/media/audiofx/Visualizer;Landroid/os/Looper;)V

    #@32
    iput-object v2, p0, Landroid/media/audiofx/Visualizer;->mNativeEventHandler:Landroid/media/audiofx/Visualizer$NativeEventHandler;

    #@34
    goto :goto_23

    #@35
    .line 505
    :cond_35
    const/4 v2, 0x0

    #@36
    iput-object v2, p0, Landroid/media/audiofx/Visualizer;->mNativeEventHandler:Landroid/media/audiofx/Visualizer$NativeEventHandler;

    #@38
    .line 506
    const/4 v1, -0x3

    #@39
    goto :goto_23
.end method

.method public setEnabled(Z)I
    .registers 7
    .parameter "enabled"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x2

    #@1
    const/4 v2, 0x1

    #@2
    .line 237
    iget-object v3, p0, Landroid/media/audiofx/Visualizer;->mStateLock:Ljava/lang/Object;

    #@4
    monitor-enter v3

    #@5
    .line 238
    :try_start_5
    iget v4, p0, Landroid/media/audiofx/Visualizer;->mState:I

    #@7
    if-nez v4, :cond_28

    #@9
    .line 239
    new-instance v1, Ljava/lang/IllegalStateException;

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string/jumbo v4, "setEnabled() called in wrong state: "

    #@13
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    iget v4, p0, Landroid/media/audiofx/Visualizer;->mState:I

    #@19
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@24
    throw v1

    #@25
    .line 250
    :catchall_25
    move-exception v1

    #@26
    monitor-exit v3
    :try_end_27
    .catchall {:try_start_5 .. :try_end_27} :catchall_25

    #@27
    throw v1

    #@28
    .line 241
    :cond_28
    const/4 v0, 0x0

    #@29
    .line 242
    .local v0, status:I
    if-eqz p1, :cond_2f

    #@2b
    :try_start_2b
    iget v4, p0, Landroid/media/audiofx/Visualizer;->mState:I

    #@2d
    if-eq v4, v2, :cond_35

    #@2f
    :cond_2f
    if-nez p1, :cond_3f

    #@31
    iget v4, p0, Landroid/media/audiofx/Visualizer;->mState:I

    #@33
    if-ne v4, v1, :cond_3f

    #@35
    .line 244
    :cond_35
    invoke-direct {p0, p1}, Landroid/media/audiofx/Visualizer;->native_setEnabled(Z)I

    #@38
    move-result v0

    #@39
    .line 245
    if-nez v0, :cond_3f

    #@3b
    .line 246
    if-eqz p1, :cond_41

    #@3d
    :goto_3d
    iput v1, p0, Landroid/media/audiofx/Visualizer;->mState:I

    #@3f
    .line 249
    :cond_3f
    monitor-exit v3
    :try_end_40
    .catchall {:try_start_2b .. :try_end_40} :catchall_25

    #@40
    return v0

    #@41
    :cond_41
    move v1, v2

    #@42
    .line 246
    goto :goto_3d
.end method

.method public setScalingMode(I)I
    .registers 6
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 326
    iget-object v1, p0, Landroid/media/audiofx/Visualizer;->mStateLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 327
    :try_start_3
    iget v0, p0, Landroid/media/audiofx/Visualizer;->mState:I

    #@5
    if-nez v0, :cond_26

    #@7
    .line 328
    new-instance v0, Ljava/lang/IllegalStateException;

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string/jumbo v3, "setScalingMode() called in wrong state: "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    iget v3, p0, Landroid/media/audiofx/Visualizer;->mState:I

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@22
    throw v0

    #@23
    .line 332
    :catchall_23
    move-exception v0

    #@24
    monitor-exit v1
    :try_end_25
    .catchall {:try_start_3 .. :try_end_25} :catchall_23

    #@25
    throw v0

    #@26
    .line 331
    :cond_26
    :try_start_26
    invoke-direct {p0, p1}, Landroid/media/audiofx/Visualizer;->native_setScalingMode(I)I

    #@29
    move-result v0

    #@2a
    monitor-exit v1
    :try_end_2b
    .catchall {:try_start_26 .. :try_end_2b} :catchall_23

    #@2b
    return v0
.end method

.method public setServerDiedListener(Landroid/media/audiofx/Visualizer$OnServerDiedListener;)I
    .registers 4
    .parameter "listener"

    #@0
    .prologue
    .line 544
    iget-object v1, p0, Landroid/media/audiofx/Visualizer;->mListenerLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 545
    :try_start_3
    iput-object p1, p0, Landroid/media/audiofx/Visualizer;->mServerDiedListener:Landroid/media/audiofx/Visualizer$OnServerDiedListener;

    #@5
    .line 546
    monitor-exit v1

    #@6
    .line 547
    const/4 v0, 0x0

    #@7
    return v0

    #@8
    .line 546
    :catchall_8
    move-exception v0

    #@9
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method
