.class Landroid/media/audiofx/Visualizer$NativeEventHandler;
.super Landroid/os/Handler;
.source "Visualizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/audiofx/Visualizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NativeEventHandler"
.end annotation


# instance fields
.field private mVisualizer:Landroid/media/audiofx/Visualizer;

.field final synthetic this$0:Landroid/media/audiofx/Visualizer;


# direct methods
.method public constructor <init>(Landroid/media/audiofx/Visualizer;Landroid/media/audiofx/Visualizer;Landroid/os/Looper;)V
    .registers 4
    .parameter
    .parameter "v"
    .parameter "looper"

    #@0
    .prologue
    .line 557
    iput-object p1, p0, Landroid/media/audiofx/Visualizer$NativeEventHandler;->this$0:Landroid/media/audiofx/Visualizer;

    #@2
    .line 558
    invoke-direct {p0, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 559
    iput-object p2, p0, Landroid/media/audiofx/Visualizer$NativeEventHandler;->mVisualizer:Landroid/media/audiofx/Visualizer;

    #@7
    .line 560
    return-void
.end method

.method private handleCaptureMessage(Landroid/os/Message;)V
    .registers 8
    .parameter "msg"

    #@0
    .prologue
    .line 563
    const/4 v1, 0x0

    #@1
    .line 564
    .local v1, l:Landroid/media/audiofx/Visualizer$OnDataCaptureListener;
    iget-object v3, p0, Landroid/media/audiofx/Visualizer$NativeEventHandler;->this$0:Landroid/media/audiofx/Visualizer;

    #@3
    invoke-static {v3}, Landroid/media/audiofx/Visualizer;->access$000(Landroid/media/audiofx/Visualizer;)Ljava/lang/Object;

    #@6
    move-result-object v4

    #@7
    monitor-enter v4

    #@8
    .line 565
    :try_start_8
    iget-object v3, p0, Landroid/media/audiofx/Visualizer$NativeEventHandler;->mVisualizer:Landroid/media/audiofx/Visualizer;

    #@a
    invoke-static {v3}, Landroid/media/audiofx/Visualizer;->access$100(Landroid/media/audiofx/Visualizer;)Landroid/media/audiofx/Visualizer$OnDataCaptureListener;

    #@d
    move-result-object v1

    #@e
    .line 566
    monitor-exit v4
    :try_end_f
    .catchall {:try_start_8 .. :try_end_f} :catchall_3a

    #@f
    .line 568
    if-eqz v1, :cond_39

    #@11
    .line 569
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@13
    check-cast v3, [B

    #@15
    move-object v0, v3

    #@16
    check-cast v0, [B

    #@18
    .line 570
    .local v0, data:[B
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@1a
    .line 572
    .local v2, samplingRate:I
    iget v3, p1, Landroid/os/Message;->what:I

    #@1c
    packed-switch v3, :pswitch_data_4a

    #@1f
    .line 580
    const-string v3, "Visualizer-JAVA"

    #@21
    new-instance v4, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v5, "Unknown native event in handleCaptureMessge: "

    #@28
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v4

    #@2c
    iget v5, p1, Landroid/os/Message;->what:I

    #@2e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@31
    move-result-object v4

    #@32
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v4

    #@36
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 584
    .end local v0           #data:[B
    .end local v2           #samplingRate:I
    :cond_39
    :goto_39
    return-void

    #@3a
    .line 566
    :catchall_3a
    move-exception v3

    #@3b
    :try_start_3b
    monitor-exit v4
    :try_end_3c
    .catchall {:try_start_3b .. :try_end_3c} :catchall_3a

    #@3c
    throw v3

    #@3d
    .line 574
    .restart local v0       #data:[B
    .restart local v2       #samplingRate:I
    :pswitch_3d
    iget-object v3, p0, Landroid/media/audiofx/Visualizer$NativeEventHandler;->mVisualizer:Landroid/media/audiofx/Visualizer;

    #@3f
    invoke-interface {v1, v3, v0, v2}, Landroid/media/audiofx/Visualizer$OnDataCaptureListener;->onWaveFormDataCapture(Landroid/media/audiofx/Visualizer;[BI)V

    #@42
    goto :goto_39

    #@43
    .line 577
    :pswitch_43
    iget-object v3, p0, Landroid/media/audiofx/Visualizer$NativeEventHandler;->mVisualizer:Landroid/media/audiofx/Visualizer;

    #@45
    invoke-interface {v1, v3, v0, v2}, Landroid/media/audiofx/Visualizer$OnDataCaptureListener;->onFftDataCapture(Landroid/media/audiofx/Visualizer;[BI)V

    #@48
    goto :goto_39

    #@49
    .line 572
    nop

    #@4a
    :pswitch_data_4a
    .packed-switch 0x0
        :pswitch_3d
        :pswitch_43
    .end packed-switch
.end method

.method private handleServerDiedMessage(Landroid/os/Message;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 587
    const/4 v0, 0x0

    #@1
    .line 588
    .local v0, l:Landroid/media/audiofx/Visualizer$OnServerDiedListener;
    iget-object v1, p0, Landroid/media/audiofx/Visualizer$NativeEventHandler;->this$0:Landroid/media/audiofx/Visualizer;

    #@3
    invoke-static {v1}, Landroid/media/audiofx/Visualizer;->access$000(Landroid/media/audiofx/Visualizer;)Ljava/lang/Object;

    #@6
    move-result-object v2

    #@7
    monitor-enter v2

    #@8
    .line 589
    :try_start_8
    iget-object v1, p0, Landroid/media/audiofx/Visualizer$NativeEventHandler;->mVisualizer:Landroid/media/audiofx/Visualizer;

    #@a
    invoke-static {v1}, Landroid/media/audiofx/Visualizer;->access$200(Landroid/media/audiofx/Visualizer;)Landroid/media/audiofx/Visualizer$OnServerDiedListener;

    #@d
    move-result-object v0

    #@e
    .line 590
    monitor-exit v2
    :try_end_f
    .catchall {:try_start_8 .. :try_end_f} :catchall_15

    #@f
    .line 592
    if-eqz v0, :cond_14

    #@11
    .line 593
    invoke-interface {v0}, Landroid/media/audiofx/Visualizer$OnServerDiedListener;->onServerDied()V

    #@14
    .line 594
    :cond_14
    return-void

    #@15
    .line 590
    :catchall_15
    move-exception v1

    #@16
    :try_start_16
    monitor-exit v2
    :try_end_17
    .catchall {:try_start_16 .. :try_end_17} :catchall_15

    #@17
    throw v1
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 598
    iget-object v0, p0, Landroid/media/audiofx/Visualizer$NativeEventHandler;->mVisualizer:Landroid/media/audiofx/Visualizer;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 614
    :goto_4
    return-void

    #@5
    .line 602
    :cond_5
    iget v0, p1, Landroid/os/Message;->what:I

    #@7
    packed-switch v0, :pswitch_data_2e

    #@a
    .line 611
    const-string v0, "Visualizer-JAVA"

    #@c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v2, "Unknown native event: "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    iget v2, p1, Landroid/os/Message;->what:I

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    goto :goto_4

    #@25
    .line 605
    :pswitch_25
    invoke-direct {p0, p1}, Landroid/media/audiofx/Visualizer$NativeEventHandler;->handleCaptureMessage(Landroid/os/Message;)V

    #@28
    goto :goto_4

    #@29
    .line 608
    :pswitch_29
    invoke-direct {p0, p1}, Landroid/media/audiofx/Visualizer$NativeEventHandler;->handleServerDiedMessage(Landroid/os/Message;)V

    #@2c
    goto :goto_4

    #@2d
    .line 602
    nop

    #@2e
    :pswitch_data_2e
    .packed-switch 0x0
        :pswitch_25
        :pswitch_25
        :pswitch_29
    .end packed-switch
.end method
