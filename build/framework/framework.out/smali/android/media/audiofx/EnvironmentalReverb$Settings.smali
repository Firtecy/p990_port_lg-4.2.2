.class public Landroid/media/audiofx/EnvironmentalReverb$Settings;
.super Ljava/lang/Object;
.source "EnvironmentalReverb.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/audiofx/EnvironmentalReverb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Settings"
.end annotation


# instance fields
.field public decayHFRatio:S

.field public decayTime:I

.field public density:S

.field public diffusion:S

.field public reflectionsDelay:I

.field public reflectionsLevel:S

.field public reverbDelay:I

.field public reverbLevel:S

.field public roomHFLevel:S

.field public roomLevel:S


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 521
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 522
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 9
    .parameter "settings"

    #@0
    .prologue
    .line 529
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 530
    new-instance v2, Ljava/util/StringTokenizer;

    #@5
    const-string v4, "=;"

    #@7
    invoke-direct {v2, p1, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 531
    .local v2, st:Ljava/util/StringTokenizer;
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->countTokens()I

    #@d
    move-result v3

    #@e
    .line 532
    .local v3, tokens:I
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->countTokens()I

    #@11
    move-result v4

    #@12
    const/16 v5, 0x15

    #@14
    if-eq v4, v5, :cond_30

    #@16
    .line 533
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@18
    new-instance v5, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string/jumbo v6, "settings: "

    #@20
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v5

    #@24
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v5

    #@28
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v5

    #@2c
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2f
    throw v4

    #@30
    .line 535
    :cond_30
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@33
    move-result-object v0

    #@34
    .line 536
    .local v0, key:Ljava/lang/String;
    const-string v4, "EnvironmentalReverb"

    #@36
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39
    move-result v4

    #@3a
    if-nez v4, :cond_55

    #@3c
    .line 537
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@3e
    new-instance v5, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v6, "invalid settings for EnvironmentalReverb: "

    #@45
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v5

    #@49
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v5

    #@4d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v5

    #@51
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@54
    throw v4

    #@55
    .line 542
    :cond_55
    :try_start_55
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@58
    move-result-object v0

    #@59
    .line 543
    const-string/jumbo v4, "roomLevel"

    #@5c
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5f
    move-result v4

    #@60
    if-nez v4, :cond_95

    #@62
    .line 544
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@64
    new-instance v5, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v6, "invalid key name: "

    #@6b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v5

    #@6f
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v5

    #@73
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v5

    #@77
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@7a
    throw v4
    :try_end_7b
    .catch Ljava/lang/NumberFormatException; {:try_start_55 .. :try_end_7b} :catch_7b

    #@7b
    .line 592
    :catch_7b
    move-exception v1

    #@7c
    .line 593
    .local v1, nfe:Ljava/lang/NumberFormatException;
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@7e
    new-instance v5, Ljava/lang/StringBuilder;

    #@80
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@83
    const-string v6, "invalid value for key: "

    #@85
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v5

    #@89
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v5

    #@8d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@90
    move-result-object v5

    #@91
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@94
    throw v4

    #@95
    .line 546
    .end local v1           #nfe:Ljava/lang/NumberFormatException;
    :cond_95
    :try_start_95
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@98
    move-result-object v4

    #@99
    invoke-static {v4}, Ljava/lang/Short;->parseShort(Ljava/lang/String;)S

    #@9c
    move-result v4

    #@9d
    iput-short v4, p0, Landroid/media/audiofx/EnvironmentalReverb$Settings;->roomLevel:S

    #@9f
    .line 547
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@a2
    move-result-object v0

    #@a3
    .line 548
    const-string/jumbo v4, "roomHFLevel"

    #@a6
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a9
    move-result v4

    #@aa
    if-nez v4, :cond_c5

    #@ac
    .line 549
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@ae
    new-instance v5, Ljava/lang/StringBuilder;

    #@b0
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@b3
    const-string v6, "invalid key name: "

    #@b5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v5

    #@b9
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v5

    #@bd
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c0
    move-result-object v5

    #@c1
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@c4
    throw v4

    #@c5
    .line 551
    :cond_c5
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@c8
    move-result-object v4

    #@c9
    invoke-static {v4}, Ljava/lang/Short;->parseShort(Ljava/lang/String;)S

    #@cc
    move-result v4

    #@cd
    iput-short v4, p0, Landroid/media/audiofx/EnvironmentalReverb$Settings;->roomHFLevel:S

    #@cf
    .line 552
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@d2
    move-result-object v0

    #@d3
    .line 553
    const-string v4, "decayTime"

    #@d5
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d8
    move-result v4

    #@d9
    if-nez v4, :cond_f4

    #@db
    .line 554
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@dd
    new-instance v5, Ljava/lang/StringBuilder;

    #@df
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@e2
    const-string v6, "invalid key name: "

    #@e4
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v5

    #@e8
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v5

    #@ec
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ef
    move-result-object v5

    #@f0
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f3
    throw v4

    #@f4
    .line 556
    :cond_f4
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@f7
    move-result-object v4

    #@f8
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@fb
    move-result v4

    #@fc
    iput v4, p0, Landroid/media/audiofx/EnvironmentalReverb$Settings;->decayTime:I

    #@fe
    .line 557
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@101
    move-result-object v0

    #@102
    .line 558
    const-string v4, "decayHFRatio"

    #@104
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@107
    move-result v4

    #@108
    if-nez v4, :cond_123

    #@10a
    .line 559
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@10c
    new-instance v5, Ljava/lang/StringBuilder;

    #@10e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@111
    const-string v6, "invalid key name: "

    #@113
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@116
    move-result-object v5

    #@117
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11a
    move-result-object v5

    #@11b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11e
    move-result-object v5

    #@11f
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@122
    throw v4

    #@123
    .line 561
    :cond_123
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@126
    move-result-object v4

    #@127
    invoke-static {v4}, Ljava/lang/Short;->parseShort(Ljava/lang/String;)S

    #@12a
    move-result v4

    #@12b
    iput-short v4, p0, Landroid/media/audiofx/EnvironmentalReverb$Settings;->decayHFRatio:S

    #@12d
    .line 562
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@130
    move-result-object v0

    #@131
    .line 563
    const-string/jumbo v4, "reflectionsLevel"

    #@134
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@137
    move-result v4

    #@138
    if-nez v4, :cond_153

    #@13a
    .line 564
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@13c
    new-instance v5, Ljava/lang/StringBuilder;

    #@13e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@141
    const-string v6, "invalid key name: "

    #@143
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@146
    move-result-object v5

    #@147
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14a
    move-result-object v5

    #@14b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14e
    move-result-object v5

    #@14f
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@152
    throw v4

    #@153
    .line 566
    :cond_153
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@156
    move-result-object v4

    #@157
    invoke-static {v4}, Ljava/lang/Short;->parseShort(Ljava/lang/String;)S

    #@15a
    move-result v4

    #@15b
    iput-short v4, p0, Landroid/media/audiofx/EnvironmentalReverb$Settings;->reflectionsLevel:S

    #@15d
    .line 567
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@160
    move-result-object v0

    #@161
    .line 568
    const-string/jumbo v4, "reflectionsDelay"

    #@164
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@167
    move-result v4

    #@168
    if-nez v4, :cond_183

    #@16a
    .line 569
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@16c
    new-instance v5, Ljava/lang/StringBuilder;

    #@16e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@171
    const-string v6, "invalid key name: "

    #@173
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@176
    move-result-object v5

    #@177
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17a
    move-result-object v5

    #@17b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17e
    move-result-object v5

    #@17f
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@182
    throw v4

    #@183
    .line 571
    :cond_183
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@186
    move-result-object v4

    #@187
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@18a
    move-result v4

    #@18b
    iput v4, p0, Landroid/media/audiofx/EnvironmentalReverb$Settings;->reflectionsDelay:I

    #@18d
    .line 572
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@190
    move-result-object v0

    #@191
    .line 573
    const-string/jumbo v4, "reverbLevel"

    #@194
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@197
    move-result v4

    #@198
    if-nez v4, :cond_1b3

    #@19a
    .line 574
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@19c
    new-instance v5, Ljava/lang/StringBuilder;

    #@19e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1a1
    const-string v6, "invalid key name: "

    #@1a3
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a6
    move-result-object v5

    #@1a7
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1aa
    move-result-object v5

    #@1ab
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ae
    move-result-object v5

    #@1af
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b2
    throw v4

    #@1b3
    .line 576
    :cond_1b3
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@1b6
    move-result-object v4

    #@1b7
    invoke-static {v4}, Ljava/lang/Short;->parseShort(Ljava/lang/String;)S

    #@1ba
    move-result v4

    #@1bb
    iput-short v4, p0, Landroid/media/audiofx/EnvironmentalReverb$Settings;->reverbLevel:S

    #@1bd
    .line 577
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@1c0
    move-result-object v0

    #@1c1
    .line 578
    const-string/jumbo v4, "reverbDelay"

    #@1c4
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c7
    move-result v4

    #@1c8
    if-nez v4, :cond_1e3

    #@1ca
    .line 579
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@1cc
    new-instance v5, Ljava/lang/StringBuilder;

    #@1ce
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1d1
    const-string v6, "invalid key name: "

    #@1d3
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d6
    move-result-object v5

    #@1d7
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1da
    move-result-object v5

    #@1db
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1de
    move-result-object v5

    #@1df
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e2
    throw v4

    #@1e3
    .line 581
    :cond_1e3
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@1e6
    move-result-object v4

    #@1e7
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1ea
    move-result v4

    #@1eb
    iput v4, p0, Landroid/media/audiofx/EnvironmentalReverb$Settings;->reverbDelay:I

    #@1ed
    .line 582
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@1f0
    move-result-object v0

    #@1f1
    .line 583
    const-string v4, "diffusion"

    #@1f3
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f6
    move-result v4

    #@1f7
    if-nez v4, :cond_212

    #@1f9
    .line 584
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@1fb
    new-instance v5, Ljava/lang/StringBuilder;

    #@1fd
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@200
    const-string v6, "invalid key name: "

    #@202
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@205
    move-result-object v5

    #@206
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@209
    move-result-object v5

    #@20a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20d
    move-result-object v5

    #@20e
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@211
    throw v4

    #@212
    .line 586
    :cond_212
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@215
    move-result-object v4

    #@216
    invoke-static {v4}, Ljava/lang/Short;->parseShort(Ljava/lang/String;)S

    #@219
    move-result v4

    #@21a
    iput-short v4, p0, Landroid/media/audiofx/EnvironmentalReverb$Settings;->diffusion:S

    #@21c
    .line 587
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@21f
    move-result-object v0

    #@220
    .line 588
    const-string v4, "density"

    #@222
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@225
    move-result v4

    #@226
    if-nez v4, :cond_241

    #@228
    .line 589
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@22a
    new-instance v5, Ljava/lang/StringBuilder;

    #@22c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@22f
    const-string v6, "invalid key name: "

    #@231
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@234
    move-result-object v5

    #@235
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@238
    move-result-object v5

    #@239
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23c
    move-result-object v5

    #@23d
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@240
    throw v4

    #@241
    .line 591
    :cond_241
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@244
    move-result-object v4

    #@245
    invoke-static {v4}, Ljava/lang/Short;->parseShort(Ljava/lang/String;)S

    #@248
    move-result v4

    #@249
    iput-short v4, p0, Landroid/media/audiofx/EnvironmentalReverb$Settings;->density:S
    :try_end_24b
    .catch Ljava/lang/NumberFormatException; {:try_start_95 .. :try_end_24b} :catch_7b

    #@24b
    .line 595
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 599
    new-instance v0, Ljava/lang/String;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "EnvironmentalReverb;roomLevel="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-short v2, p0, Landroid/media/audiofx/EnvironmentalReverb$Settings;->roomLevel:S

    #@f
    invoke-static {v2}, Ljava/lang/Short;->toString(S)Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, ";roomHFLevel="

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    iget-short v2, p0, Landroid/media/audiofx/EnvironmentalReverb$Settings;->roomHFLevel:S

    #@1f
    invoke-static {v2}, Ljava/lang/Short;->toString(S)Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    const-string v2, ";decayTime="

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    iget v2, p0, Landroid/media/audiofx/EnvironmentalReverb$Settings;->decayTime:I

    #@2f
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    const-string v2, ";decayHFRatio="

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    iget-short v2, p0, Landroid/media/audiofx/EnvironmentalReverb$Settings;->decayHFRatio:S

    #@3f
    invoke-static {v2}, Ljava/lang/Short;->toString(S)Ljava/lang/String;

    #@42
    move-result-object v2

    #@43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    const-string v2, ";reflectionsLevel="

    #@49
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v1

    #@4d
    iget-short v2, p0, Landroid/media/audiofx/EnvironmentalReverb$Settings;->reflectionsLevel:S

    #@4f
    invoke-static {v2}, Ljava/lang/Short;->toString(S)Ljava/lang/String;

    #@52
    move-result-object v2

    #@53
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v1

    #@57
    const-string v2, ";reflectionsDelay="

    #@59
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v1

    #@5d
    iget v2, p0, Landroid/media/audiofx/EnvironmentalReverb$Settings;->reflectionsDelay:I

    #@5f
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@62
    move-result-object v2

    #@63
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v1

    #@67
    const-string v2, ";reverbLevel="

    #@69
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v1

    #@6d
    iget-short v2, p0, Landroid/media/audiofx/EnvironmentalReverb$Settings;->reverbLevel:S

    #@6f
    invoke-static {v2}, Ljava/lang/Short;->toString(S)Ljava/lang/String;

    #@72
    move-result-object v2

    #@73
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v1

    #@77
    const-string v2, ";reverbDelay="

    #@79
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v1

    #@7d
    iget v2, p0, Landroid/media/audiofx/EnvironmentalReverb$Settings;->reverbDelay:I

    #@7f
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@82
    move-result-object v2

    #@83
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v1

    #@87
    const-string v2, ";diffusion="

    #@89
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v1

    #@8d
    iget-short v2, p0, Landroid/media/audiofx/EnvironmentalReverb$Settings;->diffusion:S

    #@8f
    invoke-static {v2}, Ljava/lang/Short;->toString(S)Ljava/lang/String;

    #@92
    move-result-object v2

    #@93
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v1

    #@97
    const-string v2, ";density="

    #@99
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v1

    #@9d
    iget-short v2, p0, Landroid/media/audiofx/EnvironmentalReverb$Settings;->density:S

    #@9f
    invoke-static {v2}, Ljava/lang/Short;->toString(S)Ljava/lang/String;

    #@a2
    move-result-object v2

    #@a3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v1

    #@a7
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@aa
    move-result-object v1

    #@ab
    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@ae
    return-object v0
.end method
