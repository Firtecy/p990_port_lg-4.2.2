.class Landroid/media/audiofx/PresetReverb$BaseParameterListener;
.super Ljava/lang/Object;
.source "PresetReverb.java"

# interfaces
.implements Landroid/media/audiofx/AudioEffect$OnParameterChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/audiofx/PresetReverb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BaseParameterListener"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/media/audiofx/PresetReverb;


# direct methods
.method private constructor <init>(Landroid/media/audiofx/PresetReverb;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 191
    iput-object p1, p0, Landroid/media/audiofx/PresetReverb$BaseParameterListener;->this$0:Landroid/media/audiofx/PresetReverb;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 193
    return-void
.end method

.method synthetic constructor <init>(Landroid/media/audiofx/PresetReverb;Landroid/media/audiofx/PresetReverb$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 190
    invoke-direct {p0, p1}, Landroid/media/audiofx/PresetReverb$BaseParameterListener;-><init>(Landroid/media/audiofx/PresetReverb;)V

    #@3
    return-void
.end method


# virtual methods
.method public onParameterChange(Landroid/media/audiofx/AudioEffect;I[B[B)V
    .registers 12
    .parameter "effect"
    .parameter "status"
    .parameter "param"
    .parameter "value"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, -0x1

    #@2
    .line 195
    const/4 v0, 0x0

    #@3
    .line 197
    .local v0, l:Landroid/media/audiofx/PresetReverb$OnParameterChangeListener;
    iget-object v3, p0, Landroid/media/audiofx/PresetReverb$BaseParameterListener;->this$0:Landroid/media/audiofx/PresetReverb;

    #@5
    invoke-static {v3}, Landroid/media/audiofx/PresetReverb;->access$000(Landroid/media/audiofx/PresetReverb;)Ljava/lang/Object;

    #@8
    move-result-object v4

    #@9
    monitor-enter v4

    #@a
    .line 198
    :try_start_a
    iget-object v3, p0, Landroid/media/audiofx/PresetReverb$BaseParameterListener;->this$0:Landroid/media/audiofx/PresetReverb;

    #@c
    invoke-static {v3}, Landroid/media/audiofx/PresetReverb;->access$100(Landroid/media/audiofx/PresetReverb;)Landroid/media/audiofx/PresetReverb$OnParameterChangeListener;

    #@f
    move-result-object v3

    #@10
    if-eqz v3, :cond_18

    #@12
    .line 199
    iget-object v3, p0, Landroid/media/audiofx/PresetReverb$BaseParameterListener;->this$0:Landroid/media/audiofx/PresetReverb;

    #@14
    invoke-static {v3}, Landroid/media/audiofx/PresetReverb;->access$100(Landroid/media/audiofx/PresetReverb;)Landroid/media/audiofx/PresetReverb$OnParameterChangeListener;

    #@17
    move-result-object v0

    #@18
    .line 201
    :cond_18
    monitor-exit v4
    :try_end_19
    .catchall {:try_start_a .. :try_end_19} :catchall_3b

    #@19
    .line 202
    if-eqz v0, :cond_3a

    #@1b
    .line 203
    const/4 v1, -0x1

    #@1c
    .line 204
    .local v1, p:I
    const/4 v2, -0x1

    #@1d
    .line 206
    .local v2, v:S
    array-length v3, p3

    #@1e
    const/4 v4, 0x4

    #@1f
    if-ne v3, v4, :cond_27

    #@21
    .line 207
    iget-object v3, p0, Landroid/media/audiofx/PresetReverb$BaseParameterListener;->this$0:Landroid/media/audiofx/PresetReverb;

    #@23
    invoke-virtual {v3, p3, v6}, Landroid/media/audiofx/PresetReverb;->byteArrayToInt([BI)I

    #@26
    move-result v1

    #@27
    .line 209
    :cond_27
    array-length v3, p4

    #@28
    const/4 v4, 0x2

    #@29
    if-ne v3, v4, :cond_31

    #@2b
    .line 210
    iget-object v3, p0, Landroid/media/audiofx/PresetReverb$BaseParameterListener;->this$0:Landroid/media/audiofx/PresetReverb;

    #@2d
    invoke-virtual {v3, p4, v6}, Landroid/media/audiofx/PresetReverb;->byteArrayToShort([BI)S

    #@30
    move-result v2

    #@31
    .line 212
    :cond_31
    if-eq v1, v5, :cond_3a

    #@33
    if-eq v2, v5, :cond_3a

    #@35
    .line 213
    iget-object v3, p0, Landroid/media/audiofx/PresetReverb$BaseParameterListener;->this$0:Landroid/media/audiofx/PresetReverb;

    #@37
    invoke-interface {v0, v3, p2, v1, v2}, Landroid/media/audiofx/PresetReverb$OnParameterChangeListener;->onParameterChange(Landroid/media/audiofx/PresetReverb;IIS)V

    #@3a
    .line 216
    .end local v1           #p:I
    .end local v2           #v:S
    :cond_3a
    return-void

    #@3b
    .line 201
    :catchall_3b
    move-exception v3

    #@3c
    :try_start_3c
    monitor-exit v4
    :try_end_3d
    .catchall {:try_start_3c .. :try_end_3d} :catchall_3b

    #@3d
    throw v3
.end method
