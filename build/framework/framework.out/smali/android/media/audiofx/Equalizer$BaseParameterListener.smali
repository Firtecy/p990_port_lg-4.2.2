.class Landroid/media/audiofx/Equalizer$BaseParameterListener;
.super Ljava/lang/Object;
.source "Equalizer.java"

# interfaces
.implements Landroid/media/audiofx/AudioEffect$OnParameterChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/audiofx/Equalizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BaseParameterListener"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/media/audiofx/Equalizer;


# direct methods
.method private constructor <init>(Landroid/media/audiofx/Equalizer;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 400
    iput-object p1, p0, Landroid/media/audiofx/Equalizer$BaseParameterListener;->this$0:Landroid/media/audiofx/Equalizer;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 402
    return-void
.end method

.method synthetic constructor <init>(Landroid/media/audiofx/Equalizer;Landroid/media/audiofx/Equalizer$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 399
    invoke-direct {p0, p1}, Landroid/media/audiofx/Equalizer$BaseParameterListener;-><init>(Landroid/media/audiofx/Equalizer;)V

    #@3
    return-void
.end method


# virtual methods
.method public onParameterChange(Landroid/media/audiofx/AudioEffect;I[B[B)V
    .registers 14
    .parameter "effect"
    .parameter "status"
    .parameter "param"
    .parameter "value"

    #@0
    .prologue
    const/4 v8, -0x1

    #@1
    const/4 v7, 0x4

    #@2
    const/4 v6, 0x0

    #@3
    .line 404
    const/4 v0, 0x0

    #@4
    .line 406
    .local v0, l:Landroid/media/audiofx/Equalizer$OnParameterChangeListener;
    iget-object v1, p0, Landroid/media/audiofx/Equalizer$BaseParameterListener;->this$0:Landroid/media/audiofx/Equalizer;

    #@6
    invoke-static {v1}, Landroid/media/audiofx/Equalizer;->access$000(Landroid/media/audiofx/Equalizer;)Ljava/lang/Object;

    #@9
    move-result-object v2

    #@a
    monitor-enter v2

    #@b
    .line 407
    :try_start_b
    iget-object v1, p0, Landroid/media/audiofx/Equalizer$BaseParameterListener;->this$0:Landroid/media/audiofx/Equalizer;

    #@d
    invoke-static {v1}, Landroid/media/audiofx/Equalizer;->access$100(Landroid/media/audiofx/Equalizer;)Landroid/media/audiofx/Equalizer$OnParameterChangeListener;

    #@10
    move-result-object v1

    #@11
    if-eqz v1, :cond_19

    #@13
    .line 408
    iget-object v1, p0, Landroid/media/audiofx/Equalizer$BaseParameterListener;->this$0:Landroid/media/audiofx/Equalizer;

    #@15
    invoke-static {v1}, Landroid/media/audiofx/Equalizer;->access$100(Landroid/media/audiofx/Equalizer;)Landroid/media/audiofx/Equalizer$OnParameterChangeListener;

    #@18
    move-result-object v0

    #@19
    .line 410
    :cond_19
    monitor-exit v2
    :try_end_1a
    .catchall {:try_start_b .. :try_end_1a} :catchall_48

    #@1a
    .line 411
    if-eqz v0, :cond_47

    #@1c
    .line 412
    const/4 v3, -0x1

    #@1d
    .line 413
    .local v3, p1:I
    const/4 v4, -0x1

    #@1e
    .line 414
    .local v4, p2:I
    const/4 v5, -0x1

    #@1f
    .line 416
    .local v5, v:I
    array-length v1, p3

    #@20
    if-lt v1, v7, :cond_33

    #@22
    .line 417
    iget-object v1, p0, Landroid/media/audiofx/Equalizer$BaseParameterListener;->this$0:Landroid/media/audiofx/Equalizer;

    #@24
    invoke-virtual {v1, p3, v6}, Landroid/media/audiofx/Equalizer;->byteArrayToInt([BI)I

    #@27
    move-result v3

    #@28
    .line 418
    array-length v1, p3

    #@29
    const/16 v2, 0x8

    #@2b
    if-lt v1, v2, :cond_33

    #@2d
    .line 419
    iget-object v1, p0, Landroid/media/audiofx/Equalizer$BaseParameterListener;->this$0:Landroid/media/audiofx/Equalizer;

    #@2f
    invoke-virtual {v1, p3, v7}, Landroid/media/audiofx/Equalizer;->byteArrayToInt([BI)I

    #@32
    move-result v4

    #@33
    .line 422
    :cond_33
    array-length v1, p4

    #@34
    const/4 v2, 0x2

    #@35
    if-ne v1, v2, :cond_4b

    #@37
    .line 423
    iget-object v1, p0, Landroid/media/audiofx/Equalizer$BaseParameterListener;->this$0:Landroid/media/audiofx/Equalizer;

    #@39
    invoke-virtual {v1, p4, v6}, Landroid/media/audiofx/Equalizer;->byteArrayToShort([BI)S

    #@3c
    move-result v5

    #@3d
    .line 428
    :cond_3d
    :goto_3d
    if-eq v3, v8, :cond_47

    #@3f
    if-eq v5, v8, :cond_47

    #@41
    .line 429
    iget-object v1, p0, Landroid/media/audiofx/Equalizer$BaseParameterListener;->this$0:Landroid/media/audiofx/Equalizer;

    #@43
    move v2, p2

    #@44
    invoke-interface/range {v0 .. v5}, Landroid/media/audiofx/Equalizer$OnParameterChangeListener;->onParameterChange(Landroid/media/audiofx/Equalizer;IIII)V

    #@47
    .line 432
    .end local v3           #p1:I
    .end local v4           #p2:I
    .end local v5           #v:I
    :cond_47
    return-void

    #@48
    .line 410
    :catchall_48
    move-exception v1

    #@49
    :try_start_49
    monitor-exit v2
    :try_end_4a
    .catchall {:try_start_49 .. :try_end_4a} :catchall_48

    #@4a
    throw v1

    #@4b
    .line 424
    .restart local v3       #p1:I
    .restart local v4       #p2:I
    .restart local v5       #v:I
    :cond_4b
    array-length v1, p4

    #@4c
    if-ne v1, v7, :cond_3d

    #@4e
    .line 425
    iget-object v1, p0, Landroid/media/audiofx/Equalizer$BaseParameterListener;->this$0:Landroid/media/audiofx/Equalizer;

    #@50
    invoke-virtual {v1, p4, v6}, Landroid/media/audiofx/Equalizer;->byteArrayToInt([BI)I

    #@53
    move-result v5

    #@54
    goto :goto_3d
.end method
