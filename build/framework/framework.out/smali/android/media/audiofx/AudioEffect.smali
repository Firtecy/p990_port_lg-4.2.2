.class public Landroid/media/audiofx/AudioEffect;
.super Ljava/lang/Object;
.source "AudioEffect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/audiofx/AudioEffect$NativeEventHandler;,
        Landroid/media/audiofx/AudioEffect$OnParameterChangeListener;,
        Landroid/media/audiofx/AudioEffect$OnControlStatusChangeListener;,
        Landroid/media/audiofx/AudioEffect$OnEnableStatusChangeListener;,
        Landroid/media/audiofx/AudioEffect$Descriptor;
    }
.end annotation


# static fields
.field public static final ACTION_CLOSE_AUDIO_EFFECT_CONTROL_SESSION:Ljava/lang/String; = "android.media.action.CLOSE_AUDIO_EFFECT_CONTROL_SESSION"

.field public static final ACTION_DISPLAY_AUDIO_EFFECT_CONTROL_PANEL:Ljava/lang/String; = "android.media.action.DISPLAY_AUDIO_EFFECT_CONTROL_PANEL"

.field public static final ACTION_OPEN_AUDIO_EFFECT_CONTROL_SESSION:Ljava/lang/String; = "android.media.action.OPEN_AUDIO_EFFECT_CONTROL_SESSION"

.field public static final ALREADY_EXISTS:I = -0x2

.field public static final CONTENT_TYPE_GAME:I = 0x2

.field public static final CONTENT_TYPE_MOVIE:I = 0x1

.field public static final CONTENT_TYPE_MUSIC:I = 0x0

.field public static final CONTENT_TYPE_VOICE:I = 0x3

.field public static final EFFECT_AUXILIARY:Ljava/lang/String; = "Auxiliary"

.field public static final EFFECT_INSERT:Ljava/lang/String; = "Insert"

.field public static final EFFECT_PRE_PROCESSING:Ljava/lang/String; = "Pre Processing"

.field public static final EFFECT_TYPE_AEC:Ljava/util/UUID; = null

.field public static final EFFECT_TYPE_AGC:Ljava/util/UUID; = null

.field public static final EFFECT_TYPE_BASS_BOOST:Ljava/util/UUID; = null

.field public static final EFFECT_TYPE_ENV_REVERB:Ljava/util/UUID; = null

.field public static final EFFECT_TYPE_EQUALIZER:Ljava/util/UUID; = null

.field public static final EFFECT_TYPE_NS:Ljava/util/UUID; = null

.field public static final EFFECT_TYPE_NULL:Ljava/util/UUID; = null

.field public static final EFFECT_TYPE_PRESET_REVERB:Ljava/util/UUID; = null

.field public static final EFFECT_TYPE_VIRTUALIZER:Ljava/util/UUID; = null

.field public static final ERROR:I = -0x1

.field public static final ERROR_BAD_VALUE:I = -0x4

.field public static final ERROR_DEAD_OBJECT:I = -0x7

.field public static final ERROR_INVALID_OPERATION:I = -0x5

.field public static final ERROR_NO_INIT:I = -0x3

.field public static final ERROR_NO_MEMORY:I = -0x6

.field public static final EXTRA_AUDIO_SESSION:Ljava/lang/String; = "android.media.extra.AUDIO_SESSION"

.field public static final EXTRA_CONTENT_TYPE:Ljava/lang/String; = "android.media.extra.CONTENT_TYPE"

.field public static final EXTRA_PACKAGE_NAME:Ljava/lang/String; = "android.media.extra.PACKAGE_NAME"

.field public static final NATIVE_EVENT_CONTROL_STATUS:I = 0x0

.field public static final NATIVE_EVENT_ENABLED_STATUS:I = 0x1

.field public static final NATIVE_EVENT_PARAMETER_CHANGED:I = 0x2

.field public static final STATE_INITIALIZED:I = 0x1

.field public static final STATE_UNINITIALIZED:I = 0x0

.field public static final SUCCESS:I = 0x0

.field private static final TAG:Ljava/lang/String; = "AudioEffect-JAVA"


# instance fields
.field private mControlChangeStatusListener:Landroid/media/audiofx/AudioEffect$OnControlStatusChangeListener;

.field private mDescriptor:Landroid/media/audiofx/AudioEffect$Descriptor;

.field private mEnableStatusChangeListener:Landroid/media/audiofx/AudioEffect$OnEnableStatusChangeListener;

.field private mId:I

.field private mJniData:I

.field public final mListenerLock:Ljava/lang/Object;

.field private mNativeAudioEffect:I

.field public mNativeEventHandler:Landroid/media/audiofx/AudioEffect$NativeEventHandler;

.field private mParameterChangeListener:Landroid/media/audiofx/AudioEffect$OnParameterChangeListener;

.field private mState:I

.field private final mStateLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 61
    const-string v0, "audioeffect_jni"

    #@2
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@5
    .line 62
    invoke-static {}, Landroid/media/audiofx/AudioEffect;->native_init()V

    #@8
    .line 80
    const-string v0, "c2e5d5f0-94bd-4763-9cac-4e234d06839e"

    #@a
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    #@d
    move-result-object v0

    #@e
    sput-object v0, Landroid/media/audiofx/AudioEffect;->EFFECT_TYPE_ENV_REVERB:Ljava/util/UUID;

    #@10
    .line 86
    const-string v0, "47382d60-ddd8-11db-bf3a-0002a5d5c51b"

    #@12
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    #@15
    move-result-object v0

    #@16
    sput-object v0, Landroid/media/audiofx/AudioEffect;->EFFECT_TYPE_PRESET_REVERB:Ljava/util/UUID;

    #@18
    .line 92
    const-string v0, "0bed4300-ddd6-11db-8f34-0002a5d5c51b"

    #@1a
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    #@1d
    move-result-object v0

    #@1e
    sput-object v0, Landroid/media/audiofx/AudioEffect;->EFFECT_TYPE_EQUALIZER:Ljava/util/UUID;

    #@20
    .line 98
    const-string v0, "0634f220-ddd4-11db-a0fc-0002a5d5c51b"

    #@22
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    #@25
    move-result-object v0

    #@26
    sput-object v0, Landroid/media/audiofx/AudioEffect;->EFFECT_TYPE_BASS_BOOST:Ljava/util/UUID;

    #@28
    .line 104
    const-string v0, "37cc2c00-dddd-11db-8577-0002a5d5c51b"

    #@2a
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    #@2d
    move-result-object v0

    #@2e
    sput-object v0, Landroid/media/audiofx/AudioEffect;->EFFECT_TYPE_VIRTUALIZER:Ljava/util/UUID;

    #@30
    .line 111
    const-string v0, "0a8abfe0-654c-11e0-ba26-0002a5d5c51b"

    #@32
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    #@35
    move-result-object v0

    #@36
    sput-object v0, Landroid/media/audiofx/AudioEffect;->EFFECT_TYPE_AGC:Ljava/util/UUID;

    #@38
    .line 118
    const-string v0, "7b491460-8d4d-11e0-bd61-0002a5d5c51b"

    #@3a
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    #@3d
    move-result-object v0

    #@3e
    sput-object v0, Landroid/media/audiofx/AudioEffect;->EFFECT_TYPE_AEC:Ljava/util/UUID;

    #@40
    .line 125
    const-string v0, "58b4b260-8e06-11e0-aa8e-0002a5d5c51b"

    #@42
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    #@45
    move-result-object v0

    #@46
    sput-object v0, Landroid/media/audiofx/AudioEffect;->EFFECT_TYPE_NS:Ljava/util/UUID;

    #@48
    .line 132
    const-string v0, "ec7178ec-e5e1-4432-a3f4-4657e6795210"

    #@4a
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    #@4d
    move-result-object v0

    #@4e
    sput-object v0, Landroid/media/audiofx/AudioEffect;->EFFECT_TYPE_NULL:Ljava/util/UUID;

    #@50
    return-void
.end method

.method public constructor <init>(Ljava/util/UUID;Ljava/util/UUID;II)V
    .registers 15
    .parameter "type"
    .parameter "uuid"
    .parameter "priority"
    .parameter "audioSession"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/UnsupportedOperationException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v9, 0x0

    #@2
    const/4 v1, 0x0

    #@3
    .line 369
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 283
    iput v9, p0, Landroid/media/audiofx/AudioEffect;->mState:I

    #@8
    .line 287
    new-instance v0, Ljava/lang/Object;

    #@a
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@d
    iput-object v0, p0, Landroid/media/audiofx/AudioEffect;->mStateLock:Ljava/lang/Object;

    #@f
    .line 307
    iput-object v1, p0, Landroid/media/audiofx/AudioEffect;->mEnableStatusChangeListener:Landroid/media/audiofx/AudioEffect$OnEnableStatusChangeListener;

    #@11
    .line 313
    iput-object v1, p0, Landroid/media/audiofx/AudioEffect;->mControlChangeStatusListener:Landroid/media/audiofx/AudioEffect$OnControlStatusChangeListener;

    #@13
    .line 319
    iput-object v1, p0, Landroid/media/audiofx/AudioEffect;->mParameterChangeListener:Landroid/media/audiofx/AudioEffect$OnParameterChangeListener;

    #@15
    .line 324
    new-instance v0, Ljava/lang/Object;

    #@17
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@1a
    iput-object v0, p0, Landroid/media/audiofx/AudioEffect;->mListenerLock:Ljava/lang/Object;

    #@1c
    .line 329
    iput-object v1, p0, Landroid/media/audiofx/AudioEffect;->mNativeEventHandler:Landroid/media/audiofx/AudioEffect$NativeEventHandler;

    #@1e
    .line 370
    new-array v6, v2, [I

    #@20
    .line 371
    .local v6, id:[I
    new-array v7, v2, [Landroid/media/audiofx/AudioEffect$Descriptor;

    #@22
    .line 373
    .local v7, desc:[Landroid/media/audiofx/AudioEffect$Descriptor;
    new-instance v1, Ljava/lang/ref/WeakReference;

    #@24
    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@27
    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {p2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    move-object v0, p0

    #@30
    move v4, p3

    #@31
    move v5, p4

    #@32
    invoke-direct/range {v0 .. v7}, Landroid/media/audiofx/AudioEffect;->native_setup(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II[I[Ljava/lang/Object;)I

    #@35
    move-result v8

    #@36
    .line 376
    .local v8, initResult:I
    if-eqz v8, :cond_a6

    #@38
    const/4 v0, -0x2

    #@39
    if-eq v8, v0, :cond_a6

    #@3b
    .line 377
    const-string v0, "AudioEffect-JAVA"

    #@3d
    new-instance v1, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v2, "Error code "

    #@44
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v1

    #@48
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v1

    #@4c
    const-string v2, " when initializing AudioEffect."

    #@4e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v1

    #@52
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v1

    #@56
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    .line 379
    packed-switch v8, :pswitch_data_ba

    #@5c
    .line 387
    new-instance v0, Ljava/lang/RuntimeException;

    #@5e
    new-instance v1, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string v2, "Cannot initialize effect engine for type: "

    #@65
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v1

    #@69
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v1

    #@6d
    const-string v2, " Error: "

    #@6f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v1

    #@73
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@76
    move-result-object v1

    #@77
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v1

    #@7b
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@7e
    throw v0

    #@7f
    .line 381
    :pswitch_7f
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@81
    new-instance v1, Ljava/lang/StringBuilder;

    #@83
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@86
    const-string v2, "Effect type: "

    #@88
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v1

    #@8c
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v1

    #@90
    const-string v2, " not supported."

    #@92
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v1

    #@96
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@99
    move-result-object v1

    #@9a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9d
    throw v0

    #@9e
    .line 384
    :pswitch_9e
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@a0
    const-string v1, "Effect library not loaded"

    #@a2
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@a5
    throw v0

    #@a6
    .line 392
    :cond_a6
    aget v0, v6, v9

    #@a8
    iput v0, p0, Landroid/media/audiofx/AudioEffect;->mId:I

    #@aa
    .line 393
    aget-object v0, v7, v9

    #@ac
    iput-object v0, p0, Landroid/media/audiofx/AudioEffect;->mDescriptor:Landroid/media/audiofx/AudioEffect$Descriptor;

    #@ae
    .line 394
    iget-object v1, p0, Landroid/media/audiofx/AudioEffect;->mStateLock:Ljava/lang/Object;

    #@b0
    monitor-enter v1

    #@b1
    .line 395
    const/4 v0, 0x1

    #@b2
    :try_start_b2
    iput v0, p0, Landroid/media/audiofx/AudioEffect;->mState:I

    #@b4
    .line 396
    monitor-exit v1

    #@b5
    .line 397
    return-void

    #@b6
    .line 396
    :catchall_b6
    move-exception v0

    #@b7
    monitor-exit v1
    :try_end_b8
    .catchall {:try_start_b2 .. :try_end_b8} :catchall_b6

    #@b8
    throw v0

    #@b9
    .line 379
    nop

    #@ba
    :pswitch_data_ba
    .packed-switch -0x5
        :pswitch_9e
        :pswitch_7f
    .end packed-switch
.end method

.method static synthetic access$000(Landroid/media/audiofx/AudioEffect;)Landroid/media/audiofx/AudioEffect$OnEnableStatusChangeListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Landroid/media/audiofx/AudioEffect;->mEnableStatusChangeListener:Landroid/media/audiofx/AudioEffect$OnEnableStatusChangeListener;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/media/audiofx/AudioEffect;)Landroid/media/audiofx/AudioEffect$OnControlStatusChangeListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Landroid/media/audiofx/AudioEffect;->mControlChangeStatusListener:Landroid/media/audiofx/AudioEffect$OnControlStatusChangeListener;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/media/audiofx/AudioEffect;)Landroid/media/audiofx/AudioEffect$OnParameterChangeListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Landroid/media/audiofx/AudioEffect;->mParameterChangeListener:Landroid/media/audiofx/AudioEffect$OnParameterChangeListener;

    #@2
    return-object v0
.end method

.method private createNativeEventHandler()V
    .registers 3

    #@0
    .prologue
    .line 918
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@3
    move-result-object v0

    #@4
    .local v0, looper:Landroid/os/Looper;
    if-eqz v0, :cond_e

    #@6
    .line 919
    new-instance v1, Landroid/media/audiofx/AudioEffect$NativeEventHandler;

    #@8
    invoke-direct {v1, p0, p0, v0}, Landroid/media/audiofx/AudioEffect$NativeEventHandler;-><init>(Landroid/media/audiofx/AudioEffect;Landroid/media/audiofx/AudioEffect;Landroid/os/Looper;)V

    #@b
    iput-object v1, p0, Landroid/media/audiofx/AudioEffect;->mNativeEventHandler:Landroid/media/audiofx/AudioEffect$NativeEventHandler;

    #@d
    .line 925
    :goto_d
    return-void

    #@e
    .line 920
    :cond_e
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    #@11
    move-result-object v0

    #@12
    if-eqz v0, :cond_1c

    #@14
    .line 921
    new-instance v1, Landroid/media/audiofx/AudioEffect$NativeEventHandler;

    #@16
    invoke-direct {v1, p0, p0, v0}, Landroid/media/audiofx/AudioEffect$NativeEventHandler;-><init>(Landroid/media/audiofx/AudioEffect;Landroid/media/audiofx/AudioEffect;Landroid/os/Looper;)V

    #@19
    iput-object v1, p0, Landroid/media/audiofx/AudioEffect;->mNativeEventHandler:Landroid/media/audiofx/AudioEffect$NativeEventHandler;

    #@1b
    goto :goto_d

    #@1c
    .line 923
    :cond_1c
    const/4 v1, 0x0

    #@1d
    iput-object v1, p0, Landroid/media/audiofx/AudioEffect;->mNativeEventHandler:Landroid/media/audiofx/AudioEffect$NativeEventHandler;

    #@1f
    goto :goto_d
.end method

.method public static isEffectTypeAvailable(Ljava/util/UUID;)Z
    .registers 4
    .parameter "type"

    #@0
    .prologue
    .line 462
    invoke-static {}, Landroid/media/audiofx/AudioEffect;->queryEffects()[Landroid/media/audiofx/AudioEffect$Descriptor;

    #@3
    move-result-object v0

    #@4
    .line 463
    .local v0, desc:[Landroid/media/audiofx/AudioEffect$Descriptor;
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    array-length v2, v0

    #@6
    if-ge v1, v2, :cond_17

    #@8
    .line 464
    aget-object v2, v0, v1

    #@a
    iget-object v2, v2, Landroid/media/audiofx/AudioEffect$Descriptor;->type:Ljava/util/UUID;

    #@c
    invoke-virtual {v2, p0}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v2

    #@10
    if-eqz v2, :cond_14

    #@12
    .line 465
    const/4 v2, 0x1

    #@13
    .line 468
    :goto_13
    return v2

    #@14
    .line 463
    :cond_14
    add-int/lit8 v1, v1, 0x1

    #@16
    goto :goto_5

    #@17
    .line 468
    :cond_17
    const/4 v2, 0x0

    #@18
    goto :goto_13
.end method

.method public static isError(I)Z
    .registers 2
    .parameter "status"

    #@0
    .prologue
    .line 1256
    if-gez p0, :cond_4

    #@2
    const/4 v0, 0x1

    #@3
    :goto_3
    return v0

    #@4
    :cond_4
    const/4 v0, 0x0

    #@5
    goto :goto_3
.end method

.method private final native native_command(II[BI[B)I
.end method

.method private final native native_finalize()V
.end method

.method private final native native_getEnabled()Z
.end method

.method private final native native_getParameter(I[BI[B)I
.end method

.method private final native native_hasControl()Z
.end method

.method private static final native native_init()V
.end method

.method private static native native_query_effects()[Ljava/lang/Object;
.end method

.method private static native native_query_pre_processing(I)[Ljava/lang/Object;
.end method

.method private final native native_release()V
.end method

.method private final native native_setEnabled(Z)I
.end method

.method private final native native_setParameter(I[BI[B)I
.end method

.method private final native native_setup(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;II[I[Ljava/lang/Object;)I
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .registers 8
    .parameter "effect_ref"
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"
    .parameter "obj"

    #@0
    .prologue
    .line 1174
    check-cast p0, Ljava/lang/ref/WeakReference;

    #@2
    .end local p0
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/media/audiofx/AudioEffect;

    #@8
    .line 1175
    .local v0, effect:Landroid/media/audiofx/AudioEffect;
    if-nez v0, :cond_b

    #@a
    .line 1184
    :cond_a
    :goto_a
    return-void

    #@b
    .line 1178
    :cond_b
    iget-object v2, v0, Landroid/media/audiofx/AudioEffect;->mNativeEventHandler:Landroid/media/audiofx/AudioEffect$NativeEventHandler;

    #@d
    if-eqz v2, :cond_a

    #@f
    .line 1179
    iget-object v2, v0, Landroid/media/audiofx/AudioEffect;->mNativeEventHandler:Landroid/media/audiofx/AudioEffect$NativeEventHandler;

    #@11
    invoke-virtual {v2, p1, p2, p3, p4}, Landroid/media/audiofx/AudioEffect$NativeEventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@14
    move-result-object v1

    #@15
    .line 1181
    .local v1, m:Landroid/os/Message;
    iget-object v2, v0, Landroid/media/audiofx/AudioEffect;->mNativeEventHandler:Landroid/media/audiofx/AudioEffect$NativeEventHandler;

    #@17
    invoke-virtual {v2, v1}, Landroid/media/audiofx/AudioEffect$NativeEventHandler;->sendMessage(Landroid/os/Message;)Z

    #@1a
    goto :goto_a
.end method

.method public static queryEffects()[Landroid/media/audiofx/AudioEffect$Descriptor;
    .registers 1

    #@0
    .prologue
    .line 439
    invoke-static {}, Landroid/media/audiofx/AudioEffect;->native_query_effects()[Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    check-cast v0, [Landroid/media/audiofx/AudioEffect$Descriptor;

    #@6
    check-cast v0, [Landroid/media/audiofx/AudioEffect$Descriptor;

    #@8
    return-object v0
.end method

.method public static queryPreProcessings(I)[Landroid/media/audiofx/AudioEffect$Descriptor;
    .registers 2
    .parameter "audioSession"

    #@0
    .prologue
    .line 452
    invoke-static {p0}, Landroid/media/audiofx/AudioEffect;->native_query_pre_processing(I)[Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    check-cast v0, [Landroid/media/audiofx/AudioEffect$Descriptor;

    #@6
    check-cast v0, [Landroid/media/audiofx/AudioEffect$Descriptor;

    #@8
    return-object v0
.end method


# virtual methods
.method public byteArrayToInt([B)I
    .registers 3
    .parameter "valueBuf"

    #@0
    .prologue
    .line 1263
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/media/audiofx/AudioEffect;->byteArrayToInt([BI)I

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public byteArrayToInt([BI)I
    .registers 5
    .parameter "valueBuf"
    .parameter "offset"

    #@0
    .prologue
    .line 1271
    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    #@3
    move-result-object v0

    #@4
    .line 1272
    .local v0, converter:Ljava/nio/ByteBuffer;
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@b
    .line 1273
    invoke-virtual {v0, p2}, Ljava/nio/ByteBuffer;->getInt(I)I

    #@e
    move-result v1

    #@f
    return v1
.end method

.method public byteArrayToShort([B)S
    .registers 3
    .parameter "valueBuf"

    #@0
    .prologue
    .line 1291
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/media/audiofx/AudioEffect;->byteArrayToShort([BI)S

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public byteArrayToShort([BI)S
    .registers 5
    .parameter "valueBuf"
    .parameter "offset"

    #@0
    .prologue
    .line 1298
    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    #@3
    move-result-object v0

    #@4
    .line 1299
    .local v0, converter:Ljava/nio/ByteBuffer;
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@b
    .line 1300
    invoke-virtual {v0, p2}, Ljava/nio/ByteBuffer;->getShort(I)S

    #@e
    move-result v1

    #@f
    return v1
.end method

.method public checkState(Ljava/lang/String;)V
    .registers 6
    .parameter "methodName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1226
    iget-object v1, p0, Landroid/media/audiofx/AudioEffect;->mStateLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 1227
    :try_start_3
    iget v0, p0, Landroid/media/audiofx/AudioEffect;->mState:I

    #@5
    const/4 v2, 0x1

    #@6
    if-eq v0, v2, :cond_24

    #@8
    .line 1228
    new-instance v0, Ljava/lang/IllegalStateException;

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    const-string v3, " called on uninitialized AudioEffect."

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@20
    throw v0

    #@21
    .line 1231
    :catchall_21
    move-exception v0

    #@22
    monitor-exit v1
    :try_end_23
    .catchall {:try_start_3 .. :try_end_23} :catchall_21

    #@23
    throw v0

    #@24
    :cond_24
    :try_start_24
    monitor-exit v1
    :try_end_25
    .catchall {:try_start_24 .. :try_end_25} :catchall_21

    #@25
    .line 1232
    return-void
.end method

.method public checkStatus(I)V
    .registers 4
    .parameter "status"

    #@0
    .prologue
    .line 1238
    invoke-static {p1}, Landroid/media/audiofx/AudioEffect;->isError(I)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_21

    #@6
    .line 1239
    packed-switch p1, :pswitch_data_22

    #@9
    .line 1247
    new-instance v0, Ljava/lang/RuntimeException;

    #@b
    const-string v1, "AudioEffect: set/get parameter error"

    #@d
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0

    #@11
    .line 1241
    :pswitch_11
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@13
    const-string v1, "AudioEffect: bad parameter value"

    #@15
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@18
    throw v0

    #@19
    .line 1244
    :pswitch_19
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@1b
    const-string v1, "AudioEffect: invalid parameter operation"

    #@1d
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@20
    throw v0

    #@21
    .line 1250
    :cond_21
    return-void

    #@22
    .line 1239
    :pswitch_data_22
    .packed-switch -0x5
        :pswitch_19
        :pswitch_11
    .end packed-switch
.end method

.method public command(I[B[B)I
    .registers 10
    .parameter "cmdCode"
    .parameter "command"
    .parameter "reply"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 820
    const-string v0, "command()"

    #@2
    invoke-virtual {p0, v0}, Landroid/media/audiofx/AudioEffect;->checkState(Ljava/lang/String;)V

    #@5
    .line 821
    array-length v2, p2

    #@6
    array-length v4, p3

    #@7
    move-object v0, p0

    #@8
    move v1, p1

    #@9
    move-object v3, p2

    #@a
    move-object v5, p3

    #@b
    invoke-direct/range {v0 .. v5}, Landroid/media/audiofx/AudioEffect;->native_command(II[BI[B)I

    #@e
    move-result v0

    #@f
    return v0
.end method

.method public varargs concatArrays([[B)[B
    .registers 11
    .parameter "arrays"

    #@0
    .prologue
    .line 1319
    const/4 v4, 0x0

    #@1
    .line 1320
    .local v4, len:I
    move-object v1, p1

    #@2
    .local v1, arr$:[[B
    array-length v5, v1

    #@3
    .local v5, len$:I
    const/4 v3, 0x0

    #@4
    .local v3, i$:I
    :goto_4
    if-ge v3, v5, :cond_d

    #@6
    aget-object v0, v1, v3

    #@8
    .line 1321
    .local v0, a:[B
    array-length v7, v0

    #@9
    add-int/2addr v4, v7

    #@a
    .line 1320
    add-int/lit8 v3, v3, 0x1

    #@c
    goto :goto_4

    #@d
    .line 1323
    .end local v0           #a:[B
    :cond_d
    new-array v2, v4, [B

    #@f
    .line 1325
    .local v2, b:[B
    const/4 v6, 0x0

    #@10
    .line 1326
    .local v6, offs:I
    move-object v1, p1

    #@11
    array-length v5, v1

    #@12
    const/4 v3, 0x0

    #@13
    :goto_13
    if-ge v3, v5, :cond_21

    #@15
    aget-object v0, v1, v3

    #@17
    .line 1327
    .restart local v0       #a:[B
    const/4 v7, 0x0

    #@18
    array-length v8, v0

    #@19
    invoke-static {v0, v7, v2, v6, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@1c
    .line 1328
    array-length v7, v0

    #@1d
    add-int/2addr v6, v7

    #@1e
    .line 1326
    add-int/lit8 v3, v3, 0x1

    #@20
    goto :goto_13

    #@21
    .line 1330
    .end local v0           #a:[B
    :cond_21
    return-object v2
.end method

.method protected finalize()V
    .registers 1

    #@0
    .prologue
    .line 413
    invoke-direct {p0}, Landroid/media/audiofx/AudioEffect;->native_finalize()V

    #@3
    .line 414
    return-void
.end method

.method public getDescriptor()Landroid/media/audiofx/AudioEffect$Descriptor;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 423
    const-string v0, "getDescriptor()"

    #@2
    invoke-virtual {p0, v0}, Landroid/media/audiofx/AudioEffect;->checkState(Ljava/lang/String;)V

    #@5
    .line 424
    iget-object v0, p0, Landroid/media/audiofx/AudioEffect;->mDescriptor:Landroid/media/audiofx/AudioEffect$Descriptor;

    #@7
    return-object v0
.end method

.method public getEnabled()Z
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 848
    const-string v0, "getEnabled()"

    #@2
    invoke-virtual {p0, v0}, Landroid/media/audiofx/AudioEffect;->checkState(Ljava/lang/String;)V

    #@5
    .line 849
    invoke-direct {p0}, Landroid/media/audiofx/AudioEffect;->native_getEnabled()Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public getId()I
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 837
    const-string v0, "getId()"

    #@2
    invoke-virtual {p0, v0}, Landroid/media/audiofx/AudioEffect;->checkState(Ljava/lang/String;)V

    #@5
    .line 838
    iget v0, p0, Landroid/media/audiofx/AudioEffect;->mId:I

    #@7
    return v0
.end method

.method public getParameter(I[B)I
    .registers 5
    .parameter "param"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 655
    invoke-virtual {p0, p1}, Landroid/media/audiofx/AudioEffect;->intToByteArray(I)[B

    #@3
    move-result-object v0

    #@4
    .line 657
    .local v0, p:[B
    invoke-virtual {p0, v0, p2}, Landroid/media/audiofx/AudioEffect;->getParameter([B[B)I

    #@7
    move-result v1

    #@8
    return v1
.end method

.method public getParameter(I[I)I
    .registers 10
    .parameter "param"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v6, 0x8

    #@2
    const/4 v5, 0x4

    #@3
    .line 670
    array-length v3, p2

    #@4
    const/4 v4, 0x2

    #@5
    if-le v3, v4, :cond_9

    #@7
    .line 671
    const/4 v1, -0x4

    #@8
    .line 688
    :goto_8
    return v1

    #@9
    .line 673
    :cond_9
    invoke-virtual {p0, p1}, Landroid/media/audiofx/AudioEffect;->intToByteArray(I)[B

    #@c
    move-result-object v0

    #@d
    .line 675
    .local v0, p:[B
    array-length v3, p2

    #@e
    mul-int/lit8 v3, v3, 0x4

    #@10
    new-array v2, v3, [B

    #@12
    .line 677
    .local v2, v:[B
    invoke-virtual {p0, v0, v2}, Landroid/media/audiofx/AudioEffect;->getParameter([B[B)I

    #@15
    move-result v1

    #@16
    .line 679
    .local v1, status:I
    if-eq v1, v5, :cond_1a

    #@18
    if-ne v1, v6, :cond_2d

    #@1a
    .line 680
    :cond_1a
    const/4 v3, 0x0

    #@1b
    invoke-virtual {p0, v2}, Landroid/media/audiofx/AudioEffect;->byteArrayToInt([B)I

    #@1e
    move-result v4

    #@1f
    aput v4, p2, v3

    #@21
    .line 681
    if-ne v1, v6, :cond_2a

    #@23
    .line 682
    const/4 v3, 0x1

    #@24
    invoke-virtual {p0, v2, v5}, Landroid/media/audiofx/AudioEffect;->byteArrayToInt([BI)I

    #@27
    move-result v4

    #@28
    aput v4, p2, v3

    #@2a
    .line 684
    :cond_2a
    div-int/lit8 v1, v1, 0x4

    #@2c
    goto :goto_8

    #@2d
    .line 686
    :cond_2d
    const/4 v1, -0x1

    #@2e
    goto :goto_8
.end method

.method public getParameter(I[S)I
    .registers 10
    .parameter "param"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x4

    #@1
    const/4 v5, 0x2

    #@2
    .line 701
    array-length v3, p2

    #@3
    if-le v3, v5, :cond_7

    #@5
    .line 702
    const/4 v1, -0x4

    #@6
    .line 719
    :goto_6
    return v1

    #@7
    .line 704
    :cond_7
    invoke-virtual {p0, p1}, Landroid/media/audiofx/AudioEffect;->intToByteArray(I)[B

    #@a
    move-result-object v0

    #@b
    .line 706
    .local v0, p:[B
    array-length v3, p2

    #@c
    mul-int/lit8 v3, v3, 0x2

    #@e
    new-array v2, v3, [B

    #@10
    .line 708
    .local v2, v:[B
    invoke-virtual {p0, v0, v2}, Landroid/media/audiofx/AudioEffect;->getParameter([B[B)I

    #@13
    move-result v1

    #@14
    .line 710
    .local v1, status:I
    if-eq v1, v5, :cond_18

    #@16
    if-ne v1, v6, :cond_2b

    #@18
    .line 711
    :cond_18
    const/4 v3, 0x0

    #@19
    invoke-virtual {p0, v2}, Landroid/media/audiofx/AudioEffect;->byteArrayToShort([B)S

    #@1c
    move-result v4

    #@1d
    aput-short v4, p2, v3

    #@1f
    .line 712
    if-ne v1, v6, :cond_28

    #@21
    .line 713
    const/4 v3, 0x1

    #@22
    invoke-virtual {p0, v2, v5}, Landroid/media/audiofx/AudioEffect;->byteArrayToShort([BI)S

    #@25
    move-result v4

    #@26
    aput-short v4, p2, v3

    #@28
    .line 715
    :cond_28
    div-int/lit8 v1, v1, 0x2

    #@2a
    goto :goto_6

    #@2b
    .line 717
    :cond_2b
    const/4 v1, -0x1

    #@2c
    goto :goto_6
.end method

.method public getParameter([B[B)I
    .registers 5
    .parameter "param"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 642
    const-string v0, "getParameter()"

    #@2
    invoke-virtual {p0, v0}, Landroid/media/audiofx/AudioEffect;->checkState(Ljava/lang/String;)V

    #@5
    .line 643
    array-length v0, p1

    #@6
    array-length v1, p2

    #@7
    invoke-direct {p0, v0, p1, v1, p2}, Landroid/media/audiofx/AudioEffect;->native_getParameter(I[BI[B)I

    #@a
    move-result v0

    #@b
    return v0
.end method

.method public getParameter([I[B)I
    .registers 9
    .parameter "param"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    const/4 v4, 0x0

    #@2
    const/4 v3, 0x1

    #@3
    .line 799
    array-length v2, p1

    #@4
    if-le v2, v5, :cond_8

    #@6
    .line 800
    const/4 v2, -0x4

    #@7
    .line 808
    :goto_7
    return v2

    #@8
    .line 802
    :cond_8
    aget v2, p1, v4

    #@a
    invoke-virtual {p0, v2}, Landroid/media/audiofx/AudioEffect;->intToByteArray(I)[B

    #@d
    move-result-object v0

    #@e
    .line 803
    .local v0, p:[B
    array-length v2, p1

    #@f
    if-le v2, v3, :cond_21

    #@11
    .line 804
    aget v2, p1, v3

    #@13
    invoke-virtual {p0, v2}, Landroid/media/audiofx/AudioEffect;->intToByteArray(I)[B

    #@16
    move-result-object v1

    #@17
    .line 805
    .local v1, p2:[B
    new-array v2, v5, [[B

    #@19
    aput-object v0, v2, v4

    #@1b
    aput-object v1, v2, v3

    #@1d
    invoke-virtual {p0, v2}, Landroid/media/audiofx/AudioEffect;->concatArrays([[B)[B

    #@20
    move-result-object v0

    #@21
    .line 808
    .end local v1           #p2:[B
    :cond_21
    invoke-virtual {p0, v0, p2}, Landroid/media/audiofx/AudioEffect;->getParameter([B[B)I

    #@24
    move-result v2

    #@25
    goto :goto_7
.end method

.method public getParameter([I[I)I
    .registers 13
    .parameter "param"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v9, 0x8

    #@2
    const/4 v8, 0x4

    #@3
    const/4 v7, 0x2

    #@4
    const/4 v6, 0x0

    #@5
    const/4 v5, 0x1

    #@6
    .line 732
    array-length v4, p1

    #@7
    if-gt v4, v7, :cond_c

    #@9
    array-length v4, p2

    #@a
    if-le v4, v7, :cond_e

    #@c
    .line 733
    :cond_c
    const/4 v2, -0x4

    #@d
    .line 753
    :goto_d
    return v2

    #@e
    .line 735
    :cond_e
    aget v4, p1, v6

    #@10
    invoke-virtual {p0, v4}, Landroid/media/audiofx/AudioEffect;->intToByteArray(I)[B

    #@13
    move-result-object v0

    #@14
    .line 736
    .local v0, p:[B
    array-length v4, p1

    #@15
    if-le v4, v5, :cond_27

    #@17
    .line 737
    aget v4, p1, v5

    #@19
    invoke-virtual {p0, v4}, Landroid/media/audiofx/AudioEffect;->intToByteArray(I)[B

    #@1c
    move-result-object v1

    #@1d
    .line 738
    .local v1, p2:[B
    new-array v4, v7, [[B

    #@1f
    aput-object v0, v4, v6

    #@21
    aput-object v1, v4, v5

    #@23
    invoke-virtual {p0, v4}, Landroid/media/audiofx/AudioEffect;->concatArrays([[B)[B

    #@26
    move-result-object v0

    #@27
    .line 740
    .end local v1           #p2:[B
    :cond_27
    array-length v4, p2

    #@28
    mul-int/lit8 v4, v4, 0x4

    #@2a
    new-array v3, v4, [B

    #@2c
    .line 742
    .local v3, v:[B
    invoke-virtual {p0, v0, v3}, Landroid/media/audiofx/AudioEffect;->getParameter([B[B)I

    #@2f
    move-result v2

    #@30
    .line 744
    .local v2, status:I
    if-eq v2, v8, :cond_34

    #@32
    if-ne v2, v9, :cond_45

    #@34
    .line 745
    :cond_34
    invoke-virtual {p0, v3}, Landroid/media/audiofx/AudioEffect;->byteArrayToInt([B)I

    #@37
    move-result v4

    #@38
    aput v4, p2, v6

    #@3a
    .line 746
    if-ne v2, v9, :cond_42

    #@3c
    .line 747
    invoke-virtual {p0, v3, v8}, Landroid/media/audiofx/AudioEffect;->byteArrayToInt([BI)I

    #@3f
    move-result v4

    #@40
    aput v4, p2, v5

    #@42
    .line 749
    :cond_42
    div-int/lit8 v2, v2, 0x4

    #@44
    goto :goto_d

    #@45
    .line 751
    :cond_45
    const/4 v2, -0x1

    #@46
    goto :goto_d
.end method

.method public getParameter([I[S)I
    .registers 12
    .parameter "param"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v8, 0x4

    #@1
    const/4 v7, 0x0

    #@2
    const/4 v6, 0x1

    #@3
    const/4 v5, 0x2

    #@4
    .line 766
    array-length v4, p1

    #@5
    if-gt v4, v5, :cond_a

    #@7
    array-length v4, p2

    #@8
    if-le v4, v5, :cond_c

    #@a
    .line 767
    :cond_a
    const/4 v2, -0x4

    #@b
    .line 787
    :goto_b
    return v2

    #@c
    .line 769
    :cond_c
    aget v4, p1, v7

    #@e
    invoke-virtual {p0, v4}, Landroid/media/audiofx/AudioEffect;->intToByteArray(I)[B

    #@11
    move-result-object v0

    #@12
    .line 770
    .local v0, p:[B
    array-length v4, p1

    #@13
    if-le v4, v6, :cond_25

    #@15
    .line 771
    aget v4, p1, v6

    #@17
    invoke-virtual {p0, v4}, Landroid/media/audiofx/AudioEffect;->intToByteArray(I)[B

    #@1a
    move-result-object v1

    #@1b
    .line 772
    .local v1, p2:[B
    new-array v4, v5, [[B

    #@1d
    aput-object v0, v4, v7

    #@1f
    aput-object v1, v4, v6

    #@21
    invoke-virtual {p0, v4}, Landroid/media/audiofx/AudioEffect;->concatArrays([[B)[B

    #@24
    move-result-object v0

    #@25
    .line 774
    .end local v1           #p2:[B
    :cond_25
    array-length v4, p2

    #@26
    mul-int/lit8 v4, v4, 0x2

    #@28
    new-array v3, v4, [B

    #@2a
    .line 776
    .local v3, v:[B
    invoke-virtual {p0, v0, v3}, Landroid/media/audiofx/AudioEffect;->getParameter([B[B)I

    #@2d
    move-result v2

    #@2e
    .line 778
    .local v2, status:I
    if-eq v2, v5, :cond_32

    #@30
    if-ne v2, v8, :cond_43

    #@32
    .line 779
    :cond_32
    invoke-virtual {p0, v3}, Landroid/media/audiofx/AudioEffect;->byteArrayToShort([B)S

    #@35
    move-result v4

    #@36
    aput-short v4, p2, v7

    #@38
    .line 780
    if-ne v2, v8, :cond_40

    #@3a
    .line 781
    invoke-virtual {p0, v3, v5}, Landroid/media/audiofx/AudioEffect;->byteArrayToShort([BI)S

    #@3d
    move-result v4

    #@3e
    aput-short v4, p2, v6

    #@40
    .line 783
    :cond_40
    div-int/lit8 v2, v2, 0x2

    #@42
    goto :goto_b

    #@43
    .line 785
    :cond_43
    const/4 v2, -0x1

    #@44
    goto :goto_b
.end method

.method public hasControl()Z
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 860
    const-string v0, "hasControl()"

    #@2
    invoke-virtual {p0, v0}, Landroid/media/audiofx/AudioEffect;->checkState(Ljava/lang/String;)V

    #@5
    .line 861
    invoke-direct {p0}, Landroid/media/audiofx/AudioEffect;->native_hasControl()Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public intToByteArray(I)[B
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 1281
    const/4 v1, 0x4

    #@1
    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@4
    move-result-object v0

    #@5
    .line 1282
    .local v0, converter:Ljava/nio/ByteBuffer;
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@c
    .line 1283
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@f
    .line 1284
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    #@12
    move-result-object v1

    #@13
    return-object v1
.end method

.method public release()V
    .registers 3

    #@0
    .prologue
    .line 405
    iget-object v1, p0, Landroid/media/audiofx/AudioEffect;->mStateLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 406
    :try_start_3
    invoke-direct {p0}, Landroid/media/audiofx/AudioEffect;->native_release()V

    #@6
    .line 407
    const/4 v0, 0x0

    #@7
    iput v0, p0, Landroid/media/audiofx/AudioEffect;->mState:I

    #@9
    .line 408
    monitor-exit v1

    #@a
    .line 409
    return-void

    #@b
    .line 408
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method public setControlStatusListener(Landroid/media/audiofx/AudioEffect$OnControlStatusChangeListener;)V
    .registers 4
    .parameter "listener"

    #@0
    .prologue
    .line 889
    iget-object v1, p0, Landroid/media/audiofx/AudioEffect;->mListenerLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 890
    :try_start_3
    iput-object p1, p0, Landroid/media/audiofx/AudioEffect;->mControlChangeStatusListener:Landroid/media/audiofx/AudioEffect$OnControlStatusChangeListener;

    #@5
    .line 891
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_10

    #@6
    .line 892
    if-eqz p1, :cond_f

    #@8
    iget-object v0, p0, Landroid/media/audiofx/AudioEffect;->mNativeEventHandler:Landroid/media/audiofx/AudioEffect$NativeEventHandler;

    #@a
    if-nez v0, :cond_f

    #@c
    .line 893
    invoke-direct {p0}, Landroid/media/audiofx/AudioEffect;->createNativeEventHandler()V

    #@f
    .line 895
    :cond_f
    return-void

    #@10
    .line 891
    :catchall_10
    move-exception v0

    #@11
    :try_start_11
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_11 .. :try_end_12} :catchall_10

    #@12
    throw v0
.end method

.method public setEnableStatusListener(Landroid/media/audiofx/AudioEffect$OnEnableStatusChangeListener;)V
    .registers 4
    .parameter "listener"

    #@0
    .prologue
    .line 874
    iget-object v1, p0, Landroid/media/audiofx/AudioEffect;->mListenerLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 875
    :try_start_3
    iput-object p1, p0, Landroid/media/audiofx/AudioEffect;->mEnableStatusChangeListener:Landroid/media/audiofx/AudioEffect$OnEnableStatusChangeListener;

    #@5
    .line 876
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_10

    #@6
    .line 877
    if-eqz p1, :cond_f

    #@8
    iget-object v0, p0, Landroid/media/audiofx/AudioEffect;->mNativeEventHandler:Landroid/media/audiofx/AudioEffect$NativeEventHandler;

    #@a
    if-nez v0, :cond_f

    #@c
    .line 878
    invoke-direct {p0}, Landroid/media/audiofx/AudioEffect;->createNativeEventHandler()V

    #@f
    .line 880
    :cond_f
    return-void

    #@10
    .line 876
    :catchall_10
    move-exception v0

    #@11
    :try_start_11
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_11 .. :try_end_12} :catchall_10

    #@12
    throw v0
.end method

.method public setEnabled(Z)I
    .registers 3
    .parameter "enabled"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 488
    const-string/jumbo v0, "setEnabled()"

    #@3
    invoke-virtual {p0, v0}, Landroid/media/audiofx/AudioEffect;->checkState(Ljava/lang/String;)V

    #@6
    .line 489
    invoke-direct {p0, p1}, Landroid/media/audiofx/AudioEffect;->native_setEnabled(Z)I

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public setParameter(II)I
    .registers 6
    .parameter "param"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 522
    invoke-virtual {p0, p1}, Landroid/media/audiofx/AudioEffect;->intToByteArray(I)[B

    #@3
    move-result-object v0

    #@4
    .line 523
    .local v0, p:[B
    invoke-virtual {p0, p2}, Landroid/media/audiofx/AudioEffect;->intToByteArray(I)[B

    #@7
    move-result-object v1

    #@8
    .line 524
    .local v1, v:[B
    invoke-virtual {p0, v0, v1}, Landroid/media/audiofx/AudioEffect;->setParameter([B[B)I

    #@b
    move-result v2

    #@c
    return v2
.end method

.method public setParameter(IS)I
    .registers 6
    .parameter "param"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 536
    invoke-virtual {p0, p1}, Landroid/media/audiofx/AudioEffect;->intToByteArray(I)[B

    #@3
    move-result-object v0

    #@4
    .line 537
    .local v0, p:[B
    invoke-virtual {p0, p2}, Landroid/media/audiofx/AudioEffect;->shortToByteArray(S)[B

    #@7
    move-result-object v1

    #@8
    .line 538
    .local v1, v:[B
    invoke-virtual {p0, v0, v1}, Landroid/media/audiofx/AudioEffect;->setParameter([B[B)I

    #@b
    move-result v2

    #@c
    return v2
.end method

.method public setParameter(I[B)I
    .registers 5
    .parameter "param"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 550
    invoke-virtual {p0, p1}, Landroid/media/audiofx/AudioEffect;->intToByteArray(I)[B

    #@3
    move-result-object v0

    #@4
    .line 551
    .local v0, p:[B
    invoke-virtual {p0, v0, p2}, Landroid/media/audiofx/AudioEffect;->setParameter([B[B)I

    #@7
    move-result v1

    #@8
    return v1
.end method

.method public setParameter([B[B)I
    .registers 5
    .parameter "param"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 511
    const-string/jumbo v0, "setParameter()"

    #@3
    invoke-virtual {p0, v0}, Landroid/media/audiofx/AudioEffect;->checkState(Ljava/lang/String;)V

    #@6
    .line 512
    array-length v0, p1

    #@7
    array-length v1, p2

    #@8
    invoke-direct {p0, v0, p1, v1, p2}, Landroid/media/audiofx/AudioEffect;->native_setParameter(I[BI[B)I

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public setParameter([I[B)I
    .registers 9
    .parameter "param"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    const/4 v4, 0x0

    #@2
    const/4 v3, 0x1

    #@3
    .line 614
    array-length v2, p1

    #@4
    if-le v2, v5, :cond_8

    #@6
    .line 615
    const/4 v2, -0x4

    #@7
    .line 622
    :goto_7
    return v2

    #@8
    .line 617
    :cond_8
    aget v2, p1, v4

    #@a
    invoke-virtual {p0, v2}, Landroid/media/audiofx/AudioEffect;->intToByteArray(I)[B

    #@d
    move-result-object v0

    #@e
    .line 618
    .local v0, p:[B
    array-length v2, p1

    #@f
    if-le v2, v3, :cond_21

    #@11
    .line 619
    aget v2, p1, v3

    #@13
    invoke-virtual {p0, v2}, Landroid/media/audiofx/AudioEffect;->intToByteArray(I)[B

    #@16
    move-result-object v1

    #@17
    .line 620
    .local v1, p2:[B
    new-array v2, v5, [[B

    #@19
    aput-object v0, v2, v4

    #@1b
    aput-object v1, v2, v3

    #@1d
    invoke-virtual {p0, v2}, Landroid/media/audiofx/AudioEffect;->concatArrays([[B)[B

    #@20
    move-result-object v0

    #@21
    .line 622
    .end local v1           #p2:[B
    :cond_21
    invoke-virtual {p0, v0, p2}, Landroid/media/audiofx/AudioEffect;->setParameter([B[B)I

    #@24
    move-result v2

    #@25
    goto :goto_7
.end method

.method public setParameter([I[I)I
    .registers 11
    .parameter "param"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x2

    #@1
    const/4 v6, 0x0

    #@2
    const/4 v5, 0x1

    #@3
    .line 563
    array-length v4, p1

    #@4
    if-gt v4, v7, :cond_9

    #@6
    array-length v4, p2

    #@7
    if-le v4, v7, :cond_b

    #@9
    .line 564
    :cond_9
    const/4 v4, -0x4

    #@a
    .line 576
    :goto_a
    return v4

    #@b
    .line 566
    :cond_b
    aget v4, p1, v6

    #@d
    invoke-virtual {p0, v4}, Landroid/media/audiofx/AudioEffect;->intToByteArray(I)[B

    #@10
    move-result-object v0

    #@11
    .line 567
    .local v0, p:[B
    array-length v4, p1

    #@12
    if-le v4, v5, :cond_24

    #@14
    .line 568
    aget v4, p1, v5

    #@16
    invoke-virtual {p0, v4}, Landroid/media/audiofx/AudioEffect;->intToByteArray(I)[B

    #@19
    move-result-object v1

    #@1a
    .line 569
    .local v1, p2:[B
    new-array v4, v7, [[B

    #@1c
    aput-object v0, v4, v6

    #@1e
    aput-object v1, v4, v5

    #@20
    invoke-virtual {p0, v4}, Landroid/media/audiofx/AudioEffect;->concatArrays([[B)[B

    #@23
    move-result-object v0

    #@24
    .line 571
    .end local v1           #p2:[B
    :cond_24
    aget v4, p2, v6

    #@26
    invoke-virtual {p0, v4}, Landroid/media/audiofx/AudioEffect;->intToByteArray(I)[B

    #@29
    move-result-object v2

    #@2a
    .line 572
    .local v2, v:[B
    array-length v4, p2

    #@2b
    if-le v4, v5, :cond_3d

    #@2d
    .line 573
    aget v4, p2, v5

    #@2f
    invoke-virtual {p0, v4}, Landroid/media/audiofx/AudioEffect;->intToByteArray(I)[B

    #@32
    move-result-object v3

    #@33
    .line 574
    .local v3, v2:[B
    new-array v4, v7, [[B

    #@35
    aput-object v2, v4, v6

    #@37
    aput-object v3, v4, v5

    #@39
    invoke-virtual {p0, v4}, Landroid/media/audiofx/AudioEffect;->concatArrays([[B)[B

    #@3c
    move-result-object v2

    #@3d
    .line 576
    .end local v3           #v2:[B
    :cond_3d
    invoke-virtual {p0, v0, v2}, Landroid/media/audiofx/AudioEffect;->setParameter([B[B)I

    #@40
    move-result v4

    #@41
    goto :goto_a
.end method

.method public setParameter([I[S)I
    .registers 11
    .parameter "param"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x2

    #@1
    const/4 v6, 0x0

    #@2
    const/4 v5, 0x1

    #@3
    .line 588
    array-length v4, p1

    #@4
    if-gt v4, v7, :cond_9

    #@6
    array-length v4, p2

    #@7
    if-le v4, v7, :cond_b

    #@9
    .line 589
    :cond_9
    const/4 v4, -0x4

    #@a
    .line 602
    :goto_a
    return v4

    #@b
    .line 591
    :cond_b
    aget v4, p1, v6

    #@d
    invoke-virtual {p0, v4}, Landroid/media/audiofx/AudioEffect;->intToByteArray(I)[B

    #@10
    move-result-object v0

    #@11
    .line 592
    .local v0, p:[B
    array-length v4, p1

    #@12
    if-le v4, v5, :cond_24

    #@14
    .line 593
    aget v4, p1, v5

    #@16
    invoke-virtual {p0, v4}, Landroid/media/audiofx/AudioEffect;->intToByteArray(I)[B

    #@19
    move-result-object v1

    #@1a
    .line 594
    .local v1, p2:[B
    new-array v4, v7, [[B

    #@1c
    aput-object v0, v4, v6

    #@1e
    aput-object v1, v4, v5

    #@20
    invoke-virtual {p0, v4}, Landroid/media/audiofx/AudioEffect;->concatArrays([[B)[B

    #@23
    move-result-object v0

    #@24
    .line 597
    .end local v1           #p2:[B
    :cond_24
    aget-short v4, p2, v6

    #@26
    invoke-virtual {p0, v4}, Landroid/media/audiofx/AudioEffect;->shortToByteArray(S)[B

    #@29
    move-result-object v2

    #@2a
    .line 598
    .local v2, v:[B
    array-length v4, p2

    #@2b
    if-le v4, v5, :cond_3d

    #@2d
    .line 599
    aget-short v4, p2, v5

    #@2f
    invoke-virtual {p0, v4}, Landroid/media/audiofx/AudioEffect;->shortToByteArray(S)[B

    #@32
    move-result-object v3

    #@33
    .line 600
    .local v3, v2:[B
    new-array v4, v7, [[B

    #@35
    aput-object v2, v4, v6

    #@37
    aput-object v3, v4, v5

    #@39
    invoke-virtual {p0, v4}, Landroid/media/audiofx/AudioEffect;->concatArrays([[B)[B

    #@3c
    move-result-object v2

    #@3d
    .line 602
    .end local v3           #v2:[B
    :cond_3d
    invoke-virtual {p0, v0, v2}, Landroid/media/audiofx/AudioEffect;->setParameter([B[B)I

    #@40
    move-result v4

    #@41
    goto :goto_a
.end method

.method public setParameterListener(Landroid/media/audiofx/AudioEffect$OnParameterChangeListener;)V
    .registers 4
    .parameter "listener"

    #@0
    .prologue
    .line 904
    iget-object v1, p0, Landroid/media/audiofx/AudioEffect;->mListenerLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 905
    :try_start_3
    iput-object p1, p0, Landroid/media/audiofx/AudioEffect;->mParameterChangeListener:Landroid/media/audiofx/AudioEffect$OnParameterChangeListener;

    #@5
    .line 906
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_10

    #@6
    .line 907
    if-eqz p1, :cond_f

    #@8
    iget-object v0, p0, Landroid/media/audiofx/AudioEffect;->mNativeEventHandler:Landroid/media/audiofx/AudioEffect$NativeEventHandler;

    #@a
    if-nez v0, :cond_f

    #@c
    .line 908
    invoke-direct {p0}, Landroid/media/audiofx/AudioEffect;->createNativeEventHandler()V

    #@f
    .line 910
    :cond_f
    return-void

    #@10
    .line 906
    :catchall_10
    move-exception v0

    #@11
    :try_start_11
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_11 .. :try_end_12} :catchall_10

    #@12
    throw v0
.end method

.method public shortToByteArray(S)[B
    .registers 5
    .parameter "value"

    #@0
    .prologue
    .line 1308
    const/4 v2, 0x2

    #@1
    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@4
    move-result-object v0

    #@5
    .line 1309
    .local v0, converter:Ljava/nio/ByteBuffer;
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    #@8
    move-result-object v2

    #@9
    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@c
    .line 1310
    move v1, p1

    #@d
    .line 1311
    .local v1, sValue:S
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@10
    .line 1312
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    #@13
    move-result-object v2

    #@14
    return-object v2
.end method
