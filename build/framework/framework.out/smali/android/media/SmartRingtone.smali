.class Landroid/media/SmartRingtone;
.super Ljava/lang/Object;
.source "SmartRingtone.java"


# static fields
.field private static LGE_DEBUG:Z = false

.field private static final NUM_MAX_FRAMES:I = 0xf

.field private static final SAMPLE_RATE:I = 0xac44

.field private static TAG:Ljava/lang/String; = null

.field private static final filtDen:S = -0x60e4s

.field private static final filtNum:[S

.field private static mNoiseAverage:I

.field private static smart_ringtone_level_1:I

.field private static smart_ringtone_level_2:I

.field private static smart_ringtone_level_3:I

.field private static smart_ringtone_level_4:I

.field private static smart_ringtone_level_vib_1:I

.field private static smart_ringtone_level_vib_2:I

.field private static smart_ringtone_level_vib_3:I

.field private static smart_ringtone_level_vib_4:I


# instance fields
.field arec:Landroid/media/AudioRecord;

.field buffer:[S

.field buffersize:I

.field private mAdjustVolume:I

.field private mAudio:Landroid/media/MediaPlayer;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mContext:Landroid/content/Context;

.field private mDelayedVolumeUpHandler:Landroid/os/Handler;

.field private mExitThread:Z

.field private mInitializationLooper:Landroid/os/Looper;

.field private mIsFromPhoneApp:Z

.field private mMicTestDone:Z

.field private mPrevVolume:I

.field private mSmartRingtoneLevel:I

.field private mStreamType:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 23
    const-string v2, "SmartRingtone"

    #@4
    sput-object v2, Landroid/media/SmartRingtone;->TAG:Ljava/lang/String;

    #@6
    .line 24
    const-string/jumbo v2, "ro.debuggable"

    #@9
    invoke-static {v2, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@c
    move-result v2

    #@d
    if-ne v2, v0, :cond_1c

    #@f
    :goto_f
    sput-boolean v0, Landroid/media/SmartRingtone;->LGE_DEBUG:Z

    #@11
    .line 43
    sput v1, Landroid/media/SmartRingtone;->mNoiseAverage:I

    #@13
    .line 48
    const/4 v0, 0x2

    #@14
    new-array v0, v0, [S

    #@16
    fill-array-data v0, :array_1e

    #@19
    sput-object v0, Landroid/media/SmartRingtone;->filtNum:[S

    #@1b
    return-void

    #@1c
    :cond_1c
    move v0, v1

    #@1d
    .line 24
    goto :goto_f

    #@1e
    .line 48
    :array_1e
    .array-data 0x2
        0x71t 0x70t
        0x8ft 0x8ft
    .end array-data
.end method

.method public constructor <init>(Landroid/media/AudioManager;Landroid/content/Context;)V
    .registers 6
    .parameter "manager"
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 63
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 30
    iput-boolean v0, p0, Landroid/media/SmartRingtone;->mMicTestDone:Z

    #@7
    .line 31
    iput-boolean v0, p0, Landroid/media/SmartRingtone;->mIsFromPhoneApp:Z

    #@9
    .line 32
    iput v0, p0, Landroid/media/SmartRingtone;->mSmartRingtoneLevel:I

    #@b
    .line 40
    const/4 v0, 0x2

    #@c
    iput v0, p0, Landroid/media/SmartRingtone;->mStreamType:I

    #@e
    .line 60
    iput-object v1, p0, Landroid/media/SmartRingtone;->mDelayedVolumeUpHandler:Landroid/os/Handler;

    #@10
    .line 61
    iput-object v1, p0, Landroid/media/SmartRingtone;->mInitializationLooper:Landroid/os/Looper;

    #@12
    .line 64
    iput-object p1, p0, Landroid/media/SmartRingtone;->mAudioManager:Landroid/media/AudioManager;

    #@14
    .line 65
    iput-object p2, p0, Landroid/media/SmartRingtone;->mContext:Landroid/content/Context;

    #@16
    .line 67
    sget-boolean v0, Landroid/media/SmartRingtone;->LGE_DEBUG:Z

    #@18
    if-eqz v0, :cond_39

    #@1a
    sget-object v0, Landroid/media/SmartRingtone;->TAG:Ljava/lang/String;

    #@1c
    new-instance v1, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string/jumbo v2, "mContext.toString() = "

    #@24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    iget-object v2, p0, Landroid/media/SmartRingtone;->mContext:Landroid/content/Context;

    #@2a
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v1

    #@36
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 69
    :cond_39
    iget-object v0, p0, Landroid/media/SmartRingtone;->mContext:Landroid/content/Context;

    #@3b
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@3e
    move-result-object v0

    #@3f
    const-string v1, "PhoneApp"

    #@41
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@44
    move-result v0

    #@45
    if-nez v0, :cond_71

    #@47
    iget-object v0, p0, Landroid/media/SmartRingtone;->mContext:Landroid/content/Context;

    #@49
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@4c
    move-result-object v0

    #@4d
    const-string v1, "com.android.phone"

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@52
    move-result v0

    #@53
    if-nez v0, :cond_71

    #@55
    iget-object v0, p0, Landroid/media/SmartRingtone;->mContext:Landroid/content/Context;

    #@57
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@5a
    move-result-object v0

    #@5b
    const-string v1, "LTECallApp"

    #@5d
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@60
    move-result v0

    #@61
    if-nez v0, :cond_71

    #@63
    iget-object v0, p0, Landroid/media/SmartRingtone;->mContext:Landroid/content/Context;

    #@65
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@68
    move-result-object v0

    #@69
    const-string v1, "VideoTelephony"

    #@6b
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@6e
    move-result v0

    #@6f
    if-eqz v0, :cond_74

    #@71
    .line 73
    :cond_71
    const/4 v0, 0x1

    #@72
    iput-boolean v0, p0, Landroid/media/SmartRingtone;->mIsFromPhoneApp:Z

    #@74
    .line 75
    :cond_74
    const/4 v0, -0x1

    #@75
    iput v0, p0, Landroid/media/SmartRingtone;->mPrevVolume:I

    #@77
    .line 77
    iget-object v0, p0, Landroid/media/SmartRingtone;->mContext:Landroid/content/Context;

    #@79
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7c
    move-result-object v0

    #@7d
    const v1, 0x20b0003

    #@80
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@83
    move-result v0

    #@84
    sput v0, Landroid/media/SmartRingtone;->smart_ringtone_level_1:I

    #@86
    .line 78
    iget-object v0, p0, Landroid/media/SmartRingtone;->mContext:Landroid/content/Context;

    #@88
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@8b
    move-result-object v0

    #@8c
    const v1, 0x20b0004

    #@8f
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@92
    move-result v0

    #@93
    sput v0, Landroid/media/SmartRingtone;->smart_ringtone_level_2:I

    #@95
    .line 79
    iget-object v0, p0, Landroid/media/SmartRingtone;->mContext:Landroid/content/Context;

    #@97
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@9a
    move-result-object v0

    #@9b
    const v1, 0x20b0005

    #@9e
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@a1
    move-result v0

    #@a2
    sput v0, Landroid/media/SmartRingtone;->smart_ringtone_level_3:I

    #@a4
    .line 80
    iget-object v0, p0, Landroid/media/SmartRingtone;->mContext:Landroid/content/Context;

    #@a6
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@a9
    move-result-object v0

    #@aa
    const v1, 0x20b0006

    #@ad
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@b0
    move-result v0

    #@b1
    sput v0, Landroid/media/SmartRingtone;->smart_ringtone_level_4:I

    #@b3
    .line 81
    iget-object v0, p0, Landroid/media/SmartRingtone;->mContext:Landroid/content/Context;

    #@b5
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@b8
    move-result-object v0

    #@b9
    const v1, 0x20b0007

    #@bc
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@bf
    move-result v0

    #@c0
    sput v0, Landroid/media/SmartRingtone;->smart_ringtone_level_vib_1:I

    #@c2
    .line 82
    iget-object v0, p0, Landroid/media/SmartRingtone;->mContext:Landroid/content/Context;

    #@c4
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@c7
    move-result-object v0

    #@c8
    const v1, 0x20b0008

    #@cb
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@ce
    move-result v0

    #@cf
    sput v0, Landroid/media/SmartRingtone;->smart_ringtone_level_vib_2:I

    #@d1
    .line 83
    iget-object v0, p0, Landroid/media/SmartRingtone;->mContext:Landroid/content/Context;

    #@d3
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@d6
    move-result-object v0

    #@d7
    const v1, 0x20b0009

    #@da
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@dd
    move-result v0

    #@de
    sput v0, Landroid/media/SmartRingtone;->smart_ringtone_level_vib_3:I

    #@e0
    .line 84
    iget-object v0, p0, Landroid/media/SmartRingtone;->mContext:Landroid/content/Context;

    #@e2
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@e5
    move-result-object v0

    #@e6
    const v1, 0x20b000a

    #@e9
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@ec
    move-result v0

    #@ed
    sput v0, Landroid/media/SmartRingtone;->smart_ringtone_level_vib_4:I

    #@ef
    .line 86
    sget-boolean v0, Landroid/media/SmartRingtone;->LGE_DEBUG:Z

    #@f1
    if-eqz v0, :cond_1cb

    #@f3
    .line 87
    sget-object v0, Landroid/media/SmartRingtone;->TAG:Ljava/lang/String;

    #@f5
    new-instance v1, Ljava/lang/StringBuilder;

    #@f7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@fa
    const-string/jumbo v2, "vib_4 = "

    #@fd
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v1

    #@101
    sget v2, Landroid/media/SmartRingtone;->smart_ringtone_level_vib_4:I

    #@103
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@106
    move-result-object v1

    #@107
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10a
    move-result-object v1

    #@10b
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@10e
    .line 88
    sget-object v0, Landroid/media/SmartRingtone;->TAG:Ljava/lang/String;

    #@110
    new-instance v1, Ljava/lang/StringBuilder;

    #@112
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@115
    const-string/jumbo v2, "vib_3 = "

    #@118
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v1

    #@11c
    sget v2, Landroid/media/SmartRingtone;->smart_ringtone_level_vib_3:I

    #@11e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@121
    move-result-object v1

    #@122
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@125
    move-result-object v1

    #@126
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@129
    .line 89
    sget-object v0, Landroid/media/SmartRingtone;->TAG:Ljava/lang/String;

    #@12b
    new-instance v1, Ljava/lang/StringBuilder;

    #@12d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@130
    const-string/jumbo v2, "vib_2 = "

    #@133
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@136
    move-result-object v1

    #@137
    sget v2, Landroid/media/SmartRingtone;->smart_ringtone_level_vib_2:I

    #@139
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13c
    move-result-object v1

    #@13d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@140
    move-result-object v1

    #@141
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@144
    .line 90
    sget-object v0, Landroid/media/SmartRingtone;->TAG:Ljava/lang/String;

    #@146
    new-instance v1, Ljava/lang/StringBuilder;

    #@148
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@14b
    const-string/jumbo v2, "vib_1 = "

    #@14e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@151
    move-result-object v1

    #@152
    sget v2, Landroid/media/SmartRingtone;->smart_ringtone_level_vib_1:I

    #@154
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@157
    move-result-object v1

    #@158
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15b
    move-result-object v1

    #@15c
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@15f
    .line 91
    sget-object v0, Landroid/media/SmartRingtone;->TAG:Ljava/lang/String;

    #@161
    new-instance v1, Ljava/lang/StringBuilder;

    #@163
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@166
    const-string/jumbo v2, "level_4 = "

    #@169
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16c
    move-result-object v1

    #@16d
    sget v2, Landroid/media/SmartRingtone;->smart_ringtone_level_4:I

    #@16f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@172
    move-result-object v1

    #@173
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@176
    move-result-object v1

    #@177
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@17a
    .line 92
    sget-object v0, Landroid/media/SmartRingtone;->TAG:Ljava/lang/String;

    #@17c
    new-instance v1, Ljava/lang/StringBuilder;

    #@17e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@181
    const-string/jumbo v2, "level_3 = "

    #@184
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@187
    move-result-object v1

    #@188
    sget v2, Landroid/media/SmartRingtone;->smart_ringtone_level_3:I

    #@18a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18d
    move-result-object v1

    #@18e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@191
    move-result-object v1

    #@192
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@195
    .line 93
    sget-object v0, Landroid/media/SmartRingtone;->TAG:Ljava/lang/String;

    #@197
    new-instance v1, Ljava/lang/StringBuilder;

    #@199
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@19c
    const-string/jumbo v2, "level_2 = "

    #@19f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a2
    move-result-object v1

    #@1a3
    sget v2, Landroid/media/SmartRingtone;->smart_ringtone_level_2:I

    #@1a5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a8
    move-result-object v1

    #@1a9
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ac
    move-result-object v1

    #@1ad
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1b0
    .line 94
    sget-object v0, Landroid/media/SmartRingtone;->TAG:Ljava/lang/String;

    #@1b2
    new-instance v1, Ljava/lang/StringBuilder;

    #@1b4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1b7
    const-string/jumbo v2, "level_1 = "

    #@1ba
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bd
    move-result-object v1

    #@1be
    sget v2, Landroid/media/SmartRingtone;->smart_ringtone_level_1:I

    #@1c0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c3
    move-result-object v1

    #@1c4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c7
    move-result-object v1

    #@1c8
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1cb
    .line 96
    :cond_1cb
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 22
    sget-object v0, Landroid/media/SmartRingtone;->TAG:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/media/SmartRingtone;)Landroid/media/MediaPlayer;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 22
    iget-object v0, p0, Landroid/media/SmartRingtone;->mAudio:Landroid/media/MediaPlayer;

    #@2
    return-object v0
.end method

.method static synthetic access$1000()[S
    .registers 1

    #@0
    .prologue
    .line 22
    sget-object v0, Landroid/media/SmartRingtone;->filtNum:[S

    #@2
    return-object v0
.end method

.method static synthetic access$1100()I
    .registers 1

    #@0
    .prologue
    .line 22
    sget v0, Landroid/media/SmartRingtone;->mNoiseAverage:I

    #@2
    return v0
.end method

.method static synthetic access$1102(I)I
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 22
    sput p0, Landroid/media/SmartRingtone;->mNoiseAverage:I

    #@2
    return p0
.end method

.method static synthetic access$1112(I)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 22
    sget v0, Landroid/media/SmartRingtone;->mNoiseAverage:I

    #@2
    add-int/2addr v0, p0

    #@3
    sput v0, Landroid/media/SmartRingtone;->mNoiseAverage:I

    #@5
    return v0
.end method

.method static synthetic access$1136(I)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 22
    sget v0, Landroid/media/SmartRingtone;->mNoiseAverage:I

    #@2
    div-int/2addr v0, p0

    #@3
    sput v0, Landroid/media/SmartRingtone;->mNoiseAverage:I

    #@5
    return v0
.end method

.method static synthetic access$1200(Landroid/media/SmartRingtone;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 22
    invoke-direct {p0, p1}, Landroid/media/SmartRingtone;->calculateSmartRingtoneLevel(I)V

    #@3
    return-void
.end method

.method static synthetic access$1300(Landroid/media/SmartRingtone;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 22
    iget-object v0, p0, Landroid/media/SmartRingtone;->mDelayedVolumeUpHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/media/SmartRingtone;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 22
    iget v0, p0, Landroid/media/SmartRingtone;->mSmartRingtoneLevel:I

    #@2
    return v0
.end method

.method static synthetic access$202(Landroid/media/SmartRingtone;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 22
    iput p1, p0, Landroid/media/SmartRingtone;->mSmartRingtoneLevel:I

    #@2
    return p1
.end method

.method static synthetic access$300(Landroid/media/SmartRingtone;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 22
    iget v0, p0, Landroid/media/SmartRingtone;->mPrevVolume:I

    #@2
    return v0
.end method

.method static synthetic access$302(Landroid/media/SmartRingtone;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 22
    iput p1, p0, Landroid/media/SmartRingtone;->mPrevVolume:I

    #@2
    return p1
.end method

.method static synthetic access$400(Landroid/media/SmartRingtone;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 22
    iget v0, p0, Landroid/media/SmartRingtone;->mStreamType:I

    #@2
    return v0
.end method

.method static synthetic access$500(Landroid/media/SmartRingtone;)Landroid/media/AudioManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 22
    iget-object v0, p0, Landroid/media/SmartRingtone;->mAudioManager:Landroid/media/AudioManager;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Landroid/media/SmartRingtone;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 22
    invoke-direct {p0}, Landroid/media/SmartRingtone;->setSmartRingtoneLevel()V

    #@3
    return-void
.end method

.method static synthetic access$700(Landroid/media/SmartRingtone;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 22
    iget v0, p0, Landroid/media/SmartRingtone;->mAdjustVolume:I

    #@2
    return v0
.end method

.method static synthetic access$800()Z
    .registers 1

    #@0
    .prologue
    .line 22
    sget-boolean v0, Landroid/media/SmartRingtone;->LGE_DEBUG:Z

    #@2
    return v0
.end method

.method static synthetic access$900(Landroid/media/SmartRingtone;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 22
    iget-boolean v0, p0, Landroid/media/SmartRingtone;->mExitThread:Z

    #@2
    return v0
.end method

.method private calculateSmartRingtoneLevel(I)V
    .registers 11
    .parameter "noise"

    #@0
    .prologue
    const/4 v8, 0x4

    #@1
    const/4 v7, 0x3

    #@2
    const/4 v6, 0x2

    #@3
    const/4 v5, 0x1

    #@4
    const/4 v4, 0x0

    #@5
    .line 290
    sget-object v0, Landroid/media/SmartRingtone;->TAG:Ljava/lang/String;

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "VIBRATE_WHEN_RINGING = "

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    iget-object v2, p0, Landroid/media/SmartRingtone;->mContext:Landroid/content/Context;

    #@14
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@17
    move-result-object v2

    #@18
    const-string/jumbo v3, "vibrate_when_ringing"

    #@1b
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@1e
    move-result v2

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 291
    iget-object v0, p0, Landroid/media/SmartRingtone;->mContext:Landroid/content/Context;

    #@2c
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2f
    move-result-object v0

    #@30
    const-string/jumbo v1, "vibrate_when_ringing"

    #@33
    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@36
    move-result v0

    #@37
    if-eqz v0, :cond_55

    #@39
    .line 292
    sget v0, Landroid/media/SmartRingtone;->smart_ringtone_level_vib_4:I

    #@3b
    if-lt p1, v0, :cond_40

    #@3d
    .line 293
    iput v8, p0, Landroid/media/SmartRingtone;->mSmartRingtoneLevel:I

    #@3f
    .line 312
    :cond_3f
    :goto_3f
    return-void

    #@40
    .line 294
    :cond_40
    sget v0, Landroid/media/SmartRingtone;->smart_ringtone_level_vib_3:I

    #@42
    if-lt p1, v0, :cond_47

    #@44
    .line 295
    iput v7, p0, Landroid/media/SmartRingtone;->mSmartRingtoneLevel:I

    #@46
    goto :goto_3f

    #@47
    .line 296
    :cond_47
    sget v0, Landroid/media/SmartRingtone;->smart_ringtone_level_vib_2:I

    #@49
    if-lt p1, v0, :cond_4e

    #@4b
    .line 297
    iput v6, p0, Landroid/media/SmartRingtone;->mSmartRingtoneLevel:I

    #@4d
    goto :goto_3f

    #@4e
    .line 298
    :cond_4e
    sget v0, Landroid/media/SmartRingtone;->smart_ringtone_level_vib_1:I

    #@50
    if-lt p1, v0, :cond_3f

    #@52
    .line 299
    iput v5, p0, Landroid/media/SmartRingtone;->mSmartRingtoneLevel:I

    #@54
    goto :goto_3f

    #@55
    .line 302
    :cond_55
    sget v0, Landroid/media/SmartRingtone;->smart_ringtone_level_4:I

    #@57
    if-lt p1, v0, :cond_5c

    #@59
    .line 303
    iput v8, p0, Landroid/media/SmartRingtone;->mSmartRingtoneLevel:I

    #@5b
    goto :goto_3f

    #@5c
    .line 304
    :cond_5c
    sget v0, Landroid/media/SmartRingtone;->smart_ringtone_level_3:I

    #@5e
    if-lt p1, v0, :cond_63

    #@60
    .line 305
    iput v7, p0, Landroid/media/SmartRingtone;->mSmartRingtoneLevel:I

    #@62
    goto :goto_3f

    #@63
    .line 306
    :cond_63
    sget v0, Landroid/media/SmartRingtone;->smart_ringtone_level_2:I

    #@65
    if-lt p1, v0, :cond_6a

    #@67
    .line 307
    iput v6, p0, Landroid/media/SmartRingtone;->mSmartRingtoneLevel:I

    #@69
    goto :goto_3f

    #@6a
    .line 308
    :cond_6a
    sget v0, Landroid/media/SmartRingtone;->smart_ringtone_level_1:I

    #@6c
    if-lt p1, v0, :cond_3f

    #@6e
    .line 309
    iput v5, p0, Landroid/media/SmartRingtone;->mSmartRingtoneLevel:I

    #@70
    goto :goto_3f
.end method

.method private setSmartRingtoneLevel()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x7

    #@1
    const/4 v2, 0x2

    #@2
    .line 316
    iget v0, p0, Landroid/media/SmartRingtone;->mSmartRingtoneLevel:I

    #@4
    if-lez v0, :cond_34

    #@6
    iget-object v0, p0, Landroid/media/SmartRingtone;->mAudioManager:Landroid/media/AudioManager;

    #@8
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    #@b
    move-result v0

    #@c
    if-ne v0, v2, :cond_34

    #@e
    iget-object v0, p0, Landroid/media/SmartRingtone;->mAudioManager:Landroid/media/AudioManager;

    #@10
    iget v1, p0, Landroid/media/SmartRingtone;->mStreamType:I

    #@12
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getDevicesForStream(I)I

    #@15
    move-result v0

    #@16
    if-ne v0, v2, :cond_34

    #@18
    iget-object v0, p0, Landroid/media/SmartRingtone;->mAudioManager:Landroid/media/AudioManager;

    #@1a
    iget v1, p0, Landroid/media/SmartRingtone;->mStreamType:I

    #@1c
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    #@1f
    move-result v0

    #@20
    if-eqz v0, :cond_34

    #@22
    .line 321
    iget v0, p0, Landroid/media/SmartRingtone;->mSmartRingtoneLevel:I

    #@24
    iget v1, p0, Landroid/media/SmartRingtone;->mPrevVolume:I

    #@26
    add-int/2addr v0, v1

    #@27
    if-gt v0, v3, :cond_31

    #@29
    .line 322
    iget v0, p0, Landroid/media/SmartRingtone;->mSmartRingtoneLevel:I

    #@2b
    iget v1, p0, Landroid/media/SmartRingtone;->mPrevVolume:I

    #@2d
    add-int/2addr v0, v1

    #@2e
    iput v0, p0, Landroid/media/SmartRingtone;->mAdjustVolume:I

    #@30
    .line 329
    :goto_30
    return-void

    #@31
    .line 324
    :cond_31
    iput v3, p0, Landroid/media/SmartRingtone;->mAdjustVolume:I

    #@33
    goto :goto_30

    #@34
    .line 327
    :cond_34
    iget v0, p0, Landroid/media/SmartRingtone;->mPrevVolume:I

    #@36
    iput v0, p0, Landroid/media/SmartRingtone;->mAdjustVolume:I

    #@38
    goto :goto_30
.end method


# virtual methods
.method public onExitRecordingLoop()V
    .registers 2

    #@0
    .prologue
    .line 333
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/media/SmartRingtone;->mExitThread:Z

    #@3
    .line 334
    return-void
.end method

.method public onNoiseEstimation()V
    .registers 9

    #@0
    .prologue
    const v3, 0xac44

    #@3
    const/16 v7, 0x140

    #@5
    const/4 v2, 0x1

    #@6
    const/4 v1, 0x2

    #@7
    .line 118
    iget-boolean v0, p0, Landroid/media/SmartRingtone;->mIsFromPhoneApp:Z

    #@9
    if-nez v0, :cond_c

    #@b
    .line 286
    :cond_b
    :goto_b
    return-void

    #@c
    .line 120
    :cond_c
    iget-boolean v0, p0, Landroid/media/SmartRingtone;->mMicTestDone:Z

    #@e
    if-eq v0, v2, :cond_b

    #@10
    .line 122
    iget-object v0, p0, Landroid/media/SmartRingtone;->mAudioManager:Landroid/media/AudioManager;

    #@12
    invoke-virtual {v0}, Landroid/media/AudioManager;->getSmartRingtoneMode()Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_b

    #@18
    .line 125
    iget-object v0, p0, Landroid/media/SmartRingtone;->mDelayedVolumeUpHandler:Landroid/os/Handler;

    #@1a
    if-nez v0, :cond_35

    #@1c
    .line 126
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@1f
    move-result-object v0

    #@20
    iput-object v0, p0, Landroid/media/SmartRingtone;->mInitializationLooper:Landroid/os/Looper;

    #@22
    if-nez v0, :cond_2a

    #@24
    .line 127
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    #@27
    move-result-object v0

    #@28
    iput-object v0, p0, Landroid/media/SmartRingtone;->mInitializationLooper:Landroid/os/Looper;

    #@2a
    .line 130
    :cond_2a
    iget-object v0, p0, Landroid/media/SmartRingtone;->mInitializationLooper:Landroid/os/Looper;

    #@2c
    if-eqz v0, :cond_35

    #@2e
    .line 131
    new-instance v0, Landroid/media/SmartRingtone$2;

    #@30
    invoke-direct {v0, p0}, Landroid/media/SmartRingtone$2;-><init>(Landroid/media/SmartRingtone;)V

    #@33
    iput-object v0, p0, Landroid/media/SmartRingtone;->mDelayedVolumeUpHandler:Landroid/os/Handler;

    #@35
    .line 158
    :cond_35
    iput-boolean v2, p0, Landroid/media/SmartRingtone;->mMicTestDone:Z

    #@37
    .line 159
    const/4 v0, 0x0

    #@38
    iput-boolean v0, p0, Landroid/media/SmartRingtone;->mExitThread:Z

    #@3a
    .line 161
    invoke-static {v3, v1, v1}, Landroid/media/AudioRecord;->getMinBufferSize(III)I

    #@3d
    move-result v0

    #@3e
    iput v0, p0, Landroid/media/SmartRingtone;->buffersize:I

    #@40
    .line 165
    iget-object v0, p0, Landroid/media/SmartRingtone;->mContext:Landroid/content/Context;

    #@42
    const-string v1, "android.permission.RECORD_AUDIO"

    #@44
    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@47
    move-result v0

    #@48
    const/4 v1, -0x1

    #@49
    if-ne v0, v1, :cond_53

    #@4b
    .line 166
    sget-object v0, Landroid/media/SmartRingtone;->TAG:Ljava/lang/String;

    #@4d
    const-string v1, "Process doesn\'t have RECORD_AUDIO permission"

    #@4f
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    goto :goto_b

    #@53
    .line 171
    :cond_53
    :try_start_53
    new-instance v0, Landroid/media/AudioRecord;

    #@55
    const/4 v1, 0x1

    #@56
    const v2, 0xac44

    #@59
    const/4 v3, 0x2

    #@5a
    const/4 v4, 0x2

    #@5b
    iget v5, p0, Landroid/media/SmartRingtone;->buffersize:I

    #@5d
    invoke-direct/range {v0 .. v5}, Landroid/media/AudioRecord;-><init>(IIIII)V

    #@60
    iput-object v0, p0, Landroid/media/SmartRingtone;->arec:Landroid/media/AudioRecord;
    :try_end_62
    .catch Ljava/lang/IllegalArgumentException; {:try_start_53 .. :try_end_62} :catch_7a

    #@62
    .line 182
    iget-object v0, p0, Landroid/media/SmartRingtone;->arec:Landroid/media/AudioRecord;

    #@64
    invoke-virtual {v0}, Landroid/media/AudioRecord;->getState()I

    #@67
    move-result v0

    #@68
    if-nez v0, :cond_89

    #@6a
    .line 183
    sget-object v0, Landroid/media/SmartRingtone;->TAG:Ljava/lang/String;

    #@6c
    const-string v1, "arec AudioRecord.STATE_UNINITIALIZED"

    #@6e
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@71
    .line 184
    iget-object v0, p0, Landroid/media/SmartRingtone;->arec:Landroid/media/AudioRecord;

    #@73
    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    #@76
    .line 185
    const/4 v0, 0x0

    #@77
    iput-object v0, p0, Landroid/media/SmartRingtone;->arec:Landroid/media/AudioRecord;

    #@79
    goto :goto_b

    #@7a
    .line 176
    :catch_7a
    move-exception v6

    #@7b
    .line 177
    .local v6, ex:Ljava/lang/IllegalArgumentException;
    sget-object v0, Landroid/media/SmartRingtone;->TAG:Ljava/lang/String;

    #@7d
    const-string/jumbo v1, "smart ringtone caught "

    #@80
    invoke-static {v0, v1, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@83
    .line 178
    iget-object v0, p0, Landroid/media/SmartRingtone;->arec:Landroid/media/AudioRecord;

    #@85
    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    #@88
    goto :goto_b

    #@89
    .line 189
    .end local v6           #ex:Ljava/lang/IllegalArgumentException;
    :cond_89
    iget v0, p0, Landroid/media/SmartRingtone;->buffersize:I

    #@8b
    new-array v0, v0, [S

    #@8d
    iput-object v0, p0, Landroid/media/SmartRingtone;->buffer:[S

    #@8f
    .line 190
    iget-object v0, p0, Landroid/media/SmartRingtone;->arec:Landroid/media/AudioRecord;

    #@91
    invoke-virtual {v0}, Landroid/media/AudioRecord;->startRecording()V

    #@94
    .line 192
    iget v0, p0, Landroid/media/SmartRingtone;->buffersize:I

    #@96
    if-le v0, v7, :cond_9a

    #@98
    .line 193
    iput v7, p0, Landroid/media/SmartRingtone;->buffersize:I

    #@9a
    .line 196
    :cond_9a
    new-instance v0, Ljava/lang/Thread;

    #@9c
    new-instance v1, Landroid/media/SmartRingtone$3;

    #@9e
    invoke-direct {v1, p0}, Landroid/media/SmartRingtone$3;-><init>(Landroid/media/SmartRingtone;)V

    #@a1
    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@a4
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@a7
    goto/16 :goto_b
.end method

.method public declared-synchronized restoreVolumeAfterStop()V
    .registers 5

    #@0
    .prologue
    .line 338
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/media/SmartRingtone;->mIsFromPhoneApp:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_52

    #@3
    if-nez v0, :cond_7

    #@5
    .line 349
    :goto_5
    monitor-exit p0

    #@6
    return-void

    #@7
    .line 341
    :cond_7
    :try_start_7
    sget-boolean v0, Landroid/media/SmartRingtone;->LGE_DEBUG:Z

    #@9
    if-eqz v0, :cond_38

    #@b
    sget-object v0, Landroid/media/SmartRingtone;->TAG:Ljava/lang/String;

    #@d
    new-instance v1, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string/jumbo v2, "prev "

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    iget v2, p0, Landroid/media/SmartRingtone;->mPrevVolume:I

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    const-string v2, " getStreamVolume "

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    iget-object v2, p0, Landroid/media/SmartRingtone;->mAudioManager:Landroid/media/AudioManager;

    #@27
    iget v3, p0, Landroid/media/SmartRingtone;->mStreamType:I

    #@29
    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    #@2c
    move-result v2

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v1

    #@35
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 342
    :cond_38
    iget v0, p0, Landroid/media/SmartRingtone;->mPrevVolume:I

    #@3a
    if-lez v0, :cond_4b

    #@3c
    iget-boolean v0, p0, Landroid/media/SmartRingtone;->mMicTestDone:Z

    #@3e
    const/4 v1, 0x1

    #@3f
    if-ne v0, v1, :cond_4b

    #@41
    .line 344
    iget-object v0, p0, Landroid/media/SmartRingtone;->mAudioManager:Landroid/media/AudioManager;

    #@43
    iget v1, p0, Landroid/media/SmartRingtone;->mStreamType:I

    #@45
    iget v2, p0, Landroid/media/SmartRingtone;->mPrevVolume:I

    #@47
    const/4 v3, 0x0

    #@48
    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    #@4b
    .line 347
    :cond_4b
    const/4 v0, 0x0

    #@4c
    iput-boolean v0, p0, Landroid/media/SmartRingtone;->mMicTestDone:Z

    #@4e
    .line 348
    const/4 v0, 0x0

    #@4f
    iput v0, p0, Landroid/media/SmartRingtone;->mSmartRingtoneLevel:I
    :try_end_51
    .catchall {:try_start_7 .. :try_end_51} :catchall_52

    #@51
    goto :goto_5

    #@52
    .line 338
    :catchall_52
    move-exception v0

    #@53
    monitor-exit p0

    #@54
    throw v0
.end method

.method public setMediaPlayer(Landroid/media/MediaPlayer;)V
    .registers 4
    .parameter "player"

    #@0
    .prologue
    .line 105
    iput-object p1, p0, Landroid/media/SmartRingtone;->mAudio:Landroid/media/MediaPlayer;

    #@2
    .line 106
    iget v0, p0, Landroid/media/SmartRingtone;->mStreamType:I

    #@4
    const/4 v1, 0x2

    #@5
    if-ne v0, v1, :cond_11

    #@7
    .line 107
    iget-object v0, p0, Landroid/media/SmartRingtone;->mAudio:Landroid/media/MediaPlayer;

    #@9
    new-instance v1, Landroid/media/SmartRingtone$1;

    #@b
    invoke-direct {v1, p0}, Landroid/media/SmartRingtone$1;-><init>(Landroid/media/SmartRingtone;)V

    #@e
    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    #@11
    .line 115
    :cond_11
    return-void
.end method

.method public setStreamType(I)V
    .registers 2
    .parameter "streamType"

    #@0
    .prologue
    .line 99
    iput p1, p0, Landroid/media/SmartRingtone;->mStreamType:I

    #@2
    .line 100
    return-void
.end method
