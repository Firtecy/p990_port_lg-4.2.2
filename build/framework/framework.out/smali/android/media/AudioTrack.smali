.class public Landroid/media/AudioTrack;
.super Ljava/lang/Object;
.source "AudioTrack.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/AudioTrack$NativeEventHandlerDelegate;,
        Landroid/media/AudioTrack$OnPlaybackPositionUpdateListener;
    }
.end annotation


# static fields
.field public static final ERROR:I = -0x1

.field public static final ERROR_BAD_VALUE:I = -0x2

.field public static final ERROR_INVALID_OPERATION:I = -0x3

.field private static final ERROR_NATIVESETUP_AUDIOSYSTEM:I = -0x10

.field private static final ERROR_NATIVESETUP_INVALIDCHANNELMASK:I = -0x11

.field private static final ERROR_NATIVESETUP_INVALIDFORMAT:I = -0x12

.field private static final ERROR_NATIVESETUP_INVALIDSTREAMTYPE:I = -0x13

.field private static final ERROR_NATIVESETUP_NATIVEINITFAILED:I = -0x14

.field public static final MODE_STATIC:I = 0x0

.field public static final MODE_STREAM:I = 0x1

.field private static final NATIVE_EVENT_MARKER:I = 0x3

.field private static final NATIVE_EVENT_NEW_POS:I = 0x4

.field public static final PLAYSTATE_PAUSED:I = 0x2

.field public static final PLAYSTATE_PLAYING:I = 0x3

.field public static final PLAYSTATE_STOPPED:I = 0x1

.field public static final STATE_INITIALIZED:I = 0x1

.field public static final STATE_NO_STATIC_DATA:I = 0x2

.field public static final STATE_UNINITIALIZED:I = 0x0

.field public static final SUCCESS:I = 0x0

.field private static final SUPPORTED_OUT_CHANNELS:I = 0x4fc

.field private static final TAG:Ljava/lang/String; = "AudioTrack-Java"

.field private static final VOLUME_MAX:F = 1.0f

.field private static final VOLUME_MIN:F


# instance fields
.field private mAudioFormat:I

.field private mChannelConfiguration:I

.field private mChannelCount:I

.field private mChannels:I

.field private mDataLoadMode:I

.field private mEventHandlerDelegate:Landroid/media/AudioTrack$NativeEventHandlerDelegate;

.field private mInitializationLooper:Landroid/os/Looper;

.field private mJniData:I

.field private mNativeBufferSizeInBytes:I

.field private mNativeTrackInJavaObj:I

.field private mPlayState:I

.field private final mPlayStateLock:Ljava/lang/Object;

.field private mPositionListener:Landroid/media/AudioTrack$OnPlaybackPositionUpdateListener;

.field private final mPositionListenerLock:Ljava/lang/Object;

.field private mSampleRate:I

.field private mSessionId:I

.field private mState:I

.field private mStreamType:I


# direct methods
.method public constructor <init>(IIIIII)V
    .registers 15
    .parameter "streamType"
    .parameter "sampleRateInHz"
    .parameter "channelConfig"
    .parameter "audioFormat"
    .parameter "bufferSizeInBytes"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 267
    const/4 v7, 0x0

    #@1
    move-object v0, p0

    #@2
    move v1, p1

    #@3
    move v2, p2

    #@4
    move v3, p3

    #@5
    move v4, p4

    #@6
    move v5, p5

    #@7
    move v6, p6

    #@8
    invoke-direct/range {v0 .. v7}, Landroid/media/AudioTrack;-><init>(IIIIIII)V

    #@b
    .line 269
    return-void
.end method

.method public constructor <init>(IIIIIII)V
    .registers 19
    .parameter "streamType"
    .parameter "sampleRateInHz"
    .parameter "channelConfig"
    .parameter "audioFormat"
    .parameter "bufferSizeInBytes"
    .parameter "mode"
    .parameter "sessionId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 307
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 150
    const/4 v1, 0x0

    #@4
    iput v1, p0, Landroid/media/AudioTrack;->mState:I

    #@6
    .line 154
    const/4 v1, 0x1

    #@7
    iput v1, p0, Landroid/media/AudioTrack;->mPlayState:I

    #@9
    .line 158
    new-instance v1, Ljava/lang/Object;

    #@b
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@e
    iput-object v1, p0, Landroid/media/AudioTrack;->mPlayStateLock:Ljava/lang/Object;

    #@10
    .line 164
    const/4 v1, 0x0

    #@11
    iput-object v1, p0, Landroid/media/AudioTrack;->mPositionListener:Landroid/media/AudioTrack$OnPlaybackPositionUpdateListener;

    #@13
    .line 168
    new-instance v1, Ljava/lang/Object;

    #@15
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@18
    iput-object v1, p0, Landroid/media/AudioTrack;->mPositionListenerLock:Ljava/lang/Object;

    #@1a
    .line 172
    const/4 v1, 0x0

    #@1b
    iput v1, p0, Landroid/media/AudioTrack;->mNativeBufferSizeInBytes:I

    #@1d
    .line 176
    const/4 v1, 0x0

    #@1e
    iput-object v1, p0, Landroid/media/AudioTrack;->mEventHandlerDelegate:Landroid/media/AudioTrack$NativeEventHandlerDelegate;

    #@20
    .line 180
    const/4 v1, 0x0

    #@21
    iput-object v1, p0, Landroid/media/AudioTrack;->mInitializationLooper:Landroid/os/Looper;

    #@23
    .line 188
    const/4 v1, 0x1

    #@24
    iput v1, p0, Landroid/media/AudioTrack;->mChannelCount:I

    #@26
    .line 192
    const/4 v1, 0x4

    #@27
    iput v1, p0, Landroid/media/AudioTrack;->mChannels:I

    #@29
    .line 201
    const/4 v1, 0x3

    #@2a
    iput v1, p0, Landroid/media/AudioTrack;->mStreamType:I

    #@2c
    .line 205
    const/4 v1, 0x1

    #@2d
    iput v1, p0, Landroid/media/AudioTrack;->mDataLoadMode:I

    #@2f
    .line 209
    const/4 v1, 0x4

    #@30
    iput v1, p0, Landroid/media/AudioTrack;->mChannelConfiguration:I

    #@32
    .line 215
    const/4 v1, 0x2

    #@33
    iput v1, p0, Landroid/media/AudioTrack;->mAudioFormat:I

    #@35
    .line 219
    const/4 v1, 0x0

    #@36
    iput v1, p0, Landroid/media/AudioTrack;->mSessionId:I

    #@38
    .line 308
    const/4 v1, 0x0

    #@39
    iput v1, p0, Landroid/media/AudioTrack;->mState:I

    #@3b
    .line 311
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@3e
    move-result-object v1

    #@3f
    iput-object v1, p0, Landroid/media/AudioTrack;->mInitializationLooper:Landroid/os/Looper;

    #@41
    if-nez v1, :cond_49

    #@43
    .line 312
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    #@46
    move-result-object v1

    #@47
    iput-object v1, p0, Landroid/media/AudioTrack;->mInitializationLooper:Landroid/os/Looper;

    #@49
    :cond_49
    move-object v1, p0

    #@4a
    move v2, p1

    #@4b
    move v3, p2

    #@4c
    move v4, p3

    #@4d
    move v5, p4

    #@4e
    move/from16 v6, p6

    #@50
    .line 315
    invoke-direct/range {v1 .. v6}, Landroid/media/AudioTrack;->audioParamCheck(IIIII)V

    #@53
    .line 317
    move/from16 v0, p5

    #@55
    invoke-direct {p0, v0}, Landroid/media/AudioTrack;->audioBuffSizeCheck(I)V

    #@58
    .line 319
    if-gez p7, :cond_75

    #@5a
    .line 320
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@5c
    new-instance v2, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    const-string v3, "Invalid audio session ID: "

    #@63
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v2

    #@67
    move/from16 v0, p7

    #@69
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v2

    #@6d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v2

    #@71
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@74
    throw v1

    #@75
    .line 323
    :cond_75
    const/4 v1, 0x1

    #@76
    new-array v9, v1, [I

    #@78
    .line 324
    .local v9, session:[I
    const/4 v1, 0x0

    #@79
    aput p7, v9, v1

    #@7b
    .line 326
    new-instance v2, Ljava/lang/ref/WeakReference;

    #@7d
    invoke-direct {v2, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@80
    iget v3, p0, Landroid/media/AudioTrack;->mStreamType:I

    #@82
    iget v4, p0, Landroid/media/AudioTrack;->mSampleRate:I

    #@84
    iget v5, p0, Landroid/media/AudioTrack;->mChannels:I

    #@86
    iget v6, p0, Landroid/media/AudioTrack;->mAudioFormat:I

    #@88
    iget v7, p0, Landroid/media/AudioTrack;->mNativeBufferSizeInBytes:I

    #@8a
    iget v8, p0, Landroid/media/AudioTrack;->mDataLoadMode:I

    #@8c
    move-object v1, p0

    #@8d
    invoke-direct/range {v1 .. v9}, Landroid/media/AudioTrack;->native_setup(Ljava/lang/Object;IIIIII[I)I

    #@90
    move-result v10

    #@91
    .line 329
    .local v10, initResult:I
    if-eqz v10, :cond_b0

    #@93
    .line 330
    new-instance v1, Ljava/lang/StringBuilder;

    #@95
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@98
    const-string v2, "Error code "

    #@9a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v1

    #@9e
    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v1

    #@a2
    const-string v2, " when initializing AudioTrack."

    #@a4
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v1

    #@a8
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ab
    move-result-object v1

    #@ac
    invoke-static {v1}, Landroid/media/AudioTrack;->loge(Ljava/lang/String;)V

    #@af
    .line 341
    :goto_af
    return-void

    #@b0
    .line 334
    :cond_b0
    const/4 v1, 0x0

    #@b1
    aget v1, v9, v1

    #@b3
    iput v1, p0, Landroid/media/AudioTrack;->mSessionId:I

    #@b5
    .line 336
    iget v1, p0, Landroid/media/AudioTrack;->mDataLoadMode:I

    #@b7
    if-nez v1, :cond_bd

    #@b9
    .line 337
    const/4 v1, 0x2

    #@ba
    iput v1, p0, Landroid/media/AudioTrack;->mState:I

    #@bc
    goto :goto_af

    #@bd
    .line 339
    :cond_bd
    const/4 v1, 0x1

    #@be
    iput v1, p0, Landroid/media/AudioTrack;->mState:I

    #@c0
    goto :goto_af
.end method

.method static synthetic access$000(Landroid/media/AudioTrack;)Landroid/os/Looper;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Landroid/media/AudioTrack;->mInitializationLooper:Landroid/os/Looper;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/media/AudioTrack;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Landroid/media/AudioTrack;->mPositionListenerLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/media/AudioTrack;)Landroid/media/AudioTrack$OnPlaybackPositionUpdateListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Landroid/media/AudioTrack;->mPositionListener:Landroid/media/AudioTrack$OnPlaybackPositionUpdateListener;

    #@2
    return-object v0
.end method

.method private audioBuffSizeCheck(I)V
    .registers 7
    .parameter "audioBufferSize"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 490
    iget v3, p0, Landroid/media/AudioTrack;->mChannelCount:I

    #@3
    iget v1, p0, Landroid/media/AudioTrack;->mAudioFormat:I

    #@5
    const/4 v4, 0x3

    #@6
    if-ne v1, v4, :cond_19

    #@8
    move v1, v2

    #@9
    :goto_9
    mul-int v0, v3, v1

    #@b
    .line 492
    .local v0, frameSizeInBytes:I
    rem-int v1, p1, v0

    #@d
    if-nez v1, :cond_11

    #@f
    if-ge p1, v2, :cond_1b

    #@11
    .line 493
    :cond_11
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@13
    const-string v2, "Invalid audio buffer size."

    #@15
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@18
    throw v1

    #@19
    .line 490
    .end local v0           #frameSizeInBytes:I
    :cond_19
    const/4 v1, 0x2

    #@1a
    goto :goto_9

    #@1b
    .line 496
    .restart local v0       #frameSizeInBytes:I
    :cond_1b
    iput p1, p0, Landroid/media/AudioTrack;->mNativeBufferSizeInBytes:I

    #@1d
    .line 497
    return-void
.end method

.method private audioParamCheck(IIIII)V
    .registers 11
    .parameter "streamType"
    .parameter "sampleRateInHz"
    .parameter "channelConfig"
    .parameter "audioFormat"
    .parameter "mode"

    #@0
    .prologue
    const/4 v4, 0x4

    #@1
    const/4 v3, 0x2

    #@2
    const/4 v2, 0x1

    #@3
    const/4 v1, 0x0

    #@4
    .line 367
    if-eq p1, v4, :cond_25

    #@6
    const/4 v0, 0x3

    #@7
    if-eq p1, v0, :cond_25

    #@9
    if-eq p1, v3, :cond_25

    #@b
    if-eq p1, v2, :cond_25

    #@d
    if-eqz p1, :cond_25

    #@f
    const/4 v0, 0x5

    #@10
    if-eq p1, v0, :cond_25

    #@12
    const/4 v0, 0x6

    #@13
    if-eq p1, v0, :cond_25

    #@15
    const/16 v0, 0x8

    #@17
    if-eq p1, v0, :cond_25

    #@19
    const/16 v0, 0x9

    #@1b
    if-eq p1, v0, :cond_25

    #@1d
    .line 376
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1f
    const-string v1, "Invalid stream type."

    #@21
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@24
    throw v0

    #@25
    .line 378
    :cond_25
    iput p1, p0, Landroid/media/AudioTrack;->mStreamType:I

    #@27
    .line 383
    const/16 v0, 0xfa0

    #@29
    if-lt p2, v0, :cond_30

    #@2b
    const v0, 0xbb80

    #@2e
    if-le p2, v0, :cond_49

    #@30
    .line 384
    :cond_30
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@32
    new-instance v1, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v1

    #@3b
    const-string v2, "Hz is not a supported sample rate."

    #@3d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v1

    #@45
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@48
    throw v0

    #@49
    .line 387
    :cond_49
    iput p2, p0, Landroid/media/AudioTrack;->mSampleRate:I

    #@4b
    .line 392
    iput p3, p0, Landroid/media/AudioTrack;->mChannelConfiguration:I

    #@4d
    .line 394
    sparse-switch p3, :sswitch_data_9a

    #@50
    .line 407
    invoke-static {p3}, Landroid/media/AudioTrack;->isMultichannelConfigSupported(I)Z

    #@53
    move-result v0

    #@54
    if-nez v0, :cond_7c

    #@56
    .line 409
    iput v1, p0, Landroid/media/AudioTrack;->mChannelCount:I

    #@58
    .line 410
    iput v1, p0, Landroid/media/AudioTrack;->mChannels:I

    #@5a
    .line 411
    iput v1, p0, Landroid/media/AudioTrack;->mChannelConfiguration:I

    #@5c
    .line 412
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@5e
    const-string v1, "Unsupported channel configuration."

    #@60
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@63
    throw v0

    #@64
    .line 398
    :sswitch_64
    iput v2, p0, Landroid/media/AudioTrack;->mChannelCount:I

    #@66
    .line 399
    iput v4, p0, Landroid/media/AudioTrack;->mChannels:I

    #@68
    .line 421
    :goto_68
    sparse-switch p4, :sswitch_data_b0

    #@6b
    .line 435
    iput v1, p0, Landroid/media/AudioTrack;->mAudioFormat:I

    #@6d
    .line 436
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@6f
    const-string v1, "Unsupported sample encoding. Should be ENCODING_PCM_8BIT or ENCODING_PCM_16BIT."

    #@71
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@74
    throw v0

    #@75
    .line 403
    :sswitch_75
    iput v3, p0, Landroid/media/AudioTrack;->mChannelCount:I

    #@77
    .line 404
    const/16 v0, 0xc

    #@79
    iput v0, p0, Landroid/media/AudioTrack;->mChannels:I

    #@7b
    goto :goto_68

    #@7c
    .line 414
    :cond_7c
    iput p3, p0, Landroid/media/AudioTrack;->mChannels:I

    #@7e
    .line 415
    invoke-static {p3}, Ljava/lang/Integer;->bitCount(I)I

    #@81
    move-result v0

    #@82
    iput v0, p0, Landroid/media/AudioTrack;->mChannelCount:I

    #@84
    goto :goto_68

    #@85
    .line 423
    :sswitch_85
    iput v3, p0, Landroid/media/AudioTrack;->mAudioFormat:I

    #@87
    .line 442
    :goto_87
    if-eq p5, v2, :cond_96

    #@89
    if-eqz p5, :cond_96

    #@8b
    .line 443
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8d
    const-string v1, "Invalid mode."

    #@8f
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@92
    throw v0

    #@93
    .line 432
    :sswitch_93
    iput p4, p0, Landroid/media/AudioTrack;->mAudioFormat:I

    #@95
    goto :goto_87

    #@96
    .line 445
    :cond_96
    iput p5, p0, Landroid/media/AudioTrack;->mDataLoadMode:I

    #@98
    .line 447
    return-void

    #@99
    .line 394
    nop

    #@9a
    :sswitch_data_9a
    .sparse-switch
        0x1 -> :sswitch_64
        0x2 -> :sswitch_64
        0x3 -> :sswitch_75
        0x4 -> :sswitch_64
        0xc -> :sswitch_75
    .end sparse-switch

    #@b0
    .line 421
    :sswitch_data_b0
    .sparse-switch
        0x1 -> :sswitch_85
        0x2 -> :sswitch_93
        0x3 -> :sswitch_93
        0x64 -> :sswitch_93
        0x65 -> :sswitch_93
        0x66 -> :sswitch_93
        0x67 -> :sswitch_93
        0x68 -> :sswitch_93
    .end sparse-switch
.end method

.method public static getMaxVolume()F
    .registers 1

    #@0
    .prologue
    .line 538
    const/high16 v0, 0x3f80

    #@2
    return v0
.end method

.method public static getMinBufferSize(III)I
    .registers 7
    .parameter "sampleRateInHz"
    .parameter "channelConfig"
    .parameter "audioFormat"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    const/4 v1, -0x2

    #@2
    .line 669
    const/4 v0, 0x0

    #@3
    .line 670
    .local v0, channelCount:I
    sparse-switch p1, :sswitch_data_6e

    #@6
    .line 680
    and-int/lit16 v3, p1, 0x4fc

    #@8
    if-eq v3, p1, :cond_33

    #@a
    .line 682
    const-string v2, "getMinBufferSize(): Invalid channel configuration."

    #@c
    invoke-static {v2}, Landroid/media/AudioTrack;->loge(Ljava/lang/String;)V

    #@f
    .line 712
    :cond_f
    :goto_f
    return v1

    #@10
    .line 673
    :sswitch_10
    const/4 v0, 0x1

    #@11
    .line 689
    :goto_11
    const/4 v3, 0x2

    #@12
    if-eq p2, v3, :cond_38

    #@14
    const/4 v3, 0x3

    #@15
    if-eq p2, v3, :cond_38

    #@17
    const/16 v3, 0x64

    #@19
    if-eq p2, v3, :cond_38

    #@1b
    const/16 v3, 0x65

    #@1d
    if-eq p2, v3, :cond_38

    #@1f
    const/16 v3, 0x66

    #@21
    if-eq p2, v3, :cond_38

    #@23
    const/16 v3, 0x67

    #@25
    if-eq p2, v3, :cond_38

    #@27
    const/16 v3, 0x68

    #@29
    if-eq p2, v3, :cond_38

    #@2b
    .line 696
    const-string v2, "getMinBufferSize(): Invalid audio format."

    #@2d
    invoke-static {v2}, Landroid/media/AudioTrack;->loge(Ljava/lang/String;)V

    #@30
    goto :goto_f

    #@31
    .line 677
    :sswitch_31
    const/4 v0, 0x2

    #@32
    .line 678
    goto :goto_11

    #@33
    .line 685
    :cond_33
    invoke-static {p1}, Ljava/lang/Integer;->bitCount(I)I

    #@36
    move-result v0

    #@37
    goto :goto_11

    #@38
    .line 701
    :cond_38
    const/16 v3, 0xfa0

    #@3a
    if-lt p0, v3, :cond_41

    #@3c
    const v3, 0xbb80

    #@3f
    if-le p0, v3, :cond_5e

    #@41
    .line 702
    :cond_41
    new-instance v2, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v3, "getMinBufferSize(): "

    #@48
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v2

    #@4c
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v2

    #@50
    const-string v3, "Hz is not a supported sample rate."

    #@52
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v2

    #@56
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v2

    #@5a
    invoke-static {v2}, Landroid/media/AudioTrack;->loge(Ljava/lang/String;)V

    #@5d
    goto :goto_f

    #@5e
    .line 706
    :cond_5e
    invoke-static {p0, v0, p2}, Landroid/media/AudioTrack;->native_get_min_buff_size(III)I

    #@61
    move-result v1

    #@62
    .line 707
    .local v1, size:I
    if-eq v1, v2, :cond_66

    #@64
    if-nez v1, :cond_f

    #@66
    .line 708
    :cond_66
    const-string v3, "getMinBufferSize(): error querying hardware"

    #@68
    invoke-static {v3}, Landroid/media/AudioTrack;->loge(Ljava/lang/String;)V

    #@6b
    move v1, v2

    #@6c
    .line 709
    goto :goto_f

    #@6d
    .line 670
    nop

    #@6e
    :sswitch_data_6e
    .sparse-switch
        0x2 -> :sswitch_10
        0x3 -> :sswitch_31
        0x4 -> :sswitch_10
        0xc -> :sswitch_31
    .end sparse-switch
.end method

.method public static getMinVolume()F
    .registers 1

    #@0
    .prologue
    .line 529
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public static getNativeOutputSampleRate(I)I
    .registers 2
    .parameter "streamType"

    #@0
    .prologue
    .line 648
    invoke-static {p0}, Landroid/media/AudioTrack;->native_get_output_sample_rate(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private static isMultichannelConfigSupported(I)Z
    .registers 6
    .parameter "channelConfig"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 456
    and-int/lit16 v3, p0, 0x4fc

    #@3
    if-eq v3, p0, :cond_d

    #@5
    .line 457
    const-string v3, "AudioTrack-Java"

    #@7
    const-string v4, "Channel configuration features unsupported channels"

    #@9
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 477
    :goto_c
    return v2

    #@d
    .line 463
    :cond_d
    const/16 v1, 0xc

    #@f
    .line 465
    .local v1, frontPair:I
    and-int/lit8 v3, p0, 0xc

    #@11
    const/16 v4, 0xc

    #@13
    if-eq v3, v4, :cond_1d

    #@15
    .line 466
    const-string v3, "AudioTrack-Java"

    #@17
    const-string v4, "Front channels must be present in multichannel configurations"

    #@19
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    goto :goto_c

    #@1d
    .line 469
    :cond_1d
    const/16 v0, 0xc0

    #@1f
    .line 471
    .local v0, backPair:I
    and-int/lit16 v3, p0, 0xc0

    #@21
    if-eqz v3, :cond_31

    #@23
    .line 472
    and-int/lit16 v3, p0, 0xc0

    #@25
    const/16 v4, 0xc0

    #@27
    if-eq v3, v4, :cond_31

    #@29
    .line 473
    const-string v3, "AudioTrack-Java"

    #@2b
    const-string v4, "Rear channels can\'t be used independently"

    #@2d
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    goto :goto_c

    #@31
    .line 477
    :cond_31
    const/4 v2, 0x1

    #@32
    goto :goto_c
.end method

.method private static logd(Ljava/lang/String;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 1275
    const-string v0, "AudioTrack-Java"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[ android.media.AudioTrack ] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1276
    return-void
.end method

.method private static loge(Ljava/lang/String;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 1279
    const-string v0, "AudioTrack-Java"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[ android.media.AudioTrack ] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1280
    return-void
.end method

.method private final native native_attachAuxEffect(I)I
.end method

.method private final native native_finalize()V
.end method

.method private final native native_flush()V
.end method

.method private final native native_get_marker_pos()I
.end method

.method private static final native native_get_min_buff_size(III)I
.end method

.method private final native native_get_native_frame_count()I
.end method

.method private static final native native_get_output_sample_rate(I)I
.end method

.method private final native native_get_playback_rate()I
.end method

.method private final native native_get_pos_update_period()I
.end method

.method private final native native_get_position()I
.end method

.method private final native native_get_session_id()I
.end method

.method private final native native_pause()V
.end method

.method private final native native_release()V
.end method

.method private final native native_reload_static()I
.end method

.method private final native native_setAuxEffectSendLevel(F)V
.end method

.method private final native native_setVolume(FF)V
.end method

.method private final native native_set_loop(III)I
.end method

.method private final native native_set_marker_pos(I)I
.end method

.method private final native native_set_playback_rate(I)I
.end method

.method private final native native_set_pos_update_period(I)I
.end method

.method private final native native_set_position(I)I
.end method

.method private final native native_setup(Ljava/lang/Object;IIIIII[I)I
.end method

.method private final native native_start()V
.end method

.method private final native native_stop()V
.end method

.method private final native native_write_byte([BIII)I
.end method

.method private final native native_write_short([SIII)I
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .registers 8
    .parameter "audiotrack_ref"
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"
    .parameter "obj"

    #@0
    .prologue
    .line 1201
    check-cast p0, Ljava/lang/ref/WeakReference;

    #@2
    .end local p0
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Landroid/media/AudioTrack;

    #@8
    .line 1202
    .local v1, track:Landroid/media/AudioTrack;
    if-nez v1, :cond_b

    #@a
    .line 1212
    :cond_a
    :goto_a
    return-void

    #@b
    .line 1206
    :cond_b
    iget-object v2, v1, Landroid/media/AudioTrack;->mEventHandlerDelegate:Landroid/media/AudioTrack$NativeEventHandlerDelegate;

    #@d
    if-eqz v2, :cond_a

    #@f
    .line 1207
    iget-object v2, v1, Landroid/media/AudioTrack;->mEventHandlerDelegate:Landroid/media/AudioTrack$NativeEventHandlerDelegate;

    #@11
    invoke-virtual {v2}, Landroid/media/AudioTrack$NativeEventHandlerDelegate;->getHandler()Landroid/os/Handler;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, p1, p2, p3, p4}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@18
    move-result-object v0

    #@19
    .line 1209
    .local v0, m:Landroid/os/Message;
    iget-object v2, v1, Landroid/media/AudioTrack;->mEventHandlerDelegate:Landroid/media/AudioTrack$NativeEventHandlerDelegate;

    #@1b
    invoke-virtual {v2}, Landroid/media/AudioTrack$NativeEventHandlerDelegate;->getHandler()Landroid/os/Handler;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@22
    goto :goto_a
.end method


# virtual methods
.method public attachAuxEffect(I)I
    .registers 4
    .parameter "effectId"

    #@0
    .prologue
    .line 1072
    iget v0, p0, Landroid/media/AudioTrack;->mState:I

    #@2
    const/4 v1, 0x1

    #@3
    if-eq v0, v1, :cond_7

    #@5
    .line 1073
    const/4 v0, -0x3

    #@6
    .line 1075
    :goto_6
    return v0

    #@7
    :cond_7
    invoke-direct {p0, p1}, Landroid/media/AudioTrack;->native_attachAuxEffect(I)I

    #@a
    move-result v0

    #@b
    goto :goto_6
.end method

.method protected finalize()V
    .registers 1

    #@0
    .prologue
    .line 517
    invoke-direct {p0}, Landroid/media/AudioTrack;->native_finalize()V

    #@3
    .line 518
    return-void
.end method

.method public flush()V
    .registers 3

    #@0
    .prologue
    .line 957
    iget v0, p0, Landroid/media/AudioTrack;->mState:I

    #@2
    const/4 v1, 0x1

    #@3
    if-ne v0, v1, :cond_8

    #@5
    .line 959
    invoke-direct {p0}, Landroid/media/AudioTrack;->native_flush()V

    #@8
    .line 962
    :cond_8
    return-void
.end method

.method public getAudioFormat()I
    .registers 2

    #@0
    .prologue
    .line 560
    iget v0, p0, Landroid/media/AudioTrack;->mAudioFormat:I

    #@2
    return v0
.end method

.method public getAudioSessionId()I
    .registers 2

    #@0
    .prologue
    .line 722
    iget v0, p0, Landroid/media/AudioTrack;->mSessionId:I

    #@2
    return v0
.end method

.method public getChannelConfiguration()I
    .registers 2

    #@0
    .prologue
    .line 581
    iget v0, p0, Landroid/media/AudioTrack;->mChannelConfiguration:I

    #@2
    return v0
.end method

.method public getChannelCount()I
    .registers 2

    #@0
    .prologue
    .line 588
    iget v0, p0, Landroid/media/AudioTrack;->mChannelCount:I

    #@2
    return v0
.end method

.method protected getNativeFrameCount()I
    .registers 2

    #@0
    .prologue
    .line 620
    invoke-direct {p0}, Landroid/media/AudioTrack;->native_get_native_frame_count()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getNotificationMarkerPosition()I
    .registers 2

    #@0
    .prologue
    .line 627
    invoke-direct {p0}, Landroid/media/AudioTrack;->native_get_marker_pos()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getPlayState()I
    .registers 3

    #@0
    .prologue
    .line 611
    iget-object v1, p0, Landroid/media/AudioTrack;->mPlayStateLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 612
    :try_start_3
    iget v0, p0, Landroid/media/AudioTrack;->mPlayState:I

    #@5
    monitor-exit v1

    #@6
    return v0

    #@7
    .line 613
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public getPlaybackHeadPosition()I
    .registers 2

    #@0
    .prologue
    .line 641
    invoke-direct {p0}, Landroid/media/AudioTrack;->native_get_position()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getPlaybackRate()I
    .registers 2

    #@0
    .prologue
    .line 552
    invoke-direct {p0}, Landroid/media/AudioTrack;->native_get_playback_rate()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getPositionNotificationPeriod()I
    .registers 2

    #@0
    .prologue
    .line 634
    invoke-direct {p0}, Landroid/media/AudioTrack;->native_get_pos_update_period()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getSampleRate()I
    .registers 2

    #@0
    .prologue
    .line 545
    iget v0, p0, Landroid/media/AudioTrack;->mSampleRate:I

    #@2
    return v0
.end method

.method public getState()I
    .registers 2

    #@0
    .prologue
    .line 601
    iget v0, p0, Landroid/media/AudioTrack;->mState:I

    #@2
    return v0
.end method

.method public getStreamType()I
    .registers 2

    #@0
    .prologue
    .line 571
    iget v0, p0, Landroid/media/AudioTrack;->mStreamType:I

    #@2
    return v0
.end method

.method public pause()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 935
    iget v0, p0, Landroid/media/AudioTrack;->mState:I

    #@2
    const/4 v1, 0x1

    #@3
    if-eq v0, v1, :cond_e

    #@5
    .line 936
    new-instance v0, Ljava/lang/IllegalStateException;

    #@7
    const-string/jumbo v1, "pause() called on uninitialized AudioTrack."

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 941
    :cond_e
    iget-object v1, p0, Landroid/media/AudioTrack;->mPlayStateLock:Ljava/lang/Object;

    #@10
    monitor-enter v1

    #@11
    .line 942
    :try_start_11
    invoke-direct {p0}, Landroid/media/AudioTrack;->native_pause()V

    #@14
    .line 943
    const/4 v0, 0x2

    #@15
    iput v0, p0, Landroid/media/AudioTrack;->mPlayState:I

    #@17
    .line 944
    monitor-exit v1

    #@18
    .line 945
    return-void

    #@19
    .line 944
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_11 .. :try_end_1b} :catchall_19

    #@1b
    throw v0
.end method

.method public play()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 895
    iget v0, p0, Landroid/media/AudioTrack;->mState:I

    #@2
    const/4 v1, 0x1

    #@3
    if-eq v0, v1, :cond_e

    #@5
    .line 896
    new-instance v0, Ljava/lang/IllegalStateException;

    #@7
    const-string/jumbo v1, "play() called on uninitialized AudioTrack."

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 899
    :cond_e
    iget-object v1, p0, Landroid/media/AudioTrack;->mPlayStateLock:Ljava/lang/Object;

    #@10
    monitor-enter v1

    #@11
    .line 900
    :try_start_11
    invoke-direct {p0}, Landroid/media/AudioTrack;->native_start()V

    #@14
    .line 901
    const/4 v0, 0x3

    #@15
    iput v0, p0, Landroid/media/AudioTrack;->mPlayState:I

    #@17
    .line 902
    monitor-exit v1

    #@18
    .line 903
    return-void

    #@19
    .line 902
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_11 .. :try_end_1b} :catchall_19

    #@1b
    throw v0
.end method

.method public release()V
    .registers 2

    #@0
    .prologue
    .line 507
    :try_start_0
    invoke-virtual {p0}, Landroid/media/AudioTrack;->stop()V
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_3} :catch_a

    #@3
    .line 511
    :goto_3
    invoke-direct {p0}, Landroid/media/AudioTrack;->native_release()V

    #@6
    .line 512
    const/4 v0, 0x0

    #@7
    iput v0, p0, Landroid/media/AudioTrack;->mState:I

    #@9
    .line 513
    return-void

    #@a
    .line 508
    :catch_a
    move-exception v0

    #@b
    goto :goto_3
.end method

.method public reloadStaticData()I
    .registers 3

    #@0
    .prologue
    .line 1044
    iget v0, p0, Landroid/media/AudioTrack;->mDataLoadMode:I

    #@2
    const/4 v1, 0x1

    #@3
    if-ne v0, v1, :cond_7

    #@5
    .line 1045
    const/4 v0, -0x3

    #@6
    .line 1047
    :goto_6
    return v0

    #@7
    :cond_7
    invoke-direct {p0}, Landroid/media/AudioTrack;->native_reload_static()I

    #@a
    move-result v0

    #@b
    goto :goto_6
.end method

.method public setAuxEffectSendLevel(F)I
    .registers 4
    .parameter "level"

    #@0
    .prologue
    .line 1094
    iget v0, p0, Landroid/media/AudioTrack;->mState:I

    #@2
    const/4 v1, 0x1

    #@3
    if-eq v0, v1, :cond_7

    #@5
    .line 1095
    const/4 v0, -0x3

    #@6
    .line 1105
    :goto_6
    return v0

    #@7
    .line 1098
    :cond_7
    invoke-static {}, Landroid/media/AudioTrack;->getMinVolume()F

    #@a
    move-result v0

    #@b
    cmpg-float v0, p1, v0

    #@d
    if-gez v0, :cond_13

    #@f
    .line 1099
    invoke-static {}, Landroid/media/AudioTrack;->getMinVolume()F

    #@12
    move-result p1

    #@13
    .line 1101
    :cond_13
    invoke-static {}, Landroid/media/AudioTrack;->getMaxVolume()F

    #@16
    move-result v0

    #@17
    cmpl-float v0, p1, v0

    #@19
    if-lez v0, :cond_1f

    #@1b
    .line 1102
    invoke-static {}, Landroid/media/AudioTrack;->getMaxVolume()F

    #@1e
    move-result p1

    #@1f
    .line 1104
    :cond_1f
    invoke-direct {p0, p1}, Landroid/media/AudioTrack;->native_setAuxEffectSendLevel(F)V

    #@22
    .line 1105
    const/4 v0, 0x0

    #@23
    goto :goto_6
.end method

.method public setLoopPoints(III)I
    .registers 6
    .parameter "startInFrames"
    .parameter "endInFrames"
    .parameter "loopCount"

    #@0
    .prologue
    .line 869
    iget v0, p0, Landroid/media/AudioTrack;->mDataLoadMode:I

    #@2
    const/4 v1, 0x1

    #@3
    if-ne v0, v1, :cond_7

    #@5
    .line 870
    const/4 v0, -0x3

    #@6
    .line 872
    :goto_6
    return v0

    #@7
    :cond_7
    invoke-direct {p0, p1, p2, p3}, Landroid/media/AudioTrack;->native_set_loop(III)I

    #@a
    move-result v0

    #@b
    goto :goto_6
.end method

.method public setNotificationMarkerPosition(I)I
    .registers 4
    .parameter "markerInFrames"

    #@0
    .prologue
    .line 823
    iget v0, p0, Landroid/media/AudioTrack;->mState:I

    #@2
    const/4 v1, 0x1

    #@3
    if-eq v0, v1, :cond_7

    #@5
    .line 824
    const/4 v0, -0x3

    #@6
    .line 826
    :goto_6
    return v0

    #@7
    :cond_7
    invoke-direct {p0, p1}, Landroid/media/AudioTrack;->native_set_marker_pos(I)I

    #@a
    move-result v0

    #@b
    goto :goto_6
.end method

.method public setPlaybackHeadPosition(I)I
    .registers 5
    .parameter "positionInFrames"

    #@0
    .prologue
    .line 850
    iget-object v1, p0, Landroid/media/AudioTrack;->mPlayStateLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 851
    :try_start_3
    iget v0, p0, Landroid/media/AudioTrack;->mPlayState:I

    #@5
    const/4 v2, 0x1

    #@6
    if-eq v0, v2, :cond_d

    #@8
    iget v0, p0, Landroid/media/AudioTrack;->mPlayState:I

    #@a
    const/4 v2, 0x2

    #@b
    if-ne v0, v2, :cond_13

    #@d
    .line 852
    :cond_d
    invoke-direct {p0, p1}, Landroid/media/AudioTrack;->native_set_position(I)I

    #@10
    move-result v0

    #@11
    monitor-exit v1

    #@12
    .line 854
    :goto_12
    return v0

    #@13
    :cond_13
    const/4 v0, -0x3

    #@14
    monitor-exit v1

    #@15
    goto :goto_12

    #@16
    .line 856
    :catchall_16
    move-exception v0

    #@17
    monitor-exit v1
    :try_end_18
    .catchall {:try_start_3 .. :try_end_18} :catchall_16

    #@18
    throw v0
.end method

.method public setPlaybackPositionUpdateListener(Landroid/media/AudioTrack$OnPlaybackPositionUpdateListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 736
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/media/AudioTrack;->setPlaybackPositionUpdateListener(Landroid/media/AudioTrack$OnPlaybackPositionUpdateListener;Landroid/os/Handler;)V

    #@4
    .line 737
    return-void
.end method

.method public setPlaybackPositionUpdateListener(Landroid/media/AudioTrack$OnPlaybackPositionUpdateListener;Landroid/os/Handler;)V
    .registers 5
    .parameter "listener"
    .parameter "handler"

    #@0
    .prologue
    .line 749
    iget-object v1, p0, Landroid/media/AudioTrack;->mPositionListenerLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 750
    :try_start_3
    iput-object p1, p0, Landroid/media/AudioTrack;->mPositionListener:Landroid/media/AudioTrack$OnPlaybackPositionUpdateListener;

    #@5
    .line 751
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_10

    #@6
    .line 752
    if-eqz p1, :cond_f

    #@8
    .line 753
    new-instance v0, Landroid/media/AudioTrack$NativeEventHandlerDelegate;

    #@a
    invoke-direct {v0, p0, p0, p2}, Landroid/media/AudioTrack$NativeEventHandlerDelegate;-><init>(Landroid/media/AudioTrack;Landroid/media/AudioTrack;Landroid/os/Handler;)V

    #@d
    iput-object v0, p0, Landroid/media/AudioTrack;->mEventHandlerDelegate:Landroid/media/AudioTrack$NativeEventHandlerDelegate;

    #@f
    .line 756
    :cond_f
    return-void

    #@10
    .line 751
    :catchall_10
    move-exception v0

    #@11
    :try_start_11
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_11 .. :try_end_12} :catchall_10

    #@12
    throw v0
.end method

.method public setPlaybackRate(I)I
    .registers 4
    .parameter "sampleRateInHz"

    #@0
    .prologue
    .line 806
    iget v0, p0, Landroid/media/AudioTrack;->mState:I

    #@2
    const/4 v1, 0x1

    #@3
    if-eq v0, v1, :cond_7

    #@5
    .line 807
    const/4 v0, -0x3

    #@6
    .line 812
    :goto_6
    return v0

    #@7
    .line 809
    :cond_7
    if-gtz p1, :cond_b

    #@9
    .line 810
    const/4 v0, -0x2

    #@a
    goto :goto_6

    #@b
    .line 812
    :cond_b
    invoke-direct {p0, p1}, Landroid/media/AudioTrack;->native_set_playback_rate(I)I

    #@e
    move-result v0

    #@f
    goto :goto_6
.end method

.method public setPositionNotificationPeriod(I)I
    .registers 4
    .parameter "periodInFrames"

    #@0
    .prologue
    .line 836
    iget v0, p0, Landroid/media/AudioTrack;->mState:I

    #@2
    const/4 v1, 0x1

    #@3
    if-eq v0, v1, :cond_7

    #@5
    .line 837
    const/4 v0, -0x3

    #@6
    .line 839
    :goto_6
    return v0

    #@7
    :cond_7
    invoke-direct {p0, p1}, Landroid/media/AudioTrack;->native_set_pos_update_period(I)I

    #@a
    move-result v0

    #@b
    goto :goto_6
.end method

.method protected setState(I)V
    .registers 2
    .parameter "state"

    #@0
    .prologue
    .line 881
    iput p1, p0, Landroid/media/AudioTrack;->mState:I

    #@2
    .line 882
    return-void
.end method

.method public setStereoVolume(FF)I
    .registers 5
    .parameter "leftVolume"
    .parameter "rightVolume"

    #@0
    .prologue
    .line 770
    iget v0, p0, Landroid/media/AudioTrack;->mState:I

    #@2
    const/4 v1, 0x1

    #@3
    if-eq v0, v1, :cond_7

    #@5
    .line 771
    const/4 v0, -0x3

    #@6
    .line 790
    :goto_6
    return v0

    #@7
    .line 775
    :cond_7
    invoke-static {}, Landroid/media/AudioTrack;->getMinVolume()F

    #@a
    move-result v0

    #@b
    cmpg-float v0, p1, v0

    #@d
    if-gez v0, :cond_13

    #@f
    .line 776
    invoke-static {}, Landroid/media/AudioTrack;->getMinVolume()F

    #@12
    move-result p1

    #@13
    .line 778
    :cond_13
    invoke-static {}, Landroid/media/AudioTrack;->getMaxVolume()F

    #@16
    move-result v0

    #@17
    cmpl-float v0, p1, v0

    #@19
    if-lez v0, :cond_1f

    #@1b
    .line 779
    invoke-static {}, Landroid/media/AudioTrack;->getMaxVolume()F

    #@1e
    move-result p1

    #@1f
    .line 781
    :cond_1f
    invoke-static {}, Landroid/media/AudioTrack;->getMinVolume()F

    #@22
    move-result v0

    #@23
    cmpg-float v0, p2, v0

    #@25
    if-gez v0, :cond_2b

    #@27
    .line 782
    invoke-static {}, Landroid/media/AudioTrack;->getMinVolume()F

    #@2a
    move-result p2

    #@2b
    .line 784
    :cond_2b
    invoke-static {}, Landroid/media/AudioTrack;->getMaxVolume()F

    #@2e
    move-result v0

    #@2f
    cmpl-float v0, p2, v0

    #@31
    if-lez v0, :cond_37

    #@33
    .line 785
    invoke-static {}, Landroid/media/AudioTrack;->getMaxVolume()F

    #@36
    move-result p2

    #@37
    .line 788
    :cond_37
    invoke-direct {p0, p1, p2}, Landroid/media/AudioTrack;->native_setVolume(FF)V

    #@3a
    .line 790
    const/4 v0, 0x0

    #@3b
    goto :goto_6
.end method

.method public stop()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 915
    iget v0, p0, Landroid/media/AudioTrack;->mState:I

    #@3
    if-eq v0, v1, :cond_e

    #@5
    .line 916
    new-instance v0, Ljava/lang/IllegalStateException;

    #@7
    const-string/jumbo v1, "stop() called on uninitialized AudioTrack."

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 920
    :cond_e
    iget-object v1, p0, Landroid/media/AudioTrack;->mPlayStateLock:Ljava/lang/Object;

    #@10
    monitor-enter v1

    #@11
    .line 921
    :try_start_11
    invoke-direct {p0}, Landroid/media/AudioTrack;->native_stop()V

    #@14
    .line 922
    const/4 v0, 0x1

    #@15
    iput v0, p0, Landroid/media/AudioTrack;->mPlayState:I

    #@17
    .line 923
    monitor-exit v1

    #@18
    .line 924
    return-void

    #@19
    .line 923
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_11 .. :try_end_1b} :catchall_19

    #@1b
    throw v0
.end method

.method public write([BII)I
    .registers 7
    .parameter "audioData"
    .parameter "offsetInBytes"
    .parameter "sizeInBytes"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 981
    iget v0, p0, Landroid/media/AudioTrack;->mDataLoadMode:I

    #@3
    if-nez v0, :cond_e

    #@5
    iget v0, p0, Landroid/media/AudioTrack;->mState:I

    #@7
    const/4 v1, 0x2

    #@8
    if-ne v0, v1, :cond_e

    #@a
    if-lez p3, :cond_e

    #@c
    .line 984
    iput v2, p0, Landroid/media/AudioTrack;->mState:I

    #@e
    .line 987
    :cond_e
    iget v0, p0, Landroid/media/AudioTrack;->mState:I

    #@10
    if-eq v0, v2, :cond_14

    #@12
    .line 988
    const/4 v0, -0x3

    #@13
    .line 996
    :goto_13
    return v0

    #@14
    .line 991
    :cond_14
    if-eqz p1, :cond_1f

    #@16
    if-ltz p2, :cond_1f

    #@18
    if-ltz p3, :cond_1f

    #@1a
    add-int v0, p2, p3

    #@1c
    array-length v1, p1

    #@1d
    if-le v0, v1, :cond_21

    #@1f
    .line 993
    :cond_1f
    const/4 v0, -0x2

    #@20
    goto :goto_13

    #@21
    .line 996
    :cond_21
    iget v0, p0, Landroid/media/AudioTrack;->mAudioFormat:I

    #@23
    invoke-direct {p0, p1, p2, p3, v0}, Landroid/media/AudioTrack;->native_write_byte([BIII)I

    #@26
    move-result v0

    #@27
    goto :goto_13
.end method

.method public write([SII)I
    .registers 7
    .parameter "audioData"
    .parameter "offsetInShorts"
    .parameter "sizeInShorts"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 1017
    iget v0, p0, Landroid/media/AudioTrack;->mDataLoadMode:I

    #@3
    if-nez v0, :cond_e

    #@5
    iget v0, p0, Landroid/media/AudioTrack;->mState:I

    #@7
    const/4 v1, 0x2

    #@8
    if-ne v0, v1, :cond_e

    #@a
    if-lez p3, :cond_e

    #@c
    .line 1020
    iput v2, p0, Landroid/media/AudioTrack;->mState:I

    #@e
    .line 1023
    :cond_e
    iget v0, p0, Landroid/media/AudioTrack;->mState:I

    #@10
    if-eq v0, v2, :cond_14

    #@12
    .line 1024
    const/4 v0, -0x3

    #@13
    .line 1032
    :goto_13
    return v0

    #@14
    .line 1027
    :cond_14
    if-eqz p1, :cond_1f

    #@16
    if-ltz p2, :cond_1f

    #@18
    if-ltz p3, :cond_1f

    #@1a
    add-int v0, p2, p3

    #@1c
    array-length v1, p1

    #@1d
    if-le v0, v1, :cond_21

    #@1f
    .line 1029
    :cond_1f
    const/4 v0, -0x2

    #@20
    goto :goto_13

    #@21
    .line 1032
    :cond_21
    iget v0, p0, Landroid/media/AudioTrack;->mAudioFormat:I

    #@23
    invoke-direct {p0, p1, p2, p3, v0}, Landroid/media/AudioTrack;->native_write_short([SIII)I

    #@26
    move-result v0

    #@27
    goto :goto_13
.end method
