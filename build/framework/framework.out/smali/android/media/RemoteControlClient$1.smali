.class Landroid/media/RemoteControlClient$1;
.super Landroid/media/IRemoteControlClient$Stub;
.source "RemoteControlClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/RemoteControlClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/media/RemoteControlClient;


# direct methods
.method constructor <init>(Landroid/media/RemoteControlClient;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 828
    iput-object p1, p0, Landroid/media/RemoteControlClient$1;->this$0:Landroid/media/RemoteControlClient;

    #@2
    invoke-direct {p0}, Landroid/media/IRemoteControlClient$Stub;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onInformationRequested(IIII)V
    .registers 13
    .parameter "clientGeneration"
    .parameter "infoFlags"
    .parameter "artWidth"
    .parameter "artHeight"

    #@0
    .prologue
    const/4 v7, 0x5

    #@1
    const/4 v6, 0x4

    #@2
    const/4 v5, 0x3

    #@3
    const/4 v4, 0x2

    #@4
    const/4 v3, 0x1

    #@5
    .line 833
    iget-object v0, p0, Landroid/media/RemoteControlClient$1;->this$0:Landroid/media/RemoteControlClient;

    #@7
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$1300(Landroid/media/RemoteControlClient;)Landroid/media/RemoteControlClient$EventHandler;

    #@a
    move-result-object v0

    #@b
    if-eqz v0, :cond_9e

    #@d
    .line 835
    iget-object v0, p0, Landroid/media/RemoteControlClient$1;->this$0:Landroid/media/RemoteControlClient;

    #@f
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$1300(Landroid/media/RemoteControlClient;)Landroid/media/RemoteControlClient$EventHandler;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0, v7}, Landroid/media/RemoteControlClient$EventHandler;->removeMessages(I)V

    #@16
    .line 836
    iget-object v0, p0, Landroid/media/RemoteControlClient$1;->this$0:Landroid/media/RemoteControlClient;

    #@18
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$1300(Landroid/media/RemoteControlClient;)Landroid/media/RemoteControlClient$EventHandler;

    #@1b
    move-result-object v0

    #@1c
    iget-object v1, p0, Landroid/media/RemoteControlClient$1;->this$0:Landroid/media/RemoteControlClient;

    #@1e
    invoke-static {v1}, Landroid/media/RemoteControlClient;->access$1300(Landroid/media/RemoteControlClient;)Landroid/media/RemoteControlClient$EventHandler;

    #@21
    move-result-object v1

    #@22
    new-instance v2, Ljava/lang/Integer;

    #@24
    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    #@27
    invoke-virtual {v1, v7, p3, p4, v2}, Landroid/media/RemoteControlClient$EventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient$EventHandler;->dispatchMessage(Landroid/os/Message;)V

    #@2e
    .line 842
    iget-object v0, p0, Landroid/media/RemoteControlClient$1;->this$0:Landroid/media/RemoteControlClient;

    #@30
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$1300(Landroid/media/RemoteControlClient;)Landroid/media/RemoteControlClient$EventHandler;

    #@33
    move-result-object v0

    #@34
    invoke-virtual {v0, v3}, Landroid/media/RemoteControlClient$EventHandler;->removeMessages(I)V

    #@37
    .line 843
    iget-object v0, p0, Landroid/media/RemoteControlClient$1;->this$0:Landroid/media/RemoteControlClient;

    #@39
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$1300(Landroid/media/RemoteControlClient;)Landroid/media/RemoteControlClient$EventHandler;

    #@3c
    move-result-object v0

    #@3d
    invoke-virtual {v0, v4}, Landroid/media/RemoteControlClient$EventHandler;->removeMessages(I)V

    #@40
    .line 844
    iget-object v0, p0, Landroid/media/RemoteControlClient$1;->this$0:Landroid/media/RemoteControlClient;

    #@42
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$1300(Landroid/media/RemoteControlClient;)Landroid/media/RemoteControlClient$EventHandler;

    #@45
    move-result-object v0

    #@46
    invoke-virtual {v0, v5}, Landroid/media/RemoteControlClient$EventHandler;->removeMessages(I)V

    #@49
    .line 845
    iget-object v0, p0, Landroid/media/RemoteControlClient$1;->this$0:Landroid/media/RemoteControlClient;

    #@4b
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$1300(Landroid/media/RemoteControlClient;)Landroid/media/RemoteControlClient$EventHandler;

    #@4e
    move-result-object v0

    #@4f
    invoke-virtual {v0, v6}, Landroid/media/RemoteControlClient$EventHandler;->removeMessages(I)V

    #@52
    .line 846
    iget-object v0, p0, Landroid/media/RemoteControlClient$1;->this$0:Landroid/media/RemoteControlClient;

    #@54
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$1300(Landroid/media/RemoteControlClient;)Landroid/media/RemoteControlClient$EventHandler;

    #@57
    move-result-object v0

    #@58
    iget-object v1, p0, Landroid/media/RemoteControlClient$1;->this$0:Landroid/media/RemoteControlClient;

    #@5a
    invoke-static {v1}, Landroid/media/RemoteControlClient;->access$1300(Landroid/media/RemoteControlClient;)Landroid/media/RemoteControlClient$EventHandler;

    #@5d
    move-result-object v1

    #@5e
    invoke-virtual {v1, v3}, Landroid/media/RemoteControlClient$EventHandler;->obtainMessage(I)Landroid/os/Message;

    #@61
    move-result-object v1

    #@62
    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient$EventHandler;->dispatchMessage(Landroid/os/Message;)V

    #@65
    .line 848
    iget-object v0, p0, Landroid/media/RemoteControlClient$1;->this$0:Landroid/media/RemoteControlClient;

    #@67
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$1300(Landroid/media/RemoteControlClient;)Landroid/media/RemoteControlClient$EventHandler;

    #@6a
    move-result-object v0

    #@6b
    iget-object v1, p0, Landroid/media/RemoteControlClient$1;->this$0:Landroid/media/RemoteControlClient;

    #@6d
    invoke-static {v1}, Landroid/media/RemoteControlClient;->access$1300(Landroid/media/RemoteControlClient;)Landroid/media/RemoteControlClient$EventHandler;

    #@70
    move-result-object v1

    #@71
    invoke-virtual {v1, v5}, Landroid/media/RemoteControlClient$EventHandler;->obtainMessage(I)Landroid/os/Message;

    #@74
    move-result-object v1

    #@75
    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient$EventHandler;->dispatchMessage(Landroid/os/Message;)V

    #@78
    .line 850
    iget-object v0, p0, Landroid/media/RemoteControlClient$1;->this$0:Landroid/media/RemoteControlClient;

    #@7a
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$1300(Landroid/media/RemoteControlClient;)Landroid/media/RemoteControlClient$EventHandler;

    #@7d
    move-result-object v0

    #@7e
    iget-object v1, p0, Landroid/media/RemoteControlClient$1;->this$0:Landroid/media/RemoteControlClient;

    #@80
    invoke-static {v1}, Landroid/media/RemoteControlClient;->access$1300(Landroid/media/RemoteControlClient;)Landroid/media/RemoteControlClient$EventHandler;

    #@83
    move-result-object v1

    #@84
    invoke-virtual {v1, v4}, Landroid/media/RemoteControlClient$EventHandler;->obtainMessage(I)Landroid/os/Message;

    #@87
    move-result-object v1

    #@88
    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient$EventHandler;->dispatchMessage(Landroid/os/Message;)V

    #@8b
    .line 851
    iget-object v0, p0, Landroid/media/RemoteControlClient$1;->this$0:Landroid/media/RemoteControlClient;

    #@8d
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$1300(Landroid/media/RemoteControlClient;)Landroid/media/RemoteControlClient$EventHandler;

    #@90
    move-result-object v0

    #@91
    iget-object v1, p0, Landroid/media/RemoteControlClient$1;->this$0:Landroid/media/RemoteControlClient;

    #@93
    invoke-static {v1}, Landroid/media/RemoteControlClient;->access$1300(Landroid/media/RemoteControlClient;)Landroid/media/RemoteControlClient$EventHandler;

    #@96
    move-result-object v1

    #@97
    invoke-virtual {v1, v6}, Landroid/media/RemoteControlClient$EventHandler;->obtainMessage(I)Landroid/os/Message;

    #@9a
    move-result-object v1

    #@9b
    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient$EventHandler;->dispatchMessage(Landroid/os/Message;)V

    #@9e
    .line 853
    :cond_9e
    return-void
.end method

.method public plugRemoteControlDisplay(Landroid/media/IRemoteControlDisplay;)V
    .registers 5
    .parameter "rcd"

    #@0
    .prologue
    .line 866
    iget-object v0, p0, Landroid/media/RemoteControlClient$1;->this$0:Landroid/media/RemoteControlClient;

    #@2
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$1300(Landroid/media/RemoteControlClient;)Landroid/media/RemoteControlClient$EventHandler;

    #@5
    move-result-object v0

    #@6
    if-eqz v0, :cond_1c

    #@8
    .line 867
    iget-object v0, p0, Landroid/media/RemoteControlClient$1;->this$0:Landroid/media/RemoteControlClient;

    #@a
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$1300(Landroid/media/RemoteControlClient;)Landroid/media/RemoteControlClient$EventHandler;

    #@d
    move-result-object v0

    #@e
    iget-object v1, p0, Landroid/media/RemoteControlClient$1;->this$0:Landroid/media/RemoteControlClient;

    #@10
    invoke-static {v1}, Landroid/media/RemoteControlClient;->access$1300(Landroid/media/RemoteControlClient;)Landroid/media/RemoteControlClient$EventHandler;

    #@13
    move-result-object v1

    #@14
    const/4 v2, 0x7

    #@15
    invoke-virtual {v1, v2, p1}, Landroid/media/RemoteControlClient$EventHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient$EventHandler;->dispatchMessage(Landroid/os/Message;)V

    #@1c
    .line 870
    :cond_1c
    return-void
.end method

.method public setCurrentClientGenerationId(I)V
    .registers 6
    .parameter "clientGeneration"

    #@0
    .prologue
    const/4 v3, 0x6

    #@1
    .line 857
    iget-object v0, p0, Landroid/media/RemoteControlClient$1;->this$0:Landroid/media/RemoteControlClient;

    #@3
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$1300(Landroid/media/RemoteControlClient;)Landroid/media/RemoteControlClient$EventHandler;

    #@6
    move-result-object v0

    #@7
    if-eqz v0, :cond_26

    #@9
    .line 858
    iget-object v0, p0, Landroid/media/RemoteControlClient$1;->this$0:Landroid/media/RemoteControlClient;

    #@b
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$1300(Landroid/media/RemoteControlClient;)Landroid/media/RemoteControlClient$EventHandler;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0, v3}, Landroid/media/RemoteControlClient$EventHandler;->removeMessages(I)V

    #@12
    .line 859
    iget-object v0, p0, Landroid/media/RemoteControlClient$1;->this$0:Landroid/media/RemoteControlClient;

    #@14
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$1300(Landroid/media/RemoteControlClient;)Landroid/media/RemoteControlClient$EventHandler;

    #@17
    move-result-object v0

    #@18
    iget-object v1, p0, Landroid/media/RemoteControlClient$1;->this$0:Landroid/media/RemoteControlClient;

    #@1a
    invoke-static {v1}, Landroid/media/RemoteControlClient;->access$1300(Landroid/media/RemoteControlClient;)Landroid/media/RemoteControlClient$EventHandler;

    #@1d
    move-result-object v1

    #@1e
    const/4 v2, 0x0

    #@1f
    invoke-virtual {v1, v3, p1, v2}, Landroid/media/RemoteControlClient$EventHandler;->obtainMessage(III)Landroid/os/Message;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient$EventHandler;->dispatchMessage(Landroid/os/Message;)V

    #@26
    .line 862
    :cond_26
    return-void
.end method

.method public unplugRemoteControlDisplay(Landroid/media/IRemoteControlDisplay;)V
    .registers 5
    .parameter "rcd"

    #@0
    .prologue
    .line 874
    iget-object v0, p0, Landroid/media/RemoteControlClient$1;->this$0:Landroid/media/RemoteControlClient;

    #@2
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$1300(Landroid/media/RemoteControlClient;)Landroid/media/RemoteControlClient$EventHandler;

    #@5
    move-result-object v0

    #@6
    if-eqz v0, :cond_1d

    #@8
    .line 875
    iget-object v0, p0, Landroid/media/RemoteControlClient$1;->this$0:Landroid/media/RemoteControlClient;

    #@a
    invoke-static {v0}, Landroid/media/RemoteControlClient;->access$1300(Landroid/media/RemoteControlClient;)Landroid/media/RemoteControlClient$EventHandler;

    #@d
    move-result-object v0

    #@e
    iget-object v1, p0, Landroid/media/RemoteControlClient$1;->this$0:Landroid/media/RemoteControlClient;

    #@10
    invoke-static {v1}, Landroid/media/RemoteControlClient;->access$1300(Landroid/media/RemoteControlClient;)Landroid/media/RemoteControlClient$EventHandler;

    #@13
    move-result-object v1

    #@14
    const/16 v2, 0x8

    #@16
    invoke-virtual {v1, v2, p1}, Landroid/media/RemoteControlClient$EventHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient$EventHandler;->dispatchMessage(Landroid/os/Message;)V

    #@1d
    .line 878
    :cond_1d
    return-void
.end method
