.class public Landroid/media/MediaSyncEvent;
.super Ljava/lang/Object;
.source "MediaSyncEvent.java"


# static fields
.field public static final SYNC_EVENT_NONE:I = 0x0

.field public static final SYNC_EVENT_PRESENTATION_COMPLETE:I = 0x1


# instance fields
.field private mAudioSession:I

.field private final mType:I


# direct methods
.method private constructor <init>(I)V
    .registers 3
    .parameter "eventType"

    #@0
    .prologue
    .line 68
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 66
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/media/MediaSyncEvent;->mAudioSession:I

    #@6
    .line 69
    iput p1, p0, Landroid/media/MediaSyncEvent;->mType:I

    #@8
    .line 70
    return-void
.end method

.method public static createEvent(I)Landroid/media/MediaSyncEvent;
    .registers 4
    .parameter "eventType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 57
    invoke-static {p0}, Landroid/media/MediaSyncEvent;->isValidType(I)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_1f

    #@6
    .line 58
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, "is not a valid MediaSyncEvent type."

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v0

    #@1f
    .line 61
    :cond_1f
    new-instance v0, Landroid/media/MediaSyncEvent;

    #@21
    invoke-direct {v0, p0}, Landroid/media/MediaSyncEvent;-><init>(I)V

    #@24
    return-object v0
.end method

.method private static isValidType(I)Z
    .registers 2
    .parameter "type"

    #@0
    .prologue
    .line 114
    packed-switch p0, :pswitch_data_8

    #@3
    .line 119
    const/4 v0, 0x0

    #@4
    :goto_4
    return v0

    #@5
    .line 117
    :pswitch_5
    const/4 v0, 0x1

    #@6
    goto :goto_4

    #@7
    .line 114
    nop

    #@8
    :pswitch_data_8
    .packed-switch 0x0
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public getAudioSessionId()I
    .registers 2

    #@0
    .prologue
    .line 110
    iget v0, p0, Landroid/media/MediaSyncEvent;->mAudioSession:I

    #@2
    return v0
.end method

.method public getType()I
    .registers 2

    #@0
    .prologue
    .line 100
    iget v0, p0, Landroid/media/MediaSyncEvent;->mType:I

    #@2
    return v0
.end method

.method public setAudioSessionId(I)Landroid/media/MediaSyncEvent;
    .registers 5
    .parameter "audioSessionId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 86
    if-lez p1, :cond_5

    #@2
    .line 87
    iput p1, p0, Landroid/media/MediaSyncEvent;->mAudioSession:I

    #@4
    .line 91
    return-object p0

    #@5
    .line 89
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    const-string v2, " is not a valid session ID."

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v0
.end method
