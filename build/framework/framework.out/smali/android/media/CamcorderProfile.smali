.class public Landroid/media/CamcorderProfile;
.super Ljava/lang/Object;
.source "CamcorderProfile.java"


# static fields
.field public static final QUALITY_1080P:I = 0x6

.field public static final QUALITY_480P:I = 0x4

.field public static final QUALITY_4kDCI:I = 0xd

.field public static final QUALITY_4kUHD:I = 0xc

.field public static final QUALITY_720P:I = 0x5

.field public static final QUALITY_CIF:I = 0x3

.field public static final QUALITY_FWVGA:I = 0x8

.field public static final QUALITY_HIGH:I = 0x1

.field private static final QUALITY_LIST_END:I = 0xd

.field private static final QUALITY_LIST_START:I = 0x0

.field public static final QUALITY_LOW:I = 0x0

.field public static final QUALITY_QCIF:I = 0x2

.field public static final QUALITY_QVGA:I = 0x7

.field public static final QUALITY_TIME_LAPSE_1080P:I = 0x3ee

.field public static final QUALITY_TIME_LAPSE_480P:I = 0x3ec

.field public static final QUALITY_TIME_LAPSE_720P:I = 0x3ed

.field public static final QUALITY_TIME_LAPSE_CIF:I = 0x3eb

.field public static final QUALITY_TIME_LAPSE_FWVGA:I = 0x3f0

.field public static final QUALITY_TIME_LAPSE_HIGH:I = 0x3e9

.field private static final QUALITY_TIME_LAPSE_LIST_END:I = 0x3f3

.field private static final QUALITY_TIME_LAPSE_LIST_START:I = 0x3e8

.field public static final QUALITY_TIME_LAPSE_LOW:I = 0x3e8

.field public static final QUALITY_TIME_LAPSE_QCIF:I = 0x3ea

.field public static final QUALITY_TIME_LAPSE_QVGA:I = 0x3ef

.field public static final QUALITY_TIME_LAPSE_VGA:I = 0x3f2

.field public static final QUALITY_TIME_LAPSE_WQVGA:I = 0x3f3

.field public static final QUALITY_TIME_LAPSE_WVGA:I = 0x3f1

.field public static final QUALITY_VGA:I = 0xa

.field public static final QUALITY_WQVGA:I = 0xb

.field public static final QUALITY_WVGA:I = 0x9


# instance fields
.field public HFRbitrate:I

.field public audioBitRate:I

.field public audioChannels:I

.field public audioCodec:I

.field public audioSampleRate:I

.field public duration:I

.field public fileFormat:I

.field public quality:I

.field public videoBitRate:I

.field public videoCodec:I

.field public videoFrameHeight:I

.field public videoFrameRate:I

.field public videoFrameWidth:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 360
    const-string/jumbo v0, "media_jni"

    #@3
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@6
    .line 361
    invoke-static {}, Landroid/media/CamcorderProfile;->native_init()V

    #@9
    .line 362
    return-void
.end method

.method private constructor <init>(IIIIIIIIIIIII)V
    .registers 18
    .parameter "duration"
    .parameter "quality"
    .parameter "fileFormat"
    .parameter "videoCodec"
    .parameter "videoBitRate"
    .parameter "videoFrameRate"
    .parameter "videoWidth"
    .parameter "videoHeight"
    .parameter "audioCodec"
    .parameter "audioBitRate"
    .parameter "audioSampleRate"
    .parameter "audioChannels"
    .parameter "HFRbitrate"

    #@0
    .prologue
    .line 377
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 379
    iput p1, p0, Landroid/media/CamcorderProfile;->duration:I

    #@5
    .line 380
    iput p2, p0, Landroid/media/CamcorderProfile;->quality:I

    #@7
    .line 381
    iput p3, p0, Landroid/media/CamcorderProfile;->fileFormat:I

    #@9
    .line 382
    iput p4, p0, Landroid/media/CamcorderProfile;->videoCodec:I

    #@b
    .line 383
    iput p5, p0, Landroid/media/CamcorderProfile;->videoBitRate:I

    #@d
    .line 384
    iput p6, p0, Landroid/media/CamcorderProfile;->videoFrameRate:I

    #@f
    .line 385
    iput p7, p0, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    #@11
    .line 386
    iput p8, p0, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    #@13
    .line 387
    iput p9, p0, Landroid/media/CamcorderProfile;->audioCodec:I

    #@15
    .line 388
    iput p10, p0, Landroid/media/CamcorderProfile;->audioBitRate:I

    #@17
    .line 389
    iput p11, p0, Landroid/media/CamcorderProfile;->audioSampleRate:I

    #@19
    .line 390
    move/from16 v0, p12

    #@1b
    iput v0, p0, Landroid/media/CamcorderProfile;->audioChannels:I

    #@1d
    .line 391
    move/from16 v0, p13

    #@1f
    iput v0, p0, Landroid/media/CamcorderProfile;->HFRbitrate:I

    #@21
    .line 392
    const-string v1, "CamcorderProfile"

    #@23
    new-instance v2, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v3, "HFRbitrate="

    #@2a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    move/from16 v0, p13

    #@30
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    const-string/jumbo v3, "this.HFRbitrate="

    #@37
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v2

    #@3b
    iget v3, p0, Landroid/media/CamcorderProfile;->HFRbitrate:I

    #@3d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v2

    #@41
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v2

    #@45
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 393
    return-void
.end method

.method public static get(I)Landroid/media/CamcorderProfile;
    .registers 5
    .parameter "quality"

    #@0
    .prologue
    .line 271
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    #@3
    move-result v2

    #@4
    .line 272
    .local v2, numberOfCameras:I
    new-instance v0, Landroid/hardware/Camera$CameraInfo;

    #@6
    invoke-direct {v0}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    #@9
    .line 273
    .local v0, cameraInfo:Landroid/hardware/Camera$CameraInfo;
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    :goto_a
    if-ge v1, v2, :cond_1b

    #@c
    .line 274
    invoke-static {v1, v0}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    #@f
    .line 275
    iget v3, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    #@11
    if-nez v3, :cond_18

    #@13
    .line 276
    invoke-static {v1, p0}, Landroid/media/CamcorderProfile;->get(II)Landroid/media/CamcorderProfile;

    #@16
    move-result-object v3

    #@17
    .line 279
    :goto_17
    return-object v3

    #@18
    .line 273
    :cond_18
    add-int/lit8 v1, v1, 0x1

    #@1a
    goto :goto_a

    #@1b
    .line 279
    :cond_1b
    const/4 v3, 0x0

    #@1c
    goto :goto_17
.end method

.method public static get(II)Landroid/media/CamcorderProfile;
    .registers 5
    .parameter "cameraId"
    .parameter "quality"

    #@0
    .prologue
    .line 322
    if-ltz p1, :cond_6

    #@2
    const/16 v1, 0xd

    #@4
    if-le p1, v1, :cond_27

    #@6
    :cond_6
    const/16 v1, 0x3e8

    #@8
    if-lt p1, v1, :cond_e

    #@a
    const/16 v1, 0x3f3

    #@c
    if-le p1, v1, :cond_27

    #@e
    .line 326
    :cond_e
    new-instance v1, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v2, "Unsupported quality level: "

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    .line 327
    .local v0, errMessage:Ljava/lang/String;
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@23
    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@26
    throw v1

    #@27
    .line 329
    .end local v0           #errMessage:Ljava/lang/String;
    :cond_27
    invoke-static {p0, p1}, Landroid/media/CamcorderProfile;->native_get_camcorder_profile(II)Landroid/media/CamcorderProfile;

    #@2a
    move-result-object v1

    #@2b
    return-object v1
.end method

.method public static hasProfile(I)Z
    .registers 5
    .parameter "quality"

    #@0
    .prologue
    .line 338
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    #@3
    move-result v2

    #@4
    .line 339
    .local v2, numberOfCameras:I
    new-instance v0, Landroid/hardware/Camera$CameraInfo;

    #@6
    invoke-direct {v0}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    #@9
    .line 340
    .local v0, cameraInfo:Landroid/hardware/Camera$CameraInfo;
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    :goto_a
    if-ge v1, v2, :cond_1b

    #@c
    .line 341
    invoke-static {v1, v0}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    #@f
    .line 342
    iget v3, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    #@11
    if-nez v3, :cond_18

    #@13
    .line 343
    invoke-static {v1, p0}, Landroid/media/CamcorderProfile;->hasProfile(II)Z

    #@16
    move-result v3

    #@17
    .line 346
    :goto_17
    return v3

    #@18
    .line 340
    :cond_18
    add-int/lit8 v1, v1, 0x1

    #@1a
    goto :goto_a

    #@1b
    .line 346
    :cond_1b
    const/4 v3, 0x0

    #@1c
    goto :goto_17
.end method

.method public static hasProfile(II)Z
    .registers 3
    .parameter "cameraId"
    .parameter "quality"

    #@0
    .prologue
    .line 356
    invoke-static {p0, p1}, Landroid/media/CamcorderProfile;->native_has_camcorder_profile(II)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private static final native native_get_camcorder_profile(II)Landroid/media/CamcorderProfile;
.end method

.method private static final native native_has_camcorder_profile(II)Z
.end method

.method private static final native native_init()V
.end method
