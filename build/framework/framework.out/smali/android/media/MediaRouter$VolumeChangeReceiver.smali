.class Landroid/media/MediaRouter$VolumeChangeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MediaRouter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/MediaRouter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "VolumeChangeReceiver"
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 2073
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 9
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 2076
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@4
    move-result-object v3

    #@5
    const-string v4, "android.media.VOLUME_CHANGED_ACTION"

    #@7
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v3

    #@b
    if-eqz v3, :cond_17

    #@d
    .line 2077
    const-string v3, "android.media.EXTRA_VOLUME_STREAM_TYPE"

    #@f
    const/4 v4, -0x1

    #@10
    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@13
    move-result v2

    #@14
    .line 2079
    .local v2, streamType:I
    const/4 v3, 0x3

    #@15
    if-eq v2, v3, :cond_18

    #@17
    .line 2090
    .end local v2           #streamType:I
    :cond_17
    :goto_17
    return-void

    #@18
    .line 2083
    .restart local v2       #streamType:I
    :cond_18
    const-string v3, "android.media.EXTRA_VOLUME_STREAM_VALUE"

    #@1a
    invoke-virtual {p2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@1d
    move-result v0

    #@1e
    .line 2084
    .local v0, newVolume:I
    const-string v3, "android.media.EXTRA_PREV_VOLUME_STREAM_VALUE"

    #@20
    invoke-virtual {p2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@23
    move-result v1

    #@24
    .line 2086
    .local v1, oldVolume:I
    if-eq v0, v1, :cond_17

    #@26
    .line 2087
    invoke-static {v0}, Landroid/media/MediaRouter;->systemVolumeChanged(I)V

    #@29
    goto :goto_17
.end method
