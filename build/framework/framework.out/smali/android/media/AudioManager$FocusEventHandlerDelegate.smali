.class Landroid/media/AudioManager$FocusEventHandlerDelegate;
.super Ljava/lang/Object;
.source "AudioManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/AudioManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FocusEventHandlerDelegate"
.end annotation


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field final synthetic this$0:Landroid/media/AudioManager;


# direct methods
.method constructor <init>(Landroid/media/AudioManager;)V
    .registers 4
    .parameter

    #@0
    .prologue
    .line 2128
    iput-object p1, p0, Landroid/media/AudioManager$FocusEventHandlerDelegate;->this$0:Landroid/media/AudioManager;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 2130
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@8
    move-result-object v0

    #@9
    .local v0, looper:Landroid/os/Looper;
    if-nez v0, :cond_f

    #@b
    .line 2131
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    #@e
    move-result-object v0

    #@f
    .line 2134
    :cond_f
    if-eqz v0, :cond_19

    #@11
    .line 2136
    new-instance v1, Landroid/media/AudioManager$FocusEventHandlerDelegate$1;

    #@13
    invoke-direct {v1, p0, v0, p1}, Landroid/media/AudioManager$FocusEventHandlerDelegate$1;-><init>(Landroid/media/AudioManager$FocusEventHandlerDelegate;Landroid/os/Looper;Landroid/media/AudioManager;)V

    #@16
    iput-object v1, p0, Landroid/media/AudioManager$FocusEventHandlerDelegate;->mHandler:Landroid/os/Handler;

    #@18
    .line 2151
    :goto_18
    return-void

    #@19
    .line 2149
    :cond_19
    const/4 v1, 0x0

    #@1a
    iput-object v1, p0, Landroid/media/AudioManager$FocusEventHandlerDelegate;->mHandler:Landroid/os/Handler;

    #@1c
    goto :goto_18
.end method


# virtual methods
.method getHandler()Landroid/os/Handler;
    .registers 2

    #@0
    .prologue
    .line 2154
    iget-object v0, p0, Landroid/media/AudioManager$FocusEventHandlerDelegate;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method
