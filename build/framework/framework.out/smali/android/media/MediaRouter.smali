.class public Landroid/media/MediaRouter;
.super Ljava/lang/Object;
.source "MediaRouter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/MediaRouter$WifiDisplayStatusChangedReceiver;,
        Landroid/media/MediaRouter$VolumeChangeReceiver;,
        Landroid/media/MediaRouter$VolumeCallback;,
        Landroid/media/MediaRouter$VolumeCallbackInfo;,
        Landroid/media/MediaRouter$SimpleCallback;,
        Landroid/media/MediaRouter$Callback;,
        Landroid/media/MediaRouter$CallbackInfo;,
        Landroid/media/MediaRouter$RouteCategory;,
        Landroid/media/MediaRouter$RouteGroup;,
        Landroid/media/MediaRouter$UserRouteInfo;,
        Landroid/media/MediaRouter$RouteInfo;,
        Landroid/media/MediaRouter$Static;
    }
.end annotation


# static fields
.field public static final ROUTE_TYPE_LIVE_AUDIO:I = 0x1

.field public static final ROUTE_TYPE_LIVE_VIDEO:I = 0x2

.field public static final ROUTE_TYPE_USER:I = 0x800000

.field private static final TAG:Ljava/lang/String; = "MediaRouter"

.field static final sRouters:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/Context;",
            "Landroid/media/MediaRouter;",
            ">;"
        }
    .end annotation
.end field

.field static sStatic:Landroid/media/MediaRouter$Static;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 274
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    sput-object v0, Landroid/media/MediaRouter;->sRouters:Ljava/util/HashMap;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 291
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 292
    const-class v2, Landroid/media/MediaRouter$Static;

    #@5
    monitor-enter v2

    #@6
    .line 293
    :try_start_6
    sget-object v1, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@8
    if-nez v1, :cond_1a

    #@a
    .line 294
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@d
    move-result-object v0

    #@e
    .line 295
    .local v0, appContext:Landroid/content/Context;
    new-instance v1, Landroid/media/MediaRouter$Static;

    #@10
    invoke-direct {v1, v0}, Landroid/media/MediaRouter$Static;-><init>(Landroid/content/Context;)V

    #@13
    sput-object v1, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@15
    .line 296
    sget-object v1, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@17
    invoke-virtual {v1, v0}, Landroid/media/MediaRouter$Static;->startMonitoringRoutes(Landroid/content/Context;)V

    #@1a
    .line 298
    .end local v0           #appContext:Landroid/content/Context;
    :cond_1a
    monitor-exit v2

    #@1b
    .line 299
    return-void

    #@1c
    .line 298
    :catchall_1c
    move-exception v1

    #@1d
    monitor-exit v2
    :try_end_1e
    .catchall {:try_start_6 .. :try_end_1e} :catchall_1c

    #@1e
    throw v1
.end method

.method static synthetic access$000(Landroid/media/MediaRouter$RouteInfo;[Landroid/view/Display;)Landroid/view/Display;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    invoke-static {p0, p1}, Landroid/media/MediaRouter;->choosePresentationDisplayForRoute(Landroid/media/MediaRouter$RouteInfo;[Landroid/view/Display;)Landroid/view/Display;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static addRouteStatic(Landroid/media/MediaRouter$RouteInfo;)V
    .registers 4
    .parameter "info"

    #@0
    .prologue
    .line 472
    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteInfo;->getCategory()Landroid/media/MediaRouter$RouteCategory;

    #@3
    move-result-object v0

    #@4
    .line 473
    .local v0, cat:Landroid/media/MediaRouter$RouteCategory;
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@6
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mCategories:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@b
    move-result v2

    #@c
    if-nez v2, :cond_15

    #@e
    .line 474
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@10
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mCategories:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@15
    .line 476
    :cond_15
    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteCategory;->isGroupable()Z

    #@18
    move-result v2

    #@19
    if-eqz v2, :cond_3b

    #@1b
    instance-of v2, p0, Landroid/media/MediaRouter$RouteGroup;

    #@1d
    if-nez v2, :cond_3b

    #@1f
    .line 478
    new-instance v1, Landroid/media/MediaRouter$RouteGroup;

    #@21
    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteInfo;->getCategory()Landroid/media/MediaRouter$RouteCategory;

    #@24
    move-result-object v2

    #@25
    invoke-direct {v1, v2}, Landroid/media/MediaRouter$RouteGroup;-><init>(Landroid/media/MediaRouter$RouteCategory;)V

    #@28
    .line 479
    .local v1, group:Landroid/media/MediaRouter$RouteGroup;
    iget v2, p0, Landroid/media/MediaRouter$RouteInfo;->mSupportedTypes:I

    #@2a
    iput v2, v1, Landroid/media/MediaRouter$RouteInfo;->mSupportedTypes:I

    #@2c
    .line 480
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2e
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mRoutes:Ljava/util/ArrayList;

    #@30
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@33
    .line 481
    invoke-static {v1}, Landroid/media/MediaRouter;->dispatchRouteAdded(Landroid/media/MediaRouter$RouteInfo;)V

    #@36
    .line 482
    invoke-virtual {v1, p0}, Landroid/media/MediaRouter$RouteGroup;->addRoute(Landroid/media/MediaRouter$RouteInfo;)V

    #@39
    .line 484
    move-object p0, v1

    #@3a
    .line 489
    .end local v1           #group:Landroid/media/MediaRouter$RouteGroup;
    :goto_3a
    return-void

    #@3b
    .line 486
    :cond_3b
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@3d
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mRoutes:Ljava/util/ArrayList;

    #@3f
    invoke-virtual {v2, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@42
    .line 487
    invoke-static {p0}, Landroid/media/MediaRouter;->dispatchRouteAdded(Landroid/media/MediaRouter$RouteInfo;)V

    #@45
    goto :goto_3a
.end method

.method private static choosePresentationDisplayForRoute(Landroid/media/MediaRouter$RouteInfo;[Landroid/view/Display;)Landroid/view/Display;
    .registers 9
    .parameter "route"
    .parameter "displays"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 896
    iget v5, p0, Landroid/media/MediaRouter$RouteInfo;->mSupportedTypes:I

    #@3
    and-int/lit8 v5, v5, 0x2

    #@5
    if-eqz v5, :cond_38

    #@7
    .line 897
    iget-object v5, p0, Landroid/media/MediaRouter$RouteInfo;->mDeviceAddress:Ljava/lang/String;

    #@9
    if-eqz v5, :cond_2b

    #@b
    .line 899
    move-object v0, p1

    #@c
    .local v0, arr$:[Landroid/view/Display;
    array-length v3, v0

    #@d
    .local v3, len$:I
    const/4 v2, 0x0

    #@e
    .local v2, i$:I
    :goto_e
    if-ge v2, v3, :cond_29

    #@10
    aget-object v1, v0, v2

    #@12
    .line 900
    .local v1, display:Landroid/view/Display;
    invoke-virtual {v1}, Landroid/view/Display;->getType()I

    #@15
    move-result v5

    #@16
    const/4 v6, 0x3

    #@17
    if-ne v5, v6, :cond_26

    #@19
    iget-object v5, p0, Landroid/media/MediaRouter$RouteInfo;->mDeviceAddress:Ljava/lang/String;

    #@1b
    invoke-virtual {v1}, Landroid/view/Display;->getAddress()Ljava/lang/String;

    #@1e
    move-result-object v6

    #@1f
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v5

    #@23
    if-eqz v5, :cond_26

    #@25
    .line 913
    .end local v0           #arr$:[Landroid/view/Display;
    .end local v1           #display:Landroid/view/Display;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :goto_25
    return-object v1

    #@26
    .line 899
    .restart local v0       #arr$:[Landroid/view/Display;
    .restart local v1       #display:Landroid/view/Display;
    .restart local v2       #i$:I
    .restart local v3       #len$:I
    :cond_26
    add-int/lit8 v2, v2, 0x1

    #@28
    goto :goto_e

    #@29
    .end local v1           #display:Landroid/view/Display;
    :cond_29
    move-object v1, v4

    #@2a
    .line 905
    goto :goto_25

    #@2b
    .line 908
    .end local v0           #arr$:[Landroid/view/Display;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :cond_2b
    sget-object v5, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2d
    iget-object v5, v5, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    #@2f
    if-ne p0, v5, :cond_38

    #@31
    array-length v5, p1

    #@32
    if-lez v5, :cond_38

    #@34
    .line 910
    const/4 v4, 0x0

    #@35
    aget-object v1, p1, v4

    #@37
    goto :goto_25

    #@38
    :cond_38
    move-object v1, v4

    #@39
    .line 913
    goto :goto_25
.end method

.method static dispatchRouteAdded(Landroid/media/MediaRouter$RouteInfo;)V
    .registers 5
    .parameter "info"

    #@0
    .prologue
    .line 689
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@4
    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    #@7
    move-result-object v1

    #@8
    .local v1, i$:Ljava/util/Iterator;
    :cond_8
    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_23

    #@e
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Landroid/media/MediaRouter$CallbackInfo;

    #@14
    .line 690
    .local v0, cbi:Landroid/media/MediaRouter$CallbackInfo;
    iget v2, v0, Landroid/media/MediaRouter$CallbackInfo;->type:I

    #@16
    iget v3, p0, Landroid/media/MediaRouter$RouteInfo;->mSupportedTypes:I

    #@18
    and-int/2addr v2, v3

    #@19
    if-eqz v2, :cond_8

    #@1b
    .line 691
    iget-object v2, v0, Landroid/media/MediaRouter$CallbackInfo;->cb:Landroid/media/MediaRouter$Callback;

    #@1d
    iget-object v3, v0, Landroid/media/MediaRouter$CallbackInfo;->router:Landroid/media/MediaRouter;

    #@1f
    invoke-virtual {v2, v3, p0}, Landroid/media/MediaRouter$Callback;->onRouteAdded(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;)V

    #@22
    goto :goto_8

    #@23
    .line 694
    .end local v0           #cbi:Landroid/media/MediaRouter$CallbackInfo;
    :cond_23
    return-void
.end method

.method static dispatchRouteChanged(Landroid/media/MediaRouter$RouteInfo;)V
    .registers 5
    .parameter "info"

    #@0
    .prologue
    .line 681
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@4
    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    #@7
    move-result-object v1

    #@8
    .local v1, i$:Ljava/util/Iterator;
    :cond_8
    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_23

    #@e
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Landroid/media/MediaRouter$CallbackInfo;

    #@14
    .line 682
    .local v0, cbi:Landroid/media/MediaRouter$CallbackInfo;
    iget v2, v0, Landroid/media/MediaRouter$CallbackInfo;->type:I

    #@16
    iget v3, p0, Landroid/media/MediaRouter$RouteInfo;->mSupportedTypes:I

    #@18
    and-int/2addr v2, v3

    #@19
    if-eqz v2, :cond_8

    #@1b
    .line 683
    iget-object v2, v0, Landroid/media/MediaRouter$CallbackInfo;->cb:Landroid/media/MediaRouter$Callback;

    #@1d
    iget-object v3, v0, Landroid/media/MediaRouter$CallbackInfo;->router:Landroid/media/MediaRouter;

    #@1f
    invoke-virtual {v2, v3, p0}, Landroid/media/MediaRouter$Callback;->onRouteChanged(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;)V

    #@22
    goto :goto_8

    #@23
    .line 686
    .end local v0           #cbi:Landroid/media/MediaRouter$CallbackInfo;
    :cond_23
    return-void
.end method

.method static dispatchRouteGrouped(Landroid/media/MediaRouter$RouteInfo;Landroid/media/MediaRouter$RouteGroup;I)V
    .registers 7
    .parameter "info"
    .parameter "group"
    .parameter "index"

    #@0
    .prologue
    .line 705
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@4
    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    #@7
    move-result-object v1

    #@8
    .local v1, i$:Ljava/util/Iterator;
    :cond_8
    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_23

    #@e
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Landroid/media/MediaRouter$CallbackInfo;

    #@14
    .line 706
    .local v0, cbi:Landroid/media/MediaRouter$CallbackInfo;
    iget v2, v0, Landroid/media/MediaRouter$CallbackInfo;->type:I

    #@16
    iget v3, p1, Landroid/media/MediaRouter$RouteInfo;->mSupportedTypes:I

    #@18
    and-int/2addr v2, v3

    #@19
    if-eqz v2, :cond_8

    #@1b
    .line 707
    iget-object v2, v0, Landroid/media/MediaRouter$CallbackInfo;->cb:Landroid/media/MediaRouter$Callback;

    #@1d
    iget-object v3, v0, Landroid/media/MediaRouter$CallbackInfo;->router:Landroid/media/MediaRouter;

    #@1f
    invoke-virtual {v2, v3, p0, p1, p2}, Landroid/media/MediaRouter$Callback;->onRouteGrouped(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;Landroid/media/MediaRouter$RouteGroup;I)V

    #@22
    goto :goto_8

    #@23
    .line 710
    .end local v0           #cbi:Landroid/media/MediaRouter$CallbackInfo;
    :cond_23
    return-void
.end method

.method static dispatchRoutePresentationDisplayChanged(Landroid/media/MediaRouter$RouteInfo;)V
    .registers 5
    .parameter "info"

    #@0
    .prologue
    .line 729
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@4
    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    #@7
    move-result-object v1

    #@8
    .local v1, i$:Ljava/util/Iterator;
    :cond_8
    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_23

    #@e
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Landroid/media/MediaRouter$CallbackInfo;

    #@14
    .line 730
    .local v0, cbi:Landroid/media/MediaRouter$CallbackInfo;
    iget v2, v0, Landroid/media/MediaRouter$CallbackInfo;->type:I

    #@16
    iget v3, p0, Landroid/media/MediaRouter$RouteInfo;->mSupportedTypes:I

    #@18
    and-int/2addr v2, v3

    #@19
    if-eqz v2, :cond_8

    #@1b
    .line 731
    iget-object v2, v0, Landroid/media/MediaRouter$CallbackInfo;->cb:Landroid/media/MediaRouter$Callback;

    #@1d
    iget-object v3, v0, Landroid/media/MediaRouter$CallbackInfo;->router:Landroid/media/MediaRouter;

    #@1f
    invoke-virtual {v2, v3, p0}, Landroid/media/MediaRouter$Callback;->onRoutePresentationDisplayChanged(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;)V

    #@22
    goto :goto_8

    #@23
    .line 734
    .end local v0           #cbi:Landroid/media/MediaRouter$CallbackInfo;
    :cond_23
    return-void
.end method

.method static dispatchRouteRemoved(Landroid/media/MediaRouter$RouteInfo;)V
    .registers 5
    .parameter "info"

    #@0
    .prologue
    .line 697
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@4
    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    #@7
    move-result-object v1

    #@8
    .local v1, i$:Ljava/util/Iterator;
    :cond_8
    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_23

    #@e
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Landroid/media/MediaRouter$CallbackInfo;

    #@14
    .line 698
    .local v0, cbi:Landroid/media/MediaRouter$CallbackInfo;
    iget v2, v0, Landroid/media/MediaRouter$CallbackInfo;->type:I

    #@16
    iget v3, p0, Landroid/media/MediaRouter$RouteInfo;->mSupportedTypes:I

    #@18
    and-int/2addr v2, v3

    #@19
    if-eqz v2, :cond_8

    #@1b
    .line 699
    iget-object v2, v0, Landroid/media/MediaRouter$CallbackInfo;->cb:Landroid/media/MediaRouter$Callback;

    #@1d
    iget-object v3, v0, Landroid/media/MediaRouter$CallbackInfo;->router:Landroid/media/MediaRouter;

    #@1f
    invoke-virtual {v2, v3, p0}, Landroid/media/MediaRouter$Callback;->onRouteRemoved(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;)V

    #@22
    goto :goto_8

    #@23
    .line 702
    .end local v0           #cbi:Landroid/media/MediaRouter$CallbackInfo;
    :cond_23
    return-void
.end method

.method static dispatchRouteSelected(ILandroid/media/MediaRouter$RouteInfo;)V
    .registers 6
    .parameter "type"
    .parameter "info"

    #@0
    .prologue
    .line 665
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@4
    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    #@7
    move-result-object v1

    #@8
    .local v1, i$:Ljava/util/Iterator;
    :cond_8
    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_21

    #@e
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Landroid/media/MediaRouter$CallbackInfo;

    #@14
    .line 666
    .local v0, cbi:Landroid/media/MediaRouter$CallbackInfo;
    iget v2, v0, Landroid/media/MediaRouter$CallbackInfo;->type:I

    #@16
    and-int/2addr v2, p0

    #@17
    if-eqz v2, :cond_8

    #@19
    .line 667
    iget-object v2, v0, Landroid/media/MediaRouter$CallbackInfo;->cb:Landroid/media/MediaRouter$Callback;

    #@1b
    iget-object v3, v0, Landroid/media/MediaRouter$CallbackInfo;->router:Landroid/media/MediaRouter;

    #@1d
    invoke-virtual {v2, v3, p0, p1}, Landroid/media/MediaRouter$Callback;->onRouteSelected(Landroid/media/MediaRouter;ILandroid/media/MediaRouter$RouteInfo;)V

    #@20
    goto :goto_8

    #@21
    .line 670
    .end local v0           #cbi:Landroid/media/MediaRouter$CallbackInfo;
    :cond_21
    return-void
.end method

.method static dispatchRouteUngrouped(Landroid/media/MediaRouter$RouteInfo;Landroid/media/MediaRouter$RouteGroup;)V
    .registers 6
    .parameter "info"
    .parameter "group"

    #@0
    .prologue
    .line 713
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@4
    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    #@7
    move-result-object v1

    #@8
    .local v1, i$:Ljava/util/Iterator;
    :cond_8
    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_23

    #@e
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Landroid/media/MediaRouter$CallbackInfo;

    #@14
    .line 714
    .local v0, cbi:Landroid/media/MediaRouter$CallbackInfo;
    iget v2, v0, Landroid/media/MediaRouter$CallbackInfo;->type:I

    #@16
    iget v3, p1, Landroid/media/MediaRouter$RouteInfo;->mSupportedTypes:I

    #@18
    and-int/2addr v2, v3

    #@19
    if-eqz v2, :cond_8

    #@1b
    .line 715
    iget-object v2, v0, Landroid/media/MediaRouter$CallbackInfo;->cb:Landroid/media/MediaRouter$Callback;

    #@1d
    iget-object v3, v0, Landroid/media/MediaRouter$CallbackInfo;->router:Landroid/media/MediaRouter;

    #@1f
    invoke-virtual {v2, v3, p0, p1}, Landroid/media/MediaRouter$Callback;->onRouteUngrouped(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;Landroid/media/MediaRouter$RouteGroup;)V

    #@22
    goto :goto_8

    #@23
    .line 718
    .end local v0           #cbi:Landroid/media/MediaRouter$CallbackInfo;
    :cond_23
    return-void
.end method

.method static dispatchRouteUnselected(ILandroid/media/MediaRouter$RouteInfo;)V
    .registers 6
    .parameter "type"
    .parameter "info"

    #@0
    .prologue
    .line 673
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@4
    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    #@7
    move-result-object v1

    #@8
    .local v1, i$:Ljava/util/Iterator;
    :cond_8
    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_21

    #@e
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Landroid/media/MediaRouter$CallbackInfo;

    #@14
    .line 674
    .local v0, cbi:Landroid/media/MediaRouter$CallbackInfo;
    iget v2, v0, Landroid/media/MediaRouter$CallbackInfo;->type:I

    #@16
    and-int/2addr v2, p0

    #@17
    if-eqz v2, :cond_8

    #@19
    .line 675
    iget-object v2, v0, Landroid/media/MediaRouter$CallbackInfo;->cb:Landroid/media/MediaRouter$Callback;

    #@1b
    iget-object v3, v0, Landroid/media/MediaRouter$CallbackInfo;->router:Landroid/media/MediaRouter;

    #@1d
    invoke-virtual {v2, v3, p0, p1}, Landroid/media/MediaRouter$Callback;->onRouteUnselected(Landroid/media/MediaRouter;ILandroid/media/MediaRouter$RouteInfo;)V

    #@20
    goto :goto_8

    #@21
    .line 678
    .end local v0           #cbi:Landroid/media/MediaRouter$CallbackInfo;
    :cond_21
    return-void
.end method

.method static dispatchRouteVolumeChanged(Landroid/media/MediaRouter$RouteInfo;)V
    .registers 5
    .parameter "info"

    #@0
    .prologue
    .line 721
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@4
    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    #@7
    move-result-object v1

    #@8
    .local v1, i$:Ljava/util/Iterator;
    :cond_8
    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_23

    #@e
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Landroid/media/MediaRouter$CallbackInfo;

    #@14
    .line 722
    .local v0, cbi:Landroid/media/MediaRouter$CallbackInfo;
    iget v2, v0, Landroid/media/MediaRouter$CallbackInfo;->type:I

    #@16
    iget v3, p0, Landroid/media/MediaRouter$RouteInfo;->mSupportedTypes:I

    #@18
    and-int/2addr v2, v3

    #@19
    if-eqz v2, :cond_8

    #@1b
    .line 723
    iget-object v2, v0, Landroid/media/MediaRouter$CallbackInfo;->cb:Landroid/media/MediaRouter$Callback;

    #@1d
    iget-object v3, v0, Landroid/media/MediaRouter$CallbackInfo;->router:Landroid/media/MediaRouter;

    #@1f
    invoke-virtual {v2, v3, p0}, Landroid/media/MediaRouter$Callback;->onRouteVolumeChanged(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;)V

    #@22
    goto :goto_8

    #@23
    .line 726
    .end local v0           #cbi:Landroid/media/MediaRouter$CallbackInfo;
    :cond_23
    return-void
.end method

.method private static findMatchingDisplay(Landroid/hardware/display/WifiDisplay;[Landroid/hardware/display/WifiDisplay;)Landroid/hardware/display/WifiDisplay;
    .registers 5
    .parameter "d"
    .parameter "displays"

    #@0
    .prologue
    .line 875
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    array-length v2, p1

    #@2
    if-ge v0, v2, :cond_10

    #@4
    .line 876
    aget-object v1, p1, v0

    #@6
    .line 877
    .local v1, other:Landroid/hardware/display/WifiDisplay;
    invoke-virtual {p0, v1}, Landroid/hardware/display/WifiDisplay;->hasSameAddress(Landroid/hardware/display/WifiDisplay;)Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_d

    #@c
    .line 881
    .end local v1           #other:Landroid/hardware/display/WifiDisplay;
    :goto_c
    return-object v1

    #@d
    .line 875
    .restart local v1       #other:Landroid/hardware/display/WifiDisplay;
    :cond_d
    add-int/lit8 v0, v0, 0x1

    #@f
    goto :goto_1

    #@10
    .line 881
    .end local v1           #other:Landroid/hardware/display/WifiDisplay;
    :cond_10
    const/4 v1, 0x0

    #@11
    goto :goto_c
.end method

.method private static findWifiDisplayRoute(Landroid/hardware/display/WifiDisplay;)Landroid/media/MediaRouter$RouteInfo;
    .registers 6
    .parameter "d"

    #@0
    .prologue
    .line 885
    sget-object v3, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2
    iget-object v3, v3, Landroid/media/MediaRouter$Static;->mRoutes:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v0

    #@8
    .line 886
    .local v0, count:I
    const/4 v1, 0x0

    #@9
    .local v1, i:I
    :goto_9
    if-ge v1, v0, :cond_25

    #@b
    .line 887
    sget-object v3, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@d
    iget-object v3, v3, Landroid/media/MediaRouter$Static;->mRoutes:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v2

    #@13
    check-cast v2, Landroid/media/MediaRouter$RouteInfo;

    #@15
    .line 888
    .local v2, info:Landroid/media/MediaRouter$RouteInfo;
    invoke-virtual {p0}, Landroid/hardware/display/WifiDisplay;->getDeviceAddress()Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    iget-object v4, v2, Landroid/media/MediaRouter$RouteInfo;->mDeviceAddress:Ljava/lang/String;

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_22

    #@21
    .line 892
    .end local v2           #info:Landroid/media/MediaRouter$RouteInfo;
    :goto_21
    return-object v2

    #@22
    .line 886
    .restart local v2       #info:Landroid/media/MediaRouter$RouteInfo;
    :cond_22
    add-int/lit8 v1, v1, 0x1

    #@24
    goto :goto_9

    #@25
    .line 892
    .end local v2           #info:Landroid/media/MediaRouter$RouteInfo;
    :cond_25
    const/4 v2, 0x0

    #@26
    goto :goto_21
.end method

.method static getRouteAtStatic(I)Landroid/media/MediaRouter$RouteInfo;
    .registers 2
    .parameter "index"

    #@0
    .prologue
    .line 621
    sget-object v0, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2
    iget-object v0, v0, Landroid/media/MediaRouter$Static;->mRoutes:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    #@a
    return-object v0
.end method

.method static getRouteCountStatic()I
    .registers 1

    #@0
    .prologue
    .line 617
    sget-object v0, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2
    iget-object v0, v0, Landroid/media/MediaRouter$Static;->mRoutes:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method static makeWifiDisplayRoute(Landroid/hardware/display/WifiDisplay;Z)Landroid/media/MediaRouter$RouteInfo;
    .registers 5
    .parameter "display"
    .parameter "available"

    #@0
    .prologue
    const/4 v1, 0x3

    #@1
    .line 806
    new-instance v0, Landroid/media/MediaRouter$RouteInfo;

    #@3
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@5
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mSystemCategory:Landroid/media/MediaRouter$RouteCategory;

    #@7
    invoke-direct {v0, v2}, Landroid/media/MediaRouter$RouteInfo;-><init>(Landroid/media/MediaRouter$RouteCategory;)V

    #@a
    .line 807
    .local v0, newRoute:Landroid/media/MediaRouter$RouteInfo;
    invoke-virtual {p0}, Landroid/hardware/display/WifiDisplay;->getDeviceAddress()Ljava/lang/String;

    #@d
    move-result-object v2

    #@e
    iput-object v2, v0, Landroid/media/MediaRouter$RouteInfo;->mDeviceAddress:Ljava/lang/String;

    #@10
    .line 808
    iput v1, v0, Landroid/media/MediaRouter$RouteInfo;->mSupportedTypes:I

    #@12
    .line 809
    const/4 v2, 0x0

    #@13
    iput v2, v0, Landroid/media/MediaRouter$RouteInfo;->mVolumeHandling:I

    #@15
    .line 810
    const/4 v2, 0x1

    #@16
    iput v2, v0, Landroid/media/MediaRouter$RouteInfo;->mPlaybackType:I

    #@18
    .line 812
    if-eqz p1, :cond_32

    #@1a
    :goto_1a
    invoke-virtual {v0, v1}, Landroid/media/MediaRouter$RouteInfo;->setStatusCode(I)Z

    #@1d
    .line 814
    iput-boolean p1, v0, Landroid/media/MediaRouter$RouteInfo;->mEnabled:Z

    #@1f
    .line 816
    invoke-virtual {p0}, Landroid/hardware/display/WifiDisplay;->getFriendlyDisplayName()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    iput-object v1, v0, Landroid/media/MediaRouter$RouteInfo;->mName:Ljava/lang/CharSequence;

    #@25
    .line 818
    sget-object v1, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@27
    invoke-virtual {v1}, Landroid/media/MediaRouter$Static;->getAllPresentationDisplays()[Landroid/view/Display;

    #@2a
    move-result-object v1

    #@2b
    invoke-static {v0, v1}, Landroid/media/MediaRouter;->choosePresentationDisplayForRoute(Landroid/media/MediaRouter$RouteInfo;[Landroid/view/Display;)Landroid/view/Display;

    #@2e
    move-result-object v1

    #@2f
    iput-object v1, v0, Landroid/media/MediaRouter$RouteInfo;->mPresentationDisplay:Landroid/view/Display;

    #@31
    .line 820
    return-object v0

    #@32
    .line 812
    :cond_32
    const/4 v1, 0x2

    #@33
    goto :goto_1a
.end method

.method static matchesDeviceAddress(Landroid/hardware/display/WifiDisplay;Landroid/media/MediaRouter$RouteInfo;)Z
    .registers 6
    .parameter "display"
    .parameter "info"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 441
    if-eqz p1, :cond_e

    #@4
    iget-object v3, p1, Landroid/media/MediaRouter$RouteInfo;->mDeviceAddress:Ljava/lang/String;

    #@6
    if-eqz v3, :cond_e

    #@8
    move v0, v1

    #@9
    .line 442
    .local v0, routeHasAddress:Z
    :goto_9
    if-nez p0, :cond_10

    #@b
    if-nez v0, :cond_10

    #@d
    .line 449
    :goto_d
    return v1

    #@e
    .end local v0           #routeHasAddress:Z
    :cond_e
    move v0, v2

    #@f
    .line 441
    goto :goto_9

    #@10
    .line 446
    .restart local v0       #routeHasAddress:Z
    :cond_10
    if-eqz p0, :cond_1f

    #@12
    if-eqz v0, :cond_1f

    #@14
    .line 447
    invoke-virtual {p0}, Landroid/hardware/display/WifiDisplay;->getDeviceAddress()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    iget-object v2, p1, Landroid/media/MediaRouter$RouteInfo;->mDeviceAddress:Ljava/lang/String;

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v1

    #@1e
    goto :goto_d

    #@1f
    :cond_1f
    move v1, v2

    #@20
    .line 449
    goto :goto_d
.end method

.method static removeRoute(Landroid/media/MediaRouter$RouteInfo;)V
    .registers 8
    .parameter "info"

    #@0
    .prologue
    .line 526
    sget-object v5, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2
    iget-object v5, v5, Landroid/media/MediaRouter$Static;->mRoutes:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v5, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@7
    move-result v5

    #@8
    if-eqz v5, :cond_47

    #@a
    .line 527
    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteInfo;->getCategory()Landroid/media/MediaRouter$RouteCategory;

    #@d
    move-result-object v4

    #@e
    .line 528
    .local v4, removingCat:Landroid/media/MediaRouter$RouteCategory;
    sget-object v5, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@10
    iget-object v5, v5, Landroid/media/MediaRouter$Static;->mRoutes:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@15
    move-result v1

    #@16
    .line 529
    .local v1, count:I
    const/4 v2, 0x0

    #@17
    .line 530
    .local v2, found:Z
    const/4 v3, 0x0

    #@18
    .local v3, i:I
    :goto_18
    if-ge v3, v1, :cond_2b

    #@1a
    .line 531
    sget-object v5, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@1c
    iget-object v5, v5, Landroid/media/MediaRouter$Static;->mRoutes:Ljava/util/ArrayList;

    #@1e
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@21
    move-result-object v5

    #@22
    check-cast v5, Landroid/media/MediaRouter$RouteInfo;

    #@24
    invoke-virtual {v5}, Landroid/media/MediaRouter$RouteInfo;->getCategory()Landroid/media/MediaRouter$RouteCategory;

    #@27
    move-result-object v0

    #@28
    .line 532
    .local v0, cat:Landroid/media/MediaRouter$RouteCategory;
    if-ne v4, v0, :cond_48

    #@2a
    .line 533
    const/4 v2, 0x1

    #@2b
    .line 537
    .end local v0           #cat:Landroid/media/MediaRouter$RouteCategory;
    :cond_2b
    sget-object v5, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2d
    iget-object v5, v5, Landroid/media/MediaRouter$Static;->mSelectedRoute:Landroid/media/MediaRouter$RouteInfo;

    #@2f
    if-ne p0, v5, :cond_3b

    #@31
    .line 540
    const v5, 0x800001

    #@34
    sget-object v6, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@36
    iget-object v6, v6, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    #@38
    invoke-static {v5, v6}, Landroid/media/MediaRouter;->selectRouteStatic(ILandroid/media/MediaRouter$RouteInfo;)V

    #@3b
    .line 542
    :cond_3b
    if-nez v2, :cond_44

    #@3d
    .line 543
    sget-object v5, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@3f
    iget-object v5, v5, Landroid/media/MediaRouter$Static;->mCategories:Ljava/util/ArrayList;

    #@41
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@44
    .line 545
    :cond_44
    invoke-static {p0}, Landroid/media/MediaRouter;->dispatchRouteRemoved(Landroid/media/MediaRouter$RouteInfo;)V

    #@47
    .line 547
    .end local v1           #count:I
    .end local v2           #found:Z
    .end local v3           #i:I
    .end local v4           #removingCat:Landroid/media/MediaRouter$RouteCategory;
    :cond_47
    return-void

    #@48
    .line 530
    .restart local v0       #cat:Landroid/media/MediaRouter$RouteCategory;
    .restart local v1       #count:I
    .restart local v2       #found:Z
    .restart local v3       #i:I
    .restart local v4       #removingCat:Landroid/media/MediaRouter$RouteCategory;
    :cond_48
    add-int/lit8 v3, v3, 0x1

    #@4a
    goto :goto_18
.end method

.method static selectRouteStatic(ILandroid/media/MediaRouter$RouteInfo;)V
    .registers 11
    .parameter "types"
    .parameter "route"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 394
    sget-object v7, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@4
    iget-object v4, v7, Landroid/media/MediaRouter$Static;->mSelectedRoute:Landroid/media/MediaRouter$RouteInfo;

    #@6
    .line 395
    .local v4, oldRoute:Landroid/media/MediaRouter$RouteInfo;
    if-ne v4, p1, :cond_9

    #@8
    .line 434
    :cond_8
    :goto_8
    return-void

    #@9
    .line 396
    :cond_9
    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getSupportedTypes()I

    #@c
    move-result v7

    #@d
    and-int/2addr v7, p0

    #@e
    if-nez v7, :cond_40

    #@10
    .line 397
    const-string v6, "MediaRouter"

    #@12
    new-instance v7, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string/jumbo v8, "selectRoute ignored; cannot select route with supported types "

    #@1a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v7

    #@1e
    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getSupportedTypes()I

    #@21
    move-result v8

    #@22
    invoke-static {v8}, Landroid/media/MediaRouter;->typesToString(I)Ljava/lang/String;

    #@25
    move-result-object v8

    #@26
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v7

    #@2a
    const-string v8, " into route types "

    #@2c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v7

    #@30
    invoke-static {p0}, Landroid/media/MediaRouter;->typesToString(I)Ljava/lang/String;

    #@33
    move-result-object v8

    #@34
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v7

    #@38
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v7

    #@3c
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    goto :goto_8

    #@40
    .line 403
    :cond_40
    sget-object v7, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@42
    iget-object v1, v7, Landroid/media/MediaRouter$Static;->mBluetoothA2dpRoute:Landroid/media/MediaRouter$RouteInfo;

    #@44
    .line 404
    .local v1, btRoute:Landroid/media/MediaRouter$RouteInfo;
    if-eqz v1, :cond_5c

    #@46
    and-int/lit8 v7, p0, 0x1

    #@48
    if-eqz v7, :cond_5c

    #@4a
    if-eq p1, v1, :cond_52

    #@4c
    sget-object v7, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@4e
    iget-object v7, v7, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    #@50
    if-ne p1, v7, :cond_5c

    #@52
    .line 407
    :cond_52
    :try_start_52
    sget-object v7, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@54
    iget-object v8, v7, Landroid/media/MediaRouter$Static;->mAudioService:Landroid/media/IAudioService;

    #@56
    if-ne p1, v1, :cond_a6

    #@58
    move v7, v3

    #@59
    :goto_59
    invoke-interface {v8, v7}, Landroid/media/IAudioService;->setBluetoothA2dpOn(Z)V
    :try_end_5c
    .catch Landroid/os/RemoteException; {:try_start_52 .. :try_end_5c} :catch_a8

    #@5c
    .line 413
    :cond_5c
    :goto_5c
    sget-object v7, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@5e
    iget-object v7, v7, Landroid/media/MediaRouter$Static;->mDisplayService:Landroid/hardware/display/DisplayManager;

    #@60
    invoke-virtual {v7}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    #@63
    move-result-object v7

    #@64
    invoke-virtual {v7}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplay()Landroid/hardware/display/WifiDisplay;

    #@67
    move-result-object v0

    #@68
    .line 415
    .local v0, activeDisplay:Landroid/hardware/display/WifiDisplay;
    if-eqz v4, :cond_b1

    #@6a
    iget-object v7, v4, Landroid/media/MediaRouter$RouteInfo;->mDeviceAddress:Ljava/lang/String;

    #@6c
    if-eqz v7, :cond_b1

    #@6e
    move v5, v3

    #@6f
    .line 416
    .local v5, oldRouteHasAddress:Z
    :goto_6f
    if-eqz p1, :cond_b3

    #@71
    iget-object v7, p1, Landroid/media/MediaRouter$RouteInfo;->mDeviceAddress:Ljava/lang/String;

    #@73
    if-eqz v7, :cond_b3

    #@75
    .line 417
    .local v3, newRouteHasAddress:Z
    :goto_75
    if-nez v0, :cond_7b

    #@77
    if-nez v5, :cond_7b

    #@79
    if-eqz v3, :cond_8c

    #@7b
    .line 418
    :cond_7b
    if-eqz v3, :cond_b5

    #@7d
    invoke-static {v0, p1}, Landroid/media/MediaRouter;->matchesDeviceAddress(Landroid/hardware/display/WifiDisplay;Landroid/media/MediaRouter$RouteInfo;)Z

    #@80
    move-result v6

    #@81
    if-nez v6, :cond_b5

    #@83
    .line 419
    sget-object v6, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@85
    iget-object v6, v6, Landroid/media/MediaRouter$Static;->mDisplayService:Landroid/hardware/display/DisplayManager;

    #@87
    iget-object v7, p1, Landroid/media/MediaRouter$RouteInfo;->mDeviceAddress:Ljava/lang/String;

    #@89
    invoke-virtual {v6, v7}, Landroid/hardware/display/DisplayManager;->connectWifiDisplay(Ljava/lang/String;)V

    #@8c
    .line 425
    :cond_8c
    :goto_8c
    if-eqz v4, :cond_96

    #@8e
    .line 427
    invoke-virtual {v4}, Landroid/media/MediaRouter$RouteInfo;->getSupportedTypes()I

    #@91
    move-result v6

    #@92
    and-int/2addr v6, p0

    #@93
    invoke-static {v6, v4}, Landroid/media/MediaRouter;->dispatchRouteUnselected(ILandroid/media/MediaRouter$RouteInfo;)V

    #@96
    .line 429
    :cond_96
    sget-object v6, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@98
    iput-object p1, v6, Landroid/media/MediaRouter$Static;->mSelectedRoute:Landroid/media/MediaRouter$RouteInfo;

    #@9a
    .line 430
    if-eqz p1, :cond_8

    #@9c
    .line 432
    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getSupportedTypes()I

    #@9f
    move-result v6

    #@a0
    and-int/2addr v6, p0

    #@a1
    invoke-static {v6, p1}, Landroid/media/MediaRouter;->dispatchRouteSelected(ILandroid/media/MediaRouter$RouteInfo;)V

    #@a4
    goto/16 :goto_8

    #@a6
    .end local v0           #activeDisplay:Landroid/hardware/display/WifiDisplay;
    .end local v3           #newRouteHasAddress:Z
    .end local v5           #oldRouteHasAddress:Z
    :cond_a6
    move v7, v6

    #@a7
    .line 407
    goto :goto_59

    #@a8
    .line 408
    :catch_a8
    move-exception v2

    #@a9
    .line 409
    .local v2, e:Landroid/os/RemoteException;
    const-string v7, "MediaRouter"

    #@ab
    const-string v8, "Error changing Bluetooth A2DP state"

    #@ad
    invoke-static {v7, v8, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@b0
    goto :goto_5c

    #@b1
    .end local v2           #e:Landroid/os/RemoteException;
    .restart local v0       #activeDisplay:Landroid/hardware/display/WifiDisplay;
    :cond_b1
    move v5, v6

    #@b2
    .line 415
    goto :goto_6f

    #@b3
    .restart local v5       #oldRouteHasAddress:Z
    :cond_b3
    move v3, v6

    #@b4
    .line 416
    goto :goto_75

    #@b5
    .line 420
    .restart local v3       #newRouteHasAddress:Z
    :cond_b5
    if-eqz v0, :cond_8c

    #@b7
    if-nez v3, :cond_8c

    #@b9
    .line 421
    sget-object v6, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@bb
    iget-object v6, v6, Landroid/media/MediaRouter$Static;->mDisplayService:Landroid/hardware/display/DisplayManager;

    #@bd
    invoke-virtual {v6}, Landroid/hardware/display/DisplayManager;->disconnectWifiDisplay()V

    #@c0
    goto :goto_8c
.end method

.method static systemVolumeChanged(I)V
    .registers 5
    .parameter "newValue"

    #@0
    .prologue
    .line 737
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2
    iget-object v1, v2, Landroid/media/MediaRouter$Static;->mSelectedRoute:Landroid/media/MediaRouter$RouteInfo;

    #@4
    .line 738
    .local v1, selectedRoute:Landroid/media/MediaRouter$RouteInfo;
    if-nez v1, :cond_7

    #@6
    .line 753
    :goto_6
    return-void

    #@7
    .line 740
    :cond_7
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@9
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mBluetoothA2dpRoute:Landroid/media/MediaRouter$RouteInfo;

    #@b
    if-eq v1, v2, :cond_13

    #@d
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@f
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    #@11
    if-ne v1, v2, :cond_17

    #@13
    .line 742
    :cond_13
    invoke-static {v1}, Landroid/media/MediaRouter;->dispatchRouteVolumeChanged(Landroid/media/MediaRouter$RouteInfo;)V

    #@16
    goto :goto_6

    #@17
    .line 743
    :cond_17
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@19
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mBluetoothA2dpRoute:Landroid/media/MediaRouter$RouteInfo;

    #@1b
    if-eqz v2, :cond_3d

    #@1d
    .line 745
    :try_start_1d
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@1f
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mAudioService:Landroid/media/IAudioService;

    #@21
    invoke-interface {v2}, Landroid/media/IAudioService;->isBluetoothA2dpOn()Z

    #@24
    move-result v2

    #@25
    if-eqz v2, :cond_38

    #@27
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@29
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mBluetoothA2dpRoute:Landroid/media/MediaRouter$RouteInfo;

    #@2b
    :goto_2b
    invoke-static {v2}, Landroid/media/MediaRouter;->dispatchRouteVolumeChanged(Landroid/media/MediaRouter$RouteInfo;)V
    :try_end_2e
    .catch Landroid/os/RemoteException; {:try_start_1d .. :try_end_2e} :catch_2f

    #@2e
    goto :goto_6

    #@2f
    .line 747
    :catch_2f
    move-exception v0

    #@30
    .line 748
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "MediaRouter"

    #@32
    const-string v3, "Error checking Bluetooth A2DP state to report volume change"

    #@34
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@37
    goto :goto_6

    #@38
    .line 745
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_38
    :try_start_38
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@3a
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;
    :try_end_3c
    .catch Landroid/os/RemoteException; {:try_start_38 .. :try_end_3c} :catch_2f

    #@3c
    goto :goto_2b

    #@3d
    .line 751
    :cond_3d
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@3f
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    #@41
    invoke-static {v2}, Landroid/media/MediaRouter;->dispatchRouteVolumeChanged(Landroid/media/MediaRouter$RouteInfo;)V

    #@44
    goto :goto_6
.end method

.method static typesToString(I)Ljava/lang/String;
    .registers 3
    .parameter "types"

    #@0
    .prologue
    .line 277
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 278
    .local v0, result:Ljava/lang/StringBuilder;
    and-int/lit8 v1, p0, 0x1

    #@7
    if-eqz v1, :cond_e

    #@9
    .line 279
    const-string v1, "ROUTE_TYPE_LIVE_AUDIO "

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    .line 281
    :cond_e
    and-int/lit8 v1, p0, 0x2

    #@10
    if-eqz v1, :cond_17

    #@12
    .line 282
    const-string v1, "ROUTE_TYPE_LIVE_VIDEO "

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    .line 284
    :cond_17
    const/high16 v1, 0x80

    #@19
    and-int/2addr v1, p0

    #@1a
    if-eqz v1, :cond_21

    #@1c
    .line 285
    const-string v1, "ROUTE_TYPE_USER "

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    .line 287
    :cond_21
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    return-object v1
.end method

.method static updateRoute(Landroid/media/MediaRouter$RouteInfo;)V
    .registers 1
    .parameter "info"

    #@0
    .prologue
    .line 661
    invoke-static {p0}, Landroid/media/MediaRouter;->dispatchRouteChanged(Landroid/media/MediaRouter$RouteInfo;)V

    #@3
    .line 662
    return-void
.end method

.method private static updateWifiDisplayRoute(Landroid/media/MediaRouter$RouteInfo;Landroid/hardware/display/WifiDisplay;ZLandroid/hardware/display/WifiDisplayStatus;)V
    .registers 14
    .parameter "route"
    .parameter "display"
    .parameter "available"
    .parameter "wifiDisplayStatus"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 825
    invoke-virtual {p3}, Landroid/hardware/display/WifiDisplayStatus;->getScanState()I

    #@5
    move-result v8

    #@6
    if-ne v8, v6, :cond_57

    #@8
    move v3, v6

    #@9
    .line 828
    .local v3, isScanning:Z
    :goto_9
    const/4 v1, 0x0

    #@a
    .line 829
    .local v1, changed:Z
    const/4 v5, 0x0

    #@b
    .line 831
    .local v5, newStatus:I
    if-eqz p2, :cond_5b

    #@d
    .line 832
    if-eqz v3, :cond_59

    #@f
    move v5, v6

    #@10
    .line 837
    :goto_10
    invoke-virtual {p3}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplay()Landroid/hardware/display/WifiDisplay;

    #@13
    move-result-object v8

    #@14
    invoke-virtual {p1, v8}, Landroid/hardware/display/WifiDisplay;->equals(Landroid/hardware/display/WifiDisplay;)Z

    #@17
    move-result v8

    #@18
    if-eqz v8, :cond_21

    #@1a
    .line 838
    invoke-virtual {p3}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    #@1d
    move-result v0

    #@1e
    .line 839
    .local v0, activeState:I
    packed-switch v0, :pswitch_data_6c

    #@21
    .line 852
    .end local v0           #activeState:I
    :cond_21
    :goto_21
    invoke-virtual {p1}, Landroid/hardware/display/WifiDisplay;->getFriendlyDisplayName()Ljava/lang/String;

    #@24
    move-result-object v4

    #@25
    .line 853
    .local v4, newName:Ljava/lang/String;
    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteInfo;->getName()Ljava/lang/CharSequence;

    #@28
    move-result-object v8

    #@29
    invoke-virtual {v8, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@2c
    move-result v8

    #@2d
    if-nez v8, :cond_32

    #@2f
    .line 854
    iput-object v4, p0, Landroid/media/MediaRouter$RouteInfo;->mName:Ljava/lang/CharSequence;

    #@31
    .line 855
    const/4 v1, 0x1

    #@32
    .line 858
    :cond_32
    iget-boolean v8, p0, Landroid/media/MediaRouter$RouteInfo;->mEnabled:Z

    #@34
    if-eq v8, p2, :cond_69

    #@36
    :goto_36
    or-int/2addr v1, v6

    #@37
    .line 859
    iput-boolean p2, p0, Landroid/media/MediaRouter$RouteInfo;->mEnabled:Z

    #@39
    .line 861
    invoke-virtual {p0, v5}, Landroid/media/MediaRouter$RouteInfo;->setStatusCode(I)Z

    #@3c
    move-result v6

    #@3d
    or-int/2addr v1, v6

    #@3e
    .line 863
    if-eqz v1, :cond_43

    #@40
    .line 864
    invoke-static {p0}, Landroid/media/MediaRouter;->dispatchRouteChanged(Landroid/media/MediaRouter$RouteInfo;)V

    #@43
    .line 867
    :cond_43
    if-nez p2, :cond_56

    #@45
    sget-object v6, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@47
    iget-object v6, v6, Landroid/media/MediaRouter$Static;->mSelectedRoute:Landroid/media/MediaRouter$RouteInfo;

    #@49
    if-ne p0, v6, :cond_56

    #@4b
    .line 869
    sget-object v6, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@4d
    iget-object v2, v6, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    #@4f
    .line 870
    .local v2, defaultRoute:Landroid/media/MediaRouter$RouteInfo;
    invoke-virtual {v2}, Landroid/media/MediaRouter$RouteInfo;->getSupportedTypes()I

    #@52
    move-result v6

    #@53
    invoke-static {v6, v2}, Landroid/media/MediaRouter;->selectRouteStatic(ILandroid/media/MediaRouter$RouteInfo;)V

    #@56
    .line 872
    .end local v2           #defaultRoute:Landroid/media/MediaRouter$RouteInfo;
    :cond_56
    return-void

    #@57
    .end local v1           #changed:Z
    .end local v3           #isScanning:Z
    .end local v4           #newName:Ljava/lang/String;
    .end local v5           #newStatus:I
    :cond_57
    move v3, v7

    #@58
    .line 825
    goto :goto_9

    #@59
    .line 832
    .restart local v1       #changed:Z
    .restart local v3       #isScanning:Z
    .restart local v5       #newStatus:I
    :cond_59
    const/4 v5, 0x3

    #@5a
    goto :goto_10

    #@5b
    .line 834
    :cond_5b
    const/4 v5, 0x4

    #@5c
    goto :goto_10

    #@5d
    .line 841
    .restart local v0       #activeState:I
    :pswitch_5d
    const/4 v5, 0x0

    #@5e
    .line 842
    goto :goto_21

    #@5f
    .line 844
    :pswitch_5f
    const/4 v5, 0x2

    #@60
    .line 845
    goto :goto_21

    #@61
    .line 847
    :pswitch_61
    const-string v8, "MediaRouter"

    #@63
    const-string v9, "Active display is not connected!"

    #@65
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@68
    goto :goto_21

    #@69
    .end local v0           #activeState:I
    .restart local v4       #newName:Ljava/lang/String;
    :cond_69
    move v6, v7

    #@6a
    .line 858
    goto :goto_36

    #@6b
    .line 839
    nop

    #@6c
    :pswitch_data_6c
    .packed-switch 0x0
        :pswitch_61
        :pswitch_5f
        :pswitch_5d
    .end packed-switch
.end method

.method static updateWifiDisplayStatus(Landroid/hardware/display/WifiDisplayStatus;)V
    .registers 16
    .parameter "newStatus"

    #@0
    .prologue
    .line 756
    sget-object v14, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2
    iget-object v11, v14, Landroid/media/MediaRouter$Static;->mLastKnownWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    #@4
    .line 759
    .local v11, oldStatus:Landroid/hardware/display/WifiDisplayStatus;
    const/4 v13, 0x0

    #@5
    .line 760
    .local v13, wantScan:Z
    const/4 v4, 0x0

    #@6
    .line 761
    .local v4, blockScan:Z
    if-eqz v11, :cond_4a

    #@8
    invoke-virtual {v11}, Landroid/hardware/display/WifiDisplayStatus;->getRememberedDisplays()[Landroid/hardware/display/WifiDisplay;

    #@b
    move-result-object v9

    #@c
    .line 763
    .local v9, oldDisplays:[Landroid/hardware/display/WifiDisplay;
    :goto_c
    invoke-virtual {p0}, Landroid/hardware/display/WifiDisplayStatus;->getRememberedDisplays()[Landroid/hardware/display/WifiDisplay;

    #@f
    move-result-object v8

    #@10
    .line 764
    .local v8, newDisplays:[Landroid/hardware/display/WifiDisplay;
    invoke-virtual {p0}, Landroid/hardware/display/WifiDisplayStatus;->getAvailableDisplays()[Landroid/hardware/display/WifiDisplay;

    #@13
    move-result-object v3

    #@14
    .line 765
    .local v3, availableDisplays:[Landroid/hardware/display/WifiDisplay;
    invoke-virtual {p0}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplay()Landroid/hardware/display/WifiDisplay;

    #@17
    move-result-object v0

    #@18
    .line 767
    .local v0, activeDisplay:Landroid/hardware/display/WifiDisplay;
    const/4 v6, 0x0

    #@19
    .local v6, i:I
    :goto_19
    array-length v14, v8

    #@1a
    if-ge v6, v14, :cond_61

    #@1c
    .line 768
    aget-object v5, v8, v6

    #@1e
    .line 769
    .local v5, d:Landroid/hardware/display/WifiDisplay;
    invoke-static {v5, v9}, Landroid/media/MediaRouter;->findMatchingDisplay(Landroid/hardware/display/WifiDisplay;[Landroid/hardware/display/WifiDisplay;)Landroid/hardware/display/WifiDisplay;

    #@21
    move-result-object v10

    #@22
    .line 770
    .local v10, oldRemembered:Landroid/hardware/display/WifiDisplay;
    if-nez v10, :cond_50

    #@24
    .line 771
    invoke-static {v5, v3}, Landroid/media/MediaRouter;->findMatchingDisplay(Landroid/hardware/display/WifiDisplay;[Landroid/hardware/display/WifiDisplay;)Landroid/hardware/display/WifiDisplay;

    #@27
    move-result-object v14

    #@28
    if-eqz v14, :cond_4e

    #@2a
    const/4 v14, 0x1

    #@2b
    :goto_2b
    invoke-static {v5, v14}, Landroid/media/MediaRouter;->makeWifiDisplayRoute(Landroid/hardware/display/WifiDisplay;Z)Landroid/media/MediaRouter$RouteInfo;

    #@2e
    move-result-object v14

    #@2f
    invoke-static {v14}, Landroid/media/MediaRouter;->addRouteStatic(Landroid/media/MediaRouter$RouteInfo;)V

    #@32
    .line 773
    const/4 v13, 0x1

    #@33
    .line 779
    :goto_33
    invoke-virtual {v5, v0}, Landroid/hardware/display/WifiDisplay;->equals(Landroid/hardware/display/WifiDisplay;)Z

    #@36
    move-result v14

    #@37
    if-eqz v14, :cond_47

    #@39
    .line 780
    invoke-static {v5}, Landroid/media/MediaRouter;->findWifiDisplayRoute(Landroid/hardware/display/WifiDisplay;)Landroid/media/MediaRouter$RouteInfo;

    #@3c
    move-result-object v1

    #@3d
    .line 781
    .local v1, activeRoute:Landroid/media/MediaRouter$RouteInfo;
    if-eqz v1, :cond_47

    #@3f
    .line 782
    invoke-virtual {v1}, Landroid/media/MediaRouter$RouteInfo;->getSupportedTypes()I

    #@42
    move-result v14

    #@43
    invoke-static {v14, v1}, Landroid/media/MediaRouter;->selectRouteStatic(ILandroid/media/MediaRouter$RouteInfo;)V

    #@46
    .line 786
    const/4 v4, 0x1

    #@47
    .line 767
    .end local v1           #activeRoute:Landroid/media/MediaRouter$RouteInfo;
    :cond_47
    add-int/lit8 v6, v6, 0x1

    #@49
    goto :goto_19

    #@4a
    .line 761
    .end local v0           #activeDisplay:Landroid/hardware/display/WifiDisplay;
    .end local v3           #availableDisplays:[Landroid/hardware/display/WifiDisplay;
    .end local v5           #d:Landroid/hardware/display/WifiDisplay;
    .end local v6           #i:I
    .end local v8           #newDisplays:[Landroid/hardware/display/WifiDisplay;
    .end local v9           #oldDisplays:[Landroid/hardware/display/WifiDisplay;
    .end local v10           #oldRemembered:Landroid/hardware/display/WifiDisplay;
    :cond_4a
    const/4 v14, 0x0

    #@4b
    new-array v9, v14, [Landroid/hardware/display/WifiDisplay;

    #@4d
    goto :goto_c

    #@4e
    .line 771
    .restart local v0       #activeDisplay:Landroid/hardware/display/WifiDisplay;
    .restart local v3       #availableDisplays:[Landroid/hardware/display/WifiDisplay;
    .restart local v5       #d:Landroid/hardware/display/WifiDisplay;
    .restart local v6       #i:I
    .restart local v8       #newDisplays:[Landroid/hardware/display/WifiDisplay;
    .restart local v9       #oldDisplays:[Landroid/hardware/display/WifiDisplay;
    .restart local v10       #oldRemembered:Landroid/hardware/display/WifiDisplay;
    :cond_4e
    const/4 v14, 0x0

    #@4f
    goto :goto_2b

    #@50
    .line 775
    :cond_50
    invoke-static {v5, v3}, Landroid/media/MediaRouter;->findMatchingDisplay(Landroid/hardware/display/WifiDisplay;[Landroid/hardware/display/WifiDisplay;)Landroid/hardware/display/WifiDisplay;

    #@53
    move-result-object v14

    #@54
    if-eqz v14, :cond_5f

    #@56
    const/4 v2, 0x1

    #@57
    .line 776
    .local v2, available:Z
    :goto_57
    invoke-static {v5}, Landroid/media/MediaRouter;->findWifiDisplayRoute(Landroid/hardware/display/WifiDisplay;)Landroid/media/MediaRouter$RouteInfo;

    #@5a
    move-result-object v12

    #@5b
    .line 777
    .local v12, route:Landroid/media/MediaRouter$RouteInfo;
    invoke-static {v12, v5, v2, p0}, Landroid/media/MediaRouter;->updateWifiDisplayRoute(Landroid/media/MediaRouter$RouteInfo;Landroid/hardware/display/WifiDisplay;ZLandroid/hardware/display/WifiDisplayStatus;)V

    #@5e
    goto :goto_33

    #@5f
    .line 775
    .end local v2           #available:Z
    .end local v12           #route:Landroid/media/MediaRouter$RouteInfo;
    :cond_5f
    const/4 v2, 0x0

    #@60
    goto :goto_57

    #@61
    .line 790
    .end local v5           #d:Landroid/hardware/display/WifiDisplay;
    .end local v10           #oldRemembered:Landroid/hardware/display/WifiDisplay;
    :cond_61
    const/4 v6, 0x0

    #@62
    :goto_62
    array-length v14, v9

    #@63
    if-ge v6, v14, :cond_77

    #@65
    .line 791
    aget-object v5, v9, v6

    #@67
    .line 792
    .restart local v5       #d:Landroid/hardware/display/WifiDisplay;
    invoke-static {v5, v8}, Landroid/media/MediaRouter;->findMatchingDisplay(Landroid/hardware/display/WifiDisplay;[Landroid/hardware/display/WifiDisplay;)Landroid/hardware/display/WifiDisplay;

    #@6a
    move-result-object v7

    #@6b
    .line 793
    .local v7, newDisplay:Landroid/hardware/display/WifiDisplay;
    if-nez v7, :cond_74

    #@6d
    .line 794
    invoke-static {v5}, Landroid/media/MediaRouter;->findWifiDisplayRoute(Landroid/hardware/display/WifiDisplay;)Landroid/media/MediaRouter$RouteInfo;

    #@70
    move-result-object v14

    #@71
    invoke-static {v14}, Landroid/media/MediaRouter;->removeRoute(Landroid/media/MediaRouter$RouteInfo;)V

    #@74
    .line 790
    :cond_74
    add-int/lit8 v6, v6, 0x1

    #@76
    goto :goto_62

    #@77
    .line 798
    .end local v5           #d:Landroid/hardware/display/WifiDisplay;
    .end local v7           #newDisplay:Landroid/hardware/display/WifiDisplay;
    :cond_77
    if-eqz v13, :cond_82

    #@79
    if-nez v4, :cond_82

    #@7b
    .line 799
    sget-object v14, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@7d
    iget-object v14, v14, Landroid/media/MediaRouter$Static;->mDisplayService:Landroid/hardware/display/DisplayManager;

    #@7f
    invoke-virtual {v14}, Landroid/hardware/display/DisplayManager;->scanWifiDisplays()V

    #@82
    .line 802
    :cond_82
    sget-object v14, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@84
    iput-object p0, v14, Landroid/media/MediaRouter$Static;->mLastKnownWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    #@86
    .line 803
    return-void
.end method


# virtual methods
.method public addCallback(ILandroid/media/MediaRouter$Callback;)V
    .registers 8
    .parameter "types"
    .parameter "cb"

    #@0
    .prologue
    .line 346
    sget-object v3, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2
    iget-object v3, v3, Landroid/media/MediaRouter$Static;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@4
    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    #@7
    move-result v0

    #@8
    .line 347
    .local v0, count:I
    const/4 v1, 0x0

    #@9
    .local v1, i:I
    :goto_9
    if-ge v1, v0, :cond_22

    #@b
    .line 348
    sget-object v3, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@d
    iget-object v3, v3, Landroid/media/MediaRouter$Static;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@f
    invoke-virtual {v3, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v2

    #@13
    check-cast v2, Landroid/media/MediaRouter$CallbackInfo;

    #@15
    .line 349
    .local v2, info:Landroid/media/MediaRouter$CallbackInfo;
    iget-object v3, v2, Landroid/media/MediaRouter$CallbackInfo;->cb:Landroid/media/MediaRouter$Callback;

    #@17
    if-ne v3, p2, :cond_1f

    #@19
    .line 350
    iget v3, v2, Landroid/media/MediaRouter$CallbackInfo;->type:I

    #@1b
    or-int/2addr v3, p1

    #@1c
    iput v3, v2, Landroid/media/MediaRouter$CallbackInfo;->type:I

    #@1e
    .line 355
    .end local v2           #info:Landroid/media/MediaRouter$CallbackInfo;
    :goto_1e
    return-void

    #@1f
    .line 347
    .restart local v2       #info:Landroid/media/MediaRouter$CallbackInfo;
    :cond_1f
    add-int/lit8 v1, v1, 0x1

    #@21
    goto :goto_9

    #@22
    .line 354
    .end local v2           #info:Landroid/media/MediaRouter$CallbackInfo;
    :cond_22
    sget-object v3, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@24
    iget-object v3, v3, Landroid/media/MediaRouter$Static;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@26
    new-instance v4, Landroid/media/MediaRouter$CallbackInfo;

    #@28
    invoke-direct {v4, p2, p1, p0}, Landroid/media/MediaRouter$CallbackInfo;-><init>(Landroid/media/MediaRouter$Callback;ILandroid/media/MediaRouter;)V

    #@2b
    invoke-virtual {v3, v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    #@2e
    goto :goto_1e
.end method

.method public addRouteInt(Landroid/media/MediaRouter$RouteInfo;)V
    .registers 2
    .parameter "info"

    #@0
    .prologue
    .line 468
    invoke-static {p1}, Landroid/media/MediaRouter;->addRouteStatic(Landroid/media/MediaRouter$RouteInfo;)V

    #@3
    .line 469
    return-void
.end method

.method public addUserRoute(Landroid/media/MediaRouter$UserRouteInfo;)V
    .registers 2
    .parameter "info"

    #@0
    .prologue
    .line 461
    invoke-static {p1}, Landroid/media/MediaRouter;->addRouteStatic(Landroid/media/MediaRouter$RouteInfo;)V

    #@3
    .line 462
    return-void
.end method

.method public clearUserRoutes()V
    .registers 4

    #@0
    .prologue
    .line 507
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@3
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mRoutes:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v2

    #@9
    if-ge v0, v2, :cond_25

    #@b
    .line 508
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@d
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mRoutes:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v1

    #@13
    check-cast v1, Landroid/media/MediaRouter$RouteInfo;

    #@15
    .line 511
    .local v1, info:Landroid/media/MediaRouter$RouteInfo;
    instance-of v2, v1, Landroid/media/MediaRouter$UserRouteInfo;

    #@17
    if-nez v2, :cond_1d

    #@19
    instance-of v2, v1, Landroid/media/MediaRouter$RouteGroup;

    #@1b
    if-eqz v2, :cond_22

    #@1d
    .line 512
    :cond_1d
    invoke-virtual {p0, v0}, Landroid/media/MediaRouter;->removeRouteAt(I)V

    #@20
    .line 513
    add-int/lit8 v0, v0, -0x1

    #@22
    .line 507
    :cond_22
    add-int/lit8 v0, v0, 0x1

    #@24
    goto :goto_1

    #@25
    .line 516
    .end local v1           #info:Landroid/media/MediaRouter$RouteInfo;
    :cond_25
    return-void
.end method

.method public createRouteCategory(IZ)Landroid/media/MediaRouter$RouteCategory;
    .registers 5
    .parameter "nameResId"
    .parameter "isGroupable"

    #@0
    .prologue
    .line 657
    new-instance v0, Landroid/media/MediaRouter$RouteCategory;

    #@2
    const/high16 v1, 0x80

    #@4
    invoke-direct {v0, p1, v1, p2}, Landroid/media/MediaRouter$RouteCategory;-><init>(IIZ)V

    #@7
    return-object v0
.end method

.method public createRouteCategory(Ljava/lang/CharSequence;Z)Landroid/media/MediaRouter$RouteCategory;
    .registers 5
    .parameter "name"
    .parameter "isGroupable"

    #@0
    .prologue
    .line 646
    new-instance v0, Landroid/media/MediaRouter$RouteCategory;

    #@2
    const/high16 v1, 0x80

    #@4
    invoke-direct {v0, p1, v1, p2}, Landroid/media/MediaRouter$RouteCategory;-><init>(Ljava/lang/CharSequence;IZ)V

    #@7
    return-object v0
.end method

.method public createUserRoute(Landroid/media/MediaRouter$RouteCategory;)Landroid/media/MediaRouter$UserRouteInfo;
    .registers 3
    .parameter "category"

    #@0
    .prologue
    .line 635
    new-instance v0, Landroid/media/MediaRouter$UserRouteInfo;

    #@2
    invoke-direct {v0, p1}, Landroid/media/MediaRouter$UserRouteInfo;-><init>(Landroid/media/MediaRouter$RouteCategory;)V

    #@5
    return-object v0
.end method

.method public getCategoryAt(I)Landroid/media/MediaRouter$RouteCategory;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 593
    sget-object v0, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2
    iget-object v0, v0, Landroid/media/MediaRouter$Static;->mCategories:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Landroid/media/MediaRouter$RouteCategory;

    #@a
    return-object v0
.end method

.method public getCategoryCount()I
    .registers 2

    #@0
    .prologue
    .line 582
    sget-object v0, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2
    iget-object v0, v0, Landroid/media/MediaRouter$Static;->mCategories:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getRouteAt(I)Landroid/media/MediaRouter$RouteInfo;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 613
    sget-object v0, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2
    iget-object v0, v0, Landroid/media/MediaRouter$Static;->mRoutes:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    #@a
    return-object v0
.end method

.method public getRouteCount()I
    .registers 2

    #@0
    .prologue
    .line 603
    sget-object v0, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2
    iget-object v0, v0, Landroid/media/MediaRouter$Static;->mRoutes:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getSelectedRoute(I)Landroid/media/MediaRouter$RouteInfo;
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 322
    sget-object v0, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2
    iget-object v0, v0, Landroid/media/MediaRouter$Static;->mSelectedRoute:Landroid/media/MediaRouter$RouteInfo;

    #@4
    if-eqz v0, :cond_14

    #@6
    sget-object v0, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@8
    iget-object v0, v0, Landroid/media/MediaRouter$Static;->mSelectedRoute:Landroid/media/MediaRouter$RouteInfo;

    #@a
    iget v0, v0, Landroid/media/MediaRouter$RouteInfo;->mSupportedTypes:I

    #@c
    and-int/2addr v0, p1

    #@d
    if-eqz v0, :cond_14

    #@f
    .line 326
    sget-object v0, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@11
    iget-object v0, v0, Landroid/media/MediaRouter$Static;->mSelectedRoute:Landroid/media/MediaRouter$RouteInfo;

    #@13
    .line 334
    :goto_13
    return-object v0

    #@14
    .line 327
    :cond_14
    const/high16 v0, 0x80

    #@16
    if-ne p1, v0, :cond_1a

    #@18
    .line 330
    const/4 v0, 0x0

    #@19
    goto :goto_13

    #@1a
    .line 334
    :cond_1a
    sget-object v0, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@1c
    iget-object v0, v0, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    #@1e
    goto :goto_13
.end method

.method public getSystemAudioCategory()Landroid/media/MediaRouter$RouteCategory;
    .registers 2

    #@0
    .prologue
    .line 312
    sget-object v0, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2
    iget-object v0, v0, Landroid/media/MediaRouter$Static;->mSystemCategory:Landroid/media/MediaRouter$RouteCategory;

    #@4
    return-object v0
.end method

.method public getSystemAudioRoute()Landroid/media/MediaRouter$RouteInfo;
    .registers 2

    #@0
    .prologue
    .line 305
    sget-object v0, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2
    iget-object v0, v0, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    #@4
    return-object v0
.end method

.method public removeCallback(Landroid/media/MediaRouter$Callback;)V
    .registers 7
    .parameter "cb"

    #@0
    .prologue
    .line 363
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@4
    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    #@7
    move-result v0

    #@8
    .line 364
    .local v0, count:I
    const/4 v1, 0x0

    #@9
    .local v1, i:I
    :goto_9
    if-ge v1, v0, :cond_24

    #@b
    .line 365
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@d
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@f
    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v2

    #@13
    check-cast v2, Landroid/media/MediaRouter$CallbackInfo;

    #@15
    iget-object v2, v2, Landroid/media/MediaRouter$CallbackInfo;->cb:Landroid/media/MediaRouter$Callback;

    #@17
    if-ne v2, p1, :cond_21

    #@19
    .line 366
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@1b
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@1d
    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(I)Ljava/lang/Object;

    #@20
    .line 371
    :goto_20
    return-void

    #@21
    .line 364
    :cond_21
    add-int/lit8 v1, v1, 0x1

    #@23
    goto :goto_9

    #@24
    .line 370
    :cond_24
    const-string v2, "MediaRouter"

    #@26
    new-instance v3, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string/jumbo v4, "removeCallback("

    #@2e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v3

    #@36
    const-string v4, "): callback not registered"

    #@38
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v3

    #@3c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v3

    #@40
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    goto :goto_20
.end method

.method removeRouteAt(I)V
    .registers 10
    .parameter "routeIndex"

    #@0
    .prologue
    .line 550
    if-ltz p1, :cond_53

    #@2
    sget-object v6, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@4
    iget-object v6, v6, Landroid/media/MediaRouter$Static;->mRoutes:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v6

    #@a
    if-ge p1, v6, :cond_53

    #@c
    .line 551
    sget-object v6, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@e
    iget-object v6, v6, Landroid/media/MediaRouter$Static;->mRoutes:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@13
    move-result-object v4

    #@14
    check-cast v4, Landroid/media/MediaRouter$RouteInfo;

    #@16
    .line 552
    .local v4, info:Landroid/media/MediaRouter$RouteInfo;
    invoke-virtual {v4}, Landroid/media/MediaRouter$RouteInfo;->getCategory()Landroid/media/MediaRouter$RouteCategory;

    #@19
    move-result-object v5

    #@1a
    .line 553
    .local v5, removingCat:Landroid/media/MediaRouter$RouteCategory;
    sget-object v6, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@1c
    iget-object v6, v6, Landroid/media/MediaRouter$Static;->mRoutes:Ljava/util/ArrayList;

    #@1e
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@21
    move-result v1

    #@22
    .line 554
    .local v1, count:I
    const/4 v2, 0x0

    #@23
    .line 555
    .local v2, found:Z
    const/4 v3, 0x0

    #@24
    .local v3, i:I
    :goto_24
    if-ge v3, v1, :cond_37

    #@26
    .line 556
    sget-object v6, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@28
    iget-object v6, v6, Landroid/media/MediaRouter$Static;->mRoutes:Ljava/util/ArrayList;

    #@2a
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2d
    move-result-object v6

    #@2e
    check-cast v6, Landroid/media/MediaRouter$RouteInfo;

    #@30
    invoke-virtual {v6}, Landroid/media/MediaRouter$RouteInfo;->getCategory()Landroid/media/MediaRouter$RouteCategory;

    #@33
    move-result-object v0

    #@34
    .line 557
    .local v0, cat:Landroid/media/MediaRouter$RouteCategory;
    if-ne v5, v0, :cond_54

    #@36
    .line 558
    const/4 v2, 0x1

    #@37
    .line 562
    .end local v0           #cat:Landroid/media/MediaRouter$RouteCategory;
    :cond_37
    sget-object v6, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@39
    iget-object v6, v6, Landroid/media/MediaRouter$Static;->mSelectedRoute:Landroid/media/MediaRouter$RouteInfo;

    #@3b
    if-ne v4, v6, :cond_47

    #@3d
    .line 565
    const v6, 0x800003

    #@40
    sget-object v7, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@42
    iget-object v7, v7, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    #@44
    invoke-static {v6, v7}, Landroid/media/MediaRouter;->selectRouteStatic(ILandroid/media/MediaRouter$RouteInfo;)V

    #@47
    .line 568
    :cond_47
    if-nez v2, :cond_50

    #@49
    .line 569
    sget-object v6, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@4b
    iget-object v6, v6, Landroid/media/MediaRouter$Static;->mCategories:Ljava/util/ArrayList;

    #@4d
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@50
    .line 571
    :cond_50
    invoke-static {v4}, Landroid/media/MediaRouter;->dispatchRouteRemoved(Landroid/media/MediaRouter$RouteInfo;)V

    #@53
    .line 573
    .end local v1           #count:I
    .end local v2           #found:Z
    .end local v3           #i:I
    .end local v4           #info:Landroid/media/MediaRouter$RouteInfo;
    .end local v5           #removingCat:Landroid/media/MediaRouter$RouteCategory;
    :cond_53
    return-void

    #@54
    .line 555
    .restart local v0       #cat:Landroid/media/MediaRouter$RouteCategory;
    .restart local v1       #count:I
    .restart local v2       #found:Z
    .restart local v3       #i:I
    .restart local v4       #info:Landroid/media/MediaRouter$RouteInfo;
    .restart local v5       #removingCat:Landroid/media/MediaRouter$RouteCategory;
    :cond_54
    add-int/lit8 v3, v3, 0x1

    #@56
    goto :goto_24
.end method

.method public removeRouteInt(Landroid/media/MediaRouter$RouteInfo;)V
    .registers 2
    .parameter "info"

    #@0
    .prologue
    .line 522
    invoke-static {p1}, Landroid/media/MediaRouter;->removeRoute(Landroid/media/MediaRouter$RouteInfo;)V

    #@3
    .line 523
    return-void
.end method

.method public removeUserRoute(Landroid/media/MediaRouter$UserRouteInfo;)V
    .registers 2
    .parameter "info"

    #@0
    .prologue
    .line 498
    invoke-static {p1}, Landroid/media/MediaRouter;->removeRoute(Landroid/media/MediaRouter$RouteInfo;)V

    #@3
    .line 499
    return-void
.end method

.method public selectRoute(ILandroid/media/MediaRouter$RouteInfo;)V
    .registers 4
    .parameter "types"
    .parameter "route"

    #@0
    .prologue
    .line 382
    const/high16 v0, 0x80

    #@2
    and-int/2addr p1, v0

    #@3
    .line 383
    invoke-static {p1, p2}, Landroid/media/MediaRouter;->selectRouteStatic(ILandroid/media/MediaRouter$RouteInfo;)V

    #@6
    .line 384
    return-void
.end method

.method public selectRouteInt(ILandroid/media/MediaRouter$RouteInfo;)V
    .registers 3
    .parameter "types"
    .parameter "route"

    #@0
    .prologue
    .line 390
    invoke-static {p1, p2}, Landroid/media/MediaRouter;->selectRouteStatic(ILandroid/media/MediaRouter$RouteInfo;)V

    #@3
    .line 391
    return-void
.end method
