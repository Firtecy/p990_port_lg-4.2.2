.class Landroid/media/MediaRouter$Static;
.super Ljava/lang/Object;
.source "MediaRouter.java"

# interfaces
.implements Landroid/hardware/display/DisplayManager$DisplayListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/MediaRouter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Static"
.end annotation


# instance fields
.field final mAudioRoutesObserver:Landroid/media/IAudioRoutesObserver$Stub;

.field final mAudioService:Landroid/media/IAudioService;

.field mBluetoothA2dpRoute:Landroid/media/MediaRouter$RouteInfo;

.field final mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Landroid/media/MediaRouter$CallbackInfo;",
            ">;"
        }
    .end annotation
.end field

.field final mCategories:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/media/MediaRouter$RouteCategory;",
            ">;"
        }
    .end annotation
.end field

.field final mCurAudioRoutesInfo:Landroid/media/AudioRoutesInfo;

.field mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

.field final mDisplayService:Landroid/hardware/display/DisplayManager;

.field final mHandler:Landroid/os/Handler;

.field mLastKnownWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

.field final mResources:Landroid/content/res/Resources;

.field final mRoutes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/media/MediaRouter$RouteInfo;",
            ">;"
        }
    .end annotation
.end field

.field mSelectedRoute:Landroid/media/MediaRouter$RouteInfo;

.field final mSystemCategory:Landroid/media/MediaRouter$RouteCategory;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .registers 7
    .parameter "appContext"

    #@0
    .prologue
    .line 87
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 60
    new-instance v1, Ljava/util/concurrent/CopyOnWriteArrayList;

    #@5
    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    #@8
    iput-object v1, p0, Landroid/media/MediaRouter$Static;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@a
    .line 63
    new-instance v1, Ljava/util/ArrayList;

    #@c
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@f
    iput-object v1, p0, Landroid/media/MediaRouter$Static;->mRoutes:Ljava/util/ArrayList;

    #@11
    .line 64
    new-instance v1, Ljava/util/ArrayList;

    #@13
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@16
    iput-object v1, p0, Landroid/media/MediaRouter$Static;->mCategories:Ljava/util/ArrayList;

    #@18
    .line 68
    new-instance v1, Landroid/media/AudioRoutesInfo;

    #@1a
    invoke-direct {v1}, Landroid/media/AudioRoutesInfo;-><init>()V

    #@1d
    iput-object v1, p0, Landroid/media/MediaRouter$Static;->mCurAudioRoutesInfo:Landroid/media/AudioRoutesInfo;

    #@1f
    .line 77
    new-instance v1, Landroid/media/MediaRouter$Static$1;

    #@21
    invoke-direct {v1, p0}, Landroid/media/MediaRouter$Static$1;-><init>(Landroid/media/MediaRouter$Static;)V

    #@24
    iput-object v1, p0, Landroid/media/MediaRouter$Static;->mAudioRoutesObserver:Landroid/media/IAudioRoutesObserver$Stub;

    #@26
    .line 88
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@29
    move-result-object v1

    #@2a
    iput-object v1, p0, Landroid/media/MediaRouter$Static;->mResources:Landroid/content/res/Resources;

    #@2c
    .line 89
    new-instance v1, Landroid/os/Handler;

    #@2e
    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    #@31
    move-result-object v2

    #@32
    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@35
    iput-object v1, p0, Landroid/media/MediaRouter$Static;->mHandler:Landroid/os/Handler;

    #@37
    .line 91
    const-string v1, "audio"

    #@39
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@3c
    move-result-object v0

    #@3d
    .line 92
    .local v0, b:Landroid/os/IBinder;
    invoke-static {v0}, Landroid/media/IAudioService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IAudioService;

    #@40
    move-result-object v1

    #@41
    iput-object v1, p0, Landroid/media/MediaRouter$Static;->mAudioService:Landroid/media/IAudioService;

    #@43
    .line 94
    const-string v1, "display"

    #@45
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@48
    move-result-object v1

    #@49
    check-cast v1, Landroid/hardware/display/DisplayManager;

    #@4b
    iput-object v1, p0, Landroid/media/MediaRouter$Static;->mDisplayService:Landroid/hardware/display/DisplayManager;

    #@4d
    .line 96
    new-instance v1, Landroid/media/MediaRouter$RouteCategory;

    #@4f
    const v2, 0x104053b

    #@52
    const/4 v3, 0x3

    #@53
    const/4 v4, 0x0

    #@54
    invoke-direct {v1, v2, v3, v4}, Landroid/media/MediaRouter$RouteCategory;-><init>(IIZ)V

    #@57
    iput-object v1, p0, Landroid/media/MediaRouter$Static;->mSystemCategory:Landroid/media/MediaRouter$RouteCategory;

    #@59
    .line 99
    iget-object v1, p0, Landroid/media/MediaRouter$Static;->mSystemCategory:Landroid/media/MediaRouter$RouteCategory;

    #@5b
    const/4 v2, 0x1

    #@5c
    iput-boolean v2, v1, Landroid/media/MediaRouter$RouteCategory;->mIsSystem:Z

    #@5e
    .line 100
    return-void
.end method

.method private updatePresentationDisplays(I)V
    .registers 8
    .parameter "changedDisplayId"

    #@0
    .prologue
    .line 218
    invoke-virtual {p0}, Landroid/media/MediaRouter$Static;->getAllPresentationDisplays()[Landroid/view/Display;

    #@3
    move-result-object v2

    #@4
    .line 219
    .local v2, displays:[Landroid/view/Display;
    iget-object v5, p0, Landroid/media/MediaRouter$Static;->mRoutes:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v0

    #@a
    .line 220
    .local v0, count:I
    const/4 v3, 0x0

    #@b
    .local v3, i:I
    :goto_b
    if-ge v3, v0, :cond_2d

    #@d
    .line 221
    iget-object v5, p0, Landroid/media/MediaRouter$Static;->mRoutes:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v4

    #@13
    check-cast v4, Landroid/media/MediaRouter$RouteInfo;

    #@15
    .line 222
    .local v4, info:Landroid/media/MediaRouter$RouteInfo;
    invoke-static {v4, v2}, Landroid/media/MediaRouter;->access$000(Landroid/media/MediaRouter$RouteInfo;[Landroid/view/Display;)Landroid/view/Display;

    #@18
    move-result-object v1

    #@19
    .line 223
    .local v1, display:Landroid/view/Display;
    iget-object v5, v4, Landroid/media/MediaRouter$RouteInfo;->mPresentationDisplay:Landroid/view/Display;

    #@1b
    if-ne v1, v5, :cond_25

    #@1d
    if-eqz v1, :cond_2a

    #@1f
    invoke-virtual {v1}, Landroid/view/Display;->getDisplayId()I

    #@22
    move-result v5

    #@23
    if-ne v5, p1, :cond_2a

    #@25
    .line 225
    :cond_25
    iput-object v1, v4, Landroid/media/MediaRouter$RouteInfo;->mPresentationDisplay:Landroid/view/Display;

    #@27
    .line 226
    invoke-static {v4}, Landroid/media/MediaRouter;->dispatchRoutePresentationDisplayChanged(Landroid/media/MediaRouter$RouteInfo;)V

    #@2a
    .line 220
    :cond_2a
    add-int/lit8 v3, v3, 0x1

    #@2c
    goto :goto_b

    #@2d
    .line 229
    .end local v1           #display:Landroid/view/Display;
    .end local v4           #info:Landroid/media/MediaRouter$RouteInfo;
    :cond_2d
    return-void
.end method


# virtual methods
.method public getAllPresentationDisplays()[Landroid/view/Display;
    .registers 3

    #@0
    .prologue
    .line 214
    iget-object v0, p0, Landroid/media/MediaRouter$Static;->mDisplayService:Landroid/hardware/display/DisplayManager;

    #@2
    const-string v1, "android.hardware.display.category.PRESENTATION"

    #@4
    invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManager;->getDisplays(Ljava/lang/String;)[Landroid/view/Display;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public onDisplayAdded(I)V
    .registers 2
    .parameter "displayId"

    #@0
    .prologue
    .line 200
    invoke-direct {p0, p1}, Landroid/media/MediaRouter$Static;->updatePresentationDisplays(I)V

    #@3
    .line 201
    return-void
.end method

.method public onDisplayChanged(I)V
    .registers 2
    .parameter "displayId"

    #@0
    .prologue
    .line 205
    invoke-direct {p0, p1}, Landroid/media/MediaRouter$Static;->updatePresentationDisplays(I)V

    #@3
    .line 206
    return-void
.end method

.method public onDisplayRemoved(I)V
    .registers 2
    .parameter "displayId"

    #@0
    .prologue
    .line 210
    invoke-direct {p0, p1}, Landroid/media/MediaRouter$Static;->updatePresentationDisplays(I)V

    #@3
    .line 211
    return-void
.end method

.method startMonitoringRoutes(Landroid/content/Context;)V
    .registers 6
    .parameter "appContext"

    #@0
    .prologue
    .line 104
    new-instance v1, Landroid/media/MediaRouter$RouteInfo;

    #@2
    iget-object v2, p0, Landroid/media/MediaRouter$Static;->mSystemCategory:Landroid/media/MediaRouter$RouteCategory;

    #@4
    invoke-direct {v1, v2}, Landroid/media/MediaRouter$RouteInfo;-><init>(Landroid/media/MediaRouter$RouteCategory;)V

    #@7
    iput-object v1, p0, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    #@9
    .line 105
    iget-object v1, p0, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    #@b
    const v2, 0x1040537

    #@e
    iput v2, v1, Landroid/media/MediaRouter$RouteInfo;->mNameResId:I

    #@10
    .line 106
    iget-object v1, p0, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    #@12
    const/4 v2, 0x3

    #@13
    iput v2, v1, Landroid/media/MediaRouter$RouteInfo;->mSupportedTypes:I

    #@15
    .line 107
    iget-object v1, p0, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    #@17
    iget-object v2, p0, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    #@19
    invoke-virtual {p0}, Landroid/media/MediaRouter$Static;->getAllPresentationDisplays()[Landroid/view/Display;

    #@1c
    move-result-object v3

    #@1d
    invoke-static {v2, v3}, Landroid/media/MediaRouter;->access$000(Landroid/media/MediaRouter$RouteInfo;[Landroid/view/Display;)Landroid/view/Display;

    #@20
    move-result-object v2

    #@21
    iput-object v2, v1, Landroid/media/MediaRouter$RouteInfo;->mPresentationDisplay:Landroid/view/Display;

    #@23
    .line 109
    iget-object v1, p0, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    #@25
    invoke-static {v1}, Landroid/media/MediaRouter;->addRouteStatic(Landroid/media/MediaRouter$RouteInfo;)V

    #@28
    .line 112
    iget-object v1, p0, Landroid/media/MediaRouter$Static;->mDisplayService:Landroid/hardware/display/DisplayManager;

    #@2a
    invoke-virtual {v1}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    #@2d
    move-result-object v1

    #@2e
    invoke-static {v1}, Landroid/media/MediaRouter;->updateWifiDisplayStatus(Landroid/hardware/display/WifiDisplayStatus;)V

    #@31
    .line 114
    new-instance v1, Landroid/media/MediaRouter$WifiDisplayStatusChangedReceiver;

    #@33
    invoke-direct {v1}, Landroid/media/MediaRouter$WifiDisplayStatusChangedReceiver;-><init>()V

    #@36
    new-instance v2, Landroid/content/IntentFilter;

    #@38
    const-string v3, "android.hardware.display.action.WIFI_DISPLAY_STATUS_CHANGED"

    #@3a
    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@3d
    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@40
    .line 116
    new-instance v1, Landroid/media/MediaRouter$VolumeChangeReceiver;

    #@42
    invoke-direct {v1}, Landroid/media/MediaRouter$VolumeChangeReceiver;-><init>()V

    #@45
    new-instance v2, Landroid/content/IntentFilter;

    #@47
    const-string v3, "android.media.VOLUME_CHANGED_ACTION"

    #@49
    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@4c
    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@4f
    .line 119
    iget-object v1, p0, Landroid/media/MediaRouter$Static;->mDisplayService:Landroid/hardware/display/DisplayManager;

    #@51
    iget-object v2, p0, Landroid/media/MediaRouter$Static;->mHandler:Landroid/os/Handler;

    #@53
    invoke-virtual {v1, p0, v2}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V

    #@56
    .line 121
    const/4 v0, 0x0

    #@57
    .line 123
    .local v0, newAudioRoutes:Landroid/media/AudioRoutesInfo;
    :try_start_57
    iget-object v1, p0, Landroid/media/MediaRouter$Static;->mAudioService:Landroid/media/IAudioService;

    #@59
    iget-object v2, p0, Landroid/media/MediaRouter$Static;->mAudioRoutesObserver:Landroid/media/IAudioRoutesObserver$Stub;

    #@5b
    invoke-interface {v1, v2}, Landroid/media/IAudioService;->startWatchingRoutes(Landroid/media/IAudioRoutesObserver;)Landroid/media/AudioRoutesInfo;
    :try_end_5e
    .catch Landroid/os/RemoteException; {:try_start_57 .. :try_end_5e} :catch_74

    #@5e
    move-result-object v0

    #@5f
    .line 126
    :goto_5f
    if-eqz v0, :cond_64

    #@61
    .line 130
    invoke-virtual {p0, v0}, Landroid/media/MediaRouter$Static;->updateAudioRoutes(Landroid/media/AudioRoutesInfo;)V

    #@64
    .line 135
    :cond_64
    iget-object v1, p0, Landroid/media/MediaRouter$Static;->mSelectedRoute:Landroid/media/MediaRouter$RouteInfo;

    #@66
    if-nez v1, :cond_73

    #@68
    .line 136
    iget-object v1, p0, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    #@6a
    invoke-virtual {v1}, Landroid/media/MediaRouter$RouteInfo;->getSupportedTypes()I

    #@6d
    move-result v1

    #@6e
    iget-object v2, p0, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    #@70
    invoke-static {v1, v2}, Landroid/media/MediaRouter;->selectRouteStatic(ILandroid/media/MediaRouter$RouteInfo;)V

    #@73
    .line 138
    :cond_73
    return-void

    #@74
    .line 124
    :catch_74
    move-exception v1

    #@75
    goto :goto_5f
.end method

.method updateAudioRoutes(Landroid/media/AudioRoutesInfo;)V
    .registers 10
    .parameter "newRoutes"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 141
    iget v5, p1, Landroid/media/AudioRoutesInfo;->mMainType:I

    #@3
    iget-object v6, p0, Landroid/media/MediaRouter$Static;->mCurAudioRoutesInfo:Landroid/media/AudioRoutesInfo;

    #@5
    iget v6, v6, Landroid/media/AudioRoutesInfo;->mMainType:I

    #@7
    if-eq v5, v6, :cond_2b

    #@9
    .line 142
    iget-object v5, p0, Landroid/media/MediaRouter$Static;->mCurAudioRoutesInfo:Landroid/media/AudioRoutesInfo;

    #@b
    iget v6, p1, Landroid/media/AudioRoutesInfo;->mMainType:I

    #@d
    iput v6, v5, Landroid/media/AudioRoutesInfo;->mMainType:I

    #@f
    .line 144
    iget v5, p1, Landroid/media/AudioRoutesInfo;->mMainType:I

    #@11
    and-int/lit8 v5, v5, 0x2

    #@13
    if-nez v5, :cond_1b

    #@15
    iget v5, p1, Landroid/media/AudioRoutesInfo;->mMainType:I

    #@17
    and-int/lit8 v5, v5, 0x1

    #@19
    if-eqz v5, :cond_83

    #@1b
    .line 146
    :cond_1b
    const v4, 0x1040538

    #@1e
    .line 154
    .local v4, name:I
    :goto_1e
    sget-object v5, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@20
    iget-object v5, v5, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    #@22
    iput v4, v5, Landroid/media/MediaRouter$RouteInfo;->mNameResId:I

    #@24
    .line 155
    sget-object v5, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@26
    iget-object v5, v5, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    #@28
    invoke-static {v5}, Landroid/media/MediaRouter;->dispatchRouteChanged(Landroid/media/MediaRouter$RouteInfo;)V

    #@2b
    .line 158
    .end local v4           #name:I
    :cond_2b
    iget-object v5, p0, Landroid/media/MediaRouter$Static;->mCurAudioRoutesInfo:Landroid/media/AudioRoutesInfo;

    #@2d
    iget v3, v5, Landroid/media/AudioRoutesInfo;->mMainType:I

    #@2f
    .line 162
    .local v3, mainType:I
    :try_start_2f
    iget-object v5, p0, Landroid/media/MediaRouter$Static;->mAudioService:Landroid/media/IAudioService;

    #@31
    invoke-interface {v5}, Landroid/media/IAudioService;->isBluetoothA2dpOn()Z
    :try_end_34
    .catch Landroid/os/RemoteException; {:try_start_2f .. :try_end_34} :catch_9b

    #@34
    move-result v0

    #@35
    .line 168
    .local v0, a2dpEnabled:Z
    :goto_35
    iget-object v5, p1, Landroid/media/AudioRoutesInfo;->mBluetoothName:Ljava/lang/CharSequence;

    #@37
    iget-object v6, p0, Landroid/media/MediaRouter$Static;->mCurAudioRoutesInfo:Landroid/media/AudioRoutesInfo;

    #@39
    iget-object v6, v6, Landroid/media/AudioRoutesInfo;->mBluetoothName:Ljava/lang/CharSequence;

    #@3b
    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@3e
    move-result v5

    #@3f
    if-nez v5, :cond_6f

    #@41
    .line 169
    iget-object v5, p0, Landroid/media/MediaRouter$Static;->mCurAudioRoutesInfo:Landroid/media/AudioRoutesInfo;

    #@43
    iget-object v6, p1, Landroid/media/AudioRoutesInfo;->mBluetoothName:Ljava/lang/CharSequence;

    #@45
    iput-object v6, v5, Landroid/media/AudioRoutesInfo;->mBluetoothName:Ljava/lang/CharSequence;

    #@47
    .line 170
    iget-object v5, p0, Landroid/media/MediaRouter$Static;->mCurAudioRoutesInfo:Landroid/media/AudioRoutesInfo;

    #@49
    iget-object v5, v5, Landroid/media/AudioRoutesInfo;->mBluetoothName:Ljava/lang/CharSequence;

    #@4b
    if-eqz v5, :cond_b7

    #@4d
    .line 171
    sget-object v5, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@4f
    iget-object v5, v5, Landroid/media/MediaRouter$Static;->mBluetoothA2dpRoute:Landroid/media/MediaRouter$RouteInfo;

    #@51
    if-nez v5, :cond_a5

    #@53
    .line 172
    new-instance v2, Landroid/media/MediaRouter$RouteInfo;

    #@55
    sget-object v5, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@57
    iget-object v5, v5, Landroid/media/MediaRouter$Static;->mSystemCategory:Landroid/media/MediaRouter$RouteCategory;

    #@59
    invoke-direct {v2, v5}, Landroid/media/MediaRouter$RouteInfo;-><init>(Landroid/media/MediaRouter$RouteCategory;)V

    #@5c
    .line 173
    .local v2, info:Landroid/media/MediaRouter$RouteInfo;
    iget-object v5, p0, Landroid/media/MediaRouter$Static;->mCurAudioRoutesInfo:Landroid/media/AudioRoutesInfo;

    #@5e
    iget-object v5, v5, Landroid/media/AudioRoutesInfo;->mBluetoothName:Ljava/lang/CharSequence;

    #@60
    iput-object v5, v2, Landroid/media/MediaRouter$RouteInfo;->mName:Ljava/lang/CharSequence;

    #@62
    .line 174
    iput v7, v2, Landroid/media/MediaRouter$RouteInfo;->mSupportedTypes:I

    #@64
    .line 175
    sget-object v5, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@66
    iput-object v2, v5, Landroid/media/MediaRouter$Static;->mBluetoothA2dpRoute:Landroid/media/MediaRouter$RouteInfo;

    #@68
    .line 176
    sget-object v5, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@6a
    iget-object v5, v5, Landroid/media/MediaRouter$Static;->mBluetoothA2dpRoute:Landroid/media/MediaRouter$RouteInfo;

    #@6c
    invoke-static {v5}, Landroid/media/MediaRouter;->addRouteStatic(Landroid/media/MediaRouter$RouteInfo;)V

    #@6f
    .line 187
    .end local v2           #info:Landroid/media/MediaRouter$RouteInfo;
    :cond_6f
    :goto_6f
    iget-object v5, p0, Landroid/media/MediaRouter$Static;->mBluetoothA2dpRoute:Landroid/media/MediaRouter$RouteInfo;

    #@71
    if-eqz v5, :cond_82

    #@73
    .line 188
    if-eqz v3, :cond_ca

    #@75
    iget-object v5, p0, Landroid/media/MediaRouter$Static;->mSelectedRoute:Landroid/media/MediaRouter$RouteInfo;

    #@77
    iget-object v6, p0, Landroid/media/MediaRouter$Static;->mBluetoothA2dpRoute:Landroid/media/MediaRouter$RouteInfo;

    #@79
    if-ne v5, v6, :cond_ca

    #@7b
    if-nez v0, :cond_ca

    #@7d
    .line 190
    iget-object v5, p0, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    #@7f
    invoke-static {v7, v5}, Landroid/media/MediaRouter;->selectRouteStatic(ILandroid/media/MediaRouter$RouteInfo;)V

    #@82
    .line 196
    :cond_82
    :goto_82
    return-void

    #@83
    .line 147
    .end local v0           #a2dpEnabled:Z
    .end local v3           #mainType:I
    :cond_83
    iget v5, p1, Landroid/media/AudioRoutesInfo;->mMainType:I

    #@85
    and-int/lit8 v5, v5, 0x4

    #@87
    if-eqz v5, :cond_8d

    #@89
    .line 148
    const v4, 0x1040539

    #@8c
    .restart local v4       #name:I
    goto :goto_1e

    #@8d
    .line 149
    .end local v4           #name:I
    :cond_8d
    iget v5, p1, Landroid/media/AudioRoutesInfo;->mMainType:I

    #@8f
    and-int/lit8 v5, v5, 0x8

    #@91
    if-eqz v5, :cond_97

    #@93
    .line 150
    const v4, 0x104053a

    #@96
    .restart local v4       #name:I
    goto :goto_1e

    #@97
    .line 152
    .end local v4           #name:I
    :cond_97
    const v4, 0x1040537

    #@9a
    .restart local v4       #name:I
    goto :goto_1e

    #@9b
    .line 163
    .end local v4           #name:I
    .restart local v3       #mainType:I
    :catch_9b
    move-exception v1

    #@9c
    .line 164
    .local v1, e:Landroid/os/RemoteException;
    const-string v5, "MediaRouter"

    #@9e
    const-string v6, "Error querying Bluetooth A2DP state"

    #@a0
    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@a3
    .line 165
    const/4 v0, 0x0

    #@a4
    .restart local v0       #a2dpEnabled:Z
    goto :goto_35

    #@a5
    .line 178
    .end local v1           #e:Landroid/os/RemoteException;
    :cond_a5
    sget-object v5, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@a7
    iget-object v5, v5, Landroid/media/MediaRouter$Static;->mBluetoothA2dpRoute:Landroid/media/MediaRouter$RouteInfo;

    #@a9
    iget-object v6, p0, Landroid/media/MediaRouter$Static;->mCurAudioRoutesInfo:Landroid/media/AudioRoutesInfo;

    #@ab
    iget-object v6, v6, Landroid/media/AudioRoutesInfo;->mBluetoothName:Ljava/lang/CharSequence;

    #@ad
    iput-object v6, v5, Landroid/media/MediaRouter$RouteInfo;->mName:Ljava/lang/CharSequence;

    #@af
    .line 179
    sget-object v5, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@b1
    iget-object v5, v5, Landroid/media/MediaRouter$Static;->mBluetoothA2dpRoute:Landroid/media/MediaRouter$RouteInfo;

    #@b3
    invoke-static {v5}, Landroid/media/MediaRouter;->dispatchRouteChanged(Landroid/media/MediaRouter$RouteInfo;)V

    #@b6
    goto :goto_6f

    #@b7
    .line 181
    :cond_b7
    sget-object v5, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@b9
    iget-object v5, v5, Landroid/media/MediaRouter$Static;->mBluetoothA2dpRoute:Landroid/media/MediaRouter$RouteInfo;

    #@bb
    if-eqz v5, :cond_6f

    #@bd
    .line 182
    sget-object v5, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@bf
    iget-object v5, v5, Landroid/media/MediaRouter$Static;->mBluetoothA2dpRoute:Landroid/media/MediaRouter$RouteInfo;

    #@c1
    invoke-static {v5}, Landroid/media/MediaRouter;->removeRoute(Landroid/media/MediaRouter$RouteInfo;)V

    #@c4
    .line 183
    sget-object v5, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@c6
    const/4 v6, 0x0

    #@c7
    iput-object v6, v5, Landroid/media/MediaRouter$Static;->mBluetoothA2dpRoute:Landroid/media/MediaRouter$RouteInfo;

    #@c9
    goto :goto_6f

    #@ca
    .line 191
    :cond_ca
    iget-object v5, p0, Landroid/media/MediaRouter$Static;->mSelectedRoute:Landroid/media/MediaRouter$RouteInfo;

    #@cc
    iget-object v6, p0, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    #@ce
    if-eq v5, v6, :cond_d4

    #@d0
    iget-object v5, p0, Landroid/media/MediaRouter$Static;->mSelectedRoute:Landroid/media/MediaRouter$RouteInfo;

    #@d2
    if-nez v5, :cond_82

    #@d4
    :cond_d4
    if-eqz v0, :cond_82

    #@d6
    .line 193
    iget-object v5, p0, Landroid/media/MediaRouter$Static;->mBluetoothA2dpRoute:Landroid/media/MediaRouter$RouteInfo;

    #@d8
    invoke-static {v7, v5}, Landroid/media/MediaRouter;->selectRouteStatic(ILandroid/media/MediaRouter$RouteInfo;)V

    #@db
    goto :goto_82
.end method
