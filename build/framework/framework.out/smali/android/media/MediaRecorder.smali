.class public Landroid/media/MediaRecorder;
.super Ljava/lang/Object;
.source "MediaRecorder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/MediaRecorder$EventHandler;,
        Landroid/media/MediaRecorder$OnInfoListener;,
        Landroid/media/MediaRecorder$OnErrorListener;,
        Landroid/media/MediaRecorder$VideoEncoder;,
        Landroid/media/MediaRecorder$AudioEncoder;,
        Landroid/media/MediaRecorder$OutputFormat;,
        Landroid/media/MediaRecorder$VideoSource;,
        Landroid/media/MediaRecorder$AudioSource;
    }
.end annotation


# static fields
.field public static final MEDIA_ERROR_SERVER_DIED:I = 0x64

.field public static final MEDIA_RECORDER_ERROR_UNKNOWN:I = 0x1

.field public static final MEDIA_RECORDER_INFO_MAX_DURATION_REACHED:I = 0x320

.field public static final MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED:I = 0x321

.field public static final MEDIA_RECORDER_INFO_UNKNOWN:I = 0x1

.field public static final MEDIA_RECORDER_TRACK_INFO_COMPLETION_STATUS:I = 0x3e8

.field public static final MEDIA_RECORDER_TRACK_INFO_DATA_KBYTES:I = 0x3f1

.field public static final MEDIA_RECORDER_TRACK_INFO_DURATION_MS:I = 0x3eb

.field public static final MEDIA_RECORDER_TRACK_INFO_ENCODED_FRAMES:I = 0x3ed

.field public static final MEDIA_RECORDER_TRACK_INFO_INITIAL_DELAY_MS:I = 0x3ef

.field public static final MEDIA_RECORDER_TRACK_INFO_LIST_END:I = 0x7d0

.field public static final MEDIA_RECORDER_TRACK_INFO_LIST_START:I = 0x3e8

.field public static final MEDIA_RECORDER_TRACK_INFO_MAX_CHUNK_DUR_MS:I = 0x3ec

.field public static final MEDIA_RECORDER_TRACK_INFO_PROGRESS_IN_TIME:I = 0x3e9

.field public static final MEDIA_RECORDER_TRACK_INFO_START_OFFSET_MS:I = 0x3f0

.field public static final MEDIA_RECORDER_TRACK_INFO_TYPE:I = 0x3ea

.field public static final MEDIA_RECORDER_TRACK_INTER_CHUNK_TIME_MS:I = 0x3ee

.field private static final TAG:Ljava/lang/String; = "MediaRecorder"


# instance fields
.field private mAudioManager:Landroid/media/AudioManager;

.field private mEventHandler:Landroid/media/MediaRecorder$EventHandler;

.field private mFd:Ljava/io/FileDescriptor;

.field private mNativeContext:I

.field private mOnErrorListener:Landroid/media/MediaRecorder$OnErrorListener;

.field private mOnInfoListener:Landroid/media/MediaRecorder$OnInfoListener;

.field private mPath:Ljava/lang/String;

.field private mSurface:Landroid/view/Surface;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 82
    const-string/jumbo v0, "media_jni"

    #@3
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@6
    .line 83
    invoke-static {}, Landroid/media/MediaRecorder;->native_init()V

    #@9
    .line 84
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 104
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 107
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@6
    move-result-object v0

    #@7
    .local v0, looper:Landroid/os/Looper;
    if-eqz v0, :cond_19

    #@9
    .line 108
    new-instance v1, Landroid/media/MediaRecorder$EventHandler;

    #@b
    invoke-direct {v1, p0, p0, v0}, Landroid/media/MediaRecorder$EventHandler;-><init>(Landroid/media/MediaRecorder;Landroid/media/MediaRecorder;Landroid/os/Looper;)V

    #@e
    iput-object v1, p0, Landroid/media/MediaRecorder;->mEventHandler:Landroid/media/MediaRecorder$EventHandler;

    #@10
    .line 118
    :goto_10
    new-instance v1, Ljava/lang/ref/WeakReference;

    #@12
    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@15
    invoke-direct {p0, v1}, Landroid/media/MediaRecorder;->native_setup(Ljava/lang/Object;)V

    #@18
    .line 119
    return-void

    #@19
    .line 109
    :cond_19
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    #@1c
    move-result-object v0

    #@1d
    if-eqz v0, :cond_27

    #@1f
    .line 110
    new-instance v1, Landroid/media/MediaRecorder$EventHandler;

    #@21
    invoke-direct {v1, p0, p0, v0}, Landroid/media/MediaRecorder$EventHandler;-><init>(Landroid/media/MediaRecorder;Landroid/media/MediaRecorder;Landroid/os/Looper;)V

    #@24
    iput-object v1, p0, Landroid/media/MediaRecorder;->mEventHandler:Landroid/media/MediaRecorder$EventHandler;

    #@26
    goto :goto_10

    #@27
    .line 112
    :cond_27
    const/4 v1, 0x0

    #@28
    iput-object v1, p0, Landroid/media/MediaRecorder;->mEventHandler:Landroid/media/MediaRecorder$EventHandler;

    #@2a
    goto :goto_10
.end method

.method private native _prepare()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method private native _setOutputFile(Ljava/io/FileDescriptor;JJ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method static synthetic access$000(Landroid/media/MediaRecorder;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 79
    iget v0, p0, Landroid/media/MediaRecorder;->mNativeContext:I

    #@2
    return v0
.end method

.method static synthetic access$100(Landroid/media/MediaRecorder;)Landroid/media/MediaRecorder$OnErrorListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 79
    iget-object v0, p0, Landroid/media/MediaRecorder;->mOnErrorListener:Landroid/media/MediaRecorder$OnErrorListener;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/media/MediaRecorder;)Landroid/media/MediaRecorder$OnInfoListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 79
    iget-object v0, p0, Landroid/media/MediaRecorder;->mOnInfoListener:Landroid/media/MediaRecorder$OnInfoListener;

    #@2
    return-object v0
.end method

.method public static final getAudioSourceMax()I
    .registers 1

    #@0
    .prologue
    .line 333
    const/16 v0, 0xa

    #@2
    return v0
.end method

.method private native native_audiozoom()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private final native native_finalize()V
.end method

.method private static final native native_init()V
.end method

.method private native native_pause()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native native_reset()V
.end method

.method private native native_resume()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native native_setAudioZoomExceptionCase()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native native_setRecordAngle(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native native_setRecordZoomEnable(II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private final native native_setup(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native start()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native stop()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .registers 8
    .parameter "mediarecorder_ref"
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"
    .parameter "obj"

    #@0
    .prologue
    .line 1065
    check-cast p0, Ljava/lang/ref/WeakReference;

    #@2
    .end local p0
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Landroid/media/MediaRecorder;

    #@8
    .line 1066
    .local v1, mr:Landroid/media/MediaRecorder;
    if-nez v1, :cond_b

    #@a
    .line 1074
    :cond_a
    :goto_a
    return-void

    #@b
    .line 1070
    :cond_b
    iget-object v2, v1, Landroid/media/MediaRecorder;->mEventHandler:Landroid/media/MediaRecorder$EventHandler;

    #@d
    if-eqz v2, :cond_a

    #@f
    .line 1071
    iget-object v2, v1, Landroid/media/MediaRecorder;->mEventHandler:Landroid/media/MediaRecorder$EventHandler;

    #@11
    invoke-virtual {v2, p1, p2, p3, p4}, Landroid/media/MediaRecorder$EventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@14
    move-result-object v0

    #@15
    .line 1072
    .local v0, m:Landroid/os/Message;
    iget-object v2, v1, Landroid/media/MediaRecorder;->mEventHandler:Landroid/media/MediaRecorder$EventHandler;

    #@17
    invoke-virtual {v2, v0}, Landroid/media/MediaRecorder$EventHandler;->sendMessage(Landroid/os/Message;)Z

    #@1a
    goto :goto_a
.end method

.method private native setParameter(Ljava/lang/String;)V
.end method


# virtual methods
.method protected finalize()V
    .registers 1

    #@0
    .prologue
    .line 1110
    invoke-direct {p0}, Landroid/media/MediaRecorder;->native_finalize()V

    #@3
    return-void
.end method

.method public native getMaxAmplitude()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public native isRecording()Z
.end method

.method public pause()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 821
    const-string v0, "MediaRecorder"

    #@2
    const-string/jumbo v1, "mediarecorder pause"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 822
    invoke-direct {p0}, Landroid/media/MediaRecorder;->native_pause()V

    #@b
    .line 823
    return-void
.end method

.method public prepare()V
    .registers 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const-wide/16 v2, 0x0

    #@2
    .line 695
    iget-object v0, p0, Landroid/media/MediaRecorder;->mPath:Ljava/lang/String;

    #@4
    if-eqz v0, :cond_5b

    #@6
    .line 696
    const-string v0, "MediaRecorder"

    #@8
    const-string v1, "MediaRecorder-prepare_FileOutputStream__start"

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 697
    new-instance v6, Ljava/io/FileOutputStream;

    #@f
    iget-object v0, p0, Landroid/media/MediaRecorder;->mPath:Ljava/lang/String;

    #@11
    invoke-direct {v6, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    #@14
    .line 698
    .local v6, fos:Ljava/io/FileOutputStream;
    const-string v0, "MediaRecorder"

    #@16
    new-instance v1, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v2, "MediaRecorder-prepare_-mFilePath"

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    iget-object v2, p0, Landroid/media/MediaRecorder;->mPath:Ljava/lang/String;

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 699
    const-string v0, "MediaRecorder"

    #@30
    const-string v1, "MediaRecorder-prepare_FileOutputStream_end"

    #@32
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 701
    :try_start_35
    const-string v0, "MediaRecorder"

    #@37
    const-string v1, "MediaRecorder-_setOutputFile__start"

    #@39
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 702
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    #@3f
    move-result-object v1

    #@40
    const-wide/16 v2, 0x0

    #@42
    const-wide/16 v4, 0x0

    #@44
    move-object v0, p0

    #@45
    invoke-direct/range {v0 .. v5}, Landroid/media/MediaRecorder;->_setOutputFile(Ljava/io/FileDescriptor;JJ)V

    #@48
    .line 703
    const-string v0, "MediaRecorder"

    #@4a
    const-string v1, "MediaRecorder-_setOutputFile__end"

    #@4c
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4f
    .catchall {:try_start_35 .. :try_end_4f} :catchall_56

    #@4f
    .line 705
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V

    #@52
    .line 713
    .end local v6           #fos:Ljava/io/FileOutputStream;
    :goto_52
    invoke-direct {p0}, Landroid/media/MediaRecorder;->_prepare()V

    #@55
    .line 714
    return-void

    #@56
    .line 705
    .restart local v6       #fos:Ljava/io/FileOutputStream;
    :catchall_56
    move-exception v0

    #@57
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V

    #@5a
    throw v0

    #@5b
    .line 707
    .end local v6           #fos:Ljava/io/FileOutputStream;
    :cond_5b
    iget-object v0, p0, Landroid/media/MediaRecorder;->mFd:Ljava/io/FileDescriptor;

    #@5d
    if-eqz v0, :cond_67

    #@5f
    .line 708
    iget-object v1, p0, Landroid/media/MediaRecorder;->mFd:Ljava/io/FileDescriptor;

    #@61
    move-object v0, p0

    #@62
    move-wide v4, v2

    #@63
    invoke-direct/range {v0 .. v5}, Landroid/media/MediaRecorder;->_setOutputFile(Ljava/io/FileDescriptor;JJ)V

    #@66
    goto :goto_52

    #@67
    .line 710
    :cond_67
    new-instance v0, Ljava/io/IOException;

    #@69
    const-string v1, "No valid output file"

    #@6b
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@6e
    throw v0
.end method

.method public native release()V
.end method

.method public reset()V
    .registers 3

    #@0
    .prologue
    .line 806
    invoke-direct {p0}, Landroid/media/MediaRecorder;->native_reset()V

    #@3
    .line 810
    iget-object v0, p0, Landroid/media/MediaRecorder;->mEventHandler:Landroid/media/MediaRecorder$EventHandler;

    #@5
    if-eqz v0, :cond_d

    #@7
    .line 811
    iget-object v0, p0, Landroid/media/MediaRecorder;->mEventHandler:Landroid/media/MediaRecorder$EventHandler;

    #@9
    const/4 v1, 0x0

    #@a
    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder$EventHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    #@d
    .line 814
    :cond_d
    return-void
.end method

.method public resume()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 826
    const-string v0, "MediaRecorder"

    #@2
    const-string/jumbo v1, "mediarecorder resume"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 827
    invoke-direct {p0}, Landroid/media/MediaRecorder;->native_resume()V

    #@b
    .line 828
    return-void
.end method

.method public setAudioChannels(I)V
    .registers 4
    .parameter "numChannels"

    #@0
    .prologue
    .line 589
    if-gtz p1, :cond_a

    #@2
    .line 590
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "Number of channels is not positive"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 592
    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v1, "audio-param-number-of-channels="

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    invoke-direct {p0, v0}, Landroid/media/MediaRecorder;->setParameter(Ljava/lang/String;)V

    #@20
    .line 593
    return-void
.end method

.method public native setAudioEncoder(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public setAudioEncodingBitRate(I)V
    .registers 4
    .parameter "bitRate"

    #@0
    .prologue
    .line 605
    if-gtz p1, :cond_a

    #@2
    .line 606
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "Audio encoding bit rate is not positive"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 608
    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v1, "audio-param-encoding-bitrate="

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    invoke-direct {p0, v0}, Landroid/media/MediaRecorder;->setParameter(Ljava/lang/String;)V

    #@20
    .line 609
    return-void
.end method

.method public setAudioSamplingRate(I)V
    .registers 4
    .parameter "samplingRate"

    #@0
    .prologue
    .line 574
    if-gtz p1, :cond_a

    #@2
    .line 575
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "Audio sampling rate is not positive"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 577
    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v1, "audio-param-sampling-rate="

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    invoke-direct {p0, v0}, Landroid/media/MediaRecorder;->setParameter(Ljava/lang/String;)V

    #@20
    .line 578
    return-void
.end method

.method public native setAudioSource(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public setAudioZoomExceptionCase()V
    .registers 3

    #@0
    .prologue
    .line 1131
    const-string v0, "MediaRecorder"

    #@2
    const-string v1, "MediaRecorder setAudioZoomExceptionCase"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1132
    invoke-direct {p0}, Landroid/media/MediaRecorder;->native_setAudioZoomExceptionCase()V

    #@a
    .line 1133
    return-void
.end method

.method public setAudioZooming()V
    .registers 3

    #@0
    .prologue
    .line 1121
    const-string v0, "MediaRecorder"

    #@2
    const-string v1, "MediaRecorder setAudioZooming"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1122
    invoke-direct {p0}, Landroid/media/MediaRecorder;->native_audiozoom()V

    #@a
    .line 1123
    return-void
.end method

.method public setAuxiliaryOutputFile(Ljava/io/FileDescriptor;)V
    .registers 4
    .parameter "fd"

    #@0
    .prologue
    .line 635
    const-string v0, "MediaRecorder"

    #@2
    const-string/jumbo v1, "setAuxiliaryOutputFile(FileDescriptor) is no longer supported."

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 636
    return-void
.end method

.method public setAuxiliaryOutputFile(Ljava/lang/String;)V
    .registers 4
    .parameter "path"

    #@0
    .prologue
    .line 646
    const-string v0, "MediaRecorder"

    #@2
    const-string/jumbo v1, "setAuxiliaryOutputFile(String) is no longer supported."

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 647
    return-void
.end method

.method public native setCamera(Landroid/hardware/Camera;)V
.end method

.method public setCaptureRate(D)V
    .registers 8
    .parameter "fps"

    #@0
    .prologue
    .line 395
    const-string/jumbo v3, "time-lapse-enable=1"

    #@3
    invoke-direct {p0, v3}, Landroid/media/MediaRecorder;->setParameter(Ljava/lang/String;)V

    #@6
    .line 397
    const-wide/high16 v3, 0x3ff0

    #@8
    div-double v0, v3, p1

    #@a
    .line 398
    .local v0, timeBetweenFrameCapture:D
    const-wide v3, 0x408f400000000000L

    #@f
    mul-double/2addr v3, v0

    #@10
    double-to-int v2, v3

    #@11
    .line 399
    .local v2, timeBetweenFrameCaptureMs:I
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string/jumbo v4, "time-between-time-lapse-frame-capture="

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    invoke-direct {p0, v3}, Landroid/media/MediaRecorder;->setParameter(Ljava/lang/String;)V

    #@28
    .line 400
    return-void
.end method

.method public setLocation(FF)V
    .registers 11
    .parameter "latitude"
    .parameter "longitude"

    #@0
    .prologue
    const v7, 0x461c4000

    #@3
    const-wide/high16 v5, 0x3fe0

    #@5
    .line 444
    mul-float v3, p1, v7

    #@7
    float-to-double v3, v3

    #@8
    add-double/2addr v3, v5

    #@9
    double-to-int v0, v3

    #@a
    .line 445
    .local v0, latitudex10000:I
    mul-float v3, p2, v7

    #@c
    float-to-double v3, v3

    #@d
    add-double/2addr v3, v5

    #@e
    double-to-int v1, v3

    #@f
    .line 447
    .local v1, longitudex10000:I
    const v3, 0xdbba0

    #@12
    if-gt v0, v3, :cond_19

    #@14
    const v3, -0xdbba0

    #@17
    if-ge v0, v3, :cond_38

    #@19
    .line 448
    :cond_19
    new-instance v3, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v4, "Latitude: "

    #@20
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    const-string v4, " out of range."

    #@2a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v2

    #@32
    .line 449
    .local v2, msg:Ljava/lang/String;
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@34
    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@37
    throw v3

    #@38
    .line 451
    .end local v2           #msg:Ljava/lang/String;
    :cond_38
    const v3, 0x1b7740

    #@3b
    if-gt v1, v3, :cond_42

    #@3d
    const v3, -0x1b7740

    #@40
    if-ge v1, v3, :cond_61

    #@42
    .line 452
    :cond_42
    new-instance v3, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v4, "Longitude: "

    #@49
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v3

    #@4d
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@50
    move-result-object v3

    #@51
    const-string v4, " out of range"

    #@53
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v3

    #@57
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v2

    #@5b
    .line 453
    .restart local v2       #msg:Ljava/lang/String;
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@5d
    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@60
    throw v3

    #@61
    .line 456
    .end local v2           #msg:Ljava/lang/String;
    :cond_61
    new-instance v3, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    const-string/jumbo v4, "param-geotag-latitude="

    #@69
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v3

    #@6d
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@70
    move-result-object v3

    #@71
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v3

    #@75
    invoke-direct {p0, v3}, Landroid/media/MediaRecorder;->setParameter(Ljava/lang/String;)V

    #@78
    .line 457
    new-instance v3, Ljava/lang/StringBuilder;

    #@7a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7d
    const-string/jumbo v4, "param-geotag-longitude="

    #@80
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v3

    #@84
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@87
    move-result-object v3

    #@88
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8b
    move-result-object v3

    #@8c
    invoke-direct {p0, v3}, Landroid/media/MediaRecorder;->setParameter(Ljava/lang/String;)V

    #@8f
    .line 458
    return-void
.end method

.method public native setMaxDuration(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation
.end method

.method public native setMaxFileSize(J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation
.end method

.method public setOnErrorListener(Landroid/media/MediaRecorder$OnErrorListener;)V
    .registers 2
    .parameter "l"

    #@0
    .prologue
    .line 883
    iput-object p1, p0, Landroid/media/MediaRecorder;->mOnErrorListener:Landroid/media/MediaRecorder$OnErrorListener;

    #@2
    .line 884
    return-void
.end method

.method public setOnInfoListener(Landroid/media/MediaRecorder$OnInfoListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 1000
    iput-object p1, p0, Landroid/media/MediaRecorder;->mOnInfoListener:Landroid/media/MediaRecorder$OnInfoListener;

    #@2
    .line 1001
    return-void
.end method

.method public setOrientationHint(I)V
    .registers 5
    .parameter "degrees"

    #@0
    .prologue
    .line 418
    if-eqz p1, :cond_27

    #@2
    const/16 v0, 0x5a

    #@4
    if-eq p1, v0, :cond_27

    #@6
    const/16 v0, 0xb4

    #@8
    if-eq p1, v0, :cond_27

    #@a
    const/16 v0, 0x10e

    #@c
    if-eq p1, v0, :cond_27

    #@e
    .line 422
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@10
    new-instance v1, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v2, "Unsupported angle: "

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@26
    throw v0

    #@27
    .line 424
    :cond_27
    new-instance v0, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string/jumbo v1, "video-param-rotation-angle-degrees="

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v0

    #@33
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v0

    #@37
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v0

    #@3b
    invoke-direct {p0, v0}, Landroid/media/MediaRecorder;->setParameter(Ljava/lang/String;)V

    #@3e
    .line 425
    return-void
.end method

.method public setOutputFile(Ljava/io/FileDescriptor;)V
    .registers 4
    .parameter "fd"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 659
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Landroid/media/MediaRecorder;->mPath:Ljava/lang/String;

    #@3
    .line 660
    iput-object p1, p0, Landroid/media/MediaRecorder;->mFd:Ljava/io/FileDescriptor;

    #@5
    .line 661
    const-string v0, "MediaRecorder"

    #@7
    const-string v1, "MediaRecorder-setOutputFile111_"

    #@9
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 662
    return-void
.end method

.method public setOutputFile(Ljava/lang/String;)V
    .registers 5
    .parameter "path"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 674
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Landroid/media/MediaRecorder;->mFd:Ljava/io/FileDescriptor;

    #@3
    .line 675
    iput-object p1, p0, Landroid/media/MediaRecorder;->mPath:Ljava/lang/String;

    #@5
    .line 676
    const-string v0, "MediaRecorder"

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "MediaRecorder-setOutputFile111_"

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    iget-object v2, p0, Landroid/media/MediaRecorder;->mPath:Ljava/lang/String;

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 677
    return-void
.end method

.method public native setOutputFormat(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public setPreviewDisplay(Landroid/view/Surface;)V
    .registers 2
    .parameter "sv"

    #@0
    .prologue
    .line 145
    iput-object p1, p0, Landroid/media/MediaRecorder;->mSurface:Landroid/view/Surface;

    #@2
    .line 146
    return-void
.end method

.method public setProfile(Landroid/media/CamcorderProfile;)V
    .registers 4
    .parameter "profile"

    #@0
    .prologue
    .line 360
    iget v0, p1, Landroid/media/CamcorderProfile;->fileFormat:I

    #@2
    invoke-virtual {p0, v0}, Landroid/media/MediaRecorder;->setOutputFormat(I)V

    #@5
    .line 361
    iget v0, p1, Landroid/media/CamcorderProfile;->videoFrameRate:I

    #@7
    invoke-virtual {p0, v0}, Landroid/media/MediaRecorder;->setVideoFrameRate(I)V

    #@a
    .line 362
    iget v0, p1, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    #@c
    iget v1, p1, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    #@e
    invoke-virtual {p0, v0, v1}, Landroid/media/MediaRecorder;->setVideoSize(II)V

    #@11
    .line 363
    iget v0, p1, Landroid/media/CamcorderProfile;->videoBitRate:I

    #@13
    invoke-virtual {p0, v0}, Landroid/media/MediaRecorder;->setVideoEncodingBitRate(I)V

    #@16
    .line 364
    iget v0, p1, Landroid/media/CamcorderProfile;->videoCodec:I

    #@18
    invoke-virtual {p0, v0}, Landroid/media/MediaRecorder;->setVideoEncoder(I)V

    #@1b
    .line 365
    iget v0, p1, Landroid/media/CamcorderProfile;->quality:I

    #@1d
    const/16 v1, 0x3e8

    #@1f
    if-lt v0, v1, :cond_35

    #@21
    iget v0, p1, Landroid/media/CamcorderProfile;->quality:I

    #@23
    const/16 v1, 0x3ef

    #@25
    if-gt v0, v1, :cond_35

    #@27
    .line 368
    const-string/jumbo v0, "time-lapse-enable=1"

    #@2a
    const/4 v1, 0x0

    #@2b
    new-array v1, v1, [Ljava/lang/Object;

    #@2d
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@30
    move-result-object v0

    #@31
    invoke-direct {p0, v0}, Landroid/media/MediaRecorder;->setParameter(Ljava/lang/String;)V

    #@34
    .line 375
    :cond_34
    :goto_34
    return-void

    #@35
    .line 369
    :cond_35
    iget v0, p1, Landroid/media/CamcorderProfile;->audioCodec:I

    #@37
    if-ltz v0, :cond_34

    #@39
    .line 370
    iget v0, p1, Landroid/media/CamcorderProfile;->audioBitRate:I

    #@3b
    invoke-virtual {p0, v0}, Landroid/media/MediaRecorder;->setAudioEncodingBitRate(I)V

    #@3e
    .line 371
    iget v0, p1, Landroid/media/CamcorderProfile;->audioChannels:I

    #@40
    invoke-virtual {p0, v0}, Landroid/media/MediaRecorder;->setAudioChannels(I)V

    #@43
    .line 372
    iget v0, p1, Landroid/media/CamcorderProfile;->audioSampleRate:I

    #@45
    invoke-virtual {p0, v0}, Landroid/media/MediaRecorder;->setAudioSamplingRate(I)V

    #@48
    .line 373
    iget v0, p1, Landroid/media/CamcorderProfile;->audioCodec:I

    #@4a
    invoke-virtual {p0, v0}, Landroid/media/MediaRecorder;->setAudioEncoder(I)V

    #@4d
    goto :goto_34
.end method

.method public setRecordAngle(I)V
    .registers 4
    .parameter "angle"

    #@0
    .prologue
    .line 1152
    const-string v0, "MediaRecorder"

    #@2
    const-string v1, "MediaRecorder setRecordAngle"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1153
    invoke-direct {p0, p1}, Landroid/media/MediaRecorder;->native_setRecordAngle(I)V

    #@a
    .line 1154
    return-void
.end method

.method public setRecordZoomEnable(II)V
    .registers 5
    .parameter "angle"
    .parameter "zoommode"

    #@0
    .prologue
    .line 1142
    const-string v0, "MediaRecorder"

    #@2
    const-string v1, "MediaRecorder setRecordZoomEnable"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1143
    invoke-direct {p0, p1, p2}, Landroid/media/MediaRecorder;->native_setRecordZoomEnable(II)V

    #@a
    .line 1144
    return-void
.end method

.method public native setVideoEncoder(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public setVideoEncodingBitRate(I)V
    .registers 4
    .parameter "bitRate"

    #@0
    .prologue
    .line 621
    if-gtz p1, :cond_a

    #@2
    .line 622
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "Video encoding bit rate is not positive"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 624
    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string/jumbo v1, "video-param-encoding-bitrate="

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v0

    #@1e
    invoke-direct {p0, v0}, Landroid/media/MediaRecorder;->setParameter(Ljava/lang/String;)V

    #@21
    .line 625
    return-void
.end method

.method public native setVideoFrameRate(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public native setVideoSize(II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public native setVideoSource(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public native_start()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 737
    const-string v2, "MediaRecorder"

    #@2
    const-string/jumbo v3, "start()"

    #@5
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 738
    const-string/jumbo v2, "persist.sys.voice_state"

    #@b
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v2

    #@f
    const-string/jumbo v3, "recording"

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v2

    #@16
    if-eqz v2, :cond_32

    #@18
    .line 740
    :try_start_18
    invoke-static {}, Landroid/media/AudioManager;->getAudioService()Landroid/media/IAudioService;

    #@1b
    move-result-object v0

    #@1c
    .line 741
    .local v0, audioService:Landroid/media/IAudioService;
    if-eqz v0, :cond_49

    #@1e
    .line 742
    const-string v2, "MediaRecorder"

    #@20
    const-string/jumbo v3, "start() voiceActivation will be start,"

    #@23
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 743
    const/4 v2, 0x1

    #@27
    invoke-interface {v0, v2}, Landroid/media/IAudioService;->sendBroadcastRecordState(I)V

    #@2a
    .line 744
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@2d
    const-wide/16 v2, 0x1f4

    #@2f
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_32
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_32} :catch_52

    #@32
    .line 754
    .end local v0           #audioService:Landroid/media/IAudioService;
    :cond_32
    :goto_32
    invoke-direct {p0}, Landroid/media/MediaRecorder;->native_start()V

    #@35
    .line 757
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@38
    move-result-object v2

    #@39
    if-eqz v2, :cond_48

    #@3b
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@3e
    move-result-object v2

    #@3f
    invoke-interface {v2}, Lcom/lge/cappuccino/IMdm;->checkAllowMicrophone()Z

    #@42
    move-result v2

    #@43
    if-eqz v2, :cond_48

    #@45
    .line 759
    invoke-virtual {p0}, Landroid/media/MediaRecorder;->stop()V

    #@48
    .line 762
    :cond_48
    return-void

    #@49
    .line 746
    .restart local v0       #audioService:Landroid/media/IAudioService;
    :cond_49
    :try_start_49
    const-string v2, "MediaRecorder"

    #@4b
    const-string/jumbo v3, "start() audioService is null."

    #@4e
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_51
    .catch Ljava/lang/Exception; {:try_start_49 .. :try_end_51} :catch_52

    #@51
    goto :goto_32

    #@52
    .line 748
    .end local v0           #audioService:Landroid/media/IAudioService;
    :catch_52
    move-exception v1

    #@53
    .line 749
    .local v1, e:Ljava/lang/Exception;
    const-string v2, "MediaRecorder"

    #@55
    const-string/jumbo v3, "start() "

    #@58
    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@5b
    goto :goto_32
.end method

.method public native_stop()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 782
    const-string v2, "MediaRecorder"

    #@2
    const-string/jumbo v3, "stop()"

    #@5
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 783
    const-string/jumbo v2, "persist.sys.voice_state"

    #@b
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v2

    #@f
    const-string/jumbo v3, "recording"

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v2

    #@16
    if-nez v2, :cond_2a

    #@18
    .line 785
    :try_start_18
    invoke-static {}, Landroid/media/AudioManager;->getAudioService()Landroid/media/IAudioService;

    #@1b
    move-result-object v0

    #@1c
    .line 786
    .local v0, audioService:Landroid/media/IAudioService;
    if-eqz v0, :cond_2e

    #@1e
    .line 787
    const-string v2, "MediaRecorder"

    #@20
    const-string/jumbo v3, "stop() voiceActivation will be stop."

    #@23
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 788
    const/4 v2, 0x0

    #@27
    invoke-interface {v0, v2}, Landroid/media/IAudioService;->sendBroadcastRecordState(I)V
    :try_end_2a
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_2a} :catch_37

    #@2a
    .line 796
    .end local v0           #audioService:Landroid/media/IAudioService;
    :cond_2a
    :goto_2a
    invoke-direct {p0}, Landroid/media/MediaRecorder;->native_stop()V

    #@2d
    .line 797
    return-void

    #@2e
    .line 790
    .restart local v0       #audioService:Landroid/media/IAudioService;
    :cond_2e
    :try_start_2e
    const-string v2, "MediaRecorder"

    #@30
    const-string/jumbo v3, "stop() audioService is null."

    #@33
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_36
    .catch Ljava/lang/Exception; {:try_start_2e .. :try_end_36} :catch_37

    #@36
    goto :goto_2a

    #@37
    .line 792
    .end local v0           #audioService:Landroid/media/IAudioService;
    :catch_37
    move-exception v1

    #@38
    .line 793
    .local v1, e:Ljava/lang/Exception;
    const-string v2, "MediaRecorder"

    #@3a
    const-string/jumbo v3, "stop() "

    #@3d
    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@40
    goto :goto_2a
.end method
