.class public final Landroid/media/AmrInputStream;
.super Ljava/io/InputStream;
.source "AmrInputStream.java"


# static fields
.field private static final SAMPLES_PER_FRAME:I = 0xa0

.field private static final TAG:Ljava/lang/String; = "AmrInputStream"


# instance fields
.field private final mBuf:[B

.field private mBufIn:I

.field private mBufOut:I

.field private mGae:I

.field private mInputStream:Ljava/io/InputStream;

.field private mOneByte:[B


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 32
    const-string/jumbo v0, "media_jni"

    #@3
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@6
    .line 33
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .registers 4
    .parameter "inputStream"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 58
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    #@4
    .line 47
    const/16 v0, 0x140

    #@6
    new-array v0, v0, [B

    #@8
    iput-object v0, p0, Landroid/media/AmrInputStream;->mBuf:[B

    #@a
    .line 48
    iput v1, p0, Landroid/media/AmrInputStream;->mBufIn:I

    #@c
    .line 49
    iput v1, p0, Landroid/media/AmrInputStream;->mBufOut:I

    #@e
    .line 52
    const/4 v0, 0x1

    #@f
    new-array v0, v0, [B

    #@11
    iput-object v0, p0, Landroid/media/AmrInputStream;->mOneByte:[B

    #@13
    .line 59
    iput-object p1, p0, Landroid/media/AmrInputStream;->mInputStream:Ljava/io/InputStream;

    #@15
    .line 60
    invoke-static {}, Landroid/media/AmrInputStream;->GsmAmrEncoderNew()I

    #@18
    move-result v0

    #@19
    iput v0, p0, Landroid/media/AmrInputStream;->mGae:I

    #@1b
    .line 61
    iget v0, p0, Landroid/media/AmrInputStream;->mGae:I

    #@1d
    invoke-static {v0}, Landroid/media/AmrInputStream;->GsmAmrEncoderInitialize(I)V

    #@20
    .line 62
    return-void
.end method

.method private static native GsmAmrEncoderCleanup(I)V
.end method

.method private static native GsmAmrEncoderDelete(I)V
.end method

.method private static native GsmAmrEncoderEncode(I[BI[BI)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method private static native GsmAmrEncoderInitialize(I)V
.end method

.method private static native GsmAmrEncoderNew()I
.end method


# virtual methods
.method public close()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 107
    :try_start_2
    iget-object v0, p0, Landroid/media/AmrInputStream;->mInputStream:Ljava/io/InputStream;

    #@4
    if-eqz v0, :cond_b

    #@6
    iget-object v0, p0, Landroid/media/AmrInputStream;->mInputStream:Ljava/io/InputStream;

    #@8
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catchall {:try_start_2 .. :try_end_b} :catchall_37

    #@b
    .line 109
    :cond_b
    iput-object v1, p0, Landroid/media/AmrInputStream;->mInputStream:Ljava/io/InputStream;

    #@d
    .line 111
    :try_start_d
    iget v0, p0, Landroid/media/AmrInputStream;->mGae:I

    #@f
    if-eqz v0, :cond_16

    #@11
    iget v0, p0, Landroid/media/AmrInputStream;->mGae:I

    #@13
    invoke-static {v0}, Landroid/media/AmrInputStream;->GsmAmrEncoderCleanup(I)V
    :try_end_16
    .catchall {:try_start_d .. :try_end_16} :catchall_26

    #@16
    .line 114
    :cond_16
    :try_start_16
    iget v0, p0, Landroid/media/AmrInputStream;->mGae:I

    #@18
    if-eqz v0, :cond_1f

    #@1a
    iget v0, p0, Landroid/media/AmrInputStream;->mGae:I

    #@1c
    invoke-static {v0}, Landroid/media/AmrInputStream;->GsmAmrEncoderDelete(I)V
    :try_end_1f
    .catchall {:try_start_16 .. :try_end_1f} :catchall_22

    #@1f
    .line 116
    :cond_1f
    iput v2, p0, Landroid/media/AmrInputStream;->mGae:I

    #@21
    .line 120
    return-void

    #@22
    .line 116
    :catchall_22
    move-exception v0

    #@23
    iput v2, p0, Landroid/media/AmrInputStream;->mGae:I

    #@25
    throw v0

    #@26
    .line 113
    :catchall_26
    move-exception v0

    #@27
    .line 114
    :try_start_27
    iget v1, p0, Landroid/media/AmrInputStream;->mGae:I

    #@29
    if-eqz v1, :cond_30

    #@2b
    iget v1, p0, Landroid/media/AmrInputStream;->mGae:I

    #@2d
    invoke-static {v1}, Landroid/media/AmrInputStream;->GsmAmrEncoderDelete(I)V
    :try_end_30
    .catchall {:try_start_27 .. :try_end_30} :catchall_33

    #@30
    .line 116
    :cond_30
    iput v2, p0, Landroid/media/AmrInputStream;->mGae:I

    #@32
    throw v0

    #@33
    :catchall_33
    move-exception v0

    #@34
    iput v2, p0, Landroid/media/AmrInputStream;->mGae:I

    #@36
    throw v0

    #@37
    .line 109
    :catchall_37
    move-exception v0

    #@38
    iput-object v1, p0, Landroid/media/AmrInputStream;->mInputStream:Ljava/io/InputStream;

    #@3a
    .line 111
    :try_start_3a
    iget v1, p0, Landroid/media/AmrInputStream;->mGae:I

    #@3c
    if-eqz v1, :cond_43

    #@3e
    iget v1, p0, Landroid/media/AmrInputStream;->mGae:I

    #@40
    invoke-static {v1}, Landroid/media/AmrInputStream;->GsmAmrEncoderCleanup(I)V
    :try_end_43
    .catchall {:try_start_3a .. :try_end_43} :catchall_53

    #@43
    .line 114
    :cond_43
    :try_start_43
    iget v1, p0, Landroid/media/AmrInputStream;->mGae:I

    #@45
    if-eqz v1, :cond_4c

    #@47
    iget v1, p0, Landroid/media/AmrInputStream;->mGae:I

    #@49
    invoke-static {v1}, Landroid/media/AmrInputStream;->GsmAmrEncoderDelete(I)V
    :try_end_4c
    .catchall {:try_start_43 .. :try_end_4c} :catchall_4f

    #@4c
    .line 116
    :cond_4c
    iput v2, p0, Landroid/media/AmrInputStream;->mGae:I

    #@4e
    throw v0

    #@4f
    :catchall_4f
    move-exception v0

    #@50
    iput v2, p0, Landroid/media/AmrInputStream;->mGae:I

    #@52
    throw v0

    #@53
    .line 113
    :catchall_53
    move-exception v0

    #@54
    .line 114
    :try_start_54
    iget v1, p0, Landroid/media/AmrInputStream;->mGae:I

    #@56
    if-eqz v1, :cond_5d

    #@58
    iget v1, p0, Landroid/media/AmrInputStream;->mGae:I

    #@5a
    invoke-static {v1}, Landroid/media/AmrInputStream;->GsmAmrEncoderDelete(I)V
    :try_end_5d
    .catchall {:try_start_54 .. :try_end_5d} :catchall_60

    #@5d
    .line 116
    :cond_5d
    iput v2, p0, Landroid/media/AmrInputStream;->mGae:I

    #@5f
    throw v0

    #@60
    :catchall_60
    move-exception v0

    #@61
    iput v2, p0, Landroid/media/AmrInputStream;->mGae:I

    #@63
    throw v0
.end method

.method protected finalize()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 124
    iget v0, p0, Landroid/media/AmrInputStream;->mGae:I

    #@2
    if-eqz v0, :cond_10

    #@4
    .line 125
    invoke-virtual {p0}, Landroid/media/AmrInputStream;->close()V

    #@7
    .line 126
    new-instance v0, Ljava/lang/IllegalStateException;

    #@9
    const-string/jumbo v1, "someone forgot to close AmrInputStream"

    #@c
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 128
    :cond_10
    return-void
.end method

.method public read()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 66
    iget-object v1, p0, Landroid/media/AmrInputStream;->mOneByte:[B

    #@4
    invoke-virtual {p0, v1, v2, v3}, Landroid/media/AmrInputStream;->read([BII)I

    #@7
    move-result v0

    #@8
    .line 67
    .local v0, rtn:I
    if-ne v0, v3, :cond_11

    #@a
    iget-object v1, p0, Landroid/media/AmrInputStream;->mOneByte:[B

    #@c
    aget-byte v1, v1, v2

    #@e
    and-int/lit16 v1, v1, 0xff

    #@10
    :goto_10
    return v1

    #@11
    :cond_11
    const/4 v1, -0x1

    #@12
    goto :goto_10
.end method

.method public read([B)I
    .registers 4
    .parameter "b"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 72
    const/4 v0, 0x0

    #@1
    array-length v1, p1

    #@2
    invoke-virtual {p0, p1, v0, v1}, Landroid/media/AmrInputStream;->read([BII)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public read([BII)I
    .registers 11
    .parameter "b"
    .parameter "offset"
    .parameter "length"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 77
    iget v3, p0, Landroid/media/AmrInputStream;->mGae:I

    #@4
    if-nez v3, :cond_f

    #@6
    new-instance v2, Ljava/lang/IllegalStateException;

    #@8
    const-string/jumbo v3, "not open"

    #@b
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@e
    throw v2

    #@f
    .line 80
    :cond_f
    iget v3, p0, Landroid/media/AmrInputStream;->mBufOut:I

    #@11
    iget v4, p0, Landroid/media/AmrInputStream;->mBufIn:I

    #@13
    if-lt v3, v4, :cond_39

    #@15
    .line 82
    iput v6, p0, Landroid/media/AmrInputStream;->mBufOut:I

    #@17
    .line 83
    iput v6, p0, Landroid/media/AmrInputStream;->mBufIn:I

    #@19
    .line 86
    const/4 v0, 0x0

    #@1a
    .local v0, i:I
    :goto_1a
    const/16 v3, 0x140

    #@1c
    if-ge v0, v3, :cond_2d

    #@1e
    .line 87
    iget-object v3, p0, Landroid/media/AmrInputStream;->mInputStream:Ljava/io/InputStream;

    #@20
    iget-object v4, p0, Landroid/media/AmrInputStream;->mBuf:[B

    #@22
    rsub-int v5, v0, 0x140

    #@24
    invoke-virtual {v3, v4, v0, v5}, Ljava/io/InputStream;->read([BII)I

    #@27
    move-result v1

    #@28
    .line 88
    .local v1, n:I
    if-ne v1, v2, :cond_2b

    #@2a
    .line 101
    .end local v0           #i:I
    .end local v1           #n:I
    :goto_2a
    return v2

    #@2b
    .line 89
    .restart local v0       #i:I
    .restart local v1       #n:I
    :cond_2b
    add-int/2addr v0, v1

    #@2c
    .line 90
    goto :goto_1a

    #@2d
    .line 93
    .end local v1           #n:I
    :cond_2d
    iget v2, p0, Landroid/media/AmrInputStream;->mGae:I

    #@2f
    iget-object v3, p0, Landroid/media/AmrInputStream;->mBuf:[B

    #@31
    iget-object v4, p0, Landroid/media/AmrInputStream;->mBuf:[B

    #@33
    invoke-static {v2, v3, v6, v4, v6}, Landroid/media/AmrInputStream;->GsmAmrEncoderEncode(I[BI[BI)I

    #@36
    move-result v2

    #@37
    iput v2, p0, Landroid/media/AmrInputStream;->mBufIn:I

    #@39
    .line 97
    .end local v0           #i:I
    :cond_39
    iget v2, p0, Landroid/media/AmrInputStream;->mBufIn:I

    #@3b
    iget v3, p0, Landroid/media/AmrInputStream;->mBufOut:I

    #@3d
    sub-int/2addr v2, v3

    #@3e
    if-le p3, v2, :cond_46

    #@40
    iget v2, p0, Landroid/media/AmrInputStream;->mBufIn:I

    #@42
    iget v3, p0, Landroid/media/AmrInputStream;->mBufOut:I

    #@44
    sub-int p3, v2, v3

    #@46
    .line 98
    :cond_46
    iget-object v2, p0, Landroid/media/AmrInputStream;->mBuf:[B

    #@48
    iget v3, p0, Landroid/media/AmrInputStream;->mBufOut:I

    #@4a
    invoke-static {v2, v3, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@4d
    .line 99
    iget v2, p0, Landroid/media/AmrInputStream;->mBufOut:I

    #@4f
    add-int/2addr v2, p3

    #@50
    iput v2, p0, Landroid/media/AmrInputStream;->mBufOut:I

    #@52
    move v2, p3

    #@53
    .line 101
    goto :goto_2a
.end method
