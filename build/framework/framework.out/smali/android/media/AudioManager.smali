.class public Landroid/media/AudioManager;
.super Ljava/lang/Object;
.source "AudioManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/AudioManager$FocusEventHandlerDelegate;,
        Landroid/media/AudioManager$OnAudioFocusChangeListener;
    }
.end annotation


# static fields
.field public static final ACTION_AUDIO_BECOMING_NOISY:Ljava/lang/String; = "android.media.AUDIO_BECOMING_NOISY"

.field public static final ACTION_AUDIO_STOP_NOTIFICATION:Ljava/lang/String; = "com.lge.media.STOP_NOTIFICATION"

.field public static final ACTION_SCO_AUDIO_STATE_CHANGED:Ljava/lang/String; = "android.media.SCO_AUDIO_STATE_CHANGED"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ACTION_SCO_AUDIO_STATE_UPDATED:Ljava/lang/String; = "android.media.ACTION_SCO_AUDIO_STATE_UPDATED"

.field public static final ADJUST_LOWER:I = -0x1

.field public static final ADJUST_RAISE:I = 0x1

.field public static final ADJUST_SAME:I = 0x0

.field public static final AUDIOFOCUS_GAIN:I = 0x1

.field public static final AUDIOFOCUS_GAIN_TRANSIENT:I = 0x2

.field public static final AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK:I = 0x3

.field public static final AUDIOFOCUS_LOSS:I = -0x1

.field public static final AUDIOFOCUS_LOSS_TRANSIENT:I = -0x2

.field public static final AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:I = -0x3

.field public static final AUDIOFOCUS_REQUEST_FAILED:I = 0x0

.field public static final AUDIOFOCUS_REQUEST_GRANTED:I = 0x1

.field public static final AUDIORECORD_STATE_CHANGED_ACTION:Ljava/lang/String; = "com.lge.media.AUDIORECORD_STATE_CHANGED_ACTION"

.field public static final AUDIO_RECORDHOOKING_STATE_HOOKING:I = 0x3

.field public static final AUDIO_RECORDHOOKING_STATE_NONE:I = 0x0

.field public static final AUDIO_RECORDHOOKING_STATE_RECORDING:I = 0x2

.field public static final AUDIO_RECORDHOOKING_STATE_STARTING:I = 0x1

.field public static final AUDIO_RECORDHOOKING_TYPE_NONE:I = 0x0

.field public static final AUDIO_RECORDHOOKING_TYPE_VISUALVOICE:I = 0x2

.field public static final AUDIO_RECORDHOOKING_TYPE_VOICETAGGING:I = 0x1

.field public static final AUDIO_SET_FORCE_USE_EARPIECE:Ljava/lang/String; = "com.lge.media.SET_FORCE_USE_EARPIECE"

.field public static final AUDIO_VOICEACTIVATION_NONE:I = 0x0

.field public static final AUDIO_VOICEACTIVATION_PROP:Ljava/lang/String; = "persist.sys.voice_state"

.field public static final AUDIO_VOICEACTIVATION_PROP_NONE:Ljava/lang/String; = "none"

.field public static final AUDIO_VOICEACTIVATION_PROP_RECORDING:Ljava/lang/String; = "recording"

.field public static final AUDIO_VOICEACTIVATION_PROP_STARTING:Ljava/lang/String; = "starting"

.field public static final AUDIO_VOICEACTIVATION_RECORDING:I = 0x2

.field public static final AUDIO_VOICEACTIVATION_STARTING:I = 0x1

.field public static final CS_ACTIVE:I = 0x1

.field public static final CS_ACTIVE_SESSION2:I = 0x100

.field public static final CS_HOLD:I = 0x2

.field public static final CS_HOLD_SESSION2:I = 0x200

.field public static final CS_INACTIVE:I = 0x0

.field public static final CS_INACTIVE_SESSION2:I = 0x0

.field public static final DEFAULT_STREAM_VOLUME:[I = null

.field public static final DEVICE_OUT_ANLG_DOCK_HEADSET:I = 0x800

.field public static final DEVICE_OUT_AUX_DIGITAL:I = 0x400

.field public static final DEVICE_OUT_BLUETOOTH_A2DP:I = 0x80

.field public static final DEVICE_OUT_BLUETOOTH_A2DP_HEADPHONES:I = 0x100

.field public static final DEVICE_OUT_BLUETOOTH_A2DP_SPEAKER:I = 0x200

.field public static final DEVICE_OUT_BLUETOOTH_SCO:I = 0x10

.field public static final DEVICE_OUT_BLUETOOTH_SCO_CARKIT:I = 0x40

.field public static final DEVICE_OUT_BLUETOOTH_SCO_HEADSET:I = 0x20

.field public static final DEVICE_OUT_DEFAULT:I = 0x40000000

.field public static final DEVICE_OUT_DGTL_DOCK_HEADSET:I = 0x1000

.field public static final DEVICE_OUT_EARPIECE:I = 0x1

.field public static final DEVICE_OUT_SPEAKER:I = 0x2

.field public static final DEVICE_OUT_USB_ACCESSORY:I = 0x2000

.field public static final DEVICE_OUT_USB_DEVICE:I = 0x4000

.field public static final DEVICE_OUT_WIRED_HEADPHONE:I = 0x8

.field public static final DEVICE_OUT_WIRED_HEADSET:I = 0x4

.field public static final EXTRA_AUDIORECORD_STATE:Ljava/lang/String; = "com.lge.media.EXTRA_AUDIORECORD_STATE"

.field public static final EXTRA_AUDIORECORD_STATE_START:I = 0x1

.field public static final EXTRA_AUDIORECORD_STATE_STOP:I = 0x0

.field public static final EXTRA_MASTER_VOLUME_MUTED:Ljava/lang/String; = "android.media.EXTRA_MASTER_VOLUME_MUTED"

.field public static final EXTRA_MASTER_VOLUME_VALUE:Ljava/lang/String; = "android.media.EXTRA_MASTER_VOLUME_VALUE"

.field public static final EXTRA_PREV_MASTER_VOLUME_VALUE:Ljava/lang/String; = "android.media.EXTRA_PREV_MASTER_VOLUME_VALUE"

.field public static final EXTRA_PREV_VOLUME_STREAM_VALUE:Ljava/lang/String; = "android.media.EXTRA_PREV_VOLUME_STREAM_VALUE"

.field public static final EXTRA_REMOTE_CONTROL_CLIENT_GENERATION:Ljava/lang/String; = "android.media.EXTRA_REMOTE_CONTROL_CLIENT_GENERATION"

.field public static final EXTRA_REMOTE_CONTROL_CLIENT_INFO_CHANGED:Ljava/lang/String; = "android.media.EXTRA_REMOTE_CONTROL_CLIENT_INFO_CHANGED"

.field public static final EXTRA_REMOTE_CONTROL_CLIENT_NAME:Ljava/lang/String; = "android.media.EXTRA_REMOTE_CONTROL_CLIENT_NAME"

.field public static final EXTRA_REMOTE_CONTROL_EVENT_RECEIVER:Ljava/lang/String; = "android.media.EXTRA_REMOTE_CONTROL_EVENT_RECEIVER"

.field public static final EXTRA_RINGER_MODE:Ljava/lang/String; = "android.media.EXTRA_RINGER_MODE"

.field public static final EXTRA_SCO_AUDIO_PREVIOUS_STATE:Ljava/lang/String; = "android.media.extra.SCO_AUDIO_PREVIOUS_STATE"

.field public static final EXTRA_SCO_AUDIO_STATE:Ljava/lang/String; = "android.media.extra.SCO_AUDIO_STATE"

.field public static final EXTRA_SET_FORCE_USE_EARPIECE:Ljava/lang/String; = "com.lge.media.EXTRA_USE_EARPIECE_ON"

.field public static final EXTRA_VIBRATE_SETTING:Ljava/lang/String; = "android.media.EXTRA_VIBRATE_SETTING"

.field public static final EXTRA_VIBRATE_TYPE:Ljava/lang/String; = "android.media.EXTRA_VIBRATE_TYPE"

.field public static final EXTRA_VOLUME_STREAM_TYPE:Ljava/lang/String; = "android.media.EXTRA_VOLUME_STREAM_TYPE"

.field public static final EXTRA_VOLUME_STREAM_VALUE:Ljava/lang/String; = "android.media.EXTRA_VOLUME_STREAM_VALUE"

.field public static final FLAG_ALLOW_RINGER_MODES:I = 0x2

.field public static final FLAG_EXPAND_VOLUME_PANEL:I = 0x100

.field public static final FLAG_FIXED_VOLUME:I = 0x20

.field public static final FLAG_KEEP_RINGER_MODES:I = 0x80

.field public static final FLAG_NOT_ALLOW_RINGER_MODES:I = 0x40

.field public static final FLAG_PLAY_SOUND:I = 0x4

.field public static final FLAG_REMOVE_SOUND_AND_VIBRATE:I = 0x8

.field public static final FLAG_SHOW_UI:I = 0x1

.field public static final FLAG_VIBRATE:I = 0x10

.field public static final FX_FOCUS_NAVIGATION_DOWN:I = 0x2

.field public static final FX_FOCUS_NAVIGATION_LEFT:I = 0x3

.field public static final FX_FOCUS_NAVIGATION_RIGHT:I = 0x4

.field public static final FX_FOCUS_NAVIGATION_UP:I = 0x1

.field public static final FX_KEYPRESS_DELETE:I = 0x7

.field public static final FX_KEYPRESS_RETURN:I = 0x8

.field public static final FX_KEYPRESS_SPACEBAR:I = 0x6

.field public static final FX_KEYPRESS_STANDARD:I = 0x5

.field public static final FX_KEY_CLICK:I = 0x0

.field public static final FX_SWITCH_OFF:I = 0xa

.field public static final FX_SWITCH_ON:I = 0x9

.field public static final IMS_ACTIVE:I = 0x10

.field public static final IMS_HOLD:I = 0x20

.field public static final IMS_INACTIVE:I = 0x0

.field public static final MASTER_MUTE_CHANGED_ACTION:Ljava/lang/String; = "android.media.MASTER_MUTE_CHANGED_ACTION"

.field public static final MASTER_VOLUME_CHANGED_ACTION:Ljava/lang/String; = "android.media.MASTER_VOLUME_CHANGED_ACTION"

.field public static final MODE_CURRENT:I = -0x1

.field public static final MODE_INVALID:I = -0x2

.field public static final MODE_IN_CALL:I = 0x2

.field public static final MODE_IN_COMMUNICATION:I = 0x3

.field public static final MODE_NORMAL:I = 0x0

.field public static final MODE_RINGTONE:I = 0x1

.field public static final NUM_SOUND_EFFECTS:I = 0xb

.field public static final NUM_STREAMS:I = 0x5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final PROPERTY_OUTPUT_FRAMES_PER_BUFFER:Ljava/lang/String; = "android.media.property.OUTPUT_FRAMES_PER_BUFFER"

.field public static final PROPERTY_OUTPUT_SAMPLE_RATE:Ljava/lang/String; = "android.media.property.OUTPUT_SAMPLE_RATE"

.field public static final REMOTE_CONTROL_CLIENT_CHANGED:Ljava/lang/String; = "android.media.REMOTE_CONTROL_CLIENT_CHANGED"

.field public static final RINGER_MODE_CHANGED_ACTION:Ljava/lang/String; = "android.media.RINGER_MODE_CHANGED"

.field private static final RINGER_MODE_MAX:I = 0x2

.field public static final RINGER_MODE_NORMAL:I = 0x2

.field public static final RINGER_MODE_SILENT:I = 0x0

.field public static final RINGER_MODE_VIBRATE:I = 0x1

.field public static final ROUTE_ALL:I = -0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ROUTE_BLUETOOTH:I = 0x4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ROUTE_BLUETOOTH_A2DP:I = 0x10
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ROUTE_BLUETOOTH_SCO:I = 0x4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ROUTE_EARPIECE:I = 0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ROUTE_HEADSET:I = 0x8
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ROUTE_SPEAKER:I = 0x2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SCO_AUDIO_STATE_CONNECTED:I = 0x1

.field public static final SCO_AUDIO_STATE_CONNECTING:I = 0x2

.field public static final SCO_AUDIO_STATE_DISCONNECTED:I = 0x0

.field public static final SCO_AUDIO_STATE_ERROR:I = -0x1

.field public static final STREAM_ALARM:I = 0x4

.field public static final STREAM_BLUETOOTH_SCO:I = 0x6

.field public static final STREAM_DMB:I = 0xb

.field public static final STREAM_DTMF:I = 0x8

.field public static final STREAM_FM:I = 0xa

.field public static final STREAM_INCALL_MSG:I = 0xc

.field public static final STREAM_MUSIC:I = 0x3

.field public static final STREAM_NOTIFICATION:I = 0x5

.field public static final STREAM_RING:I = 0x2

.field public static final STREAM_SYSTEM:I = 0x1

.field public static final STREAM_SYSTEM_ENFORCED:I = 0x7

.field public static final STREAM_TTS:I = 0x9

.field public static final STREAM_VOICE_CALL:I = 0x0

.field private static TAG:Ljava/lang/String; = null

.field public static final USE_DEFAULT_STREAM_TYPE:I = -0x80000000

.field public static final VIBRATE_SETTING_CHANGED_ACTION:Ljava/lang/String; = "android.media.VIBRATE_SETTING_CHANGED"

.field public static final VIBRATE_SETTING_OFF:I = 0x0

.field public static final VIBRATE_SETTING_ON:I = 0x1

.field public static final VIBRATE_SETTING_ONLY_SILENT:I = 0x2

.field public static final VIBRATE_TYPE_NOTIFICATION:I = 0x1

.field public static final VIBRATE_TYPE_RINGER:I = 0x0

.field public static final VOLUME_CHANGED_ACTION:Ljava/lang/String; = "android.media.VOLUME_CHANGED_ACTION"

.field private static sService:Landroid/media/IAudioService;


# instance fields
.field private final mAudioFocusDispatcher:Landroid/media/IAudioFocusDispatcher;

.field private final mAudioFocusEventHandlerDelegate:Landroid/media/AudioManager$FocusEventHandlerDelegate;

.field private final mAudioFocusIdListenerMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/media/AudioManager$OnAudioFocusChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mFocusListenerLock:Ljava/lang/Object;

.field private final mICallBack:Landroid/os/IBinder;

.field private final mUseMasterVolume:Z

.field private final mUseVolumeKeySounds:Z

.field private mVolumeKeyUpTime:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 60
    const-string v0, "AudioManager"

    #@2
    sput-object v0, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@4
    .line 297
    const/16 v0, 0xd

    #@6
    new-array v0, v0, [I

    #@8
    fill-array-data v0, :array_e

    #@b
    sput-object v0, Landroid/media/AudioManager;->DEFAULT_STREAM_VOLUME:[I

    #@d
    return-void

    #@e
    :array_e
    .array-data 0x4
        0x4t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
        0xbt 0x0t 0x0t 0x0t
        0xbt 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
        0xbt 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 526
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 2104
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Landroid/media/AudioManager;->mAudioFocusIdListenerMap:Ljava/util/HashMap;

    #@a
    .line 2110
    new-instance v0, Ljava/lang/Object;

    #@c
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@f
    iput-object v0, p0, Landroid/media/AudioManager;->mFocusListenerLock:Ljava/lang/Object;

    #@11
    .line 2119
    new-instance v0, Landroid/media/AudioManager$FocusEventHandlerDelegate;

    #@13
    invoke-direct {v0, p0}, Landroid/media/AudioManager$FocusEventHandlerDelegate;-><init>(Landroid/media/AudioManager;)V

    #@16
    iput-object v0, p0, Landroid/media/AudioManager;->mAudioFocusEventHandlerDelegate:Landroid/media/AudioManager$FocusEventHandlerDelegate;

    #@18
    .line 2158
    new-instance v0, Landroid/media/AudioManager$1;

    #@1a
    invoke-direct {v0, p0}, Landroid/media/AudioManager$1;-><init>(Landroid/media/AudioManager;)V

    #@1d
    iput-object v0, p0, Landroid/media/AudioManager;->mAudioFocusDispatcher:Landroid/media/IAudioFocusDispatcher;

    #@1f
    .line 2583
    new-instance v0, Landroid/os/Binder;

    #@21
    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    #@24
    iput-object v0, p0, Landroid/media/AudioManager;->mICallBack:Landroid/os/IBinder;

    #@26
    .line 527
    iput-object p1, p0, Landroid/media/AudioManager;->mContext:Landroid/content/Context;

    #@28
    .line 528
    iget-object v0, p0, Landroid/media/AudioManager;->mContext:Landroid/content/Context;

    #@2a
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@2d
    move-result-object v0

    #@2e
    const v1, 0x1110010

    #@31
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@34
    move-result v0

    #@35
    iput-boolean v0, p0, Landroid/media/AudioManager;->mUseMasterVolume:Z

    #@37
    .line 530
    iget-object v0, p0, Landroid/media/AudioManager;->mContext:Landroid/content/Context;

    #@39
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3c
    move-result-object v0

    #@3d
    const v1, 0x1110011

    #@40
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@43
    move-result v0

    #@44
    iput-boolean v0, p0, Landroid/media/AudioManager;->mUseVolumeKeySounds:Z

    #@46
    .line 532
    return-void
.end method

.method static synthetic access$000(Landroid/media/AudioManager;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 54
    iget-object v0, p0, Landroid/media/AudioManager;->mFocusListenerLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/media/AudioManager;Ljava/lang/String;)Landroid/media/AudioManager$OnAudioFocusChangeListener;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 54
    invoke-direct {p0, p1}, Landroid/media/AudioManager;->findFocusListener(Ljava/lang/String;)Landroid/media/AudioManager$OnAudioFocusChangeListener;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$200(Landroid/media/AudioManager;)Landroid/media/AudioManager$FocusEventHandlerDelegate;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 54
    iget-object v0, p0, Landroid/media/AudioManager;->mAudioFocusEventHandlerDelegate:Landroid/media/AudioManager$FocusEventHandlerDelegate;

    #@2
    return-object v0
.end method

.method private findFocusListener(Ljava/lang/String;)Landroid/media/AudioManager$OnAudioFocusChangeListener;
    .registers 3
    .parameter "id"

    #@0
    .prologue
    .line 2113
    iget-object v0, p0, Landroid/media/AudioManager;->mAudioFocusIdListenerMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/media/AudioManager$OnAudioFocusChangeListener;

    #@8
    return-object v0
.end method

.method public static getAudioService()Landroid/media/IAudioService;
    .registers 2

    #@0
    .prologue
    .line 2980
    sget-object v1, Landroid/media/AudioManager;->sService:Landroid/media/IAudioService;

    #@2
    if-eqz v1, :cond_7

    #@4
    .line 2981
    sget-object v1, Landroid/media/AudioManager;->sService:Landroid/media/IAudioService;

    #@6
    .line 2985
    .local v0, b:Landroid/os/IBinder;
    :goto_6
    return-object v1

    #@7
    .line 2983
    .end local v0           #b:Landroid/os/IBinder;
    :cond_7
    const-string v1, "audio"

    #@9
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@c
    move-result-object v0

    #@d
    .line 2984
    .restart local v0       #b:Landroid/os/IBinder;
    invoke-static {v0}, Landroid/media/IAudioService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IAudioService;

    #@10
    move-result-object v1

    #@11
    sput-object v1, Landroid/media/AudioManager;->sService:Landroid/media/IAudioService;

    #@13
    .line 2985
    sget-object v1, Landroid/media/AudioManager;->sService:Landroid/media/IAudioService;

    #@15
    goto :goto_6
.end method

.method private getIdForAudioFocusListener(Landroid/media/AudioManager$OnAudioFocusChangeListener;)Ljava/lang/String;
    .registers 5
    .parameter "l"

    #@0
    .prologue
    .line 2168
    if-nez p1, :cond_c

    #@2
    .line 2169
    new-instance v0, Ljava/lang/String;

    #@4
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@b
    .line 2171
    :goto_b
    return-object v0

    #@c
    :cond_c
    new-instance v0, Ljava/lang/String;

    #@e
    new-instance v1, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@2a
    goto :goto_b
.end method

.method private static getService()Landroid/media/IAudioService;
    .registers 2

    #@0
    .prologue
    .line 536
    sget-object v1, Landroid/media/AudioManager;->sService:Landroid/media/IAudioService;

    #@2
    if-eqz v1, :cond_7

    #@4
    .line 537
    sget-object v1, Landroid/media/AudioManager;->sService:Landroid/media/IAudioService;

    #@6
    .line 541
    .local v0, b:Landroid/os/IBinder;
    :goto_6
    return-object v1

    #@7
    .line 539
    .end local v0           #b:Landroid/os/IBinder;
    :cond_7
    const-string v1, "audio"

    #@9
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@c
    move-result-object v0

    #@d
    .line 540
    .restart local v0       #b:Landroid/os/IBinder;
    invoke-static {v0}, Landroid/media/IAudioService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IAudioService;

    #@10
    move-result-object v1

    #@11
    sput-object v1, Landroid/media/AudioManager;->sService:Landroid/media/IAudioService;

    #@13
    .line 541
    sget-object v1, Landroid/media/AudioManager;->sService:Landroid/media/IAudioService;

    #@15
    goto :goto_6
.end method

.method public static isValidRingerMode(I)Z
    .registers 2
    .parameter "ringerMode"

    #@0
    .prologue
    .line 774
    if-ltz p0, :cond_5

    #@2
    const/4 v0, 0x2

    #@3
    if-le p0, v0, :cond_7

    #@5
    .line 775
    :cond_5
    const/4 v0, 0x0

    #@6
    .line 777
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x1

    #@8
    goto :goto_6
.end method

.method private querySoundEffectsEnabled()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 2005
    iget-object v1, p0, Landroid/media/AudioManager;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v1

    #@7
    const-string/jumbo v2, "sound_effects_enabled"

    #@a
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_11

    #@10
    const/4 v0, 0x1

    #@11
    :cond_11
    return v0
.end method


# virtual methods
.method public abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I
    .registers 8
    .parameter "l"

    #@0
    .prologue
    .line 2291
    const/4 v2, 0x0

    #@1
    .line 2292
    .local v2, status:I
    invoke-virtual {p0, p1}, Landroid/media/AudioManager;->unregisterAudioFocusListener(Landroid/media/AudioManager$OnAudioFocusChangeListener;)V

    #@4
    .line 2293
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@7
    move-result-object v1

    #@8
    .line 2295
    .local v1, service:Landroid/media/IAudioService;
    :try_start_8
    iget-object v3, p0, Landroid/media/AudioManager;->mAudioFocusDispatcher:Landroid/media/IAudioFocusDispatcher;

    #@a
    invoke-direct {p0, p1}, Landroid/media/AudioManager;->getIdForAudioFocusListener(Landroid/media/AudioManager$OnAudioFocusChangeListener;)Ljava/lang/String;

    #@d
    move-result-object v4

    #@e
    invoke-interface {v1, v3, v4}, Landroid/media/IAudioService;->abandonAudioFocus(Landroid/media/IAudioFocusDispatcher;Ljava/lang/String;)I
    :try_end_11
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_11} :catch_13

    #@11
    move-result v2

    #@12
    .line 2300
    :goto_12
    return v2

    #@13
    .line 2297
    :catch_13
    move-exception v0

    #@14
    .line 2298
    .local v0, e:Landroid/os/RemoteException;
    sget-object v3, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@16
    new-instance v4, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v5, "Can\'t call abandonAudioFocus() on AudioService due to "

    #@1d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v4

    #@29
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    goto :goto_12
.end method

.method public abandonAudioFocusForCall()V
    .registers 6

    #@0
    .prologue
    .line 2277
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 2279
    .local v1, service:Landroid/media/IAudioService;
    const/4 v2, 0x0

    #@5
    :try_start_5
    const-string v3, "AudioFocus_For_Phone_Ring_And_Calls"

    #@7
    invoke-interface {v1, v2, v3}, Landroid/media/IAudioService;->abandonAudioFocus(Landroid/media/IAudioFocusDispatcher;Ljava/lang/String;)I
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_a} :catch_b

    #@a
    .line 2283
    :goto_a
    return-void

    #@b
    .line 2280
    :catch_b
    move-exception v0

    #@c
    .line 2281
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@e
    new-instance v3, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v4, "Can\'t call abandonAudioFocusForCall() on AudioService due to "

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    goto :goto_a
.end method

.method public addMediaButtonEventReceiverForCalls(Landroid/content/ComponentName;)V
    .registers 6
    .parameter "eventReceiver"

    #@0
    .prologue
    .line 2856
    if-nez p1, :cond_3

    #@2
    .line 2870
    :goto_2
    return-void

    #@3
    .line 2859
    :cond_3
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@6
    move-result-object v1

    #@7
    .line 2860
    .local v1, service:Landroid/media/IAudioService;
    if-eqz v1, :cond_16

    #@9
    .line 2863
    :try_start_9
    invoke-interface {v1, p1}, Landroid/media/IAudioService;->addMediaButtonEventReceiverForCalls(Landroid/content/ComponentName;)V
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_c} :catch_d

    #@c
    goto :goto_2

    #@d
    .line 2864
    :catch_d
    move-exception v0

    #@e
    .line 2865
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@10
    const-string v3, "Dead object in addMediaButtonEventReceiverForCallsList"

    #@12
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@15
    goto :goto_2

    #@16
    .line 2868
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_16
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@18
    const-string v3, "AudioService is null in addMediaButtonEventReceiverForCallsList"

    #@1a
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    goto :goto_2
.end method

.method public adjustLocalOrRemoteStreamVolume(II)V
    .registers 8
    .parameter "streamType"
    .parameter "direction"

    #@0
    .prologue
    .line 1822
    const/4 v2, 0x3

    #@1
    if-eq p1, v2, :cond_1f

    #@3
    const/16 v2, 0xa

    #@5
    if-eq p1, v2, :cond_1f

    #@7
    .line 1823
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@9
    new-instance v3, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v4, "adjustLocalOrRemoteStreamVolume() doesn\'t support stream "

    #@10
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 1825
    :cond_1f
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@22
    move-result-object v1

    #@23
    .line 1827
    .local v1, service:Landroid/media/IAudioService;
    :try_start_23
    invoke-interface {v1, p1, p2}, Landroid/media/IAudioService;->adjustLocalOrRemoteStreamVolume(II)V
    :try_end_26
    .catch Landroid/os/RemoteException; {:try_start_23 .. :try_end_26} :catch_27

    #@26
    .line 1831
    :goto_26
    return-void

    #@27
    .line 1828
    :catch_27
    move-exception v0

    #@28
    .line 1829
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@2a
    const-string v3, "Dead object in adjustLocalOrRemoteStreamVolume"

    #@2c
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2f
    goto :goto_26
.end method

.method public adjustMasterVolume(II)V
    .registers 7
    .parameter "steps"
    .parameter "flags"

    #@0
    .prologue
    .line 740
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 742
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    invoke-interface {v1, p1, p2}, Landroid/media/IAudioService;->adjustMasterVolume(II)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_7} :catch_8

    #@7
    .line 746
    :goto_7
    return-void

    #@8
    .line 743
    :catch_8
    move-exception v0

    #@9
    .line 744
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@b
    const-string v3, "Dead object in adjustMasterVolume"

    #@d
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@10
    goto :goto_7
.end method

.method public adjustStreamVolume(III)V
    .registers 8
    .parameter "streamType"
    .parameter "direction"
    .parameter "flags"

    #@0
    .prologue
    .line 658
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 660
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    iget-boolean v2, p0, Landroid/media/AudioManager;->mUseMasterVolume:Z

    #@6
    if-eqz v2, :cond_c

    #@8
    .line 661
    invoke-interface {v1, p2, p3}, Landroid/media/IAudioService;->adjustMasterVolume(II)V

    #@b
    .line 668
    :goto_b
    return-void

    #@c
    .line 663
    :cond_c
    invoke-interface {v1, p1, p2, p3}, Landroid/media/IAudioService;->adjustStreamVolume(III)V
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_f} :catch_10

    #@f
    goto :goto_b

    #@10
    .line 665
    :catch_10
    move-exception v0

    #@11
    .line 666
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@13
    const-string v3, "Dead object in adjustStreamVolume"

    #@15
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@18
    goto :goto_b
.end method

.method public adjustSuggestedStreamVolume(III)V
    .registers 8
    .parameter "direction"
    .parameter "suggestedStreamType"
    .parameter "flags"

    #@0
    .prologue
    .line 718
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 720
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    iget-boolean v2, p0, Landroid/media/AudioManager;->mUseMasterVolume:Z

    #@6
    if-eqz v2, :cond_c

    #@8
    .line 721
    invoke-interface {v1, p1, p3}, Landroid/media/IAudioService;->adjustMasterVolume(II)V

    #@b
    .line 728
    :goto_b
    return-void

    #@c
    .line 723
    :cond_c
    invoke-interface {v1, p1, p2, p3}, Landroid/media/IAudioService;->adjustSuggestedStreamVolume(III)V
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_f} :catch_10

    #@f
    goto :goto_b

    #@10
    .line 725
    :catch_10
    move-exception v0

    #@11
    .line 726
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@13
    const-string v3, "Dead object in adjustSuggestedStreamVolume"

    #@15
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@18
    goto :goto_b
.end method

.method public adjustVolume(II)V
    .registers 7
    .parameter "direction"
    .parameter "flags"

    #@0
    .prologue
    .line 688
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 690
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    iget-boolean v2, p0, Landroid/media/AudioManager;->mUseMasterVolume:Z

    #@6
    if-eqz v2, :cond_c

    #@8
    .line 691
    invoke-interface {v1, p1, p2}, Landroid/media/IAudioService;->adjustMasterVolume(II)V

    #@b
    .line 698
    :goto_b
    return-void

    #@c
    .line 693
    :cond_c
    invoke-interface {v1, p1, p2}, Landroid/media/IAudioService;->adjustVolume(II)V
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_f} :catch_10

    #@f
    goto :goto_b

    #@10
    .line 695
    :catch_10
    move-exception v0

    #@11
    .line 696
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@13
    const-string v3, "Dead object in adjustVolume"

    #@15
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@18
    goto :goto_b
.end method

.method public checkPlayConditions(I)Z
    .registers 7
    .parameter "streamType"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2897
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@4
    move-result-object v1

    #@5
    .line 2898
    .local v1, service:Landroid/media/IAudioService;
    if-eqz v1, :cond_15

    #@7
    .line 2900
    :try_start_7
    invoke-interface {v1, p1}, Landroid/media/IAudioService;->checkPlayConditions(I)Z
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_a} :catch_c

    #@a
    move-result v2

    #@b
    .line 2907
    :goto_b
    return v2

    #@c
    .line 2901
    :catch_c
    move-exception v0

    #@d
    .line 2902
    .local v0, e:Landroid/os/RemoteException;
    sget-object v3, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@f
    const-string v4, "Dead object in checkPlayCondition"

    #@11
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@14
    goto :goto_b

    #@15
    .line 2906
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_15
    sget-object v3, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@17
    const-string v4, "AudioService is null in checkPlayCondition"

    #@19
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    goto :goto_b
.end method

.method public closeRecordHooking(Ljava/io/FileDescriptor;)V
    .registers 3
    .parameter "fd"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 2931
    if-eqz p1, :cond_6

    #@3
    .line 2932
    invoke-static {v0, v0, v0}, Landroid/media/AudioSystem;->setRecordHookingEnabled(III)Ljava/io/FileDescriptor;

    #@6
    .line 2934
    :cond_6
    return-void
.end method

.method public forceVolumeControlStream(I)V
    .registers 6
    .parameter "streamType"

    #@0
    .prologue
    .line 1121
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 1123
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    iget-object v2, p0, Landroid/media/AudioManager;->mICallBack:Landroid/os/IBinder;

    #@6
    invoke-interface {v1, p1, v2}, Landroid/media/IAudioService;->forceVolumeControlStream(ILandroid/os/IBinder;)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 1127
    :goto_9
    return-void

    #@a
    .line 1124
    :catch_a
    move-exception v0

    #@b
    .line 1125
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@d
    const-string v3, "Dead object in forceVolumeControlStream"

    #@f
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@12
    goto :goto_9
.end method

.method public getDevicesForStream(I)I
    .registers 3
    .parameter "streamType"

    #@0
    .prologue
    .line 2697
    packed-switch p1, :pswitch_data_a

    #@3
    .line 2709
    :pswitch_3
    const/4 v0, 0x0

    #@4
    :goto_4
    return v0

    #@5
    .line 2707
    :pswitch_5
    invoke-static {p1}, Landroid/media/AudioSystem;->getDevicesForStream(I)I

    #@8
    move-result v0

    #@9
    goto :goto_4

    #@a
    .line 2697
    :pswitch_data_a
    .packed-switch 0x0
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_3
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public getInCallMode()I
    .registers 5

    #@0
    .prologue
    .line 1635
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 1637
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    invoke-interface {v1}, Landroid/media/IAudioService;->getInCallMode()I
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_7} :catch_9

    #@7
    move-result v2

    #@8
    .line 1640
    :goto_8
    return v2

    #@9
    .line 1638
    :catch_9
    move-exception v0

    #@a
    .line 1639
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@c
    const-string v3, "Dead object in getInCallMode"

    #@e
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@11
    .line 1640
    const/4 v2, -0x2

    #@12
    goto :goto_8
.end method

.method public getLastAudibleMasterVolume()I
    .registers 5

    #@0
    .prologue
    .line 973
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 975
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    invoke-interface {v1}, Landroid/media/IAudioService;->getLastAudibleMasterVolume()I
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_7} :catch_9

    #@7
    move-result v2

    #@8
    .line 978
    :goto_8
    return v2

    #@9
    .line 976
    :catch_9
    move-exception v0

    #@a
    .line 977
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@c
    const-string v3, "Dead object in getLastAudibleMasterVolume"

    #@e
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@11
    .line 978
    const/4 v2, 0x0

    #@12
    goto :goto_8
.end method

.method public getLastAudibleStreamVolume(I)I
    .registers 6
    .parameter "streamType"

    #@0
    .prologue
    .line 829
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 831
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    iget-boolean v2, p0, Landroid/media/AudioManager;->mUseMasterVolume:Z

    #@6
    if-eqz v2, :cond_d

    #@8
    .line 832
    invoke-interface {v1}, Landroid/media/IAudioService;->getLastAudibleMasterVolume()I

    #@b
    move-result v2

    #@c
    .line 838
    :goto_c
    return v2

    #@d
    .line 834
    :cond_d
    invoke-interface {v1, p1}, Landroid/media/IAudioService;->getLastAudibleStreamVolume(I)I
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_10} :catch_12

    #@10
    move-result v2

    #@11
    goto :goto_c

    #@12
    .line 836
    :catch_12
    move-exception v0

    #@13
    .line 837
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@15
    const-string v3, "Dead object in getLastAudibleStreamVolume"

    #@17
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1a
    .line 838
    const/4 v2, 0x0

    #@1b
    goto :goto_c
.end method

.method public getMasterMaxVolume()I
    .registers 5

    #@0
    .prologue
    .line 942
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 944
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    invoke-interface {v1}, Landroid/media/IAudioService;->getMasterMaxVolume()I
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_7} :catch_9

    #@7
    move-result v2

    #@8
    .line 947
    :goto_8
    return v2

    #@9
    .line 945
    :catch_9
    move-exception v0

    #@a
    .line 946
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@c
    const-string v3, "Dead object in getMasterMaxVolume"

    #@e
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@11
    .line 947
    const/4 v2, 0x0

    #@12
    goto :goto_8
.end method

.method public getMasterStreamType()I
    .registers 5

    #@0
    .prologue
    .line 848
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 850
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    invoke-interface {v1}, Landroid/media/IAudioService;->getMasterStreamType()I
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_7} :catch_9

    #@7
    move-result v2

    #@8
    .line 853
    :goto_8
    return v2

    #@9
    .line 851
    :catch_9
    move-exception v0

    #@a
    .line 852
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@c
    const-string v3, "Dead object in getMasterStreamType"

    #@e
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@11
    .line 853
    const/4 v2, 0x2

    #@12
    goto :goto_8
.end method

.method public getMasterVolume()I
    .registers 5

    #@0
    .prologue
    .line 958
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 960
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    invoke-interface {v1}, Landroid/media/IAudioService;->getMasterVolume()I
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_7} :catch_9

    #@7
    move-result v2

    #@8
    .line 963
    :goto_8
    return v2

    #@9
    .line 961
    :catch_9
    move-exception v0

    #@a
    .line 962
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@c
    const-string v3, "Dead object in getMasterVolume"

    #@e
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@11
    .line 963
    const/4 v2, 0x0

    #@12
    goto :goto_8
.end method

.method public getMode()I
    .registers 5

    #@0
    .prologue
    .line 1600
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 1602
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    invoke-interface {v1}, Landroid/media/IAudioService;->getMode()I
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_7} :catch_9

    #@7
    move-result v2

    #@8
    .line 1605
    :goto_8
    return v2

    #@9
    .line 1603
    :catch_9
    move-exception v0

    #@a
    .line 1604
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@c
    const-string v3, "Dead object in getMode"

    #@e
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@11
    .line 1605
    const/4 v2, -0x2

    #@12
    goto :goto_8
.end method

.method public getParameters(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "keys"

    #@0
    .prologue
    .line 1871
    invoke-static {p1}, Landroid/media/AudioSystem;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getProperty(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "key"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2781
    const-string v3, "android.media.property.OUTPUT_SAMPLE_RATE"

    #@3
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6
    move-result v3

    #@7
    if-eqz v3, :cond_14

    #@9
    .line 2782
    invoke-static {}, Landroid/media/AudioSystem;->getPrimaryOutputSamplingRate()I

    #@c
    move-result v1

    #@d
    .line 2783
    .local v1, outputSampleRate:I
    if-lez v1, :cond_13

    #@f
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    .line 2789
    .end local v1           #outputSampleRate:I
    :cond_13
    :goto_13
    return-object v2

    #@14
    .line 2784
    :cond_14
    const-string v3, "android.media.property.OUTPUT_FRAMES_PER_BUFFER"

    #@16
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v3

    #@1a
    if-eqz v3, :cond_13

    #@1c
    .line 2785
    invoke-static {}, Landroid/media/AudioSystem;->getPrimaryOutputFrameCount()I

    #@1f
    move-result v0

    #@20
    .line 2786
    .local v0, outputFramesPerBuffer:I
    if-lez v0, :cond_13

    #@22
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    goto :goto_13
.end method

.method public getRecordHookingState()I
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 3078
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@4
    move-result-object v1

    #@5
    .line 3079
    .local v1, service:Landroid/media/IAudioService;
    if-eqz v1, :cond_15

    #@7
    .line 3081
    :try_start_7
    invoke-interface {v1}, Landroid/media/IAudioService;->getRecordHookingState()I
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_a} :catch_c

    #@a
    move-result v2

    #@b
    .line 3088
    :goto_b
    return v2

    #@c
    .line 3082
    :catch_c
    move-exception v0

    #@d
    .line 3083
    .local v0, e:Landroid/os/RemoteException;
    sget-object v3, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@f
    const-string v4, "Dead object in getRecordHookingState"

    #@11
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@14
    goto :goto_b

    #@15
    .line 3087
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_15
    sget-object v3, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@17
    const-string v4, "AudioService is null in getRecordHookingState"

    #@19
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    goto :goto_b
.end method

.method public getRingerMode()I
    .registers 5

    #@0
    .prologue
    .line 756
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 758
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    invoke-interface {v1}, Landroid/media/IAudioService;->getRingerMode()I
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_7} :catch_9

    #@7
    move-result v2

    #@8
    .line 761
    :goto_8
    return v2

    #@9
    .line 759
    :catch_9
    move-exception v0

    #@a
    .line 760
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@c
    const-string v3, "Dead object in getRingerMode"

    #@e
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@11
    .line 761
    const/4 v2, 0x2

    #@12
    goto :goto_8
.end method

.method public getRingtonePlayer()Landroid/media/IRingtonePlayer;
    .registers 3

    #@0
    .prologue
    .line 2752
    :try_start_0
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1}, Landroid/media/IAudioService;->getRingtonePlayer()Landroid/media/IRingtonePlayer;
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result-object v1

    #@8
    .line 2754
    :goto_8
    return-object v1

    #@9
    .line 2753
    :catch_9
    move-exception v0

    #@a
    .line 2754
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@b
    goto :goto_8
.end method

.method public getRouting(I)I
    .registers 3
    .parameter "mode"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1783
    const/4 v0, -0x1

    #@1
    return v0
.end method

.method public getSmartRingtoneMode()Z
    .registers 8

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 2961
    iget-object v5, p0, Landroid/media/AudioManager;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v5

    #@8
    const v6, 0x2060028

    #@b
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@e
    move-result v0

    #@f
    .line 2962
    .local v0, config_use_smart_ringtone:Z
    if-eqz v0, :cond_38

    #@11
    .line 2963
    iget-object v5, p0, Landroid/media/AudioManager;->mContext:Landroid/content/Context;

    #@13
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@16
    move-result-object v5

    #@17
    const-string/jumbo v6, "smart_ringtone"

    #@1a
    invoke-static {v5, v6, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@1d
    move-result v2

    #@1e
    .line 2965
    .local v2, isSmartRingtoneEnabled:I
    :try_start_1e
    iget-object v5, p0, Landroid/media/AudioManager;->mContext:Landroid/content/Context;

    #@20
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@23
    move-result-object v5

    #@24
    const-string/jumbo v6, "smart_ringtone"

    #@27
    invoke-static {v5, v6, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_2a
    .catch Ljava/lang/SecurityException; {:try_start_1e .. :try_end_2a} :catch_2d

    #@2a
    .line 2969
    :goto_2a
    if-ne v2, v3, :cond_36

    #@2c
    .line 2971
    .end local v2           #isSmartRingtoneEnabled:I
    :goto_2c
    return v3

    #@2d
    .line 2966
    .restart local v2       #isSmartRingtoneEnabled:I
    :catch_2d
    move-exception v1

    #@2e
    .line 2967
    .local v1, e:Ljava/lang/SecurityException;
    sget-object v5, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@30
    const-string v6, "[smart ringtone] AudioManager:getSmartRingtoneMode: "

    #@32
    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@35
    goto :goto_2a

    #@36
    .end local v1           #e:Ljava/lang/SecurityException;
    :cond_36
    move v3, v4

    #@37
    .line 2969
    goto :goto_2c

    #@38
    .end local v2           #isSmartRingtoneEnabled:I
    :cond_38
    move v3, v4

    #@39
    .line 2971
    goto :goto_2c
.end method

.method public getStreamMaxVolume(I)I
    .registers 6
    .parameter "streamType"

    #@0
    .prologue
    .line 788
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 790
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    iget-boolean v2, p0, Landroid/media/AudioManager;->mUseMasterVolume:Z

    #@6
    if-eqz v2, :cond_d

    #@8
    .line 791
    invoke-interface {v1}, Landroid/media/IAudioService;->getMasterMaxVolume()I

    #@b
    move-result v2

    #@c
    .line 797
    :goto_c
    return v2

    #@d
    .line 793
    :cond_d
    invoke-interface {v1, p1}, Landroid/media/IAudioService;->getStreamMaxVolume(I)I
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_10} :catch_12

    #@10
    move-result v2

    #@11
    goto :goto_c

    #@12
    .line 795
    :catch_12
    move-exception v0

    #@13
    .line 796
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@15
    const-string v3, "Dead object in getStreamMaxVolume"

    #@17
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1a
    .line 797
    const/4 v2, 0x0

    #@1b
    goto :goto_c
.end method

.method public getStreamVolume(I)I
    .registers 6
    .parameter "streamType"

    #@0
    .prologue
    .line 810
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 812
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    iget-boolean v2, p0, Landroid/media/AudioManager;->mUseMasterVolume:Z

    #@6
    if-eqz v2, :cond_d

    #@8
    .line 813
    invoke-interface {v1}, Landroid/media/IAudioService;->getMasterVolume()I

    #@b
    move-result v2

    #@c
    .line 819
    :goto_c
    return v2

    #@d
    .line 815
    :cond_d
    invoke-interface {v1, p1}, Landroid/media/IAudioService;->getStreamVolume(I)I
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_10} :catch_12

    #@10
    move-result v2

    #@11
    goto :goto_c

    #@12
    .line 817
    :catch_12
    move-exception v0

    #@13
    .line 818
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@15
    const-string v3, "Dead object in getStreamVolume"

    #@17
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1a
    .line 819
    const/4 v2, 0x0

    #@1b
    goto :goto_c
.end method

.method public getVibrateSetting(I)I
    .registers 6
    .parameter "vibrateType"

    #@0
    .prologue
    .line 1176
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 1178
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    invoke-interface {v1, p1}, Landroid/media/IAudioService;->getVibrateSetting(I)I
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_7} :catch_9

    #@7
    move-result v2

    #@8
    .line 1181
    :goto_8
    return v2

    #@9
    .line 1179
    :catch_9
    move-exception v0

    #@a
    .line 1180
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@c
    const-string v3, "Dead object in getVibrateSetting"

    #@e
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@11
    .line 1181
    const/4 v2, 0x0

    #@12
    goto :goto_8
.end method

.method public getVoiceActivationState()I
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 3020
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@4
    move-result-object v1

    #@5
    .line 3021
    .local v1, service:Landroid/media/IAudioService;
    if-eqz v1, :cond_15

    #@7
    .line 3023
    :try_start_7
    invoke-interface {v1}, Landroid/media/IAudioService;->getVoiceActivationState()I
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_a} :catch_c

    #@a
    move-result v2

    #@b
    .line 3030
    :goto_b
    return v2

    #@c
    .line 3024
    :catch_c
    move-exception v0

    #@d
    .line 3025
    .local v0, e:Landroid/os/RemoteException;
    sget-object v3, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@f
    const-string v4, "Dead object in getVoiceActivationState"

    #@11
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@14
    goto :goto_b

    #@15
    .line 3029
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_15
    sget-object v3, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@17
    const-string v4, "AudioService is null in getVoiceActivationState"

    #@19
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    goto :goto_b
.end method

.method public handleKeyDown(Landroid/view/KeyEvent;I)V
    .registers 11
    .parameter "event"
    .parameter "stream"

    #@0
    .prologue
    const/16 v7, 0x18

    #@2
    const/4 v3, -0x1

    #@3
    const/4 v2, 0x1

    #@4
    .line 574
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@7
    move-result v1

    #@8
    .line 575
    .local v1, keyCode:I
    sget-object v4, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@a
    new-instance v5, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v6, "handleKeyDown() keycode = "

    #@11
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v5

    #@15
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v5

    #@19
    const-string v6, ", stream = "

    #@1b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v5

    #@1f
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v5

    #@23
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v5

    #@27
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 576
    sparse-switch v1, :sswitch_data_5a

    #@2d
    .line 610
    :cond_2d
    :goto_2d
    return-void

    #@2e
    .line 583
    :sswitch_2e
    const/16 v0, 0x11

    #@30
    .line 585
    .local v0, flags:I
    iget-boolean v4, p0, Landroid/media/AudioManager;->mUseMasterVolume:Z

    #@32
    if-eqz v4, :cond_3c

    #@34
    .line 586
    if-ne v1, v7, :cond_3a

    #@36
    :goto_36
    invoke-virtual {p0, v2, v0}, Landroid/media/AudioManager;->adjustMasterVolume(II)V

    #@39
    goto :goto_2d

    #@3a
    :cond_3a
    move v2, v3

    #@3b
    goto :goto_36

    #@3c
    .line 592
    :cond_3c
    if-ne v1, v7, :cond_42

    #@3e
    :goto_3e
    invoke-virtual {p0, v2, p2, v0}, Landroid/media/AudioManager;->adjustSuggestedStreamVolume(III)V

    #@41
    goto :goto_2d

    #@42
    :cond_42
    move v2, v3

    #@43
    goto :goto_3e

    #@44
    .line 601
    .end local v0           #flags:I
    :sswitch_44
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@47
    move-result v3

    #@48
    if-nez v3, :cond_2d

    #@4a
    .line 602
    iget-boolean v3, p0, Landroid/media/AudioManager;->mUseMasterVolume:Z

    #@4c
    if-eqz v3, :cond_2d

    #@4e
    .line 603
    invoke-virtual {p0}, Landroid/media/AudioManager;->isMasterMute()Z

    #@51
    move-result v3

    #@52
    if-nez v3, :cond_58

    #@54
    :goto_54
    invoke-virtual {p0, v2}, Landroid/media/AudioManager;->setMasterMute(Z)V

    #@57
    goto :goto_2d

    #@58
    :cond_58
    const/4 v2, 0x0

    #@59
    goto :goto_54

    #@5a
    .line 576
    :sswitch_data_5a
    .sparse-switch
        0x18 -> :sswitch_2e
        0x19 -> :sswitch_2e
        0xa4 -> :sswitch_44
    .end sparse-switch
.end method

.method public handleKeyUp(Landroid/view/KeyEvent;I)V
    .registers 9
    .parameter "event"
    .parameter "stream"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 616
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@4
    move-result v1

    #@5
    .line 617
    .local v1, keyCode:I
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@7
    new-instance v3, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v4, "handleKeyUp() keycode = "

    #@e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    const-string v4, ", stream = "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 618
    packed-switch v1, :pswitch_data_44

    #@2a
    .line 639
    :goto_2a
    return-void

    #@2b
    .line 625
    :pswitch_2b
    iget-boolean v2, p0, Landroid/media/AudioManager;->mUseVolumeKeySounds:Z

    #@2d
    if-eqz v2, :cond_37

    #@2f
    .line 626
    iget-boolean v2, p0, Landroid/media/AudioManager;->mUseMasterVolume:Z

    #@31
    if-eqz v2, :cond_3e

    #@33
    .line 627
    const/4 v2, 0x4

    #@34
    invoke-virtual {p0, v5, v2}, Landroid/media/AudioManager;->adjustMasterVolume(II)V

    #@37
    .line 636
    :cond_37
    :goto_37
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3a
    move-result-wide v2

    #@3b
    iput-wide v2, p0, Landroid/media/AudioManager;->mVolumeKeyUpTime:J

    #@3d
    goto :goto_2a

    #@3e
    .line 629
    :cond_3e
    const/4 v0, 0x4

    #@3f
    .line 630
    .local v0, flags:I
    invoke-virtual {p0, v5, p2, v0}, Landroid/media/AudioManager;->adjustSuggestedStreamVolume(III)V

    #@42
    goto :goto_37

    #@43
    .line 618
    nop

    #@44
    :pswitch_data_44
    .packed-switch 0x18
        :pswitch_2b
        :pswitch_2b
    .end packed-switch
.end method

.method public isBluetoothA2dpOn()Z
    .registers 3

    #@0
    .prologue
    .line 1447
    const/16 v0, 0x80

    #@2
    const-string v1, ""

    #@4
    invoke-static {v0, v1}, Landroid/media/AudioSystem;->getDeviceConnectionState(ILjava/lang/String;)I

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_c

    #@a
    .line 1449
    const/4 v0, 0x0

    #@b
    .line 1451
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    goto :goto_b
.end method

.method public isBluetoothAGConnected()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1460
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@4
    move-result-object v1

    #@5
    .line 1462
    .local v1, service:Landroid/media/IAudioService;
    :try_start_5
    invoke-interface {v1}, Landroid/media/IAudioService;->isBluetoothAGConnected()Z
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_8} :catch_a
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_8} :catch_13

    #@8
    move-result v2

    #@9
    .line 1468
    :goto_9
    return v2

    #@a
    .line 1463
    :catch_a
    move-exception v0

    #@b
    .line 1464
    .local v0, e:Landroid/os/RemoteException;
    sget-object v3, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@d
    const-string v4, "Dead object in isBluetoothAGConnected"

    #@f
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@12
    goto :goto_9

    #@13
    .line 1466
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_13
    move-exception v0

    #@14
    .line 1467
    .local v0, e:Ljava/lang/Exception;
    sget-object v3, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@16
    const-string v4, "Exception in isBluetoothAGConnected"

    #@18
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1b
    goto :goto_9
.end method

.method public isBluetoothAVConnected()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1475
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@4
    move-result-object v1

    #@5
    .line 1477
    .local v1, service:Landroid/media/IAudioService;
    :try_start_5
    invoke-interface {v1}, Landroid/media/IAudioService;->isBluetoothAVConnected()Z
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_8} :catch_a
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_8} :catch_13

    #@8
    move-result v2

    #@9
    .line 1483
    :goto_9
    return v2

    #@a
    .line 1478
    :catch_a
    move-exception v0

    #@b
    .line 1479
    .local v0, e:Landroid/os/RemoteException;
    sget-object v3, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@d
    const-string v4, "Dead object in isBluetoothAVConnected"

    #@f
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@12
    goto :goto_9

    #@13
    .line 1481
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_13
    move-exception v0

    #@14
    .line 1482
    .local v0, e:Ljava/lang/Exception;
    sget-object v3, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@16
    const-string v4, "Exception in isBluetoothAVConnected"

    #@18
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1b
    goto :goto_9
.end method

.method public isBluetoothScoAvailableOffCall()Z
    .registers 3

    #@0
    .prologue
    .line 1327
    iget-object v0, p0, Landroid/media/AudioManager;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v0

    #@6
    const v1, 0x111002c

    #@9
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@c
    move-result v0

    #@d
    return v0
.end method

.method public isBluetoothScoOn()Z
    .registers 5

    #@0
    .prologue
    .line 1423
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 1425
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    invoke-interface {v1}, Landroid/media/IAudioService;->isBluetoothScoOn()Z
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_7} :catch_9

    #@7
    move-result v2

    #@8
    .line 1428
    :goto_8
    return v2

    #@9
    .line 1426
    :catch_9
    move-exception v0

    #@a
    .line 1427
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@c
    const-string v3, "Dead object in isBluetoothScoOn"

    #@e
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@11
    .line 1428
    const/4 v2, 0x0

    #@12
    goto :goto_8
.end method

.method public isFMActive()Z
    .registers 3

    #@0
    .prologue
    .line 1793
    const/16 v0, 0xa

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v0, v1}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public isMasterMute()Z
    .registers 5

    #@0
    .prologue
    .line 1104
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 1106
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    invoke-interface {v1}, Landroid/media/IAudioService;->isMasterMute()Z
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_7} :catch_9

    #@7
    move-result v2

    #@8
    .line 1109
    :goto_8
    return v2

    #@9
    .line 1107
    :catch_9
    move-exception v0

    #@a
    .line 1108
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@c
    const-string v3, "Dead object in isMasterMute"

    #@e
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@11
    .line 1109
    const/4 v2, 0x0

    #@12
    goto :goto_8
.end method

.method public isMicrophoneMute()Z
    .registers 2

    #@0
    .prologue
    .line 1565
    invoke-static {}, Landroid/media/AudioSystem;->isMicrophoneMuted()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public isMusicActive()Z
    .registers 3

    #@0
    .prologue
    .line 1802
    const/4 v0, 0x3

    #@1
    const/4 v1, 0x0

    #@2
    invoke-static {v0, v1}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isRecording()Z
    .registers 3

    #@0
    .prologue
    .line 2839
    const-string v1, "audiorecording_state"

    #@2
    invoke-static {v1}, Landroid/media/AudioSystem;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 2840
    .local v0, isRecording:Ljava/lang/String;
    const-string/jumbo v1, "off"

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_11

    #@f
    .line 2841
    const/4 v1, 0x0

    #@10
    .line 2843
    :goto_10
    return v1

    #@11
    :cond_11
    const/4 v1, 0x1

    #@12
    goto :goto_10
.end method

.method public isSilentMode()Z
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 2595
    invoke-virtual {p0}, Landroid/media/AudioManager;->getRingerMode()I

    #@4
    move-result v0

    #@5
    .line 2596
    .local v0, ringerMode:I
    if-eqz v0, :cond_9

    #@7
    if-ne v0, v1, :cond_a

    #@9
    .line 2599
    .local v1, silentMode:Z
    :cond_9
    :goto_9
    return v1

    #@a
    .line 2596
    .end local v1           #silentMode:Z
    :cond_a
    const/4 v1, 0x0

    #@b
    goto :goto_9
.end method

.method public isSpeakerOnForMedia()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2817
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@4
    move-result-object v1

    #@5
    .line 2818
    .local v1, service:Landroid/media/IAudioService;
    if-eqz v1, :cond_15

    #@7
    .line 2820
    :try_start_7
    invoke-interface {v1}, Landroid/media/IAudioService;->isSpeakerOnForMedia()Z
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_a} :catch_c

    #@a
    move-result v2

    #@b
    .line 2827
    :goto_b
    return v2

    #@c
    .line 2821
    :catch_c
    move-exception v0

    #@d
    .line 2822
    .local v0, e:Landroid/os/RemoteException;
    sget-object v3, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@f
    const-string v4, "Dead object in isSpeakerOnForMedia"

    #@11
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@14
    goto :goto_b

    #@15
    .line 2826
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_15
    sget-object v3, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@17
    const-string v4, "AudioService is null in isSpeakerOnForMedia"

    #@19
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    goto :goto_b
.end method

.method public isSpeakerphoneOn()Z
    .registers 5

    #@0
    .prologue
    .line 1236
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 1238
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    invoke-interface {v1}, Landroid/media/IAudioService;->isSpeakerphoneOn()Z
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_7} :catch_9

    #@7
    move-result v2

    #@8
    .line 1241
    :goto_8
    return v2

    #@9
    .line 1239
    :catch_9
    move-exception v0

    #@a
    .line 1240
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@c
    const-string v3, "Dead object in isSpeakerphoneOn"

    #@e
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@11
    .line 1241
    const/4 v2, 0x0

    #@12
    goto :goto_8
.end method

.method public isSpeechRecognitionActive()Z
    .registers 2

    #@0
    .prologue
    .line 1812
    const/4 v0, 0x6

    #@1
    invoke-static {v0}, Landroid/media/AudioSystem;->isSourceActive(I)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public isStreamMute(I)Z
    .registers 6
    .parameter "streamType"

    #@0
    .prologue
    .line 1066
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 1068
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    invoke-interface {v1, p1}, Landroid/media/IAudioService;->isStreamMute(I)Z
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_7} :catch_9

    #@7
    move-result v2

    #@8
    .line 1071
    :goto_8
    return v2

    #@9
    .line 1069
    :catch_9
    move-exception v0

    #@a
    .line 1070
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@c
    const-string v3, "Dead object in isStreamMute"

    #@e
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@11
    .line 1071
    const/4 v2, 0x0

    #@12
    goto :goto_8
.end method

.method public isWiredHeadsetConnected()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1490
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@4
    move-result-object v1

    #@5
    .line 1492
    .local v1, service:Landroid/media/IAudioService;
    :try_start_5
    invoke-interface {v1}, Landroid/media/IAudioService;->isWiredHeadsetConnected()Z
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_8} :catch_a
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_8} :catch_13

    #@8
    move-result v2

    #@9
    .line 1498
    :goto_9
    return v2

    #@a
    .line 1493
    :catch_a
    move-exception v0

    #@b
    .line 1494
    .local v0, e:Landroid/os/RemoteException;
    sget-object v3, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@d
    const-string v4, "Dead object in isWiredHeadsetConnected"

    #@f
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@12
    goto :goto_9

    #@13
    .line 1496
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_13
    move-exception v0

    #@14
    .line 1497
    .local v0, e:Ljava/lang/Exception;
    sget-object v3, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@16
    const-string v4, "Exception in isWiredHeadsetConnected"

    #@18
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1b
    goto :goto_9
.end method

.method public isWiredHeadsetOn()Z
    .registers 3

    #@0
    .prologue
    .line 1536
    const/4 v0, 0x4

    #@1
    const-string v1, ""

    #@3
    invoke-static {v0, v1}, Landroid/media/AudioSystem;->getDeviceConnectionState(ILjava/lang/String;)I

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_15

    #@9
    const/16 v0, 0x8

    #@b
    const-string v1, ""

    #@d
    invoke-static {v0, v1}, Landroid/media/AudioSystem;->getDeviceConnectionState(ILjava/lang/String;)I

    #@10
    move-result v0

    #@11
    if-nez v0, :cond_15

    #@13
    .line 1540
    const/4 v0, 0x0

    #@14
    .line 1542
    :goto_14
    return v0

    #@15
    :cond_15
    const/4 v0, 0x1

    #@16
    goto :goto_14
.end method

.method public loadSoundEffects()V
    .registers 6

    #@0
    .prologue
    .line 2014
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 2016
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    invoke-interface {v1}, Landroid/media/IAudioService;->loadSoundEffects()Z
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_7} :catch_8

    #@7
    .line 2020
    :goto_7
    return-void

    #@8
    .line 2017
    :catch_8
    move-exception v0

    #@9
    .line 2018
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@b
    new-instance v3, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v4, "Dead object in loadSoundEffects"

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    goto :goto_7
.end method

.method public openRecordHooking(II)Ljava/io/FileDescriptor;
    .registers 4
    .parameter "sampleRate"
    .parameter "flag"

    #@0
    .prologue
    .line 2921
    const/4 v0, 0x1

    #@1
    invoke-static {v0, p1, p2}, Landroid/media/AudioSystem;->setRecordHookingEnabled(III)Ljava/io/FileDescriptor;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public playSoundEffect(I)V
    .registers 7
    .parameter "effectType"

    #@0
    .prologue
    .line 1951
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v4, "playSoundEffect effectType = "

    #@a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v3

    #@16
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 1952
    if-ltz p1, :cond_1f

    #@1b
    const/16 v2, 0xb

    #@1d
    if-lt p1, v2, :cond_20

    #@1f
    .line 1967
    :cond_1f
    :goto_1f
    return-void

    #@20
    .line 1956
    :cond_20
    invoke-direct {p0}, Landroid/media/AudioManager;->querySoundEffectsEnabled()Z

    #@23
    move-result v2

    #@24
    if-nez v2, :cond_44

    #@26
    .line 1957
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@28
    new-instance v3, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string/jumbo v4, "playSoundEffect querySoundEffectsEnabled = "

    #@30
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    invoke-direct {p0}, Landroid/media/AudioManager;->querySoundEffectsEnabled()Z

    #@37
    move-result v4

    #@38
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v3

    #@3c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v3

    #@40
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    goto :goto_1f

    #@44
    .line 1961
    :cond_44
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@47
    move-result-object v1

    #@48
    .line 1963
    .local v1, service:Landroid/media/IAudioService;
    :try_start_48
    invoke-interface {v1, p1}, Landroid/media/IAudioService;->playSoundEffect(I)V
    :try_end_4b
    .catch Landroid/os/RemoteException; {:try_start_48 .. :try_end_4b} :catch_4c

    #@4b
    goto :goto_1f

    #@4c
    .line 1964
    :catch_4c
    move-exception v0

    #@4d
    .line 1965
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@4f
    new-instance v3, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    const-string v4, "Dead object in playSoundEffect"

    #@56
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v3

    #@5a
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v3

    #@5e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v3

    #@62
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@65
    goto :goto_1f
.end method

.method public playSoundEffect(IF)V
    .registers 8
    .parameter "effectType"
    .parameter "volume"

    #@0
    .prologue
    .line 1988
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v4, "playSoundEffect effectType = "

    #@a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v3

    #@12
    const-string v4, " volume = "

    #@14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 1989
    if-ltz p1, :cond_29

    #@25
    const/16 v2, 0xb

    #@27
    if-lt p1, v2, :cond_2a

    #@29
    .line 1999
    :cond_29
    :goto_29
    return-void

    #@2a
    .line 1993
    :cond_2a
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@2d
    move-result-object v1

    #@2e
    .line 1995
    .local v1, service:Landroid/media/IAudioService;
    :try_start_2e
    invoke-interface {v1, p1, p2}, Landroid/media/IAudioService;->playSoundEffectVolume(IF)V
    :try_end_31
    .catch Landroid/os/RemoteException; {:try_start_2e .. :try_end_31} :catch_32

    #@31
    goto :goto_29

    #@32
    .line 1996
    :catch_32
    move-exception v0

    #@33
    .line 1997
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@35
    new-instance v3, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v4, "Dead object in playSoundEffect"

    #@3c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v3

    #@44
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v3

    #@48
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    goto :goto_29
.end method

.method public preDispatchKeyEvent(Landroid/view/KeyEvent;I)V
    .registers 10
    .parameter "event"
    .parameter "stream"

    #@0
    .prologue
    const/16 v6, 0x8

    #@2
    const/4 v5, 0x0

    #@3
    .line 552
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@6
    move-result v0

    #@7
    .line 553
    .local v0, keyCode:I
    const/16 v1, 0x19

    #@9
    if-eq v0, v1, :cond_27

    #@b
    const/16 v1, 0x18

    #@d
    if-eq v0, v1, :cond_27

    #@f
    const/16 v1, 0xa4

    #@11
    if-eq v0, v1, :cond_27

    #@13
    iget-wide v1, p0, Landroid/media/AudioManager;->mVolumeKeyUpTime:J

    #@15
    const-wide/16 v3, 0x32

    #@17
    add-long/2addr v1, v3

    #@18
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@1b
    move-result-wide v3

    #@1c
    cmp-long v1, v1, v3

    #@1e
    if-lez v1, :cond_27

    #@20
    .line 561
    iget-boolean v1, p0, Landroid/media/AudioManager;->mUseMasterVolume:Z

    #@22
    if-eqz v1, :cond_28

    #@24
    .line 562
    invoke-virtual {p0, v5, v6}, Landroid/media/AudioManager;->adjustMasterVolume(II)V

    #@27
    .line 568
    :cond_27
    :goto_27
    return-void

    #@28
    .line 564
    :cond_28
    invoke-virtual {p0, v5, p2, v6}, Landroid/media/AudioManager;->adjustSuggestedStreamVolume(III)V

    #@2b
    goto :goto_27
.end method

.method public registerAudioFocusListener(Landroid/media/AudioManager$OnAudioFocusChangeListener;)V
    .registers 5
    .parameter "l"

    #@0
    .prologue
    .line 2183
    iget-object v1, p0, Landroid/media/AudioManager;->mFocusListenerLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 2184
    :try_start_3
    iget-object v0, p0, Landroid/media/AudioManager;->mAudioFocusIdListenerMap:Ljava/util/HashMap;

    #@5
    invoke-direct {p0, p1}, Landroid/media/AudioManager;->getIdForAudioFocusListener(Landroid/media/AudioManager$OnAudioFocusChangeListener;)Ljava/lang/String;

    #@8
    move-result-object v2

    #@9
    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_11

    #@f
    .line 2185
    monitor-exit v1

    #@10
    .line 2189
    :goto_10
    return-void

    #@11
    .line 2187
    :cond_11
    iget-object v0, p0, Landroid/media/AudioManager;->mAudioFocusIdListenerMap:Ljava/util/HashMap;

    #@13
    invoke-direct {p0, p1}, Landroid/media/AudioManager;->getIdForAudioFocusListener(Landroid/media/AudioManager$OnAudioFocusChangeListener;)Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v0, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1a
    .line 2188
    monitor-exit v1

    #@1b
    goto :goto_10

    #@1c
    :catchall_1c
    move-exception v0

    #@1d
    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_3 .. :try_end_1e} :catchall_1c

    #@1e
    throw v0
.end method

.method public registerMediaButtonEventReceiver(Landroid/content/ComponentName;)V
    .registers 7
    .parameter "eventReceiver"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 2314
    if-nez p1, :cond_4

    #@3
    .line 2329
    :goto_3
    return-void

    #@4
    .line 2317
    :cond_4
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@7
    move-result-object v2

    #@8
    iget-object v3, p0, Landroid/media/AudioManager;->mContext:Landroid/content/Context;

    #@a
    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v2

    #@12
    if-nez v2, :cond_1d

    #@14
    .line 2318
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@16
    const-string/jumbo v3, "registerMediaButtonEventReceiver() error: receiver and context package names don\'t match"

    #@19
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    goto :goto_3

    #@1d
    .line 2323
    :cond_1d
    new-instance v0, Landroid/content/Intent;

    #@1f
    const-string v2, "android.intent.action.MEDIA_BUTTON"

    #@21
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@24
    .line 2325
    .local v0, mediaButtonIntent:Landroid/content/Intent;
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@27
    .line 2326
    iget-object v2, p0, Landroid/media/AudioManager;->mContext:Landroid/content/Context;

    #@29
    invoke-static {v2, v4, v0, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@2c
    move-result-object v1

    #@2d
    .line 2328
    .local v1, pi:Landroid/app/PendingIntent;
    invoke-virtual {p0, v1, p1}, Landroid/media/AudioManager;->registerMediaButtonIntent(Landroid/app/PendingIntent;Landroid/content/ComponentName;)V

    #@30
    goto :goto_3
.end method

.method public registerMediaButtonEventReceiverForCalls(Landroid/content/ComponentName;)V
    .registers 6
    .parameter "eventReceiver"

    #@0
    .prologue
    .line 2356
    if-nez p1, :cond_3

    #@2
    .line 2366
    :goto_2
    return-void

    #@3
    .line 2359
    :cond_3
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@6
    move-result-object v1

    #@7
    .line 2362
    .local v1, service:Landroid/media/IAudioService;
    :try_start_7
    invoke-interface {v1, p1}, Landroid/media/IAudioService;->registerMediaButtonEventReceiverForCalls(Landroid/content/ComponentName;)V
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_a} :catch_b

    #@a
    goto :goto_2

    #@b
    .line 2363
    :catch_b
    move-exception v0

    #@c
    .line 2364
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@e
    const-string v3, "Dead object in registerMediaButtonEventReceiverForCalls"

    #@10
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@13
    goto :goto_2
.end method

.method public registerMediaButtonIntent(Landroid/app/PendingIntent;Landroid/content/ComponentName;)V
    .registers 8
    .parameter "pi"
    .parameter "eventReceiver"

    #@0
    .prologue
    .line 2336
    if-eqz p1, :cond_4

    #@2
    if-nez p2, :cond_c

    #@4
    .line 2337
    :cond_4
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@6
    const-string v3, "Cannot call registerMediaButtonIntent() with a null parameter"

    #@8
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 2347
    :goto_b
    return-void

    #@c
    .line 2340
    :cond_c
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@f
    move-result-object v1

    #@10
    .line 2343
    .local v1, service:Landroid/media/IAudioService;
    :try_start_10
    invoke-interface {v1, p1, p2}, Landroid/media/IAudioService;->registerMediaButtonIntent(Landroid/app/PendingIntent;Landroid/content/ComponentName;)V
    :try_end_13
    .catch Landroid/os/RemoteException; {:try_start_10 .. :try_end_13} :catch_14

    #@13
    goto :goto_b

    #@14
    .line 2344
    :catch_14
    move-exception v0

    #@15
    .line 2345
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@17
    new-instance v3, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v4, "Dead object in registerMediaButtonIntent"

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    goto :goto_b
.end method

.method public registerRemoteControlClient(Landroid/media/RemoteControlClient;)V
    .registers 8
    .parameter "rcClient"

    #@0
    .prologue
    .line 2418
    if-eqz p1, :cond_8

    #@2
    invoke-virtual {p1}, Landroid/media/RemoteControlClient;->getRcMediaIntent()Landroid/app/PendingIntent;

    #@5
    move-result-object v3

    #@6
    if-nez v3, :cond_9

    #@8
    .line 2432
    :cond_8
    :goto_8
    return-void

    #@9
    .line 2421
    :cond_9
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@c
    move-result-object v2

    #@d
    .line 2423
    .local v2, service:Landroid/media/IAudioService;
    :try_start_d
    invoke-virtual {p1}, Landroid/media/RemoteControlClient;->getRcMediaIntent()Landroid/app/PendingIntent;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {p1}, Landroid/media/RemoteControlClient;->getIRemoteControlClient()Landroid/media/IRemoteControlClient;

    #@14
    move-result-object v4

    #@15
    iget-object v5, p0, Landroid/media/AudioManager;->mContext:Landroid/content/Context;

    #@17
    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@1a
    move-result-object v5

    #@1b
    invoke-interface {v2, v3, v4, v5}, Landroid/media/IAudioService;->registerRemoteControlClient(Landroid/app/PendingIntent;Landroid/media/IRemoteControlClient;Ljava/lang/String;)I

    #@1e
    move-result v1

    #@1f
    .line 2428
    .local v1, rcseId:I
    invoke-virtual {p1, v1}, Landroid/media/RemoteControlClient;->setRcseId(I)V
    :try_end_22
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_22} :catch_23

    #@22
    goto :goto_8

    #@23
    .line 2429
    .end local v1           #rcseId:I
    :catch_23
    move-exception v0

    #@24
    .line 2430
    .local v0, e:Landroid/os/RemoteException;
    sget-object v3, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@26
    new-instance v4, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v5, "Dead object in registerRemoteControlClient"

    #@2d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v4

    #@39
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    goto :goto_8
.end method

.method public registerRemoteControlDisplay(Landroid/media/IRemoteControlDisplay;)V
    .registers 7
    .parameter "rcd"

    #@0
    .prologue
    .line 2459
    if-nez p1, :cond_3

    #@2
    .line 2468
    :goto_2
    return-void

    #@3
    .line 2462
    :cond_3
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@6
    move-result-object v1

    #@7
    .line 2464
    .local v1, service:Landroid/media/IAudioService;
    :try_start_7
    invoke-interface {v1, p1}, Landroid/media/IAudioService;->registerRemoteControlDisplay(Landroid/media/IRemoteControlDisplay;)V
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_a} :catch_b

    #@a
    goto :goto_2

    #@b
    .line 2465
    :catch_b
    move-exception v0

    #@c
    .line 2466
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@e
    new-instance v3, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v4, "Dead object in registerRemoteControlDisplay "

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    goto :goto_2
.end method

.method public reloadAudioSettings()V
    .registers 6

    #@0
    .prologue
    .line 2572
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 2574
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    invoke-interface {v1}, Landroid/media/IAudioService;->reloadAudioSettings()V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_7} :catch_8

    #@7
    .line 2578
    :goto_7
    return-void

    #@8
    .line 2575
    :catch_8
    move-exception v0

    #@9
    .line 2576
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@b
    new-instance v3, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v4, "Dead object in reloadAudioSettings"

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    goto :goto_7
.end method

.method public remoteControlDisplayUsesBitmapSize(Landroid/media/IRemoteControlDisplay;II)V
    .registers 9
    .parameter "rcd"
    .parameter "w"
    .parameter "h"

    #@0
    .prologue
    .line 2497
    if-nez p1, :cond_3

    #@2
    .line 2506
    :goto_2
    return-void

    #@3
    .line 2500
    :cond_3
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@6
    move-result-object v1

    #@7
    .line 2502
    .local v1, service:Landroid/media/IAudioService;
    :try_start_7
    invoke-interface {v1, p1, p2, p3}, Landroid/media/IAudioService;->remoteControlDisplayUsesBitmapSize(Landroid/media/IRemoteControlDisplay;II)V
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_a} :catch_b

    #@a
    goto :goto_2

    #@b
    .line 2503
    :catch_b
    move-exception v0

    #@c
    .line 2504
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@e
    new-instance v3, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v4, "Dead object in remoteControlDisplayUsesBitmapSize "

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    goto :goto_2
.end method

.method public removeMediaButtonEventReceiverForCalls(Landroid/content/ComponentName;)V
    .registers 6
    .parameter "eventReceiver"

    #@0
    .prologue
    .line 2876
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 2877
    .local v1, service:Landroid/media/IAudioService;
    if-eqz v1, :cond_13

    #@6
    .line 2879
    :try_start_6
    invoke-interface {v1, p1}, Landroid/media/IAudioService;->removeMediaButtonEventReceiverForCalls(Landroid/content/ComponentName;)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_9} :catch_a

    #@9
    .line 2886
    :goto_9
    return-void

    #@a
    .line 2880
    :catch_a
    move-exception v0

    #@b
    .line 2881
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@d
    const-string v3, "Dead object in unregisterMediaButtonEventReceiverForCalls"

    #@f
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@12
    goto :goto_9

    #@13
    .line 2884
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_13
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@15
    const-string v3, "AudioService is null in unregisterMediaButtonEventReceiverForCalls"

    #@17
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    goto :goto_9
.end method

.method public requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I
    .registers 14
    .parameter "l"
    .parameter "streamType"
    .parameter "durationHint"

    #@0
    .prologue
    .line 2230
    const/4 v8, 0x0

    #@1
    .line 2231
    .local v8, status:I
    const/4 v1, 0x1

    #@2
    if-lt p3, v1, :cond_7

    #@4
    const/4 v1, 0x3

    #@5
    if-le p3, v1, :cond_10

    #@7
    .line 2233
    :cond_7
    sget-object v1, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@9
    const-string v2, "Invalid duration hint, audio focus request denied"

    #@b
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    move v9, v8

    #@f
    .line 2246
    .end local v8           #status:I
    .local v9, status:I
    :goto_f
    return v9

    #@10
    .line 2236
    .end local v9           #status:I
    .restart local v8       #status:I
    :cond_10
    invoke-virtual {p0, p1}, Landroid/media/AudioManager;->registerAudioFocusListener(Landroid/media/AudioManager$OnAudioFocusChangeListener;)V

    #@13
    .line 2238
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@16
    move-result-object v0

    #@17
    .line 2240
    .local v0, service:Landroid/media/IAudioService;
    :try_start_17
    iget-object v3, p0, Landroid/media/AudioManager;->mICallBack:Landroid/os/IBinder;

    #@19
    iget-object v4, p0, Landroid/media/AudioManager;->mAudioFocusDispatcher:Landroid/media/IAudioFocusDispatcher;

    #@1b
    invoke-direct {p0, p1}, Landroid/media/AudioManager;->getIdForAudioFocusListener(Landroid/media/AudioManager$OnAudioFocusChangeListener;)Ljava/lang/String;

    #@1e
    move-result-object v5

    #@1f
    iget-object v1, p0, Landroid/media/AudioManager;->mContext:Landroid/content/Context;

    #@21
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@24
    move-result-object v6

    #@25
    move v1, p2

    #@26
    move v2, p3

    #@27
    invoke-interface/range {v0 .. v6}, Landroid/media/IAudioService;->requestAudioFocus(IILandroid/os/IBinder;Landroid/media/IAudioFocusDispatcher;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2a
    .catch Landroid/os/RemoteException; {:try_start_17 .. :try_end_2a} :catch_2d

    #@2a
    move-result v8

    #@2b
    :goto_2b
    move v9, v8

    #@2c
    .line 2246
    .end local v8           #status:I
    .restart local v9       #status:I
    goto :goto_f

    #@2d
    .line 2243
    .end local v9           #status:I
    .restart local v8       #status:I
    :catch_2d
    move-exception v7

    #@2e
    .line 2244
    .local v7, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@30
    new-instance v2, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v3, "Can\'t call requestAudioFocus() on AudioService due to "

    #@37
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v2

    #@43
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    goto :goto_2b
.end method

.method public requestAudioFocusForCall(II)V
    .registers 11
    .parameter "streamType"
    .parameter "durationHint"

    #@0
    .prologue
    .line 2260
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v0

    #@4
    .line 2262
    .local v0, service:Landroid/media/IAudioService;
    :try_start_4
    iget-object v3, p0, Landroid/media/AudioManager;->mICallBack:Landroid/os/IBinder;

    #@6
    const/4 v4, 0x0

    #@7
    const-string v5, "AudioFocus_For_Phone_Ring_And_Calls"

    #@9
    const-string/jumbo v6, "system"

    #@c
    move v1, p1

    #@d
    move v2, p2

    #@e
    invoke-interface/range {v0 .. v6}, Landroid/media/IAudioService;->requestAudioFocus(IILandroid/os/IBinder;Landroid/media/IAudioFocusDispatcher;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_11
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_11} :catch_12

    #@11
    .line 2268
    :goto_11
    return-void

    #@12
    .line 2265
    :catch_12
    move-exception v7

    #@13
    .line 2266
    .local v7, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@15
    new-instance v2, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v3, "Can\'t call requestAudioFocusForCall() on AudioService due to "

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    goto :goto_11
.end method

.method public setBluetoothA2dpDeviceConnectionState(Landroid/bluetooth/BluetoothDevice;I)I
    .registers 10
    .parameter "device"
    .parameter "state"

    #@0
    .prologue
    .line 2738
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v3

    #@4
    .line 2739
    .local v3, service:Landroid/media/IAudioService;
    const/4 v0, 0x0

    #@5
    .line 2741
    .local v0, delay:I
    :try_start_5
    invoke-interface {v3, p1, p2}, Landroid/media/IAudioService;->setBluetoothA2dpDeviceConnectionState(Landroid/bluetooth/BluetoothDevice;I)I
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_25
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_8} :catch_b

    #@8
    move-result v0

    #@9
    :goto_9
    move v1, v0

    #@a
    .line 2745
    .end local v0           #delay:I
    .local v1, delay:I
    return v1

    #@b
    .line 2742
    .end local v1           #delay:I
    .restart local v0       #delay:I
    :catch_b
    move-exception v2

    #@c
    .line 2743
    .local v2, e:Landroid/os/RemoteException;
    :try_start_c
    sget-object v4, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@e
    new-instance v5, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v6, "Dead object in setBluetoothA2dpDeviceConnectionState "

    #@15
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v5

    #@19
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v5

    #@1d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v5

    #@21
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_24
    .catchall {:try_start_c .. :try_end_24} :catchall_25

    #@24
    goto :goto_9

    #@25
    .line 2745
    .end local v2           #e:Landroid/os/RemoteException;
    :catchall_25
    move-exception v4

    #@26
    goto :goto_9
.end method

.method public setBluetoothA2dpOn(Z)V
    .registers 2
    .parameter "on"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1438
    return-void
.end method

.method public setBluetoothScoOn(Z)V
    .registers 6
    .parameter "on"

    #@0
    .prologue
    .line 1408
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 1410
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    invoke-interface {v1, p1}, Landroid/media/IAudioService;->setBluetoothScoOn(Z)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_7} :catch_8

    #@7
    .line 1414
    :goto_7
    return-void

    #@8
    .line 1411
    :catch_8
    move-exception v0

    #@9
    .line 1412
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@b
    const-string v3, "Dead object in setBluetoothScoOn"

    #@d
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@10
    goto :goto_7
.end method

.method public setInCallMode(I)V
    .registers 6
    .parameter "state"

    #@0
    .prologue
    .line 1620
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 1622
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    iget-object v2, p0, Landroid/media/AudioManager;->mICallBack:Landroid/os/IBinder;

    #@6
    invoke-interface {v1, p1, v2}, Landroid/media/IAudioService;->setInCallMode(ILandroid/os/IBinder;)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 1626
    :goto_9
    return-void

    #@a
    .line 1623
    :catch_a
    move-exception v0

    #@b
    .line 1624
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@d
    const-string v3, "Dead object in setInCallMode"

    #@f
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@12
    goto :goto_9
.end method

.method public setMABLControl(II)V
    .registers 5
    .parameter "curLvl"
    .parameter "levelMax"

    #@0
    .prologue
    .line 2954
    invoke-static {p1, p2}, Landroid/media/AudioSystem;->setMABLControl(II)I

    #@3
    .line 2955
    new-instance v0, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v1, "MABL_CurLvl="

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v0

    #@e
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    #@19
    .line 2956
    return-void
.end method

.method public setMABLEnable(I)V
    .registers 3
    .parameter "enable"

    #@0
    .prologue
    .line 2940
    invoke-static {p1}, Landroid/media/AudioSystem;->setMABLEnable(I)I

    #@3
    .line 2941
    if-lez p1, :cond_b

    #@5
    .line 2943
    const-string v0, "MABL_Enable=enable"

    #@7
    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    #@a
    .line 2949
    :goto_a
    return-void

    #@b
    .line 2947
    :cond_b
    const-string v0, "MABL_Enable=disable"

    #@d
    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    #@10
    goto :goto_a
.end method

.method public setMasterMute(Z)V
    .registers 3
    .parameter "state"

    #@0
    .prologue
    .line 1081
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/media/AudioManager;->setMasterMute(ZI)V

    #@4
    .line 1082
    return-void
.end method

.method public setMasterMute(ZI)V
    .registers 7
    .parameter "state"
    .parameter "flags"

    #@0
    .prologue
    .line 1090
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 1092
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    iget-object v2, p0, Landroid/media/AudioManager;->mICallBack:Landroid/os/IBinder;

    #@6
    invoke-interface {v1, p1, p2, v2}, Landroid/media/IAudioService;->setMasterMute(ZILandroid/os/IBinder;)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 1096
    :goto_9
    return-void

    #@a
    .line 1093
    :catch_a
    move-exception v0

    #@b
    .line 1094
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@d
    const-string v3, "Dead object in setMasterMute"

    #@f
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@12
    goto :goto_9
.end method

.method public setMasterVolume(II)V
    .registers 7
    .parameter "index"
    .parameter "flags"

    #@0
    .prologue
    .line 993
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 995
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    invoke-interface {v1, p1, p2}, Landroid/media/IAudioService;->setMasterVolume(II)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_7} :catch_8

    #@7
    .line 999
    :goto_7
    return-void

    #@8
    .line 996
    :catch_8
    move-exception v0

    #@9
    .line 997
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@b
    const-string v3, "Dead object in setMasterVolume"

    #@d
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@10
    goto :goto_7
.end method

.method public setMicrophoneMute(Z)V
    .registers 2
    .parameter "on"

    #@0
    .prologue
    .line 1556
    invoke-static {p1}, Landroid/media/AudioSystem;->muteMicrophone(Z)I

    #@3
    .line 1557
    return-void
.end method

.method public setMode(I)V
    .registers 6
    .parameter "mode"

    #@0
    .prologue
    .line 1584
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 1586
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    iget-object v2, p0, Landroid/media/AudioManager;->mICallBack:Landroid/os/IBinder;

    #@6
    invoke-interface {v1, p1, v2}, Landroid/media/IAudioService;->setMode(ILandroid/os/IBinder;)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 1590
    :goto_9
    return-void

    #@a
    .line 1587
    :catch_a
    move-exception v0

    #@b
    .line 1588
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@d
    const-string v3, "Dead object in setMode"

    #@f
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@12
    goto :goto_9
.end method

.method public setParameter(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "key"
    .parameter "value"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1849
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8
    move-result-object v0

    #@9
    const-string v1, "="

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {p0, v0}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    #@1a
    .line 1850
    return-void
.end method

.method public setParameters(Ljava/lang/String;)V
    .registers 2
    .parameter "keyValuePairs"

    #@0
    .prologue
    .line 1860
    invoke-static {p1}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    #@3
    .line 1861
    return-void
.end method

.method public setRecordHookingState(II)Z
    .registers 8
    .parameter "state"
    .parameter "flags"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 3058
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@4
    move-result-object v1

    #@5
    .line 3059
    .local v1, service:Landroid/media/IAudioService;
    if-eqz v1, :cond_15

    #@7
    .line 3061
    :try_start_7
    invoke-interface {v1, p1, p2}, Landroid/media/IAudioService;->setRecordHookingState(II)Z
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_a} :catch_c

    #@a
    move-result v2

    #@b
    .line 3068
    :goto_b
    return v2

    #@c
    .line 3062
    :catch_c
    move-exception v0

    #@d
    .line 3063
    .local v0, e:Landroid/os/RemoteException;
    sget-object v3, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@f
    const-string v4, "Dead object in setRecordHookingState"

    #@11
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@14
    goto :goto_b

    #@15
    .line 3067
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_15
    sget-object v3, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@17
    const-string v4, "AudioService is null in setRecordHookingState"

    #@19
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    goto :goto_b
.end method

.method public setRemoteSubmixOn(ZI)V
    .registers 7
    .parameter "on"
    .parameter "address"

    #@0
    .prologue
    .line 1508
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 1510
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    invoke-interface {v1, p1, p2}, Landroid/media/IAudioService;->setRemoteSubmixOn(ZI)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_7} :catch_8

    #@7
    .line 1514
    :goto_7
    return-void

    #@8
    .line 1511
    :catch_8
    move-exception v0

    #@9
    .line 1512
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@b
    const-string v3, "Dead object in setRemoteSubmixOn"

    #@d
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@10
    goto :goto_7
.end method

.method public setRingerMode(I)V
    .registers 6
    .parameter "ringerMode"

    #@0
    .prologue
    .line 869
    invoke-static {p1}, Landroid/media/AudioManager;->isValidRingerMode(I)Z

    #@3
    move-result v2

    #@4
    if-nez v2, :cond_7

    #@6
    .line 878
    :goto_6
    return-void

    #@7
    .line 872
    :cond_7
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@a
    move-result-object v1

    #@b
    .line 874
    .local v1, service:Landroid/media/IAudioService;
    :try_start_b
    invoke-interface {v1, p1}, Landroid/media/IAudioService;->setRingerMode(I)V
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_e} :catch_f

    #@e
    goto :goto_6

    #@f
    .line 875
    :catch_f
    move-exception v0

    #@10
    .line 876
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@12
    const-string v3, "Dead object in setRingerMode"

    #@14
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@17
    goto :goto_6
.end method

.method public setRouting(III)V
    .registers 4
    .parameter "mode"
    .parameter "routes"
    .parameter "mask"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1770
    return-void
.end method

.method public setSpeakerOnForMedia(Z)V
    .registers 6
    .parameter "on"

    #@0
    .prologue
    .line 2800
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 2801
    .local v1, service:Landroid/media/IAudioService;
    if-eqz v1, :cond_13

    #@6
    .line 2803
    :try_start_6
    invoke-interface {v1, p1}, Landroid/media/IAudioService;->setSpeakerOnForMedia(Z)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_9} :catch_a

    #@9
    .line 2810
    :goto_9
    return-void

    #@a
    .line 2804
    :catch_a
    move-exception v0

    #@b
    .line 2805
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@d
    const-string v3, "Dead object in setSpeakerOnForMedia"

    #@f
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@12
    goto :goto_9

    #@13
    .line 2808
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_13
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@15
    const-string v3, "AudioService is null in setSpeakerOnForMedia"

    #@17
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    goto :goto_9
.end method

.method public setSpeakerphoneOn(Z)V
    .registers 6
    .parameter "on"

    #@0
    .prologue
    .line 1222
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 1224
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    invoke-interface {v1, p1}, Landroid/media/IAudioService;->setSpeakerphoneOn(Z)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_7} :catch_8

    #@7
    .line 1228
    :goto_7
    return-void

    #@8
    .line 1225
    :catch_8
    move-exception v0

    #@9
    .line 1226
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@b
    const-string v3, "Dead object in setSpeakerphoneOn"

    #@d
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@10
    goto :goto_7
.end method

.method public setStreamMute(IZ)V
    .registers 8
    .parameter "streamType"
    .parameter "state"

    #@0
    .prologue
    .line 1048
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 1051
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@6
    const-string v3, "Warning!! This method should only be used by applications that replace the platform-wide management of audio settings or the main telephony application."

    #@8
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 1052
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@d
    new-instance v3, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string/jumbo v4, "setStreamMute() This method do not allowed streamType = "

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    const-string v4, " state = "

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    const-string v4, " package name = "

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    iget-object v4, p0, Landroid/media/AudioManager;->mContext:Landroid/content/Context;

    #@2f
    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v3

    #@3b
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 1054
    iget-object v2, p0, Landroid/media/AudioManager;->mICallBack:Landroid/os/IBinder;

    #@40
    invoke-interface {v1, p1, p2, v2}, Landroid/media/IAudioService;->setStreamMute(IZLandroid/os/IBinder;)V
    :try_end_43
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_43} :catch_44

    #@43
    .line 1058
    :goto_43
    return-void

    #@44
    .line 1055
    :catch_44
    move-exception v0

    #@45
    .line 1056
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@47
    const-string v3, "Dead object in setStreamMute"

    #@49
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4c
    goto :goto_43
.end method

.method public setStreamSolo(IZ)V
    .registers 7
    .parameter "streamType"
    .parameter "state"

    #@0
    .prologue
    .line 1019
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 1021
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    iget-object v2, p0, Landroid/media/AudioManager;->mICallBack:Landroid/os/IBinder;

    #@6
    invoke-interface {v1, p1, p2, v2}, Landroid/media/IAudioService;->setStreamSolo(IZLandroid/os/IBinder;)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 1025
    :goto_9
    return-void

    #@a
    .line 1022
    :catch_a
    move-exception v0

    #@b
    .line 1023
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@d
    const-string v3, "Dead object in setStreamSolo"

    #@f
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@12
    goto :goto_9
.end method

.method public setStreamVolume(III)V
    .registers 9
    .parameter "streamType"
    .parameter "index"
    .parameter "flags"

    #@0
    .prologue
    .line 891
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v4, "setStreamVolume() Request Package = "

    #@a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v3

    #@e
    iget-object v4, p0, Landroid/media/AudioManager;->mContext:Landroid/content/Context;

    #@10
    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@13
    move-result-object v4

    #@14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 892
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@22
    move-result-object v1

    #@23
    .line 894
    .local v1, service:Landroid/media/IAudioService;
    :try_start_23
    iget-boolean v2, p0, Landroid/media/AudioManager;->mUseMasterVolume:Z

    #@25
    if-eqz v2, :cond_2b

    #@27
    .line 895
    invoke-interface {v1, p2, p3}, Landroid/media/IAudioService;->setMasterVolume(II)V

    #@2a
    .line 902
    :goto_2a
    return-void

    #@2b
    .line 897
    :cond_2b
    invoke-interface {v1, p1, p2, p3}, Landroid/media/IAudioService;->setStreamVolume(III)V
    :try_end_2e
    .catch Landroid/os/RemoteException; {:try_start_23 .. :try_end_2e} :catch_2f

    #@2e
    goto :goto_2a

    #@2f
    .line 899
    :catch_2f
    move-exception v0

    #@30
    .line 900
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@32
    const-string v3, "Dead object in setStreamVolume"

    #@34
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@37
    goto :goto_2a
.end method

.method public setStreamVolumeAll(III)V
    .registers 9
    .parameter "streamType"
    .parameter "index"
    .parameter "flags"

    #@0
    .prologue
    .line 917
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v4, "setStreamVolumeAll() : streamType = "

    #@a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v3

    #@12
    const-string v4, ", index = "

    #@14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    const-string v4, ", flag = "

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    const-string v4, ", Request Package = "

    #@28
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v3

    #@2c
    iget-object v4, p0, Landroid/media/AudioManager;->mContext:Landroid/content/Context;

    #@2e
    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@31
    move-result-object v4

    #@32
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v3

    #@3a
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 918
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@40
    move-result-object v1

    #@41
    .line 919
    .local v1, service:Landroid/media/IAudioService;
    if-eqz v1, :cond_58

    #@43
    .line 921
    :try_start_43
    iget-boolean v2, p0, Landroid/media/AudioManager;->mUseMasterVolume:Z

    #@45
    if-eqz v2, :cond_4b

    #@47
    .line 922
    invoke-interface {v1, p2, p3}, Landroid/media/IAudioService;->setMasterVolume(II)V

    #@4a
    .line 933
    :goto_4a
    return-void

    #@4b
    .line 924
    :cond_4b
    invoke-interface {v1, p1, p2, p3}, Landroid/media/IAudioService;->setStreamVolumeAll(III)V
    :try_end_4e
    .catch Landroid/os/RemoteException; {:try_start_43 .. :try_end_4e} :catch_4f

    #@4e
    goto :goto_4a

    #@4f
    .line 926
    :catch_4f
    move-exception v0

    #@50
    .line 927
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@52
    const-string v3, "Dead object in setStreamVolumeAll"

    #@54
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@57
    goto :goto_4a

    #@58
    .line 930
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_58
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@5a
    const-string v3, "AudioService is null in getVoiceActivationState"

    #@5c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    goto :goto_4a
.end method

.method public setVibrateSetting(II)V
    .registers 7
    .parameter "vibrateType"
    .parameter "vibrateSetting"

    #@0
    .prologue
    .line 1204
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 1206
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    invoke-interface {v1, p1, p2}, Landroid/media/IAudioService;->setVibrateSetting(II)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_7} :catch_8

    #@7
    .line 1210
    :goto_7
    return-void

    #@8
    .line 1207
    :catch_8
    move-exception v0

    #@9
    .line 1208
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@b
    const-string v3, "Dead object in setVibrateSetting"

    #@d
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@10
    goto :goto_7
.end method

.method public setVoiceActivationState(I)V
    .registers 6
    .parameter "state"

    #@0
    .prologue
    .line 3005
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 3006
    .local v1, service:Landroid/media/IAudioService;
    if-eqz v1, :cond_13

    #@6
    .line 3008
    :try_start_6
    invoke-interface {v1, p1}, Landroid/media/IAudioService;->setVoiceActivationState(I)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_9} :catch_a

    #@9
    .line 3017
    :goto_9
    return-void

    #@a
    .line 3009
    :catch_a
    move-exception v0

    #@b
    .line 3010
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@d
    const-string v3, "Dead object in setVoiceActivationState"

    #@f
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@12
    goto :goto_9

    #@13
    .line 3014
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_13
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@15
    const-string v3, "AudioService is null in setVoiceActivationState"

    #@17
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    goto :goto_9
.end method

.method public setWiredDeviceConnectionState(IILjava/lang/String;)V
    .registers 9
    .parameter "device"
    .parameter "state"
    .parameter "name"

    #@0
    .prologue
    .line 2721
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 2723
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    invoke-interface {v1, p1, p2, p3}, Landroid/media/IAudioService;->setWiredDeviceConnectionState(IILjava/lang/String;)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_7} :catch_8

    #@7
    .line 2727
    :goto_7
    return-void

    #@8
    .line 2724
    :catch_8
    move-exception v0

    #@9
    .line 2725
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@b
    new-instance v3, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v4, "Dead object in setWiredDeviceConnectionState "

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    goto :goto_7
.end method

.method public setWiredHeadsetOn(Z)V
    .registers 2
    .parameter "on"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1524
    return-void
.end method

.method public shouldVibrate(I)Z
    .registers 6
    .parameter "vibrateType"

    #@0
    .prologue
    .line 1149
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 1151
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    invoke-interface {v1, p1}, Landroid/media/IAudioService;->shouldVibrate(I)Z
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_7} :catch_9

    #@7
    move-result v2

    #@8
    .line 1154
    :goto_8
    return v2

    #@9
    .line 1152
    :catch_9
    move-exception v0

    #@a
    .line 1153
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@c
    const-string v3, "Dead object in shouldVibrate"

    #@e
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@11
    .line 1154
    const/4 v2, 0x0

    #@12
    goto :goto_8
.end method

.method public startBluetoothSco()V
    .registers 5

    #@0
    .prologue
    .line 1372
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 1374
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    iget-object v2, p0, Landroid/media/AudioManager;->mICallBack:Landroid/os/IBinder;

    #@6
    invoke-interface {v1, v2}, Landroid/media/IAudioService;->startBluetoothSco(Landroid/os/IBinder;)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 1378
    :goto_9
    return-void

    #@a
    .line 1375
    :catch_a
    move-exception v0

    #@b
    .line 1376
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@d
    const-string v3, "Dead object in startBluetoothSco"

    #@f
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@12
    goto :goto_9
.end method

.method public stopBluetoothSco()V
    .registers 5

    #@0
    .prologue
    .line 1390
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 1392
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    iget-object v2, p0, Landroid/media/AudioManager;->mICallBack:Landroid/os/IBinder;

    #@6
    invoke-interface {v1, v2}, Landroid/media/IAudioService;->stopBluetoothSco(Landroid/os/IBinder;)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 1396
    :goto_9
    return-void

    #@a
    .line 1393
    :catch_a
    move-exception v0

    #@b
    .line 1394
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@d
    const-string v3, "Dead object in stopBluetoothSco"

    #@f
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@12
    goto :goto_9
.end method

.method public unloadSoundEffects()V
    .registers 6

    #@0
    .prologue
    .line 2028
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v4, "unloadSoundEffects() package name = "

    #@a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v3

    #@e
    iget-object v4, p0, Landroid/media/AudioManager;->mContext:Landroid/content/Context;

    #@10
    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@13
    move-result-object v4

    #@14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 2029
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@22
    move-result-object v1

    #@23
    .line 2031
    .local v1, service:Landroid/media/IAudioService;
    :try_start_23
    invoke-interface {v1}, Landroid/media/IAudioService;->unloadSoundEffects()V
    :try_end_26
    .catch Landroid/os/RemoteException; {:try_start_23 .. :try_end_26} :catch_27

    #@26
    .line 2035
    :goto_26
    return-void

    #@27
    .line 2032
    :catch_27
    move-exception v0

    #@28
    .line 2033
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@2a
    new-instance v3, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v4, "Dead object in unloadSoundEffects"

    #@31
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v3

    #@3d
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    goto :goto_26
.end method

.method public unregisterAudioFocusListener(Landroid/media/AudioManager$OnAudioFocusChangeListener;)V
    .registers 5
    .parameter "l"

    #@0
    .prologue
    .line 2199
    iget-object v1, p0, Landroid/media/AudioManager;->mFocusListenerLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 2200
    :try_start_3
    iget-object v0, p0, Landroid/media/AudioManager;->mAudioFocusIdListenerMap:Ljava/util/HashMap;

    #@5
    invoke-direct {p0, p1}, Landroid/media/AudioManager;->getIdForAudioFocusListener(Landroid/media/AudioManager$OnAudioFocusChangeListener;)Ljava/lang/String;

    #@8
    move-result-object v2

    #@9
    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    .line 2201
    monitor-exit v1

    #@d
    .line 2202
    return-void

    #@e
    .line 2201
    :catchall_e
    move-exception v0

    #@f
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    #@10
    throw v0
.end method

.method public unregisterMediaButtonEventReceiver(Landroid/content/ComponentName;)V
    .registers 6
    .parameter "eventReceiver"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 2386
    if-nez p1, :cond_4

    #@3
    .line 2396
    :goto_3
    return-void

    #@4
    .line 2390
    :cond_4
    new-instance v0, Landroid/content/Intent;

    #@6
    const-string v2, "android.intent.action.MEDIA_BUTTON"

    #@8
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@b
    .line 2392
    .local v0, mediaButtonIntent:Landroid/content/Intent;
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@e
    .line 2393
    iget-object v2, p0, Landroid/media/AudioManager;->mContext:Landroid/content/Context;

    #@10
    invoke-static {v2, v3, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@13
    move-result-object v1

    #@14
    .line 2395
    .local v1, pi:Landroid/app/PendingIntent;
    invoke-virtual {p0, v1, p1}, Landroid/media/AudioManager;->unregisterMediaButtonIntent(Landroid/app/PendingIntent;Landroid/content/ComponentName;)V

    #@17
    goto :goto_3
.end method

.method public unregisterMediaButtonEventReceiverForCalls()V
    .registers 5

    #@0
    .prologue
    .line 2372
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 2374
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    invoke-interface {v1}, Landroid/media/IAudioService;->unregisterMediaButtonEventReceiverForCalls()V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_7} :catch_8

    #@7
    .line 2378
    :goto_7
    return-void

    #@8
    .line 2375
    :catch_8
    move-exception v0

    #@9
    .line 2376
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@b
    const-string v3, "Dead object in unregisterMediaButtonEventReceiverForCalls"

    #@d
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@10
    goto :goto_7
.end method

.method public unregisterMediaButtonIntent(Landroid/app/PendingIntent;Landroid/content/ComponentName;)V
    .registers 8
    .parameter "pi"
    .parameter "eventReceiver"

    #@0
    .prologue
    .line 2402
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@3
    move-result-object v1

    #@4
    .line 2404
    .local v1, service:Landroid/media/IAudioService;
    :try_start_4
    invoke-interface {v1, p1, p2}, Landroid/media/IAudioService;->unregisterMediaButtonIntent(Landroid/app/PendingIntent;Landroid/content/ComponentName;)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_7} :catch_8

    #@7
    .line 2408
    :goto_7
    return-void

    #@8
    .line 2405
    :catch_8
    move-exception v0

    #@9
    .line 2406
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@b
    new-instance v3, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v4, "Dead object in unregisterMediaButtonIntent"

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    goto :goto_7
.end method

.method public unregisterRemoteControlClient(Landroid/media/RemoteControlClient;)V
    .registers 7
    .parameter "rcClient"

    #@0
    .prologue
    .line 2441
    if-eqz p1, :cond_8

    #@2
    invoke-virtual {p1}, Landroid/media/RemoteControlClient;->getRcMediaIntent()Landroid/app/PendingIntent;

    #@5
    move-result-object v2

    #@6
    if-nez v2, :cond_9

    #@8
    .line 2451
    :cond_8
    :goto_8
    return-void

    #@9
    .line 2444
    :cond_9
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@c
    move-result-object v1

    #@d
    .line 2446
    .local v1, service:Landroid/media/IAudioService;
    :try_start_d
    invoke-virtual {p1}, Landroid/media/RemoteControlClient;->getRcMediaIntent()Landroid/app/PendingIntent;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {p1}, Landroid/media/RemoteControlClient;->getIRemoteControlClient()Landroid/media/IRemoteControlClient;

    #@14
    move-result-object v3

    #@15
    invoke-interface {v1, v2, v3}, Landroid/media/IAudioService;->unregisterRemoteControlClient(Landroid/app/PendingIntent;Landroid/media/IRemoteControlClient;)V
    :try_end_18
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_18} :catch_19

    #@18
    goto :goto_8

    #@19
    .line 2448
    :catch_19
    move-exception v0

    #@1a
    .line 2449
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@1c
    new-instance v3, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v4, "Dead object in unregisterRemoteControlClient"

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    goto :goto_8
.end method

.method public unregisterRemoteControlDisplay(Landroid/media/IRemoteControlDisplay;)V
    .registers 7
    .parameter "rcd"

    #@0
    .prologue
    .line 2476
    if-nez p1, :cond_3

    #@2
    .line 2485
    :goto_2
    return-void

    #@3
    .line 2479
    :cond_3
    invoke-static {}, Landroid/media/AudioManager;->getService()Landroid/media/IAudioService;

    #@6
    move-result-object v1

    #@7
    .line 2481
    .local v1, service:Landroid/media/IAudioService;
    :try_start_7
    invoke-interface {v1, p1}, Landroid/media/IAudioService;->unregisterRemoteControlDisplay(Landroid/media/IRemoteControlDisplay;)V
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_a} :catch_b

    #@a
    goto :goto_2

    #@b
    .line 2482
    :catch_b
    move-exception v0

    #@c
    .line 2483
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/media/AudioManager;->TAG:Ljava/lang/String;

    #@e
    new-instance v3, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v4, "Dead object in unregisterRemoteControlDisplay "

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    goto :goto_2
.end method
