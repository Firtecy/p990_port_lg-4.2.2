.class Landroid/media/AudioService$AudioServiceBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AudioService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/AudioService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AudioServiceBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/media/AudioService;


# direct methods
.method private constructor <init>(Landroid/media/AudioService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 4509
    iput-object p1, p0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/media/AudioService;Landroid/media/AudioService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 4509
    invoke-direct {p0, p1}, Landroid/media/AudioService$AudioServiceBroadcastReceiver;-><init>(Landroid/media/AudioService;)V

    #@3
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 38
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 4512
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v10

    #@4
    .line 4516
    .local v10, action:Ljava/lang/String;
    const-string v3, "android.intent.action.DOCK_EVENT"

    #@6
    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v3

    #@a
    if-eqz v3, :cond_48

    #@c
    .line 4517
    const-string v3, "android.intent.extra.DOCK_STATE"

    #@e
    const/4 v4, 0x0

    #@f
    move-object/from16 v0, p2

    #@11
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@14
    move-result v24

    #@15
    .line 4520
    .local v24, dockState:I
    packed-switch v24, :pswitch_data_618

    #@18
    .line 4535
    const/16 v20, 0x0

    #@1a
    .line 4539
    .local v20, config:I
    :goto_1a
    const/4 v3, 0x3

    #@1b
    move/from16 v0, v24

    #@1d
    if-eq v0, v3, :cond_32

    #@1f
    if-nez v24, :cond_2c

    #@21
    move-object/from16 v0, p0

    #@23
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@25
    invoke-static {v3}, Landroid/media/AudioService;->access$8000(Landroid/media/AudioService;)I

    #@28
    move-result v3

    #@29
    const/4 v4, 0x3

    #@2a
    if-eq v3, v4, :cond_32

    #@2c
    .line 4542
    :cond_2c
    const/4 v3, 0x3

    #@2d
    move/from16 v0, v20

    #@2f
    invoke-static {v3, v0}, Landroid/media/AudioSystem;->setForceUse(II)I

    #@32
    .line 4544
    :cond_32
    move-object/from16 v0, p0

    #@34
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@36
    move/from16 v0, v24

    #@38
    invoke-static {v3, v0}, Landroid/media/AudioService;->access$8002(Landroid/media/AudioService;I)I

    #@3b
    .line 4818
    .end local v20           #config:I
    .end local v24           #dockState:I
    :cond_3b
    :goto_3b
    return-void

    #@3c
    .line 4522
    .restart local v24       #dockState:I
    :pswitch_3c
    const/16 v20, 0x7

    #@3e
    .line 4523
    .restart local v20       #config:I
    goto :goto_1a

    #@3f
    .line 4525
    .end local v20           #config:I
    :pswitch_3f
    const/16 v20, 0x6

    #@41
    .line 4526
    .restart local v20       #config:I
    goto :goto_1a

    #@42
    .line 4528
    .end local v20           #config:I
    :pswitch_42
    const/16 v20, 0x8

    #@44
    .line 4529
    .restart local v20       #config:I
    goto :goto_1a

    #@45
    .line 4531
    .end local v20           #config:I
    :pswitch_45
    const/16 v20, 0x9

    #@47
    .line 4532
    .restart local v20       #config:I
    goto :goto_1a

    #@48
    .line 4545
    .end local v20           #config:I
    .end local v24           #dockState:I
    :cond_48
    const-string v3, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    #@4a
    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4d
    move-result v3

    #@4e
    if-eqz v3, :cond_c5

    #@50
    .line 4546
    const-string v3, "android.bluetooth.profile.extra.STATE"

    #@52
    const/4 v4, 0x0

    #@53
    move-object/from16 v0, p2

    #@55
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@58
    move-result v34

    #@59
    .line 4550
    .local v34, state:I
    const/16 v23, 0x20

    #@5b
    .line 4556
    .local v23, device:I
    const/4 v12, 0x0

    #@5c
    .line 4558
    .local v12, address:Ljava/lang/String;
    const-string v3, "android.bluetooth.device.extra.DEVICE"

    #@5e
    move-object/from16 v0, p2

    #@60
    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@63
    move-result-object v17

    #@64
    check-cast v17, Landroid/bluetooth/BluetoothDevice;

    #@66
    .line 4559
    .local v17, btDevice:Landroid/bluetooth/BluetoothDevice;
    if-eqz v17, :cond_3b

    #@68
    .line 4563
    invoke-virtual/range {v17 .. v17}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@6b
    move-result-object v12

    #@6c
    .line 4564
    invoke-virtual/range {v17 .. v17}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    #@6f
    move-result-object v16

    #@70
    .line 4565
    .local v16, btClass:Landroid/bluetooth/BluetoothClass;
    if-eqz v16, :cond_79

    #@72
    .line 4566
    invoke-virtual/range {v16 .. v16}, Landroid/bluetooth/BluetoothClass;->getDeviceClass()I

    #@75
    move-result v3

    #@76
    sparse-switch v3, :sswitch_data_624

    #@79
    .line 4584
    :cond_79
    :goto_79
    invoke-static {v12}, Landroid/bluetooth/BluetoothAdapter;->checkBluetoothAddress(Ljava/lang/String;)Z

    #@7c
    move-result v3

    #@7d
    if-nez v3, :cond_81

    #@7f
    .line 4585
    const-string v12, ""

    #@81
    .line 4588
    :cond_81
    const/4 v3, 0x2

    #@82
    move/from16 v0, v34

    #@84
    if-ne v0, v3, :cond_b2

    #@86
    const/16 v21, 0x1

    #@88
    .line 4589
    .local v21, connected:Z
    :goto_88
    move-object/from16 v0, p0

    #@8a
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@8c
    move/from16 v0, v21

    #@8e
    move/from16 v1, v23

    #@90
    invoke-static {v3, v0, v1, v12}, Landroid/media/AudioService;->access$8100(Landroid/media/AudioService;ZILjava/lang/String;)Z

    #@93
    move-result v3

    #@94
    if-eqz v3, :cond_3b

    #@96
    .line 4590
    move-object/from16 v0, p0

    #@98
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@9a
    invoke-static {v3}, Landroid/media/AudioService;->access$2300(Landroid/media/AudioService;)Ljava/util/ArrayList;

    #@9d
    move-result-object v4

    #@9e
    monitor-enter v4

    #@9f
    .line 4591
    if-eqz v21, :cond_b5

    #@a1
    .line 4592
    :try_start_a1
    move-object/from16 v0, p0

    #@a3
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@a5
    move-object/from16 v0, v17

    #@a7
    invoke-static {v3, v0}, Landroid/media/AudioService;->access$2802(Landroid/media/AudioService;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;

    #@aa
    .line 4597
    :goto_aa
    monitor-exit v4

    #@ab
    goto :goto_3b

    #@ac
    :catchall_ac
    move-exception v3

    #@ad
    monitor-exit v4
    :try_end_ae
    .catchall {:try_start_a1 .. :try_end_ae} :catchall_ac

    #@ae
    throw v3

    #@af
    .line 4572
    .end local v21           #connected:Z
    :sswitch_af
    const/16 v23, 0x20

    #@b1
    goto :goto_79

    #@b2
    .line 4588
    :cond_b2
    const/16 v21, 0x0

    #@b4
    goto :goto_88

    #@b5
    .line 4594
    .restart local v21       #connected:Z
    :cond_b5
    :try_start_b5
    move-object/from16 v0, p0

    #@b7
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@b9
    const/4 v5, 0x0

    #@ba
    invoke-static {v3, v5}, Landroid/media/AudioService;->access$2802(Landroid/media/AudioService;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;

    #@bd
    .line 4595
    move-object/from16 v0, p0

    #@bf
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@c1
    invoke-static {v3}, Landroid/media/AudioService;->access$6500(Landroid/media/AudioService;)V
    :try_end_c4
    .catchall {:try_start_b5 .. :try_end_c4} :catchall_ac

    #@c4
    goto :goto_aa

    #@c5
    .line 4599
    .end local v12           #address:Ljava/lang/String;
    .end local v16           #btClass:Landroid/bluetooth/BluetoothClass;
    .end local v17           #btDevice:Landroid/bluetooth/BluetoothDevice;
    .end local v21           #connected:Z
    .end local v23           #device:I
    .end local v34           #state:I
    :cond_c5
    const-string v3, "android.intent.action.USB_AUDIO_ACCESSORY_PLUG"

    #@c7
    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ca
    move-result v3

    #@cb
    if-nez v3, :cond_d5

    #@cd
    const-string v3, "android.intent.action.USB_AUDIO_DEVICE_PLUG"

    #@cf
    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d2
    move-result v3

    #@d3
    if-eqz v3, :cond_178

    #@d5
    .line 4601
    :cond_d5
    const-string/jumbo v3, "state"

    #@d8
    const/4 v4, 0x0

    #@d9
    move-object/from16 v0, p2

    #@db
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@de
    move-result v34

    #@df
    .line 4602
    .restart local v34       #state:I
    const-string v3, "card"

    #@e1
    const/4 v4, -0x1

    #@e2
    move-object/from16 v0, p2

    #@e4
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@e7
    move-result v13

    #@e8
    .line 4603
    .local v13, alsaCard:I
    const-string v3, "device"

    #@ea
    const/4 v4, -0x1

    #@eb
    move-object/from16 v0, p2

    #@ed
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@f0
    move-result v14

    #@f1
    .line 4604
    .local v14, alsaDevice:I
    const/4 v3, -0x1

    #@f2
    if-ne v13, v3, :cond_154

    #@f4
    const/4 v3, -0x1

    #@f5
    if-ne v14, v3, :cond_154

    #@f7
    const-string v32, ""

    #@f9
    .line 4606
    .local v32, params:Ljava/lang/String;
    :goto_f9
    const-string v3, "android.intent.action.USB_AUDIO_ACCESSORY_PLUG"

    #@fb
    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@fe
    move-result v3

    #@ff
    if-eqz v3, :cond_172

    #@101
    const/16 v23, 0x2000

    #@103
    .line 4608
    .restart local v23       #device:I
    :goto_103
    const-string v4, "AudioService"

    #@105
    new-instance v3, Ljava/lang/StringBuilder;

    #@107
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@10a
    const-string v5, "Broadcast Receiver: Got "

    #@10c
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10f
    move-result-object v5

    #@110
    const-string v3, "android.intent.action.USB_AUDIO_ACCESSORY_PLUG"

    #@112
    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@115
    move-result v3

    #@116
    if-eqz v3, :cond_175

    #@118
    const-string v3, "ACTION_USB_AUDIO_ACCESSORY_PLUG"

    #@11a
    :goto_11a
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v3

    #@11e
    const-string v5, ", state = "

    #@120
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@123
    move-result-object v3

    #@124
    move/from16 v0, v34

    #@126
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@129
    move-result-object v3

    #@12a
    const-string v5, ", card: "

    #@12c
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12f
    move-result-object v3

    #@130
    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@133
    move-result-object v3

    #@134
    const-string v5, ", device: "

    #@136
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@139
    move-result-object v3

    #@13a
    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v3

    #@13e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@141
    move-result-object v3

    #@142
    invoke-static {v4, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@145
    .line 4612
    move-object/from16 v0, p0

    #@147
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@149
    move/from16 v0, v23

    #@14b
    move/from16 v1, v34

    #@14d
    move-object/from16 v2, v32

    #@14f
    invoke-virtual {v3, v0, v1, v2}, Landroid/media/AudioService;->setWiredDeviceConnectionState(IILjava/lang/String;)V

    #@152
    goto/16 :goto_3b

    #@154
    .line 4604
    .end local v23           #device:I
    .end local v32           #params:Ljava/lang/String;
    :cond_154
    new-instance v3, Ljava/lang/StringBuilder;

    #@156
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@159
    const-string v4, "card="

    #@15b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15e
    move-result-object v3

    #@15f
    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@162
    move-result-object v3

    #@163
    const-string v4, ";device="

    #@165
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@168
    move-result-object v3

    #@169
    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16c
    move-result-object v3

    #@16d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@170
    move-result-object v32

    #@171
    goto :goto_f9

    #@172
    .line 4606
    .restart local v32       #params:Ljava/lang/String;
    :cond_172
    const/16 v23, 0x4000

    #@174
    goto :goto_103

    #@175
    .line 4608
    .restart local v23       #device:I
    :cond_175
    const-string v3, "ACTION_USB_AUDIO_DEVICE_PLUG"

    #@177
    goto :goto_11a

    #@178
    .line 4616
    .end local v13           #alsaCard:I
    .end local v14           #alsaDevice:I
    .end local v23           #device:I
    .end local v32           #params:Ljava/lang/String;
    .end local v34           #state:I
    :cond_178
    const-string v3, "com.lge.mirrorlink.audio.started"

    #@17a
    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17d
    move-result v3

    #@17e
    if-eqz v3, :cond_190

    #@180
    .line 4617
    const-string v3, "AudioService"

    #@182
    const-string/jumbo v4, "wkcp Broadcast Receiver: Got com.lge.mirrorlink.audio.started"

    #@185
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@188
    .line 4618
    const-string/jumbo v3, "isVirtualMirrorLinkDevice=true"

    #@18b
    invoke-static {v3}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    #@18e
    goto/16 :goto_3b

    #@190
    .line 4620
    :cond_190
    const-string v3, "com.lge.mirrorlink.audio.stopped"

    #@192
    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@195
    move-result v3

    #@196
    if-eqz v3, :cond_1a8

    #@198
    .line 4621
    const-string v3, "AudioService"

    #@19a
    const-string/jumbo v4, "wkcp Broadcast Receiver: Got com.lge.mirrorlink.audio.stopped"

    #@19d
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1a0
    .line 4622
    const-string/jumbo v3, "isVirtualMirrorLinkDevice=false"

    #@1a3
    invoke-static {v3}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    #@1a6
    goto/16 :goto_3b

    #@1a8
    .line 4625
    :cond_1a8
    const-string v3, "android.bluetooth.headset.profile.action.AUDIO_STATE_CHANGED"

    #@1aa
    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1ad
    move-result v3

    #@1ae
    if-eqz v3, :cond_290

    #@1b0
    .line 4626
    const/4 v15, 0x0

    #@1b1
    .line 4627
    .local v15, broadcast:Z
    const/16 v33, -0x1

    #@1b3
    .line 4628
    .local v33, scoAudioState:I
    move-object/from16 v0, p0

    #@1b5
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@1b7
    invoke-static {v3}, Landroid/media/AudioService;->access$2300(Landroid/media/AudioService;)Ljava/util/ArrayList;

    #@1ba
    move-result-object v4

    #@1bb
    monitor-enter v4

    #@1bc
    .line 4629
    :try_start_1bc
    const-string v3, "android.bluetooth.profile.extra.STATE"

    #@1be
    const/4 v5, -0x1

    #@1bf
    move-object/from16 v0, p2

    #@1c1
    invoke-virtual {v0, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@1c4
    move-result v18

    #@1c5
    .line 4631
    .local v18, btState:I
    move-object/from16 v0, p0

    #@1c7
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@1c9
    invoke-static {v3}, Landroid/media/AudioService;->access$2300(Landroid/media/AudioService;)Ljava/util/ArrayList;

    #@1cc
    move-result-object v3

    #@1cd
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    #@1d0
    move-result v3

    #@1d1
    if-nez v3, :cond_1f5

    #@1d3
    move-object/from16 v0, p0

    #@1d5
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@1d7
    invoke-static {v3}, Landroid/media/AudioService;->access$2600(Landroid/media/AudioService;)I

    #@1da
    move-result v3

    #@1db
    const/4 v5, 0x3

    #@1dc
    if-eq v3, v5, :cond_1f4

    #@1de
    move-object/from16 v0, p0

    #@1e0
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@1e2
    invoke-static {v3}, Landroid/media/AudioService;->access$2600(Landroid/media/AudioService;)I

    #@1e5
    move-result v3

    #@1e6
    const/4 v5, 0x1

    #@1e7
    if-eq v3, v5, :cond_1f4

    #@1e9
    move-object/from16 v0, p0

    #@1eb
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@1ed
    invoke-static {v3}, Landroid/media/AudioService;->access$2600(Landroid/media/AudioService;)I

    #@1f0
    move-result v3

    #@1f1
    const/4 v5, 0x5

    #@1f2
    if-ne v3, v5, :cond_1f5

    #@1f4
    .line 4635
    :cond_1f4
    const/4 v15, 0x1

    #@1f5
    .line 4637
    :cond_1f5
    packed-switch v18, :pswitch_data_632

    #@1f8
    .line 4659
    :cond_1f8
    :goto_1f8
    const/4 v15, 0x0

    #@1f9
    .line 4662
    :cond_1f9
    :goto_1f9
    monitor-exit v4
    :try_end_1fa
    .catchall {:try_start_1bc .. :try_end_1fa} :catchall_24e

    #@1fa
    .line 4663
    if-eqz v15, :cond_3b

    #@1fc
    .line 4664
    move-object/from16 v0, p0

    #@1fe
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@200
    move/from16 v0, v33

    #@202
    invoke-static {v3, v0}, Landroid/media/AudioService;->access$2500(Landroid/media/AudioService;I)V

    #@205
    .line 4667
    new-instance v30, Landroid/content/Intent;

    #@207
    const-string v3, "android.media.SCO_AUDIO_STATE_CHANGED"

    #@209
    move-object/from16 v0, v30

    #@20b
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@20e
    .line 4668
    .local v30, newIntent:Landroid/content/Intent;
    const-string v3, "android.media.extra.SCO_AUDIO_STATE"

    #@210
    move-object/from16 v0, v30

    #@212
    move/from16 v1, v33

    #@214
    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@217
    .line 4669
    move-object/from16 v0, p0

    #@219
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@21b
    move-object/from16 v0, v30

    #@21d
    invoke-static {v3, v0}, Landroid/media/AudioService;->access$8200(Landroid/media/AudioService;Landroid/content/Intent;)V

    #@220
    goto/16 :goto_3b

    #@222
    .line 4639
    .end local v30           #newIntent:Landroid/content/Intent;
    :pswitch_222
    const/16 v33, 0x1

    #@224
    .line 4640
    :try_start_224
    move-object/from16 v0, p0

    #@226
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@228
    invoke-static {v3}, Landroid/media/AudioService;->access$2600(Landroid/media/AudioService;)I

    #@22b
    move-result v3

    #@22c
    const/4 v5, 0x3

    #@22d
    if-eq v3, v5, :cond_1f9

    #@22f
    move-object/from16 v0, p0

    #@231
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@233
    invoke-static {v3}, Landroid/media/AudioService;->access$2600(Landroid/media/AudioService;)I

    #@236
    move-result v3

    #@237
    const/4 v5, 0x5

    #@238
    if-eq v3, v5, :cond_1f9

    #@23a
    move-object/from16 v0, p0

    #@23c
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@23e
    invoke-static {v3}, Landroid/media/AudioService;->access$2600(Landroid/media/AudioService;)I

    #@241
    move-result v3

    #@242
    const/4 v5, 0x4

    #@243
    if-eq v3, v5, :cond_1f9

    #@245
    .line 4643
    move-object/from16 v0, p0

    #@247
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@249
    const/4 v5, 0x2

    #@24a
    invoke-static {v3, v5}, Landroid/media/AudioService;->access$2602(Landroid/media/AudioService;I)I

    #@24d
    goto :goto_1f9

    #@24e
    .line 4662
    .end local v18           #btState:I
    :catchall_24e
    move-exception v3

    #@24f
    monitor-exit v4
    :try_end_250
    .catchall {:try_start_224 .. :try_end_250} :catchall_24e

    #@250
    throw v3

    #@251
    .line 4647
    .restart local v18       #btState:I
    :pswitch_251
    const/16 v33, 0x0

    #@253
    .line 4648
    :try_start_253
    move-object/from16 v0, p0

    #@255
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@257
    const/4 v5, 0x0

    #@258
    invoke-static {v3, v5}, Landroid/media/AudioService;->access$2602(Landroid/media/AudioService;I)I

    #@25b
    .line 4649
    move-object/from16 v0, p0

    #@25d
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@25f
    const/4 v5, 0x0

    #@260
    const/4 v6, 0x0

    #@261
    invoke-virtual {v3, v5, v6}, Landroid/media/AudioService;->clearAllScoClients(IZ)V

    #@264
    goto :goto_1f9

    #@265
    .line 4652
    :pswitch_265
    move-object/from16 v0, p0

    #@267
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@269
    invoke-static {v3}, Landroid/media/AudioService;->access$2600(Landroid/media/AudioService;)I

    #@26c
    move-result v3

    #@26d
    const/4 v5, 0x3

    #@26e
    if-eq v3, v5, :cond_1f8

    #@270
    move-object/from16 v0, p0

    #@272
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@274
    invoke-static {v3}, Landroid/media/AudioService;->access$2600(Landroid/media/AudioService;)I

    #@277
    move-result v3

    #@278
    const/4 v5, 0x5

    #@279
    if-eq v3, v5, :cond_1f8

    #@27b
    move-object/from16 v0, p0

    #@27d
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@27f
    invoke-static {v3}, Landroid/media/AudioService;->access$2600(Landroid/media/AudioService;)I

    #@282
    move-result v3

    #@283
    const/4 v5, 0x4

    #@284
    if-eq v3, v5, :cond_1f8

    #@286
    .line 4655
    move-object/from16 v0, p0

    #@288
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@28a
    const/4 v5, 0x2

    #@28b
    invoke-static {v3, v5}, Landroid/media/AudioService;->access$2602(Landroid/media/AudioService;I)I
    :try_end_28e
    .catchall {:try_start_253 .. :try_end_28e} :catchall_24e

    #@28e
    goto/16 :goto_1f8

    #@290
    .line 4671
    .end local v15           #broadcast:Z
    .end local v18           #btState:I
    .end local v33           #scoAudioState:I
    :cond_290
    const-string v3, "android.intent.action.BOOT_COMPLETED"

    #@292
    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@295
    move-result v3

    #@296
    if-eqz v3, :cond_3cb

    #@298
    .line 4672
    move-object/from16 v0, p0

    #@29a
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@29c
    const/4 v4, 0x1

    #@29d
    invoke-static {v3, v4}, Landroid/media/AudioService;->access$8302(Landroid/media/AudioService;Z)Z

    #@2a0
    .line 4673
    move-object/from16 v0, p0

    #@2a2
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@2a4
    invoke-static {v3}, Landroid/media/AudioService;->access$100(Landroid/media/AudioService;)Landroid/media/AudioService$AudioHandler;

    #@2a7
    move-result-object v3

    #@2a8
    const/16 v4, 0x8

    #@2aa
    const/4 v5, 0x1

    #@2ab
    const/4 v6, 0x0

    #@2ac
    const/4 v7, 0x0

    #@2ad
    const/4 v8, 0x0

    #@2ae
    const/4 v9, 0x0

    #@2af
    invoke-static/range {v3 .. v9}, Landroid/media/AudioService;->access$200(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@2b2
    .line 4676
    move-object/from16 v0, p0

    #@2b4
    iget-object v4, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@2b6
    move-object/from16 v0, p0

    #@2b8
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@2ba
    invoke-static {v3}, Landroid/media/AudioService;->access$8500(Landroid/media/AudioService;)Landroid/content/Context;

    #@2bd
    move-result-object v3

    #@2be
    const-string/jumbo v5, "keyguard"

    #@2c1
    invoke-virtual {v3, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2c4
    move-result-object v3

    #@2c5
    check-cast v3, Landroid/app/KeyguardManager;

    #@2c7
    invoke-static {v4, v3}, Landroid/media/AudioService;->access$8402(Landroid/media/AudioService;Landroid/app/KeyguardManager;)Landroid/app/KeyguardManager;

    #@2ca
    .line 4678
    move-object/from16 v0, p0

    #@2cc
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@2ce
    const/4 v4, -0x1

    #@2cf
    invoke-static {v3, v4}, Landroid/media/AudioService;->access$8602(Landroid/media/AudioService;I)I

    #@2d2
    .line 4679
    move-object/from16 v0, p0

    #@2d4
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@2d6
    invoke-static {v3}, Landroid/media/AudioService;->access$6500(Landroid/media/AudioService;)V

    #@2d9
    .line 4680
    move-object/from16 v0, p0

    #@2db
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@2dd
    invoke-static {v3}, Landroid/media/AudioService;->access$2900(Landroid/media/AudioService;)Z

    #@2e0
    .line 4683
    new-instance v30, Landroid/content/Intent;

    #@2e2
    const-string v3, "android.media.SCO_AUDIO_STATE_CHANGED"

    #@2e4
    move-object/from16 v0, v30

    #@2e6
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@2e9
    .line 4684
    .restart local v30       #newIntent:Landroid/content/Intent;
    const-string v3, "android.media.extra.SCO_AUDIO_STATE"

    #@2eb
    const/4 v4, 0x0

    #@2ec
    move-object/from16 v0, v30

    #@2ee
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@2f1
    .line 4686
    move-object/from16 v0, p0

    #@2f3
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@2f5
    move-object/from16 v0, v30

    #@2f7
    invoke-static {v3, v0}, Landroid/media/AudioService;->access$8200(Landroid/media/AudioService;Landroid/content/Intent;)V

    #@2fa
    .line 4688
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@2fd
    move-result-object v11

    #@2fe
    .line 4689
    .local v11, adapter:Landroid/bluetooth/BluetoothAdapter;
    if-eqz v11, :cond_314

    #@300
    .line 4690
    move-object/from16 v0, p0

    #@302
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@304
    invoke-static {v3}, Landroid/media/AudioService;->access$8500(Landroid/media/AudioService;)Landroid/content/Context;

    #@307
    move-result-object v3

    #@308
    move-object/from16 v0, p0

    #@30a
    iget-object v4, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@30c
    invoke-static {v4}, Landroid/media/AudioService;->access$8700(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothProfile$ServiceListener;

    #@30f
    move-result-object v4

    #@310
    const/4 v5, 0x2

    #@311
    invoke-virtual {v11, v3, v4, v5}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    #@314
    .line 4694
    :cond_314
    move-object/from16 v0, p0

    #@316
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@318
    invoke-static {v3}, Landroid/media/AudioService;->access$100(Landroid/media/AudioService;)Landroid/media/AudioService$AudioHandler;

    #@31b
    move-result-object v3

    #@31c
    const/16 v4, 0x1b

    #@31e
    const/4 v5, 0x0

    #@31f
    const/4 v6, 0x0

    #@320
    const/4 v7, 0x0

    #@321
    const/4 v8, 0x0

    #@322
    const/16 v9, 0x7530

    #@324
    invoke-static/range {v3 .. v9}, Landroid/media/AudioService;->access$200(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@327
    .line 4703
    const/16 v26, 0x0

    #@329
    .line 4705
    .local v26, file:Ljava/io/FileReader;
    const/16 v3, 0x400

    #@32b
    :try_start_32b
    new-array v0, v3, [C

    #@32d
    move-object/from16 v19, v0

    #@32f
    .line 4706
    .local v19, buffer:[C
    const/16 v22, 0x0

    #@331
    .line 4708
    .local v22, curState:I
    new-instance v27, Ljava/io/FileReader;

    #@333
    const-string v3, "/sys/class/switch/h2w/state"

    #@335
    move-object/from16 v0, v27

    #@337
    invoke-direct {v0, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_33a
    .catchall {:try_start_32b .. :try_end_33a} :catchall_3c4
    .catch Ljava/io/FileNotFoundException; {:try_start_32b .. :try_end_33a} :catch_614
    .catch Ljava/lang/Exception; {:try_start_32b .. :try_end_33a} :catch_611

    #@33a
    .line 4709
    .end local v26           #file:Ljava/io/FileReader;
    .local v27, file:Ljava/io/FileReader;
    const/4 v3, 0x0

    #@33b
    const/16 v4, 0x400

    #@33d
    :try_start_33d
    move-object/from16 v0, v27

    #@33f
    move-object/from16 v1, v19

    #@341
    invoke-virtual {v0, v1, v3, v4}, Ljava/io/FileReader;->read([CII)I

    #@344
    move-result v29

    #@345
    .line 4710
    .local v29, len:I
    new-instance v3, Ljava/lang/String;

    #@347
    const/4 v4, 0x0

    #@348
    move-object/from16 v0, v19

    #@34a
    move/from16 v1, v29

    #@34c
    invoke-direct {v3, v0, v4, v1}, Ljava/lang/String;-><init>([CII)V

    #@34f
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@352
    move-result-object v3

    #@353
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    #@356
    move-result-object v3

    #@357
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@35a
    move-result v22

    #@35b
    .line 4712
    const/4 v3, 0x1

    #@35c
    move/from16 v0, v22

    #@35e
    if-ne v0, v3, :cond_370

    #@360
    .line 4713
    const/4 v3, 0x4

    #@361
    const/4 v4, 0x1

    #@362
    const-string v5, ""

    #@364
    invoke-static {v3, v4, v5}, Landroid/media/AudioSystem;->setDeviceConnectionState(IILjava/lang/String;)I
    :try_end_367
    .catchall {:try_start_33d .. :try_end_367} :catchall_60c
    .catch Ljava/io/FileNotFoundException; {:try_start_33d .. :try_end_367} :catch_37e
    .catch Ljava/lang/Exception; {:try_start_33d .. :try_end_367} :catch_3ab

    #@367
    .line 4725
    :goto_367
    if-eqz v27, :cond_36c

    #@369
    .line 4727
    :try_start_369
    invoke-virtual/range {v27 .. v27}, Ljava/io/FileReader;->close()V
    :try_end_36c
    .catch Ljava/io/IOException; {:try_start_369 .. :try_end_36c} :catch_600

    #@36c
    :cond_36c
    :goto_36c
    move-object/from16 v26, v27

    #@36e
    .line 4732
    .end local v27           #file:Ljava/io/FileReader;
    .restart local v26       #file:Ljava/io/FileReader;
    goto/16 :goto_3b

    #@370
    .line 4714
    .end local v26           #file:Ljava/io/FileReader;
    .restart local v27       #file:Ljava/io/FileReader;
    :cond_370
    const/4 v3, 0x2

    #@371
    move/from16 v0, v22

    #@373
    if-ne v0, v3, :cond_39b

    #@375
    .line 4715
    const/16 v3, 0x8

    #@377
    const/4 v4, 0x1

    #@378
    :try_start_378
    const-string v5, ""

    #@37a
    invoke-static {v3, v4, v5}, Landroid/media/AudioSystem;->setDeviceConnectionState(IILjava/lang/String;)I
    :try_end_37d
    .catchall {:try_start_378 .. :try_end_37d} :catchall_60c
    .catch Ljava/io/FileNotFoundException; {:try_start_378 .. :try_end_37d} :catch_37e
    .catch Ljava/lang/Exception; {:try_start_378 .. :try_end_37d} :catch_3ab

    #@37d
    goto :goto_367

    #@37e
    .line 4720
    .end local v29           #len:I
    :catch_37e
    move-exception v25

    #@37f
    move-object/from16 v26, v27

    #@381
    .line 4721
    .end local v19           #buffer:[C
    .end local v22           #curState:I
    .end local v27           #file:Ljava/io/FileReader;
    .local v25, e:Ljava/io/FileNotFoundException;
    .restart local v26       #file:Ljava/io/FileReader;
    :goto_381
    :try_start_381
    const-string v3, "AudioService"

    #@383
    const-string v4, "/sys/class/switch/h2w/state not found while attempting to determine initial switch state"

    #@385
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_388
    .catchall {:try_start_381 .. :try_end_388} :catchall_3c4

    #@388
    .line 4725
    if-eqz v26, :cond_3b

    #@38a
    .line 4727
    :try_start_38a
    invoke-virtual/range {v26 .. v26}, Ljava/io/FileReader;->close()V
    :try_end_38d
    .catch Ljava/io/IOException; {:try_start_38a .. :try_end_38d} :catch_38f

    #@38d
    goto/16 :goto_3b

    #@38f
    .line 4728
    :catch_38f
    move-exception v25

    #@390
    .line 4729
    .local v25, e:Ljava/io/IOException;
    const-string v3, "AudioService"

    #@392
    const-string v4, ""

    #@394
    :goto_394
    move-object/from16 v0, v25

    #@396
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@399
    goto/16 :goto_3b

    #@39b
    .line 4717
    .end local v25           #e:Ljava/io/IOException;
    .end local v26           #file:Ljava/io/FileReader;
    .restart local v19       #buffer:[C
    .restart local v22       #curState:I
    .restart local v27       #file:Ljava/io/FileReader;
    .restart local v29       #len:I
    :cond_39b
    const/4 v3, 0x4

    #@39c
    const/4 v4, 0x0

    #@39d
    :try_start_39d
    const-string v5, ""

    #@39f
    invoke-static {v3, v4, v5}, Landroid/media/AudioSystem;->setDeviceConnectionState(IILjava/lang/String;)I

    #@3a2
    .line 4718
    const/16 v3, 0x8

    #@3a4
    const/4 v4, 0x0

    #@3a5
    const-string v5, ""

    #@3a7
    invoke-static {v3, v4, v5}, Landroid/media/AudioSystem;->setDeviceConnectionState(IILjava/lang/String;)I
    :try_end_3aa
    .catchall {:try_start_39d .. :try_end_3aa} :catchall_60c
    .catch Ljava/io/FileNotFoundException; {:try_start_39d .. :try_end_3aa} :catch_37e
    .catch Ljava/lang/Exception; {:try_start_39d .. :try_end_3aa} :catch_3ab

    #@3aa
    goto :goto_367

    #@3ab
    .line 4722
    .end local v29           #len:I
    :catch_3ab
    move-exception v25

    #@3ac
    move-object/from16 v26, v27

    #@3ae
    .line 4723
    .end local v19           #buffer:[C
    .end local v22           #curState:I
    .end local v27           #file:Ljava/io/FileReader;
    .local v25, e:Ljava/lang/Exception;
    .restart local v26       #file:Ljava/io/FileReader;
    :goto_3ae
    :try_start_3ae
    const-string v3, "AudioService"

    #@3b0
    const-string v4, ""

    #@3b2
    move-object/from16 v0, v25

    #@3b4
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3b7
    .catchall {:try_start_3ae .. :try_end_3b7} :catchall_3c4

    #@3b7
    .line 4725
    if-eqz v26, :cond_3b

    #@3b9
    .line 4727
    :try_start_3b9
    invoke-virtual/range {v26 .. v26}, Ljava/io/FileReader;->close()V
    :try_end_3bc
    .catch Ljava/io/IOException; {:try_start_3b9 .. :try_end_3bc} :catch_3be

    #@3bc
    goto/16 :goto_3b

    #@3be
    .line 4728
    :catch_3be
    move-exception v25

    #@3bf
    .line 4729
    .local v25, e:Ljava/io/IOException;
    const-string v3, "AudioService"

    #@3c1
    const-string v4, ""

    #@3c3
    goto :goto_394

    #@3c4
    .line 4725
    .end local v25           #e:Ljava/io/IOException;
    :catchall_3c4
    move-exception v3

    #@3c5
    :goto_3c5
    if-eqz v26, :cond_3ca

    #@3c7
    .line 4727
    :try_start_3c7
    invoke-virtual/range {v26 .. v26}, Ljava/io/FileReader;->close()V
    :try_end_3ca
    .catch Ljava/io/IOException; {:try_start_3c7 .. :try_end_3ca} :catch_5f4

    #@3ca
    .line 4725
    :cond_3ca
    :goto_3ca
    throw v3

    #@3cb
    .line 4734
    .end local v11           #adapter:Landroid/bluetooth/BluetoothAdapter;
    .end local v26           #file:Ljava/io/FileReader;
    .end local v30           #newIntent:Landroid/content/Intent;
    :cond_3cb
    const-string v3, "android.intent.action.PACKAGE_REMOVED"

    #@3cd
    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3d0
    move-result v3

    #@3d1
    if-eqz v3, :cond_3f3

    #@3d3
    .line 4735
    const-string v3, "android.intent.extra.REPLACING"

    #@3d5
    const/4 v4, 0x0

    #@3d6
    move-object/from16 v0, p2

    #@3d8
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@3db
    move-result v3

    #@3dc
    if-nez v3, :cond_3b

    #@3de
    .line 4737
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@3e1
    move-result-object v3

    #@3e2
    invoke-virtual {v3}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    #@3e5
    move-result-object v31

    #@3e6
    .line 4738
    .local v31, packageName:Ljava/lang/String;
    if-eqz v31, :cond_3b

    #@3e8
    .line 4739
    move-object/from16 v0, p0

    #@3ea
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@3ec
    move-object/from16 v0, v31

    #@3ee
    invoke-static {v3, v0}, Landroid/media/AudioService;->access$8800(Landroid/media/AudioService;Ljava/lang/String;)V

    #@3f1
    goto/16 :goto_3b

    #@3f3
    .line 4742
    .end local v31           #packageName:Ljava/lang/String;
    :cond_3f3
    const-string/jumbo v3, "qualcomm.intent.action.FM"

    #@3f6
    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3f9
    move-result v3

    #@3fa
    if-eqz v3, :cond_44d

    #@3fc
    .line 4743
    const-string v3, "AudioService"

    #@3fe
    const-string v4, "FM Intent received"

    #@400
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@403
    .line 4744
    const-string/jumbo v3, "state"

    #@406
    const/4 v4, 0x0

    #@407
    move-object/from16 v0, p2

    #@409
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@40c
    move-result v34

    #@40d
    .line 4745
    .restart local v34       #state:I
    const/4 v3, 0x1

    #@40e
    move/from16 v0, v34

    #@410
    if-ne v0, v3, :cond_430

    #@412
    .line 4746
    const/high16 v3, 0x8

    #@414
    const/4 v4, 0x1

    #@415
    const-string v5, ""

    #@417
    invoke-static {v3, v4, v5}, Landroid/media/AudioSystem;->setDeviceConnectionState(IILjava/lang/String;)I

    #@41a
    .line 4749
    move-object/from16 v0, p0

    #@41c
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@41e
    invoke-static {v3}, Landroid/media/AudioService;->access$400(Landroid/media/AudioService;)Ljava/util/HashMap;

    #@421
    move-result-object v3

    #@422
    new-instance v4, Ljava/lang/Integer;

    #@424
    const/high16 v5, 0x8

    #@426
    invoke-direct {v4, v5}, Ljava/lang/Integer;-><init>(I)V

    #@429
    const-string v5, ""

    #@42b
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@42e
    goto/16 :goto_3b

    #@430
    .line 4750
    :cond_430
    if-nez v34, :cond_3b

    #@432
    .line 4751
    const/high16 v3, 0x8

    #@434
    const/4 v4, 0x0

    #@435
    const-string v5, ""

    #@437
    invoke-static {v3, v4, v5}, Landroid/media/AudioSystem;->setDeviceConnectionState(IILjava/lang/String;)I

    #@43a
    .line 4754
    move-object/from16 v0, p0

    #@43c
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@43e
    invoke-static {v3}, Landroid/media/AudioService;->access$400(Landroid/media/AudioService;)Ljava/util/HashMap;

    #@441
    move-result-object v3

    #@442
    const/high16 v4, 0x8

    #@444
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@447
    move-result-object v4

    #@448
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@44b
    goto/16 :goto_3b

    #@44d
    .line 4756
    .end local v34           #state:I
    :cond_44d
    const-string/jumbo v3, "qualcomm.intent.action.FMTX"

    #@450
    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@453
    move-result v3

    #@454
    if-eqz v3, :cond_4d0

    #@456
    .line 4757
    const-string/jumbo v3, "state"

    #@459
    const/4 v4, 0x0

    #@45a
    move-object/from16 v0, p2

    #@45c
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@45f
    move-result v34

    #@460
    .line 4758
    .restart local v34       #state:I
    const-string v3, "AudioService"

    #@462
    new-instance v4, Ljava/lang/StringBuilder;

    #@464
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@467
    const-string v5, "FM Tx Intent received "

    #@469
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46c
    move-result-object v4

    #@46d
    move/from16 v0, v34

    #@46f
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@472
    move-result-object v4

    #@473
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@476
    move-result-object v4

    #@477
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@47a
    .line 4759
    move-object/from16 v0, p0

    #@47c
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@47e
    invoke-static {v3}, Landroid/media/AudioService;->access$400(Landroid/media/AudioService;)Ljava/util/HashMap;

    #@481
    move-result-object v3

    #@482
    const/high16 v4, 0x10

    #@484
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@487
    move-result-object v4

    #@488
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@48b
    move-result v28

    #@48c
    .line 4760
    .local v28, isConnected:Z
    const/4 v3, 0x1

    #@48d
    move/from16 v0, v34

    #@48f
    if-ne v0, v3, :cond_4b1

    #@491
    if-nez v28, :cond_4b1

    #@493
    .line 4761
    const/high16 v3, 0x10

    #@495
    const/4 v4, 0x1

    #@496
    const-string v5, ""

    #@498
    invoke-static {v3, v4, v5}, Landroid/media/AudioSystem;->setDeviceConnectionState(IILjava/lang/String;)I

    #@49b
    .line 4764
    move-object/from16 v0, p0

    #@49d
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@49f
    invoke-static {v3}, Landroid/media/AudioService;->access$400(Landroid/media/AudioService;)Ljava/util/HashMap;

    #@4a2
    move-result-object v3

    #@4a3
    new-instance v4, Ljava/lang/Integer;

    #@4a5
    const/high16 v5, 0x10

    #@4a7
    invoke-direct {v4, v5}, Ljava/lang/Integer;-><init>(I)V

    #@4aa
    const-string v5, ""

    #@4ac
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4af
    goto/16 :goto_3b

    #@4b1
    .line 4765
    :cond_4b1
    if-nez v34, :cond_3b

    #@4b3
    if-eqz v28, :cond_3b

    #@4b5
    .line 4766
    const/high16 v3, 0x10

    #@4b7
    const/4 v4, 0x0

    #@4b8
    const-string v5, ""

    #@4ba
    invoke-static {v3, v4, v5}, Landroid/media/AudioSystem;->setDeviceConnectionState(IILjava/lang/String;)I

    #@4bd
    .line 4769
    move-object/from16 v0, p0

    #@4bf
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@4c1
    invoke-static {v3}, Landroid/media/AudioService;->access$400(Landroid/media/AudioService;)Ljava/util/HashMap;

    #@4c4
    move-result-object v3

    #@4c5
    const/high16 v4, 0x10

    #@4c7
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4ca
    move-result-object v4

    #@4cb
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@4ce
    goto/16 :goto_3b

    #@4d0
    .line 4771
    .end local v28           #isConnected:Z
    .end local v34           #state:I
    :cond_4d0
    const-string v3, "android.intent.action.SCREEN_ON"

    #@4d2
    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4d5
    move-result v3

    #@4d6
    if-eqz v3, :cond_4ea

    #@4d8
    .line 4772
    move-object/from16 v0, p0

    #@4da
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@4dc
    const/4 v4, 0x1

    #@4dd
    move-object/from16 v0, p1

    #@4df
    invoke-static {v3, v0, v4}, Landroid/media/AudioService;->access$8900(Landroid/media/AudioService;Landroid/content/Context;Z)V

    #@4e2
    .line 4773
    const-string/jumbo v3, "screen_state=on"

    #@4e5
    invoke-static {v3}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    #@4e8
    goto/16 :goto_3b

    #@4ea
    .line 4774
    :cond_4ea
    const-string v3, "android.intent.action.SCREEN_OFF"

    #@4ec
    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4ef
    move-result v3

    #@4f0
    if-eqz v3, :cond_504

    #@4f2
    .line 4775
    move-object/from16 v0, p0

    #@4f4
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@4f6
    const/4 v4, 0x0

    #@4f7
    move-object/from16 v0, p1

    #@4f9
    invoke-static {v3, v0, v4}, Landroid/media/AudioService;->access$8900(Landroid/media/AudioService;Landroid/content/Context;Z)V

    #@4fc
    .line 4776
    const-string/jumbo v3, "screen_state=off"

    #@4ff
    invoke-static {v3}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    #@502
    goto/16 :goto_3b

    #@504
    .line 4777
    :cond_504
    const-string v3, "android.intent.action.CONFIGURATION_CHANGED"

    #@506
    invoke-virtual {v10, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@509
    move-result v3

    #@50a
    if-eqz v3, :cond_517

    #@50c
    .line 4778
    move-object/from16 v0, p0

    #@50e
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@510
    move-object/from16 v0, p1

    #@512
    invoke-static {v3, v0}, Landroid/media/AudioService;->access$9000(Landroid/media/AudioService;Landroid/content/Context;)V

    #@515
    goto/16 :goto_3b

    #@517
    .line 4779
    :cond_517
    const-string v3, "android.intent.action.USER_SWITCHED"

    #@519
    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@51c
    move-result v3

    #@51d
    if-eqz v3, :cond_55e

    #@51f
    .line 4781
    move-object/from16 v0, p0

    #@521
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@523
    invoke-static {v3}, Landroid/media/AudioService;->access$100(Landroid/media/AudioService;)Landroid/media/AudioService$AudioHandler;

    #@526
    move-result-object v3

    #@527
    const/16 v4, 0x19

    #@529
    const/4 v5, 0x0

    #@52a
    const/4 v6, 0x0

    #@52b
    const/4 v7, 0x0

    #@52c
    const/4 v8, 0x0

    #@52d
    const/4 v9, 0x0

    #@52e
    invoke-static/range {v3 .. v9}, Landroid/media/AudioService;->access$200(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@531
    .line 4789
    move-object/from16 v0, p0

    #@533
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@535
    invoke-static {v3}, Landroid/media/AudioService;->access$9100(Landroid/media/AudioService;)V

    #@538
    .line 4792
    move-object/from16 v0, p0

    #@53a
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@53c
    const/4 v4, 0x1

    #@53d
    invoke-static {v3, v4}, Landroid/media/AudioService;->access$9200(Landroid/media/AudioService;Z)V

    #@540
    .line 4794
    move-object/from16 v0, p0

    #@542
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@544
    invoke-static {v3}, Landroid/media/AudioService;->access$100(Landroid/media/AudioService;)Landroid/media/AudioService$AudioHandler;

    #@547
    move-result-object v3

    #@548
    const/16 v4, 0xe

    #@54a
    const/4 v5, 0x2

    #@54b
    const/4 v6, 0x0

    #@54c
    const/4 v7, 0x0

    #@54d
    move-object/from16 v0, p0

    #@54f
    iget-object v8, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@551
    invoke-static {v8}, Landroid/media/AudioService;->access$4200(Landroid/media/AudioService;)[Landroid/media/AudioService$VolumeStreamState;

    #@554
    move-result-object v8

    #@555
    const/4 v9, 0x3

    #@556
    aget-object v8, v8, v9

    #@558
    const/4 v9, 0x0

    #@559
    invoke-static/range {v3 .. v9}, Landroid/media/AudioService;->access$200(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@55c
    goto/16 :goto_3b

    #@55e
    .line 4800
    :cond_55e
    const-string/jumbo v3, "qualcomm.intent.action.WIFI_DISPLAY_AUDIO"

    #@561
    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@564
    move-result v3

    #@565
    if-eqz v3, :cond_5e1

    #@567
    .line 4801
    const-string/jumbo v3, "state"

    #@56a
    const/4 v4, 0x0

    #@56b
    move-object/from16 v0, p2

    #@56d
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@570
    move-result v34

    #@571
    .line 4802
    .restart local v34       #state:I
    const-string v3, "AudioService"

    #@573
    new-instance v4, Ljava/lang/StringBuilder;

    #@575
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@578
    const-string v5, "WiFi Display device state "

    #@57a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57d
    move-result-object v4

    #@57e
    move/from16 v0, v34

    #@580
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@583
    move-result-object v4

    #@584
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@587
    move-result-object v4

    #@588
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@58b
    .line 4803
    move-object/from16 v0, p0

    #@58d
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@58f
    invoke-static {v3}, Landroid/media/AudioService;->access$400(Landroid/media/AudioService;)Ljava/util/HashMap;

    #@592
    move-result-object v3

    #@593
    const/high16 v4, 0x4

    #@595
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@598
    move-result-object v4

    #@599
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@59c
    move-result v28

    #@59d
    .line 4804
    .restart local v28       #isConnected:Z
    const/4 v3, 0x1

    #@59e
    move/from16 v0, v34

    #@5a0
    if-ne v0, v3, :cond_5c2

    #@5a2
    if-nez v28, :cond_5c2

    #@5a4
    .line 4805
    const/high16 v3, 0x4

    #@5a6
    const/4 v4, 0x1

    #@5a7
    const-string v5, ""

    #@5a9
    invoke-static {v3, v4, v5}, Landroid/media/AudioSystem;->setDeviceConnectionState(IILjava/lang/String;)I

    #@5ac
    .line 4808
    move-object/from16 v0, p0

    #@5ae
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@5b0
    invoke-static {v3}, Landroid/media/AudioService;->access$400(Landroid/media/AudioService;)Ljava/util/HashMap;

    #@5b3
    move-result-object v3

    #@5b4
    new-instance v4, Ljava/lang/Integer;

    #@5b6
    const/high16 v5, 0x4

    #@5b8
    invoke-direct {v4, v5}, Ljava/lang/Integer;-><init>(I)V

    #@5bb
    const-string v5, ""

    #@5bd
    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5c0
    goto/16 :goto_3b

    #@5c2
    .line 4809
    :cond_5c2
    if-nez v34, :cond_3b

    #@5c4
    if-eqz v28, :cond_3b

    #@5c6
    .line 4810
    const/high16 v3, 0x4

    #@5c8
    const/4 v4, 0x0

    #@5c9
    const-string v5, ""

    #@5cb
    invoke-static {v3, v4, v5}, Landroid/media/AudioSystem;->setDeviceConnectionState(IILjava/lang/String;)I

    #@5ce
    .line 4813
    move-object/from16 v0, p0

    #@5d0
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@5d2
    invoke-static {v3}, Landroid/media/AudioService;->access$400(Landroid/media/AudioService;)Ljava/util/HashMap;

    #@5d5
    move-result-object v3

    #@5d6
    const/high16 v4, 0x4

    #@5d8
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5db
    move-result-object v4

    #@5dc
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@5df
    goto/16 :goto_3b

    #@5e1
    .line 4815
    .end local v28           #isConnected:Z
    .end local v34           #state:I
    :cond_5e1
    const-string v3, "android.intent.action.LOCALE_CHANGED"

    #@5e3
    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5e6
    move-result v3

    #@5e7
    if-eqz v3, :cond_3b

    #@5e9
    .line 4816
    move-object/from16 v0, p0

    #@5eb
    iget-object v3, v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;->this$0:Landroid/media/AudioService;

    #@5ed
    move-object/from16 v0, p1

    #@5ef
    invoke-static {v3, v0}, Landroid/media/AudioService;->access$9300(Landroid/media/AudioService;Landroid/content/Context;)V

    #@5f2
    goto/16 :goto_3b

    #@5f4
    .line 4728
    .restart local v11       #adapter:Landroid/bluetooth/BluetoothAdapter;
    .restart local v26       #file:Ljava/io/FileReader;
    .restart local v30       #newIntent:Landroid/content/Intent;
    :catch_5f4
    move-exception v25

    #@5f5
    .line 4729
    .restart local v25       #e:Ljava/io/IOException;
    const-string v4, "AudioService"

    #@5f7
    const-string v5, ""

    #@5f9
    move-object/from16 v0, v25

    #@5fb
    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@5fe
    goto/16 :goto_3ca

    #@600
    .line 4728
    .end local v25           #e:Ljava/io/IOException;
    .end local v26           #file:Ljava/io/FileReader;
    .restart local v19       #buffer:[C
    .restart local v22       #curState:I
    .restart local v27       #file:Ljava/io/FileReader;
    .restart local v29       #len:I
    :catch_600
    move-exception v25

    #@601
    .line 4729
    .restart local v25       #e:Ljava/io/IOException;
    const-string v3, "AudioService"

    #@603
    const-string v4, ""

    #@605
    move-object/from16 v0, v25

    #@607
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@60a
    goto/16 :goto_36c

    #@60c
    .line 4725
    .end local v25           #e:Ljava/io/IOException;
    .end local v29           #len:I
    :catchall_60c
    move-exception v3

    #@60d
    move-object/from16 v26, v27

    #@60f
    .end local v27           #file:Ljava/io/FileReader;
    .restart local v26       #file:Ljava/io/FileReader;
    goto/16 :goto_3c5

    #@611
    .line 4722
    .end local v19           #buffer:[C
    .end local v22           #curState:I
    :catch_611
    move-exception v25

    #@612
    goto/16 :goto_3ae

    #@614
    .line 4720
    :catch_614
    move-exception v25

    #@615
    goto/16 :goto_381

    #@617
    .line 4520
    nop

    #@618
    :pswitch_data_618
    .packed-switch 0x1
        :pswitch_3c
        :pswitch_3f
        :pswitch_42
        :pswitch_45
    .end packed-switch

    #@624
    .line 4566
    :sswitch_data_624
    .sparse-switch
        0x404 -> :sswitch_af
        0x408 -> :sswitch_af
        0x420 -> :sswitch_af
    .end sparse-switch

    #@632
    .line 4637
    :pswitch_data_632
    .packed-switch 0xa
        :pswitch_251
        :pswitch_265
        :pswitch_222
    .end packed-switch
.end method
