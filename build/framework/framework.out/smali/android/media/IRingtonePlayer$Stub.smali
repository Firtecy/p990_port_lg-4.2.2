.class public abstract Landroid/media/IRingtonePlayer$Stub;
.super Landroid/os/Binder;
.source "IRingtonePlayer.java"

# interfaces
.implements Landroid/media/IRingtonePlayer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/IRingtonePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/IRingtonePlayer$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.media.IRingtonePlayer"

.field static final TRANSACTION_isPlaying:I = 0x3

.field static final TRANSACTION_play:I = 0x1

.field static final TRANSACTION_playAsync:I = 0x4

.field static final TRANSACTION_stop:I = 0x2

.field static final TRANSACTION_stopAsync:I = 0x5


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 18
    const-string v0, "android.media.IRingtonePlayer"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/media/IRingtonePlayer$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 19
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/media/IRingtonePlayer;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 26
    if-nez p0, :cond_4

    #@2
    .line 27
    const/4 v0, 0x0

    #@3
    .line 33
    :goto_3
    return-object v0

    #@4
    .line 29
    :cond_4
    const-string v1, "android.media.IRingtonePlayer"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 30
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/media/IRingtonePlayer;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 31
    check-cast v0, Landroid/media/IRingtonePlayer;

    #@12
    goto :goto_3

    #@13
    .line 33
    :cond_13
    new-instance v0, Landroid/media/IRingtonePlayer$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/media/IRingtonePlayer$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 13
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 41
    sparse-switch p1, :sswitch_data_a2

    #@5
    .line 118
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v6

    #@9
    :goto_9
    return v6

    #@a
    .line 45
    :sswitch_a
    const-string v5, "android.media.IRingtonePlayer"

    #@c
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 50
    :sswitch_10
    const-string v5, "android.media.IRingtonePlayer"

    #@12
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@18
    move-result-object v0

    #@19
    .line 54
    .local v0, _arg0:Landroid/os/IBinder;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1c
    move-result v5

    #@1d
    if-eqz v5, :cond_32

    #@1f
    .line 55
    sget-object v5, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@21
    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@24
    move-result-object v1

    #@25
    check-cast v1, Landroid/net/Uri;

    #@27
    .line 61
    .local v1, _arg1:Landroid/net/Uri;
    :goto_27
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2a
    move-result v2

    #@2b
    .line 62
    .local v2, _arg2:I
    invoke-virtual {p0, v0, v1, v2}, Landroid/media/IRingtonePlayer$Stub;->play(Landroid/os/IBinder;Landroid/net/Uri;I)V

    #@2e
    .line 63
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@31
    goto :goto_9

    #@32
    .line 58
    .end local v1           #_arg1:Landroid/net/Uri;
    .end local v2           #_arg2:I
    :cond_32
    const/4 v1, 0x0

    #@33
    .restart local v1       #_arg1:Landroid/net/Uri;
    goto :goto_27

    #@34
    .line 68
    .end local v0           #_arg0:Landroid/os/IBinder;
    .end local v1           #_arg1:Landroid/net/Uri;
    :sswitch_34
    const-string v5, "android.media.IRingtonePlayer"

    #@36
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@39
    .line 70
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@3c
    move-result-object v0

    #@3d
    .line 71
    .restart local v0       #_arg0:Landroid/os/IBinder;
    invoke-virtual {p0, v0}, Landroid/media/IRingtonePlayer$Stub;->stop(Landroid/os/IBinder;)V

    #@40
    .line 72
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@43
    goto :goto_9

    #@44
    .line 77
    .end local v0           #_arg0:Landroid/os/IBinder;
    :sswitch_44
    const-string v7, "android.media.IRingtonePlayer"

    #@46
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@49
    .line 79
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@4c
    move-result-object v0

    #@4d
    .line 80
    .restart local v0       #_arg0:Landroid/os/IBinder;
    invoke-virtual {p0, v0}, Landroid/media/IRingtonePlayer$Stub;->isPlaying(Landroid/os/IBinder;)Z

    #@50
    move-result v4

    #@51
    .line 81
    .local v4, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@54
    .line 82
    if-eqz v4, :cond_57

    #@56
    move v5, v6

    #@57
    :cond_57
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@5a
    goto :goto_9

    #@5b
    .line 87
    .end local v0           #_arg0:Landroid/os/IBinder;
    .end local v4           #_result:Z
    :sswitch_5b
    const-string v7, "android.media.IRingtonePlayer"

    #@5d
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@60
    .line 89
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@63
    move-result v7

    #@64
    if-eqz v7, :cond_8f

    #@66
    .line 90
    sget-object v7, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@68
    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@6b
    move-result-object v0

    #@6c
    check-cast v0, Landroid/net/Uri;

    #@6e
    .line 96
    .local v0, _arg0:Landroid/net/Uri;
    :goto_6e
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@71
    move-result v7

    #@72
    if-eqz v7, :cond_91

    #@74
    .line 97
    sget-object v7, Landroid/os/UserHandle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@76
    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@79
    move-result-object v1

    #@7a
    check-cast v1, Landroid/os/UserHandle;

    #@7c
    .line 103
    .local v1, _arg1:Landroid/os/UserHandle;
    :goto_7c
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@7f
    move-result v7

    #@80
    if-eqz v7, :cond_93

    #@82
    move v2, v6

    #@83
    .line 105
    .local v2, _arg2:Z
    :goto_83
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@86
    move-result v3

    #@87
    .line 106
    .local v3, _arg3:I
    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/media/IRingtonePlayer$Stub;->playAsync(Landroid/net/Uri;Landroid/os/UserHandle;ZI)V

    #@8a
    .line 107
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@8d
    goto/16 :goto_9

    #@8f
    .line 93
    .end local v0           #_arg0:Landroid/net/Uri;
    .end local v1           #_arg1:Landroid/os/UserHandle;
    .end local v2           #_arg2:Z
    .end local v3           #_arg3:I
    :cond_8f
    const/4 v0, 0x0

    #@90
    .restart local v0       #_arg0:Landroid/net/Uri;
    goto :goto_6e

    #@91
    .line 100
    :cond_91
    const/4 v1, 0x0

    #@92
    .restart local v1       #_arg1:Landroid/os/UserHandle;
    goto :goto_7c

    #@93
    :cond_93
    move v2, v5

    #@94
    .line 103
    goto :goto_83

    #@95
    .line 112
    .end local v0           #_arg0:Landroid/net/Uri;
    .end local v1           #_arg1:Landroid/os/UserHandle;
    :sswitch_95
    const-string v5, "android.media.IRingtonePlayer"

    #@97
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9a
    .line 113
    invoke-virtual {p0}, Landroid/media/IRingtonePlayer$Stub;->stopAsync()V

    #@9d
    .line 114
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a0
    goto/16 :goto_9

    #@a2
    .line 41
    :sswitch_data_a2
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_34
        0x3 -> :sswitch_44
        0x4 -> :sswitch_5b
        0x5 -> :sswitch_95
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
