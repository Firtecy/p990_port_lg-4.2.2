.class public final Landroid/media/MediaExtractor;
.super Ljava/lang/Object;
.source "MediaExtractor.java"


# static fields
.field public static final SAMPLE_FLAG_ENCRYPTED:I = 0x2

.field public static final SAMPLE_FLAG_SYNC:I = 0x1

.field public static final SEEK_TO_CLOSEST_SYNC:I = 0x2

.field public static final SEEK_TO_NEXT_SYNC:I = 0x1

.field public static final SEEK_TO_PREVIOUS_SYNC:I


# instance fields
.field private mNativeContext:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 315
    const-string/jumbo v0, "media_jni"

    #@3
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@6
    .line 316
    invoke-static {}, Landroid/media/MediaExtractor;->native_init()V

    #@9
    .line 317
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 59
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 60
    invoke-direct {p0}, Landroid/media/MediaExtractor;->native_setup()V

    #@6
    .line 61
    return-void
.end method

.method private native getTrackFormatNative(I)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private final native native_finalize()V
.end method

.method private static final native native_init()V
.end method

.method private final native native_setup()V
.end method

.method private final native setDataSource(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V
.end method


# virtual methods
.method public native advance()Z
.end method

.method protected finalize()V
    .registers 1

    #@0
    .prologue
    .line 182
    invoke-direct {p0}, Landroid/media/MediaExtractor;->native_finalize()V

    #@3
    .line 183
    return-void
.end method

.method public native getCachedDuration()J
.end method

.method public native getSampleCryptoInfo(Landroid/media/MediaCodec$CryptoInfo;)Z
.end method

.method public native getSampleFlags()I
.end method

.method public native getSampleTime()J
.end method

.method public native getSampleTrackIndex()I
.end method

.method public final native getTrackCount()I
.end method

.method public getTrackFormat(I)Landroid/media/MediaFormat;
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 202
    new-instance v0, Landroid/media/MediaFormat;

    #@2
    invoke-direct {p0, p1}, Landroid/media/MediaExtractor;->getTrackFormatNative(I)Ljava/util/Map;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1}, Landroid/media/MediaFormat;-><init>(Ljava/util/Map;)V

    #@9
    return-object v0
.end method

.method public native hasCacheReachedEndOfStream()Z
.end method

.method public native readSampleData(Ljava/nio/ByteBuffer;I)I
.end method

.method public final native release()V
.end method

.method public native seekTo(JI)V
.end method

.method public native selectTrack(I)V
.end method

.method public final setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V
    .registers 13
    .parameter "context"
    .parameter "uri"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 79
    .local p3, headers:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@3
    move-result-object v8

    #@4
    .line 80
    .local v8, scheme:Ljava/lang/String;
    if-eqz v8, :cond_e

    #@6
    const-string v0, "file"

    #@8
    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_16

    #@e
    .line 81
    :cond_e
    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {p0, v0}, Landroid/media/MediaExtractor;->setDataSource(Ljava/lang/String;)V

    #@15
    .line 113
    :cond_15
    :goto_15
    return-void

    #@16
    .line 85
    :cond_16
    const/4 v6, 0x0

    #@17
    .line 87
    .local v6, fd:Landroid/content/res/AssetFileDescriptor;
    :try_start_17
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1a
    move-result-object v7

    #@1b
    .line 88
    .local v7, resolver:Landroid/content/ContentResolver;
    const-string/jumbo v0, "r"

    #@1e
    invoke-virtual {v7, p2, v0}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    :try_end_21
    .catchall {:try_start_17 .. :try_end_21} :catchall_67
    .catch Ljava/lang/SecurityException; {:try_start_17 .. :try_end_21} :catch_52
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_21} :catch_60

    #@21
    move-result-object v6

    #@22
    .line 89
    if-nez v6, :cond_2a

    #@24
    .line 107
    if-eqz v6, :cond_15

    #@26
    .line 108
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V

    #@29
    goto :goto_15

    #@2a
    .line 95
    :cond_2a
    :try_start_2a
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getDeclaredLength()J

    #@2d
    move-result-wide v0

    #@2e
    const-wide/16 v2, 0x0

    #@30
    cmp-long v0, v0, v2

    #@32
    if-gez v0, :cond_41

    #@34
    .line 96
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@37
    move-result-object v0

    #@38
    invoke-virtual {p0, v0}, Landroid/media/MediaExtractor;->setDataSource(Ljava/io/FileDescriptor;)V
    :try_end_3b
    .catchall {:try_start_2a .. :try_end_3b} :catchall_67
    .catch Ljava/lang/SecurityException; {:try_start_2a .. :try_end_3b} :catch_52
    .catch Ljava/io/IOException; {:try_start_2a .. :try_end_3b} :catch_60

    #@3b
    .line 107
    :goto_3b
    if-eqz v6, :cond_15

    #@3d
    .line 108
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V

    #@40
    goto :goto_15

    #@41
    .line 98
    :cond_41
    :try_start_41
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@44
    move-result-object v1

    #@45
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    #@48
    move-result-wide v2

    #@49
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getDeclaredLength()J

    #@4c
    move-result-wide v4

    #@4d
    move-object v0, p0

    #@4e
    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaExtractor;->setDataSource(Ljava/io/FileDescriptor;JJ)V
    :try_end_51
    .catchall {:try_start_41 .. :try_end_51} :catchall_67
    .catch Ljava/lang/SecurityException; {:try_start_41 .. :try_end_51} :catch_52
    .catch Ljava/io/IOException; {:try_start_41 .. :try_end_51} :catch_60

    #@51
    goto :goto_3b

    #@52
    .line 104
    .end local v7           #resolver:Landroid/content/ContentResolver;
    :catch_52
    move-exception v0

    #@53
    .line 107
    if-eqz v6, :cond_58

    #@55
    .line 108
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V

    #@58
    .line 112
    :cond_58
    :goto_58
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@5b
    move-result-object v0

    #@5c
    invoke-virtual {p0, v0, p3}, Landroid/media/MediaExtractor;->setDataSource(Ljava/lang/String;Ljava/util/Map;)V

    #@5f
    goto :goto_15

    #@60
    .line 105
    :catch_60
    move-exception v0

    #@61
    .line 107
    if-eqz v6, :cond_58

    #@63
    .line 108
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V

    #@66
    goto :goto_58

    #@67
    .line 107
    :catchall_67
    move-exception v0

    #@68
    if-eqz v6, :cond_6d

    #@6a
    .line 108
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V

    #@6d
    :cond_6d
    throw v0
.end method

.method public final native setDataSource(Landroid/media/DataSource;)V
.end method

.method public final setDataSource(Ljava/io/FileDescriptor;)V
    .registers 8
    .parameter "fd"

    #@0
    .prologue
    .line 165
    const-wide/16 v2, 0x0

    #@2
    const-wide v4, 0x7ffffffffffffffL

    #@7
    move-object v0, p0

    #@8
    move-object v1, p1

    #@9
    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaExtractor;->setDataSource(Ljava/io/FileDescriptor;JJ)V

    #@c
    .line 166
    return-void
.end method

.method public final native setDataSource(Ljava/io/FileDescriptor;JJ)V
.end method

.method public final setDataSource(Ljava/lang/String;)V
    .registers 3
    .parameter "path"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 155
    invoke-direct {p0, p1, v0, v0}, Landroid/media/MediaExtractor;->setDataSource(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    #@4
    .line 156
    return-void
.end method

.method public final setDataSource(Ljava/lang/String;Ljava/util/Map;)V
    .registers 9
    .parameter "path"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 122
    .local p2, headers:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v3, 0x0

    #@1
    .line 123
    .local v3, keys:[Ljava/lang/String;
    const/4 v4, 0x0

    #@2
    .line 125
    .local v4, values:[Ljava/lang/String;
    if-eqz p2, :cond_38

    #@4
    .line 126
    invoke-interface {p2}, Ljava/util/Map;->size()I

    #@7
    move-result v5

    #@8
    new-array v3, v5, [Ljava/lang/String;

    #@a
    .line 127
    invoke-interface {p2}, Ljava/util/Map;->size()I

    #@d
    move-result v5

    #@e
    new-array v4, v5, [Ljava/lang/String;

    #@10
    .line 129
    const/4 v1, 0x0

    #@11
    .line 130
    .local v1, i:I
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@14
    move-result-object v5

    #@15
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@18
    move-result-object v2

    #@19
    .local v2, i$:Ljava/util/Iterator;
    :goto_19
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@1c
    move-result v5

    #@1d
    if-eqz v5, :cond_38

    #@1f
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@22
    move-result-object v0

    #@23
    check-cast v0, Ljava/util/Map$Entry;

    #@25
    .line 131
    .local v0, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@28
    move-result-object v5

    #@29
    check-cast v5, Ljava/lang/String;

    #@2b
    aput-object v5, v3, v1

    #@2d
    .line 132
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@30
    move-result-object v5

    #@31
    check-cast v5, Ljava/lang/String;

    #@33
    aput-object v5, v4, v1

    #@35
    .line 133
    add-int/lit8 v1, v1, 0x1

    #@37
    goto :goto_19

    #@38
    .line 136
    .end local v0           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1           #i:I
    .end local v2           #i$:Ljava/util/Iterator;
    :cond_38
    invoke-direct {p0, p1, v3, v4}, Landroid/media/MediaExtractor;->setDataSource(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    #@3b
    .line 137
    return-void
.end method

.method public native unselectTrack(I)V
.end method
