.class public final Landroid/media/MediaCodec;
.super Ljava/lang/Object;
.source "MediaCodec.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/MediaCodec$CryptoInfo;,
        Landroid/media/MediaCodec$CryptoException;,
        Landroid/media/MediaCodec$BufferInfo;
    }
.end annotation


# static fields
.field public static final BUFFER_FLAG_CODEC_CONFIG:I = 0x2

.field public static final BUFFER_FLAG_END_OF_STREAM:I = 0x4

.field public static final BUFFER_FLAG_SYNC_FRAME:I = 0x1

.field public static final CONFIGURE_FLAG_ENCODE:I = 0x1

.field public static final CRYPTO_MODE_AES_CTR:I = 0x1

.field public static final CRYPTO_MODE_UNENCRYPTED:I = 0x0

.field public static final INFO_OUTPUT_BUFFERS_CHANGED:I = -0x3

.field public static final INFO_OUTPUT_FORMAT_CHANGED:I = -0x2

.field public static final INFO_TRY_AGAIN_LATER:I = -0x1

.field public static final VIDEO_SCALING_MODE_SCALE_TO_FIT:I = 0x1

.field public static final VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING:I = 0x2


# instance fields
.field private mNativeContext:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 511
    const-string/jumbo v0, "media_jni"

    #@3
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@6
    .line 512
    invoke-static {}, Landroid/media/MediaCodec;->native_init()V

    #@9
    .line 513
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ZZ)V
    .registers 4
    .parameter "name"
    .parameter "nameIsType"
    .parameter "encoder"

    #@0
    .prologue
    .line 203
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 204
    invoke-direct {p0, p1, p2, p3}, Landroid/media/MediaCodec;->native_setup(Ljava/lang/String;ZZ)V

    #@6
    .line 205
    return-void
.end method

.method public static createByCodecName(Ljava/lang/String;)Landroid/media/MediaCodec;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 198
    new-instance v0, Landroid/media/MediaCodec;

    #@3
    invoke-direct {v0, p0, v1, v1}, Landroid/media/MediaCodec;-><init>(Ljava/lang/String;ZZ)V

    #@6
    return-object v0
.end method

.method public static createDecoderByType(Ljava/lang/String;)Landroid/media/MediaCodec;
    .registers 4
    .parameter "type"

    #@0
    .prologue
    .line 180
    new-instance v0, Landroid/media/MediaCodec;

    #@2
    const/4 v1, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    invoke-direct {v0, p0, v1, v2}, Landroid/media/MediaCodec;-><init>(Ljava/lang/String;ZZ)V

    #@7
    return-object v0
.end method

.method public static createEncoderByType(Ljava/lang/String;)Landroid/media/MediaCodec;
    .registers 3
    .parameter "type"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 188
    new-instance v0, Landroid/media/MediaCodec;

    #@3
    invoke-direct {v0, p0, v1, v1}, Landroid/media/MediaCodec;-><init>(Ljava/lang/String;ZZ)V

    #@6
    return-object v0
.end method

.method private final native getBuffers(Z)[Ljava/nio/ByteBuffer;
.end method

.method private final native getOutputFormatNative()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method private final native native_configure([Ljava/lang/String;[Ljava/lang/Object;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V
.end method

.method private final native native_finalize()V
.end method

.method private static final native native_init()V
.end method

.method private final native native_setup(Ljava/lang/String;ZZ)V
.end method


# virtual methods
.method public configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V
    .registers 15
    .parameter "format"
    .parameter "surface"
    .parameter "crypto"
    .parameter "flags"

    #@0
    .prologue
    .line 239
    invoke-virtual {p1}, Landroid/media/MediaFormat;->getMap()Ljava/util/Map;

    #@3
    move-result-object v7

    #@4
    .line 241
    .local v7, formatMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v1, 0x0

    #@5
    .line 242
    .local v1, keys:[Ljava/lang/String;
    const/4 v2, 0x0

    #@6
    .line 244
    .local v2, values:[Ljava/lang/Object;
    if-eqz p1, :cond_3a

    #@8
    .line 245
    invoke-interface {v7}, Ljava/util/Map;->size()I

    #@b
    move-result v0

    #@c
    new-array v1, v0, [Ljava/lang/String;

    #@e
    .line 246
    invoke-interface {v7}, Ljava/util/Map;->size()I

    #@11
    move-result v0

    #@12
    new-array v2, v0, [Ljava/lang/Object;

    #@14
    .line 248
    const/4 v8, 0x0

    #@15
    .line 249
    .local v8, i:I
    invoke-interface {v7}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@18
    move-result-object v0

    #@19
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@1c
    move-result-object v9

    #@1d
    .local v9, i$:Ljava/util/Iterator;
    :goto_1d
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    #@20
    move-result v0

    #@21
    if-eqz v0, :cond_3a

    #@23
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@26
    move-result-object v6

    #@27
    check-cast v6, Ljava/util/Map$Entry;

    #@29
    .line 250
    .local v6, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@2c
    move-result-object v0

    #@2d
    check-cast v0, Ljava/lang/String;

    #@2f
    aput-object v0, v1, v8

    #@31
    .line 251
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@34
    move-result-object v0

    #@35
    aput-object v0, v2, v8

    #@37
    .line 252
    add-int/lit8 v8, v8, 0x1

    #@39
    goto :goto_1d

    #@3a
    .end local v6           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v8           #i:I
    .end local v9           #i$:Ljava/util/Iterator;
    :cond_3a
    move-object v0, p0

    #@3b
    move-object v3, p2

    #@3c
    move-object v4, p3

    #@3d
    move v5, p4

    #@3e
    .line 256
    invoke-direct/range {v0 .. v5}, Landroid/media/MediaCodec;->native_configure([Ljava/lang/String;[Ljava/lang/Object;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    #@41
    .line 257
    return-void
.end method

.method public final native dequeueInputBuffer(J)I
.end method

.method public final native dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I
.end method

.method protected finalize()V
    .registers 1

    #@0
    .prologue
    .line 209
    invoke-direct {p0}, Landroid/media/MediaCodec;->native_finalize()V

    #@3
    .line 210
    return-void
.end method

.method public final native flush()V
.end method

.method public getInputBuffers()[Ljava/nio/ByteBuffer;
    .registers 2

    #@0
    .prologue
    .line 472
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, v0}, Landroid/media/MediaCodec;->getBuffers(Z)[Ljava/nio/ByteBuffer;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public getOutputBuffers()[Ljava/nio/ByteBuffer;
    .registers 2

    #@0
    .prologue
    .line 481
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Landroid/media/MediaCodec;->getBuffers(Z)[Ljava/nio/ByteBuffer;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public final getOutputFormat()Landroid/media/MediaFormat;
    .registers 3

    #@0
    .prologue
    .line 463
    new-instance v0, Landroid/media/MediaFormat;

    #@2
    invoke-direct {p0}, Landroid/media/MediaCodec;->getOutputFormatNative()Ljava/util/Map;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1}, Landroid/media/MediaFormat;-><init>(Ljava/util/Map;)V

    #@9
    return-object v0
.end method

.method public final native queueInputBuffer(IIIJI)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/media/MediaCodec$CryptoException;
        }
    .end annotation
.end method

.method public final native queueSecureInputBuffer(IILandroid/media/MediaCodec$CryptoInfo;JI)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/media/MediaCodec$CryptoException;
        }
    .end annotation
.end method

.method public final native release()V
.end method

.method public final native releaseOutputBuffer(IZ)V
.end method

.method public final native setVideoScalingMode(I)V
.end method

.method public final native start()V
.end method

.method public final native stop()V
.end method
