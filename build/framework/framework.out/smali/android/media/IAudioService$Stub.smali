.class public abstract Landroid/media/IAudioService$Stub;
.super Landroid/os/Binder;
.source "IAudioService.java"

# interfaces
.implements Landroid/media/IAudioService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/IAudioService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/IAudioService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.media.IAudioService"

.field static final TRANSACTION_abandonAudioFocus:I = 0x2b

.field static final TRANSACTION_addMediaButtonEventReceiverForCalls:I = 0x4b

.field static final TRANSACTION_adjustLocalOrRemoteStreamVolume:I = 0x2

.field static final TRANSACTION_adjustMasterVolume:I = 0x5

.field static final TRANSACTION_adjustStreamVolume:I = 0x4

.field static final TRANSACTION_adjustSuggestedStreamVolume:I = 0x3

.field static final TRANSACTION_adjustVolume:I = 0x1

.field static final TRANSACTION_checkPlayConditions:I = 0x4d

.field static final TRANSACTION_dispatchMediaKeyEvent:I = 0x2d

.field static final TRANSACTION_dispatchMediaKeyEventUnderWakelock:I = 0x2e

.field static final TRANSACTION_forceVolumeControlStream:I = 0x41

.field static final TRANSACTION_getInCallMode:I = 0x1d

.field static final TRANSACTION_getLastAudibleMasterVolume:I = 0x14

.field static final TRANSACTION_getLastAudibleStreamVolume:I = 0x13

.field static final TRANSACTION_getMasterMaxVolume:I = 0x12

.field static final TRANSACTION_getMasterStreamType:I = 0x44

.field static final TRANSACTION_getMasterVolume:I = 0x10

.field static final TRANSACTION_getMode:I = 0x1c

.field static final TRANSACTION_getRecordHookingState:I = 0x52

.field static final TRANSACTION_getRemoteStreamMaxVolume:I = 0x39

.field static final TRANSACTION_getRemoteStreamVolume:I = 0x3a

.field static final TRANSACTION_getRingerMode:I = 0x16

.field static final TRANSACTION_getRingtonePlayer:I = 0x43

.field static final TRANSACTION_getStreamMaxVolume:I = 0x11

.field static final TRANSACTION_getStreamVolume:I = 0xf

.field static final TRANSACTION_getVibrateSetting:I = 0x18

.field static final TRANSACTION_getVoiceActivationState:I = 0x4f

.field static final TRANSACTION_isBluetoothA2dpOn:I = 0x28

.field static final TRANSACTION_isBluetoothAGConnected:I = 0x3e

.field static final TRANSACTION_isBluetoothAVConnected:I = 0x3f

.field static final TRANSACTION_isBluetoothScoOn:I = 0x26

.field static final TRANSACTION_isCameraSoundForced:I = 0x48

.field static final TRANSACTION_isMasterMute:I = 0xe

.field static final TRANSACTION_isSpeakerOnForMedia:I = 0x4a

.field static final TRANSACTION_isSpeakerphoneOn:I = 0x24

.field static final TRANSACTION_isStreamMute:I = 0xc

.field static final TRANSACTION_isWiredHeadsetConnected:I = 0x40

.field static final TRANSACTION_loadSoundEffects:I = 0x20

.field static final TRANSACTION_playSoundEffect:I = 0x1e

.field static final TRANSACTION_playSoundEffectVolume:I = 0x1f

.field static final TRANSACTION_registerMediaButtonEventReceiverForCalls:I = 0x31

.field static final TRANSACTION_registerMediaButtonIntent:I = 0x2f

.field static final TRANSACTION_registerRemoteControlClient:I = 0x33

.field static final TRANSACTION_registerRemoteControlDisplay:I = 0x35

.field static final TRANSACTION_registerRemoteVolumeObserverForRcc:I = 0x3b

.field static final TRANSACTION_reloadAudioSettings:I = 0x22

.field static final TRANSACTION_remoteControlDisplayUsesBitmapSize:I = 0x37

.field static final TRANSACTION_removeMediaButtonEventReceiverForCalls:I = 0x4c

.field static final TRANSACTION_requestAudioFocus:I = 0x2a

.field static final TRANSACTION_sendBroadcastRecordState:I = 0x50

.field static final TRANSACTION_setBluetoothA2dpDeviceConnectionState:I = 0x46

.field static final TRANSACTION_setBluetoothA2dpOn:I = 0x27

.field static final TRANSACTION_setBluetoothScoOn:I = 0x25

.field static final TRANSACTION_setInCallMode:I = 0x1b

.field static final TRANSACTION_setMasterMute:I = 0xd

.field static final TRANSACTION_setMasterVolume:I = 0x9

.field static final TRANSACTION_setMode:I = 0x1a

.field static final TRANSACTION_setPlaybackInfoForRcc:I = 0x38

.field static final TRANSACTION_setRecordHookingState:I = 0x51

.field static final TRANSACTION_setRemoteStreamVolume:I = 0x8

.field static final TRANSACTION_setRemoteSubmixOn:I = 0x29

.field static final TRANSACTION_setRingerMode:I = 0x15

.field static final TRANSACTION_setRingtonePlayer:I = 0x42

.field static final TRANSACTION_setSpeakerOnForMedia:I = 0x49

.field static final TRANSACTION_setSpeakerphoneOn:I = 0x23

.field static final TRANSACTION_setStreamMute:I = 0xb

.field static final TRANSACTION_setStreamSolo:I = 0xa

.field static final TRANSACTION_setStreamVolume:I = 0x6

.field static final TRANSACTION_setStreamVolumeAll:I = 0x7

.field static final TRANSACTION_setVibrateSetting:I = 0x17

.field static final TRANSACTION_setVoiceActivationState:I = 0x4e

.field static final TRANSACTION_setWiredDeviceConnectionState:I = 0x45

.field static final TRANSACTION_shouldVibrate:I = 0x19

.field static final TRANSACTION_startBluetoothSco:I = 0x3c

.field static final TRANSACTION_startWatchingRoutes:I = 0x47

.field static final TRANSACTION_stopBluetoothSco:I = 0x3d

.field static final TRANSACTION_unloadSoundEffects:I = 0x21

.field static final TRANSACTION_unregisterAudioFocusClient:I = 0x2c

.field static final TRANSACTION_unregisterMediaButtonEventReceiverForCalls:I = 0x32

.field static final TRANSACTION_unregisterMediaButtonIntent:I = 0x30

.field static final TRANSACTION_unregisterRemoteControlClient:I = 0x34

.field static final TRANSACTION_unregisterRemoteControlDisplay:I = 0x36


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 18
    const-string v0, "android.media.IAudioService"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/media/IAudioService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 19
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/media/IAudioService;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 26
    if-nez p0, :cond_4

    #@2
    .line 27
    const/4 v0, 0x0

    #@3
    .line 33
    :goto_3
    return-object v0

    #@4
    .line 29
    :cond_4
    const-string v1, "android.media.IAudioService"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 30
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/media/IAudioService;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 31
    check-cast v0, Landroid/media/IAudioService;

    #@12
    goto :goto_3

    #@13
    .line 33
    :cond_13
    new-instance v0, Landroid/media/IAudioService$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/media/IAudioService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 15
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v8, 0x1

    #@2
    .line 41
    sparse-switch p1, :sswitch_data_736

    #@5
    .line 906
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v8

    #@9
    :goto_9
    return v8

    #@a
    .line 45
    :sswitch_a
    const-string v0, "android.media.IAudioService"

    #@c
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 50
    :sswitch_10
    const-string v0, "android.media.IAudioService"

    #@12
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v1

    #@19
    .line 54
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1c
    move-result v2

    #@1d
    .line 55
    .local v2, _arg1:I
    invoke-virtual {p0, v1, v2}, Landroid/media/IAudioService$Stub;->adjustVolume(II)V

    #@20
    .line 56
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@23
    goto :goto_9

    #@24
    .line 61
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    :sswitch_24
    const-string v0, "android.media.IAudioService"

    #@26
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@29
    .line 63
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2c
    move-result v1

    #@2d
    .line 65
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@30
    move-result v2

    #@31
    .line 66
    .restart local v2       #_arg1:I
    invoke-virtual {p0, v1, v2}, Landroid/media/IAudioService$Stub;->adjustLocalOrRemoteStreamVolume(II)V

    #@34
    goto :goto_9

    #@35
    .line 71
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    :sswitch_35
    const-string v0, "android.media.IAudioService"

    #@37
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3a
    .line 73
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3d
    move-result v1

    #@3e
    .line 75
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@41
    move-result v2

    #@42
    .line 77
    .restart local v2       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@45
    move-result v3

    #@46
    .line 78
    .local v3, _arg2:I
    invoke-virtual {p0, v1, v2, v3}, Landroid/media/IAudioService$Stub;->adjustSuggestedStreamVolume(III)V

    #@49
    .line 79
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4c
    goto :goto_9

    #@4d
    .line 84
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    .end local v3           #_arg2:I
    :sswitch_4d
    const-string v0, "android.media.IAudioService"

    #@4f
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@52
    .line 86
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@55
    move-result v1

    #@56
    .line 88
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@59
    move-result v2

    #@5a
    .line 90
    .restart local v2       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@5d
    move-result v3

    #@5e
    .line 91
    .restart local v3       #_arg2:I
    invoke-virtual {p0, v1, v2, v3}, Landroid/media/IAudioService$Stub;->adjustStreamVolume(III)V

    #@61
    .line 92
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@64
    goto :goto_9

    #@65
    .line 97
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    .end local v3           #_arg2:I
    :sswitch_65
    const-string v0, "android.media.IAudioService"

    #@67
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6a
    .line 99
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@6d
    move-result v1

    #@6e
    .line 101
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@71
    move-result v2

    #@72
    .line 102
    .restart local v2       #_arg1:I
    invoke-virtual {p0, v1, v2}, Landroid/media/IAudioService$Stub;->adjustMasterVolume(II)V

    #@75
    .line 103
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@78
    goto :goto_9

    #@79
    .line 108
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    :sswitch_79
    const-string v0, "android.media.IAudioService"

    #@7b
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7e
    .line 110
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@81
    move-result v1

    #@82
    .line 112
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@85
    move-result v2

    #@86
    .line 114
    .restart local v2       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@89
    move-result v3

    #@8a
    .line 115
    .restart local v3       #_arg2:I
    invoke-virtual {p0, v1, v2, v3}, Landroid/media/IAudioService$Stub;->setStreamVolume(III)V

    #@8d
    .line 116
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@90
    goto/16 :goto_9

    #@92
    .line 121
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    .end local v3           #_arg2:I
    :sswitch_92
    const-string v0, "android.media.IAudioService"

    #@94
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@97
    .line 123
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@9a
    move-result v1

    #@9b
    .line 125
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@9e
    move-result v2

    #@9f
    .line 127
    .restart local v2       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@a2
    move-result v3

    #@a3
    .line 128
    .restart local v3       #_arg2:I
    invoke-virtual {p0, v1, v2, v3}, Landroid/media/IAudioService$Stub;->setStreamVolumeAll(III)V

    #@a6
    .line 129
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a9
    goto/16 :goto_9

    #@ab
    .line 134
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    .end local v3           #_arg2:I
    :sswitch_ab
    const-string v0, "android.media.IAudioService"

    #@ad
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b0
    .line 136
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@b3
    move-result v1

    #@b4
    .line 137
    .restart local v1       #_arg0:I
    invoke-virtual {p0, v1}, Landroid/media/IAudioService$Stub;->setRemoteStreamVolume(I)V

    #@b7
    goto/16 :goto_9

    #@b9
    .line 142
    .end local v1           #_arg0:I
    :sswitch_b9
    const-string v0, "android.media.IAudioService"

    #@bb
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@be
    .line 144
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@c1
    move-result v1

    #@c2
    .line 146
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@c5
    move-result v2

    #@c6
    .line 147
    .restart local v2       #_arg1:I
    invoke-virtual {p0, v1, v2}, Landroid/media/IAudioService$Stub;->setMasterVolume(II)V

    #@c9
    .line 148
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@cc
    goto/16 :goto_9

    #@ce
    .line 153
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    :sswitch_ce
    const-string v9, "android.media.IAudioService"

    #@d0
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d3
    .line 155
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@d6
    move-result v1

    #@d7
    .line 157
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@da
    move-result v9

    #@db
    if-eqz v9, :cond_ea

    #@dd
    move v2, v8

    #@de
    .line 159
    .local v2, _arg1:Z
    :goto_de
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@e1
    move-result-object v3

    #@e2
    .line 160
    .local v3, _arg2:Landroid/os/IBinder;
    invoke-virtual {p0, v1, v2, v3}, Landroid/media/IAudioService$Stub;->setStreamSolo(IZLandroid/os/IBinder;)V

    #@e5
    .line 161
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@e8
    goto/16 :goto_9

    #@ea
    .end local v2           #_arg1:Z
    .end local v3           #_arg2:Landroid/os/IBinder;
    :cond_ea
    move v2, v0

    #@eb
    .line 157
    goto :goto_de

    #@ec
    .line 166
    .end local v1           #_arg0:I
    :sswitch_ec
    const-string v9, "android.media.IAudioService"

    #@ee
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f1
    .line 168
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@f4
    move-result v1

    #@f5
    .line 170
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@f8
    move-result v9

    #@f9
    if-eqz v9, :cond_108

    #@fb
    move v2, v8

    #@fc
    .line 172
    .restart local v2       #_arg1:Z
    :goto_fc
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@ff
    move-result-object v3

    #@100
    .line 173
    .restart local v3       #_arg2:Landroid/os/IBinder;
    invoke-virtual {p0, v1, v2, v3}, Landroid/media/IAudioService$Stub;->setStreamMute(IZLandroid/os/IBinder;)V

    #@103
    .line 174
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@106
    goto/16 :goto_9

    #@108
    .end local v2           #_arg1:Z
    .end local v3           #_arg2:Landroid/os/IBinder;
    :cond_108
    move v2, v0

    #@109
    .line 170
    goto :goto_fc

    #@10a
    .line 179
    .end local v1           #_arg0:I
    :sswitch_10a
    const-string v9, "android.media.IAudioService"

    #@10c
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@10f
    .line 181
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@112
    move-result v1

    #@113
    .line 182
    .restart local v1       #_arg0:I
    invoke-virtual {p0, v1}, Landroid/media/IAudioService$Stub;->isStreamMute(I)Z

    #@116
    move-result v7

    #@117
    .line 183
    .local v7, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@11a
    .line 184
    if-eqz v7, :cond_11d

    #@11c
    move v0, v8

    #@11d
    :cond_11d
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@120
    goto/16 :goto_9

    #@122
    .line 189
    .end local v1           #_arg0:I
    .end local v7           #_result:Z
    :sswitch_122
    const-string v9, "android.media.IAudioService"

    #@124
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@127
    .line 191
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@12a
    move-result v9

    #@12b
    if-eqz v9, :cond_13e

    #@12d
    move v1, v8

    #@12e
    .line 193
    .local v1, _arg0:Z
    :goto_12e
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@131
    move-result v2

    #@132
    .line 195
    .local v2, _arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@135
    move-result-object v3

    #@136
    .line 196
    .restart local v3       #_arg2:Landroid/os/IBinder;
    invoke-virtual {p0, v1, v2, v3}, Landroid/media/IAudioService$Stub;->setMasterMute(ZILandroid/os/IBinder;)V

    #@139
    .line 197
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@13c
    goto/16 :goto_9

    #@13e
    .end local v1           #_arg0:Z
    .end local v2           #_arg1:I
    .end local v3           #_arg2:Landroid/os/IBinder;
    :cond_13e
    move v1, v0

    #@13f
    .line 191
    goto :goto_12e

    #@140
    .line 202
    :sswitch_140
    const-string v9, "android.media.IAudioService"

    #@142
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@145
    .line 203
    invoke-virtual {p0}, Landroid/media/IAudioService$Stub;->isMasterMute()Z

    #@148
    move-result v7

    #@149
    .line 204
    .restart local v7       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@14c
    .line 205
    if-eqz v7, :cond_14f

    #@14e
    move v0, v8

    #@14f
    :cond_14f
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@152
    goto/16 :goto_9

    #@154
    .line 210
    .end local v7           #_result:Z
    :sswitch_154
    const-string v0, "android.media.IAudioService"

    #@156
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@159
    .line 212
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@15c
    move-result v1

    #@15d
    .line 213
    .local v1, _arg0:I
    invoke-virtual {p0, v1}, Landroid/media/IAudioService$Stub;->getStreamVolume(I)I

    #@160
    move-result v7

    #@161
    .line 214
    .local v7, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@164
    .line 215
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@167
    goto/16 :goto_9

    #@169
    .line 220
    .end local v1           #_arg0:I
    .end local v7           #_result:I
    :sswitch_169
    const-string v0, "android.media.IAudioService"

    #@16b
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@16e
    .line 221
    invoke-virtual {p0}, Landroid/media/IAudioService$Stub;->getMasterVolume()I

    #@171
    move-result v7

    #@172
    .line 222
    .restart local v7       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@175
    .line 223
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@178
    goto/16 :goto_9

    #@17a
    .line 228
    .end local v7           #_result:I
    :sswitch_17a
    const-string v0, "android.media.IAudioService"

    #@17c
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@17f
    .line 230
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@182
    move-result v1

    #@183
    .line 231
    .restart local v1       #_arg0:I
    invoke-virtual {p0, v1}, Landroid/media/IAudioService$Stub;->getStreamMaxVolume(I)I

    #@186
    move-result v7

    #@187
    .line 232
    .restart local v7       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@18a
    .line 233
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@18d
    goto/16 :goto_9

    #@18f
    .line 238
    .end local v1           #_arg0:I
    .end local v7           #_result:I
    :sswitch_18f
    const-string v0, "android.media.IAudioService"

    #@191
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@194
    .line 239
    invoke-virtual {p0}, Landroid/media/IAudioService$Stub;->getMasterMaxVolume()I

    #@197
    move-result v7

    #@198
    .line 240
    .restart local v7       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@19b
    .line 241
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@19e
    goto/16 :goto_9

    #@1a0
    .line 246
    .end local v7           #_result:I
    :sswitch_1a0
    const-string v0, "android.media.IAudioService"

    #@1a2
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1a5
    .line 248
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1a8
    move-result v1

    #@1a9
    .line 249
    .restart local v1       #_arg0:I
    invoke-virtual {p0, v1}, Landroid/media/IAudioService$Stub;->getLastAudibleStreamVolume(I)I

    #@1ac
    move-result v7

    #@1ad
    .line 250
    .restart local v7       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1b0
    .line 251
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@1b3
    goto/16 :goto_9

    #@1b5
    .line 256
    .end local v1           #_arg0:I
    .end local v7           #_result:I
    :sswitch_1b5
    const-string v0, "android.media.IAudioService"

    #@1b7
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1ba
    .line 257
    invoke-virtual {p0}, Landroid/media/IAudioService$Stub;->getLastAudibleMasterVolume()I

    #@1bd
    move-result v7

    #@1be
    .line 258
    .restart local v7       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1c1
    .line 259
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@1c4
    goto/16 :goto_9

    #@1c6
    .line 264
    .end local v7           #_result:I
    :sswitch_1c6
    const-string v0, "android.media.IAudioService"

    #@1c8
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1cb
    .line 266
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1ce
    move-result v1

    #@1cf
    .line 267
    .restart local v1       #_arg0:I
    invoke-virtual {p0, v1}, Landroid/media/IAudioService$Stub;->setRingerMode(I)V

    #@1d2
    .line 268
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1d5
    goto/16 :goto_9

    #@1d7
    .line 273
    .end local v1           #_arg0:I
    :sswitch_1d7
    const-string v0, "android.media.IAudioService"

    #@1d9
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1dc
    .line 274
    invoke-virtual {p0}, Landroid/media/IAudioService$Stub;->getRingerMode()I

    #@1df
    move-result v7

    #@1e0
    .line 275
    .restart local v7       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1e3
    .line 276
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@1e6
    goto/16 :goto_9

    #@1e8
    .line 281
    .end local v7           #_result:I
    :sswitch_1e8
    const-string v0, "android.media.IAudioService"

    #@1ea
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1ed
    .line 283
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1f0
    move-result v1

    #@1f1
    .line 285
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1f4
    move-result v2

    #@1f5
    .line 286
    .restart local v2       #_arg1:I
    invoke-virtual {p0, v1, v2}, Landroid/media/IAudioService$Stub;->setVibrateSetting(II)V

    #@1f8
    .line 287
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1fb
    goto/16 :goto_9

    #@1fd
    .line 292
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    :sswitch_1fd
    const-string v0, "android.media.IAudioService"

    #@1ff
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@202
    .line 294
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@205
    move-result v1

    #@206
    .line 295
    .restart local v1       #_arg0:I
    invoke-virtual {p0, v1}, Landroid/media/IAudioService$Stub;->getVibrateSetting(I)I

    #@209
    move-result v7

    #@20a
    .line 296
    .restart local v7       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@20d
    .line 297
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@210
    goto/16 :goto_9

    #@212
    .line 302
    .end local v1           #_arg0:I
    .end local v7           #_result:I
    :sswitch_212
    const-string v9, "android.media.IAudioService"

    #@214
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@217
    .line 304
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@21a
    move-result v1

    #@21b
    .line 305
    .restart local v1       #_arg0:I
    invoke-virtual {p0, v1}, Landroid/media/IAudioService$Stub;->shouldVibrate(I)Z

    #@21e
    move-result v7

    #@21f
    .line 306
    .local v7, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@222
    .line 307
    if-eqz v7, :cond_225

    #@224
    move v0, v8

    #@225
    :cond_225
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@228
    goto/16 :goto_9

    #@22a
    .line 312
    .end local v1           #_arg0:I
    .end local v7           #_result:Z
    :sswitch_22a
    const-string v0, "android.media.IAudioService"

    #@22c
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@22f
    .line 314
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@232
    move-result v1

    #@233
    .line 316
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@236
    move-result-object v2

    #@237
    .line 317
    .local v2, _arg1:Landroid/os/IBinder;
    invoke-virtual {p0, v1, v2}, Landroid/media/IAudioService$Stub;->setMode(ILandroid/os/IBinder;)V

    #@23a
    .line 318
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@23d
    goto/16 :goto_9

    #@23f
    .line 323
    .end local v1           #_arg0:I
    .end local v2           #_arg1:Landroid/os/IBinder;
    :sswitch_23f
    const-string v0, "android.media.IAudioService"

    #@241
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@244
    .line 325
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@247
    move-result v1

    #@248
    .line 327
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@24b
    move-result-object v2

    #@24c
    .line 328
    .restart local v2       #_arg1:Landroid/os/IBinder;
    invoke-virtual {p0, v1, v2}, Landroid/media/IAudioService$Stub;->setInCallMode(ILandroid/os/IBinder;)V

    #@24f
    .line 329
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@252
    goto/16 :goto_9

    #@254
    .line 334
    .end local v1           #_arg0:I
    .end local v2           #_arg1:Landroid/os/IBinder;
    :sswitch_254
    const-string v0, "android.media.IAudioService"

    #@256
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@259
    .line 335
    invoke-virtual {p0}, Landroid/media/IAudioService$Stub;->getMode()I

    #@25c
    move-result v7

    #@25d
    .line 336
    .local v7, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@260
    .line 337
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@263
    goto/16 :goto_9

    #@265
    .line 342
    .end local v7           #_result:I
    :sswitch_265
    const-string v0, "android.media.IAudioService"

    #@267
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@26a
    .line 343
    invoke-virtual {p0}, Landroid/media/IAudioService$Stub;->getInCallMode()I

    #@26d
    move-result v7

    #@26e
    .line 344
    .restart local v7       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@271
    .line 345
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@274
    goto/16 :goto_9

    #@276
    .line 350
    .end local v7           #_result:I
    :sswitch_276
    const-string v0, "android.media.IAudioService"

    #@278
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@27b
    .line 352
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@27e
    move-result v1

    #@27f
    .line 353
    .restart local v1       #_arg0:I
    invoke-virtual {p0, v1}, Landroid/media/IAudioService$Stub;->playSoundEffect(I)V

    #@282
    goto/16 :goto_9

    #@284
    .line 358
    .end local v1           #_arg0:I
    :sswitch_284
    const-string v0, "android.media.IAudioService"

    #@286
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@289
    .line 360
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@28c
    move-result v1

    #@28d
    .line 362
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readFloat()F

    #@290
    move-result v2

    #@291
    .line 363
    .local v2, _arg1:F
    invoke-virtual {p0, v1, v2}, Landroid/media/IAudioService$Stub;->playSoundEffectVolume(IF)V

    #@294
    goto/16 :goto_9

    #@296
    .line 368
    .end local v1           #_arg0:I
    .end local v2           #_arg1:F
    :sswitch_296
    const-string v9, "android.media.IAudioService"

    #@298
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@29b
    .line 369
    invoke-virtual {p0}, Landroid/media/IAudioService$Stub;->loadSoundEffects()Z

    #@29e
    move-result v7

    #@29f
    .line 370
    .local v7, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2a2
    .line 371
    if-eqz v7, :cond_2a5

    #@2a4
    move v0, v8

    #@2a5
    :cond_2a5
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2a8
    goto/16 :goto_9

    #@2aa
    .line 376
    .end local v7           #_result:Z
    :sswitch_2aa
    const-string v0, "android.media.IAudioService"

    #@2ac
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2af
    .line 377
    invoke-virtual {p0}, Landroid/media/IAudioService$Stub;->unloadSoundEffects()V

    #@2b2
    goto/16 :goto_9

    #@2b4
    .line 382
    :sswitch_2b4
    const-string v0, "android.media.IAudioService"

    #@2b6
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2b9
    .line 383
    invoke-virtual {p0}, Landroid/media/IAudioService$Stub;->reloadAudioSettings()V

    #@2bc
    goto/16 :goto_9

    #@2be
    .line 388
    :sswitch_2be
    const-string v9, "android.media.IAudioService"

    #@2c0
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2c3
    .line 390
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2c6
    move-result v9

    #@2c7
    if-eqz v9, :cond_2d2

    #@2c9
    move v1, v8

    #@2ca
    .line 391
    .local v1, _arg0:Z
    :goto_2ca
    invoke-virtual {p0, v1}, Landroid/media/IAudioService$Stub;->setSpeakerphoneOn(Z)V

    #@2cd
    .line 392
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2d0
    goto/16 :goto_9

    #@2d2
    .end local v1           #_arg0:Z
    :cond_2d2
    move v1, v0

    #@2d3
    .line 390
    goto :goto_2ca

    #@2d4
    .line 397
    :sswitch_2d4
    const-string v9, "android.media.IAudioService"

    #@2d6
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2d9
    .line 398
    invoke-virtual {p0}, Landroid/media/IAudioService$Stub;->isSpeakerphoneOn()Z

    #@2dc
    move-result v7

    #@2dd
    .line 399
    .restart local v7       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2e0
    .line 400
    if-eqz v7, :cond_2e3

    #@2e2
    move v0, v8

    #@2e3
    :cond_2e3
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2e6
    goto/16 :goto_9

    #@2e8
    .line 405
    .end local v7           #_result:Z
    :sswitch_2e8
    const-string v9, "android.media.IAudioService"

    #@2ea
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2ed
    .line 407
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2f0
    move-result v9

    #@2f1
    if-eqz v9, :cond_2fc

    #@2f3
    move v1, v8

    #@2f4
    .line 408
    .restart local v1       #_arg0:Z
    :goto_2f4
    invoke-virtual {p0, v1}, Landroid/media/IAudioService$Stub;->setBluetoothScoOn(Z)V

    #@2f7
    .line 409
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2fa
    goto/16 :goto_9

    #@2fc
    .end local v1           #_arg0:Z
    :cond_2fc
    move v1, v0

    #@2fd
    .line 407
    goto :goto_2f4

    #@2fe
    .line 414
    :sswitch_2fe
    const-string v9, "android.media.IAudioService"

    #@300
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@303
    .line 415
    invoke-virtual {p0}, Landroid/media/IAudioService$Stub;->isBluetoothScoOn()Z

    #@306
    move-result v7

    #@307
    .line 416
    .restart local v7       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@30a
    .line 417
    if-eqz v7, :cond_30d

    #@30c
    move v0, v8

    #@30d
    :cond_30d
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@310
    goto/16 :goto_9

    #@312
    .line 422
    .end local v7           #_result:Z
    :sswitch_312
    const-string v9, "android.media.IAudioService"

    #@314
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@317
    .line 424
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@31a
    move-result v9

    #@31b
    if-eqz v9, :cond_326

    #@31d
    move v1, v8

    #@31e
    .line 425
    .restart local v1       #_arg0:Z
    :goto_31e
    invoke-virtual {p0, v1}, Landroid/media/IAudioService$Stub;->setBluetoothA2dpOn(Z)V

    #@321
    .line 426
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@324
    goto/16 :goto_9

    #@326
    .end local v1           #_arg0:Z
    :cond_326
    move v1, v0

    #@327
    .line 424
    goto :goto_31e

    #@328
    .line 431
    :sswitch_328
    const-string v9, "android.media.IAudioService"

    #@32a
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@32d
    .line 432
    invoke-virtual {p0}, Landroid/media/IAudioService$Stub;->isBluetoothA2dpOn()Z

    #@330
    move-result v7

    #@331
    .line 433
    .restart local v7       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@334
    .line 434
    if-eqz v7, :cond_337

    #@336
    move v0, v8

    #@337
    :cond_337
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@33a
    goto/16 :goto_9

    #@33c
    .line 439
    .end local v7           #_result:Z
    :sswitch_33c
    const-string v9, "android.media.IAudioService"

    #@33e
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@341
    .line 441
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@344
    move-result v9

    #@345
    if-eqz v9, :cond_351

    #@347
    move v1, v8

    #@348
    .line 443
    .restart local v1       #_arg0:Z
    :goto_348
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@34b
    move-result v2

    #@34c
    .line 444
    .local v2, _arg1:I
    invoke-virtual {p0, v1, v2}, Landroid/media/IAudioService$Stub;->setRemoteSubmixOn(ZI)V

    #@34f
    goto/16 :goto_9

    #@351
    .end local v1           #_arg0:Z
    .end local v2           #_arg1:I
    :cond_351
    move v1, v0

    #@352
    .line 441
    goto :goto_348

    #@353
    .line 449
    :sswitch_353
    const-string v0, "android.media.IAudioService"

    #@355
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@358
    .line 451
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@35b
    move-result v1

    #@35c
    .line 453
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@35f
    move-result v2

    #@360
    .line 455
    .restart local v2       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@363
    move-result-object v3

    #@364
    .line 457
    .restart local v3       #_arg2:Landroid/os/IBinder;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@367
    move-result-object v0

    #@368
    invoke-static {v0}, Landroid/media/IAudioFocusDispatcher$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IAudioFocusDispatcher;

    #@36b
    move-result-object v4

    #@36c
    .line 459
    .local v4, _arg3:Landroid/media/IAudioFocusDispatcher;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@36f
    move-result-object v5

    #@370
    .line 461
    .local v5, _arg4:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@373
    move-result-object v6

    #@374
    .local v6, _arg5:Ljava/lang/String;
    move-object v0, p0

    #@375
    .line 462
    invoke-virtual/range {v0 .. v6}, Landroid/media/IAudioService$Stub;->requestAudioFocus(IILandroid/os/IBinder;Landroid/media/IAudioFocusDispatcher;Ljava/lang/String;Ljava/lang/String;)I

    #@378
    move-result v7

    #@379
    .line 463
    .local v7, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@37c
    .line 464
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@37f
    goto/16 :goto_9

    #@381
    .line 469
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    .end local v3           #_arg2:Landroid/os/IBinder;
    .end local v4           #_arg3:Landroid/media/IAudioFocusDispatcher;
    .end local v5           #_arg4:Ljava/lang/String;
    .end local v6           #_arg5:Ljava/lang/String;
    .end local v7           #_result:I
    :sswitch_381
    const-string v0, "android.media.IAudioService"

    #@383
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@386
    .line 471
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@389
    move-result-object v0

    #@38a
    invoke-static {v0}, Landroid/media/IAudioFocusDispatcher$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IAudioFocusDispatcher;

    #@38d
    move-result-object v1

    #@38e
    .line 473
    .local v1, _arg0:Landroid/media/IAudioFocusDispatcher;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@391
    move-result-object v2

    #@392
    .line 474
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Landroid/media/IAudioService$Stub;->abandonAudioFocus(Landroid/media/IAudioFocusDispatcher;Ljava/lang/String;)I

    #@395
    move-result v7

    #@396
    .line 475
    .restart local v7       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@399
    .line 476
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@39c
    goto/16 :goto_9

    #@39e
    .line 481
    .end local v1           #_arg0:Landroid/media/IAudioFocusDispatcher;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v7           #_result:I
    :sswitch_39e
    const-string v0, "android.media.IAudioService"

    #@3a0
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3a3
    .line 483
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3a6
    move-result-object v1

    #@3a7
    .line 484
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/media/IAudioService$Stub;->unregisterAudioFocusClient(Ljava/lang/String;)V

    #@3aa
    .line 485
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3ad
    goto/16 :goto_9

    #@3af
    .line 490
    .end local v1           #_arg0:Ljava/lang/String;
    :sswitch_3af
    const-string v0, "android.media.IAudioService"

    #@3b1
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3b4
    .line 492
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3b7
    move-result v0

    #@3b8
    if-eqz v0, :cond_3c7

    #@3ba
    .line 493
    sget-object v0, Landroid/view/KeyEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@3bc
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@3bf
    move-result-object v1

    #@3c0
    check-cast v1, Landroid/view/KeyEvent;

    #@3c2
    .line 498
    .local v1, _arg0:Landroid/view/KeyEvent;
    :goto_3c2
    invoke-virtual {p0, v1}, Landroid/media/IAudioService$Stub;->dispatchMediaKeyEvent(Landroid/view/KeyEvent;)V

    #@3c5
    goto/16 :goto_9

    #@3c7
    .line 496
    .end local v1           #_arg0:Landroid/view/KeyEvent;
    :cond_3c7
    const/4 v1, 0x0

    #@3c8
    .restart local v1       #_arg0:Landroid/view/KeyEvent;
    goto :goto_3c2

    #@3c9
    .line 503
    .end local v1           #_arg0:Landroid/view/KeyEvent;
    :sswitch_3c9
    const-string v0, "android.media.IAudioService"

    #@3cb
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3ce
    .line 505
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3d1
    move-result v0

    #@3d2
    if-eqz v0, :cond_3e4

    #@3d4
    .line 506
    sget-object v0, Landroid/view/KeyEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@3d6
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@3d9
    move-result-object v1

    #@3da
    check-cast v1, Landroid/view/KeyEvent;

    #@3dc
    .line 511
    .restart local v1       #_arg0:Landroid/view/KeyEvent;
    :goto_3dc
    invoke-virtual {p0, v1}, Landroid/media/IAudioService$Stub;->dispatchMediaKeyEventUnderWakelock(Landroid/view/KeyEvent;)V

    #@3df
    .line 512
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3e2
    goto/16 :goto_9

    #@3e4
    .line 509
    .end local v1           #_arg0:Landroid/view/KeyEvent;
    :cond_3e4
    const/4 v1, 0x0

    #@3e5
    .restart local v1       #_arg0:Landroid/view/KeyEvent;
    goto :goto_3dc

    #@3e6
    .line 517
    .end local v1           #_arg0:Landroid/view/KeyEvent;
    :sswitch_3e6
    const-string v0, "android.media.IAudioService"

    #@3e8
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3eb
    .line 519
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3ee
    move-result v0

    #@3ef
    if-eqz v0, :cond_40c

    #@3f1
    .line 520
    sget-object v0, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@3f3
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@3f6
    move-result-object v1

    #@3f7
    check-cast v1, Landroid/app/PendingIntent;

    #@3f9
    .line 526
    .local v1, _arg0:Landroid/app/PendingIntent;
    :goto_3f9
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3fc
    move-result v0

    #@3fd
    if-eqz v0, :cond_40e

    #@3ff
    .line 527
    sget-object v0, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@401
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@404
    move-result-object v2

    #@405
    check-cast v2, Landroid/content/ComponentName;

    #@407
    .line 532
    .local v2, _arg1:Landroid/content/ComponentName;
    :goto_407
    invoke-virtual {p0, v1, v2}, Landroid/media/IAudioService$Stub;->registerMediaButtonIntent(Landroid/app/PendingIntent;Landroid/content/ComponentName;)V

    #@40a
    goto/16 :goto_9

    #@40c
    .line 523
    .end local v1           #_arg0:Landroid/app/PendingIntent;
    .end local v2           #_arg1:Landroid/content/ComponentName;
    :cond_40c
    const/4 v1, 0x0

    #@40d
    .restart local v1       #_arg0:Landroid/app/PendingIntent;
    goto :goto_3f9

    #@40e
    .line 530
    :cond_40e
    const/4 v2, 0x0

    #@40f
    .restart local v2       #_arg1:Landroid/content/ComponentName;
    goto :goto_407

    #@410
    .line 537
    .end local v1           #_arg0:Landroid/app/PendingIntent;
    .end local v2           #_arg1:Landroid/content/ComponentName;
    :sswitch_410
    const-string v0, "android.media.IAudioService"

    #@412
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@415
    .line 539
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@418
    move-result v0

    #@419
    if-eqz v0, :cond_436

    #@41b
    .line 540
    sget-object v0, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@41d
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@420
    move-result-object v1

    #@421
    check-cast v1, Landroid/app/PendingIntent;

    #@423
    .line 546
    .restart local v1       #_arg0:Landroid/app/PendingIntent;
    :goto_423
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@426
    move-result v0

    #@427
    if-eqz v0, :cond_438

    #@429
    .line 547
    sget-object v0, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@42b
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@42e
    move-result-object v2

    #@42f
    check-cast v2, Landroid/content/ComponentName;

    #@431
    .line 552
    .restart local v2       #_arg1:Landroid/content/ComponentName;
    :goto_431
    invoke-virtual {p0, v1, v2}, Landroid/media/IAudioService$Stub;->unregisterMediaButtonIntent(Landroid/app/PendingIntent;Landroid/content/ComponentName;)V

    #@434
    goto/16 :goto_9

    #@436
    .line 543
    .end local v1           #_arg0:Landroid/app/PendingIntent;
    .end local v2           #_arg1:Landroid/content/ComponentName;
    :cond_436
    const/4 v1, 0x0

    #@437
    .restart local v1       #_arg0:Landroid/app/PendingIntent;
    goto :goto_423

    #@438
    .line 550
    :cond_438
    const/4 v2, 0x0

    #@439
    .restart local v2       #_arg1:Landroid/content/ComponentName;
    goto :goto_431

    #@43a
    .line 557
    .end local v1           #_arg0:Landroid/app/PendingIntent;
    .end local v2           #_arg1:Landroid/content/ComponentName;
    :sswitch_43a
    const-string v0, "android.media.IAudioService"

    #@43c
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@43f
    .line 559
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@442
    move-result v0

    #@443
    if-eqz v0, :cond_452

    #@445
    .line 560
    sget-object v0, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@447
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@44a
    move-result-object v1

    #@44b
    check-cast v1, Landroid/content/ComponentName;

    #@44d
    .line 565
    .local v1, _arg0:Landroid/content/ComponentName;
    :goto_44d
    invoke-virtual {p0, v1}, Landroid/media/IAudioService$Stub;->registerMediaButtonEventReceiverForCalls(Landroid/content/ComponentName;)V

    #@450
    goto/16 :goto_9

    #@452
    .line 563
    .end local v1           #_arg0:Landroid/content/ComponentName;
    :cond_452
    const/4 v1, 0x0

    #@453
    .restart local v1       #_arg0:Landroid/content/ComponentName;
    goto :goto_44d

    #@454
    .line 570
    .end local v1           #_arg0:Landroid/content/ComponentName;
    :sswitch_454
    const-string v0, "android.media.IAudioService"

    #@456
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@459
    .line 571
    invoke-virtual {p0}, Landroid/media/IAudioService$Stub;->unregisterMediaButtonEventReceiverForCalls()V

    #@45c
    goto/16 :goto_9

    #@45e
    .line 576
    :sswitch_45e
    const-string v0, "android.media.IAudioService"

    #@460
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@463
    .line 578
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@466
    move-result v0

    #@467
    if-eqz v0, :cond_489

    #@469
    .line 579
    sget-object v0, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@46b
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@46e
    move-result-object v1

    #@46f
    check-cast v1, Landroid/app/PendingIntent;

    #@471
    .line 585
    .local v1, _arg0:Landroid/app/PendingIntent;
    :goto_471
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@474
    move-result-object v0

    #@475
    invoke-static {v0}, Landroid/media/IRemoteControlClient$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IRemoteControlClient;

    #@478
    move-result-object v2

    #@479
    .line 587
    .local v2, _arg1:Landroid/media/IRemoteControlClient;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@47c
    move-result-object v3

    #@47d
    .line 588
    .local v3, _arg2:Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Landroid/media/IAudioService$Stub;->registerRemoteControlClient(Landroid/app/PendingIntent;Landroid/media/IRemoteControlClient;Ljava/lang/String;)I

    #@480
    move-result v7

    #@481
    .line 589
    .restart local v7       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@484
    .line 590
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@487
    goto/16 :goto_9

    #@489
    .line 582
    .end local v1           #_arg0:Landroid/app/PendingIntent;
    .end local v2           #_arg1:Landroid/media/IRemoteControlClient;
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v7           #_result:I
    :cond_489
    const/4 v1, 0x0

    #@48a
    .restart local v1       #_arg0:Landroid/app/PendingIntent;
    goto :goto_471

    #@48b
    .line 595
    .end local v1           #_arg0:Landroid/app/PendingIntent;
    :sswitch_48b
    const-string v0, "android.media.IAudioService"

    #@48d
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@490
    .line 597
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@493
    move-result v0

    #@494
    if-eqz v0, :cond_4ab

    #@496
    .line 598
    sget-object v0, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@498
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@49b
    move-result-object v1

    #@49c
    check-cast v1, Landroid/app/PendingIntent;

    #@49e
    .line 604
    .restart local v1       #_arg0:Landroid/app/PendingIntent;
    :goto_49e
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@4a1
    move-result-object v0

    #@4a2
    invoke-static {v0}, Landroid/media/IRemoteControlClient$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IRemoteControlClient;

    #@4a5
    move-result-object v2

    #@4a6
    .line 605
    .restart local v2       #_arg1:Landroid/media/IRemoteControlClient;
    invoke-virtual {p0, v1, v2}, Landroid/media/IAudioService$Stub;->unregisterRemoteControlClient(Landroid/app/PendingIntent;Landroid/media/IRemoteControlClient;)V

    #@4a9
    goto/16 :goto_9

    #@4ab
    .line 601
    .end local v1           #_arg0:Landroid/app/PendingIntent;
    .end local v2           #_arg1:Landroid/media/IRemoteControlClient;
    :cond_4ab
    const/4 v1, 0x0

    #@4ac
    .restart local v1       #_arg0:Landroid/app/PendingIntent;
    goto :goto_49e

    #@4ad
    .line 610
    .end local v1           #_arg0:Landroid/app/PendingIntent;
    :sswitch_4ad
    const-string v0, "android.media.IAudioService"

    #@4af
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4b2
    .line 612
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@4b5
    move-result-object v0

    #@4b6
    invoke-static {v0}, Landroid/media/IRemoteControlDisplay$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IRemoteControlDisplay;

    #@4b9
    move-result-object v1

    #@4ba
    .line 613
    .local v1, _arg0:Landroid/media/IRemoteControlDisplay;
    invoke-virtual {p0, v1}, Landroid/media/IAudioService$Stub;->registerRemoteControlDisplay(Landroid/media/IRemoteControlDisplay;)V

    #@4bd
    goto/16 :goto_9

    #@4bf
    .line 618
    .end local v1           #_arg0:Landroid/media/IRemoteControlDisplay;
    :sswitch_4bf
    const-string v0, "android.media.IAudioService"

    #@4c1
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4c4
    .line 620
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@4c7
    move-result-object v0

    #@4c8
    invoke-static {v0}, Landroid/media/IRemoteControlDisplay$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IRemoteControlDisplay;

    #@4cb
    move-result-object v1

    #@4cc
    .line 621
    .restart local v1       #_arg0:Landroid/media/IRemoteControlDisplay;
    invoke-virtual {p0, v1}, Landroid/media/IAudioService$Stub;->unregisterRemoteControlDisplay(Landroid/media/IRemoteControlDisplay;)V

    #@4cf
    goto/16 :goto_9

    #@4d1
    .line 626
    .end local v1           #_arg0:Landroid/media/IRemoteControlDisplay;
    :sswitch_4d1
    const-string v0, "android.media.IAudioService"

    #@4d3
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4d6
    .line 628
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@4d9
    move-result-object v0

    #@4da
    invoke-static {v0}, Landroid/media/IRemoteControlDisplay$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IRemoteControlDisplay;

    #@4dd
    move-result-object v1

    #@4de
    .line 630
    .restart local v1       #_arg0:Landroid/media/IRemoteControlDisplay;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4e1
    move-result v2

    #@4e2
    .line 632
    .local v2, _arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4e5
    move-result v3

    #@4e6
    .line 633
    .local v3, _arg2:I
    invoke-virtual {p0, v1, v2, v3}, Landroid/media/IAudioService$Stub;->remoteControlDisplayUsesBitmapSize(Landroid/media/IRemoteControlDisplay;II)V

    #@4e9
    goto/16 :goto_9

    #@4eb
    .line 638
    .end local v1           #_arg0:Landroid/media/IRemoteControlDisplay;
    .end local v2           #_arg1:I
    .end local v3           #_arg2:I
    :sswitch_4eb
    const-string v0, "android.media.IAudioService"

    #@4ed
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4f0
    .line 640
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4f3
    move-result v1

    #@4f4
    .line 642
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4f7
    move-result v2

    #@4f8
    .line 644
    .restart local v2       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4fb
    move-result v3

    #@4fc
    .line 645
    .restart local v3       #_arg2:I
    invoke-virtual {p0, v1, v2, v3}, Landroid/media/IAudioService$Stub;->setPlaybackInfoForRcc(III)V

    #@4ff
    goto/16 :goto_9

    #@501
    .line 650
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    .end local v3           #_arg2:I
    :sswitch_501
    const-string v0, "android.media.IAudioService"

    #@503
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@506
    .line 651
    invoke-virtual {p0}, Landroid/media/IAudioService$Stub;->getRemoteStreamMaxVolume()I

    #@509
    move-result v7

    #@50a
    .line 652
    .restart local v7       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@50d
    .line 653
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@510
    goto/16 :goto_9

    #@512
    .line 658
    .end local v7           #_result:I
    :sswitch_512
    const-string v0, "android.media.IAudioService"

    #@514
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@517
    .line 659
    invoke-virtual {p0}, Landroid/media/IAudioService$Stub;->getRemoteStreamVolume()I

    #@51a
    move-result v7

    #@51b
    .line 660
    .restart local v7       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@51e
    .line 661
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@521
    goto/16 :goto_9

    #@523
    .line 666
    .end local v7           #_result:I
    :sswitch_523
    const-string v0, "android.media.IAudioService"

    #@525
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@528
    .line 668
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@52b
    move-result v1

    #@52c
    .line 670
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@52f
    move-result-object v0

    #@530
    invoke-static {v0}, Landroid/media/IRemoteVolumeObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IRemoteVolumeObserver;

    #@533
    move-result-object v2

    #@534
    .line 671
    .local v2, _arg1:Landroid/media/IRemoteVolumeObserver;
    invoke-virtual {p0, v1, v2}, Landroid/media/IAudioService$Stub;->registerRemoteVolumeObserverForRcc(ILandroid/media/IRemoteVolumeObserver;)V

    #@537
    goto/16 :goto_9

    #@539
    .line 676
    .end local v1           #_arg0:I
    .end local v2           #_arg1:Landroid/media/IRemoteVolumeObserver;
    :sswitch_539
    const-string v0, "android.media.IAudioService"

    #@53b
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@53e
    .line 678
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@541
    move-result-object v1

    #@542
    .line 679
    .local v1, _arg0:Landroid/os/IBinder;
    invoke-virtual {p0, v1}, Landroid/media/IAudioService$Stub;->startBluetoothSco(Landroid/os/IBinder;)V

    #@545
    .line 680
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@548
    goto/16 :goto_9

    #@54a
    .line 685
    .end local v1           #_arg0:Landroid/os/IBinder;
    :sswitch_54a
    const-string v0, "android.media.IAudioService"

    #@54c
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@54f
    .line 687
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@552
    move-result-object v1

    #@553
    .line 688
    .restart local v1       #_arg0:Landroid/os/IBinder;
    invoke-virtual {p0, v1}, Landroid/media/IAudioService$Stub;->stopBluetoothSco(Landroid/os/IBinder;)V

    #@556
    .line 689
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@559
    goto/16 :goto_9

    #@55b
    .line 694
    .end local v1           #_arg0:Landroid/os/IBinder;
    :sswitch_55b
    const-string v9, "android.media.IAudioService"

    #@55d
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@560
    .line 695
    invoke-virtual {p0}, Landroid/media/IAudioService$Stub;->isBluetoothAGConnected()Z

    #@563
    move-result v7

    #@564
    .line 696
    .local v7, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@567
    .line 697
    if-eqz v7, :cond_56a

    #@569
    move v0, v8

    #@56a
    :cond_56a
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@56d
    goto/16 :goto_9

    #@56f
    .line 702
    .end local v7           #_result:Z
    :sswitch_56f
    const-string v9, "android.media.IAudioService"

    #@571
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@574
    .line 703
    invoke-virtual {p0}, Landroid/media/IAudioService$Stub;->isBluetoothAVConnected()Z

    #@577
    move-result v7

    #@578
    .line 704
    .restart local v7       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@57b
    .line 705
    if-eqz v7, :cond_57e

    #@57d
    move v0, v8

    #@57e
    :cond_57e
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@581
    goto/16 :goto_9

    #@583
    .line 710
    .end local v7           #_result:Z
    :sswitch_583
    const-string v9, "android.media.IAudioService"

    #@585
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@588
    .line 711
    invoke-virtual {p0}, Landroid/media/IAudioService$Stub;->isWiredHeadsetConnected()Z

    #@58b
    move-result v7

    #@58c
    .line 712
    .restart local v7       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@58f
    .line 713
    if-eqz v7, :cond_592

    #@591
    move v0, v8

    #@592
    :cond_592
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@595
    goto/16 :goto_9

    #@597
    .line 718
    .end local v7           #_result:Z
    :sswitch_597
    const-string v0, "android.media.IAudioService"

    #@599
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@59c
    .line 720
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@59f
    move-result v1

    #@5a0
    .line 722
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@5a3
    move-result-object v2

    #@5a4
    .line 723
    .local v2, _arg1:Landroid/os/IBinder;
    invoke-virtual {p0, v1, v2}, Landroid/media/IAudioService$Stub;->forceVolumeControlStream(ILandroid/os/IBinder;)V

    #@5a7
    .line 724
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5aa
    goto/16 :goto_9

    #@5ac
    .line 729
    .end local v1           #_arg0:I
    .end local v2           #_arg1:Landroid/os/IBinder;
    :sswitch_5ac
    const-string v0, "android.media.IAudioService"

    #@5ae
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5b1
    .line 731
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@5b4
    move-result-object v0

    #@5b5
    invoke-static {v0}, Landroid/media/IRingtonePlayer$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IRingtonePlayer;

    #@5b8
    move-result-object v1

    #@5b9
    .line 732
    .local v1, _arg0:Landroid/media/IRingtonePlayer;
    invoke-virtual {p0, v1}, Landroid/media/IAudioService$Stub;->setRingtonePlayer(Landroid/media/IRingtonePlayer;)V

    #@5bc
    .line 733
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5bf
    goto/16 :goto_9

    #@5c1
    .line 738
    .end local v1           #_arg0:Landroid/media/IRingtonePlayer;
    :sswitch_5c1
    const-string v0, "android.media.IAudioService"

    #@5c3
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5c6
    .line 739
    invoke-virtual {p0}, Landroid/media/IAudioService$Stub;->getRingtonePlayer()Landroid/media/IRingtonePlayer;

    #@5c9
    move-result-object v7

    #@5ca
    .line 740
    .local v7, _result:Landroid/media/IRingtonePlayer;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5cd
    .line 741
    if-eqz v7, :cond_5d8

    #@5cf
    invoke-interface {v7}, Landroid/media/IRingtonePlayer;->asBinder()Landroid/os/IBinder;

    #@5d2
    move-result-object v0

    #@5d3
    :goto_5d3
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@5d6
    goto/16 :goto_9

    #@5d8
    :cond_5d8
    const/4 v0, 0x0

    #@5d9
    goto :goto_5d3

    #@5da
    .line 746
    .end local v7           #_result:Landroid/media/IRingtonePlayer;
    :sswitch_5da
    const-string v0, "android.media.IAudioService"

    #@5dc
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5df
    .line 747
    invoke-virtual {p0}, Landroid/media/IAudioService$Stub;->getMasterStreamType()I

    #@5e2
    move-result v7

    #@5e3
    .line 748
    .local v7, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5e6
    .line 749
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@5e9
    goto/16 :goto_9

    #@5eb
    .line 754
    .end local v7           #_result:I
    :sswitch_5eb
    const-string v0, "android.media.IAudioService"

    #@5ed
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5f0
    .line 756
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@5f3
    move-result v1

    #@5f4
    .line 758
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@5f7
    move-result v2

    #@5f8
    .line 760
    .local v2, _arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5fb
    move-result-object v3

    #@5fc
    .line 761
    .local v3, _arg2:Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Landroid/media/IAudioService$Stub;->setWiredDeviceConnectionState(IILjava/lang/String;)V

    #@5ff
    .line 762
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@602
    goto/16 :goto_9

    #@604
    .line 767
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    .end local v3           #_arg2:Ljava/lang/String;
    :sswitch_604
    const-string v0, "android.media.IAudioService"

    #@606
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@609
    .line 769
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@60c
    move-result v0

    #@60d
    if-eqz v0, :cond_627

    #@60f
    .line 770
    sget-object v0, Landroid/bluetooth/BluetoothDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@611
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@614
    move-result-object v1

    #@615
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@617
    .line 776
    .local v1, _arg0:Landroid/bluetooth/BluetoothDevice;
    :goto_617
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@61a
    move-result v2

    #@61b
    .line 777
    .restart local v2       #_arg1:I
    invoke-virtual {p0, v1, v2}, Landroid/media/IAudioService$Stub;->setBluetoothA2dpDeviceConnectionState(Landroid/bluetooth/BluetoothDevice;I)I

    #@61e
    move-result v7

    #@61f
    .line 778
    .restart local v7       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@622
    .line 779
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@625
    goto/16 :goto_9

    #@627
    .line 773
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    .end local v2           #_arg1:I
    .end local v7           #_result:I
    :cond_627
    const/4 v1, 0x0

    #@628
    .restart local v1       #_arg0:Landroid/bluetooth/BluetoothDevice;
    goto :goto_617

    #@629
    .line 784
    .end local v1           #_arg0:Landroid/bluetooth/BluetoothDevice;
    :sswitch_629
    const-string v9, "android.media.IAudioService"

    #@62b
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@62e
    .line 786
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@631
    move-result-object v9

    #@632
    invoke-static {v9}, Landroid/media/IAudioRoutesObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IAudioRoutesObserver;

    #@635
    move-result-object v1

    #@636
    .line 787
    .local v1, _arg0:Landroid/media/IAudioRoutesObserver;
    invoke-virtual {p0, v1}, Landroid/media/IAudioService$Stub;->startWatchingRoutes(Landroid/media/IAudioRoutesObserver;)Landroid/media/AudioRoutesInfo;

    #@639
    move-result-object v7

    #@63a
    .line 788
    .local v7, _result:Landroid/media/AudioRoutesInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@63d
    .line 789
    if-eqz v7, :cond_647

    #@63f
    .line 790
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@642
    .line 791
    invoke-virtual {v7, p3, v8}, Landroid/media/AudioRoutesInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@645
    goto/16 :goto_9

    #@647
    .line 794
    :cond_647
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@64a
    goto/16 :goto_9

    #@64c
    .line 800
    .end local v1           #_arg0:Landroid/media/IAudioRoutesObserver;
    .end local v7           #_result:Landroid/media/AudioRoutesInfo;
    :sswitch_64c
    const-string v9, "android.media.IAudioService"

    #@64e
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@651
    .line 801
    invoke-virtual {p0}, Landroid/media/IAudioService$Stub;->isCameraSoundForced()Z

    #@654
    move-result v7

    #@655
    .line 802
    .local v7, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@658
    .line 803
    if-eqz v7, :cond_65b

    #@65a
    move v0, v8

    #@65b
    :cond_65b
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@65e
    goto/16 :goto_9

    #@660
    .line 808
    .end local v7           #_result:Z
    :sswitch_660
    const-string v9, "android.media.IAudioService"

    #@662
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@665
    .line 810
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@668
    move-result v9

    #@669
    if-eqz v9, :cond_674

    #@66b
    move v1, v8

    #@66c
    .line 811
    .local v1, _arg0:Z
    :goto_66c
    invoke-virtual {p0, v1}, Landroid/media/IAudioService$Stub;->setSpeakerOnForMedia(Z)V

    #@66f
    .line 812
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@672
    goto/16 :goto_9

    #@674
    .end local v1           #_arg0:Z
    :cond_674
    move v1, v0

    #@675
    .line 810
    goto :goto_66c

    #@676
    .line 817
    :sswitch_676
    const-string v9, "android.media.IAudioService"

    #@678
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@67b
    .line 818
    invoke-virtual {p0}, Landroid/media/IAudioService$Stub;->isSpeakerOnForMedia()Z

    #@67e
    move-result v7

    #@67f
    .line 819
    .restart local v7       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@682
    .line 820
    if-eqz v7, :cond_685

    #@684
    move v0, v8

    #@685
    :cond_685
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@688
    goto/16 :goto_9

    #@68a
    .line 825
    .end local v7           #_result:Z
    :sswitch_68a
    const-string v0, "android.media.IAudioService"

    #@68c
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@68f
    .line 827
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@692
    move-result v0

    #@693
    if-eqz v0, :cond_6a2

    #@695
    .line 828
    sget-object v0, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@697
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@69a
    move-result-object v1

    #@69b
    check-cast v1, Landroid/content/ComponentName;

    #@69d
    .line 833
    .local v1, _arg0:Landroid/content/ComponentName;
    :goto_69d
    invoke-virtual {p0, v1}, Landroid/media/IAudioService$Stub;->addMediaButtonEventReceiverForCalls(Landroid/content/ComponentName;)V

    #@6a0
    goto/16 :goto_9

    #@6a2
    .line 831
    .end local v1           #_arg0:Landroid/content/ComponentName;
    :cond_6a2
    const/4 v1, 0x0

    #@6a3
    .restart local v1       #_arg0:Landroid/content/ComponentName;
    goto :goto_69d

    #@6a4
    .line 838
    .end local v1           #_arg0:Landroid/content/ComponentName;
    :sswitch_6a4
    const-string v0, "android.media.IAudioService"

    #@6a6
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6a9
    .line 840
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@6ac
    move-result v0

    #@6ad
    if-eqz v0, :cond_6bc

    #@6af
    .line 841
    sget-object v0, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@6b1
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@6b4
    move-result-object v1

    #@6b5
    check-cast v1, Landroid/content/ComponentName;

    #@6b7
    .line 846
    .restart local v1       #_arg0:Landroid/content/ComponentName;
    :goto_6b7
    invoke-virtual {p0, v1}, Landroid/media/IAudioService$Stub;->removeMediaButtonEventReceiverForCalls(Landroid/content/ComponentName;)V

    #@6ba
    goto/16 :goto_9

    #@6bc
    .line 844
    .end local v1           #_arg0:Landroid/content/ComponentName;
    :cond_6bc
    const/4 v1, 0x0

    #@6bd
    .restart local v1       #_arg0:Landroid/content/ComponentName;
    goto :goto_6b7

    #@6be
    .line 851
    .end local v1           #_arg0:Landroid/content/ComponentName;
    :sswitch_6be
    const-string v9, "android.media.IAudioService"

    #@6c0
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6c3
    .line 853
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@6c6
    move-result v1

    #@6c7
    .line 854
    .local v1, _arg0:I
    invoke-virtual {p0, v1}, Landroid/media/IAudioService$Stub;->checkPlayConditions(I)Z

    #@6ca
    move-result v7

    #@6cb
    .line 855
    .restart local v7       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@6ce
    .line 856
    if-eqz v7, :cond_6d1

    #@6d0
    move v0, v8

    #@6d1
    :cond_6d1
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6d4
    goto/16 :goto_9

    #@6d6
    .line 861
    .end local v1           #_arg0:I
    .end local v7           #_result:Z
    :sswitch_6d6
    const-string v0, "android.media.IAudioService"

    #@6d8
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6db
    .line 863
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@6de
    move-result v1

    #@6df
    .line 864
    .restart local v1       #_arg0:I
    invoke-virtual {p0, v1}, Landroid/media/IAudioService$Stub;->setVoiceActivationState(I)V

    #@6e2
    .line 865
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@6e5
    goto/16 :goto_9

    #@6e7
    .line 870
    .end local v1           #_arg0:I
    :sswitch_6e7
    const-string v0, "android.media.IAudioService"

    #@6e9
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6ec
    .line 871
    invoke-virtual {p0}, Landroid/media/IAudioService$Stub;->getVoiceActivationState()I

    #@6ef
    move-result v7

    #@6f0
    .line 872
    .local v7, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@6f3
    .line 873
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@6f6
    goto/16 :goto_9

    #@6f8
    .line 878
    .end local v7           #_result:I
    :sswitch_6f8
    const-string v0, "android.media.IAudioService"

    #@6fa
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6fd
    .line 880
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@700
    move-result v1

    #@701
    .line 881
    .restart local v1       #_arg0:I
    invoke-virtual {p0, v1}, Landroid/media/IAudioService$Stub;->sendBroadcastRecordState(I)V

    #@704
    .line 882
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@707
    goto/16 :goto_9

    #@709
    .line 887
    .end local v1           #_arg0:I
    :sswitch_709
    const-string v9, "android.media.IAudioService"

    #@70b
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@70e
    .line 889
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@711
    move-result v1

    #@712
    .line 891
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@715
    move-result v2

    #@716
    .line 892
    .restart local v2       #_arg1:I
    invoke-virtual {p0, v1, v2}, Landroid/media/IAudioService$Stub;->setRecordHookingState(II)Z

    #@719
    move-result v7

    #@71a
    .line 893
    .local v7, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@71d
    .line 894
    if-eqz v7, :cond_720

    #@71f
    move v0, v8

    #@720
    :cond_720
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@723
    goto/16 :goto_9

    #@725
    .line 899
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    .end local v7           #_result:Z
    :sswitch_725
    const-string v0, "android.media.IAudioService"

    #@727
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@72a
    .line 900
    invoke-virtual {p0}, Landroid/media/IAudioService$Stub;->getRecordHookingState()I

    #@72d
    move-result v7

    #@72e
    .line 901
    .local v7, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@731
    .line 902
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@734
    goto/16 :goto_9

    #@736
    .line 41
    :sswitch_data_736
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_24
        0x3 -> :sswitch_35
        0x4 -> :sswitch_4d
        0x5 -> :sswitch_65
        0x6 -> :sswitch_79
        0x7 -> :sswitch_92
        0x8 -> :sswitch_ab
        0x9 -> :sswitch_b9
        0xa -> :sswitch_ce
        0xb -> :sswitch_ec
        0xc -> :sswitch_10a
        0xd -> :sswitch_122
        0xe -> :sswitch_140
        0xf -> :sswitch_154
        0x10 -> :sswitch_169
        0x11 -> :sswitch_17a
        0x12 -> :sswitch_18f
        0x13 -> :sswitch_1a0
        0x14 -> :sswitch_1b5
        0x15 -> :sswitch_1c6
        0x16 -> :sswitch_1d7
        0x17 -> :sswitch_1e8
        0x18 -> :sswitch_1fd
        0x19 -> :sswitch_212
        0x1a -> :sswitch_22a
        0x1b -> :sswitch_23f
        0x1c -> :sswitch_254
        0x1d -> :sswitch_265
        0x1e -> :sswitch_276
        0x1f -> :sswitch_284
        0x20 -> :sswitch_296
        0x21 -> :sswitch_2aa
        0x22 -> :sswitch_2b4
        0x23 -> :sswitch_2be
        0x24 -> :sswitch_2d4
        0x25 -> :sswitch_2e8
        0x26 -> :sswitch_2fe
        0x27 -> :sswitch_312
        0x28 -> :sswitch_328
        0x29 -> :sswitch_33c
        0x2a -> :sswitch_353
        0x2b -> :sswitch_381
        0x2c -> :sswitch_39e
        0x2d -> :sswitch_3af
        0x2e -> :sswitch_3c9
        0x2f -> :sswitch_3e6
        0x30 -> :sswitch_410
        0x31 -> :sswitch_43a
        0x32 -> :sswitch_454
        0x33 -> :sswitch_45e
        0x34 -> :sswitch_48b
        0x35 -> :sswitch_4ad
        0x36 -> :sswitch_4bf
        0x37 -> :sswitch_4d1
        0x38 -> :sswitch_4eb
        0x39 -> :sswitch_501
        0x3a -> :sswitch_512
        0x3b -> :sswitch_523
        0x3c -> :sswitch_539
        0x3d -> :sswitch_54a
        0x3e -> :sswitch_55b
        0x3f -> :sswitch_56f
        0x40 -> :sswitch_583
        0x41 -> :sswitch_597
        0x42 -> :sswitch_5ac
        0x43 -> :sswitch_5c1
        0x44 -> :sswitch_5da
        0x45 -> :sswitch_5eb
        0x46 -> :sswitch_604
        0x47 -> :sswitch_629
        0x48 -> :sswitch_64c
        0x49 -> :sswitch_660
        0x4a -> :sswitch_676
        0x4b -> :sswitch_68a
        0x4c -> :sswitch_6a4
        0x4d -> :sswitch_6be
        0x4e -> :sswitch_6d6
        0x4f -> :sswitch_6e7
        0x50 -> :sswitch_6f8
        0x51 -> :sswitch_709
        0x52 -> :sswitch_725
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
