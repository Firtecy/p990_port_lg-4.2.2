.class Landroid/media/AudioService$SoundPoolListenerThread;
.super Ljava/lang/Thread;
.source "AudioService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/AudioService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SoundPoolListenerThread"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/media/AudioService;


# direct methods
.method public constructor <init>(Landroid/media/AudioService;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 2311
    iput-object p1, p0, Landroid/media/AudioService$SoundPoolListenerThread;->this$0:Landroid/media/AudioService;

    #@2
    .line 2312
    const-string v0, "SoundPoolListenerThread"

    #@4
    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    #@7
    .line 2313
    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    #@0
    .prologue
    .line 2318
    invoke-static {}, Landroid/os/Looper;->prepare()V

    #@3
    .line 2319
    iget-object v0, p0, Landroid/media/AudioService$SoundPoolListenerThread;->this$0:Landroid/media/AudioService;

    #@5
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@8
    move-result-object v1

    #@9
    invoke-static {v0, v1}, Landroid/media/AudioService;->access$1602(Landroid/media/AudioService;Landroid/os/Looper;)Landroid/os/Looper;

    #@c
    .line 2321
    iget-object v0, p0, Landroid/media/AudioService$SoundPoolListenerThread;->this$0:Landroid/media/AudioService;

    #@e
    invoke-static {v0}, Landroid/media/AudioService;->access$1700(Landroid/media/AudioService;)Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    monitor-enter v1

    #@13
    .line 2322
    :try_start_13
    iget-object v0, p0, Landroid/media/AudioService$SoundPoolListenerThread;->this$0:Landroid/media/AudioService;

    #@15
    invoke-static {v0}, Landroid/media/AudioService;->access$1800(Landroid/media/AudioService;)Landroid/media/SoundPool;

    #@18
    move-result-object v0

    #@19
    if-eqz v0, :cond_37

    #@1b
    .line 2323
    iget-object v0, p0, Landroid/media/AudioService$SoundPoolListenerThread;->this$0:Landroid/media/AudioService;

    #@1d
    new-instance v2, Landroid/media/AudioService$SoundPoolCallback;

    #@1f
    iget-object v3, p0, Landroid/media/AudioService$SoundPoolListenerThread;->this$0:Landroid/media/AudioService;

    #@21
    const/4 v4, 0x0

    #@22
    invoke-direct {v2, v3, v4}, Landroid/media/AudioService$SoundPoolCallback;-><init>(Landroid/media/AudioService;Landroid/media/AudioService$1;)V

    #@25
    invoke-static {v0, v2}, Landroid/media/AudioService;->access$1902(Landroid/media/AudioService;Landroid/media/AudioService$SoundPoolCallback;)Landroid/media/AudioService$SoundPoolCallback;

    #@28
    .line 2324
    iget-object v0, p0, Landroid/media/AudioService$SoundPoolListenerThread;->this$0:Landroid/media/AudioService;

    #@2a
    invoke-static {v0}, Landroid/media/AudioService;->access$1800(Landroid/media/AudioService;)Landroid/media/SoundPool;

    #@2d
    move-result-object v0

    #@2e
    iget-object v2, p0, Landroid/media/AudioService$SoundPoolListenerThread;->this$0:Landroid/media/AudioService;

    #@30
    invoke-static {v2}, Landroid/media/AudioService;->access$1900(Landroid/media/AudioService;)Landroid/media/AudioService$SoundPoolCallback;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v0, v2}, Landroid/media/SoundPool;->setOnLoadCompleteListener(Landroid/media/SoundPool$OnLoadCompleteListener;)V

    #@37
    .line 2326
    :cond_37
    iget-object v0, p0, Landroid/media/AudioService$SoundPoolListenerThread;->this$0:Landroid/media/AudioService;

    #@39
    invoke-static {v0}, Landroid/media/AudioService;->access$1700(Landroid/media/AudioService;)Ljava/lang/Object;

    #@3c
    move-result-object v0

    #@3d
    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    #@40
    .line 2327
    monitor-exit v1
    :try_end_41
    .catchall {:try_start_13 .. :try_end_41} :catchall_45

    #@41
    .line 2328
    invoke-static {}, Landroid/os/Looper;->loop()V

    #@44
    .line 2329
    return-void

    #@45
    .line 2327
    :catchall_45
    move-exception v0

    #@46
    :try_start_46
    monitor-exit v1
    :try_end_47
    .catchall {:try_start_46 .. :try_end_47} :catchall_45

    #@47
    throw v0
.end method
