.class public Landroid/media/RemoteControlClient;
.super Ljava/lang/Object;
.source "RemoteControlClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/RemoteControlClient$EventHandler;,
        Landroid/media/RemoteControlClient$MetadataEditor;
    }
.end annotation


# static fields
.field public static final DEFAULT_PLAYBACK_VOLUME:I = 0xf

.field public static final DEFAULT_PLAYBACK_VOLUME_HANDLING:I = 0x1

.field public static final FLAGS_KEY_MEDIA_NONE:I = 0x0

.field public static final FLAG_INFORMATION_REQUEST_ALBUM_ART:I = 0x8

.field public static final FLAG_INFORMATION_REQUEST_KEY_MEDIA:I = 0x2

.field public static final FLAG_INFORMATION_REQUEST_METADATA:I = 0x1

.field public static final FLAG_INFORMATION_REQUEST_PLAYSTATE:I = 0x4

.field public static final FLAG_KEY_MEDIA_FAST_FORWARD:I = 0x40

.field public static final FLAG_KEY_MEDIA_NEXT:I = 0x80

.field public static final FLAG_KEY_MEDIA_PAUSE:I = 0x10

.field public static final FLAG_KEY_MEDIA_PLAY:I = 0x4

.field public static final FLAG_KEY_MEDIA_PLAY_PAUSE:I = 0x8

.field public static final FLAG_KEY_MEDIA_PREVIOUS:I = 0x1

.field public static final FLAG_KEY_MEDIA_REWIND:I = 0x2

.field public static final FLAG_KEY_MEDIA_STOP:I = 0x20

.field private static final METADATA_KEYS_TYPE_LONG:[I = null

.field private static final METADATA_KEYS_TYPE_STRING:[I = null

.field private static final MSG_NEW_CURRENT_CLIENT_GEN:I = 0x6

.field private static final MSG_NEW_INTERNAL_CLIENT_GEN:I = 0x5

.field private static final MSG_PLUG_DISPLAY:I = 0x7

.field private static final MSG_REQUEST_ARTWORK:I = 0x4

.field private static final MSG_REQUEST_METADATA:I = 0x2

.field private static final MSG_REQUEST_PLAYBACK_STATE:I = 0x1

.field private static final MSG_REQUEST_TRANSPORTCONTROL:I = 0x3

.field private static final MSG_UNPLUG_DISPLAY:I = 0x8

.field public static final PLAYBACKINFO_INVALID_VALUE:I = -0x80000000

.field public static final PLAYBACKINFO_PLAYBACK_TYPE:I = 0x1

.field public static final PLAYBACKINFO_PLAYSTATE:I = 0xff

.field public static final PLAYBACKINFO_USES_STREAM:I = 0x5

.field public static final PLAYBACKINFO_VOLUME:I = 0x2

.field public static final PLAYBACKINFO_VOLUME_HANDLING:I = 0x4

.field public static final PLAYBACKINFO_VOLUME_MAX:I = 0x3

.field public static final PLAYBACK_TYPE_LOCAL:I = 0x0

.field private static final PLAYBACK_TYPE_MAX:I = 0x1

.field private static final PLAYBACK_TYPE_MIN:I = 0x0

.field public static final PLAYBACK_TYPE_REMOTE:I = 0x1

.field public static final PLAYBACK_VOLUME_FIXED:I = 0x0

.field public static final PLAYBACK_VOLUME_VARIABLE:I = 0x1

.field public static final PLAYSTATE_BUFFERING:I = 0x8

.field public static final PLAYSTATE_ERROR:I = 0x9

.field public static final PLAYSTATE_FAST_FORWARDING:I = 0x4

.field public static final PLAYSTATE_NONE:I = 0x0

.field public static final PLAYSTATE_PAUSED:I = 0x2

.field public static final PLAYSTATE_PLAYING:I = 0x3

.field public static final PLAYSTATE_REWINDING:I = 0x5

.field public static final PLAYSTATE_SKIPPING_BACKWARDS:I = 0x7

.field public static final PLAYSTATE_SKIPPING_FORWARDS:I = 0x6

.field public static final PLAYSTATE_STOPPED:I = 0x1

.field public static final RCSE_ID_UNREGISTERED:I = -0x1

.field private static final TAG:Ljava/lang/String; = "RemoteControlClient"

.field private static sService:Landroid/media/IAudioService;


# instance fields
.field private final ARTWORK_DEFAULT_SIZE:I

.field private final ARTWORK_INVALID_SIZE:I

.field private mArtwork:Landroid/graphics/Bitmap;

.field private mArtworkExpectedHeight:I

.field private mArtworkExpectedWidth:I

.field private final mCacheLock:Ljava/lang/Object;

.field private mCurrentClientGenId:I

.field private mEventHandler:Landroid/media/RemoteControlClient$EventHandler;

.field private final mIRCC:Landroid/media/IRemoteControlClient;

.field private mInternalClientGenId:I

.field private mMetadata:Landroid/os/Bundle;

.field private mPlaybackState:I

.field private mPlaybackStateChangeTimeMs:J

.field private mPlaybackStream:I

.field private mPlaybackType:I

.field private mPlaybackVolume:I

.field private mPlaybackVolumeHandling:I

.field private mPlaybackVolumeMax:I

.field private mRcDisplay:Landroid/media/IRemoteControlDisplay;

.field private final mRcMediaIntent:Landroid/app/PendingIntent;

.field private mRcseId:I

.field private mTransportControlFlags:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 355
    const/16 v0, 0xb

    #@2
    new-array v0, v0, [I

    #@4
    fill-array-data v0, :array_12

    #@7
    sput-object v0, Landroid/media/RemoteControlClient;->METADATA_KEYS_TYPE_STRING:[I

    #@9
    .line 367
    const/4 v0, 0x3

    #@a
    new-array v0, v0, [I

    #@c
    fill-array-data v0, :array_2c

    #@f
    sput-object v0, Landroid/media/RemoteControlClient;->METADATA_KEYS_TYPE_LONG:[I

    #@11
    return-void

    #@12
    .line 355
    :array_12
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0xdt 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0xft 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
        0xbt 0x0t 0x0t 0x0t
    .end array-data

    #@2c
    .line 367
    :array_2c
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0xet 0x0t 0x0t 0x0t
        0x9t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>(Landroid/app/PendingIntent;)V
    .registers 8
    .parameter "mediaButtonIntent"

    #@0
    .prologue
    const/16 v1, 0xf

    #@2
    const/16 v5, 0x100

    #@4
    const/4 v4, 0x0

    #@5
    const/4 v3, -0x1

    #@6
    .line 320
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@9
    .line 638
    iput v4, p0, Landroid/media/RemoteControlClient;->mPlaybackType:I

    #@b
    .line 639
    iput v1, p0, Landroid/media/RemoteControlClient;->mPlaybackVolumeMax:I

    #@d
    .line 640
    iput v1, p0, Landroid/media/RemoteControlClient;->mPlaybackVolume:I

    #@f
    .line 641
    const/4 v1, 0x1

    #@10
    iput v1, p0, Landroid/media/RemoteControlClient;->mPlaybackVolumeHandling:I

    #@12
    .line 642
    const/4 v1, 0x3

    #@13
    iput v1, p0, Landroid/media/RemoteControlClient;->mPlaybackStream:I

    #@15
    .line 751
    new-instance v1, Ljava/lang/Object;

    #@17
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@1a
    iput-object v1, p0, Landroid/media/RemoteControlClient;->mCacheLock:Ljava/lang/Object;

    #@1c
    .line 756
    iput v4, p0, Landroid/media/RemoteControlClient;->mPlaybackState:I

    #@1e
    .line 761
    const-wide/16 v1, 0x0

    #@20
    iput-wide v1, p0, Landroid/media/RemoteControlClient;->mPlaybackStateChangeTimeMs:J

    #@22
    .line 770
    iput v5, p0, Landroid/media/RemoteControlClient;->ARTWORK_DEFAULT_SIZE:I

    #@24
    .line 771
    iput v3, p0, Landroid/media/RemoteControlClient;->ARTWORK_INVALID_SIZE:I

    #@26
    .line 772
    iput v5, p0, Landroid/media/RemoteControlClient;->mArtworkExpectedWidth:I

    #@28
    .line 773
    iput v5, p0, Landroid/media/RemoteControlClient;->mArtworkExpectedHeight:I

    #@2a
    .line 778
    iput v4, p0, Landroid/media/RemoteControlClient;->mTransportControlFlags:I

    #@2c
    .line 784
    new-instance v1, Landroid/os/Bundle;

    #@2e
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    #@31
    iput-object v1, p0, Landroid/media/RemoteControlClient;->mMetadata:Landroid/os/Bundle;

    #@33
    .line 789
    iput v3, p0, Landroid/media/RemoteControlClient;->mCurrentClientGenId:I

    #@35
    .line 796
    const/4 v1, -0x2

    #@36
    iput v1, p0, Landroid/media/RemoteControlClient;->mInternalClientGenId:I

    #@38
    .line 828
    new-instance v1, Landroid/media/RemoteControlClient$1;

    #@3a
    invoke-direct {v1, p0}, Landroid/media/RemoteControlClient$1;-><init>(Landroid/media/RemoteControlClient;)V

    #@3d
    iput-object v1, p0, Landroid/media/RemoteControlClient;->mIRCC:Landroid/media/IRemoteControlClient;

    #@3f
    .line 890
    iput v3, p0, Landroid/media/RemoteControlClient;->mRcseId:I

    #@41
    .line 321
    iput-object p1, p0, Landroid/media/RemoteControlClient;->mRcMediaIntent:Landroid/app/PendingIntent;

    #@43
    .line 324
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@46
    move-result-object v0

    #@47
    .local v0, looper:Landroid/os/Looper;
    if-eqz v0, :cond_51

    #@49
    .line 325
    new-instance v1, Landroid/media/RemoteControlClient$EventHandler;

    #@4b
    invoke-direct {v1, p0, p0, v0}, Landroid/media/RemoteControlClient$EventHandler;-><init>(Landroid/media/RemoteControlClient;Landroid/media/RemoteControlClient;Landroid/os/Looper;)V

    #@4e
    iput-object v1, p0, Landroid/media/RemoteControlClient;->mEventHandler:Landroid/media/RemoteControlClient$EventHandler;

    #@50
    .line 332
    :goto_50
    return-void

    #@51
    .line 326
    :cond_51
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    #@54
    move-result-object v0

    #@55
    if-eqz v0, :cond_5f

    #@57
    .line 327
    new-instance v1, Landroid/media/RemoteControlClient$EventHandler;

    #@59
    invoke-direct {v1, p0, p0, v0}, Landroid/media/RemoteControlClient$EventHandler;-><init>(Landroid/media/RemoteControlClient;Landroid/media/RemoteControlClient;Landroid/os/Looper;)V

    #@5c
    iput-object v1, p0, Landroid/media/RemoteControlClient;->mEventHandler:Landroid/media/RemoteControlClient$EventHandler;

    #@5e
    goto :goto_50

    #@5f
    .line 329
    :cond_5f
    const/4 v1, 0x0

    #@60
    iput-object v1, p0, Landroid/media/RemoteControlClient;->mEventHandler:Landroid/media/RemoteControlClient$EventHandler;

    #@62
    .line 330
    const-string v1, "RemoteControlClient"

    #@64
    const-string v2, "RemoteControlClient() couldn\'t find main application thread"

    #@66
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@69
    goto :goto_50
.end method

.method public constructor <init>(Landroid/app/PendingIntent;Landroid/os/Looper;)V
    .registers 8
    .parameter "mediaButtonIntent"
    .parameter "looper"

    #@0
    .prologue
    const/16 v0, 0xf

    #@2
    const/16 v4, 0x100

    #@4
    const/4 v3, 0x0

    #@5
    const/4 v2, -0x1

    #@6
    .line 349
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@9
    .line 638
    iput v3, p0, Landroid/media/RemoteControlClient;->mPlaybackType:I

    #@b
    .line 639
    iput v0, p0, Landroid/media/RemoteControlClient;->mPlaybackVolumeMax:I

    #@d
    .line 640
    iput v0, p0, Landroid/media/RemoteControlClient;->mPlaybackVolume:I

    #@f
    .line 641
    const/4 v0, 0x1

    #@10
    iput v0, p0, Landroid/media/RemoteControlClient;->mPlaybackVolumeHandling:I

    #@12
    .line 642
    const/4 v0, 0x3

    #@13
    iput v0, p0, Landroid/media/RemoteControlClient;->mPlaybackStream:I

    #@15
    .line 751
    new-instance v0, Ljava/lang/Object;

    #@17
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@1a
    iput-object v0, p0, Landroid/media/RemoteControlClient;->mCacheLock:Ljava/lang/Object;

    #@1c
    .line 756
    iput v3, p0, Landroid/media/RemoteControlClient;->mPlaybackState:I

    #@1e
    .line 761
    const-wide/16 v0, 0x0

    #@20
    iput-wide v0, p0, Landroid/media/RemoteControlClient;->mPlaybackStateChangeTimeMs:J

    #@22
    .line 770
    iput v4, p0, Landroid/media/RemoteControlClient;->ARTWORK_DEFAULT_SIZE:I

    #@24
    .line 771
    iput v2, p0, Landroid/media/RemoteControlClient;->ARTWORK_INVALID_SIZE:I

    #@26
    .line 772
    iput v4, p0, Landroid/media/RemoteControlClient;->mArtworkExpectedWidth:I

    #@28
    .line 773
    iput v4, p0, Landroid/media/RemoteControlClient;->mArtworkExpectedHeight:I

    #@2a
    .line 778
    iput v3, p0, Landroid/media/RemoteControlClient;->mTransportControlFlags:I

    #@2c
    .line 784
    new-instance v0, Landroid/os/Bundle;

    #@2e
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@31
    iput-object v0, p0, Landroid/media/RemoteControlClient;->mMetadata:Landroid/os/Bundle;

    #@33
    .line 789
    iput v2, p0, Landroid/media/RemoteControlClient;->mCurrentClientGenId:I

    #@35
    .line 796
    const/4 v0, -0x2

    #@36
    iput v0, p0, Landroid/media/RemoteControlClient;->mInternalClientGenId:I

    #@38
    .line 828
    new-instance v0, Landroid/media/RemoteControlClient$1;

    #@3a
    invoke-direct {v0, p0}, Landroid/media/RemoteControlClient$1;-><init>(Landroid/media/RemoteControlClient;)V

    #@3d
    iput-object v0, p0, Landroid/media/RemoteControlClient;->mIRCC:Landroid/media/IRemoteControlClient;

    #@3f
    .line 890
    iput v2, p0, Landroid/media/RemoteControlClient;->mRcseId:I

    #@41
    .line 350
    iput-object p1, p0, Landroid/media/RemoteControlClient;->mRcMediaIntent:Landroid/app/PendingIntent;

    #@43
    .line 352
    new-instance v0, Landroid/media/RemoteControlClient$EventHandler;

    #@45
    invoke-direct {v0, p0, p0, p2}, Landroid/media/RemoteControlClient$EventHandler;-><init>(Landroid/media/RemoteControlClient;Landroid/media/RemoteControlClient;Landroid/os/Looper;)V

    #@48
    iput-object v0, p0, Landroid/media/RemoteControlClient;->mEventHandler:Landroid/media/RemoteControlClient$EventHandler;

    #@4a
    .line 353
    return-void
.end method

.method static synthetic access$000()[I
    .registers 1

    #@0
    .prologue
    .line 63
    sget-object v0, Landroid/media/RemoteControlClient;->METADATA_KEYS_TYPE_STRING:[I

    #@2
    return-object v0
.end method

.method static synthetic access$100(I[I)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    invoke-static {p0, p1}, Landroid/media/RemoteControlClient;->validTypeForKey(I[I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1000(Landroid/media/RemoteControlClient;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 63
    invoke-direct {p0}, Landroid/media/RemoteControlClient;->sendMetadata_syncCacheLock()V

    #@3
    return-void
.end method

.method static synthetic access$1100(Landroid/media/RemoteControlClient;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 63
    invoke-direct {p0}, Landroid/media/RemoteControlClient;->sendArtwork_syncCacheLock()V

    #@3
    return-void
.end method

.method static synthetic access$1300(Landroid/media/RemoteControlClient;)Landroid/media/RemoteControlClient$EventHandler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Landroid/media/RemoteControlClient;->mEventHandler:Landroid/media/RemoteControlClient$EventHandler;

    #@2
    return-object v0
.end method

.method static synthetic access$1400(Landroid/media/RemoteControlClient;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 63
    invoke-direct {p0}, Landroid/media/RemoteControlClient;->sendPlaybackState_syncCacheLock()V

    #@3
    return-void
.end method

.method static synthetic access$1500(Landroid/media/RemoteControlClient;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 63
    invoke-direct {p0}, Landroid/media/RemoteControlClient;->sendTransportControlFlags_syncCacheLock()V

    #@3
    return-void
.end method

.method static synthetic access$1600(Landroid/media/RemoteControlClient;Ljava/lang/Integer;II)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Landroid/media/RemoteControlClient;->onNewInternalClientGen(Ljava/lang/Integer;II)V

    #@3
    return-void
.end method

.method static synthetic access$1700(Landroid/media/RemoteControlClient;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1}, Landroid/media/RemoteControlClient;->onNewCurrentClientGen(I)V

    #@3
    return-void
.end method

.method static synthetic access$1800(Landroid/media/RemoteControlClient;Landroid/media/IRemoteControlDisplay;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1}, Landroid/media/RemoteControlClient;->onPlugDisplay(Landroid/media/IRemoteControlDisplay;)V

    #@3
    return-void
.end method

.method static synthetic access$1900(Landroid/media/RemoteControlClient;Landroid/media/IRemoteControlDisplay;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1}, Landroid/media/RemoteControlClient;->onUnplugDisplay(Landroid/media/IRemoteControlDisplay;)V

    #@3
    return-void
.end method

.method static synthetic access$200()[I
    .registers 1

    #@0
    .prologue
    .line 63
    sget-object v0, Landroid/media/RemoteControlClient;->METADATA_KEYS_TYPE_LONG:[I

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/media/RemoteControlClient;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget v0, p0, Landroid/media/RemoteControlClient;->mArtworkExpectedWidth:I

    #@2
    return v0
.end method

.method static synthetic access$400(Landroid/media/RemoteControlClient;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget v0, p0, Landroid/media/RemoteControlClient;->mArtworkExpectedHeight:I

    #@2
    return v0
.end method

.method static synthetic access$500(Landroid/media/RemoteControlClient;Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Landroid/media/RemoteControlClient;->scaleBitmapIfTooBig(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$600(Landroid/media/RemoteControlClient;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Landroid/media/RemoteControlClient;->mCacheLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$702(Landroid/media/RemoteControlClient;Landroid/os/Bundle;)Landroid/os/Bundle;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    iput-object p1, p0, Landroid/media/RemoteControlClient;->mMetadata:Landroid/os/Bundle;

    #@2
    return-object p1
.end method

.method static synthetic access$800(Landroid/media/RemoteControlClient;)Landroid/graphics/Bitmap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Landroid/media/RemoteControlClient;->mArtwork:Landroid/graphics/Bitmap;

    #@2
    return-object v0
.end method

.method static synthetic access$802(Landroid/media/RemoteControlClient;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    iput-object p1, p0, Landroid/media/RemoteControlClient;->mArtwork:Landroid/graphics/Bitmap;

    #@2
    return-object p1
.end method

.method static synthetic access$900(Landroid/media/RemoteControlClient;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 63
    invoke-direct {p0}, Landroid/media/RemoteControlClient;->sendMetadataWithArtwork_syncCacheLock()V

    #@3
    return-void
.end method

.method private detachFromDisplay_syncCacheLock()V
    .registers 3

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 969
    const/4 v0, 0x0

    #@2
    iput-object v0, p0, Landroid/media/RemoteControlClient;->mRcDisplay:Landroid/media/IRemoteControlDisplay;

    #@4
    .line 970
    iput v1, p0, Landroid/media/RemoteControlClient;->mArtworkExpectedWidth:I

    #@6
    .line 971
    iput v1, p0, Landroid/media/RemoteControlClient;->mArtworkExpectedHeight:I

    #@8
    .line 972
    return-void
.end method

.method private static getService()Landroid/media/IAudioService;
    .registers 2

    #@0
    .prologue
    .line 1046
    sget-object v1, Landroid/media/RemoteControlClient;->sService:Landroid/media/IAudioService;

    #@2
    if-eqz v1, :cond_7

    #@4
    .line 1047
    sget-object v1, Landroid/media/RemoteControlClient;->sService:Landroid/media/IAudioService;

    #@6
    .line 1051
    .local v0, b:Landroid/os/IBinder;
    :goto_6
    return-object v1

    #@7
    .line 1049
    .end local v0           #b:Landroid/os/IBinder;
    :cond_7
    const-string v1, "audio"

    #@9
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@c
    move-result-object v0

    #@d
    .line 1050
    .restart local v0       #b:Landroid/os/IBinder;
    invoke-static {v0}, Landroid/media/IAudioService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IAudioService;

    #@10
    move-result-object v1

    #@11
    sput-object v1, Landroid/media/RemoteControlClient;->sService:Landroid/media/IAudioService;

    #@13
    .line 1051
    sget-object v1, Landroid/media/RemoteControlClient;->sService:Landroid/media/IAudioService;

    #@15
    goto :goto_6
.end method

.method private onNewCurrentClientGen(I)V
    .registers 4
    .parameter "clientGeneration"

    #@0
    .prologue
    .line 1083
    iget-object v1, p0, Landroid/media/RemoteControlClient;->mCacheLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 1084
    :try_start_3
    iput p1, p0, Landroid/media/RemoteControlClient;->mCurrentClientGenId:I

    #@5
    .line 1085
    monitor-exit v1

    #@6
    .line 1086
    return-void

    #@7
    .line 1085
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method private onNewInternalClientGen(Ljava/lang/Integer;II)V
    .registers 6
    .parameter "clientGeneration"
    .parameter "artWidth"
    .parameter "artHeight"

    #@0
    .prologue
    .line 1071
    iget-object v1, p0, Landroid/media/RemoteControlClient;->mCacheLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 1074
    :try_start_3
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/media/RemoteControlClient;->mInternalClientGenId:I

    #@9
    .line 1075
    if-lez p2, :cond_f

    #@b
    .line 1076
    iput p2, p0, Landroid/media/RemoteControlClient;->mArtworkExpectedWidth:I

    #@d
    .line 1077
    iput p3, p0, Landroid/media/RemoteControlClient;->mArtworkExpectedHeight:I

    #@f
    .line 1079
    :cond_f
    monitor-exit v1

    #@10
    .line 1080
    return-void

    #@11
    .line 1079
    :catchall_11
    move-exception v0

    #@12
    monitor-exit v1
    :try_end_13
    .catchall {:try_start_3 .. :try_end_13} :catchall_11

    #@13
    throw v0
.end method

.method private onPlugDisplay(Landroid/media/IRemoteControlDisplay;)V
    .registers 4
    .parameter "rcd"

    #@0
    .prologue
    .line 1089
    iget-object v1, p0, Landroid/media/RemoteControlClient;->mCacheLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 1090
    :try_start_3
    iput-object p1, p0, Landroid/media/RemoteControlClient;->mRcDisplay:Landroid/media/IRemoteControlDisplay;

    #@5
    .line 1091
    monitor-exit v1

    #@6
    .line 1092
    return-void

    #@7
    .line 1091
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method private onUnplugDisplay(Landroid/media/IRemoteControlDisplay;)V
    .registers 5
    .parameter "rcd"

    #@0
    .prologue
    .line 1095
    iget-object v1, p0, Landroid/media/RemoteControlClient;->mCacheLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 1096
    :try_start_3
    iget-object v0, p0, Landroid/media/RemoteControlClient;->mRcDisplay:Landroid/media/IRemoteControlDisplay;

    #@5
    if-eqz v0, :cond_22

    #@7
    iget-object v0, p0, Landroid/media/RemoteControlClient;->mRcDisplay:Landroid/media/IRemoteControlDisplay;

    #@9
    invoke-interface {v0}, Landroid/media/IRemoteControlDisplay;->asBinder()Landroid/os/IBinder;

    #@c
    move-result-object v0

    #@d
    invoke-interface {p1}, Landroid/media/IRemoteControlDisplay;->asBinder()Landroid/os/IBinder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_22

    #@17
    .line 1097
    const/4 v0, 0x0

    #@18
    iput-object v0, p0, Landroid/media/RemoteControlClient;->mRcDisplay:Landroid/media/IRemoteControlDisplay;

    #@1a
    .line 1098
    const/16 v0, 0x100

    #@1c
    iput v0, p0, Landroid/media/RemoteControlClient;->mArtworkExpectedWidth:I

    #@1e
    .line 1099
    const/16 v0, 0x100

    #@20
    iput v0, p0, Landroid/media/RemoteControlClient;->mArtworkExpectedHeight:I

    #@22
    .line 1101
    :cond_22
    monitor-exit v1

    #@23
    .line 1102
    return-void

    #@24
    .line 1101
    :catchall_24
    move-exception v0

    #@25
    monitor-exit v1
    :try_end_26
    .catchall {:try_start_3 .. :try_end_26} :catchall_24

    #@26
    throw v0
.end method

.method private scaleBitmapIfTooBig(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .registers 20
    .parameter "bitmap"
    .parameter "maxWidth"
    .parameter "maxHeight"

    #@0
    .prologue
    .line 1118
    if-eqz p1, :cond_63

    #@2
    .line 1119
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    #@5
    move-result v9

    #@6
    .line 1120
    .local v9, width:I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    #@9
    move-result v2

    #@a
    .line 1121
    .local v2, height:I
    move/from16 v0, p2

    #@c
    if-gt v9, v0, :cond_12

    #@e
    move/from16 v0, p3

    #@10
    if-le v2, v0, :cond_63

    #@12
    .line 1122
    :cond_12
    move/from16 v0, p2

    #@14
    int-to-float v10, v0

    #@15
    int-to-float v11, v9

    #@16
    div-float/2addr v10, v11

    #@17
    move/from16 v0, p3

    #@19
    int-to-float v11, v0

    #@1a
    int-to-float v12, v2

    #@1b
    div-float/2addr v11, v12

    #@1c
    invoke-static {v10, v11}, Ljava/lang/Math;->min(FF)F

    #@1f
    move-result v8

    #@20
    .line 1123
    .local v8, scale:F
    int-to-float v10, v9

    #@21
    mul-float/2addr v10, v8

    #@22
    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    #@25
    move-result v5

    #@26
    .line 1124
    .local v5, newWidth:I
    int-to-float v10, v2

    #@27
    mul-float/2addr v10, v8

    #@28
    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    #@2b
    move-result v4

    #@2c
    .line 1125
    .local v4, newHeight:I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    #@2f
    move-result-object v3

    #@30
    .line 1126
    .local v3, newConfig:Landroid/graphics/Bitmap$Config;
    if-nez v3, :cond_34

    #@32
    .line 1127
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@34
    .line 1129
    :cond_34
    invoke-static {v5, v4, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    #@37
    move-result-object v6

    #@38
    .line 1130
    .local v6, outBitmap:Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    #@3a
    invoke-direct {v1, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    #@3d
    .line 1131
    .local v1, canvas:Landroid/graphics/Canvas;
    new-instance v7, Landroid/graphics/Paint;

    #@3f
    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    #@42
    .line 1132
    .local v7, paint:Landroid/graphics/Paint;
    const/4 v10, 0x1

    #@43
    invoke-virtual {v7, v10}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    #@46
    .line 1133
    const/4 v10, 0x1

    #@47
    invoke-virtual {v7, v10}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    #@4a
    .line 1134
    const/4 v10, 0x0

    #@4b
    new-instance v11, Landroid/graphics/RectF;

    #@4d
    const/4 v12, 0x0

    #@4e
    const/4 v13, 0x0

    #@4f
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    #@52
    move-result v14

    #@53
    int-to-float v14, v14

    #@54
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    #@57
    move-result v15

    #@58
    int-to-float v15, v15

    #@59
    invoke-direct {v11, v12, v13, v14, v15}, Landroid/graphics/RectF;-><init>(FFFF)V

    #@5c
    move-object/from16 v0, p1

    #@5e
    invoke-virtual {v1, v0, v10, v11, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    #@61
    .line 1136
    move-object/from16 p1, v6

    #@63
    .line 1139
    .end local v1           #canvas:Landroid/graphics/Canvas;
    .end local v2           #height:I
    .end local v3           #newConfig:Landroid/graphics/Bitmap$Config;
    .end local v4           #newHeight:I
    .end local v5           #newWidth:I
    .end local v6           #outBitmap:Landroid/graphics/Bitmap;
    .end local v7           #paint:Landroid/graphics/Paint;
    .end local v8           #scale:F
    .end local v9           #width:I
    :cond_63
    return-object p1
.end method

.method private sendArtwork_syncCacheLock()V
    .registers 5

    #@0
    .prologue
    .line 1010
    iget v1, p0, Landroid/media/RemoteControlClient;->mCurrentClientGenId:I

    #@2
    iget v2, p0, Landroid/media/RemoteControlClient;->mInternalClientGenId:I

    #@4
    if-ne v1, v2, :cond_1f

    #@6
    iget-object v1, p0, Landroid/media/RemoteControlClient;->mRcDisplay:Landroid/media/IRemoteControlDisplay;

    #@8
    if-eqz v1, :cond_1f

    #@a
    .line 1014
    iget-object v1, p0, Landroid/media/RemoteControlClient;->mArtwork:Landroid/graphics/Bitmap;

    #@c
    iget v2, p0, Landroid/media/RemoteControlClient;->mArtworkExpectedWidth:I

    #@e
    iget v3, p0, Landroid/media/RemoteControlClient;->mArtworkExpectedHeight:I

    #@10
    invoke-direct {p0, v1, v2, v3}, Landroid/media/RemoteControlClient;->scaleBitmapIfTooBig(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    #@13
    move-result-object v1

    #@14
    iput-object v1, p0, Landroid/media/RemoteControlClient;->mArtwork:Landroid/graphics/Bitmap;

    #@16
    .line 1016
    :try_start_16
    iget-object v1, p0, Landroid/media/RemoteControlClient;->mRcDisplay:Landroid/media/IRemoteControlDisplay;

    #@18
    iget v2, p0, Landroid/media/RemoteControlClient;->mInternalClientGenId:I

    #@1a
    iget-object v3, p0, Landroid/media/RemoteControlClient;->mArtwork:Landroid/graphics/Bitmap;

    #@1c
    invoke-interface {v1, v2, v3}, Landroid/media/IRemoteControlDisplay;->setArtwork(ILandroid/graphics/Bitmap;)V
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_1f} :catch_20

    #@1f
    .line 1022
    :cond_1f
    :goto_1f
    return-void

    #@20
    .line 1017
    :catch_20
    move-exception v0

    #@21
    .line 1018
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "RemoteControlClient"

    #@23
    new-instance v2, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v3, "Error in sendArtwork(), dead display "

    #@2a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v2

    #@32
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v2

    #@36
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 1019
    invoke-direct {p0}, Landroid/media/RemoteControlClient;->detachFromDisplay_syncCacheLock()V

    #@3c
    goto :goto_1f
.end method

.method private sendAudioServiceNewPlaybackInfo_syncCacheLock(II)V
    .registers 7
    .parameter "what"
    .parameter "value"

    #@0
    .prologue
    .line 1055
    iget v2, p0, Landroid/media/RemoteControlClient;->mRcseId:I

    #@2
    const/4 v3, -0x1

    #@3
    if-ne v2, v3, :cond_6

    #@5
    .line 1065
    :goto_5
    return-void

    #@6
    .line 1059
    :cond_6
    invoke-static {}, Landroid/media/RemoteControlClient;->getService()Landroid/media/IAudioService;

    #@9
    move-result-object v1

    #@a
    .line 1061
    .local v1, service:Landroid/media/IAudioService;
    :try_start_a
    iget v2, p0, Landroid/media/RemoteControlClient;->mRcseId:I

    #@c
    invoke-interface {v1, v2, p1, p2}, Landroid/media/IAudioService;->setPlaybackInfoForRcc(III)V
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_f} :catch_10

    #@f
    goto :goto_5

    #@10
    .line 1062
    :catch_10
    move-exception v0

    #@11
    .line 1063
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "RemoteControlClient"

    #@13
    const-string v3, "Dead object in sendAudioServiceNewPlaybackInfo_syncCacheLock"

    #@15
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@18
    goto :goto_5
.end method

.method private sendMetadataWithArtwork_syncCacheLock()V
    .registers 6

    #@0
    .prologue
    .line 1025
    iget v1, p0, Landroid/media/RemoteControlClient;->mCurrentClientGenId:I

    #@2
    iget v2, p0, Landroid/media/RemoteControlClient;->mInternalClientGenId:I

    #@4
    if-ne v1, v2, :cond_21

    #@6
    iget-object v1, p0, Landroid/media/RemoteControlClient;->mRcDisplay:Landroid/media/IRemoteControlDisplay;

    #@8
    if-eqz v1, :cond_21

    #@a
    .line 1029
    iget-object v1, p0, Landroid/media/RemoteControlClient;->mArtwork:Landroid/graphics/Bitmap;

    #@c
    iget v2, p0, Landroid/media/RemoteControlClient;->mArtworkExpectedWidth:I

    #@e
    iget v3, p0, Landroid/media/RemoteControlClient;->mArtworkExpectedHeight:I

    #@10
    invoke-direct {p0, v1, v2, v3}, Landroid/media/RemoteControlClient;->scaleBitmapIfTooBig(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    #@13
    move-result-object v1

    #@14
    iput-object v1, p0, Landroid/media/RemoteControlClient;->mArtwork:Landroid/graphics/Bitmap;

    #@16
    .line 1031
    :try_start_16
    iget-object v1, p0, Landroid/media/RemoteControlClient;->mRcDisplay:Landroid/media/IRemoteControlDisplay;

    #@18
    iget v2, p0, Landroid/media/RemoteControlClient;->mInternalClientGenId:I

    #@1a
    iget-object v3, p0, Landroid/media/RemoteControlClient;->mMetadata:Landroid/os/Bundle;

    #@1c
    iget-object v4, p0, Landroid/media/RemoteControlClient;->mArtwork:Landroid/graphics/Bitmap;

    #@1e
    invoke-interface {v1, v2, v3, v4}, Landroid/media/IRemoteControlDisplay;->setAllMetadata(ILandroid/os/Bundle;Landroid/graphics/Bitmap;)V
    :try_end_21
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_21} :catch_22

    #@21
    .line 1037
    :cond_21
    :goto_21
    return-void

    #@22
    .line 1032
    :catch_22
    move-exception v0

    #@23
    .line 1033
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "RemoteControlClient"

    #@25
    new-instance v2, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v3, "Error in setAllMetadata(), dead display "

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v2

    #@38
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 1034
    invoke-direct {p0}, Landroid/media/RemoteControlClient;->detachFromDisplay_syncCacheLock()V

    #@3e
    goto :goto_21
.end method

.method private sendMetadata_syncCacheLock()V
    .registers 5

    #@0
    .prologue
    .line 987
    iget v1, p0, Landroid/media/RemoteControlClient;->mCurrentClientGenId:I

    #@2
    iget v2, p0, Landroid/media/RemoteControlClient;->mInternalClientGenId:I

    #@4
    if-ne v1, v2, :cond_13

    #@6
    iget-object v1, p0, Landroid/media/RemoteControlClient;->mRcDisplay:Landroid/media/IRemoteControlDisplay;

    #@8
    if-eqz v1, :cond_13

    #@a
    .line 989
    :try_start_a
    iget-object v1, p0, Landroid/media/RemoteControlClient;->mRcDisplay:Landroid/media/IRemoteControlDisplay;

    #@c
    iget v2, p0, Landroid/media/RemoteControlClient;->mInternalClientGenId:I

    #@e
    iget-object v3, p0, Landroid/media/RemoteControlClient;->mMetadata:Landroid/os/Bundle;

    #@10
    invoke-interface {v1, v2, v3}, Landroid/media/IRemoteControlDisplay;->setMetadata(ILandroid/os/Bundle;)V
    :try_end_13
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_13} :catch_14

    #@13
    .line 995
    :cond_13
    :goto_13
    return-void

    #@14
    .line 990
    :catch_14
    move-exception v0

    #@15
    .line 991
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "RemoteControlClient"

    #@17
    new-instance v2, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v3, "Error in sendPlaybackState(), dead display "

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 992
    invoke-direct {p0}, Landroid/media/RemoteControlClient;->detachFromDisplay_syncCacheLock()V

    #@30
    goto :goto_13
.end method

.method private sendPlaybackState_syncCacheLock()V
    .registers 7

    #@0
    .prologue
    .line 975
    iget v1, p0, Landroid/media/RemoteControlClient;->mCurrentClientGenId:I

    #@2
    iget v2, p0, Landroid/media/RemoteControlClient;->mInternalClientGenId:I

    #@4
    if-ne v1, v2, :cond_15

    #@6
    iget-object v1, p0, Landroid/media/RemoteControlClient;->mRcDisplay:Landroid/media/IRemoteControlDisplay;

    #@8
    if-eqz v1, :cond_15

    #@a
    .line 977
    :try_start_a
    iget-object v1, p0, Landroid/media/RemoteControlClient;->mRcDisplay:Landroid/media/IRemoteControlDisplay;

    #@c
    iget v2, p0, Landroid/media/RemoteControlClient;->mInternalClientGenId:I

    #@e
    iget v3, p0, Landroid/media/RemoteControlClient;->mPlaybackState:I

    #@10
    iget-wide v4, p0, Landroid/media/RemoteControlClient;->mPlaybackStateChangeTimeMs:J

    #@12
    invoke-interface {v1, v2, v3, v4, v5}, Landroid/media/IRemoteControlDisplay;->setPlaybackState(IIJ)V
    :try_end_15
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_15} :catch_16

    #@15
    .line 984
    :cond_15
    :goto_15
    return-void

    #@16
    .line 979
    :catch_16
    move-exception v0

    #@17
    .line 980
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "RemoteControlClient"

    #@19
    new-instance v2, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v3, "Error in setPlaybackState(), dead display "

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 981
    invoke-direct {p0}, Landroid/media/RemoteControlClient;->detachFromDisplay_syncCacheLock()V

    #@32
    goto :goto_15
.end method

.method private sendTransportControlFlags_syncCacheLock()V
    .registers 5

    #@0
    .prologue
    .line 998
    iget v1, p0, Landroid/media/RemoteControlClient;->mCurrentClientGenId:I

    #@2
    iget v2, p0, Landroid/media/RemoteControlClient;->mInternalClientGenId:I

    #@4
    if-ne v1, v2, :cond_13

    #@6
    iget-object v1, p0, Landroid/media/RemoteControlClient;->mRcDisplay:Landroid/media/IRemoteControlDisplay;

    #@8
    if-eqz v1, :cond_13

    #@a
    .line 1000
    :try_start_a
    iget-object v1, p0, Landroid/media/RemoteControlClient;->mRcDisplay:Landroid/media/IRemoteControlDisplay;

    #@c
    iget v2, p0, Landroid/media/RemoteControlClient;->mInternalClientGenId:I

    #@e
    iget v3, p0, Landroid/media/RemoteControlClient;->mTransportControlFlags:I

    #@10
    invoke-interface {v1, v2, v3}, Landroid/media/IRemoteControlDisplay;->setTransportControlFlags(II)V
    :try_end_13
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_13} :catch_14

    #@13
    .line 1007
    :cond_13
    :goto_13
    return-void

    #@14
    .line 1002
    :catch_14
    move-exception v0

    #@15
    .line 1003
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "RemoteControlClient"

    #@17
    new-instance v2, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v3, "Error in sendTransportControlFlags(), dead display "

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 1004
    invoke-direct {p0}, Landroid/media/RemoteControlClient;->detachFromDisplay_syncCacheLock()V

    #@30
    goto :goto_13
.end method

.method private static validTypeForKey(I[I)Z
    .registers 5
    .parameter "key"
    .parameter "validKeys"

    #@0
    .prologue
    .line 1151
    const/4 v1, 0x0

    #@1
    .line 1152
    .local v1, i:I
    :goto_1
    :try_start_1
    aget v2, p1, v1
    :try_end_3
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_3} :catch_a

    #@3
    if-ne p0, v2, :cond_7

    #@5
    .line 1153
    const/4 v2, 0x1

    #@6
    .line 1157
    :goto_6
    return v2

    #@7
    .line 1151
    :cond_7
    add-int/lit8 v1, v1, 0x1

    #@9
    goto :goto_1

    #@a
    .line 1156
    :catch_a
    move-exception v0

    #@b
    .line 1157
    .local v0, e:Ljava/lang/ArrayIndexOutOfBoundsException;
    const/4 v2, 0x0

    #@c
    goto :goto_6
.end method


# virtual methods
.method public editMetadata(Z)Landroid/media/RemoteControlClient$MetadataEditor;
    .registers 7
    .parameter "startEmpty"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 566
    new-instance v0, Landroid/media/RemoteControlClient$MetadataEditor;

    #@5
    invoke-direct {v0, p0, v4}, Landroid/media/RemoteControlClient$MetadataEditor;-><init>(Landroid/media/RemoteControlClient;Landroid/media/RemoteControlClient$1;)V

    #@8
    .line 567
    .local v0, editor:Landroid/media/RemoteControlClient$MetadataEditor;
    if-eqz p1, :cond_18

    #@a
    .line 568
    new-instance v1, Landroid/os/Bundle;

    #@c
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    #@f
    iput-object v1, v0, Landroid/media/RemoteControlClient$MetadataEditor;->mEditorMetadata:Landroid/os/Bundle;

    #@11
    .line 569
    iput-object v4, v0, Landroid/media/RemoteControlClient$MetadataEditor;->mEditorArtwork:Landroid/graphics/Bitmap;

    #@13
    .line 570
    iput-boolean v2, v0, Landroid/media/RemoteControlClient$MetadataEditor;->mMetadataChanged:Z

    #@15
    .line 571
    iput-boolean v2, v0, Landroid/media/RemoteControlClient$MetadataEditor;->mArtworkChanged:Z

    #@17
    .line 578
    :goto_17
    return-object v0

    #@18
    .line 573
    :cond_18
    new-instance v1, Landroid/os/Bundle;

    #@1a
    iget-object v2, p0, Landroid/media/RemoteControlClient;->mMetadata:Landroid/os/Bundle;

    #@1c
    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@1f
    iput-object v1, v0, Landroid/media/RemoteControlClient$MetadataEditor;->mEditorMetadata:Landroid/os/Bundle;

    #@21
    .line 574
    iget-object v1, p0, Landroid/media/RemoteControlClient;->mArtwork:Landroid/graphics/Bitmap;

    #@23
    iput-object v1, v0, Landroid/media/RemoteControlClient$MetadataEditor;->mEditorArtwork:Landroid/graphics/Bitmap;

    #@25
    .line 575
    iput-boolean v3, v0, Landroid/media/RemoteControlClient$MetadataEditor;->mMetadataChanged:Z

    #@27
    .line 576
    iput-boolean v3, v0, Landroid/media/RemoteControlClient$MetadataEditor;->mArtworkChanged:Z

    #@29
    goto :goto_17
.end method

.method public getIRemoteControlClient()Landroid/media/IRemoteControlClient;
    .registers 2

    #@0
    .prologue
    .line 822
    iget-object v0, p0, Landroid/media/RemoteControlClient;->mIRCC:Landroid/media/IRemoteControlClient;

    #@2
    return-object v0
.end method

.method public getIntPlaybackInformation(I)I
    .registers 6
    .parameter "what"

    #@0
    .prologue
    .line 729
    iget-object v1, p0, Landroid/media/RemoteControlClient;->mCacheLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 730
    packed-switch p1, :pswitch_data_3a

    #@6
    .line 742
    :try_start_6
    const-string v0, "RemoteControlClient"

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "getIntPlaybackInformation() unknown key "

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 743
    const/high16 v0, -0x8000

    #@20
    monitor-exit v1

    #@21
    :goto_21
    return v0

    #@22
    .line 732
    :pswitch_22
    iget v0, p0, Landroid/media/RemoteControlClient;->mPlaybackType:I

    #@24
    monitor-exit v1

    #@25
    goto :goto_21

    #@26
    .line 745
    :catchall_26
    move-exception v0

    #@27
    monitor-exit v1
    :try_end_28
    .catchall {:try_start_6 .. :try_end_28} :catchall_26

    #@28
    throw v0

    #@29
    .line 734
    :pswitch_29
    :try_start_29
    iget v0, p0, Landroid/media/RemoteControlClient;->mPlaybackVolume:I

    #@2b
    monitor-exit v1

    #@2c
    goto :goto_21

    #@2d
    .line 736
    :pswitch_2d
    iget v0, p0, Landroid/media/RemoteControlClient;->mPlaybackVolumeMax:I

    #@2f
    monitor-exit v1

    #@30
    goto :goto_21

    #@31
    .line 738
    :pswitch_31
    iget v0, p0, Landroid/media/RemoteControlClient;->mPlaybackStream:I

    #@33
    monitor-exit v1

    #@34
    goto :goto_21

    #@35
    .line 740
    :pswitch_35
    iget v0, p0, Landroid/media/RemoteControlClient;->mPlaybackVolumeHandling:I

    #@37
    monitor-exit v1
    :try_end_38
    .catchall {:try_start_29 .. :try_end_38} :catchall_26

    #@38
    goto :goto_21

    #@39
    .line 730
    nop

    #@3a
    :pswitch_data_3a
    .packed-switch 0x1
        :pswitch_22
        :pswitch_29
        :pswitch_2d
        :pswitch_35
        :pswitch_31
    .end packed-switch
.end method

.method public getRcMediaIntent()Landroid/app/PendingIntent;
    .registers 2

    #@0
    .prologue
    .line 815
    iget-object v0, p0, Landroid/media/RemoteControlClient;->mRcMediaIntent:Landroid/app/PendingIntent;

    #@2
    return-object v0
.end method

.method public getRcseId()I
    .registers 2

    #@0
    .prologue
    .line 906
    iget v0, p0, Landroid/media/RemoteControlClient;->mRcseId:I

    #@2
    return v0
.end method

.method public setPlaybackInformation(II)V
    .registers 7
    .parameter "what"
    .parameter "value"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 657
    iget-object v1, p0, Landroid/media/RemoteControlClient;->mCacheLock:Ljava/lang/Object;

    #@3
    monitor-enter v1

    #@4
    .line 658
    packed-switch p1, :pswitch_data_96

    #@7
    .line 709
    :try_start_7
    const-string v0, "RemoteControlClient"

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string/jumbo v3, "setPlaybackInformation() ignoring unknown key "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 712
    :cond_20
    :goto_20
    monitor-exit v1

    #@21
    .line 713
    return-void

    #@22
    .line 660
    :pswitch_22
    if-ltz p2, :cond_33

    #@24
    if-gt p2, v0, :cond_33

    #@26
    .line 661
    iget v0, p0, Landroid/media/RemoteControlClient;->mPlaybackType:I

    #@28
    if-eq v0, p2, :cond_20

    #@2a
    .line 662
    iput p2, p0, Landroid/media/RemoteControlClient;->mPlaybackType:I

    #@2c
    .line 663
    invoke-direct {p0, p1, p2}, Landroid/media/RemoteControlClient;->sendAudioServiceNewPlaybackInfo_syncCacheLock(II)V

    #@2f
    goto :goto_20

    #@30
    .line 712
    :catchall_30
    move-exception v0

    #@31
    monitor-exit v1
    :try_end_32
    .catchall {:try_start_7 .. :try_end_32} :catchall_30

    #@32
    throw v0

    #@33
    .line 666
    :cond_33
    :try_start_33
    const-string v0, "RemoteControlClient"

    #@35
    const-string/jumbo v2, "using invalid value for PLAYBACKINFO_PLAYBACK_TYPE"

    #@38
    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    goto :goto_20

    #@3c
    .line 670
    :pswitch_3c
    const/4 v0, -0x1

    #@3d
    if-le p2, v0, :cond_4d

    #@3f
    iget v0, p0, Landroid/media/RemoteControlClient;->mPlaybackVolumeMax:I

    #@41
    if-gt p2, v0, :cond_4d

    #@43
    .line 671
    iget v0, p0, Landroid/media/RemoteControlClient;->mPlaybackVolume:I

    #@45
    if-eq v0, p2, :cond_20

    #@47
    .line 672
    iput p2, p0, Landroid/media/RemoteControlClient;->mPlaybackVolume:I

    #@49
    .line 673
    invoke-direct {p0, p1, p2}, Landroid/media/RemoteControlClient;->sendAudioServiceNewPlaybackInfo_syncCacheLock(II)V

    #@4c
    goto :goto_20

    #@4d
    .line 676
    :cond_4d
    const-string v0, "RemoteControlClient"

    #@4f
    const-string/jumbo v2, "using invalid value for PLAYBACKINFO_VOLUME"

    #@52
    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    goto :goto_20

    #@56
    .line 680
    :pswitch_56
    if-lez p2, :cond_62

    #@58
    .line 681
    iget v0, p0, Landroid/media/RemoteControlClient;->mPlaybackVolumeMax:I

    #@5a
    if-eq v0, p2, :cond_20

    #@5c
    .line 682
    iput p2, p0, Landroid/media/RemoteControlClient;->mPlaybackVolumeMax:I

    #@5e
    .line 683
    invoke-direct {p0, p1, p2}, Landroid/media/RemoteControlClient;->sendAudioServiceNewPlaybackInfo_syncCacheLock(II)V

    #@61
    goto :goto_20

    #@62
    .line 686
    :cond_62
    const-string v0, "RemoteControlClient"

    #@64
    const-string/jumbo v2, "using invalid value for PLAYBACKINFO_VOLUME_MAX"

    #@67
    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    goto :goto_20

    #@6b
    .line 690
    :pswitch_6b
    if-ltz p2, :cond_76

    #@6d
    invoke-static {}, Landroid/media/AudioSystem;->getNumStreamTypes()I

    #@70
    move-result v0

    #@71
    if-ge p2, v0, :cond_76

    #@73
    .line 691
    iput p2, p0, Landroid/media/RemoteControlClient;->mPlaybackStream:I

    #@75
    goto :goto_20

    #@76
    .line 693
    :cond_76
    const-string v0, "RemoteControlClient"

    #@78
    const-string/jumbo v2, "using invalid value for PLAYBACKINFO_USES_STREAM"

    #@7b
    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7e
    goto :goto_20

    #@7f
    .line 697
    :pswitch_7f
    if-ltz p2, :cond_8d

    #@81
    if-gt p2, v0, :cond_8d

    #@83
    .line 698
    iget v0, p0, Landroid/media/RemoteControlClient;->mPlaybackVolumeHandling:I

    #@85
    if-eq v0, p2, :cond_20

    #@87
    .line 699
    iput p2, p0, Landroid/media/RemoteControlClient;->mPlaybackVolumeHandling:I

    #@89
    .line 700
    invoke-direct {p0, p1, p2}, Landroid/media/RemoteControlClient;->sendAudioServiceNewPlaybackInfo_syncCacheLock(II)V

    #@8c
    goto :goto_20

    #@8d
    .line 703
    :cond_8d
    const-string v0, "RemoteControlClient"

    #@8f
    const-string/jumbo v2, "using invalid value for PLAYBACKINFO_VOLUME_HANDLING"

    #@92
    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_95
    .catchall {:try_start_33 .. :try_end_95} :catchall_30

    #@95
    goto :goto_20

    #@96
    .line 658
    :pswitch_data_96
    .packed-switch 0x1
        :pswitch_22
        :pswitch_3c
        :pswitch_56
        :pswitch_7f
        :pswitch_6b
    .end packed-switch
.end method

.method public setPlaybackState(I)V
    .registers 6
    .parameter "state"

    #@0
    .prologue
    .line 595
    iget-object v1, p0, Landroid/media/RemoteControlClient;->mCacheLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 596
    :try_start_3
    iget v0, p0, Landroid/media/RemoteControlClient;->mPlaybackState:I

    #@5
    if-eq v0, p1, :cond_17

    #@7
    .line 598
    iput p1, p0, Landroid/media/RemoteControlClient;->mPlaybackState:I

    #@9
    .line 600
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@c
    move-result-wide v2

    #@d
    iput-wide v2, p0, Landroid/media/RemoteControlClient;->mPlaybackStateChangeTimeMs:J

    #@f
    .line 603
    invoke-direct {p0}, Landroid/media/RemoteControlClient;->sendPlaybackState_syncCacheLock()V

    #@12
    .line 605
    const/16 v0, 0xff

    #@14
    invoke-direct {p0, v0, p1}, Landroid/media/RemoteControlClient;->sendAudioServiceNewPlaybackInfo_syncCacheLock(II)V

    #@17
    .line 607
    :cond_17
    monitor-exit v1

    #@18
    .line 608
    return-void

    #@19
    .line 607
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_3 .. :try_end_1b} :catchall_19

    #@1b
    throw v0
.end method

.method public setRcseId(I)V
    .registers 2
    .parameter "id"

    #@0
    .prologue
    .line 899
    iput p1, p0, Landroid/media/RemoteControlClient;->mRcseId:I

    #@2
    .line 900
    return-void
.end method

.method public setTransportControlFlags(I)V
    .registers 4
    .parameter "transportControlFlags"

    #@0
    .prologue
    .line 623
    iget-object v1, p0, Landroid/media/RemoteControlClient;->mCacheLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 625
    :try_start_3
    iput p1, p0, Landroid/media/RemoteControlClient;->mTransportControlFlags:I

    #@5
    .line 628
    invoke-direct {p0}, Landroid/media/RemoteControlClient;->sendTransportControlFlags_syncCacheLock()V

    #@8
    .line 629
    monitor-exit v1

    #@9
    .line 630
    return-void

    #@a
    .line 629
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method
