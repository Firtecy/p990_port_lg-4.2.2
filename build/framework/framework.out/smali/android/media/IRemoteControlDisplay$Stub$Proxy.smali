.class Landroid/media/IRemoteControlDisplay$Stub$Proxy;
.super Ljava/lang/Object;
.source "IRemoteControlDisplay.java"

# interfaces
.implements Landroid/media/IRemoteControlDisplay;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/IRemoteControlDisplay$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 149
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 150
    iput-object p1, p0, Landroid/media/IRemoteControlDisplay$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 151
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 154
    iget-object v0, p0, Landroid/media/IRemoteControlDisplay$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 158
    const-string v0, "android.media.IRemoteControlDisplay"

    #@2
    return-object v0
.end method

.method public setAllMetadata(ILandroid/os/Bundle;Landroid/graphics/Bitmap;)V
    .registers 9
    .parameter "generationId"
    .parameter "metadata"
    .parameter "artwork"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 258
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 260
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.media.IRemoteControlDisplay"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 261
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 262
    if-eqz p2, :cond_2c

    #@e
    .line 263
    const/4 v1, 0x1

    #@f
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 264
    const/4 v1, 0x0

    #@13
    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@16
    .line 269
    :goto_16
    if-eqz p3, :cond_36

    #@18
    .line 270
    const/4 v1, 0x1

    #@19
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 271
    const/4 v1, 0x0

    #@1d
    invoke-virtual {p3, v0, v1}, Landroid/graphics/Bitmap;->writeToParcel(Landroid/os/Parcel;I)V

    #@20
    .line 276
    :goto_20
    iget-object v1, p0, Landroid/media/IRemoteControlDisplay$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@22
    const/4 v2, 0x6

    #@23
    const/4 v3, 0x0

    #@24
    const/4 v4, 0x1

    #@25
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_28
    .catchall {:try_start_4 .. :try_end_28} :catchall_31

    #@28
    .line 279
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 281
    return-void

    #@2c
    .line 267
    :cond_2c
    const/4 v1, 0x0

    #@2d
    :try_start_2d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_30
    .catchall {:try_start_2d .. :try_end_30} :catchall_31

    #@30
    goto :goto_16

    #@31
    .line 279
    :catchall_31
    move-exception v1

    #@32
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    throw v1

    #@36
    .line 274
    :cond_36
    const/4 v1, 0x0

    #@37
    :try_start_37
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_3a
    .catchall {:try_start_37 .. :try_end_3a} :catchall_31

    #@3a
    goto :goto_20
.end method

.method public setArtwork(ILandroid/graphics/Bitmap;)V
    .registers 8
    .parameter "generationId"
    .parameter "artwork"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 236
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 238
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.media.IRemoteControlDisplay"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 239
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 240
    if-eqz p2, :cond_22

    #@e
    .line 241
    const/4 v1, 0x1

    #@f
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 242
    const/4 v1, 0x0

    #@13
    invoke-virtual {p2, v0, v1}, Landroid/graphics/Bitmap;->writeToParcel(Landroid/os/Parcel;I)V

    #@16
    .line 247
    :goto_16
    iget-object v1, p0, Landroid/media/IRemoteControlDisplay$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/4 v2, 0x5

    #@19
    const/4 v3, 0x0

    #@1a
    const/4 v4, 0x1

    #@1b
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1e
    .catchall {:try_start_4 .. :try_end_1e} :catchall_27

    #@1e
    .line 250
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 252
    return-void

    #@22
    .line 245
    :cond_22
    const/4 v1, 0x0

    #@23
    :try_start_23
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_26
    .catchall {:try_start_23 .. :try_end_26} :catchall_27

    #@26
    goto :goto_16

    #@27
    .line 250
    :catchall_27
    move-exception v1

    #@28
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    throw v1
.end method

.method public setCurrentClientId(ILandroid/app/PendingIntent;Z)V
    .registers 9
    .parameter "clientGeneration"
    .parameter "clientMediaIntent"
    .parameter "clearing"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 170
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 172
    .local v0, _data:Landroid/os/Parcel;
    :try_start_6
    const-string v3, "android.media.IRemoteControlDisplay"

    #@8
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@b
    .line 173
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@e
    .line 174
    if-eqz p2, :cond_29

    #@10
    .line 175
    const/4 v3, 0x1

    #@11
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 176
    const/4 v3, 0x0

    #@15
    invoke-virtual {p2, v0, v3}, Landroid/app/PendingIntent;->writeToParcel(Landroid/os/Parcel;I)V

    #@18
    .line 181
    :goto_18
    if-eqz p3, :cond_33

    #@1a
    :goto_1a
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 182
    iget-object v1, p0, Landroid/media/IRemoteControlDisplay$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1f
    const/4 v2, 0x1

    #@20
    const/4 v3, 0x0

    #@21
    const/4 v4, 0x1

    #@22
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_25
    .catchall {:try_start_6 .. :try_end_25} :catchall_2e

    #@25
    .line 185
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 187
    return-void

    #@29
    .line 179
    :cond_29
    const/4 v3, 0x0

    #@2a
    :try_start_2a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2d
    .catchall {:try_start_2a .. :try_end_2d} :catchall_2e

    #@2d
    goto :goto_18

    #@2e
    .line 185
    :catchall_2e
    move-exception v1

    #@2f
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@32
    throw v1

    #@33
    :cond_33
    move v1, v2

    #@34
    .line 181
    goto :goto_1a
.end method

.method public setMetadata(ILandroid/os/Bundle;)V
    .registers 8
    .parameter "generationId"
    .parameter "metadata"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 217
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 219
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.media.IRemoteControlDisplay"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 220
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 221
    if-eqz p2, :cond_22

    #@e
    .line 222
    const/4 v1, 0x1

    #@f
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 223
    const/4 v1, 0x0

    #@13
    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@16
    .line 228
    :goto_16
    iget-object v1, p0, Landroid/media/IRemoteControlDisplay$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/4 v2, 0x4

    #@19
    const/4 v3, 0x0

    #@1a
    const/4 v4, 0x1

    #@1b
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1e
    .catchall {:try_start_4 .. :try_end_1e} :catchall_27

    #@1e
    .line 231
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 233
    return-void

    #@22
    .line 226
    :cond_22
    const/4 v1, 0x0

    #@23
    :try_start_23
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_26
    .catchall {:try_start_23 .. :try_end_26} :catchall_27

    #@26
    goto :goto_16

    #@27
    .line 231
    :catchall_27
    move-exception v1

    #@28
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    throw v1
.end method

.method public setPlaybackState(IIJ)V
    .registers 10
    .parameter "generationId"
    .parameter "state"
    .parameter "stateChangeTimeMs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 190
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 192
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.media.IRemoteControlDisplay"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 193
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 194
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 195
    invoke-virtual {v0, p3, p4}, Landroid/os/Parcel;->writeLong(J)V

    #@12
    .line 196
    iget-object v1, p0, Landroid/media/IRemoteControlDisplay$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@14
    const/4 v2, 0x2

    #@15
    const/4 v3, 0x0

    #@16
    const/4 v4, 0x1

    #@17
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1a
    .catchall {:try_start_4 .. :try_end_1a} :catchall_1e

    #@1a
    .line 199
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 201
    return-void

    #@1e
    .line 199
    :catchall_1e
    move-exception v1

    #@1f
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    throw v1
.end method

.method public setTransportControlFlags(II)V
    .registers 8
    .parameter "generationId"
    .parameter "transportControlFlags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 204
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 206
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.media.IRemoteControlDisplay"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 207
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 208
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 209
    iget-object v1, p0, Landroid/media/IRemoteControlDisplay$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@11
    const/4 v2, 0x3

    #@12
    const/4 v3, 0x0

    #@13
    const/4 v4, 0x1

    #@14
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_17
    .catchall {:try_start_4 .. :try_end_17} :catchall_1b

    #@17
    .line 212
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 214
    return-void

    #@1b
    .line 212
    :catchall_1b
    move-exception v1

    #@1c
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1f
    throw v1
.end method
