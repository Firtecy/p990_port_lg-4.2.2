.class public abstract Landroid/media/IMediaScannerListener$Stub;
.super Landroid/os/Binder;
.source "IMediaScannerListener.java"

# interfaces
.implements Landroid/media/IMediaScannerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/IMediaScannerListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/IMediaScannerListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.media.IMediaScannerListener"

.field static final TRANSACTION_scanCompleted:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 18
    const-string v0, "android.media.IMediaScannerListener"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/media/IMediaScannerListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 19
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/media/IMediaScannerListener;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 26
    if-nez p0, :cond_4

    #@2
    .line 27
    const/4 v0, 0x0

    #@3
    .line 33
    :goto_3
    return-object v0

    #@4
    .line 29
    :cond_4
    const-string v1, "android.media.IMediaScannerListener"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 30
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/media/IMediaScannerListener;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 31
    check-cast v0, Landroid/media/IMediaScannerListener;

    #@12
    goto :goto_3

    #@13
    .line 33
    :cond_13
    new-instance v0, Landroid/media/IMediaScannerListener$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/media/IMediaScannerListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 9
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 41
    sparse-switch p1, :sswitch_data_2c

    #@4
    .line 64
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v2

    #@8
    :goto_8
    return v2

    #@9
    .line 45
    :sswitch_9
    const-string v3, "android.media.IMediaScannerListener"

    #@b
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 50
    :sswitch_f
    const-string v3, "android.media.IMediaScannerListener"

    #@11
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@17
    move-result-object v0

    #@18
    .line 54
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_2a

    #@1e
    .line 55
    sget-object v3, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@20
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@23
    move-result-object v1

    #@24
    check-cast v1, Landroid/net/Uri;

    #@26
    .line 60
    .local v1, _arg1:Landroid/net/Uri;
    :goto_26
    invoke-virtual {p0, v0, v1}, Landroid/media/IMediaScannerListener$Stub;->scanCompleted(Ljava/lang/String;Landroid/net/Uri;)V

    #@29
    goto :goto_8

    #@2a
    .line 58
    .end local v1           #_arg1:Landroid/net/Uri;
    :cond_2a
    const/4 v1, 0x0

    #@2b
    .restart local v1       #_arg1:Landroid/net/Uri;
    goto :goto_26

    #@2c
    .line 41
    :sswitch_data_2c
    .sparse-switch
        0x1 -> :sswitch_f
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
