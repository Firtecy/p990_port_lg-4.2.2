.class public Landroid/media/AudioService$VolumeStreamState;
.super Ljava/lang/Object;
.source "AudioService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/AudioService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "VolumeStreamState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;
    }
.end annotation


# instance fields
.field private mDeathHandlers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final mIndex:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mIndexMax:I

.field private final mLastAudibleIndex:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mLastAudibleVolumeIndexSettingName:Ljava/lang/String;

.field private final mStreamType:I

.field private mVolumeIndexSettingName:Ljava/lang/String;

.field final synthetic this$0:Landroid/media/AudioService;


# direct methods
.method private constructor <init>(Landroid/media/AudioService;Ljava/lang/String;I)V
    .registers 8
    .parameter
    .parameter "settingName"
    .parameter "streamType"

    #@0
    .prologue
    const/16 v3, 0x8

    #@2
    const/4 v2, 0x4

    #@3
    const/high16 v1, 0x3f40

    #@5
    .line 3308
    iput-object p1, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@7
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@a
    .line 3302
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    #@c
    invoke-direct {v0, v3, v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    #@f
    iput-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->mIndex:Ljava/util/concurrent/ConcurrentHashMap;

    #@11
    .line 3304
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    #@13
    invoke-direct {v0, v3, v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    #@16
    iput-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->mLastAudibleIndex:Ljava/util/concurrent/ConcurrentHashMap;

    #@18
    .line 3310
    iput-object p2, p0, Landroid/media/AudioService$VolumeStreamState;->mVolumeIndexSettingName:Ljava/lang/String;

    #@1a
    .line 3311
    new-instance v0, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    const-string v1, "_last_audible"

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v0

    #@2d
    iput-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->mLastAudibleVolumeIndexSettingName:Ljava/lang/String;

    #@2f
    .line 3313
    iput p3, p0, Landroid/media/AudioService$VolumeStreamState;->mStreamType:I

    #@31
    .line 3314
    invoke-static {p1}, Landroid/media/AudioService;->access$3300(Landroid/media/AudioService;)[I

    #@34
    move-result-object v0

    #@35
    aget v0, v0, p3

    #@37
    iput v0, p0, Landroid/media/AudioService$VolumeStreamState;->mIndexMax:I

    #@39
    .line 3315
    const/4 v0, 0x0

    #@3a
    iget v1, p0, Landroid/media/AudioService$VolumeStreamState;->mIndexMax:I

    #@3c
    invoke-static {p3, v0, v1}, Landroid/media/AudioSystem;->initStreamVolume(III)I

    #@3f
    .line 3316
    iget v0, p0, Landroid/media/AudioService$VolumeStreamState;->mIndexMax:I

    #@41
    mul-int/lit8 v0, v0, 0xa

    #@43
    iput v0, p0, Landroid/media/AudioService$VolumeStreamState;->mIndexMax:I

    #@45
    .line 3319
    new-instance v0, Ljava/util/ArrayList;

    #@47
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@4a
    iput-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->mDeathHandlers:Ljava/util/ArrayList;

    #@4c
    .line 3321
    invoke-virtual {p0}, Landroid/media/AudioService$VolumeStreamState;->readSettings()V

    #@4f
    .line 3322
    return-void
.end method

.method synthetic constructor <init>(Landroid/media/AudioService;Ljava/lang/String;ILandroid/media/AudioService$1;)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 3296
    invoke-direct {p0, p1, p2, p3}, Landroid/media/AudioService$VolumeStreamState;-><init>(Landroid/media/AudioService;Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method static synthetic access$1300(Landroid/media/AudioService$VolumeStreamState;)Ljava/util/concurrent/ConcurrentHashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 3296
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->mLastAudibleIndex:Ljava/util/concurrent/ConcurrentHashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$2100(Landroid/media/AudioService$VolumeStreamState;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 3296
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->mDeathHandlers:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$4300(Landroid/media/AudioService$VolumeStreamState;)Ljava/util/concurrent/ConcurrentHashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 3296
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->mIndex:Ljava/util/concurrent/ConcurrentHashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$4400(Landroid/media/AudioService$VolumeStreamState;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 3296
    iget v0, p0, Landroid/media/AudioService$VolumeStreamState;->mStreamType:I

    #@2
    return v0
.end method

.method static synthetic access$5500(Landroid/media/AudioService$VolumeStreamState;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 3296
    iget v0, p0, Landroid/media/AudioService$VolumeStreamState;->mIndexMax:I

    #@2
    return v0
.end method

.method static synthetic access$700(Landroid/media/AudioService$VolumeStreamState;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 3296
    invoke-direct {p0}, Landroid/media/AudioService$VolumeStreamState;->muteCount()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$900(Landroid/media/AudioService$VolumeStreamState;Ljava/io/PrintWriter;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 3296
    invoke-direct {p0, p1}, Landroid/media/AudioService$VolumeStreamState;->dump(Ljava/io/PrintWriter;)V

    #@3
    return-void
.end method

.method private dump(Ljava/io/PrintWriter;)V
    .registers 7
    .parameter "pw"

    #@0
    .prologue
    .line 3745
    const-string v3, "   Mute count: "

    #@2
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5
    .line 3746
    invoke-direct {p0}, Landroid/media/AudioService$VolumeStreamState;->muteCount()I

    #@8
    move-result v3

    #@9
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(I)V

    #@c
    .line 3747
    const-string v3, "   Current: "

    #@e
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@11
    .line 3748
    iget-object v3, p0, Landroid/media/AudioService$VolumeStreamState;->mIndex:Ljava/util/concurrent/ConcurrentHashMap;

    #@13
    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    #@16
    move-result-object v2

    #@17
    .line 3749
    .local v2, set:Ljava/util/Set;
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@1a
    move-result-object v1

    #@1b
    .line 3750
    .local v1, i:Ljava/util/Iterator;
    :goto_1b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_64

    #@21
    .line 3751
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@24
    move-result-object v0

    #@25
    check-cast v0, Ljava/util/Map$Entry;

    #@27
    .line 3752
    .local v0, entry:Ljava/util/Map$Entry;
    new-instance v4, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@2f
    move-result-object v3

    #@30
    check-cast v3, Ljava/lang/Integer;

    #@32
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@35
    move-result v3

    #@36
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    const-string v4, ": "

    #@40
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v4

    #@44
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@47
    move-result-object v3

    #@48
    check-cast v3, Ljava/lang/Integer;

    #@4a
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@4d
    move-result v3

    #@4e
    add-int/lit8 v3, v3, 0x5

    #@50
    div-int/lit8 v3, v3, 0xa

    #@52
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@55
    move-result-object v3

    #@56
    const-string v4, ", "

    #@58
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v3

    #@5c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v3

    #@60
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@63
    goto :goto_1b

    #@64
    .line 3755
    .end local v0           #entry:Ljava/util/Map$Entry;
    :cond_64
    const-string v3, "\n   Last audible: "

    #@66
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@69
    .line 3756
    iget-object v3, p0, Landroid/media/AudioService$VolumeStreamState;->mLastAudibleIndex:Ljava/util/concurrent/ConcurrentHashMap;

    #@6b
    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    #@6e
    move-result-object v2

    #@6f
    .line 3757
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@72
    move-result-object v1

    #@73
    .line 3758
    :goto_73
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@76
    move-result v3

    #@77
    if-eqz v3, :cond_bc

    #@79
    .line 3759
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@7c
    move-result-object v0

    #@7d
    check-cast v0, Ljava/util/Map$Entry;

    #@7f
    .line 3760
    .restart local v0       #entry:Ljava/util/Map$Entry;
    new-instance v4, Ljava/lang/StringBuilder;

    #@81
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@84
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@87
    move-result-object v3

    #@88
    check-cast v3, Ljava/lang/Integer;

    #@8a
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@8d
    move-result v3

    #@8e
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@91
    move-result-object v3

    #@92
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v3

    #@96
    const-string v4, ": "

    #@98
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v4

    #@9c
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@9f
    move-result-object v3

    #@a0
    check-cast v3, Ljava/lang/Integer;

    #@a2
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@a5
    move-result v3

    #@a6
    add-int/lit8 v3, v3, 0x5

    #@a8
    div-int/lit8 v3, v3, 0xa

    #@aa
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v3

    #@ae
    const-string v4, ", "

    #@b0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v3

    #@b4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b7
    move-result-object v3

    #@b8
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@bb
    goto :goto_73

    #@bc
    .line 3763
    .end local v0           #entry:Ljava/util/Map$Entry;
    :cond_bc
    return-void
.end method

.method private getDeathHandler(Landroid/os/IBinder;Z)Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;
    .registers 9
    .parameter "cb"
    .parameter "state"

    #@0
    .prologue
    .line 3726
    iget-object v4, p0, Landroid/media/AudioService$VolumeStreamState;->mDeathHandlers:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v3

    #@6
    .line 3727
    .local v3, size:I
    const/4 v2, 0x0

    #@7
    .local v2, i:I
    :goto_7
    if-ge v2, v3, :cond_1c

    #@9
    .line 3728
    iget-object v4, p0, Landroid/media/AudioService$VolumeStreamState;->mDeathHandlers:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;

    #@11
    .line 3729
    .local v0, handler:Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;
    invoke-static {v0}, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->access$4500(Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;)Landroid/os/IBinder;

    #@14
    move-result-object v4

    #@15
    if-ne p1, v4, :cond_19

    #@17
    move-object v1, v0

    #@18
    .line 3741
    .end local v0           #handler:Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;
    .local v1, handler:Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;
    :goto_18
    return-object v1

    #@19
    .line 3727
    .end local v1           #handler:Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;
    .restart local v0       #handler:Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;
    :cond_19
    add-int/lit8 v2, v2, 0x1

    #@1b
    goto :goto_7

    #@1c
    .line 3735
    .end local v0           #handler:Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;
    :cond_1c
    if-eqz p2, :cond_25

    #@1e
    .line 3736
    new-instance v0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;

    #@20
    invoke-direct {v0, p0, p1}, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;-><init>(Landroid/media/AudioService$VolumeStreamState;Landroid/os/IBinder;)V

    #@23
    .restart local v0       #handler:Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;
    :goto_23
    move-object v1, v0

    #@24
    .line 3741
    .end local v0           #handler:Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;
    .restart local v1       #handler:Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;
    goto :goto_18

    #@25
    .line 3738
    .end local v1           #handler:Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;
    :cond_25
    const-string v4, "AudioService"

    #@27
    const-string/jumbo v5, "stream was not muted by this client"

    #@2a
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 3739
    const/4 v0, 0x0

    #@2e
    .restart local v0       #handler:Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;
    goto :goto_23
.end method

.method private getValidIndex(I)I
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 3608
    if-gez p1, :cond_4

    #@2
    .line 3609
    const/4 p1, 0x0

    #@3
    .line 3614
    .end local p1
    :cond_3
    :goto_3
    return p1

    #@4
    .line 3610
    .restart local p1
    :cond_4
    iget v0, p0, Landroid/media/AudioService$VolumeStreamState;->mIndexMax:I

    #@6
    if-le p1, v0, :cond_3

    #@8
    .line 3611
    iget p1, p0, Landroid/media/AudioService$VolumeStreamState;->mIndexMax:I

    #@a
    goto :goto_3
.end method

.method private declared-synchronized muteCount()I
    .registers 5

    #@0
    .prologue
    .line 3715
    monitor-enter p0

    #@1
    const/4 v0, 0x0

    #@2
    .line 3716
    .local v0, count:I
    :try_start_2
    iget-object v3, p0, Landroid/media/AudioService$VolumeStreamState;->mDeathHandlers:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v2

    #@8
    .line 3717
    .local v2, size:I
    const/4 v1, 0x0

    #@9
    .local v1, i:I
    :goto_9
    if-ge v1, v2, :cond_1b

    #@b
    .line 3718
    iget-object v3, p0, Landroid/media/AudioService$VolumeStreamState;->mDeathHandlers:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v3

    #@11
    check-cast v3, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;

    #@13
    invoke-static {v3}, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->access$2200(Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;)I
    :try_end_16
    .catchall {:try_start_2 .. :try_end_16} :catchall_1d

    #@16
    move-result v3

    #@17
    add-int/2addr v0, v3

    #@18
    .line 3717
    add-int/lit8 v1, v1, 0x1

    #@1a
    goto :goto_9

    #@1b
    .line 3720
    :cond_1b
    monitor-exit p0

    #@1c
    return v0

    #@1d
    .line 3715
    .end local v1           #i:I
    .end local v2           #size:I
    :catchall_1d
    move-exception v3

    #@1e
    monitor-exit p0

    #@1f
    throw v3
.end method


# virtual methods
.method public adjustIndex(II)Z
    .registers 5
    .parameter "deltaIndex"
    .parameter "device"

    #@0
    .prologue
    .line 3462
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p2, v0}, Landroid/media/AudioService$VolumeStreamState;->getIndex(IZ)I

    #@4
    move-result v0

    #@5
    add-int/2addr v0, p1

    #@6
    const/4 v1, 0x1

    #@7
    invoke-virtual {p0, v0, p2, v1}, Landroid/media/AudioService$VolumeStreamState;->setIndex(IIZ)Z

    #@a
    move-result v0

    #@b
    return v0
.end method

.method public declared-synchronized adjustLastAudibleIndex(II)V
    .registers 4
    .parameter "deltaIndex"
    .parameter "device"

    #@0
    .prologue
    .line 3543
    monitor-enter p0

    #@1
    const/4 v0, 0x1

    #@2
    :try_start_2
    invoke-virtual {p0, p2, v0}, Landroid/media/AudioService$VolumeStreamState;->getIndex(IZ)I

    #@5
    move-result v0

    #@6
    add-int/2addr v0, p1

    #@7
    invoke-virtual {p0, v0, p2}, Landroid/media/AudioService$VolumeStreamState;->setLastAudibleIndex(II)V
    :try_end_a
    .catchall {:try_start_2 .. :try_end_a} :catchall_c

    #@a
    .line 3546
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 3543
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized applyAllVolumes()V
    .registers 9

    #@0
    .prologue
    const/high16 v7, 0x4000

    #@2
    .line 3444
    monitor-enter p0

    #@3
    :try_start_3
    iget v4, p0, Landroid/media/AudioService$VolumeStreamState;->mStreamType:I

    #@5
    const/high16 v5, 0x4000

    #@7
    const/4 v6, 0x0

    #@8
    invoke-virtual {p0, v5, v6}, Landroid/media/AudioService$VolumeStreamState;->getIndex(IZ)I

    #@b
    move-result v5

    #@c
    add-int/lit8 v5, v5, 0x5

    #@e
    div-int/lit8 v5, v5, 0xa

    #@10
    const/high16 v6, 0x4000

    #@12
    invoke-static {v4, v5, v6}, Landroid/media/AudioSystem;->setStreamVolumeIndex(III)I

    #@15
    .line 3448
    iget-object v4, p0, Landroid/media/AudioService$VolumeStreamState;->mIndex:Ljava/util/concurrent/ConcurrentHashMap;

    #@17
    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    #@1a
    move-result-object v3

    #@1b
    .line 3449
    .local v3, set:Ljava/util/Set;
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@1e
    move-result-object v2

    #@1f
    .line 3450
    .local v2, i:Ljava/util/Iterator;
    :cond_1f
    :goto_1f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@22
    move-result v4

    #@23
    if-eqz v4, :cond_4e

    #@25
    .line 3451
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@28
    move-result-object v1

    #@29
    check-cast v1, Ljava/util/Map$Entry;

    #@2b
    .line 3452
    .local v1, entry:Ljava/util/Map$Entry;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@2e
    move-result-object v4

    #@2f
    check-cast v4, Ljava/lang/Integer;

    #@31
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    #@34
    move-result v0

    #@35
    .line 3453
    .local v0, device:I
    if-eq v0, v7, :cond_1f

    #@37
    .line 3454
    iget v5, p0, Landroid/media/AudioService$VolumeStreamState;->mStreamType:I

    #@39
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@3c
    move-result-object v4

    #@3d
    check-cast v4, Ljava/lang/Integer;

    #@3f
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    #@42
    move-result v4

    #@43
    add-int/lit8 v4, v4, 0x5

    #@45
    div-int/lit8 v4, v4, 0xa

    #@47
    invoke-static {v5, v4, v0}, Landroid/media/AudioSystem;->setStreamVolumeIndex(III)I
    :try_end_4a
    .catchall {:try_start_3 .. :try_end_4a} :catchall_4b

    #@4a
    goto :goto_1f

    #@4b
    .line 3444
    .end local v0           #device:I
    .end local v1           #entry:Ljava/util/Map$Entry;
    .end local v2           #i:Ljava/util/Iterator;
    .end local v3           #set:Ljava/util/Set;
    :catchall_4b
    move-exception v4

    #@4c
    monitor-exit p0

    #@4d
    throw v4

    #@4e
    .line 3459
    .restart local v2       #i:Ljava/util/Iterator;
    .restart local v3       #set:Ljava/util/Set;
    :cond_4e
    monitor-exit p0

    #@4f
    return-void
.end method

.method public applyDeviceVolume(I)V
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 3436
    iget v0, p0, Landroid/media/AudioService$VolumeStreamState;->mStreamType:I

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {p0, p1, v1}, Landroid/media/AudioService$VolumeStreamState;->getIndex(IZ)I

    #@6
    move-result v1

    #@7
    add-int/lit8 v1, v1, 0x5

    #@9
    div-int/lit8 v1, v1, 0xa

    #@b
    invoke-static {v0, v1, p1}, Landroid/media/AudioSystem;->setStreamVolumeIndex(III)I

    #@e
    .line 3439
    return-void
.end method

.method public getAllIndexes(Z)Ljava/util/concurrent/ConcurrentHashMap;
    .registers 3
    .parameter "lastAudible"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 3554
    if-eqz p1, :cond_5

    #@2
    .line 3555
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->mLastAudibleIndex:Ljava/util/concurrent/ConcurrentHashMap;

    #@4
    .line 3557
    :goto_4
    return-object v0

    #@5
    :cond_5
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->mIndex:Ljava/util/concurrent/ConcurrentHashMap;

    #@7
    goto :goto_4
.end method

.method public declared-synchronized getIndex(IZ)I
    .registers 6
    .parameter "device"
    .parameter "lastAudible"

    #@0
    .prologue
    .line 3509
    monitor-enter p0

    #@1
    if-eqz p2, :cond_23

    #@3
    .line 3510
    :try_start_3
    iget-object v1, p0, Landroid/media/AudioService$VolumeStreamState;->mLastAudibleIndex:Ljava/util/concurrent/ConcurrentHashMap;

    #@5
    .line 3514
    .local v1, indexes:Ljava/util/concurrent/ConcurrentHashMap;,"Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :goto_5
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8
    move-result-object v2

    #@9
    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Ljava/lang/Integer;

    #@f
    .line 3515
    .local v0, index:Ljava/lang/Integer;
    if-nez v0, :cond_1d

    #@11
    .line 3517
    const/high16 v2, 0x4000

    #@13
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1a
    move-result-object v0

    #@1b
    .end local v0           #index:Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    #@1d
    .line 3519
    .restart local v0       #index:Ljava/lang/Integer;
    :cond_1d
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_20
    .catchall {:try_start_3 .. :try_end_20} :catchall_26

    #@20
    move-result v2

    #@21
    monitor-exit p0

    #@22
    return v2

    #@23
    .line 3512
    .end local v0           #index:Ljava/lang/Integer;
    .end local v1           #indexes:Ljava/util/concurrent/ConcurrentHashMap;,"Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_23
    :try_start_23
    iget-object v1, p0, Landroid/media/AudioService$VolumeStreamState;->mIndex:Ljava/util/concurrent/ConcurrentHashMap;
    :try_end_25
    .catchall {:try_start_23 .. :try_end_25} :catchall_26

    #@25
    .restart local v1       #indexes:Ljava/util/concurrent/ConcurrentHashMap;,"Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    goto :goto_5

    #@26
    .line 3509
    .end local v1           #indexes:Ljava/util/concurrent/ConcurrentHashMap;,"Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :catchall_26
    move-exception v2

    #@27
    monitor-exit p0

    #@28
    throw v2
.end method

.method public getMaxIndex()I
    .registers 2

    #@0
    .prologue
    .line 3549
    iget v0, p0, Landroid/media/AudioService$VolumeStreamState;->mIndexMax:I

    #@2
    return v0
.end method

.method public getSettingNameForDevice(ZI)Ljava/lang/String;
    .registers 7
    .parameter "lastAudible"
    .parameter "device"

    #@0
    .prologue
    .line 3325
    if-eqz p1, :cond_f

    #@2
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->mLastAudibleVolumeIndexSettingName:Ljava/lang/String;

    #@4
    .line 3328
    .local v0, name:Ljava/lang/String;
    :goto_4
    invoke-static {p2}, Landroid/media/AudioSystem;->getDeviceName(I)Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    .line 3329
    .local v1, suffix:Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_12

    #@e
    .line 3332
    .end local v0           #name:Ljava/lang/String;
    :goto_e
    return-object v0

    #@f
    .line 3325
    .end local v1           #suffix:Ljava/lang/String;
    :cond_f
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->mVolumeIndexSettingName:Ljava/lang/String;

    #@11
    goto :goto_4

    #@12
    .line 3332
    .restart local v0       #name:Ljava/lang/String;
    .restart local v1       #suffix:Ljava/lang/String;
    :cond_12
    new-instance v2, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    const-string v3, "_"

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    goto :goto_e
.end method

.method public getStreamType()I
    .registers 2

    #@0
    .prologue
    .line 3604
    iget v0, p0, Landroid/media/AudioService$VolumeStreamState;->mStreamType:I

    #@2
    return v0
.end method

.method public declared-synchronized mute(Landroid/os/IBinder;Z)V
    .registers 7
    .parameter "cb"
    .parameter "state"

    #@0
    .prologue
    .line 3595
    monitor-enter p0

    #@1
    :try_start_1
    invoke-direct {p0, p1, p2}, Landroid/media/AudioService$VolumeStreamState;->getDeathHandler(Landroid/os/IBinder;Z)Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;

    #@4
    move-result-object v0

    #@5
    .line 3596
    .local v0, handler:Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;
    if-nez v0, :cond_23

    #@7
    .line 3597
    const-string v1, "AudioService"

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "Could not get client death handler for stream: "

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    iget v3, p0, Landroid/media/AudioService$VolumeStreamState;->mStreamType:I

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_21
    .catchall {:try_start_1 .. :try_end_21} :catchall_27

    #@21
    .line 3601
    :goto_21
    monitor-exit p0

    #@22
    return-void

    #@23
    .line 3600
    :cond_23
    :try_start_23
    invoke-virtual {v0, p2}, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->mute(Z)V
    :try_end_26
    .catchall {:try_start_23 .. :try_end_26} :catchall_27

    #@26
    goto :goto_21

    #@27
    .line 3595
    .end local v0           #handler:Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;
    :catchall_27
    move-exception v1

    #@28
    monitor-exit p0

    #@29
    throw v1
.end method

.method public declared-synchronized readSettings()V
    .registers 14

    #@0
    .prologue
    .line 3336
    monitor-enter p0

    #@1
    const v12, 0x401fffff

    #@4
    .line 3344
    .local v12, remainingDevices:I
    :try_start_4
    iget v0, p0, Landroid/media/AudioService$VolumeStreamState;->mStreamType:I

    #@6
    const/4 v1, 0x7

    #@7
    if-ne v0, v1, :cond_53

    #@9
    .line 3347
    sget-object v0, Landroid/media/AudioManager;->DEFAULT_STREAM_VOLUME:[I

    #@b
    iget v1, p0, Landroid/media/AudioService$VolumeStreamState;->mStreamType:I

    #@d
    aget v0, v0, v1

    #@f
    mul-int/lit8 v9, v0, 0xa

    #@11
    .line 3348
    .local v9, index:I
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@13
    invoke-static {v0}, Landroid/media/AudioService;->access$3400(Landroid/media/AudioService;)Ljava/lang/Boolean;

    #@16
    move-result-object v1

    #@17
    monitor-enter v1
    :try_end_18
    .catchall {:try_start_4 .. :try_end_18} :catchall_50

    #@18
    .line 3349
    :try_start_18
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@1a
    invoke-static {v0}, Landroid/media/AudioService;->access$3400(Landroid/media/AudioService;)Ljava/lang/Boolean;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    #@21
    move-result v0

    #@22
    if-eqz v0, :cond_26

    #@24
    .line 3350
    iget v9, p0, Landroid/media/AudioService$VolumeStreamState;->mIndexMax:I

    #@26
    .line 3352
    :cond_26
    monitor-exit v1
    :try_end_27
    .catchall {:try_start_18 .. :try_end_27} :catchall_4d

    #@27
    .line 3353
    :try_start_27
    invoke-direct {p0}, Landroid/media/AudioService$VolumeStreamState;->muteCount()I

    #@2a
    move-result v0

    #@2b
    if-nez v0, :cond_3c

    #@2d
    .line 3354
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->mIndex:Ljava/util/concurrent/ConcurrentHashMap;

    #@2f
    const/high16 v1, 0x4000

    #@31
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@34
    move-result-object v1

    #@35
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3c
    .line 3356
    :cond_3c
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->mLastAudibleIndex:Ljava/util/concurrent/ConcurrentHashMap;

    #@3e
    const/high16 v1, 0x4000

    #@40
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@43
    move-result-object v1

    #@44
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@47
    move-result-object v2

    #@48
    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4b
    .catchall {:try_start_27 .. :try_end_4b} :catchall_50

    #@4b
    .line 3433
    .end local v9           #index:I
    :cond_4b
    monitor-exit p0

    #@4c
    return-void

    #@4d
    .line 3352
    .restart local v9       #index:I
    :catchall_4d
    move-exception v0

    #@4e
    :try_start_4e
    monitor-exit v1
    :try_end_4f
    .catchall {:try_start_4e .. :try_end_4f} :catchall_4d

    #@4f
    :try_start_4f
    throw v0
    :try_end_50
    .catchall {:try_start_4f .. :try_end_50} :catchall_50

    #@50
    .line 3336
    .end local v9           #index:I
    :catchall_50
    move-exception v0

    #@51
    monitor-exit p0

    #@52
    throw v0

    #@53
    .line 3360
    :cond_53
    const/4 v8, 0x0

    #@54
    .local v8, i:I
    :goto_54
    if-eqz v12, :cond_4b

    #@56
    .line 3361
    const/4 v0, 0x1

    #@57
    shl-int v4, v0, v8

    #@59
    .line 3362
    .local v4, device:I
    and-int v0, v4, v12

    #@5b
    if-nez v0, :cond_60

    #@5d
    .line 3360
    :cond_5d
    :goto_5d
    add-int/lit8 v8, v8, 0x1

    #@5f
    goto :goto_54

    #@60
    .line 3365
    :cond_60
    xor-int/lit8 v0, v4, -0x1

    #@62
    and-int/2addr v12, v0

    #@63
    .line 3368
    :try_start_63
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@65
    invoke-static {v0}, Landroid/media/AudioService;->access$3500(Landroid/media/AudioService;)[I

    #@68
    move-result-object v0

    #@69
    iget v1, p0, Landroid/media/AudioService$VolumeStreamState;->mStreamType:I

    #@6b
    aget v0, v0, v1

    #@6d
    const/4 v1, 0x3

    #@6e
    if-ne v0, v1, :cond_99

    #@70
    and-int/lit8 v0, v4, 0x0

    #@72
    if-eqz v0, :cond_99

    #@74
    .line 3370
    invoke-direct {p0}, Landroid/media/AudioService$VolumeStreamState;->muteCount()I

    #@77
    move-result v0

    #@78
    if-nez v0, :cond_89

    #@7a
    .line 3371
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->mIndex:Ljava/util/concurrent/ConcurrentHashMap;

    #@7c
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7f
    move-result-object v1

    #@80
    iget v2, p0, Landroid/media/AudioService$VolumeStreamState;->mIndexMax:I

    #@82
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@85
    move-result-object v2

    #@86
    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@89
    .line 3373
    :cond_89
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->mLastAudibleIndex:Ljava/util/concurrent/ConcurrentHashMap;

    #@8b
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8e
    move-result-object v1

    #@8f
    iget v2, p0, Landroid/media/AudioService$VolumeStreamState;->mIndexMax:I

    #@91
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@94
    move-result-object v2

    #@95
    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@98
    goto :goto_5d

    #@99
    .line 3377
    :cond_99
    const/4 v0, 0x0

    #@9a
    invoke-virtual {p0, v0, v4}, Landroid/media/AudioService$VolumeStreamState;->getSettingNameForDevice(ZI)Ljava/lang/String;

    #@9d
    move-result-object v11

    #@9e
    .line 3380
    .local v11, name:Ljava/lang/String;
    const/high16 v0, 0x4000

    #@a0
    if-ne v4, v0, :cond_163

    #@a2
    sget-object v0, Landroid/media/AudioManager;->DEFAULT_STREAM_VOLUME:[I

    #@a4
    iget v1, p0, Landroid/media/AudioService$VolumeStreamState;->mStreamType:I

    #@a6
    aget v7, v0, v1

    #@a8
    .line 3382
    .local v7, defaultIndex:I
    :goto_a8
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@aa
    invoke-static {v0}, Landroid/media/AudioService;->access$3600(Landroid/media/AudioService;)Landroid/content/ContentResolver;

    #@ad
    move-result-object v0

    #@ae
    const/4 v1, -0x2

    #@af
    invoke-static {v0, v11, v7, v1}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@b2
    move-result v9

    #@b3
    .line 3384
    .restart local v9       #index:I
    const/4 v0, -0x1

    #@b4
    if-eq v9, v0, :cond_5d

    #@b6
    .line 3389
    const/4 v0, 0x1

    #@b7
    invoke-virtual {p0, v0, v4}, Landroid/media/AudioService$VolumeStreamState;->getSettingNameForDevice(ZI)Ljava/lang/String;

    #@ba
    move-result-object v11

    #@bb
    .line 3392
    if-lez v9, :cond_166

    #@bd
    move v7, v9

    #@be
    .line 3394
    :goto_be
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@c0
    invoke-static {v0}, Landroid/media/AudioService;->access$3600(Landroid/media/AudioService;)Landroid/content/ContentResolver;

    #@c3
    move-result-object v0

    #@c4
    const/4 v1, -0x2

    #@c5
    invoke-static {v0, v11, v7, v1}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@c8
    move-result v10

    #@c9
    .line 3399
    .local v10, lastAudibleIndex:I
    if-nez v10, :cond_fd

    #@cb
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@cd
    invoke-static {v0}, Landroid/media/AudioService;->access$3700(Landroid/media/AudioService;)Z

    #@d0
    move-result v0

    #@d1
    if-eqz v0, :cond_fd

    #@d3
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@d5
    invoke-static {v0}, Landroid/media/AudioService;->access$3800(Landroid/media/AudioService;)Z

    #@d8
    move-result v0

    #@d9
    if-nez v0, :cond_fd

    #@db
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@dd
    invoke-static {v0}, Landroid/media/AudioService;->access$3500(Landroid/media/AudioService;)[I

    #@e0
    move-result-object v0

    #@e1
    iget v1, p0, Landroid/media/AudioService$VolumeStreamState;->mStreamType:I

    #@e3
    aget v0, v0, v1

    #@e5
    const/4 v1, 0x2

    #@e6
    if-ne v0, v1, :cond_fd

    #@e8
    .line 3402
    sget-object v0, Landroid/media/AudioManager;->DEFAULT_STREAM_VOLUME:[I

    #@ea
    iget v1, p0, Landroid/media/AudioService$VolumeStreamState;->mStreamType:I

    #@ec
    aget v10, v0, v1

    #@ee
    .line 3404
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@f0
    invoke-static {v0}, Landroid/media/AudioService;->access$100(Landroid/media/AudioService;)Landroid/media/AudioService$AudioHandler;

    #@f3
    move-result-object v0

    #@f4
    const/4 v1, 0x1

    #@f5
    const/4 v2, 0x2

    #@f6
    const/4 v3, 0x2

    #@f7
    const/16 v6, 0x1f4

    #@f9
    move-object v5, p0

    #@fa
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->access$200(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@fd
    .line 3412
    :cond_fd
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->mLastAudibleIndex:Ljava/util/concurrent/ConcurrentHashMap;

    #@ff
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@102
    move-result-object v1

    #@103
    mul-int/lit8 v2, v10, 0xa

    #@105
    invoke-direct {p0, v2}, Landroid/media/AudioService$VolumeStreamState;->getValidIndex(I)I

    #@108
    move-result v2

    #@109
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10c
    move-result-object v2

    #@10d
    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@110
    .line 3415
    if-nez v9, :cond_148

    #@112
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@114
    invoke-static {v0}, Landroid/media/AudioService;->access$3900(Landroid/media/AudioService;)I

    #@117
    move-result v0

    #@118
    const/4 v1, 0x2

    #@119
    if-ne v0, v1, :cond_148

    #@11b
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@11d
    invoke-static {v0}, Landroid/media/AudioService;->access$3800(Landroid/media/AudioService;)Z

    #@120
    move-result v0

    #@121
    if-nez v0, :cond_148

    #@123
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@125
    invoke-static {v0}, Landroid/media/AudioService;->access$3700(Landroid/media/AudioService;)Z

    #@128
    move-result v0

    #@129
    if-eqz v0, :cond_148

    #@12b
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@12d
    invoke-static {v0}, Landroid/media/AudioService;->access$3500(Landroid/media/AudioService;)[I

    #@130
    move-result-object v0

    #@131
    iget v1, p0, Landroid/media/AudioService$VolumeStreamState;->mStreamType:I

    #@133
    aget v0, v0, v1

    #@135
    const/4 v1, 0x2

    #@136
    if-ne v0, v1, :cond_148

    #@138
    .line 3419
    move v9, v10

    #@139
    .line 3421
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@13b
    invoke-static {v0}, Landroid/media/AudioService;->access$100(Landroid/media/AudioService;)Landroid/media/AudioService$AudioHandler;

    #@13e
    move-result-object v0

    #@13f
    const/4 v1, 0x1

    #@140
    const/4 v2, 0x2

    #@141
    const/4 v3, 0x1

    #@142
    const/16 v6, 0x1f4

    #@144
    move-object v5, p0

    #@145
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->access$200(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@148
    .line 3429
    :cond_148
    invoke-direct {p0}, Landroid/media/AudioService$VolumeStreamState;->muteCount()I

    #@14b
    move-result v0

    #@14c
    if-nez v0, :cond_5d

    #@14e
    .line 3430
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState;->mIndex:Ljava/util/concurrent/ConcurrentHashMap;

    #@150
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@153
    move-result-object v1

    #@154
    mul-int/lit8 v2, v9, 0xa

    #@156
    invoke-direct {p0, v2}, Landroid/media/AudioService$VolumeStreamState;->getValidIndex(I)I

    #@159
    move-result v2

    #@15a
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@15d
    move-result-object v2

    #@15e
    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@161
    goto/16 :goto_5d

    #@163
    .line 3380
    .end local v7           #defaultIndex:I
    .end local v9           #index:I
    .end local v10           #lastAudibleIndex:I
    :cond_163
    const/4 v7, -0x1

    #@164
    goto/16 :goto_a8

    #@166
    .line 3392
    .restart local v7       #defaultIndex:I
    .restart local v9       #index:I
    :cond_166
    sget-object v0, Landroid/media/AudioManager;->DEFAULT_STREAM_VOLUME:[I

    #@168
    iget v1, p0, Landroid/media/AudioService$VolumeStreamState;->mStreamType:I

    #@16a
    aget v7, v0, v1
    :try_end_16c
    .catchall {:try_start_63 .. :try_end_16c} :catchall_50

    #@16c
    goto/16 :goto_be
.end method

.method public declared-synchronized setAllIndexes(Landroid/media/AudioService$VolumeStreamState;Z)V
    .registers 12
    .parameter "srcStream"
    .parameter "lastAudible"

    #@0
    .prologue
    .line 3562
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p1, p2}, Landroid/media/AudioService$VolumeStreamState;->getAllIndexes(Z)Ljava/util/concurrent/ConcurrentHashMap;

    #@4
    move-result-object v4

    #@5
    .line 3563
    .local v4, indexes:Ljava/util/concurrent/ConcurrentHashMap;,"Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    #@8
    move-result-object v5

    #@9
    .line 3564
    .local v5, set:Ljava/util/Set;
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@c
    move-result-object v2

    #@d
    .line 3565
    .local v2, i:Ljava/util/Iterator;
    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@10
    move-result v6

    #@11
    if-eqz v6, :cond_47

    #@13
    .line 3566
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@16
    move-result-object v1

    #@17
    check-cast v1, Ljava/util/Map$Entry;

    #@19
    .line 3567
    .local v1, entry:Ljava/util/Map$Entry;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@1c
    move-result-object v6

    #@1d
    check-cast v6, Ljava/lang/Integer;

    #@1f
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    #@22
    move-result v0

    #@23
    .line 3568
    .local v0, device:I
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@26
    move-result-object v6

    #@27
    check-cast v6, Ljava/lang/Integer;

    #@29
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    #@2c
    move-result v3

    #@2d
    .line 3569
    .local v3, index:I
    iget-object v6, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@2f
    invoke-virtual {p1}, Landroid/media/AudioService$VolumeStreamState;->getStreamType()I

    #@32
    move-result v7

    #@33
    iget v8, p0, Landroid/media/AudioService$VolumeStreamState;->mStreamType:I

    #@35
    invoke-static {v6, v3, v7, v8}, Landroid/media/AudioService;->access$4100(Landroid/media/AudioService;III)I

    #@38
    move-result v3

    #@39
    .line 3571
    if-eqz p2, :cond_42

    #@3b
    .line 3572
    invoke-virtual {p0, v3, v0}, Landroid/media/AudioService$VolumeStreamState;->setLastAudibleIndex(II)V
    :try_end_3e
    .catchall {:try_start_1 .. :try_end_3e} :catchall_3f

    #@3e
    goto :goto_d

    #@3f
    .line 3562
    .end local v0           #device:I
    .end local v1           #entry:Ljava/util/Map$Entry;
    .end local v2           #i:Ljava/util/Iterator;
    .end local v3           #index:I
    .end local v4           #indexes:Ljava/util/concurrent/ConcurrentHashMap;,"Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .end local v5           #set:Ljava/util/Set;
    :catchall_3f
    move-exception v6

    #@40
    monitor-exit p0

    #@41
    throw v6

    #@42
    .line 3574
    .restart local v0       #device:I
    .restart local v1       #entry:Ljava/util/Map$Entry;
    .restart local v2       #i:Ljava/util/Iterator;
    .restart local v3       #index:I
    .restart local v4       #indexes:Ljava/util/concurrent/ConcurrentHashMap;,"Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .restart local v5       #set:Ljava/util/Set;
    :cond_42
    const/4 v6, 0x0

    #@43
    :try_start_43
    invoke-virtual {p0, v3, v0, v6}, Landroid/media/AudioService$VolumeStreamState;->setIndex(IIZ)Z
    :try_end_46
    .catchall {:try_start_43 .. :try_end_46} :catchall_3f

    #@46
    goto :goto_d

    #@47
    .line 3577
    .end local v0           #device:I
    .end local v1           #entry:Ljava/util/Map$Entry;
    .end local v3           #index:I
    :cond_47
    monitor-exit p0

    #@48
    return-void
.end method

.method public declared-synchronized setAllIndexesToMax()V
    .registers 5

    #@0
    .prologue
    .line 3580
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v3, p0, Landroid/media/AudioService$VolumeStreamState;->mIndex:Ljava/util/concurrent/ConcurrentHashMap;

    #@3
    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    #@6
    move-result-object v2

    #@7
    .line 3581
    .local v2, set:Ljava/util/Set;
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v1

    #@b
    .line 3582
    .local v1, i:Ljava/util/Iterator;
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_24

    #@11
    .line 3583
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Ljava/util/Map$Entry;

    #@17
    .line 3584
    .local v0, entry:Ljava/util/Map$Entry;
    iget v3, p0, Landroid/media/AudioService$VolumeStreamState;->mIndexMax:I

    #@19
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1c
    move-result-object v3

    #@1d
    invoke-interface {v0, v3}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_20
    .catchall {:try_start_1 .. :try_end_20} :catchall_21

    #@20
    goto :goto_b

    #@21
    .line 3580
    .end local v0           #entry:Ljava/util/Map$Entry;
    .end local v1           #i:Ljava/util/Iterator;
    .end local v2           #set:Ljava/util/Set;
    :catchall_21
    move-exception v3

    #@22
    monitor-exit p0

    #@23
    throw v3

    #@24
    .line 3586
    .restart local v1       #i:Ljava/util/Iterator;
    .restart local v2       #set:Ljava/util/Set;
    :cond_24
    :try_start_24
    iget-object v3, p0, Landroid/media/AudioService$VolumeStreamState;->mLastAudibleIndex:Ljava/util/concurrent/ConcurrentHashMap;

    #@26
    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    #@29
    move-result-object v2

    #@2a
    .line 3587
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@2d
    move-result-object v1

    #@2e
    .line 3588
    :goto_2e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@31
    move-result v3

    #@32
    if-eqz v3, :cond_44

    #@34
    .line 3589
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@37
    move-result-object v0

    #@38
    check-cast v0, Ljava/util/Map$Entry;

    #@3a
    .line 3590
    .restart local v0       #entry:Ljava/util/Map$Entry;
    iget v3, p0, Landroid/media/AudioService$VolumeStreamState;->mIndexMax:I

    #@3c
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3f
    move-result-object v3

    #@40
    invoke-interface {v0, v3}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_43
    .catchall {:try_start_24 .. :try_end_43} :catchall_21

    #@43
    goto :goto_2e

    #@44
    .line 3592
    .end local v0           #entry:Ljava/util/Map$Entry;
    :cond_44
    monitor-exit p0

    #@45
    return-void
.end method

.method public declared-synchronized setIndex(IIZ)Z
    .registers 14
    .parameter "index"
    .parameter "device"
    .parameter "lastAudible"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 3469
    monitor-enter p0

    #@3
    const/4 v7, 0x0

    #@4
    :try_start_4
    invoke-virtual {p0, p2, v7}, Landroid/media/AudioService$VolumeStreamState;->getIndex(IZ)I

    #@7
    move-result v2

    #@8
    .line 3470
    .local v2, oldIndex:I
    invoke-direct {p0, p1}, Landroid/media/AudioService$VolumeStreamState;->getValidIndex(I)I

    #@b
    move-result p1

    #@c
    .line 3471
    iget-object v7, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@e
    invoke-static {v7}, Landroid/media/AudioService;->access$3400(Landroid/media/AudioService;)Ljava/lang/Boolean;

    #@11
    move-result-object v7

    #@12
    monitor-enter v7
    :try_end_13
    .catchall {:try_start_4 .. :try_end_13} :catchall_98

    #@13
    .line 3472
    :try_start_13
    iget v8, p0, Landroid/media/AudioService$VolumeStreamState;->mStreamType:I

    #@15
    const/4 v9, 0x7

    #@16
    if-ne v8, v9, :cond_26

    #@18
    iget-object v8, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@1a
    invoke-static {v8}, Landroid/media/AudioService;->access$3400(Landroid/media/AudioService;)Ljava/lang/Boolean;

    #@1d
    move-result-object v8

    #@1e
    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    #@21
    move-result v8

    #@22
    if-eqz v8, :cond_26

    #@24
    .line 3473
    iget p1, p0, Landroid/media/AudioService$VolumeStreamState;->mIndexMax:I

    #@26
    .line 3475
    :cond_26
    monitor-exit v7
    :try_end_27
    .catchall {:try_start_13 .. :try_end_27} :catchall_95

    #@27
    .line 3476
    :try_start_27
    iget-object v7, p0, Landroid/media/AudioService$VolumeStreamState;->mIndex:Ljava/util/concurrent/ConcurrentHashMap;

    #@29
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2c
    move-result-object v8

    #@2d
    invoke-direct {p0, p1}, Landroid/media/AudioService$VolumeStreamState;->getValidIndex(I)I

    #@30
    move-result v9

    #@31
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@34
    move-result-object v9

    #@35
    invoke-virtual {v7, v8, v9}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@38
    .line 3478
    if-eq v2, p1, :cond_9d

    #@3a
    .line 3479
    if-eqz p3, :cond_49

    #@3c
    .line 3480
    iget-object v7, p0, Landroid/media/AudioService$VolumeStreamState;->mLastAudibleIndex:Ljava/util/concurrent/ConcurrentHashMap;

    #@3e
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@41
    move-result-object v8

    #@42
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@45
    move-result-object v9

    #@46
    invoke-virtual {v7, v8, v9}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@49
    .line 3485
    :cond_49
    iget-object v7, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@4b
    iget v8, p0, Landroid/media/AudioService$VolumeStreamState;->mStreamType:I

    #@4d
    invoke-static {v7, v8}, Landroid/media/AudioService;->access$4000(Landroid/media/AudioService;I)I

    #@50
    move-result v7

    #@51
    if-ne p2, v7, :cond_9b

    #@53
    move v0, v5

    #@54
    .line 3486
    .local v0, currentDevice:Z
    :goto_54
    invoke-static {}, Landroid/media/AudioSystem;->getNumStreamTypes()I

    #@57
    move-result v1

    #@58
    .line 3487
    .local v1, numStreamTypes:I
    add-int/lit8 v4, v1, -0x1

    #@5a
    .local v4, streamType:I
    :goto_5a
    if-ltz v4, :cond_9e

    #@5c
    .line 3488
    iget v6, p0, Landroid/media/AudioService$VolumeStreamState;->mStreamType:I

    #@5e
    if-eq v4, v6, :cond_92

    #@60
    iget-object v6, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@62
    invoke-static {v6}, Landroid/media/AudioService;->access$3500(Landroid/media/AudioService;)[I

    #@65
    move-result-object v6

    #@66
    aget v6, v6, v4

    #@68
    iget v7, p0, Landroid/media/AudioService$VolumeStreamState;->mStreamType:I

    #@6a
    if-ne v6, v7, :cond_92

    #@6c
    .line 3490
    iget-object v6, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@6e
    iget v7, p0, Landroid/media/AudioService$VolumeStreamState;->mStreamType:I

    #@70
    invoke-static {v6, p1, v7, v4}, Landroid/media/AudioService;->access$4100(Landroid/media/AudioService;III)I

    #@73
    move-result v3

    #@74
    .line 3491
    .local v3, scaledIndex:I
    iget-object v6, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@76
    invoke-static {v6}, Landroid/media/AudioService;->access$4200(Landroid/media/AudioService;)[Landroid/media/AudioService$VolumeStreamState;

    #@79
    move-result-object v6

    #@7a
    aget-object v6, v6, v4

    #@7c
    invoke-virtual {v6, v3, p2, p3}, Landroid/media/AudioService$VolumeStreamState;->setIndex(IIZ)Z

    #@7f
    .line 3494
    if-eqz v0, :cond_92

    #@81
    .line 3495
    iget-object v6, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@83
    invoke-static {v6}, Landroid/media/AudioService;->access$4200(Landroid/media/AudioService;)[Landroid/media/AudioService$VolumeStreamState;

    #@86
    move-result-object v6

    #@87
    aget-object v6, v6, v4

    #@89
    iget-object v7, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@8b
    invoke-static {v7, v4}, Landroid/media/AudioService;->access$4000(Landroid/media/AudioService;I)I

    #@8e
    move-result v7

    #@8f
    invoke-virtual {v6, v3, v7, p3}, Landroid/media/AudioService$VolumeStreamState;->setIndex(IIZ)Z
    :try_end_92
    .catchall {:try_start_27 .. :try_end_92} :catchall_98

    #@92
    .line 3487
    .end local v3           #scaledIndex:I
    :cond_92
    add-int/lit8 v4, v4, -0x1

    #@94
    goto :goto_5a

    #@95
    .line 3475
    .end local v0           #currentDevice:Z
    .end local v1           #numStreamTypes:I
    .end local v4           #streamType:I
    :catchall_95
    move-exception v5

    #@96
    :try_start_96
    monitor-exit v7
    :try_end_97
    .catchall {:try_start_96 .. :try_end_97} :catchall_95

    #@97
    :try_start_97
    throw v5
    :try_end_98
    .catchall {:try_start_97 .. :try_end_98} :catchall_98

    #@98
    .line 3469
    .end local v2           #oldIndex:I
    :catchall_98
    move-exception v5

    #@99
    monitor-exit p0

    #@9a
    throw v5

    #@9b
    .restart local v2       #oldIndex:I
    :cond_9b
    move v0, v6

    #@9c
    .line 3485
    goto :goto_54

    #@9d
    :cond_9d
    move v5, v6

    #@9e
    .line 3503
    :cond_9e
    monitor-exit p0

    #@9f
    return v5
.end method

.method public declared-synchronized setLastAudibleIndex(II)V
    .registers 10
    .parameter "index"
    .parameter "device"

    #@0
    .prologue
    .line 3526
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v4, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@3
    iget v5, p0, Landroid/media/AudioService$VolumeStreamState;->mStreamType:I

    #@5
    invoke-static {v4, v5}, Landroid/media/AudioService;->access$4000(Landroid/media/AudioService;I)I

    #@8
    move-result v4

    #@9
    if-ne p2, v4, :cond_4d

    #@b
    const/4 v0, 0x1

    #@c
    .line 3527
    .local v0, currentDevice:Z
    :goto_c
    invoke-static {}, Landroid/media/AudioSystem;->getNumStreamTypes()I

    #@f
    move-result v1

    #@10
    .line 3528
    .local v1, numStreamTypes:I
    add-int/lit8 v3, v1, -0x1

    #@12
    .local v3, streamType:I
    :goto_12
    if-ltz v3, :cond_4f

    #@14
    .line 3529
    iget v4, p0, Landroid/media/AudioService$VolumeStreamState;->mStreamType:I

    #@16
    if-eq v3, v4, :cond_4a

    #@18
    iget-object v4, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@1a
    invoke-static {v4}, Landroid/media/AudioService;->access$3500(Landroid/media/AudioService;)[I

    #@1d
    move-result-object v4

    #@1e
    aget v4, v4, v3

    #@20
    iget v5, p0, Landroid/media/AudioService$VolumeStreamState;->mStreamType:I

    #@22
    if-ne v4, v5, :cond_4a

    #@24
    .line 3531
    iget-object v4, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@26
    iget v5, p0, Landroid/media/AudioService$VolumeStreamState;->mStreamType:I

    #@28
    invoke-static {v4, p1, v5, v3}, Landroid/media/AudioService;->access$4100(Landroid/media/AudioService;III)I

    #@2b
    move-result v2

    #@2c
    .line 3532
    .local v2, scaledIndex:I
    iget-object v4, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@2e
    invoke-static {v4}, Landroid/media/AudioService;->access$4200(Landroid/media/AudioService;)[Landroid/media/AudioService$VolumeStreamState;

    #@31
    move-result-object v4

    #@32
    aget-object v4, v4, v3

    #@34
    invoke-virtual {v4, v2, p2}, Landroid/media/AudioService$VolumeStreamState;->setLastAudibleIndex(II)V

    #@37
    .line 3533
    if-eqz v0, :cond_4a

    #@39
    .line 3534
    iget-object v4, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@3b
    invoke-static {v4}, Landroid/media/AudioService;->access$4200(Landroid/media/AudioService;)[Landroid/media/AudioService$VolumeStreamState;

    #@3e
    move-result-object v4

    #@3f
    aget-object v4, v4, v3

    #@41
    iget-object v5, p0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@43
    invoke-static {v5, v3}, Landroid/media/AudioService;->access$4000(Landroid/media/AudioService;I)I

    #@46
    move-result v5

    #@47
    invoke-virtual {v4, v2, v5}, Landroid/media/AudioService$VolumeStreamState;->setLastAudibleIndex(II)V

    #@4a
    .line 3528
    .end local v2           #scaledIndex:I
    :cond_4a
    add-int/lit8 v3, v3, -0x1

    #@4c
    goto :goto_12

    #@4d
    .line 3526
    .end local v0           #currentDevice:Z
    .end local v1           #numStreamTypes:I
    .end local v3           #streamType:I
    :cond_4d
    const/4 v0, 0x0

    #@4e
    goto :goto_c

    #@4f
    .line 3539
    .restart local v0       #currentDevice:Z
    .restart local v1       #numStreamTypes:I
    .restart local v3       #streamType:I
    :cond_4f
    iget-object v4, p0, Landroid/media/AudioService$VolumeStreamState;->mLastAudibleIndex:Ljava/util/concurrent/ConcurrentHashMap;

    #@51
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@54
    move-result-object v5

    #@55
    invoke-direct {p0, p1}, Landroid/media/AudioService$VolumeStreamState;->getValidIndex(I)I

    #@58
    move-result v6

    #@59
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5c
    move-result-object v6

    #@5d
    invoke-virtual {v4, v5, v6}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_60
    .catchall {:try_start_1 .. :try_end_60} :catchall_62

    #@60
    .line 3540
    monitor-exit p0

    #@61
    return-void

    #@62
    .line 3526
    .end local v0           #currentDevice:Z
    .end local v1           #numStreamTypes:I
    .end local v3           #streamType:I
    :catchall_62
    move-exception v4

    #@63
    monitor-exit p0

    #@64
    throw v4
.end method
