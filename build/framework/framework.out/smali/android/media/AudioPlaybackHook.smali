.class public Landroid/media/AudioPlaybackHook;
.super Ljava/lang/Object;
.source "AudioPlaybackHook.java"


# static fields
.field private static final AUDIOBHOOK_ERROR_SETUP_INVALIDCHANNELMASK:I = -0x11

.field private static final AUDIOBHOOK_ERROR_SETUP_INVALIDFORMAT:I = -0x12

.field private static final AUDIOBHOOK_ERROR_SETUP_INVALIDSOURCE:I = -0x13

.field private static final AUDIOBHOOK_ERROR_SETUP_NATIVEINITFAILED:I = -0x14

.field private static final AUDIOBHOOK_ERROR_SETUP_ZEROFRAMECOUNT:I = -0x10

.field public static final ERROR:I = -0x1

.field public static final ERROR_BAD_VALUE:I = -0x2

.field public static final ERROR_INVALID_OPERATION:I = -0x3

.field public static final STATE_HOOKING:I = 0x2

.field public static final STATE_INITIALIZED:I = 0x1

.field public static final STATE_UNINITIALIZED:I = 0x0

.field public static final SUCCESS:I = 0x0

.field private static final TAG:Ljava/lang/String; = "AudioHook-Java"


# instance fields
.field private mBufferSize:I

.field private mFlags:I

.field private mNativeCallbackCookie:I

.field private mNativeHookInJavaObj:I

.field private mState:I

.field private mStateLock:Ljava/lang/Object;


# direct methods
.method public constructor <init>(II)V
    .registers 6
    .parameter "bufferSize"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 98
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 89
    const/16 v1, 0x1000

    #@6
    iput v1, p0, Landroid/media/AudioPlaybackHook;->mBufferSize:I

    #@8
    .line 91
    iput v2, p0, Landroid/media/AudioPlaybackHook;->mFlags:I

    #@a
    .line 93
    iput v2, p0, Landroid/media/AudioPlaybackHook;->mState:I

    #@c
    .line 95
    new-instance v1, Ljava/lang/Object;

    #@e
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@11
    iput-object v1, p0, Landroid/media/AudioPlaybackHook;->mStateLock:Ljava/lang/Object;

    #@13
    .line 99
    iput p1, p0, Landroid/media/AudioPlaybackHook;->mBufferSize:I

    #@15
    .line 100
    iput p2, p0, Landroid/media/AudioPlaybackHook;->mFlags:I

    #@17
    .line 101
    new-instance v1, Ljava/lang/ref/WeakReference;

    #@19
    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@1c
    invoke-direct {p0, v1, p1, p2}, Landroid/media/AudioPlaybackHook;->native_setup(Ljava/lang/Object;II)I

    #@1f
    move-result v0

    #@20
    .line 102
    .local v0, initResult:I
    if-eqz v0, :cond_3f

    #@22
    .line 103
    new-instance v1, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v2, "Error code "

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    const-string v2, " when initializing native AudioHook object."

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v1

    #@3b
    invoke-static {v1}, Landroid/media/AudioPlaybackHook;->loge(Ljava/lang/String;)V

    #@3e
    .line 110
    :goto_3e
    return-void

    #@3f
    .line 107
    :cond_3f
    iget-object v2, p0, Landroid/media/AudioPlaybackHook;->mStateLock:Ljava/lang/Object;

    #@41
    monitor-enter v2

    #@42
    .line 108
    const/4 v1, 0x1

    #@43
    :try_start_43
    iput v1, p0, Landroid/media/AudioPlaybackHook;->mState:I

    #@45
    .line 109
    monitor-exit v2

    #@46
    goto :goto_3e

    #@47
    :catchall_47
    move-exception v1

    #@48
    monitor-exit v2
    :try_end_49
    .catchall {:try_start_43 .. :try_end_49} :catchall_47

    #@49
    throw v1
.end method

.method public static native bufferSize(I)I
.end method

.method public static native getMixerOutput()I
.end method

.method public static native getMixerOutputFormat(I)I
.end method

.method public static native getMixerSampleRate(I)I
.end method

.method private static logd(Ljava/lang/String;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 203
    const-string v0, "AudioHook-Java"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[ android.media.AudioPlaybackHook ] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 204
    return-void
.end method

.method private static loge(Ljava/lang/String;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 207
    const-string v0, "AudioHook-Java"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[ android.media.AudioPlaybackHook ] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 208
    return-void
.end method

.method private final native native_finalize()V
.end method

.method private final native native_release()V
.end method

.method private final native native_setup(Ljava/lang/Object;II)I
.end method

.method private final native native_start()I
.end method

.method private final native native_stop()V
.end method


# virtual methods
.method protected finalize()V
    .registers 1

    #@0
    .prologue
    .line 129
    invoke-direct {p0}, Landroid/media/AudioPlaybackHook;->native_finalize()V

    #@3
    .line 130
    return-void
.end method

.method public getBufferSize()I
    .registers 2

    #@0
    .prologue
    .line 138
    iget v0, p0, Landroid/media/AudioPlaybackHook;->mBufferSize:I

    #@2
    return v0
.end method

.method public getFlags()I
    .registers 2

    #@0
    .prologue
    .line 143
    iget v0, p0, Landroid/media/AudioPlaybackHook;->mFlags:I

    #@2
    return v0
.end method

.method public getState()I
    .registers 2

    #@0
    .prologue
    .line 148
    iget v0, p0, Landroid/media/AudioPlaybackHook;->mState:I

    #@2
    return v0
.end method

.method public release()V
    .registers 3

    #@0
    .prologue
    .line 115
    :try_start_0
    invoke-virtual {p0}, Landroid/media/AudioPlaybackHook;->stop()V
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_3} :catch_11

    #@3
    .line 120
    :goto_3
    iget-object v1, p0, Landroid/media/AudioPlaybackHook;->mStateLock:Ljava/lang/Object;

    #@5
    monitor-enter v1

    #@6
    .line 121
    :try_start_6
    invoke-direct {p0}, Landroid/media/AudioPlaybackHook;->native_release()V

    #@9
    .line 122
    const/4 v0, 0x0

    #@a
    iput v0, p0, Landroid/media/AudioPlaybackHook;->mState:I

    #@c
    .line 123
    monitor-exit v1

    #@d
    .line 124
    return-void

    #@e
    .line 123
    :catchall_e
    move-exception v0

    #@f
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_6 .. :try_end_10} :catchall_e

    #@10
    throw v0

    #@11
    .line 116
    :catch_11
    move-exception v0

    #@12
    goto :goto_3
.end method

.method public start()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 163
    iget-object v1, p0, Landroid/media/AudioPlaybackHook;->mStateLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 164
    :try_start_3
    invoke-direct {p0}, Landroid/media/AudioPlaybackHook;->native_start()I

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_c

    #@9
    .line 165
    const/4 v0, 0x2

    #@a
    iput v0, p0, Landroid/media/AudioPlaybackHook;->mState:I

    #@c
    .line 167
    :cond_c
    monitor-exit v1

    #@d
    .line 168
    return-void

    #@e
    .line 167
    :catchall_e
    move-exception v0

    #@f
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    #@10
    throw v0
.end method

.method public stop()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 179
    iget-object v1, p0, Landroid/media/AudioPlaybackHook;->mStateLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 180
    :try_start_3
    invoke-direct {p0}, Landroid/media/AudioPlaybackHook;->native_stop()V

    #@6
    .line 181
    const/4 v0, 0x1

    #@7
    iput v0, p0, Landroid/media/AudioPlaybackHook;->mState:I

    #@9
    .line 182
    monitor-exit v1

    #@a
    .line 184
    return-void

    #@b
    .line 182
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method
