.class public Landroid/media/SoundPool;
.super Ljava/lang/Object;
.source "SoundPool.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/SoundPool$EventHandler;,
        Landroid/media/SoundPool$OnLoadCompleteListener;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final SAMPLE_LOADED:I = 0x1

.field private static final TAG:Ljava/lang/String; = "SoundPool"


# instance fields
.field private mEventHandler:Landroid/media/SoundPool$EventHandler;

.field private final mLock:Ljava/lang/Object;

.field private mNativeContext:I

.field private mOnLoadCompleteListener:Landroid/media/SoundPool$OnLoadCompleteListener;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 107
    const-string/jumbo v0, "soundpool"

    #@3
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@6
    return-void
.end method

.method public constructor <init>(III)V
    .registers 6
    .parameter "maxStreams"
    .parameter "streamType"
    .parameter "srcQuality"

    #@0
    .prologue
    .line 137
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 140
    new-instance v0, Ljava/lang/ref/WeakReference;

    #@5
    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@8
    invoke-direct {p0, v0, p1, p2, p3}, Landroid/media/SoundPool;->native_setup(Ljava/lang/Object;III)I

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_16

    #@e
    .line 141
    new-instance v0, Ljava/lang/RuntimeException;

    #@10
    const-string v1, "Native setup failed"

    #@12
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@15
    throw v0

    #@16
    .line 143
    :cond_16
    new-instance v0, Ljava/lang/Object;

    #@18
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@1b
    iput-object v0, p0, Landroid/media/SoundPool;->mLock:Ljava/lang/Object;

    #@1d
    .line 144
    return-void
.end method

.method private final native _load(Ljava/io/FileDescriptor;JJI)I
.end method

.method private final native _load(Ljava/lang/String;I)I
.end method

.method static synthetic access$000(Landroid/media/SoundPool;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 105
    iget-object v0, p0, Landroid/media/SoundPool;->mLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/media/SoundPool;)Landroid/media/SoundPool$OnLoadCompleteListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 105
    iget-object v0, p0, Landroid/media/SoundPool;->mOnLoadCompleteListener:Landroid/media/SoundPool$OnLoadCompleteListener;

    #@2
    return-object v0
.end method

.method private final native native_setup(Ljava/lang/Object;III)I
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .registers 8
    .parameter "weakRef"
    .parameter "msg"
    .parameter "arg1"
    .parameter "arg2"
    .parameter "obj"

    #@0
    .prologue
    .line 464
    check-cast p0, Ljava/lang/ref/WeakReference;

    #@2
    .end local p0
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Landroid/media/SoundPool;

    #@8
    .line 465
    .local v1, soundPool:Landroid/media/SoundPool;
    if-nez v1, :cond_b

    #@a
    .line 472
    :cond_a
    :goto_a
    return-void

    #@b
    .line 468
    :cond_b
    iget-object v2, v1, Landroid/media/SoundPool;->mEventHandler:Landroid/media/SoundPool$EventHandler;

    #@d
    if-eqz v2, :cond_a

    #@f
    .line 469
    iget-object v2, v1, Landroid/media/SoundPool;->mEventHandler:Landroid/media/SoundPool$EventHandler;

    #@11
    invoke-virtual {v2, p1, p2, p3, p4}, Landroid/media/SoundPool$EventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@14
    move-result-object v0

    #@15
    .line 470
    .local v0, m:Landroid/os/Message;
    iget-object v2, v1, Landroid/media/SoundPool;->mEventHandler:Landroid/media/SoundPool$EventHandler;

    #@17
    invoke-virtual {v2, v0}, Landroid/media/SoundPool$EventHandler;->sendMessage(Landroid/os/Message;)Z

    #@1a
    goto :goto_a
.end method


# virtual methods
.method public final native autoPause()V
.end method

.method public final native autoResume()V
.end method

.method protected finalize()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 492
    :try_start_0
    invoke-virtual {p0}, Landroid/media/SoundPool;->release()V
    :try_end_3
    .catchall {:try_start_0 .. :try_end_3} :catchall_7

    #@3
    .line 495
    :try_start_3
    invoke-super {p0}, Ljava/lang/Object;->finalize()V
    :try_end_6
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_3 .. :try_end_6} :catch_26

    #@6
    .line 500
    :goto_6
    return-void

    #@7
    .line 494
    :catchall_7
    move-exception v1

    #@8
    .line 495
    :try_start_8
    invoke-super {p0}, Ljava/lang/Object;->finalize()V
    :try_end_b
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_8 .. :try_end_b} :catch_c

    #@b
    .line 494
    :goto_b
    throw v1

    #@c
    .line 496
    :catch_c
    move-exception v0

    #@d
    .line 497
    .local v0, te:Ljava/util/concurrent/TimeoutException;
    const-string v2, "SoundPool"

    #@f
    new-instance v3, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v4, "Exception catched :"

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v3

    #@22
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    goto :goto_b

    #@26
    .line 496
    .end local v0           #te:Ljava/util/concurrent/TimeoutException;
    :catch_26
    move-exception v0

    #@27
    .line 497
    .restart local v0       #te:Ljava/util/concurrent/TimeoutException;
    const-string v1, "SoundPool"

    #@29
    new-instance v2, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v3, "Exception catched :"

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v2

    #@3c
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    goto :goto_6
.end method

.method public load(Landroid/content/Context;II)I
    .registers 13
    .parameter "context"
    .parameter "resId"
    .parameter "priority"

    #@0
    .prologue
    .line 191
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;

    #@7
    move-result-object v7

    #@8
    .line 192
    .local v7, afd:Landroid/content/res/AssetFileDescriptor;
    const/4 v8, 0x0

    #@9
    .line 193
    .local v8, id:I
    if-eqz v7, :cond_20

    #@b
    .line 194
    invoke-virtual {v7}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v7}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    #@12
    move-result-wide v2

    #@13
    invoke-virtual {v7}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    #@16
    move-result-wide v4

    #@17
    move-object v0, p0

    #@18
    move v6, p3

    #@19
    invoke-direct/range {v0 .. v6}, Landroid/media/SoundPool;->_load(Ljava/io/FileDescriptor;JJI)I

    #@1c
    move-result v8

    #@1d
    .line 196
    :try_start_1d
    invoke-virtual {v7}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_20
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_20} :catch_21

    #@20
    .line 201
    :cond_20
    :goto_20
    return v8

    #@21
    .line 197
    :catch_21
    move-exception v0

    #@22
    goto :goto_20
.end method

.method public load(Landroid/content/res/AssetFileDescriptor;I)I
    .registers 10
    .parameter "afd"
    .parameter "priority"

    #@0
    .prologue
    .line 213
    if-eqz p1, :cond_24

    #@2
    .line 214
    invoke-virtual {p1}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    #@5
    move-result-wide v4

    #@6
    .line 215
    .local v4, len:J
    const-wide/16 v0, 0x0

    #@8
    cmp-long v0, v4, v0

    #@a
    if-gez v0, :cond_15

    #@c
    .line 216
    new-instance v0, Landroid/util/AndroidRuntimeException;

    #@e
    const-string/jumbo v1, "no length for fd"

    #@11
    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    #@14
    throw v0

    #@15
    .line 218
    :cond_15
    invoke-virtual {p1}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {p1}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    #@1c
    move-result-wide v2

    #@1d
    move-object v0, p0

    #@1e
    move v6, p2

    #@1f
    invoke-direct/range {v0 .. v6}, Landroid/media/SoundPool;->_load(Ljava/io/FileDescriptor;JJI)I

    #@22
    move-result v0

    #@23
    .line 220
    .end local v4           #len:J
    :goto_23
    return v0

    #@24
    :cond_24
    const/4 v0, 0x0

    #@25
    goto :goto_23
.end method

.method public load(Ljava/io/FileDescriptor;JJI)I
    .registers 8
    .parameter "fd"
    .parameter "offset"
    .parameter "length"
    .parameter "priority"

    #@0
    .prologue
    .line 239
    invoke-direct/range {p0 .. p6}, Landroid/media/SoundPool;->_load(Ljava/io/FileDescriptor;JJI)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public load(Ljava/lang/String;I)I
    .registers 14
    .parameter "path"
    .parameter "priority"

    #@0
    .prologue
    .line 157
    const-string v0, "http:"

    #@2
    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_d

    #@8
    .line 158
    invoke-direct {p0, p1, p2}, Landroid/media/SoundPool;->_load(Ljava/lang/String;I)I

    #@b
    move-result v10

    #@c
    .line 172
    :cond_c
    :goto_c
    return v10

    #@d
    .line 161
    :cond_d
    const/4 v10, 0x0

    #@e
    .line 163
    .local v10, id:I
    :try_start_e
    new-instance v8, Ljava/io/File;

    #@10
    invoke-direct {v8, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@13
    .line 164
    .local v8, f:Ljava/io/File;
    const/high16 v0, 0x1000

    #@15
    invoke-static {v8, v0}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    #@18
    move-result-object v9

    #@19
    .line 165
    .local v9, fd:Landroid/os/ParcelFileDescriptor;
    if-eqz v9, :cond_c

    #@1b
    .line 166
    invoke-virtual {v9}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@1e
    move-result-object v1

    #@1f
    const-wide/16 v2, 0x0

    #@21
    invoke-virtual {v8}, Ljava/io/File;->length()J

    #@24
    move-result-wide v4

    #@25
    move-object v0, p0

    #@26
    move v6, p2

    #@27
    invoke-direct/range {v0 .. v6}, Landroid/media/SoundPool;->_load(Ljava/io/FileDescriptor;JJI)I

    #@2a
    move-result v10

    #@2b
    .line 167
    invoke-virtual {v9}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_2e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_2e} :catch_2f

    #@2e
    goto :goto_c

    #@2f
    .line 169
    .end local v8           #f:Ljava/io/File;
    .end local v9           #fd:Landroid/os/ParcelFileDescriptor;
    :catch_2f
    move-exception v7

    #@30
    .line 170
    .local v7, e:Ljava/io/IOException;
    const-string v0, "SoundPool"

    #@32
    new-instance v1, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v2, "error loading "

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v1

    #@45
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    goto :goto_c
.end method

.method public final native pause(I)V
.end method

.method public final native play(IFFIIF)I
.end method

.method public final native release()V
.end method

.method public final native resume(I)V
.end method

.method public final native setLoop(II)V
.end method

.method public setOnLoadCompleteListener(Landroid/media/SoundPool$OnLoadCompleteListener;)V
    .registers 5
    .parameter "listener"

    #@0
    .prologue
    .line 416
    iget-object v2, p0, Landroid/media/SoundPool;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 417
    if-eqz p1, :cond_2b

    #@5
    .line 420
    :try_start_5
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@8
    move-result-object v0

    #@9
    .local v0, looper:Landroid/os/Looper;
    if-eqz v0, :cond_16

    #@b
    .line 421
    new-instance v1, Landroid/media/SoundPool$EventHandler;

    #@d
    invoke-direct {v1, p0, p0, v0}, Landroid/media/SoundPool$EventHandler;-><init>(Landroid/media/SoundPool;Landroid/media/SoundPool;Landroid/os/Looper;)V

    #@10
    iput-object v1, p0, Landroid/media/SoundPool;->mEventHandler:Landroid/media/SoundPool$EventHandler;

    #@12
    .line 430
    .end local v0           #looper:Landroid/os/Looper;
    :goto_12
    iput-object p1, p0, Landroid/media/SoundPool;->mOnLoadCompleteListener:Landroid/media/SoundPool$OnLoadCompleteListener;

    #@14
    .line 431
    monitor-exit v2

    #@15
    .line 432
    return-void

    #@16
    .line 422
    .restart local v0       #looper:Landroid/os/Looper;
    :cond_16
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    #@19
    move-result-object v0

    #@1a
    if-eqz v0, :cond_27

    #@1c
    .line 423
    new-instance v1, Landroid/media/SoundPool$EventHandler;

    #@1e
    invoke-direct {v1, p0, p0, v0}, Landroid/media/SoundPool$EventHandler;-><init>(Landroid/media/SoundPool;Landroid/media/SoundPool;Landroid/os/Looper;)V

    #@21
    iput-object v1, p0, Landroid/media/SoundPool;->mEventHandler:Landroid/media/SoundPool$EventHandler;

    #@23
    goto :goto_12

    #@24
    .line 431
    .end local v0           #looper:Landroid/os/Looper;
    :catchall_24
    move-exception v1

    #@25
    monitor-exit v2
    :try_end_26
    .catchall {:try_start_5 .. :try_end_26} :catchall_24

    #@26
    throw v1

    #@27
    .line 425
    .restart local v0       #looper:Landroid/os/Looper;
    :cond_27
    const/4 v1, 0x0

    #@28
    :try_start_28
    iput-object v1, p0, Landroid/media/SoundPool;->mEventHandler:Landroid/media/SoundPool$EventHandler;

    #@2a
    goto :goto_12

    #@2b
    .line 428
    .end local v0           #looper:Landroid/os/Looper;
    :cond_2b
    const/4 v1, 0x0

    #@2c
    iput-object v1, p0, Landroid/media/SoundPool;->mEventHandler:Landroid/media/SoundPool$EventHandler;
    :try_end_2e
    .catchall {:try_start_28 .. :try_end_2e} :catchall_24

    #@2e
    goto :goto_12
.end method

.method public final native setPriority(II)V
.end method

.method public final native setRate(IF)V
.end method

.method public final native setVolume(IFF)V
.end method

.method public final native stop(I)V
.end method

.method public final native unload(I)Z
.end method
