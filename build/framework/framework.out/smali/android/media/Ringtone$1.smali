.class Landroid/media/Ringtone$1;
.super Ljava/lang/Object;
.source "Ringtone.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/Ringtone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/media/Ringtone;


# direct methods
.method constructor <init>(Landroid/media/Ringtone;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 768
    iput-object p1, p0, Landroid/media/Ringtone$1;->this$0:Landroid/media/Ringtone;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .registers 11
    .parameter "mp"
    .parameter "what"
    .parameter "extra"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 770
    packed-switch p2, :pswitch_data_e6

    #@4
    .line 778
    iget-object v4, p0, Landroid/media/Ringtone$1;->this$0:Landroid/media/Ringtone;

    #@6
    invoke-static {v4}, Landroid/media/Ringtone;->access$100(Landroid/media/Ringtone;)Landroid/media/MediaPlayer;

    #@9
    move-result-object v4

    #@a
    if-eqz v4, :cond_9d

    #@c
    .line 779
    iget-object v4, p0, Landroid/media/Ringtone$1;->this$0:Landroid/media/Ringtone;

    #@e
    invoke-static {v4}, Landroid/media/Ringtone;->access$000(Landroid/media/Ringtone;)Z

    #@11
    move-result v4

    #@12
    if-nez v4, :cond_7f

    #@14
    .line 780
    const-string v4, "Ringtone"

    #@16
    const-string v5, "LocalPlayer play default ringtone."

    #@18
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 781
    iget-object v4, p0, Landroid/media/Ringtone$1;->this$0:Landroid/media/Ringtone;

    #@1d
    invoke-static {v4, v3}, Landroid/media/Ringtone;->access$002(Landroid/media/Ringtone;Z)Z

    #@20
    .line 782
    iget-object v3, p0, Landroid/media/Ringtone$1;->this$0:Landroid/media/Ringtone;

    #@22
    invoke-static {v3}, Landroid/media/Ringtone;->access$100(Landroid/media/Ringtone;)Landroid/media/MediaPlayer;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Landroid/media/MediaPlayer;->release()V

    #@29
    .line 783
    iget-object v3, p0, Landroid/media/Ringtone$1;->this$0:Landroid/media/Ringtone;

    #@2b
    const/4 v4, 0x0

    #@2c
    invoke-static {v3, v4}, Landroid/media/Ringtone;->access$102(Landroid/media/Ringtone;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;

    #@2f
    .line 785
    iget-object v3, p0, Landroid/media/Ringtone$1;->this$0:Landroid/media/Ringtone;

    #@31
    invoke-static {v3}, Landroid/media/Ringtone;->access$200(Landroid/media/Ringtone;)Ljava/lang/String;

    #@34
    move-result-object v0

    #@35
    .line 786
    .local v0, defaultPath:Ljava/lang/String;
    const-string v3, "Ringtone"

    #@37
    new-instance v4, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v5, "[LGE] OnErrorListener... set Default Ring... = "

    #@3e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v4

    #@42
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v4

    #@46
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v4

    #@4a
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    .line 787
    if-eqz v0, :cond_7f

    #@4f
    .line 788
    iget-object v3, p0, Landroid/media/Ringtone$1;->this$0:Landroid/media/Ringtone;

    #@51
    new-instance v4, Landroid/media/MediaPlayer;

    #@53
    invoke-direct {v4}, Landroid/media/MediaPlayer;-><init>()V

    #@56
    invoke-static {v3, v4}, Landroid/media/Ringtone;->access$102(Landroid/media/Ringtone;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;

    #@59
    .line 790
    :try_start_59
    iget-object v3, p0, Landroid/media/Ringtone$1;->this$0:Landroid/media/Ringtone;

    #@5b
    invoke-static {v3}, Landroid/media/Ringtone;->access$100(Landroid/media/Ringtone;)Landroid/media/MediaPlayer;

    #@5e
    move-result-object v3

    #@5f
    invoke-virtual {v3, v0}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    #@62
    .line 791
    iget-object v3, p0, Landroid/media/Ringtone$1;->this$0:Landroid/media/Ringtone;

    #@64
    invoke-static {v3}, Landroid/media/Ringtone;->access$100(Landroid/media/Ringtone;)Landroid/media/MediaPlayer;

    #@67
    move-result-object v3

    #@68
    iget-object v4, p0, Landroid/media/Ringtone$1;->this$0:Landroid/media/Ringtone;

    #@6a
    invoke-static {v4}, Landroid/media/Ringtone;->access$300(Landroid/media/Ringtone;)I

    #@6d
    move-result v4

    #@6e
    invoke-virtual {v3, v4}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    #@71
    .line 792
    iget-object v3, p0, Landroid/media/Ringtone$1;->this$0:Landroid/media/Ringtone;

    #@73
    invoke-static {v3}, Landroid/media/Ringtone;->access$100(Landroid/media/Ringtone;)Landroid/media/MediaPlayer;

    #@76
    move-result-object v3

    #@77
    invoke-virtual {v3}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_7a
    .catch Ljava/io/IOException; {:try_start_59 .. :try_end_7a} :catch_94

    #@7a
    .line 796
    :goto_7a
    iget-object v3, p0, Landroid/media/Ringtone$1;->this$0:Landroid/media/Ringtone;

    #@7c
    invoke-virtual {v3}, Landroid/media/Ringtone;->play()V

    #@7f
    .line 809
    .end local v0           #defaultPath:Ljava/lang/String;
    :cond_7f
    :goto_7f
    const/4 v3, 0x0

    #@80
    :cond_80
    :goto_80
    return v3

    #@81
    .line 772
    :pswitch_81
    iget-object v4, p0, Landroid/media/Ringtone$1;->this$0:Landroid/media/Ringtone;

    #@83
    invoke-static {v4}, Landroid/media/Ringtone;->access$000(Landroid/media/Ringtone;)Z

    #@86
    move-result v4

    #@87
    if-nez v4, :cond_80

    #@89
    .line 773
    iget-object v4, p0, Landroid/media/Ringtone$1;->this$0:Landroid/media/Ringtone;

    #@8b
    invoke-static {v4, v3}, Landroid/media/Ringtone;->access$002(Landroid/media/Ringtone;Z)Z

    #@8e
    .line 774
    iget-object v4, p0, Landroid/media/Ringtone$1;->this$0:Landroid/media/Ringtone;

    #@90
    invoke-virtual {v4}, Landroid/media/Ringtone;->stop()V

    #@93
    goto :goto_80

    #@94
    .line 793
    .restart local v0       #defaultPath:Ljava/lang/String;
    :catch_94
    move-exception v2

    #@95
    .line 794
    .local v2, ex:Ljava/io/IOException;
    const-string v3, "Ringtone"

    #@97
    const-string v4, "[LGE] default filepath is not set"

    #@99
    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@9c
    goto :goto_7a

    #@9d
    .line 799
    .end local v0           #defaultPath:Ljava/lang/String;
    .end local v2           #ex:Ljava/io/IOException;
    :cond_9d
    iget-object v3, p0, Landroid/media/Ringtone$1;->this$0:Landroid/media/Ringtone;

    #@9f
    invoke-static {v3}, Landroid/media/Ringtone;->access$400(Landroid/media/Ringtone;)Z

    #@a2
    move-result v3

    #@a3
    if-eqz v3, :cond_7f

    #@a5
    .line 801
    :try_start_a5
    const-string v3, "Ringtone"

    #@a7
    const-string v4, "RemotePlayer play default ringtone."

    #@a9
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ac
    .line 802
    iget-object v3, p0, Landroid/media/Ringtone$1;->this$0:Landroid/media/Ringtone;

    #@ae
    invoke-static {v3}, Landroid/media/Ringtone;->access$600(Landroid/media/Ringtone;)Landroid/media/IRingtonePlayer;

    #@b1
    move-result-object v3

    #@b2
    iget-object v4, p0, Landroid/media/Ringtone$1;->this$0:Landroid/media/Ringtone;

    #@b4
    invoke-static {v4}, Landroid/media/Ringtone;->access$500(Landroid/media/Ringtone;)Landroid/os/Binder;

    #@b7
    move-result-object v4

    #@b8
    iget-object v5, p0, Landroid/media/Ringtone$1;->this$0:Landroid/media/Ringtone;

    #@ba
    invoke-static {v5}, Landroid/media/Ringtone;->access$200(Landroid/media/Ringtone;)Ljava/lang/String;

    #@bd
    move-result-object v5

    #@be
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@c1
    move-result-object v5

    #@c2
    iget-object v6, p0, Landroid/media/Ringtone$1;->this$0:Landroid/media/Ringtone;

    #@c4
    invoke-static {v6}, Landroid/media/Ringtone;->access$300(Landroid/media/Ringtone;)I

    #@c7
    move-result v6

    #@c8
    invoke-interface {v3, v4, v5, v6}, Landroid/media/IRingtonePlayer;->play(Landroid/os/IBinder;Landroid/net/Uri;I)V
    :try_end_cb
    .catch Landroid/os/RemoteException; {:try_start_a5 .. :try_end_cb} :catch_cc

    #@cb
    goto :goto_7f

    #@cc
    .line 803
    :catch_cc
    move-exception v1

    #@cd
    .line 804
    .local v1, e:Landroid/os/RemoteException;
    const-string v3, "Ringtone"

    #@cf
    new-instance v4, Ljava/lang/StringBuilder;

    #@d1
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@d4
    const-string v5, "Problem playing default ringtone: "

    #@d6
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v4

    #@da
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v4

    #@de
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e1
    move-result-object v4

    #@e2
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e5
    goto :goto_7f

    #@e6
    .line 770
    :pswitch_data_e6
    .packed-switch 0x64
        :pswitch_81
    .end packed-switch
.end method
