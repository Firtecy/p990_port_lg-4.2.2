.class Landroid/media/JetPlayer$NativeEventHandler;
.super Landroid/os/Handler;
.source "JetPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/JetPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NativeEventHandler"
.end annotation


# instance fields
.field private mJet:Landroid/media/JetPlayer;

.field final synthetic this$0:Landroid/media/JetPlayer;


# direct methods
.method public constructor <init>(Landroid/media/JetPlayer;Landroid/media/JetPlayer;Landroid/os/Looper;)V
    .registers 4
    .parameter
    .parameter "jet"
    .parameter "looper"

    #@0
    .prologue
    .line 400
    iput-object p1, p0, Landroid/media/JetPlayer$NativeEventHandler;->this$0:Landroid/media/JetPlayer;

    #@2
    .line 401
    invoke-direct {p0, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 402
    iput-object p2, p0, Landroid/media/JetPlayer$NativeEventHandler;->mJet:Landroid/media/JetPlayer;

    #@7
    .line 403
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 10
    .parameter "msg"

    #@0
    .prologue
    .line 407
    const/4 v7, 0x0

    #@1
    .line 408
    .local v7, listener:Landroid/media/JetPlayer$OnJetEventListener;
    iget-object v0, p0, Landroid/media/JetPlayer$NativeEventHandler;->this$0:Landroid/media/JetPlayer;

    #@3
    invoke-static {v0}, Landroid/media/JetPlayer;->access$000(Landroid/media/JetPlayer;)Ljava/lang/Object;

    #@6
    move-result-object v1

    #@7
    monitor-enter v1

    #@8
    .line 409
    :try_start_8
    iget-object v0, p0, Landroid/media/JetPlayer$NativeEventHandler;->mJet:Landroid/media/JetPlayer;

    #@a
    invoke-static {v0}, Landroid/media/JetPlayer;->access$100(Landroid/media/JetPlayer;)Landroid/media/JetPlayer$OnJetEventListener;

    #@d
    move-result-object v7

    #@e
    .line 410
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_8 .. :try_end_f} :catchall_2d

    #@f
    .line 411
    iget v0, p1, Landroid/os/Message;->what:I

    #@11
    packed-switch v0, :pswitch_data_86

    #@14
    .line 443
    new-instance v0, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v1, "Unknown message type "

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    iget v1, p1, Landroid/os/Message;->what:I

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    invoke-static {v0}, Landroid/media/JetPlayer;->access$200(Ljava/lang/String;)V

    #@2c
    .line 444
    :cond_2c
    :goto_2c
    return-void

    #@2d
    .line 410
    :catchall_2d
    move-exception v0

    #@2e
    :try_start_2e
    monitor-exit v1
    :try_end_2f
    .catchall {:try_start_2e .. :try_end_2f} :catchall_2d

    #@2f
    throw v0

    #@30
    .line 413
    :pswitch_30
    if-eqz v7, :cond_2c

    #@32
    .line 416
    iget-object v0, p0, Landroid/media/JetPlayer$NativeEventHandler;->this$0:Landroid/media/JetPlayer;

    #@34
    invoke-static {v0}, Landroid/media/JetPlayer;->access$100(Landroid/media/JetPlayer;)Landroid/media/JetPlayer$OnJetEventListener;

    #@37
    move-result-object v0

    #@38
    iget-object v1, p0, Landroid/media/JetPlayer$NativeEventHandler;->mJet:Landroid/media/JetPlayer;

    #@3a
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@3c
    const/high16 v3, -0x100

    #@3e
    and-int/2addr v2, v3

    #@3f
    shr-int/lit8 v2, v2, 0x18

    #@41
    int-to-short v2, v2

    #@42
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@44
    const/high16 v4, 0xfc

    #@46
    and-int/2addr v3, v4

    #@47
    shr-int/lit8 v3, v3, 0x12

    #@49
    int-to-byte v3, v3

    #@4a
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@4c
    const v5, 0x3c000

    #@4f
    and-int/2addr v4, v5

    #@50
    shr-int/lit8 v4, v4, 0xe

    #@52
    add-int/lit8 v4, v4, 0x1

    #@54
    int-to-byte v4, v4

    #@55
    iget v5, p1, Landroid/os/Message;->arg1:I

    #@57
    and-int/lit16 v5, v5, 0x3f80

    #@59
    shr-int/lit8 v5, v5, 0x7

    #@5b
    int-to-byte v5, v5

    #@5c
    iget v6, p1, Landroid/os/Message;->arg1:I

    #@5e
    and-int/lit8 v6, v6, 0x7f

    #@60
    int-to-byte v6, v6

    #@61
    invoke-interface/range {v0 .. v6}, Landroid/media/JetPlayer$OnJetEventListener;->onJetEvent(Landroid/media/JetPlayer;SBBBB)V

    #@64
    goto :goto_2c

    #@65
    .line 428
    :pswitch_65
    if-eqz v7, :cond_2c

    #@67
    .line 429
    iget-object v0, p0, Landroid/media/JetPlayer$NativeEventHandler;->mJet:Landroid/media/JetPlayer;

    #@69
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@6b
    iget v2, p1, Landroid/os/Message;->arg2:I

    #@6d
    invoke-interface {v7, v0, v1, v2}, Landroid/media/JetPlayer$OnJetEventListener;->onJetUserIdUpdate(Landroid/media/JetPlayer;II)V

    #@70
    goto :goto_2c

    #@71
    .line 433
    :pswitch_71
    if-eqz v7, :cond_2c

    #@73
    .line 434
    iget-object v0, p0, Landroid/media/JetPlayer$NativeEventHandler;->mJet:Landroid/media/JetPlayer;

    #@75
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@77
    invoke-interface {v7, v0, v1}, Landroid/media/JetPlayer$OnJetEventListener;->onJetNumQueuedSegmentUpdate(Landroid/media/JetPlayer;I)V

    #@7a
    goto :goto_2c

    #@7b
    .line 438
    :pswitch_7b
    if-eqz v7, :cond_2c

    #@7d
    .line 439
    iget-object v0, p0, Landroid/media/JetPlayer$NativeEventHandler;->mJet:Landroid/media/JetPlayer;

    #@7f
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@81
    invoke-interface {v7, v0, v1}, Landroid/media/JetPlayer$OnJetEventListener;->onJetPauseUpdate(Landroid/media/JetPlayer;I)V

    #@84
    goto :goto_2c

    #@85
    .line 411
    nop

    #@86
    :pswitch_data_86
    .packed-switch 0x1
        :pswitch_30
        :pswitch_65
        :pswitch_71
        :pswitch_7b
    .end packed-switch
.end method
