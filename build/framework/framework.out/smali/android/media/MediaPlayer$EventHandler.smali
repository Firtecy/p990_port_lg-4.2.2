.class Landroid/media/MediaPlayer$EventHandler;
.super Landroid/os/Handler;
.source "MediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/MediaPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventHandler"
.end annotation


# instance fields
.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field final synthetic this$0:Landroid/media/MediaPlayer;


# direct methods
.method public constructor <init>(Landroid/media/MediaPlayer;Landroid/media/MediaPlayer;Landroid/os/Looper;)V
    .registers 4
    .parameter
    .parameter "mp"
    .parameter "looper"

    #@0
    .prologue
    .line 2192
    iput-object p1, p0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@2
    .line 2193
    invoke-direct {p0, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 2194
    iput-object p2, p0, Landroid/media/MediaPlayer$EventHandler;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@7
    .line 2195
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 20
    .parameter "msg"

    #@0
    .prologue
    .line 2199
    move-object/from16 v0, p0

    #@2
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@4
    invoke-static {v2}, Landroid/media/MediaPlayer;->access$000(Landroid/media/MediaPlayer;)I

    #@7
    move-result v2

    #@8
    if-nez v2, :cond_13

    #@a
    .line 2200
    const-string v2, "MediaPlayer[JAVA]"

    #@c
    const-string/jumbo v4, "mediaplayer went away with unhandled events"

    #@f
    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 2407
    :cond_12
    :goto_12
    :sswitch_12
    return-void

    #@13
    .line 2203
    :cond_13
    move-object/from16 v0, p1

    #@15
    iget v2, v0, Landroid/os/Message;->what:I

    #@17
    sparse-switch v2, :sswitch_data_424

    #@1a
    .line 2404
    const-string v2, "MediaPlayer[JAVA]"

    #@1c
    new-instance v4, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v5, "Unknown message type "

    #@23
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v4

    #@27
    move-object/from16 v0, p1

    #@29
    iget v5, v0, Landroid/os/Message;->what:I

    #@2b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v4

    #@33
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    goto :goto_12

    #@37
    .line 2205
    :sswitch_37
    move-object/from16 v0, p0

    #@39
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@3b
    invoke-static {v2}, Landroid/media/MediaPlayer;->access$100(Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnPreparedListener;

    #@3e
    move-result-object v2

    #@3f
    if-eqz v2, :cond_12

    #@41
    .line 2206
    move-object/from16 v0, p0

    #@43
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@45
    invoke-static {v2}, Landroid/media/MediaPlayer;->access$100(Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnPreparedListener;

    #@48
    move-result-object v2

    #@49
    move-object/from16 v0, p0

    #@4b
    iget-object v4, v0, Landroid/media/MediaPlayer$EventHandler;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@4d
    invoke-interface {v2, v4}, Landroid/media/MediaPlayer$OnPreparedListener;->onPrepared(Landroid/media/MediaPlayer;)V

    #@50
    goto :goto_12

    #@51
    .line 2210
    :sswitch_51
    move-object/from16 v0, p0

    #@53
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@55
    invoke-static {v2}, Landroid/media/MediaPlayer;->access$200(Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnCompletionListener;

    #@58
    move-result-object v2

    #@59
    if-eqz v2, :cond_6a

    #@5b
    .line 2211
    move-object/from16 v0, p0

    #@5d
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@5f
    invoke-static {v2}, Landroid/media/MediaPlayer;->access$200(Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnCompletionListener;

    #@62
    move-result-object v2

    #@63
    move-object/from16 v0, p0

    #@65
    iget-object v4, v0, Landroid/media/MediaPlayer$EventHandler;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@67
    invoke-interface {v2, v4}, Landroid/media/MediaPlayer$OnCompletionListener;->onCompletion(Landroid/media/MediaPlayer;)V

    #@6a
    .line 2212
    :cond_6a
    move-object/from16 v0, p0

    #@6c
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@6e
    const/4 v4, 0x0

    #@6f
    invoke-static {v2, v4}, Landroid/media/MediaPlayer;->access$300(Landroid/media/MediaPlayer;Z)V

    #@72
    goto :goto_12

    #@73
    .line 2216
    :sswitch_73
    move-object/from16 v0, p0

    #@75
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@77
    invoke-static {v2}, Landroid/media/MediaPlayer;->access$400(Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    #@7a
    move-result-object v2

    #@7b
    if-eqz v2, :cond_12

    #@7d
    .line 2217
    move-object/from16 v0, p0

    #@7f
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@81
    invoke-static {v2}, Landroid/media/MediaPlayer;->access$400(Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    #@84
    move-result-object v2

    #@85
    move-object/from16 v0, p0

    #@87
    iget-object v4, v0, Landroid/media/MediaPlayer$EventHandler;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@89
    move-object/from16 v0, p1

    #@8b
    iget v5, v0, Landroid/os/Message;->arg1:I

    #@8d
    invoke-interface {v2, v4, v5}, Landroid/media/MediaPlayer$OnBufferingUpdateListener;->onBufferingUpdate(Landroid/media/MediaPlayer;I)V

    #@90
    goto :goto_12

    #@91
    .line 2223
    :sswitch_91
    move-object/from16 v0, p0

    #@93
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@95
    invoke-static {v2}, Landroid/media/MediaPlayer;->access$500(Landroid/media/MediaPlayer;)Landroid/content/Context;

    #@98
    move-result-object v2

    #@99
    if-eqz v2, :cond_11a

    #@9b
    .line 2225
    const-string v2, "MediaPlayer[JAVA]"

    #@9d
    const-string v4, "broadcasting MEDIA_SEEK_COMPLETE "

    #@9f
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a2
    .line 2227
    new-instance v3, Landroid/content/Intent;

    #@a4
    const-string v2, "com.lge.intent.action.MEDIA_PLAYER_STATE_CHANGED"

    #@a6
    invoke-direct {v3, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@a9
    .line 2228
    .local v3, intent:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@ab
    iget-object v4, v0, Landroid/media/MediaPlayer$EventHandler;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@ad
    monitor-enter v4

    #@ae
    .line 2229
    :try_start_ae
    move-object/from16 v0, p0

    #@b0
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@b2
    invoke-static {v2}, Landroid/media/MediaPlayer;->access$600(Landroid/media/MediaPlayer;)I

    #@b5
    move-result v16

    #@b6
    .line 2231
    .local v16, state:I
    const-string v2, "com.lge.intent.extra.MEDIA_PLAYER_URI"

    #@b8
    move-object/from16 v0, p0

    #@ba
    iget-object v5, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@bc
    invoke-static {v5}, Landroid/media/MediaPlayer;->access$700(Landroid/media/MediaPlayer;)Ljava/lang/String;

    #@bf
    move-result-object v5

    #@c0
    invoke-virtual {v3, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@c3
    .line 2232
    const-string v2, "com.lge.intent.extra.MEDIA_PLAYER_STATE"

    #@c5
    const/16 v5, 0x200

    #@c7
    invoke-virtual {v3, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@ca
    .line 2234
    move/from16 v0, v16

    #@cc
    and-int/lit16 v2, v0, 0xfe

    #@ce
    if-lez v2, :cond_dd

    #@d0
    .line 2236
    const-string v2, "com.lge.intent.extra.MEDIA_PLAYER_CURRENT_POSITION"

    #@d2
    move-object/from16 v0, p0

    #@d4
    iget-object v5, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@d6
    invoke-virtual {v5}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    #@d9
    move-result v5

    #@da
    invoke-virtual {v3, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@dd
    .line 2238
    :cond_dd
    move/from16 v0, v16

    #@df
    and-int/lit16 v2, v0, 0xb8

    #@e1
    if-lez v2, :cond_f0

    #@e3
    .line 2240
    const-string v2, "com.lge.intent.extra.MEDIA_PLAYER_TOTAL_DURATION"

    #@e5
    move-object/from16 v0, p0

    #@e7
    iget-object v5, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@e9
    invoke-virtual {v5}, Landroid/media/MediaPlayer;->getDuration()I

    #@ec
    move-result v5

    #@ed
    invoke-virtual {v3, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@f0
    .line 2242
    :cond_f0
    monitor-exit v4
    :try_end_f1
    .catchall {:try_start_ae .. :try_end_f1} :catchall_117

    #@f1
    .line 2244
    move-object/from16 v0, p0

    #@f3
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@f5
    invoke-static {v2}, Landroid/media/MediaPlayer;->access$500(Landroid/media/MediaPlayer;)Landroid/content/Context;

    #@f8
    move-result-object v2

    #@f9
    invoke-virtual {v2, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@fc
    .line 2287
    :goto_fc
    move-object/from16 v0, p0

    #@fe
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@100
    invoke-static {v2}, Landroid/media/MediaPlayer;->access$800(Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnSeekCompleteListener;

    #@103
    move-result-object v2

    #@104
    if-eqz v2, :cond_12

    #@106
    .line 2288
    move-object/from16 v0, p0

    #@108
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@10a
    invoke-static {v2}, Landroid/media/MediaPlayer;->access$800(Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnSeekCompleteListener;

    #@10d
    move-result-object v2

    #@10e
    move-object/from16 v0, p0

    #@110
    iget-object v4, v0, Landroid/media/MediaPlayer$EventHandler;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@112
    invoke-interface {v2, v4}, Landroid/media/MediaPlayer$OnSeekCompleteListener;->onSeekComplete(Landroid/media/MediaPlayer;)V

    #@115
    goto/16 :goto_12

    #@117
    .line 2242
    .end local v16           #state:I
    :catchall_117
    move-exception v2

    #@118
    :try_start_118
    monitor-exit v4
    :try_end_119
    .catchall {:try_start_118 .. :try_end_119} :catchall_117

    #@119
    throw v2

    #@11a
    .line 2249
    .end local v3           #intent:Landroid/content/Intent;
    :cond_11a
    const-string v2, "MediaPlayer[JAVA]"

    #@11c
    const-string v4, "broadcasting MEDIA_SEEK_COMPLETE "

    #@11e
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@121
    .line 2251
    new-instance v3, Landroid/content/Intent;

    #@123
    const-string v2, "com.lge.intent.action.MEDIA_PLAYER_STATE_CHANGED"

    #@125
    invoke-direct {v3, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@128
    .line 2252
    .restart local v3       #intent:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@12a
    iget-object v4, v0, Landroid/media/MediaPlayer$EventHandler;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@12c
    monitor-enter v4

    #@12d
    .line 2253
    :try_start_12d
    move-object/from16 v0, p0

    #@12f
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@131
    invoke-static {v2}, Landroid/media/MediaPlayer;->access$600(Landroid/media/MediaPlayer;)I

    #@134
    move-result v16

    #@135
    .line 2255
    .restart local v16       #state:I
    const-string v2, "com.lge.intent.extra.MEDIA_PLAYER_URI"

    #@137
    move-object/from16 v0, p0

    #@139
    iget-object v5, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@13b
    invoke-static {v5}, Landroid/media/MediaPlayer;->access$700(Landroid/media/MediaPlayer;)Ljava/lang/String;

    #@13e
    move-result-object v5

    #@13f
    invoke-virtual {v3, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@142
    .line 2256
    const-string v2, "com.lge.intent.extra.MEDIA_PLAYER_STATE"

    #@144
    const/16 v5, 0x200

    #@146
    invoke-virtual {v3, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@149
    .line 2258
    move/from16 v0, v16

    #@14b
    and-int/lit16 v2, v0, 0xfe

    #@14d
    if-lez v2, :cond_15c

    #@14f
    .line 2260
    const-string v2, "com.lge.intent.extra.MEDIA_PLAYER_CURRENT_POSITION"

    #@151
    move-object/from16 v0, p0

    #@153
    iget-object v5, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@155
    invoke-virtual {v5}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    #@158
    move-result v5

    #@159
    invoke-virtual {v3, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@15c
    .line 2262
    :cond_15c
    move/from16 v0, v16

    #@15e
    and-int/lit16 v2, v0, 0xb8

    #@160
    if-lez v2, :cond_16f

    #@162
    .line 2264
    const-string v2, "com.lge.intent.extra.MEDIA_PLAYER_TOTAL_DURATION"

    #@164
    move-object/from16 v0, p0

    #@166
    iget-object v5, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@168
    invoke-virtual {v5}, Landroid/media/MediaPlayer;->getDuration()I

    #@16b
    move-result v5

    #@16c
    invoke-virtual {v3, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@16f
    .line 2266
    :cond_16f
    monitor-exit v4
    :try_end_170
    .catchall {:try_start_12d .. :try_end_170} :catchall_193

    #@170
    .line 2269
    const/4 v2, 0x0

    #@171
    :try_start_171
    invoke-virtual {v3, v2}, Landroid/content/Intent;->setAllowFds(Z)V

    #@174
    .line 2270
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@177
    move-result-object v1

    #@178
    .line 2271
    .local v1, myManager:Landroid/app/IActivityManager;
    if-eqz v1, :cond_196

    #@17a
    .line 2273
    const/4 v2, 0x0

    #@17b
    const/4 v4, 0x0

    #@17c
    const/4 v5, 0x0

    #@17d
    const/4 v6, -0x1

    #@17e
    const/4 v7, 0x0

    #@17f
    const/4 v8, 0x0

    #@180
    const/4 v9, 0x0

    #@181
    const/4 v10, 0x0

    #@182
    const/4 v11, 0x0

    #@183
    const/4 v12, -0x3

    #@184
    invoke-interface/range {v1 .. v12}, Landroid/app/IActivityManager;->broadcastIntent(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ZZI)I
    :try_end_187
    .catch Landroid/os/RemoteException; {:try_start_171 .. :try_end_187} :catch_189

    #@187
    goto/16 :goto_fc

    #@189
    .line 2280
    .end local v1           #myManager:Landroid/app/IActivityManager;
    :catch_189
    move-exception v13

    #@18a
    .line 2281
    .local v13, e:Landroid/os/RemoteException;
    const-string v2, "MediaPlayer[JAVA]"

    #@18c
    const-string v4, "Error: Sending broadcast intent"

    #@18e
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@191
    goto/16 :goto_fc

    #@193
    .line 2266
    .end local v13           #e:Landroid/os/RemoteException;
    .end local v16           #state:I
    :catchall_193
    move-exception v2

    #@194
    :try_start_194
    monitor-exit v4
    :try_end_195
    .catchall {:try_start_194 .. :try_end_195} :catchall_193

    #@195
    throw v2

    #@196
    .line 2278
    .restart local v1       #myManager:Landroid/app/IActivityManager;
    .restart local v16       #state:I
    :cond_196
    :try_start_196
    const-string v2, "MediaPlayer[JAVA]"

    #@198
    const-string/jumbo v4, "myManager is null"

    #@19b
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_19e
    .catch Landroid/os/RemoteException; {:try_start_196 .. :try_end_19e} :catch_189

    #@19e
    goto/16 :goto_fc

    #@1a0
    .line 2293
    .end local v1           #myManager:Landroid/app/IActivityManager;
    .end local v3           #intent:Landroid/content/Intent;
    .end local v16           #state:I
    :sswitch_1a0
    move-object/from16 v0, p0

    #@1a2
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@1a4
    invoke-static {v2}, Landroid/media/MediaPlayer;->access$900(Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    #@1a7
    move-result-object v2

    #@1a8
    if-eqz v2, :cond_12

    #@1aa
    .line 2294
    move-object/from16 v0, p0

    #@1ac
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@1ae
    invoke-static {v2}, Landroid/media/MediaPlayer;->access$900(Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    #@1b1
    move-result-object v2

    #@1b2
    move-object/from16 v0, p0

    #@1b4
    iget-object v4, v0, Landroid/media/MediaPlayer$EventHandler;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@1b6
    move-object/from16 v0, p1

    #@1b8
    iget v5, v0, Landroid/os/Message;->arg1:I

    #@1ba
    move-object/from16 v0, p1

    #@1bc
    iget v6, v0, Landroid/os/Message;->arg2:I

    #@1be
    invoke-interface {v2, v4, v5, v6}, Landroid/media/MediaPlayer$OnVideoSizeChangedListener;->onVideoSizeChanged(Landroid/media/MediaPlayer;II)V

    #@1c1
    goto/16 :goto_12

    #@1c3
    .line 2298
    :sswitch_1c3
    const-string v2, "MediaPlayer[JAVA]"

    #@1c5
    new-instance v4, Ljava/lang/StringBuilder;

    #@1c7
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1ca
    const-string v5, "Error ("

    #@1cc
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cf
    move-result-object v4

    #@1d0
    move-object/from16 v0, p1

    #@1d2
    iget v5, v0, Landroid/os/Message;->arg1:I

    #@1d4
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d7
    move-result-object v4

    #@1d8
    const-string v5, ","

    #@1da
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1dd
    move-result-object v4

    #@1de
    move-object/from16 v0, p1

    #@1e0
    iget v5, v0, Landroid/os/Message;->arg2:I

    #@1e2
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e5
    move-result-object v4

    #@1e6
    const-string v5, ")"

    #@1e8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1eb
    move-result-object v4

    #@1ec
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ef
    move-result-object v4

    #@1f0
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f3
    .line 2299
    const/4 v14, 0x0

    #@1f4
    .line 2300
    .local v14, error_was_handled:Z
    move-object/from16 v0, p0

    #@1f6
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@1f8
    invoke-static {v2}, Landroid/media/MediaPlayer;->access$1000(Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnErrorListener;

    #@1fb
    move-result-object v2

    #@1fc
    if-eqz v2, :cond_216

    #@1fe
    .line 2301
    move-object/from16 v0, p0

    #@200
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@202
    invoke-static {v2}, Landroid/media/MediaPlayer;->access$1000(Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnErrorListener;

    #@205
    move-result-object v2

    #@206
    move-object/from16 v0, p0

    #@208
    iget-object v4, v0, Landroid/media/MediaPlayer$EventHandler;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@20a
    move-object/from16 v0, p1

    #@20c
    iget v5, v0, Landroid/os/Message;->arg1:I

    #@20e
    move-object/from16 v0, p1

    #@210
    iget v6, v0, Landroid/os/Message;->arg2:I

    #@212
    invoke-interface {v2, v4, v5, v6}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    #@215
    move-result v14

    #@216
    .line 2303
    :cond_216
    move-object/from16 v0, p0

    #@218
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@21a
    invoke-static {v2}, Landroid/media/MediaPlayer;->access$200(Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnCompletionListener;

    #@21d
    move-result-object v2

    #@21e
    if-eqz v2, :cond_231

    #@220
    if-nez v14, :cond_231

    #@222
    .line 2304
    move-object/from16 v0, p0

    #@224
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@226
    invoke-static {v2}, Landroid/media/MediaPlayer;->access$200(Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnCompletionListener;

    #@229
    move-result-object v2

    #@22a
    move-object/from16 v0, p0

    #@22c
    iget-object v4, v0, Landroid/media/MediaPlayer$EventHandler;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@22e
    invoke-interface {v2, v4}, Landroid/media/MediaPlayer$OnCompletionListener;->onCompletion(Landroid/media/MediaPlayer;)V

    #@231
    .line 2306
    :cond_231
    move-object/from16 v0, p0

    #@233
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@235
    const/4 v4, 0x0

    #@236
    invoke-static {v2, v4}, Landroid/media/MediaPlayer;->access$300(Landroid/media/MediaPlayer;Z)V

    #@239
    goto/16 :goto_12

    #@23b
    .line 2310
    .end local v14           #error_was_handled:Z
    :sswitch_23b
    move-object/from16 v0, p1

    #@23d
    iget v2, v0, Landroid/os/Message;->arg1:I

    #@23f
    const/16 v4, 0x2bc

    #@241
    if-eq v2, v4, :cond_273

    #@243
    .line 2311
    const-string v2, "MediaPlayer[JAVA]"

    #@245
    new-instance v4, Ljava/lang/StringBuilder;

    #@247
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@24a
    const-string v5, "Info ("

    #@24c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24f
    move-result-object v4

    #@250
    move-object/from16 v0, p1

    #@252
    iget v5, v0, Landroid/os/Message;->arg1:I

    #@254
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@257
    move-result-object v4

    #@258
    const-string v5, ","

    #@25a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25d
    move-result-object v4

    #@25e
    move-object/from16 v0, p1

    #@260
    iget v5, v0, Landroid/os/Message;->arg2:I

    #@262
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@265
    move-result-object v4

    #@266
    const-string v5, ")"

    #@268
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26b
    move-result-object v4

    #@26c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26f
    move-result-object v4

    #@270
    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@273
    .line 2313
    :cond_273
    move-object/from16 v0, p0

    #@275
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@277
    invoke-static {v2}, Landroid/media/MediaPlayer;->access$1100(Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnInfoListener;

    #@27a
    move-result-object v2

    #@27b
    if-eqz v2, :cond_12

    #@27d
    .line 2314
    move-object/from16 v0, p0

    #@27f
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@281
    invoke-static {v2}, Landroid/media/MediaPlayer;->access$1100(Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnInfoListener;

    #@284
    move-result-object v2

    #@285
    move-object/from16 v0, p0

    #@287
    iget-object v4, v0, Landroid/media/MediaPlayer$EventHandler;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@289
    move-object/from16 v0, p1

    #@28b
    iget v5, v0, Landroid/os/Message;->arg1:I

    #@28d
    move-object/from16 v0, p1

    #@28f
    iget v6, v0, Landroid/os/Message;->arg2:I

    #@291
    invoke-interface {v2, v4, v5, v6}, Landroid/media/MediaPlayer$OnInfoListener;->onInfo(Landroid/media/MediaPlayer;II)Z

    #@294
    goto/16 :goto_12

    #@296
    .line 2319
    :sswitch_296
    move-object/from16 v0, p0

    #@298
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@29a
    invoke-static {v2}, Landroid/media/MediaPlayer;->access$1200(Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnTimedTextListener;

    #@29d
    move-result-object v2

    #@29e
    if-eqz v2, :cond_12

    #@2a0
    .line 2321
    move-object/from16 v0, p1

    #@2a2
    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2a4
    if-nez v2, :cond_2b8

    #@2a6
    .line 2322
    move-object/from16 v0, p0

    #@2a8
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@2aa
    invoke-static {v2}, Landroid/media/MediaPlayer;->access$1200(Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnTimedTextListener;

    #@2ad
    move-result-object v2

    #@2ae
    move-object/from16 v0, p0

    #@2b0
    iget-object v4, v0, Landroid/media/MediaPlayer$EventHandler;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@2b2
    const/4 v5, 0x0

    #@2b3
    invoke-interface {v2, v4, v5}, Landroid/media/MediaPlayer$OnTimedTextListener;->onTimedText(Landroid/media/MediaPlayer;Landroid/media/TimedText;)V

    #@2b6
    goto/16 :goto_12

    #@2b8
    .line 2324
    :cond_2b8
    move-object/from16 v0, p1

    #@2ba
    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2bc
    instance-of v2, v2, Landroid/os/Parcel;

    #@2be
    if-eqz v2, :cond_12

    #@2c0
    .line 2325
    move-object/from16 v0, p1

    #@2c2
    iget-object v15, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2c4
    check-cast v15, Landroid/os/Parcel;

    #@2c6
    .line 2326
    .local v15, parcel:Landroid/os/Parcel;
    new-instance v17, Landroid/media/TimedText;

    #@2c8
    move-object/from16 v0, v17

    #@2ca
    invoke-direct {v0, v15}, Landroid/media/TimedText;-><init>(Landroid/os/Parcel;)V

    #@2cd
    .line 2327
    .local v17, text:Landroid/media/TimedText;
    invoke-virtual {v15}, Landroid/os/Parcel;->recycle()V

    #@2d0
    .line 2328
    move-object/from16 v0, p0

    #@2d2
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@2d4
    invoke-static {v2}, Landroid/media/MediaPlayer;->access$1200(Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnTimedTextListener;

    #@2d7
    move-result-object v2

    #@2d8
    move-object/from16 v0, p0

    #@2da
    iget-object v4, v0, Landroid/media/MediaPlayer$EventHandler;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@2dc
    move-object/from16 v0, v17

    #@2de
    invoke-interface {v2, v4, v0}, Landroid/media/MediaPlayer$OnTimedTextListener;->onTimedText(Landroid/media/MediaPlayer;Landroid/media/TimedText;)V

    #@2e1
    goto/16 :goto_12

    #@2e3
    .line 2335
    .end local v15           #parcel:Landroid/os/Parcel;
    .end local v17           #text:Landroid/media/TimedText;
    :sswitch_2e3
    move-object/from16 v0, p0

    #@2e5
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@2e7
    invoke-static {v2}, Landroid/media/MediaPlayer;->access$500(Landroid/media/MediaPlayer;)Landroid/content/Context;

    #@2ea
    move-result-object v2

    #@2eb
    if-eqz v2, :cond_378

    #@2ed
    .line 2337
    const-string v2, "MediaPlayer[JAVA]"

    #@2ef
    new-instance v4, Ljava/lang/StringBuilder;

    #@2f1
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2f4
    const-string v5, "broadcasting MEDIA_CHANGE_PLAYER_STATE : 0x"

    #@2f6
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f9
    move-result-object v4

    #@2fa
    const-string v5, "%x"

    #@2fc
    const/4 v6, 0x1

    #@2fd
    new-array v6, v6, [Ljava/lang/Object;

    #@2ff
    const/4 v7, 0x0

    #@300
    move-object/from16 v0, p1

    #@302
    iget v8, v0, Landroid/os/Message;->arg1:I

    #@304
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@307
    move-result-object v8

    #@308
    aput-object v8, v6, v7

    #@30a
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@30d
    move-result-object v5

    #@30e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@311
    move-result-object v4

    #@312
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@315
    move-result-object v4

    #@316
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@319
    .line 2339
    new-instance v3, Landroid/content/Intent;

    #@31b
    const-string v2, "com.lge.intent.action.MEDIA_PLAYER_STATE_CHANGED"

    #@31d
    invoke-direct {v3, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@320
    .line 2340
    .restart local v3       #intent:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@322
    iget-object v4, v0, Landroid/media/MediaPlayer$EventHandler;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@324
    monitor-enter v4

    #@325
    .line 2341
    :try_start_325
    move-object/from16 v0, p0

    #@327
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@329
    invoke-static {v2}, Landroid/media/MediaPlayer;->access$600(Landroid/media/MediaPlayer;)I

    #@32c
    move-result v16

    #@32d
    .line 2343
    .restart local v16       #state:I
    const-string v2, "com.lge.intent.extra.MEDIA_PLAYER_URI"

    #@32f
    move-object/from16 v0, p0

    #@331
    iget-object v5, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@333
    invoke-static {v5}, Landroid/media/MediaPlayer;->access$700(Landroid/media/MediaPlayer;)Ljava/lang/String;

    #@336
    move-result-object v5

    #@337
    invoke-virtual {v3, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@33a
    .line 2344
    const-string v2, "com.lge.intent.extra.MEDIA_PLAYER_STATE"

    #@33c
    move/from16 v0, v16

    #@33e
    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@341
    .line 2346
    move/from16 v0, v16

    #@343
    and-int/lit16 v2, v0, 0xfe

    #@345
    if-lez v2, :cond_354

    #@347
    .line 2348
    const-string v2, "com.lge.intent.extra.MEDIA_PLAYER_CURRENT_POSITION"

    #@349
    move-object/from16 v0, p0

    #@34b
    iget-object v5, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@34d
    invoke-virtual {v5}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    #@350
    move-result v5

    #@351
    invoke-virtual {v3, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@354
    .line 2350
    :cond_354
    move/from16 v0, v16

    #@356
    and-int/lit16 v2, v0, 0xb8

    #@358
    if-lez v2, :cond_367

    #@35a
    .line 2352
    const-string v2, "com.lge.intent.extra.MEDIA_PLAYER_TOTAL_DURATION"

    #@35c
    move-object/from16 v0, p0

    #@35e
    iget-object v5, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@360
    invoke-virtual {v5}, Landroid/media/MediaPlayer;->getDuration()I

    #@363
    move-result v5

    #@364
    invoke-virtual {v3, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@367
    .line 2354
    :cond_367
    monitor-exit v4
    :try_end_368
    .catchall {:try_start_325 .. :try_end_368} :catchall_375

    #@368
    .line 2356
    move-object/from16 v0, p0

    #@36a
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@36c
    invoke-static {v2}, Landroid/media/MediaPlayer;->access$500(Landroid/media/MediaPlayer;)Landroid/content/Context;

    #@36f
    move-result-object v2

    #@370
    invoke-virtual {v2, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@373
    goto/16 :goto_12

    #@375
    .line 2354
    .end local v16           #state:I
    :catchall_375
    move-exception v2

    #@376
    :try_start_376
    monitor-exit v4
    :try_end_377
    .catchall {:try_start_376 .. :try_end_377} :catchall_375

    #@377
    throw v2

    #@378
    .line 2361
    .end local v3           #intent:Landroid/content/Intent;
    :cond_378
    const-string v2, "MediaPlayer[JAVA]"

    #@37a
    new-instance v4, Ljava/lang/StringBuilder;

    #@37c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@37f
    const-string v5, "broadcasting MEDIA_CHANGE_PLAYER_STATE : 0x"

    #@381
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@384
    move-result-object v4

    #@385
    const-string v5, "%x"

    #@387
    const/4 v6, 0x1

    #@388
    new-array v6, v6, [Ljava/lang/Object;

    #@38a
    const/4 v7, 0x0

    #@38b
    move-object/from16 v0, p1

    #@38d
    iget v8, v0, Landroid/os/Message;->arg1:I

    #@38f
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@392
    move-result-object v8

    #@393
    aput-object v8, v6, v7

    #@395
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@398
    move-result-object v5

    #@399
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39c
    move-result-object v4

    #@39d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a0
    move-result-object v4

    #@3a1
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a4
    .line 2363
    new-instance v3, Landroid/content/Intent;

    #@3a6
    const-string v2, "com.lge.intent.action.MEDIA_PLAYER_STATE_CHANGED"

    #@3a8
    invoke-direct {v3, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@3ab
    .line 2364
    .restart local v3       #intent:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@3ad
    iget-object v4, v0, Landroid/media/MediaPlayer$EventHandler;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@3af
    monitor-enter v4

    #@3b0
    .line 2365
    :try_start_3b0
    move-object/from16 v0, p0

    #@3b2
    iget-object v2, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@3b4
    invoke-static {v2}, Landroid/media/MediaPlayer;->access$600(Landroid/media/MediaPlayer;)I

    #@3b7
    move-result v16

    #@3b8
    .line 2367
    .restart local v16       #state:I
    const-string v2, "com.lge.intent.extra.MEDIA_PLAYER_URI"

    #@3ba
    move-object/from16 v0, p0

    #@3bc
    iget-object v5, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@3be
    invoke-static {v5}, Landroid/media/MediaPlayer;->access$700(Landroid/media/MediaPlayer;)Ljava/lang/String;

    #@3c1
    move-result-object v5

    #@3c2
    invoke-virtual {v3, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@3c5
    .line 2368
    const-string v2, "com.lge.intent.extra.MEDIA_PLAYER_STATE"

    #@3c7
    move/from16 v0, v16

    #@3c9
    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@3cc
    .line 2370
    move/from16 v0, v16

    #@3ce
    and-int/lit16 v2, v0, 0xfe

    #@3d0
    if-lez v2, :cond_3df

    #@3d2
    .line 2372
    const-string v2, "com.lge.intent.extra.MEDIA_PLAYER_CURRENT_POSITION"

    #@3d4
    move-object/from16 v0, p0

    #@3d6
    iget-object v5, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@3d8
    invoke-virtual {v5}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    #@3db
    move-result v5

    #@3dc
    invoke-virtual {v3, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@3df
    .line 2374
    :cond_3df
    move/from16 v0, v16

    #@3e1
    and-int/lit16 v2, v0, 0xb8

    #@3e3
    if-lez v2, :cond_3f2

    #@3e5
    .line 2376
    const-string v2, "com.lge.intent.extra.MEDIA_PLAYER_TOTAL_DURATION"

    #@3e7
    move-object/from16 v0, p0

    #@3e9
    iget-object v5, v0, Landroid/media/MediaPlayer$EventHandler;->this$0:Landroid/media/MediaPlayer;

    #@3eb
    invoke-virtual {v5}, Landroid/media/MediaPlayer;->getDuration()I

    #@3ee
    move-result v5

    #@3ef
    invoke-virtual {v3, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@3f2
    .line 2378
    :cond_3f2
    monitor-exit v4
    :try_end_3f3
    .catchall {:try_start_3b0 .. :try_end_3f3} :catchall_416

    #@3f3
    .line 2381
    const/4 v2, 0x0

    #@3f4
    :try_start_3f4
    invoke-virtual {v3, v2}, Landroid/content/Intent;->setAllowFds(Z)V

    #@3f7
    .line 2382
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3fa
    move-result-object v1

    #@3fb
    .line 2383
    .restart local v1       #myManager:Landroid/app/IActivityManager;
    if-eqz v1, :cond_419

    #@3fd
    .line 2385
    const/4 v2, 0x0

    #@3fe
    const/4 v4, 0x0

    #@3ff
    const/4 v5, 0x0

    #@400
    const/4 v6, -0x1

    #@401
    const/4 v7, 0x0

    #@402
    const/4 v8, 0x0

    #@403
    const/4 v9, 0x0

    #@404
    const/4 v10, 0x0

    #@405
    const/4 v11, 0x0

    #@406
    const/4 v12, -0x3

    #@407
    invoke-interface/range {v1 .. v12}, Landroid/app/IActivityManager;->broadcastIntent(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ZZI)I
    :try_end_40a
    .catch Landroid/os/RemoteException; {:try_start_3f4 .. :try_end_40a} :catch_40c

    #@40a
    goto/16 :goto_12

    #@40c
    .line 2392
    .end local v1           #myManager:Landroid/app/IActivityManager;
    :catch_40c
    move-exception v13

    #@40d
    .line 2393
    .restart local v13       #e:Landroid/os/RemoteException;
    const-string v2, "MediaPlayer[JAVA]"

    #@40f
    const-string v4, "Error: Sending broadcast intent"

    #@411
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@414
    goto/16 :goto_12

    #@416
    .line 2378
    .end local v13           #e:Landroid/os/RemoteException;
    .end local v16           #state:I
    :catchall_416
    move-exception v2

    #@417
    :try_start_417
    monitor-exit v4
    :try_end_418
    .catchall {:try_start_417 .. :try_end_418} :catchall_416

    #@418
    throw v2

    #@419
    .line 2390
    .restart local v1       #myManager:Landroid/app/IActivityManager;
    .restart local v16       #state:I
    :cond_419
    :try_start_419
    const-string v2, "MediaPlayer[JAVA]"

    #@41b
    const-string/jumbo v4, "myManager is null"

    #@41e
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_421
    .catch Landroid/os/RemoteException; {:try_start_419 .. :try_end_421} :catch_40c

    #@421
    goto/16 :goto_12

    #@423
    .line 2203
    nop

    #@424
    :sswitch_data_424
    .sparse-switch
        0x0 -> :sswitch_12
        0x1 -> :sswitch_37
        0x2 -> :sswitch_51
        0x3 -> :sswitch_73
        0x4 -> :sswitch_91
        0x5 -> :sswitch_1a0
        0x63 -> :sswitch_296
        0x64 -> :sswitch_1c3
        0xc8 -> :sswitch_23b
        0x1f4 -> :sswitch_2e3
    .end sparse-switch
.end method
