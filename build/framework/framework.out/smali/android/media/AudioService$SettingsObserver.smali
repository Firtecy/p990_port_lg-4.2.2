.class Landroid/media/AudioService$SettingsObserver;
.super Landroid/database/ContentObserver;
.source "AudioService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/AudioService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/media/AudioService;


# direct methods
.method constructor <init>(Landroid/media/AudioService;)V
    .registers 5
    .parameter

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 4179
    iput-object p1, p0, Landroid/media/AudioService$SettingsObserver;->this$0:Landroid/media/AudioService;

    #@3
    .line 4180
    new-instance v0, Landroid/os/Handler;

    #@5
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@8
    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    #@b
    .line 4181
    invoke-static {p1}, Landroid/media/AudioService;->access$3600(Landroid/media/AudioService;)Landroid/content/ContentResolver;

    #@e
    move-result-object v0

    #@f
    const-string/jumbo v1, "mode_ringer_streams_affected"

    #@12
    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@19
    .line 4183
    invoke-static {p1}, Landroid/media/AudioService;->access$3600(Landroid/media/AudioService;)Landroid/content/ContentResolver;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, "dock_audio_media_enabled"

    #@1f
    invoke-static {v1}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@26
    .line 4185
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .registers 8
    .parameter "selfChange"

    #@0
    .prologue
    .line 4189
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    #@3
    .line 4194
    iget-object v1, p0, Landroid/media/AudioService$SettingsObserver;->this$0:Landroid/media/AudioService;

    #@5
    invoke-static {v1}, Landroid/media/AudioService;->access$6100(Landroid/media/AudioService;)Ljava/lang/Object;

    #@8
    move-result-object v2

    #@9
    monitor-enter v2

    #@a
    .line 4195
    :try_start_a
    iget-object v1, p0, Landroid/media/AudioService$SettingsObserver;->this$0:Landroid/media/AudioService;

    #@c
    invoke-static {v1}, Landroid/media/AudioService;->access$3600(Landroid/media/AudioService;)Landroid/content/ContentResolver;

    #@f
    move-result-object v1

    #@10
    const-string/jumbo v3, "mode_ringer_streams_affected"

    #@13
    const/16 v4, 0xa6

    #@15
    const/4 v5, -0x2

    #@16
    invoke-static {v1, v3, v4, v5}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@19
    move-result v0

    #@1a
    .line 4200
    .local v0, ringerModeAffectedStreams:I
    iget-object v1, p0, Landroid/media/AudioService$SettingsObserver;->this$0:Landroid/media/AudioService;

    #@1c
    invoke-static {v1}, Landroid/media/AudioService;->access$3700(Landroid/media/AudioService;)Z

    #@1f
    move-result v1

    #@20
    if-eqz v1, :cond_6a

    #@22
    .line 4201
    and-int/lit8 v0, v0, -0x9

    #@24
    .line 4205
    :goto_24
    iget-object v1, p0, Landroid/media/AudioService$SettingsObserver;->this$0:Landroid/media/AudioService;

    #@26
    invoke-static {v1}, Landroid/media/AudioService;->access$3400(Landroid/media/AudioService;)Ljava/lang/Boolean;

    #@29
    move-result-object v3

    #@2a
    monitor-enter v3
    :try_end_2b
    .catchall {:try_start_a .. :try_end_2b} :catchall_73

    #@2b
    .line 4206
    :try_start_2b
    iget-object v1, p0, Landroid/media/AudioService$SettingsObserver;->this$0:Landroid/media/AudioService;

    #@2d
    invoke-static {v1}, Landroid/media/AudioService;->access$3400(Landroid/media/AudioService;)Ljava/lang/Boolean;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    #@34
    move-result v1

    #@35
    if-eqz v1, :cond_6d

    #@37
    .line 4207
    and-int/lit16 v0, v0, -0x81

    #@39
    .line 4211
    :goto_39
    monitor-exit v3
    :try_end_3a
    .catchall {:try_start_2b .. :try_end_3a} :catchall_70

    #@3a
    .line 4213
    :try_start_3a
    iget-object v1, p0, Landroid/media/AudioService$SettingsObserver;->this$0:Landroid/media/AudioService;

    #@3c
    invoke-static {v1}, Landroid/media/AudioService;->access$7700(Landroid/media/AudioService;)Z

    #@3f
    move-result v1

    #@40
    if-eqz v1, :cond_44

    #@42
    .line 4214
    and-int/lit8 v0, v0, -0x5

    #@44
    .line 4217
    :cond_44
    iget-object v1, p0, Landroid/media/AudioService$SettingsObserver;->this$0:Landroid/media/AudioService;

    #@46
    invoke-static {v1}, Landroid/media/AudioService;->access$7800(Landroid/media/AudioService;)I

    #@49
    move-result v1

    #@4a
    if-eq v0, v1, :cond_5d

    #@4c
    .line 4222
    iget-object v1, p0, Landroid/media/AudioService$SettingsObserver;->this$0:Landroid/media/AudioService;

    #@4e
    invoke-static {v1, v0}, Landroid/media/AudioService;->access$7802(Landroid/media/AudioService;I)I

    #@51
    .line 4223
    iget-object v1, p0, Landroid/media/AudioService$SettingsObserver;->this$0:Landroid/media/AudioService;

    #@53
    iget-object v3, p0, Landroid/media/AudioService$SettingsObserver;->this$0:Landroid/media/AudioService;

    #@55
    invoke-virtual {v3}, Landroid/media/AudioService;->getRingerMode()I

    #@58
    move-result v3

    #@59
    const/4 v4, 0x0

    #@5a
    invoke-static {v1, v3, v4}, Landroid/media/AudioService;->access$5600(Landroid/media/AudioService;IZ)V

    #@5d
    .line 4225
    :cond_5d
    iget-object v1, p0, Landroid/media/AudioService$SettingsObserver;->this$0:Landroid/media/AudioService;

    #@5f
    iget-object v3, p0, Landroid/media/AudioService$SettingsObserver;->this$0:Landroid/media/AudioService;

    #@61
    invoke-static {v3}, Landroid/media/AudioService;->access$3600(Landroid/media/AudioService;)Landroid/content/ContentResolver;

    #@64
    move-result-object v3

    #@65
    invoke-static {v1, v3}, Landroid/media/AudioService;->access$7900(Landroid/media/AudioService;Landroid/content/ContentResolver;)V

    #@68
    .line 4226
    monitor-exit v2
    :try_end_69
    .catchall {:try_start_3a .. :try_end_69} :catchall_73

    #@69
    .line 4227
    return-void

    #@6a
    .line 4203
    :cond_6a
    or-int/lit8 v0, v0, 0x8

    #@6c
    goto :goto_24

    #@6d
    .line 4209
    :cond_6d
    or-int/lit16 v0, v0, 0x80

    #@6f
    goto :goto_39

    #@70
    .line 4211
    :catchall_70
    move-exception v1

    #@71
    :try_start_71
    monitor-exit v3
    :try_end_72
    .catchall {:try_start_71 .. :try_end_72} :catchall_70

    #@72
    :try_start_72
    throw v1

    #@73
    .line 4226
    .end local v0           #ringerModeAffectedStreams:I
    :catchall_73
    move-exception v1

    #@74
    monitor-exit v2
    :try_end_75
    .catchall {:try_start_72 .. :try_end_75} :catchall_73

    #@75
    throw v1
.end method
