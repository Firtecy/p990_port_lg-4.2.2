.class Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;
.super Ljava/lang/Object;
.source "AudioService.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/AudioService$VolumeStreamState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VolumeDeathHandler"
.end annotation


# instance fields
.field private mICallback:Landroid/os/IBinder;

.field private mMuteCount:I

.field final synthetic this$1:Landroid/media/AudioService$VolumeStreamState;


# direct methods
.method constructor <init>(Landroid/media/AudioService$VolumeStreamState;Landroid/os/IBinder;)V
    .registers 3
    .parameter
    .parameter "cb"

    #@0
    .prologue
    .line 3621
    iput-object p1, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->this$1:Landroid/media/AudioService$VolumeStreamState;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 3622
    iput-object p2, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->mICallback:Landroid/os/IBinder;

    #@7
    .line 3623
    return-void
.end method

.method static synthetic access$2200(Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 3617
    iget v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->mMuteCount:I

    #@2
    return v0
.end method

.method static synthetic access$2202(Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 3617
    iput p1, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->mMuteCount:I

    #@2
    return p1
.end method

.method static synthetic access$4500(Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;)Landroid/os/IBinder;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 3617
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->mICallback:Landroid/os/IBinder;

    #@2
    return-object v0
.end method


# virtual methods
.method public binderDied()V
    .registers 4

    #@0
    .prologue
    .line 3705
    const-string v0, "AudioService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Volume service client died for stream: "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->this$1:Landroid/media/AudioService$VolumeStreamState;

    #@f
    invoke-static {v2}, Landroid/media/AudioService$VolumeStreamState;->access$4400(Landroid/media/AudioService$VolumeStreamState;)I

    #@12
    move-result v2

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 3706
    iget v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->mMuteCount:I

    #@20
    if-eqz v0, :cond_29

    #@22
    .line 3708
    const/4 v0, 0x1

    #@23
    iput v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->mMuteCount:I

    #@25
    .line 3709
    const/4 v0, 0x0

    #@26
    invoke-virtual {p0, v0}, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->mute(Z)V

    #@29
    .line 3711
    :cond_29
    return-void
.end method

.method public mute(Z)V
    .registers 15
    .parameter "state"

    #@0
    .prologue
    const/16 v1, 0xe

    #@2
    const/4 v2, 0x2

    #@3
    const/4 v3, 0x0

    #@4
    .line 3627
    if-eqz p1, :cond_b5

    #@6
    .line 3628
    iget v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->mMuteCount:I

    #@8
    if-nez v0, :cond_8f

    #@a
    .line 3632
    :try_start_a
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->mICallback:Landroid/os/IBinder;

    #@c
    if-eqz v0, :cond_14

    #@e
    .line 3633
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->mICallback:Landroid/os/IBinder;

    #@10
    const/4 v1, 0x0

    #@11
    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    #@14
    .line 3635
    :cond_14
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->this$1:Landroid/media/AudioService$VolumeStreamState;

    #@16
    invoke-static {v0}, Landroid/media/AudioService$VolumeStreamState;->access$2100(Landroid/media/AudioService$VolumeStreamState;)Ljava/util/ArrayList;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1d
    .line 3637
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->this$1:Landroid/media/AudioService$VolumeStreamState;

    #@1f
    invoke-static {v0}, Landroid/media/AudioService$VolumeStreamState;->access$700(Landroid/media/AudioService$VolumeStreamState;)I

    #@22
    move-result v0

    #@23
    if-nez v0, :cond_88

    #@25
    .line 3638
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->this$1:Landroid/media/AudioService$VolumeStreamState;

    #@27
    invoke-static {v0}, Landroid/media/AudioService$VolumeStreamState;->access$4300(Landroid/media/AudioService$VolumeStreamState;)Ljava/util/concurrent/ConcurrentHashMap;

    #@2a
    move-result-object v0

    #@2b
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    #@2e
    move-result-object v11

    #@2f
    .line 3639
    .local v11, set:Ljava/util/Set;
    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@32
    move-result-object v10

    #@33
    .line 3640
    .local v10, i:Ljava/util/Iterator;
    :goto_33
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@36
    move-result v0

    #@37
    if-eqz v0, :cond_75

    #@39
    .line 3641
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3c
    move-result-object v9

    #@3d
    check-cast v9, Ljava/util/Map$Entry;

    #@3f
    .line 3642
    .local v9, entry:Ljava/util/Map$Entry;
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->this$1:Landroid/media/AudioService$VolumeStreamState;

    #@41
    iget-object v0, v0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@43
    invoke-static {v0}, Landroid/media/AudioService;->access$3500(Landroid/media/AudioService;)[I

    #@46
    move-result-object v0

    #@47
    iget-object v1, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->this$1:Landroid/media/AudioService$VolumeStreamState;

    #@49
    invoke-static {v1}, Landroid/media/AudioService$VolumeStreamState;->access$4400(Landroid/media/AudioService$VolumeStreamState;)I

    #@4c
    move-result v1

    #@4d
    aget v12, v0, v1

    #@4f
    .line 3643
    .local v12, streamTypeAlias:I
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->this$1:Landroid/media/AudioService$VolumeStreamState;

    #@51
    iget-object v0, v0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@53
    invoke-static {v0, v12}, Landroid/media/AudioService;->access$4000(Landroid/media/AudioService;I)I

    #@56
    move-result v7

    #@57
    .line 3644
    .local v7, device:I
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->this$1:Landroid/media/AudioService$VolumeStreamState;

    #@59
    const/4 v1, 0x0

    #@5a
    const/4 v2, 0x0

    #@5b
    invoke-virtual {v0, v1, v7, v2}, Landroid/media/AudioService$VolumeStreamState;->setIndex(IIZ)Z

    #@5e
    .line 3645
    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@61
    move-result-object v0

    #@62
    check-cast v0, Ljava/lang/Integer;

    #@64
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@67
    move-result v7

    #@68
    .line 3646
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->this$1:Landroid/media/AudioService$VolumeStreamState;

    #@6a
    const/4 v1, 0x0

    #@6b
    const/4 v2, 0x0

    #@6c
    invoke-virtual {v0, v1, v7, v2}, Landroid/media/AudioService$VolumeStreamState;->setIndex(IIZ)Z
    :try_end_6f
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_6f} :catch_70

    #@6f
    goto :goto_33

    #@70
    .line 3655
    .end local v7           #device:I
    .end local v9           #entry:Ljava/util/Map$Entry;
    .end local v10           #i:Ljava/util/Iterator;
    .end local v11           #set:Ljava/util/Set;
    .end local v12           #streamTypeAlias:I
    :catch_70
    move-exception v8

    #@71
    .line 3657
    .local v8, e:Landroid/os/RemoteException;
    invoke-virtual {p0}, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->binderDied()V

    #@74
    .line 3702
    .end local v8           #e:Landroid/os/RemoteException;
    :cond_74
    :goto_74
    return-void

    #@75
    .line 3648
    .restart local v10       #i:Ljava/util/Iterator;
    .restart local v11       #set:Ljava/util/Set;
    :cond_75
    :try_start_75
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->this$1:Landroid/media/AudioService$VolumeStreamState;

    #@77
    iget-object v0, v0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@79
    invoke-static {v0}, Landroid/media/AudioService;->access$100(Landroid/media/AudioService;)Landroid/media/AudioService$AudioHandler;

    #@7c
    move-result-object v0

    #@7d
    const/16 v1, 0xe

    #@7f
    const/4 v2, 0x2

    #@80
    const/4 v3, 0x0

    #@81
    const/4 v4, 0x0

    #@82
    iget-object v5, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->this$1:Landroid/media/AudioService$VolumeStreamState;

    #@84
    const/4 v6, 0x0

    #@85
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->access$200(Landroid/os/Handler;IIIILjava/lang/Object;I)V
    :try_end_88
    .catch Landroid/os/RemoteException; {:try_start_75 .. :try_end_88} :catch_70

    #@88
    .line 3663
    .end local v10           #i:Ljava/util/Iterator;
    .end local v11           #set:Ljava/util/Set;
    :cond_88
    :goto_88
    iget v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->mMuteCount:I

    #@8a
    add-int/lit8 v0, v0, 0x1

    #@8c
    iput v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->mMuteCount:I

    #@8e
    goto :goto_74

    #@8f
    .line 3661
    :cond_8f
    const-string v0, "AudioService"

    #@91
    new-instance v1, Ljava/lang/StringBuilder;

    #@93
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@96
    const-string/jumbo v2, "stream: "

    #@99
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v1

    #@9d
    iget-object v2, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->this$1:Landroid/media/AudioService$VolumeStreamState;

    #@9f
    invoke-static {v2}, Landroid/media/AudioService$VolumeStreamState;->access$4400(Landroid/media/AudioService$VolumeStreamState;)I

    #@a2
    move-result v2

    #@a3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v1

    #@a7
    const-string v2, " was already muted by this client"

    #@a9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v1

    #@ad
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b0
    move-result-object v1

    #@b1
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b4
    goto :goto_88

    #@b5
    .line 3665
    :cond_b5
    iget v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->mMuteCount:I

    #@b7
    if-nez v0, :cond_d9

    #@b9
    .line 3666
    const-string v0, "AudioService"

    #@bb
    new-instance v1, Ljava/lang/StringBuilder;

    #@bd
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c0
    const-string/jumbo v2, "unexpected unmute for stream: "

    #@c3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v1

    #@c7
    iget-object v2, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->this$1:Landroid/media/AudioService$VolumeStreamState;

    #@c9
    invoke-static {v2}, Landroid/media/AudioService$VolumeStreamState;->access$4400(Landroid/media/AudioService$VolumeStreamState;)I

    #@cc
    move-result v2

    #@cd
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v1

    #@d1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d4
    move-result-object v1

    #@d5
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d8
    goto :goto_74

    #@d9
    .line 3668
    :cond_d9
    iget v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->mMuteCount:I

    #@db
    add-int/lit8 v0, v0, -0x1

    #@dd
    iput v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->mMuteCount:I

    #@df
    .line 3669
    iget v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->mMuteCount:I

    #@e1
    if-nez v0, :cond_74

    #@e3
    .line 3671
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->this$1:Landroid/media/AudioService$VolumeStreamState;

    #@e5
    invoke-static {v0}, Landroid/media/AudioService$VolumeStreamState;->access$2100(Landroid/media/AudioService$VolumeStreamState;)Ljava/util/ArrayList;

    #@e8
    move-result-object v0

    #@e9
    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@ec
    .line 3673
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->mICallback:Landroid/os/IBinder;

    #@ee
    if-eqz v0, :cond_f5

    #@f0
    .line 3674
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->mICallback:Landroid/os/IBinder;

    #@f2
    invoke-interface {v0, p0, v3}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    #@f5
    .line 3676
    :cond_f5
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->this$1:Landroid/media/AudioService$VolumeStreamState;

    #@f7
    invoke-static {v0}, Landroid/media/AudioService$VolumeStreamState;->access$700(Landroid/media/AudioService$VolumeStreamState;)I

    #@fa
    move-result v0

    #@fb
    if-nez v0, :cond_74

    #@fd
    .line 3679
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->this$1:Landroid/media/AudioService$VolumeStreamState;

    #@ff
    iget-object v0, v0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@101
    iget-object v4, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->this$1:Landroid/media/AudioService$VolumeStreamState;

    #@103
    invoke-static {v4}, Landroid/media/AudioService$VolumeStreamState;->access$4400(Landroid/media/AudioService$VolumeStreamState;)I

    #@106
    move-result v4

    #@107
    invoke-virtual {v0, v4}, Landroid/media/AudioService;->isStreamAffectedByRingerMode(I)Z

    #@10a
    move-result v0

    #@10b
    if-eqz v0, :cond_117

    #@10d
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->this$1:Landroid/media/AudioService$VolumeStreamState;

    #@10f
    iget-object v0, v0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@111
    invoke-static {v0}, Landroid/media/AudioService;->access$3900(Landroid/media/AudioService;)I

    #@114
    move-result v0

    #@115
    if-ne v0, v2, :cond_74

    #@117
    .line 3681
    :cond_117
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->this$1:Landroid/media/AudioService$VolumeStreamState;

    #@119
    invoke-static {v0}, Landroid/media/AudioService$VolumeStreamState;->access$4300(Landroid/media/AudioService$VolumeStreamState;)Ljava/util/concurrent/ConcurrentHashMap;

    #@11c
    move-result-object v0

    #@11d
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    #@120
    move-result-object v11

    #@121
    .line 3682
    .restart local v11       #set:Ljava/util/Set;
    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@124
    move-result-object v10

    #@125
    .line 3683
    .restart local v10       #i:Ljava/util/Iterator;
    :goto_125
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@128
    move-result v0

    #@129
    if-eqz v0, :cond_148

    #@12b
    .line 3684
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@12e
    move-result-object v9

    #@12f
    check-cast v9, Ljava/util/Map$Entry;

    #@131
    .line 3685
    .restart local v9       #entry:Ljava/util/Map$Entry;
    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@134
    move-result-object v0

    #@135
    check-cast v0, Ljava/lang/Integer;

    #@137
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@13a
    move-result v7

    #@13b
    .line 3686
    .restart local v7       #device:I
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->this$1:Landroid/media/AudioService$VolumeStreamState;

    #@13d
    iget-object v4, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->this$1:Landroid/media/AudioService$VolumeStreamState;

    #@13f
    const/4 v5, 0x1

    #@140
    invoke-virtual {v4, v7, v5}, Landroid/media/AudioService$VolumeStreamState;->getIndex(IZ)I

    #@143
    move-result v4

    #@144
    invoke-virtual {v0, v4, v7, v3}, Landroid/media/AudioService$VolumeStreamState;->setIndex(IIZ)Z

    #@147
    goto :goto_125

    #@148
    .line 3691
    .end local v7           #device:I
    .end local v9           #entry:Ljava/util/Map$Entry;
    :cond_148
    iget-object v0, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->this$1:Landroid/media/AudioService$VolumeStreamState;

    #@14a
    iget-object v0, v0, Landroid/media/AudioService$VolumeStreamState;->this$0:Landroid/media/AudioService;

    #@14c
    invoke-static {v0}, Landroid/media/AudioService;->access$100(Landroid/media/AudioService;)Landroid/media/AudioService$AudioHandler;

    #@14f
    move-result-object v0

    #@150
    iget-object v5, p0, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->this$1:Landroid/media/AudioService$VolumeStreamState;

    #@152
    move v4, v3

    #@153
    move v6, v3

    #@154
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->access$200(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@157
    goto/16 :goto_74
.end method
