.class public Landroid/media/ExifInterface;
.super Ljava/lang/Object;
.source "ExifInterface.java"


# static fields
.field public static final ORIENTATION_FLIP_HORIZONTAL:I = 0x2

.field public static final ORIENTATION_FLIP_VERTICAL:I = 0x4

.field public static final ORIENTATION_NORMAL:I = 0x1

.field public static final ORIENTATION_ROTATE_180:I = 0x3

.field public static final ORIENTATION_ROTATE_270:I = 0x8

.field public static final ORIENTATION_ROTATE_90:I = 0x6

.field public static final ORIENTATION_TRANSPOSE:I = 0x5

.field public static final ORIENTATION_TRANSVERSE:I = 0x7

.field public static final ORIENTATION_UNDEFINED:I = 0x0

.field public static final TAG_APERTURE:Ljava/lang/String; = "FNumber"

.field public static final TAG_DATETIME:Ljava/lang/String; = "DateTime"

.field public static final TAG_EXPOSURE_TIME:Ljava/lang/String; = "ExposureTime"

.field public static final TAG_FLASH:Ljava/lang/String; = "Flash"

.field public static final TAG_FOCAL_LENGTH:Ljava/lang/String; = "FocalLength"

.field public static final TAG_GPS_ALTITUDE:Ljava/lang/String; = "GPSAltitude"

.field public static final TAG_GPS_ALTITUDE_REF:Ljava/lang/String; = "GPSAltitudeRef"

.field public static final TAG_GPS_DATESTAMP:Ljava/lang/String; = "GPSDateStamp"

.field public static final TAG_GPS_LATITUDE:Ljava/lang/String; = "GPSLatitude"

.field public static final TAG_GPS_LATITUDE_REF:Ljava/lang/String; = "GPSLatitudeRef"

.field public static final TAG_GPS_LONGITUDE:Ljava/lang/String; = "GPSLongitude"

.field public static final TAG_GPS_LONGITUDE_REF:Ljava/lang/String; = "GPSLongitudeRef"

.field public static final TAG_GPS_PROCESSING_METHOD:Ljava/lang/String; = "GPSProcessingMethod"

.field public static final TAG_GPS_TIMESTAMP:Ljava/lang/String; = "GPSTimeStamp"

.field public static final TAG_IMAGE_LENGTH:Ljava/lang/String; = "ImageLength"

.field public static final TAG_IMAGE_WIDTH:Ljava/lang/String; = "ImageWidth"

.field public static final TAG_ISO:Ljava/lang/String; = "ISOSpeedRatings"

.field public static final TAG_MAKE:Ljava/lang/String; = "Make"

.field public static final TAG_MODEL:Ljava/lang/String; = "Model"

.field public static final TAG_ORIENTATION:Ljava/lang/String; = "Orientation"

.field public static final TAG_WHITE_BALANCE:Ljava/lang/String; = "WhiteBalance"

.field public static final WHITEBALANCE_AUTO:I = 0x0

.field public static final WHITEBALANCE_MANUAL:I = 0x1

.field private static sFormatter:Ljava/text/SimpleDateFormat;

.field private static final sLock:Ljava/lang/Object;


# instance fields
.field private mAttributes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mFilename:Ljava/lang/String;

.field private mHasThumbnail:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 101
    const-string v0, "exif_jni"

    #@2
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@5
    .line 102
    new-instance v0, Ljava/text/SimpleDateFormat;

    #@7
    const-string/jumbo v1, "yyyy:MM:dd HH:mm:ss"

    #@a
    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    #@d
    sput-object v0, Landroid/media/ExifInterface;->sFormatter:Ljava/text/SimpleDateFormat;

    #@f
    .line 103
    sget-object v0, Landroid/media/ExifInterface;->sFormatter:Ljava/text/SimpleDateFormat;

    #@11
    const-string v1, "UTC"

    #@13
    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    #@1a
    .line 114
    new-instance v0, Ljava/lang/Object;

    #@1c
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@1f
    sput-object v0, Landroid/media/ExifInterface;->sLock:Ljava/lang/Object;

    #@21
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 2
    .parameter "filename"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 119
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 120
    iput-object p1, p0, Landroid/media/ExifInterface;->mFilename:Ljava/lang/String;

    #@5
    .line 121
    invoke-direct {p0}, Landroid/media/ExifInterface;->loadAttributes()V

    #@8
    .line 122
    return-void
.end method

.method private native appendThumbnailNative(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method private native commitChangesNative(Ljava/lang/String;)V
.end method

.method private static convertRationalLatLonToFloat(Ljava/lang/String;Ljava/lang/String;)F
    .registers 18
    .parameter "rationalString"
    .parameter "ref"

    #@0
    .prologue
    .line 376
    :try_start_0
    const-string v12, ","

    #@2
    move-object/from16 v0, p0

    #@4
    invoke-virtual {v0, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@7
    move-result-object v7

    #@8
    .line 379
    .local v7, parts:[Ljava/lang/String;
    const/4 v12, 0x0

    #@9
    aget-object v12, v7, v12

    #@b
    const-string v13, "/"

    #@d
    invoke-virtual {v12, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@10
    move-result-object v6

    #@11
    .line 380
    .local v6, pair:[Ljava/lang/String;
    const/4 v12, 0x0

    #@12
    aget-object v12, v6, v12

    #@14
    invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@17
    move-result-object v12

    #@18
    invoke-static {v12}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    #@1b
    move-result-wide v12

    #@1c
    const/4 v14, 0x1

    #@1d
    aget-object v14, v6, v14

    #@1f
    invoke-virtual {v14}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@22
    move-result-object v14

    #@23
    invoke-static {v14}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    #@26
    move-result-wide v14

    #@27
    div-double v1, v12, v14

    #@29
    .line 383
    .local v1, degrees:D
    const/4 v12, 0x1

    #@2a
    aget-object v12, v7, v12

    #@2c
    const-string v13, "/"

    #@2e
    invoke-virtual {v12, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@31
    move-result-object v6

    #@32
    .line 384
    const/4 v12, 0x0

    #@33
    aget-object v12, v6, v12

    #@35
    invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@38
    move-result-object v12

    #@39
    invoke-static {v12}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    #@3c
    move-result-wide v12

    #@3d
    const/4 v14, 0x1

    #@3e
    aget-object v14, v6, v14

    #@40
    invoke-virtual {v14}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@43
    move-result-object v14

    #@44
    invoke-static {v14}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    #@47
    move-result-wide v14

    #@48
    div-double v4, v12, v14

    #@4a
    .line 387
    .local v4, minutes:D
    const/4 v12, 0x2

    #@4b
    aget-object v12, v7, v12

    #@4d
    const-string v13, "/"

    #@4f
    invoke-virtual {v12, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@52
    move-result-object v6

    #@53
    .line 388
    const/4 v12, 0x0

    #@54
    aget-object v12, v6, v12

    #@56
    invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@59
    move-result-object v12

    #@5a
    invoke-static {v12}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    #@5d
    move-result-wide v12

    #@5e
    const/4 v14, 0x1

    #@5f
    aget-object v14, v6, v14

    #@61
    invoke-virtual {v14}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@64
    move-result-object v14

    #@65
    invoke-static {v14}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    #@68
    move-result-wide v14

    #@69
    div-double v10, v12, v14

    #@6b
    .line 391
    .local v10, seconds:D
    const-wide/high16 v12, 0x404e

    #@6d
    div-double v12, v4, v12

    #@6f
    add-double/2addr v12, v1

    #@70
    const-wide v14, 0x40ac200000000000L

    #@75
    div-double v14, v10, v14

    #@77
    add-double v8, v12, v14

    #@79
    .line 392
    .local v8, result:D
    const-string v12, "S"

    #@7b
    move-object/from16 v0, p1

    #@7d
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@80
    move-result v12

    #@81
    if-nez v12, :cond_8d

    #@83
    const-string v12, "W"

    #@85
    move-object/from16 v0, p1

    #@87
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_8a
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_8a} :catch_92
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_8a} :catch_99

    #@8a
    move-result v12

    #@8b
    if-eqz v12, :cond_90

    #@8d
    .line 393
    :cond_8d
    neg-double v12, v8

    #@8e
    double-to-float v12, v12

    #@8f
    .line 395
    :goto_8f
    return v12

    #@90
    :cond_90
    double-to-float v12, v8

    #@91
    goto :goto_8f

    #@92
    .line 396
    .end local v1           #degrees:D
    .end local v4           #minutes:D
    .end local v6           #pair:[Ljava/lang/String;
    .end local v7           #parts:[Ljava/lang/String;
    .end local v8           #result:D
    .end local v10           #seconds:D
    :catch_92
    move-exception v3

    #@93
    .line 398
    .local v3, e:Ljava/lang/NumberFormatException;
    new-instance v12, Ljava/lang/IllegalArgumentException;

    #@95
    invoke-direct {v12}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@98
    throw v12

    #@99
    .line 399
    .end local v3           #e:Ljava/lang/NumberFormatException;
    :catch_99
    move-exception v3

    #@9a
    .line 401
    .local v3, e:Ljava/lang/ArrayIndexOutOfBoundsException;
    new-instance v12, Ljava/lang/IllegalArgumentException;

    #@9c
    invoke-direct {v12}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@9f
    throw v12
.end method

.method private native getAttributesNative(Ljava/lang/String;)Ljava/lang/String;
.end method

.method private native getThumbnailNative(Ljava/lang/String;)[B
.end method

.method private loadAttributes()V
    .registers 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v11, 0x20

    #@2
    .line 200
    new-instance v9, Ljava/util/HashMap;

    #@4
    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    #@7
    iput-object v9, p0, Landroid/media/ExifInterface;->mAttributes:Ljava/util/HashMap;

    #@9
    .line 203
    sget-object v10, Landroid/media/ExifInterface;->sLock:Ljava/lang/Object;

    #@b
    monitor-enter v10

    #@c
    .line 204
    :try_start_c
    iget-object v9, p0, Landroid/media/ExifInterface;->mFilename:Ljava/lang/String;

    #@e
    invoke-direct {p0, v9}, Landroid/media/ExifInterface;->getAttributesNative(Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v2

    #@12
    .line 205
    .local v2, attrStr:Ljava/lang/String;
    monitor-exit v10
    :try_end_13
    .catchall {:try_start_c .. :try_end_13} :catchall_5a

    #@13
    .line 208
    invoke-virtual {v2, v11}, Ljava/lang/String;->indexOf(I)I

    #@16
    move-result v8

    #@17
    .line 209
    .local v8, ptr:I
    const/4 v9, 0x0

    #@18
    invoke-virtual {v2, v9, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1b
    move-result-object v9

    #@1c
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1f
    move-result v4

    #@20
    .line 211
    .local v4, count:I
    add-int/lit8 v8, v8, 0x1

    #@22
    .line 213
    const/4 v6, 0x0

    #@23
    .local v6, i:I
    :goto_23
    if-ge v6, v4, :cond_63

    #@25
    .line 215
    const/16 v9, 0x3d

    #@27
    invoke-virtual {v2, v9, v8}, Ljava/lang/String;->indexOf(II)I

    #@2a
    move-result v5

    #@2b
    .line 216
    .local v5, equalPos:I
    invoke-virtual {v2, v8, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    .line 217
    .local v1, attrName:Ljava/lang/String;
    add-int/lit8 v8, v5, 0x1

    #@31
    .line 220
    invoke-virtual {v2, v11, v8}, Ljava/lang/String;->indexOf(II)I

    #@34
    move-result v7

    #@35
    .line 221
    .local v7, lenPos:I
    invoke-virtual {v2, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@38
    move-result-object v9

    #@39
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@3c
    move-result v0

    #@3d
    .line 222
    .local v0, attrLen:I
    add-int/lit8 v8, v7, 0x1

    #@3f
    .line 225
    add-int v9, v8, v0

    #@41
    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@44
    move-result-object v3

    #@45
    .line 226
    .local v3, attrValue:Ljava/lang/String;
    add-int/2addr v8, v0

    #@46
    .line 228
    const-string v9, "hasThumbnail"

    #@48
    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4b
    move-result v9

    #@4c
    if-eqz v9, :cond_5d

    #@4e
    .line 229
    const-string/jumbo v9, "true"

    #@51
    invoke-virtual {v3, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@54
    move-result v9

    #@55
    iput-boolean v9, p0, Landroid/media/ExifInterface;->mHasThumbnail:Z

    #@57
    .line 213
    :goto_57
    add-int/lit8 v6, v6, 0x1

    #@59
    goto :goto_23

    #@5a
    .line 205
    .end local v0           #attrLen:I
    .end local v1           #attrName:Ljava/lang/String;
    .end local v2           #attrStr:Ljava/lang/String;
    .end local v3           #attrValue:Ljava/lang/String;
    .end local v4           #count:I
    .end local v5           #equalPos:I
    .end local v6           #i:I
    .end local v7           #lenPos:I
    .end local v8           #ptr:I
    :catchall_5a
    move-exception v9

    #@5b
    :try_start_5b
    monitor-exit v10
    :try_end_5c
    .catchall {:try_start_5b .. :try_end_5c} :catchall_5a

    #@5c
    throw v9

    #@5d
    .line 231
    .restart local v0       #attrLen:I
    .restart local v1       #attrName:Ljava/lang/String;
    .restart local v2       #attrStr:Ljava/lang/String;
    .restart local v3       #attrValue:Ljava/lang/String;
    .restart local v4       #count:I
    .restart local v5       #equalPos:I
    .restart local v6       #i:I
    .restart local v7       #lenPos:I
    .restart local v8       #ptr:I
    :cond_5d
    iget-object v9, p0, Landroid/media/ExifInterface;->mAttributes:Ljava/util/HashMap;

    #@5f
    invoke-virtual {v9, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@62
    goto :goto_57

    #@63
    .line 234
    .end local v0           #attrLen:I
    .end local v1           #attrName:Ljava/lang/String;
    .end local v3           #attrValue:Ljava/lang/String;
    .end local v5           #equalPos:I
    .end local v7           #lenPos:I
    :cond_63
    return-void
.end method

.method private native saveAttributesNative(Ljava/lang/String;Ljava/lang/String;)V
.end method


# virtual methods
.method public getAltitude(D)D
    .registers 11
    .parameter "defaultValue"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, -0x1

    #@2
    .line 321
    const-string v5, "GPSAltitude"

    #@4
    const-wide/high16 v6, -0x4010

    #@6
    invoke-virtual {p0, v5, v6, v7}, Landroid/media/ExifInterface;->getAttributeDouble(Ljava/lang/String;D)D

    #@9
    move-result-wide v0

    #@a
    .line 322
    .local v0, altitude:D
    const-string v5, "GPSAltitudeRef"

    #@c
    invoke-virtual {p0, v5, v3}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    #@f
    move-result v2

    #@10
    .line 324
    .local v2, ref:I
    const-wide/16 v5, 0x0

    #@12
    cmpl-double v5, v0, v5

    #@14
    if-ltz v5, :cond_1d

    #@16
    if-ltz v2, :cond_1d

    #@18
    .line 325
    if-ne v2, v4, :cond_1e

    #@1a
    :goto_1a
    int-to-double v3, v3

    #@1b
    mul-double p1, v0, v3

    #@1d
    .line 327
    .end local p1
    :cond_1d
    return-wide p1

    #@1e
    .restart local p1
    :cond_1e
    move v3, v4

    #@1f
    .line 325
    goto :goto_1a
.end method

.method public getAttribute(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "tag"

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Landroid/media/ExifInterface;->mAttributes:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/lang/String;

    #@8
    return-object v0
.end method

.method public getAttributeDouble(Ljava/lang/String;D)D
    .registers 13
    .parameter "tag"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 161
    iget-object v7, p0, Landroid/media/ExifInterface;->mAttributes:Ljava/util/HashMap;

    #@2
    invoke-virtual {v7, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v6

    #@6
    check-cast v6, Ljava/lang/String;

    #@8
    .line 162
    .local v6, value:Ljava/lang/String;
    if-nez v6, :cond_b

    #@a
    .line 171
    .end local p2
    :cond_a
    :goto_a
    return-wide p2

    #@b
    .line 164
    .restart local p2
    :cond_b
    :try_start_b
    const-string v7, "/"

    #@d
    invoke-virtual {v6, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@10
    move-result v3

    #@11
    .line 165
    .local v3, index:I
    const/4 v7, -0x1

    #@12
    if-eq v3, v7, :cond_a

    #@14
    .line 166
    add-int/lit8 v7, v3, 0x1

    #@16
    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@19
    move-result-object v7

    #@1a
    invoke-static {v7}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    #@1d
    move-result-wide v0

    #@1e
    .line 167
    .local v0, denom:D
    const-wide/16 v7, 0x0

    #@20
    cmpl-double v7, v0, v7

    #@22
    if-eqz v7, :cond_a

    #@24
    .line 168
    const/4 v7, 0x0

    #@25
    invoke-virtual {v6, v7, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@28
    move-result-object v7

    #@29
    invoke-static {v7}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_2c
    .catch Ljava/lang/NumberFormatException; {:try_start_b .. :try_end_2c} :catch_30

    #@2c
    move-result-wide v4

    #@2d
    .line 169
    .local v4, num:D
    div-double p2, v4, v0

    #@2f
    goto :goto_a

    #@30
    .line 170
    .end local v0           #denom:D
    .end local v3           #index:I
    .end local v4           #num:D
    :catch_30
    move-exception v2

    #@31
    .line 171
    .local v2, ex:Ljava/lang/NumberFormatException;
    goto :goto_a
.end method

.method public getAttributeInt(Ljava/lang/String;I)I
    .registers 6
    .parameter "tag"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 143
    iget-object v2, p0, Landroid/media/ExifInterface;->mAttributes:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Ljava/lang/String;

    #@8
    .line 144
    .local v1, value:Ljava/lang/String;
    if-nez v1, :cond_b

    #@a
    .line 148
    .end local p2
    :goto_a
    return p2

    #@b
    .line 146
    .restart local p2
    :cond_b
    :try_start_b
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_12
    .catch Ljava/lang/NumberFormatException; {:try_start_b .. :try_end_12} :catch_14

    #@12
    move-result p2

    #@13
    goto :goto_a

    #@14
    .line 147
    :catch_14
    move-exception v0

    #@15
    .line 148
    .local v0, ex:Ljava/lang/NumberFormatException;
    goto :goto_a
.end method

.method public getDateTime()J
    .registers 9

    #@0
    .prologue
    const-wide/16 v4, -0x1

    #@2
    .line 337
    iget-object v6, p0, Landroid/media/ExifInterface;->mAttributes:Ljava/util/HashMap;

    #@4
    const-string v7, "DateTime"

    #@6
    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Ljava/lang/String;

    #@c
    .line 338
    .local v0, dateTimeString:Ljava/lang/String;
    if-nez v0, :cond_f

    #@e
    .line 346
    :cond_e
    :goto_e
    return-wide v4

    #@f
    .line 340
    :cond_f
    new-instance v3, Ljava/text/ParsePosition;

    #@11
    const/4 v6, 0x0

    #@12
    invoke-direct {v3, v6}, Ljava/text/ParsePosition;-><init>(I)V

    #@15
    .line 342
    .local v3, pos:Ljava/text/ParsePosition;
    :try_start_15
    sget-object v6, Landroid/media/ExifInterface;->sFormatter:Ljava/text/SimpleDateFormat;

    #@17
    invoke-virtual {v6, v0, v3}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/util/Date;

    #@1a
    move-result-object v1

    #@1b
    .line 343
    .local v1, datetime:Ljava/util/Date;
    if-eqz v1, :cond_e

    #@1d
    .line 344
    invoke-virtual {v1}, Ljava/util/Date;->getTime()J
    :try_end_20
    .catch Ljava/lang/IllegalArgumentException; {:try_start_15 .. :try_end_20} :catch_22

    #@20
    move-result-wide v4

    #@21
    goto :goto_e

    #@22
    .line 345
    .end local v1           #datetime:Ljava/util/Date;
    :catch_22
    move-exception v2

    #@23
    .line 346
    .local v2, ex:Ljava/lang/IllegalArgumentException;
    goto :goto_e
.end method

.method public getGpsDateTime()J
    .registers 11

    #@0
    .prologue
    const-wide/16 v6, -0x1

    #@2
    .line 356
    iget-object v8, p0, Landroid/media/ExifInterface;->mAttributes:Ljava/util/HashMap;

    #@4
    const-string v9, "GPSDateStamp"

    #@6
    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Ljava/lang/String;

    #@c
    .line 357
    .local v0, date:Ljava/lang/String;
    iget-object v8, p0, Landroid/media/ExifInterface;->mAttributes:Ljava/util/HashMap;

    #@e
    const-string v9, "GPSTimeStamp"

    #@10
    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@13
    move-result-object v5

    #@14
    check-cast v5, Ljava/lang/String;

    #@16
    .line 358
    .local v5, time:Ljava/lang/String;
    if-eqz v0, :cond_1a

    #@18
    if-nez v5, :cond_1b

    #@1a
    .line 369
    :cond_1a
    :goto_1a
    return-wide v6

    #@1b
    .line 360
    :cond_1b
    new-instance v8, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v8

    #@24
    const/16 v9, 0x20

    #@26
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@29
    move-result-object v8

    #@2a
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v8

    #@2e
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v1

    #@32
    .line 361
    .local v1, dateTimeString:Ljava/lang/String;
    if-eqz v1, :cond_1a

    #@34
    .line 363
    new-instance v4, Ljava/text/ParsePosition;

    #@36
    const/4 v8, 0x0

    #@37
    invoke-direct {v4, v8}, Ljava/text/ParsePosition;-><init>(I)V

    #@3a
    .line 365
    .local v4, pos:Ljava/text/ParsePosition;
    :try_start_3a
    sget-object v8, Landroid/media/ExifInterface;->sFormatter:Ljava/text/SimpleDateFormat;

    #@3c
    invoke-virtual {v8, v1, v4}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/util/Date;

    #@3f
    move-result-object v2

    #@40
    .line 366
    .local v2, datetime:Ljava/util/Date;
    if-eqz v2, :cond_1a

    #@42
    .line 367
    invoke-virtual {v2}, Ljava/util/Date;->getTime()J
    :try_end_45
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3a .. :try_end_45} :catch_47

    #@45
    move-result-wide v6

    #@46
    goto :goto_1a

    #@47
    .line 368
    .end local v2           #datetime:Ljava/util/Date;
    :catch_47
    move-exception v3

    #@48
    .line 369
    .local v3, ex:Ljava/lang/IllegalArgumentException;
    goto :goto_1a
.end method

.method public getLatLong([F)Z
    .registers 10
    .parameter "output"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 296
    iget-object v6, p0, Landroid/media/ExifInterface;->mAttributes:Ljava/util/HashMap;

    #@4
    const-string v7, "GPSLatitude"

    #@6
    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Ljava/lang/String;

    #@c
    .line 297
    .local v1, latValue:Ljava/lang/String;
    iget-object v6, p0, Landroid/media/ExifInterface;->mAttributes:Ljava/util/HashMap;

    #@e
    const-string v7, "GPSLatitudeRef"

    #@10
    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Ljava/lang/String;

    #@16
    .line 298
    .local v0, latRef:Ljava/lang/String;
    iget-object v6, p0, Landroid/media/ExifInterface;->mAttributes:Ljava/util/HashMap;

    #@18
    const-string v7, "GPSLongitude"

    #@1a
    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1d
    move-result-object v3

    #@1e
    check-cast v3, Ljava/lang/String;

    #@20
    .line 299
    .local v3, lngValue:Ljava/lang/String;
    iget-object v6, p0, Landroid/media/ExifInterface;->mAttributes:Ljava/util/HashMap;

    #@22
    const-string v7, "GPSLongitudeRef"

    #@24
    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@27
    move-result-object v2

    #@28
    check-cast v2, Ljava/lang/String;

    #@2a
    .line 301
    .local v2, lngRef:Ljava/lang/String;
    if-eqz v1, :cond_42

    #@2c
    if-eqz v0, :cond_42

    #@2e
    if-eqz v3, :cond_42

    #@30
    if-eqz v2, :cond_42

    #@32
    .line 303
    const/4 v6, 0x0

    #@33
    :try_start_33
    invoke-static {v1, v0}, Landroid/media/ExifInterface;->convertRationalLatLonToFloat(Ljava/lang/String;Ljava/lang/String;)F

    #@36
    move-result v7

    #@37
    aput v7, p1, v6

    #@39
    .line 304
    const/4 v6, 0x1

    #@3a
    invoke-static {v3, v2}, Landroid/media/ExifInterface;->convertRationalLatLonToFloat(Ljava/lang/String;Ljava/lang/String;)F

    #@3d
    move-result v7

    #@3e
    aput v7, p1, v6
    :try_end_40
    .catch Ljava/lang/IllegalArgumentException; {:try_start_33 .. :try_end_40} :catch_41

    #@40
    .line 311
    :goto_40
    return v4

    #@41
    .line 306
    :catch_41
    move-exception v4

    #@42
    :cond_42
    move v4, v5

    #@43
    .line 311
    goto :goto_40
.end method

.method public getThumbnail()[B
    .registers 3

    #@0
    .prologue
    .line 285
    sget-object v1, Landroid/media/ExifInterface;->sLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 286
    :try_start_3
    iget-object v0, p0, Landroid/media/ExifInterface;->mFilename:Ljava/lang/String;

    #@5
    invoke-direct {p0, v0}, Landroid/media/ExifInterface;->getThumbnailNative(Ljava/lang/String;)[B

    #@8
    move-result-object v0

    #@9
    monitor-exit v1

    #@a
    return-object v0

    #@b
    .line 287
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method public hasThumbnail()Z
    .registers 2

    #@0
    .prologue
    .line 276
    iget-boolean v0, p0, Landroid/media/ExifInterface;->mHasThumbnail:Z

    #@2
    return v0
.end method

.method public saveAttributes()V
    .registers 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 248
    new-instance v4, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 249
    .local v4, sb:Ljava/lang/StringBuilder;
    iget-object v7, p0, Landroid/media/ExifInterface;->mAttributes:Ljava/util/HashMap;

    #@7
    invoke-virtual {v7}, Ljava/util/HashMap;->size()I

    #@a
    move-result v5

    #@b
    .line 250
    .local v5, size:I
    iget-object v7, p0, Landroid/media/ExifInterface;->mAttributes:Ljava/util/HashMap;

    #@d
    const-string v8, "hasThumbnail"

    #@f
    invoke-virtual {v7, v8}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@12
    move-result v7

    #@13
    if-eqz v7, :cond_17

    #@15
    .line 251
    add-int/lit8 v5, v5, -0x1

    #@17
    .line 253
    :cond_17
    new-instance v7, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v7

    #@20
    const-string v8, " "

    #@22
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v7

    #@26
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v7

    #@2a
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    .line 254
    iget-object v7, p0, Landroid/media/ExifInterface;->mAttributes:Ljava/util/HashMap;

    #@2f
    invoke-virtual {v7}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@32
    move-result-object v7

    #@33
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@36
    move-result-object v0

    #@37
    .local v0, i$:Ljava/util/Iterator;
    :cond_37
    :goto_37
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@3a
    move-result v7

    #@3b
    if-eqz v7, :cond_8b

    #@3d
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@40
    move-result-object v1

    #@41
    check-cast v1, Ljava/util/Map$Entry;

    #@43
    .line 255
    .local v1, iter:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@46
    move-result-object v2

    #@47
    check-cast v2, Ljava/lang/String;

    #@49
    .line 256
    .local v2, key:Ljava/lang/String;
    const-string v7, "hasThumbnail"

    #@4b
    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4e
    move-result v7

    #@4f
    if-nez v7, :cond_37

    #@51
    .line 260
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@54
    move-result-object v6

    #@55
    check-cast v6, Ljava/lang/String;

    #@57
    .line 261
    .local v6, val:Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v7

    #@60
    const-string v8, "="

    #@62
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v7

    #@66
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v7

    #@6a
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    .line 262
    new-instance v7, Ljava/lang/StringBuilder;

    #@6f
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@72
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@75
    move-result v8

    #@76
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@79
    move-result-object v7

    #@7a
    const-string v8, " "

    #@7c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v7

    #@80
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v7

    #@84
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    .line 263
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    goto :goto_37

    #@8b
    .line 265
    .end local v1           #iter:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2           #key:Ljava/lang/String;
    .end local v6           #val:Ljava/lang/String;
    :cond_8b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v3

    #@8f
    .line 266
    .local v3, s:Ljava/lang/String;
    sget-object v8, Landroid/media/ExifInterface;->sLock:Ljava/lang/Object;

    #@91
    monitor-enter v8

    #@92
    .line 267
    :try_start_92
    iget-object v7, p0, Landroid/media/ExifInterface;->mFilename:Ljava/lang/String;

    #@94
    invoke-direct {p0, v7, v3}, Landroid/media/ExifInterface;->saveAttributesNative(Ljava/lang/String;Ljava/lang/String;)V

    #@97
    .line 268
    iget-object v7, p0, Landroid/media/ExifInterface;->mFilename:Ljava/lang/String;

    #@99
    invoke-direct {p0, v7}, Landroid/media/ExifInterface;->commitChangesNative(Ljava/lang/String;)V

    #@9c
    .line 269
    monitor-exit v8

    #@9d
    .line 270
    return-void

    #@9e
    .line 269
    :catchall_9e
    move-exception v7

    #@9f
    monitor-exit v8
    :try_end_a0
    .catchall {:try_start_92 .. :try_end_a0} :catchall_9e

    #@a0
    throw v7
.end method

.method public setAttribute(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "tag"
    .parameter "value"

    #@0
    .prologue
    .line 182
    iget-object v0, p0, Landroid/media/ExifInterface;->mAttributes:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    .line 183
    return-void
.end method
