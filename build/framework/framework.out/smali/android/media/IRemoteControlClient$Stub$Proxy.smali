.class Landroid/media/IRemoteControlClient$Stub$Proxy;
.super Ljava/lang/Object;
.source "IRemoteControlClient.java"

# interfaces
.implements Landroid/media/IRemoteControlClient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/IRemoteControlClient$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 97
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 98
    iput-object p1, p0, Landroid/media/IRemoteControlClient$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 99
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 102
    iget-object v0, p0, Landroid/media/IRemoteControlClient$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 106
    const-string v0, "android.media.IRemoteControlClient"

    #@2
    return-object v0
.end method

.method public onInformationRequested(IIII)V
    .registers 10
    .parameter "generationId"
    .parameter "infoFlags"
    .parameter "artWidth"
    .parameter "artHeight"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 122
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 124
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.media.IRemoteControlClient"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 125
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 126
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 127
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 128
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 129
    iget-object v1, p0, Landroid/media/IRemoteControlClient$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@17
    const/4 v2, 0x1

    #@18
    const/4 v3, 0x0

    #@19
    const/4 v4, 0x1

    #@1a
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1d
    .catchall {:try_start_4 .. :try_end_1d} :catchall_21

    #@1d
    .line 132
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 134
    return-void

    #@21
    .line 132
    :catchall_21
    move-exception v1

    #@22
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    throw v1
.end method

.method public plugRemoteControlDisplay(Landroid/media/IRemoteControlDisplay;)V
    .registers 7
    .parameter "rcd"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 152
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 154
    .local v0, _data:Landroid/os/Parcel;
    :try_start_5
    const-string v2, "android.media.IRemoteControlClient"

    #@7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@a
    .line 155
    if-eqz p1, :cond_10

    #@c
    invoke-interface {p1}, Landroid/media/IRemoteControlDisplay;->asBinder()Landroid/os/IBinder;

    #@f
    move-result-object v1

    #@10
    :cond_10
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@13
    .line 156
    iget-object v1, p0, Landroid/media/IRemoteControlClient$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/4 v2, 0x3

    #@16
    const/4 v3, 0x0

    #@17
    const/4 v4, 0x1

    #@18
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1b
    .catchall {:try_start_5 .. :try_end_1b} :catchall_1f

    #@1b
    .line 159
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 161
    return-void

    #@1f
    .line 159
    :catchall_1f
    move-exception v1

    #@20
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@23
    throw v1
.end method

.method public setCurrentClientGenerationId(I)V
    .registers 7
    .parameter "clientGeneration"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 140
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 142
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.media.IRemoteControlClient"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 143
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 144
    iget-object v1, p0, Landroid/media/IRemoteControlClient$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@e
    const/4 v2, 0x2

    #@f
    const/4 v3, 0x0

    #@10
    const/4 v4, 0x1

    #@11
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_14
    .catchall {:try_start_4 .. :try_end_14} :catchall_18

    #@14
    .line 147
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@17
    .line 149
    return-void

    #@18
    .line 147
    :catchall_18
    move-exception v1

    #@19
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1c
    throw v1
.end method

.method public unplugRemoteControlDisplay(Landroid/media/IRemoteControlDisplay;)V
    .registers 7
    .parameter "rcd"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 164
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 166
    .local v0, _data:Landroid/os/Parcel;
    :try_start_5
    const-string v2, "android.media.IRemoteControlClient"

    #@7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@a
    .line 167
    if-eqz p1, :cond_10

    #@c
    invoke-interface {p1}, Landroid/media/IRemoteControlDisplay;->asBinder()Landroid/os/IBinder;

    #@f
    move-result-object v1

    #@10
    :cond_10
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@13
    .line 168
    iget-object v1, p0, Landroid/media/IRemoteControlClient$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/4 v2, 0x4

    #@16
    const/4 v3, 0x0

    #@17
    const/4 v4, 0x1

    #@18
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1b
    .catchall {:try_start_5 .. :try_end_1b} :catchall_1f

    #@1b
    .line 171
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 173
    return-void

    #@1f
    .line 171
    :catchall_1f
    move-exception v1

    #@20
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@23
    throw v1
.end method
