.class public Landroid/media/Ringtone;
.super Ljava/lang/Object;
.source "Ringtone.java"


# static fields
.field private static final CUPSS_ALARM_FILEPATH:Ljava/lang/String; = null

.field private static final CUPSS_NOTIFICATION_FILEPATH:Ljava/lang/String; = null

.field private static final CUPSS_RINGTONE_FILEPATH:Ljava/lang/String; = null

.field public static final DEFAULT_ALARMS_FILEPATH:Ljava/lang/String; = null

.field public static final DEFAULT_NOTIFICATIONS_FILEPATH:Ljava/lang/String; = null

.field public static final DEFAULT_RINGTONES_FILEPATH:Ljava/lang/String; = null

.field private static final DEFAULT_RINGTONE_PROPERTY_PREFIX:Ljava/lang/String; = "ro.config."

.field private static final DRM_COLUMNS:[Ljava/lang/String; = null

.field private static final LOGD:Z = true

.field private static final MEDIA_COLUMNS:[Ljava/lang/String; = null

.field private static final TAG:Ljava/lang/String; = "Ringtone"


# instance fields
.field errorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private final mAllowRemote:Z

.field private final mAudioManager:Landroid/media/AudioManager;

.field private final mContext:Landroid/content/Context;

.field private mDrmPath:Ljava/lang/String;

.field private mErrorCheck:Z

.field private mIsSmartRingtoneOnNoiseEstimation:Z

.field private mIsSoundException:Z

.field private mLocalPlayer:Landroid/media/MediaPlayer;

.field private final mRemotePlayer:Landroid/media/IRingtonePlayer;

.field private final mRemoteToken:Landroid/os/Binder;

.field private mSmartRingtone:Landroid/media/SmartRingtone;

.field private mStreamType:I

.field private mTitle:Ljava/lang/String;

.field private mUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/4 v4, 0x2

    #@2
    const/4 v3, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    .line 59
    new-array v0, v5, [Ljava/lang/String;

    #@6
    const-string v1, "_id"

    #@8
    aput-object v1, v0, v2

    #@a
    const-string v1, "_data"

    #@c
    aput-object v1, v0, v3

    #@e
    const-string/jumbo v1, "title"

    #@11
    aput-object v1, v0, v4

    #@13
    sput-object v0, Landroid/media/Ringtone;->MEDIA_COLUMNS:[Ljava/lang/String;

    #@15
    .line 65
    new-array v0, v5, [Ljava/lang/String;

    #@17
    const-string v1, "_id"

    #@19
    aput-object v1, v0, v2

    #@1b
    const-string v1, "_data"

    #@1d
    aput-object v1, v0, v3

    #@1f
    const-string/jumbo v1, "title"

    #@22
    aput-object v1, v0, v4

    #@24
    sput-object v0, Landroid/media/Ringtone;->DRM_COLUMNS:[Ljava/lang/String;

    #@26
    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v1, "/system/media/audio/ringtones/"

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v0

    #@31
    const-string/jumbo v1, "ro.config.ringtone"

    #@34
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@37
    move-result-object v1

    #@38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v0

    #@3c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v0

    #@40
    sput-object v0, Landroid/media/Ringtone;->DEFAULT_RINGTONES_FILEPATH:Ljava/lang/String;

    #@42
    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v1, "/system/media/audio/notifications/"

    #@49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    const-string/jumbo v1, "ro.config.notification_sound"

    #@50
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@53
    move-result-object v1

    #@54
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v0

    #@58
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v0

    #@5c
    sput-object v0, Landroid/media/Ringtone;->DEFAULT_NOTIFICATIONS_FILEPATH:Ljava/lang/String;

    #@5e
    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string v1, "/system/media/audio/alarms/"

    #@65
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v0

    #@69
    const-string/jumbo v1, "ro.config.alarm_alert"

    #@6c
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6f
    move-result-object v1

    #@70
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v0

    #@74
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v0

    #@78
    sput-object v0, Landroid/media/Ringtone;->DEFAULT_ALARMS_FILEPATH:Ljava/lang/String;

    #@7a
    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    const-string/jumbo v1, "ro.lge.capp_cupss.rootdir"

    #@82
    const-string v2, "/cust"

    #@84
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@87
    move-result-object v1

    #@88
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v0

    #@8c
    const-string v1, "/media/audio/ringtones/"

    #@8e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v0

    #@92
    const-string/jumbo v1, "ro.config.ringtone"

    #@95
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@98
    move-result-object v1

    #@99
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v0

    #@9d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v0

    #@a1
    sput-object v0, Landroid/media/Ringtone;->CUPSS_RINGTONE_FILEPATH:Ljava/lang/String;

    #@a3
    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    #@a5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@a8
    const-string/jumbo v1, "ro.lge.capp_cupss.rootdir"

    #@ab
    const-string v2, "/cust"

    #@ad
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@b0
    move-result-object v1

    #@b1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v0

    #@b5
    const-string v1, "/media/audio/alarms/"

    #@b7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v0

    #@bb
    const-string/jumbo v1, "ro.config.alarm_alert"

    #@be
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@c1
    move-result-object v1

    #@c2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v0

    #@c6
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c9
    move-result-object v0

    #@ca
    sput-object v0, Landroid/media/Ringtone;->CUPSS_ALARM_FILEPATH:Ljava/lang/String;

    #@cc
    .line 116
    new-instance v0, Ljava/lang/StringBuilder;

    #@ce
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d1
    const-string/jumbo v1, "ro.lge.capp_cupss.rootdir"

    #@d4
    const-string v2, "/cust"

    #@d6
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@d9
    move-result-object v1

    #@da
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v0

    #@de
    const-string v1, "/media/audio/notifications/"

    #@e0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e3
    move-result-object v0

    #@e4
    const-string/jumbo v1, "ro.config.notification_sound"

    #@e7
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@ea
    move-result-object v1

    #@eb
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v0

    #@ef
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f2
    move-result-object v0

    #@f3
    sput-object v0, Landroid/media/Ringtone;->CUPSS_NOTIFICATION_FILEPATH:Ljava/lang/String;

    #@f5
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .registers 9
    .parameter "context"
    .parameter "allowRemote"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v5, 0x0

    #@2
    .line 126
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 78
    iput-boolean v5, p0, Landroid/media/Ringtone;->mErrorCheck:Z

    #@7
    .line 83
    const/4 v1, 0x2

    #@8
    iput v1, p0, Landroid/media/Ringtone;->mStreamType:I

    #@a
    .line 86
    iput-object v2, p0, Landroid/media/Ringtone;->mDrmPath:Ljava/lang/String;

    #@c
    .line 91
    iput-boolean v5, p0, Landroid/media/Ringtone;->mIsSoundException:Z

    #@e
    .line 121
    iput-object v2, p0, Landroid/media/Ringtone;->mSmartRingtone:Landroid/media/SmartRingtone;

    #@10
    .line 122
    iput-boolean v5, p0, Landroid/media/Ringtone;->mIsSmartRingtoneOnNoiseEstimation:Z

    #@12
    .line 768
    new-instance v1, Landroid/media/Ringtone$1;

    #@14
    invoke-direct {v1, p0}, Landroid/media/Ringtone$1;-><init>(Landroid/media/Ringtone;)V

    #@17
    iput-object v1, p0, Landroid/media/Ringtone;->errorListener:Landroid/media/MediaPlayer$OnErrorListener;

    #@19
    .line 127
    iput-object p1, p0, Landroid/media/Ringtone;->mContext:Landroid/content/Context;

    #@1b
    .line 128
    iget-object v1, p0, Landroid/media/Ringtone;->mContext:Landroid/content/Context;

    #@1d
    const-string v3, "audio"

    #@1f
    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@22
    move-result-object v1

    #@23
    check-cast v1, Landroid/media/AudioManager;

    #@25
    iput-object v1, p0, Landroid/media/Ringtone;->mAudioManager:Landroid/media/AudioManager;

    #@27
    .line 129
    iput-boolean p2, p0, Landroid/media/Ringtone;->mAllowRemote:Z

    #@29
    .line 130
    if-eqz p2, :cond_57

    #@2b
    iget-object v1, p0, Landroid/media/Ringtone;->mAudioManager:Landroid/media/AudioManager;

    #@2d
    invoke-virtual {v1}, Landroid/media/AudioManager;->getRingtonePlayer()Landroid/media/IRingtonePlayer;

    #@30
    move-result-object v1

    #@31
    :goto_31
    iput-object v1, p0, Landroid/media/Ringtone;->mRemotePlayer:Landroid/media/IRingtonePlayer;

    #@33
    .line 131
    if-eqz p2, :cond_59

    #@35
    new-instance v1, Landroid/os/Binder;

    #@37
    invoke-direct {v1}, Landroid/os/Binder;-><init>()V

    #@3a
    :goto_3a
    iput-object v1, p0, Landroid/media/Ringtone;->mRemoteToken:Landroid/os/Binder;

    #@3c
    .line 133
    iput-boolean v5, p0, Landroid/media/Ringtone;->mErrorCheck:Z

    #@3e
    .line 136
    iget-object v1, p0, Landroid/media/Ringtone;->mSmartRingtone:Landroid/media/SmartRingtone;

    #@40
    if-nez v1, :cond_4d

    #@42
    .line 138
    :try_start_42
    new-instance v1, Landroid/media/SmartRingtone;

    #@44
    iget-object v3, p0, Landroid/media/Ringtone;->mAudioManager:Landroid/media/AudioManager;

    #@46
    iget-object v4, p0, Landroid/media/Ringtone;->mContext:Landroid/content/Context;

    #@48
    invoke-direct {v1, v3, v4}, Landroid/media/SmartRingtone;-><init>(Landroid/media/AudioManager;Landroid/content/Context;)V

    #@4b
    iput-object v1, p0, Landroid/media/Ringtone;->mSmartRingtone:Landroid/media/SmartRingtone;
    :try_end_4d
    .catch Ljava/lang/RuntimeException; {:try_start_42 .. :try_end_4d} :catch_5b

    #@4d
    .line 147
    :cond_4d
    :goto_4d
    const-string/jumbo v1, "ro.lge.audio_soundexception"

    #@50
    invoke-static {v1, v5}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@53
    move-result v1

    #@54
    iput-boolean v1, p0, Landroid/media/Ringtone;->mIsSoundException:Z

    #@56
    .line 149
    return-void

    #@57
    :cond_57
    move-object v1, v2

    #@58
    .line 130
    goto :goto_31

    #@59
    :cond_59
    move-object v1, v2

    #@5a
    .line 131
    goto :goto_3a

    #@5b
    .line 139
    :catch_5b
    move-exception v0

    #@5c
    .line 140
    .local v0, e:Ljava/lang/RuntimeException;
    const-string v1, "Ringtone"

    #@5e
    const-string v3, "Couldn\'t instantiate SmartRingtone"

    #@60
    invoke-static {v1, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@63
    .line 141
    iput-object v2, p0, Landroid/media/Ringtone;->mSmartRingtone:Landroid/media/SmartRingtone;

    #@65
    goto :goto_4d
.end method

.method public constructor <init>(Landroid/content/Context;ZI)V
    .registers 8
    .parameter "context"
    .parameter "allowRemote"
    .parameter "streamType"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    .line 153
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 78
    iput-boolean v3, p0, Landroid/media/Ringtone;->mErrorCheck:Z

    #@7
    .line 83
    const/4 v0, 0x2

    #@8
    iput v0, p0, Landroid/media/Ringtone;->mStreamType:I

    #@a
    .line 86
    iput-object v1, p0, Landroid/media/Ringtone;->mDrmPath:Ljava/lang/String;

    #@c
    .line 91
    iput-boolean v3, p0, Landroid/media/Ringtone;->mIsSoundException:Z

    #@e
    .line 121
    iput-object v1, p0, Landroid/media/Ringtone;->mSmartRingtone:Landroid/media/SmartRingtone;

    #@10
    .line 122
    iput-boolean v3, p0, Landroid/media/Ringtone;->mIsSmartRingtoneOnNoiseEstimation:Z

    #@12
    .line 768
    new-instance v0, Landroid/media/Ringtone$1;

    #@14
    invoke-direct {v0, p0}, Landroid/media/Ringtone$1;-><init>(Landroid/media/Ringtone;)V

    #@17
    iput-object v0, p0, Landroid/media/Ringtone;->errorListener:Landroid/media/MediaPlayer$OnErrorListener;

    #@19
    .line 154
    iput-object p1, p0, Landroid/media/Ringtone;->mContext:Landroid/content/Context;

    #@1b
    .line 155
    iget-object v0, p0, Landroid/media/Ringtone;->mContext:Landroid/content/Context;

    #@1d
    const-string v2, "audio"

    #@1f
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@22
    move-result-object v0

    #@23
    check-cast v0, Landroid/media/AudioManager;

    #@25
    iput-object v0, p0, Landroid/media/Ringtone;->mAudioManager:Landroid/media/AudioManager;

    #@27
    .line 156
    iput-boolean p2, p0, Landroid/media/Ringtone;->mAllowRemote:Z

    #@29
    .line 157
    if-eqz p2, :cond_4a

    #@2b
    iget-object v0, p0, Landroid/media/Ringtone;->mAudioManager:Landroid/media/AudioManager;

    #@2d
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingtonePlayer()Landroid/media/IRingtonePlayer;

    #@30
    move-result-object v0

    #@31
    :goto_31
    iput-object v0, p0, Landroid/media/Ringtone;->mRemotePlayer:Landroid/media/IRingtonePlayer;

    #@33
    .line 158
    if-eqz p2, :cond_3a

    #@35
    new-instance v1, Landroid/os/Binder;

    #@37
    invoke-direct {v1}, Landroid/os/Binder;-><init>()V

    #@3a
    :cond_3a
    iput-object v1, p0, Landroid/media/Ringtone;->mRemoteToken:Landroid/os/Binder;

    #@3c
    .line 159
    iput p3, p0, Landroid/media/Ringtone;->mStreamType:I

    #@3e
    .line 161
    iput-boolean v3, p0, Landroid/media/Ringtone;->mErrorCheck:Z

    #@40
    .line 164
    const-string/jumbo v0, "ro.lge.audio_soundexception"

    #@43
    invoke-static {v0, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@46
    move-result v0

    #@47
    iput-boolean v0, p0, Landroid/media/Ringtone;->mIsSoundException:Z

    #@49
    .line 166
    return-void

    #@4a
    :cond_4a
    move-object v0, v1

    #@4b
    .line 157
    goto :goto_31
.end method

.method static synthetic access$000(Landroid/media/Ringtone;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget-boolean v0, p0, Landroid/media/Ringtone;->mErrorCheck:Z

    #@2
    return v0
.end method

.method static synthetic access$002(Landroid/media/Ringtone;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 55
    iput-boolean p1, p0, Landroid/media/Ringtone;->mErrorCheck:Z

    #@2
    return p1
.end method

.method static synthetic access$100(Landroid/media/Ringtone;)Landroid/media/MediaPlayer;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget-object v0, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@2
    return-object v0
.end method

.method static synthetic access$102(Landroid/media/Ringtone;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 55
    iput-object p1, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Landroid/media/Ringtone;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    invoke-direct {p0}, Landroid/media/Ringtone;->getDefaultPath()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$300(Landroid/media/Ringtone;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget v0, p0, Landroid/media/Ringtone;->mStreamType:I

    #@2
    return v0
.end method

.method static synthetic access$400(Landroid/media/Ringtone;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget-boolean v0, p0, Landroid/media/Ringtone;->mAllowRemote:Z

    #@2
    return v0
.end method

.method static synthetic access$500(Landroid/media/Ringtone;)Landroid/os/Binder;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget-object v0, p0, Landroid/media/Ringtone;->mRemoteToken:Landroid/os/Binder;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Landroid/media/Ringtone;)Landroid/media/IRingtonePlayer;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget-object v0, p0, Landroid/media/Ringtone;->mRemotePlayer:Landroid/media/IRingtonePlayer;

    #@2
    return-object v0
.end method

.method private destroyLocalPlayer()V
    .registers 2

    #@0
    .prologue
    .line 573
    iget-object v0, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@2
    if-eqz v0, :cond_11

    #@4
    .line 574
    iget-object v0, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@6
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    #@9
    .line 575
    iget-object v0, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@b
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    #@e
    .line 576
    const/4 v0, 0x0

    #@f
    iput-object v0, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@11
    .line 578
    :cond_11
    return-void
.end method

.method private getDefaultAudioType()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 675
    const/4 v0, 0x0

    #@1
    .line 677
    .local v0, audioType:Ljava/lang/String;
    iget v1, p0, Landroid/media/Ringtone;->mStreamType:I

    #@3
    packed-switch v1, :pswitch_data_2e

    #@6
    .line 688
    :pswitch_6
    const-string/jumbo v0, "is_ringtone"

    #@9
    .line 693
    :goto_9
    const-string v1, "Ringtone"

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "getDefaultAudioType : "

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 695
    return-object v0

    #@22
    .line 679
    :pswitch_22
    const-string/jumbo v0, "is_ringtone"

    #@25
    .line 680
    goto :goto_9

    #@26
    .line 682
    :pswitch_26
    const-string/jumbo v0, "is_notification"

    #@29
    .line 683
    goto :goto_9

    #@2a
    .line 685
    :pswitch_2a
    const-string/jumbo v0, "is_alarm"

    #@2d
    .line 686
    goto :goto_9

    #@2e
    .line 677
    :pswitch_data_2e
    .packed-switch 0x2
        :pswitch_22
        :pswitch_6
        :pswitch_2a
        :pswitch_26
    .end packed-switch
.end method

.method private getDefaultPath()Ljava/lang/String;
    .registers 9

    #@0
    .prologue
    .line 617
    const/4 v4, 0x0

    #@1
    .line 618
    .local v4, defaultRingtone:Ljava/lang/String;
    iget v5, p0, Landroid/media/Ringtone;->mStreamType:I

    #@3
    packed-switch v5, :pswitch_data_9a

    #@6
    .line 661
    :pswitch_6
    sget-object v4, Landroid/media/Ringtone;->DEFAULT_NOTIFICATIONS_FILEPATH:Ljava/lang/String;

    #@8
    .line 666
    :goto_8
    const-string v5, "Ringtone"

    #@a
    new-instance v6, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v7, "default ringtone path: "

    #@11
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v6

    #@15
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v6

    #@19
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v6

    #@1d
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 668
    return-object v4

    #@21
    .line 621
    :pswitch_21
    iget-object v5, p0, Landroid/media/Ringtone;->mContext:Landroid/content/Context;

    #@23
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@26
    move-result-object v5

    #@27
    const v6, 0x206001c

    #@2a
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@2d
    move-result v0

    #@2e
    .line 623
    .local v0, chameleonSupported:Z
    const-string v5, "Ringtone"

    #@30
    new-instance v6, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v7, "chameleonSupported : "

    #@37
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v6

    #@3b
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v6

    #@3f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v6

    #@43
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 626
    if-eqz v0, :cond_4d

    #@48
    .line 627
    invoke-direct {p0}, Landroid/media/Ringtone;->getDefaultRingtonePathChameleon()Ljava/lang/String;

    #@4b
    move-result-object v4

    #@4c
    goto :goto_8

    #@4d
    .line 630
    :cond_4d
    new-instance v3, Ljava/io/File;

    #@4f
    sget-object v5, Landroid/media/Ringtone;->CUPSS_RINGTONE_FILEPATH:Ljava/lang/String;

    #@51
    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@54
    .line 631
    .local v3, custRingtone:Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    #@57
    move-result v5

    #@58
    if-eqz v5, :cond_63

    #@5a
    invoke-virtual {v3}, Ljava/io/File;->canRead()Z

    #@5d
    move-result v5

    #@5e
    if-eqz v5, :cond_63

    #@60
    .line 632
    sget-object v4, Landroid/media/Ringtone;->CUPSS_RINGTONE_FILEPATH:Ljava/lang/String;

    #@62
    goto :goto_8

    #@63
    .line 635
    :cond_63
    sget-object v4, Landroid/media/Ringtone;->DEFAULT_RINGTONES_FILEPATH:Ljava/lang/String;

    #@65
    goto :goto_8

    #@66
    .line 642
    .end local v0           #chameleonSupported:Z
    .end local v3           #custRingtone:Ljava/io/File;
    :pswitch_66
    new-instance v2, Ljava/io/File;

    #@68
    sget-object v5, Landroid/media/Ringtone;->CUPSS_NOTIFICATION_FILEPATH:Ljava/lang/String;

    #@6a
    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@6d
    .line 643
    .local v2, custNotification:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    #@70
    move-result v5

    #@71
    if-eqz v5, :cond_7c

    #@73
    invoke-virtual {v2}, Ljava/io/File;->canRead()Z

    #@76
    move-result v5

    #@77
    if-eqz v5, :cond_7c

    #@79
    .line 644
    sget-object v4, Landroid/media/Ringtone;->CUPSS_NOTIFICATION_FILEPATH:Ljava/lang/String;

    #@7b
    goto :goto_8

    #@7c
    .line 647
    :cond_7c
    sget-object v4, Landroid/media/Ringtone;->DEFAULT_NOTIFICATIONS_FILEPATH:Ljava/lang/String;

    #@7e
    .line 649
    goto :goto_8

    #@7f
    .line 652
    .end local v2           #custNotification:Ljava/io/File;
    :pswitch_7f
    new-instance v1, Ljava/io/File;

    #@81
    sget-object v5, Landroid/media/Ringtone;->CUPSS_ALARM_FILEPATH:Ljava/lang/String;

    #@83
    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@86
    .line 653
    .local v1, custAlarm:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@89
    move-result v5

    #@8a
    if-eqz v5, :cond_96

    #@8c
    invoke-virtual {v1}, Ljava/io/File;->canRead()Z

    #@8f
    move-result v5

    #@90
    if-eqz v5, :cond_96

    #@92
    .line 654
    sget-object v4, Landroid/media/Ringtone;->CUPSS_ALARM_FILEPATH:Ljava/lang/String;

    #@94
    goto/16 :goto_8

    #@96
    .line 657
    :cond_96
    sget-object v4, Landroid/media/Ringtone;->DEFAULT_ALARMS_FILEPATH:Ljava/lang/String;

    #@98
    .line 659
    goto/16 :goto_8

    #@9a
    .line 618
    :pswitch_data_9a
    .packed-switch 0x2
        :pswitch_21
        :pswitch_6
        :pswitch_7f
        :pswitch_66
    .end packed-switch
.end method

.method private getDefaultRingtonePathChameleon()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    .line 752
    sget-object v2, Landroid/media/Ringtone;->DEFAULT_RINGTONES_FILEPATH:Ljava/lang/String;

    #@2
    .line 753
    .local v2, ringtone:Ljava/lang/String;
    const-string v1, "/carrier/media/ringtones/default_ringer.mp3"

    #@4
    .line 754
    .local v1, carrierRingtonePath:Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    #@6
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@9
    .line 756
    .local v0, carrierRingtone:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@c
    move-result v3

    #@d
    if-eqz v3, :cond_29

    #@f
    .line 757
    const-string v3, "Ringtone"

    #@11
    new-instance v4, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v5, "Ringtone in CP, set to CP tone : "

    #@18
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v4

    #@20
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v4

    #@24
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 758
    move-object v2, v1

    #@28
    .line 764
    :goto_28
    return-object v2

    #@29
    .line 760
    :cond_29
    const-string v3, "Ringtone"

    #@2b
    new-instance v4, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v5, "No Ringtone in CP, set to OEM : "

    #@32
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v4

    #@36
    sget-object v5, Landroid/media/Ringtone;->DEFAULT_RINGTONES_FILEPATH:Ljava/lang/String;

    #@38
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v4

    #@3c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v4

    #@40
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 761
    sget-object v2, Landroid/media/Ringtone;->DEFAULT_RINGTONES_FILEPATH:Ljava/lang/String;

    #@45
    goto :goto_28
.end method

.method private getDefaultTitle(Landroid/content/Context;)Ljava/lang/String;
    .registers 14
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    const/4 v11, 0x1

    #@2
    .line 704
    const/4 v6, 0x0

    #@3
    .line 705
    .local v6, cursor:Landroid/database/Cursor;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v0

    #@7
    .line 707
    .local v0, res:Landroid/content/ContentResolver;
    const/4 v9, 0x0

    #@8
    .line 708
    .local v9, title:Ljava/lang/String;
    const/4 v8, 0x0

    #@9
    .line 709
    .local v8, defaultPath:Ljava/lang/String;
    const/4 v7, 0x0

    #@a
    .line 711
    .local v7, defaultAudioType:Ljava/lang/String;
    iget-object v1, p0, Landroid/media/Ringtone;->mUri:Landroid/net/Uri;

    #@c
    invoke-static {v1}, Landroid/media/RingtoneManager;->getDefaultType(Landroid/net/Uri;)I

    #@f
    move-result v1

    #@10
    if-ne v1, v2, :cond_66

    #@12
    .line 712
    sget-object v8, Landroid/media/Ringtone;->DEFAULT_NOTIFICATIONS_FILEPATH:Ljava/lang/String;

    #@14
    .line 713
    const-string/jumbo v7, "is_notification"

    #@17
    .line 720
    :goto_17
    :try_start_17
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    #@19
    const/4 v2, 0x1

    #@1a
    new-array v2, v2, [Ljava/lang/String;

    #@1c
    const/4 v3, 0x0

    #@1d
    const-string/jumbo v4, "title"

    #@20
    aput-object v4, v2, v3

    #@22
    new-instance v3, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v4, "_data =? AND mime_type =?  AND "

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    const-string v4, " =? "

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v3

    #@3b
    const/4 v4, 0x3

    #@3c
    new-array v4, v4, [Ljava/lang/String;

    #@3e
    const/4 v5, 0x0

    #@3f
    aput-object v8, v4, v5

    #@41
    const/4 v5, 0x1

    #@42
    const-string v10, "application/ogg"

    #@44
    aput-object v10, v4, v5

    #@46
    const/4 v5, 0x2

    #@47
    const-string v10, "1"

    #@49
    aput-object v10, v4, v5

    #@4b
    const/4 v5, 0x0

    #@4c
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_4f
    .catch Ljava/lang/SecurityException; {:try_start_17 .. :try_end_4f} :catch_85

    #@4f
    move-result-object v6

    #@50
    .line 731
    :goto_50
    if-eqz v6, :cond_6f

    #@52
    :try_start_52
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    #@55
    move-result v1

    #@56
    if-ne v1, v11, :cond_6f

    #@58
    .line 733
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@5b
    .line 734
    const/4 v1, 0x0

    #@5c
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_5f
    .catchall {:try_start_52 .. :try_end_5f} :catchall_7e

    #@5f
    move-result-object v9

    #@60
    .line 740
    :goto_60
    if-eqz v6, :cond_65

    #@62
    .line 741
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@65
    .line 745
    :cond_65
    return-object v9

    #@66
    .line 715
    :cond_66
    invoke-direct {p0}, Landroid/media/Ringtone;->getDefaultPath()Ljava/lang/String;

    #@69
    move-result-object v8

    #@6a
    .line 716
    invoke-direct {p0}, Landroid/media/Ringtone;->getDefaultAudioType()Ljava/lang/String;

    #@6d
    move-result-object v7

    #@6e
    goto :goto_17

    #@6f
    .line 736
    :cond_6f
    :try_start_6f
    const-string v1, "Ringtone"

    #@71
    const-string v2, "Default ringtone does NOT exist. "

    #@73
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@76
    .line 737
    const v1, 0x104042e

    #@79
    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_7c
    .catchall {:try_start_6f .. :try_end_7c} :catchall_7e

    #@7c
    move-result-object v9

    #@7d
    goto :goto_60

    #@7e
    .line 740
    :catchall_7e
    move-exception v1

    #@7f
    if-eqz v6, :cond_84

    #@81
    .line 741
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@84
    :cond_84
    throw v1

    #@85
    .line 726
    :catch_85
    move-exception v1

    #@86
    goto :goto_50
.end method

.method private static getTitle(Landroid/content/Context;Landroid/net/Uri;Z)Ljava/lang/String;
    .registers 15
    .parameter "context"
    .parameter "uri"
    .parameter "followSettingsUri"

    #@0
    .prologue
    .line 230
    const/4 v9, 0x0

    #@1
    .line 231
    .local v9, cursor:Landroid/database/Cursor;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4
    move-result-object v0

    #@5
    .line 233
    .local v0, res:Landroid/content/ContentResolver;
    const/4 v11, 0x0

    #@6
    .line 235
    .local v11, title:Ljava/lang/String;
    if-eqz p1, :cond_38

    #@8
    .line 236
    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    #@b
    move-result-object v8

    #@c
    .line 238
    .local v8, authority:Ljava/lang/String;
    const-string/jumbo v1, "settings"

    #@f
    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_48

    #@15
    .line 239
    if-eqz p2, :cond_38

    #@17
    .line 241
    invoke-static {p1}, Landroid/media/RingtoneManager;->getDefaultType(Landroid/net/Uri;)I

    #@1a
    move-result v10

    #@1b
    .line 242
    .local v10, ringToneType:I
    invoke-static {p0, v10}, Landroid/media/RingtoneManager;->getActualDefaultRingtoneUri(Landroid/content/Context;I)Landroid/net/Uri;

    #@1e
    move-result-object v7

    #@1f
    .line 243
    .local v7, actualUri:Landroid/net/Uri;
    const/4 v1, 0x0

    #@20
    invoke-static {p0, v7, v1}, Landroid/media/Ringtone;->getTitle(Landroid/content/Context;Landroid/net/Uri;Z)Ljava/lang/String;

    #@23
    move-result-object v6

    #@24
    .line 245
    .local v6, actualTitle:Ljava/lang/String;
    if-nez v6, :cond_28

    #@26
    .line 246
    const/4 v1, 0x0

    #@27
    .line 300
    .end local v6           #actualTitle:Ljava/lang/String;
    .end local v7           #actualUri:Landroid/net/Uri;
    .end local v8           #authority:Ljava/lang/String;
    .end local v10           #ringToneType:I
    :cond_27
    :goto_27
    return-object v1

    #@28
    .line 249
    .restart local v6       #actualTitle:Ljava/lang/String;
    .restart local v7       #actualUri:Landroid/net/Uri;
    .restart local v8       #authority:Ljava/lang/String;
    .restart local v10       #ringToneType:I
    :cond_28
    const/4 v1, 0x2

    #@29
    if-ne v10, v1, :cond_3a

    #@2b
    .line 250
    const v1, 0x2090303

    #@2e
    const/4 v2, 0x1

    #@2f
    new-array v2, v2, [Ljava/lang/Object;

    #@31
    const/4 v3, 0x0

    #@32
    aput-object v6, v2, v3

    #@34
    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@37
    move-result-object v11

    #@38
    .end local v6           #actualTitle:Ljava/lang/String;
    .end local v7           #actualUri:Landroid/net/Uri;
    .end local v8           #authority:Ljava/lang/String;
    .end local v10           #ringToneType:I
    :cond_38
    :goto_38
    move-object v1, v11

    #@39
    .line 300
    goto :goto_27

    #@3a
    .line 254
    .restart local v6       #actualTitle:Ljava/lang/String;
    .restart local v7       #actualUri:Landroid/net/Uri;
    .restart local v8       #authority:Ljava/lang/String;
    .restart local v10       #ringToneType:I
    :cond_3a
    const v1, 0x104042b

    #@3d
    const/4 v2, 0x1

    #@3e
    new-array v2, v2, [Ljava/lang/Object;

    #@40
    const/4 v3, 0x0

    #@41
    aput-object v6, v2, v3

    #@43
    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@46
    move-result-object v11

    #@47
    goto :goto_38

    #@48
    .line 262
    .end local v6           #actualTitle:Ljava/lang/String;
    .end local v7           #actualUri:Landroid/net/Uri;
    .end local v10           #ringToneType:I
    :cond_48
    :try_start_48
    const-string v1, "drm"

    #@4a
    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4d
    move-result v1

    #@4e
    if-eqz v1, :cond_71

    #@50
    .line 263
    sget-object v2, Landroid/media/Ringtone;->DRM_COLUMNS:[Ljava/lang/String;

    #@52
    const/4 v3, 0x0

    #@53
    const/4 v4, 0x0

    #@54
    const/4 v5, 0x0

    #@55
    move-object v1, p1

    #@56
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_59
    .catch Ljava/lang/SecurityException; {:try_start_48 .. :try_end_59} :catch_93

    #@59
    move-result-object v9

    #@5a
    .line 272
    :cond_5a
    :goto_5a
    if-eqz v9, :cond_85

    #@5c
    :try_start_5c
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    #@5f
    move-result v1

    #@60
    const/4 v2, 0x1

    #@61
    if-ne v1, v2, :cond_85

    #@63
    .line 273
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    #@66
    .line 274
    const/4 v1, 0x2

    #@67
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_6a
    .catchall {:try_start_5c .. :try_end_6a} :catchall_8c

    #@6a
    move-result-object v1

    #@6b
    .line 282
    if-eqz v9, :cond_27

    #@6d
    .line 283
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@70
    goto :goto_27

    #@71
    .line 264
    :cond_71
    :try_start_71
    const-string/jumbo v1, "media"

    #@74
    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@77
    move-result v1

    #@78
    if-eqz v1, :cond_5a

    #@7a
    .line 265
    sget-object v2, Landroid/media/Ringtone;->MEDIA_COLUMNS:[Ljava/lang/String;

    #@7c
    const/4 v3, 0x0

    #@7d
    const/4 v4, 0x0

    #@7e
    const/4 v5, 0x0

    #@7f
    move-object v1, p1

    #@80
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_83
    .catch Ljava/lang/SecurityException; {:try_start_71 .. :try_end_83} :catch_93

    #@83
    move-result-object v9

    #@84
    goto :goto_5a

    #@85
    .line 278
    :cond_85
    const/4 v1, 0x0

    #@86
    .line 282
    if-eqz v9, :cond_27

    #@88
    .line 283
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@8b
    goto :goto_27

    #@8c
    .line 282
    :catchall_8c
    move-exception v1

    #@8d
    if-eqz v9, :cond_92

    #@8f
    .line 283
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@92
    :cond_92
    throw v1

    #@93
    .line 267
    :catch_93
    move-exception v1

    #@94
    goto :goto_5a
.end method


# virtual methods
.method public getStreamType()I
    .registers 2

    #@0
    .prologue
    .line 188
    iget v0, p0, Landroid/media/Ringtone;->mStreamType:I

    #@2
    return v0
.end method

.method public getTitle(Landroid/content/Context;)Ljava/lang/String;
    .registers 7
    .parameter "context"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 198
    iget-object v1, p0, Landroid/media/Ringtone;->mTitle:Ljava/lang/String;

    #@4
    if-eqz v1, :cond_9

    #@6
    iget-object v1, p0, Landroid/media/Ringtone;->mTitle:Ljava/lang/String;

    #@8
    .line 225
    :goto_8
    return-object v1

    #@9
    .line 202
    :cond_9
    iget-object v1, p0, Landroid/media/Ringtone;->mUri:Landroid/net/Uri;

    #@b
    if-eqz v1, :cond_27

    #@d
    iget-object v1, p0, Landroid/media/Ringtone;->mUri:Landroid/net/Uri;

    #@f
    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    const-string v2, ""

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v1

    #@19
    if-eqz v1, :cond_27

    #@1b
    .line 204
    const v1, 0x104042c

    #@1e
    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    iput-object v1, p0, Landroid/media/Ringtone;->mTitle:Ljava/lang/String;

    #@24
    .line 205
    iget-object v1, p0, Landroid/media/Ringtone;->mTitle:Ljava/lang/String;

    #@26
    goto :goto_8

    #@27
    .line 208
    :cond_27
    iget-object v1, p0, Landroid/media/Ringtone;->mUri:Landroid/net/Uri;

    #@29
    invoke-static {p1, v1, v3}, Landroid/media/Ringtone;->getTitle(Landroid/content/Context;Landroid/net/Uri;Z)Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    iput-object v1, p0, Landroid/media/Ringtone;->mTitle:Ljava/lang/String;

    #@2f
    .line 210
    iget-object v1, p0, Landroid/media/Ringtone;->mTitle:Ljava/lang/String;

    #@31
    if-nez v1, :cond_4d

    #@33
    .line 212
    invoke-direct {p0, p1}, Landroid/media/Ringtone;->getDefaultTitle(Landroid/content/Context;)Ljava/lang/String;

    #@36
    move-result-object v0

    #@37
    .line 214
    .local v0, actualTitle:Ljava/lang/String;
    iget-object v1, p0, Landroid/media/Ringtone;->mUri:Landroid/net/Uri;

    #@39
    invoke-static {v1}, Landroid/media/RingtoneManager;->getDefaultType(Landroid/net/Uri;)I

    #@3c
    move-result v1

    #@3d
    const/4 v2, 0x2

    #@3e
    if-ne v1, v2, :cond_50

    #@40
    .line 215
    const v1, 0x2090303

    #@43
    new-array v2, v3, [Ljava/lang/Object;

    #@45
    aput-object v0, v2, v4

    #@47
    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@4a
    move-result-object v1

    #@4b
    iput-object v1, p0, Landroid/media/Ringtone;->mTitle:Ljava/lang/String;

    #@4d
    .line 225
    .end local v0           #actualTitle:Ljava/lang/String;
    :cond_4d
    :goto_4d
    iget-object v1, p0, Landroid/media/Ringtone;->mTitle:Ljava/lang/String;

    #@4f
    goto :goto_8

    #@50
    .line 219
    .restart local v0       #actualTitle:Ljava/lang/String;
    :cond_50
    const v1, 0x104042b

    #@53
    new-array v2, v3, [Ljava/lang/Object;

    #@55
    aput-object v0, v2, v4

    #@57
    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@5a
    move-result-object v1

    #@5b
    iput-object v1, p0, Landroid/media/Ringtone;->mTitle:Ljava/lang/String;

    #@5d
    goto :goto_4d
.end method

.method public getUri()Landroid/net/Uri;
    .registers 2

    #@0
    .prologue
    .line 468
    iget-object v0, p0, Landroid/media/Ringtone;->mUri:Landroid/net/Uri;

    #@2
    return-object v0
.end method

.method public isPlaying()Z
    .registers 6

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 587
    iget-object v2, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@3
    if-eqz v2, :cond_11

    #@5
    iget-boolean v2, p0, Landroid/media/Ringtone;->mErrorCheck:Z

    #@7
    if-eqz v2, :cond_11

    #@9
    .line 588
    const-string v2, "Ringtone"

    #@b
    const-string v3, "Can\'t check isPlaying() during ErrorChecking"

    #@d
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 604
    :goto_10
    return v1

    #@11
    .line 593
    :cond_11
    iget-object v2, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@13
    if-eqz v2, :cond_1c

    #@15
    .line 594
    iget-object v1, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@17
    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    #@1a
    move-result v1

    #@1b
    goto :goto_10

    #@1c
    .line 595
    :cond_1c
    iget-boolean v2, p0, Landroid/media/Ringtone;->mAllowRemote:Z

    #@1e
    if-eqz v2, :cond_43

    #@20
    .line 597
    :try_start_20
    iget-object v2, p0, Landroid/media/Ringtone;->mRemotePlayer:Landroid/media/IRingtonePlayer;

    #@22
    iget-object v3, p0, Landroid/media/Ringtone;->mRemoteToken:Landroid/os/Binder;

    #@24
    invoke-interface {v2, v3}, Landroid/media/IRingtonePlayer;->isPlaying(Landroid/os/IBinder;)Z
    :try_end_27
    .catch Landroid/os/RemoteException; {:try_start_20 .. :try_end_27} :catch_29

    #@27
    move-result v1

    #@28
    goto :goto_10

    #@29
    .line 598
    :catch_29
    move-exception v0

    #@2a
    .line 599
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "Ringtone"

    #@2c
    new-instance v3, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v4, "Problem checking ringtone: "

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v3

    #@3f
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    goto :goto_10

    #@43
    .line 603
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_43
    const-string v2, "Ringtone"

    #@45
    const-string v3, "Neither local nor remote playback available"

    #@47
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    goto :goto_10
.end method

.method public play()V
    .registers 2

    #@0
    .prologue
    .line 496
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Landroid/media/Ringtone;->play(Z)V

    #@4
    .line 497
    return-void
.end method

.method public play(Z)V
    .registers 7
    .parameter "loop"

    #@0
    .prologue
    .line 506
    iget-object v2, p0, Landroid/media/Ringtone;->mAudioManager:Landroid/media/AudioManager;

    #@2
    iget v3, p0, Landroid/media/Ringtone;->mStreamType:I

    #@4
    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->checkPlayConditions(I)Z

    #@7
    move-result v2

    #@8
    if-nez v2, :cond_b

    #@a
    .line 540
    :cond_a
    :goto_a
    return-void

    #@b
    .line 509
    :cond_b
    iget-object v2, p0, Landroid/media/Ringtone;->mSmartRingtone:Landroid/media/SmartRingtone;

    #@d
    if-eqz v2, :cond_19

    #@f
    iget-boolean v2, p0, Landroid/media/Ringtone;->mIsSmartRingtoneOnNoiseEstimation:Z

    #@11
    const/4 v3, 0x1

    #@12
    if-ne v2, v3, :cond_19

    #@14
    .line 510
    iget-object v2, p0, Landroid/media/Ringtone;->mSmartRingtone:Landroid/media/SmartRingtone;

    #@16
    invoke-virtual {v2}, Landroid/media/SmartRingtone;->onExitRecordingLoop()V

    #@19
    .line 514
    :cond_19
    iget-object v2, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@1b
    if-eqz v2, :cond_4f

    #@1d
    .line 518
    iget-object v2, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@1f
    iget-object v3, p0, Landroid/media/Ringtone;->errorListener:Landroid/media/MediaPlayer$OnErrorListener;

    #@21
    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    #@24
    .line 520
    iget-object v2, p0, Landroid/media/Ringtone;->mAudioManager:Landroid/media/AudioManager;

    #@26
    iget v3, p0, Landroid/media/Ringtone;->mStreamType:I

    #@28
    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    #@2b
    move-result v2

    #@2c
    if-eqz v2, :cond_39

    #@2e
    .line 521
    iget-object v2, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@30
    invoke-virtual {v2, p1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    #@33
    .line 522
    iget-object v2, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@35
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->start()V

    #@38
    goto :goto_a

    #@39
    .line 525
    :cond_39
    iget-boolean v2, p0, Landroid/media/Ringtone;->mIsSoundException:Z

    #@3b
    if-eqz v2, :cond_a

    #@3d
    iget v2, p0, Landroid/media/Ringtone;->mStreamType:I

    #@3f
    iget-object v3, p0, Landroid/media/Ringtone;->mAudioManager:Landroid/media/AudioManager;

    #@41
    const/4 v3, 0x2

    #@42
    if-ne v2, v3, :cond_a

    #@44
    .line 526
    iget-object v2, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@46
    invoke-virtual {v2, p1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    #@49
    .line 527
    iget-object v2, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@4b
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->start()V

    #@4e
    goto :goto_a

    #@4f
    .line 530
    :cond_4f
    iget-boolean v2, p0, Landroid/media/Ringtone;->mAllowRemote:Z

    #@51
    if-eqz v2, :cond_7d

    #@53
    .line 531
    iget-object v2, p0, Landroid/media/Ringtone;->mUri:Landroid/net/Uri;

    #@55
    invoke-virtual {v2}, Landroid/net/Uri;->getCanonicalUri()Landroid/net/Uri;

    #@58
    move-result-object v0

    #@59
    .line 533
    .local v0, canonicalUri:Landroid/net/Uri;
    :try_start_59
    iget-object v2, p0, Landroid/media/Ringtone;->mRemotePlayer:Landroid/media/IRingtonePlayer;

    #@5b
    iget-object v3, p0, Landroid/media/Ringtone;->mRemoteToken:Landroid/os/Binder;

    #@5d
    iget v4, p0, Landroid/media/Ringtone;->mStreamType:I

    #@5f
    invoke-interface {v2, v3, v0, v4}, Landroid/media/IRingtonePlayer;->play(Landroid/os/IBinder;Landroid/net/Uri;I)V
    :try_end_62
    .catch Landroid/os/RemoteException; {:try_start_59 .. :try_end_62} :catch_63

    #@62
    goto :goto_a

    #@63
    .line 534
    :catch_63
    move-exception v1

    #@64
    .line 535
    .local v1, e:Landroid/os/RemoteException;
    const-string v2, "Ringtone"

    #@66
    new-instance v3, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    const-string v4, "Problem playing ringtone: "

    #@6d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v3

    #@71
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v3

    #@75
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v3

    #@79
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7c
    goto :goto_a

    #@7d
    .line 538
    .end local v0           #canonicalUri:Landroid/net/Uri;
    .end local v1           #e:Landroid/os/RemoteException;
    :cond_7d
    const-string v2, "Ringtone"

    #@7f
    const-string v3, "Neither local nor remote playback available"

    #@81
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@84
    goto :goto_a
.end method

.method public setStreamType(I)V
    .registers 3
    .parameter "streamType"

    #@0
    .prologue
    .line 175
    iput p1, p0, Landroid/media/Ringtone;->mStreamType:I

    #@2
    .line 179
    iget-object v0, p0, Landroid/media/Ringtone;->mUri:Landroid/net/Uri;

    #@4
    invoke-virtual {p0, v0}, Landroid/media/Ringtone;->setUri(Landroid/net/Uri;)V

    #@7
    .line 180
    return-void
.end method

.method setTitle(Ljava/lang/String;)V
    .registers 2
    .parameter "title"

    #@0
    .prologue
    .line 609
    iput-object p1, p0, Landroid/media/Ringtone;->mTitle:Ljava/lang/String;

    #@2
    .line 610
    return-void
.end method

.method public setUri(Landroid/net/Uri;)V
    .registers 16
    .parameter "uri"

    #@0
    .prologue
    const/4 v13, 0x1

    #@1
    .line 311
    const/4 v8, 0x0

    #@2
    .line 312
    .local v8, setDefault:Z
    invoke-direct {p0}, Landroid/media/Ringtone;->destroyLocalPlayer()V

    #@5
    .line 314
    iput-object p1, p0, Landroid/media/Ringtone;->mUri:Landroid/net/Uri;

    #@7
    .line 315
    iget-object v9, p0, Landroid/media/Ringtone;->mUri:Landroid/net/Uri;

    #@9
    if-eqz v9, :cond_19

    #@b
    iget-object v9, p0, Landroid/media/Ringtone;->mUri:Landroid/net/Uri;

    #@d
    const-string v10, ""

    #@f
    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@12
    move-result-object v10

    #@13
    invoke-virtual {v9, v10}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v9

    #@17
    if-eqz v9, :cond_1a

    #@19
    .line 464
    :cond_19
    :goto_19
    return-void

    #@1a
    .line 322
    :cond_1a
    new-instance v9, Landroid/media/MediaPlayer;

    #@1c
    invoke-direct {v9}, Landroid/media/MediaPlayer;-><init>()V

    #@1f
    iput-object v9, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@21
    .line 325
    const/4 v4, 0x0

    #@22
    .line 326
    .local v4, path:Ljava/lang/String;
    const/4 v3, 0x0

    #@23
    .line 329
    .local v3, nStatus:I
    :try_start_23
    iget-object v9, p0, Landroid/media/Ringtone;->mSmartRingtone:Landroid/media/SmartRingtone;

    #@25
    if-eqz v9, :cond_3e

    #@27
    .line 330
    const-string v9, "Ringtone"

    #@29
    const-string/jumbo v10, "setStreamType: mSmartRingtone.onNoiseEstimation()"

    #@2c
    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 331
    iget-object v9, p0, Landroid/media/Ringtone;->mSmartRingtone:Landroid/media/SmartRingtone;

    #@31
    invoke-virtual {v9}, Landroid/media/SmartRingtone;->onNoiseEstimation()V

    #@34
    .line 332
    const/4 v9, 0x1

    #@35
    iput-boolean v9, p0, Landroid/media/Ringtone;->mIsSmartRingtoneOnNoiseEstimation:Z

    #@37
    .line 333
    iget-object v9, p0, Landroid/media/Ringtone;->mSmartRingtone:Landroid/media/SmartRingtone;

    #@39
    iget-object v10, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@3b
    invoke-virtual {v9, v10}, Landroid/media/SmartRingtone;->setMediaPlayer(Landroid/media/MediaPlayer;)V

    #@3e
    .line 337
    :cond_3e
    sget-boolean v9, Lcom/lge/config/ConfigBuildFlags;->CAPP_DRM:Z

    #@40
    if-eqz v9, :cond_59

    #@42
    .line 338
    iget-object v9, p0, Landroid/media/Ringtone;->mContext:Landroid/content/Context;

    #@44
    iget-object v10, p0, Landroid/media/Ringtone;->mUri:Landroid/net/Uri;

    #@46
    invoke-virtual {v10}, Landroid/net/Uri;->getCanonicalUri()Landroid/net/Uri;

    #@49
    move-result-object v10

    #@4a
    invoke-static {v9, v10}, Lcom/lge/lgdrm/DrmFwExt;->getActualRingtoneUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    #@4d
    move-result-object v4

    #@4e
    .line 339
    if-eqz v4, :cond_59

    #@50
    .line 340
    iget-object v9, p0, Landroid/media/Ringtone;->mContext:Landroid/content/Context;

    #@52
    const/4 v10, 0x1

    #@53
    const/4 v11, 0x0

    #@54
    const/4 v12, 0x0

    #@55
    invoke-static {v9, v4, v10, v11, v12}, Lcom/lge/lgdrm/DrmFwExt;->checkDRMRingtone(Landroid/content/Context;Ljava/lang/String;ZZZ)I

    #@58
    move-result v3

    #@59
    .line 343
    :cond_59
    if-ne v3, v13, :cond_b8

    #@5b
    .line 349
    iget-object v9, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@5d
    invoke-direct {p0}, Landroid/media/Ringtone;->getDefaultPath()Ljava/lang/String;

    #@60
    move-result-object v10

    #@61
    invoke-virtual {v9, v10}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    #@64
    .line 403
    :goto_64
    iget-object v9, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@66
    iget v10, p0, Landroid/media/Ringtone;->mStreamType:I

    #@68
    invoke-virtual {v9, v10}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    #@6b
    .line 405
    const-string v9, "DCM"

    #@6d
    const-string/jumbo v10, "ro.build.target_operator"

    #@70
    invoke-static {v10}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@73
    move-result-object v10

    #@74
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_77
    .catch Ljava/lang/SecurityException; {:try_start_23 .. :try_end_77} :catch_c3
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_77} :catch_13b
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_23 .. :try_end_77} :catch_190

    #@77
    move-result v9

    #@78
    if-eqz v9, :cond_1f6

    #@7a
    .line 407
    :try_start_7a
    const-string v9, "Ringtone"

    #@7c
    const-string v10, "[hy] mLocalPlayer.prepare();"

    #@7e
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    .line 408
    iget-object v9, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@83
    invoke-virtual {v9}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_86
    .catch Ljava/lang/Exception; {:try_start_7a .. :try_end_86} :catch_1c8
    .catch Ljava/lang/SecurityException; {:try_start_7a .. :try_end_86} :catch_c3
    .catch Ljava/io/IOException; {:try_start_7a .. :try_end_86} :catch_13b
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_7a .. :try_end_86} :catch_190

    #@86
    .line 444
    :cond_86
    :goto_86
    if-eqz v8, :cond_ab

    #@88
    .line 445
    const-string v9, "Ringtone"

    #@8a
    const-string v10, "Set default ringtone!"

    #@8c
    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@8f
    .line 447
    :try_start_8f
    new-instance v9, Landroid/media/MediaPlayer;

    #@91
    invoke-direct {v9}, Landroid/media/MediaPlayer;-><init>()V

    #@94
    iput-object v9, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@96
    .line 448
    iget-object v9, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@98
    invoke-direct {p0}, Landroid/media/Ringtone;->getDefaultPath()Ljava/lang/String;

    #@9b
    move-result-object v10

    #@9c
    invoke-virtual {v9, v10}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    #@9f
    .line 449
    iget-object v9, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@a1
    iget v10, p0, Landroid/media/Ringtone;->mStreamType:I

    #@a3
    invoke-virtual {v9, v10}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    #@a6
    .line 450
    iget-object v9, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@a8
    invoke-virtual {v9}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_ab
    .catch Ljava/io/IOException; {:try_start_8f .. :try_end_ab} :catch_1fd

    #@ab
    .line 458
    :cond_ab
    :goto_ab
    iget-object v9, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@ad
    if-eqz v9, :cond_219

    #@af
    .line 459
    const-string v9, "Ringtone"

    #@b1
    const-string v10, "Successfully created local player"

    #@b3
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b6
    goto/16 :goto_19

    #@b8
    .line 351
    :cond_b8
    const/4 v9, 0x2

    #@b9
    if-ne v3, v9, :cond_e5

    #@bb
    .line 352
    :try_start_bb
    iget-object v9, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@bd
    invoke-virtual {v9, v4}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    #@c0
    .line 353
    iput-object v4, p0, Landroid/media/Ringtone;->mDrmPath:Ljava/lang/String;
    :try_end_c2
    .catch Ljava/lang/SecurityException; {:try_start_bb .. :try_end_c2} :catch_c3
    .catch Ljava/io/IOException; {:try_start_bb .. :try_end_c2} :catch_13b
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_bb .. :try_end_c2} :catch_190

    #@c2
    goto :goto_64

    #@c3
    .line 424
    :catch_c3
    move-exception v1

    #@c4
    .line 425
    .local v1, e:Ljava/lang/SecurityException;
    invoke-direct {p0}, Landroid/media/Ringtone;->destroyLocalPlayer()V

    #@c7
    .line 426
    iget-boolean v9, p0, Landroid/media/Ringtone;->mAllowRemote:Z

    #@c9
    if-nez v9, :cond_86

    #@cb
    .line 427
    const-string v9, "Ringtone"

    #@cd
    new-instance v10, Ljava/lang/StringBuilder;

    #@cf
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@d2
    const-string v11, "Remote playback not allowed: "

    #@d4
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v10

    #@d8
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@db
    move-result-object v10

    #@dc
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@df
    move-result-object v10

    #@e0
    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e3
    .line 428
    const/4 v8, 0x1

    #@e4
    goto :goto_86

    #@e5
    .line 360
    .end local v1           #e:Ljava/lang/SecurityException;
    :cond_e5
    :try_start_e5
    const-string v9, "Ringtone"

    #@e7
    new-instance v10, Ljava/lang/StringBuilder;

    #@e9
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@ec
    const-string/jumbo v11, "ringtone uri :"

    #@ef
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v10

    #@f3
    iget-object v11, p0, Landroid/media/Ringtone;->mUri:Landroid/net/Uri;

    #@f5
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v10

    #@f9
    const-string v11, "  path :"

    #@fb
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v10

    #@ff
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@102
    move-result-object v10

    #@103
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@106
    move-result-object v10

    #@107
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10a
    .line 363
    iget-object v9, p0, Landroid/media/Ringtone;->mUri:Landroid/net/Uri;

    #@10c
    invoke-virtual {v9}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    #@10f
    move-result-object v0

    #@110
    .line 364
    .local v0, authority:Ljava/lang/String;
    iget-object v9, p0, Landroid/media/Ringtone;->mContext:Landroid/content/Context;

    #@112
    const/4 v10, 0x1

    #@113
    invoke-static {v9, v10}, Landroid/media/RingtoneManager;->getActualDefaultRingtoneUri(Landroid/content/Context;I)Landroid/net/Uri;

    #@116
    move-result-object v6

    #@117
    .line 366
    .local v6, ringtoneUri:Landroid/net/Uri;
    const-string/jumbo v9, "settings"

    #@11a
    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11d
    move-result v9

    #@11e
    if-eqz v9, :cond_15e

    #@120
    if-nez v6, :cond_15e

    #@122
    .line 370
    new-instance v9, Landroid/content/res/Resources$NotFoundException;

    #@124
    invoke-direct {v9}, Landroid/content/res/Resources$NotFoundException;-><init>()V

    #@127
    throw v9
    :try_end_128
    .catch Ljava/io/IOException; {:try_start_e5 .. :try_end_128} :catch_128
    .catch Ljava/lang/SecurityException; {:try_start_e5 .. :try_end_128} :catch_c3
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_e5 .. :try_end_128} :catch_190

    #@128
    .line 396
    .end local v0           #authority:Ljava/lang/String;
    .end local v6           #ringtoneUri:Landroid/net/Uri;
    :catch_128
    move-exception v2

    #@129
    .line 397
    .local v2, ex:Ljava/io/IOException;
    :try_start_129
    const-string v9, "Ringtone"

    #@12b
    const-string v10, "Problem setDataSource; try to play default ringtone"

    #@12d
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@130
    .line 398
    iget-object v9, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@132
    invoke-direct {p0}, Landroid/media/Ringtone;->getDefaultPath()Ljava/lang/String;

    #@135
    move-result-object v10

    #@136
    invoke-virtual {v9, v10}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V
    :try_end_139
    .catch Ljava/lang/SecurityException; {:try_start_129 .. :try_end_139} :catch_c3
    .catch Ljava/io/IOException; {:try_start_129 .. :try_end_139} :catch_13b
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_129 .. :try_end_139} :catch_190

    #@139
    goto/16 :goto_64

    #@13b
    .line 430
    .end local v2           #ex:Ljava/io/IOException;
    :catch_13b
    move-exception v1

    #@13c
    .line 431
    .local v1, e:Ljava/io/IOException;
    invoke-direct {p0}, Landroid/media/Ringtone;->destroyLocalPlayer()V

    #@13f
    .line 432
    iget-boolean v9, p0, Landroid/media/Ringtone;->mAllowRemote:Z

    #@141
    if-nez v9, :cond_86

    #@143
    .line 433
    const-string v9, "Ringtone"

    #@145
    new-instance v10, Ljava/lang/StringBuilder;

    #@147
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@14a
    const-string v11, "Remote playback not allowed: "

    #@14c
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14f
    move-result-object v10

    #@150
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@153
    move-result-object v10

    #@154
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@157
    move-result-object v10

    #@158
    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@15b
    .line 434
    const/4 v8, 0x1

    #@15c
    goto/16 :goto_86

    #@15e
    .line 372
    .end local v1           #e:Ljava/io/IOException;
    .restart local v0       #authority:Ljava/lang/String;
    .restart local v6       #ringtoneUri:Landroid/net/Uri;
    :cond_15e
    if-eqz v4, :cond_173

    #@160
    .line 373
    :try_start_160
    new-instance v5, Ljava/io/File;

    #@162
    invoke-direct {v5, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@165
    .line 374
    .local v5, ringtoneFile:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    #@168
    move-result v9

    #@169
    if-nez v9, :cond_173

    #@16b
    .line 375
    const-string v9, "Ringtone"

    #@16d
    const-string v10, "File not exists, Change path to null"

    #@16f
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@172
    .line 376
    const/4 v4, 0x0

    #@173
    .line 381
    .end local v5           #ringtoneFile:Ljava/io/File;
    :cond_173
    if-nez v4, :cond_1bd

    #@175
    .line 382
    iget-object v9, p0, Landroid/media/Ringtone;->mUri:Landroid/net/Uri;

    #@177
    invoke-virtual {v9}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@17a
    move-result-object v7

    #@17b
    .line 383
    .local v7, scheme:Ljava/lang/String;
    if-eqz v7, :cond_1b2

    #@17d
    const-string v9, "android.resource"

    #@17f
    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@182
    move-result v9

    #@183
    if-eqz v9, :cond_1b2

    #@185
    .line 384
    iget-object v9, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@187
    iget-object v10, p0, Landroid/media/Ringtone;->mContext:Landroid/content/Context;

    #@189
    iget-object v11, p0, Landroid/media/Ringtone;->mUri:Landroid/net/Uri;

    #@18b
    invoke-virtual {v9, v10, v11}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    :try_end_18e
    .catch Ljava/io/IOException; {:try_start_160 .. :try_end_18e} :catch_128
    .catch Ljava/lang/SecurityException; {:try_start_160 .. :try_end_18e} :catch_c3
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_160 .. :try_end_18e} :catch_190

    #@18e
    goto/16 :goto_64

    #@190
    .line 436
    .end local v0           #authority:Ljava/lang/String;
    .end local v6           #ringtoneUri:Landroid/net/Uri;
    .end local v7           #scheme:Ljava/lang/String;
    :catch_190
    move-exception v1

    #@191
    .line 437
    .local v1, e:Landroid/content/res/Resources$NotFoundException;
    invoke-direct {p0}, Landroid/media/Ringtone;->destroyLocalPlayer()V

    #@194
    .line 438
    iget-boolean v9, p0, Landroid/media/Ringtone;->mAllowRemote:Z

    #@196
    if-nez v9, :cond_86

    #@198
    .line 439
    const-string v9, "Ringtone"

    #@19a
    new-instance v10, Ljava/lang/StringBuilder;

    #@19c
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@19f
    const-string v11, "Remote playback not allowed: "

    #@1a1
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a4
    move-result-object v10

    #@1a5
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a8
    move-result-object v10

    #@1a9
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ac
    move-result-object v10

    #@1ad
    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1b0
    goto/16 :goto_86

    #@1b2
    .line 386
    .end local v1           #e:Landroid/content/res/Resources$NotFoundException;
    .restart local v0       #authority:Ljava/lang/String;
    .restart local v6       #ringtoneUri:Landroid/net/Uri;
    .restart local v7       #scheme:Ljava/lang/String;
    :cond_1b2
    :try_start_1b2
    iget-object v9, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@1b4
    invoke-direct {p0}, Landroid/media/Ringtone;->getDefaultPath()Ljava/lang/String;

    #@1b7
    move-result-object v10

    #@1b8
    invoke-virtual {v9, v10}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    #@1bb
    goto/16 :goto_64

    #@1bd
    .line 394
    .end local v7           #scheme:Ljava/lang/String;
    :cond_1bd
    iget-object v9, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@1bf
    iget-object v10, p0, Landroid/media/Ringtone;->mContext:Landroid/content/Context;

    #@1c1
    iget-object v11, p0, Landroid/media/Ringtone;->mUri:Landroid/net/Uri;

    #@1c3
    invoke-virtual {v9, v10, v11}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    :try_end_1c6
    .catch Ljava/io/IOException; {:try_start_1b2 .. :try_end_1c6} :catch_128
    .catch Ljava/lang/SecurityException; {:try_start_1b2 .. :try_end_1c6} :catch_c3
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1b2 .. :try_end_1c6} :catch_190

    #@1c6
    goto/16 :goto_64

    #@1c8
    .line 409
    .end local v0           #authority:Ljava/lang/String;
    .end local v6           #ringtoneUri:Landroid/net/Uri;
    :catch_1c8
    move-exception v2

    #@1c9
    .line 410
    .local v2, ex:Ljava/lang/Exception;
    :try_start_1c9
    const-string v9, "Ringtone"

    #@1cb
    const-string v10, "[run][hy] exception is occurred. mLocalPlayer.prepare() Error: Try to play a default Ringtone!!!"

    #@1cd
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1d0
    .line 412
    iget-object v9, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@1d2
    invoke-virtual {v9}, Landroid/media/MediaPlayer;->release()V

    #@1d5
    .line 413
    const/4 v9, 0x0

    #@1d6
    iput-object v9, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@1d8
    .line 414
    new-instance v9, Landroid/media/MediaPlayer;

    #@1da
    invoke-direct {v9}, Landroid/media/MediaPlayer;-><init>()V

    #@1dd
    iput-object v9, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@1df
    .line 415
    iget-object v9, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@1e1
    invoke-direct {p0}, Landroid/media/Ringtone;->getDefaultPath()Ljava/lang/String;

    #@1e4
    move-result-object v10

    #@1e5
    invoke-virtual {v9, v10}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    #@1e8
    .line 416
    iget-object v9, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@1ea
    iget v10, p0, Landroid/media/Ringtone;->mStreamType:I

    #@1ec
    invoke-virtual {v9, v10}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    #@1ef
    .line 417
    iget-object v9, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@1f1
    invoke-virtual {v9}, Landroid/media/MediaPlayer;->prepare()V

    #@1f4
    goto/16 :goto_86

    #@1f6
    .line 420
    .end local v2           #ex:Ljava/lang/Exception;
    :cond_1f6
    iget-object v9, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@1f8
    invoke-virtual {v9}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_1fb
    .catch Ljava/lang/SecurityException; {:try_start_1c9 .. :try_end_1fb} :catch_c3
    .catch Ljava/io/IOException; {:try_start_1c9 .. :try_end_1fb} :catch_13b
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1c9 .. :try_end_1fb} :catch_190

    #@1fb
    goto/16 :goto_86

    #@1fd
    .line 451
    :catch_1fd
    move-exception v1

    #@1fe
    .line 452
    .local v1, e:Ljava/io/IOException;
    const-string v9, "Ringtone"

    #@200
    new-instance v10, Ljava/lang/StringBuilder;

    #@202
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@205
    const-string/jumbo v11, "setDefault Ringtone is errer:"

    #@208
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20b
    move-result-object v10

    #@20c
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20f
    move-result-object v10

    #@210
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@213
    move-result-object v10

    #@214
    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@217
    goto/16 :goto_ab

    #@219
    .line 461
    .end local v1           #e:Ljava/io/IOException;
    :cond_219
    const-string v9, "Ringtone"

    #@21b
    const-string v10, "Problem opening; delegating to remote player"

    #@21d
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@220
    goto/16 :goto_19
.end method

.method public setVolume(F)V
    .registers 4
    .parameter "volume"

    #@0
    .prologue
    .line 482
    iget-object v0, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 483
    iget-object v0, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@6
    invoke-virtual {v0, p1, p1}, Landroid/media/MediaPlayer;->setVolume(FF)V

    #@9
    .line 490
    :goto_9
    return-void

    #@a
    .line 484
    :cond_a
    iget-boolean v0, p0, Landroid/media/Ringtone;->mAllowRemote:Z

    #@c
    if-eqz v0, :cond_17

    #@e
    .line 485
    const-string v0, "Ringtone"

    #@10
    const-string/jumbo v1, "setVolume is only supported by local playback"

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    goto :goto_9

    #@17
    .line 488
    :cond_17
    const-string v0, "Ringtone"

    #@19
    const-string v1, "Neither local nor remote playback available"

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    goto :goto_9
.end method

.method public stop()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 548
    iget-object v1, p0, Landroid/media/Ringtone;->mDrmPath:Ljava/lang/String;

    #@4
    if-eqz v1, :cond_d

    #@6
    .line 550
    iget-object v1, p0, Landroid/media/Ringtone;->mContext:Landroid/content/Context;

    #@8
    iget-object v2, p0, Landroid/media/Ringtone;->mDrmPath:Ljava/lang/String;

    #@a
    invoke-static {v1, v2, v3, v4, v3}, Lcom/lge/lgdrm/DrmFwExt;->checkDRMRingtone(Landroid/content/Context;Ljava/lang/String;ZZZ)I

    #@d
    .line 555
    :cond_d
    iget-object v1, p0, Landroid/media/Ringtone;->mSmartRingtone:Landroid/media/SmartRingtone;

    #@f
    if-eqz v1, :cond_1c

    #@11
    iget-boolean v1, p0, Landroid/media/Ringtone;->mIsSmartRingtoneOnNoiseEstimation:Z

    #@13
    if-ne v1, v4, :cond_1c

    #@15
    .line 556
    iget-object v1, p0, Landroid/media/Ringtone;->mSmartRingtone:Landroid/media/SmartRingtone;

    #@17
    invoke-virtual {v1}, Landroid/media/SmartRingtone;->restoreVolumeAfterStop()V

    #@1a
    .line 557
    iput-boolean v3, p0, Landroid/media/Ringtone;->mIsSmartRingtoneOnNoiseEstimation:Z

    #@1c
    .line 561
    :cond_1c
    iget-object v1, p0, Landroid/media/Ringtone;->mLocalPlayer:Landroid/media/MediaPlayer;

    #@1e
    if-eqz v1, :cond_24

    #@20
    .line 562
    invoke-direct {p0}, Landroid/media/Ringtone;->destroyLocalPlayer()V

    #@23
    .line 570
    :cond_23
    :goto_23
    return-void

    #@24
    .line 563
    :cond_24
    iget-boolean v1, p0, Landroid/media/Ringtone;->mAllowRemote:Z

    #@26
    if-eqz v1, :cond_23

    #@28
    .line 565
    :try_start_28
    iget-object v1, p0, Landroid/media/Ringtone;->mRemotePlayer:Landroid/media/IRingtonePlayer;

    #@2a
    iget-object v2, p0, Landroid/media/Ringtone;->mRemoteToken:Landroid/os/Binder;

    #@2c
    invoke-interface {v1, v2}, Landroid/media/IRingtonePlayer;->stop(Landroid/os/IBinder;)V
    :try_end_2f
    .catch Landroid/os/RemoteException; {:try_start_28 .. :try_end_2f} :catch_30

    #@2f
    goto :goto_23

    #@30
    .line 566
    :catch_30
    move-exception v0

    #@31
    .line 567
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "Ringtone"

    #@33
    new-instance v2, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v3, "Problem stopping ringtone: "

    #@3a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v2

    #@46
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    goto :goto_23
.end method
