.class public Landroid/media/effect/EffectFactory;
.super Ljava/lang/Object;
.source "EffectFactory.java"


# static fields
.field public static final EFFECT_AUTOFIX:Ljava/lang/String; = "android.media.effect.effects.AutoFixEffect"

.field public static final EFFECT_BACKDROPPER:Ljava/lang/String; = "android.media.effect.effects.BackDropperEffect"

.field public static final EFFECT_BITMAPOVERLAY:Ljava/lang/String; = "android.media.effect.effects.BitmapOverlayEffect"

.field public static final EFFECT_BLACKWHITE:Ljava/lang/String; = "android.media.effect.effects.BlackWhiteEffect"

.field public static final EFFECT_BRIGHTNESS:Ljava/lang/String; = "android.media.effect.effects.BrightnessEffect"

.field public static final EFFECT_CONTRAST:Ljava/lang/String; = "android.media.effect.effects.ContrastEffect"

.field public static final EFFECT_CROP:Ljava/lang/String; = "android.media.effect.effects.CropEffect"

.field public static final EFFECT_CROSSPROCESS:Ljava/lang/String; = "android.media.effect.effects.CrossProcessEffect"

.field public static final EFFECT_DOCUMENTARY:Ljava/lang/String; = "android.media.effect.effects.DocumentaryEffect"

.field public static final EFFECT_DUOTONE:Ljava/lang/String; = "android.media.effect.effects.DuotoneEffect"

.field public static final EFFECT_FILLLIGHT:Ljava/lang/String; = "android.media.effect.effects.FillLightEffect"

.field public static final EFFECT_FISHEYE:Ljava/lang/String; = "android.media.effect.effects.FisheyeEffect"

.field public static final EFFECT_FLIP:Ljava/lang/String; = "android.media.effect.effects.FlipEffect"

.field public static final EFFECT_GRAIN:Ljava/lang/String; = "android.media.effect.effects.GrainEffect"

.field public static final EFFECT_GRAYSCALE:Ljava/lang/String; = "android.media.effect.effects.GrayscaleEffect"

.field public static final EFFECT_IDENTITY:Ljava/lang/String; = "IdentityEffect"

.field public static final EFFECT_LOMOISH:Ljava/lang/String; = "android.media.effect.effects.LomoishEffect"

.field public static final EFFECT_NEGATIVE:Ljava/lang/String; = "android.media.effect.effects.NegativeEffect"

.field private static final EFFECT_PACKAGES:[Ljava/lang/String; = null

.field public static final EFFECT_POSTERIZE:Ljava/lang/String; = "android.media.effect.effects.PosterizeEffect"

.field public static final EFFECT_REDEYE:Ljava/lang/String; = "android.media.effect.effects.RedEyeEffect"

.field public static final EFFECT_ROTATE:Ljava/lang/String; = "android.media.effect.effects.RotateEffect"

.field public static final EFFECT_SATURATE:Ljava/lang/String; = "android.media.effect.effects.SaturateEffect"

.field public static final EFFECT_SEPIA:Ljava/lang/String; = "android.media.effect.effects.SepiaEffect"

.field public static final EFFECT_SHARPEN:Ljava/lang/String; = "android.media.effect.effects.SharpenEffect"

.field public static final EFFECT_STRAIGHTEN:Ljava/lang/String; = "android.media.effect.effects.StraightenEffect"

.field public static final EFFECT_TEMPERATURE:Ljava/lang/String; = "android.media.effect.effects.ColorTemperatureEffect"

.field public static final EFFECT_TINT:Ljava/lang/String; = "android.media.effect.effects.TintEffect"

.field public static final EFFECT_VIGNETTE:Ljava/lang/String; = "android.media.effect.effects.VignetteEffect"


# instance fields
.field private mEffectContext:Landroid/media/effect/EffectContext;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 33
    const/4 v0, 0x2

    #@1
    new-array v0, v0, [Ljava/lang/String;

    #@3
    const/4 v1, 0x0

    #@4
    const-string v2, "android.media.effect.effects."

    #@6
    aput-object v2, v0, v1

    #@8
    const/4 v1, 0x1

    #@9
    const-string v2, ""

    #@b
    aput-object v2, v0, v1

    #@d
    sput-object v0, Landroid/media/effect/EffectFactory;->EFFECT_PACKAGES:[Ljava/lang/String;

    #@f
    return-void
.end method

.method constructor <init>(Landroid/media/effect/EffectContext;)V
    .registers 2
    .parameter "effectContext"

    #@0
    .prologue
    .line 426
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 427
    iput-object p1, p0, Landroid/media/effect/EffectFactory;->mEffectContext:Landroid/media/effect/EffectContext;

    #@5
    .line 428
    return-void
.end method

.method private static getEffectClassByName(Ljava/lang/String;)Ljava/lang/Class;
    .registers 9
    .parameter "className"

    #@0
    .prologue
    .line 468
    const/4 v3, 0x0

    #@1
    .line 471
    .local v3, effectClass:Ljava/lang/Class;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@4
    move-result-object v7

    #@5
    invoke-virtual {v7}, Ljava/lang/Thread;->getContextClassLoader()Ljava/lang/ClassLoader;

    #@8
    move-result-object v1

    #@9
    .line 474
    .local v1, contextClassLoader:Ljava/lang/ClassLoader;
    sget-object v0, Landroid/media/effect/EffectFactory;->EFFECT_PACKAGES:[Ljava/lang/String;

    #@b
    .local v0, arr$:[Ljava/lang/String;
    array-length v5, v0

    #@c
    .local v5, len$:I
    const/4 v4, 0x0

    #@d
    .local v4, i$:I
    :goto_d
    if-ge v4, v5, :cond_28

    #@f
    aget-object v6, v0, v4

    #@11
    .line 476
    .local v6, packageName:Ljava/lang/String;
    :try_start_11
    new-instance v7, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v7

    #@1a
    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v7

    #@1e
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v7

    #@22
    invoke-virtual {v1, v7}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_25
    .catch Ljava/lang/ClassNotFoundException; {:try_start_11 .. :try_end_25} :catch_29

    #@25
    move-result-object v3

    #@26
    .line 481
    if-eqz v3, :cond_2a

    #@28
    .line 485
    .end local v6           #packageName:Ljava/lang/String;
    :cond_28
    return-object v3

    #@29
    .line 477
    .restart local v6       #packageName:Ljava/lang/String;
    :catch_29
    move-exception v2

    #@2a
    .line 474
    :cond_2a
    add-int/lit8 v4, v4, 0x1

    #@2c
    goto :goto_d
.end method

.method private instantiateEffect(Ljava/lang/Class;Ljava/lang/String;)Landroid/media/effect/Effect;
    .registers 10
    .parameter "effectClass"
    .parameter "name"

    #@0
    .prologue
    .line 491
    :try_start_0
    const-class v4, Landroid/media/effect/Effect;

    #@2
    invoke-virtual {p1, v4}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;
    :try_end_5
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_5} :catch_2a

    #@5
    .line 498
    const/4 v2, 0x0

    #@6
    .line 500
    .local v2, effectConstructor:Ljava/lang/reflect/Constructor;
    const/4 v4, 0x2

    #@7
    :try_start_7
    new-array v4, v4, [Ljava/lang/Class;

    #@9
    const/4 v5, 0x0

    #@a
    const-class v6, Landroid/media/effect/EffectContext;

    #@c
    aput-object v6, v4, v5

    #@e
    const/4 v5, 0x1

    #@f
    const-class v6, Ljava/lang/String;

    #@11
    aput-object v6, v4, v5

    #@13
    invoke-virtual {p1, v4}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
    :try_end_16
    .catch Ljava/lang/NoSuchMethodException; {:try_start_7 .. :try_end_16} :catch_4a

    #@16
    move-result-object v2

    #@17
    .line 507
    const/4 v1, 0x0

    #@18
    .line 509
    .local v1, effect:Landroid/media/effect/Effect;
    const/4 v4, 0x2

    #@19
    :try_start_19
    new-array v4, v4, [Ljava/lang/Object;

    #@1b
    const/4 v5, 0x0

    #@1c
    iget-object v6, p0, Landroid/media/effect/EffectFactory;->mEffectContext:Landroid/media/effect/EffectContext;

    #@1e
    aput-object v6, v4, v5

    #@20
    const/4 v5, 0x1

    #@21
    aput-object p2, v4, v5

    #@23
    invoke-virtual {v2, v4}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    #@26
    move-result-object v1

    #@27
    .end local v1           #effect:Landroid/media/effect/Effect;
    check-cast v1, Landroid/media/effect/Effect;
    :try_end_29
    .catch Ljava/lang/Throwable; {:try_start_19 .. :try_end_29} :catch_71

    #@29
    .line 515
    .restart local v1       #effect:Landroid/media/effect/Effect;
    return-object v1

    #@2a
    .line 492
    .end local v1           #effect:Landroid/media/effect/Effect;
    .end local v2           #effectConstructor:Ljava/lang/reflect/Constructor;
    :catch_2a
    move-exception v0

    #@2b
    .line 493
    .local v0, e:Ljava/lang/ClassCastException;
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@2d
    new-instance v5, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v6, "Attempting to allocate effect \'"

    #@34
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v5

    #@38
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v5

    #@3c
    const-string v6, "\' which is not a subclass of Effect!"

    #@3e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v5

    #@42
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v5

    #@46
    invoke-direct {v4, v5, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@49
    throw v4

    #@4a
    .line 501
    .end local v0           #e:Ljava/lang/ClassCastException;
    .restart local v2       #effectConstructor:Ljava/lang/reflect/Constructor;
    :catch_4a
    move-exception v0

    #@4b
    .line 502
    .local v0, e:Ljava/lang/NoSuchMethodException;
    new-instance v4, Ljava/lang/RuntimeException;

    #@4d
    new-instance v5, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v6, "The effect class \'"

    #@54
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v5

    #@58
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v5

    #@5c
    const-string v6, "\' does not have "

    #@5e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v5

    #@62
    const-string/jumbo v6, "the required constructor."

    #@65
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v5

    #@69
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v5

    #@6d
    invoke-direct {v4, v5, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@70
    throw v4

    #@71
    .line 510
    .end local v0           #e:Ljava/lang/NoSuchMethodException;
    :catch_71
    move-exception v3

    #@72
    .line 511
    .local v3, t:Ljava/lang/Throwable;
    new-instance v4, Ljava/lang/RuntimeException;

    #@74
    new-instance v5, Ljava/lang/StringBuilder;

    #@76
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@79
    const-string v6, "There was an error constructing the effect \'"

    #@7b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v5

    #@7f
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v5

    #@83
    const-string v6, "\'!"

    #@85
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v5

    #@89
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8c
    move-result-object v5

    #@8d
    invoke-direct {v4, v5, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@90
    throw v4
.end method

.method public static isEffectSupported(Ljava/lang/String;)Z
    .registers 2
    .parameter "effectName"

    #@0
    .prologue
    .line 464
    invoke-static {p0}, Landroid/media/effect/EffectFactory;->getEffectClassByName(Ljava/lang/String;)Ljava/lang/Class;

    #@3
    move-result-object v0

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method


# virtual methods
.method public createEffect(Ljava/lang/String;)Landroid/media/effect/Effect;
    .registers 6
    .parameter "effectName"

    #@0
    .prologue
    .line 445
    invoke-static {p1}, Landroid/media/effect/EffectFactory;->getEffectClassByName(Ljava/lang/String;)Ljava/lang/Class;

    #@3
    move-result-object v0

    #@4
    .line 446
    .local v0, effectClass:Ljava/lang/Class;
    if-nez v0, :cond_25

    #@6
    .line 447
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "Cannot instantiate unknown effect \'"

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    const-string v3, "\'!"

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@24
    throw v1

    #@25
    .line 450
    :cond_25
    invoke-direct {p0, v0, p1}, Landroid/media/effect/EffectFactory;->instantiateEffect(Ljava/lang/Class;Ljava/lang/String;)Landroid/media/effect/Effect;

    #@28
    move-result-object v1

    #@29
    return-object v1
.end method
