.class public Landroid/media/effect/EffectContext;
.super Ljava/lang/Object;
.source "EffectContext.java"


# instance fields
.field private final GL_STATE_ARRAYBUFFER:I

.field private final GL_STATE_COUNT:I

.field private final GL_STATE_FBO:I

.field private final GL_STATE_PROGRAM:I

.field private mFactory:Landroid/media/effect/EffectFactory;

.field mFilterContext:Landroid/filterfw/core/FilterContext;

.field private mOldState:[I


# direct methods
.method private constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x3

    #@1
    .line 92
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 40
    const/4 v0, 0x0

    #@5
    iput v0, p0, Landroid/media/effect/EffectContext;->GL_STATE_FBO:I

    #@7
    .line 41
    const/4 v0, 0x1

    #@8
    iput v0, p0, Landroid/media/effect/EffectContext;->GL_STATE_PROGRAM:I

    #@a
    .line 42
    const/4 v0, 0x2

    #@b
    iput v0, p0, Landroid/media/effect/EffectContext;->GL_STATE_ARRAYBUFFER:I

    #@d
    .line 43
    iput v1, p0, Landroid/media/effect/EffectContext;->GL_STATE_COUNT:I

    #@f
    .line 49
    new-array v0, v1, [I

    #@11
    iput-object v0, p0, Landroid/media/effect/EffectContext;->mOldState:[I

    #@13
    .line 93
    new-instance v0, Landroid/filterfw/core/FilterContext;

    #@15
    invoke-direct {v0}, Landroid/filterfw/core/FilterContext;-><init>()V

    #@18
    iput-object v0, p0, Landroid/media/effect/EffectContext;->mFilterContext:Landroid/filterfw/core/FilterContext;

    #@1a
    .line 94
    iget-object v0, p0, Landroid/media/effect/EffectContext;->mFilterContext:Landroid/filterfw/core/FilterContext;

    #@1c
    new-instance v1, Landroid/filterfw/core/CachedFrameManager;

    #@1e
    invoke-direct {v1}, Landroid/filterfw/core/CachedFrameManager;-><init>()V

    #@21
    invoke-virtual {v0, v1}, Landroid/filterfw/core/FilterContext;->setFrameManager(Landroid/filterfw/core/FrameManager;)V

    #@24
    .line 95
    new-instance v0, Landroid/media/effect/EffectFactory;

    #@26
    invoke-direct {v0, p0}, Landroid/media/effect/EffectFactory;-><init>(Landroid/media/effect/EffectContext;)V

    #@29
    iput-object v0, p0, Landroid/media/effect/EffectContext;->mFactory:Landroid/media/effect/EffectFactory;

    #@2b
    .line 96
    return-void
.end method

.method public static createWithCurrentGlContext()Landroid/media/effect/EffectContext;
    .registers 1

    #@0
    .prologue
    .line 60
    new-instance v0, Landroid/media/effect/EffectContext;

    #@2
    invoke-direct {v0}, Landroid/media/effect/EffectContext;-><init>()V

    #@5
    .line 61
    .local v0, result:Landroid/media/effect/EffectContext;
    invoke-direct {v0}, Landroid/media/effect/EffectContext;->initInCurrentGlContext()V

    #@8
    .line 62
    return-object v0
.end method

.method private initInCurrentGlContext()V
    .registers 4

    #@0
    .prologue
    .line 99
    invoke-static {}, Landroid/filterfw/core/GLEnvironment;->isAnyContextActive()Z

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_e

    #@6
    .line 100
    new-instance v1, Ljava/lang/RuntimeException;

    #@8
    const-string v2, "Attempting to initialize EffectContext with no active GL context!"

    #@a
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@d
    throw v1

    #@e
    .line 103
    :cond_e
    new-instance v0, Landroid/filterfw/core/GLEnvironment;

    #@10
    invoke-direct {v0}, Landroid/filterfw/core/GLEnvironment;-><init>()V

    #@13
    .line 104
    .local v0, glEnvironment:Landroid/filterfw/core/GLEnvironment;
    invoke-virtual {v0}, Landroid/filterfw/core/GLEnvironment;->initWithCurrentContext()V

    #@16
    .line 105
    iget-object v1, p0, Landroid/media/effect/EffectContext;->mFilterContext:Landroid/filterfw/core/FilterContext;

    #@18
    invoke-virtual {v1, v0}, Landroid/filterfw/core/FilterContext;->initGLEnvironment(Landroid/filterfw/core/GLEnvironment;)V

    #@1b
    .line 106
    return-void
.end method


# virtual methods
.method final assertValidGLState()V
    .registers 4

    #@0
    .prologue
    .line 109
    iget-object v1, p0, Landroid/media/effect/EffectContext;->mFilterContext:Landroid/filterfw/core/FilterContext;

    #@2
    invoke-virtual {v1}, Landroid/filterfw/core/FilterContext;->getGLEnvironment()Landroid/filterfw/core/GLEnvironment;

    #@5
    move-result-object v0

    #@6
    .line 110
    .local v0, glEnv:Landroid/filterfw/core/GLEnvironment;
    if-eqz v0, :cond_e

    #@8
    invoke-virtual {v0}, Landroid/filterfw/core/GLEnvironment;->isContextActive()Z

    #@b
    move-result v1

    #@c
    if-nez v1, :cond_24

    #@e
    .line 111
    :cond_e
    invoke-static {}, Landroid/filterfw/core/GLEnvironment;->isAnyContextActive()Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_1c

    #@14
    .line 112
    new-instance v1, Ljava/lang/RuntimeException;

    #@16
    const-string v2, "Applying effect in wrong GL context!"

    #@18
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v1

    #@1c
    .line 114
    :cond_1c
    new-instance v1, Ljava/lang/RuntimeException;

    #@1e
    const-string v2, "Attempting to apply effect without valid GL context!"

    #@20
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@23
    throw v1

    #@24
    .line 117
    :cond_24
    return-void
.end method

.method public getFactory()Landroid/media/effect/EffectFactory;
    .registers 2

    #@0
    .prologue
    .line 74
    iget-object v0, p0, Landroid/media/effect/EffectContext;->mFactory:Landroid/media/effect/EffectFactory;

    #@2
    return-object v0
.end method

.method public release()V
    .registers 2

    #@0
    .prologue
    .line 88
    iget-object v0, p0, Landroid/media/effect/EffectContext;->mFilterContext:Landroid/filterfw/core/FilterContext;

    #@2
    invoke-virtual {v0}, Landroid/filterfw/core/FilterContext;->tearDown()V

    #@5
    .line 89
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Landroid/media/effect/EffectContext;->mFilterContext:Landroid/filterfw/core/FilterContext;

    #@8
    .line 90
    return-void
.end method

.method final restoreGLState()V
    .registers 4

    #@0
    .prologue
    .line 126
    const v0, 0x8d40

    #@3
    iget-object v1, p0, Landroid/media/effect/EffectContext;->mOldState:[I

    #@5
    const/4 v2, 0x0

    #@6
    aget v1, v1, v2

    #@8
    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    #@b
    .line 127
    iget-object v0, p0, Landroid/media/effect/EffectContext;->mOldState:[I

    #@d
    const/4 v1, 0x1

    #@e
    aget v0, v0, v1

    #@10
    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    #@13
    .line 128
    const v0, 0x8892

    #@16
    iget-object v1, p0, Landroid/media/effect/EffectContext;->mOldState:[I

    #@18
    const/4 v2, 0x2

    #@19
    aget v1, v1, v2

    #@1b
    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    #@1e
    .line 129
    return-void
.end method

.method final saveGLState()V
    .registers 4

    #@0
    .prologue
    .line 120
    const v0, 0x8ca6

    #@3
    iget-object v1, p0, Landroid/media/effect/EffectContext;->mOldState:[I

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    #@9
    .line 121
    const v0, 0x8b8d

    #@c
    iget-object v1, p0, Landroid/media/effect/EffectContext;->mOldState:[I

    #@e
    const/4 v2, 0x1

    #@f
    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    #@12
    .line 122
    const v0, 0x8894

    #@15
    iget-object v1, p0, Landroid/media/effect/EffectContext;->mOldState:[I

    #@17
    const/4 v2, 0x2

    #@18
    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    #@1b
    .line 123
    return-void
.end method
