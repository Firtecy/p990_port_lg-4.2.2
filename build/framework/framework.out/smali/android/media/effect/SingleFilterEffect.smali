.class public Landroid/media/effect/SingleFilterEffect;
.super Landroid/media/effect/FilterEffect;
.source "SingleFilterEffect.java"


# instance fields
.field protected mFunction:Landroid/filterfw/core/FilterFunction;

.field protected mInputName:Ljava/lang/String;

.field protected mOutputName:Ljava/lang/String;


# direct methods
.method public varargs constructor <init>(Landroid/media/effect/EffectContext;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .registers 12
    .parameter "context"
    .parameter "name"
    .parameter "filterClass"
    .parameter "inputName"
    .parameter "outputName"
    .parameter "finalParameters"

    #@0
    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Landroid/media/effect/FilterEffect;-><init>(Landroid/media/effect/EffectContext;Ljava/lang/String;)V

    #@3
    .line 58
    iput-object p4, p0, Landroid/media/effect/SingleFilterEffect;->mInputName:Ljava/lang/String;

    #@5
    .line 59
    iput-object p5, p0, Landroid/media/effect/SingleFilterEffect;->mOutputName:Ljava/lang/String;

    #@7
    .line 61
    invoke-virtual {p3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@a
    move-result-object v2

    #@b
    .line 62
    .local v2, filterName:Ljava/lang/String;
    invoke-static {}, Landroid/filterfw/core/FilterFactory;->sharedFactory()Landroid/filterfw/core/FilterFactory;

    #@e
    move-result-object v0

    #@f
    .line 63
    .local v0, factory:Landroid/filterfw/core/FilterFactory;
    invoke-virtual {v0, p3, v2}, Landroid/filterfw/core/FilterFactory;->createFilterByClass(Ljava/lang/Class;Ljava/lang/String;)Landroid/filterfw/core/Filter;

    #@12
    move-result-object v1

    #@13
    .line 64
    .local v1, filter:Landroid/filterfw/core/Filter;
    invoke-virtual {v1, p6}, Landroid/filterfw/core/Filter;->initWithAssignmentList([Ljava/lang/Object;)V

    #@16
    .line 66
    new-instance v3, Landroid/filterfw/core/FilterFunction;

    #@18
    invoke-virtual {p0}, Landroid/media/effect/SingleFilterEffect;->getFilterContext()Landroid/filterfw/core/FilterContext;

    #@1b
    move-result-object v4

    #@1c
    invoke-direct {v3, v4, v1}, Landroid/filterfw/core/FilterFunction;-><init>(Landroid/filterfw/core/FilterContext;Landroid/filterfw/core/Filter;)V

    #@1f
    iput-object v3, p0, Landroid/media/effect/SingleFilterEffect;->mFunction:Landroid/filterfw/core/FilterFunction;

    #@21
    .line 67
    return-void
.end method


# virtual methods
.method public apply(IIII)V
    .registers 12
    .parameter "inputTexId"
    .parameter "width"
    .parameter "height"
    .parameter "outputTexId"

    #@0
    .prologue
    .line 71
    invoke-virtual {p0}, Landroid/media/effect/SingleFilterEffect;->beginGLEffect()V

    #@3
    .line 73
    invoke-virtual {p0, p1, p2, p3}, Landroid/media/effect/SingleFilterEffect;->frameFromTexture(III)Landroid/filterfw/core/Frame;

    #@6
    move-result-object v0

    #@7
    .line 74
    .local v0, inputFrame:Landroid/filterfw/core/Frame;
    invoke-virtual {p0, p4, p2, p3}, Landroid/media/effect/SingleFilterEffect;->frameFromTexture(III)Landroid/filterfw/core/Frame;

    #@a
    move-result-object v1

    #@b
    .line 76
    .local v1, outputFrame:Landroid/filterfw/core/Frame;
    iget-object v3, p0, Landroid/media/effect/SingleFilterEffect;->mFunction:Landroid/filterfw/core/FilterFunction;

    #@d
    const/4 v4, 0x2

    #@e
    new-array v4, v4, [Ljava/lang/Object;

    #@10
    const/4 v5, 0x0

    #@11
    iget-object v6, p0, Landroid/media/effect/SingleFilterEffect;->mInputName:Ljava/lang/String;

    #@13
    aput-object v6, v4, v5

    #@15
    const/4 v5, 0x1

    #@16
    aput-object v0, v4, v5

    #@18
    invoke-virtual {v3, v4}, Landroid/filterfw/core/FilterFunction;->executeWithArgList([Ljava/lang/Object;)Landroid/filterfw/core/Frame;

    #@1b
    move-result-object v2

    #@1c
    .line 78
    .local v2, resultFrame:Landroid/filterfw/core/Frame;
    invoke-virtual {v1, v2}, Landroid/filterfw/core/Frame;->setDataFromFrame(Landroid/filterfw/core/Frame;)V

    #@1f
    .line 80
    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@22
    .line 81
    invoke-virtual {v1}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@25
    .line 82
    invoke-virtual {v2}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@28
    .line 84
    invoke-virtual {p0}, Landroid/media/effect/SingleFilterEffect;->endGLEffect()V

    #@2b
    .line 85
    return-void
.end method

.method public release()V
    .registers 2

    #@0
    .prologue
    .line 94
    iget-object v0, p0, Landroid/media/effect/SingleFilterEffect;->mFunction:Landroid/filterfw/core/FilterFunction;

    #@2
    invoke-virtual {v0}, Landroid/filterfw/core/FilterFunction;->tearDown()V

    #@5
    .line 95
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Landroid/media/effect/SingleFilterEffect;->mFunction:Landroid/filterfw/core/FilterFunction;

    #@8
    .line 96
    return-void
.end method

.method public setParameter(Ljava/lang/String;Ljava/lang/Object;)V
    .registers 4
    .parameter "parameterKey"
    .parameter "value"

    #@0
    .prologue
    .line 89
    iget-object v0, p0, Landroid/media/effect/SingleFilterEffect;->mFunction:Landroid/filterfw/core/FilterFunction;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/filterfw/core/FilterFunction;->setInputValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@5
    .line 90
    return-void
.end method
