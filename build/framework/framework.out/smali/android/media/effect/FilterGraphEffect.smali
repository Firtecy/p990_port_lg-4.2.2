.class public Landroid/media/effect/FilterGraphEffect;
.super Landroid/media/effect/FilterEffect;
.source "FilterGraphEffect.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "FilterGraphEffect"


# instance fields
.field protected mGraph:Landroid/filterfw/core/FilterGraph;

.field protected mInputName:Ljava/lang/String;

.field protected mOutputName:Ljava/lang/String;

.field protected mRunner:Landroid/filterfw/core/GraphRunner;

.field protected mSchedulerClass:Ljava/lang/Class;


# direct methods
.method public constructor <init>(Landroid/media/effect/EffectContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V
    .registers 7
    .parameter "context"
    .parameter "name"
    .parameter "graphString"
    .parameter "inputName"
    .parameter "outputName"
    .parameter "scheduler"

    #@0
    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/media/effect/FilterEffect;-><init>(Landroid/media/effect/EffectContext;Ljava/lang/String;)V

    #@3
    .line 65
    iput-object p4, p0, Landroid/media/effect/FilterGraphEffect;->mInputName:Ljava/lang/String;

    #@5
    .line 66
    iput-object p5, p0, Landroid/media/effect/FilterGraphEffect;->mOutputName:Ljava/lang/String;

    #@7
    .line 67
    iput-object p6, p0, Landroid/media/effect/FilterGraphEffect;->mSchedulerClass:Ljava/lang/Class;

    #@9
    .line 68
    invoke-direct {p0, p3}, Landroid/media/effect/FilterGraphEffect;->createGraph(Ljava/lang/String;)V

    #@c
    .line 70
    return-void
.end method

.method private createGraph(Ljava/lang/String;)V
    .registers 8
    .parameter "graphString"

    #@0
    .prologue
    .line 73
    new-instance v1, Landroid/filterfw/io/TextGraphReader;

    #@2
    invoke-direct {v1}, Landroid/filterfw/io/TextGraphReader;-><init>()V

    #@5
    .line 75
    .local v1, reader:Landroid/filterfw/io/GraphReader;
    :try_start_5
    invoke-virtual {v1, p1}, Landroid/filterfw/io/TextGraphReader;->readGraphString(Ljava/lang/String;)Landroid/filterfw/core/FilterGraph;

    #@8
    move-result-object v2

    #@9
    iput-object v2, p0, Landroid/media/effect/FilterGraphEffect;->mGraph:Landroid/filterfw/core/FilterGraph;
    :try_end_b
    .catch Landroid/filterfw/io/GraphIOException; {:try_start_5 .. :try_end_b} :catch_17

    #@b
    .line 80
    iget-object v2, p0, Landroid/media/effect/FilterGraphEffect;->mGraph:Landroid/filterfw/core/FilterGraph;

    #@d
    if-nez v2, :cond_20

    #@f
    .line 81
    new-instance v2, Ljava/lang/RuntimeException;

    #@11
    const-string v3, "Could not setup effect"

    #@13
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@16
    throw v2

    #@17
    .line 76
    :catch_17
    move-exception v0

    #@18
    .line 77
    .local v0, e:Landroid/filterfw/io/GraphIOException;
    new-instance v2, Ljava/lang/RuntimeException;

    #@1a
    const-string v3, "Could not setup effect"

    #@1c
    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@1f
    throw v2

    #@20
    .line 83
    .end local v0           #e:Landroid/filterfw/io/GraphIOException;
    :cond_20
    new-instance v2, Landroid/filterfw/core/SyncRunner;

    #@22
    invoke-virtual {p0}, Landroid/media/effect/FilterGraphEffect;->getFilterContext()Landroid/filterfw/core/FilterContext;

    #@25
    move-result-object v3

    #@26
    iget-object v4, p0, Landroid/media/effect/FilterGraphEffect;->mGraph:Landroid/filterfw/core/FilterGraph;

    #@28
    iget-object v5, p0, Landroid/media/effect/FilterGraphEffect;->mSchedulerClass:Ljava/lang/Class;

    #@2a
    invoke-direct {v2, v3, v4, v5}, Landroid/filterfw/core/SyncRunner;-><init>(Landroid/filterfw/core/FilterContext;Landroid/filterfw/core/FilterGraph;Ljava/lang/Class;)V

    #@2d
    iput-object v2, p0, Landroid/media/effect/FilterGraphEffect;->mRunner:Landroid/filterfw/core/GraphRunner;

    #@2f
    .line 84
    return-void
.end method


# virtual methods
.method public apply(IIII)V
    .registers 10
    .parameter "inputTexId"
    .parameter "width"
    .parameter "height"
    .parameter "outputTexId"

    #@0
    .prologue
    .line 88
    invoke-virtual {p0}, Landroid/media/effect/FilterGraphEffect;->beginGLEffect()V

    #@3
    .line 89
    iget-object v3, p0, Landroid/media/effect/FilterGraphEffect;->mGraph:Landroid/filterfw/core/FilterGraph;

    #@5
    iget-object v4, p0, Landroid/media/effect/FilterGraphEffect;->mInputName:Ljava/lang/String;

    #@7
    invoke-virtual {v3, v4}, Landroid/filterfw/core/FilterGraph;->getFilter(Ljava/lang/String;)Landroid/filterfw/core/Filter;

    #@a
    move-result-object v2

    #@b
    .line 90
    .local v2, src:Landroid/filterfw/core/Filter;
    if-eqz v2, :cond_47

    #@d
    .line 91
    const-string/jumbo v3, "texId"

    #@10
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13
    move-result-object v4

    #@14
    invoke-virtual {v2, v3, v4}, Landroid/filterfw/core/Filter;->setInputValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@17
    .line 92
    const-string/jumbo v3, "width"

    #@1a
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1d
    move-result-object v4

    #@1e
    invoke-virtual {v2, v3, v4}, Landroid/filterfw/core/Filter;->setInputValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@21
    .line 93
    const-string v3, "height"

    #@23
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@26
    move-result-object v4

    #@27
    invoke-virtual {v2, v3, v4}, Landroid/filterfw/core/Filter;->setInputValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@2a
    .line 97
    iget-object v3, p0, Landroid/media/effect/FilterGraphEffect;->mGraph:Landroid/filterfw/core/FilterGraph;

    #@2c
    iget-object v4, p0, Landroid/media/effect/FilterGraphEffect;->mOutputName:Ljava/lang/String;

    #@2e
    invoke-virtual {v3, v4}, Landroid/filterfw/core/FilterGraph;->getFilter(Ljava/lang/String;)Landroid/filterfw/core/Filter;

    #@31
    move-result-object v0

    #@32
    .line 98
    .local v0, dest:Landroid/filterfw/core/Filter;
    if-eqz v0, :cond_4f

    #@34
    .line 99
    const-string/jumbo v3, "texId"

    #@37
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3a
    move-result-object v4

    #@3b
    invoke-virtual {v0, v3, v4}, Landroid/filterfw/core/Filter;->setInputValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@3e
    .line 104
    :try_start_3e
    iget-object v3, p0, Landroid/media/effect/FilterGraphEffect;->mRunner:Landroid/filterfw/core/GraphRunner;

    #@40
    invoke-virtual {v3}, Landroid/filterfw/core/GraphRunner;->run()V
    :try_end_43
    .catch Ljava/lang/RuntimeException; {:try_start_3e .. :try_end_43} :catch_57

    #@43
    .line 108
    invoke-virtual {p0}, Landroid/media/effect/FilterGraphEffect;->endGLEffect()V

    #@46
    .line 109
    return-void

    #@47
    .line 95
    .end local v0           #dest:Landroid/filterfw/core/Filter;
    :cond_47
    new-instance v3, Ljava/lang/RuntimeException;

    #@49
    const-string v4, "Internal error applying effect"

    #@4b
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@4e
    throw v3

    #@4f
    .line 101
    .restart local v0       #dest:Landroid/filterfw/core/Filter;
    :cond_4f
    new-instance v3, Ljava/lang/RuntimeException;

    #@51
    const-string v4, "Internal error applying effect"

    #@53
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@56
    throw v3

    #@57
    .line 105
    :catch_57
    move-exception v1

    #@58
    .line 106
    .local v1, e:Ljava/lang/RuntimeException;
    new-instance v3, Ljava/lang/RuntimeException;

    #@5a
    const-string v4, "Internal error applying effect: "

    #@5c
    invoke-direct {v3, v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@5f
    throw v3
.end method

.method public release()V
    .registers 3

    #@0
    .prologue
    .line 117
    iget-object v0, p0, Landroid/media/effect/FilterGraphEffect;->mGraph:Landroid/filterfw/core/FilterGraph;

    #@2
    invoke-virtual {p0}, Landroid/media/effect/FilterGraphEffect;->getFilterContext()Landroid/filterfw/core/FilterContext;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1}, Landroid/filterfw/core/FilterGraph;->tearDown(Landroid/filterfw/core/FilterContext;)V

    #@9
    .line 118
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Landroid/media/effect/FilterGraphEffect;->mGraph:Landroid/filterfw/core/FilterGraph;

    #@c
    .line 119
    return-void
.end method

.method public setParameter(Ljava/lang/String;Ljava/lang/Object;)V
    .registers 3
    .parameter "parameterKey"
    .parameter "value"

    #@0
    .prologue
    .line 113
    return-void
.end method
