.class public Landroid/media/effect/effects/RotateEffect;
.super Landroid/media/effect/SizeChangeEffect;
.source "RotateEffect.java"


# direct methods
.method public constructor <init>(Landroid/media/effect/EffectContext;Ljava/lang/String;)V
    .registers 10
    .parameter "context"
    .parameter "name"

    #@0
    .prologue
    .line 29
    const-class v3, Landroid/filterpacks/imageproc/RotateFilter;

    #@2
    const-string v4, "image"

    #@4
    const-string v5, "image"

    #@6
    const/4 v0, 0x0

    #@7
    new-array v6, v0, [Ljava/lang/Object;

    #@9
    move-object v0, p0

    #@a
    move-object v1, p1

    #@b
    move-object v2, p2

    #@c
    invoke-direct/range {v0 .. v6}, Landroid/media/effect/SizeChangeEffect;-><init>(Landroid/media/effect/EffectContext;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    #@f
    .line 30
    return-void
.end method
