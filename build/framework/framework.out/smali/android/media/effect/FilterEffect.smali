.class public abstract Landroid/media/effect/FilterEffect;
.super Landroid/media/effect/Effect;
.source "FilterEffect.java"


# instance fields
.field protected mEffectContext:Landroid/media/effect/EffectContext;

.field private mName:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Landroid/media/effect/EffectContext;Ljava/lang/String;)V
    .registers 3
    .parameter "context"
    .parameter "name"

    #@0
    .prologue
    .line 43
    invoke-direct {p0}, Landroid/media/effect/Effect;-><init>()V

    #@3
    .line 44
    iput-object p1, p0, Landroid/media/effect/FilterEffect;->mEffectContext:Landroid/media/effect/EffectContext;

    #@5
    .line 45
    iput-object p2, p0, Landroid/media/effect/FilterEffect;->mName:Ljava/lang/String;

    #@7
    .line 46
    return-void
.end method


# virtual methods
.method protected beginGLEffect()V
    .registers 2

    #@0
    .prologue
    .line 67
    iget-object v0, p0, Landroid/media/effect/FilterEffect;->mEffectContext:Landroid/media/effect/EffectContext;

    #@2
    invoke-virtual {v0}, Landroid/media/effect/EffectContext;->assertValidGLState()V

    #@5
    .line 68
    iget-object v0, p0, Landroid/media/effect/FilterEffect;->mEffectContext:Landroid/media/effect/EffectContext;

    #@7
    invoke-virtual {v0}, Landroid/media/effect/EffectContext;->saveGLState()V

    #@a
    .line 69
    return-void
.end method

.method protected endGLEffect()V
    .registers 2

    #@0
    .prologue
    .line 75
    iget-object v0, p0, Landroid/media/effect/FilterEffect;->mEffectContext:Landroid/media/effect/EffectContext;

    #@2
    invoke-virtual {v0}, Landroid/media/effect/EffectContext;->restoreGLState()V

    #@5
    .line 76
    return-void
.end method

.method protected frameFromTexture(III)Landroid/filterfw/core/Frame;
    .registers 10
    .parameter "texId"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    const/4 v4, 0x3

    #@1
    .line 89
    invoke-virtual {p0}, Landroid/media/effect/FilterEffect;->getFilterContext()Landroid/filterfw/core/FilterContext;

    #@4
    move-result-object v3

    #@5
    invoke-virtual {v3}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@8
    move-result-object v2

    #@9
    .line 90
    .local v2, manager:Landroid/filterfw/core/FrameManager;
    invoke-static {p2, p3, v4, v4}, Landroid/filterfw/format/ImageFormat;->create(IIII)Landroid/filterfw/core/MutableFrameFormat;

    #@c
    move-result-object v0

    #@d
    .line 93
    .local v0, format:Landroid/filterfw/core/FrameFormat;
    const/16 v3, 0x64

    #@f
    int-to-long v4, p1

    #@10
    invoke-virtual {v2, v0, v3, v4, v5}, Landroid/filterfw/core/FrameManager;->newBoundFrame(Landroid/filterfw/core/FrameFormat;IJ)Landroid/filterfw/core/Frame;

    #@13
    move-result-object v1

    #@14
    .line 96
    .local v1, frame:Landroid/filterfw/core/Frame;
    const-wide/16 v3, -0x1

    #@16
    invoke-virtual {v1, v3, v4}, Landroid/filterfw/core/Frame;->setTimestamp(J)V

    #@19
    .line 97
    return-object v1
.end method

.method protected getFilterContext()Landroid/filterfw/core/FilterContext;
    .registers 2

    #@0
    .prologue
    .line 82
    iget-object v0, p0, Landroid/media/effect/FilterEffect;->mEffectContext:Landroid/media/effect/EffectContext;

    #@2
    iget-object v0, v0, Landroid/media/effect/EffectContext;->mFilterContext:Landroid/filterfw/core/FilterContext;

    #@4
    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 58
    iget-object v0, p0, Landroid/media/effect/FilterEffect;->mName:Ljava/lang/String;

    #@2
    return-object v0
.end method
