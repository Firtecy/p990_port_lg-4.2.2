.class public Landroid/media/MediaRouter$RouteCategory;
.super Ljava/lang/Object;
.source "MediaRouter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/MediaRouter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RouteCategory"
.end annotation


# instance fields
.field final mGroupable:Z

.field mIsSystem:Z

.field mName:Ljava/lang/CharSequence;

.field mNameResId:I

.field mTypes:I


# direct methods
.method constructor <init>(IIZ)V
    .registers 4
    .parameter "nameResId"
    .parameter "types"
    .parameter "groupable"

    #@0
    .prologue
    .line 1787
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1788
    iput p1, p0, Landroid/media/MediaRouter$RouteCategory;->mNameResId:I

    #@5
    .line 1789
    iput p2, p0, Landroid/media/MediaRouter$RouteCategory;->mTypes:I

    #@7
    .line 1790
    iput-boolean p3, p0, Landroid/media/MediaRouter$RouteCategory;->mGroupable:Z

    #@9
    .line 1791
    return-void
.end method

.method constructor <init>(Ljava/lang/CharSequence;IZ)V
    .registers 4
    .parameter "name"
    .parameter "types"
    .parameter "groupable"

    #@0
    .prologue
    .line 1781
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1782
    iput-object p1, p0, Landroid/media/MediaRouter$RouteCategory;->mName:Ljava/lang/CharSequence;

    #@5
    .line 1783
    iput p2, p0, Landroid/media/MediaRouter$RouteCategory;->mTypes:I

    #@7
    .line 1784
    iput-boolean p3, p0, Landroid/media/MediaRouter$RouteCategory;->mGroupable:Z

    #@9
    .line 1785
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 1797
    sget-object v0, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2
    iget-object v0, v0, Landroid/media/MediaRouter$Static;->mResources:Landroid/content/res/Resources;

    #@4
    invoke-virtual {p0, v0}, Landroid/media/MediaRouter$RouteCategory;->getName(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getName(Landroid/content/Context;)Ljava/lang/CharSequence;
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 1807
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0}, Landroid/media/MediaRouter$RouteCategory;->getName(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method getName(Landroid/content/res/Resources;)Ljava/lang/CharSequence;
    .registers 3
    .parameter "res"

    #@0
    .prologue
    .line 1811
    iget v0, p0, Landroid/media/MediaRouter$RouteCategory;->mNameResId:I

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 1812
    iget v0, p0, Landroid/media/MediaRouter$RouteCategory;->mNameResId:I

    #@6
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@9
    move-result-object v0

    #@a
    .line 1814
    :goto_a
    return-object v0

    #@b
    :cond_b
    iget-object v0, p0, Landroid/media/MediaRouter$RouteCategory;->mName:Ljava/lang/CharSequence;

    #@d
    goto :goto_a
.end method

.method public getRoutes(Ljava/util/List;)Ljava/util/List;
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/media/MediaRouter$RouteInfo;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Landroid/media/MediaRouter$RouteInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1831
    .local p1, out:Ljava/util/List;,"Ljava/util/List<Landroid/media/MediaRouter$RouteInfo;>;"
    if-nez p1, :cond_1c

    #@2
    .line 1832
    new-instance p1, Ljava/util/ArrayList;

    #@4
    .end local p1           #out:Ljava/util/List;,"Ljava/util/List<Landroid/media/MediaRouter$RouteInfo;>;"
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    #@7
    .line 1837
    .restart local p1       #out:Ljava/util/List;,"Ljava/util/List<Landroid/media/MediaRouter$RouteInfo;>;"
    :goto_7
    invoke-static {}, Landroid/media/MediaRouter;->getRouteCountStatic()I

    #@a
    move-result v0

    #@b
    .line 1838
    .local v0, count:I
    const/4 v1, 0x0

    #@c
    .local v1, i:I
    :goto_c
    if-ge v1, v0, :cond_20

    #@e
    .line 1839
    invoke-static {v1}, Landroid/media/MediaRouter;->getRouteAtStatic(I)Landroid/media/MediaRouter$RouteInfo;

    #@11
    move-result-object v2

    #@12
    .line 1840
    .local v2, route:Landroid/media/MediaRouter$RouteInfo;
    iget-object v3, v2, Landroid/media/MediaRouter$RouteInfo;->mCategory:Landroid/media/MediaRouter$RouteCategory;

    #@14
    if-ne v3, p0, :cond_19

    #@16
    .line 1841
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@19
    .line 1838
    :cond_19
    add-int/lit8 v1, v1, 0x1

    #@1b
    goto :goto_c

    #@1c
    .line 1834
    .end local v0           #count:I
    .end local v1           #i:I
    .end local v2           #route:Landroid/media/MediaRouter$RouteInfo;
    :cond_1c
    invoke-interface {p1}, Ljava/util/List;->clear()V

    #@1f
    goto :goto_7

    #@20
    .line 1844
    .restart local v0       #count:I
    .restart local v1       #i:I
    :cond_20
    return-object p1
.end method

.method public getSupportedTypes()I
    .registers 2

    #@0
    .prologue
    .line 1851
    iget v0, p0, Landroid/media/MediaRouter$RouteCategory;->mTypes:I

    #@2
    return v0
.end method

.method public isGroupable()Z
    .registers 2

    #@0
    .prologue
    .line 1863
    iget-boolean v0, p0, Landroid/media/MediaRouter$RouteCategory;->mGroupable:Z

    #@2
    return v0
.end method

.method public isSystem()Z
    .registers 2

    #@0
    .prologue
    .line 1871
    iget-boolean v0, p0, Landroid/media/MediaRouter$RouteCategory;->mIsSystem:Z

    #@2
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 1875
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "RouteCategory{ name="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Landroid/media/MediaRouter$RouteCategory;->mName:Ljava/lang/CharSequence;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, " types="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Landroid/media/MediaRouter$RouteCategory;->mTypes:I

    #@19
    invoke-static {v1}, Landroid/media/MediaRouter;->typesToString(I)Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    const-string v1, " groupable="

    #@23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v0

    #@27
    iget-boolean v1, p0, Landroid/media/MediaRouter$RouteCategory;->mGroupable:Z

    #@29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v0

    #@2d
    const-string v1, " }"

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v0

    #@33
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v0

    #@37
    return-object v0
.end method
