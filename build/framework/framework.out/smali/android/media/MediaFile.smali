.class public Landroid/media/MediaFile;
.super Ljava/lang/Object;
.source "MediaFile.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/MediaFile$MediaFileType;
    }
.end annotation


# static fields
.field public static final FILE_TYPE_3GPA:I = 0xb

.field public static final FILE_TYPE_3GPP:I = 0x17

.field public static final FILE_TYPE_3GPP2:I = 0x18

.field public static final FILE_TYPE_AAC:I = 0x8

.field public static final FILE_TYPE_AC3:I = 0xc

.field public static final FILE_TYPE_AMR:I = 0x4

.field public static final FILE_TYPE_ASF:I = 0x1a

.field public static final FILE_TYPE_AVI:I = 0x1d

.field public static final FILE_TYPE_AWB:I = 0x5

.field public static final FILE_TYPE_BMP:I = 0x23

.field public static final FILE_TYPE_DCF:I = 0x35

.field public static final FILE_TYPE_DCI:I = 0x190

.field public static final FILE_TYPE_DIVX:I = 0x1f

.field public static final FILE_TYPE_DM:I = 0x34

.field public static final FILE_TYPE_DMB:I = 0xc9

.field public static final FILE_TYPE_DTS:I = 0x12c

.field public static final FILE_TYPE_EC3:I = 0x10

.field public static final FILE_TYPE_FL:I = 0x33

.field public static final FILE_TYPE_FLAC:I = 0xa

.field public static final FILE_TYPE_FLV:I = 0xca

.field public static final FILE_TYPE_GIF:I = 0x21

.field public static final FILE_TYPE_HTML:I = 0x65

.field public static final FILE_TYPE_HTTPLIVE:I = 0x2c

.field public static final FILE_TYPE_IMY:I = 0x13

.field public static final FILE_TYPE_ISMA:I = 0x12e

.field public static final FILE_TYPE_ISMV:I = 0xd0

.field public static final FILE_TYPE_JPEG:I = 0x20

.field public static final FILE_TYPE_JPS:I = 0x191

.field public static final FILE_TYPE_K3G:I = 0xcc

.field public static final FILE_TYPE_M3U:I = 0x29

.field public static final FILE_TYPE_M4A:I = 0x2

.field public static final FILE_TYPE_M4V:I = 0x16

.field public static final FILE_TYPE_MID:I = 0x11

.field public static final FILE_TYPE_MKA:I = 0x9

.field public static final FILE_TYPE_MKV:I = 0x1b

.field public static final FILE_TYPE_MP2PS:I = 0xc8

.field public static final FILE_TYPE_MP2TS:I = 0x1c

.field public static final FILE_TYPE_MP3:I = 0x1

.field public static final FILE_TYPE_MP4:I = 0x15

.field public static final FILE_TYPE_MPO:I = 0x192

.field public static final FILE_TYPE_MS_EXCEL:I = 0x69

.field public static final FILE_TYPE_MS_POWERPOINT:I = 0x6a

.field public static final FILE_TYPE_MS_WORD:I = 0x68

.field public static final FILE_TYPE_O4A:I = 0x38

.field public static final FILE_TYPE_O4I:I = 0x39

.field public static final FILE_TYPE_O4V:I = 0x37

.field public static final FILE_TYPE_ODF:I = 0x36

.field public static final FILE_TYPE_OGG:I = 0x7

.field public static final FILE_TYPE_OGM:I = 0xcb

.field public static final FILE_TYPE_PCM:I = 0xf

.field public static final FILE_TYPE_PDF:I = 0x66

.field public static final FILE_TYPE_PLS:I = 0x2a

.field public static final FILE_TYPE_PNG:I = 0x22

.field public static final FILE_TYPE_PYA:I = 0x12d

.field public static final FILE_TYPE_PYV:I = 0xcf

.field public static final FILE_TYPE_QCP:I = 0xd

.field public static final FILE_TYPE_SKM:I = 0xcd

.field public static final FILE_TYPE_SMF:I = 0x12

.field public static final FILE_TYPE_TEXT:I = 0x64

.field public static final FILE_TYPE_WAV:I = 0x3

.field public static final FILE_TYPE_WBMP:I = 0x24

.field public static final FILE_TYPE_WEBM:I = 0x1e

.field public static final FILE_TYPE_WEBMA:I = 0xe

.field public static final FILE_TYPE_WEBP:I = 0x25

.field public static final FILE_TYPE_WMA:I = 0x6

.field public static final FILE_TYPE_WMV:I = 0x19

.field public static final FILE_TYPE_WPL:I = 0x2b

.field public static final FILE_TYPE_XML:I = 0x67

.field public static final FILE_TYPE_ZIP:I = 0x6b

.field private static final FIRST_AUDIO_FILE_TYPE:I = 0x1

.field private static final FIRST_AUDIO_FILE_TYPE2:I = 0x12c

.field private static final FIRST_DRM_FILE_TYPE:I = 0x33

.field private static final FIRST_IMAGE_FILE_TYPE:I = 0x20

.field private static final FIRST_IMAGE_FILE_TYPE2:I = 0x190

.field private static final FIRST_MIDI_FILE_TYPE:I = 0x11

.field private static final FIRST_PLAYLIST_FILE_TYPE:I = 0x29

.field private static final FIRST_VIDEO_FILE_TYPE:I = 0x15

.field private static final FIRST_VIDEO_FILE_TYPE2:I = 0xc8

.field private static final LAST_AUDIO_FILE_TYPE:I = 0x10

.field private static final LAST_AUDIO_FILE_TYPE2:I = 0x12e

.field private static final LAST_DRM_FILE_TYPE:I = 0x39

.field private static final LAST_IMAGE_FILE_TYPE:I = 0x25

.field private static final LAST_IMAGE_FILE_TYPE2:I = 0x192

.field private static final LAST_MIDI_FILE_TYPE:I = 0x13

.field private static final LAST_PLAYLIST_FILE_TYPE:I = 0x2c

.field private static final LAST_VIDEO_FILE_TYPE:I = 0x1f

.field private static final LAST_VIDEO_FILE_TYPE2:I = 0xd0

.field private static final sFileTypeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/media/MediaFile$MediaFileType;",
            ">;"
        }
    .end annotation
.end field

.field private static final sFileTypeToFormatMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final sFormatToMimeTypeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sMimeTypeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final sMimeTypeToFormatMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 9

    #@0
    .prologue
    const v8, 0xb902

    #@3
    const/4 v7, 0x7

    #@4
    const v6, 0xb984

    #@7
    const/16 v5, 0x300b

    #@9
    const/16 v4, 0x11

    #@b
    .line 177
    new-instance v0, Ljava/util/HashMap;

    #@d
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@10
    sput-object v0, Landroid/media/MediaFile;->sFileTypeMap:Ljava/util/HashMap;

    #@12
    .line 179
    new-instance v0, Ljava/util/HashMap;

    #@14
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@17
    sput-object v0, Landroid/media/MediaFile;->sMimeTypeMap:Ljava/util/HashMap;

    #@19
    .line 182
    new-instance v0, Ljava/util/HashMap;

    #@1b
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@1e
    sput-object v0, Landroid/media/MediaFile;->sFileTypeToFormatMap:Ljava/util/HashMap;

    #@20
    .line 185
    new-instance v0, Ljava/util/HashMap;

    #@22
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@25
    sput-object v0, Landroid/media/MediaFile;->sMimeTypeToFormatMap:Ljava/util/HashMap;

    #@27
    .line 188
    new-instance v0, Ljava/util/HashMap;

    #@29
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@2c
    sput-object v0, Landroid/media/MediaFile;->sFormatToMimeTypeMap:Ljava/util/HashMap;

    #@2e
    .line 236
    const-string v0, "MP3"

    #@30
    const/4 v1, 0x1

    #@31
    const-string v2, "audio/mpeg"

    #@33
    const/16 v3, 0x3009

    #@35
    invoke-static {v0, v1, v2, v3}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@38
    .line 237
    const-string v0, "MPGA"

    #@3a
    const/4 v1, 0x1

    #@3b
    const-string v2, "audio/mpeg"

    #@3d
    const/16 v3, 0x3009

    #@3f
    invoke-static {v0, v1, v2, v3}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@42
    .line 238
    const-string v0, "M4A"

    #@44
    const/4 v1, 0x2

    #@45
    const-string v2, "audio/mp4"

    #@47
    invoke-static {v0, v1, v2, v5}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@4a
    .line 239
    const-string v0, "WAV"

    #@4c
    const/4 v1, 0x3

    #@4d
    const-string v2, "audio/x-wav"

    #@4f
    const/16 v3, 0x3008

    #@51
    invoke-static {v0, v1, v2, v3}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@54
    .line 240
    const-string v0, "WAV"

    #@56
    const/16 v1, 0xf

    #@58
    const-string v2, "audio/wav"

    #@5a
    const/16 v3, 0x3008

    #@5c
    invoke-static {v0, v1, v2, v3}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@5f
    .line 241
    const-string v0, "AMR"

    #@61
    const/4 v1, 0x4

    #@62
    const-string v2, "audio/amr"

    #@64
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@67
    .line 242
    const-string v0, "AWB"

    #@69
    const/4 v1, 0x5

    #@6a
    const-string v2, "audio/amr-wb"

    #@6c
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@6f
    .line 243
    const-string v0, "DIVX"

    #@71
    const/16 v1, 0x1f

    #@73
    const-string/jumbo v2, "video/divx"

    #@76
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@79
    .line 244
    invoke-static {}, Landroid/media/MediaFile;->isWMAEnabled()Z

    #@7c
    move-result v0

    #@7d
    if-eqz v0, :cond_8a

    #@7f
    .line 245
    const-string v0, "WMA"

    #@81
    const/4 v1, 0x6

    #@82
    const-string v2, "audio/x-ms-wma"

    #@84
    const v3, 0xb901

    #@87
    invoke-static {v0, v1, v2, v3}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@8a
    .line 247
    :cond_8a
    const-string v0, "QCP"

    #@8c
    const/16 v1, 0xd

    #@8e
    const-string v2, "audio/qcelp"

    #@90
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@93
    .line 248
    const-string v0, "OGG"

    #@95
    const-string v1, "audio/ogg"

    #@97
    invoke-static {v0, v7, v1, v8}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@9a
    .line 249
    const-string v0, "OGG"

    #@9c
    const-string v1, "application/ogg"

    #@9e
    invoke-static {v0, v7, v1, v8}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@a1
    .line 250
    const-string v0, "OGA"

    #@a3
    const-string v1, "audio/ogg"

    #@a5
    invoke-static {v0, v7, v1, v8}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@a8
    .line 251
    const-string v0, "OGA"

    #@aa
    const-string v1, "application/ogg"

    #@ac
    invoke-static {v0, v7, v1, v8}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@af
    .line 252
    const-string v0, "AAC"

    #@b1
    const/16 v1, 0x8

    #@b3
    const-string v2, "audio/aac"

    #@b5
    const v3, 0xb903

    #@b8
    invoke-static {v0, v1, v2, v3}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@bb
    .line 253
    const-string v0, "AAC"

    #@bd
    const/16 v1, 0x8

    #@bf
    const-string v2, "audio/aac-adts"

    #@c1
    const v3, 0xb903

    #@c4
    invoke-static {v0, v1, v2, v3}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@c7
    .line 254
    const-string v0, "MKA"

    #@c9
    const/16 v1, 0x9

    #@cb
    const-string v2, "audio/x-matroska"

    #@cd
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@d0
    .line 256
    const-string v0, "ISMA"

    #@d2
    const/16 v1, 0x12e

    #@d4
    const-string v2, "audio/mp4"

    #@d6
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@d9
    .line 257
    const-string v0, "PYA"

    #@db
    const/16 v1, 0x12d

    #@dd
    const-string v2, "audio/vnd.ms-playready.media.pya"

    #@df
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@e2
    .line 259
    const-string v0, "3GPP"

    #@e4
    const/16 v1, 0xb

    #@e6
    const-string v2, "audio/3gpp"

    #@e8
    invoke-static {v0, v1, v2, v6}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@eb
    .line 260
    const-string v0, "3GA"

    #@ed
    const/16 v1, 0xb

    #@ef
    const-string v2, "audio/3gpp"

    #@f1
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@f4
    .line 261
    const-string v0, "MID"

    #@f6
    const-string v1, "audio/midi"

    #@f8
    invoke-static {v0, v4, v1}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@fb
    .line 262
    const-string v0, "MIDI"

    #@fd
    const-string v1, "audio/midi"

    #@ff
    invoke-static {v0, v4, v1}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@102
    .line 263
    const-string v0, "XMF"

    #@104
    const-string v1, "audio/midi"

    #@106
    invoke-static {v0, v4, v1}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@109
    .line 264
    const-string v0, "RTTTL"

    #@10b
    const-string v1, "audio/midi"

    #@10d
    invoke-static {v0, v4, v1}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@110
    .line 265
    const-string v0, "SMF"

    #@112
    const/16 v1, 0x12

    #@114
    const-string v2, "audio/sp-midi"

    #@116
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@119
    .line 266
    const-string v0, "IMY"

    #@11b
    const/16 v1, 0x13

    #@11d
    const-string v2, "audio/imelody"

    #@11f
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@122
    .line 267
    const-string v0, "RTX"

    #@124
    const-string v1, "audio/midi"

    #@126
    invoke-static {v0, v4, v1}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@129
    .line 268
    const-string v0, "OTA"

    #@12b
    const-string v1, "audio/midi"

    #@12d
    invoke-static {v0, v4, v1}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@130
    .line 269
    const-string v0, "MXMF"

    #@132
    const-string v1, "audio/midi"

    #@134
    invoke-static {v0, v4, v1}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@137
    .line 272
    const-string v0, "SKM"

    #@139
    const/16 v1, 0xcd

    #@13b
    const-string/jumbo v2, "video/skm"

    #@13e
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@141
    .line 273
    const-string v0, "K3G"

    #@143
    const/16 v1, 0xcc

    #@145
    const-string/jumbo v2, "video/k3g"

    #@148
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@14b
    .line 274
    const-string v0, "FLV"

    #@14d
    const/16 v1, 0xca

    #@14f
    const-string/jumbo v2, "video/flv"

    #@152
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@155
    .line 275
    const-string v0, "F4V"

    #@157
    const/16 v1, 0xca

    #@159
    const-string/jumbo v2, "video/flv"

    #@15c
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@15f
    .line 276
    const-string v0, "OGM"

    #@161
    const/16 v1, 0xcb

    #@163
    const-string/jumbo v2, "video/ogm"

    #@166
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@169
    .line 277
    const-string v0, "OGV"

    #@16b
    const/16 v1, 0xcb

    #@16d
    const-string/jumbo v2, "video/ogm"

    #@170
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@173
    .line 285
    const-string v0, "MPEG"

    #@175
    const/16 v1, 0x15

    #@177
    const-string/jumbo v2, "video/mpeg"

    #@17a
    invoke-static {v0, v1, v2, v5}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@17d
    .line 286
    const-string v0, "MPG"

    #@17f
    const/16 v1, 0x15

    #@181
    const-string/jumbo v2, "video/mpeg"

    #@184
    invoke-static {v0, v1, v2, v5}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@187
    .line 287
    const-string v0, "MP4"

    #@189
    const/16 v1, 0x15

    #@18b
    const-string/jumbo v2, "video/mp4"

    #@18e
    invoke-static {v0, v1, v2, v5}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@191
    .line 288
    const-string v0, "M4V"

    #@193
    const/16 v1, 0x16

    #@195
    const-string/jumbo v2, "video/mp4"

    #@198
    invoke-static {v0, v1, v2, v5}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@19b
    .line 290
    const-string v0, "ISMV"

    #@19d
    const/16 v1, 0xd0

    #@19f
    const-string/jumbo v2, "video/mp4"

    #@1a2
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@1a5
    .line 291
    const-string v0, "PYV"

    #@1a7
    const/16 v1, 0xcf

    #@1a9
    const-string/jumbo v2, "video/vnd.ms-playready.media.pyv"

    #@1ac
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@1af
    .line 293
    const-string v0, "3GP"

    #@1b1
    const/16 v1, 0x17

    #@1b3
    const-string/jumbo v2, "video/3gpp"

    #@1b6
    invoke-static {v0, v1, v2, v6}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@1b9
    .line 294
    const-string v0, "3GPP"

    #@1bb
    const/16 v1, 0x17

    #@1bd
    const-string/jumbo v2, "video/3gpp"

    #@1c0
    invoke-static {v0, v1, v2, v6}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@1c3
    .line 295
    const-string v0, "3G2"

    #@1c5
    const/16 v1, 0x18

    #@1c7
    const-string/jumbo v2, "video/3gpp2"

    #@1ca
    invoke-static {v0, v1, v2, v6}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@1cd
    .line 296
    const-string v0, "3GPP2"

    #@1cf
    const/16 v1, 0x18

    #@1d1
    const-string/jumbo v2, "video/3gpp2"

    #@1d4
    invoke-static {v0, v1, v2, v6}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@1d7
    .line 297
    const-string v0, "MKV"

    #@1d9
    const/16 v1, 0x1b

    #@1db
    const-string/jumbo v2, "video/x-matroska"

    #@1de
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@1e1
    .line 298
    const-string v0, "WEBM"

    #@1e3
    const/16 v1, 0x1e

    #@1e5
    const-string/jumbo v2, "video/webm"

    #@1e8
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@1eb
    .line 299
    const-string v0, "TS"

    #@1ed
    const/16 v1, 0x1c

    #@1ef
    const-string/jumbo v2, "video/mp2ts"

    #@1f2
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@1f5
    .line 301
    const-string v0, "TTS"

    #@1f7
    const/16 v1, 0x1c

    #@1f9
    const-string/jumbo v2, "video/mp2ts"

    #@1fc
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@1ff
    .line 303
    const-string v0, "MPG"

    #@201
    const/16 v1, 0x1c

    #@203
    const-string/jumbo v2, "video/mp2ts"

    #@206
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@209
    .line 305
    const-string v0, "AVI"

    #@20b
    const/16 v1, 0x1d

    #@20d
    const-string/jumbo v2, "video/avi"

    #@210
    const/16 v3, 0x300a

    #@212
    invoke-static {v0, v1, v2, v3}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@215
    .line 307
    invoke-static {}, Landroid/media/MediaFile;->isWMVEnabled()Z

    #@218
    move-result v0

    #@219
    if-eqz v0, :cond_234

    #@21b
    .line 308
    const-string v0, "WMV"

    #@21d
    const/16 v1, 0x19

    #@21f
    const-string/jumbo v2, "video/x-ms-wmv"

    #@222
    const v3, 0xb981

    #@225
    invoke-static {v0, v1, v2, v3}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@228
    .line 309
    const-string v0, "ASF"

    #@22a
    const/16 v1, 0x1a

    #@22c
    const-string/jumbo v2, "video/x-ms-asf"

    #@22f
    const/16 v3, 0x300c

    #@231
    invoke-static {v0, v1, v2, v3}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@234
    .line 311
    :cond_234
    const-string v0, "DMB"

    #@236
    const/16 v1, 0xc9

    #@238
    const-string/jumbo v2, "video/dmb"

    #@23b
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@23e
    .line 313
    const-string v0, "JPG"

    #@240
    const/16 v1, 0x20

    #@242
    const-string v2, "image/jpeg"

    #@244
    const/16 v3, 0x3801

    #@246
    invoke-static {v0, v1, v2, v3}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@249
    .line 314
    const-string v0, "JPEG"

    #@24b
    const/16 v1, 0x20

    #@24d
    const-string v2, "image/jpeg"

    #@24f
    const/16 v3, 0x3801

    #@251
    invoke-static {v0, v1, v2, v3}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@254
    .line 315
    const-string v0, "GIF"

    #@256
    const/16 v1, 0x21

    #@258
    const-string v2, "image/gif"

    #@25a
    const/16 v3, 0x3807

    #@25c
    invoke-static {v0, v1, v2, v3}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@25f
    .line 316
    const-string v0, "PNG"

    #@261
    const/16 v1, 0x22

    #@263
    const-string v2, "image/png"

    #@265
    const/16 v3, 0x380b

    #@267
    invoke-static {v0, v1, v2, v3}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@26a
    .line 317
    const-string v0, "BMP"

    #@26c
    const/16 v1, 0x23

    #@26e
    const-string v2, "image/x-ms-bmp"

    #@270
    const/16 v3, 0x3804

    #@272
    invoke-static {v0, v1, v2, v3}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@275
    .line 318
    const-string v0, "WBMP"

    #@277
    const/16 v1, 0x24

    #@279
    const-string v2, "image/vnd.wap.wbmp"

    #@27b
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@27e
    .line 319
    const-string v0, "WEBP"

    #@280
    const/16 v1, 0x25

    #@282
    const-string v2, "image/webp"

    #@284
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@287
    .line 320
    const-string v0, "DCI"

    #@289
    const/16 v1, 0x190

    #@28b
    const-string v2, "image/dci"

    #@28d
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@290
    .line 322
    const-string v0, "JPS"

    #@292
    const/16 v1, 0x191

    #@294
    const-string v2, "image/jps"

    #@296
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@299
    .line 323
    const-string v0, "MPO"

    #@29b
    const/16 v1, 0x192

    #@29d
    const-string v2, "image/mpo"

    #@29f
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@2a2
    .line 326
    const-string v0, "M3U"

    #@2a4
    const/16 v1, 0x29

    #@2a6
    const-string v2, "audio/x-mpegurl"

    #@2a8
    const v3, 0xba11

    #@2ab
    invoke-static {v0, v1, v2, v3}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@2ae
    .line 327
    const-string v0, "M3U"

    #@2b0
    const/16 v1, 0x29

    #@2b2
    const-string v2, "application/x-mpegurl"

    #@2b4
    const v3, 0xba11

    #@2b7
    invoke-static {v0, v1, v2, v3}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@2ba
    .line 328
    const-string v0, "PLS"

    #@2bc
    const/16 v1, 0x2a

    #@2be
    const-string v2, "audio/x-scpls"

    #@2c0
    const v3, 0xba14

    #@2c3
    invoke-static {v0, v1, v2, v3}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@2c6
    .line 329
    const-string v0, "WPL"

    #@2c8
    const/16 v1, 0x2b

    #@2ca
    const-string v2, "application/vnd.ms-wpl"

    #@2cc
    const v3, 0xba10

    #@2cf
    invoke-static {v0, v1, v2, v3}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@2d2
    .line 330
    const-string v0, "M3U8"

    #@2d4
    const/16 v1, 0x2c

    #@2d6
    const-string v2, "application/vnd.apple.mpegurl"

    #@2d8
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@2db
    .line 331
    const-string v0, "M3U8"

    #@2dd
    const/16 v1, 0x2c

    #@2df
    const-string v2, "audio/mpegurl"

    #@2e1
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@2e4
    .line 332
    const-string v0, "M3U8"

    #@2e6
    const/16 v1, 0x2c

    #@2e8
    const-string v2, "audio/x-mpegurl"

    #@2ea
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@2ed
    .line 334
    const-string v0, "FL"

    #@2ef
    const/16 v1, 0x33

    #@2f1
    const-string v2, "application/x-android-drm-fl"

    #@2f3
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@2f6
    .line 336
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_DRM:Z

    #@2f8
    if-eqz v0, :cond_330

    #@2fa
    .line 337
    const-string v0, "DM"

    #@2fc
    const/16 v1, 0x34

    #@2fe
    const-string v2, "application/vnd.oma.drm.message"

    #@300
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@303
    .line 338
    const-string v0, "DCF"

    #@305
    const/16 v1, 0x35

    #@307
    const-string v2, "application/vnd.oma.drm.content"

    #@309
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@30c
    .line 340
    const-string v0, "ODF"

    #@30e
    const/16 v1, 0x36

    #@310
    const-string v2, "application/vnd.oma.drm.dcf"

    #@312
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@315
    .line 341
    const-string v0, "O4A"

    #@317
    const/16 v1, 0x38

    #@319
    const-string v2, "application/vnd.oma.drm.dcf"

    #@31b
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@31e
    .line 342
    const-string v0, "O4V"

    #@320
    const/16 v1, 0x37

    #@322
    const-string v2, "application/vnd.oma.drm.dcf"

    #@324
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@327
    .line 343
    const-string v0, "O4I"

    #@329
    const/16 v1, 0x39

    #@32b
    const-string v2, "application/vnd.oma.drm.dcf"

    #@32d
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@330
    .line 347
    :cond_330
    const-string v0, "TXT"

    #@332
    const/16 v1, 0x64

    #@334
    const-string/jumbo v2, "text/plain"

    #@337
    const/16 v3, 0x3004

    #@339
    invoke-static {v0, v1, v2, v3}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@33c
    .line 348
    const-string v0, "HTM"

    #@33e
    const/16 v1, 0x65

    #@340
    const-string/jumbo v2, "text/html"

    #@343
    const/16 v3, 0x3005

    #@345
    invoke-static {v0, v1, v2, v3}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@348
    .line 349
    const-string v0, "HTML"

    #@34a
    const/16 v1, 0x65

    #@34c
    const-string/jumbo v2, "text/html"

    #@34f
    const/16 v3, 0x3005

    #@351
    invoke-static {v0, v1, v2, v3}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@354
    .line 350
    const-string v0, "PDF"

    #@356
    const/16 v1, 0x66

    #@358
    const-string v2, "application/pdf"

    #@35a
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@35d
    .line 351
    const-string v0, "DOC"

    #@35f
    const/16 v1, 0x68

    #@361
    const-string v2, "application/msword"

    #@363
    const v3, 0xba83

    #@366
    invoke-static {v0, v1, v2, v3}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@369
    .line 352
    const-string v0, "XLS"

    #@36b
    const/16 v1, 0x69

    #@36d
    const-string v2, "application/vnd.ms-excel"

    #@36f
    const v3, 0xba85

    #@372
    invoke-static {v0, v1, v2, v3}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@375
    .line 353
    const-string v0, "PPT"

    #@377
    const/16 v1, 0x6a

    #@379
    const-string v2, "application/mspowerpoint"

    #@37b
    const v3, 0xba86

    #@37e
    invoke-static {v0, v1, v2, v3}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@381
    .line 354
    const-string v0, "FLAC"

    #@383
    const/16 v1, 0xa

    #@385
    const-string v2, "audio/flac"

    #@387
    const v3, 0xb906

    #@38a
    invoke-static {v0, v1, v2, v3}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;I)V

    #@38d
    .line 355
    const-string v0, "ZIP"

    #@38f
    const/16 v1, 0x6b

    #@391
    const-string v2, "application/zip"

    #@393
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@396
    .line 356
    const-string v0, "MPG"

    #@398
    const/16 v1, 0xc8

    #@39a
    const-string/jumbo v2, "video/mp2p"

    #@39d
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@3a0
    .line 357
    const-string v0, "MPEG"

    #@3a2
    const/16 v1, 0xc8

    #@3a4
    const-string/jumbo v2, "video/mp2p"

    #@3a7
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@3aa
    .line 358
    const-string v0, "AC3"

    #@3ac
    const/16 v1, 0x10

    #@3ae
    const-string v2, "audio/ac3"

    #@3b0
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@3b3
    .line 359
    const-string v0, "EC3"

    #@3b5
    const/16 v1, 0x10

    #@3b7
    const-string v2, "audio/eac3"

    #@3b9
    invoke-static {v0, v1, v2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@3bc
    .line 360
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 44
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 167
    return-void
.end method

.method static addFileType(Ljava/lang/String;ILjava/lang/String;)V
    .registers 5
    .parameter "extension"
    .parameter "fileType"
    .parameter "mimeType"

    #@0
    .prologue
    .line 192
    sget-object v0, Landroid/media/MediaFile;->sFileTypeMap:Ljava/util/HashMap;

    #@2
    new-instance v1, Landroid/media/MediaFile$MediaFileType;

    #@4
    invoke-direct {v1, p1, p2}, Landroid/media/MediaFile$MediaFileType;-><init>(ILjava/lang/String;)V

    #@7
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    .line 193
    sget-object v0, Landroid/media/MediaFile;->sMimeTypeMap:Ljava/util/HashMap;

    #@c
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v0, p2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@13
    .line 195
    invoke-static {p0, p1, p2}, Lcom/lge/lgdrm/DrmFwExt$MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@16
    .line 197
    return-void
.end method

.method static addFileType(Ljava/lang/String;ILjava/lang/String;I)V
    .registers 6
    .parameter "extension"
    .parameter "fileType"
    .parameter "mimeType"
    .parameter "mtpFormatCode"

    #@0
    .prologue
    .line 200
    invoke-static {p0, p1, p2}, Landroid/media/MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@3
    .line 201
    sget-object v0, Landroid/media/MediaFile;->sFileTypeToFormatMap:Ljava/util/HashMap;

    #@5
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    .line 202
    sget-object v0, Landroid/media/MediaFile;->sMimeTypeToFormatMap:Ljava/util/HashMap;

    #@e
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v0, p2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@15
    .line 203
    sget-object v0, Landroid/media/MediaFile;->sFormatToMimeTypeMap:Ljava/util/HashMap;

    #@17
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    .line 205
    invoke-static {p1}, Landroid/media/MediaFile;->isAudioFileType(I)Z

    #@21
    move-result v0

    #@22
    if-nez v0, :cond_2a

    #@24
    invoke-static {p1}, Landroid/media/MediaFile;->isVideoFileType(I)Z

    #@27
    move-result v0

    #@28
    if-eqz v0, :cond_2d

    #@2a
    .line 206
    :cond_2a
    invoke-static {p0, p1, p2}, Lcom/lge/lgdrm/DrmFwExt$MediaFile;->addFileType(Ljava/lang/String;ILjava/lang/String;)V

    #@2d
    .line 209
    :cond_2d
    return-void
.end method

.method public static getFileTitle(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "path"

    #@0
    .prologue
    .line 417
    const/16 v2, 0x2f

    #@2
    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    #@5
    move-result v1

    #@6
    .line 418
    .local v1, lastSlash:I
    if-ltz v1, :cond_14

    #@8
    .line 419
    add-int/lit8 v1, v1, 0x1

    #@a
    .line 420
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@d
    move-result v2

    #@e
    if-ge v1, v2, :cond_14

    #@10
    .line 421
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@13
    move-result-object p0

    #@14
    .line 425
    :cond_14
    const/16 v2, 0x2e

    #@16
    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    #@19
    move-result v0

    #@1a
    .line 426
    .local v0, lastDot:I
    if-lez v0, :cond_21

    #@1c
    .line 427
    const/4 v2, 0x0

    #@1d
    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@20
    move-result-object p0

    #@21
    .line 429
    :cond_21
    return-object p0
.end method

.method public static getFileType(Ljava/lang/String;)Landroid/media/MediaFile$MediaFileType;
    .registers 4
    .parameter "path"

    #@0
    .prologue
    .line 402
    const-string v1, "."

    #@2
    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    .line 403
    .local v0, lastDot:I
    if-gez v0, :cond_a

    #@8
    .line 404
    const/4 v1, 0x0

    #@9
    .line 405
    :goto_9
    return-object v1

    #@a
    :cond_a
    sget-object v1, Landroid/media/MediaFile;->sFileTypeMap:Ljava/util/HashMap;

    #@c
    add-int/lit8 v2, v0, 0x1

    #@e
    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@19
    move-result-object v1

    #@1a
    check-cast v1, Landroid/media/MediaFile$MediaFileType;

    #@1c
    goto :goto_9
.end method

.method public static getFileTypeForMimeType(Ljava/lang/String;)I
    .registers 3
    .parameter "mimeType"

    #@0
    .prologue
    .line 433
    sget-object v1, Landroid/media/MediaFile;->sMimeTypeMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/lang/Integer;

    #@8
    .line 434
    .local v0, value:Ljava/lang/Integer;
    if-nez v0, :cond_c

    #@a
    const/4 v1, 0x0

    #@b
    :goto_b
    return v1

    #@c
    :cond_c
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@f
    move-result v1

    #@10
    goto :goto_b
.end method

.method public static getFileTypeMediaHash()Ljava/util/HashMap;
    .registers 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/media/MediaFile$MediaFileType;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 465
    sget-object v0, Landroid/media/MediaFile;->sFileTypeMap:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method public static getFormatCode(Ljava/lang/String;Ljava/lang/String;)I
    .registers 6
    .parameter "fileName"
    .parameter "mimeType"

    #@0
    .prologue
    .line 443
    if-eqz p1, :cond_11

    #@2
    .line 444
    sget-object v3, Landroid/media/MediaFile;->sMimeTypeToFormatMap:Ljava/util/HashMap;

    #@4
    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@7
    move-result-object v2

    #@8
    check-cast v2, Ljava/lang/Integer;

    #@a
    .line 445
    .local v2, value:Ljava/lang/Integer;
    if-eqz v2, :cond_11

    #@c
    .line 446
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@f
    move-result v3

    #@10
    .line 457
    .end local v2           #value:Ljava/lang/Integer;
    :goto_10
    return v3

    #@11
    .line 449
    :cond_11
    const/16 v3, 0x2e

    #@13
    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(I)I

    #@16
    move-result v1

    #@17
    .line 450
    .local v1, lastDot:I
    if-lez v1, :cond_32

    #@19
    .line 451
    add-int/lit8 v3, v1, 0x1

    #@1b
    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    .line 452
    .local v0, extension:Ljava/lang/String;
    sget-object v3, Landroid/media/MediaFile;->sFileTypeToFormatMap:Ljava/util/HashMap;

    #@25
    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@28
    move-result-object v2

    #@29
    check-cast v2, Ljava/lang/Integer;

    #@2b
    .line 453
    .restart local v2       #value:Ljava/lang/Integer;
    if-eqz v2, :cond_32

    #@2d
    .line 454
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@30
    move-result v3

    #@31
    goto :goto_10

    #@32
    .line 457
    .end local v0           #extension:Ljava/lang/String;
    .end local v2           #value:Ljava/lang/Integer;
    :cond_32
    const/16 v3, 0x3000

    #@34
    goto :goto_10
.end method

.method public static getMimeTypeForFile(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "path"

    #@0
    .prologue
    .line 438
    invoke-static {p0}, Landroid/media/MediaFile;->getFileType(Ljava/lang/String;)Landroid/media/MediaFile$MediaFileType;

    #@3
    move-result-object v0

    #@4
    .line 439
    .local v0, mediaFileType:Landroid/media/MediaFile$MediaFileType;
    if-nez v0, :cond_8

    #@6
    const/4 v1, 0x0

    #@7
    :goto_7
    return-object v1

    #@8
    :cond_8
    iget-object v1, v0, Landroid/media/MediaFile$MediaFileType;->mimeType:Ljava/lang/String;

    #@a
    goto :goto_7
.end method

.method public static getMimeTypeForFormatCode(I)Ljava/lang/String;
    .registers 3
    .parameter "formatCode"

    #@0
    .prologue
    .line 461
    sget-object v0, Landroid/media/MediaFile;->sFormatToMimeTypeMap:Ljava/util/HashMap;

    #@2
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Ljava/lang/String;

    #@c
    return-object v0
.end method

.method public static isAudioFileType(I)Z
    .registers 3
    .parameter "fileType"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 363
    if-lt p0, v0, :cond_7

    #@3
    const/16 v1, 0x10

    #@5
    if-le p0, v1, :cond_17

    #@7
    :cond_7
    const/16 v1, 0x11

    #@9
    if-lt p0, v1, :cond_f

    #@b
    const/16 v1, 0x13

    #@d
    if-le p0, v1, :cond_17

    #@f
    :cond_f
    const/16 v1, 0x12c

    #@11
    if-lt p0, v1, :cond_18

    #@13
    const/16 v1, 0x12e

    #@15
    if-gt p0, v1, :cond_18

    #@17
    :cond_17
    :goto_17
    return v0

    #@18
    :cond_18
    const/4 v0, 0x0

    #@19
    goto :goto_17
.end method

.method public static isDrmFileType(I)Z
    .registers 2
    .parameter "fileType"

    #@0
    .prologue
    .line 391
    const/16 v0, 0x33

    #@2
    if-lt p0, v0, :cond_a

    #@4
    const/16 v0, 0x39

    #@6
    if-gt p0, v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public static isImageFileType(I)Z
    .registers 2
    .parameter "fileType"

    #@0
    .prologue
    .line 379
    const/16 v0, 0x20

    #@2
    if-lt p0, v0, :cond_8

    #@4
    const/16 v0, 0x25

    #@6
    if-le p0, v0, :cond_10

    #@8
    :cond_8
    const/16 v0, 0x190

    #@a
    if-lt p0, v0, :cond_12

    #@c
    const/16 v0, 0x192

    #@e
    if-gt p0, v0, :cond_12

    #@10
    :cond_10
    const/4 v0, 0x1

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method public static isMimeTypeMedia(Ljava/lang/String;)Z
    .registers 3
    .parameter "mimeType"

    #@0
    .prologue
    .line 409
    invoke-static {p0}, Landroid/media/MediaFile;->getFileTypeForMimeType(Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    .line 410
    .local v0, fileType:I
    invoke-static {v0}, Landroid/media/MediaFile;->isAudioFileType(I)Z

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_1c

    #@a
    invoke-static {v0}, Landroid/media/MediaFile;->isVideoFileType(I)Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_1c

    #@10
    invoke-static {v0}, Landroid/media/MediaFile;->isImageFileType(I)Z

    #@13
    move-result v1

    #@14
    if-nez v1, :cond_1c

    #@16
    invoke-static {v0}, Landroid/media/MediaFile;->isPlayListFileType(I)Z

    #@19
    move-result v1

    #@1a
    if-eqz v1, :cond_1e

    #@1c
    :cond_1c
    const/4 v1, 0x1

    #@1d
    :goto_1d
    return v1

    #@1e
    :cond_1e
    const/4 v1, 0x0

    #@1f
    goto :goto_1d
.end method

.method public static isPlayListFileType(I)Z
    .registers 2
    .parameter "fileType"

    #@0
    .prologue
    .line 386
    const/16 v0, 0x29

    #@2
    if-lt p0, v0, :cond_a

    #@4
    const/16 v0, 0x2c

    #@6
    if-gt p0, v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public static isTDMBFileType(I)Z
    .registers 2
    .parameter "fileType"

    #@0
    .prologue
    .line 397
    const/16 v0, 0xc9

    #@2
    if-eq p0, v0, :cond_8

    #@4
    const/16 v0, 0x190

    #@6
    if-ne p0, v0, :cond_a

    #@8
    :cond_8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public static isVideoFileType(I)Z
    .registers 2
    .parameter "fileType"

    #@0
    .prologue
    .line 372
    const/16 v0, 0x15

    #@2
    if-lt p0, v0, :cond_8

    #@4
    const/16 v0, 0x1f

    #@6
    if-le p0, v0, :cond_10

    #@8
    :cond_8
    const/16 v0, 0xc8

    #@a
    if-lt p0, v0, :cond_12

    #@c
    const/16 v0, 0xd0

    #@e
    if-gt p0, v0, :cond_12

    #@10
    :cond_10
    const/4 v0, 0x1

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method private static isWMAEnabled()Z
    .registers 5

    #@0
    .prologue
    .line 212
    invoke-static {}, Landroid/media/DecoderCapabilities;->getAudioDecoders()Ljava/util/List;

    #@3
    move-result-object v2

    #@4
    .line 213
    .local v2, decoders:Ljava/util/List;,"Ljava/util/List<Landroid/media/DecoderCapabilities$AudioDecoder;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@7
    move-result v0

    #@8
    .line 214
    .local v0, count:I
    const/4 v3, 0x0

    #@9
    .local v3, i:I
    :goto_9
    if-ge v3, v0, :cond_1a

    #@b
    .line 215
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Landroid/media/DecoderCapabilities$AudioDecoder;

    #@11
    .line 216
    .local v1, decoder:Landroid/media/DecoderCapabilities$AudioDecoder;
    sget-object v4, Landroid/media/DecoderCapabilities$AudioDecoder;->AUDIO_DECODER_WMA:Landroid/media/DecoderCapabilities$AudioDecoder;

    #@13
    if-ne v1, v4, :cond_17

    #@15
    .line 217
    const/4 v4, 0x1

    #@16
    .line 220
    .end local v1           #decoder:Landroid/media/DecoderCapabilities$AudioDecoder;
    :goto_16
    return v4

    #@17
    .line 214
    .restart local v1       #decoder:Landroid/media/DecoderCapabilities$AudioDecoder;
    :cond_17
    add-int/lit8 v3, v3, 0x1

    #@19
    goto :goto_9

    #@1a
    .line 220
    .end local v1           #decoder:Landroid/media/DecoderCapabilities$AudioDecoder;
    :cond_1a
    const/4 v4, 0x0

    #@1b
    goto :goto_16
.end method

.method private static isWMVEnabled()Z
    .registers 5

    #@0
    .prologue
    .line 224
    invoke-static {}, Landroid/media/DecoderCapabilities;->getVideoDecoders()Ljava/util/List;

    #@3
    move-result-object v2

    #@4
    .line 225
    .local v2, decoders:Ljava/util/List;,"Ljava/util/List<Landroid/media/DecoderCapabilities$VideoDecoder;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@7
    move-result v0

    #@8
    .line 226
    .local v0, count:I
    const/4 v3, 0x0

    #@9
    .local v3, i:I
    :goto_9
    if-ge v3, v0, :cond_1a

    #@b
    .line 227
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Landroid/media/DecoderCapabilities$VideoDecoder;

    #@11
    .line 228
    .local v1, decoder:Landroid/media/DecoderCapabilities$VideoDecoder;
    sget-object v4, Landroid/media/DecoderCapabilities$VideoDecoder;->VIDEO_DECODER_WMV:Landroid/media/DecoderCapabilities$VideoDecoder;

    #@13
    if-ne v1, v4, :cond_17

    #@15
    .line 229
    const/4 v4, 0x1

    #@16
    .line 232
    .end local v1           #decoder:Landroid/media/DecoderCapabilities$VideoDecoder;
    :goto_16
    return v4

    #@17
    .line 226
    .restart local v1       #decoder:Landroid/media/DecoderCapabilities$VideoDecoder;
    :cond_17
    add-int/lit8 v3, v3, 0x1

    #@19
    goto :goto_9

    #@1a
    .line 232
    .end local v1           #decoder:Landroid/media/DecoderCapabilities$VideoDecoder;
    :cond_1a
    const/4 v4, 0x0

    #@1b
    goto :goto_16
.end method
