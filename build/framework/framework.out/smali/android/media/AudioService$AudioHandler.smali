.class Landroid/media/AudioService$AudioHandler;
.super Landroid/os/Handler;
.source "AudioService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/AudioService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AudioHandler"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/media/AudioService;


# direct methods
.method private constructor <init>(Landroid/media/AudioService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 3790
    iput-object p1, p0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/media/AudioService;Landroid/media/AudioService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 3790
    invoke-direct {p0, p1}, Landroid/media/AudioService$AudioHandler;-><init>(Landroid/media/AudioService;)V

    #@3
    return-void
.end method

.method static synthetic access$5200(Landroid/media/AudioService$AudioHandler;Landroid/media/MediaPlayer;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 3790
    invoke-direct {p0, p1}, Landroid/media/AudioService$AudioHandler;->cleanupPlayer(Landroid/media/MediaPlayer;)V

    #@3
    return-void
.end method

.method private cleanupPlayer(Landroid/media/MediaPlayer;)V
    .registers 6
    .parameter "mp"

    #@0
    .prologue
    .line 3934
    if-eqz p1, :cond_8

    #@2
    .line 3936
    :try_start_2
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->stop()V

    #@5
    .line 3937
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->release()V
    :try_end_8
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_8} :catch_9

    #@8
    .line 3942
    :cond_8
    :goto_8
    return-void

    #@9
    .line 3938
    :catch_9
    move-exception v0

    #@a
    .line 3939
    .local v0, ex:Ljava/lang/IllegalStateException;
    const-string v1, "AudioService"

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "MediaPlayer IllegalStateException: "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    goto :goto_8
.end method

.method private onHandlePersistMediaButtonReceiver(Landroid/content/ComponentName;)V
    .registers 6
    .parameter "receiver"

    #@0
    .prologue
    .line 3927
    iget-object v0, p0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@2
    invoke-static {v0}, Landroid/media/AudioService;->access$3600(Landroid/media/AudioService;)Landroid/content/ContentResolver;

    #@5
    move-result-object v1

    #@6
    const-string/jumbo v2, "media_button_receiver"

    #@9
    if-nez p1, :cond_12

    #@b
    const-string v0, ""

    #@d
    :goto_d
    const/4 v3, -0x2

    #@e
    invoke-static {v1, v2, v0, v3}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    #@11
    .line 3931
    return-void

    #@12
    .line 3927
    :cond_12
    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    goto :goto_d
.end method

.method private onPersistSafeVolumeState(I)V
    .registers 4
    .parameter "state"

    #@0
    .prologue
    .line 3949
    iget-object v0, p0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@2
    invoke-static {v0}, Landroid/media/AudioService;->access$3600(Landroid/media/AudioService;)Landroid/content/ContentResolver;

    #@5
    move-result-object v0

    #@6
    const-string v1, "audio_safe_volume_state"

    #@8
    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@b
    .line 3952
    return-void
.end method

.method private persistRingerMode(I)V
    .registers 4
    .parameter "ringerMode"

    #@0
    .prologue
    .line 3856
    iget-object v0, p0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@2
    invoke-static {v0}, Landroid/media/AudioService;->access$3600(Landroid/media/AudioService;)Landroid/content/ContentResolver;

    #@5
    move-result-object v0

    #@6
    const-string/jumbo v1, "mode_ringer"

    #@9
    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@c
    .line 3858
    const-string/jumbo v0, "persist.sys.sound_enable"

    #@f
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@16
    .line 3860
    return-void
.end method

.method private persistVolume(Landroid/media/AudioService$VolumeStreamState;II)V
    .registers 10
    .parameter "streamState"
    .parameter "persistType"
    .parameter "device"

    #@0
    .prologue
    const/4 v5, -0x2

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 3835
    and-int/lit8 v0, p2, 0x1

    #@5
    if-eqz v0, :cond_1c

    #@7
    .line 3836
    iget-object v0, p0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@9
    invoke-static {v0}, Landroid/media/AudioService;->access$3600(Landroid/media/AudioService;)Landroid/content/ContentResolver;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {p1, v3, p3}, Landroid/media/AudioService$VolumeStreamState;->getSettingNameForDevice(ZI)Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {p1, p3, v3}, Landroid/media/AudioService$VolumeStreamState;->getIndex(IZ)I

    #@14
    move-result v2

    #@15
    add-int/lit8 v2, v2, 0x5

    #@17
    div-int/lit8 v2, v2, 0xa

    #@19
    invoke-static {v0, v1, v2, v5}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    #@1c
    .line 3841
    :cond_1c
    and-int/lit8 v0, p2, 0x2

    #@1e
    if-eqz v0, :cond_35

    #@20
    .line 3842
    iget-object v0, p0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@22
    invoke-static {v0}, Landroid/media/AudioService;->access$3600(Landroid/media/AudioService;)Landroid/content/ContentResolver;

    #@25
    move-result-object v0

    #@26
    invoke-virtual {p1, v4, p3}, Landroid/media/AudioService$VolumeStreamState;->getSettingNameForDevice(ZI)Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {p1, p3, v4}, Landroid/media/AudioService$VolumeStreamState;->getIndex(IZ)I

    #@2d
    move-result v2

    #@2e
    add-int/lit8 v2, v2, 0x5

    #@30
    div-int/lit8 v2, v2, 0xa

    #@32
    invoke-static {v0, v1, v2, v5}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    #@35
    .line 3848
    :cond_35
    invoke-static {p1}, Landroid/media/AudioService$VolumeStreamState;->access$4400(Landroid/media/AudioService$VolumeStreamState;)I

    #@38
    move-result v0

    #@39
    if-ne v0, v4, :cond_6e

    #@3b
    .line 3849
    const-string/jumbo v0, "persist.sys.system_volume"

    #@3e
    invoke-virtual {p1, p3, v3}, Landroid/media/AudioService$VolumeStreamState;->getIndex(IZ)I

    #@41
    move-result v1

    #@42
    add-int/lit8 v1, v1, 0x5

    #@44
    div-int/lit8 v1, v1, 0xa

    #@46
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@49
    move-result-object v1

    #@4a
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@4d
    .line 3850
    const-string v0, "AudioService"

    #@4f
    new-instance v1, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    const-string/jumbo v2, "persistVolume vol: "

    #@57
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v1

    #@5b
    invoke-virtual {p1, p3, v3}, Landroid/media/AudioService$VolumeStreamState;->getIndex(IZ)I

    #@5e
    move-result v2

    #@5f
    add-int/lit8 v2, v2, 0x5

    #@61
    div-int/lit8 v2, v2, 0xa

    #@63
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@66
    move-result-object v1

    #@67
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v1

    #@6b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    .line 3853
    :cond_6e
    return-void
.end method

.method private playSoundEffect(II)V
    .registers 14
    .parameter "effectType"
    .parameter "volume"

    #@0
    .prologue
    .line 3863
    iget-object v0, p0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@2
    invoke-static {v0}, Landroid/media/AudioService;->access$1700(Landroid/media/AudioService;)Ljava/lang/Object;

    #@5
    move-result-object v10

    #@6
    monitor-enter v10

    #@7
    .line 3864
    :try_start_7
    iget-object v0, p0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@9
    invoke-static {v0}, Landroid/media/AudioService;->access$1800(Landroid/media/AudioService;)Landroid/media/SoundPool;

    #@c
    move-result-object v0

    #@d
    if-nez v0, :cond_19

    #@f
    .line 3865
    const-string v0, "AudioService"

    #@11
    const-string/jumbo v1, "playSoundEffect mSoundPool is null."

    #@14
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 3866
    monitor-exit v10

    #@18
    .line 3924
    :goto_18
    return-void

    #@19
    .line 3869
    :cond_19
    iget-object v0, p0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@1b
    const/4 v1, 0x1

    #@1c
    invoke-virtual {v0, v1}, Landroid/media/AudioService;->checkPlayConditions(I)Z

    #@1f
    move-result v0

    #@20
    if-nez v0, :cond_27

    #@22
    monitor-exit v10

    #@23
    goto :goto_18

    #@24
    .line 3923
    :catchall_24
    move-exception v0

    #@25
    monitor-exit v10
    :try_end_26
    .catchall {:try_start_7 .. :try_end_26} :catchall_24

    #@26
    throw v0

    #@27
    .line 3872
    :cond_27
    if-nez p1, :cond_87

    #@29
    :try_start_29
    iget-object v0, p0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@2b
    invoke-static {v0}, Landroid/media/AudioService;->access$4700(Landroid/media/AudioService;)I

    #@2e
    move-result v0

    #@2f
    if-nez v0, :cond_87

    #@31
    iget-object v0, p0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@33
    invoke-static {v0}, Landroid/media/AudioService;->access$4800(Landroid/media/AudioService;)Ljava/util/Stack;

    #@36
    move-result-object v0

    #@37
    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    #@3a
    move-result v0

    #@3b
    if-nez v0, :cond_87

    #@3d
    iget-object v0, p0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@3f
    invoke-static {v0}, Landroid/media/AudioService;->access$4800(Landroid/media/AudioService;)Ljava/util/Stack;

    #@42
    move-result-object v0

    #@43
    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    #@46
    move-result-object v0

    #@47
    check-cast v0, Landroid/media/AudioService$FocusStackEntry;

    #@49
    iget-object v0, v0, Landroid/media/AudioService$FocusStackEntry;->mPackageName:Ljava/lang/String;

    #@4b
    const-string v1, "com.lge.camera"

    #@4d
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@50
    move-result v0

    #@51
    if-eqz v0, :cond_87

    #@53
    iget-object v0, p0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@55
    invoke-static {v0}, Landroid/media/AudioService;->access$400(Landroid/media/AudioService;)Ljava/util/HashMap;

    #@58
    move-result-object v0

    #@59
    const/4 v1, 0x4

    #@5a
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5d
    move-result-object v1

    #@5e
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@61
    move-result v0

    #@62
    if-nez v0, :cond_7e

    #@64
    iget-object v0, p0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@66
    invoke-static {v0}, Landroid/media/AudioService;->access$400(Landroid/media/AudioService;)Ljava/util/HashMap;

    #@69
    move-result-object v0

    #@6a
    const/16 v1, 0x8

    #@6c
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6f
    move-result-object v1

    #@70
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@73
    move-result v0

    #@74
    if-nez v0, :cond_7e

    #@76
    iget-object v0, p0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@78
    invoke-static {v0}, Landroid/media/AudioService;->access$4900(Landroid/media/AudioService;)Z

    #@7b
    move-result v0

    #@7c
    if-eqz v0, :cond_87

    #@7e
    .line 3877
    :cond_7e
    const-string v0, "AudioService"

    #@80
    const-string v1, "do not play touch effect sound for guarantee camera timer or shutter sound"

    #@82
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@85
    .line 3878
    monitor-exit v10

    #@86
    goto :goto_18

    #@87
    .line 3885
    :cond_87
    if-gez p2, :cond_b4

    #@89
    .line 3887
    const/high16 v2, 0x3f80

    #@8b
    .line 3893
    .local v2, volFloat:F
    :goto_8b
    iget-object v0, p0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@8d
    invoke-static {v0}, Landroid/media/AudioService;->access$5000(Landroid/media/AudioService;)[[I

    #@90
    move-result-object v0

    #@91
    aget-object v0, v0, p1

    #@93
    const/4 v1, 0x1

    #@94
    aget v0, v0, v1

    #@96
    if-lez v0, :cond_ba

    #@98
    .line 3894
    iget-object v0, p0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@9a
    invoke-static {v0}, Landroid/media/AudioService;->access$1800(Landroid/media/AudioService;)Landroid/media/SoundPool;

    #@9d
    move-result-object v0

    #@9e
    iget-object v1, p0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@a0
    invoke-static {v1}, Landroid/media/AudioService;->access$5000(Landroid/media/AudioService;)[[I

    #@a3
    move-result-object v1

    #@a4
    aget-object v1, v1, p1

    #@a6
    const/4 v3, 0x1

    #@a7
    aget v1, v1, v3

    #@a9
    const/4 v4, 0x0

    #@aa
    const/4 v5, 0x0

    #@ab
    const/high16 v6, 0x3f80

    #@ad
    move v3, v2

    #@ae
    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    #@b1
    .line 3923
    :goto_b1
    monitor-exit v10

    #@b2
    goto/16 :goto_18

    #@b4
    .line 3890
    .end local v2           #volFloat:F
    :cond_b4
    int-to-float v0, p2

    #@b5
    const/high16 v1, 0x447a

    #@b7
    div-float v2, v0, v1

    #@b9
    .restart local v2       #volFloat:F
    goto :goto_8b

    #@ba
    .line 3896
    :cond_ba
    new-instance v9, Landroid/media/MediaPlayer;

    #@bc
    invoke-direct {v9}, Landroid/media/MediaPlayer;-><init>()V
    :try_end_bf
    .catchall {:try_start_29 .. :try_end_bf} :catchall_24

    #@bf
    .line 3898
    .local v9, mediaPlayer:Landroid/media/MediaPlayer;
    :try_start_bf
    new-instance v0, Ljava/lang/StringBuilder;

    #@c1
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@c4
    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    #@c7
    move-result-object v1

    #@c8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v0

    #@cc
    const-string v1, "/media/audio/ui/"

    #@ce
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v0

    #@d2
    invoke-static {}, Landroid/media/AudioService;->access$5100()[Ljava/lang/String;

    #@d5
    move-result-object v1

    #@d6
    iget-object v3, p0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@d8
    invoke-static {v3}, Landroid/media/AudioService;->access$5000(Landroid/media/AudioService;)[[I

    #@db
    move-result-object v3

    #@dc
    aget-object v3, v3, p1

    #@de
    const/4 v4, 0x0

    #@df
    aget v3, v3, v4

    #@e1
    aget-object v1, v1, v3

    #@e3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e6
    move-result-object v0

    #@e7
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ea
    move-result-object v8

    #@eb
    .line 3899
    .local v8, filePath:Ljava/lang/String;
    invoke-virtual {v9, v8}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    #@ee
    .line 3900
    const/4 v0, 0x1

    #@ef
    invoke-virtual {v9, v0}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    #@f2
    .line 3901
    invoke-virtual {v9}, Landroid/media/MediaPlayer;->prepare()V

    #@f5
    .line 3902
    invoke-virtual {v9, v2, v2}, Landroid/media/MediaPlayer;->setVolume(FF)V

    #@f8
    .line 3903
    new-instance v0, Landroid/media/AudioService$AudioHandler$1;

    #@fa
    invoke-direct {v0, p0}, Landroid/media/AudioService$AudioHandler$1;-><init>(Landroid/media/AudioService$AudioHandler;)V

    #@fd
    invoke-virtual {v9, v0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    #@100
    .line 3908
    new-instance v0, Landroid/media/AudioService$AudioHandler$2;

    #@102
    invoke-direct {v0, p0}, Landroid/media/AudioService$AudioHandler$2;-><init>(Landroid/media/AudioService$AudioHandler;)V

    #@105
    invoke-virtual {v9, v0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    #@108
    .line 3914
    invoke-virtual {v9}, Landroid/media/MediaPlayer;->start()V
    :try_end_10b
    .catchall {:try_start_bf .. :try_end_10b} :catchall_24
    .catch Ljava/io/IOException; {:try_start_bf .. :try_end_10b} :catch_10c
    .catch Ljava/lang/IllegalArgumentException; {:try_start_bf .. :try_end_10b} :catch_126
    .catch Ljava/lang/IllegalStateException; {:try_start_bf .. :try_end_10b} :catch_141

    #@10b
    goto :goto_b1

    #@10c
    .line 3915
    .end local v8           #filePath:Ljava/lang/String;
    :catch_10c
    move-exception v7

    #@10d
    .line 3916
    .local v7, ex:Ljava/io/IOException;
    :try_start_10d
    const-string v0, "AudioService"

    #@10f
    new-instance v1, Ljava/lang/StringBuilder;

    #@111
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@114
    const-string v3, "MediaPlayer IOException: "

    #@116
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@119
    move-result-object v1

    #@11a
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v1

    #@11e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@121
    move-result-object v1

    #@122
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@125
    goto :goto_b1

    #@126
    .line 3917
    .end local v7           #ex:Ljava/io/IOException;
    :catch_126
    move-exception v7

    #@127
    .line 3918
    .local v7, ex:Ljava/lang/IllegalArgumentException;
    const-string v0, "AudioService"

    #@129
    new-instance v1, Ljava/lang/StringBuilder;

    #@12b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@12e
    const-string v3, "MediaPlayer IllegalArgumentException: "

    #@130
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@133
    move-result-object v1

    #@134
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@137
    move-result-object v1

    #@138
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13b
    move-result-object v1

    #@13c
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@13f
    goto/16 :goto_b1

    #@141
    .line 3919
    .end local v7           #ex:Ljava/lang/IllegalArgumentException;
    :catch_141
    move-exception v7

    #@142
    .line 3920
    .local v7, ex:Ljava/lang/IllegalStateException;
    const-string v0, "AudioService"

    #@144
    new-instance v1, Ljava/lang/StringBuilder;

    #@146
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@149
    const-string v3, "MediaPlayer IllegalStateException: "

    #@14b
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14e
    move-result-object v1

    #@14f
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@152
    move-result-object v1

    #@153
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@156
    move-result-object v1

    #@157
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_15a
    .catchall {:try_start_10d .. :try_end_15a} :catchall_24

    #@15a
    goto/16 :goto_b1
.end method

.method private setAllVolumes(Landroid/media/AudioService$VolumeStreamState;)V
    .registers 6
    .parameter "streamState"

    #@0
    .prologue
    .line 3820
    invoke-virtual {p1}, Landroid/media/AudioService$VolumeStreamState;->applyAllVolumes()V

    #@3
    .line 3823
    invoke-static {}, Landroid/media/AudioSystem;->getNumStreamTypes()I

    #@6
    move-result v0

    #@7
    .line 3824
    .local v0, numStreamTypes:I
    add-int/lit8 v1, v0, -0x1

    #@9
    .local v1, streamType:I
    :goto_9
    if-ltz v1, :cond_2d

    #@b
    .line 3825
    invoke-static {p1}, Landroid/media/AudioService$VolumeStreamState;->access$4400(Landroid/media/AudioService$VolumeStreamState;)I

    #@e
    move-result v2

    #@f
    if-eq v1, v2, :cond_2a

    #@11
    iget-object v2, p0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@13
    invoke-static {v2}, Landroid/media/AudioService;->access$3500(Landroid/media/AudioService;)[I

    #@16
    move-result-object v2

    #@17
    aget v2, v2, v1

    #@19
    invoke-static {p1}, Landroid/media/AudioService$VolumeStreamState;->access$4400(Landroid/media/AudioService$VolumeStreamState;)I

    #@1c
    move-result v3

    #@1d
    if-ne v2, v3, :cond_2a

    #@1f
    .line 3827
    iget-object v2, p0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@21
    invoke-static {v2}, Landroid/media/AudioService;->access$4200(Landroid/media/AudioService;)[Landroid/media/AudioService$VolumeStreamState;

    #@24
    move-result-object v2

    #@25
    aget-object v2, v2, v1

    #@27
    invoke-virtual {v2}, Landroid/media/AudioService$VolumeStreamState;->applyAllVolumes()V

    #@2a
    .line 3824
    :cond_2a
    add-int/lit8 v1, v1, -0x1

    #@2c
    goto :goto_9

    #@2d
    .line 3830
    :cond_2d
    return-void
.end method

.method private setDeviceVolume(Landroid/media/AudioService$VolumeStreamState;I)V
    .registers 12
    .parameter "streamState"
    .parameter "device"

    #@0
    .prologue
    .line 3795
    invoke-virtual {p1, p2}, Landroid/media/AudioService$VolumeStreamState;->applyDeviceVolume(I)V

    #@3
    .line 3798
    invoke-static {}, Landroid/media/AudioSystem;->getNumStreamTypes()I

    #@6
    move-result v7

    #@7
    .line 3799
    .local v7, numStreamTypes:I
    add-int/lit8 v8, v7, -0x1

    #@9
    .local v8, streamType:I
    :goto_9
    if-ltz v8, :cond_33

    #@b
    .line 3800
    invoke-static {p1}, Landroid/media/AudioService$VolumeStreamState;->access$4400(Landroid/media/AudioService$VolumeStreamState;)I

    #@e
    move-result v0

    #@f
    if-eq v8, v0, :cond_30

    #@11
    iget-object v0, p0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@13
    invoke-static {v0}, Landroid/media/AudioService;->access$3500(Landroid/media/AudioService;)[I

    #@16
    move-result-object v0

    #@17
    aget v0, v0, v8

    #@19
    invoke-static {p1}, Landroid/media/AudioService$VolumeStreamState;->access$4400(Landroid/media/AudioService$VolumeStreamState;)I

    #@1c
    move-result v1

    #@1d
    if-ne v0, v1, :cond_30

    #@1f
    .line 3802
    iget-object v0, p0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@21
    invoke-static {v0}, Landroid/media/AudioService;->access$4200(Landroid/media/AudioService;)[Landroid/media/AudioService$VolumeStreamState;

    #@24
    move-result-object v0

    #@25
    aget-object v0, v0, v8

    #@27
    iget-object v1, p0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@29
    invoke-static {v1, v8}, Landroid/media/AudioService;->access$4000(Landroid/media/AudioService;I)I

    #@2c
    move-result v1

    #@2d
    invoke-virtual {v0, v1}, Landroid/media/AudioService$VolumeStreamState;->applyDeviceVolume(I)V

    #@30
    .line 3799
    :cond_30
    add-int/lit8 v8, v8, -0x1

    #@32
    goto :goto_9

    #@33
    .line 3807
    :cond_33
    iget-object v0, p0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@35
    invoke-static {v0}, Landroid/media/AudioService;->access$100(Landroid/media/AudioService;)Landroid/media/AudioService$AudioHandler;

    #@38
    move-result-object v0

    #@39
    const/4 v1, 0x1

    #@3a
    const/4 v2, 0x2

    #@3b
    const/4 v3, 0x3

    #@3c
    const/16 v6, 0x1f4

    #@3e
    move v4, p2

    #@3f
    move-object v5, p1

    #@40
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->access$200(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@43
    .line 3815
    return-void
.end method

.method private setForceUse(II)V
    .registers 3
    .parameter "usage"
    .parameter "config"

    #@0
    .prologue
    .line 3945
    invoke-static {p1, p2}, Landroid/media/AudioSystem;->setForceUse(II)I

    #@3
    .line 3946
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 19
    .parameter "msg"

    #@0
    .prologue
    .line 3957
    move-object/from16 v0, p1

    #@2
    iget v1, v0, Landroid/os/Message;->what:I

    #@4
    packed-switch v1, :pswitch_data_340

    #@7
    .line 4174
    :cond_7
    :goto_7
    return-void

    #@8
    .line 3960
    :pswitch_8
    move-object/from16 v0, p1

    #@a
    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@c
    check-cast v1, Landroid/media/AudioService$VolumeStreamState;

    #@e
    move-object/from16 v0, p1

    #@10
    iget v2, v0, Landroid/os/Message;->arg1:I

    #@12
    move-object/from16 v0, p0

    #@14
    invoke-direct {v0, v1, v2}, Landroid/media/AudioService$AudioHandler;->setDeviceVolume(Landroid/media/AudioService$VolumeStreamState;I)V

    #@17
    goto :goto_7

    #@18
    .line 3964
    :pswitch_18
    move-object/from16 v0, p1

    #@1a
    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1c
    check-cast v1, Landroid/media/AudioService$VolumeStreamState;

    #@1e
    move-object/from16 v0, p0

    #@20
    invoke-direct {v0, v1}, Landroid/media/AudioService$AudioHandler;->setAllVolumes(Landroid/media/AudioService$VolumeStreamState;)V

    #@23
    goto :goto_7

    #@24
    .line 3968
    :pswitch_24
    move-object/from16 v0, p1

    #@26
    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@28
    check-cast v1, Landroid/media/AudioService$VolumeStreamState;

    #@2a
    move-object/from16 v0, p1

    #@2c
    iget v2, v0, Landroid/os/Message;->arg1:I

    #@2e
    move-object/from16 v0, p1

    #@30
    iget v3, v0, Landroid/os/Message;->arg2:I

    #@32
    move-object/from16 v0, p0

    #@34
    invoke-direct {v0, v1, v2, v3}, Landroid/media/AudioService$AudioHandler;->persistVolume(Landroid/media/AudioService$VolumeStreamState;II)V

    #@37
    goto :goto_7

    #@38
    .line 3972
    :pswitch_38
    move-object/from16 v0, p0

    #@3a
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@3c
    invoke-static {v1}, Landroid/media/AudioService;->access$3600(Landroid/media/AudioService;)Landroid/content/ContentResolver;

    #@3f
    move-result-object v1

    #@40
    const-string/jumbo v2, "volume_master"

    #@43
    move-object/from16 v0, p1

    #@45
    iget v3, v0, Landroid/os/Message;->arg1:I

    #@47
    int-to-float v3, v3

    #@48
    const/high16 v4, 0x447a

    #@4a
    div-float/2addr v3, v4

    #@4b
    const/4 v4, -0x2

    #@4c
    invoke-static {v1, v2, v3, v4}, Landroid/provider/Settings$System;->putFloatForUser(Landroid/content/ContentResolver;Ljava/lang/String;FI)Z

    #@4f
    goto :goto_7

    #@50
    .line 3979
    :pswitch_50
    move-object/from16 v0, p0

    #@52
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@54
    invoke-static {v1}, Landroid/media/AudioService;->access$3600(Landroid/media/AudioService;)Landroid/content/ContentResolver;

    #@57
    move-result-object v1

    #@58
    const-string/jumbo v2, "volume_master_mute"

    #@5b
    move-object/from16 v0, p1

    #@5d
    iget v3, v0, Landroid/os/Message;->arg1:I

    #@5f
    const/4 v4, -0x2

    #@60
    invoke-static {v1, v2, v3, v4}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    #@63
    goto :goto_7

    #@64
    .line 3988
    :pswitch_64
    move-object/from16 v0, p0

    #@66
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@68
    invoke-virtual {v1}, Landroid/media/AudioService;->getRingerMode()I

    #@6b
    move-result v1

    #@6c
    move-object/from16 v0, p0

    #@6e
    invoke-direct {v0, v1}, Landroid/media/AudioService$AudioHandler;->persistRingerMode(I)V

    #@71
    goto :goto_7

    #@72
    .line 3992
    :pswitch_72
    move-object/from16 v0, p0

    #@74
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@76
    invoke-static {v1}, Landroid/media/AudioService;->access$000(Landroid/media/AudioService;)Z

    #@79
    move-result v1

    #@7a
    if-nez v1, :cond_7

    #@7c
    .line 3993
    const-string v1, "AudioService"

    #@7e
    const-string v2, "Media server died."

    #@80
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@83
    .line 3996
    move-object/from16 v0, p0

    #@85
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@87
    invoke-static {v1}, Landroid/media/AudioService;->access$5300(Landroid/media/AudioService;)Landroid/media/AudioSystem$ErrorCallback;

    #@8a
    move-result-object v1

    #@8b
    invoke-static {v1}, Landroid/media/AudioSystem;->setErrorCallback(Landroid/media/AudioSystem$ErrorCallback;)V

    #@8e
    .line 3997
    move-object/from16 v0, p0

    #@90
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@92
    invoke-static {v1}, Landroid/media/AudioService;->access$100(Landroid/media/AudioService;)Landroid/media/AudioService$AudioHandler;

    #@95
    move-result-object v1

    #@96
    const/4 v2, 0x4

    #@97
    const/4 v3, 0x1

    #@98
    const/4 v4, 0x0

    #@99
    const/4 v5, 0x0

    #@9a
    const/4 v6, 0x0

    #@9b
    const/16 v7, 0x1f4

    #@9d
    invoke-static/range {v1 .. v7}, Landroid/media/AudioService;->access$200(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@a0
    goto/16 :goto_7

    #@a2
    .line 4003
    :pswitch_a2
    const-string v1, "AudioService"

    #@a4
    const-string v2, "Media server started."

    #@a6
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a9
    .line 4008
    const-string/jumbo v1, "restarting=true"

    #@ac
    invoke-static {v1}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    #@af
    .line 4011
    move-object/from16 v0, p0

    #@b1
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@b3
    invoke-static {v1}, Landroid/media/AudioService;->access$400(Landroid/media/AudioService;)Ljava/util/HashMap;

    #@b6
    move-result-object v2

    #@b7
    monitor-enter v2

    #@b8
    .line 4012
    :try_start_b8
    move-object/from16 v0, p0

    #@ba
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@bc
    invoke-static {v1}, Landroid/media/AudioService;->access$400(Landroid/media/AudioService;)Ljava/util/HashMap;

    #@bf
    move-result-object v1

    #@c0
    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@c3
    move-result-object v14

    #@c4
    .line 4013
    .local v14, set:Ljava/util/Set;
    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@c7
    move-result-object v10

    #@c8
    .line 4014
    .local v10, i:Ljava/util/Iterator;
    :goto_c8
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@cb
    move-result v1

    #@cc
    if-eqz v1, :cond_ec

    #@ce
    .line 4015
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@d1
    move-result-object v9

    #@d2
    check-cast v9, Ljava/util/Map$Entry;

    #@d4
    .line 4016
    .local v9, device:Ljava/util/Map$Entry;
    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@d7
    move-result-object v1

    #@d8
    check-cast v1, Ljava/lang/Integer;

    #@da
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    #@dd
    move-result v3

    #@de
    const/4 v4, 0x1

    #@df
    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@e2
    move-result-object v1

    #@e3
    check-cast v1, Ljava/lang/String;

    #@e5
    invoke-static {v3, v4, v1}, Landroid/media/AudioSystem;->setDeviceConnectionState(IILjava/lang/String;)I

    #@e8
    goto :goto_c8

    #@e9
    .line 4021
    .end local v9           #device:Ljava/util/Map$Entry;
    .end local v10           #i:Ljava/util/Iterator;
    .end local v14           #set:Ljava/util/Set;
    :catchall_e9
    move-exception v1

    #@ea
    monitor-exit v2
    :try_end_eb
    .catchall {:try_start_b8 .. :try_end_eb} :catchall_e9

    #@eb
    throw v1

    #@ec
    .restart local v10       #i:Ljava/util/Iterator;
    .restart local v14       #set:Ljava/util/Set;
    :cond_ec
    :try_start_ec
    monitor-exit v2
    :try_end_ed
    .catchall {:try_start_ec .. :try_end_ed} :catchall_e9

    #@ed
    .line 4023
    move-object/from16 v0, p0

    #@ef
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@f1
    invoke-static {v1}, Landroid/media/AudioService;->access$4700(Landroid/media/AudioService;)I

    #@f4
    move-result v1

    #@f5
    invoke-static {v1}, Landroid/media/AudioSystem;->setPhoneState(I)I

    #@f8
    .line 4026
    const/4 v1, 0x0

    #@f9
    move-object/from16 v0, p0

    #@fb
    iget-object v2, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@fd
    invoke-static {v2}, Landroid/media/AudioService;->access$5400(Landroid/media/AudioService;)I

    #@100
    move-result v2

    #@101
    invoke-static {v1, v2}, Landroid/media/AudioSystem;->setForceUse(II)I

    #@104
    .line 4027
    const/4 v1, 0x2

    #@105
    move-object/from16 v0, p0

    #@107
    iget-object v2, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@109
    invoke-static {v2}, Landroid/media/AudioService;->access$5400(Landroid/media/AudioService;)I

    #@10c
    move-result v2

    #@10d
    invoke-static {v1, v2}, Landroid/media/AudioSystem;->setForceUse(II)I

    #@110
    .line 4028
    const/4 v2, 0x4

    #@111
    move-object/from16 v0, p0

    #@113
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@115
    invoke-static {v1}, Landroid/media/AudioService;->access$3400(Landroid/media/AudioService;)Ljava/lang/Boolean;

    #@118
    move-result-object v1

    #@119
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    #@11c
    move-result v1

    #@11d
    if-eqz v1, :cond_14a

    #@11f
    const/16 v1, 0xb

    #@121
    :goto_121
    invoke-static {v2, v1}, Landroid/media/AudioSystem;->setForceUse(II)I

    #@124
    .line 4032
    invoke-static {}, Landroid/media/AudioSystem;->getNumStreamTypes()I

    #@127
    move-result v11

    #@128
    .line 4033
    .local v11, numStreamTypes:I
    add-int/lit8 v16, v11, -0x1

    #@12a
    .local v16, streamType:I
    :goto_12a
    if-ltz v16, :cond_14c

    #@12c
    .line 4034
    move-object/from16 v0, p0

    #@12e
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@130
    invoke-static {v1}, Landroid/media/AudioService;->access$4200(Landroid/media/AudioService;)[Landroid/media/AudioService$VolumeStreamState;

    #@133
    move-result-object v1

    #@134
    aget-object v15, v1, v16

    #@136
    .line 4035
    .local v15, streamState:Landroid/media/AudioService$VolumeStreamState;
    const/4 v1, 0x0

    #@137
    invoke-static {v15}, Landroid/media/AudioService$VolumeStreamState;->access$5500(Landroid/media/AudioService$VolumeStreamState;)I

    #@13a
    move-result v2

    #@13b
    add-int/lit8 v2, v2, 0x5

    #@13d
    div-int/lit8 v2, v2, 0xa

    #@13f
    move/from16 v0, v16

    #@141
    invoke-static {v0, v1, v2}, Landroid/media/AudioSystem;->initStreamVolume(III)I

    #@144
    .line 4037
    invoke-virtual {v15}, Landroid/media/AudioService$VolumeStreamState;->applyAllVolumes()V

    #@147
    .line 4033
    add-int/lit8 v16, v16, -0x1

    #@149
    goto :goto_12a

    #@14a
    .line 4028
    .end local v11           #numStreamTypes:I
    .end local v15           #streamState:Landroid/media/AudioService$VolumeStreamState;
    .end local v16           #streamType:I
    :cond_14a
    const/4 v1, 0x0

    #@14b
    goto :goto_121

    #@14c
    .line 4041
    .restart local v11       #numStreamTypes:I
    .restart local v16       #streamType:I
    :cond_14c
    move-object/from16 v0, p0

    #@14e
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@150
    move-object/from16 v0, p0

    #@152
    iget-object v2, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@154
    invoke-virtual {v2}, Landroid/media/AudioService;->getRingerMode()I

    #@157
    move-result v2

    #@158
    const/4 v3, 0x0

    #@159
    invoke-static {v1, v2, v3}, Landroid/media/AudioService;->access$5600(Landroid/media/AudioService;IZ)V

    #@15c
    .line 4044
    move-object/from16 v0, p0

    #@15e
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@160
    invoke-static {v1}, Landroid/media/AudioService;->access$5700(Landroid/media/AudioService;)V

    #@163
    .line 4047
    move-object/from16 v0, p0

    #@165
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@167
    invoke-static {v1}, Landroid/media/AudioService;->access$5800(Landroid/media/AudioService;)Z

    #@16a
    move-result v1

    #@16b
    if-eqz v1, :cond_174

    #@16d
    .line 4048
    move-object/from16 v0, p0

    #@16f
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@171
    invoke-static {v1}, Landroid/media/AudioService;->access$5900(Landroid/media/AudioService;)V

    #@174
    .line 4051
    :cond_174
    move-object/from16 v0, p0

    #@176
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@178
    invoke-static {v1}, Landroid/media/AudioService;->access$6000(Landroid/media/AudioService;)Ljava/lang/Object;

    #@17b
    move-result-object v2

    #@17c
    monitor-enter v2

    #@17d
    .line 4052
    const/4 v3, 0x1

    #@17e
    :try_start_17e
    move-object/from16 v0, p0

    #@180
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@182
    invoke-static {v1}, Landroid/media/AudioService;->access$4900(Landroid/media/AudioService;)Z

    #@185
    move-result v1

    #@186
    if-eqz v1, :cond_1af

    #@188
    const/4 v1, 0x0

    #@189
    :goto_189
    invoke-static {v3, v1}, Landroid/media/AudioSystem;->setForceUse(II)I

    #@18c
    .line 4055
    monitor-exit v2
    :try_end_18d
    .catchall {:try_start_17e .. :try_end_18d} :catchall_1b2

    #@18d
    .line 4057
    move-object/from16 v0, p0

    #@18f
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@191
    invoke-static {v1}, Landroid/media/AudioService;->access$6100(Landroid/media/AudioService;)Ljava/lang/Object;

    #@194
    move-result-object v2

    #@195
    monitor-enter v2

    #@196
    .line 4058
    const/4 v3, 0x3

    #@197
    :try_start_197
    move-object/from16 v0, p0

    #@199
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@19b
    invoke-static {v1}, Landroid/media/AudioService;->access$6200(Landroid/media/AudioService;)Z

    #@19e
    move-result v1

    #@19f
    if-eqz v1, :cond_1b5

    #@1a1
    const/16 v1, 0x8

    #@1a3
    :goto_1a3
    invoke-static {v3, v1}, Landroid/media/AudioSystem;->setForceUse(II)I

    #@1a6
    .line 4061
    monitor-exit v2
    :try_end_1a7
    .catchall {:try_start_197 .. :try_end_1a7} :catchall_1b7

    #@1a7
    .line 4064
    const-string/jumbo v1, "restarting=false"

    #@1aa
    invoke-static {v1}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    #@1ad
    goto/16 :goto_7

    #@1af
    .line 4052
    :cond_1af
    const/16 v1, 0xa

    #@1b1
    goto :goto_189

    #@1b2
    .line 4055
    :catchall_1b2
    move-exception v1

    #@1b3
    :try_start_1b3
    monitor-exit v2
    :try_end_1b4
    .catchall {:try_start_1b3 .. :try_end_1b4} :catchall_1b2

    #@1b4
    throw v1

    #@1b5
    .line 4058
    :cond_1b5
    const/4 v1, 0x0

    #@1b6
    goto :goto_1a3

    #@1b7
    .line 4061
    :catchall_1b7
    move-exception v1

    #@1b8
    :try_start_1b8
    monitor-exit v2
    :try_end_1b9
    .catchall {:try_start_1b8 .. :try_end_1b9} :catchall_1b7

    #@1b9
    throw v1

    #@1ba
    .line 4068
    .end local v10           #i:Ljava/util/Iterator;
    .end local v11           #numStreamTypes:I
    .end local v14           #set:Ljava/util/Set;
    .end local v16           #streamType:I
    :pswitch_1ba
    move-object/from16 v0, p0

    #@1bc
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@1be
    invoke-virtual {v1}, Landroid/media/AudioService;->loadSoundEffects()Z

    #@1c1
    goto/16 :goto_7

    #@1c3
    .line 4072
    :pswitch_1c3
    move-object/from16 v0, p1

    #@1c5
    iget v1, v0, Landroid/os/Message;->arg1:I

    #@1c7
    move-object/from16 v0, p1

    #@1c9
    iget v2, v0, Landroid/os/Message;->arg2:I

    #@1cb
    move-object/from16 v0, p0

    #@1cd
    invoke-direct {v0, v1, v2}, Landroid/media/AudioService$AudioHandler;->playSoundEffect(II)V

    #@1d0
    goto/16 :goto_7

    #@1d2
    .line 4077
    :pswitch_1d2
    move-object/from16 v0, p0

    #@1d4
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@1d6
    invoke-static {v1}, Landroid/media/AudioService;->access$400(Landroid/media/AudioService;)Ljava/util/HashMap;

    #@1d9
    move-result-object v2

    #@1da
    monitor-enter v2

    #@1db
    .line 4078
    :try_start_1db
    move-object/from16 v0, p0

    #@1dd
    iget-object v3, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@1df
    move-object/from16 v0, p1

    #@1e1
    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1e3
    check-cast v1, Ljava/lang/String;

    #@1e5
    invoke-static {v3, v1}, Landroid/media/AudioService;->access$3200(Landroid/media/AudioService;Ljava/lang/String;)V

    #@1e8
    .line 4079
    monitor-exit v2

    #@1e9
    goto/16 :goto_7

    #@1eb
    :catchall_1eb
    move-exception v1

    #@1ec
    monitor-exit v2
    :try_end_1ed
    .catchall {:try_start_1db .. :try_end_1ed} :catchall_1eb

    #@1ed
    throw v1

    #@1ee
    .line 4084
    :pswitch_1ee
    move-object/from16 v0, p1

    #@1f0
    iget v1, v0, Landroid/os/Message;->arg1:I

    #@1f2
    move-object/from16 v0, p1

    #@1f4
    iget v2, v0, Landroid/os/Message;->arg2:I

    #@1f6
    move-object/from16 v0, p0

    #@1f8
    invoke-direct {v0, v1, v2}, Landroid/media/AudioService$AudioHandler;->setForceUse(II)V

    #@1fb
    goto/16 :goto_7

    #@1fd
    .line 4088
    :pswitch_1fd
    move-object/from16 v0, p1

    #@1ff
    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@201
    check-cast v1, Landroid/content/ComponentName;

    #@203
    move-object/from16 v0, p0

    #@205
    invoke-direct {v0, v1}, Landroid/media/AudioService$AudioHandler;->onHandlePersistMediaButtonReceiver(Landroid/content/ComponentName;)V

    #@208
    goto/16 :goto_7

    #@20a
    .line 4092
    :pswitch_20a
    move-object/from16 v0, p0

    #@20c
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@20e
    invoke-static {v1}, Landroid/media/AudioService;->access$6300(Landroid/media/AudioService;)V

    #@211
    goto/16 :goto_7

    #@213
    .line 4097
    :pswitch_213
    move-object/from16 v0, p0

    #@215
    iget-object v2, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@217
    move-object/from16 v0, p1

    #@219
    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@21b
    check-cast v1, Landroid/media/AudioService$RemoteControlStackEntry;

    #@21d
    move-object/from16 v0, p1

    #@21f
    iget v3, v0, Landroid/os/Message;->arg1:I

    #@221
    invoke-static {v2, v1, v3}, Landroid/media/AudioService;->access$6400(Landroid/media/AudioService;Landroid/media/AudioService$RemoteControlStackEntry;I)V

    #@224
    goto/16 :goto_7

    #@226
    .line 4101
    :pswitch_226
    move-object/from16 v0, p0

    #@228
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@22a
    invoke-static {v1}, Landroid/media/AudioService;->access$6500(Landroid/media/AudioService;)V

    #@22d
    goto/16 :goto_7

    #@22f
    .line 4105
    :pswitch_22f
    move-object/from16 v0, p0

    #@231
    iget-object v2, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@233
    move-object/from16 v0, p1

    #@235
    iget v3, v0, Landroid/os/Message;->arg1:I

    #@237
    move-object/from16 v0, p1

    #@239
    iget v4, v0, Landroid/os/Message;->arg2:I

    #@23b
    move-object/from16 v0, p1

    #@23d
    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@23f
    check-cast v1, Ljava/lang/String;

    #@241
    invoke-static {v2, v3, v4, v1}, Landroid/media/AudioService;->access$6600(Landroid/media/AudioService;IILjava/lang/String;)V

    #@244
    .line 4106
    move-object/from16 v0, p0

    #@246
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@248
    invoke-static {v1}, Landroid/media/AudioService;->access$6700(Landroid/media/AudioService;)Landroid/os/PowerManager$WakeLock;

    #@24b
    move-result-object v1

    #@24c
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    #@24f
    goto/16 :goto_7

    #@251
    .line 4110
    :pswitch_251
    move-object/from16 v0, p0

    #@253
    iget-object v2, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@255
    move-object/from16 v0, p1

    #@257
    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@259
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@25b
    move-object/from16 v0, p1

    #@25d
    iget v3, v0, Landroid/os/Message;->arg1:I

    #@25f
    invoke-static {v2, v1, v3}, Landroid/media/AudioService;->access$6800(Landroid/media/AudioService;Landroid/bluetooth/BluetoothDevice;I)V

    #@262
    .line 4111
    move-object/from16 v0, p0

    #@264
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@266
    invoke-static {v1}, Landroid/media/AudioService;->access$6700(Landroid/media/AudioService;)Landroid/os/PowerManager$WakeLock;

    #@269
    move-result-object v1

    #@26a
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    #@26d
    goto/16 :goto_7

    #@26f
    .line 4115
    :pswitch_26f
    move-object/from16 v0, p0

    #@271
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@273
    iget-object v1, v1, Landroid/media/AudioService;->mRoutesObservers:Landroid/os/RemoteCallbackList;

    #@275
    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    #@278
    move-result v8

    #@279
    .line 4116
    .local v8, N:I
    if-lez v8, :cond_2a7

    #@27b
    .line 4118
    move-object/from16 v0, p0

    #@27d
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@27f
    iget-object v2, v1, Landroid/media/AudioService;->mCurAudioRoutes:Landroid/media/AudioRoutesInfo;

    #@281
    monitor-enter v2

    #@282
    .line 4119
    :try_start_282
    new-instance v13, Landroid/media/AudioRoutesInfo;

    #@284
    move-object/from16 v0, p0

    #@286
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@288
    iget-object v1, v1, Landroid/media/AudioService;->mCurAudioRoutes:Landroid/media/AudioRoutesInfo;

    #@28a
    invoke-direct {v13, v1}, Landroid/media/AudioRoutesInfo;-><init>(Landroid/media/AudioRoutesInfo;)V

    #@28d
    .line 4120
    .local v13, routes:Landroid/media/AudioRoutesInfo;
    monitor-exit v2
    :try_end_28e
    .catchall {:try_start_282 .. :try_end_28e} :catchall_2a4

    #@28e
    .line 4121
    :goto_28e
    if-lez v8, :cond_2a7

    #@290
    .line 4122
    add-int/lit8 v8, v8, -0x1

    #@292
    .line 4123
    move-object/from16 v0, p0

    #@294
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@296
    iget-object v1, v1, Landroid/media/AudioService;->mRoutesObservers:Landroid/os/RemoteCallbackList;

    #@298
    invoke-virtual {v1, v8}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@29b
    move-result-object v12

    #@29c
    check-cast v12, Landroid/media/IAudioRoutesObserver;

    #@29e
    .line 4125
    .local v12, obs:Landroid/media/IAudioRoutesObserver;
    :try_start_29e
    invoke-interface {v12, v13}, Landroid/media/IAudioRoutesObserver;->dispatchAudioRoutesChanged(Landroid/media/AudioRoutesInfo;)V
    :try_end_2a1
    .catch Landroid/os/RemoteException; {:try_start_29e .. :try_end_2a1} :catch_2a2

    #@2a1
    goto :goto_28e

    #@2a2
    .line 4126
    :catch_2a2
    move-exception v1

    #@2a3
    goto :goto_28e

    #@2a4
    .line 4120
    .end local v12           #obs:Landroid/media/IAudioRoutesObserver;
    .end local v13           #routes:Landroid/media/AudioRoutesInfo;
    :catchall_2a4
    move-exception v1

    #@2a5
    :try_start_2a5
    monitor-exit v2
    :try_end_2a6
    .catchall {:try_start_2a5 .. :try_end_2a6} :catchall_2a4

    #@2a6
    throw v1

    #@2a7
    .line 4130
    :cond_2a7
    move-object/from16 v0, p0

    #@2a9
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@2ab
    iget-object v1, v1, Landroid/media/AudioService;->mRoutesObservers:Landroid/os/RemoteCallbackList;

    #@2ad
    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@2b0
    goto/16 :goto_7

    #@2b2
    .line 4135
    .end local v8           #N:I
    :pswitch_2b2
    move-object/from16 v0, p0

    #@2b4
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@2b6
    invoke-static {v1}, Landroid/media/AudioService;->access$6900(Landroid/media/AudioService;)V

    #@2b9
    goto/16 :goto_7

    #@2bb
    .line 4139
    :pswitch_2bb
    move-object/from16 v0, p0

    #@2bd
    iget-object v2, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@2bf
    move-object/from16 v0, p1

    #@2c1
    iget v3, v0, Landroid/os/Message;->arg1:I

    #@2c3
    move-object/from16 v0, p1

    #@2c5
    iget v4, v0, Landroid/os/Message;->arg2:I

    #@2c7
    move-object/from16 v0, p1

    #@2c9
    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2cb
    check-cast v1, Ljava/lang/Integer;

    #@2cd
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    #@2d0
    move-result v1

    #@2d1
    invoke-static {v2, v3, v4, v1}, Landroid/media/AudioService;->access$7000(Landroid/media/AudioService;III)V

    #@2d4
    goto/16 :goto_7

    #@2d6
    .line 4143
    :pswitch_2d6
    move-object/from16 v0, p0

    #@2d8
    iget-object v2, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@2da
    move-object/from16 v0, p1

    #@2dc
    iget v3, v0, Landroid/os/Message;->arg1:I

    #@2de
    move-object/from16 v0, p1

    #@2e0
    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2e2
    check-cast v1, Landroid/media/IRemoteVolumeObserver;

    #@2e4
    invoke-static {v2, v3, v1}, Landroid/media/AudioService;->access$7100(Landroid/media/AudioService;ILandroid/media/IRemoteVolumeObserver;)V

    #@2e7
    goto/16 :goto_7

    #@2e9
    .line 4148
    :pswitch_2e9
    move-object/from16 v0, p0

    #@2eb
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@2ed
    move-object/from16 v0, p1

    #@2ef
    iget v2, v0, Landroid/os/Message;->arg1:I

    #@2f1
    move-object/from16 v0, p1

    #@2f3
    iget v3, v0, Landroid/os/Message;->arg2:I

    #@2f5
    invoke-static {v1, v2, v3}, Landroid/media/AudioService;->access$7200(Landroid/media/AudioService;II)V

    #@2f8
    goto/16 :goto_7

    #@2fa
    .line 4152
    :pswitch_2fa
    move-object/from16 v0, p0

    #@2fc
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@2fe
    invoke-static {v1}, Landroid/media/AudioService;->access$7300(Landroid/media/AudioService;)V

    #@301
    goto/16 :goto_7

    #@303
    .line 4156
    :pswitch_303
    move-object/from16 v0, p0

    #@305
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@307
    invoke-static {v1}, Landroid/media/AudioService;->access$7400(Landroid/media/AudioService;)V

    #@30a
    goto/16 :goto_7

    #@30c
    .line 4161
    :pswitch_30c
    move-object/from16 v0, p0

    #@30e
    iget-object v2, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@310
    move-object/from16 v0, p1

    #@312
    iget v1, v0, Landroid/os/Message;->what:I

    #@314
    const/16 v3, 0x1b

    #@316
    if-ne v1, v3, :cond_31e

    #@318
    const/4 v1, 0x1

    #@319
    :goto_319
    invoke-static {v2, v1}, Landroid/media/AudioService;->access$7500(Landroid/media/AudioService;Z)V

    #@31c
    goto/16 :goto_7

    #@31e
    :cond_31e
    const/4 v1, 0x0

    #@31f
    goto :goto_319

    #@320
    .line 4166
    :pswitch_320
    const-string v1, "AudioService"

    #@322
    const-string v2, "MSG_SHOW_VOLUME_INFO"

    #@324
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@327
    .line 4167
    move-object/from16 v0, p0

    #@329
    iget-object v1, v0, Landroid/media/AudioService$AudioHandler;->this$0:Landroid/media/AudioService;

    #@32b
    move-object/from16 v0, p1

    #@32d
    iget v2, v0, Landroid/os/Message;->arg1:I

    #@32f
    invoke-static {v1, v2}, Landroid/media/AudioService;->access$7600(Landroid/media/AudioService;I)V

    #@332
    goto/16 :goto_7

    #@334
    .line 4171
    :pswitch_334
    move-object/from16 v0, p1

    #@336
    iget v1, v0, Landroid/os/Message;->arg1:I

    #@338
    move-object/from16 v0, p0

    #@33a
    invoke-direct {v0, v1}, Landroid/media/AudioService$AudioHandler;->onPersistSafeVolumeState(I)V

    #@33d
    goto/16 :goto_7

    #@33f
    .line 3957
    nop

    #@340
    :pswitch_data_340
    .packed-switch 0x0
        :pswitch_8
        :pswitch_24
        :pswitch_38
        :pswitch_64
        :pswitch_72
        :pswitch_a2
        :pswitch_1c3
        :pswitch_1d2
        :pswitch_1ba
        :pswitch_1ee
        :pswitch_1fd
        :pswitch_226
        :pswitch_20a
        :pswitch_213
        :pswitch_18
        :pswitch_50
        :pswitch_26f
        :pswitch_2b2
        :pswitch_2bb
        :pswitch_2d6
        :pswitch_1ee
        :pswitch_22f
        :pswitch_251
        :pswitch_2e9
        :pswitch_2fa
        :pswitch_303
        :pswitch_30c
        :pswitch_30c
        :pswitch_334
        :pswitch_320
    .end packed-switch
.end method
