.class public Landroid/media/MediaRouter$UserRouteInfo;
.super Landroid/media/MediaRouter$RouteInfo;
.source "MediaRouter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/MediaRouter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UserRouteInfo"
.end annotation


# instance fields
.field mRcc:Landroid/media/RemoteControlClient;


# direct methods
.method constructor <init>(Landroid/media/MediaRouter$RouteCategory;)V
    .registers 3
    .parameter "category"

    #@0
    .prologue
    .line 1297
    invoke-direct {p0, p1}, Landroid/media/MediaRouter$RouteInfo;-><init>(Landroid/media/MediaRouter$RouteCategory;)V

    #@3
    .line 1298
    const/high16 v0, 0x80

    #@5
    iput v0, p0, Landroid/media/MediaRouter$RouteInfo;->mSupportedTypes:I

    #@7
    .line 1299
    const/4 v0, 0x1

    #@8
    iput v0, p0, Landroid/media/MediaRouter$RouteInfo;->mPlaybackType:I

    #@a
    .line 1300
    const/4 v0, 0x0

    #@b
    iput v0, p0, Landroid/media/MediaRouter$RouteInfo;->mVolumeHandling:I

    #@d
    .line 1301
    return-void
.end method

.method private setPlaybackInfoOnRcc(II)V
    .registers 4
    .parameter "what"
    .parameter "value"

    #@0
    .prologue
    .line 1500
    iget-object v0, p0, Landroid/media/MediaRouter$UserRouteInfo;->mRcc:Landroid/media/RemoteControlClient;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1501
    iget-object v0, p0, Landroid/media/MediaRouter$UserRouteInfo;->mRcc:Landroid/media/RemoteControlClient;

    #@6
    invoke-virtual {v0, p1, p2}, Landroid/media/RemoteControlClient;->setPlaybackInformation(II)V

    #@9
    .line 1503
    :cond_9
    return-void
.end method

.method private updatePlaybackInfoOnRcc()V
    .registers 5

    #@0
    .prologue
    .line 1478
    iget-object v1, p0, Landroid/media/MediaRouter$UserRouteInfo;->mRcc:Landroid/media/RemoteControlClient;

    #@2
    if-eqz v1, :cond_44

    #@4
    iget-object v1, p0, Landroid/media/MediaRouter$UserRouteInfo;->mRcc:Landroid/media/RemoteControlClient;

    #@6
    invoke-virtual {v1}, Landroid/media/RemoteControlClient;->getRcseId()I

    #@9
    move-result v1

    #@a
    const/4 v2, -0x1

    #@b
    if-eq v1, v2, :cond_44

    #@d
    .line 1479
    iget-object v1, p0, Landroid/media/MediaRouter$UserRouteInfo;->mRcc:Landroid/media/RemoteControlClient;

    #@f
    const/4 v2, 0x3

    #@10
    iget v3, p0, Landroid/media/MediaRouter$RouteInfo;->mVolumeMax:I

    #@12
    invoke-virtual {v1, v2, v3}, Landroid/media/RemoteControlClient;->setPlaybackInformation(II)V

    #@15
    .line 1481
    iget-object v1, p0, Landroid/media/MediaRouter$UserRouteInfo;->mRcc:Landroid/media/RemoteControlClient;

    #@17
    const/4 v2, 0x2

    #@18
    iget v3, p0, Landroid/media/MediaRouter$RouteInfo;->mVolume:I

    #@1a
    invoke-virtual {v1, v2, v3}, Landroid/media/RemoteControlClient;->setPlaybackInformation(II)V

    #@1d
    .line 1483
    iget-object v1, p0, Landroid/media/MediaRouter$UserRouteInfo;->mRcc:Landroid/media/RemoteControlClient;

    #@1f
    const/4 v2, 0x4

    #@20
    iget v3, p0, Landroid/media/MediaRouter$RouteInfo;->mVolumeHandling:I

    #@22
    invoke-virtual {v1, v2, v3}, Landroid/media/RemoteControlClient;->setPlaybackInformation(II)V

    #@25
    .line 1485
    iget-object v1, p0, Landroid/media/MediaRouter$UserRouteInfo;->mRcc:Landroid/media/RemoteControlClient;

    #@27
    const/4 v2, 0x5

    #@28
    iget v3, p0, Landroid/media/MediaRouter$RouteInfo;->mPlaybackStream:I

    #@2a
    invoke-virtual {v1, v2, v3}, Landroid/media/RemoteControlClient;->setPlaybackInformation(II)V

    #@2d
    .line 1487
    iget-object v1, p0, Landroid/media/MediaRouter$UserRouteInfo;->mRcc:Landroid/media/RemoteControlClient;

    #@2f
    const/4 v2, 0x1

    #@30
    iget v3, p0, Landroid/media/MediaRouter$RouteInfo;->mPlaybackType:I

    #@32
    invoke-virtual {v1, v2, v3}, Landroid/media/RemoteControlClient;->setPlaybackInformation(II)V

    #@35
    .line 1491
    :try_start_35
    sget-object v1, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@37
    iget-object v1, v1, Landroid/media/MediaRouter$Static;->mAudioService:Landroid/media/IAudioService;

    #@39
    iget-object v2, p0, Landroid/media/MediaRouter$UserRouteInfo;->mRcc:Landroid/media/RemoteControlClient;

    #@3b
    invoke-virtual {v2}, Landroid/media/RemoteControlClient;->getRcseId()I

    #@3e
    move-result v2

    #@3f
    iget-object v3, p0, Landroid/media/MediaRouter$RouteInfo;->mRemoteVolObserver:Landroid/media/IRemoteVolumeObserver$Stub;

    #@41
    invoke-interface {v1, v2, v3}, Landroid/media/IAudioService;->registerRemoteVolumeObserverForRcc(ILandroid/media/IRemoteVolumeObserver;)V
    :try_end_44
    .catch Landroid/os/RemoteException; {:try_start_35 .. :try_end_44} :catch_45

    #@44
    .line 1497
    :cond_44
    :goto_44
    return-void

    #@45
    .line 1493
    :catch_45
    move-exception v0

    #@46
    .line 1494
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "MediaRouter"

    #@48
    const-string v2, "Error registering remote volume observer"

    #@4a
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4d
    goto :goto_44
.end method


# virtual methods
.method public getRemoteControlClient()Landroid/media/RemoteControlClient;
    .registers 2

    #@0
    .prologue
    .line 1356
    iget-object v0, p0, Landroid/media/MediaRouter$UserRouteInfo;->mRcc:Landroid/media/RemoteControlClient;

    #@2
    return-object v0
.end method

.method public requestSetVolume(I)V
    .registers 4
    .parameter "volume"

    #@0
    .prologue
    .line 1433
    iget v0, p0, Landroid/media/MediaRouter$RouteInfo;->mVolumeHandling:I

    #@2
    const/4 v1, 0x1

    #@3
    if-ne v0, v1, :cond_10

    #@5
    .line 1434
    iget-object v0, p0, Landroid/media/MediaRouter$RouteInfo;->mVcb:Landroid/media/MediaRouter$VolumeCallbackInfo;

    #@7
    if-nez v0, :cond_11

    #@9
    .line 1435
    const-string v0, "MediaRouter"

    #@b
    const-string v1, "Cannot requestSetVolume on user route - no volume callback set"

    #@d
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 1440
    :cond_10
    :goto_10
    return-void

    #@11
    .line 1438
    :cond_11
    iget-object v0, p0, Landroid/media/MediaRouter$RouteInfo;->mVcb:Landroid/media/MediaRouter$VolumeCallbackInfo;

    #@13
    iget-object v0, v0, Landroid/media/MediaRouter$VolumeCallbackInfo;->vcb:Landroid/media/MediaRouter$VolumeCallback;

    #@15
    invoke-virtual {v0, p0, p1}, Landroid/media/MediaRouter$VolumeCallback;->onVolumeSetRequest(Landroid/media/MediaRouter$RouteInfo;I)V

    #@18
    goto :goto_10
.end method

.method public requestUpdateVolume(I)V
    .registers 4
    .parameter "direction"

    #@0
    .prologue
    .line 1444
    iget v0, p0, Landroid/media/MediaRouter$RouteInfo;->mVolumeHandling:I

    #@2
    const/4 v1, 0x1

    #@3
    if-ne v0, v1, :cond_10

    #@5
    .line 1445
    iget-object v0, p0, Landroid/media/MediaRouter$RouteInfo;->mVcb:Landroid/media/MediaRouter$VolumeCallbackInfo;

    #@7
    if-nez v0, :cond_11

    #@9
    .line 1446
    const-string v0, "MediaRouter"

    #@b
    const-string v1, "Cannot requestChangeVolume on user route - no volumec callback set"

    #@d
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 1451
    :cond_10
    :goto_10
    return-void

    #@11
    .line 1449
    :cond_11
    iget-object v0, p0, Landroid/media/MediaRouter$RouteInfo;->mVcb:Landroid/media/MediaRouter$VolumeCallbackInfo;

    #@13
    iget-object v0, v0, Landroid/media/MediaRouter$VolumeCallbackInfo;->vcb:Landroid/media/MediaRouter$VolumeCallback;

    #@15
    invoke-virtual {v0, p0, p1}, Landroid/media/MediaRouter$VolumeCallback;->onVolumeUpdateRequest(Landroid/media/MediaRouter$RouteInfo;I)V

    #@18
    goto :goto_10
.end method

.method public setIconDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 2
    .parameter "icon"

    #@0
    .prologue
    .line 1366
    iput-object p1, p0, Landroid/media/MediaRouter$RouteInfo;->mIcon:Landroid/graphics/drawable/Drawable;

    #@2
    .line 1367
    return-void
.end method

.method public setIconResource(I)V
    .registers 3
    .parameter "resId"

    #@0
    .prologue
    .line 1376
    sget-object v0, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2
    iget-object v0, v0, Landroid/media/MediaRouter$Static;->mResources:Landroid/content/res/Resources;

    #@4
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {p0, v0}, Landroid/media/MediaRouter$UserRouteInfo;->setIconDrawable(Landroid/graphics/drawable/Drawable;)V

    #@b
    .line 1377
    return-void
.end method

.method public setName(I)V
    .registers 3
    .parameter "resId"

    #@0
    .prologue
    .line 1317
    iput p1, p0, Landroid/media/MediaRouter$RouteInfo;->mNameResId:I

    #@2
    .line 1318
    const/4 v0, 0x0

    #@3
    iput-object v0, p0, Landroid/media/MediaRouter$RouteInfo;->mName:Ljava/lang/CharSequence;

    #@5
    .line 1319
    invoke-virtual {p0}, Landroid/media/MediaRouter$UserRouteInfo;->routeUpdated()V

    #@8
    .line 1320
    return-void
.end method

.method public setName(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 1308
    iput-object p1, p0, Landroid/media/MediaRouter$RouteInfo;->mName:Ljava/lang/CharSequence;

    #@2
    .line 1309
    invoke-virtual {p0}, Landroid/media/MediaRouter$UserRouteInfo;->routeUpdated()V

    #@5
    .line 1310
    return-void
.end method

.method public setPlaybackStream(I)V
    .registers 3
    .parameter "stream"

    #@0
    .prologue
    .line 1471
    iget v0, p0, Landroid/media/MediaRouter$RouteInfo;->mPlaybackStream:I

    #@2
    if-eq v0, p1, :cond_a

    #@4
    .line 1472
    iput p1, p0, Landroid/media/MediaRouter$RouteInfo;->mPlaybackStream:I

    #@6
    .line 1473
    const/4 v0, 0x5

    #@7
    invoke-direct {p0, v0, p1}, Landroid/media/MediaRouter$UserRouteInfo;->setPlaybackInfoOnRcc(II)V

    #@a
    .line 1475
    :cond_a
    return-void
.end method

.method public setPlaybackType(I)V
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 1394
    iget v0, p0, Landroid/media/MediaRouter$RouteInfo;->mPlaybackType:I

    #@2
    if-eq v0, p1, :cond_a

    #@4
    .line 1395
    iput p1, p0, Landroid/media/MediaRouter$RouteInfo;->mPlaybackType:I

    #@6
    .line 1396
    const/4 v0, 0x1

    #@7
    invoke-direct {p0, v0, p1}, Landroid/media/MediaRouter$UserRouteInfo;->setPlaybackInfoOnRcc(II)V

    #@a
    .line 1398
    :cond_a
    return-void
.end method

.method public setRemoteControlClient(Landroid/media/RemoteControlClient;)V
    .registers 2
    .parameter "rcc"

    #@0
    .prologue
    .line 1345
    iput-object p1, p0, Landroid/media/MediaRouter$UserRouteInfo;->mRcc:Landroid/media/RemoteControlClient;

    #@2
    .line 1346
    invoke-direct {p0}, Landroid/media/MediaRouter$UserRouteInfo;->updatePlaybackInfoOnRcc()V

    #@5
    .line 1347
    return-void
.end method

.method public setStatus(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "status"

    #@0
    .prologue
    .line 1328
    invoke-virtual {p0, p1}, Landroid/media/MediaRouter$UserRouteInfo;->setStatusInt(Ljava/lang/CharSequence;)V

    #@3
    .line 1329
    return-void
.end method

.method public setVolume(I)V
    .registers 4
    .parameter "volume"

    #@0
    .prologue
    .line 1420
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0}, Landroid/media/MediaRouter$UserRouteInfo;->getVolumeMax()I

    #@4
    move-result v1

    #@5
    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    #@8
    move-result v1

    #@9
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@c
    move-result p1

    #@d
    .line 1421
    iget v0, p0, Landroid/media/MediaRouter$RouteInfo;->mVolume:I

    #@f
    if-eq v0, p1, :cond_23

    #@11
    .line 1422
    iput p1, p0, Landroid/media/MediaRouter$RouteInfo;->mVolume:I

    #@13
    .line 1423
    const/4 v0, 0x2

    #@14
    invoke-direct {p0, v0, p1}, Landroid/media/MediaRouter$UserRouteInfo;->setPlaybackInfoOnRcc(II)V

    #@17
    .line 1424
    invoke-static {p0}, Landroid/media/MediaRouter;->dispatchRouteVolumeChanged(Landroid/media/MediaRouter$RouteInfo;)V

    #@1a
    .line 1425
    iget-object v0, p0, Landroid/media/MediaRouter$RouteInfo;->mGroup:Landroid/media/MediaRouter$RouteGroup;

    #@1c
    if-eqz v0, :cond_23

    #@1e
    .line 1426
    iget-object v0, p0, Landroid/media/MediaRouter$RouteInfo;->mGroup:Landroid/media/MediaRouter$RouteGroup;

    #@20
    invoke-virtual {v0, p0}, Landroid/media/MediaRouter$RouteGroup;->memberVolumeChanged(Landroid/media/MediaRouter$RouteInfo;)V

    #@23
    .line 1429
    :cond_23
    return-void
.end method

.method public setVolumeCallback(Landroid/media/MediaRouter$VolumeCallback;)V
    .registers 3
    .parameter "vcb"

    #@0
    .prologue
    .line 1384
    new-instance v0, Landroid/media/MediaRouter$VolumeCallbackInfo;

    #@2
    invoke-direct {v0, p1, p0}, Landroid/media/MediaRouter$VolumeCallbackInfo;-><init>(Landroid/media/MediaRouter$VolumeCallback;Landroid/media/MediaRouter$RouteInfo;)V

    #@5
    iput-object v0, p0, Landroid/media/MediaRouter$RouteInfo;->mVcb:Landroid/media/MediaRouter$VolumeCallbackInfo;

    #@7
    .line 1385
    return-void
.end method

.method public setVolumeHandling(I)V
    .registers 3
    .parameter "volumeHandling"

    #@0
    .prologue
    .line 1407
    iget v0, p0, Landroid/media/MediaRouter$RouteInfo;->mVolumeHandling:I

    #@2
    if-eq v0, p1, :cond_a

    #@4
    .line 1408
    iput p1, p0, Landroid/media/MediaRouter$RouteInfo;->mVolumeHandling:I

    #@6
    .line 1409
    const/4 v0, 0x4

    #@7
    invoke-direct {p0, v0, p1}, Landroid/media/MediaRouter$UserRouteInfo;->setPlaybackInfoOnRcc(II)V

    #@a
    .line 1412
    :cond_a
    return-void
.end method

.method public setVolumeMax(I)V
    .registers 3
    .parameter "volumeMax"

    #@0
    .prologue
    .line 1460
    iget v0, p0, Landroid/media/MediaRouter$RouteInfo;->mVolumeMax:I

    #@2
    if-eq v0, p1, :cond_a

    #@4
    .line 1461
    iput p1, p0, Landroid/media/MediaRouter$RouteInfo;->mVolumeMax:I

    #@6
    .line 1462
    const/4 v0, 0x3

    #@7
    invoke-direct {p0, v0, p1}, Landroid/media/MediaRouter$UserRouteInfo;->setPlaybackInfoOnRcc(II)V

    #@a
    .line 1464
    :cond_a
    return-void
.end method
