.class public Landroid/media/FaceDetector$Face;
.super Ljava/lang/Object;
.source "FaceDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/FaceDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Face"
.end annotation


# static fields
.field public static final CONFIDENCE_THRESHOLD:F = 0.4f

.field public static final EULER_X:I = 0x0

.field public static final EULER_Y:I = 0x1

.field public static final EULER_Z:I = 0x2


# instance fields
.field private mConfidence:F

.field private mEyesDist:F

.field private mMidPointX:F

.field private mMidPointY:F

.field private mPoseEulerX:F

.field private mPoseEulerY:F

.field private mPoseEulerZ:F

.field final synthetic this$0:Landroid/media/FaceDetector;


# direct methods
.method private constructor <init>(Landroid/media/FaceDetector;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 89
    iput-object p1, p0, Landroid/media/FaceDetector$Face;->this$0:Landroid/media/FaceDetector;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 90
    return-void
.end method

.method synthetic constructor <init>(Landroid/media/FaceDetector;Landroid/media/FaceDetector$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/media/FaceDetector$Face;-><init>(Landroid/media/FaceDetector;)V

    #@3
    return-void
.end method


# virtual methods
.method public confidence()F
    .registers 2

    #@0
    .prologue
    .line 51
    iget v0, p0, Landroid/media/FaceDetector$Face;->mConfidence:F

    #@2
    return v0
.end method

.method public eyesDistance()F
    .registers 2

    #@0
    .prologue
    .line 66
    iget v0, p0, Landroid/media/FaceDetector$Face;->mEyesDist:F

    #@2
    return v0
.end method

.method public getMidPoint(Landroid/graphics/PointF;)V
    .registers 4
    .parameter "point"

    #@0
    .prologue
    .line 60
    iget v0, p0, Landroid/media/FaceDetector$Face;->mMidPointX:F

    #@2
    iget v1, p0, Landroid/media/FaceDetector$Face;->mMidPointY:F

    #@4
    invoke-virtual {p1, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    #@7
    .line 61
    return-void
.end method

.method public pose(I)F
    .registers 3
    .parameter "euler"

    #@0
    .prologue
    .line 79
    if-nez p1, :cond_5

    #@2
    .line 80
    iget v0, p0, Landroid/media/FaceDetector$Face;->mPoseEulerX:F

    #@4
    .line 84
    :goto_4
    return v0

    #@5
    .line 81
    :cond_5
    const/4 v0, 0x1

    #@6
    if-ne p1, v0, :cond_b

    #@8
    .line 82
    iget v0, p0, Landroid/media/FaceDetector$Face;->mPoseEulerY:F

    #@a
    goto :goto_4

    #@b
    .line 83
    :cond_b
    const/4 v0, 0x2

    #@c
    if-ne p1, v0, :cond_11

    #@e
    .line 84
    iget v0, p0, Landroid/media/FaceDetector$Face;->mPoseEulerZ:F

    #@10
    goto :goto_4

    #@11
    .line 85
    :cond_11
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@13
    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@16
    throw v0
.end method
