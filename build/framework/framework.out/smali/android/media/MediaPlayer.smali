.class public Landroid/media/MediaPlayer;
.super Ljava/lang/Object;
.source "MediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/MediaPlayer$OnInfoListener;,
        Landroid/media/MediaPlayer$OnErrorListener;,
        Landroid/media/MediaPlayer$OnTimedTextListener;,
        Landroid/media/MediaPlayer$OnVideoSizeChangedListener;,
        Landroid/media/MediaPlayer$OnSeekCompleteListener;,
        Landroid/media/MediaPlayer$OnBufferingUpdateListener;,
        Landroid/media/MediaPlayer$OnCompletionListener;,
        Landroid/media/MediaPlayer$OnPreparedListener;,
        Landroid/media/MediaPlayer$EventHandler;,
        Landroid/media/MediaPlayer$TrackInfo;
    }
.end annotation


# static fields
.field public static final APPLY_METADATA_FILTER:Z = true

.field public static final BYPASS_METADATA_FILTER:Z = false

.field private static final IMEDIA_PLAYER:Ljava/lang/String; = "android.media.IMediaPlayer"

.field private static final INVOKE_ID_ADD_EXTERNAL_SOURCE:I = 0x2

.field private static final INVOKE_ID_ADD_EXTERNAL_SOURCE_FD:I = 0x3

.field private static final INVOKE_ID_DESELECT_TRACK:I = 0x5

.field private static final INVOKE_ID_GET_TRACK_INFO:I = 0x1

.field private static final INVOKE_ID_SELECT_TRACK:I = 0x4

.field private static final INVOKE_ID_SET_VIDEO_SCALE_MODE:I = 0x6

.field private static final KEY_PARAMETER_LGEPLAYER_GET_STATE:I = 0xfa0

.field public static final KEY_PARAMETER_LGE_HIFI_ENABLED:I = 0x1770

.field private static final MASK_VALID_GET_CURRENTPOSITON:I = 0xfe

.field private static final MASK_VALID_GET_DURATION:I = 0xb8

.field private static final MEDIA_BUFFERING_UPDATE:I = 0x3

.field private static final MEDIA_CHANGE_PLAYER_STATE:I = 0x1f4

.field private static final MEDIA_ERROR:I = 0x64

.field public static final MEDIA_ERROR_IO:I = -0x3ec

.field public static final MEDIA_ERROR_MALFORMED:I = -0x3ef

.field public static final MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:I = 0xc8

.field public static final MEDIA_ERROR_SERVER_DIED:I = 0x64

.field public static final MEDIA_ERROR_TIMED_OUT:I = -0x6e

.field public static final MEDIA_ERROR_UNKNOWN:I = 0x1

.field public static final MEDIA_ERROR_UNSUPPORTED:I = -0x3f2

.field public static final MEDIA_IMPLEMENT_ERROR_DRM_NOT_AUTHORIZED:I = 0x24b8

.field public static final MEDIA_IMPLEMENT_ERROR_NOT_AVAILABLE_NETWORK:I = 0x2454

.field public static final MEDIA_IMPLEMENT_ERROR_NOT_EXIST_AUDIO:I = 0x2396

.field public static final MEDIA_IMPLEMENT_ERROR_NOT_EXIST_VIDEO:I = 0x23a0

.field public static final MEDIA_IMPLEMENT_ERROR_NOT_SUPPORT_AUDIO:I = 0x23f0

.field public static final MEDIA_IMPLEMENT_ERROR_NOT_SUPPORT_BITRATE:I = 0x23aa

.field public static final MEDIA_IMPLEMENT_ERROR_NOT_SUPPORT_MEDIA:I = 0x2404

.field public static final MEDIA_IMPLEMENT_ERROR_NOT_SUPPORT_RESOLUTIONS:I = 0x238c

.field public static final MEDIA_IMPLEMENT_ERROR_NOT_SUPPORT_VIDEO:I = 0x23fa

.field private static final MEDIA_INFO:I = 0xc8

.field public static final MEDIA_INFO_BAD_INTERLEAVING:I = 0x320

.field public static final MEDIA_INFO_BUFFERING_END:I = 0x2be

.field public static final MEDIA_INFO_BUFFERING_START:I = 0x2bd

.field public static final MEDIA_INFO_HIFI_AUDIO_DISABLED:I = 0x3e9

.field public static final MEDIA_INFO_HIFI_AUDIO_ENABLED:I = 0x3e8

.field public static final MEDIA_INFO_METADATA_UPDATE:I = 0x322

.field public static final MEDIA_INFO_NOT_SEEKABLE:I = 0x321

.field public static final MEDIA_INFO_STARTED_AS_NEXT:I = 0x2

.field public static final MEDIA_INFO_TIMED_TEXT_ERROR:I = 0x384

.field public static final MEDIA_INFO_UNKNOWN:I = 0x1

.field public static final MEDIA_INFO_VIDEO_RENDERING_START:I = 0x3

.field public static final MEDIA_INFO_VIDEO_TRACK_LAGGING:I = 0x2bc

.field public static final MEDIA_KEY_AUDIO_ZOOM_INFO:I = 0x23f0

.field public static final MEDIA_KEY_AUDIO_ZOOM_INIT:I = 0x23f1

.field public static final MEDIA_KEY_AUDIO_ZOOM_START:I = 0x23f2

.field public static final MEDIA_KEY_HTTP_ADD_HEADER:I = 0x2329

.field public static final MEDIA_KEY_HTTP_GET_RESPONSE:I = 0x232b

.field public static final MEDIA_KEY_HTTP_REMOVE_HEADER:I = 0x232a

.field public static final MEDIA_KEY_HTTP_REQUEST_OPTION_CONNECTION_TIMEOUT:I = 0x238c

.field public static final MEDIA_KEY_HTTP_REQUEST_OPTION_ENABLE_HTTPRANGE:I = 0x2390

.field public static final MEDIA_KEY_HTTP_REQUEST_OPTION_KEEPCONNECTION_ON_PAUSE:I = 0x238f

.field public static final MEDIA_KEY_HTTP_REQUEST_OPTION_KEEPCONNECTION_ON_PLAY:I = 0x238e

.field public static final MEDIA_KEY_HTTP_REQUEST_OPTION_READ_TIMEOUT:I = 0x238d

.field public static final MEDIA_MIMETYPE_TEXT_SUBRIP:Ljava/lang/String; = "application/x-subrip"

.field private static final MEDIA_NOP:I = 0x0

.field private static final MEDIA_PLAYBACK_COMPLETE:I = 0x2

.field private static final MEDIA_PLAYER_END:I = 0x100

.field private static final MEDIA_PLAYER_IDLE:I = 0x1

.field private static final MEDIA_PLAYER_INITIALIZED:I = 0x2

.field private static final MEDIA_PLAYER_PAUSED:I = 0x20

.field private static final MEDIA_PLAYER_PLAYBACK_COMPLETE:I = 0x80

.field private static final MEDIA_PLAYER_PREPARED:I = 0x8

.field private static final MEDIA_PLAYER_PREPARING:I = 0x4

.field private static final MEDIA_PLAYER_SEEK_COMPLETE:I = 0x200

.field private static final MEDIA_PLAYER_STARTED:I = 0x10

.field private static final MEDIA_PLAYER_STATE_ERROR:I = 0x0

.field private static final MEDIA_PLAYER_STOPPED:I = 0x40

.field private static final MEDIA_PREPARED:I = 0x1

.field private static final MEDIA_SEEK_COMPLETE:I = 0x4

.field private static final MEDIA_SET_VIDEO_SIZE:I = 0x5

.field private static final MEDIA_TIMED_TEXT:I = 0x63

.field public static final METADATA_ALL:Z = false

.field public static final METADATA_UPDATE_ONLY:Z = true

.field private static final TAG:Ljava/lang/String; = "MediaPlayer[JAVA]"

.field public static final VIDEO_SCALING_MODE_SCALE_TO_FIT:I = 0x1

.field public static final VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING:I = 0x2


# instance fields
.field private mContext:Landroid/content/Context;

.field private mEventHandler:Landroid/media/MediaPlayer$EventHandler;

.field private mListenerContext:I

.field private mNativeContext:I

.field private mNativeSurfaceTexture:I

.field private mOnBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

.field private mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mOnInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

.field private mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field private mOnSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

.field private mOnTimedTextListener:Landroid/media/MediaPlayer$OnTimedTextListener;

.field private mOnVideoSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

.field private mScreenOnWhilePlaying:Z

.field private mStayAwake:Z

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mUriPath:Ljava/lang/String;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 560
    const-string/jumbo v0, "media_jni"

    #@3
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@6
    .line 561
    invoke-static {}, Landroid/media/MediaPlayer;->native_init()V

    #@9
    .line 562
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 596
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 575
    iput-object v1, p0, Landroid/media/MediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@6
    .line 582
    iput-object v1, p0, Landroid/media/MediaPlayer;->mContext:Landroid/content/Context;

    #@8
    .line 586
    iput-object v1, p0, Landroid/media/MediaPlayer;->mUriPath:Ljava/lang/String;

    #@a
    .line 599
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@d
    move-result-object v0

    #@e
    .local v0, looper:Landroid/os/Looper;
    if-eqz v0, :cond_20

    #@10
    .line 600
    new-instance v1, Landroid/media/MediaPlayer$EventHandler;

    #@12
    invoke-direct {v1, p0, p0, v0}, Landroid/media/MediaPlayer$EventHandler;-><init>(Landroid/media/MediaPlayer;Landroid/media/MediaPlayer;Landroid/os/Looper;)V

    #@15
    iput-object v1, p0, Landroid/media/MediaPlayer;->mEventHandler:Landroid/media/MediaPlayer$EventHandler;

    #@17
    .line 610
    :goto_17
    new-instance v1, Ljava/lang/ref/WeakReference;

    #@19
    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@1c
    invoke-direct {p0, v1}, Landroid/media/MediaPlayer;->native_setup(Ljava/lang/Object;)V

    #@1f
    .line 611
    return-void

    #@20
    .line 601
    :cond_20
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    #@23
    move-result-object v0

    #@24
    if-eqz v0, :cond_2e

    #@26
    .line 602
    new-instance v1, Landroid/media/MediaPlayer$EventHandler;

    #@28
    invoke-direct {v1, p0, p0, v0}, Landroid/media/MediaPlayer$EventHandler;-><init>(Landroid/media/MediaPlayer;Landroid/media/MediaPlayer;Landroid/os/Looper;)V

    #@2b
    iput-object v1, p0, Landroid/media/MediaPlayer;->mEventHandler:Landroid/media/MediaPlayer$EventHandler;

    #@2d
    goto :goto_17

    #@2e
    .line 604
    :cond_2e
    iput-object v1, p0, Landroid/media/MediaPlayer;->mEventHandler:Landroid/media/MediaPlayer$EventHandler;

    #@30
    goto :goto_17
.end method

.method private native _pause()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private synchronized native declared-synchronized _release()V
.end method

.method private synchronized native declared-synchronized _reset()V
.end method

.method private native _setDataSource(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native _setVideoSurface(Landroid/view/Surface;)V
.end method

.method private native _start()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native _stop()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method static synthetic access$000(Landroid/media/MediaPlayer;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 524
    iget v0, p0, Landroid/media/MediaPlayer;->mNativeContext:I

    #@2
    return v0
.end method

.method static synthetic access$100(Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnPreparedListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 524
    iget-object v0, p0, Landroid/media/MediaPlayer;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnErrorListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 524
    iget-object v0, p0, Landroid/media/MediaPlayer;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnInfoListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 524
    iget-object v0, p0, Landroid/media/MediaPlayer;->mOnInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnTimedTextListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 524
    iget-object v0, p0, Landroid/media/MediaPlayer;->mOnTimedTextListener:Landroid/media/MediaPlayer$OnTimedTextListener;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnCompletionListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 524
    iget-object v0, p0, Landroid/media/MediaPlayer;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/media/MediaPlayer;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 524
    invoke-direct {p0, p1}, Landroid/media/MediaPlayer;->stayAwake(Z)V

    #@3
    return-void
.end method

.method static synthetic access$400(Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnBufferingUpdateListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 524
    iget-object v0, p0, Landroid/media/MediaPlayer;->mOnBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Landroid/media/MediaPlayer;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 524
    iget-object v0, p0, Landroid/media/MediaPlayer;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Landroid/media/MediaPlayer;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 524
    invoke-direct {p0}, Landroid/media/MediaPlayer;->getPlayerState()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$700(Landroid/media/MediaPlayer;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 524
    iget-object v0, p0, Landroid/media/MediaPlayer;->mUriPath:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnSeekCompleteListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 524
    iget-object v0, p0, Landroid/media/MediaPlayer;->mOnSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer$OnVideoSizeChangedListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 524
    iget-object v0, p0, Landroid/media/MediaPlayer;->mOnVideoSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    #@2
    return-object v0
.end method

.method private static availableMimeTypeForExternalSource(Ljava/lang/String;)Z
    .registers 2
    .parameter "mimeType"

    #@0
    .prologue
    .line 1839
    const-string v0, "application/x-subrip"

    #@2
    if-ne p0, v0, :cond_6

    #@4
    .line 1840
    const/4 v0, 0x1

    #@5
    .line 1842
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public static create(Landroid/content/Context;I)Landroid/media/MediaPlayer;
    .registers 11
    .parameter "context"
    .parameter "resid"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 879
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@4
    move-result-object v1

    #@5
    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;

    #@8
    move-result-object v6

    #@9
    .line 880
    .local v6, afd:Landroid/content/res/AssetFileDescriptor;
    if-nez v6, :cond_d

    #@b
    move-object v0, v8

    #@c
    .line 900
    .end local v6           #afd:Landroid/content/res/AssetFileDescriptor;
    :goto_c
    return-object v0

    #@d
    .line 882
    .restart local v6       #afd:Landroid/content/res/AssetFileDescriptor;
    :cond_d
    new-instance v0, Landroid/media/MediaPlayer;

    #@f
    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    #@12
    .line 884
    .local v0, mp:Landroid/media/MediaPlayer;
    invoke-direct {v0, p0}, Landroid/media/MediaPlayer;->setContext(Landroid/content/Context;)V

    #@15
    .line 886
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    #@1c
    move-result-wide v2

    #@1d
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    #@20
    move-result-wide v4

    #@21
    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V

    #@24
    .line 887
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V

    #@27
    .line 888
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_2a
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_2a} :catch_2b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_2a} :catch_35
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_2a} :catch_3e

    #@2a
    goto :goto_c

    #@2b
    .line 890
    .end local v0           #mp:Landroid/media/MediaPlayer;
    .end local v6           #afd:Landroid/content/res/AssetFileDescriptor;
    :catch_2b
    move-exception v7

    #@2c
    .line 891
    .local v7, ex:Ljava/io/IOException;
    const-string v1, "MediaPlayer[JAVA]"

    #@2e
    const-string v2, "create failed:"

    #@30
    invoke-static {v1, v2, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@33
    .end local v7           #ex:Ljava/io/IOException;
    :goto_33
    move-object v0, v8

    #@34
    .line 900
    goto :goto_c

    #@35
    .line 893
    :catch_35
    move-exception v7

    #@36
    .line 894
    .local v7, ex:Ljava/lang/IllegalArgumentException;
    const-string v1, "MediaPlayer[JAVA]"

    #@38
    const-string v2, "create failed:"

    #@3a
    invoke-static {v1, v2, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3d
    goto :goto_33

    #@3e
    .line 896
    .end local v7           #ex:Ljava/lang/IllegalArgumentException;
    :catch_3e
    move-exception v7

    #@3f
    .line 897
    .local v7, ex:Ljava/lang/SecurityException;
    const-string v1, "MediaPlayer[JAVA]"

    #@41
    const-string v2, "create failed:"

    #@43
    invoke-static {v1, v2, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@46
    goto :goto_33
.end method

.method public static create(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/MediaPlayer;
    .registers 3
    .parameter "context"
    .parameter "uri"

    #@0
    .prologue
    .line 821
    const/4 v0, 0x0

    #@1
    invoke-static {p0, p1, v0}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;Landroid/net/Uri;Landroid/view/SurfaceHolder;)Landroid/media/MediaPlayer;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static create(Landroid/content/Context;Landroid/net/Uri;Landroid/view/SurfaceHolder;)Landroid/media/MediaPlayer;
    .registers 7
    .parameter "context"
    .parameter "uri"
    .parameter "holder"

    #@0
    .prologue
    .line 839
    :try_start_0
    new-instance v1, Landroid/media/MediaPlayer;

    #@2
    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    #@5
    .line 841
    .local v1, mp:Landroid/media/MediaPlayer;
    invoke-direct {v1, p0}, Landroid/media/MediaPlayer;->setContext(Landroid/content/Context;)V

    #@8
    .line 843
    invoke-virtual {v1, p0, p1}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    #@b
    .line 844
    if-eqz p2, :cond_10

    #@d
    .line 845
    invoke-virtual {v1, p2}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    #@10
    .line 847
    :cond_10
    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_13} :catch_14
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_13} :catch_1e
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_13} :catch_27

    #@13
    .line 860
    .end local v1           #mp:Landroid/media/MediaPlayer;
    :goto_13
    return-object v1

    #@14
    .line 849
    :catch_14
    move-exception v0

    #@15
    .line 850
    .local v0, ex:Ljava/io/IOException;
    const-string v2, "MediaPlayer[JAVA]"

    #@17
    const-string v3, "create failed:"

    #@19
    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1c
    .line 860
    .end local v0           #ex:Ljava/io/IOException;
    :goto_1c
    const/4 v1, 0x0

    #@1d
    goto :goto_13

    #@1e
    .line 852
    :catch_1e
    move-exception v0

    #@1f
    .line 853
    .local v0, ex:Ljava/lang/IllegalArgumentException;
    const-string v2, "MediaPlayer[JAVA]"

    #@21
    const-string v3, "create failed:"

    #@23
    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@26
    goto :goto_1c

    #@27
    .line 855
    .end local v0           #ex:Ljava/lang/IllegalArgumentException;
    :catch_27
    move-exception v0

    #@28
    .line 856
    .local v0, ex:Ljava/lang/SecurityException;
    const-string v2, "MediaPlayer[JAVA]"

    #@2a
    const-string v3, "create failed:"

    #@2c
    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2f
    goto :goto_1c
.end method

.method private native getParameter(ILandroid/os/Parcel;)V
.end method

.method private getPlayerState()I
    .registers 4

    #@0
    .prologue
    .line 2864
    const/4 v1, 0x0

    #@1
    .line 2865
    .local v1, state:I
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 2866
    .local v0, reply:Landroid/os/Parcel;
    const/16 v2, 0xfa0

    #@7
    invoke-direct {p0, v2, v0}, Landroid/media/MediaPlayer;->getParameter(ILandroid/os/Parcel;)V

    #@a
    .line 2867
    const/4 v2, 0x0

    #@b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->setDataPosition(I)V

    #@e
    .line 2868
    invoke-virtual {v0}, Landroid/os/Parcel;->readInt()I

    #@11
    move-result v1

    #@12
    .line 2869
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@15
    .line 2871
    return v1
.end method

.method private isVideoScalingModeSupported(I)Z
    .registers 4
    .parameter "mode"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 2810
    if-eq p1, v0, :cond_6

    #@3
    const/4 v1, 0x2

    #@4
    if-ne p1, v1, :cond_7

    #@6
    :cond_6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method private final native native_finalize()V
.end method

.method private final native native_getMetadata(ZZLandroid/os/Parcel;)Z
.end method

.method private static final native native_init()V
.end method

.method private final native native_invoke(Landroid/os/Parcel;Landroid/os/Parcel;)I
.end method

.method public static native native_pullBatteryData(Landroid/os/Parcel;)I
.end method

.method private final native native_setMetadataFilter(Landroid/os/Parcel;)I
.end method

.method private final native native_setRetransmitEndpoint(Ljava/lang/String;I)I
.end method

.method private final native native_setup(Ljava/lang/Object;)V
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .registers 8
    .parameter "mediaplayer_ref"
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"
    .parameter "obj"

    #@0
    .prologue
    .line 2420
    check-cast p0, Ljava/lang/ref/WeakReference;

    #@2
    .end local p0
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Landroid/media/MediaPlayer;

    #@8
    .line 2421
    .local v1, mp:Landroid/media/MediaPlayer;
    if-nez v1, :cond_b

    #@a
    .line 2433
    :cond_a
    :goto_a
    return-void

    #@b
    .line 2425
    :cond_b
    const/16 v2, 0xc8

    #@d
    if-ne p1, v2, :cond_15

    #@f
    const/4 v2, 0x2

    #@10
    if-ne p2, v2, :cond_15

    #@12
    .line 2427
    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    #@15
    .line 2429
    :cond_15
    iget-object v2, v1, Landroid/media/MediaPlayer;->mEventHandler:Landroid/media/MediaPlayer$EventHandler;

    #@17
    if-eqz v2, :cond_a

    #@19
    .line 2430
    iget-object v2, v1, Landroid/media/MediaPlayer;->mEventHandler:Landroid/media/MediaPlayer$EventHandler;

    #@1b
    invoke-virtual {v2, p1, p2, p3, p4}, Landroid/media/MediaPlayer$EventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@1e
    move-result-object v0

    #@1f
    .line 2431
    .local v0, m:Landroid/os/Message;
    iget-object v2, v1, Landroid/media/MediaPlayer;->mEventHandler:Landroid/media/MediaPlayer$EventHandler;

    #@21
    invoke-virtual {v2, v0}, Landroid/media/MediaPlayer$EventHandler;->sendMessage(Landroid/os/Message;)Z

    #@24
    goto :goto_a
.end method

.method private selectOrDeselectTrack(IZ)V
    .registers 6
    .parameter "index"
    .parameter "select"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 2035
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v1

    #@4
    .line 2036
    .local v1, request:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v0

    #@8
    .line 2038
    .local v0, reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.media.IMediaPlayer"

    #@a
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2039
    if-eqz p2, :cond_20

    #@f
    const/4 v2, 0x4

    #@10
    :goto_10
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 2040
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 2041
    invoke-virtual {p0, v1, v0}, Landroid/media/MediaPlayer;->invoke(Landroid/os/Parcel;Landroid/os/Parcel;)V
    :try_end_19
    .catchall {:try_start_8 .. :try_end_19} :catchall_22

    #@19
    .line 2043
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1c
    .line 2044
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 2046
    return-void

    #@20
    .line 2039
    :cond_20
    const/4 v2, 0x5

    #@21
    goto :goto_10

    #@22
    .line 2043
    :catchall_22
    move-exception v2

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 2044
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 2043
    throw v2
.end method

.method private setContext(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 616
    iput-object p1, p0, Landroid/media/MediaPlayer;->mContext:Landroid/content/Context;

    #@2
    .line 617
    return-void
.end method

.method private setDataSource(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V
    .registers 8
    .parameter "path"
    .parameter "keys"
    .parameter "values"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1014
    new-instance v1, Ljava/io/File;

    #@2
    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@5
    .line 1016
    .local v1, file:Ljava/io/File;
    invoke-direct {p0, p1}, Landroid/media/MediaPlayer;->setUri(Ljava/lang/String;)V

    #@8
    .line 1019
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@b
    move-result v3

    #@c
    if-eqz v3, :cond_1e

    #@e
    .line 1020
    new-instance v2, Ljava/io/FileInputStream;

    #@10
    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    #@13
    .line 1021
    .local v2, is:Ljava/io/FileInputStream;
    invoke-virtual {v2}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    #@16
    move-result-object v0

    #@17
    .line 1022
    .local v0, fd:Ljava/io/FileDescriptor;
    invoke-virtual {p0, v0}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;)V

    #@1a
    .line 1023
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    #@1d
    .line 1027
    .end local v0           #fd:Ljava/io/FileDescriptor;
    .end local v2           #is:Ljava/io/FileInputStream;
    :goto_1d
    return-void

    #@1e
    .line 1025
    :cond_1e
    invoke-direct {p0, p1, p2, p3}, Landroid/media/MediaPlayer;->_setDataSource(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    #@21
    goto :goto_1d
.end method

.method private setUri(Ljava/lang/String;)V
    .registers 2
    .parameter "uri"

    #@0
    .prologue
    .line 621
    iput-object p1, p0, Landroid/media/MediaPlayer;->mUriPath:Ljava/lang/String;

    #@2
    .line 622
    return-void
.end method

.method private stayAwake(Z)V
    .registers 3
    .parameter "awake"

    #@0
    .prologue
    .line 1192
    iget-object v0, p0, Landroid/media/MediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    if-eqz v0, :cond_13

    #@4
    .line 1193
    if-eqz p1, :cond_19

    #@6
    iget-object v0, p0, Landroid/media/MediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@8
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_19

    #@e
    .line 1194
    iget-object v0, p0, Landroid/media/MediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@10
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@13
    .line 1199
    :cond_13
    :goto_13
    iput-boolean p1, p0, Landroid/media/MediaPlayer;->mStayAwake:Z

    #@15
    .line 1200
    invoke-direct {p0}, Landroid/media/MediaPlayer;->updateSurfaceScreenOn()V

    #@18
    .line 1201
    return-void

    #@19
    .line 1195
    :cond_19
    if-nez p1, :cond_13

    #@1b
    iget-object v0, p0, Landroid/media/MediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@1d
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@20
    move-result v0

    #@21
    if-eqz v0, :cond_13

    #@23
    .line 1196
    iget-object v0, p0, Landroid/media/MediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@25
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@28
    goto :goto_13
.end method

.method private updateSurfaceScreenOn()V
    .registers 3

    #@0
    .prologue
    .line 1204
    iget-object v0, p0, Landroid/media/MediaPlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    #@2
    if-eqz v0, :cond_12

    #@4
    .line 1205
    iget-object v1, p0, Landroid/media/MediaPlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    #@6
    iget-boolean v0, p0, Landroid/media/MediaPlayer;->mScreenOnWhilePlaying:Z

    #@8
    if-eqz v0, :cond_13

    #@a
    iget-boolean v0, p0, Landroid/media/MediaPlayer;->mStayAwake:Z

    #@c
    if-eqz v0, :cond_13

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    invoke-interface {v1, v0}, Landroid/view/SurfaceHolder;->setKeepScreenOn(Z)V

    #@12
    .line 1207
    :cond_12
    return-void

    #@13
    .line 1205
    :cond_13
    const/4 v0, 0x0

    #@14
    goto :goto_f
.end method


# virtual methods
.method public native _setLG3DMusicIntro(I)I
.end method

.method public native _setLGAudioEffect(IIII)I
.end method

.method public native _setLGSoleCustomEQ(II)I
.end method

.method public native _setLGSoundNormalizerOnOff(I)V
.end method

.method public addTimedTextSource(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)V
    .registers 8
    .parameter "context"
    .parameter "uri"
    .parameter "mimeType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1897
    invoke-virtual {p2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@3
    move-result-object v2

    #@4
    .line 1898
    .local v2, scheme:Ljava/lang/String;
    if-eqz v2, :cond_e

    #@6
    const-string v3, "file"

    #@8
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v3

    #@c
    if-eqz v3, :cond_16

    #@e
    .line 1899
    :cond_e
    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {p0, v3, p3}, Landroid/media/MediaPlayer;->addTimedTextSource(Ljava/lang/String;Ljava/lang/String;)V

    #@15
    .line 1919
    :cond_15
    :goto_15
    return-void

    #@16
    .line 1903
    :cond_16
    const/4 v0, 0x0

    #@17
    .line 1905
    .local v0, fd:Landroid/content/res/AssetFileDescriptor;
    :try_start_17
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1a
    move-result-object v1

    #@1b
    .line 1906
    .local v1, resolver:Landroid/content/ContentResolver;
    const-string/jumbo v3, "r"

    #@1e
    invoke-virtual {v1, p2, v3}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    :try_end_21
    .catchall {:try_start_17 .. :try_end_21} :catchall_34
    .catch Ljava/lang/SecurityException; {:try_start_17 .. :try_end_21} :catch_3b
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_21} :catch_3f

    #@21
    move-result-object v0

    #@22
    .line 1907
    if-nez v0, :cond_2a

    #@24
    .line 1915
    if-eqz v0, :cond_15

    #@26
    .line 1916
    .end local v1           #resolver:Landroid/content/ContentResolver;
    :goto_26
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->close()V

    #@29
    goto :goto_15

    #@2a
    .line 1910
    .restart local v1       #resolver:Landroid/content/ContentResolver;
    :cond_2a
    :try_start_2a
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {p0, v3, p3}, Landroid/media/MediaPlayer;->addTimedTextSource(Ljava/io/FileDescriptor;Ljava/lang/String;)V
    :try_end_31
    .catchall {:try_start_2a .. :try_end_31} :catchall_34
    .catch Ljava/lang/SecurityException; {:try_start_2a .. :try_end_31} :catch_3b
    .catch Ljava/io/IOException; {:try_start_2a .. :try_end_31} :catch_3f

    #@31
    .line 1915
    if-eqz v0, :cond_15

    #@33
    goto :goto_26

    #@34
    .end local v1           #resolver:Landroid/content/ContentResolver;
    :catchall_34
    move-exception v3

    #@35
    if-eqz v0, :cond_3a

    #@37
    .line 1916
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->close()V

    #@3a
    .line 1915
    :cond_3a
    throw v3

    #@3b
    .line 1912
    :catch_3b
    move-exception v3

    #@3c
    .line 1915
    if-eqz v0, :cond_15

    #@3e
    goto :goto_26

    #@3f
    .line 1913
    :catch_3f
    move-exception v3

    #@40
    .line 1915
    if-eqz v0, :cond_15

    #@42
    goto :goto_26
.end method

.method public addTimedTextSource(Ljava/io/FileDescriptor;JJLjava/lang/String;)V
    .registers 12
    .parameter "fd"
    .parameter "offset"
    .parameter "length"
    .parameter "mimeType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1963
    invoke-static {p6}, Landroid/media/MediaPlayer;->availableMimeTypeForExternalSource(Ljava/lang/String;)Z

    #@3
    move-result v2

    #@4
    if-nez v2, :cond_1f

    #@6
    .line 1964
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@8
    new-instance v3, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v4, "Illegal mimeType for timed text source: "

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v2

    #@1f
    .line 1967
    :cond_1f
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@22
    move-result-object v1

    #@23
    .line 1968
    .local v1, request:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@26
    move-result-object v0

    #@27
    .line 1970
    .local v0, reply:Landroid/os/Parcel;
    :try_start_27
    const-string v2, "android.media.IMediaPlayer"

    #@29
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@2c
    .line 1971
    const/4 v2, 0x3

    #@2d
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@30
    .line 1972
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeFileDescriptor(Ljava/io/FileDescriptor;)V

    #@33
    .line 1973
    invoke-virtual {v1, p2, p3}, Landroid/os/Parcel;->writeLong(J)V

    #@36
    .line 1974
    invoke-virtual {v1, p4, p5}, Landroid/os/Parcel;->writeLong(J)V

    #@39
    .line 1975
    invoke-virtual {v1, p6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@3c
    .line 1976
    invoke-virtual {p0, v1, v0}, Landroid/media/MediaPlayer;->invoke(Landroid/os/Parcel;Landroid/os/Parcel;)V
    :try_end_3f
    .catchall {:try_start_27 .. :try_end_3f} :catchall_46

    #@3f
    .line 1978
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@42
    .line 1979
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@45
    .line 1981
    return-void

    #@46
    .line 1978
    :catchall_46
    move-exception v2

    #@47
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@4a
    .line 1979
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@4d
    .line 1978
    throw v2
.end method

.method public addTimedTextSource(Ljava/io/FileDescriptor;Ljava/lang/String;)V
    .registers 10
    .parameter "fd"
    .parameter "mimeType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1940
    const-wide/16 v2, 0x0

    #@2
    const-wide v4, 0x7ffffffffffffffL

    #@7
    move-object v0, p0

    #@8
    move-object v1, p1

    #@9
    move-object v6, p2

    #@a
    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaPlayer;->addTimedTextSource(Ljava/io/FileDescriptor;JJLjava/lang/String;)V

    #@d
    .line 1941
    return-void
.end method

.method public addTimedTextSource(Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "path"
    .parameter "mimeType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1863
    invoke-static {p2}, Landroid/media/MediaPlayer;->availableMimeTypeForExternalSource(Ljava/lang/String;)Z

    #@3
    move-result v4

    #@4
    if-nez v4, :cond_1f

    #@6
    .line 1864
    new-instance v4, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v5, "Illegal mimeType for timed text source: "

    #@d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v4

    #@15
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    .line 1865
    .local v3, msg:Ljava/lang/String;
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@1b
    invoke-direct {v4, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v4

    #@1f
    .line 1868
    .end local v3           #msg:Ljava/lang/String;
    :cond_1f
    new-instance v1, Ljava/io/File;

    #@21
    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@24
    .line 1869
    .local v1, file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@27
    move-result v4

    #@28
    if-eqz v4, :cond_3a

    #@2a
    .line 1870
    new-instance v2, Ljava/io/FileInputStream;

    #@2c
    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    #@2f
    .line 1871
    .local v2, is:Ljava/io/FileInputStream;
    invoke-virtual {v2}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    #@32
    move-result-object v0

    #@33
    .line 1872
    .local v0, fd:Ljava/io/FileDescriptor;
    invoke-virtual {p0, v0, p2}, Landroid/media/MediaPlayer;->addTimedTextSource(Ljava/io/FileDescriptor;Ljava/lang/String;)V

    #@36
    .line 1873
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    #@39
    .line 1878
    return-void

    #@3a
    .line 1876
    .end local v0           #fd:Ljava/io/FileDescriptor;
    .end local v2           #is:Ljava/io/FileInputStream;
    :cond_3a
    new-instance v4, Ljava/io/IOException;

    #@3c
    invoke-direct {v4, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@3f
    throw v4
.end method

.method public native attachAuxEffect(I)V
.end method

.method public deselectTrack(I)V
    .registers 3
    .parameter "index"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 2030
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/media/MediaPlayer;->selectOrDeselectTrack(IZ)V

    #@4
    .line 2031
    return-void
.end method

.method protected finalize()V
    .registers 1

    #@0
    .prologue
    .line 2104
    invoke-direct {p0}, Landroid/media/MediaPlayer;->native_finalize()V

    #@3
    return-void
.end method

.method public native getAudioSessionId()I
.end method

.method public native getCurrentPosition()I
.end method

.method public native getDuration()I
.end method

.method public native getFrameAt(I)Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public getIntParameter(I)I
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 1671
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1672
    .local v0, p:Landroid/os/Parcel;
    invoke-direct {p0, p1, v0}, Landroid/media/MediaPlayer;->getParameter(ILandroid/os/Parcel;)V

    #@7
    .line 1673
    invoke-virtual {v0}, Landroid/os/Parcel;->readInt()I

    #@a
    move-result v1

    #@b
    .line 1674
    .local v1, ret:I
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@e
    .line 1675
    return v1
.end method

.method public getMetadata(ZZ)Landroid/media/Metadata;
    .registers 7
    .parameter "update_only"
    .parameter "apply_filter"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1281
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v1

    #@5
    .line 1282
    .local v1, reply:Landroid/os/Parcel;
    new-instance v0, Landroid/media/Metadata;

    #@7
    invoke-direct {v0}, Landroid/media/Metadata;-><init>()V

    #@a
    .line 1284
    .local v0, data:Landroid/media/Metadata;
    invoke-direct {p0, p1, p2, v1}, Landroid/media/MediaPlayer;->native_getMetadata(ZZLandroid/os/Parcel;)Z

    #@d
    move-result v3

    #@e
    if-nez v3, :cond_15

    #@10
    .line 1285
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@13
    move-object v0, v2

    #@14
    .line 1295
    .end local v0           #data:Landroid/media/Metadata;
    :cond_14
    :goto_14
    return-object v0

    #@15
    .line 1291
    .restart local v0       #data:Landroid/media/Metadata;
    :cond_15
    invoke-virtual {v0, v1}, Landroid/media/Metadata;->parse(Landroid/os/Parcel;)Z

    #@18
    move-result v3

    #@19
    if-nez v3, :cond_14

    #@1b
    .line 1292
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    move-object v0, v2

    #@1f
    .line 1293
    goto :goto_14
.end method

.method public getParcelParameter(I)Landroid/os/Parcel;
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 1645
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1646
    .local v0, p:Landroid/os/Parcel;
    invoke-direct {p0, p1, v0}, Landroid/media/MediaPlayer;->getParameter(ILandroid/os/Parcel;)V

    #@7
    .line 1647
    return-object v0
.end method

.method public getStringParameter(I)Ljava/lang/String;
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 1657
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1658
    .local v0, p:Landroid/os/Parcel;
    invoke-direct {p0, p1, v0}, Landroid/media/MediaPlayer;->getParameter(ILandroid/os/Parcel;)V

    #@7
    .line 1659
    invoke-virtual {v0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    .line 1660
    .local v1, ret:Ljava/lang/String;
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@e
    .line 1661
    return-object v1
.end method

.method public getTrackInfo()[Landroid/media/MediaPlayer$TrackInfo;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1813
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v1

    #@4
    .line 1814
    .local v1, request:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v0

    #@8
    .line 1816
    .local v0, reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.media.IMediaPlayer"

    #@a
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1817
    const/4 v3, 0x1

    #@e
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 1818
    invoke-virtual {p0, v1, v0}, Landroid/media/MediaPlayer;->invoke(Landroid/os/Parcel;Landroid/os/Parcel;)V

    #@14
    .line 1819
    sget-object v3, Landroid/media/MediaPlayer$TrackInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@16
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    #@19
    move-result-object v2

    #@1a
    check-cast v2, [Landroid/media/MediaPlayer$TrackInfo;
    :try_end_1c
    .catchall {:try_start_8 .. :try_end_1c} :catchall_23

    #@1c
    .line 1822
    .local v2, trackInfo:[Landroid/media/MediaPlayer$TrackInfo;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 1823
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 1820
    return-object v2

    #@23
    .line 1822
    .end local v2           #trackInfo:[Landroid/media/MediaPlayer$TrackInfo;
    :catchall_23
    move-exception v3

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 1823
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 1822
    throw v3
.end method

.method public native getVideoHeight()I
.end method

.method public native getVideoWidth()I
.end method

.method public invoke(Landroid/os/Parcel;Landroid/os/Parcel;)V
    .registers 7
    .parameter "request"
    .parameter "reply"

    #@0
    .prologue
    .line 693
    invoke-direct {p0, p1, p2}, Landroid/media/MediaPlayer;->native_invoke(Landroid/os/Parcel;Landroid/os/Parcel;)I

    #@3
    move-result v0

    #@4
    .line 694
    .local v0, retcode:I
    const/4 v1, 0x0

    #@5
    invoke-virtual {p2, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    #@8
    .line 695
    if-eqz v0, :cond_23

    #@a
    .line 696
    new-instance v1, Ljava/lang/RuntimeException;

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "failure code: "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@22
    throw v1

    #@23
    .line 698
    :cond_23
    return-void
.end method

.method public native isLooping()Z
.end method

.method public native isPlaying()Z
.end method

.method public newRequest()Landroid/os/Parcel;
    .registers 3

    #@0
    .prologue
    .line 672
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 673
    .local v0, parcel:Landroid/os/Parcel;
    const-string v1, "android.media.IMediaPlayer"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 674
    return-object v0
.end method

.method public pause()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1130
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Landroid/media/MediaPlayer;->stayAwake(Z)V

    #@4
    .line 1131
    invoke-direct {p0}, Landroid/media/MediaPlayer;->_pause()V

    #@7
    .line 1132
    return-void
.end method

.method public native prepare()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public native prepareAsync()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public release()V
    .registers 14

    #@0
    .prologue
    const/16 v4, 0x100

    #@2
    const/4 v3, 0x0

    #@3
    const/4 v1, 0x0

    #@4
    .line 1384
    invoke-direct {p0, v3}, Landroid/media/MediaPlayer;->stayAwake(Z)V

    #@7
    .line 1385
    invoke-direct {p0}, Landroid/media/MediaPlayer;->updateSurfaceScreenOn()V

    #@a
    .line 1386
    iput-object v1, p0, Landroid/media/MediaPlayer;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    #@c
    .line 1387
    iput-object v1, p0, Landroid/media/MediaPlayer;->mOnBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    #@e
    .line 1388
    iput-object v1, p0, Landroid/media/MediaPlayer;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    #@10
    .line 1389
    iput-object v1, p0, Landroid/media/MediaPlayer;->mOnSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

    #@12
    .line 1390
    iput-object v1, p0, Landroid/media/MediaPlayer;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    #@14
    .line 1391
    iput-object v1, p0, Landroid/media/MediaPlayer;->mOnInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    #@16
    .line 1392
    iput-object v1, p0, Landroid/media/MediaPlayer;->mOnVideoSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    #@18
    .line 1393
    iput-object v1, p0, Landroid/media/MediaPlayer;->mOnTimedTextListener:Landroid/media/MediaPlayer$OnTimedTextListener;

    #@1a
    .line 1396
    iget-object v1, p0, Landroid/media/MediaPlayer;->mContext:Landroid/content/Context;

    #@1c
    if-eqz v1, :cond_41

    #@1e
    .line 1398
    const-string v1, "MediaPlayer[JAVA]"

    #@20
    const-string v3, "broadcasting MEDIA_CHANGE_PLAYER_STATE : released"

    #@22
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 1399
    new-instance v2, Landroid/content/Intent;

    #@27
    const-string v1, "com.lge.intent.action.MEDIA_PLAYER_STATE_CHANGED"

    #@29
    invoke-direct {v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@2c
    .line 1400
    .local v2, intent:Landroid/content/Intent;
    const-string v1, "com.lge.intent.extra.MEDIA_PLAYER_URI"

    #@2e
    iget-object v3, p0, Landroid/media/MediaPlayer;->mUriPath:Ljava/lang/String;

    #@30
    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@33
    .line 1401
    const-string v1, "com.lge.intent.extra.MEDIA_PLAYER_STATE"

    #@35
    invoke-virtual {v2, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@38
    .line 1402
    iget-object v1, p0, Landroid/media/MediaPlayer;->mContext:Landroid/content/Context;

    #@3a
    invoke-virtual {v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@3d
    .line 1430
    :goto_3d
    invoke-direct {p0}, Landroid/media/MediaPlayer;->_release()V

    #@40
    .line 1431
    return-void

    #@41
    .line 1407
    .end local v2           #intent:Landroid/content/Intent;
    :cond_41
    const-string v1, "MediaPlayer[JAVA]"

    #@43
    const-string v3, "broadcasting MEDIA_CHANGE_PLAYER_STATE : released"

    #@45
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 1408
    new-instance v2, Landroid/content/Intent;

    #@4a
    const-string v1, "com.lge.intent.action.MEDIA_PLAYER_STATE_CHANGED"

    #@4c
    invoke-direct {v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@4f
    .line 1409
    .restart local v2       #intent:Landroid/content/Intent;
    const-string v1, "com.lge.intent.extra.MEDIA_PLAYER_URI"

    #@51
    iget-object v3, p0, Landroid/media/MediaPlayer;->mUriPath:Ljava/lang/String;

    #@53
    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@56
    .line 1410
    const-string v1, "com.lge.intent.extra.MEDIA_PLAYER_STATE"

    #@58
    invoke-virtual {v2, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@5b
    .line 1412
    const/4 v1, 0x0

    #@5c
    :try_start_5c
    invoke-virtual {v2, v1}, Landroid/content/Intent;->setAllowFds(Z)V

    #@5f
    .line 1413
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@62
    move-result-object v0

    #@63
    .line 1414
    .local v0, myManager:Landroid/app/IActivityManager;
    if-eqz v0, :cond_7c

    #@65
    .line 1416
    const/4 v1, 0x0

    #@66
    const/4 v3, 0x0

    #@67
    const/4 v4, 0x0

    #@68
    const/4 v5, -0x1

    #@69
    const/4 v6, 0x0

    #@6a
    const/4 v7, 0x0

    #@6b
    const/4 v8, 0x0

    #@6c
    const/4 v9, 0x0

    #@6d
    const/4 v10, 0x0

    #@6e
    const/4 v11, -0x3

    #@6f
    invoke-interface/range {v0 .. v11}, Landroid/app/IActivityManager;->broadcastIntent(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ZZI)I
    :try_end_72
    .catch Landroid/os/RemoteException; {:try_start_5c .. :try_end_72} :catch_73

    #@72
    goto :goto_3d

    #@73
    .line 1423
    .end local v0           #myManager:Landroid/app/IActivityManager;
    :catch_73
    move-exception v12

    #@74
    .line 1424
    .local v12, e:Landroid/os/RemoteException;
    const-string v1, "MediaPlayer[JAVA]"

    #@76
    const-string v3, "Error: Sending broadcast intent"

    #@78
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7b
    goto :goto_3d

    #@7c
    .line 1421
    .end local v12           #e:Landroid/os/RemoteException;
    .restart local v0       #myManager:Landroid/app/IActivityManager;
    :cond_7c
    :try_start_7c
    const-string v1, "MediaPlayer[JAVA]"

    #@7e
    const-string/jumbo v3, "myManager is null"

    #@81
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_84
    .catch Landroid/os/RemoteException; {:try_start_7c .. :try_end_84} :catch_73

    #@84
    goto :goto_3d
.end method

.method public reset()V
    .registers 15

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    const/4 v13, 0x0

    #@3
    .line 1443
    invoke-direct {p0, v1}, Landroid/media/MediaPlayer;->stayAwake(Z)V

    #@6
    .line 1446
    iget-object v1, p0, Landroid/media/MediaPlayer;->mContext:Landroid/content/Context;

    #@8
    if-eqz v1, :cond_36

    #@a
    .line 1448
    const-string v1, "MediaPlayer[JAVA]"

    #@c
    const-string v3, "broadcasting MEDIA_CHANGE_PLAYER_STATE : reset"

    #@e
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 1449
    new-instance v2, Landroid/content/Intent;

    #@13
    const-string v1, "com.lge.intent.action.MEDIA_PLAYER_STATE_CHANGED"

    #@15
    invoke-direct {v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@18
    .line 1450
    .local v2, intent:Landroid/content/Intent;
    const-string v1, "com.lge.intent.extra.MEDIA_PLAYER_URI"

    #@1a
    iget-object v3, p0, Landroid/media/MediaPlayer;->mUriPath:Ljava/lang/String;

    #@1c
    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1f
    .line 1451
    const-string v1, "com.lge.intent.extra.MEDIA_PLAYER_STATE"

    #@21
    invoke-virtual {v2, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@24
    .line 1452
    iget-object v1, p0, Landroid/media/MediaPlayer;->mContext:Landroid/content/Context;

    #@26
    invoke-virtual {v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@29
    .line 1480
    :goto_29
    invoke-direct {p0}, Landroid/media/MediaPlayer;->_reset()V

    #@2c
    .line 1484
    iget-object v1, p0, Landroid/media/MediaPlayer;->mEventHandler:Landroid/media/MediaPlayer$EventHandler;

    #@2e
    if-eqz v1, :cond_35

    #@30
    .line 1485
    iget-object v1, p0, Landroid/media/MediaPlayer;->mEventHandler:Landroid/media/MediaPlayer$EventHandler;

    #@32
    invoke-virtual {v1, v13}, Landroid/media/MediaPlayer$EventHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    #@35
    .line 1488
    :cond_35
    return-void

    #@36
    .line 1457
    .end local v2           #intent:Landroid/content/Intent;
    :cond_36
    const-string v1, "MediaPlayer[JAVA]"

    #@38
    const-string v3, "broadcasting MEDIA_CHANGE_PLAYER_STATE : reset"

    #@3a
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 1458
    new-instance v2, Landroid/content/Intent;

    #@3f
    const-string v1, "com.lge.intent.action.MEDIA_PLAYER_STATE_CHANGED"

    #@41
    invoke-direct {v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@44
    .line 1459
    .restart local v2       #intent:Landroid/content/Intent;
    const-string v1, "com.lge.intent.extra.MEDIA_PLAYER_URI"

    #@46
    iget-object v3, p0, Landroid/media/MediaPlayer;->mUriPath:Ljava/lang/String;

    #@48
    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@4b
    .line 1460
    const-string v1, "com.lge.intent.extra.MEDIA_PLAYER_STATE"

    #@4d
    invoke-virtual {v2, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@50
    .line 1462
    const/4 v1, 0x0

    #@51
    :try_start_51
    invoke-virtual {v2, v1}, Landroid/content/Intent;->setAllowFds(Z)V

    #@54
    .line 1463
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@57
    move-result-object v0

    #@58
    .line 1464
    .local v0, myManager:Landroid/app/IActivityManager;
    if-eqz v0, :cond_71

    #@5a
    .line 1466
    const/4 v1, 0x0

    #@5b
    const/4 v3, 0x0

    #@5c
    const/4 v4, 0x0

    #@5d
    const/4 v5, -0x1

    #@5e
    const/4 v6, 0x0

    #@5f
    const/4 v7, 0x0

    #@60
    const/4 v8, 0x0

    #@61
    const/4 v9, 0x0

    #@62
    const/4 v10, 0x0

    #@63
    const/4 v11, -0x3

    #@64
    invoke-interface/range {v0 .. v11}, Landroid/app/IActivityManager;->broadcastIntent(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ZZI)I
    :try_end_67
    .catch Landroid/os/RemoteException; {:try_start_51 .. :try_end_67} :catch_68

    #@67
    goto :goto_29

    #@68
    .line 1473
    .end local v0           #myManager:Landroid/app/IActivityManager;
    :catch_68
    move-exception v12

    #@69
    .line 1474
    .local v12, e:Landroid/os/RemoteException;
    const-string v1, "MediaPlayer[JAVA]"

    #@6b
    const-string v3, "Error: Sending broadcast intent"

    #@6d
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@70
    goto :goto_29

    #@71
    .line 1471
    .end local v12           #e:Landroid/os/RemoteException;
    .restart local v0       #myManager:Landroid/app/IActivityManager;
    :cond_71
    :try_start_71
    const-string v1, "MediaPlayer[JAVA]"

    #@73
    const-string/jumbo v3, "myManager is null"

    #@76
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_79
    .catch Landroid/os/RemoteException; {:try_start_71 .. :try_end_79} :catch_68

    #@79
    goto :goto_29
.end method

.method public native seekTo(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public selectTrack(I)V
    .registers 3
    .parameter "index"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 2012
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, v0}, Landroid/media/MediaPlayer;->selectOrDeselectTrack(IZ)V

    #@4
    .line 2013
    return-void
.end method

.method public native setAudioSessionId(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public native setAudioStreamType(I)V
.end method

.method public native setAuxEffectSendLevel(F)V
.end method

.method public setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    .registers 4
    .parameter "context"
    .parameter "uri"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 912
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, v0}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    #@4
    .line 913
    return-void
.end method

.method public setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V
    .registers 13
    .parameter "context"
    .parameter "uri"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 927
    .local p3, headers:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Landroid/media/MediaPlayer;->setContext(Landroid/content/Context;)V

    #@3
    .line 928
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    invoke-direct {p0, v0}, Landroid/media/MediaPlayer;->setUri(Ljava/lang/String;)V

    #@a
    .line 931
    invoke-virtual {p2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@d
    move-result-object v8

    #@e
    .line 932
    .local v8, scheme:Ljava/lang/String;
    if-eqz v8, :cond_18

    #@10
    const-string v0, "file"

    #@12
    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_20

    #@18
    .line 933
    :cond_18
    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {p0, v0}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    #@1f
    .line 963
    :cond_1f
    :goto_1f
    return-void

    #@20
    .line 937
    :cond_20
    const/4 v6, 0x0

    #@21
    .line 939
    .local v6, fd:Landroid/content/res/AssetFileDescriptor;
    :try_start_21
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@24
    move-result-object v7

    #@25
    .line 940
    .local v7, resolver:Landroid/content/ContentResolver;
    const-string/jumbo v0, "r"

    #@28
    invoke-virtual {v7, p2, v0}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    :try_end_2b
    .catchall {:try_start_21 .. :try_end_2b} :catchall_6e
    .catch Ljava/lang/SecurityException; {:try_start_21 .. :try_end_2b} :catch_59
    .catch Ljava/io/IOException; {:try_start_21 .. :try_end_2b} :catch_75

    #@2b
    move-result-object v6

    #@2c
    .line 941
    if-nez v6, :cond_34

    #@2e
    .line 956
    if-eqz v6, :cond_1f

    #@30
    .line 957
    :goto_30
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V

    #@33
    goto :goto_1f

    #@34
    .line 947
    :cond_34
    :try_start_34
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getDeclaredLength()J

    #@37
    move-result-wide v0

    #@38
    const-wide/16 v2, 0x0

    #@3a
    cmp-long v0, v0, v2

    #@3c
    if-gez v0, :cond_48

    #@3e
    .line 948
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@41
    move-result-object v0

    #@42
    invoke-virtual {p0, v0}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;)V

    #@45
    .line 956
    :goto_45
    if-eqz v6, :cond_1f

    #@47
    goto :goto_30

    #@48
    .line 950
    :cond_48
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@4b
    move-result-object v1

    #@4c
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    #@4f
    move-result-wide v2

    #@50
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getDeclaredLength()J

    #@53
    move-result-wide v4

    #@54
    move-object v0, p0

    #@55
    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V
    :try_end_58
    .catchall {:try_start_34 .. :try_end_58} :catchall_6e
    .catch Ljava/lang/SecurityException; {:try_start_34 .. :try_end_58} :catch_59
    .catch Ljava/io/IOException; {:try_start_34 .. :try_end_58} :catch_75

    #@58
    goto :goto_45

    #@59
    .line 953
    .end local v7           #resolver:Landroid/content/ContentResolver;
    :catch_59
    move-exception v0

    #@5a
    .line 956
    if-eqz v6, :cond_5f

    #@5c
    .line 957
    :goto_5c
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V

    #@5f
    .line 961
    :cond_5f
    const-string v0, "MediaPlayer[JAVA]"

    #@61
    const-string v1, "Couldn\'t open file on client side, trying server side"

    #@63
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    .line 962
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@69
    move-result-object v0

    #@6a
    invoke-virtual {p0, v0, p3}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;Ljava/util/Map;)V

    #@6d
    goto :goto_1f

    #@6e
    .line 956
    :catchall_6e
    move-exception v0

    #@6f
    if-eqz v6, :cond_74

    #@71
    .line 957
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V

    #@74
    .line 956
    :cond_74
    throw v0

    #@75
    .line 954
    :catch_75
    move-exception v0

    #@76
    .line 956
    if-eqz v6, :cond_5f

    #@78
    goto :goto_5c
.end method

.method public setDataSource(Ljava/io/FileDescriptor;)V
    .registers 8
    .parameter "fd"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1043
    const-wide/16 v2, 0x0

    #@2
    const-wide v4, 0x7ffffffffffffffL

    #@7
    move-object v0, p0

    #@8
    move-object v1, p1

    #@9
    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V

    #@c
    .line 1044
    return-void
.end method

.method public native setDataSource(Ljava/io/FileDescriptor;JJ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public setDataSource(Ljava/lang/String;)V
    .registers 3
    .parameter "path"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 981
    invoke-direct {p0, p1, v0, v0}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    #@4
    .line 982
    return-void
.end method

.method public setDataSource(Ljava/lang/String;Ljava/util/Map;)V
    .registers 9
    .parameter "path"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 995
    .local p2, headers:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v3, 0x0

    #@1
    .line 996
    .local v3, keys:[Ljava/lang/String;
    const/4 v4, 0x0

    #@2
    .line 998
    .local v4, values:[Ljava/lang/String;
    if-eqz p2, :cond_38

    #@4
    .line 999
    invoke-interface {p2}, Ljava/util/Map;->size()I

    #@7
    move-result v5

    #@8
    new-array v3, v5, [Ljava/lang/String;

    #@a
    .line 1000
    invoke-interface {p2}, Ljava/util/Map;->size()I

    #@d
    move-result v5

    #@e
    new-array v4, v5, [Ljava/lang/String;

    #@10
    .line 1002
    const/4 v1, 0x0

    #@11
    .line 1003
    .local v1, i:I
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@14
    move-result-object v5

    #@15
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@18
    move-result-object v2

    #@19
    .local v2, i$:Ljava/util/Iterator;
    :goto_19
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@1c
    move-result v5

    #@1d
    if-eqz v5, :cond_38

    #@1f
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@22
    move-result-object v0

    #@23
    check-cast v0, Ljava/util/Map$Entry;

    #@25
    .line 1004
    .local v0, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@28
    move-result-object v5

    #@29
    check-cast v5, Ljava/lang/String;

    #@2b
    aput-object v5, v3, v1

    #@2d
    .line 1005
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@30
    move-result-object v5

    #@31
    check-cast v5, Ljava/lang/String;

    #@33
    aput-object v5, v4, v1

    #@35
    .line 1006
    add-int/lit8 v1, v1, 0x1

    #@37
    goto :goto_19

    #@38
    .line 1009
    .end local v0           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1           #i:I
    .end local v2           #i$:Ljava/util/Iterator;
    :cond_38
    invoke-direct {p0, p1, v3, v4}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    #@3b
    .line 1010
    return-void
.end method

.method public setDisplay(Landroid/view/SurfaceHolder;)V
    .registers 3
    .parameter "sh"

    #@0
    .prologue
    .line 713
    iput-object p1, p0, Landroid/media/MediaPlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    #@2
    .line 715
    if-eqz p1, :cond_f

    #@4
    .line 716
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    #@7
    move-result-object v0

    #@8
    .line 720
    .local v0, surface:Landroid/view/Surface;
    :goto_8
    invoke-direct {p0, v0}, Landroid/media/MediaPlayer;->_setVideoSurface(Landroid/view/Surface;)V

    #@b
    .line 721
    invoke-direct {p0}, Landroid/media/MediaPlayer;->updateSurfaceScreenOn()V

    #@e
    .line 722
    return-void

    #@f
    .line 718
    .end local v0           #surface:Landroid/view/Surface;
    :cond_f
    const/4 v0, 0x0

    #@10
    .restart local v0       #surface:Landroid/view/Surface;
    goto :goto_8
.end method

.method public setLG3DMusicIntro(I)I
    .registers 3
    .parameter "Prev1Next2"

    #@0
    .prologue
    .line 2850
    invoke-virtual {p0, p1}, Landroid/media/MediaPlayer;->_setLG3DMusicIntro(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public setLGAudioEffect(IIII)I
    .registers 6
    .parameter "iEnable"
    .parameter "iType"
    .parameter "iPath"
    .parameter "iMedia"

    #@0
    .prologue
    .line 2820
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/media/MediaPlayer;->_setLGAudioEffect(IIII)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public setLGSoleCustomEQ(II)I
    .registers 4
    .parameter "iNumBand"
    .parameter "iNumGain"

    #@0
    .prologue
    .line 2835
    invoke-virtual {p0, p1, p2}, Landroid/media/MediaPlayer;->_setLGSoleCustomEQ(II)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public setLGSoundNormalizerOnOff(I)V
    .registers 2
    .parameter "normalzierOnOff"

    #@0
    .prologue
    .line 2880
    invoke-virtual {p0, p1}, Landroid/media/MediaPlayer;->_setLGSoundNormalizerOnOff(I)V

    #@3
    .line 2881
    return-void
.end method

.method public native setLooping(Z)V
.end method

.method public setMetadataFilter(Ljava/util/Set;Ljava/util/Set;)I
    .registers 10
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    #@0
    .prologue
    .line 1323
    .local p1, allow:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/Integer;>;"
    .local p2, block:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-virtual {p0}, Landroid/media/MediaPlayer;->newRequest()Landroid/os/Parcel;

    #@3
    move-result-object v2

    #@4
    .line 1329
    .local v2, request:Landroid/os/Parcel;
    invoke-virtual {v2}, Landroid/os/Parcel;->dataSize()I

    #@7
    move-result v4

    #@8
    invoke-interface {p1}, Ljava/util/Set;->size()I

    #@b
    move-result v5

    #@c
    add-int/lit8 v5, v5, 0x1

    #@e
    add-int/lit8 v5, v5, 0x1

    #@10
    invoke-interface {p2}, Ljava/util/Set;->size()I

    #@13
    move-result v6

    #@14
    add-int/2addr v5, v6

    #@15
    mul-int/lit8 v5, v5, 0x4

    #@17
    add-int v0, v4, v5

    #@19
    .line 1331
    .local v0, capacity:I
    invoke-virtual {v2}, Landroid/os/Parcel;->dataCapacity()I

    #@1c
    move-result v4

    #@1d
    if-ge v4, v0, :cond_22

    #@1f
    .line 1332
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->setDataCapacity(I)V

    #@22
    .line 1335
    :cond_22
    invoke-interface {p1}, Ljava/util/Set;->size()I

    #@25
    move-result v4

    #@26
    invoke-virtual {v2, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    .line 1336
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@2c
    move-result-object v1

    #@2d
    .local v1, i$:Ljava/util/Iterator;
    :goto_2d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@30
    move-result v4

    #@31
    if-eqz v4, :cond_41

    #@33
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@36
    move-result-object v3

    #@37
    check-cast v3, Ljava/lang/Integer;

    #@39
    .line 1337
    .local v3, t:Ljava/lang/Integer;
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@3c
    move-result v4

    #@3d
    invoke-virtual {v2, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@40
    goto :goto_2d

    #@41
    .line 1339
    .end local v3           #t:Ljava/lang/Integer;
    :cond_41
    invoke-interface {p2}, Ljava/util/Set;->size()I

    #@44
    move-result v4

    #@45
    invoke-virtual {v2, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@48
    .line 1340
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@4b
    move-result-object v1

    #@4c
    :goto_4c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@4f
    move-result v4

    #@50
    if-eqz v4, :cond_60

    #@52
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@55
    move-result-object v3

    #@56
    check-cast v3, Ljava/lang/Integer;

    #@58
    .line 1341
    .restart local v3       #t:Ljava/lang/Integer;
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@5b
    move-result v4

    #@5c
    invoke-virtual {v2, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@5f
    goto :goto_4c

    #@60
    .line 1343
    .end local v3           #t:Ljava/lang/Integer;
    :cond_60
    invoke-direct {p0, v2}, Landroid/media/MediaPlayer;->native_setMetadataFilter(Landroid/os/Parcel;)I

    #@63
    move-result v4

    #@64
    return v4
.end method

.method public native setNextMediaPlayer(Landroid/media/MediaPlayer;)V
.end method

.method public setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 2518
    iput-object p1, p0, Landroid/media/MediaPlayer;->mOnBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    #@2
    .line 2519
    return-void
.end method

.method public setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 2484
    iput-object p1, p0, Landroid/media/MediaPlayer;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    #@2
    .line 2485
    return-void
.end method

.method public setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 2682
    iput-object p1, p0, Landroid/media/MediaPlayer;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    #@2
    .line 2683
    return-void
.end method

.method public setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 2801
    iput-object p1, p0, Landroid/media/MediaPlayer;->mOnInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    #@2
    .line 2802
    return-void
.end method

.method public setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 2457
    iput-object p1, p0, Landroid/media/MediaPlayer;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    #@2
    .line 2458
    return-void
.end method

.method public setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 2545
    iput-object p1, p0, Landroid/media/MediaPlayer;->mOnSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

    #@2
    .line 2546
    return-void
.end method

.method public setOnTimedTextListener(Landroid/media/MediaPlayer$OnTimedTextListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 2606
    iput-object p1, p0, Landroid/media/MediaPlayer;->mOnTimedTextListener:Landroid/media/MediaPlayer$OnTimedTextListener;

    #@2
    .line 2607
    return-void
.end method

.method public setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 2577
    iput-object p1, p0, Landroid/media/MediaPlayer;->mOnVideoSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    #@2
    .line 2578
    return-void
.end method

.method public setParameter(II)Z
    .registers 5
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 1623
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1624
    .local v0, p:Landroid/os/Parcel;
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@7
    .line 1625
    invoke-virtual {p0, p1, v0}, Landroid/media/MediaPlayer;->setParameter(ILandroid/os/Parcel;)Z

    #@a
    move-result v1

    #@b
    .line 1626
    .local v1, ret:Z
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@e
    .line 1627
    return v1
.end method

.method public native setParameter(ILandroid/os/Parcel;)Z
.end method

.method public setParameter(ILjava/lang/String;)Z
    .registers 5
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 1608
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1609
    .local v0, p:Landroid/os/Parcel;
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@7
    .line 1610
    invoke-virtual {p0, p1, v0}, Landroid/media/MediaPlayer;->setParameter(ILandroid/os/Parcel;)Z

    #@a
    move-result v1

    #@b
    .line 1611
    .local v1, ret:Z
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@e
    .line 1612
    return v1
.end method

.method public setRetransmitEndpoint(Ljava/net/InetSocketAddress;)V
    .registers 8
    .parameter "endpoint"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 2087
    const/4 v0, 0x0

    #@1
    .line 2088
    .local v0, addrString:Ljava/lang/String;
    const/4 v1, 0x0

    #@2
    .line 2090
    .local v1, port:I
    if-eqz p1, :cond_10

    #@4
    .line 2091
    invoke-virtual {p1}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;

    #@7
    move-result-object v3

    #@8
    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    .line 2092
    invoke-virtual {p1}, Ljava/net/InetSocketAddress;->getPort()I

    #@f
    move-result v1

    #@10
    .line 2095
    :cond_10
    invoke-direct {p0, v0, v1}, Landroid/media/MediaPlayer;->native_setRetransmitEndpoint(Ljava/lang/String;I)I

    #@13
    move-result v2

    #@14
    .line 2096
    .local v2, ret:I
    if-eqz v2, :cond_2f

    #@16
    .line 2097
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@18
    new-instance v4, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v5, "Illegal re-transmit endpoint; native ret "

    #@1f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v4

    #@27
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v4

    #@2b
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2e
    throw v3

    #@2f
    .line 2099
    :cond_2f
    return-void
.end method

.method public setScreenOnWhilePlaying(Z)V
    .registers 4
    .parameter "screenOn"

    #@0
    .prologue
    .line 1182
    iget-boolean v0, p0, Landroid/media/MediaPlayer;->mScreenOnWhilePlaying:Z

    #@2
    if-eq v0, p1, :cond_17

    #@4
    .line 1183
    if-eqz p1, :cond_12

    #@6
    iget-object v0, p0, Landroid/media/MediaPlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    #@8
    if-nez v0, :cond_12

    #@a
    .line 1184
    const-string v0, "MediaPlayer[JAVA]"

    #@c
    const-string/jumbo v1, "setScreenOnWhilePlaying(true) is ineffective without a SurfaceHolder"

    #@f
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 1186
    :cond_12
    iput-boolean p1, p0, Landroid/media/MediaPlayer;->mScreenOnWhilePlaying:Z

    #@14
    .line 1187
    invoke-direct {p0}, Landroid/media/MediaPlayer;->updateSurfaceScreenOn()V

    #@17
    .line 1189
    :cond_17
    return-void
.end method

.method public setSurface(Landroid/view/Surface;)V
    .registers 4
    .parameter "surface"

    #@0
    .prologue
    .line 743
    iget-boolean v0, p0, Landroid/media/MediaPlayer;->mScreenOnWhilePlaying:Z

    #@2
    if-eqz v0, :cond_e

    #@4
    if-eqz p1, :cond_e

    #@6
    .line 744
    const-string v0, "MediaPlayer[JAVA]"

    #@8
    const-string/jumbo v1, "setScreenOnWhilePlaying(true) is ineffective for Surface"

    #@b
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 746
    :cond_e
    const/4 v0, 0x0

    #@f
    iput-object v0, p0, Landroid/media/MediaPlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    #@11
    .line 747
    invoke-direct {p0, p1}, Landroid/media/MediaPlayer;->_setVideoSurface(Landroid/view/Surface;)V

    #@14
    .line 748
    invoke-direct {p0}, Landroid/media/MediaPlayer;->updateSurfaceScreenOn()V

    #@17
    .line 749
    return-void
.end method

.method public setVideoScalingMode(I)V
    .registers 7
    .parameter "mode"

    #@0
    .prologue
    .line 792
    invoke-direct {p0, p1}, Landroid/media/MediaPlayer;->isVideoScalingModeSupported(I)Z

    #@3
    move-result v3

    #@4
    if-nez v3, :cond_25

    #@6
    .line 793
    new-instance v3, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v4, "Scaling mode "

    #@d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    const-string v4, " is not supported"

    #@17
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    .line 794
    .local v0, msg:Ljava/lang/String;
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@21
    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@24
    throw v3

    #@25
    .line 796
    .end local v0           #msg:Ljava/lang/String;
    :cond_25
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@28
    move-result-object v2

    #@29
    .line 797
    .local v2, request:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@2c
    move-result-object v1

    #@2d
    .line 799
    .local v1, reply:Landroid/os/Parcel;
    :try_start_2d
    const-string v3, "android.media.IMediaPlayer"

    #@2f
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@32
    .line 800
    const/4 v3, 0x6

    #@33
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@36
    .line 801
    invoke-virtual {v2, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@39
    .line 802
    invoke-virtual {p0, v2, v1}, Landroid/media/MediaPlayer;->invoke(Landroid/os/Parcel;Landroid/os/Parcel;)V
    :try_end_3c
    .catchall {:try_start_2d .. :try_end_3c} :catchall_43

    #@3c
    .line 804
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@3f
    .line 805
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@42
    .line 807
    return-void

    #@43
    .line 804
    :catchall_43
    move-exception v3

    #@44
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@47
    .line 805
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@4a
    .line 804
    throw v3
.end method

.method public native setVolume(FF)V
.end method

.method public setWakeMode(Landroid/content/Context;I)V
    .registers 7
    .parameter "context"
    .parameter "mode"

    #@0
    .prologue
    .line 1154
    const/4 v1, 0x0

    #@1
    .line 1155
    .local v1, washeld:Z
    iget-object v2, p0, Landroid/media/MediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@3
    if-eqz v2, :cond_16

    #@5
    .line 1156
    iget-object v2, p0, Landroid/media/MediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@7
    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@a
    move-result v2

    #@b
    if-eqz v2, :cond_13

    #@d
    .line 1157
    const/4 v1, 0x1

    #@e
    .line 1158
    iget-object v2, p0, Landroid/media/MediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@10
    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    #@13
    .line 1160
    :cond_13
    const/4 v2, 0x0

    #@14
    iput-object v2, p0, Landroid/media/MediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@16
    .line 1163
    :cond_16
    const-string/jumbo v2, "power"

    #@19
    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1c
    move-result-object v0

    #@1d
    check-cast v0, Landroid/os/PowerManager;

    #@1f
    .line 1164
    .local v0, pm:Landroid/os/PowerManager;
    const/high16 v2, 0x2000

    #@21
    or-int/2addr v2, p2

    #@22
    const-class v3, Landroid/media/MediaPlayer;

    #@24
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@2b
    move-result-object v2

    #@2c
    iput-object v2, p0, Landroid/media/MediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2e
    .line 1165
    iget-object v2, p0, Landroid/media/MediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@30
    const/4 v3, 0x0

    #@31
    invoke-virtual {v2, v3}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    #@34
    .line 1166
    if-eqz v1, :cond_3b

    #@36
    .line 1167
    iget-object v2, p0, Landroid/media/MediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@38
    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@3b
    .line 1169
    :cond_3b
    return-void
.end method

.method public start()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1091
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, v0}, Landroid/media/MediaPlayer;->stayAwake(Z)V

    #@4
    .line 1095
    iget-object v0, p0, Landroid/media/MediaPlayer;->mContext:Landroid/content/Context;

    #@6
    if-eqz v0, :cond_41

    #@8
    .line 1096
    iget-object v0, p0, Landroid/media/MediaPlayer;->mContext:Landroid/content/Context;

    #@a
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    const-string v1, "com.nttdocomo.android.mediaplayer.VideoPlayerActivity"

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@13
    move-result v0

    #@14
    if-eqz v0, :cond_41

    #@16
    .line 1097
    const/4 v0, 0x0

    #@17
    invoke-virtual {p0, v0}, Landroid/media/MediaPlayer;->setLGSoundNormalizerOnOff(I)V

    #@1a
    .line 1098
    const-string v0, "MediaPlayer[JAVA]"

    #@1c
    new-instance v1, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string/jumbo v2, "mContext.toString() = "

    #@24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    iget-object v2, p0, Landroid/media/MediaPlayer;->mContext:Landroid/content/Context;

    #@2a
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v1

    #@36
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 1099
    const-string v0, "MediaPlayer[JAVA]"

    #@3b
    const-string/jumbo v1, "setLGSoundNormalizerOnOff(0)"

    #@3e
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 1105
    :cond_41
    invoke-direct {p0}, Landroid/media/MediaPlayer;->_start()V

    #@44
    .line 1106
    return-void
.end method

.method public stop()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1117
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Landroid/media/MediaPlayer;->stayAwake(Z)V

    #@4
    .line 1118
    invoke-direct {p0}, Landroid/media/MediaPlayer;->_stop()V

    #@7
    .line 1119
    return-void
.end method
