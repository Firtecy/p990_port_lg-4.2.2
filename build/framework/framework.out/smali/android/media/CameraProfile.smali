.class public Landroid/media/CameraProfile;
.super Ljava/lang/Object;
.source "CameraProfile.java"


# static fields
.field public static final QUALITY_HIGH:I = 0x2

.field public static final QUALITY_LOW:I = 0x0

.field public static final QUALITY_MEDIUM:I = 0x1

.field private static final sCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "[I>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 47
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    sput-object v0, Landroid/media/CameraProfile;->sCache:Ljava/util/HashMap;

    #@7
    .line 92
    const-string/jumbo v0, "media_jni"

    #@a
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@d
    .line 93
    invoke-static {}, Landroid/media/CameraProfile;->native_init()V

    #@10
    .line 94
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 31
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method private static getImageEncodingQualityLevels(I)[I
    .registers 7
    .parameter "cameraId"

    #@0
    .prologue
    .line 97
    invoke-static {p0}, Landroid/media/CameraProfile;->native_get_num_image_encoding_quality_levels(I)I

    #@3
    move-result v2

    #@4
    .line 98
    .local v2, nLevels:I
    const/4 v3, 0x3

    #@5
    if-eq v2, v3, :cond_20

    #@7
    .line 99
    new-instance v3, Ljava/lang/RuntimeException;

    #@9
    new-instance v4, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v5, "Unexpected Jpeg encoding quality levels "

    #@10
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v4

    #@14
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v4

    #@1c
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v3

    #@20
    .line 102
    :cond_20
    new-array v1, v2, [I

    #@22
    .line 103
    .local v1, levels:[I
    const/4 v0, 0x0

    #@23
    .local v0, i:I
    :goto_23
    if-ge v0, v2, :cond_2e

    #@25
    .line 104
    invoke-static {p0, v0}, Landroid/media/CameraProfile;->native_get_image_encoding_quality_level(II)I

    #@28
    move-result v3

    #@29
    aput v3, v1, v0

    #@2b
    .line 103
    add-int/lit8 v0, v0, 0x1

    #@2d
    goto :goto_23

    #@2e
    .line 106
    :cond_2e
    invoke-static {v1}, Ljava/util/Arrays;->sort([I)V

    #@31
    .line 107
    return-object v1
.end method

.method public static getJpegEncodingQualityParameter(I)I
    .registers 5
    .parameter "quality"

    #@0
    .prologue
    .line 58
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    #@3
    move-result v2

    #@4
    .line 59
    .local v2, numberOfCameras:I
    new-instance v0, Landroid/hardware/Camera$CameraInfo;

    #@6
    invoke-direct {v0}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    #@9
    .line 60
    .local v0, cameraInfo:Landroid/hardware/Camera$CameraInfo;
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    :goto_a
    if-ge v1, v2, :cond_1b

    #@c
    .line 61
    invoke-static {v1, v0}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    #@f
    .line 62
    iget v3, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    #@11
    if-nez v3, :cond_18

    #@13
    .line 63
    invoke-static {v1, p0}, Landroid/media/CameraProfile;->getJpegEncodingQualityParameter(II)I

    #@16
    move-result v3

    #@17
    .line 66
    :goto_17
    return v3

    #@18
    .line 60
    :cond_18
    add-int/lit8 v1, v1, 0x1

    #@1a
    goto :goto_a

    #@1b
    .line 66
    :cond_1b
    const/4 v3, 0x0

    #@1c
    goto :goto_17
.end method

.method public static getJpegEncodingQualityParameter(II)I
    .registers 6
    .parameter "cameraId"
    .parameter "quality"

    #@0
    .prologue
    .line 78
    if-ltz p1, :cond_5

    #@2
    const/4 v1, 0x2

    #@3
    if-le p1, v1, :cond_1e

    #@5
    .line 79
    :cond_5
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@7
    new-instance v2, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v3, "Unsupported quality level: "

    #@e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v1

    #@1e
    .line 81
    :cond_1e
    sget-object v2, Landroid/media/CameraProfile;->sCache:Ljava/util/HashMap;

    #@20
    monitor-enter v2

    #@21
    .line 82
    :try_start_21
    sget-object v1, Landroid/media/CameraProfile;->sCache:Ljava/util/HashMap;

    #@23
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2a
    move-result-object v0

    #@2b
    check-cast v0, [I

    #@2d
    .line 83
    .local v0, levels:[I
    if-nez v0, :cond_3c

    #@2f
    .line 84
    invoke-static {p0}, Landroid/media/CameraProfile;->getImageEncodingQualityLevels(I)[I

    #@32
    move-result-object v0

    #@33
    .line 85
    sget-object v1, Landroid/media/CameraProfile;->sCache:Ljava/util/HashMap;

    #@35
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3c
    .line 87
    :cond_3c
    aget v1, v0, p1

    #@3e
    monitor-exit v2

    #@3f
    return v1

    #@40
    .line 88
    .end local v0           #levels:[I
    :catchall_40
    move-exception v1

    #@41
    monitor-exit v2
    :try_end_42
    .catchall {:try_start_21 .. :try_end_42} :catchall_40

    #@42
    throw v1
.end method

.method private static final native native_get_image_encoding_quality_level(II)I
.end method

.method private static final native native_get_num_image_encoding_quality_levels(I)I
.end method

.method private static final native native_init()V
.end method
