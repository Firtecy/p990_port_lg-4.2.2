.class public Landroid/media/MediaScanner;
.super Ljava/lang/Object;
.source "MediaScanner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/MediaScanner$WplHandler;,
        Landroid/media/MediaScanner$MediaBulkDeleter;,
        Landroid/media/MediaScanner$MyMediaScannerClient;,
        Landroid/media/MediaScanner$PlaylistEntry;,
        Landroid/media/MediaScanner$FileEntry;
    }
.end annotation


# static fields
.field private static final ALARMS_DIR:Ljava/lang/String; = "/alarms/"

.field private static final CAMERA_DIR:Ljava/lang/String; = "/dcim/"

.field private static final DATE_MODIFIED_PLAYLISTS_COLUMN_INDEX:I = 0x2

.field private static final DEFAULT_RINGTONE_PROPERTY_PREFIX:Ljava/lang/String; = "ro.config."

.field private static final ENABLE_BULK_INSERTS:Z = true

.field private static final FILES_PRESCAN_DATE_MODIFIED_COLUMN_INDEX:I = 0x3

.field private static final FILES_PRESCAN_FORMAT_COLUMN_INDEX:I = 0x2

.field private static final FILES_PRESCAN_ID_COLUMN_INDEX:I = 0x0

.field private static final FILES_PRESCAN_PATH_COLUMN_INDEX:I = 0x1

.field private static final FILES_PRESCAN_PROJECTION:[Ljava/lang/String; = null

.field private static final ID3_GENRES:[Ljava/lang/String; = null

.field private static final ID_PLAYLISTS_COLUMN_INDEX:I = 0x0

.field private static final ID_PROJECTION:[Ljava/lang/String; = null

.field private static final MUSIC_DIR:Ljava/lang/String; = "/music/"

.field private static final NOTIFICATIONS_DIR:Ljava/lang/String; = "/notifications/"

.field private static final PATH_PLAYLISTS_COLUMN_INDEX:I = 0x1

.field private static final PLAYLIST_MEMBERS_PROJECTION:[Ljava/lang/String; = null

.field private static final PODCAST_DIR:Ljava/lang/String; = "/podcasts/"

.field private static final RINGTONES_DIR:Ljava/lang/String; = "/ringtones/"

.field private static final SETTINGS_ITEM_COUNT:I = 0x3

.field private static final TAG:Ljava/lang/String; = "MediaScanner"

.field private static final VOICERECORDING_DIR:Ljava/lang/String; = "/my_sounds/"


# instance fields
.field private mAudioUri:Landroid/net/Uri;

.field private final mBitmapOptions:Landroid/graphics/BitmapFactory$Options;

.field private mCaseInsensitivePaths:Z

.field private final mClient:Landroid/media/MediaScanner$MyMediaScannerClient;

.field private mContext:Landroid/content/Context;

.field private mDefaultAlarmAlertFilename:Ljava/lang/String;

.field private mDefaultAlarmSet:Z

.field private mDefaultNotificationFilename:Ljava/lang/String;

.field private mDefaultNotificationSet:Z

.field private mDefaultRingtoneFilename:Ljava/lang/String;

.field private mDefaultRingtoneSet:Z

.field private mDrmManagerClient:Landroid/drm/DrmManagerClient;

.field private final mExternalIsEmulated:Z

.field private final mExternalStoragePath:Ljava/lang/String;

.field private mFileCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/media/MediaScanner$FileEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mFilesUri:Landroid/net/Uri;

.field private mImagesUri:Landroid/net/Uri;

.field private mMediaInserter:Landroid/media/MediaInserter;

.field private mMediaProvider:Landroid/content/IContentProvider;

.field private mMtpObjectHandle:I

.field private mNativeContext:I

.field private mOriginalCount:I

.field private mPlayLists:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/media/MediaScanner$FileEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mPlaylistEntries:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/media/MediaScanner$PlaylistEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mPlaylistsUri:Landroid/net/Uri;

.field private mProcessGenres:Z

.field private mProcessPlaylists:Z

.field private mThumbsUri:Landroid/net/Uri;

.field private mVideoUri:Landroid/net/Uri;

.field private mWasEmptyPriorToScan:Z


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    const/4 v6, 0x4

    #@1
    const/4 v5, 0x3

    #@2
    const/4 v4, 0x2

    #@3
    const/4 v3, 0x1

    #@4
    const/4 v2, 0x0

    #@5
    .line 127
    const-string/jumbo v0, "media_jni"

    #@8
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@b
    .line 128
    invoke-static {}, Landroid/media/MediaScanner;->native_init()V

    #@e
    .line 133
    new-array v0, v6, [Ljava/lang/String;

    #@10
    const-string v1, "_id"

    #@12
    aput-object v1, v0, v2

    #@14
    const-string v1, "_data"

    #@16
    aput-object v1, v0, v3

    #@18
    const-string v1, "format"

    #@1a
    aput-object v1, v0, v4

    #@1c
    const-string v1, "date_modified"

    #@1e
    aput-object v1, v0, v5

    #@20
    sput-object v0, Landroid/media/MediaScanner;->FILES_PRESCAN_PROJECTION:[Ljava/lang/String;

    #@22
    .line 140
    new-array v0, v3, [Ljava/lang/String;

    #@24
    const-string v1, "_id"

    #@26
    aput-object v1, v0, v2

    #@28
    sput-object v0, Landroid/media/MediaScanner;->ID_PROJECTION:[Ljava/lang/String;

    #@2a
    .line 149
    new-array v0, v3, [Ljava/lang/String;

    #@2c
    const-string/jumbo v1, "playlist_id"

    #@2f
    aput-object v1, v0, v2

    #@31
    sput-object v0, Landroid/media/MediaScanner;->PLAYLIST_MEMBERS_PROJECTION:[Ljava/lang/String;

    #@33
    .line 169
    const/16 v0, 0x94

    #@35
    new-array v0, v0, [Ljava/lang/String;

    #@37
    const-string v1, "Blues"

    #@39
    aput-object v1, v0, v2

    #@3b
    const-string v1, "Classic Rock"

    #@3d
    aput-object v1, v0, v3

    #@3f
    const-string v1, "Country"

    #@41
    aput-object v1, v0, v4

    #@43
    const-string v1, "Dance"

    #@45
    aput-object v1, v0, v5

    #@47
    const-string v1, "Disco"

    #@49
    aput-object v1, v0, v6

    #@4b
    const/4 v1, 0x5

    #@4c
    const-string v2, "Funk"

    #@4e
    aput-object v2, v0, v1

    #@50
    const/4 v1, 0x6

    #@51
    const-string v2, "Grunge"

    #@53
    aput-object v2, v0, v1

    #@55
    const/4 v1, 0x7

    #@56
    const-string v2, "Hip-Hop"

    #@58
    aput-object v2, v0, v1

    #@5a
    const/16 v1, 0x8

    #@5c
    const-string v2, "Jazz"

    #@5e
    aput-object v2, v0, v1

    #@60
    const/16 v1, 0x9

    #@62
    const-string v2, "Metal"

    #@64
    aput-object v2, v0, v1

    #@66
    const/16 v1, 0xa

    #@68
    const-string v2, "New Age"

    #@6a
    aput-object v2, v0, v1

    #@6c
    const/16 v1, 0xb

    #@6e
    const-string v2, "Oldies"

    #@70
    aput-object v2, v0, v1

    #@72
    const/16 v1, 0xc

    #@74
    const-string v2, "Other"

    #@76
    aput-object v2, v0, v1

    #@78
    const/16 v1, 0xd

    #@7a
    const-string v2, "Pop"

    #@7c
    aput-object v2, v0, v1

    #@7e
    const/16 v1, 0xe

    #@80
    const-string v2, "R&B"

    #@82
    aput-object v2, v0, v1

    #@84
    const/16 v1, 0xf

    #@86
    const-string v2, "Rap"

    #@88
    aput-object v2, v0, v1

    #@8a
    const/16 v1, 0x10

    #@8c
    const-string v2, "Reggae"

    #@8e
    aput-object v2, v0, v1

    #@90
    const/16 v1, 0x11

    #@92
    const-string v2, "Rock"

    #@94
    aput-object v2, v0, v1

    #@96
    const/16 v1, 0x12

    #@98
    const-string v2, "Techno"

    #@9a
    aput-object v2, v0, v1

    #@9c
    const/16 v1, 0x13

    #@9e
    const-string v2, "Industrial"

    #@a0
    aput-object v2, v0, v1

    #@a2
    const/16 v1, 0x14

    #@a4
    const-string v2, "Alternative"

    #@a6
    aput-object v2, v0, v1

    #@a8
    const/16 v1, 0x15

    #@aa
    const-string v2, "Ska"

    #@ac
    aput-object v2, v0, v1

    #@ae
    const/16 v1, 0x16

    #@b0
    const-string v2, "Death Metal"

    #@b2
    aput-object v2, v0, v1

    #@b4
    const/16 v1, 0x17

    #@b6
    const-string v2, "Pranks"

    #@b8
    aput-object v2, v0, v1

    #@ba
    const/16 v1, 0x18

    #@bc
    const-string v2, "Soundtrack"

    #@be
    aput-object v2, v0, v1

    #@c0
    const/16 v1, 0x19

    #@c2
    const-string v2, "Euro-Techno"

    #@c4
    aput-object v2, v0, v1

    #@c6
    const/16 v1, 0x1a

    #@c8
    const-string v2, "Ambient"

    #@ca
    aput-object v2, v0, v1

    #@cc
    const/16 v1, 0x1b

    #@ce
    const-string v2, "Trip-Hop"

    #@d0
    aput-object v2, v0, v1

    #@d2
    const/16 v1, 0x1c

    #@d4
    const-string v2, "Vocal"

    #@d6
    aput-object v2, v0, v1

    #@d8
    const/16 v1, 0x1d

    #@da
    const-string v2, "Jazz+Funk"

    #@dc
    aput-object v2, v0, v1

    #@de
    const/16 v1, 0x1e

    #@e0
    const-string v2, "Fusion"

    #@e2
    aput-object v2, v0, v1

    #@e4
    const/16 v1, 0x1f

    #@e6
    const-string v2, "Trance"

    #@e8
    aput-object v2, v0, v1

    #@ea
    const/16 v1, 0x20

    #@ec
    const-string v2, "Classical"

    #@ee
    aput-object v2, v0, v1

    #@f0
    const/16 v1, 0x21

    #@f2
    const-string v2, "Instrumental"

    #@f4
    aput-object v2, v0, v1

    #@f6
    const/16 v1, 0x22

    #@f8
    const-string v2, "Acid"

    #@fa
    aput-object v2, v0, v1

    #@fc
    const/16 v1, 0x23

    #@fe
    const-string v2, "House"

    #@100
    aput-object v2, v0, v1

    #@102
    const/16 v1, 0x24

    #@104
    const-string v2, "Game"

    #@106
    aput-object v2, v0, v1

    #@108
    const/16 v1, 0x25

    #@10a
    const-string v2, "Sound Clip"

    #@10c
    aput-object v2, v0, v1

    #@10e
    const/16 v1, 0x26

    #@110
    const-string v2, "Gospel"

    #@112
    aput-object v2, v0, v1

    #@114
    const/16 v1, 0x27

    #@116
    const-string v2, "Noise"

    #@118
    aput-object v2, v0, v1

    #@11a
    const/16 v1, 0x28

    #@11c
    const-string v2, "AlternRock"

    #@11e
    aput-object v2, v0, v1

    #@120
    const/16 v1, 0x29

    #@122
    const-string v2, "Bass"

    #@124
    aput-object v2, v0, v1

    #@126
    const/16 v1, 0x2a

    #@128
    const-string v2, "Soul"

    #@12a
    aput-object v2, v0, v1

    #@12c
    const/16 v1, 0x2b

    #@12e
    const-string v2, "Punk"

    #@130
    aput-object v2, v0, v1

    #@132
    const/16 v1, 0x2c

    #@134
    const-string v2, "Space"

    #@136
    aput-object v2, v0, v1

    #@138
    const/16 v1, 0x2d

    #@13a
    const-string v2, "Meditative"

    #@13c
    aput-object v2, v0, v1

    #@13e
    const/16 v1, 0x2e

    #@140
    const-string v2, "Instrumental Pop"

    #@142
    aput-object v2, v0, v1

    #@144
    const/16 v1, 0x2f

    #@146
    const-string v2, "Instrumental Rock"

    #@148
    aput-object v2, v0, v1

    #@14a
    const/16 v1, 0x30

    #@14c
    const-string v2, "Ethnic"

    #@14e
    aput-object v2, v0, v1

    #@150
    const/16 v1, 0x31

    #@152
    const-string v2, "Gothic"

    #@154
    aput-object v2, v0, v1

    #@156
    const/16 v1, 0x32

    #@158
    const-string v2, "Darkwave"

    #@15a
    aput-object v2, v0, v1

    #@15c
    const/16 v1, 0x33

    #@15e
    const-string v2, "Techno-Industrial"

    #@160
    aput-object v2, v0, v1

    #@162
    const/16 v1, 0x34

    #@164
    const-string v2, "Electronic"

    #@166
    aput-object v2, v0, v1

    #@168
    const/16 v1, 0x35

    #@16a
    const-string v2, "Pop-Folk"

    #@16c
    aput-object v2, v0, v1

    #@16e
    const/16 v1, 0x36

    #@170
    const-string v2, "Eurodance"

    #@172
    aput-object v2, v0, v1

    #@174
    const/16 v1, 0x37

    #@176
    const-string v2, "Dream"

    #@178
    aput-object v2, v0, v1

    #@17a
    const/16 v1, 0x38

    #@17c
    const-string v2, "Southern Rock"

    #@17e
    aput-object v2, v0, v1

    #@180
    const/16 v1, 0x39

    #@182
    const-string v2, "Comedy"

    #@184
    aput-object v2, v0, v1

    #@186
    const/16 v1, 0x3a

    #@188
    const-string v2, "Cult"

    #@18a
    aput-object v2, v0, v1

    #@18c
    const/16 v1, 0x3b

    #@18e
    const-string v2, "Gangsta"

    #@190
    aput-object v2, v0, v1

    #@192
    const/16 v1, 0x3c

    #@194
    const-string v2, "Top 40"

    #@196
    aput-object v2, v0, v1

    #@198
    const/16 v1, 0x3d

    #@19a
    const-string v2, "Christian Rap"

    #@19c
    aput-object v2, v0, v1

    #@19e
    const/16 v1, 0x3e

    #@1a0
    const-string v2, "Pop/Funk"

    #@1a2
    aput-object v2, v0, v1

    #@1a4
    const/16 v1, 0x3f

    #@1a6
    const-string v2, "Jungle"

    #@1a8
    aput-object v2, v0, v1

    #@1aa
    const/16 v1, 0x40

    #@1ac
    const-string v2, "Native American"

    #@1ae
    aput-object v2, v0, v1

    #@1b0
    const/16 v1, 0x41

    #@1b2
    const-string v2, "Cabaret"

    #@1b4
    aput-object v2, v0, v1

    #@1b6
    const/16 v1, 0x42

    #@1b8
    const-string v2, "New Wave"

    #@1ba
    aput-object v2, v0, v1

    #@1bc
    const/16 v1, 0x43

    #@1be
    const-string v2, "Psychadelic"

    #@1c0
    aput-object v2, v0, v1

    #@1c2
    const/16 v1, 0x44

    #@1c4
    const-string v2, "Rave"

    #@1c6
    aput-object v2, v0, v1

    #@1c8
    const/16 v1, 0x45

    #@1ca
    const-string v2, "Showtunes"

    #@1cc
    aput-object v2, v0, v1

    #@1ce
    const/16 v1, 0x46

    #@1d0
    const-string v2, "Trailer"

    #@1d2
    aput-object v2, v0, v1

    #@1d4
    const/16 v1, 0x47

    #@1d6
    const-string v2, "Lo-Fi"

    #@1d8
    aput-object v2, v0, v1

    #@1da
    const/16 v1, 0x48

    #@1dc
    const-string v2, "Tribal"

    #@1de
    aput-object v2, v0, v1

    #@1e0
    const/16 v1, 0x49

    #@1e2
    const-string v2, "Acid Punk"

    #@1e4
    aput-object v2, v0, v1

    #@1e6
    const/16 v1, 0x4a

    #@1e8
    const-string v2, "Acid Jazz"

    #@1ea
    aput-object v2, v0, v1

    #@1ec
    const/16 v1, 0x4b

    #@1ee
    const-string v2, "Polka"

    #@1f0
    aput-object v2, v0, v1

    #@1f2
    const/16 v1, 0x4c

    #@1f4
    const-string v2, "Retro"

    #@1f6
    aput-object v2, v0, v1

    #@1f8
    const/16 v1, 0x4d

    #@1fa
    const-string v2, "Musical"

    #@1fc
    aput-object v2, v0, v1

    #@1fe
    const/16 v1, 0x4e

    #@200
    const-string v2, "Rock & Roll"

    #@202
    aput-object v2, v0, v1

    #@204
    const/16 v1, 0x4f

    #@206
    const-string v2, "Hard Rock"

    #@208
    aput-object v2, v0, v1

    #@20a
    const/16 v1, 0x50

    #@20c
    const-string v2, "Folk"

    #@20e
    aput-object v2, v0, v1

    #@210
    const/16 v1, 0x51

    #@212
    const-string v2, "Folk-Rock"

    #@214
    aput-object v2, v0, v1

    #@216
    const/16 v1, 0x52

    #@218
    const-string v2, "National Folk"

    #@21a
    aput-object v2, v0, v1

    #@21c
    const/16 v1, 0x53

    #@21e
    const-string v2, "Swing"

    #@220
    aput-object v2, v0, v1

    #@222
    const/16 v1, 0x54

    #@224
    const-string v2, "Fast Fusion"

    #@226
    aput-object v2, v0, v1

    #@228
    const/16 v1, 0x55

    #@22a
    const-string v2, "Bebob"

    #@22c
    aput-object v2, v0, v1

    #@22e
    const/16 v1, 0x56

    #@230
    const-string v2, "Latin"

    #@232
    aput-object v2, v0, v1

    #@234
    const/16 v1, 0x57

    #@236
    const-string v2, "Revival"

    #@238
    aput-object v2, v0, v1

    #@23a
    const/16 v1, 0x58

    #@23c
    const-string v2, "Celtic"

    #@23e
    aput-object v2, v0, v1

    #@240
    const/16 v1, 0x59

    #@242
    const-string v2, "Bluegrass"

    #@244
    aput-object v2, v0, v1

    #@246
    const/16 v1, 0x5a

    #@248
    const-string v2, "Avantgarde"

    #@24a
    aput-object v2, v0, v1

    #@24c
    const/16 v1, 0x5b

    #@24e
    const-string v2, "Gothic Rock"

    #@250
    aput-object v2, v0, v1

    #@252
    const/16 v1, 0x5c

    #@254
    const-string v2, "Progressive Rock"

    #@256
    aput-object v2, v0, v1

    #@258
    const/16 v1, 0x5d

    #@25a
    const-string v2, "Psychedelic Rock"

    #@25c
    aput-object v2, v0, v1

    #@25e
    const/16 v1, 0x5e

    #@260
    const-string v2, "Symphonic Rock"

    #@262
    aput-object v2, v0, v1

    #@264
    const/16 v1, 0x5f

    #@266
    const-string v2, "Slow Rock"

    #@268
    aput-object v2, v0, v1

    #@26a
    const/16 v1, 0x60

    #@26c
    const-string v2, "Big Band"

    #@26e
    aput-object v2, v0, v1

    #@270
    const/16 v1, 0x61

    #@272
    const-string v2, "Chorus"

    #@274
    aput-object v2, v0, v1

    #@276
    const/16 v1, 0x62

    #@278
    const-string v2, "Easy Listening"

    #@27a
    aput-object v2, v0, v1

    #@27c
    const/16 v1, 0x63

    #@27e
    const-string v2, "Acoustic"

    #@280
    aput-object v2, v0, v1

    #@282
    const/16 v1, 0x64

    #@284
    const-string v2, "Humour"

    #@286
    aput-object v2, v0, v1

    #@288
    const/16 v1, 0x65

    #@28a
    const-string v2, "Speech"

    #@28c
    aput-object v2, v0, v1

    #@28e
    const/16 v1, 0x66

    #@290
    const-string v2, "Chanson"

    #@292
    aput-object v2, v0, v1

    #@294
    const/16 v1, 0x67

    #@296
    const-string v2, "Opera"

    #@298
    aput-object v2, v0, v1

    #@29a
    const/16 v1, 0x68

    #@29c
    const-string v2, "Chamber Music"

    #@29e
    aput-object v2, v0, v1

    #@2a0
    const/16 v1, 0x69

    #@2a2
    const-string v2, "Sonata"

    #@2a4
    aput-object v2, v0, v1

    #@2a6
    const/16 v1, 0x6a

    #@2a8
    const-string v2, "Symphony"

    #@2aa
    aput-object v2, v0, v1

    #@2ac
    const/16 v1, 0x6b

    #@2ae
    const-string v2, "Booty Bass"

    #@2b0
    aput-object v2, v0, v1

    #@2b2
    const/16 v1, 0x6c

    #@2b4
    const-string v2, "Primus"

    #@2b6
    aput-object v2, v0, v1

    #@2b8
    const/16 v1, 0x6d

    #@2ba
    const-string v2, "Porn Groove"

    #@2bc
    aput-object v2, v0, v1

    #@2be
    const/16 v1, 0x6e

    #@2c0
    const-string v2, "Satire"

    #@2c2
    aput-object v2, v0, v1

    #@2c4
    const/16 v1, 0x6f

    #@2c6
    const-string v2, "Slow Jam"

    #@2c8
    aput-object v2, v0, v1

    #@2ca
    const/16 v1, 0x70

    #@2cc
    const-string v2, "Club"

    #@2ce
    aput-object v2, v0, v1

    #@2d0
    const/16 v1, 0x71

    #@2d2
    const-string v2, "Tango"

    #@2d4
    aput-object v2, v0, v1

    #@2d6
    const/16 v1, 0x72

    #@2d8
    const-string v2, "Samba"

    #@2da
    aput-object v2, v0, v1

    #@2dc
    const/16 v1, 0x73

    #@2de
    const-string v2, "Folklore"

    #@2e0
    aput-object v2, v0, v1

    #@2e2
    const/16 v1, 0x74

    #@2e4
    const-string v2, "Ballad"

    #@2e6
    aput-object v2, v0, v1

    #@2e8
    const/16 v1, 0x75

    #@2ea
    const-string v2, "Power Ballad"

    #@2ec
    aput-object v2, v0, v1

    #@2ee
    const/16 v1, 0x76

    #@2f0
    const-string v2, "Rhythmic Soul"

    #@2f2
    aput-object v2, v0, v1

    #@2f4
    const/16 v1, 0x77

    #@2f6
    const-string v2, "Freestyle"

    #@2f8
    aput-object v2, v0, v1

    #@2fa
    const/16 v1, 0x78

    #@2fc
    const-string v2, "Duet"

    #@2fe
    aput-object v2, v0, v1

    #@300
    const/16 v1, 0x79

    #@302
    const-string v2, "Punk Rock"

    #@304
    aput-object v2, v0, v1

    #@306
    const/16 v1, 0x7a

    #@308
    const-string v2, "Drum Solo"

    #@30a
    aput-object v2, v0, v1

    #@30c
    const/16 v1, 0x7b

    #@30e
    const-string v2, "A capella"

    #@310
    aput-object v2, v0, v1

    #@312
    const/16 v1, 0x7c

    #@314
    const-string v2, "Euro-House"

    #@316
    aput-object v2, v0, v1

    #@318
    const/16 v1, 0x7d

    #@31a
    const-string v2, "Dance Hall"

    #@31c
    aput-object v2, v0, v1

    #@31e
    const/16 v1, 0x7e

    #@320
    const-string v2, "Goa"

    #@322
    aput-object v2, v0, v1

    #@324
    const/16 v1, 0x7f

    #@326
    const-string v2, "Drum & Bass"

    #@328
    aput-object v2, v0, v1

    #@32a
    const/16 v1, 0x80

    #@32c
    const-string v2, "Club-House"

    #@32e
    aput-object v2, v0, v1

    #@330
    const/16 v1, 0x81

    #@332
    const-string v2, "Hardcore"

    #@334
    aput-object v2, v0, v1

    #@336
    const/16 v1, 0x82

    #@338
    const-string v2, "Terror"

    #@33a
    aput-object v2, v0, v1

    #@33c
    const/16 v1, 0x83

    #@33e
    const-string v2, "Indie"

    #@340
    aput-object v2, v0, v1

    #@342
    const/16 v1, 0x84

    #@344
    const-string v2, "Britpop"

    #@346
    aput-object v2, v0, v1

    #@348
    const/16 v1, 0x85

    #@34a
    const-string v2, "Negerpunk"

    #@34c
    aput-object v2, v0, v1

    #@34e
    const/16 v1, 0x86

    #@350
    const-string v2, "Polsk Punk"

    #@352
    aput-object v2, v0, v1

    #@354
    const/16 v1, 0x87

    #@356
    const-string v2, "Beat"

    #@358
    aput-object v2, v0, v1

    #@35a
    const/16 v1, 0x88

    #@35c
    const-string v2, "Christian Gangsta"

    #@35e
    aput-object v2, v0, v1

    #@360
    const/16 v1, 0x89

    #@362
    const-string v2, "Heavy Metal"

    #@364
    aput-object v2, v0, v1

    #@366
    const/16 v1, 0x8a

    #@368
    const-string v2, "Black Metal"

    #@36a
    aput-object v2, v0, v1

    #@36c
    const/16 v1, 0x8b

    #@36e
    const-string v2, "Crossover"

    #@370
    aput-object v2, v0, v1

    #@372
    const/16 v1, 0x8c

    #@374
    const-string v2, "Contemporary Christian"

    #@376
    aput-object v2, v0, v1

    #@378
    const/16 v1, 0x8d

    #@37a
    const-string v2, "Christian Rock"

    #@37c
    aput-object v2, v0, v1

    #@37e
    const/16 v1, 0x8e

    #@380
    const-string v2, "Merengue"

    #@382
    aput-object v2, v0, v1

    #@384
    const/16 v1, 0x8f

    #@386
    const-string v2, "Salsa"

    #@388
    aput-object v2, v0, v1

    #@38a
    const/16 v1, 0x90

    #@38c
    const-string v2, "Thrash Metal"

    #@38e
    aput-object v2, v0, v1

    #@390
    const/16 v1, 0x91

    #@392
    const-string v2, "Anime"

    #@394
    aput-object v2, v0, v1

    #@396
    const/16 v1, 0x92

    #@398
    const-string v2, "JPop"

    #@39a
    aput-object v2, v0, v1

    #@39c
    const/16 v1, 0x93

    #@39e
    const-string v2, "Synthpop"

    #@3a0
    aput-object v2, v0, v1

    #@3a2
    sput-object v0, Landroid/media/MediaScanner;->ID3_GENRES:[Ljava/lang/String;

    #@3a4
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "c"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 414
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 346
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Landroid/media/MediaScanner;->mWasEmptyPriorToScan:Z

    #@8
    .line 370
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    #@a
    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    #@d
    iput-object v0, p0, Landroid/media/MediaScanner;->mBitmapOptions:Landroid/graphics/BitmapFactory$Options;

    #@f
    .line 403
    new-instance v0, Ljava/util/ArrayList;

    #@11
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@14
    iput-object v0, p0, Landroid/media/MediaScanner;->mPlaylistEntries:Ljava/util/ArrayList;

    #@16
    .line 412
    iput-object v2, p0, Landroid/media/MediaScanner;->mDrmManagerClient:Landroid/drm/DrmManagerClient;

    #@18
    .line 436
    new-instance v0, Landroid/media/MediaScanner$MyMediaScannerClient;

    #@1a
    invoke-direct {v0, p0, v2}, Landroid/media/MediaScanner$MyMediaScannerClient;-><init>(Landroid/media/MediaScanner;Landroid/media/MediaScanner$1;)V

    #@1d
    iput-object v0, p0, Landroid/media/MediaScanner;->mClient:Landroid/media/MediaScanner$MyMediaScannerClient;

    #@1f
    .line 415
    invoke-direct {p0}, Landroid/media/MediaScanner;->native_setup()V

    #@22
    .line 416
    iput-object p1, p0, Landroid/media/MediaScanner;->mContext:Landroid/content/Context;

    #@24
    .line 417
    iget-object v0, p0, Landroid/media/MediaScanner;->mBitmapOptions:Landroid/graphics/BitmapFactory$Options;

    #@26
    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    #@28
    .line 418
    iget-object v0, p0, Landroid/media/MediaScanner;->mBitmapOptions:Landroid/graphics/BitmapFactory$Options;

    #@2a
    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    #@2c
    .line 420
    invoke-direct {p0}, Landroid/media/MediaScanner;->setDefaultRingtoneFileNames()V

    #@2f
    .line 422
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    #@32
    move-result-object v0

    #@33
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@36
    move-result-object v0

    #@37
    iput-object v0, p0, Landroid/media/MediaScanner;->mExternalStoragePath:Ljava/lang/String;

    #@39
    .line 423
    invoke-static {}, Landroid/os/Environment;->isExternalStorageEmulated()Z

    #@3c
    move-result v0

    #@3d
    iput-boolean v0, p0, Landroid/media/MediaScanner;->mExternalIsEmulated:Z

    #@3f
    .line 425
    return-void
.end method

.method static synthetic access$100(Ljava/lang/String;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 124
    invoke-static {p0}, Landroid/media/MediaScanner;->isNoMediaFile(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1000(Landroid/media/MediaScanner;Ljava/lang/String;Ljava/lang/String;Landroid/media/MediaScannerClient;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 124
    invoke-direct {p0, p1, p2, p3}, Landroid/media/MediaScanner;->processFile(Ljava/lang/String;Ljava/lang/String;Landroid/media/MediaScannerClient;)V

    #@3
    return-void
.end method

.method static synthetic access$1100(Landroid/media/MediaScanner;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 124
    iget-boolean v0, p0, Landroid/media/MediaScanner;->mProcessGenres:Z

    #@2
    return v0
.end method

.method static synthetic access$1200()[Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 124
    sget-object v0, Landroid/media/MediaScanner;->ID3_GENRES:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Landroid/media/MediaScanner;)Landroid/graphics/BitmapFactory$Options;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 124
    iget-object v0, p0, Landroid/media/MediaScanner;->mBitmapOptions:Landroid/graphics/BitmapFactory$Options;

    #@2
    return-object v0
.end method

.method static synthetic access$1400(Landroid/media/MediaScanner;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 124
    iget-object v0, p0, Landroid/media/MediaScanner;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1500(Landroid/media/MediaScanner;)Landroid/net/Uri;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 124
    iget-object v0, p0, Landroid/media/MediaScanner;->mFilesUri:Landroid/net/Uri;

    #@2
    return-object v0
.end method

.method static synthetic access$1600(Landroid/media/MediaScanner;)Landroid/media/MediaInserter;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 124
    iget-object v0, p0, Landroid/media/MediaScanner;->mMediaInserter:Landroid/media/MediaInserter;

    #@2
    return-object v0
.end method

.method static synthetic access$1700(Landroid/media/MediaScanner;)Landroid/net/Uri;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 124
    iget-object v0, p0, Landroid/media/MediaScanner;->mVideoUri:Landroid/net/Uri;

    #@2
    return-object v0
.end method

.method static synthetic access$1800(Landroid/media/MediaScanner;)Landroid/net/Uri;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 124
    iget-object v0, p0, Landroid/media/MediaScanner;->mImagesUri:Landroid/net/Uri;

    #@2
    return-object v0
.end method

.method static synthetic access$1900(Landroid/media/MediaScanner;)Landroid/net/Uri;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 124
    iget-object v0, p0, Landroid/media/MediaScanner;->mAudioUri:Landroid/net/Uri;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/media/MediaScanner;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 124
    invoke-direct {p0}, Landroid/media/MediaScanner;->isDrmEnabled()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2000(Landroid/media/MediaScanner;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 124
    iget-boolean v0, p0, Landroid/media/MediaScanner;->mWasEmptyPriorToScan:Z

    #@2
    return v0
.end method

.method static synthetic access$2100(Landroid/media/MediaScanner;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 124
    iget-boolean v0, p0, Landroid/media/MediaScanner;->mDefaultNotificationSet:Z

    #@2
    return v0
.end method

.method static synthetic access$2102(Landroid/media/MediaScanner;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 124
    iput-boolean p1, p0, Landroid/media/MediaScanner;->mDefaultNotificationSet:Z

    #@2
    return p1
.end method

.method static synthetic access$2200(Landroid/media/MediaScanner;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 124
    iget-object v0, p0, Landroid/media/MediaScanner;->mDefaultNotificationFilename:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$2300(Landroid/media/MediaScanner;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 124
    iget-boolean v0, p0, Landroid/media/MediaScanner;->mDefaultRingtoneSet:Z

    #@2
    return v0
.end method

.method static synthetic access$2302(Landroid/media/MediaScanner;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 124
    iput-boolean p1, p0, Landroid/media/MediaScanner;->mDefaultRingtoneSet:Z

    #@2
    return p1
.end method

.method static synthetic access$2400(Landroid/media/MediaScanner;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 124
    iget-object v0, p0, Landroid/media/MediaScanner;->mDefaultRingtoneFilename:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$2500(Landroid/media/MediaScanner;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 124
    iget-boolean v0, p0, Landroid/media/MediaScanner;->mDefaultAlarmSet:Z

    #@2
    return v0
.end method

.method static synthetic access$2502(Landroid/media/MediaScanner;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 124
    iput-boolean p1, p0, Landroid/media/MediaScanner;->mDefaultAlarmSet:Z

    #@2
    return p1
.end method

.method static synthetic access$2600(Landroid/media/MediaScanner;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 124
    iget-object v0, p0, Landroid/media/MediaScanner;->mDefaultAlarmAlertFilename:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$2700(Landroid/media/MediaScanner;)Landroid/content/IContentProvider;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 124
    iget-object v0, p0, Landroid/media/MediaScanner;->mMediaProvider:Landroid/content/IContentProvider;

    #@2
    return-object v0
.end method

.method static synthetic access$2800(Landroid/media/MediaScanner;)Landroid/drm/DrmManagerClient;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 124
    iget-object v0, p0, Landroid/media/MediaScanner;->mDrmManagerClient:Landroid/drm/DrmManagerClient;

    #@2
    return-object v0
.end method

.method static synthetic access$2802(Landroid/media/MediaScanner;Landroid/drm/DrmManagerClient;)Landroid/drm/DrmManagerClient;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 124
    iput-object p1, p0, Landroid/media/MediaScanner;->mDrmManagerClient:Landroid/drm/DrmManagerClient;

    #@2
    return-object p1
.end method

.method static synthetic access$300(Landroid/media/MediaScanner;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 124
    iget-boolean v0, p0, Landroid/media/MediaScanner;->mCaseInsensitivePaths:Z

    #@2
    return v0
.end method

.method static synthetic access$3000(Landroid/media/MediaScanner;Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 124
    invoke-direct {p0, p1, p2}, Landroid/media/MediaScanner;->cachePlaylistEntry(Ljava/lang/String;Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$400(Landroid/media/MediaScanner;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 124
    iget-object v0, p0, Landroid/media/MediaScanner;->mFileCache:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Landroid/media/MediaScanner;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 124
    iget-boolean v0, p0, Landroid/media/MediaScanner;->mProcessPlaylists:Z

    #@2
    return v0
.end method

.method static synthetic access$600(Landroid/media/MediaScanner;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 124
    iget-object v0, p0, Landroid/media/MediaScanner;->mPlayLists:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Landroid/media/MediaScanner;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 124
    iget v0, p0, Landroid/media/MediaScanner;->mMtpObjectHandle:I

    #@2
    return v0
.end method

.method static synthetic access$800(Landroid/media/MediaScanner;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 124
    iget-boolean v0, p0, Landroid/media/MediaScanner;->mExternalIsEmulated:Z

    #@2
    return v0
.end method

.method static synthetic access$900(Landroid/media/MediaScanner;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 124
    iget-object v0, p0, Landroid/media/MediaScanner;->mExternalStoragePath:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method private cachePlaylistEntry(Ljava/lang/String;Ljava/lang/String;)V
    .registers 10
    .parameter "line"
    .parameter "playListDirectory"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 2017
    new-instance v1, Landroid/media/MediaScanner$PlaylistEntry;

    #@4
    const/4 v5, 0x0

    #@5
    invoke-direct {v1, v5}, Landroid/media/MediaScanner$PlaylistEntry;-><init>(Landroid/media/MediaScanner$1;)V

    #@8
    .line 2019
    .local v1, entry:Landroid/media/MediaScanner$PlaylistEntry;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@b
    move-result v2

    #@c
    .line 2020
    .local v2, entryLength:I
    :goto_c
    if-lez v2, :cond_1d

    #@e
    add-int/lit8 v5, v2, -0x1

    #@10
    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    #@13
    move-result v5

    #@14
    invoke-static {v5}, Ljava/lang/Character;->isWhitespace(C)Z

    #@17
    move-result v5

    #@18
    if-eqz v5, :cond_1d

    #@1a
    add-int/lit8 v2, v2, -0x1

    #@1c
    goto :goto_c

    #@1d
    .line 2023
    :cond_1d
    const/4 v5, 0x3

    #@1e
    if-ge v2, v5, :cond_21

    #@20
    .line 2038
    :goto_20
    return-void

    #@21
    .line 2024
    :cond_21
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@24
    move-result v5

    #@25
    if-ge v2, v5, :cond_2b

    #@27
    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2a
    move-result-object p1

    #@2b
    .line 2028
    :cond_2b
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    #@2e
    move-result v0

    #@2f
    .line 2029
    .local v0, ch1:C
    const/16 v5, 0x2f

    #@31
    if-eq v0, v5, :cond_4a

    #@33
    invoke-static {v0}, Ljava/lang/Character;->isLetter(C)Z

    #@36
    move-result v5

    #@37
    if-eqz v5, :cond_4b

    #@39
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    #@3c
    move-result v5

    #@3d
    const/16 v6, 0x3a

    #@3f
    if-ne v5, v6, :cond_4b

    #@41
    const/4 v5, 0x2

    #@42
    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    #@45
    move-result v5

    #@46
    const/16 v6, 0x5c

    #@48
    if-ne v5, v6, :cond_4b

    #@4a
    :cond_4a
    move v3, v4

    #@4b
    .line 2032
    .local v3, fullPath:Z
    :cond_4b
    if-nez v3, :cond_5e

    #@4d
    .line 2033
    new-instance v4, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v4

    #@56
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v4

    #@5a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object p1

    #@5e
    .line 2034
    :cond_5e
    iput-object p1, v1, Landroid/media/MediaScanner$PlaylistEntry;->path:Ljava/lang/String;

    #@60
    .line 2037
    iget-object v4, p0, Landroid/media/MediaScanner;->mPlaylistEntries:Ljava/util/ArrayList;

    #@62
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@65
    goto :goto_20
.end method

.method private inScanDirectory(Ljava/lang/String;[Ljava/lang/String;)Z
    .registers 6
    .parameter "path"
    .parameter "directories"

    #@0
    .prologue
    .line 1533
    const/4 v1, 0x0

    #@1
    .local v1, i:I
    :goto_1
    array-length v2, p2

    #@2
    if-ge v1, v2, :cond_11

    #@4
    .line 1534
    aget-object v0, p2, v1

    #@6
    .line 1535
    .local v0, directory:Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_e

    #@c
    .line 1536
    const/4 v2, 0x1

    #@d
    .line 1539
    .end local v0           #directory:Ljava/lang/String;
    :goto_d
    return v2

    #@e
    .line 1533
    .restart local v0       #directory:Ljava/lang/String;
    :cond_e
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_1

    #@11
    .line 1539
    .end local v0           #directory:Ljava/lang/String;
    :cond_11
    const/4 v2, 0x0

    #@12
    goto :goto_d
.end method

.method private initialize(Ljava/lang/String;)V
    .registers 5
    .parameter "volumeName"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 1645
    iget-object v0, p0, Landroid/media/MediaScanner;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v0

    #@7
    const-string/jumbo v1, "media"

    #@a
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->acquireProvider(Ljava/lang/String;)Landroid/content/IContentProvider;

    #@d
    move-result-object v0

    #@e
    iput-object v0, p0, Landroid/media/MediaScanner;->mMediaProvider:Landroid/content/IContentProvider;

    #@10
    .line 1647
    invoke-static {p1}, Landroid/provider/MediaStore$Audio$Media;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    #@13
    move-result-object v0

    #@14
    iput-object v0, p0, Landroid/media/MediaScanner;->mAudioUri:Landroid/net/Uri;

    #@16
    .line 1648
    invoke-static {p1}, Landroid/provider/MediaStore$Video$Media;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    #@19
    move-result-object v0

    #@1a
    iput-object v0, p0, Landroid/media/MediaScanner;->mVideoUri:Landroid/net/Uri;

    #@1c
    .line 1649
    invoke-static {p1}, Landroid/provider/MediaStore$Images$Media;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    #@1f
    move-result-object v0

    #@20
    iput-object v0, p0, Landroid/media/MediaScanner;->mImagesUri:Landroid/net/Uri;

    #@22
    .line 1650
    invoke-static {p1}, Landroid/provider/MediaStore$Images$Thumbnails;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    #@25
    move-result-object v0

    #@26
    iput-object v0, p0, Landroid/media/MediaScanner;->mThumbsUri:Landroid/net/Uri;

    #@28
    .line 1651
    invoke-static {p1}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    #@2b
    move-result-object v0

    #@2c
    iput-object v0, p0, Landroid/media/MediaScanner;->mFilesUri:Landroid/net/Uri;

    #@2e
    .line 1653
    const-string v0, "internal"

    #@30
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v0

    #@34
    if-nez v0, :cond_42

    #@36
    .line 1655
    iput-boolean v2, p0, Landroid/media/MediaScanner;->mProcessPlaylists:Z

    #@38
    .line 1656
    iput-boolean v2, p0, Landroid/media/MediaScanner;->mProcessGenres:Z

    #@3a
    .line 1657
    invoke-static {p1}, Landroid/provider/MediaStore$Audio$Playlists;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    #@3d
    move-result-object v0

    #@3e
    iput-object v0, p0, Landroid/media/MediaScanner;->mPlaylistsUri:Landroid/net/Uri;

    #@40
    .line 1659
    iput-boolean v2, p0, Landroid/media/MediaScanner;->mCaseInsensitivePaths:Z

    #@42
    .line 1661
    :cond_42
    return-void
.end method

.method private isDrmEnabled()Z
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 440
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_DRM:Z

    #@3
    if-eqz v2, :cond_6

    #@5
    .line 445
    :cond_5
    :goto_5
    return v1

    #@6
    .line 444
    :cond_6
    const-string v2, "drm.service.enabled"

    #@8
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    .line 445
    .local v0, prop:Ljava/lang/String;
    if-eqz v0, :cond_17

    #@e
    const-string/jumbo v2, "true"

    #@11
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v2

    #@15
    if-nez v2, :cond_5

    #@17
    :cond_17
    const/4 v1, 0x0

    #@18
    goto :goto_5
.end method

.method private static isNoMediaFile(Ljava/lang/String;)Z
    .registers 11
    .parameter "path"

    #@0
    .prologue
    const/16 v9, 0xa

    #@2
    const/4 v1, 0x1

    #@3
    const/4 v4, 0x0

    #@4
    .line 1798
    new-instance v6, Ljava/io/File;

    #@6
    invoke-direct {v6, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@9
    .line 1799
    .local v6, file:Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->isDirectory()Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_11

    #@f
    move v1, v4

    #@10
    .line 1828
    :cond_10
    :goto_10
    return v1

    #@11
    .line 1804
    :cond_11
    const/16 v0, 0x2f

    #@13
    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(I)I

    #@16
    move-result v7

    #@17
    .line 1805
    .local v7, lastSlash:I
    if-ltz v7, :cond_7b

    #@19
    add-int/lit8 v0, v7, 0x2

    #@1b
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@1e
    move-result v2

    #@1f
    if-ge v0, v2, :cond_7b

    #@21
    .line 1807
    add-int/lit8 v0, v7, 0x1

    #@23
    const-string v2, "._"

    #@25
    const/4 v3, 0x2

    #@26
    invoke-virtual {p0, v0, v2, v4, v3}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    #@29
    move-result v0

    #@2a
    if-nez v0, :cond_10

    #@2c
    .line 1814
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@2f
    move-result v0

    #@30
    add-int/lit8 v2, v0, -0x4

    #@32
    const-string v3, ".jpg"

    #@34
    const/4 v5, 0x4

    #@35
    move-object v0, p0

    #@36
    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    #@39
    move-result v0

    #@3a
    if-eqz v0, :cond_7b

    #@3c
    .line 1815
    add-int/lit8 v2, v7, 0x1

    #@3e
    const-string v3, "AlbumArt_{"

    #@40
    move-object v0, p0

    #@41
    move v5, v9

    #@42
    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    #@45
    move-result v0

    #@46
    if-nez v0, :cond_10

    #@48
    add-int/lit8 v2, v7, 0x1

    #@4a
    const-string v3, "AlbumArt."

    #@4c
    const/16 v5, 0x9

    #@4e
    move-object v0, p0

    #@4f
    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    #@52
    move-result v0

    #@53
    if-nez v0, :cond_10

    #@55
    .line 1819
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@58
    move-result v0

    #@59
    sub-int/2addr v0, v7

    #@5a
    add-int/lit8 v8, v0, -0x1

    #@5c
    .line 1820
    .local v8, length:I
    const/16 v0, 0x11

    #@5e
    if-ne v8, v0, :cond_6d

    #@60
    add-int/lit8 v2, v7, 0x1

    #@62
    const-string v3, "AlbumArtSmall"

    #@64
    const/16 v5, 0xd

    #@66
    move-object v0, p0

    #@67
    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    #@6a
    move-result v0

    #@6b
    if-nez v0, :cond_10

    #@6d
    :cond_6d
    if-ne v8, v9, :cond_7b

    #@6f
    add-int/lit8 v2, v7, 0x1

    #@71
    const-string v3, "Folder"

    #@73
    const/4 v5, 0x6

    #@74
    move-object v0, p0

    #@75
    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    #@78
    move-result v0

    #@79
    if-nez v0, :cond_10

    #@7b
    .end local v8           #length:I
    :cond_7b
    move v1, v4

    #@7c
    .line 1828
    goto :goto_10
.end method

.method public static isNoMediaPath(Ljava/lang/String;)Z
    .registers 8
    .parameter "path"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 1832
    if-nez p0, :cond_5

    #@4
    .line 1852
    :goto_4
    return v3

    #@5
    .line 1835
    :cond_5
    const-string v5, "/."

    #@7
    invoke-virtual {p0, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@a
    move-result v5

    #@b
    if-ltz v5, :cond_f

    #@d
    move v3, v4

    #@e
    goto :goto_4

    #@f
    .line 1839
    :cond_f
    const/4 v1, 0x1

    #@10
    .line 1840
    .local v1, offset:I
    :goto_10
    if-ltz v1, :cond_42

    #@12
    .line 1841
    const/16 v5, 0x2f

    #@14
    invoke-virtual {p0, v5, v1}, Ljava/lang/String;->indexOf(II)I

    #@17
    move-result v2

    #@18
    .line 1842
    .local v2, slashIndex:I
    if-le v2, v1, :cond_40

    #@1a
    .line 1843
    add-int/lit8 v2, v2, 0x1

    #@1c
    .line 1844
    new-instance v0, Ljava/io/File;

    #@1e
    new-instance v5, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@26
    move-result-object v6

    #@27
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v5

    #@2b
    const-string v6, ".nomedia"

    #@2d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v5

    #@31
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v5

    #@35
    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@38
    .line 1845
    .local v0, file:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@3b
    move-result v5

    #@3c
    if-eqz v5, :cond_40

    #@3e
    move v3, v4

    #@3f
    .line 1847
    goto :goto_4

    #@40
    .line 1850
    .end local v0           #file:Ljava/io/File;
    :cond_40
    move v1, v2

    #@41
    .line 1851
    goto :goto_10

    #@42
    .line 1852
    .end local v2           #slashIndex:I
    :cond_42
    invoke-static {p0}, Landroid/media/MediaScanner;->isNoMediaFile(Ljava/lang/String;)Z

    #@45
    move-result v3

    #@46
    goto :goto_4
.end method

.method private matchEntries(JLjava/lang/String;)Z
    .registers 11
    .parameter "rowId"
    .parameter "data"

    #@0
    .prologue
    const v6, 0x7fffffff

    #@3
    .line 1993
    iget-object v5, p0, Landroid/media/MediaScanner;->mPlaylistEntries:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v3

    #@9
    .line 1994
    .local v3, len:I
    const/4 v0, 0x1

    #@a
    .line 1995
    .local v0, done:Z
    const/4 v2, 0x0

    #@b
    .local v2, i:I
    :goto_b
    if-ge v2, v3, :cond_39

    #@d
    .line 1996
    iget-object v5, p0, Landroid/media/MediaScanner;->mPlaylistEntries:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v1

    #@13
    check-cast v1, Landroid/media/MediaScanner$PlaylistEntry;

    #@15
    .line 1997
    .local v1, entry:Landroid/media/MediaScanner$PlaylistEntry;
    iget v5, v1, Landroid/media/MediaScanner$PlaylistEntry;->bestmatchlevel:I

    #@17
    if-ne v5, v6, :cond_1c

    #@19
    .line 1995
    :cond_19
    :goto_19
    add-int/lit8 v2, v2, 0x1

    #@1b
    goto :goto_b

    #@1c
    .line 2000
    :cond_1c
    const/4 v0, 0x0

    #@1d
    .line 2001
    iget-object v5, v1, Landroid/media/MediaScanner$PlaylistEntry;->path:Ljava/lang/String;

    #@1f
    invoke-virtual {p3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@22
    move-result v5

    #@23
    if-eqz v5, :cond_2a

    #@25
    .line 2002
    iput-wide p1, v1, Landroid/media/MediaScanner$PlaylistEntry;->bestmatchid:J

    #@27
    .line 2003
    iput v6, v1, Landroid/media/MediaScanner$PlaylistEntry;->bestmatchlevel:I

    #@29
    goto :goto_19

    #@2a
    .line 2007
    :cond_2a
    iget-object v5, v1, Landroid/media/MediaScanner$PlaylistEntry;->path:Ljava/lang/String;

    #@2c
    invoke-direct {p0, p3, v5}, Landroid/media/MediaScanner;->matchPaths(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    move-result v4

    #@30
    .line 2008
    .local v4, matchLength:I
    iget v5, v1, Landroid/media/MediaScanner$PlaylistEntry;->bestmatchlevel:I

    #@32
    if-le v4, v5, :cond_19

    #@34
    .line 2009
    iput-wide p1, v1, Landroid/media/MediaScanner$PlaylistEntry;->bestmatchid:J

    #@36
    .line 2010
    iput v4, v1, Landroid/media/MediaScanner$PlaylistEntry;->bestmatchlevel:I

    #@38
    goto :goto_19

    #@39
    .line 2013
    .end local v1           #entry:Landroid/media/MediaScanner$PlaylistEntry;
    .end local v4           #matchLength:I
    :cond_39
    return v0
.end method

.method private matchPaths(Ljava/lang/String;Ljava/lang/String;)I
    .registers 16
    .parameter "path1"
    .parameter "path2"

    #@0
    .prologue
    .line 1966
    const/4 v10, 0x0

    #@1
    .line 1967
    .local v10, result:I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@4
    move-result v8

    #@5
    .line 1968
    .local v8, end1:I
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    #@8
    move-result v9

    #@9
    .line 1970
    .local v9, end2:I
    :goto_9
    if-lez v8, :cond_3f

    #@b
    if-lez v9, :cond_3f

    #@d
    .line 1971
    const/16 v0, 0x2f

    #@f
    add-int/lit8 v1, v8, -0x1

    #@11
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->lastIndexOf(II)I

    #@14
    move-result v11

    #@15
    .line 1972
    .local v11, slash1:I
    const/16 v0, 0x2f

    #@17
    add-int/lit8 v1, v9, -0x1

    #@19
    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->lastIndexOf(II)I

    #@1c
    move-result v12

    #@1d
    .line 1973
    .local v12, slash2:I
    const/16 v0, 0x5c

    #@1f
    add-int/lit8 v1, v8, -0x1

    #@21
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->lastIndexOf(II)I

    #@24
    move-result v6

    #@25
    .line 1974
    .local v6, backSlash1:I
    const/16 v0, 0x5c

    #@27
    add-int/lit8 v1, v9, -0x1

    #@29
    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->lastIndexOf(II)I

    #@2c
    move-result v7

    #@2d
    .line 1975
    .local v7, backSlash2:I
    if-le v11, v6, :cond_40

    #@2f
    move v2, v11

    #@30
    .line 1976
    .local v2, start1:I
    :goto_30
    if-le v12, v7, :cond_42

    #@32
    move v4, v12

    #@33
    .line 1977
    .local v4, start2:I
    :goto_33
    if-gez v2, :cond_44

    #@35
    const/4 v2, 0x0

    #@36
    .line 1978
    :goto_36
    if-gez v4, :cond_47

    #@38
    const/4 v4, 0x0

    #@39
    .line 1979
    :goto_39
    sub-int v5, v8, v2

    #@3b
    .line 1980
    .local v5, length:I
    sub-int v0, v9, v4

    #@3d
    if-eq v0, v5, :cond_4a

    #@3f
    .line 1988
    .end local v2           #start1:I
    .end local v4           #start2:I
    .end local v5           #length:I
    .end local v6           #backSlash1:I
    .end local v7           #backSlash2:I
    .end local v11           #slash1:I
    .end local v12           #slash2:I
    :cond_3f
    return v10

    #@40
    .restart local v6       #backSlash1:I
    .restart local v7       #backSlash2:I
    .restart local v11       #slash1:I
    .restart local v12       #slash2:I
    :cond_40
    move v2, v6

    #@41
    .line 1975
    goto :goto_30

    #@42
    .restart local v2       #start1:I
    :cond_42
    move v4, v7

    #@43
    .line 1976
    goto :goto_33

    #@44
    .line 1977
    .restart local v4       #start2:I
    :cond_44
    add-int/lit8 v2, v2, 0x1

    #@46
    goto :goto_36

    #@47
    .line 1978
    :cond_47
    add-int/lit8 v4, v4, 0x1

    #@49
    goto :goto_39

    #@4a
    .line 1981
    .restart local v5       #length:I
    :cond_4a
    const/4 v1, 0x1

    #@4b
    move-object v0, p1

    #@4c
    move-object v3, p2

    #@4d
    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    #@50
    move-result v0

    #@51
    if-eqz v0, :cond_3f

    #@53
    .line 1982
    add-int/lit8 v10, v10, 0x1

    #@55
    .line 1983
    add-int/lit8 v8, v2, -0x1

    #@57
    .line 1984
    add-int/lit8 v9, v4, -0x1

    #@59
    .line 1986
    goto :goto_9
.end method

.method private final native native_finalize()V
.end method

.method private static final native native_init()V
.end method

.method private final native native_setup()V
.end method

.method private postscan([Ljava/lang/String;)V
    .registers 9
    .parameter "directories"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 1625
    iget-boolean v0, p0, Landroid/media/MediaScanner;->mProcessPlaylists:Z

    #@3
    if-eqz v0, :cond_8

    #@5
    .line 1626
    invoke-direct {p0}, Landroid/media/MediaScanner;->processPlayLists()V

    #@8
    .line 1629
    :cond_8
    iget v0, p0, Landroid/media/MediaScanner;->mOriginalCount:I

    #@a
    if-nez v0, :cond_1d

    #@c
    iget-object v0, p0, Landroid/media/MediaScanner;->mImagesUri:Landroid/net/Uri;

    #@e
    const-string v1, "external"

    #@10
    invoke-static {v1}, Landroid/provider/MediaStore$Images$Media;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_1d

    #@1a
    .line 1630
    invoke-direct {p0}, Landroid/media/MediaScanner;->pruneDeadThumbnailFiles()V

    #@1d
    .line 1633
    :cond_1d
    iget-object v0, p0, Landroid/media/MediaScanner;->mMediaProvider:Landroid/content/IContentProvider;

    #@1f
    iget-object v1, p0, Landroid/media/MediaScanner;->mFilesUri:Landroid/net/Uri;

    #@21
    const-string v2, "_data=?"

    #@23
    const/4 v3, 0x1

    #@24
    new-array v3, v3, [Ljava/lang/String;

    #@26
    const/4 v4, 0x0

    #@27
    const-string v5, ""

    #@29
    aput-object v5, v3, v4

    #@2b
    invoke-interface {v0, v1, v2, v3}, Landroid/content/IContentProvider;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@2e
    .line 1637
    iput-object v6, p0, Landroid/media/MediaScanner;->mPlayLists:Ljava/util/ArrayList;

    #@30
    .line 1639
    iput-object v6, p0, Landroid/media/MediaScanner;->mFileCache:Ljava/util/HashMap;

    #@32
    .line 1641
    iput-object v6, p0, Landroid/media/MediaScanner;->mMediaProvider:Landroid/content/IContentProvider;

    #@34
    .line 1642
    return-void
.end method

.method private prescan(Ljava/lang/String;Z)V
    .registers 40
    .parameter "filePath"
    .parameter "prescanFiles"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1375
    const/16 v22, 0x0

    #@2
    .line 1376
    .local v22, c:Landroid/database/Cursor;
    const/4 v5, 0x0

    #@3
    .line 1377
    .local v5, where:Ljava/lang/String;
    const/4 v6, 0x0

    #@4
    .line 1380
    .local v6, selectionArgs:[Ljava/lang/String;
    move-object/from16 v0, p0

    #@6
    iget-object v2, v0, Landroid/media/MediaScanner;->mFileCache:Ljava/util/HashMap;

    #@8
    if-nez v2, :cond_a3

    #@a
    .line 1381
    new-instance v2, Ljava/util/HashMap;

    #@c
    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    #@f
    move-object/from16 v0, p0

    #@11
    iput-object v2, v0, Landroid/media/MediaScanner;->mFileCache:Ljava/util/HashMap;

    #@13
    .line 1386
    :goto_13
    move-object/from16 v0, p0

    #@15
    iget-object v2, v0, Landroid/media/MediaScanner;->mPlayLists:Ljava/util/ArrayList;

    #@17
    if-nez v2, :cond_ac

    #@19
    .line 1387
    new-instance v2, Ljava/util/ArrayList;

    #@1b
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@1e
    move-object/from16 v0, p0

    #@20
    iput-object v2, v0, Landroid/media/MediaScanner;->mPlayLists:Ljava/util/ArrayList;

    #@22
    .line 1392
    :goto_22
    if-eqz p1, :cond_b5

    #@24
    .line 1394
    const-string v5, "_id>? AND _data=?"

    #@26
    .line 1396
    const/4 v2, 0x2

    #@27
    new-array v6, v2, [Ljava/lang/String;

    #@29
    .end local v6           #selectionArgs:[Ljava/lang/String;
    const/4 v2, 0x0

    #@2a
    const-string v4, ""

    #@2c
    aput-object v4, v6, v2

    #@2e
    const/4 v2, 0x1

    #@2f
    aput-object p1, v6, v2

    #@31
    .line 1406
    .restart local v6       #selectionArgs:[Ljava/lang/String;
    :goto_31
    move-object/from16 v0, p0

    #@33
    iget-object v2, v0, Landroid/media/MediaScanner;->mFilesUri:Landroid/net/Uri;

    #@35
    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    #@38
    move-result-object v21

    #@39
    .line 1407
    .local v21, builder:Landroid/net/Uri$Builder;
    const-string v2, "deletedata"

    #@3b
    const-string v4, "false"

    #@3d
    move-object/from16 v0, v21

    #@3f
    invoke-virtual {v0, v2, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@42
    .line 1408
    new-instance v23, Landroid/media/MediaScanner$MediaBulkDeleter;

    #@44
    move-object/from16 v0, p0

    #@46
    iget-object v2, v0, Landroid/media/MediaScanner;->mMediaProvider:Landroid/content/IContentProvider;

    #@48
    invoke-virtual/range {v21 .. v21}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    #@4b
    move-result-object v4

    #@4c
    move-object/from16 v0, v23

    #@4e
    invoke-direct {v0, v2, v4}, Landroid/media/MediaScanner$MediaBulkDeleter;-><init>(Landroid/content/IContentProvider;Landroid/net/Uri;)V

    #@51
    .line 1412
    .local v23, deleter:Landroid/media/MediaScanner$MediaBulkDeleter;
    if-eqz p2, :cond_f2

    #@53
    .line 1417
    const-wide/high16 v29, -0x8000

    #@55
    .line 1418
    .local v29, lastId:J
    :try_start_55
    move-object/from16 v0, p0

    #@57
    iget-object v2, v0, Landroid/media/MediaScanner;->mFilesUri:Landroid/net/Uri;

    #@59
    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    #@5c
    move-result-object v2

    #@5d
    const-string/jumbo v4, "limit"

    #@60
    const-string v14, "1000"

    #@62
    invoke-virtual {v2, v4, v14}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@65
    move-result-object v2

    #@66
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    #@69
    move-result-object v3

    #@6a
    .line 1419
    .local v3, limitUri:Landroid/net/Uri;
    const/4 v2, 0x1

    #@6b
    move-object/from16 v0, p0

    #@6d
    iput-boolean v2, v0, Landroid/media/MediaScanner;->mWasEmptyPriorToScan:Z

    #@6f
    .line 1422
    const/16 v35, 0x1

    #@71
    .line 1423
    .local v35, settingFinished:Z
    const/4 v2, 0x3

    #@72
    new-array v0, v2, [Ljava/lang/String;

    #@74
    move-object/from16 v36, v0

    #@76
    .line 1425
    .local v36, settingsItem:[Ljava/lang/String;
    const/4 v2, 0x0

    #@77
    const-string/jumbo v4, "notification_sound"

    #@7a
    aput-object v4, v36, v2

    #@7c
    .line 1426
    const/4 v2, 0x1

    #@7d
    const-string/jumbo v4, "ringtone"

    #@80
    aput-object v4, v36, v2

    #@82
    .line 1427
    const/4 v2, 0x2

    #@83
    const-string v4, "alarm_alert"

    #@85
    aput-object v4, v36, v2

    #@87
    .line 1429
    const/16 v27, 0x0

    #@89
    .local v27, i:I
    :goto_89
    const/4 v2, 0x3

    #@8a
    move/from16 v0, v27

    #@8c
    if-ge v0, v2, :cond_c4

    #@8e
    .line 1430
    move-object/from16 v0, p0

    #@90
    iget-object v2, v0, Landroid/media/MediaScanner;->mContext:Landroid/content/Context;

    #@92
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@95
    move-result-object v2

    #@96
    aget-object v4, v36, v27

    #@98
    invoke-static {v2, v4}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
    :try_end_9b
    .catchall {:try_start_55 .. :try_end_9b} :catchall_1cc

    #@9b
    move-result-object v24

    #@9c
    .line 1431
    .local v24, existingSetting:Ljava/lang/String;
    if-eqz v24, :cond_c1

    #@9e
    .line 1432
    and-int/lit8 v35, v35, 0x1

    #@a0
    .line 1429
    :goto_a0
    add-int/lit8 v27, v27, 0x1

    #@a2
    goto :goto_89

    #@a3
    .line 1383
    .end local v3           #limitUri:Landroid/net/Uri;
    .end local v21           #builder:Landroid/net/Uri$Builder;
    .end local v23           #deleter:Landroid/media/MediaScanner$MediaBulkDeleter;
    .end local v24           #existingSetting:Ljava/lang/String;
    .end local v27           #i:I
    .end local v29           #lastId:J
    .end local v35           #settingFinished:Z
    .end local v36           #settingsItem:[Ljava/lang/String;
    :cond_a3
    move-object/from16 v0, p0

    #@a5
    iget-object v2, v0, Landroid/media/MediaScanner;->mFileCache:Ljava/util/HashMap;

    #@a7
    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    #@aa
    goto/16 :goto_13

    #@ac
    .line 1389
    :cond_ac
    move-object/from16 v0, p0

    #@ae
    iget-object v2, v0, Landroid/media/MediaScanner;->mPlayLists:Ljava/util/ArrayList;

    #@b0
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@b3
    goto/16 :goto_22

    #@b5
    .line 1398
    :cond_b5
    const-string v5, "_id>?"

    #@b7
    .line 1399
    const/4 v2, 0x1

    #@b8
    new-array v6, v2, [Ljava/lang/String;

    #@ba
    .end local v6           #selectionArgs:[Ljava/lang/String;
    const/4 v2, 0x0

    #@bb
    const-string v4, ""

    #@bd
    aput-object v4, v6, v2

    #@bf
    .restart local v6       #selectionArgs:[Ljava/lang/String;
    goto/16 :goto_31

    #@c1
    .line 1434
    .restart local v3       #limitUri:Landroid/net/Uri;
    .restart local v21       #builder:Landroid/net/Uri$Builder;
    .restart local v23       #deleter:Landroid/media/MediaScanner$MediaBulkDeleter;
    .restart local v24       #existingSetting:Ljava/lang/String;
    .restart local v27       #i:I
    .restart local v29       #lastId:J
    .restart local v35       #settingFinished:Z
    .restart local v36       #settingsItem:[Ljava/lang/String;
    :cond_c1
    and-int/lit8 v35, v35, 0x0

    #@c3
    goto :goto_a0

    #@c4
    .line 1439
    .end local v24           #existingSetting:Ljava/lang/String;
    :cond_c4
    const/4 v2, 0x0

    #@c5
    :try_start_c5
    new-instance v4, Ljava/lang/StringBuilder;

    #@c7
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@ca
    const-string v14, ""

    #@cc
    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v4

    #@d0
    move-wide/from16 v0, v29

    #@d2
    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v4

    #@d6
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d9
    move-result-object v4

    #@da
    aput-object v4, v6, v2

    #@dc
    .line 1440
    if-eqz v22, :cond_e3

    #@de
    .line 1441
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    #@e1
    .line 1442
    const/16 v22, 0x0

    #@e3
    .line 1444
    :cond_e3
    move-object/from16 v0, p0

    #@e5
    iget-object v2, v0, Landroid/media/MediaScanner;->mMediaProvider:Landroid/content/IContentProvider;

    #@e7
    sget-object v4, Landroid/media/MediaScanner;->FILES_PRESCAN_PROJECTION:[Ljava/lang/String;

    #@e9
    const-string v7, "_id"

    #@eb
    const/4 v8, 0x0

    #@ec
    invoke-interface/range {v2 .. v8}, Landroid/content/IContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;
    :try_end_ef
    .catchall {:try_start_c5 .. :try_end_ef} :catchall_1cc

    #@ef
    move-result-object v22

    #@f0
    .line 1446
    if-nez v22, :cond_123

    #@f2
    .line 1517
    .end local v3           #limitUri:Landroid/net/Uri;
    .end local v27           #i:I
    .end local v29           #lastId:J
    .end local v35           #settingFinished:Z
    .end local v36           #settingsItem:[Ljava/lang/String;
    :cond_f2
    if-eqz v22, :cond_f7

    #@f4
    .line 1518
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    #@f7
    .line 1520
    :cond_f7
    invoke-virtual/range {v23 .. v23}, Landroid/media/MediaScanner$MediaBulkDeleter;->flush()V

    #@fa
    .line 1524
    const/4 v2, 0x0

    #@fb
    move-object/from16 v0, p0

    #@fd
    iput v2, v0, Landroid/media/MediaScanner;->mOriginalCount:I

    #@ff
    .line 1525
    move-object/from16 v0, p0

    #@101
    iget-object v14, v0, Landroid/media/MediaScanner;->mMediaProvider:Landroid/content/IContentProvider;

    #@103
    move-object/from16 v0, p0

    #@105
    iget-object v15, v0, Landroid/media/MediaScanner;->mImagesUri:Landroid/net/Uri;

    #@107
    sget-object v16, Landroid/media/MediaScanner;->ID_PROJECTION:[Ljava/lang/String;

    #@109
    const/16 v17, 0x0

    #@10b
    const/16 v18, 0x0

    #@10d
    const/16 v19, 0x0

    #@10f
    const/16 v20, 0x0

    #@111
    invoke-interface/range {v14 .. v20}, Landroid/content/IContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;

    #@114
    move-result-object v22

    #@115
    .line 1526
    if-eqz v22, :cond_122

    #@117
    .line 1527
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->getCount()I

    #@11a
    move-result v2

    #@11b
    move-object/from16 v0, p0

    #@11d
    iput v2, v0, Landroid/media/MediaScanner;->mOriginalCount:I

    #@11f
    .line 1528
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    #@122
    .line 1530
    :cond_122
    return-void

    #@123
    .line 1450
    .restart local v3       #limitUri:Landroid/net/Uri;
    .restart local v27       #i:I
    .restart local v29       #lastId:J
    .restart local v35       #settingFinished:Z
    .restart local v36       #settingsItem:[Ljava/lang/String;
    :cond_123
    :try_start_123
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->getCount()I

    #@126
    move-result v33

    #@127
    .line 1452
    .local v33, num:I
    if-eqz v33, :cond_f2

    #@129
    .line 1457
    if-eqz v35, :cond_130

    #@12b
    .line 1458
    const/4 v2, 0x0

    #@12c
    move-object/from16 v0, p0

    #@12e
    iput-boolean v2, v0, Landroid/media/MediaScanner;->mWasEmptyPriorToScan:Z

    #@130
    .line 1462
    :cond_130
    :goto_130
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToNext()Z

    #@133
    move-result v2

    #@134
    if-eqz v2, :cond_c4

    #@136
    .line 1463
    const/4 v2, 0x0

    #@137
    move-object/from16 v0, v22

    #@139
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    #@13c
    move-result-wide v8

    #@13d
    .line 1464
    .local v8, rowId:J
    const/4 v2, 0x1

    #@13e
    move-object/from16 v0, v22

    #@140
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@143
    move-result-object v10

    #@144
    .line 1465
    .local v10, path:Ljava/lang/String;
    const/4 v2, 0x2

    #@145
    move-object/from16 v0, v22

    #@147
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    #@14a
    move-result v13

    #@14b
    .line 1466
    .local v13, format:I
    const/4 v2, 0x3

    #@14c
    move-object/from16 v0, v22

    #@14e
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    #@151
    move-result-wide v11

    #@152
    .line 1467
    .local v11, lastModified:J
    move-wide/from16 v29, v8

    #@154
    .line 1472
    if-eqz v10, :cond_130

    #@156
    const-string v2, "/"

    #@158
    invoke-virtual {v10, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@15b
    move-result v2

    #@15c
    if-eqz v2, :cond_130

    #@15e
    .line 1473
    const/16 v25, 0x0

    #@160
    .line 1475
    .local v25, exists:Z
    const/16 v32, 0x0

    #@162
    .line 1476
    .local v32, needtodelete:Z
    move-object/from16 v28, v10

    #@164
    .line 1477
    .local v28, key:Ljava/lang/String;
    move-object/from16 v0, p0

    #@166
    iget-boolean v2, v0, Landroid/media/MediaScanner;->mCaseInsensitivePaths:Z

    #@168
    if-eqz v2, :cond_16e

    #@16a
    .line 1478
    invoke-virtual {v10}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
    :try_end_16d
    .catchall {:try_start_123 .. :try_end_16d} :catchall_1cc

    #@16d
    move-result-object v28

    #@16e
    .line 1482
    :cond_16e
    :try_start_16e
    sget-object v2, Llibcore/io/Libcore;->os:Llibcore/io/Os;

    #@170
    sget v4, Llibcore/io/OsConstants;->F_OK:I

    #@172
    invoke-interface {v2, v10, v4}, Llibcore/io/Os;->access(Ljava/lang/String;I)Z
    :try_end_175
    .catchall {:try_start_16e .. :try_end_175} :catchall_1cc
    .catch Llibcore/io/ErrnoException; {:try_start_16e .. :try_end_175} :catch_1dd

    #@175
    move-result v25

    #@176
    .line 1485
    :goto_176
    if-nez v25, :cond_1ba

    #@178
    :try_start_178
    invoke-static {v13}, Landroid/mtp/MtpConstants;->isAbstractObject(I)Z

    #@17b
    move-result v2

    #@17c
    if-nez v2, :cond_1ba

    #@17e
    .line 1490
    invoke-static {v10}, Landroid/media/MediaFile;->getFileType(Ljava/lang/String;)Landroid/media/MediaFile$MediaFileType;

    #@181
    move-result-object v31

    #@182
    .line 1491
    .local v31, mediaFileType:Landroid/media/MediaFile$MediaFileType;
    if-nez v31, :cond_1d6

    #@184
    const/16 v26, 0x0

    #@186
    .line 1493
    .local v26, fileType:I
    :goto_186
    invoke-static/range {v26 .. v26}, Landroid/media/MediaFile;->isPlayListFileType(I)Z

    #@189
    move-result v2

    #@18a
    if-nez v2, :cond_1ba

    #@18c
    .line 1495
    const/16 v32, 0x1

    #@18e
    .line 1497
    move-object/from16 v0, v23

    #@190
    invoke-virtual {v0, v8, v9}, Landroid/media/MediaScanner$MediaBulkDeleter;->delete(J)V

    #@193
    .line 1498
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    #@195
    invoke-virtual {v10, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    #@198
    move-result-object v2

    #@199
    const-string v4, "/.nomedia"

    #@19b
    invoke-virtual {v2, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@19e
    move-result v2

    #@19f
    if-eqz v2, :cond_1ba

    #@1a1
    .line 1499
    invoke-virtual/range {v23 .. v23}, Landroid/media/MediaScanner$MediaBulkDeleter;->flush()V

    #@1a4
    .line 1500
    new-instance v2, Ljava/io/File;

    #@1a6
    invoke-direct {v2, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@1a9
    invoke-virtual {v2}, Ljava/io/File;->getParent()Ljava/lang/String;

    #@1ac
    move-result-object v34

    #@1ad
    .line 1501
    .local v34, parent:Ljava/lang/String;
    move-object/from16 v0, p0

    #@1af
    iget-object v2, v0, Landroid/media/MediaScanner;->mMediaProvider:Landroid/content/IContentProvider;

    #@1b1
    const-string/jumbo v4, "unhide"

    #@1b4
    const/4 v14, 0x0

    #@1b5
    move-object/from16 v0, v34

    #@1b7
    invoke-interface {v2, v4, v0, v14}, Landroid/content/IContentProvider;->call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    #@1ba
    .line 1506
    .end local v26           #fileType:I
    .end local v31           #mediaFileType:Landroid/media/MediaFile$MediaFileType;
    .end local v34           #parent:Ljava/lang/String;
    :cond_1ba
    if-nez v32, :cond_130

    #@1bc
    .line 1507
    new-instance v7, Landroid/media/MediaScanner$FileEntry;

    #@1be
    invoke-direct/range {v7 .. v13}, Landroid/media/MediaScanner$FileEntry;-><init>(JLjava/lang/String;JI)V

    #@1c1
    .line 1508
    .local v7, entry:Landroid/media/MediaScanner$FileEntry;
    move-object/from16 v0, p0

    #@1c3
    iget-object v2, v0, Landroid/media/MediaScanner;->mFileCache:Ljava/util/HashMap;

    #@1c5
    move-object/from16 v0, v28

    #@1c7
    invoke-virtual {v2, v0, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1ca
    .catchall {:try_start_178 .. :try_end_1ca} :catchall_1cc

    #@1ca
    goto/16 :goto_130

    #@1cc
    .line 1517
    .end local v3           #limitUri:Landroid/net/Uri;
    .end local v7           #entry:Landroid/media/MediaScanner$FileEntry;
    .end local v8           #rowId:J
    .end local v10           #path:Ljava/lang/String;
    .end local v11           #lastModified:J
    .end local v13           #format:I
    .end local v25           #exists:Z
    .end local v27           #i:I
    .end local v28           #key:Ljava/lang/String;
    .end local v32           #needtodelete:Z
    .end local v33           #num:I
    .end local v35           #settingFinished:Z
    .end local v36           #settingsItem:[Ljava/lang/String;
    :catchall_1cc
    move-exception v2

    #@1cd
    if-eqz v22, :cond_1d2

    #@1cf
    .line 1518
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    #@1d2
    .line 1520
    :cond_1d2
    invoke-virtual/range {v23 .. v23}, Landroid/media/MediaScanner$MediaBulkDeleter;->flush()V

    #@1d5
    throw v2

    #@1d6
    .line 1491
    .restart local v3       #limitUri:Landroid/net/Uri;
    .restart local v8       #rowId:J
    .restart local v10       #path:Ljava/lang/String;
    .restart local v11       #lastModified:J
    .restart local v13       #format:I
    .restart local v25       #exists:Z
    .restart local v27       #i:I
    .restart local v28       #key:Ljava/lang/String;
    .restart local v31       #mediaFileType:Landroid/media/MediaFile$MediaFileType;
    .restart local v32       #needtodelete:Z
    .restart local v33       #num:I
    .restart local v35       #settingFinished:Z
    .restart local v36       #settingsItem:[Ljava/lang/String;
    :cond_1d6
    :try_start_1d6
    move-object/from16 v0, v31

    #@1d8
    iget v0, v0, Landroid/media/MediaFile$MediaFileType;->fileType:I

    #@1da
    move/from16 v26, v0
    :try_end_1dc
    .catchall {:try_start_1d6 .. :try_end_1dc} :catchall_1cc

    #@1dc
    goto :goto_186

    #@1dd
    .line 1483
    .end local v31           #mediaFileType:Landroid/media/MediaFile$MediaFileType;
    :catch_1dd
    move-exception v2

    #@1de
    goto :goto_176
.end method

.method private processCachedPlaylist(Landroid/database/Cursor;Landroid/content/ContentValues;Landroid/net/Uri;)V
    .registers 15
    .parameter "fileList"
    .parameter "values"
    .parameter "playlistUri"

    #@0
    .prologue
    .line 2041
    const/4 v8, -0x1

    #@1
    invoke-interface {p1, v8}, Landroid/database/Cursor;->moveToPosition(I)Z

    #@4
    .line 2042
    :cond_4
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    #@7
    move-result v8

    #@8
    if-eqz v8, :cond_1a

    #@a
    .line 2043
    const/4 v8, 0x0

    #@b
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getLong(I)J

    #@e
    move-result-wide v6

    #@f
    .line 2044
    .local v6, rowId:J
    const/4 v8, 0x1

    #@10
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    .line 2045
    .local v0, data:Ljava/lang/String;
    invoke-direct {p0, v6, v7, v0}, Landroid/media/MediaScanner;->matchEntries(JLjava/lang/String;)Z

    #@17
    move-result v8

    #@18
    if-eqz v8, :cond_4

    #@1a
    .line 2050
    .end local v0           #data:Ljava/lang/String;
    .end local v6           #rowId:J
    :cond_1a
    iget-object v8, p0, Landroid/media/MediaScanner;->mPlaylistEntries:Ljava/util/ArrayList;

    #@1c
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@1f
    move-result v5

    #@20
    .line 2051
    .local v5, len:I
    const/4 v4, 0x0

    #@21
    .line 2052
    .local v4, index:I
    const/4 v3, 0x0

    #@22
    .local v3, i:I
    :goto_22
    if-ge v3, v5, :cond_5b

    #@24
    .line 2053
    iget-object v8, p0, Landroid/media/MediaScanner;->mPlaylistEntries:Ljava/util/ArrayList;

    #@26
    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@29
    move-result-object v2

    #@2a
    check-cast v2, Landroid/media/MediaScanner$PlaylistEntry;

    #@2c
    .line 2054
    .local v2, entry:Landroid/media/MediaScanner$PlaylistEntry;
    iget v8, v2, Landroid/media/MediaScanner$PlaylistEntry;->bestmatchlevel:I

    #@2e
    if-lez v8, :cond_4f

    #@30
    .line 2056
    :try_start_30
    invoke-virtual {p2}, Landroid/content/ContentValues;->clear()V

    #@33
    .line 2057
    const-string/jumbo v8, "play_order"

    #@36
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@39
    move-result-object v9

    #@3a
    invoke-virtual {p2, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@3d
    .line 2058
    const-string v8, "audio_id"

    #@3f
    iget-wide v9, v2, Landroid/media/MediaScanner$PlaylistEntry;->bestmatchid:J

    #@41
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@44
    move-result-object v9

    #@45
    invoke-virtual {p2, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@48
    .line 2059
    iget-object v8, p0, Landroid/media/MediaScanner;->mMediaProvider:Landroid/content/IContentProvider;

    #@4a
    invoke-interface {v8, p3, p2}, Landroid/content/IContentProvider;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_4d
    .catch Landroid/os/RemoteException; {:try_start_30 .. :try_end_4d} :catch_52

    #@4d
    .line 2060
    add-int/lit8 v4, v4, 0x1

    #@4f
    .line 2052
    :cond_4f
    add-int/lit8 v3, v3, 0x1

    #@51
    goto :goto_22

    #@52
    .line 2061
    :catch_52
    move-exception v1

    #@53
    .line 2062
    .local v1, e:Landroid/os/RemoteException;
    const-string v8, "MediaScanner"

    #@55
    const-string v9, "RemoteException in MediaScanner.processCachedPlaylist()"

    #@57
    invoke-static {v8, v9, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@5a
    .line 2068
    .end local v1           #e:Landroid/os/RemoteException;
    .end local v2           #entry:Landroid/media/MediaScanner$PlaylistEntry;
    :goto_5a
    return-void

    #@5b
    .line 2067
    :cond_5b
    iget-object v8, p0, Landroid/media/MediaScanner;->mPlaylistEntries:Ljava/util/ArrayList;

    #@5d
    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    #@60
    goto :goto_5a
.end method

.method private native processDirectory(Ljava/lang/String;Landroid/media/MediaScannerClient;)V
.end method

.method private native processFile(Ljava/lang/String;Ljava/lang/String;Landroid/media/MediaScannerClient;)V
.end method

.method private processM3uPlayList(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/content/ContentValues;Landroid/database/Cursor;)V
    .registers 14
    .parameter "path"
    .parameter "playListDirectory"
    .parameter "uri"
    .parameter "values"
    .parameter "fileList"

    #@0
    .prologue
    .line 2072
    const/4 v3, 0x0

    #@1
    .line 2074
    .local v3, reader:Ljava/io/BufferedReader;
    :try_start_1
    new-instance v1, Ljava/io/File;

    #@3
    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@6
    .line 2075
    .local v1, f:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@9
    move-result v5

    #@a
    if-eqz v5, :cond_43

    #@c
    .line 2076
    new-instance v4, Ljava/io/BufferedReader;

    #@e
    new-instance v5, Ljava/io/InputStreamReader;

    #@10
    new-instance v6, Ljava/io/FileInputStream;

    #@12
    invoke-direct {v6, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    #@15
    invoke-direct {v5, v6}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    #@18
    const/16 v6, 0x2000

    #@1a
    invoke-direct {v4, v5, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_1d
    .catchall {:try_start_1 .. :try_end_1d} :catchall_69
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1d} :catch_52

    #@1d
    .line 2078
    .end local v3           #reader:Ljava/io/BufferedReader;
    .local v4, reader:Ljava/io/BufferedReader;
    :try_start_1d
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    .line 2079
    .local v2, line:Ljava/lang/String;
    iget-object v5, p0, Landroid/media/MediaScanner;->mPlaylistEntries:Ljava/util/ArrayList;

    #@23
    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    #@26
    .line 2080
    :goto_26
    if-eqz v2, :cond_3f

    #@28
    .line 2082
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@2b
    move-result v5

    #@2c
    if-lez v5, :cond_3a

    #@2e
    const/4 v5, 0x0

    #@2f
    invoke-virtual {v2, v5}, Ljava/lang/String;->charAt(I)C

    #@32
    move-result v5

    #@33
    const/16 v6, 0x23

    #@35
    if-eq v5, v6, :cond_3a

    #@37
    .line 2083
    invoke-direct {p0, v2, p2}, Landroid/media/MediaScanner;->cachePlaylistEntry(Ljava/lang/String;Ljava/lang/String;)V

    #@3a
    .line 2085
    :cond_3a
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@3d
    move-result-object v2

    #@3e
    goto :goto_26

    #@3f
    .line 2088
    :cond_3f
    invoke-direct {p0, p5, p4, p3}, Landroid/media/MediaScanner;->processCachedPlaylist(Landroid/database/Cursor;Landroid/content/ContentValues;Landroid/net/Uri;)V
    :try_end_42
    .catchall {:try_start_1d .. :try_end_42} :catchall_79
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_42} :catch_7c

    #@42
    move-object v3, v4

    #@43
    .line 2094
    .end local v2           #line:Ljava/lang/String;
    .end local v4           #reader:Ljava/io/BufferedReader;
    .restart local v3       #reader:Ljava/io/BufferedReader;
    :cond_43
    if-eqz v3, :cond_48

    #@45
    .line 2095
    :try_start_45
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_48
    .catch Ljava/io/IOException; {:try_start_45 .. :try_end_48} :catch_49

    #@48
    .line 2100
    .end local v1           #f:Ljava/io/File;
    :cond_48
    :goto_48
    return-void

    #@49
    .line 2096
    .restart local v1       #f:Ljava/io/File;
    :catch_49
    move-exception v0

    #@4a
    .line 2097
    .local v0, e:Ljava/io/IOException;
    const-string v5, "MediaScanner"

    #@4c
    const-string v6, "IOException in MediaScanner.processM3uPlayList()"

    #@4e
    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@51
    goto :goto_48

    #@52
    .line 2090
    .end local v0           #e:Ljava/io/IOException;
    .end local v1           #f:Ljava/io/File;
    :catch_52
    move-exception v0

    #@53
    .line 2091
    .restart local v0       #e:Ljava/io/IOException;
    :goto_53
    :try_start_53
    const-string v5, "MediaScanner"

    #@55
    const-string v6, "IOException in MediaScanner.processM3uPlayList()"

    #@57
    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5a
    .catchall {:try_start_53 .. :try_end_5a} :catchall_69

    #@5a
    .line 2094
    if-eqz v3, :cond_48

    #@5c
    .line 2095
    :try_start_5c
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_5f
    .catch Ljava/io/IOException; {:try_start_5c .. :try_end_5f} :catch_60

    #@5f
    goto :goto_48

    #@60
    .line 2096
    :catch_60
    move-exception v0

    #@61
    .line 2097
    const-string v5, "MediaScanner"

    #@63
    const-string v6, "IOException in MediaScanner.processM3uPlayList()"

    #@65
    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@68
    goto :goto_48

    #@69
    .line 2093
    .end local v0           #e:Ljava/io/IOException;
    :catchall_69
    move-exception v5

    #@6a
    .line 2094
    :goto_6a
    if-eqz v3, :cond_6f

    #@6c
    .line 2095
    :try_start_6c
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_6f
    .catch Ljava/io/IOException; {:try_start_6c .. :try_end_6f} :catch_70

    #@6f
    .line 2098
    :cond_6f
    :goto_6f
    throw v5

    #@70
    .line 2096
    :catch_70
    move-exception v0

    #@71
    .line 2097
    .restart local v0       #e:Ljava/io/IOException;
    const-string v6, "MediaScanner"

    #@73
    const-string v7, "IOException in MediaScanner.processM3uPlayList()"

    #@75
    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@78
    goto :goto_6f

    #@79
    .line 2093
    .end local v0           #e:Ljava/io/IOException;
    .end local v3           #reader:Ljava/io/BufferedReader;
    .restart local v1       #f:Ljava/io/File;
    .restart local v4       #reader:Ljava/io/BufferedReader;
    :catchall_79
    move-exception v5

    #@7a
    move-object v3, v4

    #@7b
    .end local v4           #reader:Ljava/io/BufferedReader;
    .restart local v3       #reader:Ljava/io/BufferedReader;
    goto :goto_6a

    #@7c
    .line 2090
    .end local v3           #reader:Ljava/io/BufferedReader;
    .restart local v4       #reader:Ljava/io/BufferedReader;
    :catch_7c
    move-exception v0

    #@7d
    move-object v3, v4

    #@7e
    .end local v4           #reader:Ljava/io/BufferedReader;
    .restart local v3       #reader:Ljava/io/BufferedReader;
    goto :goto_53
.end method

.method private processPlayList(Landroid/media/MediaScanner$FileEntry;Landroid/database/Cursor;)V
    .registers 20
    .parameter "entry"
    .parameter "fileList"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2200
    move-object/from16 v0, p1

    #@2
    iget-object v2, v0, Landroid/media/MediaScanner$FileEntry;->mPath:Ljava/lang/String;

    #@4
    .line 2201
    .local v2, path:Ljava/lang/String;
    new-instance v5, Landroid/content/ContentValues;

    #@6
    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    #@9
    .line 2202
    .local v5, values:Landroid/content/ContentValues;
    const/16 v1, 0x2f

    #@b
    invoke-virtual {v2, v1}, Ljava/lang/String;->lastIndexOf(I)I

    #@e
    move-result v9

    #@f
    .line 2203
    .local v9, lastSlash:I
    if-gez v9, :cond_2a

    #@11
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@13
    new-instance v6, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v15, "bad path "

    #@1a
    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v6

    #@1e
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v6

    #@22
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v6

    #@26
    invoke-direct {v1, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@29
    throw v1

    #@2a
    .line 2205
    :cond_2a
    move-object/from16 v0, p1

    #@2c
    iget-wide v12, v0, Landroid/media/MediaScanner$FileEntry;->mRowId:J

    #@2e
    .line 2208
    .local v12, rowId:J
    const-string/jumbo v1, "name"

    #@31
    invoke-virtual {v5, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    #@34
    move-result-object v11

    #@35
    .line 2209
    .local v11, name:Ljava/lang/String;
    if-nez v11, :cond_4e

    #@37
    .line 2210
    const-string/jumbo v1, "title"

    #@3a
    invoke-virtual {v5, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    #@3d
    move-result-object v11

    #@3e
    .line 2211
    if-nez v11, :cond_4e

    #@40
    .line 2213
    const/16 v1, 0x2e

    #@42
    invoke-virtual {v2, v1}, Ljava/lang/String;->lastIndexOf(I)I

    #@45
    move-result v8

    #@46
    .line 2214
    .local v8, lastDot:I
    if-gez v8, :cond_9d

    #@48
    add-int/lit8 v1, v9, 0x1

    #@4a
    invoke-virtual {v2, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@4d
    move-result-object v11

    #@4e
    .line 2219
    .end local v8           #lastDot:I
    :cond_4e
    :goto_4e
    const-string/jumbo v1, "name"

    #@51
    invoke-virtual {v5, v1, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@54
    .line 2220
    const-string v1, "date_modified"

    #@56
    move-object/from16 v0, p1

    #@58
    iget-wide v15, v0, Landroid/media/MediaScanner$FileEntry;->mLastModified:J

    #@5a
    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@5d
    move-result-object v6

    #@5e
    invoke-virtual {v5, v1, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@61
    .line 2222
    const-wide/16 v15, 0x0

    #@63
    cmp-long v1, v12, v15

    #@65
    if-nez v1, :cond_a4

    #@67
    .line 2223
    const-string v1, "_data"

    #@69
    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@6c
    .line 2224
    move-object/from16 v0, p0

    #@6e
    iget-object v1, v0, Landroid/media/MediaScanner;->mMediaProvider:Landroid/content/IContentProvider;

    #@70
    move-object/from16 v0, p0

    #@72
    iget-object v6, v0, Landroid/media/MediaScanner;->mPlaylistsUri:Landroid/net/Uri;

    #@74
    invoke-interface {v1, v6, v5}, Landroid/content/IContentProvider;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@77
    move-result-object v14

    #@78
    .line 2225
    .local v14, uri:Landroid/net/Uri;
    invoke-static {v14}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    #@7b
    move-result-wide v12

    #@7c
    .line 2226
    const-string/jumbo v1, "members"

    #@7f
    invoke-static {v14, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@82
    move-result-object v4

    #@83
    .line 2236
    .local v4, membersUri:Landroid/net/Uri;
    :goto_83
    const/4 v1, 0x0

    #@84
    add-int/lit8 v6, v9, 0x1

    #@86
    invoke-virtual {v2, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@89
    move-result-object v3

    #@8a
    .line 2237
    .local v3, playListDirectory:Ljava/lang/String;
    invoke-static {v2}, Landroid/media/MediaFile;->getFileType(Ljava/lang/String;)Landroid/media/MediaFile$MediaFileType;

    #@8d
    move-result-object v10

    #@8e
    .line 2238
    .local v10, mediaFileType:Landroid/media/MediaFile$MediaFileType;
    if-nez v10, :cond_c6

    #@90
    const/4 v7, 0x0

    #@91
    .line 2240
    .local v7, fileType:I
    :goto_91
    const/16 v1, 0x29

    #@93
    if-ne v7, v1, :cond_c9

    #@95
    move-object/from16 v1, p0

    #@97
    move-object/from16 v6, p2

    #@99
    .line 2241
    invoke-direct/range {v1 .. v6}, Landroid/media/MediaScanner;->processM3uPlayList(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/content/ContentValues;Landroid/database/Cursor;)V

    #@9c
    .line 2247
    :cond_9c
    :goto_9c
    return-void

    #@9d
    .line 2214
    .end local v3           #playListDirectory:Ljava/lang/String;
    .end local v4           #membersUri:Landroid/net/Uri;
    .end local v7           #fileType:I
    .end local v10           #mediaFileType:Landroid/media/MediaFile$MediaFileType;
    .end local v14           #uri:Landroid/net/Uri;
    .restart local v8       #lastDot:I
    :cond_9d
    add-int/lit8 v1, v9, 0x1

    #@9f
    invoke-virtual {v2, v1, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@a2
    move-result-object v11

    #@a3
    goto :goto_4e

    #@a4
    .line 2228
    .end local v8           #lastDot:I
    :cond_a4
    move-object/from16 v0, p0

    #@a6
    iget-object v1, v0, Landroid/media/MediaScanner;->mPlaylistsUri:Landroid/net/Uri;

    #@a8
    invoke-static {v1, v12, v13}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@ab
    move-result-object v14

    #@ac
    .line 2229
    .restart local v14       #uri:Landroid/net/Uri;
    move-object/from16 v0, p0

    #@ae
    iget-object v1, v0, Landroid/media/MediaScanner;->mMediaProvider:Landroid/content/IContentProvider;

    #@b0
    const/4 v6, 0x0

    #@b1
    const/4 v15, 0x0

    #@b2
    invoke-interface {v1, v14, v5, v6, v15}, Landroid/content/IContentProvider;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@b5
    .line 2232
    const-string/jumbo v1, "members"

    #@b8
    invoke-static {v14, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@bb
    move-result-object v4

    #@bc
    .line 2233
    .restart local v4       #membersUri:Landroid/net/Uri;
    move-object/from16 v0, p0

    #@be
    iget-object v1, v0, Landroid/media/MediaScanner;->mMediaProvider:Landroid/content/IContentProvider;

    #@c0
    const/4 v6, 0x0

    #@c1
    const/4 v15, 0x0

    #@c2
    invoke-interface {v1, v4, v6, v15}, Landroid/content/IContentProvider;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@c5
    goto :goto_83

    #@c6
    .line 2238
    .restart local v3       #playListDirectory:Ljava/lang/String;
    .restart local v10       #mediaFileType:Landroid/media/MediaFile$MediaFileType;
    :cond_c6
    iget v7, v10, Landroid/media/MediaFile$MediaFileType;->fileType:I

    #@c8
    goto :goto_91

    #@c9
    .line 2242
    .restart local v7       #fileType:I
    :cond_c9
    const/16 v1, 0x2a

    #@cb
    if-ne v7, v1, :cond_d5

    #@cd
    move-object/from16 v1, p0

    #@cf
    move-object/from16 v6, p2

    #@d1
    .line 2243
    invoke-direct/range {v1 .. v6}, Landroid/media/MediaScanner;->processPlsPlayList(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/content/ContentValues;Landroid/database/Cursor;)V

    #@d4
    goto :goto_9c

    #@d5
    .line 2244
    :cond_d5
    const/16 v1, 0x2b

    #@d7
    if-ne v7, v1, :cond_9c

    #@d9
    move-object/from16 v1, p0

    #@db
    move-object/from16 v6, p2

    #@dd
    .line 2245
    invoke-direct/range {v1 .. v6}, Landroid/media/MediaScanner;->processWplPlayList(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/content/ContentValues;Landroid/database/Cursor;)V

    #@e0
    goto :goto_9c
.end method

.method private processPlayLists()V
    .registers 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2250
    iget-object v0, p0, Landroid/media/MediaScanner;->mPlayLists:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v9

    #@6
    .line 2251
    .local v9, iterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/MediaScanner$FileEntry;>;"
    const/4 v8, 0x0

    #@7
    .line 2255
    .local v8, fileList:Landroid/database/Cursor;
    :try_start_7
    iget-object v0, p0, Landroid/media/MediaScanner;->mMediaProvider:Landroid/content/IContentProvider;

    #@9
    iget-object v1, p0, Landroid/media/MediaScanner;->mFilesUri:Landroid/net/Uri;

    #@b
    sget-object v2, Landroid/media/MediaScanner;->FILES_PRESCAN_PROJECTION:[Ljava/lang/String;

    #@d
    const-string/jumbo v3, "media_type=2"

    #@10
    const/4 v4, 0x0

    #@11
    const/4 v5, 0x0

    #@12
    const/4 v6, 0x0

    #@13
    invoke-interface/range {v0 .. v6}, Landroid/content/IContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;

    #@16
    move-result-object v8

    #@17
    .line 2257
    :cond_17
    :goto_17
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    #@1a
    move-result v0

    #@1b
    if-eqz v0, :cond_32

    #@1d
    .line 2258
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@20
    move-result-object v7

    #@21
    check-cast v7, Landroid/media/MediaScanner$FileEntry;

    #@23
    .line 2260
    .local v7, entry:Landroid/media/MediaScanner$FileEntry;
    iget-boolean v0, v7, Landroid/media/MediaScanner$FileEntry;->mLastModifiedChanged:Z

    #@25
    if-eqz v0, :cond_17

    #@27
    .line 2261
    invoke-direct {p0, v7, v8}, Landroid/media/MediaScanner;->processPlayList(Landroid/media/MediaScanner$FileEntry;Landroid/database/Cursor;)V
    :try_end_2a
    .catchall {:try_start_7 .. :try_end_2a} :catchall_38
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_2a} :catch_2b

    #@2a
    goto :goto_17

    #@2b
    .line 2264
    .end local v7           #entry:Landroid/media/MediaScanner$FileEntry;
    :catch_2b
    move-exception v0

    #@2c
    .line 2266
    if-eqz v8, :cond_31

    #@2e
    .line 2267
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@31
    .line 2270
    :cond_31
    :goto_31
    return-void

    #@32
    .line 2266
    :cond_32
    if-eqz v8, :cond_31

    #@34
    .line 2267
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@37
    goto :goto_31

    #@38
    .line 2266
    :catchall_38
    move-exception v0

    #@39
    if-eqz v8, :cond_3e

    #@3b
    .line 2267
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@3e
    :cond_3e
    throw v0
.end method

.method private processPlsPlayList(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/content/ContentValues;Landroid/database/Cursor;)V
    .registers 15
    .parameter "path"
    .parameter "playListDirectory"
    .parameter "uri"
    .parameter "values"
    .parameter "fileList"

    #@0
    .prologue
    .line 2104
    const/4 v4, 0x0

    #@1
    .line 2106
    .local v4, reader:Ljava/io/BufferedReader;
    :try_start_1
    new-instance v2, Ljava/io/File;

    #@3
    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@6
    .line 2107
    .local v2, f:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    #@9
    move-result v6

    #@a
    if-eqz v6, :cond_4a

    #@c
    .line 2108
    new-instance v5, Ljava/io/BufferedReader;

    #@e
    new-instance v6, Ljava/io/InputStreamReader;

    #@10
    new-instance v7, Ljava/io/FileInputStream;

    #@12
    invoke-direct {v7, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    #@15
    invoke-direct {v6, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    #@18
    const/16 v7, 0x2000

    #@1a
    invoke-direct {v5, v6, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_1d
    .catchall {:try_start_1 .. :try_end_1d} :catchall_70
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1d} :catch_59

    #@1d
    .line 2110
    .end local v4           #reader:Ljava/io/BufferedReader;
    .local v5, reader:Ljava/io/BufferedReader;
    :try_start_1d
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    .line 2111
    .local v3, line:Ljava/lang/String;
    iget-object v6, p0, Landroid/media/MediaScanner;->mPlaylistEntries:Ljava/util/ArrayList;

    #@23
    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    #@26
    .line 2112
    :goto_26
    if-eqz v3, :cond_46

    #@28
    .line 2114
    const-string v6, "File"

    #@2a
    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@2d
    move-result v6

    #@2e
    if-eqz v6, :cond_41

    #@30
    .line 2115
    const/16 v6, 0x3d

    #@32
    invoke-virtual {v3, v6}, Ljava/lang/String;->indexOf(I)I

    #@35
    move-result v1

    #@36
    .line 2116
    .local v1, equals:I
    if-lez v1, :cond_41

    #@38
    .line 2117
    add-int/lit8 v6, v1, 0x1

    #@3a
    invoke-virtual {v3, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@3d
    move-result-object v6

    #@3e
    invoke-direct {p0, v6, p2}, Landroid/media/MediaScanner;->cachePlaylistEntry(Ljava/lang/String;Ljava/lang/String;)V

    #@41
    .line 2120
    .end local v1           #equals:I
    :cond_41
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@44
    move-result-object v3

    #@45
    goto :goto_26

    #@46
    .line 2123
    :cond_46
    invoke-direct {p0, p5, p4, p3}, Landroid/media/MediaScanner;->processCachedPlaylist(Landroid/database/Cursor;Landroid/content/ContentValues;Landroid/net/Uri;)V
    :try_end_49
    .catchall {:try_start_1d .. :try_end_49} :catchall_80
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_49} :catch_83

    #@49
    move-object v4, v5

    #@4a
    .line 2129
    .end local v3           #line:Ljava/lang/String;
    .end local v5           #reader:Ljava/io/BufferedReader;
    .restart local v4       #reader:Ljava/io/BufferedReader;
    :cond_4a
    if-eqz v4, :cond_4f

    #@4c
    .line 2130
    :try_start_4c
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_4f
    .catch Ljava/io/IOException; {:try_start_4c .. :try_end_4f} :catch_50

    #@4f
    .line 2135
    .end local v2           #f:Ljava/io/File;
    :cond_4f
    :goto_4f
    return-void

    #@50
    .line 2131
    .restart local v2       #f:Ljava/io/File;
    :catch_50
    move-exception v0

    #@51
    .line 2132
    .local v0, e:Ljava/io/IOException;
    const-string v6, "MediaScanner"

    #@53
    const-string v7, "IOException in MediaScanner.processPlsPlayList()"

    #@55
    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@58
    goto :goto_4f

    #@59
    .line 2125
    .end local v0           #e:Ljava/io/IOException;
    .end local v2           #f:Ljava/io/File;
    :catch_59
    move-exception v0

    #@5a
    .line 2126
    .restart local v0       #e:Ljava/io/IOException;
    :goto_5a
    :try_start_5a
    const-string v6, "MediaScanner"

    #@5c
    const-string v7, "IOException in MediaScanner.processPlsPlayList()"

    #@5e
    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_61
    .catchall {:try_start_5a .. :try_end_61} :catchall_70

    #@61
    .line 2129
    if-eqz v4, :cond_4f

    #@63
    .line 2130
    :try_start_63
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_66
    .catch Ljava/io/IOException; {:try_start_63 .. :try_end_66} :catch_67

    #@66
    goto :goto_4f

    #@67
    .line 2131
    :catch_67
    move-exception v0

    #@68
    .line 2132
    const-string v6, "MediaScanner"

    #@6a
    const-string v7, "IOException in MediaScanner.processPlsPlayList()"

    #@6c
    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@6f
    goto :goto_4f

    #@70
    .line 2128
    .end local v0           #e:Ljava/io/IOException;
    :catchall_70
    move-exception v6

    #@71
    .line 2129
    :goto_71
    if-eqz v4, :cond_76

    #@73
    .line 2130
    :try_start_73
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_76
    .catch Ljava/io/IOException; {:try_start_73 .. :try_end_76} :catch_77

    #@76
    .line 2133
    :cond_76
    :goto_76
    throw v6

    #@77
    .line 2131
    :catch_77
    move-exception v0

    #@78
    .line 2132
    .restart local v0       #e:Ljava/io/IOException;
    const-string v7, "MediaScanner"

    #@7a
    const-string v8, "IOException in MediaScanner.processPlsPlayList()"

    #@7c
    invoke-static {v7, v8, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@7f
    goto :goto_76

    #@80
    .line 2128
    .end local v0           #e:Ljava/io/IOException;
    .end local v4           #reader:Ljava/io/BufferedReader;
    .restart local v2       #f:Ljava/io/File;
    .restart local v5       #reader:Ljava/io/BufferedReader;
    :catchall_80
    move-exception v6

    #@81
    move-object v4, v5

    #@82
    .end local v5           #reader:Ljava/io/BufferedReader;
    .restart local v4       #reader:Ljava/io/BufferedReader;
    goto :goto_71

    #@83
    .line 2125
    .end local v4           #reader:Ljava/io/BufferedReader;
    .restart local v5       #reader:Ljava/io/BufferedReader;
    :catch_83
    move-exception v0

    #@84
    move-object v4, v5

    #@85
    .end local v5           #reader:Ljava/io/BufferedReader;
    .restart local v4       #reader:Ljava/io/BufferedReader;
    goto :goto_5a
.end method

.method private processWplPlayList(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/content/ContentValues;Landroid/database/Cursor;)V
    .registers 13
    .parameter "path"
    .parameter "playListDirectory"
    .parameter "uri"
    .parameter "values"
    .parameter "fileList"

    #@0
    .prologue
    .line 2173
    const/4 v2, 0x0

    #@1
    .line 2175
    .local v2, fis:Ljava/io/FileInputStream;
    :try_start_1
    new-instance v1, Ljava/io/File;

    #@3
    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@6
    .line 2176
    .local v1, f:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@9
    move-result v4

    #@a
    if-eqz v4, :cond_2c

    #@c
    .line 2177
    new-instance v3, Ljava/io/FileInputStream;

    #@e
    invoke-direct {v3, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_61
    .catch Lorg/xml/sax/SAXException; {:try_start_1 .. :try_end_11} :catch_3b
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_11} :catch_4e

    #@11
    .line 2179
    .end local v2           #fis:Ljava/io/FileInputStream;
    .local v3, fis:Ljava/io/FileInputStream;
    :try_start_11
    iget-object v4, p0, Landroid/media/MediaScanner;->mPlaylistEntries:Ljava/util/ArrayList;

    #@13
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    #@16
    .line 2180
    const-string v4, "UTF-8"

    #@18
    invoke-static {v4}, Landroid/util/Xml;->findEncodingByName(Ljava/lang/String;)Landroid/util/Xml$Encoding;

    #@1b
    move-result-object v4

    #@1c
    new-instance v5, Landroid/media/MediaScanner$WplHandler;

    #@1e
    invoke-direct {v5, p0, p2, p3, p5}, Landroid/media/MediaScanner$WplHandler;-><init>(Landroid/media/MediaScanner;Ljava/lang/String;Landroid/net/Uri;Landroid/database/Cursor;)V

    #@21
    invoke-virtual {v5}, Landroid/media/MediaScanner$WplHandler;->getContentHandler()Lorg/xml/sax/ContentHandler;

    #@24
    move-result-object v5

    #@25
    invoke-static {v3, v4, v5}, Landroid/util/Xml;->parse(Ljava/io/InputStream;Landroid/util/Xml$Encoding;Lorg/xml/sax/ContentHandler;)V

    #@28
    .line 2183
    invoke-direct {p0, p5, p4, p3}, Landroid/media/MediaScanner;->processCachedPlaylist(Landroid/database/Cursor;Landroid/content/ContentValues;Landroid/net/Uri;)V
    :try_end_2b
    .catchall {:try_start_11 .. :try_end_2b} :catchall_71
    .catch Lorg/xml/sax/SAXException; {:try_start_11 .. :try_end_2b} :catch_77
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_2b} :catch_74

    #@2b
    move-object v2, v3

    #@2c
    .line 2191
    .end local v3           #fis:Ljava/io/FileInputStream;
    .restart local v2       #fis:Ljava/io/FileInputStream;
    :cond_2c
    if-eqz v2, :cond_31

    #@2e
    .line 2192
    :try_start_2e
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_31
    .catch Ljava/io/IOException; {:try_start_2e .. :try_end_31} :catch_32

    #@31
    .line 2197
    .end local v1           #f:Ljava/io/File;
    :cond_31
    :goto_31
    return-void

    #@32
    .line 2193
    .restart local v1       #f:Ljava/io/File;
    :catch_32
    move-exception v0

    #@33
    .line 2194
    .local v0, e:Ljava/io/IOException;
    const-string v4, "MediaScanner"

    #@35
    const-string v5, "IOException in MediaScanner.processWplPlayList()"

    #@37
    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3a
    goto :goto_31

    #@3b
    .line 2185
    .end local v0           #e:Ljava/io/IOException;
    .end local v1           #f:Ljava/io/File;
    :catch_3b
    move-exception v0

    #@3c
    .line 2186
    .local v0, e:Lorg/xml/sax/SAXException;
    :goto_3c
    :try_start_3c
    invoke-virtual {v0}, Lorg/xml/sax/SAXException;->printStackTrace()V
    :try_end_3f
    .catchall {:try_start_3c .. :try_end_3f} :catchall_61

    #@3f
    .line 2191
    if-eqz v2, :cond_31

    #@41
    .line 2192
    :try_start_41
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_44
    .catch Ljava/io/IOException; {:try_start_41 .. :try_end_44} :catch_45

    #@44
    goto :goto_31

    #@45
    .line 2193
    :catch_45
    move-exception v0

    #@46
    .line 2194
    .local v0, e:Ljava/io/IOException;
    const-string v4, "MediaScanner"

    #@48
    const-string v5, "IOException in MediaScanner.processWplPlayList()"

    #@4a
    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4d
    goto :goto_31

    #@4e
    .line 2187
    .end local v0           #e:Ljava/io/IOException;
    :catch_4e
    move-exception v0

    #@4f
    .line 2188
    .restart local v0       #e:Ljava/io/IOException;
    :goto_4f
    :try_start_4f
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_52
    .catchall {:try_start_4f .. :try_end_52} :catchall_61

    #@52
    .line 2191
    if-eqz v2, :cond_31

    #@54
    .line 2192
    :try_start_54
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_57
    .catch Ljava/io/IOException; {:try_start_54 .. :try_end_57} :catch_58

    #@57
    goto :goto_31

    #@58
    .line 2193
    :catch_58
    move-exception v0

    #@59
    .line 2194
    const-string v4, "MediaScanner"

    #@5b
    const-string v5, "IOException in MediaScanner.processWplPlayList()"

    #@5d
    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@60
    goto :goto_31

    #@61
    .line 2190
    .end local v0           #e:Ljava/io/IOException;
    :catchall_61
    move-exception v4

    #@62
    .line 2191
    :goto_62
    if-eqz v2, :cond_67

    #@64
    .line 2192
    :try_start_64
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_67
    .catch Ljava/io/IOException; {:try_start_64 .. :try_end_67} :catch_68

    #@67
    .line 2195
    :cond_67
    :goto_67
    throw v4

    #@68
    .line 2193
    :catch_68
    move-exception v0

    #@69
    .line 2194
    .restart local v0       #e:Ljava/io/IOException;
    const-string v5, "MediaScanner"

    #@6b
    const-string v6, "IOException in MediaScanner.processWplPlayList()"

    #@6d
    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@70
    goto :goto_67

    #@71
    .line 2190
    .end local v0           #e:Ljava/io/IOException;
    .end local v2           #fis:Ljava/io/FileInputStream;
    .restart local v1       #f:Ljava/io/File;
    .restart local v3       #fis:Ljava/io/FileInputStream;
    :catchall_71
    move-exception v4

    #@72
    move-object v2, v3

    #@73
    .end local v3           #fis:Ljava/io/FileInputStream;
    .restart local v2       #fis:Ljava/io/FileInputStream;
    goto :goto_62

    #@74
    .line 2187
    .end local v2           #fis:Ljava/io/FileInputStream;
    .restart local v3       #fis:Ljava/io/FileInputStream;
    :catch_74
    move-exception v0

    #@75
    move-object v2, v3

    #@76
    .end local v3           #fis:Ljava/io/FileInputStream;
    .restart local v2       #fis:Ljava/io/FileInputStream;
    goto :goto_4f

    #@77
    .line 2185
    .end local v2           #fis:Ljava/io/FileInputStream;
    .restart local v3       #fis:Ljava/io/FileInputStream;
    :catch_77
    move-exception v0

    #@78
    move-object v2, v3

    #@79
    .end local v3           #fis:Ljava/io/FileInputStream;
    .restart local v2       #fis:Ljava/io/FileInputStream;
    goto :goto_3c
.end method

.method private pruneDeadThumbnailFiles()V
    .registers 16

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1543
    new-instance v9, Ljava/util/HashSet;

    #@3
    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    #@6
    .line 1544
    .local v9, existingFiles:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    const-string v8, "/sdcard/DCIM/.thumbnails"

    #@8
    .line 1545
    .local v8, directory:Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    #@a
    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@d
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    #@10
    move-result-object v11

    #@11
    .line 1546
    .local v11, files:[Ljava/lang/String;
    if-nez v11, :cond_15

    #@13
    .line 1547
    new-array v11, v1, [Ljava/lang/String;

    #@15
    .line 1549
    :cond_15
    const/4 v13, 0x0

    #@16
    .local v13, i:I
    :goto_16
    array-length v0, v11

    #@17
    if-ge v13, v0, :cond_38

    #@19
    .line 1550
    new-instance v0, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v0

    #@22
    const-string v1, "/"

    #@24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v0

    #@28
    aget-object v1, v11, v13

    #@2a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v0

    #@2e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v12

    #@32
    .line 1551
    .local v12, fullPathString:Ljava/lang/String;
    invoke-virtual {v9, v12}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@35
    .line 1549
    add-int/lit8 v13, v13, 0x1

    #@37
    goto :goto_16

    #@38
    .line 1555
    .end local v12           #fullPathString:Ljava/lang/String;
    :cond_38
    :try_start_38
    iget-object v0, p0, Landroid/media/MediaScanner;->mMediaProvider:Landroid/content/IContentProvider;

    #@3a
    iget-object v1, p0, Landroid/media/MediaScanner;->mThumbsUri:Landroid/net/Uri;

    #@3c
    const/4 v2, 0x1

    #@3d
    new-array v2, v2, [Ljava/lang/String;

    #@3f
    const/4 v3, 0x0

    #@40
    const-string v4, "_data"

    #@42
    aput-object v4, v2, v3

    #@44
    const/4 v3, 0x0

    #@45
    const/4 v4, 0x0

    #@46
    const/4 v5, 0x0

    #@47
    const/4 v6, 0x0

    #@48
    invoke-interface/range {v0 .. v6}, Landroid/content/IContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;

    #@4b
    move-result-object v7

    #@4c
    .line 1561
    .local v7, c:Landroid/database/Cursor;
    const-string v0, "MediaScanner"

    #@4e
    new-instance v1, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string/jumbo v2, "pruneDeadThumbnailFiles... "

    #@56
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v1

    #@5a
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v1

    #@5e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v1

    #@62
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@65
    .line 1562
    if-eqz v7, :cond_7b

    #@67
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    #@6a
    move-result v0

    #@6b
    if-eqz v0, :cond_7b

    #@6d
    .line 1564
    :cond_6d
    const/4 v0, 0x0

    #@6e
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@71
    move-result-object v12

    #@72
    .line 1565
    .restart local v12       #fullPathString:Ljava/lang/String;
    invoke-virtual {v9, v12}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@75
    .line 1566
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    #@78
    move-result v0

    #@79
    if-nez v0, :cond_6d

    #@7b
    .line 1569
    .end local v12           #fullPathString:Ljava/lang/String;
    :cond_7b
    invoke-virtual {v9}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@7e
    move-result-object v14

    #@7f
    .local v14, i$:Ljava/util/Iterator;
    :goto_7f
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    #@82
    move-result v0

    #@83
    if-eqz v0, :cond_96

    #@85
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@88
    move-result-object v10

    #@89
    check-cast v10, Ljava/lang/String;
    :try_end_8b
    .catch Landroid/os/RemoteException; {:try_start_38 .. :try_end_8b} :catch_b4

    #@8b
    .line 1573
    .local v10, fileToDelete:Ljava/lang/String;
    :try_start_8b
    new-instance v0, Ljava/io/File;

    #@8d
    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@90
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_93
    .catch Ljava/lang/SecurityException; {:try_start_8b .. :try_end_93} :catch_94
    .catch Landroid/os/RemoteException; {:try_start_8b .. :try_end_93} :catch_b4

    #@93
    goto :goto_7f

    #@94
    .line 1574
    :catch_94
    move-exception v0

    #@95
    goto :goto_7f

    #@96
    .line 1578
    .end local v10           #fileToDelete:Ljava/lang/String;
    :cond_96
    :try_start_96
    const-string v0, "MediaScanner"

    #@98
    new-instance v1, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    const-string v2, "/pruneDeadThumbnailFiles... "

    #@9f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v1

    #@a3
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v1

    #@a7
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@aa
    move-result-object v1

    #@ab
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@ae
    .line 1579
    if-eqz v7, :cond_b3

    #@b0
    .line 1580
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_b3
    .catch Landroid/os/RemoteException; {:try_start_96 .. :try_end_b3} :catch_b4

    #@b3
    .line 1585
    .end local v7           #c:Landroid/database/Cursor;
    .end local v14           #i$:Ljava/util/Iterator;
    :cond_b3
    :goto_b3
    return-void

    #@b4
    .line 1582
    :catch_b4
    move-exception v0

    #@b5
    goto :goto_b3
.end method

.method private setDefaultRingtoneFileNames()V
    .registers 2

    #@0
    .prologue
    .line 428
    const-string/jumbo v0, "ro.config.ringtone"

    #@3
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/media/MediaScanner;->mDefaultRingtoneFilename:Ljava/lang/String;

    #@9
    .line 430
    const-string/jumbo v0, "ro.config.notification_sound"

    #@c
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    iput-object v0, p0, Landroid/media/MediaScanner;->mDefaultNotificationFilename:Ljava/lang/String;

    #@12
    .line 432
    const-string/jumbo v0, "ro.config.alarm_alert"

    #@15
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    iput-object v0, p0, Landroid/media/MediaScanner;->mDefaultAlarmAlertFilename:Ljava/lang/String;

    #@1b
    .line 434
    return-void
.end method


# virtual methods
.method public native extractAlbumArt(Ljava/io/FileDescriptor;)[B
.end method

.method protected finalize()V
    .registers 3

    #@0
    .prologue
    .line 2294
    iget-object v0, p0, Landroid/media/MediaScanner;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v0

    #@6
    iget-object v1, p0, Landroid/media/MediaScanner;->mMediaProvider:Landroid/content/IContentProvider;

    #@8
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->releaseProvider(Landroid/content/IContentProvider;)Z

    #@b
    .line 2295
    invoke-direct {p0}, Landroid/media/MediaScanner;->native_finalize()V

    #@e
    .line 2296
    return-void
.end method

.method makeEntryFor(Ljava/lang/String;)Landroid/media/MediaScanner$FileEntry;
    .registers 16
    .parameter "path"

    #@0
    .prologue
    .line 1917
    const/4 v12, 0x0

    #@1
    .line 1919
    .local v12, c:Landroid/database/Cursor;
    :try_start_1
    const-string v0, "_"

    #@3
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_11

    #@9
    const-string v0, "%"

    #@b
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_70

    #@11
    :cond_11
    const/4 v13, 0x1

    #@12
    .line 1921
    .local v13, hasWildCards:Z
    :goto_12
    if-nez v13, :cond_18

    #@14
    iget-boolean v0, p0, Landroid/media/MediaScanner;->mCaseInsensitivePaths:Z

    #@16
    if-nez v0, :cond_72

    #@18
    .line 1926
    :cond_18
    const-string v3, "_data=?"

    #@1a
    .line 1927
    .local v3, where:Ljava/lang/String;
    const/4 v0, 0x1

    #@1b
    new-array v4, v0, [Ljava/lang/String;

    #@1d
    const/4 v0, 0x0

    #@1e
    aput-object p1, v4, v0

    #@20
    .line 1934
    .local v4, selectionArgs:[Ljava/lang/String;
    :goto_20
    iget-object v0, p0, Landroid/media/MediaScanner;->mMediaProvider:Landroid/content/IContentProvider;

    #@22
    iget-object v1, p0, Landroid/media/MediaScanner;->mFilesUri:Landroid/net/Uri;

    #@24
    sget-object v2, Landroid/media/MediaScanner;->FILES_PRESCAN_PROJECTION:[Ljava/lang/String;

    #@26
    const/4 v5, 0x0

    #@27
    const/4 v6, 0x0

    #@28
    invoke-interface/range {v0 .. v6}, Landroid/content/IContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;

    #@2b
    move-result-object v12

    #@2c
    .line 1936
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    #@2f
    move-result v0

    #@30
    if-nez v0, :cond_4f

    #@32
    if-eqz v13, :cond_4f

    #@34
    iget-boolean v0, p0, Landroid/media/MediaScanner;->mCaseInsensitivePaths:Z

    #@36
    if-eqz v0, :cond_4f

    #@38
    .line 1941
    const-string v3, "_data LIKE ?1 AND lower(_data)=lower(?1)"

    #@3a
    .line 1942
    const/4 v0, 0x1

    #@3b
    new-array v4, v0, [Ljava/lang/String;

    #@3d
    .end local v4           #selectionArgs:[Ljava/lang/String;
    const/4 v0, 0x0

    #@3e
    aput-object p1, v4, v0

    #@40
    .line 1943
    .restart local v4       #selectionArgs:[Ljava/lang/String;
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@43
    .line 1944
    iget-object v0, p0, Landroid/media/MediaScanner;->mMediaProvider:Landroid/content/IContentProvider;

    #@45
    iget-object v1, p0, Landroid/media/MediaScanner;->mFilesUri:Landroid/net/Uri;

    #@47
    sget-object v2, Landroid/media/MediaScanner;->FILES_PRESCAN_PROJECTION:[Ljava/lang/String;

    #@49
    const/4 v5, 0x0

    #@4a
    const/4 v6, 0x0

    #@4b
    invoke-interface/range {v0 .. v6}, Landroid/content/IContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;

    #@4e
    move-result-object v12

    #@4f
    .line 1949
    :cond_4f
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    #@52
    move-result v0

    #@53
    if-eqz v0, :cond_7b

    #@55
    .line 1950
    const/4 v0, 0x0

    #@56
    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    #@59
    move-result-wide v6

    #@5a
    .line 1951
    .local v6, rowId:J
    const/4 v0, 0x2

    #@5b
    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    #@5e
    move-result v11

    #@5f
    .line 1952
    .local v11, format:I
    const/4 v0, 0x3

    #@60
    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    #@63
    move-result-wide v9

    #@64
    .line 1953
    .local v9, lastModified:J
    new-instance v5, Landroid/media/MediaScanner$FileEntry;

    #@66
    move-object v8, p1

    #@67
    invoke-direct/range {v5 .. v11}, Landroid/media/MediaScanner$FileEntry;-><init>(JLjava/lang/String;JI)V
    :try_end_6a
    .catchall {:try_start_1 .. :try_end_6a} :catchall_89
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_6a} :catch_82

    #@6a
    .line 1957
    if-eqz v12, :cond_6f

    #@6c
    .line 1958
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@6f
    .line 1961
    .end local v3           #where:Ljava/lang/String;
    .end local v4           #selectionArgs:[Ljava/lang/String;
    .end local v6           #rowId:J
    .end local v9           #lastModified:J
    .end local v11           #format:I
    .end local v13           #hasWildCards:Z
    :cond_6f
    :goto_6f
    return-object v5

    #@70
    .line 1919
    :cond_70
    const/4 v13, 0x0

    #@71
    goto :goto_12

    #@72
    .line 1931
    .restart local v13       #hasWildCards:Z
    :cond_72
    :try_start_72
    const-string v3, "_data LIKE ?1 AND lower(_data)=lower(?1)"

    #@74
    .line 1932
    .restart local v3       #where:Ljava/lang/String;
    const/4 v0, 0x1

    #@75
    new-array v4, v0, [Ljava/lang/String;

    #@77
    const/4 v0, 0x0

    #@78
    aput-object p1, v4, v0
    :try_end_7a
    .catchall {:try_start_72 .. :try_end_7a} :catchall_89
    .catch Landroid/os/RemoteException; {:try_start_72 .. :try_end_7a} :catch_82

    #@7a
    .restart local v4       #selectionArgs:[Ljava/lang/String;
    goto :goto_20

    #@7b
    .line 1957
    :cond_7b
    if-eqz v12, :cond_80

    #@7d
    .line 1958
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@80
    .line 1961
    .end local v3           #where:Ljava/lang/String;
    .end local v4           #selectionArgs:[Ljava/lang/String;
    .end local v13           #hasWildCards:Z
    :cond_80
    :goto_80
    const/4 v5, 0x0

    #@81
    goto :goto_6f

    #@82
    .line 1955
    :catch_82
    move-exception v0

    #@83
    .line 1957
    if-eqz v12, :cond_80

    #@85
    .line 1958
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@88
    goto :goto_80

    #@89
    .line 1957
    :catchall_89
    move-exception v0

    #@8a
    if-eqz v12, :cond_8f

    #@8c
    .line 1958
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@8f
    :cond_8f
    throw v0
.end method

.method public release()V
    .registers 1

    #@0
    .prologue
    .line 2289
    invoke-direct {p0}, Landroid/media/MediaScanner;->native_finalize()V

    #@3
    .line 2290
    return-void
.end method

.method public scanDirectories([Ljava/lang/String;Ljava/lang/String;)V
    .registers 14
    .parameter "directories"
    .parameter "volumeName"

    #@0
    .prologue
    .line 1665
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3
    move-result-wide v6

    #@4
    .line 1666
    .local v6, start:J
    invoke-direct {p0, p2}, Landroid/media/MediaScanner;->initialize(Ljava/lang/String;)V

    #@7
    .line 1667
    const/4 v8, 0x0

    #@8
    const/4 v9, 0x1

    #@9
    invoke-direct {p0, v8, v9}, Landroid/media/MediaScanner;->prescan(Ljava/lang/String;Z)V

    #@c
    .line 1668
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@f
    move-result-wide v2

    #@10
    .line 1673
    .local v2, prescan:J
    new-instance v8, Landroid/media/MediaInserter;

    #@12
    iget-object v9, p0, Landroid/media/MediaScanner;->mMediaProvider:Landroid/content/IContentProvider;

    #@14
    const/16 v10, 0x64

    #@16
    invoke-direct {v8, v9, v10}, Landroid/media/MediaInserter;-><init>(Landroid/content/IContentProvider;I)V

    #@19
    iput-object v8, p0, Landroid/media/MediaScanner;->mMediaInserter:Landroid/media/MediaInserter;

    #@1b
    .line 1677
    const/4 v1, 0x0

    #@1c
    .local v1, i:I
    :goto_1c
    array-length v8, p1

    #@1d
    if-ge v1, v8, :cond_29

    #@1f
    .line 1678
    aget-object v8, p1, v1

    #@21
    iget-object v9, p0, Landroid/media/MediaScanner;->mClient:Landroid/media/MediaScanner$MyMediaScannerClient;

    #@23
    invoke-direct {p0, v8, v9}, Landroid/media/MediaScanner;->processDirectory(Ljava/lang/String;Landroid/media/MediaScannerClient;)V

    #@26
    .line 1677
    add-int/lit8 v1, v1, 0x1

    #@28
    goto :goto_1c

    #@29
    .line 1683
    :cond_29
    iget-object v8, p0, Landroid/media/MediaScanner;->mMediaInserter:Landroid/media/MediaInserter;

    #@2b
    invoke-virtual {v8}, Landroid/media/MediaInserter;->flushAll()V

    #@2e
    .line 1684
    const/4 v8, 0x0

    #@2f
    iput-object v8, p0, Landroid/media/MediaScanner;->mMediaInserter:Landroid/media/MediaInserter;

    #@31
    .line 1687
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@34
    move-result-wide v4

    #@35
    .line 1688
    .local v4, scan:J
    invoke-direct {p0, p1}, Landroid/media/MediaScanner;->postscan([Ljava/lang/String;)V

    #@38
    .line 1689
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_3b
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_3b} :catch_3c
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_3b} :catch_45
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_3b} :catch_4e

    #@3b
    .line 1706
    .end local v1           #i:I
    .end local v2           #prescan:J
    .end local v4           #scan:J
    .end local v6           #start:J
    :goto_3b
    return-void

    #@3c
    .line 1697
    :catch_3c
    move-exception v0

    #@3d
    .line 1699
    .local v0, e:Landroid/database/SQLException;
    const-string v8, "MediaScanner"

    #@3f
    const-string v9, "SQLException in MediaScanner.scan()"

    #@41
    invoke-static {v8, v9, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@44
    goto :goto_3b

    #@45
    .line 1700
    .end local v0           #e:Landroid/database/SQLException;
    :catch_45
    move-exception v0

    #@46
    .line 1702
    .local v0, e:Ljava/lang/UnsupportedOperationException;
    const-string v8, "MediaScanner"

    #@48
    const-string v9, "UnsupportedOperationException in MediaScanner.scan()"

    #@4a
    invoke-static {v8, v9, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4d
    goto :goto_3b

    #@4e
    .line 1703
    .end local v0           #e:Ljava/lang/UnsupportedOperationException;
    :catch_4e
    move-exception v0

    #@4f
    .line 1704
    .local v0, e:Landroid/os/RemoteException;
    const-string v8, "MediaScanner"

    #@51
    const-string v9, "RemoteException in MediaScanner.scan()"

    #@53
    invoke-static {v8, v9, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@56
    goto :goto_3b
.end method

.method public scanMtpFile(Ljava/lang/String;Ljava/lang/String;II)V
    .registers 26
    .parameter "path"
    .parameter "volumeName"
    .parameter "objectHandle"
    .parameter "format"

    #@0
    .prologue
    .line 1856
    move-object/from16 v0, p0

    #@2
    move-object/from16 v1, p2

    #@4
    invoke-direct {v0, v1}, Landroid/media/MediaScanner;->initialize(Ljava/lang/String;)V

    #@7
    .line 1857
    invoke-static/range {p1 .. p1}, Landroid/media/MediaFile;->getFileType(Ljava/lang/String;)Landroid/media/MediaFile$MediaFileType;

    #@a
    move-result-object v18

    #@b
    .line 1858
    .local v18, mediaFileType:Landroid/media/MediaFile$MediaFileType;
    if-nez v18, :cond_79

    #@d
    const/16 v17, 0x0

    #@f
    .line 1859
    .local v17, fileType:I
    :goto_f
    new-instance v15, Ljava/io/File;

    #@11
    move-object/from16 v0, p1

    #@13
    invoke-direct {v15, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@16
    .line 1860
    .local v15, file:Ljava/io/File;
    invoke-virtual {v15}, Ljava/io/File;->lastModified()J

    #@19
    move-result-wide v2

    #@1a
    const-wide/16 v7, 0x3e8

    #@1c
    div-long v5, v2, v7

    #@1e
    .line 1862
    .local v5, lastModifiedSeconds:J
    invoke-static/range {v17 .. v17}, Landroid/media/MediaFile;->isAudioFileType(I)Z

    #@21
    move-result v2

    #@22
    if-nez v2, :cond_89

    #@24
    invoke-static/range {v17 .. v17}, Landroid/media/MediaFile;->isVideoFileType(I)Z

    #@27
    move-result v2

    #@28
    if-nez v2, :cond_89

    #@2a
    invoke-static/range {v17 .. v17}, Landroid/media/MediaFile;->isImageFileType(I)Z

    #@2d
    move-result v2

    #@2e
    if-nez v2, :cond_89

    #@30
    invoke-static/range {v17 .. v17}, Landroid/media/MediaFile;->isPlayListFileType(I)Z

    #@33
    move-result v2

    #@34
    if-nez v2, :cond_89

    #@36
    invoke-static/range {v17 .. v17}, Landroid/media/MediaFile;->isDrmFileType(I)Z

    #@39
    move-result v2

    #@3a
    if-nez v2, :cond_89

    #@3c
    .line 1867
    new-instance v19, Landroid/content/ContentValues;

    #@3e
    invoke-direct/range {v19 .. v19}, Landroid/content/ContentValues;-><init>()V

    #@41
    .line 1868
    .local v19, values:Landroid/content/ContentValues;
    const-string v2, "_size"

    #@43
    invoke-virtual {v15}, Ljava/io/File;->length()J

    #@46
    move-result-wide v3

    #@47
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@4a
    move-result-object v3

    #@4b
    move-object/from16 v0, v19

    #@4d
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@50
    .line 1869
    const-string v2, "date_modified"

    #@52
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@55
    move-result-object v3

    #@56
    move-object/from16 v0, v19

    #@58
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@5b
    .line 1871
    const/4 v2, 0x1

    #@5c
    :try_start_5c
    new-array v0, v2, [Ljava/lang/String;

    #@5e
    move-object/from16 v20, v0

    #@60
    const/4 v2, 0x0

    #@61
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@64
    move-result-object v3

    #@65
    aput-object v3, v20, v2

    #@67
    .line 1872
    .local v20, whereArgs:[Ljava/lang/String;
    move-object/from16 v0, p0

    #@69
    iget-object v2, v0, Landroid/media/MediaScanner;->mMediaProvider:Landroid/content/IContentProvider;

    #@6b
    invoke-static/range {p2 .. p2}, Landroid/provider/MediaStore$Files;->getMtpObjectsUri(Ljava/lang/String;)Landroid/net/Uri;

    #@6e
    move-result-object v3

    #@6f
    const-string v4, "_id=?"

    #@71
    move-object/from16 v0, v19

    #@73
    move-object/from16 v1, v20

    #@75
    invoke-interface {v2, v3, v0, v4, v1}, Landroid/content/IContentProvider;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_78
    .catch Landroid/os/RemoteException; {:try_start_5c .. :try_end_78} :catch_80

    #@78
    .line 1911
    .end local v5           #lastModifiedSeconds:J
    .end local v19           #values:Landroid/content/ContentValues;
    .end local v20           #whereArgs:[Ljava/lang/String;
    :cond_78
    :goto_78
    return-void

    #@79
    .line 1858
    .end local v15           #file:Ljava/io/File;
    .end local v17           #fileType:I
    :cond_79
    move-object/from16 v0, v18

    #@7b
    iget v0, v0, Landroid/media/MediaFile$MediaFileType;->fileType:I

    #@7d
    move/from16 v17, v0

    #@7f
    goto :goto_f

    #@80
    .line 1874
    .restart local v5       #lastModifiedSeconds:J
    .restart local v15       #file:Ljava/io/File;
    .restart local v17       #fileType:I
    .restart local v19       #values:Landroid/content/ContentValues;
    :catch_80
    move-exception v13

    #@81
    .line 1875
    .local v13, e:Landroid/os/RemoteException;
    const-string v2, "MediaScanner"

    #@83
    const-string v3, "RemoteException in scanMtpFile"

    #@85
    invoke-static {v2, v3, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@88
    goto :goto_78

    #@89
    .line 1880
    .end local v13           #e:Landroid/os/RemoteException;
    .end local v19           #values:Landroid/content/ContentValues;
    :cond_89
    move/from16 v0, p3

    #@8b
    move-object/from16 v1, p0

    #@8d
    iput v0, v1, Landroid/media/MediaScanner;->mMtpObjectHandle:I

    #@8f
    .line 1881
    const/16 v16, 0x0

    #@91
    .line 1883
    .local v16, fileList:Landroid/database/Cursor;
    :try_start_91
    invoke-static/range {v17 .. v17}, Landroid/media/MediaFile;->isPlayListFileType(I)Z

    #@94
    move-result v2

    #@95
    if-eqz v2, :cond_c8

    #@97
    .line 1885
    const/4 v2, 0x0

    #@98
    const/4 v3, 0x1

    #@99
    move-object/from16 v0, p0

    #@9b
    invoke-direct {v0, v2, v3}, Landroid/media/MediaScanner;->prescan(Ljava/lang/String;Z)V

    #@9e
    .line 1887
    invoke-virtual/range {p0 .. p1}, Landroid/media/MediaScanner;->makeEntryFor(Ljava/lang/String;)Landroid/media/MediaScanner$FileEntry;

    #@a1
    move-result-object v14

    #@a2
    .line 1888
    .local v14, entry:Landroid/media/MediaScanner$FileEntry;
    if-eqz v14, :cond_bd

    #@a4
    .line 1889
    move-object/from16 v0, p0

    #@a6
    iget-object v2, v0, Landroid/media/MediaScanner;->mMediaProvider:Landroid/content/IContentProvider;

    #@a8
    move-object/from16 v0, p0

    #@aa
    iget-object v3, v0, Landroid/media/MediaScanner;->mFilesUri:Landroid/net/Uri;

    #@ac
    sget-object v4, Landroid/media/MediaScanner;->FILES_PRESCAN_PROJECTION:[Ljava/lang/String;

    #@ae
    const/4 v5, 0x0

    #@af
    const/4 v6, 0x0

    #@b0
    const/4 v7, 0x0

    #@b1
    const/4 v8, 0x0

    #@b2
    invoke-interface/range {v2 .. v8}, Landroid/content/IContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;

    #@b5
    .end local v5           #lastModifiedSeconds:J
    move-result-object v16

    #@b6
    .line 1891
    move-object/from16 v0, p0

    #@b8
    move-object/from16 v1, v16

    #@ba
    invoke-direct {v0, v14, v1}, Landroid/media/MediaScanner;->processPlayList(Landroid/media/MediaScanner$FileEntry;Landroid/database/Cursor;)V
    :try_end_bd
    .catchall {:try_start_91 .. :try_end_bd} :catchall_105
    .catch Landroid/os/RemoteException; {:try_start_91 .. :try_end_bd} :catch_ef

    #@bd
    .line 1906
    .end local v14           #entry:Landroid/media/MediaScanner$FileEntry;
    :cond_bd
    :goto_bd
    const/4 v2, 0x0

    #@be
    move-object/from16 v0, p0

    #@c0
    iput v2, v0, Landroid/media/MediaScanner;->mMtpObjectHandle:I

    #@c2
    .line 1907
    if-eqz v16, :cond_78

    #@c4
    .line 1908
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    #@c7
    goto :goto_78

    #@c8
    .line 1895
    .restart local v5       #lastModifiedSeconds:J
    :cond_c8
    const/4 v2, 0x0

    #@c9
    :try_start_c9
    move-object/from16 v0, p0

    #@cb
    move-object/from16 v1, p1

    #@cd
    invoke-direct {v0, v1, v2}, Landroid/media/MediaScanner;->prescan(Ljava/lang/String;Z)V

    #@d0
    .line 1899
    move-object/from16 v0, p0

    #@d2
    iget-object v2, v0, Landroid/media/MediaScanner;->mClient:Landroid/media/MediaScanner$MyMediaScannerClient;

    #@d4
    move-object/from16 v0, v18

    #@d6
    iget-object v4, v0, Landroid/media/MediaFile$MediaFileType;->mimeType:Ljava/lang/String;

    #@d8
    invoke-virtual {v15}, Ljava/io/File;->length()J

    #@db
    move-result-wide v7

    #@dc
    const/16 v3, 0x3001

    #@de
    move/from16 v0, p4

    #@e0
    if-ne v0, v3, :cond_103

    #@e2
    const/4 v9, 0x1

    #@e3
    :goto_e3
    const/4 v10, 0x1

    #@e4
    invoke-static/range {p1 .. p1}, Landroid/media/MediaScanner;->isNoMediaPath(Ljava/lang/String;)Z

    #@e7
    move-result v11

    #@e8
    const/4 v12, 0x0

    #@e9
    move-object/from16 v3, p1

    #@eb
    invoke-virtual/range {v2 .. v12}, Landroid/media/MediaScanner$MyMediaScannerClient;->doScanFile(Ljava/lang/String;Ljava/lang/String;JJZZZZ)Landroid/net/Uri;
    :try_end_ee
    .catchall {:try_start_c9 .. :try_end_ee} :catchall_105
    .catch Landroid/os/RemoteException; {:try_start_c9 .. :try_end_ee} :catch_ef

    #@ee
    goto :goto_bd

    #@ef
    .line 1903
    .end local v5           #lastModifiedSeconds:J
    :catch_ef
    move-exception v13

    #@f0
    .line 1904
    .restart local v13       #e:Landroid/os/RemoteException;
    :try_start_f0
    const-string v2, "MediaScanner"

    #@f2
    const-string v3, "RemoteException in MediaScanner.scanFile()"

    #@f4
    invoke-static {v2, v3, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_f7
    .catchall {:try_start_f0 .. :try_end_f7} :catchall_105

    #@f7
    .line 1906
    const/4 v2, 0x0

    #@f8
    move-object/from16 v0, p0

    #@fa
    iput v2, v0, Landroid/media/MediaScanner;->mMtpObjectHandle:I

    #@fc
    .line 1907
    if-eqz v16, :cond_78

    #@fe
    .line 1908
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    #@101
    goto/16 :goto_78

    #@103
    .line 1899
    .end local v13           #e:Landroid/os/RemoteException;
    .restart local v5       #lastModifiedSeconds:J
    :cond_103
    const/4 v9, 0x0

    #@104
    goto :goto_e3

    #@105
    .line 1906
    .end local v5           #lastModifiedSeconds:J
    :catchall_105
    move-exception v2

    #@106
    const/4 v3, 0x0

    #@107
    move-object/from16 v0, p0

    #@109
    iput v3, v0, Landroid/media/MediaScanner;->mMtpObjectHandle:I

    #@10b
    .line 1907
    if-eqz v16, :cond_110

    #@10d
    .line 1908
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    #@110
    :cond_110
    throw v2
.end method

.method public scanSingleFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .registers 28
    .parameter "path"
    .parameter "volumeName"
    .parameter "mimeType"

    #@0
    .prologue
    .line 1711
    :try_start_0
    move-object/from16 v0, p0

    #@2
    move-object/from16 v1, p2

    #@4
    invoke-direct {v0, v1}, Landroid/media/MediaScanner;->initialize(Ljava/lang/String;)V

    #@7
    .line 1717
    new-instance v16, Ljava/io/File;

    #@9
    move-object/from16 v0, v16

    #@b
    move-object/from16 v1, p1

    #@d
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@10
    .line 1719
    .local v16, f:Ljava/io/File;
    if-eqz v16, :cond_c9

    #@12
    .line 1720
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->getParentFile()Ljava/io/File;

    #@15
    move-result-object v16

    #@16
    .line 1721
    if-eqz v16, :cond_c9

    #@18
    .line 1722
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@1b
    move-result-object v20

    #@1c
    .line 1723
    .local v20, parentpath:Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->getParentFile()Ljava/io/File;

    #@1f
    move-result-object v16

    #@20
    .line 1724
    if-eqz v16, :cond_c9

    #@22
    .line 1725
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@25
    move-result-object v22

    #@26
    .line 1727
    .local v22, rootpath:Ljava/lang/String;
    move-object/from16 v0, p0

    #@28
    iget-object v2, v0, Landroid/media/MediaScanner;->mExternalStoragePath:Ljava/lang/String;

    #@2a
    move-object/from16 v0, v22

    #@2c
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f
    move-result v2

    #@30
    if-eqz v2, :cond_c9

    #@32
    .line 1728
    new-instance v14, Ljava/io/File;

    #@34
    move-object/from16 v0, p0

    #@36
    iget-object v2, v0, Landroid/media/MediaScanner;->mExternalStoragePath:Ljava/lang/String;

    #@38
    invoke-direct {v14, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@3b
    .line 1729
    .local v14, directory:Ljava/io/File;
    new-instance v2, Landroid/media/MediaScanner$1;

    #@3d
    move-object/from16 v0, p0

    #@3f
    invoke-direct {v2, v0}, Landroid/media/MediaScanner$1;-><init>(Landroid/media/MediaScanner;)V

    #@42
    invoke-virtual {v14, v2}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    #@45
    move-result-object v13

    #@46
    .line 1735
    .local v13, childs:[Ljava/io/File;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    #@49
    move-result v18

    #@4a
    .line 1736
    .local v18, filelen:I
    if-eqz v13, :cond_c9

    #@4c
    .line 1737
    const/16 v19, 0x0

    #@4e
    .local v19, i:I
    :goto_4e
    array-length v2, v13

    #@4f
    move/from16 v0, v19

    #@51
    if-ge v0, v2, :cond_c9

    #@53
    .line 1738
    aget-object v2, v13, v19

    #@55
    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@58
    move-result-object v21

    #@59
    .line 1739
    .local v21, realpath:Ljava/lang/String;
    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@5c
    move-result v2

    #@5d
    if-eqz v2, :cond_f7

    #@5f
    .line 1740
    const-string v2, "MediaScanner"

    #@61
    new-instance v3, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    const-string/jumbo v4, "scanSingleFile()  childs["

    #@69
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v3

    #@6d
    move/from16 v0, v19

    #@6f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@72
    move-result-object v3

    #@73
    const-string v4, " of "

    #@75
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v3

    #@79
    array-length v4, v13

    #@7a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v3

    #@7e
    const-string v4, "] : "

    #@80
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v3

    #@84
    aget-object v4, v13, v19

    #@86
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v3

    #@8a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8d
    move-result-object v3

    #@8e
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@91
    .line 1741
    new-instance v23, Ljava/lang/StringBuffer;

    #@93
    move-object/from16 v0, v23

    #@95
    move-object/from16 v1, p1

    #@97
    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    #@9a
    .line 1742
    .local v23, sb:Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    #@9b
    move-object/from16 v0, v23

    #@9d
    move/from16 v1, v18

    #@9f
    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    #@a2
    .line 1743
    const/4 v2, 0x0

    #@a3
    aget-object v3, v13, v19

    #@a5
    move-object/from16 v0, v23

    #@a7
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->insert(ILjava/lang/Object;)Ljava/lang/StringBuffer;

    #@aa
    .line 1744
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@ad
    move-result-object p1

    #@ae
    .line 1745
    const-string v2, "MediaScanner"

    #@b0
    new-instance v3, Ljava/lang/StringBuilder;

    #@b2
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b5
    const-string/jumbo v4, "scanSingleFile() new path : "

    #@b8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v3

    #@bc
    move-object/from16 v0, p1

    #@be
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v3

    #@c2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c5
    move-result-object v3

    #@c6
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@c9
    .line 1755
    .end local v13           #childs:[Ljava/io/File;
    .end local v14           #directory:Ljava/io/File;
    .end local v18           #filelen:I
    .end local v19           #i:I
    .end local v20           #parentpath:Ljava/lang/String;
    .end local v21           #realpath:Ljava/lang/String;
    .end local v22           #rootpath:Ljava/lang/String;
    .end local v23           #sb:Ljava/lang/StringBuffer;
    :cond_c9
    const/4 v2, 0x1

    #@ca
    move-object/from16 v0, p0

    #@cc
    move-object/from16 v1, p1

    #@ce
    invoke-direct {v0, v1, v2}, Landroid/media/MediaScanner;->prescan(Ljava/lang/String;Z)V

    #@d1
    .line 1757
    new-instance v17, Ljava/io/File;

    #@d3
    move-object/from16 v0, v17

    #@d5
    move-object/from16 v1, p1

    #@d7
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@da
    .line 1760
    .local v17, file:Ljava/io/File;
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->lastModified()J

    #@dd
    move-result-wide v2

    #@de
    const-wide/16 v7, 0x3e8

    #@e0
    div-long v5, v2, v7

    #@e2
    .line 1764
    .local v5, lastModifiedSeconds:J
    move-object/from16 v0, p0

    #@e4
    iget-object v2, v0, Landroid/media/MediaScanner;->mClient:Landroid/media/MediaScanner$MyMediaScannerClient;

    #@e6
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->length()J

    #@e9
    move-result-wide v7

    #@ea
    const/4 v9, 0x0

    #@eb
    const/4 v10, 0x1

    #@ec
    const/4 v11, 0x0

    #@ed
    const/4 v12, 0x0

    #@ee
    move-object/from16 v3, p1

    #@f0
    move-object/from16 v4, p3

    #@f2
    invoke-virtual/range {v2 .. v12}, Landroid/media/MediaScanner$MyMediaScannerClient;->doScanFile(Ljava/lang/String;Ljava/lang/String;JJZZZZ)Landroid/net/Uri;
    :try_end_f5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_f5} :catch_fb

    #@f5
    move-result-object v2

    #@f6
    .line 1769
    .end local v5           #lastModifiedSeconds:J
    .end local v16           #f:Ljava/io/File;
    .end local v17           #file:Ljava/io/File;
    :goto_f6
    return-object v2

    #@f7
    .line 1737
    .restart local v13       #childs:[Ljava/io/File;
    .restart local v14       #directory:Ljava/io/File;
    .restart local v16       #f:Ljava/io/File;
    .restart local v18       #filelen:I
    .restart local v19       #i:I
    .restart local v20       #parentpath:Ljava/lang/String;
    .restart local v21       #realpath:Ljava/lang/String;
    .restart local v22       #rootpath:Ljava/lang/String;
    :cond_f7
    add-int/lit8 v19, v19, 0x1

    #@f9
    goto/16 :goto_4e

    #@fb
    .line 1767
    .end local v13           #childs:[Ljava/io/File;
    .end local v14           #directory:Ljava/io/File;
    .end local v16           #f:Ljava/io/File;
    .end local v18           #filelen:I
    .end local v19           #i:I
    .end local v20           #parentpath:Ljava/lang/String;
    .end local v21           #realpath:Ljava/lang/String;
    .end local v22           #rootpath:Ljava/lang/String;
    :catch_fb
    move-exception v15

    #@fc
    .line 1768
    .local v15, e:Landroid/os/RemoteException;
    const-string v2, "MediaScanner"

    #@fe
    const-string v3, "RemoteException in MediaScanner.scanFile()"

    #@100
    invoke-static {v2, v3, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@103
    .line 1769
    const/4 v2, 0x0

    #@104
    goto :goto_f6
.end method

.method public scanSingleFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/net/Uri;
    .registers 18
    .parameter "path"
    .parameter "volumeName"
    .parameter "mimeType"
    .parameter "checknomedia"

    #@0
    .prologue
    .line 1777
    :try_start_0
    invoke-direct {p0, p2}, Landroid/media/MediaScanner;->initialize(Ljava/lang/String;)V

    #@3
    .line 1778
    const/4 v0, 0x1

    #@4
    invoke-direct {p0, p1, v0}, Landroid/media/MediaScanner;->prescan(Ljava/lang/String;Z)V

    #@7
    .line 1780
    new-instance v12, Ljava/io/File;

    #@9
    invoke-direct {v12, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@c
    .line 1783
    .local v12, file:Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->lastModified()J

    #@f
    move-result-wide v0

    #@10
    const-wide/16 v5, 0x3e8

    #@12
    div-long v3, v0, v5

    #@14
    .line 1787
    .local v3, lastModifiedSeconds:J
    iget-object v0, p0, Landroid/media/MediaScanner;->mClient:Landroid/media/MediaScanner$MyMediaScannerClient;

    #@16
    invoke-virtual {v12}, Ljava/io/File;->length()J

    #@19
    move-result-wide v5

    #@1a
    const/4 v7, 0x0

    #@1b
    const/4 v8, 0x1

    #@1c
    const/4 v10, 0x0

    #@1d
    move-object v1, p1

    #@1e
    move-object/from16 v2, p3

    #@20
    move/from16 v9, p4

    #@22
    invoke-virtual/range {v0 .. v10}, Landroid/media/MediaScanner$MyMediaScannerClient;->doScanFile(Ljava/lang/String;Ljava/lang/String;JJZZZZ)Landroid/net/Uri;
    :try_end_25
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_25} :catch_27

    #@25
    move-result-object v0

    #@26
    .line 1792
    .end local v3           #lastModifiedSeconds:J
    .end local v12           #file:Ljava/io/File;
    :goto_26
    return-object v0

    #@27
    .line 1790
    :catch_27
    move-exception v11

    #@28
    .line 1791
    .local v11, e:Landroid/os/RemoteException;
    const-string v0, "MediaScanner"

    #@2a
    const-string v1, "RemoteException in MediaScanner.scanFile()"

    #@2c
    invoke-static {v0, v1, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2f
    .line 1792
    const/4 v0, 0x0

    #@30
    goto :goto_26
.end method

.method public native setLocale(Ljava/lang/String;)V
.end method
