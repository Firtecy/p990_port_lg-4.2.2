.class Landroid/media/AudioService$5;
.super Landroid/content/BroadcastReceiver;
.source "AudioService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/AudioService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/media/AudioService;


# direct methods
.method constructor <init>(Landroid/media/AudioService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 5505
    iput-object p1, p0, Landroid/media/AudioService$5;->this$0:Landroid/media/AudioService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 5
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 5507
    if-nez p2, :cond_3

    #@2
    .line 5517
    :cond_2
    :goto_2
    return-void

    #@3
    .line 5510
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    #@6
    move-result-object v0

    #@7
    .line 5511
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_2

    #@9
    .line 5514
    const-string v1, "android.media.AudioService.WAKELOCK_ACQUIRED"

    #@b
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_2

    #@11
    .line 5515
    iget-object v1, p0, Landroid/media/AudioService$5;->this$0:Landroid/media/AudioService;

    #@13
    invoke-static {v1}, Landroid/media/AudioService;->access$6700(Landroid/media/AudioService;)Landroid/os/PowerManager$WakeLock;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    #@1a
    goto :goto_2
.end method
