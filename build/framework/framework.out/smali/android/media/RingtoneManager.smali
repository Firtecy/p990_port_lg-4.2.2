.class public Landroid/media/RingtoneManager;
.super Ljava/lang/Object;
.source "RingtoneManager.java"


# static fields
.field public static final ACTION_RINGTONE_PICKER:Ljava/lang/String; = "android.intent.action.RINGTONE_PICKER"

.field private static final DEFAULT_ALARM_PATH:Ljava/lang/String; = null

.field private static final DEFAULT_NOTIFICATION_PATH:Ljava/lang/String; = null

.field private static final DEFAULT_RINGTONE_PATH:Ljava/lang/String; = null

.field private static final DRM_COLUMNS:[Ljava/lang/String; = null

.field public static final EXTRA_RINGTONE_DEFAULT_URI:Ljava/lang/String; = "android.intent.extra.ringtone.DEFAULT_URI"

.field public static final EXTRA_RINGTONE_EXISTING_URI:Ljava/lang/String; = "android.intent.extra.ringtone.EXISTING_URI"

.field public static final EXTRA_RINGTONE_INCLUDE_DRM:Ljava/lang/String; = "android.intent.extra.ringtone.INCLUDE_DRM"

.field public static final EXTRA_RINGTONE_PICKED_URI:Ljava/lang/String; = "android.intent.extra.ringtone.PICKED_URI"

.field public static final EXTRA_RINGTONE_SHOW_DEFAULT:Ljava/lang/String; = "android.intent.extra.ringtone.SHOW_DEFAULT"

.field public static final EXTRA_RINGTONE_SHOW_SILENT:Ljava/lang/String; = "android.intent.extra.ringtone.SHOW_SILENT"

.field public static final EXTRA_RINGTONE_TITLE:Ljava/lang/String; = "android.intent.extra.ringtone.TITLE"

.field public static final EXTRA_RINGTONE_TYPE:Ljava/lang/String; = "android.intent.extra.ringtone.TYPE"

.field public static final ID_COLUMN_INDEX:I = 0x0

.field private static final INTERNAL_COLUMNS:[Ljava/lang/String; = null

.field private static final MEDIA_COLUMNS:[Ljava/lang/String; = null

.field private static final TAG:Ljava/lang/String; = "RingtoneManager"

.field public static final TITLE_COLUMN_INDEX:I = 0x1

.field public static final TYPE_ALARM:I = 0x4

.field public static final TYPE_ALL:I = 0x7

.field public static final TYPE_NOTIFICATION:I = 0x2

.field public static final TYPE_NOTIFICATION_SIM2:I = 0x10

.field public static final TYPE_RINGTONE:I = 0x1

.field public static final TYPE_RINGTONE_SIM2:I = 0x8

.field public static final TYPE_RINGTONE_VIDEOCALL:I = 0x20

.field public static final URI_COLUMN_INDEX:I = 0x2

.field private static mDrmManagerClient:Landroid/drm/DrmManagerClient;


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mContext:Landroid/content/Context;

.field private mCursor:Landroid/database/Cursor;

.field private final mFilterColumns:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mIncludeDrm:Z

.field private mPreviousRingtone:Landroid/media/Ringtone;

.field private mStopPreviousRingtone:Z

.field private mType:I


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 206
    const/4 v0, 0x5

    #@6
    new-array v0, v0, [Ljava/lang/String;

    #@8
    const-string v1, "_id"

    #@a
    aput-object v1, v0, v3

    #@c
    const-string/jumbo v1, "title"

    #@f
    aput-object v1, v0, v4

    #@11
    new-instance v1, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v2, "\""

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    sget-object v2, Landroid/provider/MediaStore$Audio$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    const-string v2, "\""

    #@24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v1

    #@2c
    aput-object v1, v0, v5

    #@2e
    const-string/jumbo v1, "title_key"

    #@31
    aput-object v1, v0, v6

    #@33
    const-string v1, "_display_name"

    #@35
    aput-object v1, v0, v7

    #@37
    sput-object v0, Landroid/media/RingtoneManager;->INTERNAL_COLUMNS:[Ljava/lang/String;

    #@39
    .line 215
    new-array v0, v7, [Ljava/lang/String;

    #@3b
    const-string v1, "_id"

    #@3d
    aput-object v1, v0, v3

    #@3f
    const-string/jumbo v1, "title"

    #@42
    aput-object v1, v0, v4

    #@44
    new-instance v1, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v2, "\""

    #@4b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v1

    #@4f
    sget-object v2, Landroid/provider/DrmStore$Audio;->CONTENT_URI:Landroid/net/Uri;

    #@51
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v1

    #@55
    const-string v2, "\""

    #@57
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v1

    #@5b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v1

    #@5f
    aput-object v1, v0, v5

    #@61
    const-string/jumbo v1, "title AS title_key"

    #@64
    aput-object v1, v0, v6

    #@66
    sput-object v0, Landroid/media/RingtoneManager;->DRM_COLUMNS:[Ljava/lang/String;

    #@68
    .line 221
    const/4 v0, 0x5

    #@69
    new-array v0, v0, [Ljava/lang/String;

    #@6b
    const-string v1, "_id"

    #@6d
    aput-object v1, v0, v3

    #@6f
    const-string/jumbo v1, "title"

    #@72
    aput-object v1, v0, v4

    #@74
    new-instance v1, Ljava/lang/StringBuilder;

    #@76
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@79
    const-string v2, "\""

    #@7b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v1

    #@7f
    sget-object v2, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    #@81
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v1

    #@85
    const-string v2, "\""

    #@87
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v1

    #@8b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v1

    #@8f
    aput-object v1, v0, v5

    #@91
    const-string/jumbo v1, "title_key"

    #@94
    aput-object v1, v0, v6

    #@96
    const-string v1, "_display_name"

    #@98
    aput-object v1, v0, v7

    #@9a
    sput-object v0, Landroid/media/RingtoneManager;->MEDIA_COLUMNS:[Ljava/lang/String;

    #@9c
    .line 267
    new-instance v0, Ljava/lang/StringBuilder;

    #@9e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@a1
    const-string v1, "/system/media/audio/ringtones/"

    #@a3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v0

    #@a7
    const-string/jumbo v1, "ro.config.ringtone"

    #@aa
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@ad
    move-result-object v1

    #@ae
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v0

    #@b2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b5
    move-result-object v0

    #@b6
    sput-object v0, Landroid/media/RingtoneManager;->DEFAULT_RINGTONE_PATH:Ljava/lang/String;

    #@b8
    .line 268
    new-instance v0, Ljava/lang/StringBuilder;

    #@ba
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@bd
    const-string v1, "/system/media/audio/alarms/"

    #@bf
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v0

    #@c3
    const-string/jumbo v1, "ro.config.alarm_alert"

    #@c6
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@c9
    move-result-object v1

    #@ca
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v0

    #@ce
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d1
    move-result-object v0

    #@d2
    sput-object v0, Landroid/media/RingtoneManager;->DEFAULT_ALARM_PATH:Ljava/lang/String;

    #@d4
    .line 269
    new-instance v0, Ljava/lang/StringBuilder;

    #@d6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d9
    const-string v1, "/system/media/audio/notifications/"

    #@db
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v0

    #@df
    const-string/jumbo v1, "ro.config.notification_sound"

    #@e2
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@e5
    move-result-object v1

    #@e6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v0

    #@ea
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ed
    move-result-object v0

    #@ee
    sput-object v0, Landroid/media/RingtoneManager;->DEFAULT_NOTIFICATION_PATH:Ljava/lang/String;

    #@f0
    .line 270
    const/4 v0, 0x0

    #@f1
    sput-object v0, Landroid/media/RingtoneManager;->mDrmManagerClient:Landroid/drm/DrmManagerClient;

    #@f3
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .registers 4
    .parameter "activity"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 278
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 253
    iput v1, p0, Landroid/media/RingtoneManager;->mType:I

    #@6
    .line 259
    new-instance v0, Ljava/util/ArrayList;

    #@8
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@b
    iput-object v0, p0, Landroid/media/RingtoneManager;->mFilterColumns:Ljava/util/List;

    #@d
    .line 261
    iput-boolean v1, p0, Landroid/media/RingtoneManager;->mStopPreviousRingtone:Z

    #@f
    .line 279
    iput-object p1, p0, Landroid/media/RingtoneManager;->mActivity:Landroid/app/Activity;

    #@11
    iput-object p1, p0, Landroid/media/RingtoneManager;->mContext:Landroid/content/Context;

    #@13
    .line 280
    iget v0, p0, Landroid/media/RingtoneManager;->mType:I

    #@15
    invoke-virtual {p0, v0}, Landroid/media/RingtoneManager;->setType(I)V

    #@18
    .line 281
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 290
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 253
    iput v1, p0, Landroid/media/RingtoneManager;->mType:I

    #@6
    .line 259
    new-instance v0, Ljava/util/ArrayList;

    #@8
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@b
    iput-object v0, p0, Landroid/media/RingtoneManager;->mFilterColumns:Ljava/util/List;

    #@d
    .line 261
    iput-boolean v1, p0, Landroid/media/RingtoneManager;->mStopPreviousRingtone:Z

    #@f
    .line 291
    iput-object p1, p0, Landroid/media/RingtoneManager;->mContext:Landroid/content/Context;

    #@11
    .line 292
    iget v0, p0, Landroid/media/RingtoneManager;->mType:I

    #@13
    invoke-virtual {p0, v0}, Landroid/media/RingtoneManager;->setType(I)V

    #@16
    .line 293
    return-void
.end method

.method public static checkDocomoDRM(Landroid/content/Context;Ljava/lang/String;)Z
    .registers 10
    .parameter "context"
    .parameter "path"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 754
    const/4 v0, 0x0

    #@3
    .line 755
    .local v0, bCanHandle:Z
    if-nez p1, :cond_6

    #@5
    .line 789
    :goto_5
    return v3

    #@6
    .line 758
    :cond_6
    const-string v5, "RingtoneManager"

    #@8
    new-instance v6, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v7, "[checkDocomoDRM] path : "

    #@f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v6

    #@13
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v6

    #@17
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v6

    #@1b
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 759
    sget-object v5, Landroid/media/RingtoneManager;->mDrmManagerClient:Landroid/drm/DrmManagerClient;

    #@20
    if-nez v5, :cond_29

    #@22
    .line 760
    new-instance v5, Landroid/drm/DrmManagerClient;

    #@24
    invoke-direct {v5, p0}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    #@27
    sput-object v5, Landroid/media/RingtoneManager;->mDrmManagerClient:Landroid/drm/DrmManagerClient;

    #@29
    .line 762
    :cond_29
    sget-object v5, Landroid/media/RingtoneManager;->mDrmManagerClient:Landroid/drm/DrmManagerClient;

    #@2b
    const-string v6, "audio/mp4"

    #@2d
    invoke-virtual {v5, p1, v6}, Landroid/drm/DrmManagerClient;->canHandle(Ljava/lang/String;Ljava/lang/String;)Z

    #@30
    move-result v0

    #@31
    .line 763
    const-string v5, "RingtoneManager"

    #@33
    new-instance v6, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v7, "[checkDocomoDRM] bCanHandle : "

    #@3a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v6

    #@3e
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@41
    move-result-object v6

    #@42
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v6

    #@46
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    .line 765
    if-ne v0, v4, :cond_76

    #@4b
    .line 769
    :try_start_4b
    sget-object v5, Landroid/media/RingtoneManager;->mDrmManagerClient:Landroid/drm/DrmManagerClient;

    #@4d
    const/4 v6, 0x2

    #@4e
    invoke-virtual {v5, p1, v6}, Landroid/drm/DrmManagerClient;->checkRightsStatus(Ljava/lang/String;I)I

    #@51
    move-result v2

    #@52
    .line 770
    .local v2, nDrmStatus:I
    packed-switch v2, :pswitch_data_ae

    #@55
    .line 772
    const-string v5, "RingtoneManager"

    #@57
    const-string v6, "[checkDocomoDRM] default"

    #@59
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5c
    .catch Ljava/lang/Exception; {:try_start_4b .. :try_end_5c} :catch_5d

    #@5c
    goto :goto_5

    #@5d
    .line 785
    .end local v2           #nDrmStatus:I
    :catch_5d
    move-exception v1

    #@5e
    .line 786
    .local v1, ex:Ljava/lang/Exception;
    const-string v3, "RingtoneManager"

    #@60
    new-instance v5, Ljava/lang/StringBuilder;

    #@62
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@65
    const-string v6, "[checkDocomoDRM] exception is occured during checkRightsStatus"

    #@67
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v5

    #@6b
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v5

    #@6f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v5

    #@73
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@76
    .end local v1           #ex:Ljava/lang/Exception;
    :cond_76
    move v3, v4

    #@77
    .line 789
    goto :goto_5

    #@78
    .line 775
    .restart local v2       #nDrmStatus:I
    :pswitch_78
    :try_start_78
    const-string v3, "RingtoneManager"

    #@7a
    new-instance v5, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    const-string v6, "[checkDocomoDRM]  Docomo PlayReady Licnese is valid. nDrmStatus : "

    #@81
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v5

    #@85
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@88
    move-result-object v5

    #@89
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8c
    move-result-object v5

    #@8d
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@90
    move v3, v4

    #@91
    .line 776
    goto/16 :goto_5

    #@93
    .line 782
    :pswitch_93
    const-string v5, "RingtoneManager"

    #@95
    new-instance v6, Ljava/lang/StringBuilder;

    #@97
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@9a
    const-string v7, "[checkDocomoDRM] Docomo PlayReady License is not valid. nDrmStatus : "

    #@9c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v6

    #@a0
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v6

    #@a4
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a7
    move-result-object v6

    #@a8
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_ab
    .catch Ljava/lang/Exception; {:try_start_78 .. :try_end_ab} :catch_5d

    #@ab
    goto/16 :goto_5

    #@ad
    .line 770
    nop

    #@ae
    :pswitch_data_ae
    .packed-switch 0x0
        :pswitch_78
        :pswitch_93
        :pswitch_93
        :pswitch_93
    .end packed-switch
.end method

.method private static constructBooleanTrueWhereClause(Ljava/util/List;Z)Ljava/lang/String;
    .registers 6
    .parameter
    .parameter "includeDrm"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "Ljava/lang/String;"
        }
    .end annotation

    #@0
    .prologue
    .line 590
    .local p0, columns:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    if-nez p0, :cond_4

    #@2
    const/4 v2, 0x0

    #@3
    .line 622
    :goto_3
    return-object v2

    #@4
    .line 592
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    .line 593
    .local v1, sb:Ljava/lang/StringBuilder;
    const-string v2, "("

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    .line 595
    invoke-interface {p0}, Ljava/util/List;->size()I

    #@11
    move-result v2

    #@12
    add-int/lit8 v0, v2, -0x1

    #@14
    .local v0, i:I
    :goto_14
    if-ltz v0, :cond_28

    #@16
    .line 596
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v2

    #@1a
    check-cast v2, Ljava/lang/String;

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    const-string v3, "=1 or "

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    .line 595
    add-int/lit8 v0, v0, -0x1

    #@27
    goto :goto_14

    #@28
    .line 599
    :cond_28
    invoke-interface {p0}, Ljava/util/List;->size()I

    #@2b
    move-result v2

    #@2c
    if-lez v2, :cond_37

    #@2e
    .line 601
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    #@31
    move-result v2

    #@32
    add-int/lit8 v2, v2, -0x4

    #@34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    #@37
    .line 604
    :cond_37
    const-string v2, ")"

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    .line 606
    if-nez p1, :cond_68

    #@3e
    .line 610
    const-string v2, " and "

    #@40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    .line 611
    const-string v2, "("

    #@45
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    .line 612
    const-string/jumbo v2, "is_drm"

    #@4b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    .line 613
    const-string v2, "=0"

    #@50
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    .line 614
    const-string v2, " or "

    #@55
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    .line 615
    const-string/jumbo v2, "is_drm"

    #@5b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    .line 616
    const-string v2, " IS NULL "

    #@60
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    .line 617
    const-string v2, ")"

    #@65
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    .line 622
    :cond_68
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v2

    #@6c
    goto :goto_3
.end method

.method public static getActualDefaultRingtoneUri(Landroid/content/Context;I)Landroid/net/Uri;
    .registers 10
    .parameter "context"
    .parameter "type"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 707
    invoke-static {p1}, Landroid/media/RingtoneManager;->getSettingForType(I)Ljava/lang/String;

    #@4
    move-result-object v3

    #@5
    .line 708
    .local v3, setting:Ljava/lang/String;
    if-nez v3, :cond_8

    #@7
    .line 737
    :cond_7
    :goto_7
    return-object v5

    #@8
    .line 709
    :cond_8
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b
    move-result-object v6

    #@c
    invoke-static {v6, v3}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@f
    move-result-object v4

    #@10
    .line 711
    .local v4, uriString:Ljava/lang/String;
    const-string v6, "DCM"

    #@12
    const-string/jumbo v7, "ro.build.target_operator"

    #@15
    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@18
    move-result-object v7

    #@19
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v6

    #@1d
    if-eqz v6, :cond_7c

    #@1f
    .line 712
    const/4 v2, 0x0

    #@20
    .line 713
    .local v2, path:Ljava/lang/String;
    if-nez v4, :cond_27

    #@22
    .line 714
    invoke-static {p0, p1}, Landroid/media/RingtoneManager;->getDefaultPath(Landroid/content/Context;I)Landroid/net/Uri;

    #@25
    move-result-object v5

    #@26
    goto :goto_7

    #@27
    .line 718
    :cond_27
    :try_start_27
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@2a
    move-result-object v5

    #@2b
    invoke-static {p0, v5}, Lcom/lge/lgdrm/DrmFwExt;->getActualRingtoneUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    .line 719
    if-nez v2, :cond_54

    #@31
    .line 720
    invoke-static {p0, p1}, Landroid/media/RingtoneManager;->getDefaultPath(Landroid/content/Context;I)Landroid/net/Uri;
    :try_end_34
    .catch Ljava/lang/Exception; {:try_start_27 .. :try_end_34} :catch_36

    #@34
    move-result-object v5

    #@35
    goto :goto_7

    #@36
    .line 722
    :catch_36
    move-exception v0

    #@37
    .line 723
    .local v0, ex:Ljava/lang/Exception;
    const-string v5, "RingtoneManager"

    #@39
    new-instance v6, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v7, "Exception is occurred while get path from uri"

    #@40
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v6

    #@44
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v6

    #@48
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v6

    #@4c
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 724
    invoke-static {p0, p1}, Landroid/media/RingtoneManager;->getDefaultPath(Landroid/content/Context;I)Landroid/net/Uri;

    #@52
    move-result-object v5

    #@53
    goto :goto_7

    #@54
    .line 726
    .end local v0           #ex:Ljava/lang/Exception;
    :cond_54
    const-string v5, "RingtoneManager"

    #@56
    const-string v6, "check Docomo DRM "

    #@58
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    .line 727
    invoke-static {p0, v2}, Landroid/media/RingtoneManager;->checkDocomoDRM(Landroid/content/Context;Ljava/lang/String;)Z

    #@5e
    move-result v1

    #@5f
    .line 728
    .local v1, isDocomoLicenseValid:Z
    const/4 v5, 0x1

    #@60
    if-ne v1, v5, :cond_70

    #@62
    if-eqz v2, :cond_70

    #@64
    .line 729
    const-string v5, "RingtoneManager"

    #@66
    const-string v6, "Docomo DRM license is valid or normal contents."

    #@68
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6b
    .line 730
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@6e
    move-result-object v5

    #@6f
    goto :goto_7

    #@70
    .line 732
    :cond_70
    const-string v5, "RingtoneManager"

    #@72
    const-string v6, "Docomo DRM license is invalid."

    #@74
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@77
    .line 733
    invoke-static {p0, p1}, Landroid/media/RingtoneManager;->getDefaultPath(Landroid/content/Context;I)Landroid/net/Uri;

    #@7a
    move-result-object v5

    #@7b
    goto :goto_7

    #@7c
    .line 737
    .end local v1           #isDocomoLicenseValid:Z
    .end local v2           #path:Ljava/lang/String;
    :cond_7c
    if-eqz v4, :cond_7

    #@7e
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@81
    move-result-object v5

    #@82
    goto :goto_7
.end method

.method private static getDefaultPath(Landroid/content/Context;I)Landroid/net/Uri;
    .registers 14
    .parameter "context"
    .parameter "mStreamType"

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    .line 797
    const/4 v6, 0x0

    #@2
    .line 798
    .local v6, c:Landroid/database/Cursor;
    const/4 v10, 0x0

    #@3
    .line 800
    .local v10, uri:Landroid/net/Uri;
    and-int/lit8 v0, p1, 0x1

    #@5
    if-eqz v0, :cond_43

    #@7
    .line 801
    sget-object v8, Landroid/media/RingtoneManager;->DEFAULT_RINGTONE_PATH:Ljava/lang/String;

    #@9
    .line 810
    .local v8, fileUri:Ljava/lang/String;
    :goto_9
    :try_start_9
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c
    move-result-object v0

    #@d
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    #@f
    const/4 v2, 0x0

    #@10
    new-instance v3, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v4, "_data = \'"

    #@17
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v8}, Ljava/lang/String;->toString()Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    const-string v4, "\'"

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    const/4 v4, 0x0

    #@2e
    const/4 v5, 0x0

    #@2f
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@32
    move-result-object v6

    #@33
    .line 811
    if-nez v6, :cond_54

    #@35
    .line 812
    const-string v0, "RingtoneManager"

    #@37
    const-string v1, "[getDefaultPath] Cursor is null. Error is occurred."

    #@39
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3c
    .catchall {:try_start_9 .. :try_end_3c} :catchall_94
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_3c} :catch_86

    #@3c
    .line 822
    if-eqz v6, :cond_41

    #@3e
    .line 823
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@41
    :cond_41
    move-object v0, v11

    #@42
    .line 826
    :goto_42
    return-object v0

    #@43
    .line 802
    .end local v8           #fileUri:Ljava/lang/String;
    :cond_43
    and-int/lit8 v0, p1, 0x2

    #@45
    if-eqz v0, :cond_4a

    #@47
    .line 803
    sget-object v8, Landroid/media/RingtoneManager;->DEFAULT_NOTIFICATION_PATH:Ljava/lang/String;

    #@49
    .restart local v8       #fileUri:Ljava/lang/String;
    goto :goto_9

    #@4a
    .line 804
    .end local v8           #fileUri:Ljava/lang/String;
    :cond_4a
    and-int/lit8 v0, p1, 0x4

    #@4c
    if-eqz v0, :cond_51

    #@4e
    .line 805
    sget-object v8, Landroid/media/RingtoneManager;->DEFAULT_ALARM_PATH:Ljava/lang/String;

    #@50
    .restart local v8       #fileUri:Ljava/lang/String;
    goto :goto_9

    #@51
    .line 807
    .end local v8           #fileUri:Ljava/lang/String;
    :cond_51
    sget-object v8, Landroid/media/RingtoneManager;->DEFAULT_NOTIFICATION_PATH:Ljava/lang/String;

    #@53
    .restart local v8       #fileUri:Ljava/lang/String;
    goto :goto_9

    #@54
    .line 815
    :cond_54
    :try_start_54
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    #@57
    .line 816
    const/4 v0, 0x0

    #@58
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    #@5b
    move-result v9

    #@5c
    .line 817
    .local v9, id:I
    sget-object v0, Landroid/provider/MediaStore$Audio$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    #@5e
    int-to-long v1, v9

    #@5f
    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@62
    move-result-object v10

    #@63
    .line 818
    const-string v0, "RingtoneManager"

    #@65
    new-instance v1, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v2, "[getDefaultPath] uri : "

    #@6c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v1

    #@70
    invoke-virtual {v10}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@73
    move-result-object v2

    #@74
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v1

    #@78
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v1

    #@7c
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7f
    .catchall {:try_start_54 .. :try_end_7f} :catchall_94
    .catch Ljava/lang/Exception; {:try_start_54 .. :try_end_7f} :catch_86

    #@7f
    .line 822
    if-eqz v6, :cond_84

    #@81
    .line 823
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@84
    .end local v9           #id:I
    :cond_84
    :goto_84
    move-object v0, v10

    #@85
    .line 826
    goto :goto_42

    #@86
    .line 819
    :catch_86
    move-exception v7

    #@87
    .line 820
    .local v7, ex:Ljava/lang/Exception;
    :try_start_87
    const-string v0, "RingtoneManager"

    #@89
    const-string v1, "[getDefaultPath] Exception is occurred."

    #@8b
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8e
    .catchall {:try_start_87 .. :try_end_8e} :catchall_94

    #@8e
    .line 822
    if-eqz v6, :cond_84

    #@90
    .line 823
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@93
    goto :goto_84

    #@94
    .line 822
    .end local v7           #ex:Ljava/lang/Exception;
    :catchall_94
    move-exception v0

    #@95
    if-eqz v6, :cond_9a

    #@97
    .line 823
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@9a
    :cond_9a
    throw v0
.end method

.method public static getDefaultType(Landroid/net/Uri;)I
    .registers 3
    .parameter "defaultRingtoneUri"

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 887
    if-nez p0, :cond_4

    #@3
    .line 904
    :cond_3
    :goto_3
    return v0

    #@4
    .line 889
    :cond_4
    sget-object v1, Landroid/provider/Settings$System;->DEFAULT_RINGTONE_URI:Landroid/net/Uri;

    #@6
    invoke-virtual {p0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_e

    #@c
    .line 890
    const/4 v0, 0x1

    #@d
    goto :goto_3

    #@e
    .line 891
    :cond_e
    sget-object v1, Landroid/provider/Settings$System;->DEFAULT_NOTIFICATION_URI:Landroid/net/Uri;

    #@10
    invoke-virtual {p0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_18

    #@16
    .line 892
    const/4 v0, 0x2

    #@17
    goto :goto_3

    #@18
    .line 893
    :cond_18
    sget-object v1, Landroid/provider/Settings$System;->DEFAULT_ALARM_ALERT_URI:Landroid/net/Uri;

    #@1a
    invoke-virtual {p0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v1

    #@1e
    if-eqz v1, :cond_22

    #@20
    .line 894
    const/4 v0, 0x4

    #@21
    goto :goto_3

    #@22
    .line 896
    :cond_22
    sget-object v1, Landroid/provider/Settings$System;->DEFAULT_RINGTONE_VIDEOCALL_URI:Landroid/net/Uri;

    #@24
    invoke-virtual {p0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    #@27
    move-result v1

    #@28
    if-eqz v1, :cond_2d

    #@2a
    .line 897
    const/16 v0, 0x20

    #@2c
    goto :goto_3

    #@2d
    .line 898
    :cond_2d
    sget-object v1, Landroid/provider/Settings$System;->DEFAULT_RINGTONE_SIM2_URI:Landroid/net/Uri;

    #@2f
    invoke-virtual {p0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    #@32
    move-result v1

    #@33
    if-eqz v1, :cond_38

    #@35
    .line 899
    const/16 v0, 0x8

    #@37
    goto :goto_3

    #@38
    .line 900
    :cond_38
    sget-object v1, Landroid/provider/Settings$System;->DEFAULT_NOTIFICATION_SIM2_URI:Landroid/net/Uri;

    #@3a
    invoke-virtual {p0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    #@3d
    move-result v1

    #@3e
    if-eqz v1, :cond_3

    #@40
    .line 901
    const/16 v0, 0x10

    #@42
    goto :goto_3
.end method

.method public static getDefaultUri(I)Landroid/net/Uri;
    .registers 2
    .parameter "type"

    #@0
    .prologue
    .line 918
    and-int/lit8 v0, p0, 0x1

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 919
    sget-object v0, Landroid/provider/Settings$System;->DEFAULT_RINGTONE_URI:Landroid/net/Uri;

    #@6
    .line 933
    :goto_6
    return-object v0

    #@7
    .line 920
    :cond_7
    and-int/lit8 v0, p0, 0x2

    #@9
    if-eqz v0, :cond_e

    #@b
    .line 921
    sget-object v0, Landroid/provider/Settings$System;->DEFAULT_NOTIFICATION_URI:Landroid/net/Uri;

    #@d
    goto :goto_6

    #@e
    .line 922
    :cond_e
    and-int/lit8 v0, p0, 0x4

    #@10
    if-eqz v0, :cond_15

    #@12
    .line 923
    sget-object v0, Landroid/provider/Settings$System;->DEFAULT_ALARM_ALERT_URI:Landroid/net/Uri;

    #@14
    goto :goto_6

    #@15
    .line 925
    :cond_15
    and-int/lit8 v0, p0, 0x20

    #@17
    if-eqz v0, :cond_1c

    #@19
    .line 926
    sget-object v0, Landroid/provider/Settings$System;->DEFAULT_RINGTONE_VIDEOCALL_URI:Landroid/net/Uri;

    #@1b
    goto :goto_6

    #@1c
    .line 927
    :cond_1c
    and-int/lit8 v0, p0, 0x8

    #@1e
    if-eqz v0, :cond_23

    #@20
    .line 928
    sget-object v0, Landroid/provider/Settings$System;->DEFAULT_RINGTONE_SIM2_URI:Landroid/net/Uri;

    #@22
    goto :goto_6

    #@23
    .line 929
    :cond_23
    and-int/lit8 v0, p0, 0x10

    #@25
    if-eqz v0, :cond_2a

    #@27
    .line 930
    sget-object v0, Landroid/provider/Settings$System;->DEFAULT_NOTIFICATION_SIM2_URI:Landroid/net/Uri;

    #@29
    goto :goto_6

    #@2a
    .line 933
    :cond_2a
    const/4 v0, 0x0

    #@2b
    goto :goto_6
.end method

.method private getDrmRingtones()Landroid/database/Cursor;
    .registers 7

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 534
    sget-object v1, Landroid/provider/DrmStore$Audio;->CONTENT_URI:Landroid/net/Uri;

    #@3
    sget-object v2, Landroid/media/RingtoneManager;->DRM_COLUMNS:[Ljava/lang/String;

    #@5
    const-string/jumbo v5, "title"

    #@8
    move-object v0, p0

    #@9
    move-object v4, v3

    #@a
    invoke-direct/range {v0 .. v5}, Landroid/media/RingtoneManager;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@d
    move-result-object v0

    #@e
    return-object v0
.end method

.method private getInternalRingtones()Landroid/database/Cursor;
    .registers 7

    #@0
    .prologue
    .line 526
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    #@2
    sget-object v2, Landroid/media/RingtoneManager;->INTERNAL_COLUMNS:[Ljava/lang/String;

    #@4
    iget-object v0, p0, Landroid/media/RingtoneManager;->mFilterColumns:Ljava/util/List;

    #@6
    iget-boolean v3, p0, Landroid/media/RingtoneManager;->mIncludeDrm:Z

    #@8
    invoke-static {v0, v3}, Landroid/media/RingtoneManager;->constructBooleanTrueWhereClause(Ljava/util/List;Z)Ljava/lang/String;

    #@b
    move-result-object v3

    #@c
    const/4 v4, 0x0

    #@d
    const-string/jumbo v5, "title_key"

    #@10
    move-object v0, p0

    #@11
    invoke-direct/range {v0 .. v5}, Landroid/media/RingtoneManager;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@14
    move-result-object v0

    #@15
    return-object v0
.end method

.method private getMediaRingtones()Landroid/database/Cursor;
    .registers 8

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 541
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    #@4
    move-result-object v6

    #@5
    .line 543
    .local v6, status:Ljava/lang/String;
    const-string/jumbo v0, "mounted"

    #@8
    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_17

    #@e
    const-string/jumbo v0, "mounted_ro"

    #@11
    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_2b

    #@17
    :cond_17
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    #@19
    sget-object v2, Landroid/media/RingtoneManager;->MEDIA_COLUMNS:[Ljava/lang/String;

    #@1b
    iget-object v0, p0, Landroid/media/RingtoneManager;->mFilterColumns:Ljava/util/List;

    #@1d
    iget-boolean v3, p0, Landroid/media/RingtoneManager;->mIncludeDrm:Z

    #@1f
    invoke-static {v0, v3}, Landroid/media/RingtoneManager;->constructBooleanTrueWhereClause(Ljava/util/List;Z)Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    const-string/jumbo v5, "title_key"

    #@26
    move-object v0, p0

    #@27
    invoke-direct/range {v0 .. v5}, Landroid/media/RingtoneManager;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@2a
    move-result-object v4

    #@2b
    :cond_2b
    return-object v4
.end method

.method public static getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;
    .registers 3
    .parameter "context"
    .parameter "ringtoneUri"

    #@0
    .prologue
    .line 651
    const/4 v0, -0x1

    #@1
    invoke-static {p0, p1, v0}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;I)Landroid/media/Ringtone;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method private static getRingtone(Landroid/content/Context;Landroid/net/Uri;I)Landroid/media/Ringtone;
    .registers 8
    .parameter "context"
    .parameter "ringtoneUri"
    .parameter "streamType"

    #@0
    .prologue
    .line 681
    :try_start_0
    new-instance v1, Landroid/media/Ringtone;

    #@2
    const/4 v2, 0x1

    #@3
    invoke-direct {v1, p0, v2}, Landroid/media/Ringtone;-><init>(Landroid/content/Context;Z)V

    #@6
    .line 682
    .local v1, r:Landroid/media/Ringtone;
    if-ltz p2, :cond_b

    #@8
    .line 683
    invoke-virtual {v1, p2}, Landroid/media/Ringtone;->setStreamType(I)V

    #@b
    .line 685
    :cond_b
    invoke-virtual {v1, p1}, Landroid/media/Ringtone;->setUri(Landroid/net/Uri;)V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_e} :catch_f

    #@e
    .line 691
    .end local v1           #r:Landroid/media/Ringtone;
    :goto_e
    return-object v1

    #@f
    .line 687
    :catch_f
    move-exception v0

    #@10
    .line 688
    .local v0, ex:Ljava/lang/Exception;
    const-string v2, "RingtoneManager"

    #@12
    new-instance v3, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v4, "Failed to open ringtone "

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    const-string v4, ": "

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 691
    const/4 v1, 0x0

    #@33
    goto :goto_e
.end method

.method public static getRingtoneWithStreamType(Landroid/content/Context;Landroid/net/Uri;I)Landroid/media/Ringtone;
    .registers 8
    .parameter "context"
    .parameter "ringtoneUri"
    .parameter "streamType"

    #@0
    .prologue
    .line 658
    :try_start_0
    new-instance v1, Landroid/media/Ringtone;

    #@2
    const/4 v2, 0x1

    #@3
    invoke-direct {v1, p0, v2, p2}, Landroid/media/Ringtone;-><init>(Landroid/content/Context;ZI)V

    #@6
    .line 659
    .local v1, r:Landroid/media/Ringtone;
    invoke-virtual {v1, p1}, Landroid/media/Ringtone;->setUri(Landroid/net/Uri;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_9} :catch_a

    #@9
    .line 665
    .end local v1           #r:Landroid/media/Ringtone;
    :goto_9
    return-object v1

    #@a
    .line 661
    :catch_a
    move-exception v0

    #@b
    .line 662
    .local v0, ex:Ljava/lang/Exception;
    const-string v2, "RingtoneManager"

    #@d
    new-instance v3, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v4, "Failed to open ringtone "

    #@14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    const-string v4, ": "

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 665
    const/4 v1, 0x0

    #@2e
    goto :goto_9
.end method

.method private static getSettingForType(I)Ljava/lang/String;
    .registers 2
    .parameter "type"

    #@0
    .prologue
    .line 848
    and-int/lit8 v0, p0, 0x1

    #@2
    if-eqz v0, :cond_8

    #@4
    .line 849
    const-string/jumbo v0, "ringtone"

    #@7
    .line 863
    :goto_7
    return-object v0

    #@8
    .line 850
    :cond_8
    and-int/lit8 v0, p0, 0x2

    #@a
    if-eqz v0, :cond_10

    #@c
    .line 851
    const-string/jumbo v0, "notification_sound"

    #@f
    goto :goto_7

    #@10
    .line 852
    :cond_10
    and-int/lit8 v0, p0, 0x4

    #@12
    if-eqz v0, :cond_17

    #@14
    .line 853
    const-string v0, "alarm_alert"

    #@16
    goto :goto_7

    #@17
    .line 855
    :cond_17
    and-int/lit8 v0, p0, 0x20

    #@19
    if-eqz v0, :cond_1f

    #@1b
    .line 856
    const-string/jumbo v0, "ringtone_videocall"

    #@1e
    goto :goto_7

    #@1f
    .line 857
    :cond_1f
    and-int/lit8 v0, p0, 0x8

    #@21
    if-eqz v0, :cond_27

    #@23
    .line 858
    const-string/jumbo v0, "ringtone_sim2"

    #@26
    goto :goto_7

    #@27
    .line 859
    :cond_27
    and-int/lit8 v0, p0, 0x10

    #@29
    if-eqz v0, :cond_2f

    #@2b
    .line 860
    const-string/jumbo v0, "notification_sound_sim2"

    #@2e
    goto :goto_7

    #@2f
    .line 863
    :cond_2f
    const/4 v0, 0x0

    #@30
    goto :goto_7
.end method

.method private static getUriFromCursor(Landroid/database/Cursor;)Landroid/net/Uri;
    .registers 4
    .parameter "cursor"

    #@0
    .prologue
    .line 444
    const/4 v0, 0x2

    #@1
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@8
    move-result-object v0

    #@9
    const/4 v1, 0x0

    #@a
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    #@d
    move-result-wide v1

    #@e
    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@11
    move-result-object v0

    #@12
    return-object v0
.end method

.method public static getValidRingtoneUri(Landroid/content/Context;)Landroid/net/Uri;
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 495
    new-instance v0, Landroid/media/RingtoneManager;

    #@2
    invoke-direct {v0, p0}, Landroid/media/RingtoneManager;-><init>(Landroid/content/Context;)V

    #@5
    .line 497
    .local v0, rm:Landroid/media/RingtoneManager;
    invoke-direct {v0}, Landroid/media/RingtoneManager;->getInternalRingtones()Landroid/database/Cursor;

    #@8
    move-result-object v2

    #@9
    invoke-static {p0, v2}, Landroid/media/RingtoneManager;->getValidRingtoneUriFromCursorAndClose(Landroid/content/Context;Landroid/database/Cursor;)Landroid/net/Uri;

    #@c
    move-result-object v1

    #@d
    .line 499
    .local v1, uri:Landroid/net/Uri;
    if-nez v1, :cond_17

    #@f
    .line 500
    invoke-direct {v0}, Landroid/media/RingtoneManager;->getMediaRingtones()Landroid/database/Cursor;

    #@12
    move-result-object v2

    #@13
    invoke-static {p0, v2}, Landroid/media/RingtoneManager;->getValidRingtoneUriFromCursorAndClose(Landroid/content/Context;Landroid/database/Cursor;)Landroid/net/Uri;

    #@16
    move-result-object v1

    #@17
    .line 503
    :cond_17
    if-nez v1, :cond_21

    #@19
    .line 504
    invoke-direct {v0}, Landroid/media/RingtoneManager;->getDrmRingtones()Landroid/database/Cursor;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {p0, v2}, Landroid/media/RingtoneManager;->getValidRingtoneUriFromCursorAndClose(Landroid/content/Context;Landroid/database/Cursor;)Landroid/net/Uri;

    #@20
    move-result-object v1

    #@21
    .line 507
    :cond_21
    return-object v1
.end method

.method private static getValidRingtoneUriFromCursorAndClose(Landroid/content/Context;Landroid/database/Cursor;)Landroid/net/Uri;
    .registers 4
    .parameter "context"
    .parameter "cursor"

    #@0
    .prologue
    .line 511
    if-eqz p1, :cond_11

    #@2
    .line 512
    const/4 v0, 0x0

    #@3
    .line 514
    .local v0, uri:Landroid/net/Uri;
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_d

    #@9
    .line 515
    invoke-static {p1}, Landroid/media/RingtoneManager;->getUriFromCursor(Landroid/database/Cursor;)Landroid/net/Uri;

    #@c
    move-result-object v0

    #@d
    .line 517
    :cond_d
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    #@10
    .line 521
    .end local v0           #uri:Landroid/net/Uri;
    :goto_10
    return-object v0

    #@11
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_10
.end method

.method public static isDefault(Landroid/net/Uri;)Z
    .registers 3
    .parameter "ringtoneUri"

    #@0
    .prologue
    .line 874
    invoke-static {p0}, Landroid/media/RingtoneManager;->getDefaultType(Landroid/net/Uri;)I

    #@3
    move-result v0

    #@4
    const/4 v1, -0x1

    #@5
    if-eq v0, v1, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method private query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 12
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"

    #@0
    .prologue
    .line 630
    iget-object v0, p0, Landroid/media/RingtoneManager;->mActivity:Landroid/app/Activity;

    #@2
    if-eqz v0, :cond_10

    #@4
    .line 631
    iget-object v0, p0, Landroid/media/RingtoneManager;->mActivity:Landroid/app/Activity;

    #@6
    move-object v1, p1

    #@7
    move-object v2, p2

    #@8
    move-object v3, p3

    #@9
    move-object v4, p4

    #@a
    move-object v5, p5

    #@b
    invoke-virtual/range {v0 .. v5}, Landroid/app/Activity;->managedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@e
    move-result-object v0

    #@f
    .line 633
    :goto_f
    return-object v0

    #@10
    :cond_10
    iget-object v0, p0, Landroid/media/RingtoneManager;->mContext:Landroid/content/Context;

    #@12
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@15
    move-result-object v0

    #@16
    move-object v1, p1

    #@17
    move-object v2, p2

    #@18
    move-object v3, p3

    #@19
    move-object v4, p4

    #@1a
    move-object v5, p5

    #@1b
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@1e
    move-result-object v0

    #@1f
    goto :goto_f
.end method

.method public static setActualDefaultRingtoneUri(Landroid/content/Context;ILandroid/net/Uri;)V
    .registers 6
    .parameter "context"
    .parameter "type"
    .parameter "ringtoneUri"

    #@0
    .prologue
    .line 841
    invoke-static {p1}, Landroid/media/RingtoneManager;->getSettingForType(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 842
    .local v0, setting:Ljava/lang/String;
    if-nez v0, :cond_7

    #@6
    .line 845
    :goto_6
    return-void

    #@7
    .line 843
    :cond_7
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a
    move-result-object v2

    #@b
    if-eqz p2, :cond_15

    #@d
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    :goto_11
    invoke-static {v2, v0, v1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@14
    goto :goto_6

    #@15
    :cond_15
    const/4 v1, 0x0

    #@16
    goto :goto_11
.end method

.method private setFilterColumnsList(I)V
    .registers 4
    .parameter "type"

    #@0
    .prologue
    .line 553
    iget-object v0, p0, Landroid/media/RingtoneManager;->mFilterColumns:Ljava/util/List;

    #@2
    .line 554
    .local v0, columns:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/List;->clear()V

    #@5
    .line 556
    and-int/lit8 v1, p1, 0x1

    #@7
    if-eqz v1, :cond_f

    #@9
    .line 557
    const-string/jumbo v1, "is_ringtone"

    #@c
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@f
    .line 560
    :cond_f
    and-int/lit8 v1, p1, 0x2

    #@11
    if-eqz v1, :cond_19

    #@13
    .line 561
    const-string/jumbo v1, "is_notification"

    #@16
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@19
    .line 564
    :cond_19
    and-int/lit8 v1, p1, 0x4

    #@1b
    if-eqz v1, :cond_23

    #@1d
    .line 565
    const-string/jumbo v1, "is_alarm"

    #@20
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@23
    .line 568
    :cond_23
    and-int/lit8 v1, p1, 0x8

    #@25
    if-eqz v1, :cond_2d

    #@27
    .line 569
    const-string/jumbo v1, "is_ringtone"

    #@2a
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@2d
    .line 571
    :cond_2d
    and-int/lit8 v1, p1, 0x10

    #@2f
    if-eqz v1, :cond_37

    #@31
    .line 572
    const-string/jumbo v1, "is_notification"

    #@34
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@37
    .line 574
    :cond_37
    and-int/lit8 v1, p1, 0x20

    #@39
    if-eqz v1, :cond_41

    #@3b
    .line 575
    const-string/jumbo v1, "is_ringtone"

    #@3e
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@41
    .line 578
    :cond_41
    return-void
.end method


# virtual methods
.method public getCursor()Landroid/database/Cursor;
    .registers 7

    #@0
    .prologue
    .line 399
    iget-object v3, p0, Landroid/media/RingtoneManager;->mCursor:Landroid/database/Cursor;

    #@2
    if-eqz v3, :cond_f

    #@4
    iget-object v3, p0, Landroid/media/RingtoneManager;->mCursor:Landroid/database/Cursor;

    #@6
    invoke-interface {v3}, Landroid/database/Cursor;->requery()Z

    #@9
    move-result v3

    #@a
    if-eqz v3, :cond_f

    #@c
    .line 400
    iget-object v3, p0, Landroid/media/RingtoneManager;->mCursor:Landroid/database/Cursor;

    #@e
    .line 407
    :goto_e
    return-object v3

    #@f
    .line 403
    :cond_f
    invoke-direct {p0}, Landroid/media/RingtoneManager;->getInternalRingtones()Landroid/database/Cursor;

    #@12
    move-result-object v1

    #@13
    .line 404
    .local v1, internalCursor:Landroid/database/Cursor;
    iget-boolean v3, p0, Landroid/media/RingtoneManager;->mIncludeDrm:Z

    #@15
    if-eqz v3, :cond_36

    #@17
    invoke-direct {p0}, Landroid/media/RingtoneManager;->getDrmRingtones()Landroid/database/Cursor;

    #@1a
    move-result-object v0

    #@1b
    .line 405
    .local v0, drmCursor:Landroid/database/Cursor;
    :goto_1b
    invoke-direct {p0}, Landroid/media/RingtoneManager;->getMediaRingtones()Landroid/database/Cursor;

    #@1e
    move-result-object v2

    #@1f
    .line 407
    .local v2, mediaCursor:Landroid/database/Cursor;
    new-instance v3, Lcom/android/internal/database/SortCursor;

    #@21
    const/4 v4, 0x3

    #@22
    new-array v4, v4, [Landroid/database/Cursor;

    #@24
    const/4 v5, 0x0

    #@25
    aput-object v1, v4, v5

    #@27
    const/4 v5, 0x1

    #@28
    aput-object v0, v4, v5

    #@2a
    const/4 v5, 0x2

    #@2b
    aput-object v2, v4, v5

    #@2d
    const-string/jumbo v5, "title_key"

    #@30
    invoke-direct {v3, v4, v5}, Lcom/android/internal/database/SortCursor;-><init>([Landroid/database/Cursor;Ljava/lang/String;)V

    #@33
    iput-object v3, p0, Landroid/media/RingtoneManager;->mCursor:Landroid/database/Cursor;

    #@35
    goto :goto_e

    #@36
    .line 404
    .end local v0           #drmCursor:Landroid/database/Cursor;
    .end local v2           #mediaCursor:Landroid/database/Cursor;
    :cond_36
    const/4 v0, 0x0

    #@37
    goto :goto_1b
.end method

.method public getIncludeDrm()Z
    .registers 2

    #@0
    .prologue
    .line 371
    iget-boolean v0, p0, Landroid/media/RingtoneManager;->mIncludeDrm:Z

    #@2
    return v0
.end method

.method public getRingtone(I)Landroid/media/Ringtone;
    .registers 5
    .parameter "position"

    #@0
    .prologue
    .line 419
    iget-boolean v0, p0, Landroid/media/RingtoneManager;->mStopPreviousRingtone:Z

    #@2
    if-eqz v0, :cond_d

    #@4
    iget-object v0, p0, Landroid/media/RingtoneManager;->mPreviousRingtone:Landroid/media/Ringtone;

    #@6
    if-eqz v0, :cond_d

    #@8
    .line 420
    iget-object v0, p0, Landroid/media/RingtoneManager;->mPreviousRingtone:Landroid/media/Ringtone;

    #@a
    invoke-virtual {v0}, Landroid/media/Ringtone;->stop()V

    #@d
    .line 423
    :cond_d
    iget-object v0, p0, Landroid/media/RingtoneManager;->mContext:Landroid/content/Context;

    #@f
    invoke-virtual {p0, p1}, Landroid/media/RingtoneManager;->getRingtoneUri(I)Landroid/net/Uri;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {p0}, Landroid/media/RingtoneManager;->inferStreamType()I

    #@16
    move-result v2

    #@17
    invoke-static {v0, v1, v2}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;I)Landroid/media/Ringtone;

    #@1a
    move-result-object v0

    #@1b
    iput-object v0, p0, Landroid/media/RingtoneManager;->mPreviousRingtone:Landroid/media/Ringtone;

    #@1d
    .line 424
    iget-object v0, p0, Landroid/media/RingtoneManager;->mPreviousRingtone:Landroid/media/Ringtone;

    #@1f
    return-object v0
.end method

.method public getRingtonePosition(Landroid/net/Uri;)I
    .registers 11
    .parameter "ringtoneUri"

    #@0
    .prologue
    const/4 v6, -0x1

    #@1
    .line 456
    if-nez p1, :cond_5

    #@3
    move v3, v6

    #@4
    .line 484
    :cond_4
    :goto_4
    return v3

    #@5
    .line 458
    :cond_5
    invoke-virtual {p0}, Landroid/media/RingtoneManager;->getCursor()Landroid/database/Cursor;

    #@8
    move-result-object v1

    #@9
    .line 459
    .local v1, cursor:Landroid/database/Cursor;
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    #@c
    move-result v2

    #@d
    .line 461
    .local v2, cursorCount:I
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    #@10
    move-result v7

    #@11
    if-nez v7, :cond_15

    #@13
    move v3, v6

    #@14
    .line 462
    goto :goto_4

    #@15
    .line 466
    :cond_15
    const/4 v0, 0x0

    #@16
    .line 467
    .local v0, currentUri:Landroid/net/Uri;
    const/4 v4, 0x0

    #@17
    .line 468
    .local v4, previousUriString:Ljava/lang/String;
    const/4 v3, 0x0

    #@18
    .local v3, i:I
    :goto_18
    if-ge v3, v2, :cond_42

    #@1a
    .line 469
    const/4 v7, 0x2

    #@1b
    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@1e
    move-result-object v5

    #@1f
    .line 470
    .local v5, uriString:Ljava/lang/String;
    if-eqz v0, :cond_27

    #@21
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v7

    #@25
    if-nez v7, :cond_2b

    #@27
    .line 471
    :cond_27
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@2a
    move-result-object v0

    #@2b
    .line 474
    :cond_2b
    const/4 v7, 0x0

    #@2c
    invoke-interface {v1, v7}, Landroid/database/Cursor;->getLong(I)J

    #@2f
    move-result-wide v7

    #@30
    invoke-static {v0, v7, v8}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@33
    move-result-object v7

    #@34
    invoke-virtual {p1, v7}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    #@37
    move-result v7

    #@38
    if-nez v7, :cond_4

    #@3a
    .line 479
    const/4 v7, 0x1

    #@3b
    invoke-interface {v1, v7}, Landroid/database/Cursor;->move(I)Z

    #@3e
    .line 481
    move-object v4, v5

    #@3f
    .line 468
    add-int/lit8 v3, v3, 0x1

    #@41
    goto :goto_18

    #@42
    .end local v5           #uriString:Ljava/lang/String;
    :cond_42
    move v3, v6

    #@43
    .line 484
    goto :goto_4
.end method

.method public getRingtoneUri(I)Landroid/net/Uri;
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 436
    iget-object v0, p0, Landroid/media/RingtoneManager;->mCursor:Landroid/database/Cursor;

    #@2
    if-eqz v0, :cond_c

    #@4
    iget-object v0, p0, Landroid/media/RingtoneManager;->mCursor:Landroid/database/Cursor;

    #@6
    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_e

    #@c
    .line 437
    :cond_c
    const/4 v0, 0x0

    #@d
    .line 440
    :goto_d
    return-object v0

    #@e
    :cond_e
    iget-object v0, p0, Landroid/media/RingtoneManager;->mCursor:Landroid/database/Cursor;

    #@10
    invoke-static {v0}, Landroid/media/RingtoneManager;->getUriFromCursor(Landroid/database/Cursor;)Landroid/net/Uri;

    #@13
    move-result-object v0

    #@14
    goto :goto_d
.end method

.method public getStopPreviousRingtone()Z
    .registers 2

    #@0
    .prologue
    .line 352
    iget-boolean v0, p0, Landroid/media/RingtoneManager;->mStopPreviousRingtone:Z

    #@2
    return v0
.end method

.method public inferStreamType()I
    .registers 2

    #@0
    .prologue
    .line 321
    iget v0, p0, Landroid/media/RingtoneManager;->mType:I

    #@2
    packed-switch v0, :pswitch_data_c

    #@5
    .line 330
    :pswitch_5
    const/4 v0, 0x2

    #@6
    :goto_6
    return v0

    #@7
    .line 324
    :pswitch_7
    const/4 v0, 0x4

    #@8
    goto :goto_6

    #@9
    .line 327
    :pswitch_9
    const/4 v0, 0x5

    #@a
    goto :goto_6

    #@b
    .line 321
    nop

    #@c
    :pswitch_data_c
    .packed-switch 0x2
        :pswitch_9
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method

.method public setIncludeDrm(Z)V
    .registers 2
    .parameter "includeDrm"

    #@0
    .prologue
    .line 380
    iput-boolean p1, p0, Landroid/media/RingtoneManager;->mIncludeDrm:Z

    #@2
    .line 381
    return-void
.end method

.method public setStopPreviousRingtone(Z)V
    .registers 2
    .parameter "stopPreviousRingtone"

    #@0
    .prologue
    .line 345
    iput-boolean p1, p0, Landroid/media/RingtoneManager;->mStopPreviousRingtone:Z

    #@2
    .line 346
    return-void
.end method

.method public setType(I)V
    .registers 4
    .parameter "type"

    #@0
    .prologue
    .line 305
    iget-object v0, p0, Landroid/media/RingtoneManager;->mCursor:Landroid/database/Cursor;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 306
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "Setting filter columns should be done before querying for ringtones."

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 310
    :cond_c
    iput p1, p0, Landroid/media/RingtoneManager;->mType:I

    #@e
    .line 311
    invoke-direct {p0, p1}, Landroid/media/RingtoneManager;->setFilterColumnsList(I)V

    #@11
    .line 312
    return-void
.end method

.method public stopPreviousRingtone()V
    .registers 2

    #@0
    .prologue
    .line 359
    iget-object v0, p0, Landroid/media/RingtoneManager;->mPreviousRingtone:Landroid/media/Ringtone;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 360
    iget-object v0, p0, Landroid/media/RingtoneManager;->mPreviousRingtone:Landroid/media/Ringtone;

    #@6
    invoke-virtual {v0}, Landroid/media/Ringtone;->stop()V

    #@9
    .line 362
    :cond_9
    return-void
.end method
