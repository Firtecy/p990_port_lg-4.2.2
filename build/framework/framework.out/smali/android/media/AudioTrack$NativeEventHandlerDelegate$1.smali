.class Landroid/media/AudioTrack$NativeEventHandlerDelegate$1;
.super Landroid/os/Handler;
.source "AudioTrack.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/media/AudioTrack$NativeEventHandlerDelegate;-><init>(Landroid/media/AudioTrack;Landroid/media/AudioTrack;Landroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Landroid/media/AudioTrack$NativeEventHandlerDelegate;

.field final synthetic val$this$0:Landroid/media/AudioTrack;


# direct methods
.method constructor <init>(Landroid/media/AudioTrack$NativeEventHandlerDelegate;Landroid/os/Looper;Landroid/media/AudioTrack;)V
    .registers 4
    .parameter
    .parameter "x0"
    .parameter

    #@0
    .prologue
    .line 1155
    iput-object p1, p0, Landroid/media/AudioTrack$NativeEventHandlerDelegate$1;->this$1:Landroid/media/AudioTrack$NativeEventHandlerDelegate;

    #@2
    iput-object p3, p0, Landroid/media/AudioTrack$NativeEventHandlerDelegate$1;->val$this$0:Landroid/media/AudioTrack;

    #@4
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@7
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    .line 1158
    iget-object v1, p0, Landroid/media/AudioTrack$NativeEventHandlerDelegate$1;->this$1:Landroid/media/AudioTrack$NativeEventHandlerDelegate;

    #@2
    invoke-static {v1}, Landroid/media/AudioTrack$NativeEventHandlerDelegate;->access$100(Landroid/media/AudioTrack$NativeEventHandlerDelegate;)Landroid/media/AudioTrack;

    #@5
    move-result-object v1

    #@6
    if-nez v1, :cond_9

    #@8
    .line 1181
    :cond_8
    :goto_8
    return-void

    #@9
    .line 1161
    :cond_9
    const/4 v0, 0x0

    #@a
    .line 1162
    .local v0, listener:Landroid/media/AudioTrack$OnPlaybackPositionUpdateListener;
    iget-object v1, p0, Landroid/media/AudioTrack$NativeEventHandlerDelegate$1;->this$1:Landroid/media/AudioTrack$NativeEventHandlerDelegate;

    #@c
    iget-object v1, v1, Landroid/media/AudioTrack$NativeEventHandlerDelegate;->this$0:Landroid/media/AudioTrack;

    #@e
    invoke-static {v1}, Landroid/media/AudioTrack;->access$200(Landroid/media/AudioTrack;)Ljava/lang/Object;

    #@11
    move-result-object v2

    #@12
    monitor-enter v2

    #@13
    .line 1163
    :try_start_13
    iget-object v1, p0, Landroid/media/AudioTrack$NativeEventHandlerDelegate$1;->this$1:Landroid/media/AudioTrack$NativeEventHandlerDelegate;

    #@15
    invoke-static {v1}, Landroid/media/AudioTrack$NativeEventHandlerDelegate;->access$100(Landroid/media/AudioTrack$NativeEventHandlerDelegate;)Landroid/media/AudioTrack;

    #@18
    move-result-object v1

    #@19
    invoke-static {v1}, Landroid/media/AudioTrack;->access$300(Landroid/media/AudioTrack;)Landroid/media/AudioTrack$OnPlaybackPositionUpdateListener;

    #@1c
    move-result-object v0

    #@1d
    .line 1164
    monitor-exit v2
    :try_end_1e
    .catchall {:try_start_13 .. :try_end_1e} :catchall_3e

    #@1e
    .line 1165
    iget v1, p1, Landroid/os/Message;->what:I

    #@20
    packed-switch v1, :pswitch_data_5a

    #@23
    .line 1177
    const-string v1, "AudioTrack-Java"

    #@25
    new-instance v2, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v3, "[ android.media.AudioTrack.NativeEventHandler ] Unknown event type: "

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    iget v3, p1, Landroid/os/Message;->what:I

    #@32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v2

    #@36
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v2

    #@3a
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    goto :goto_8

    #@3e
    .line 1164
    :catchall_3e
    move-exception v1

    #@3f
    :try_start_3f
    monitor-exit v2
    :try_end_40
    .catchall {:try_start_3f .. :try_end_40} :catchall_3e

    #@40
    throw v1

    #@41
    .line 1167
    :pswitch_41
    if-eqz v0, :cond_8

    #@43
    .line 1168
    iget-object v1, p0, Landroid/media/AudioTrack$NativeEventHandlerDelegate$1;->this$1:Landroid/media/AudioTrack$NativeEventHandlerDelegate;

    #@45
    invoke-static {v1}, Landroid/media/AudioTrack$NativeEventHandlerDelegate;->access$100(Landroid/media/AudioTrack$NativeEventHandlerDelegate;)Landroid/media/AudioTrack;

    #@48
    move-result-object v1

    #@49
    invoke-interface {v0, v1}, Landroid/media/AudioTrack$OnPlaybackPositionUpdateListener;->onMarkerReached(Landroid/media/AudioTrack;)V

    #@4c
    goto :goto_8

    #@4d
    .line 1172
    :pswitch_4d
    if-eqz v0, :cond_8

    #@4f
    .line 1173
    iget-object v1, p0, Landroid/media/AudioTrack$NativeEventHandlerDelegate$1;->this$1:Landroid/media/AudioTrack$NativeEventHandlerDelegate;

    #@51
    invoke-static {v1}, Landroid/media/AudioTrack$NativeEventHandlerDelegate;->access$100(Landroid/media/AudioTrack$NativeEventHandlerDelegate;)Landroid/media/AudioTrack;

    #@54
    move-result-object v1

    #@55
    invoke-interface {v0, v1}, Landroid/media/AudioTrack$OnPlaybackPositionUpdateListener;->onPeriodicNotification(Landroid/media/AudioTrack;)V

    #@58
    goto :goto_8

    #@59
    .line 1165
    nop

    #@5a
    :pswitch_data_5a
    .packed-switch 0x3
        :pswitch_41
        :pswitch_4d
    .end packed-switch
.end method
