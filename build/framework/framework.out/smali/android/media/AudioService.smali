.class public Landroid/media/AudioService;
.super Landroid/media/IAudioService$Stub;
.source "AudioService.java"

# interfaces
.implements Landroid/app/PendingIntent$OnFinished;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/AudioService$RcDisplayDeathHandler;,
        Landroid/media/AudioService$RemoteControlStackEntry;,
        Landroid/media/AudioService$RemotePlaybackState;,
        Landroid/media/AudioService$RcClientDeathHandler;,
        Landroid/media/AudioService$AudioFocusDeathHandler;,
        Landroid/media/AudioService$FocusStackEntry;,
        Landroid/media/AudioService$AudioServiceBroadcastReceiver;,
        Landroid/media/AudioService$SettingsObserver;,
        Landroid/media/AudioService$AudioHandler;,
        Landroid/media/AudioService$AudioSystemThread;,
        Landroid/media/AudioService$VolumeStreamState;,
        Landroid/media/AudioService$ScoClient;,
        Landroid/media/AudioService$SoundPoolCallback;,
        Landroid/media/AudioService$SoundPoolListenerThread;,
        Landroid/media/AudioService$SetModeDeathHandler;,
        Landroid/media/AudioService$ForceControlStreamClient;
    }
.end annotation


# static fields
.field public static final ACTION_VIRTUAL_MIRRORLINK_START:Ljava/lang/String; = "com.lge.mirrorlink.audio.started"

.field public static final ACTION_VIRTUAL_MIRRORLINK_STOP:Ljava/lang/String; = "com.lge.mirrorlink.audio.stopped"

.field private static final BTA2DP_DOCK_TIMEOUT_MILLIS:I = 0x1f40

.field private static final BT_HEADSET_CNCT_TIMEOUT_MS:I = 0xbb8

.field private static final CLIENT_ID_QCHAT:Ljava/lang/String; = "QCHAT"

.field protected static final DEBUG_RC:Z = false

.field protected static final DEBUG_VOL:Z = true

.field private static final DEFAULT_STREAM_TYPE_OVERRIDE_DELAY_MS:I = 0x1388

.field private static final EXTRA_WAKELOCK_ACQUIRED:Ljava/lang/String; = "android.media.AudioService.WAKELOCK_ACQUIRED"

.field private static final FLAG_TOAST_VOL_DOWN:I = 0x1

.field private static final FLAG_TOAST_VOL_TTY_MODE:I = 0x3

.field private static final FLAG_TOAST_VOL_UP:I = 0x2

.field public static final IN_VOICE_COMM_FOCUS_ID:Ljava/lang/String; = "AudioFocus_For_Phone_Ring_And_Calls"

.field private static final MAX_BATCH_VOLUME_ADJUST_STEPS:I = 0x4

.field private static final MAX_MASTER_VOLUME:I = 0x64

.field private static final MSG_BROADCAST_AUDIO_BECOMING_NOISY:I = 0x19

.field private static final MSG_BTA2DP_DOCK_TIMEOUT:I = 0x7

.field private static final MSG_BT_HEADSET_CNCT_FAILED:I = 0xb

.field private static final MSG_CHECK_MUSIC_ACTIVE:I = 0x18

.field private static final MSG_CONFIGURE_SAFE_MEDIA_VOLUME:I = 0x1a

.field private static final MSG_CONFIGURE_SAFE_MEDIA_VOLUME_FORCED:I = 0x1b

.field private static final MSG_LOAD_SOUND_EFFECTS:I = 0x8

.field private static final MSG_MEDIA_SERVER_DIED:I = 0x4

.field private static final MSG_MEDIA_SERVER_STARTED:I = 0x5

.field private static final MSG_PERSIST_MASTER_VOLUME:I = 0x2

.field private static final MSG_PERSIST_MASTER_VOLUME_MUTE:I = 0xf

.field private static final MSG_PERSIST_MEDIABUTTONRECEIVER:I = 0xa

.field private static final MSG_PERSIST_RINGER_MODE:I = 0x3

.field private static final MSG_PERSIST_SAFE_VOLUME_STATE:I = 0x1c

.field private static final MSG_PERSIST_VOLUME:I = 0x1

.field private static final MSG_PLAY_SOUND_EFFECT:I = 0x6

.field private static final MSG_RCC_NEW_PLAYBACK_INFO:I = 0x12

.field private static final MSG_RCC_NEW_VOLUME_OBS:I = 0x13

.field private static final MSG_RCDISPLAY_CLEAR:I = 0xc

.field private static final MSG_RCDISPLAY_UPDATE:I = 0xd

.field private static final MSG_REEVALUATE_REMOTE:I = 0x11

.field private static final MSG_REPORT_NEW_ROUTES:I = 0x10

.field private static final MSG_SET_A2DP_CONNECTION_STATE:I = 0x16

.field private static final MSG_SET_ALL_VOLUMES:I = 0xe

.field private static final MSG_SET_DEVICE_VOLUME:I = 0x0

.field private static final MSG_SET_FORCE_BT_A2DP_USE:I = 0x14

.field private static final MSG_SET_FORCE_USE:I = 0x9

.field private static final MSG_SET_RSX_CONNECTION_STATE:I = 0x17

.field private static final MSG_SET_WIRED_DEVICE_CONNECTION_STATE:I = 0x15

.field private static final MSG_SHOW_VOLUME_INFO:I = 0x1d

.field private static final MUSIC_ACTIVE_POLL_PERIOD_MS:I = 0xea60

.field private static final NUM_SOUNDPOOL_CHANNELS:I = 0x4

.field private static final PERSIST_CURRENT:I = 0x1

.field private static final PERSIST_DELAY:I = 0x1f4

.field private static final PERSIST_LAST_AUDIBLE:I = 0x2

.field private static final RC_INFO_ALL:I = 0xf

.field private static final RC_INFO_NONE:I = 0x0

.field private static final RINGER_MODE_NAMES:[Ljava/lang/String; = null

.field private static final SAFE_VOLUME_CONFIGURE_TIMEOUT_MS:I = 0x7530

.field private static final SCO_STATE_ACTIVATE_REQ:I = 0x1

.field private static final SCO_STATE_ACTIVE_EXTERNAL:I = 0x2

.field private static final SCO_STATE_ACTIVE_INTERNAL:I = 0x3

.field private static final SCO_STATE_DEACTIVATE_EXT_REQ:I = 0x4

.field private static final SCO_STATE_DEACTIVATE_REQ:I = 0x5

.field private static final SCO_STATE_INACTIVE:I = 0x0

.field private static final SENDMSG_NOOP:I = 0x1

.field private static final SENDMSG_QUEUE:I = 0x2

.field private static final SENDMSG_REPLACE:I = 0x0

.field private static final SOUND_EFFECTS_PATH:Ljava/lang/String; = "/media/audio/ui/"

.field private static final SOUND_EFFECT_FILES:[Ljava/lang/String; = null

.field public static final STREAM_REMOTE_MUSIC:I = -0xc8

.field private static final TAG:Ljava/lang/String; = "AudioService"

.field private static final UNSAFE_VOLUME_MUSIC_ACTIVE_MS_MAX:I = 0x44aa200

.field private static final VOICEBUTTON_ACTION_DISCARD_CURRENT_KEY_PRESS:I = 0x1

.field private static final VOICEBUTTON_ACTION_SIMULATE_KEY_PRESS:I = 0x3

.field private static final VOICEBUTTON_ACTION_START_VOICE_INPUT:I = 0x2

.field private static final WAKELOCK_RELEASE_ON_FINISHED:I = 0x7bc

.field private static final mAudioFocusLock:Ljava/lang/Object;

.field private static final mRingingLock:Ljava/lang/Object;

.field private static sLastRccId:I

.field private static sSoundEffectVolumeDb:I


# instance fields
.field private final MAX_STREAM_VOLUME:[I

.field MirroLinkTestKey:Z

.field private final SAFE_MEDIA_VOLUME_ACTIVE:I

.field private final SAFE_MEDIA_VOLUME_DISABLED:I

.field private final SAFE_MEDIA_VOLUME_INACTIVE:I

.field private final SAFE_MEDIA_VOLUME_NOT_CONFIGURED:I

.field private final SOUND_EFFECT_FILES_MAP:[[I

.field private final STREAM_NAMES:[Ljava/lang/String;

.field private final STREAM_VOLUME_ALIAS:[I

.field private final STREAM_VOLUME_ALIAS_NON_VOICE:[I

.field private mArtworkExpectedHeight:I

.field private mArtworkExpectedWidth:I

.field private mAudioHandler:Landroid/media/AudioService$AudioHandler;

.field private final mAudioSystemCallback:Landroid/media/AudioSystem$ErrorCallback;

.field private mAudioSystemThread:Landroid/media/AudioService$AudioSystemThread;

.field mBecomingNoisyIntentDevices:I

.field private mBluetoothA2dpEnabled:Z

.field private final mBluetoothA2dpEnabledLock:Ljava/lang/Object;

.field private mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

.field private mBluetoothHeadsetDevice:Landroid/bluetooth/BluetoothDevice;

.field private mBluetoothProfileServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

.field private mBootCompleted:Z

.field private mCameraSoundForced:Ljava/lang/Boolean;

.field private final mConnectedDevices:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mContext:Landroid/content/Context;

.field final mCurAudioRoutes:Landroid/media/AudioRoutesInfo;

.field private mCurrentRcClient:Landroid/media/IRemoteControlClient;

.field private mCurrentRcClientGen:I

.field private final mCurrentRcLock:Ljava/lang/Object;

.field private mDeviceOrientation:I

.field private mDockAddress:Ljava/lang/String;

.field private mDockAudioMediaEnabled:Z

.field private mDockState:I

.field final mFixedVolumeDevices:I

.field private final mFocusStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Landroid/media/AudioService$FocusStackEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mForceControlStreamClient:Landroid/media/AudioService$ForceControlStreamClient;

.field private final mForceControlStreamLock:Ljava/lang/Object;

.field private mForcedUseForComm:I

.field private mForcedUseForMedia:I

.field private mHasRemotePlayback:Z

.field private final mHasVibrator:Z

.field private mIsQuietModeSupport:Ljava/lang/Boolean;

.field private mIsRinging:Z

.field private mIsSoundException:Z

.field private mIsSoundProfile:Z

.field private mIsVolumePanelVisible:Z

.field mKeyEventDone:Landroid/content/BroadcastReceiver;

.field private mKeyguardManager:Landroid/app/KeyguardManager;

.field private mLgSafeMediaVolumeEnabled:Ljava/lang/Boolean;

.field private mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

.field private mMainRemoteIsActive:Z

.field private final mMasterVolumeRamp:[I

.field private mMcc:I

.field private mMediaEventWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mMediaReceiverForCalls:Landroid/content/ComponentName;

.field private final mMediaReceiverForCallsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private mMediaServerOk:Z

.field private mMode:I

.field private final mMonitorOrientation:Z

.field private mMusicActiveMs:I

.field private mMuteAffectedStreams:I

.field private mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mPrevVolDirection:I

.field private final mRCStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Landroid/media/AudioService$RemoteControlStackEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mRcDisplay:Landroid/media/IRemoteControlDisplay;

.field private mRcDisplayDeathHandler:Landroid/media/AudioService$RcDisplayDeathHandler;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mRecordHookingState:I

.field private mRecordHookingType:I

.field private mRingerMode:I

.field private mRingerModeAffectedStreams:I

.field private mRingerModeMutedStreams:I

.field private volatile mRingtonePlayer:Landroid/media/IRingtonePlayer;

.field final mRoutesObservers:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Landroid/media/IAudioRoutesObserver;",
            ">;"
        }
    .end annotation
.end field

.field private final mSafeMediaVolumeDevices:I

.field private mSafeMediaVolumeIndex:I

.field private mSafeMediaVolumeState:Ljava/lang/Integer;

.field private mSafeVoiceVolumeIndex:I

.field private mScoAudioState:I

.field private final mScoClients:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/media/AudioService$ScoClient;",
            ">;"
        }
    .end annotation
.end field

.field private mScoConnectionState:I

.field private final mSetModeDeathHandlers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/media/AudioService$SetModeDeathHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final mSettingsLock:Ljava/lang/Object;

.field private mSettingsObserver:Landroid/media/AudioService$SettingsObserver;

.field private final mSoundEffectsLock:Ljava/lang/Object;

.field private mSoundPool:Landroid/media/SoundPool;

.field private mSoundPoolCallBack:Landroid/media/AudioService$SoundPoolCallback;

.field private mSoundPoolListenerThread:Landroid/media/AudioService$SoundPoolListenerThread;

.field private mSoundPoolLooper:Landroid/os/Looper;

.field private mState:I

.field private mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

.field private mStreamVolumeAlias:[I

.field private mToast:Landroid/widget/Toast;

.field private final mUseEarpieceReceiver:Landroid/content/BroadcastReceiver;

.field private final mUseMasterVolume:Z

.field private mVibrateSetting:I

.field private mVoiceActivationState:I

.field private mVoiceButtonDown:Z

.field private mVoiceButtonHandled:Z

.field private mVoiceCapable:Z

.field private final mVoiceEventLock:Ljava/lang/Object;

.field private mVolumeControlStream:I

.field private mVolumePanel:Landroid/view/VolumePanel;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    const/4 v6, 0x3

    #@1
    const/4 v5, 0x2

    #@2
    const/4 v4, 0x1

    #@3
    const/4 v3, 0x0

    #@4
    .line 224
    const/4 v0, 0x7

    #@5
    new-array v0, v0, [Ljava/lang/String;

    #@7
    const-string v1, "Effect_Tick.ogg"

    #@9
    aput-object v1, v0, v3

    #@b
    const-string v1, "KeypressStandard.ogg"

    #@d
    aput-object v1, v0, v4

    #@f
    const-string v1, "KeypressSpacebar.ogg"

    #@11
    aput-object v1, v0, v5

    #@13
    const-string v1, "KeypressDelete.ogg"

    #@15
    aput-object v1, v0, v6

    #@17
    const/4 v1, 0x4

    #@18
    const-string v2, "KeypressReturn.ogg"

    #@1a
    aput-object v2, v0, v1

    #@1c
    const/4 v1, 0x5

    #@1d
    const-string/jumbo v2, "switchon.ogg"

    #@20
    aput-object v2, v0, v1

    #@22
    const/4 v1, 0x6

    #@23
    const-string/jumbo v2, "switchoff.ogg"

    #@26
    aput-object v2, v0, v1

    #@28
    sput-object v0, Landroid/media/AudioService;->SOUND_EFFECT_FILES:[Ljava/lang/String;

    #@2a
    .line 4831
    new-instance v0, Ljava/lang/Object;

    #@2c
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@2f
    sput-object v0, Landroid/media/AudioService;->mAudioFocusLock:Ljava/lang/Object;

    #@31
    .line 4833
    new-instance v0, Ljava/lang/Object;

    #@33
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@36
    sput-object v0, Landroid/media/AudioService;->mRingingLock:Ljava/lang/Object;

    #@38
    .line 5572
    sput v3, Landroid/media/AudioService;->sLastRccId:I

    #@3a
    .line 6934
    new-array v0, v6, [Ljava/lang/String;

    #@3c
    const-string v1, "SILENT"

    #@3e
    aput-object v1, v0, v3

    #@40
    const-string v1, "VIBRATE"

    #@42
    aput-object v1, v0, v4

    #@44
    const-string v1, "NORMAL"

    #@46
    aput-object v1, v0, v5

    #@48
    sput-object v0, Landroid/media/AudioService;->RINGER_MODE_NAMES:[Ljava/lang/String;

    #@4a
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 16
    .parameter "context"

    #@0
    .prologue
    .line 554
    invoke-direct {p0}, Landroid/media/IAudioService$Stub;-><init>()V

    #@3
    .line 134
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Landroid/media/AudioService;->MirroLinkTestKey:Z

    #@6
    .line 207
    new-instance v0, Ljava/lang/Object;

    #@8
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@b
    iput-object v0, p0, Landroid/media/AudioService;->mSettingsLock:Ljava/lang/Object;

    #@d
    .line 212
    new-instance v0, Ljava/lang/Object;

    #@f
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@12
    iput-object v0, p0, Landroid/media/AudioService;->mSoundEffectsLock:Ljava/lang/Object;

    #@14
    .line 239
    const/16 v0, 0xb

    #@16
    new-array v0, v0, [[I

    #@18
    const/4 v1, 0x0

    #@19
    const/4 v2, 0x2

    #@1a
    new-array v2, v2, [I

    #@1c
    fill-array-data v2, :array_3b8

    #@1f
    aput-object v2, v0, v1

    #@21
    const/4 v1, 0x1

    #@22
    const/4 v2, 0x2

    #@23
    new-array v2, v2, [I

    #@25
    fill-array-data v2, :array_3c0

    #@28
    aput-object v2, v0, v1

    #@2a
    const/4 v1, 0x2

    #@2b
    const/4 v2, 0x2

    #@2c
    new-array v2, v2, [I

    #@2e
    fill-array-data v2, :array_3c8

    #@31
    aput-object v2, v0, v1

    #@33
    const/4 v1, 0x3

    #@34
    const/4 v2, 0x2

    #@35
    new-array v2, v2, [I

    #@37
    fill-array-data v2, :array_3d0

    #@3a
    aput-object v2, v0, v1

    #@3c
    const/4 v1, 0x4

    #@3d
    const/4 v2, 0x2

    #@3e
    new-array v2, v2, [I

    #@40
    fill-array-data v2, :array_3d8

    #@43
    aput-object v2, v0, v1

    #@45
    const/4 v1, 0x5

    #@46
    const/4 v2, 0x2

    #@47
    new-array v2, v2, [I

    #@49
    fill-array-data v2, :array_3e0

    #@4c
    aput-object v2, v0, v1

    #@4e
    const/4 v1, 0x6

    #@4f
    const/4 v2, 0x2

    #@50
    new-array v2, v2, [I

    #@52
    fill-array-data v2, :array_3e8

    #@55
    aput-object v2, v0, v1

    #@57
    const/4 v1, 0x7

    #@58
    const/4 v2, 0x2

    #@59
    new-array v2, v2, [I

    #@5b
    fill-array-data v2, :array_3f0

    #@5e
    aput-object v2, v0, v1

    #@60
    const/16 v1, 0x8

    #@62
    const/4 v2, 0x2

    #@63
    new-array v2, v2, [I

    #@65
    fill-array-data v2, :array_3f8

    #@68
    aput-object v2, v0, v1

    #@6a
    const/16 v1, 0x9

    #@6c
    const/4 v2, 0x2

    #@6d
    new-array v2, v2, [I

    #@6f
    fill-array-data v2, :array_400

    #@72
    aput-object v2, v0, v1

    #@74
    const/16 v1, 0xa

    #@76
    const/4 v2, 0x2

    #@77
    new-array v2, v2, [I

    #@79
    fill-array-data v2, :array_408

    #@7c
    aput-object v2, v0, v1

    #@7e
    iput-object v0, p0, Landroid/media/AudioService;->SOUND_EFFECT_FILES_MAP:[[I

    #@80
    .line 256
    const/16 v0, 0xd

    #@82
    new-array v0, v0, [I

    #@84
    fill-array-data v0, :array_410

    #@87
    iput-object v0, p0, Landroid/media/AudioService;->MAX_STREAM_VOLUME:[I

    #@89
    .line 281
    const/16 v0, 0xd

    #@8b
    new-array v0, v0, [I

    #@8d
    fill-array-data v0, :array_42e

    #@90
    iput-object v0, p0, Landroid/media/AudioService;->STREAM_VOLUME_ALIAS:[I

    #@92
    .line 302
    const/16 v0, 0xc

    #@94
    new-array v0, v0, [I

    #@96
    fill-array-data v0, :array_44c

    #@99
    iput-object v0, p0, Landroid/media/AudioService;->STREAM_VOLUME_ALIAS_NON_VOICE:[I

    #@9b
    .line 319
    const/16 v0, 0xc

    #@9d
    new-array v0, v0, [Ljava/lang/String;

    #@9f
    const/4 v1, 0x0

    #@a0
    const-string v2, "STREAM_VOICE_CALL"

    #@a2
    aput-object v2, v0, v1

    #@a4
    const/4 v1, 0x1

    #@a5
    const-string v2, "STREAM_SYSTEM"

    #@a7
    aput-object v2, v0, v1

    #@a9
    const/4 v1, 0x2

    #@aa
    const-string v2, "STREAM_RING"

    #@ac
    aput-object v2, v0, v1

    #@ae
    const/4 v1, 0x3

    #@af
    const-string v2, "STREAM_MUSIC"

    #@b1
    aput-object v2, v0, v1

    #@b3
    const/4 v1, 0x4

    #@b4
    const-string v2, "STREAM_ALARM"

    #@b6
    aput-object v2, v0, v1

    #@b8
    const/4 v1, 0x5

    #@b9
    const-string v2, "STREAM_NOTIFICATION"

    #@bb
    aput-object v2, v0, v1

    #@bd
    const/4 v1, 0x6

    #@be
    const-string v2, "STREAM_BLUETOOTH_SCO"

    #@c0
    aput-object v2, v0, v1

    #@c2
    const/4 v1, 0x7

    #@c3
    const-string v2, "STREAM_SYSTEM_ENFORCED"

    #@c5
    aput-object v2, v0, v1

    #@c7
    const/16 v1, 0x8

    #@c9
    const-string v2, "STREAM_DTMF"

    #@cb
    aput-object v2, v0, v1

    #@cd
    const/16 v1, 0x9

    #@cf
    const-string v2, "STREAM_TTS"

    #@d1
    aput-object v2, v0, v1

    #@d3
    const/16 v1, 0xa

    #@d5
    const-string v2, "STREAM_FM"

    #@d7
    aput-object v2, v0, v1

    #@d9
    const/16 v1, 0xb

    #@db
    const-string v2, "STREAM_DMB"

    #@dd
    aput-object v2, v0, v1

    #@df
    iput-object v0, p0, Landroid/media/AudioService;->STREAM_NAMES:[Ljava/lang/String;

    #@e1
    .line 334
    new-instance v0, Landroid/media/AudioService$1;

    #@e3
    invoke-direct {v0, p0}, Landroid/media/AudioService$1;-><init>(Landroid/media/AudioService;)V

    #@e6
    iput-object v0, p0, Landroid/media/AudioService;->mAudioSystemCallback:Landroid/media/AudioSystem$ErrorCallback;

    #@e8
    .line 385
    new-instance v0, Landroid/media/AudioService$AudioServiceBroadcastReceiver;

    #@ea
    const/4 v1, 0x0

    #@eb
    invoke-direct {v0, p0, v1}, Landroid/media/AudioService$AudioServiceBroadcastReceiver;-><init>(Landroid/media/AudioService;Landroid/media/AudioService$1;)V

    #@ee
    iput-object v0, p0, Landroid/media/AudioService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@f0
    .line 388
    const/4 v0, 0x0

    #@f1
    iput-boolean v0, p0, Landroid/media/AudioService;->mIsRinging:Z

    #@f3
    .line 396
    new-instance v0, Ljava/util/HashMap;

    #@f5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@f8
    iput-object v0, p0, Landroid/media/AudioService;->mConnectedDevices:Ljava/util/HashMap;

    #@fa
    .line 408
    new-instance v0, Ljava/util/ArrayList;

    #@fc
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@ff
    iput-object v0, p0, Landroid/media/AudioService;->mSetModeDeathHandlers:Ljava/util/ArrayList;

    #@101
    .line 411
    new-instance v0, Ljava/util/ArrayList;

    #@103
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@106
    iput-object v0, p0, Landroid/media/AudioService;->mScoClients:Ljava/util/ArrayList;

    #@108
    .line 448
    const/4 v0, 0x0

    #@109
    iput-object v0, p0, Landroid/media/AudioService;->mSoundPoolLooper:Landroid/os/Looper;

    #@10b
    .line 457
    const/4 v0, 0x0

    #@10c
    iput v0, p0, Landroid/media/AudioService;->mPrevVolDirection:I

    #@10e
    .line 462
    const/4 v0, -0x1

    #@10f
    iput v0, p0, Landroid/media/AudioService;->mVolumeControlStream:I

    #@111
    .line 463
    new-instance v0, Ljava/lang/Object;

    #@113
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@116
    iput-object v0, p0, Landroid/media/AudioService;->mForceControlStreamLock:Ljava/lang/Object;

    #@118
    .line 467
    const/4 v0, 0x0

    #@119
    iput-object v0, p0, Landroid/media/AudioService;->mForceControlStreamClient:Landroid/media/AudioService$ForceControlStreamClient;

    #@11b
    .line 472
    const/4 v0, 0x0

    #@11c
    iput-boolean v0, p0, Landroid/media/AudioService;->mIsSoundProfile:Z

    #@11e
    .line 474
    const/4 v0, 0x0

    #@11f
    iput-boolean v0, p0, Landroid/media/AudioService;->mIsVolumePanelVisible:Z

    #@121
    .line 476
    const/4 v0, 0x0

    #@122
    iput v0, p0, Landroid/media/AudioService;->mDeviceOrientation:I

    #@124
    .line 480
    new-instance v0, Ljava/lang/Object;

    #@126
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@129
    iput-object v0, p0, Landroid/media/AudioService;->mBluetoothA2dpEnabledLock:Ljava/lang/Object;

    #@12b
    .line 483
    new-instance v0, Landroid/media/AudioRoutesInfo;

    #@12d
    invoke-direct {v0}, Landroid/media/AudioRoutesInfo;-><init>()V

    #@130
    iput-object v0, p0, Landroid/media/AudioService;->mCurAudioRoutes:Landroid/media/AudioRoutesInfo;

    #@132
    .line 484
    new-instance v0, Landroid/os/RemoteCallbackList;

    #@134
    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    #@137
    iput-object v0, p0, Landroid/media/AudioService;->mRoutesObservers:Landroid/os/RemoteCallbackList;

    #@139
    .line 493
    const/4 v0, 0x0

    #@13a
    iput-boolean v0, p0, Landroid/media/AudioService;->mIsSoundException:Z

    #@13c
    .line 511
    const/4 v0, 0x0

    #@13d
    iput v0, p0, Landroid/media/AudioService;->mFixedVolumeDevices:I

    #@13f
    .line 518
    const/4 v0, 0x1

    #@140
    iput-boolean v0, p0, Landroid/media/AudioService;->mDockAudioMediaEnabled:Z

    #@142
    .line 520
    const/4 v0, 0x0

    #@143
    iput v0, p0, Landroid/media/AudioService;->mDockState:I

    #@145
    .line 524
    new-instance v0, Landroid/media/AudioService$2;

    #@147
    invoke-direct {v0, p0}, Landroid/media/AudioService$2;-><init>(Landroid/media/AudioService;)V

    #@14a
    iput-object v0, p0, Landroid/media/AudioService;->mUseEarpieceReceiver:Landroid/content/BroadcastReceiver;

    #@14c
    .line 2765
    new-instance v0, Landroid/media/AudioService$3;

    #@14e
    invoke-direct {v0, p0}, Landroid/media/AudioService$3;-><init>(Landroid/media/AudioService;)V

    #@151
    iput-object v0, p0, Landroid/media/AudioService;->mBluetoothProfileServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    #@153
    .line 4366
    const/16 v0, 0x7f8c

    #@155
    iput v0, p0, Landroid/media/AudioService;->mBecomingNoisyIntentDevices:I

    #@157
    .line 4835
    new-instance v0, Landroid/media/AudioService$4;

    #@159
    invoke-direct {v0, p0}, Landroid/media/AudioService$4;-><init>(Landroid/media/AudioService;)V

    #@15c
    iput-object v0, p0, Landroid/media/AudioService;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    #@15e
    .line 4937
    new-instance v0, Ljava/util/Stack;

    #@160
    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    #@163
    iput-object v0, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@165
    .line 5330
    new-instance v0, Ljava/lang/Object;

    #@167
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@16a
    iput-object v0, p0, Landroid/media/AudioService;->mVoiceEventLock:Ljava/lang/Object;

    #@16c
    .line 5505
    new-instance v0, Landroid/media/AudioService$5;

    #@16e
    invoke-direct {v0, p0}, Landroid/media/AudioService$5;-><init>(Landroid/media/AudioService;)V

    #@171
    iput-object v0, p0, Landroid/media/AudioService;->mKeyEventDone:Landroid/content/BroadcastReceiver;

    #@173
    .line 5520
    new-instance v0, Ljava/lang/Object;

    #@175
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@178
    iput-object v0, p0, Landroid/media/AudioService;->mCurrentRcLock:Ljava/lang/Object;

    #@17a
    .line 5526
    const/4 v0, 0x0

    #@17b
    iput-object v0, p0, Landroid/media/AudioService;->mCurrentRcClient:Landroid/media/IRemoteControlClient;

    #@17d
    .line 5540
    const/4 v0, 0x0

    #@17e
    iput v0, p0, Landroid/media/AudioService;->mCurrentRcClientGen:I

    #@180
    .line 5685
    new-instance v0, Ljava/util/Stack;

    #@182
    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    #@185
    iput-object v0, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@187
    .line 5691
    const/4 v0, 0x0

    #@188
    iput-object v0, p0, Landroid/media/AudioService;->mMediaReceiverForCalls:Landroid/content/ComponentName;

    #@18a
    .line 5693
    new-instance v0, Ljava/util/ArrayList;

    #@18c
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@18f
    iput-object v0, p0, Landroid/media/AudioService;->mMediaReceiverForCallsList:Ljava/util/ArrayList;

    #@191
    .line 6246
    const/4 v0, -0x1

    #@192
    iput v0, p0, Landroid/media/AudioService;->mArtworkExpectedWidth:I

    #@194
    .line 6247
    const/4 v0, -0x1

    #@195
    iput v0, p0, Landroid/media/AudioService;->mArtworkExpectedHeight:I

    #@197
    .line 6800
    const/4 v0, 0x0

    #@198
    iput v0, p0, Landroid/media/AudioService;->SAFE_MEDIA_VOLUME_NOT_CONFIGURED:I

    #@19a
    .line 6801
    const/4 v0, 0x1

    #@19b
    iput v0, p0, Landroid/media/AudioService;->SAFE_MEDIA_VOLUME_DISABLED:I

    #@19d
    .line 6802
    const/4 v0, 0x2

    #@19e
    iput v0, p0, Landroid/media/AudioService;->SAFE_MEDIA_VOLUME_INACTIVE:I

    #@1a0
    .line 6803
    const/4 v0, 0x3

    #@1a1
    iput v0, p0, Landroid/media/AudioService;->SAFE_MEDIA_VOLUME_ACTIVE:I

    #@1a3
    .line 6806
    const/4 v0, 0x0

    #@1a4
    iput v0, p0, Landroid/media/AudioService;->mMcc:I

    #@1a6
    .line 6815
    const/16 v0, 0xc

    #@1a8
    iput v0, p0, Landroid/media/AudioService;->mSafeMediaVolumeDevices:I

    #@1aa
    .line 7241
    const/4 v0, 0x0

    #@1ab
    iput v0, p0, Landroid/media/AudioService;->mVoiceActivationState:I

    #@1ad
    .line 7262
    const/4 v0, 0x0

    #@1ae
    iput v0, p0, Landroid/media/AudioService;->mRecordHookingState:I

    #@1b0
    .line 7263
    const/4 v0, 0x0

    #@1b1
    iput v0, p0, Landroid/media/AudioService;->mRecordHookingType:I

    #@1b3
    .line 555
    iput-object p1, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@1b5
    .line 556
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1b8
    move-result-object v0

    #@1b9
    iput-object v0, p0, Landroid/media/AudioService;->mContentResolver:Landroid/content/ContentResolver;

    #@1bb
    .line 557
    iget-object v0, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@1bd
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1c0
    move-result-object v0

    #@1c1
    const v1, 0x1110030

    #@1c4
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@1c7
    move-result v0

    #@1c8
    iput-boolean v0, p0, Landroid/media/AudioService;->mVoiceCapable:Z

    #@1ca
    .line 560
    const-string/jumbo v0, "power"

    #@1cd
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1d0
    move-result-object v11

    #@1d1
    check-cast v11, Landroid/os/PowerManager;

    #@1d3
    .line 561
    .local v11, pm:Landroid/os/PowerManager;
    const/4 v0, 0x1

    #@1d4
    const-string v1, "handleMediaEvent"

    #@1d6
    invoke-virtual {v11, v0, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@1d9
    move-result-object v0

    #@1da
    iput-object v0, p0, Landroid/media/AudioService;->mMediaEventWakeLock:Landroid/os/PowerManager$WakeLock;

    #@1dc
    .line 563
    const-string/jumbo v0, "vibrator"

    #@1df
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1e2
    move-result-object v13

    #@1e3
    check-cast v13, Landroid/os/Vibrator;

    #@1e5
    .line 564
    .local v13, vibrator:Landroid/os/Vibrator;
    if-nez v13, :cond_3a1

    #@1e7
    const/4 v0, 0x0

    #@1e8
    :goto_1e8
    iput-boolean v0, p0, Landroid/media/AudioService;->mHasVibrator:Z

    #@1ea
    .line 567
    iget-object v0, p0, Landroid/media/AudioService;->MAX_STREAM_VOLUME:[I

    #@1ec
    const/4 v1, 0x0

    #@1ed
    const-string/jumbo v2, "ro.config.vc_call_vol_steps"

    #@1f0
    iget-object v3, p0, Landroid/media/AudioService;->MAX_STREAM_VOLUME:[I

    #@1f2
    const/4 v4, 0x0

    #@1f3
    aget v3, v3, v4

    #@1f5
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@1f8
    move-result v2

    #@1f9
    aput v2, v0, v1

    #@1fb
    .line 571
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1fe
    move-result-object v0

    #@1ff
    const v1, 0x10e0004

    #@202
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@205
    move-result v0

    #@206
    sput v0, Landroid/media/AudioService;->sSoundEffectVolumeDb:I

    #@208
    .line 575
    const-string/jumbo v0, "ro.lge.audio_soundexception"

    #@20b
    const/4 v1, 0x0

    #@20c
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@20f
    move-result v0

    #@210
    iput-boolean v0, p0, Landroid/media/AudioService;->mIsSoundException:Z

    #@212
    .line 578
    const-string/jumbo v0, "ro.lge.audio_soundprofile"

    #@215
    const/4 v1, 0x0

    #@216
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@219
    move-result v0

    #@21a
    iput-boolean v0, p0, Landroid/media/AudioService;->mIsSoundProfile:Z

    #@21c
    .line 580
    new-instance v0, Landroid/view/VolumePanel;

    #@21e
    invoke-direct {v0, p1, p0}, Landroid/view/VolumePanel;-><init>(Landroid/content/Context;Landroid/media/AudioService;)V

    #@221
    iput-object v0, p0, Landroid/media/AudioService;->mVolumePanel:Landroid/view/VolumePanel;

    #@223
    .line 581
    const/4 v0, 0x0

    #@224
    iput v0, p0, Landroid/media/AudioService;->mMode:I

    #@226
    .line 582
    const/4 v0, 0x0

    #@227
    iput v0, p0, Landroid/media/AudioService;->mState:I

    #@229
    .line 583
    const/4 v0, 0x0

    #@22a
    iput v0, p0, Landroid/media/AudioService;->mForcedUseForComm:I

    #@22c
    .line 584
    const/4 v0, 0x0

    #@22d
    iput v0, p0, Landroid/media/AudioService;->mForcedUseForMedia:I

    #@22f
    .line 586
    invoke-direct {p0}, Landroid/media/AudioService;->createAudioSystemThread()V

    #@232
    .line 588
    iget-object v0, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@234
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@237
    move-result-object v0

    #@238
    const v1, 0x111004b

    #@23b
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@23e
    move-result v7

    #@23f
    .line 590
    .local v7, cameraSoundForced:Z
    new-instance v0, Ljava/lang/Boolean;

    #@241
    invoke-direct {v0, v7}, Ljava/lang/Boolean;-><init>(Z)V

    #@244
    iput-object v0, p0, Landroid/media/AudioService;->mCameraSoundForced:Ljava/lang/Boolean;

    #@246
    .line 591
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@248
    const/16 v1, 0x9

    #@24a
    const/4 v2, 0x2

    #@24b
    const/4 v3, 0x4

    #@24c
    if-eqz v7, :cond_3a7

    #@24e
    const/16 v4, 0xb

    #@250
    :goto_250
    const/4 v5, 0x0

    #@251
    const/4 v6, 0x0

    #@252
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@255
    .line 602
    :try_start_255
    iget-object v0, p0, Landroid/media/AudioService;->mContentResolver:Landroid/content/ContentResolver;

    #@257
    const-string/jumbo v1, "quiet_mode_support_model"

    #@25a
    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    #@25d
    move-result v0

    #@25e
    const/4 v1, 0x1

    #@25f
    if-ne v0, v1, :cond_3aa

    #@261
    const/4 v0, 0x1

    #@262
    :goto_262
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@265
    move-result-object v0

    #@266
    iput-object v0, p0, Landroid/media/AudioService;->mIsQuietModeSupport:Ljava/lang/Boolean;
    :try_end_268
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_255 .. :try_end_268} :catch_3ad

    #@268
    .line 609
    :goto_268
    new-instance v0, Ljava/lang/Integer;

    #@26a
    iget-object v1, p0, Landroid/media/AudioService;->mContentResolver:Landroid/content/ContentResolver;

    #@26c
    const-string v2, "audio_safe_volume_state"

    #@26e
    const/4 v3, 0x0

    #@26f
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@272
    move-result v1

    #@273
    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    #@276
    iput-object v0, p0, Landroid/media/AudioService;->mSafeMediaVolumeState:Ljava/lang/Integer;

    #@278
    .line 614
    const-string/jumbo v0, "ro.config.vc_call_vol_default"

    #@27b
    sget-object v1, Landroid/media/AudioManager;->DEFAULT_STREAM_VOLUME:[I

    #@27d
    const/4 v2, 0x0

    #@27e
    aget v1, v1, v2

    #@280
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@283
    move-result v0

    #@284
    iput v0, p0, Landroid/media/AudioService;->mSafeVoiceVolumeIndex:I

    #@286
    .line 619
    iget-object v0, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@288
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@28b
    move-result-object v0

    #@28c
    const v1, 0x10e003a

    #@28f
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@292
    move-result v0

    #@293
    mul-int/lit8 v0, v0, 0xa

    #@295
    iput v0, p0, Landroid/media/AudioService;->mSafeMediaVolumeIndex:I

    #@297
    .line 622
    iget-object v0, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@299
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@29c
    move-result-object v0

    #@29d
    const v1, 0x206001b

    #@2a0
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@2a3
    move-result v0

    #@2a4
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@2a7
    move-result-object v0

    #@2a8
    iput-object v0, p0, Landroid/media/AudioService;->mLgSafeMediaVolumeEnabled:Ljava/lang/Boolean;

    #@2aa
    .line 626
    invoke-direct {p0}, Landroid/media/AudioService;->readPersistedSettings()V

    #@2ad
    .line 627
    new-instance v0, Landroid/media/AudioService$SettingsObserver;

    #@2af
    invoke-direct {v0, p0}, Landroid/media/AudioService$SettingsObserver;-><init>(Landroid/media/AudioService;)V

    #@2b2
    iput-object v0, p0, Landroid/media/AudioService;->mSettingsObserver:Landroid/media/AudioService$SettingsObserver;

    #@2b4
    .line 628
    const/4 v0, 0x0

    #@2b5
    invoke-direct {p0, v0}, Landroid/media/AudioService;->updateStreamVolumeAlias(Z)V

    #@2b8
    .line 629
    invoke-direct {p0}, Landroid/media/AudioService;->createStreamStates()V

    #@2bb
    .line 631
    const/4 v0, 0x1

    #@2bc
    iput-boolean v0, p0, Landroid/media/AudioService;->mMediaServerOk:Z

    #@2be
    .line 635
    const/4 v0, 0x0

    #@2bf
    iput v0, p0, Landroid/media/AudioService;->mRingerModeMutedStreams:I

    #@2c1
    .line 636
    invoke-virtual {p0}, Landroid/media/AudioService;->getRingerMode()I

    #@2c4
    move-result v0

    #@2c5
    const/4 v1, 0x0

    #@2c6
    invoke-direct {p0, v0, v1}, Landroid/media/AudioService;->setRingerModeInt(IZ)V

    #@2c9
    .line 638
    iget-object v0, p0, Landroid/media/AudioService;->mAudioSystemCallback:Landroid/media/AudioSystem$ErrorCallback;

    #@2cb
    invoke-static {v0}, Landroid/media/AudioSystem;->setErrorCallback(Landroid/media/AudioSystem$ErrorCallback;)V

    #@2ce
    .line 641
    new-instance v9, Landroid/content/IntentFilter;

    #@2d0
    const-string v0, "android.bluetooth.headset.profile.action.AUDIO_STATE_CHANGED"

    #@2d2
    invoke-direct {v9, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@2d5
    .line 643
    .local v9, intentFilter:Landroid/content/IntentFilter;
    const-string v0, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    #@2d7
    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2da
    .line 644
    const-string v0, "android.intent.action.DOCK_EVENT"

    #@2dc
    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2df
    .line 645
    const-string/jumbo v0, "qualcomm.intent.action.FM"

    #@2e2
    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2e5
    .line 646
    const-string/jumbo v0, "qualcomm.intent.action.FMTX"

    #@2e8
    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2eb
    .line 647
    const-string v0, "android.intent.action.USB_AUDIO_ACCESSORY_PLUG"

    #@2ed
    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2f0
    .line 648
    const-string v0, "android.intent.action.USB_AUDIO_DEVICE_PLUG"

    #@2f2
    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2f5
    .line 649
    const-string v0, "android.intent.action.BOOT_COMPLETED"

    #@2f7
    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2fa
    .line 650
    const-string v0, "android.intent.action.SCREEN_ON"

    #@2fc
    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2ff
    .line 651
    const-string v0, "android.intent.action.SCREEN_OFF"

    #@301
    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@304
    .line 652
    const-string v0, "android.intent.action.USER_SWITCHED"

    #@306
    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@309
    .line 653
    const-string/jumbo v0, "qualcomm.intent.action.WIFI_DISPLAY_AUDIO"

    #@30c
    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@30f
    .line 654
    const-string v0, "android.intent.action.LOCALE_CHANGED"

    #@311
    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@314
    .line 656
    const-string v0, "android.intent.action.CONFIGURATION_CHANGED"

    #@316
    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@319
    .line 659
    const-string v0, "com.lge.mirrorlink.audio.started"

    #@31b
    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@31e
    .line 660
    const-string v0, "com.lge.mirrorlink.audio.stopped"

    #@320
    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@323
    .line 666
    const-string/jumbo v0, "ro.audio.monitorOrientation"

    #@326
    const/4 v1, 0x0

    #@327
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@32a
    move-result v0

    #@32b
    iput-boolean v0, p0, Landroid/media/AudioService;->mMonitorOrientation:Z

    #@32d
    .line 667
    iget-boolean v0, p0, Landroid/media/AudioService;->mMonitorOrientation:Z

    #@32f
    if-eqz v0, :cond_33c

    #@331
    .line 668
    const-string v0, "AudioService"

    #@333
    const-string/jumbo v1, "monitoring device orientation"

    #@336
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@339
    .line 670
    invoke-direct {p0}, Landroid/media/AudioService;->setOrientationForAudioSystem()V

    #@33c
    .line 673
    :cond_33c
    iget-object v0, p0, Landroid/media/AudioService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@33e
    invoke-virtual {p1, v0, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@341
    .line 676
    new-instance v10, Landroid/content/IntentFilter;

    #@343
    invoke-direct {v10}, Landroid/content/IntentFilter;-><init>()V

    #@346
    .line 677
    .local v10, pkgFilter:Landroid/content/IntentFilter;
    const-string v0, "android.intent.action.PACKAGE_REMOVED"

    #@348
    invoke-virtual {v10, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@34b
    .line 678
    const-string/jumbo v0, "package"

    #@34e
    invoke-virtual {v10, v0}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    #@351
    .line 679
    iget-object v0, p0, Landroid/media/AudioService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@353
    invoke-virtual {p1, v0, v10}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@356
    .line 682
    const-string/jumbo v0, "phone"

    #@359
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@35c
    move-result-object v12

    #@35d
    check-cast v12, Landroid/telephony/TelephonyManager;

    #@35f
    .line 684
    .local v12, tmgr:Landroid/telephony/TelephonyManager;
    iget-object v0, p0, Landroid/media/AudioService;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    #@361
    const/16 v1, 0x20

    #@363
    invoke-virtual {v12, v0, v1}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    #@366
    .line 686
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@369
    move-result-object v0

    #@36a
    const v1, 0x1110010

    #@36d
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@370
    move-result v0

    #@371
    iput-boolean v0, p0, Landroid/media/AudioService;->mUseMasterVolume:Z

    #@373
    .line 688
    invoke-direct {p0}, Landroid/media/AudioService;->restoreMasterVolume()V

    #@376
    .line 690
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@379
    move-result-object v0

    #@37a
    const v1, 0x1070017

    #@37d
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    #@380
    move-result-object v0

    #@381
    iput-object v0, p0, Landroid/media/AudioService;->mMasterVolumeRamp:[I

    #@383
    .line 693
    new-instance v0, Landroid/media/AudioService$RemotePlaybackState;

    #@385
    const/4 v2, -0x1

    #@386
    iget-object v1, p0, Landroid/media/AudioService;->MAX_STREAM_VOLUME:[I

    #@388
    const/4 v3, 0x3

    #@389
    aget v3, v1, v3

    #@38b
    iget-object v1, p0, Landroid/media/AudioService;->MAX_STREAM_VOLUME:[I

    #@38d
    const/4 v4, 0x3

    #@38e
    aget v4, v1, v4

    #@390
    const/4 v5, 0x0

    #@391
    move-object v1, p0

    #@392
    invoke-direct/range {v0 .. v5}, Landroid/media/AudioService$RemotePlaybackState;-><init>(Landroid/media/AudioService;IIILandroid/media/AudioService$1;)V

    #@395
    iput-object v0, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@397
    .line 695
    const/4 v0, 0x0

    #@398
    iput-boolean v0, p0, Landroid/media/AudioService;->mHasRemotePlayback:Z

    #@39a
    .line 696
    const/4 v0, 0x0

    #@39b
    iput-boolean v0, p0, Landroid/media/AudioService;->mMainRemoteIsActive:Z

    #@39d
    .line 697
    invoke-direct {p0}, Landroid/media/AudioService;->postReevaluateRemote()V

    #@3a0
    .line 698
    return-void

    #@3a1
    .line 564
    .end local v7           #cameraSoundForced:Z
    .end local v9           #intentFilter:Landroid/content/IntentFilter;
    .end local v10           #pkgFilter:Landroid/content/IntentFilter;
    .end local v12           #tmgr:Landroid/telephony/TelephonyManager;
    :cond_3a1
    invoke-virtual {v13}, Landroid/os/Vibrator;->hasVibrator()Z

    #@3a4
    move-result v0

    #@3a5
    goto/16 :goto_1e8

    #@3a7
    .line 591
    .restart local v7       #cameraSoundForced:Z
    :cond_3a7
    const/4 v4, 0x0

    #@3a8
    goto/16 :goto_250

    #@3aa
    .line 602
    :cond_3aa
    const/4 v0, 0x0

    #@3ab
    goto/16 :goto_262

    #@3ad
    .line 604
    :catch_3ad
    move-exception v8

    #@3ae
    .line 605
    .local v8, e:Landroid/provider/Settings$SettingNotFoundException;
    const-string v0, "AudioService"

    #@3b0
    const-string v1, "SettingNotFoundException - getDBQuietModeSupport()"

    #@3b2
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3b5
    goto/16 :goto_268

    #@3b7
    .line 239
    nop

    #@3b8
    :array_3b8
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0xfft 0xfft 0xfft 0xfft
    .end array-data

    #@3c0
    :array_3c0
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0xfft 0xfft 0xfft 0xfft
    .end array-data

    #@3c8
    :array_3c8
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0xfft 0xfft 0xfft 0xfft
    .end array-data

    #@3d0
    :array_3d0
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0xfft 0xfft 0xfft 0xfft
    .end array-data

    #@3d8
    :array_3d8
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0xfft 0xfft 0xfft 0xfft
    .end array-data

    #@3e0
    :array_3e0
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0xfft 0xfft 0xfft 0xfft
    .end array-data

    #@3e8
    :array_3e8
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0xfft 0xfft 0xfft 0xfft
    .end array-data

    #@3f0
    :array_3f0
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0xfft 0xfft 0xfft 0xfft
    .end array-data

    #@3f8
    :array_3f8
    .array-data 0x4
        0x4t 0x0t 0x0t 0x0t
        0xfft 0xfft 0xfft 0xfft
    .end array-data

    #@400
    :array_400
    .array-data 0x4
        0x5t 0x0t 0x0t 0x0t
        0xfft 0xfft 0xfft 0xfft
    .end array-data

    #@408
    :array_408
    .array-data 0x4
        0x6t 0x0t 0x0t 0x0t
        0xfft 0xfft 0xfft 0xfft
    .end array-data

    #@410
    .line 256
    :array_410
    .array-data 0x4
        0x5t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
        0xft 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
        0xft 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
        0xft 0x0t 0x0t 0x0t
        0xft 0x0t 0x0t 0x0t
        0xft 0x0t 0x0t 0x0t
        0xft 0x0t 0x0t 0x0t
        0xft 0x0t 0x0t 0x0t
    .end array-data

    #@42e
    .line 281
    :array_42e
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    #@44c
    .line 302
    :array_44c
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method private ToastPopUp(Ljava/lang/CharSequence;)V
    .registers 4
    .parameter "s"

    #@0
    .prologue
    .line 7132
    iget-object v0, p0, Landroid/media/AudioService;->mToast:Landroid/widget/Toast;

    #@2
    if-nez v0, :cond_1a

    #@4
    .line 7133
    const-string v0, "AudioService"

    #@6
    const-string v1, "ToastPopUp mToast == null"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 7134
    iget-object v0, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@d
    const/4 v1, 0x1

    #@e
    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@11
    move-result-object v0

    #@12
    iput-object v0, p0, Landroid/media/AudioService;->mToast:Landroid/widget/Toast;

    #@14
    .line 7139
    :goto_14
    iget-object v0, p0, Landroid/media/AudioService;->mToast:Landroid/widget/Toast;

    #@16
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    #@19
    .line 7140
    return-void

    #@1a
    .line 7136
    :cond_1a
    const-string v0, "AudioService"

    #@1c
    const-string v1, "ToastPopUp mToast setText"

    #@1e
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 7137
    iget-object v0, p0, Landroid/media/AudioService;->mToast:Landroid/widget/Toast;

    #@23
    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    #@26
    goto :goto_14
.end method

.method static synthetic access$000(Landroid/media/AudioService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-boolean v0, p0, Landroid/media/AudioService;->mMediaServerOk:Z

    #@2
    return v0
.end method

.method static synthetic access$002(Landroid/media/AudioService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    iput-boolean p1, p0, Landroid/media/AudioService;->mMediaServerOk:Z

    #@2
    return p1
.end method

.method static synthetic access$100(Landroid/media/AudioService;)Landroid/media/AudioService$AudioHandler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Landroid/media/AudioService;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/media/AudioService;->mForceControlStreamLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$10100(Landroid/media/AudioService;)Ljava/util/Stack;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@2
    return-object v0
.end method

.method static synthetic access$102(Landroid/media/AudioService;Landroid/media/AudioService$AudioHandler;)Landroid/media/AudioService$AudioHandler;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    iput-object p1, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@2
    return-object p1
.end method

.method static synthetic access$10202(Landroid/media/AudioService;Landroid/media/IRemoteControlDisplay;)Landroid/media/IRemoteControlDisplay;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    iput-object p1, p0, Landroid/media/AudioService;->mRcDisplay:Landroid/media/IRemoteControlDisplay;

    #@2
    return-object p1
.end method

.method static synthetic access$1100(Landroid/media/AudioService;)Landroid/media/AudioService$ForceControlStreamClient;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/media/AudioService;->mForceControlStreamClient:Landroid/media/AudioService$ForceControlStreamClient;

    #@2
    return-object v0
.end method

.method static synthetic access$1102(Landroid/media/AudioService;Landroid/media/AudioService$ForceControlStreamClient;)Landroid/media/AudioService$ForceControlStreamClient;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    iput-object p1, p0, Landroid/media/AudioService;->mForceControlStreamClient:Landroid/media/AudioService$ForceControlStreamClient;

    #@2
    return-object p1
.end method

.method static synthetic access$1202(Landroid/media/AudioService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    iput p1, p0, Landroid/media/AudioService;->mVolumeControlStream:I

    #@2
    return p1
.end method

.method static synthetic access$1400(Landroid/media/AudioService;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/media/AudioService;->mSetModeDeathHandlers:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$1500(Landroid/media/AudioService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    invoke-direct {p0, p1}, Landroid/media/AudioService;->disconnectBluetoothSco(I)V

    #@3
    return-void
.end method

.method static synthetic access$1602(Landroid/media/AudioService;Landroid/os/Looper;)Landroid/os/Looper;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    iput-object p1, p0, Landroid/media/AudioService;->mSoundPoolLooper:Landroid/os/Looper;

    #@2
    return-object p1
.end method

.method static synthetic access$1700(Landroid/media/AudioService;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/media/AudioService;->mSoundEffectsLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$1800(Landroid/media/AudioService;)Landroid/media/SoundPool;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/media/AudioService;->mSoundPool:Landroid/media/SoundPool;

    #@2
    return-object v0
.end method

.method static synthetic access$1900(Landroid/media/AudioService;)Landroid/media/AudioService$SoundPoolCallback;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/media/AudioService;->mSoundPoolCallBack:Landroid/media/AudioService$SoundPoolCallback;

    #@2
    return-object v0
.end method

.method static synthetic access$1902(Landroid/media/AudioService;Landroid/media/AudioService$SoundPoolCallback;)Landroid/media/AudioService$SoundPoolCallback;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    iput-object p1, p0, Landroid/media/AudioService;->mSoundPoolCallBack:Landroid/media/AudioService$SoundPoolCallback;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Landroid/os/Handler;IIIILjava/lang/Object;I)V
    .registers 7
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"
    .parameter "x6"

    #@0
    .prologue
    .line 114
    invoke-static/range {p0 .. p6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@3
    return-void
.end method

.method static synthetic access$2300(Landroid/media/AudioService;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/media/AudioService;->mScoClients:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$2400(Landroid/media/AudioService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 114
    invoke-direct {p0}, Landroid/media/AudioService;->checkScoAudioState()V

    #@3
    return-void
.end method

.method static synthetic access$2500(Landroid/media/AudioService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    invoke-direct {p0, p1}, Landroid/media/AudioService;->broadcastScoConnectionState(I)V

    #@3
    return-void
.end method

.method static synthetic access$2600(Landroid/media/AudioService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget v0, p0, Landroid/media/AudioService;->mScoAudioState:I

    #@2
    return v0
.end method

.method static synthetic access$2602(Landroid/media/AudioService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    iput p1, p0, Landroid/media/AudioService;->mScoAudioState:I

    #@2
    return p1
.end method

.method static synthetic access$2700(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothHeadset;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/media/AudioService;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    #@2
    return-object v0
.end method

.method static synthetic access$2702(Landroid/media/AudioService;Landroid/bluetooth/BluetoothHeadset;)Landroid/bluetooth/BluetoothHeadset;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    iput-object p1, p0, Landroid/media/AudioService;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    #@2
    return-object p1
.end method

.method static synthetic access$2800(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothDevice;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/media/AudioService;->mBluetoothHeadsetDevice:Landroid/bluetooth/BluetoothDevice;

    #@2
    return-object v0
.end method

.method static synthetic access$2802(Landroid/media/AudioService;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    iput-object p1, p0, Landroid/media/AudioService;->mBluetoothHeadsetDevice:Landroid/bluetooth/BluetoothDevice;

    #@2
    return-object p1
.end method

.method static synthetic access$2900(Landroid/media/AudioService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    invoke-direct {p0}, Landroid/media/AudioService;->getBluetoothHeadset()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$3000(Landroid/media/AudioService;II)I
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 114
    invoke-direct {p0, p1, p2}, Landroid/media/AudioService;->checkSendBecomingNoisyIntent(II)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$3100(Landroid/media/AudioService;Landroid/os/Handler;IIILjava/lang/Object;I)V
    .registers 7
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"
    .parameter "x6"

    #@0
    .prologue
    .line 114
    invoke-direct/range {p0 .. p6}, Landroid/media/AudioService;->queueMsgUnderWakeLock(Landroid/os/Handler;IIILjava/lang/Object;I)V

    #@3
    return-void
.end method

.method static synthetic access$3200(Landroid/media/AudioService;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    invoke-direct {p0, p1}, Landroid/media/AudioService;->makeA2dpDeviceUnavailableNow(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$3300(Landroid/media/AudioService;)[I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/media/AudioService;->MAX_STREAM_VOLUME:[I

    #@2
    return-object v0
.end method

.method static synthetic access$3400(Landroid/media/AudioService;)Ljava/lang/Boolean;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/media/AudioService;->mCameraSoundForced:Ljava/lang/Boolean;

    #@2
    return-object v0
.end method

.method static synthetic access$3500(Landroid/media/AudioService;)[I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@2
    return-object v0
.end method

.method static synthetic access$3600(Landroid/media/AudioService;)Landroid/content/ContentResolver;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/media/AudioService;->mContentResolver:Landroid/content/ContentResolver;

    #@2
    return-object v0
.end method

.method static synthetic access$3700(Landroid/media/AudioService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-boolean v0, p0, Landroid/media/AudioService;->mVoiceCapable:Z

    #@2
    return v0
.end method

.method static synthetic access$3800(Landroid/media/AudioService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-boolean v0, p0, Landroid/media/AudioService;->mIsSoundProfile:Z

    #@2
    return v0
.end method

.method static synthetic access$3900(Landroid/media/AudioService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget v0, p0, Landroid/media/AudioService;->mRingerMode:I

    #@2
    return v0
.end method

.method static synthetic access$400(Landroid/media/AudioService;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/media/AudioService;->mConnectedDevices:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$4000(Landroid/media/AudioService;I)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    invoke-direct {p0, p1}, Landroid/media/AudioService;->getDeviceForStream(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$4100(Landroid/media/AudioService;III)I
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 114
    invoke-direct {p0, p1, p2, p3}, Landroid/media/AudioService;->rescaleIndex(III)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$4200(Landroid/media/AudioService;)[Landroid/media/AudioService$VolumeStreamState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@2
    return-object v0
.end method

.method static synthetic access$4700(Landroid/media/AudioService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget v0, p0, Landroid/media/AudioService;->mMode:I

    #@2
    return v0
.end method

.method static synthetic access$4800(Landroid/media/AudioService;)Ljava/util/Stack;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@2
    return-object v0
.end method

.method static synthetic access$4900(Landroid/media/AudioService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-boolean v0, p0, Landroid/media/AudioService;->mBluetoothA2dpEnabled:Z

    #@2
    return v0
.end method

.method static synthetic access$500(Landroid/media/AudioService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget v0, p0, Landroid/media/AudioService;->mForcedUseForMedia:I

    #@2
    return v0
.end method

.method static synthetic access$5000(Landroid/media/AudioService;)[[I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/media/AudioService;->SOUND_EFFECT_FILES_MAP:[[I

    #@2
    return-object v0
.end method

.method static synthetic access$502(Landroid/media/AudioService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    iput p1, p0, Landroid/media/AudioService;->mForcedUseForMedia:I

    #@2
    return p1
.end method

.method static synthetic access$5100()[Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 114
    sget-object v0, Landroid/media/AudioService;->SOUND_EFFECT_FILES:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$5300(Landroid/media/AudioService;)Landroid/media/AudioSystem$ErrorCallback;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/media/AudioService;->mAudioSystemCallback:Landroid/media/AudioSystem$ErrorCallback;

    #@2
    return-object v0
.end method

.method static synthetic access$5400(Landroid/media/AudioService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget v0, p0, Landroid/media/AudioService;->mForcedUseForComm:I

    #@2
    return v0
.end method

.method static synthetic access$5600(Landroid/media/AudioService;IZ)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 114
    invoke-direct {p0, p1, p2}, Landroid/media/AudioService;->setRingerModeInt(IZ)V

    #@3
    return-void
.end method

.method static synthetic access$5700(Landroid/media/AudioService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 114
    invoke-direct {p0}, Landroid/media/AudioService;->restoreMasterVolume()V

    #@3
    return-void
.end method

.method static synthetic access$5800(Landroid/media/AudioService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-boolean v0, p0, Landroid/media/AudioService;->mMonitorOrientation:Z

    #@2
    return v0
.end method

.method static synthetic access$5900(Landroid/media/AudioService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 114
    invoke-direct {p0}, Landroid/media/AudioService;->setOrientationForAudioSystem()V

    #@3
    return-void
.end method

.method static synthetic access$6000(Landroid/media/AudioService;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/media/AudioService;->mBluetoothA2dpEnabledLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$6100(Landroid/media/AudioService;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/media/AudioService;->mSettingsLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$6200(Landroid/media/AudioService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-boolean v0, p0, Landroid/media/AudioService;->mDockAudioMediaEnabled:Z

    #@2
    return v0
.end method

.method static synthetic access$6300(Landroid/media/AudioService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 114
    invoke-direct {p0}, Landroid/media/AudioService;->onRcDisplayClear()V

    #@3
    return-void
.end method

.method static synthetic access$6400(Landroid/media/AudioService;Landroid/media/AudioService$RemoteControlStackEntry;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 114
    invoke-direct {p0, p1, p2}, Landroid/media/AudioService;->onRcDisplayUpdate(Landroid/media/AudioService$RemoteControlStackEntry;I)V

    #@3
    return-void
.end method

.method static synthetic access$6500(Landroid/media/AudioService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 114
    invoke-direct {p0}, Landroid/media/AudioService;->resetBluetoothSco()V

    #@3
    return-void
.end method

.method static synthetic access$6600(Landroid/media/AudioService;IILjava/lang/String;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 114
    invoke-direct {p0, p1, p2, p3}, Landroid/media/AudioService;->onSetWiredDeviceConnectionState(IILjava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$6700(Landroid/media/AudioService;)Landroid/os/PowerManager$WakeLock;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/media/AudioService;->mMediaEventWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    return-object v0
.end method

.method static synthetic access$6800(Landroid/media/AudioService;Landroid/bluetooth/BluetoothDevice;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 114
    invoke-direct {p0, p1, p2}, Landroid/media/AudioService;->onSetA2dpConnectionState(Landroid/bluetooth/BluetoothDevice;I)V

    #@3
    return-void
.end method

.method static synthetic access$6900(Landroid/media/AudioService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 114
    invoke-direct {p0}, Landroid/media/AudioService;->onReevaluateRemote()V

    #@3
    return-void
.end method

.method static synthetic access$7000(Landroid/media/AudioService;III)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 114
    invoke-direct {p0, p1, p2, p3}, Landroid/media/AudioService;->onNewPlaybackInfoForRcc(III)V

    #@3
    return-void
.end method

.method static synthetic access$7100(Landroid/media/AudioService;ILandroid/media/IRemoteVolumeObserver;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 114
    invoke-direct {p0, p1, p2}, Landroid/media/AudioService;->onRegisterVolumeObserverForRcc(ILandroid/media/IRemoteVolumeObserver;)V

    #@3
    return-void
.end method

.method static synthetic access$7200(Landroid/media/AudioService;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 114
    invoke-direct {p0, p1, p2}, Landroid/media/AudioService;->onSetRsxConnectionState(II)V

    #@3
    return-void
.end method

.method static synthetic access$7300(Landroid/media/AudioService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 114
    invoke-direct {p0}, Landroid/media/AudioService;->onCheckMusicActive()V

    #@3
    return-void
.end method

.method static synthetic access$7400(Landroid/media/AudioService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 114
    invoke-direct {p0}, Landroid/media/AudioService;->onSendBecomingNoisyIntent()V

    #@3
    return-void
.end method

.method static synthetic access$7500(Landroid/media/AudioService;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    invoke-direct {p0, p1}, Landroid/media/AudioService;->onConfigureSafeVolume(Z)V

    #@3
    return-void
.end method

.method static synthetic access$7600(Landroid/media/AudioService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    invoke-direct {p0, p1}, Landroid/media/AudioService;->onShowVolumeInfo(I)V

    #@3
    return-void
.end method

.method static synthetic access$7700(Landroid/media/AudioService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-boolean v0, p0, Landroid/media/AudioService;->mIsSoundException:Z

    #@2
    return v0
.end method

.method static synthetic access$7800(Landroid/media/AudioService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget v0, p0, Landroid/media/AudioService;->mRingerModeAffectedStreams:I

    #@2
    return v0
.end method

.method static synthetic access$7802(Landroid/media/AudioService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    iput p1, p0, Landroid/media/AudioService;->mRingerModeAffectedStreams:I

    #@2
    return p1
.end method

.method static synthetic access$7900(Landroid/media/AudioService;Landroid/content/ContentResolver;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    invoke-direct {p0, p1}, Landroid/media/AudioService;->readDockAudioSettings(Landroid/content/ContentResolver;)V

    #@3
    return-void
.end method

.method static synthetic access$8000(Landroid/media/AudioService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget v0, p0, Landroid/media/AudioService;->mDockState:I

    #@2
    return v0
.end method

.method static synthetic access$8002(Landroid/media/AudioService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    iput p1, p0, Landroid/media/AudioService;->mDockState:I

    #@2
    return p1
.end method

.method static synthetic access$8100(Landroid/media/AudioService;ZILjava/lang/String;)Z
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 114
    invoke-direct {p0, p1, p2, p3}, Landroid/media/AudioService;->handleDeviceConnection(ZILjava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$8200(Landroid/media/AudioService;Landroid/content/Intent;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    invoke-direct {p0, p1}, Landroid/media/AudioService;->sendStickyBroadcastToAll(Landroid/content/Intent;)V

    #@3
    return-void
.end method

.method static synthetic access$8302(Landroid/media/AudioService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    iput-boolean p1, p0, Landroid/media/AudioService;->mBootCompleted:Z

    #@2
    return p1
.end method

.method static synthetic access$8402(Landroid/media/AudioService;Landroid/app/KeyguardManager;)Landroid/app/KeyguardManager;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    iput-object p1, p0, Landroid/media/AudioService;->mKeyguardManager:Landroid/app/KeyguardManager;

    #@2
    return-object p1
.end method

.method static synthetic access$8500(Landroid/media/AudioService;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$8602(Landroid/media/AudioService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    iput p1, p0, Landroid/media/AudioService;->mScoConnectionState:I

    #@2
    return p1
.end method

.method static synthetic access$8700(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothProfile$ServiceListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/media/AudioService;->mBluetoothProfileServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    #@2
    return-object v0
.end method

.method static synthetic access$8800(Landroid/media/AudioService;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    invoke-direct {p0, p1}, Landroid/media/AudioService;->removeMediaButtonReceiverForPackage(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$8900(Landroid/media/AudioService;Landroid/content/Context;Z)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 114
    invoke-direct {p0, p1, p2}, Landroid/media/AudioService;->enableVoiceActivation(Landroid/content/Context;Z)V

    #@3
    return-void
.end method

.method static synthetic access$9000(Landroid/media/AudioService;Landroid/content/Context;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    invoke-direct {p0, p1}, Landroid/media/AudioService;->handleConfigurationChanged(Landroid/content/Context;)V

    #@3
    return-void
.end method

.method static synthetic access$9100(Landroid/media/AudioService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 114
    invoke-direct {p0}, Landroid/media/AudioService;->discardAudioFocusOwner()V

    #@3
    return-void
.end method

.method static synthetic access$9200(Landroid/media/AudioService;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    invoke-direct {p0, p1}, Landroid/media/AudioService;->readAudioSettings(Z)V

    #@3
    return-void
.end method

.method static synthetic access$9300(Landroid/media/AudioService;Landroid/content/Context;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    invoke-direct {p0, p1}, Landroid/media/AudioService;->refreshVolumePanel(Landroid/content/Context;)V

    #@3
    return-void
.end method

.method static synthetic access$9400()Ljava/lang/Object;
    .registers 1

    #@0
    .prologue
    .line 114
    sget-object v0, Landroid/media/AudioService;->mRingingLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$9502(Landroid/media/AudioService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    iput-boolean p1, p0, Landroid/media/AudioService;->mIsRinging:Z

    #@2
    return p1
.end method

.method static synthetic access$9600()Ljava/lang/Object;
    .registers 1

    #@0
    .prologue
    .line 114
    sget-object v0, Landroid/media/AudioService;->mAudioFocusLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$9700(Landroid/media/AudioService;Landroid/os/IBinder;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    invoke-direct {p0, p1}, Landroid/media/AudioService;->removeFocusStackEntryForClient(Landroid/os/IBinder;)V

    #@3
    return-void
.end method

.method static synthetic access$9800(Landroid/media/AudioService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 114
    invoke-direct {p0}, Landroid/media/AudioService;->postReevaluateRemote()V

    #@3
    return-void
.end method

.method static synthetic access$9904()I
    .registers 1

    #@0
    .prologue
    .line 114
    sget v0, Landroid/media/AudioService;->sLastRccId:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    sput v0, Landroid/media/AudioService;->sLastRccId:I

    #@6
    return v0
.end method

.method private adjustRemoteVolume(III)V
    .registers 9
    .parameter "streamType"
    .parameter "direction"
    .parameter "flags"

    #@0
    .prologue
    .line 6527
    const/4 v0, -0x1

    #@1
    .line 6528
    .local v0, rccId:I
    const/4 v1, 0x0

    #@2
    .line 6529
    .local v1, volFixed:Z
    iget-object v3, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@4
    monitor-enter v3

    #@5
    .line 6530
    :try_start_5
    iget-boolean v2, p0, Landroid/media/AudioService;->mMainRemoteIsActive:Z

    #@7
    if-nez v2, :cond_12

    #@9
    .line 6531
    const-string v2, "AudioService"

    #@b
    const-string v4, "adjustRemoteVolume didn\'t find an active client"

    #@d
    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 6532
    monitor-exit v3

    #@11
    .line 6547
    :goto_11
    return-void

    #@12
    .line 6534
    :cond_12
    iget-object v2, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@14
    iget v0, v2, Landroid/media/AudioService$RemotePlaybackState;->mRccId:I

    #@16
    .line 6535
    iget-object v2, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@18
    iget v2, v2, Landroid/media/AudioService$RemotePlaybackState;->mVolumeHandling:I

    #@1a
    if-nez v2, :cond_29

    #@1c
    const/4 v1, 0x1

    #@1d
    .line 6537
    :goto_1d
    monitor-exit v3
    :try_end_1e
    .catchall {:try_start_5 .. :try_end_1e} :catchall_2b

    #@1e
    .line 6541
    if-nez v1, :cond_23

    #@20
    .line 6542
    invoke-direct {p0, v0, p2}, Landroid/media/AudioService;->sendVolumeUpdateToRemote(II)V

    #@23
    .line 6546
    :cond_23
    iget-object v2, p0, Landroid/media/AudioService;->mVolumePanel:Landroid/view/VolumePanel;

    #@25
    invoke-virtual {v2, p1, p3}, Landroid/view/VolumePanel;->postRemoteVolumeChanged(II)V

    #@28
    goto :goto_11

    #@29
    .line 6535
    :cond_29
    const/4 v1, 0x0

    #@2a
    goto :goto_1d

    #@2b
    .line 6537
    :catchall_2b
    move-exception v2

    #@2c
    :try_start_2c
    monitor-exit v3
    :try_end_2d
    .catchall {:try_start_2c .. :try_end_2d} :catchall_2b

    #@2d
    throw v2
.end method

.method private broadcastMasterMuteStatus(Z)V
    .registers 4
    .parameter "muted"

    #@0
    .prologue
    .line 1469
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.media.MASTER_MUTE_CHANGED_ACTION"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 1470
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "android.media.EXTRA_MASTER_VOLUME_MUTED"

    #@9
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@c
    .line 1471
    const/high16 v1, 0x2800

    #@e
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@11
    .line 1473
    invoke-direct {p0, v0}, Landroid/media/AudioService;->sendStickyBroadcastToAll(Landroid/content/Intent;)V

    #@14
    .line 1474
    return-void
.end method

.method private broadcastRingerMode(I)V
    .registers 4
    .parameter "ringerMode"

    #@0
    .prologue
    .line 3187
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.media.RINGER_MODE_CHANGED"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 3188
    .local v0, broadcast:Landroid/content/Intent;
    const-string v1, "android.media.EXTRA_RINGER_MODE"

    #@9
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@c
    .line 3189
    const/high16 v1, 0x2800

    #@e
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@11
    .line 3191
    invoke-direct {p0, v0}, Landroid/media/AudioService;->sendStickyBroadcastToAll(Landroid/content/Intent;)V

    #@14
    .line 3192
    return-void
.end method

.method private broadcastScoConnectionState(I)V
    .registers 5
    .parameter "state"

    #@0
    .prologue
    .line 2755
    iget v1, p0, Landroid/media/AudioService;->mScoConnectionState:I

    #@2
    if-eq p1, v1, :cond_1c

    #@4
    .line 2756
    new-instance v0, Landroid/content/Intent;

    #@6
    const-string v1, "android.media.ACTION_SCO_AUDIO_STATE_UPDATED"

    #@8
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@b
    .line 2757
    .local v0, newIntent:Landroid/content/Intent;
    const-string v1, "android.media.extra.SCO_AUDIO_STATE"

    #@d
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@10
    .line 2758
    const-string v1, "android.media.extra.SCO_AUDIO_PREVIOUS_STATE"

    #@12
    iget v2, p0, Landroid/media/AudioService;->mScoConnectionState:I

    #@14
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@17
    .line 2760
    invoke-direct {p0, v0}, Landroid/media/AudioService;->sendStickyBroadcastToAll(Landroid/content/Intent;)V

    #@1a
    .line 2761
    iput p1, p0, Landroid/media/AudioService;->mScoConnectionState:I

    #@1c
    .line 2763
    .end local v0           #newIntent:Landroid/content/Intent;
    :cond_1c
    return-void
.end method

.method private broadcastVibrateSetting(I)V
    .registers 5
    .parameter "vibrateType"

    #@0
    .prologue
    .line 3196
    invoke-static {}, Landroid/app/ActivityManagerNative;->isSystemReady()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_1e

    #@6
    .line 3197
    new-instance v0, Landroid/content/Intent;

    #@8
    const-string v1, "android.media.VIBRATE_SETTING_CHANGED"

    #@a
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@d
    .line 3198
    .local v0, broadcast:Landroid/content/Intent;
    const-string v1, "android.media.EXTRA_VIBRATE_TYPE"

    #@f
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@12
    .line 3199
    const-string v1, "android.media.EXTRA_VIBRATE_SETTING"

    #@14
    invoke-virtual {p0, p1}, Landroid/media/AudioService;->getVibrateSetting(I)I

    #@17
    move-result v2

    #@18
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@1b
    .line 3200
    invoke-direct {p0, v0}, Landroid/media/AudioService;->sendBroadcastToAll(Landroid/content/Intent;)V

    #@1e
    .line 3202
    .end local v0           #broadcast:Landroid/content/Intent;
    :cond_1e
    return-void
.end method

.method private canReassignAudioFocus()Z
    .registers 3

    #@0
    .prologue
    .line 5035
    iget-object v0, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@2
    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_1c

    #@8
    const-string v1, "AudioFocus_For_Phone_Ring_And_Calls"

    #@a
    iget-object v0, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@c
    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/media/AudioService$FocusStackEntry;

    #@12
    iget-object v0, v0, Landroid/media/AudioService$FocusStackEntry;->mClientId:Ljava/lang/String;

    #@14
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_1c

    #@1a
    .line 5036
    const/4 v0, 0x0

    #@1b
    .line 5038
    :goto_1b
    return v0

    #@1c
    :cond_1c
    const/4 v0, 0x1

    #@1d
    goto :goto_1b
.end method

.method private canReassignAudioFocusFromQchat(ILjava/lang/String;)Z
    .registers 5
    .parameter "streamType"
    .parameter "clientId"

    #@0
    .prologue
    .line 5052
    iget-object v0, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@2
    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_27

    #@8
    iget-object v0, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@a
    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Landroid/media/AudioService$FocusStackEntry;

    #@10
    iget-object v0, v0, Landroid/media/AudioService$FocusStackEntry;->mClientId:Ljava/lang/String;

    #@12
    const-string v1, "QCHAT"

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_27

    #@1a
    const-string v0, "QCHAT"

    #@1c
    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@1f
    move-result v0

    #@20
    if-nez v0, :cond_25

    #@22
    const/4 v0, 0x3

    #@23
    if-ne p1, v0, :cond_27

    #@25
    .line 5055
    :cond_25
    const/4 v0, 0x0

    #@26
    .line 5057
    :goto_26
    return v0

    #@27
    :cond_27
    const/4 v0, 0x1

    #@28
    goto :goto_26
.end method

.method private cancelA2dpDeviceTimeout()V
    .registers 3

    #@0
    .prologue
    .line 4275
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@2
    const/4 v1, 0x7

    #@3
    invoke-virtual {v0, v1}, Landroid/media/AudioService$AudioHandler;->removeMessages(I)V

    #@6
    .line 4276
    return-void
.end method

.method private checkAllAliasStreamVolumes()V
    .registers 6

    #@0
    .prologue
    .line 721
    invoke-static {}, Landroid/media/AudioSystem;->getNumStreamTypes()I

    #@3
    move-result v0

    #@4
    .line 722
    .local v0, numStreamTypes:I
    const/4 v1, 0x0

    #@5
    .local v1, streamType:I
    :goto_5
    if-ge v1, v0, :cond_41

    #@7
    .line 723
    iget-object v2, p0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@9
    aget v2, v2, v1

    #@b
    if-eq v1, v2, :cond_2d

    #@d
    .line 724
    iget-object v2, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@f
    aget-object v2, v2, v1

    #@11
    iget-object v3, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@13
    iget-object v4, p0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@15
    aget v4, v4, v1

    #@17
    aget-object v3, v3, v4

    #@19
    const/4 v4, 0x0

    #@1a
    invoke-virtual {v2, v3, v4}, Landroid/media/AudioService$VolumeStreamState;->setAllIndexes(Landroid/media/AudioService$VolumeStreamState;Z)V

    #@1d
    .line 727
    iget-object v2, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@1f
    aget-object v2, v2, v1

    #@21
    iget-object v3, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@23
    iget-object v4, p0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@25
    aget v4, v4, v1

    #@27
    aget-object v3, v3, v4

    #@29
    const/4 v4, 0x1

    #@2a
    invoke-virtual {v2, v3, v4}, Landroid/media/AudioService$VolumeStreamState;->setAllIndexes(Landroid/media/AudioService$VolumeStreamState;Z)V

    #@2d
    .line 732
    :cond_2d
    iget-object v2, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@2f
    aget-object v2, v2, v1

    #@31
    invoke-static {v2}, Landroid/media/AudioService$VolumeStreamState;->access$700(Landroid/media/AudioService$VolumeStreamState;)I

    #@34
    move-result v2

    #@35
    if-nez v2, :cond_3e

    #@37
    .line 733
    iget-object v2, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@39
    aget-object v2, v2, v1

    #@3b
    invoke-virtual {v2}, Landroid/media/AudioService$VolumeStreamState;->applyAllVolumes()V

    #@3e
    .line 722
    :cond_3e
    add-int/lit8 v1, v1, 0x1

    #@40
    goto :goto_5

    #@41
    .line 736
    :cond_41
    return-void
.end method

.method private checkDisplaySafeMediaVolume(IIII)V
    .registers 8
    .parameter "streamType"
    .parameter "index"
    .parameter "device"
    .parameter "direction"

    #@0
    .prologue
    const/4 v2, 0x3

    #@1
    .line 6899
    iget-object v1, p0, Landroid/media/AudioService;->mSafeMediaVolumeState:Ljava/lang/Integer;

    #@3
    monitor-enter v1

    #@4
    .line 6900
    :try_start_4
    iget-object v0, p0, Landroid/media/AudioService;->mSafeMediaVolumeState:Ljava/lang/Integer;

    #@6
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@9
    move-result v0

    #@a
    if-ne v0, v2, :cond_21

    #@c
    const/4 v0, 0x1

    #@d
    if-eq p4, v0, :cond_12

    #@f
    const/4 v0, -0x1

    #@10
    if-ne p4, v0, :cond_21

    #@12
    :cond_12
    iget-object v0, p0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@14
    aget v0, v0, p1

    #@16
    if-ne v0, v2, :cond_21

    #@18
    and-int/lit8 v0, p3, 0xc

    #@1a
    if-eqz v0, :cond_21

    #@1c
    .line 6904
    iget-object v0, p0, Landroid/media/AudioService;->mVolumePanel:Landroid/view/VolumePanel;

    #@1e
    invoke-virtual {v0, p4}, Landroid/view/VolumePanel;->postDismissSafeVolumeWarning(I)V

    #@21
    .line 6906
    :cond_21
    monitor-exit v1

    #@22
    .line 6907
    return-void

    #@23
    .line 6906
    :catchall_23
    move-exception v0

    #@24
    monitor-exit v1
    :try_end_25
    .catchall {:try_start_4 .. :try_end_25} :catchall_23

    #@25
    throw v0
.end method

.method private checkForRingerModeChange(III)Z
    .registers 9
    .parameter "oldIndex"
    .parameter "direction"
    .parameter "step"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, -0x1

    #@2
    .line 2988
    const/4 v0, 0x1

    #@3
    .line 2989
    .local v0, adjustVolumeIndex:Z
    invoke-virtual {p0}, Landroid/media/AudioService;->getRingerMode()I

    #@6
    move-result v1

    #@7
    .line 2991
    .local v1, ringerMode:I
    packed-switch v1, :pswitch_data_62

    #@a
    .line 3037
    const-string v2, "AudioService"

    #@c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "checkForRingerModeChange() wrong ringer mode: "

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 3041
    :cond_22
    :goto_22
    invoke-virtual {p0, v1}, Landroid/media/AudioService;->setRingerMode(I)V

    #@25
    .line 3043
    iput p2, p0, Landroid/media/AudioService;->mPrevVolDirection:I

    #@27
    .line 3045
    return v0

    #@28
    .line 2993
    :pswitch_28
    if-ne p2, v3, :cond_22

    #@2a
    .line 2994
    iget-boolean v2, p0, Landroid/media/AudioService;->mHasVibrator:Z

    #@2c
    if-eqz v2, :cond_36

    #@2e
    .line 3000
    if-gt p3, p1, :cond_22

    #@30
    mul-int/lit8 v2, p3, 0x2

    #@32
    if-ge p1, v2, :cond_22

    #@34
    .line 3001
    const/4 v1, 0x1

    #@35
    goto :goto_22

    #@36
    .line 3005
    :cond_36
    if-ge p1, p3, :cond_22

    #@38
    iget v2, p0, Landroid/media/AudioService;->mPrevVolDirection:I

    #@3a
    if-eq v2, v3, :cond_22

    #@3c
    .line 3006
    const/4 v1, 0x0

    #@3d
    goto :goto_22

    #@3e
    .line 3012
    :pswitch_3e
    iget-boolean v2, p0, Landroid/media/AudioService;->mHasVibrator:Z

    #@40
    if-nez v2, :cond_4a

    #@42
    .line 3013
    const-string v2, "AudioService"

    #@44
    const-string v3, "checkForRingerModeChange() current ringer mode is vibratebut no vibrator is present"

    #@46
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    goto :goto_22

    #@4a
    .line 3017
    :cond_4a
    if-ne p2, v3, :cond_53

    #@4c
    .line 3018
    iget v2, p0, Landroid/media/AudioService;->mPrevVolDirection:I

    #@4e
    if-eq v2, v3, :cond_51

    #@50
    .line 3019
    const/4 v1, 0x0

    #@51
    .line 3024
    :cond_51
    :goto_51
    const/4 v0, 0x0

    #@52
    .line 3025
    goto :goto_22

    #@53
    .line 3021
    :cond_53
    if-ne p2, v4, :cond_51

    #@55
    .line 3022
    const/4 v1, 0x2

    #@56
    goto :goto_51

    #@57
    .line 3027
    :pswitch_57
    if-ne p2, v4, :cond_5e

    #@59
    .line 3028
    iget-boolean v2, p0, Landroid/media/AudioService;->mHasVibrator:Z

    #@5b
    if-eqz v2, :cond_60

    #@5d
    .line 3029
    const/4 v1, 0x1

    #@5e
    .line 3034
    :cond_5e
    :goto_5e
    const/4 v0, 0x0

    #@5f
    .line 3035
    goto :goto_22

    #@60
    .line 3031
    :cond_60
    const/4 v1, 0x2

    #@61
    goto :goto_5e

    #@62
    .line 2991
    :pswitch_data_62
    .packed-switch 0x0
        :pswitch_57
        :pswitch_3e
        :pswitch_28
    .end packed-switch
.end method

.method private checkForSoundProfileChange(III)Z
    .registers 9
    .parameter "oldIndex"
    .parameter "direction"
    .parameter "step"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, -0x1

    #@2
    .line 7149
    const/4 v0, 0x1

    #@3
    .line 7150
    .local v0, adjustVolumeIndex:Z
    invoke-virtual {p0}, Landroid/media/AudioService;->getRingerMode()I

    #@6
    move-result v1

    #@7
    .line 7152
    .local v1, ringerMode:I
    packed-switch v1, :pswitch_data_7a

    #@a
    .line 7205
    const-string v2, "AudioService"

    #@c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "checkForRingerModeChange() wrong ringer mode: "

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 7209
    :cond_22
    :goto_22
    invoke-virtual {p0, v1}, Landroid/media/AudioService;->setRingerMode(I)V

    #@25
    .line 7211
    iput p2, p0, Landroid/media/AudioService;->mPrevVolDirection:I

    #@27
    .line 7213
    return v0

    #@28
    .line 7154
    :pswitch_28
    if-ne p2, v3, :cond_22

    #@2a
    .line 7155
    iget-boolean v2, p0, Landroid/media/AudioService;->mHasVibrator:Z

    #@2c
    if-eqz v2, :cond_35

    #@2e
    .line 7162
    mul-int/lit8 v2, p3, 0x2

    #@30
    if-ge p1, v2, :cond_22

    #@32
    .line 7163
    const/4 v1, 0x1

    #@33
    .line 7164
    const/4 v0, 0x0

    #@34
    goto :goto_22

    #@35
    .line 7168
    :cond_35
    if-ge p1, p3, :cond_22

    #@37
    iget v2, p0, Landroid/media/AudioService;->mPrevVolDirection:I

    #@39
    if-eq v2, v3, :cond_22

    #@3b
    .line 7169
    const/4 v1, 0x0

    #@3c
    goto :goto_22

    #@3d
    .line 7175
    :pswitch_3d
    iget-boolean v2, p0, Landroid/media/AudioService;->mHasVibrator:Z

    #@3f
    if-nez v2, :cond_49

    #@41
    .line 7176
    const-string v2, "AudioService"

    #@43
    const-string v3, "checkForRingerModeChange() current ringer mode is vibratebut no vibrator is present"

    #@45
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    goto :goto_22

    #@49
    .line 7180
    :cond_49
    if-ne p2, v3, :cond_52

    #@4b
    .line 7181
    iget v2, p0, Landroid/media/AudioService;->mPrevVolDirection:I

    #@4d
    if-eq v2, v3, :cond_50

    #@4f
    .line 7182
    const/4 v1, 0x0

    #@50
    .line 7189
    :cond_50
    :goto_50
    const/4 v0, 0x0

    #@51
    .line 7190
    goto :goto_22

    #@52
    .line 7184
    :cond_52
    if-ne p2, v4, :cond_50

    #@54
    .line 7185
    iget-object v2, p0, Landroid/media/AudioService;->mVolumePanel:Landroid/view/VolumePanel;

    #@56
    invoke-virtual {v2}, Landroid/view/VolumePanel;->isShowing()Z

    #@59
    move-result v2

    #@5a
    if-eqz v2, :cond_50

    #@5c
    iget v2, p0, Landroid/media/AudioService;->mPrevVolDirection:I

    #@5e
    if-eq v2, v4, :cond_50

    #@60
    .line 7186
    const/4 v1, 0x2

    #@61
    goto :goto_50

    #@62
    .line 7192
    :pswitch_62
    if-ne p2, v4, :cond_76

    #@64
    .line 7193
    iget-boolean v2, p0, Landroid/media/AudioService;->mHasVibrator:Z

    #@66
    if-eqz v2, :cond_78

    #@68
    .line 7194
    iget-object v2, p0, Landroid/media/AudioService;->mVolumePanel:Landroid/view/VolumePanel;

    #@6a
    invoke-virtual {v2}, Landroid/view/VolumePanel;->isShowing()Z

    #@6d
    move-result v2

    #@6e
    if-eqz v2, :cond_76

    #@70
    iget v2, p0, Landroid/media/AudioService;->mPrevVolDirection:I

    #@72
    if-eq v2, v4, :cond_76

    #@74
    .line 7195
    const/4 v1, 0x1

    #@75
    .line 7196
    const/4 p2, 0x0

    #@76
    .line 7202
    :cond_76
    :goto_76
    const/4 v0, 0x0

    #@77
    .line 7203
    goto :goto_22

    #@78
    .line 7199
    :cond_78
    const/4 v1, 0x2

    #@79
    goto :goto_76

    #@7a
    .line 7152
    :pswitch_data_7a
    .packed-switch 0x0
        :pswitch_62
        :pswitch_3d
        :pswitch_28
    .end packed-switch
.end method

.method private checkSafeMediaVolume(III)Z
    .registers 7
    .parameter "streamType"
    .parameter "index"
    .parameter "device"

    #@0
    .prologue
    const/4 v2, 0x3

    #@1
    .line 6885
    iget-object v1, p0, Landroid/media/AudioService;->mSafeMediaVolumeState:Ljava/lang/Integer;

    #@3
    monitor-enter v1

    #@4
    .line 6886
    :try_start_4
    iget-object v0, p0, Landroid/media/AudioService;->mSafeMediaVolumeState:Ljava/lang/Integer;

    #@6
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@9
    move-result v0

    #@a
    if-ne v0, v2, :cond_22

    #@c
    iget-object v0, p0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@e
    aget v0, v0, p1

    #@10
    if-ne v0, v2, :cond_22

    #@12
    and-int/lit8 v0, p3, 0xc

    #@14
    if-eqz v0, :cond_22

    #@16
    iget v0, p0, Landroid/media/AudioService;->mSafeMediaVolumeIndex:I

    #@18
    if-le p2, v0, :cond_22

    #@1a
    .line 6890
    iget-object v0, p0, Landroid/media/AudioService;->mVolumePanel:Landroid/view/VolumePanel;

    #@1c
    invoke-virtual {v0}, Landroid/view/VolumePanel;->postDisplaySafeVolumeWarning()V

    #@1f
    .line 6891
    const/4 v0, 0x0

    #@20
    monitor-exit v1

    #@21
    .line 6893
    :goto_21
    return v0

    #@22
    :cond_22
    const/4 v0, 0x1

    #@23
    monitor-exit v1

    #@24
    goto :goto_21

    #@25
    .line 6894
    :catchall_25
    move-exception v0

    #@26
    monitor-exit v1
    :try_end_27
    .catchall {:try_start_4 .. :try_end_27} :catchall_25

    #@27
    throw v0
.end method

.method private checkScoAudioState()V
    .registers 3

    #@0
    .prologue
    .line 2663
    iget-object v0, p0, Landroid/media/AudioService;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    #@2
    if-eqz v0, :cond_1b

    #@4
    iget-object v0, p0, Landroid/media/AudioService;->mBluetoothHeadsetDevice:Landroid/bluetooth/BluetoothDevice;

    #@6
    if-eqz v0, :cond_1b

    #@8
    iget v0, p0, Landroid/media/AudioService;->mScoAudioState:I

    #@a
    if-nez v0, :cond_1b

    #@c
    iget-object v0, p0, Landroid/media/AudioService;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    #@e
    iget-object v1, p0, Landroid/media/AudioService;->mBluetoothHeadsetDevice:Landroid/bluetooth/BluetoothDevice;

    #@10
    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothHeadset;->getAudioState(Landroid/bluetooth/BluetoothDevice;)I

    #@13
    move-result v0

    #@14
    const/16 v1, 0xa

    #@16
    if-eq v0, v1, :cond_1b

    #@18
    .line 2667
    const/4 v0, 0x2

    #@19
    iput v0, p0, Landroid/media/AudioService;->mScoAudioState:I

    #@1b
    .line 2669
    :cond_1b
    return-void
.end method

.method private checkSendBecomingNoisyIntent(II)I
    .registers 14
    .parameter "device"
    .parameter "state"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 4374
    const/4 v7, 0x0

    #@2
    .line 4375
    .local v7, delay:I
    if-nez p2, :cond_3a

    #@4
    iget v0, p0, Landroid/media/AudioService;->mBecomingNoisyIntentDevices:I

    #@6
    and-int/2addr v0, p1

    #@7
    if-eqz v0, :cond_3a

    #@9
    .line 4376
    const/4 v9, 0x0

    #@a
    .line 4377
    .local v9, devices:I
    iget-object v0, p0, Landroid/media/AudioService;->mConnectedDevices:Ljava/util/HashMap;

    #@c
    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@f
    move-result-object v0

    #@10
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@13
    move-result-object v10

    #@14
    .local v10, i$:Ljava/util/Iterator;
    :cond_14
    :goto_14
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_2b

    #@1a
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1d
    move-result-object v0

    #@1e
    check-cast v0, Ljava/lang/Integer;

    #@20
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@23
    move-result v8

    #@24
    .line 4378
    .local v8, dev:I
    iget v0, p0, Landroid/media/AudioService;->mBecomingNoisyIntentDevices:I

    #@26
    and-int/2addr v0, v8

    #@27
    if-eqz v0, :cond_14

    #@29
    .line 4379
    or-int/2addr v9, v8

    #@2a
    goto :goto_14

    #@2b
    .line 4382
    .end local v8           #dev:I
    :cond_2b
    if-ne v9, p1, :cond_3a

    #@2d
    .line 4383
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@2f
    const/16 v1, 0x19

    #@31
    const/4 v5, 0x0

    #@32
    move v3, v2

    #@33
    move v4, v2

    #@34
    move v6, v2

    #@35
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@38
    .line 4390
    const/16 v7, 0x3e8

    #@3a
    .line 4394
    .end local v9           #devices:I
    .end local v10           #i$:Ljava/util/Iterator;
    :cond_3a
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@3c
    const/16 v1, 0x16

    #@3e
    invoke-virtual {v0, v1}, Landroid/media/AudioService$AudioHandler;->hasMessages(I)Z

    #@41
    move-result v0

    #@42
    if-nez v0, :cond_4e

    #@44
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@46
    const/16 v1, 0x15

    #@48
    invoke-virtual {v0, v1}, Landroid/media/AudioService$AudioHandler;->hasMessages(I)Z

    #@4b
    move-result v0

    #@4c
    if-eqz v0, :cond_50

    #@4e
    .line 4396
    :cond_4e
    const/16 v7, 0x3e8

    #@50
    .line 4398
    :cond_50
    return v7
.end method

.method private checkUpdateRemoteControlDisplay_syncAfRcs(I)V
    .registers 9
    .parameter "infoChangedFlags"

    #@0
    .prologue
    .line 6028
    iget-object v4, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@2
    invoke-virtual {v4}, Ljava/util/Stack;->isEmpty()Z

    #@5
    move-result v4

    #@6
    if-nez v4, :cond_10

    #@8
    iget-object v4, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@a
    invoke-virtual {v4}, Ljava/util/Stack;->isEmpty()Z

    #@d
    move-result v4

    #@e
    if-eqz v4, :cond_14

    #@10
    .line 6029
    :cond_10
    invoke-direct {p0}, Landroid/media/AudioService;->clearRemoteControlDisplay_syncAfRcs()V

    #@13
    .line 6078
    :goto_13
    return-void

    #@14
    .line 6041
    :cond_14
    const/4 v0, 0x0

    #@15
    .line 6043
    .local v0, af:Landroid/media/AudioService$FocusStackEntry;
    :try_start_15
    iget-object v4, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@17
    invoke-virtual {v4}, Ljava/util/Stack;->size()I

    #@1a
    move-result v4

    #@1b
    add-int/lit8 v3, v4, -0x1

    #@1d
    .local v3, index:I
    :goto_1d
    if-ltz v3, :cond_32

    #@1f
    .line 6044
    iget-object v4, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@21
    invoke-virtual {v4, v3}, Ljava/util/Stack;->elementAt(I)Ljava/lang/Object;

    #@24
    move-result-object v2

    #@25
    check-cast v2, Landroid/media/AudioService$FocusStackEntry;

    #@27
    .line 6045
    .local v2, fse:Landroid/media/AudioService$FocusStackEntry;
    iget v4, v2, Landroid/media/AudioService$FocusStackEntry;->mStreamType:I

    #@29
    const/4 v5, 0x3

    #@2a
    if-eq v4, v5, :cond_31

    #@2c
    iget v4, v2, Landroid/media/AudioService$FocusStackEntry;->mFocusChangeType:I
    :try_end_2e
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_15 .. :try_end_2e} :catch_3b

    #@2e
    const/4 v5, 0x1

    #@2f
    if-ne v4, v5, :cond_38

    #@31
    .line 6047
    :cond_31
    move-object v0, v2

    #@32
    .line 6055
    .end local v2           #fse:Landroid/media/AudioService$FocusStackEntry;
    .end local v3           #index:I
    :cond_32
    :goto_32
    if-nez v0, :cond_56

    #@34
    .line 6056
    invoke-direct {p0}, Landroid/media/AudioService;->clearRemoteControlDisplay_syncAfRcs()V

    #@37
    goto :goto_13

    #@38
    .line 6043
    .restart local v2       #fse:Landroid/media/AudioService$FocusStackEntry;
    .restart local v3       #index:I
    :cond_38
    add-int/lit8 v3, v3, -0x1

    #@3a
    goto :goto_1d

    #@3b
    .line 6051
    .end local v2           #fse:Landroid/media/AudioService$FocusStackEntry;
    .end local v3           #index:I
    :catch_3b
    move-exception v1

    #@3c
    .line 6052
    .local v1, e:Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string v4, "AudioService"

    #@3e
    new-instance v5, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v6, "Wrong index accessing audio focus stack when updating RCD: "

    #@45
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v5

    #@49
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v5

    #@4d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v5

    #@51
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    .line 6053
    const/4 v0, 0x0

    #@55
    goto :goto_32

    #@56
    .line 6061
    .end local v1           #e:Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_56
    iget-object v4, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@58
    invoke-virtual {v4}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    #@5b
    move-result-object v4

    #@5c
    check-cast v4, Landroid/media/AudioService$RemoteControlStackEntry;

    #@5e
    iget-object v4, v4, Landroid/media/AudioService$RemoteControlStackEntry;->mCallingPackageName:Ljava/lang/String;

    #@60
    if-eqz v4, :cond_7c

    #@62
    iget-object v4, v0, Landroid/media/AudioService$FocusStackEntry;->mPackageName:Ljava/lang/String;

    #@64
    if-eqz v4, :cond_7c

    #@66
    iget-object v4, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@68
    invoke-virtual {v4}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    #@6b
    move-result-object v4

    #@6c
    check-cast v4, Landroid/media/AudioService$RemoteControlStackEntry;

    #@6e
    iget-object v4, v4, Landroid/media/AudioService$RemoteControlStackEntry;->mCallingPackageName:Ljava/lang/String;

    #@70
    iget-object v5, v0, Landroid/media/AudioService$FocusStackEntry;->mPackageName:Ljava/lang/String;

    #@72
    invoke-virtual {v4, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@75
    move-result v4

    #@76
    if-eqz v4, :cond_7c

    #@78
    .line 6065
    invoke-direct {p0}, Landroid/media/AudioService;->clearRemoteControlDisplay_syncAfRcs()V

    #@7b
    goto :goto_13

    #@7c
    .line 6070
    :cond_7c
    iget-object v4, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@7e
    invoke-virtual {v4}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    #@81
    move-result-object v4

    #@82
    check-cast v4, Landroid/media/AudioService$RemoteControlStackEntry;

    #@84
    iget v4, v4, Landroid/media/AudioService$RemoteControlStackEntry;->mCallingUid:I

    #@86
    iget v5, v0, Landroid/media/AudioService$FocusStackEntry;->mCallingUid:I

    #@88
    if-eq v4, v5, :cond_8e

    #@8a
    .line 6071
    invoke-direct {p0}, Landroid/media/AudioService;->clearRemoteControlDisplay_syncAfRcs()V

    #@8d
    goto :goto_13

    #@8e
    .line 6077
    :cond_8e
    invoke-direct {p0, p1}, Landroid/media/AudioService;->updateRemoteControlDisplay_syncAfRcs(I)V

    #@91
    goto :goto_13
.end method

.method private checkUpdateRemoteStateIfActive(I)Z
    .registers 9
    .parameter "streamType"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 6480
    iget-object v4, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@4
    monitor-enter v4

    #@5
    .line 6481
    :try_start_5
    iget-object v5, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@7
    invoke-virtual {v5}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v1

    #@b
    .line 6482
    .local v1, stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :cond_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v5

    #@f
    if-eqz v5, :cond_4e

    #@11
    .line 6483
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Landroid/media/AudioService$RemoteControlStackEntry;

    #@17
    .line 6484
    .local v0, rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    iget v5, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mPlaybackType:I

    #@19
    if-ne v5, v2, :cond_b

    #@1b
    iget v5, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mPlaybackState:I

    #@1d
    invoke-static {v5}, Landroid/media/AudioService;->isPlaystateActive(I)Z

    #@20
    move-result v5

    #@21
    if-eqz v5, :cond_b

    #@23
    iget v5, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mPlaybackStream:I

    #@25
    if-ne v5, p1, :cond_b

    #@27
    .line 6489
    iget-object v3, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@29
    monitor-enter v3
    :try_end_2a
    .catchall {:try_start_5 .. :try_end_2a} :catchall_4b

    #@2a
    .line 6490
    :try_start_2a
    iget-object v5, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@2c
    iget v6, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mRccId:I

    #@2e
    iput v6, v5, Landroid/media/AudioService$RemotePlaybackState;->mRccId:I

    #@30
    .line 6491
    iget-object v5, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@32
    iget v6, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mPlaybackVolume:I

    #@34
    iput v6, v5, Landroid/media/AudioService$RemotePlaybackState;->mVolume:I

    #@36
    .line 6492
    iget-object v5, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@38
    iget v6, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mPlaybackVolumeMax:I

    #@3a
    iput v6, v5, Landroid/media/AudioService$RemotePlaybackState;->mVolumeMax:I

    #@3c
    .line 6493
    iget-object v5, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@3e
    iget v6, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mPlaybackVolumeHandling:I

    #@40
    iput v6, v5, Landroid/media/AudioService$RemotePlaybackState;->mVolumeHandling:I

    #@42
    .line 6494
    const/4 v5, 0x1

    #@43
    iput-boolean v5, p0, Landroid/media/AudioService;->mMainRemoteIsActive:Z

    #@45
    .line 6495
    monitor-exit v3
    :try_end_46
    .catchall {:try_start_2a .. :try_end_46} :catchall_48

    #@46
    .line 6496
    :try_start_46
    monitor-exit v4
    :try_end_47
    .catchall {:try_start_46 .. :try_end_47} :catchall_4b

    #@47
    .line 6503
    .end local v0           #rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    :goto_47
    return v2

    #@48
    .line 6495
    .restart local v0       #rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    :catchall_48
    move-exception v2

    #@49
    :try_start_49
    monitor-exit v3
    :try_end_4a
    .catchall {:try_start_49 .. :try_end_4a} :catchall_48

    #@4a
    :try_start_4a
    throw v2

    #@4b
    .line 6499
    .end local v0           #rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    .end local v1           #stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :catchall_4b
    move-exception v2

    #@4c
    monitor-exit v4
    :try_end_4d
    .catchall {:try_start_4a .. :try_end_4d} :catchall_4b

    #@4d
    throw v2

    #@4e
    .restart local v1       #stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :cond_4e
    :try_start_4e
    monitor-exit v4
    :try_end_4f
    .catchall {:try_start_4e .. :try_end_4f} :catchall_4b

    #@4f
    .line 6500
    iget-object v4, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@51
    monitor-enter v4

    #@52
    .line 6501
    const/4 v2, 0x0

    #@53
    :try_start_53
    iput-boolean v2, p0, Landroid/media/AudioService;->mMainRemoteIsActive:Z

    #@55
    .line 6502
    monitor-exit v4

    #@56
    move v2, v3

    #@57
    .line 6503
    goto :goto_47

    #@58
    .line 6502
    :catchall_58
    move-exception v2

    #@59
    monitor-exit v4
    :try_end_5a
    .catchall {:try_start_53 .. :try_end_5a} :catchall_58

    #@5a
    throw v2
.end method

.method private clearRemoteControlDisplay_syncAfRcs()V
    .registers 4

    #@0
    .prologue
    .line 5980
    iget-object v1, p0, Landroid/media/AudioService;->mCurrentRcLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 5981
    const/4 v0, 0x0

    #@4
    :try_start_4
    iput-object v0, p0, Landroid/media/AudioService;->mCurrentRcClient:Landroid/media/IRemoteControlClient;

    #@6
    .line 5982
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_4 .. :try_end_7} :catchall_15

    #@7
    .line 5984
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@9
    iget-object v1, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@b
    const/16 v2, 0xc

    #@d
    invoke-virtual {v1, v2}, Landroid/media/AudioService$AudioHandler;->obtainMessage(I)Landroid/os/Message;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v0, v1}, Landroid/media/AudioService$AudioHandler;->sendMessage(Landroid/os/Message;)Z

    #@14
    .line 5985
    return-void

    #@15
    .line 5982
    :catchall_15
    move-exception v0

    #@16
    :try_start_16
    monitor-exit v1
    :try_end_17
    .catchall {:try_start_16 .. :try_end_17} :catchall_15

    #@17
    throw v0
.end method

.method private createAudioSystemThread()V
    .registers 2

    #@0
    .prologue
    .line 701
    new-instance v0, Landroid/media/AudioService$AudioSystemThread;

    #@2
    invoke-direct {v0, p0}, Landroid/media/AudioService$AudioSystemThread;-><init>(Landroid/media/AudioService;)V

    #@5
    iput-object v0, p0, Landroid/media/AudioService;->mAudioSystemThread:Landroid/media/AudioService$AudioSystemThread;

    #@7
    .line 702
    iget-object v0, p0, Landroid/media/AudioService;->mAudioSystemThread:Landroid/media/AudioService$AudioSystemThread;

    #@9
    invoke-virtual {v0}, Landroid/media/AudioService$AudioSystemThread;->start()V

    #@c
    .line 703
    invoke-direct {p0}, Landroid/media/AudioService;->waitForAudioHandlerCreation()V

    #@f
    .line 704
    return-void
.end method

.method private createStreamStates()V
    .registers 7

    #@0
    .prologue
    .line 739
    invoke-static {}, Landroid/media/AudioSystem;->getNumStreamTypes()I

    #@3
    move-result v1

    #@4
    .line 740
    .local v1, numStreamTypes:I
    new-array v2, v1, [Landroid/media/AudioService$VolumeStreamState;

    #@6
    iput-object v2, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@8
    .line 742
    .local v2, streams:[Landroid/media/AudioService$VolumeStreamState;
    const/4 v0, 0x0

    #@9
    .local v0, i:I
    :goto_9
    if-ge v0, v1, :cond_1e

    #@b
    .line 743
    new-instance v3, Landroid/media/AudioService$VolumeStreamState;

    #@d
    sget-object v4, Landroid/provider/Settings$System;->VOLUME_SETTINGS:[Ljava/lang/String;

    #@f
    iget-object v5, p0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@11
    aget v5, v5, v0

    #@13
    aget-object v4, v4, v5

    #@15
    const/4 v5, 0x0

    #@16
    invoke-direct {v3, p0, v4, v0, v5}, Landroid/media/AudioService$VolumeStreamState;-><init>(Landroid/media/AudioService;Ljava/lang/String;ILandroid/media/AudioService$1;)V

    #@19
    aput-object v3, v2, v0

    #@1b
    .line 742
    add-int/lit8 v0, v0, 0x1

    #@1d
    goto :goto_9

    #@1e
    .line 746
    :cond_1e
    invoke-direct {p0}, Landroid/media/AudioService;->checkAllAliasStreamVolumes()V

    #@21
    .line 747
    return-void
.end method

.method private discardAudioFocusOwner()V
    .registers 7

    #@0
    .prologue
    .line 4858
    sget-object v3, Landroid/media/AudioService;->mAudioFocusLock:Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 4859
    :try_start_3
    iget-object v2, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@5
    invoke-virtual {v2}, Ljava/util/Stack;->empty()Z

    #@8
    move-result v2

    #@9
    if-nez v2, :cond_31

    #@b
    iget-object v2, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@d
    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    #@10
    move-result-object v2

    #@11
    check-cast v2, Landroid/media/AudioService$FocusStackEntry;

    #@13
    iget-object v2, v2, Landroid/media/AudioService$FocusStackEntry;->mFocusDispatcher:Landroid/media/IAudioFocusDispatcher;

    #@15
    if-eqz v2, :cond_31

    #@17
    .line 4861
    iget-object v2, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@19
    invoke-virtual {v2}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    #@1c
    move-result-object v1

    #@1d
    check-cast v1, Landroid/media/AudioService$FocusStackEntry;
    :try_end_1f
    .catchall {:try_start_3 .. :try_end_1f} :catchall_50

    #@1f
    .line 4863
    .local v1, focusOwner:Landroid/media/AudioService$FocusStackEntry;
    :try_start_1f
    iget-object v2, v1, Landroid/media/AudioService$FocusStackEntry;->mFocusDispatcher:Landroid/media/IAudioFocusDispatcher;

    #@21
    const/4 v4, -0x1

    #@22
    iget-object v5, v1, Landroid/media/AudioService$FocusStackEntry;->mClientId:Ljava/lang/String;

    #@24
    invoke-interface {v2, v4, v5}, Landroid/media/IAudioFocusDispatcher;->dispatchAudioFocusChange(ILjava/lang/String;)V
    :try_end_27
    .catchall {:try_start_1f .. :try_end_27} :catchall_50
    .catch Landroid/os/RemoteException; {:try_start_1f .. :try_end_27} :catch_33

    #@27
    .line 4869
    :goto_27
    :try_start_27
    invoke-virtual {v1}, Landroid/media/AudioService$FocusStackEntry;->unlinkToDeath()V

    #@2a
    .line 4871
    iget-object v4, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@2c
    monitor-enter v4
    :try_end_2d
    .catchall {:try_start_27 .. :try_end_2d} :catchall_50

    #@2d
    .line 4872
    :try_start_2d
    invoke-direct {p0}, Landroid/media/AudioService;->clearRemoteControlDisplay_syncAfRcs()V

    #@30
    .line 4873
    monitor-exit v4
    :try_end_31
    .catchall {:try_start_2d .. :try_end_31} :catchall_53

    #@31
    .line 4875
    .end local v1           #focusOwner:Landroid/media/AudioService$FocusStackEntry;
    :cond_31
    :try_start_31
    monitor-exit v3

    #@32
    .line 4876
    return-void

    #@33
    .line 4865
    .restart local v1       #focusOwner:Landroid/media/AudioService$FocusStackEntry;
    :catch_33
    move-exception v0

    #@34
    .line 4866
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "AudioService"

    #@36
    new-instance v4, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v5, "Failure to signal loss of audio focus due to "

    #@3d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v4

    #@45
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v4

    #@49
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 4867
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@4f
    goto :goto_27

    #@50
    .line 4875
    .end local v0           #e:Landroid/os/RemoteException;
    .end local v1           #focusOwner:Landroid/media/AudioService$FocusStackEntry;
    :catchall_50
    move-exception v2

    #@51
    monitor-exit v3
    :try_end_52
    .catchall {:try_start_31 .. :try_end_52} :catchall_50

    #@52
    throw v2

    #@53
    .line 4873
    .restart local v1       #focusOwner:Landroid/media/AudioService$FocusStackEntry;
    :catchall_53
    move-exception v2

    #@54
    :try_start_54
    monitor-exit v4
    :try_end_55
    .catchall {:try_start_54 .. :try_end_55} :catchall_53

    #@55
    :try_start_55
    throw v2
    :try_end_56
    .catchall {:try_start_55 .. :try_end_56} :catchall_50
.end method

.method private disconnectBluetoothSco(I)V
    .registers 10
    .parameter "exceptPid"

    #@0
    .prologue
    const/4 v2, 0x4

    #@1
    const/4 v1, 0x2

    #@2
    .line 2724
    iget-object v7, p0, Landroid/media/AudioService;->mScoClients:Ljava/util/ArrayList;

    #@4
    monitor-enter v7

    #@5
    .line 2725
    :try_start_5
    invoke-direct {p0}, Landroid/media/AudioService;->checkScoAudioState()V

    #@8
    .line 2726
    iget v0, p0, Landroid/media/AudioService;->mScoAudioState:I

    #@a
    if-eq v0, v1, :cond_10

    #@c
    iget v0, p0, Landroid/media/AudioService;->mScoAudioState:I

    #@e
    if-ne v0, v2, :cond_41

    #@10
    .line 2728
    :cond_10
    iget-object v0, p0, Landroid/media/AudioService;->mBluetoothHeadsetDevice:Landroid/bluetooth/BluetoothDevice;

    #@12
    if-eqz v0, :cond_2e

    #@14
    .line 2729
    iget-object v0, p0, Landroid/media/AudioService;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    #@16
    if-eqz v0, :cond_30

    #@18
    .line 2730
    iget-object v0, p0, Landroid/media/AudioService;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    #@1a
    iget-object v1, p0, Landroid/media/AudioService;->mBluetoothHeadsetDevice:Landroid/bluetooth/BluetoothDevice;

    #@1c
    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothHeadset;->stopVoiceRecognition(Landroid/bluetooth/BluetoothDevice;)Z

    #@1f
    move-result v0

    #@20
    if-nez v0, :cond_2e

    #@22
    .line 2732
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@24
    const/16 v1, 0xb

    #@26
    const/4 v2, 0x0

    #@27
    const/4 v3, 0x0

    #@28
    const/4 v4, 0x0

    #@29
    const/4 v5, 0x0

    #@2a
    const/4 v6, 0x0

    #@2b
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@2e
    .line 2743
    :cond_2e
    :goto_2e
    monitor-exit v7

    #@2f
    .line 2744
    return-void

    #@30
    .line 2735
    :cond_30
    iget v0, p0, Landroid/media/AudioService;->mScoAudioState:I

    #@32
    if-ne v0, v1, :cond_2e

    #@34
    invoke-direct {p0}, Landroid/media/AudioService;->getBluetoothHeadset()Z

    #@37
    move-result v0

    #@38
    if-eqz v0, :cond_2e

    #@3a
    .line 2737
    const/4 v0, 0x4

    #@3b
    iput v0, p0, Landroid/media/AudioService;->mScoAudioState:I

    #@3d
    goto :goto_2e

    #@3e
    .line 2743
    :catchall_3e
    move-exception v0

    #@3f
    monitor-exit v7
    :try_end_40
    .catchall {:try_start_5 .. :try_end_40} :catchall_3e

    #@40
    throw v0

    #@41
    .line 2741
    :cond_41
    const/4 v0, 0x1

    #@42
    :try_start_42
    invoke-virtual {p0, p1, v0}, Landroid/media/AudioService;->clearAllScoClients(IZ)V
    :try_end_45
    .catchall {:try_start_42 .. :try_end_45} :catchall_3e

    #@45
    goto :goto_2e
.end method

.method private dispatchMediaKeyEvent(Landroid/view/KeyEvent;Z)V
    .registers 19
    .parameter "keyEvent"
    .parameter "needWakeLock"

    #@0
    .prologue
    .line 5289
    if-eqz p2, :cond_9

    #@2
    .line 5290
    move-object/from16 v0, p0

    #@4
    iget-object v1, v0, Landroid/media/AudioService;->mMediaEventWakeLock:Landroid/os/PowerManager$WakeLock;

    #@6
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@9
    .line 5292
    :cond_9
    new-instance v4, Landroid/content/Intent;

    #@b
    const-string v1, "android.intent.action.MEDIA_BUTTON"

    #@d
    const/4 v2, 0x0

    #@e
    invoke-direct {v4, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@11
    .line 5293
    .local v4, keyIntent:Landroid/content/Intent;
    const-string v1, "android.intent.extra.KEY_EVENT"

    #@13
    move-object/from16 v0, p1

    #@15
    invoke-virtual {v4, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@18
    .line 5294
    move-object/from16 v0, p0

    #@1a
    iget-object v15, v0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@1c
    monitor-enter v15

    #@1d
    .line 5295
    :try_start_1d
    move-object/from16 v0, p0

    #@1f
    iget-object v1, v0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@21
    invoke-virtual {v1}, Ljava/util/Stack;->empty()Z
    :try_end_24
    .catchall {:try_start_1d .. :try_end_24} :catchall_6d

    #@24
    move-result v1

    #@25
    if-nez v1, :cond_70

    #@27
    .line 5298
    :try_start_27
    move-object/from16 v0, p0

    #@29
    iget-object v1, v0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@2b
    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    #@2e
    move-result-object v1

    #@2f
    check-cast v1, Landroid/media/AudioService$RemoteControlStackEntry;

    #@31
    iget-object v1, v1, Landroid/media/AudioService$RemoteControlStackEntry;->mMediaIntent:Landroid/app/PendingIntent;

    #@33
    move-object/from16 v0, p0

    #@35
    iget-object v2, v0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@37
    if-eqz p2, :cond_46

    #@39
    const/16 v3, 0x7bc

    #@3b
    :goto_3b
    move-object/from16 v0, p0

    #@3d
    iget-object v6, v0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@3f
    move-object/from16 v5, p0

    #@41
    invoke-virtual/range {v1 .. v6}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;Landroid/app/PendingIntent$OnFinished;Landroid/os/Handler;)V
    :try_end_44
    .catchall {:try_start_27 .. :try_end_44} :catchall_6d
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_27 .. :try_end_44} :catch_48

    #@44
    .line 5320
    :goto_44
    :try_start_44
    monitor-exit v15

    #@45
    .line 5321
    return-void

    #@46
    .line 5298
    :cond_46
    const/4 v3, 0x0

    #@47
    goto :goto_3b

    #@48
    .line 5301
    :catch_48
    move-exception v12

    #@49
    .line 5302
    .local v12, e:Landroid/app/PendingIntent$CanceledException;
    const-string v1, "AudioService"

    #@4b
    new-instance v2, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v3, "Error sending pending intent "

    #@52
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v2

    #@56
    move-object/from16 v0, p0

    #@58
    iget-object v3, v0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@5a
    invoke-virtual {v3}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    #@5d
    move-result-object v3

    #@5e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v2

    #@62
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v2

    #@66
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@69
    .line 5303
    invoke-virtual {v12}, Landroid/app/PendingIntent$CanceledException;->printStackTrace()V

    #@6c
    goto :goto_44

    #@6d
    .line 5320
    .end local v12           #e:Landroid/app/PendingIntent$CanceledException;
    :catchall_6d
    move-exception v1

    #@6e
    monitor-exit v15
    :try_end_6f
    .catchall {:try_start_44 .. :try_end_6f} :catchall_6d

    #@6f
    throw v1

    #@70
    .line 5308
    :cond_70
    if-eqz p2, :cond_79

    #@72
    .line 5309
    :try_start_72
    const-string v1, "android.media.AudioService.WAKELOCK_ACQUIRED"

    #@74
    const/16 v2, 0x7bc

    #@76
    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@79
    .line 5311
    :cond_79
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_7c
    .catchall {:try_start_72 .. :try_end_7c} :catchall_6d

    #@7c
    move-result-wide v13

    #@7d
    .line 5313
    .local v13, ident:J
    :try_start_7d
    move-object/from16 v0, p0

    #@7f
    iget-object v3, v0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@81
    sget-object v5, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@83
    const/4 v6, 0x0

    #@84
    move-object/from16 v0, p0

    #@86
    iget-object v7, v0, Landroid/media/AudioService;->mKeyEventDone:Landroid/content/BroadcastReceiver;

    #@88
    move-object/from16 v0, p0

    #@8a
    iget-object v8, v0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@8c
    const/4 v9, -0x1

    #@8d
    const/4 v10, 0x0

    #@8e
    const/4 v11, 0x0

    #@8f
    invoke-virtual/range {v3 .. v11}, Landroid/content/Context;->sendOrderedBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V
    :try_end_92
    .catchall {:try_start_7d .. :try_end_92} :catchall_96

    #@92
    .line 5317
    :try_start_92
    invoke-static {v13, v14}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@95
    goto :goto_44

    #@96
    :catchall_96
    move-exception v1

    #@97
    invoke-static {v13, v14}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@9a
    throw v1
    :try_end_9b
    .catchall {:try_start_92 .. :try_end_9b} :catchall_6d
.end method

.method private dispatchMediaKeyEventForCalls(Landroid/view/KeyEvent;Z)V
    .registers 14
    .parameter "keyEvent"
    .parameter "needWakeLock"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 5265
    new-instance v1, Landroid/content/Intent;

    #@3
    const-string v0, "android.intent.action.MEDIA_BUTTON"

    #@5
    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@8
    .line 5266
    .local v1, keyIntent:Landroid/content/Intent;
    const-string v0, "android.intent.extra.KEY_EVENT"

    #@a
    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@d
    .line 5267
    iget-object v0, p0, Landroid/media/AudioService;->mMediaReceiverForCalls:Landroid/content/ComponentName;

    #@f
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    #@16
    .line 5268
    if-eqz p2, :cond_24

    #@18
    .line 5269
    iget-object v0, p0, Landroid/media/AudioService;->mMediaEventWakeLock:Landroid/os/PowerManager$WakeLock;

    #@1a
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@1d
    .line 5270
    const-string v0, "android.media.AudioService.WAKELOCK_ACQUIRED"

    #@1f
    const/16 v2, 0x7bc

    #@21
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@24
    .line 5272
    :cond_24
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@27
    move-result-wide v9

    #@28
    .line 5274
    .local v9, ident:J
    :try_start_28
    iget-object v0, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@2a
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@2c
    const/4 v3, 0x0

    #@2d
    iget-object v4, p0, Landroid/media/AudioService;->mKeyEventDone:Landroid/content/BroadcastReceiver;

    #@2f
    iget-object v5, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@31
    const/4 v6, -0x1

    #@32
    const/4 v7, 0x0

    #@33
    const/4 v8, 0x0

    #@34
    invoke-virtual/range {v0 .. v8}, Landroid/content/Context;->sendOrderedBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V
    :try_end_37
    .catchall {:try_start_28 .. :try_end_37} :catchall_3b

    #@37
    .line 5277
    invoke-static {v9, v10}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@3a
    .line 5279
    return-void

    #@3b
    .line 5277
    :catchall_3b
    move-exception v0

    #@3c
    invoke-static {v9, v10}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@3f
    throw v0
.end method

.method private dispatchMediaKeyEventForCallsList(Landroid/view/KeyEvent;Landroid/content/ComponentName;Z)V
    .registers 15
    .parameter "keyEvent"
    .parameter "c"
    .parameter "needWakeLock"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 7018
    new-instance v1, Landroid/content/Intent;

    #@3
    const-string v0, "android.intent.action.MEDIA_BUTTON"

    #@5
    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@8
    .line 7019
    .local v1, keyIntent:Landroid/content/Intent;
    const-string v0, "android.intent.extra.KEY_EVENT"

    #@a
    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@d
    .line 7020
    invoke-virtual {p2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    #@14
    .line 7021
    if-eqz p3, :cond_22

    #@16
    .line 7022
    iget-object v0, p0, Landroid/media/AudioService;->mMediaEventWakeLock:Landroid/os/PowerManager$WakeLock;

    #@18
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@1b
    .line 7023
    const-string v0, "android.media.AudioService.WAKELOCK_ACQUIRED"

    #@1d
    const/16 v2, 0x7bc

    #@1f
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@22
    .line 7025
    :cond_22
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@25
    move-result-wide v9

    #@26
    .line 7027
    .local v9, ident:J
    :try_start_26
    const-string v0, "AudioService"

    #@28
    new-instance v2, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v3, "dispatchMediaKeyEventForCallsList sendOrderedBroadcast MEDIA_BUTTON to "

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    const-string v3, " keyEvent "

    #@39
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v2

    #@41
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v2

    #@45
    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 7028
    iget-object v0, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@4a
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@4c
    const/4 v3, 0x0

    #@4d
    iget-object v4, p0, Landroid/media/AudioService;->mKeyEventDone:Landroid/content/BroadcastReceiver;

    #@4f
    iget-object v5, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@51
    const/4 v6, -0x1

    #@52
    const/4 v7, 0x0

    #@53
    const/4 v8, 0x0

    #@54
    invoke-virtual/range {v0 .. v8}, Landroid/content/Context;->sendOrderedBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V
    :try_end_57
    .catchall {:try_start_26 .. :try_end_57} :catchall_5b

    #@57
    .line 7031
    invoke-static {v9, v10}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@5a
    .line 7033
    return-void

    #@5b
    .line 7031
    :catchall_5b
    move-exception v0

    #@5c
    invoke-static {v9, v10}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@5f
    throw v0
.end method

.method private doSetMasterVolume(FI)V
    .registers 12
    .parameter "volume"
    .parameter "flags"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1608
    invoke-static {}, Landroid/media/AudioSystem;->getMasterMute()Z

    #@4
    move-result v0

    #@5
    if-nez v0, :cond_28

    #@7
    .line 1609
    invoke-virtual {p0}, Landroid/media/AudioService;->getMasterVolume()I

    #@a
    move-result v8

    #@b
    .line 1610
    .local v8, oldVolume:I
    invoke-static {p1}, Landroid/media/AudioSystem;->setMasterVolume(F)I

    #@e
    .line 1612
    invoke-virtual {p0}, Landroid/media/AudioService;->getMasterVolume()I

    #@11
    move-result v7

    #@12
    .line 1613
    .local v7, newVolume:I
    if-eq v7, v8, :cond_25

    #@14
    .line 1615
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@16
    const/4 v1, 0x2

    #@17
    const/high16 v3, 0x447a

    #@19
    mul-float/2addr v3, p1

    #@1a
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    #@1d
    move-result v3

    #@1e
    const/4 v5, 0x0

    #@1f
    const/16 v6, 0x1f4

    #@21
    move v4, v2

    #@22
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@25
    .line 1619
    :cond_25
    invoke-direct {p0, p2, v8, v7}, Landroid/media/AudioService;->sendMasterVolumeUpdate(III)V

    #@28
    .line 1621
    .end local v7           #newVolume:I
    .end local v8           #oldVolume:I
    :cond_28
    return-void
.end method

.method private dumpFocusStack(Ljava/io/PrintWriter;)V
    .registers 7
    .parameter "pw"

    #@0
    .prologue
    .line 4944
    const-string v2, "\nAudio Focus stack entries:"

    #@2
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5
    .line 4945
    sget-object v3, Landroid/media/AudioService;->mAudioFocusLock:Ljava/lang/Object;

    #@7
    monitor-enter v3

    #@8
    .line 4946
    :try_start_8
    iget-object v2, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@a
    invoke-virtual {v2}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    #@d
    move-result-object v1

    #@e
    .line 4947
    .local v1, stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$FocusStackEntry;>;"
    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@11
    move-result v2

    #@12
    if-eqz v2, :cond_72

    #@14
    .line 4948
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@17
    move-result-object v0

    #@18
    check-cast v0, Landroid/media/AudioService$FocusStackEntry;

    #@1a
    .line 4949
    .local v0, fse:Landroid/media/AudioService$FocusStackEntry;
    new-instance v2, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v4, "  source:"

    #@21
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    iget-object v4, v0, Landroid/media/AudioService$FocusStackEntry;->mSourceRef:Landroid/os/IBinder;

    #@27
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    const-string v4, " -- pack: "

    #@2d
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    iget-object v4, v0, Landroid/media/AudioService$FocusStackEntry;->mPackageName:Ljava/lang/String;

    #@33
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    const-string v4, " -- client: "

    #@39
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    iget-object v4, v0, Landroid/media/AudioService$FocusStackEntry;->mClientId:Ljava/lang/String;

    #@3f
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    const-string v4, " -- duration: "

    #@45
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    iget v4, v0, Landroid/media/AudioService$FocusStackEntry;->mFocusChangeType:I

    #@4b
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v2

    #@4f
    const-string v4, " -- uid: "

    #@51
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v2

    #@55
    iget v4, v0, Landroid/media/AudioService$FocusStackEntry;->mCallingUid:I

    #@57
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v2

    #@5b
    const-string v4, " -- stream: "

    #@5d
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v2

    #@61
    iget v4, v0, Landroid/media/AudioService$FocusStackEntry;->mStreamType:I

    #@63
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@66
    move-result-object v2

    #@67
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v2

    #@6b
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@6e
    goto :goto_e

    #@6f
    .line 4956
    .end local v0           #fse:Landroid/media/AudioService$FocusStackEntry;
    .end local v1           #stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$FocusStackEntry;>;"
    :catchall_6f
    move-exception v2

    #@70
    monitor-exit v3
    :try_end_71
    .catchall {:try_start_8 .. :try_end_71} :catchall_6f

    #@71
    throw v2

    #@72
    .restart local v1       #stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$FocusStackEntry;>;"
    :cond_72
    :try_start_72
    monitor-exit v3
    :try_end_73
    .catchall {:try_start_72 .. :try_end_73} :catchall_6f

    #@73
    .line 4957
    return-void
.end method

.method private dumpRCCStack(Ljava/io/PrintWriter;)V
    .registers 7
    .parameter "pw"

    #@0
    .prologue
    .line 5723
    const-string v2, "\nRemote Control Client stack entries:"

    #@2
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5
    .line 5724
    iget-object v3, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@7
    monitor-enter v3

    #@8
    .line 5725
    :try_start_8
    iget-object v2, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@a
    invoke-virtual {v2}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    #@d
    move-result-object v1

    #@e
    .line 5726
    .local v1, stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@11
    move-result v2

    #@12
    if-eqz v2, :cond_8a

    #@14
    .line 5727
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@17
    move-result-object v0

    #@18
    check-cast v0, Landroid/media/AudioService$RemoteControlStackEntry;

    #@1a
    .line 5728
    .local v0, rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    new-instance v2, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v4, "  uid: "

    #@21
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    iget v4, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mCallingUid:I

    #@27
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    const-string v4, "  -- id: "

    #@2d
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    iget v4, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mRccId:I

    #@33
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    const-string v4, "  -- type: "

    #@39
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    iget v4, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mPlaybackType:I

    #@3f
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    const-string v4, "  -- state: "

    #@45
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    iget v4, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mPlaybackState:I

    #@4b
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v2

    #@4f
    const-string v4, "  -- vol handling: "

    #@51
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v2

    #@55
    iget v4, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mPlaybackVolumeHandling:I

    #@57
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v2

    #@5b
    const-string v4, "  -- vol: "

    #@5d
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v2

    #@61
    iget v4, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mPlaybackVolume:I

    #@63
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@66
    move-result-object v2

    #@67
    const-string v4, "  -- volMax: "

    #@69
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v2

    #@6d
    iget v4, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mPlaybackVolumeMax:I

    #@6f
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@72
    move-result-object v2

    #@73
    const-string v4, "  -- volObs: "

    #@75
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v2

    #@79
    iget-object v4, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mRemoteVolumeObs:Landroid/media/IRemoteVolumeObserver;

    #@7b
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v2

    #@7f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v2

    #@83
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@86
    goto :goto_e

    #@87
    .line 5738
    .end local v0           #rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    .end local v1           #stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :catchall_87
    move-exception v2

    #@88
    monitor-exit v3
    :try_end_89
    .catchall {:try_start_8 .. :try_end_89} :catchall_87

    #@89
    throw v2

    #@8a
    .restart local v1       #stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :cond_8a
    :try_start_8a
    monitor-exit v3
    :try_end_8b
    .catchall {:try_start_8a .. :try_end_8b} :catchall_87

    #@8b
    .line 5739
    iget-object v3, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@8d
    monitor-enter v3

    #@8e
    .line 5740
    :try_start_8e
    const-string v2, "\nRemote Volume State:"

    #@90
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@93
    .line 5741
    new-instance v2, Ljava/lang/StringBuilder;

    #@95
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@98
    const-string v4, "  has remote: "

    #@9a
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v2

    #@9e
    iget-boolean v4, p0, Landroid/media/AudioService;->mHasRemotePlayback:Z

    #@a0
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v2

    #@a4
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a7
    move-result-object v2

    #@a8
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@ab
    .line 5742
    new-instance v2, Ljava/lang/StringBuilder;

    #@ad
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b0
    const-string v4, "  is remote active: "

    #@b2
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v2

    #@b6
    iget-boolean v4, p0, Landroid/media/AudioService;->mMainRemoteIsActive:Z

    #@b8
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v2

    #@bc
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bf
    move-result-object v2

    #@c0
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@c3
    .line 5743
    new-instance v2, Ljava/lang/StringBuilder;

    #@c5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c8
    const-string v4, "  rccId: "

    #@ca
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v2

    #@ce
    iget-object v4, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@d0
    iget v4, v4, Landroid/media/AudioService$RemotePlaybackState;->mRccId:I

    #@d2
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v2

    #@d6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d9
    move-result-object v2

    #@da
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@dd
    .line 5744
    new-instance v2, Ljava/lang/StringBuilder;

    #@df
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e2
    const-string v4, "  volume handling: "

    #@e4
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v4

    #@e8
    iget-object v2, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@ea
    iget v2, v2, Landroid/media/AudioService$RemotePlaybackState;->mVolumeHandling:I

    #@ec
    if-nez v2, :cond_131

    #@ee
    const-string v2, "PLAYBACK_VOLUME_FIXED(0)"

    #@f0
    :goto_f0
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v2

    #@f4
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f7
    move-result-object v2

    #@f8
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@fb
    .line 5747
    new-instance v2, Ljava/lang/StringBuilder;

    #@fd
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@100
    const-string v4, "  volume: "

    #@102
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@105
    move-result-object v2

    #@106
    iget-object v4, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@108
    iget v4, v4, Landroid/media/AudioService$RemotePlaybackState;->mVolume:I

    #@10a
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v2

    #@10e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@111
    move-result-object v2

    #@112
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@115
    .line 5748
    new-instance v2, Ljava/lang/StringBuilder;

    #@117
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11a
    const-string v4, "  volume steps: "

    #@11c
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11f
    move-result-object v2

    #@120
    iget-object v4, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@122
    iget v4, v4, Landroid/media/AudioService$RemotePlaybackState;->mVolumeMax:I

    #@124
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@127
    move-result-object v2

    #@128
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12b
    move-result-object v2

    #@12c
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@12f
    .line 5749
    monitor-exit v3

    #@130
    .line 5750
    return-void

    #@131
    .line 5744
    :cond_131
    const-string v2, "PLAYBACK_VOLUME_VARIABLE(1)"

    #@133
    goto :goto_f0

    #@134
    .line 5749
    :catchall_134
    move-exception v2

    #@135
    monitor-exit v3
    :try_end_136
    .catchall {:try_start_8e .. :try_end_136} :catchall_134

    #@136
    throw v2
.end method

.method private dumpRCStack(Ljava/io/PrintWriter;)V
    .registers 7
    .parameter "pw"

    #@0
    .prologue
    .line 5701
    const-string v2, "\nRemote Control stack entries:"

    #@2
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5
    .line 5702
    iget-object v3, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@7
    monitor-enter v3

    #@8
    .line 5703
    :try_start_8
    iget-object v2, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@a
    invoke-virtual {v2}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    #@d
    move-result-object v1

    #@e
    .line 5704
    .local v1, stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@11
    move-result v2

    #@12
    if-eqz v2, :cond_7e

    #@14
    .line 5705
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@17
    move-result-object v0

    #@18
    check-cast v0, Landroid/media/AudioService$RemoteControlStackEntry;

    #@1a
    .line 5706
    .local v0, rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    new-instance v2, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v4, "  pi: "

    #@21
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    iget-object v4, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mMediaIntent:Landroid/app/PendingIntent;

    #@27
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    const-string v4, " -- pack: "

    #@2d
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    iget-object v4, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mCallingPackageName:Ljava/lang/String;

    #@33
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    const-string v4, "  -- ercvr: "

    #@39
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    iget-object v4, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mReceiverComponent:Landroid/content/ComponentName;

    #@3f
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    const-string v4, "  -- client: "

    #@45
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    iget-object v4, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mRcClient:Landroid/media/IRemoteControlClient;

    #@4b
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v2

    #@4f
    const-string v4, "  -- uid: "

    #@51
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v2

    #@55
    iget v4, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mCallingUid:I

    #@57
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v2

    #@5b
    const-string v4, "  -- type: "

    #@5d
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v2

    #@61
    iget v4, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mPlaybackType:I

    #@63
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@66
    move-result-object v2

    #@67
    const-string v4, "  state: "

    #@69
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v2

    #@6d
    iget v4, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mPlaybackState:I

    #@6f
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@72
    move-result-object v2

    #@73
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v2

    #@77
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@7a
    goto :goto_e

    #@7b
    .line 5714
    .end local v0           #rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    .end local v1           #stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :catchall_7b
    move-exception v2

    #@7c
    monitor-exit v3
    :try_end_7d
    .catchall {:try_start_8 .. :try_end_7d} :catchall_7b

    #@7d
    throw v2

    #@7e
    .restart local v1       #stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :cond_7e
    :try_start_7e
    monitor-exit v3
    :try_end_7f
    .catchall {:try_start_7e .. :try_end_7f} :catchall_7b

    #@7f
    .line 5715
    return-void
.end method

.method private dumpRingerMode(Ljava/io/PrintWriter;)V
    .registers 5
    .parameter "pw"

    #@0
    .prologue
    .line 6941
    const-string v0, "\nRinger mode: "

    #@2
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5
    .line 6942
    new-instance v0, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v1, "- mode: "

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v0

    #@10
    sget-object v1, Landroid/media/AudioService;->RINGER_MODE_NAMES:[Ljava/lang/String;

    #@12
    iget v2, p0, Landroid/media/AudioService;->mRingerMode:I

    #@14
    aget-object v1, v1, v2

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@21
    .line 6943
    const-string v0, "- ringer mode affected streams = 0x"

    #@23
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@26
    .line 6944
    iget v0, p0, Landroid/media/AudioService;->mRingerModeAffectedStreams:I

    #@28
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@2b
    move-result-object v0

    #@2c
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2f
    .line 6945
    const-string v0, "- ringer mode muted streams = 0x"

    #@31
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@34
    .line 6946
    iget v0, p0, Landroid/media/AudioService;->mRingerModeMutedStreams:I

    #@36
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@39
    move-result-object v0

    #@3a
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3d
    .line 6947
    return-void
.end method

.method private dumpStreamStates(Ljava/io/PrintWriter;)V
    .registers 6
    .parameter "pw"

    #@0
    .prologue
    .line 750
    const-string v2, "\nStream volumes (device: index)"

    #@2
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5
    .line 751
    invoke-static {}, Landroid/media/AudioSystem;->getNumStreamTypes()I

    #@8
    move-result v1

    #@9
    .line 752
    .local v1, numStreamTypes:I
    const/4 v0, 0x0

    #@a
    .local v0, i:I
    :goto_a
    if-ge v0, v1, :cond_3b

    #@c
    .line 753
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "- "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    iget-object v3, p0, Landroid/media/AudioService;->STREAM_NAMES:[Ljava/lang/String;

    #@19
    aget-object v3, v3, v0

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    const-string v3, ":"

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2c
    .line 754
    iget-object v2, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@2e
    aget-object v2, v2, v0

    #@30
    invoke-static {v2, p1}, Landroid/media/AudioService$VolumeStreamState;->access$900(Landroid/media/AudioService$VolumeStreamState;Ljava/io/PrintWriter;)V

    #@33
    .line 755
    const-string v2, ""

    #@35
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@38
    .line 752
    add-int/lit8 v0, v0, 0x1

    #@3a
    goto :goto_a

    #@3b
    .line 757
    :cond_3b
    const-string v2, "\n- mute affected streams = 0x"

    #@3d
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@40
    .line 758
    iget v2, p0, Landroid/media/AudioService;->mMuteAffectedStreams:I

    #@42
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@45
    move-result-object v2

    #@46
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@49
    .line 759
    return-void
.end method

.method private enableVoiceActivation(Landroid/content/Context;Z)V
    .registers 7
    .parameter "context"
    .parameter "onOff"

    #@0
    .prologue
    .line 7220
    const-string v1, "AudioService"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "enableVoiceActivation() onOff "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 7221
    new-instance v0, Landroid/content/Intent;

    #@1a
    const-string v1, "com.lge.pa.action.VOICE_ACTIVATION"

    #@1c
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1f
    .line 7222
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "VOICE_ACTIVATION_ON_OFF"

    #@21
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@24
    .line 7223
    invoke-direct {p0, v0}, Landroid/media/AudioService;->sendBroadcastToAll(Landroid/content/Intent;)V

    #@27
    .line 7224
    return-void
.end method

.method private enforceSafeMediaVolume()V
    .registers 20

    #@0
    .prologue
    .line 6848
    move-object/from16 v0, p0

    #@2
    iget-object v1, v0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@4
    const/4 v2, 0x3

    #@5
    aget-object v6, v1, v2

    #@7
    .line 6849
    .local v6, streamState:Landroid/media/AudioService$VolumeStreamState;
    invoke-static {v6}, Landroid/media/AudioService$VolumeStreamState;->access$700(Landroid/media/AudioService$VolumeStreamState;)I

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_22

    #@d
    const/16 v18, 0x1

    #@f
    .line 6850
    .local v18, lastAudible:Z
    :goto_f
    const/16 v14, 0xc

    #@11
    .line 6851
    .local v14, devices:I
    const/4 v15, 0x0

    #@12
    .local v15, i:I
    move/from16 v16, v15

    #@14
    .line 6853
    .end local v15           #i:I
    .local v16, i:I
    :goto_14
    if-eqz v14, :cond_64

    #@16
    .line 6854
    const/4 v1, 0x1

    #@17
    add-int/lit8 v15, v16, 0x1

    #@19
    .end local v16           #i:I
    .restart local v15       #i:I
    shl-int v5, v1, v16

    #@1b
    .line 6855
    .local v5, device:I
    and-int v1, v5, v14

    #@1d
    if-nez v1, :cond_25

    #@1f
    move/from16 v16, v15

    #@21
    .line 6856
    .end local v15           #i:I
    .restart local v16       #i:I
    goto :goto_14

    #@22
    .line 6849
    .end local v5           #device:I
    .end local v14           #devices:I
    .end local v16           #i:I
    .end local v18           #lastAudible:Z
    :cond_22
    const/16 v18, 0x0

    #@24
    goto :goto_f

    #@25
    .line 6858
    .restart local v5       #device:I
    .restart local v14       #devices:I
    .restart local v15       #i:I
    .restart local v18       #lastAudible:Z
    :cond_25
    move/from16 v0, v18

    #@27
    invoke-virtual {v6, v5, v0}, Landroid/media/AudioService$VolumeStreamState;->getIndex(IZ)I

    #@2a
    move-result v17

    #@2b
    .line 6859
    .local v17, index:I
    move-object/from16 v0, p0

    #@2d
    iget v1, v0, Landroid/media/AudioService;->mSafeMediaVolumeIndex:I

    #@2f
    move/from16 v0, v17

    #@31
    if-le v0, v1, :cond_48

    #@33
    .line 6860
    if-eqz v18, :cond_4e

    #@35
    .line 6861
    move-object/from16 v0, p0

    #@37
    iget v1, v0, Landroid/media/AudioService;->mSafeMediaVolumeIndex:I

    #@39
    invoke-virtual {v6, v1, v5}, Landroid/media/AudioService$VolumeStreamState;->setLastAudibleIndex(II)V

    #@3c
    .line 6862
    move-object/from16 v0, p0

    #@3e
    iget-object v1, v0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@40
    const/4 v2, 0x1

    #@41
    const/4 v3, 0x2

    #@42
    const/4 v4, 0x2

    #@43
    const/16 v7, 0x1f4

    #@45
    invoke-static/range {v1 .. v7}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@48
    .line 6880
    :cond_48
    :goto_48
    xor-int/lit8 v1, v5, -0x1

    #@4a
    and-int/2addr v14, v1

    #@4b
    move/from16 v16, v15

    #@4d
    .line 6881
    .end local v15           #i:I
    .restart local v16       #i:I
    goto :goto_14

    #@4e
    .line 6870
    .end local v16           #i:I
    .restart local v15       #i:I
    :cond_4e
    move-object/from16 v0, p0

    #@50
    iget v1, v0, Landroid/media/AudioService;->mSafeMediaVolumeIndex:I

    #@52
    const/4 v2, 0x1

    #@53
    invoke-virtual {v6, v1, v5, v2}, Landroid/media/AudioService$VolumeStreamState;->setIndex(IIZ)Z

    #@56
    .line 6871
    move-object/from16 v0, p0

    #@58
    iget-object v7, v0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@5a
    const/4 v8, 0x0

    #@5b
    const/4 v9, 0x2

    #@5c
    const/4 v11, 0x0

    #@5d
    const/4 v13, 0x0

    #@5e
    move v10, v5

    #@5f
    move-object v12, v6

    #@60
    invoke-static/range {v7 .. v13}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@63
    goto :goto_48

    #@64
    .line 6882
    .end local v5           #device:I
    .end local v15           #i:I
    .end local v17           #index:I
    .restart local v16       #i:I
    :cond_64
    return-void
.end method

.method private ensureValidDirection(I)V
    .registers 5
    .parameter "direction"

    #@0
    .prologue
    .line 3061
    const/4 v0, -0x1

    #@1
    if-lt p1, v0, :cond_6

    #@3
    const/4 v0, 0x1

    #@4
    if-le p1, v0, :cond_1f

    #@6
    .line 3062
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Bad direction "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v0

    #@1f
    .line 3064
    :cond_1f
    return-void
.end method

.method private ensureValidRingerMode(I)V
    .registers 5
    .parameter "ringerMode"

    #@0
    .prologue
    .line 1670
    invoke-static {p1}, Landroid/media/AudioManager;->isValidRingerMode(I)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_1f

    #@6
    .line 1671
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Bad ringer mode "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v0

    #@1f
    .line 1673
    :cond_1f
    return-void
.end method

.method private ensureValidSteps(I)V
    .registers 5
    .parameter "steps"

    #@0
    .prologue
    .line 3067
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    #@3
    move-result v0

    #@4
    const/4 v1, 0x4

    #@5
    if-le v0, v1, :cond_20

    #@7
    .line 3068
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v2, "Bad volume adjust steps "

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0

    #@20
    .line 3070
    :cond_20
    return-void
.end method

.method private ensureValidStreamType(I)V
    .registers 5
    .parameter "streamType"

    #@0
    .prologue
    .line 3073
    if-ltz p1, :cond_7

    #@2
    iget-object v0, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@4
    array-length v0, v0

    #@5
    if-lt p1, v0, :cond_20

    #@7
    .line 3074
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v2, "Bad stream type "

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0

    #@20
    .line 3076
    :cond_20
    return-void
.end method

.method private filterMediaKeyEvent(Landroid/view/KeyEvent;Z)V
    .registers 10
    .parameter "keyEvent"
    .parameter "needWakeLock"

    #@0
    .prologue
    const/4 v6, 0x2

    #@1
    .line 5211
    invoke-static {p1}, Landroid/media/AudioService;->isValidMediaKeyEvent(Landroid/view/KeyEvent;)Z

    #@4
    move-result v3

    #@5
    if-nez v3, :cond_21

    #@7
    .line 5212
    const-string v3, "AudioService"

    #@9
    new-instance v4, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string/jumbo v5, "not dispatching invalid media key event "

    #@11
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v4

    #@15
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v4

    #@1d
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 5255
    :goto_20
    return-void

    #@21
    .line 5216
    :cond_21
    sget-object v4, Landroid/media/AudioService;->mRingingLock:Ljava/lang/Object;

    #@23
    monitor-enter v4

    #@24
    .line 5217
    :try_start_24
    iget-object v5, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@26
    monitor-enter v5
    :try_end_27
    .catchall {:try_start_24 .. :try_end_27} :catchall_3b

    #@27
    .line 5218
    :try_start_27
    iget-object v3, p0, Landroid/media/AudioService;->mMediaReceiverForCalls:Landroid/content/ComponentName;

    #@29
    if-eqz v3, :cond_3e

    #@2b
    iget-boolean v3, p0, Landroid/media/AudioService;->mIsRinging:Z

    #@2d
    if-nez v3, :cond_35

    #@2f
    invoke-virtual {p0}, Landroid/media/AudioService;->getMode()I

    #@32
    move-result v3

    #@33
    if-ne v3, v6, :cond_3e

    #@35
    .line 5220
    :cond_35
    invoke-direct {p0, p1, p2}, Landroid/media/AudioService;->dispatchMediaKeyEventForCalls(Landroid/view/KeyEvent;Z)V

    #@38
    .line 5221
    monitor-exit v5
    :try_end_39
    .catchall {:try_start_27 .. :try_end_39} :catchall_b5

    #@39
    :try_start_39
    monitor-exit v4

    #@3a
    goto :goto_20

    #@3b
    .line 5248
    :catchall_3b
    move-exception v3

    #@3c
    monitor-exit v4
    :try_end_3d
    .catchall {:try_start_39 .. :try_end_3d} :catchall_3b

    #@3d
    throw v3

    #@3e
    .line 5224
    :cond_3e
    :try_start_3e
    iget-object v3, p0, Landroid/media/AudioService;->mMediaReceiverForCallsList:Ljava/util/ArrayList;

    #@40
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@43
    move-result v3

    #@44
    if-lez v3, :cond_6a

    #@46
    iget-boolean v3, p0, Landroid/media/AudioService;->mIsRinging:Z

    #@48
    if-nez v3, :cond_50

    #@4a
    invoke-virtual {p0}, Landroid/media/AudioService;->getMode()I

    #@4d
    move-result v3

    #@4e
    if-ne v3, v6, :cond_6a

    #@50
    .line 5226
    :cond_50
    iget-object v3, p0, Landroid/media/AudioService;->mMediaReceiverForCallsList:Ljava/util/ArrayList;

    #@52
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@55
    move-result v2

    #@56
    .line 5227
    .local v2, size:I
    const/4 v1, 0x0

    #@57
    .local v1, i:I
    :goto_57
    if-ge v1, v2, :cond_67

    #@59
    .line 5228
    iget-object v3, p0, Landroid/media/AudioService;->mMediaReceiverForCallsList:Ljava/util/ArrayList;

    #@5b
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5e
    move-result-object v0

    #@5f
    check-cast v0, Landroid/content/ComponentName;

    #@61
    .line 5229
    .local v0, c:Landroid/content/ComponentName;
    invoke-direct {p0, p1, v0, p2}, Landroid/media/AudioService;->dispatchMediaKeyEventForCallsList(Landroid/view/KeyEvent;Landroid/content/ComponentName;Z)V

    #@64
    .line 5227
    add-int/lit8 v1, v1, 0x1

    #@66
    goto :goto_57

    #@67
    .line 5231
    .end local v0           #c:Landroid/content/ComponentName;
    :cond_67
    monitor-exit v5
    :try_end_68
    .catchall {:try_start_3e .. :try_end_68} :catchall_b5

    #@68
    :try_start_68
    monitor-exit v4
    :try_end_69
    .catchall {:try_start_68 .. :try_end_69} :catchall_3b

    #@69
    goto :goto_20

    #@6a
    .line 5235
    .end local v1           #i:I
    .end local v2           #size:I
    :cond_6a
    :try_start_6a
    iget-object v3, p0, Landroid/media/AudioService;->mMediaReceiverForCallsList:Ljava/util/ArrayList;

    #@6c
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@6f
    move-result v3

    #@70
    if-lez v3, :cond_a4

    #@72
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@75
    move-result v3

    #@76
    const/16 v6, 0x4f

    #@78
    if-ne v3, v6, :cond_a4

    #@7a
    .line 5237
    iget-object v3, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@7c
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7f
    move-result-object v3

    #@80
    const v6, 0x206001d

    #@83
    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@86
    move-result v3

    #@87
    if-eqz v3, :cond_a4

    #@89
    .line 5238
    iget-object v3, p0, Landroid/media/AudioService;->mMediaReceiverForCallsList:Ljava/util/ArrayList;

    #@8b
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@8e
    move-result v2

    #@8f
    .line 5239
    .restart local v2       #size:I
    const/4 v1, 0x0

    #@90
    .restart local v1       #i:I
    :goto_90
    if-ge v1, v2, :cond_a0

    #@92
    .line 5240
    iget-object v3, p0, Landroid/media/AudioService;->mMediaReceiverForCallsList:Ljava/util/ArrayList;

    #@94
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@97
    move-result-object v0

    #@98
    check-cast v0, Landroid/content/ComponentName;

    #@9a
    .line 5241
    .restart local v0       #c:Landroid/content/ComponentName;
    invoke-direct {p0, p1, v0, p2}, Landroid/media/AudioService;->dispatchMediaKeyEventForCallsList(Landroid/view/KeyEvent;Landroid/content/ComponentName;Z)V

    #@9d
    .line 5239
    add-int/lit8 v1, v1, 0x1

    #@9f
    goto :goto_90

    #@a0
    .line 5243
    .end local v0           #c:Landroid/content/ComponentName;
    :cond_a0
    monitor-exit v5
    :try_end_a1
    .catchall {:try_start_6a .. :try_end_a1} :catchall_b5

    #@a1
    :try_start_a1
    monitor-exit v4
    :try_end_a2
    .catchall {:try_start_a1 .. :try_end_a2} :catchall_3b

    #@a2
    goto/16 :goto_20

    #@a4
    .line 5247
    .end local v1           #i:I
    .end local v2           #size:I
    :cond_a4
    :try_start_a4
    monitor-exit v5
    :try_end_a5
    .catchall {:try_start_a4 .. :try_end_a5} :catchall_b5

    #@a5
    .line 5248
    :try_start_a5
    monitor-exit v4
    :try_end_a6
    .catchall {:try_start_a5 .. :try_end_a6} :catchall_3b

    #@a6
    .line 5250
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@a9
    move-result v3

    #@aa
    invoke-static {v3}, Landroid/media/AudioService;->isValidVoiceInputKeyCode(I)Z

    #@ad
    move-result v3

    #@ae
    if-eqz v3, :cond_b8

    #@b0
    .line 5251
    invoke-direct {p0, p1, p2}, Landroid/media/AudioService;->filterVoiceInputKeyEvent(Landroid/view/KeyEvent;Z)V

    #@b3
    goto/16 :goto_20

    #@b5
    .line 5247
    :catchall_b5
    move-exception v3

    #@b6
    :try_start_b6
    monitor-exit v5
    :try_end_b7
    .catchall {:try_start_b6 .. :try_end_b7} :catchall_b5

    #@b7
    :try_start_b7
    throw v3
    :try_end_b8
    .catchall {:try_start_b7 .. :try_end_b8} :catchall_3b

    #@b8
    .line 5253
    :cond_b8
    invoke-direct {p0, p1, p2}, Landroid/media/AudioService;->dispatchMediaKeyEvent(Landroid/view/KeyEvent;Z)V

    #@bb
    goto/16 :goto_20
.end method

.method private filterVoiceInputKeyEvent(Landroid/view/KeyEvent;Z)V
    .registers 8
    .parameter "keyEvent"
    .parameter "needWakeLock"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 5346
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@4
    move-result v2

    #@5
    const/16 v3, 0x4f

    #@7
    if-ne v2, v3, :cond_5b

    #@9
    .line 5348
    const-string/jumbo v2, "sys.allautotest.run"

    #@c
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@f
    move-result-object v2

    #@10
    const-string/jumbo v3, "true"

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v2

    #@17
    if-nez v2, :cond_29

    #@19
    const-string/jumbo v2, "ril.cdma.inecmmode"

    #@1c
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    const-string/jumbo v3, "true"

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26
    move-result v2

    #@27
    if-eqz v2, :cond_43

    #@29
    .line 5349
    :cond_29
    const-string v2, "AudioService"

    #@2b
    new-instance v3, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string/jumbo v4, "not dispatching All autotest "

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v3

    #@3f
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 5400
    :goto_42
    :pswitch_42
    return-void

    #@43
    .line 5354
    :cond_43
    const-string v2, "audiorecording_state"

    #@45
    invoke-static {v2}, Landroid/media/AudioSystem;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    #@48
    move-result-object v2

    #@49
    const-string/jumbo v3, "off"

    #@4c
    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@4f
    move-result v2

    #@50
    if-nez v2, :cond_5b

    #@52
    .line 5355
    const-string v2, "AudioService"

    #@54
    const-string/jumbo v3, "not dispatching Hook Key during voice recording"

    #@57
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    goto :goto_42

    #@5b
    .line 5360
    :cond_5b
    const/4 v1, 0x1

    #@5c
    .line 5361
    .local v1, voiceButtonAction:I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@5f
    move-result v0

    #@60
    .line 5362
    .local v0, keyAction:I
    iget-object v3, p0, Landroid/media/AudioService;->mVoiceEventLock:Ljava/lang/Object;

    #@62
    monitor-enter v3

    #@63
    .line 5363
    if-nez v0, :cond_8f

    #@65
    .line 5364
    :try_start_65
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@68
    move-result v2

    #@69
    if-nez v2, :cond_7a

    #@6b
    .line 5366
    const/4 v2, 0x1

    #@6c
    iput-boolean v2, p0, Landroid/media/AudioService;->mVoiceButtonDown:Z

    #@6e
    .line 5367
    const/4 v2, 0x0

    #@6f
    iput-boolean v2, p0, Landroid/media/AudioService;->mVoiceButtonHandled:Z

    #@71
    .line 5383
    :cond_71
    :goto_71
    monitor-exit v3
    :try_end_72
    .catchall {:try_start_65 .. :try_end_72} :catchall_a4

    #@72
    .line 5386
    packed-switch v1, :pswitch_data_ac

    #@75
    goto :goto_42

    #@76
    .line 5393
    :pswitch_76
    invoke-direct {p0, p2}, Landroid/media/AudioService;->startVoiceBasedInteractions(Z)V

    #@79
    goto :goto_42

    #@7a
    .line 5368
    :cond_7a
    :try_start_7a
    iget-boolean v2, p0, Landroid/media/AudioService;->mVoiceButtonDown:Z

    #@7c
    if-eqz v2, :cond_71

    #@7e
    iget-boolean v2, p0, Landroid/media/AudioService;->mVoiceButtonHandled:Z

    #@80
    if-nez v2, :cond_71

    #@82
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getFlags()I

    #@85
    move-result v2

    #@86
    and-int/lit16 v2, v2, 0x80

    #@88
    if-eqz v2, :cond_71

    #@8a
    .line 5371
    const/4 v2, 0x1

    #@8b
    iput-boolean v2, p0, Landroid/media/AudioService;->mVoiceButtonHandled:Z

    #@8d
    .line 5372
    const/4 v1, 0x2

    #@8e
    goto :goto_71

    #@8f
    .line 5374
    :cond_8f
    if-ne v0, v4, :cond_71

    #@91
    .line 5375
    iget-boolean v2, p0, Landroid/media/AudioService;->mVoiceButtonDown:Z

    #@93
    if-eqz v2, :cond_71

    #@95
    .line 5377
    const/4 v2, 0x0

    #@96
    iput-boolean v2, p0, Landroid/media/AudioService;->mVoiceButtonDown:Z

    #@98
    .line 5378
    iget-boolean v2, p0, Landroid/media/AudioService;->mVoiceButtonHandled:Z

    #@9a
    if-nez v2, :cond_71

    #@9c
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isCanceled()Z

    #@9f
    move-result v2

    #@a0
    if-nez v2, :cond_71

    #@a2
    .line 5379
    const/4 v1, 0x3

    #@a3
    goto :goto_71

    #@a4
    .line 5383
    :catchall_a4
    move-exception v2

    #@a5
    monitor-exit v3
    :try_end_a6
    .catchall {:try_start_7a .. :try_end_a6} :catchall_a4

    #@a6
    throw v2

    #@a7
    .line 5397
    :pswitch_a7
    invoke-direct {p0, p1, p2}, Landroid/media/AudioService;->sendSimulatedMediaButtonEvent(Landroid/view/KeyEvent;Z)V

    #@aa
    goto :goto_42

    #@ab
    .line 5386
    nop

    #@ac
    :pswitch_data_ac
    .packed-switch 0x1
        :pswitch_42
        :pswitch_76
        :pswitch_a7
    .end packed-switch
.end method

.method private findVolumeDelta(II)I
    .registers 9
    .parameter "direction"
    .parameter "volume"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 1366
    const/4 v0, 0x0

    #@3
    .line 1367
    .local v0, delta:I
    if-ne p1, v5, :cond_26

    #@5
    .line 1368
    const/16 v4, 0x64

    #@7
    if-ne p2, v4, :cond_a

    #@9
    .line 1397
    :cond_9
    :goto_9
    return v3

    #@a
    .line 1372
    :cond_a
    iget-object v3, p0, Landroid/media/AudioService;->mMasterVolumeRamp:[I

    #@c
    aget v0, v3, v5

    #@e
    .line 1375
    iget-object v3, p0, Landroid/media/AudioService;->mMasterVolumeRamp:[I

    #@10
    array-length v3, v3

    #@11
    add-int/lit8 v1, v3, -0x1

    #@13
    .local v1, i:I
    :goto_13
    if-le v1, v5, :cond_21

    #@15
    .line 1376
    iget-object v3, p0, Landroid/media/AudioService;->mMasterVolumeRamp:[I

    #@17
    add-int/lit8 v4, v1, -0x1

    #@19
    aget v3, v3, v4

    #@1b
    if-lt p2, v3, :cond_23

    #@1d
    .line 1377
    iget-object v3, p0, Landroid/media/AudioService;->mMasterVolumeRamp:[I

    #@1f
    aget v0, v3, v1

    #@21
    .end local v1           #i:I
    :cond_21
    :goto_21
    move v3, v0

    #@22
    .line 1397
    goto :goto_9

    #@23
    .line 1375
    .restart local v1       #i:I
    :cond_23
    add-int/lit8 v1, v1, -0x2

    #@25
    goto :goto_13

    #@26
    .line 1381
    .end local v1           #i:I
    :cond_26
    const/4 v4, -0x1

    #@27
    if-ne p1, v4, :cond_21

    #@29
    .line 1382
    if-eqz p2, :cond_9

    #@2b
    .line 1385
    iget-object v3, p0, Landroid/media/AudioService;->mMasterVolumeRamp:[I

    #@2d
    array-length v2, v3

    #@2e
    .line 1387
    .local v2, length:I
    iget-object v3, p0, Landroid/media/AudioService;->mMasterVolumeRamp:[I

    #@30
    add-int/lit8 v4, v2, -0x1

    #@32
    aget v3, v3, v4

    #@34
    neg-int v0, v3

    #@35
    .line 1390
    const/4 v1, 0x2

    #@36
    .restart local v1       #i:I
    :goto_36
    if-ge v1, v2, :cond_21

    #@38
    .line 1391
    iget-object v3, p0, Landroid/media/AudioService;->mMasterVolumeRamp:[I

    #@3a
    aget v3, v3, v1

    #@3c
    if-gt p2, v3, :cond_46

    #@3e
    .line 1392
    iget-object v3, p0, Landroid/media/AudioService;->mMasterVolumeRamp:[I

    #@40
    add-int/lit8 v4, v1, -0x1

    #@42
    aget v3, v3, v4

    #@44
    neg-int v0, v3

    #@45
    .line 1393
    goto :goto_21

    #@46
    .line 1390
    :cond_46
    add-int/lit8 v1, v1, 0x2

    #@48
    goto :goto_36
.end method

.method private getActiveStreamType(I)I
    .registers 8
    .parameter "suggestedStreamType"

    #@0
    .prologue
    const/4 v3, 0x5

    #@1
    const/4 v2, 0x2

    #@2
    const/16 v5, 0x1388

    #@4
    const/4 v0, 0x0

    #@5
    const/4 v1, 0x3

    #@6
    .line 3093
    iget-boolean v4, p0, Landroid/media/AudioService;->mVoiceCapable:Z

    #@8
    if-eqz v4, :cond_c2

    #@a
    .line 3094
    invoke-direct {p0}, Landroid/media/AudioService;->isInCommunication()Z

    #@d
    move-result v4

    #@e
    if-eqz v4, :cond_1a

    #@10
    .line 3095
    invoke-static {v0}, Landroid/media/AudioSystem;->getForceUse(I)I

    #@13
    move-result v2

    #@14
    if-ne v2, v1, :cond_18

    #@16
    .line 3098
    const/4 p1, 0x6

    #@17
    .line 3180
    .end local p1
    :goto_17
    return p1

    #@18
    .restart local p1
    :cond_18
    move p1, v0

    #@19
    .line 3101
    goto :goto_17

    #@1a
    .line 3103
    :cond_1a
    const/high16 v4, -0x8000

    #@1c
    if-ne p1, v4, :cond_5f

    #@1e
    .line 3106
    invoke-direct {p0, v1}, Landroid/media/AudioService;->checkUpdateRemoteStateIfActive(I)Z

    #@21
    move-result v3

    #@22
    if-eqz v3, :cond_2e

    #@24
    .line 3108
    const-string v0, "AudioService"

    #@26
    const-string v1, "getActiveStreamType: Forcing STREAM_REMOTE_MUSIC"

    #@28
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 3109
    const/16 p1, -0xc8

    #@2d
    goto :goto_17

    #@2e
    .line 3110
    :cond_2e
    invoke-static {v1, v5}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    #@31
    move-result v3

    #@32
    if-nez v3, :cond_3c

    #@34
    const/16 v3, 0xb

    #@36
    invoke-static {v3, v5}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    #@39
    move-result v3

    #@3a
    if-eqz v3, :cond_45

    #@3c
    .line 3115
    :cond_3c
    const-string v0, "AudioService"

    #@3e
    const-string v2, "getActiveStreamType: Forcing STREAM_MUSIC stream active"

    #@40
    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    move p1, v1

    #@44
    .line 3116
    goto :goto_17

    #@45
    .line 3117
    :cond_45
    const/16 v3, 0xa

    #@47
    invoke-static {v3, v0}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    #@4a
    move-result v0

    #@4b
    if-eqz v0, :cond_56

    #@4d
    .line 3119
    const-string v0, "AudioService"

    #@4f
    const-string v2, "getActiveStreamType: Forcing STREAM_FM..."

    #@51
    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    move p1, v1

    #@55
    .line 3120
    goto :goto_17

    #@56
    .line 3123
    :cond_56
    const-string v0, "AudioService"

    #@58
    const-string v1, "getActiveStreamType: Forcing STREAM_RING b/c default"

    #@5a
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    move p1, v2

    #@5e
    .line 3124
    goto :goto_17

    #@5f
    .line 3128
    :cond_5f
    if-eq p1, v2, :cond_66

    #@61
    if-eq p1, v3, :cond_66

    #@63
    const/4 v2, 0x4

    #@64
    if-ne p1, v2, :cond_7f

    #@66
    .line 3131
    :cond_66
    const-string v0, "AudioService"

    #@68
    new-instance v1, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const-string v2, "getActiveStreamType: Returning suggested type(for Ringtone Picker) "

    #@6f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v1

    #@73
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@76
    move-result-object v1

    #@77
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v1

    #@7b
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7e
    goto :goto_17

    #@7f
    .line 3136
    :cond_7f
    invoke-static {v1, v0}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    #@82
    move-result v2

    #@83
    if-nez v2, :cond_8d

    #@85
    const/16 v2, 0xb

    #@87
    invoke-static {v2, v0}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    #@8a
    move-result v2

    #@8b
    if-eqz v2, :cond_96

    #@8d
    .line 3139
    :cond_8d
    const-string v0, "AudioService"

    #@8f
    const-string v2, "getActiveStreamType: Forcing STREAM_MUSIC stream active"

    #@91
    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@94
    move p1, v1

    #@95
    .line 3140
    goto :goto_17

    #@96
    .line 3141
    :cond_96
    const/16 v2, 0xa

    #@98
    invoke-static {v2, v0}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    #@9b
    move-result v0

    #@9c
    if-eqz v0, :cond_a8

    #@9e
    .line 3143
    const-string v0, "AudioService"

    #@a0
    const-string v2, "getActiveStreamType: Forcing STREAM_FM..."

    #@a2
    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@a5
    move p1, v1

    #@a6
    .line 3144
    goto/16 :goto_17

    #@a8
    .line 3146
    :cond_a8
    const-string v0, "AudioService"

    #@aa
    new-instance v1, Ljava/lang/StringBuilder;

    #@ac
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@af
    const-string v2, "getActiveStreamType: Returning suggested type "

    #@b1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v1

    #@b5
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v1

    #@b9
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bc
    move-result-object v1

    #@bd
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@c0
    goto/16 :goto_17

    #@c2
    .line 3151
    :cond_c2
    invoke-direct {p0}, Landroid/media/AudioService;->isInCommunication()Z

    #@c5
    move-result v4

    #@c6
    if-eqz v4, :cond_e2

    #@c8
    .line 3152
    invoke-static {v0}, Landroid/media/AudioSystem;->getForceUse(I)I

    #@cb
    move-result v2

    #@cc
    if-ne v2, v1, :cond_d8

    #@ce
    .line 3154
    const-string v0, "AudioService"

    #@d0
    const-string v1, "getActiveStreamType: Forcing STREAM_BLUETOOTH_SCO"

    #@d2
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@d5
    .line 3155
    const/4 p1, 0x6

    #@d6
    goto/16 :goto_17

    #@d8
    .line 3157
    :cond_d8
    const-string v1, "AudioService"

    #@da
    const-string v2, "getActiveStreamType: Forcing STREAM_VOICE_CALL"

    #@dc
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@df
    move p1, v0

    #@e0
    .line 3158
    goto/16 :goto_17

    #@e2
    .line 3160
    :cond_e2
    invoke-static {v3, v5}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    #@e5
    move-result v0

    #@e6
    if-nez v0, :cond_ee

    #@e8
    invoke-static {v2, v5}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    #@eb
    move-result v0

    #@ec
    if-eqz v0, :cond_f8

    #@ee
    .line 3164
    :cond_ee
    const-string v0, "AudioService"

    #@f0
    const-string v1, "getActiveStreamType: Forcing STREAM_NOTIFICATION"

    #@f2
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@f5
    move p1, v3

    #@f6
    .line 3165
    goto/16 :goto_17

    #@f8
    .line 3166
    :cond_f8
    const/high16 v0, -0x8000

    #@fa
    if-ne p1, v0, :cond_117

    #@fc
    .line 3167
    invoke-direct {p0, v1}, Landroid/media/AudioService;->checkUpdateRemoteStateIfActive(I)Z

    #@ff
    move-result v0

    #@100
    if-eqz v0, :cond_10d

    #@102
    .line 3170
    const-string v0, "AudioService"

    #@104
    const-string v1, "getActiveStreamType: Forcing STREAM_REMOTE_MUSIC"

    #@106
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@109
    .line 3171
    const/16 p1, -0xc8

    #@10b
    goto/16 :goto_17

    #@10d
    .line 3174
    :cond_10d
    const-string v0, "AudioService"

    #@10f
    const-string v2, "getActiveStreamType: using STREAM_MUSIC as default"

    #@111
    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@114
    move p1, v1

    #@115
    .line 3175
    goto/16 :goto_17

    #@117
    .line 3178
    :cond_117
    const-string v0, "AudioService"

    #@119
    new-instance v1, Ljava/lang/StringBuilder;

    #@11b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11e
    const-string v2, "getActiveStreamType: Returning suggested type "

    #@120
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@123
    move-result-object v1

    #@124
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@127
    move-result-object v1

    #@128
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12b
    move-result-object v1

    #@12c
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@12f
    goto/16 :goto_17
.end method

.method private getBluetoothHeadset()Z
    .registers 10

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2708
    const/4 v8, 0x0

    #@2
    .line 2709
    .local v8, result:Z
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@5
    move-result-object v7

    #@6
    .line 2710
    .local v7, adapter:Landroid/bluetooth/BluetoothAdapter;
    if-eqz v7, :cond_11

    #@8
    .line 2711
    iget-object v0, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@a
    iget-object v1, p0, Landroid/media/AudioService;->mBluetoothProfileServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    #@c
    const/4 v3, 0x1

    #@d
    invoke-virtual {v7, v0, v1, v3}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    #@10
    move-result v8

    #@11
    .line 2718
    :cond_11
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@13
    const/16 v1, 0xb

    #@15
    const/4 v5, 0x0

    #@16
    if-eqz v8, :cond_20

    #@18
    const/16 v6, 0xbb8

    #@1a
    :goto_1a
    move v3, v2

    #@1b
    move v4, v2

    #@1c
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@1f
    .line 2720
    return v8

    #@20
    :cond_20
    move v6, v2

    #@21
    .line 2718
    goto :goto_1a
.end method

.method private getDeviceForStream(I)I
    .registers 4
    .parameter "stream"

    #@0
    .prologue
    .line 3240
    invoke-static {p1}, Landroid/media/AudioSystem;->getDevicesForStream(I)I

    #@3
    move-result v0

    #@4
    .line 3241
    .local v0, device:I
    add-int/lit8 v1, v0, -0x1

    #@6
    and-int/2addr v1, v0

    #@7
    if-eqz v1, :cond_e

    #@9
    .line 3247
    and-int/lit8 v1, v0, 0x2

    #@b
    if-eqz v1, :cond_f

    #@d
    .line 3248
    const/4 v0, 0x2

    #@e
    .line 3257
    :cond_e
    :goto_e
    return v0

    #@f
    .line 3249
    :cond_f
    and-int/lit8 v1, v0, 0x4

    #@11
    if-eqz v1, :cond_15

    #@13
    .line 3250
    const/4 v0, 0x4

    #@14
    goto :goto_e

    #@15
    .line 3251
    :cond_15
    and-int/lit8 v1, v0, 0x8

    #@17
    if-eqz v1, :cond_1c

    #@19
    .line 3252
    const/16 v0, 0x8

    #@1b
    goto :goto_e

    #@1c
    .line 3254
    :cond_1c
    and-int/lit16 v0, v0, 0x380

    #@1e
    goto :goto_e
.end method

.method private getScoClient(Landroid/os/IBinder;Z)Landroid/media/AudioService$ScoClient;
    .registers 9
    .parameter "cb"
    .parameter "create"

    #@0
    .prologue
    .line 2672
    iget-object v5, p0, Landroid/media/AudioService;->mScoClients:Ljava/util/ArrayList;

    #@2
    monitor-enter v5

    #@3
    .line 2673
    const/4 v0, 0x0

    #@4
    .line 2674
    .local v0, client:Landroid/media/AudioService$ScoClient;
    :try_start_4
    iget-object v4, p0, Landroid/media/AudioService;->mScoClients:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I
    :try_end_9
    .catchall {:try_start_4 .. :try_end_9} :catchall_32

    #@9
    move-result v3

    #@a
    .line 2675
    .local v3, size:I
    const/4 v2, 0x0

    #@b
    .local v2, i:I
    move-object v1, v0

    #@c
    .end local v0           #client:Landroid/media/AudioService$ScoClient;
    .local v1, client:Landroid/media/AudioService$ScoClient;
    :goto_c
    if-ge v2, v3, :cond_23

    #@e
    .line 2676
    :try_start_e
    iget-object v4, p0, Landroid/media/AudioService;->mScoClients:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Landroid/media/AudioService$ScoClient;
    :try_end_16
    .catchall {:try_start_e .. :try_end_16} :catchall_35

    #@16
    .line 2677
    .end local v1           #client:Landroid/media/AudioService$ScoClient;
    .restart local v0       #client:Landroid/media/AudioService$ScoClient;
    :try_start_16
    invoke-virtual {v0}, Landroid/media/AudioService$ScoClient;->getBinder()Landroid/os/IBinder;

    #@19
    move-result-object v4

    #@1a
    if-ne v4, p1, :cond_1f

    #@1c
    .line 2678
    monitor-exit v5
    :try_end_1d
    .catchall {:try_start_16 .. :try_end_1d} :catchall_32

    #@1d
    move-object v1, v0

    #@1e
    .line 2684
    .end local v0           #client:Landroid/media/AudioService$ScoClient;
    .restart local v1       #client:Landroid/media/AudioService$ScoClient;
    :goto_1e
    return-object v1

    #@1f
    .line 2675
    .end local v1           #client:Landroid/media/AudioService$ScoClient;
    .restart local v0       #client:Landroid/media/AudioService$ScoClient;
    :cond_1f
    add-int/lit8 v2, v2, 0x1

    #@21
    move-object v1, v0

    #@22
    .end local v0           #client:Landroid/media/AudioService$ScoClient;
    .restart local v1       #client:Landroid/media/AudioService$ScoClient;
    goto :goto_c

    #@23
    .line 2680
    :cond_23
    if-eqz p2, :cond_38

    #@25
    .line 2681
    :try_start_25
    new-instance v0, Landroid/media/AudioService$ScoClient;

    #@27
    invoke-direct {v0, p0, p1}, Landroid/media/AudioService$ScoClient;-><init>(Landroid/media/AudioService;Landroid/os/IBinder;)V
    :try_end_2a
    .catchall {:try_start_25 .. :try_end_2a} :catchall_35

    #@2a
    .line 2682
    .end local v1           #client:Landroid/media/AudioService$ScoClient;
    .restart local v0       #client:Landroid/media/AudioService$ScoClient;
    :try_start_2a
    iget-object v4, p0, Landroid/media/AudioService;->mScoClients:Ljava/util/ArrayList;

    #@2c
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2f
    .line 2684
    :goto_2f
    monitor-exit v5

    #@30
    move-object v1, v0

    #@31
    .end local v0           #client:Landroid/media/AudioService$ScoClient;
    .restart local v1       #client:Landroid/media/AudioService$ScoClient;
    goto :goto_1e

    #@32
    .line 2685
    .end local v1           #client:Landroid/media/AudioService$ScoClient;
    .end local v2           #i:I
    .end local v3           #size:I
    .restart local v0       #client:Landroid/media/AudioService$ScoClient;
    :catchall_32
    move-exception v4

    #@33
    :goto_33
    monitor-exit v5
    :try_end_34
    .catchall {:try_start_2a .. :try_end_34} :catchall_32

    #@34
    throw v4

    #@35
    .end local v0           #client:Landroid/media/AudioService$ScoClient;
    .restart local v1       #client:Landroid/media/AudioService$ScoClient;
    .restart local v2       #i:I
    .restart local v3       #size:I
    :catchall_35
    move-exception v4

    #@36
    move-object v0, v1

    #@37
    .end local v1           #client:Landroid/media/AudioService$ScoClient;
    .restart local v0       #client:Landroid/media/AudioService$ScoClient;
    goto :goto_33

    #@38
    .end local v0           #client:Landroid/media/AudioService$ScoClient;
    .restart local v1       #client:Landroid/media/AudioService$ScoClient;
    :cond_38
    move-object v0, v1

    #@39
    .end local v1           #client:Landroid/media/AudioService$ScoClient;
    .restart local v0       #client:Landroid/media/AudioService$ScoClient;
    goto :goto_2f
.end method

.method public static getValueForVibrateSetting(III)I
    .registers 5
    .parameter "existingValue"
    .parameter "vibrateType"
    .parameter "vibrateSetting"

    #@0
    .prologue
    .line 1799
    const/4 v0, 0x3

    #@1
    mul-int/lit8 v1, p1, 0x2

    #@3
    shl-int/2addr v0, v1

    #@4
    xor-int/lit8 v0, v0, -0x1

    #@6
    and-int/2addr p0, v0

    #@7
    .line 1802
    and-int/lit8 v0, p2, 0x3

    #@9
    mul-int/lit8 v1, p1, 0x2

    #@b
    shl-int/2addr v0, v1

    #@c
    or-int/2addr p0, v0

    #@d
    .line 1804
    return p0
.end method

.method private handleConfigurationChanged(Landroid/content/Context;)V
    .registers 16
    .parameter "context"

    #@0
    .prologue
    .line 6667
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@7
    move-result-object v8

    #@8
    .line 6668
    .local v8, config:Landroid/content/res/Configuration;
    iget-boolean v0, p0, Landroid/media/AudioService;->mMonitorOrientation:Z

    #@a
    if-eqz v0, :cond_17

    #@c
    .line 6669
    iget v10, v8, Landroid/content/res/Configuration;->orientation:I

    #@e
    .line 6670
    .local v10, newOrientation:I
    iget v0, p0, Landroid/media/AudioService;->mDeviceOrientation:I

    #@10
    if-eq v10, v0, :cond_17

    #@12
    .line 6671
    iput v10, p0, Landroid/media/AudioService;->mDeviceOrientation:I

    #@14
    .line 6672
    invoke-direct {p0}, Landroid/media/AudioService;->setOrientationForAudioSystem()V

    #@17
    .line 6675
    .end local v10           #newOrientation:I
    :cond_17
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@19
    const/16 v1, 0x1a

    #@1b
    const/4 v2, 0x0

    #@1c
    const/4 v3, 0x0

    #@1d
    const/4 v4, 0x0

    #@1e
    const/4 v5, 0x0

    #@1f
    const/4 v6, 0x0

    #@20
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@23
    .line 6683
    iget-object v0, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@25
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@28
    move-result-object v0

    #@29
    const v1, 0x111004b

    #@2c
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@2f
    move-result v7

    #@30
    .line 6685
    .local v7, cameraSoundForced:Z
    iget-object v12, p0, Landroid/media/AudioService;->mSettingsLock:Ljava/lang/Object;

    #@32
    monitor-enter v12
    :try_end_33
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_33} :catch_9d

    #@33
    .line 6686
    :try_start_33
    iget-object v13, p0, Landroid/media/AudioService;->mCameraSoundForced:Ljava/lang/Boolean;

    #@35
    monitor-enter v13
    :try_end_36
    .catchall {:try_start_33 .. :try_end_36} :catchall_9a

    #@36
    .line 6687
    :try_start_36
    iget-object v0, p0, Landroid/media/AudioService;->mCameraSoundForced:Ljava/lang/Boolean;

    #@38
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    #@3b
    move-result v0

    #@3c
    if-eq v7, v0, :cond_7b

    #@3e
    .line 6688
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@41
    move-result-object v0

    #@42
    iput-object v0, p0, Landroid/media/AudioService;->mCameraSoundForced:Ljava/lang/Boolean;

    #@44
    .line 6690
    iget-object v0, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@46
    const/4 v1, 0x7

    #@47
    aget-object v11, v0, v1

    #@49
    .line 6691
    .local v11, s:Landroid/media/AudioService$VolumeStreamState;
    if-eqz v7, :cond_7e

    #@4b
    .line 6692
    invoke-virtual {v11}, Landroid/media/AudioService$VolumeStreamState;->setAllIndexesToMax()V

    #@4e
    .line 6693
    iget v0, p0, Landroid/media/AudioService;->mRingerModeAffectedStreams:I

    #@50
    and-int/lit16 v0, v0, -0x81

    #@52
    iput v0, p0, Landroid/media/AudioService;->mRingerModeAffectedStreams:I

    #@54
    .line 6704
    :goto_54
    invoke-virtual {p0}, Landroid/media/AudioService;->getRingerMode()I

    #@57
    move-result v0

    #@58
    const/4 v1, 0x0

    #@59
    invoke-direct {p0, v0, v1}, Landroid/media/AudioService;->setRingerModeInt(IZ)V

    #@5c
    .line 6706
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@5e
    const/16 v1, 0x9

    #@60
    const/4 v2, 0x2

    #@61
    const/4 v3, 0x4

    #@62
    if-eqz v7, :cond_b7

    #@64
    const/16 v4, 0xb

    #@66
    :goto_66
    const/4 v5, 0x0

    #@67
    const/4 v6, 0x0

    #@68
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@6b
    .line 6715
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@6d
    const/16 v1, 0xe

    #@6f
    const/4 v2, 0x2

    #@70
    const/4 v3, 0x0

    #@71
    const/4 v4, 0x0

    #@72
    iget-object v5, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@74
    const/4 v6, 0x7

    #@75
    aget-object v5, v5, v6

    #@77
    const/4 v6, 0x0

    #@78
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@7b
    .line 6722
    .end local v11           #s:Landroid/media/AudioService$VolumeStreamState;
    :cond_7b
    monitor-exit v13
    :try_end_7c
    .catchall {:try_start_36 .. :try_end_7c} :catchall_97

    #@7c
    .line 6723
    :try_start_7c
    monitor-exit v12
    :try_end_7d
    .catchall {:try_start_7c .. :try_end_7d} :catchall_9a

    #@7d
    .line 6727
    .end local v7           #cameraSoundForced:Z
    .end local v8           #config:Landroid/content/res/Configuration;
    :goto_7d
    return-void

    #@7e
    .line 6696
    .restart local v7       #cameraSoundForced:Z
    .restart local v8       #config:Landroid/content/res/Configuration;
    .restart local v11       #s:Landroid/media/AudioService$VolumeStreamState;
    :cond_7e
    :try_start_7e
    iget-object v0, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@80
    const/4 v1, 0x1

    #@81
    aget-object v0, v0, v1

    #@83
    const/4 v1, 0x0

    #@84
    invoke-virtual {v11, v0, v1}, Landroid/media/AudioService$VolumeStreamState;->setAllIndexes(Landroid/media/AudioService$VolumeStreamState;Z)V

    #@87
    .line 6698
    iget-object v0, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@89
    const/4 v1, 0x1

    #@8a
    aget-object v0, v0, v1

    #@8c
    const/4 v1, 0x1

    #@8d
    invoke-virtual {v11, v0, v1}, Landroid/media/AudioService$VolumeStreamState;->setAllIndexes(Landroid/media/AudioService$VolumeStreamState;Z)V

    #@90
    .line 6700
    iget v0, p0, Landroid/media/AudioService;->mRingerModeAffectedStreams:I

    #@92
    or-int/lit16 v0, v0, 0x80

    #@94
    iput v0, p0, Landroid/media/AudioService;->mRingerModeAffectedStreams:I

    #@96
    goto :goto_54

    #@97
    .line 6722
    .end local v11           #s:Landroid/media/AudioService$VolumeStreamState;
    :catchall_97
    move-exception v0

    #@98
    monitor-exit v13
    :try_end_99
    .catchall {:try_start_7e .. :try_end_99} :catchall_97

    #@99
    :try_start_99
    throw v0

    #@9a
    .line 6723
    :catchall_9a
    move-exception v0

    #@9b
    monitor-exit v12
    :try_end_9c
    .catchall {:try_start_99 .. :try_end_9c} :catchall_9a

    #@9c
    :try_start_9c
    throw v0
    :try_end_9d
    .catch Ljava/lang/Exception; {:try_start_9c .. :try_end_9d} :catch_9d

    #@9d
    .line 6724
    .end local v7           #cameraSoundForced:Z
    .end local v8           #config:Landroid/content/res/Configuration;
    :catch_9d
    move-exception v9

    #@9e
    .line 6725
    .local v9, e:Ljava/lang/Exception;
    const-string v0, "AudioService"

    #@a0
    new-instance v1, Ljava/lang/StringBuilder;

    #@a2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a5
    const-string v2, "Error retrieving device orientation: "

    #@a7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v1

    #@ab
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v1

    #@af
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b2
    move-result-object v1

    #@b3
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b6
    goto :goto_7d

    #@b7
    .line 6706
    .end local v9           #e:Ljava/lang/Exception;
    .restart local v7       #cameraSoundForced:Z
    .restart local v8       #config:Landroid/content/res/Configuration;
    .restart local v11       #s:Landroid/media/AudioService$VolumeStreamState;
    :cond_b7
    const/4 v4, 0x0

    #@b8
    goto :goto_66
.end method

.method private handleDeviceConnection(ZILjava/lang/String;)Z
    .registers 10
    .parameter "connected"
    .parameter "device"
    .parameter "params"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 4343
    iget-object v4, p0, Landroid/media/AudioService;->mConnectedDevices:Ljava/util/HashMap;

    #@4
    monitor-enter v4

    #@5
    .line 4344
    :try_start_5
    iget-object v1, p0, Landroid/media/AudioService;->mConnectedDevices:Ljava/util/HashMap;

    #@7
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a
    move-result-object v5

    #@b
    invoke-virtual {v1, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_4a

    #@11
    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    #@14
    move-result v1

    #@15
    if-nez v1, :cond_29

    #@17
    iget-object v1, p0, Landroid/media/AudioService;->mConnectedDevices:Ljava/util/HashMap;

    #@19
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1c
    move-result-object v5

    #@1d
    invoke-virtual {v1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@20
    move-result-object v1

    #@21
    check-cast v1, Ljava/lang/String;

    #@23
    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26
    move-result v1

    #@27
    if-eqz v1, :cond_4a

    #@29
    :cond_29
    move v0, v2

    #@2a
    .line 4347
    .local v0, isConnected:Z
    :goto_2a
    if-eqz v0, :cond_4c

    #@2c
    if-nez p1, :cond_4c

    #@2e
    .line 4348
    const/4 v3, 0x0

    #@2f
    iget-object v1, p0, Landroid/media/AudioService;->mConnectedDevices:Ljava/util/HashMap;

    #@31
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@34
    move-result-object v5

    #@35
    invoke-virtual {v1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@38
    move-result-object v1

    #@39
    check-cast v1, Ljava/lang/String;

    #@3b
    invoke-static {p2, v3, v1}, Landroid/media/AudioSystem;->setDeviceConnectionState(IILjava/lang/String;)I

    #@3e
    .line 4351
    iget-object v1, p0, Landroid/media/AudioService;->mConnectedDevices:Ljava/util/HashMap;

    #@40
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@43
    move-result-object v3

    #@44
    invoke-virtual {v1, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@47
    .line 4352
    monitor-exit v4

    #@48
    move v1, v2

    #@49
    .line 4361
    :goto_49
    return v1

    #@4a
    .end local v0           #isConnected:Z
    :cond_4a
    move v0, v3

    #@4b
    .line 4344
    goto :goto_2a

    #@4c
    .line 4353
    .restart local v0       #isConnected:Z
    :cond_4c
    if-nez v0, :cond_61

    #@4e
    if-eqz p1, :cond_61

    #@50
    .line 4354
    const/4 v1, 0x1

    #@51
    invoke-static {p2, v1, p3}, Landroid/media/AudioSystem;->setDeviceConnectionState(IILjava/lang/String;)I

    #@54
    .line 4357
    iget-object v1, p0, Landroid/media/AudioService;->mConnectedDevices:Ljava/util/HashMap;

    #@56
    new-instance v3, Ljava/lang/Integer;

    #@58
    invoke-direct {v3, p2}, Ljava/lang/Integer;-><init>(I)V

    #@5b
    invoke-virtual {v1, v3, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5e
    .line 4358
    monitor-exit v4

    #@5f
    move v1, v2

    #@60
    goto :goto_49

    #@61
    .line 4360
    :cond_61
    monitor-exit v4

    #@62
    move v1, v3

    #@63
    .line 4361
    goto :goto_49

    #@64
    .line 4360
    .end local v0           #isConnected:Z
    :catchall_64
    move-exception v1

    #@65
    monitor-exit v4
    :try_end_66
    .catchall {:try_start_5 .. :try_end_66} :catchall_64

    #@66
    throw v1
.end method

.method private handleFocusForCalls(IILandroid/os/IBinder;)V
    .registers 11
    .parameter "oldMode"
    .parameter "newMode"
    .parameter "cb"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v1, 0x2

    #@2
    .line 2105
    const/4 v0, 0x1

    #@3
    if-ne p2, v0, :cond_11

    #@5
    .line 2112
    const-string v5, "AudioFocus_For_Phone_Ring_And_Calls"

    #@7
    const-string/jumbo v6, "system"

    #@a
    move-object v0, p0

    #@b
    move v2, v1

    #@c
    move-object v3, p3

    #@d
    invoke-virtual/range {v0 .. v6}, Landroid/media/AudioService;->requestAudioFocus(IILandroid/os/IBinder;Landroid/media/IAudioFocusDispatcher;Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 2136
    :cond_10
    :goto_10
    return-void

    #@11
    .line 2121
    :cond_11
    if-eq p2, v1, :cond_16

    #@13
    const/4 v0, 0x3

    #@14
    if-ne p2, v0, :cond_22

    #@16
    .line 2125
    :cond_16
    const-string v5, "AudioFocus_For_Phone_Ring_And_Calls"

    #@18
    const-string/jumbo v6, "system"

    #@1b
    move-object v0, p0

    #@1c
    move v2, v1

    #@1d
    move-object v3, p3

    #@1e
    invoke-virtual/range {v0 .. v6}, Landroid/media/AudioService;->requestAudioFocus(IILandroid/os/IBinder;Landroid/media/IAudioFocusDispatcher;Ljava/lang/String;Ljava/lang/String;)I

    #@21
    goto :goto_10

    #@22
    .line 2132
    :cond_22
    if-nez p2, :cond_10

    #@24
    .line 2134
    const-string v0, "AudioFocus_For_Phone_Ring_And_Calls"

    #@26
    invoke-virtual {p0, v4, v0}, Landroid/media/AudioService;->abandonAudioFocus(Landroid/media/IAudioFocusDispatcher;Ljava/lang/String;)I

    #@29
    goto :goto_10
.end method

.method private hasScheduledA2dpDockTimeout()Z
    .registers 3

    #@0
    .prologue
    .line 4280
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@2
    const/4 v1, 0x7

    #@3
    invoke-virtual {v0, v1}, Landroid/media/AudioService$AudioHandler;->hasMessages(I)Z

    #@6
    move-result v0

    #@7
    return v0
.end method

.method private isCurrentRcController(Landroid/app/PendingIntent;)Z
    .registers 3
    .parameter "pi"

    #@0
    .prologue
    .line 5862
    iget-object v0, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@2
    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_1a

    #@8
    iget-object v0, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@a
    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Landroid/media/AudioService$RemoteControlStackEntry;

    #@10
    iget-object v0, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mMediaIntent:Landroid/app/PendingIntent;

    #@12
    invoke-virtual {v0, p1}, Landroid/app/PendingIntent;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_1a

    #@18
    .line 5863
    const/4 v0, 0x1

    #@19
    .line 5865
    :goto_19
    return v0

    #@1a
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_19
.end method

.method private isInCommunication()Z
    .registers 6

    #@0
    .prologue
    .line 3079
    const/4 v1, 0x0

    #@1
    .line 3081
    .local v1, isOffhook:Z
    iget-boolean v3, p0, Landroid/media/AudioService;->mVoiceCapable:Z

    #@3
    if-eqz v3, :cond_16

    #@5
    .line 3083
    :try_start_5
    const-string/jumbo v3, "phone"

    #@8
    invoke-static {v3}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@b
    move-result-object v3

    #@c
    invoke-static {v3}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    #@f
    move-result-object v2

    #@10
    .line 3084
    .local v2, phone:Lcom/android/internal/telephony/ITelephony;
    if-eqz v2, :cond_16

    #@12
    invoke-interface {v2}, Lcom/android/internal/telephony/ITelephony;->isOffhook()Z
    :try_end_15
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_15} :catch_21

    #@15
    move-result v1

    #@16
    .line 3089
    .end local v2           #phone:Lcom/android/internal/telephony/ITelephony;
    :cond_16
    :goto_16
    if-nez v1, :cond_1f

    #@18
    invoke-virtual {p0}, Landroid/media/AudioService;->getMode()I

    #@1b
    move-result v3

    #@1c
    const/4 v4, 0x3

    #@1d
    if-ne v3, v4, :cond_2a

    #@1f
    :cond_1f
    const/4 v3, 0x1

    #@20
    :goto_20
    return v3

    #@21
    .line 3085
    :catch_21
    move-exception v0

    #@22
    .line 3086
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "AudioService"

    #@24
    const-string v4, "Couldn\'t connect to phone service"

    #@26
    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@29
    goto :goto_16

    #@2a
    .line 3089
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_2a
    const/4 v3, 0x0

    #@2b
    goto :goto_20
.end method

.method private static isPlaystateActive(I)Z
    .registers 2
    .parameter "playState"

    #@0
    .prologue
    .line 6513
    packed-switch p0, :pswitch_data_8

    #@3
    .line 6522
    const/4 v0, 0x0

    #@4
    :goto_4
    return v0

    #@5
    .line 6520
    :pswitch_5
    const/4 v0, 0x1

    #@6
    goto :goto_4

    #@7
    .line 6513
    nop

    #@8
    :pswitch_data_8
    .packed-switch 0x3
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method private isSafeMediaVolumeDevices()Z
    .registers 7

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 2446
    const/4 v0, 0x0

    #@2
    .line 2447
    .local v0, device:I
    const/16 v1, 0xc

    #@4
    .line 2448
    .local v1, devices:I
    const/4 v2, 0x0

    #@5
    .line 2449
    .local v2, i:I
    :goto_5
    if-eqz v1, :cond_20

    #@7
    .line 2450
    shl-int v4, v3, v2

    #@9
    and-int v0, v1, v4

    #@b
    .line 2451
    if-eqz v0, :cond_1d

    #@d
    .line 2452
    iget-object v4, p0, Landroid/media/AudioService;->mConnectedDevices:Ljava/util/HashMap;

    #@f
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@12
    move-result-object v5

    #@13
    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@16
    move-result v4

    #@17
    if-eqz v4, :cond_1a

    #@19
    .line 2459
    :goto_19
    return v3

    #@1a
    .line 2455
    :cond_1a
    xor-int/lit8 v4, v0, -0x1

    #@1c
    and-int/2addr v1, v4

    #@1d
    .line 2457
    :cond_1d
    add-int/lit8 v2, v2, 0x1

    #@1f
    goto :goto_5

    #@20
    .line 2459
    :cond_20
    const/4 v3, 0x0

    #@21
    goto :goto_19
.end method

.method private isStreamMutedByRingerMode(I)Z
    .registers 5
    .parameter "streamType"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 3053
    iget v1, p0, Landroid/media/AudioService;->mRingerModeMutedStreams:I

    #@3
    shl-int v2, v0, p1

    #@5
    and-int/2addr v1, v2

    #@6
    if-eqz v1, :cond_9

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method private static isValidMediaKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 3
    .parameter "keyEvent"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 5414
    if-nez p0, :cond_4

    #@3
    .line 5436
    :goto_3
    return v1

    #@4
    .line 5417
    :cond_4
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    #@7
    move-result v0

    #@8
    .line 5418
    .local v0, keyCode:I
    sparse-switch v0, :sswitch_data_e

    #@b
    goto :goto_3

    #@c
    .line 5436
    :sswitch_c
    const/4 v1, 0x1

    #@d
    goto :goto_3

    #@e
    .line 5418
    :sswitch_data_e
    .sparse-switch
        0x4f -> :sswitch_c
        0x55 -> :sswitch_c
        0x56 -> :sswitch_c
        0x57 -> :sswitch_c
        0x58 -> :sswitch_c
        0x59 -> :sswitch_c
        0x5a -> :sswitch_c
        0x5b -> :sswitch_c
        0x7e -> :sswitch_c
        0x7f -> :sswitch_c
        0x80 -> :sswitch_c
        0x81 -> :sswitch_c
        0x82 -> :sswitch_c
    .end sparse-switch
.end method

.method private static isValidVoiceInputKeyCode(I)Z
    .registers 2
    .parameter "keyCode"

    #@0
    .prologue
    .line 5446
    const/16 v0, 0x4f

    #@2
    if-ne p0, v0, :cond_6

    #@4
    .line 5447
    const/4 v0, 0x1

    #@5
    .line 5449
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method private makeA2dpDeviceAvailable(Ljava/lang/String;)V
    .registers 5
    .parameter "address"

    #@0
    .prologue
    const/16 v2, 0x80

    #@2
    const/4 v0, 0x1

    #@3
    .line 4234
    invoke-virtual {p0, v0}, Landroid/media/AudioService;->setBluetoothA2dpOnInt(Z)V

    #@6
    .line 4235
    invoke-static {v2, v0, p1}, Landroid/media/AudioSystem;->setDeviceConnectionState(IILjava/lang/String;)I

    #@9
    .line 4239
    const-string v0, "A2dpSuspended=false"

    #@b
    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    #@e
    .line 4240
    iget-object v0, p0, Landroid/media/AudioService;->mConnectedDevices:Ljava/util/HashMap;

    #@10
    new-instance v1, Ljava/lang/Integer;

    #@12
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@15
    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@18
    .line 4242
    return-void
.end method

.method private makeA2dpDeviceUnavailableLater(Ljava/lang/String;)V
    .registers 6
    .parameter "address"

    #@0
    .prologue
    .line 4264
    const-string v1, "A2dpSuspended=true"

    #@2
    invoke-static {v1}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    #@5
    .line 4266
    iget-object v1, p0, Landroid/media/AudioService;->mConnectedDevices:Ljava/util/HashMap;

    #@7
    const/16 v2, 0x80

    #@9
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@10
    .line 4268
    iget-object v1, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@12
    const/4 v2, 0x7

    #@13
    invoke-virtual {v1, v2, p1}, Landroid/media/AudioService$AudioHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@16
    move-result-object v0

    #@17
    .line 4269
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@19
    const-wide/16 v2, 0x1f40

    #@1b
    invoke-virtual {v1, v0, v2, v3}, Landroid/media/AudioService$AudioHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@1e
    .line 4271
    return-void
.end method

.method private makeA2dpDeviceUnavailableNow(Ljava/lang/String;)V
    .registers 4
    .parameter "address"

    #@0
    .prologue
    const/16 v1, 0x80

    #@2
    .line 4254
    const/4 v0, 0x0

    #@3
    invoke-static {v1, v0, p1}, Landroid/media/AudioSystem;->setDeviceConnectionState(IILjava/lang/String;)I

    #@6
    .line 4257
    iget-object v0, p0, Landroid/media/AudioService;->mConnectedDevices:Ljava/util/HashMap;

    #@8
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    .line 4258
    return-void
.end method

.method private notifyTopOfAudioFocusStack()V
    .registers 5

    #@0
    .prologue
    .line 4880
    iget-object v1, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@2
    invoke-virtual {v1}, Ljava/util/Stack;->empty()Z

    #@5
    move-result v1

    #@6
    if-nez v1, :cond_32

    #@8
    iget-object v1, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@a
    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    #@d
    move-result-object v1

    #@e
    check-cast v1, Landroid/media/AudioService$FocusStackEntry;

    #@10
    iget-object v1, v1, Landroid/media/AudioService$FocusStackEntry;->mFocusDispatcher:Landroid/media/IAudioFocusDispatcher;

    #@12
    if-eqz v1, :cond_32

    #@14
    .line 4881
    invoke-direct {p0}, Landroid/media/AudioService;->canReassignAudioFocus()Z

    #@17
    move-result v1

    #@18
    if-eqz v1, :cond_32

    #@1a
    .line 4883
    :try_start_1a
    iget-object v1, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@1c
    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    #@1f
    move-result-object v1

    #@20
    check-cast v1, Landroid/media/AudioService$FocusStackEntry;

    #@22
    iget-object v2, v1, Landroid/media/AudioService$FocusStackEntry;->mFocusDispatcher:Landroid/media/IAudioFocusDispatcher;

    #@24
    const/4 v3, 0x1

    #@25
    iget-object v1, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@27
    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    #@2a
    move-result-object v1

    #@2b
    check-cast v1, Landroid/media/AudioService$FocusStackEntry;

    #@2d
    iget-object v1, v1, Landroid/media/AudioService$FocusStackEntry;->mClientId:Ljava/lang/String;

    #@2f
    invoke-interface {v2, v3, v1}, Landroid/media/IAudioFocusDispatcher;->dispatchAudioFocusChange(ILjava/lang/String;)V
    :try_end_32
    .catch Landroid/os/RemoteException; {:try_start_1a .. :try_end_32} :catch_33

    #@32
    .line 4891
    :cond_32
    :goto_32
    return-void

    #@33
    .line 4885
    :catch_33
    move-exception v0

    #@34
    .line 4886
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "AudioService"

    #@36
    new-instance v2, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v3, "Failure to signal gain of audio control focus due to "

    #@3d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v2

    #@41
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v2

    #@45
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v2

    #@49
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 4887
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@4f
    goto :goto_32
.end method

.method private onCheckMusicActive()V
    .registers 12

    #@0
    .prologue
    const v10, 0xea60

    #@3
    .line 2912
    iget-object v9, p0, Landroid/media/AudioService;->mSafeMediaVolumeState:Ljava/lang/Integer;

    #@5
    monitor-enter v9

    #@6
    .line 2913
    :try_start_6
    iget-object v0, p0, Landroid/media/AudioService;->mSafeMediaVolumeState:Ljava/lang/Integer;

    #@8
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@b
    move-result v0

    #@c
    const/4 v1, 0x2

    #@d
    if-ne v0, v1, :cond_54

    #@f
    .line 2914
    const/4 v0, 0x3

    #@10
    invoke-direct {p0, v0}, Landroid/media/AudioService;->getDeviceForStream(I)I

    #@13
    move-result v7

    #@14
    .line 2916
    .local v7, device:I
    and-int/lit8 v0, v7, 0xc

    #@16
    if-eqz v0, :cond_54

    #@18
    .line 2917
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@1a
    const/16 v1, 0x18

    #@1c
    const/4 v2, 0x0

    #@1d
    const/4 v3, 0x0

    #@1e
    const/4 v4, 0x0

    #@1f
    const/4 v5, 0x0

    #@20
    const v6, 0xea60

    #@23
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@26
    .line 2924
    iget-object v0, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@28
    const/4 v1, 0x3

    #@29
    aget-object v0, v0, v1

    #@2b
    const/4 v1, 0x0

    #@2c
    invoke-virtual {v0, v7, v1}, Landroid/media/AudioService$VolumeStreamState;->getIndex(IZ)I

    #@2f
    move-result v8

    #@30
    .line 2926
    .local v8, index:I
    const/4 v0, 0x3

    #@31
    const/4 v1, 0x0

    #@32
    invoke-static {v0, v1}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    #@35
    move-result v0

    #@36
    if-eqz v0, :cond_54

    #@38
    iget v0, p0, Landroid/media/AudioService;->mSafeMediaVolumeIndex:I

    #@3a
    if-le v8, v0, :cond_54

    #@3c
    .line 2929
    iget v0, p0, Landroid/media/AudioService;->mMusicActiveMs:I

    #@3e
    add-int/2addr v0, v10

    #@3f
    iput v0, p0, Landroid/media/AudioService;->mMusicActiveMs:I

    #@41
    .line 2930
    iget v0, p0, Landroid/media/AudioService;->mMusicActiveMs:I

    #@43
    const v1, 0x44aa200

    #@46
    if-le v0, v1, :cond_54

    #@48
    .line 2931
    const/4 v0, 0x1

    #@49
    invoke-direct {p0, v0}, Landroid/media/AudioService;->setSafeMediaVolumeEnabled(Z)V

    #@4c
    .line 2932
    const/4 v0, 0x0

    #@4d
    iput v0, p0, Landroid/media/AudioService;->mMusicActiveMs:I

    #@4f
    .line 2933
    iget-object v0, p0, Landroid/media/AudioService;->mVolumePanel:Landroid/view/VolumePanel;

    #@51
    invoke-virtual {v0}, Landroid/view/VolumePanel;->postDisplaySafeVolumeWarning()V

    #@54
    .line 2938
    .end local v7           #device:I
    .end local v8           #index:I
    :cond_54
    monitor-exit v9

    #@55
    .line 2939
    return-void

    #@56
    .line 2938
    :catchall_56
    move-exception v0

    #@57
    monitor-exit v9
    :try_end_58
    .catchall {:try_start_6 .. :try_end_58} :catchall_56

    #@58
    throw v0
.end method

.method private onConfigureSafeVolume(Z)V
    .registers 12
    .parameter "force"

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    .line 2942
    iget-object v9, p0, Landroid/media/AudioService;->mSafeMediaVolumeState:Ljava/lang/Integer;

    #@3
    monitor-enter v9

    #@4
    .line 2943
    :try_start_4
    iget-object v0, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@d
    move-result-object v0

    #@e
    iget v7, v0, Landroid/content/res/Configuration;->mcc:I

    #@10
    .line 2944
    .local v7, mcc:I
    iget v0, p0, Landroid/media/AudioService;->mMcc:I

    #@12
    if-ne v0, v7, :cond_1a

    #@14
    iget v0, p0, Landroid/media/AudioService;->mMcc:I

    #@16
    if-nez v0, :cond_5a

    #@18
    if-eqz p1, :cond_5a

    #@1a
    .line 2945
    :cond_1a
    iget-object v0, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@1c
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1f
    move-result-object v0

    #@20
    const v1, 0x10e003a

    #@23
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@26
    move-result v0

    #@27
    mul-int/lit8 v0, v0, 0xa

    #@29
    iput v0, p0, Landroid/media/AudioService;->mSafeMediaVolumeIndex:I

    #@2b
    .line 2947
    iget-object v0, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@2d
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@30
    move-result-object v0

    #@31
    const v1, 0x1110049

    #@34
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@37
    move-result v8

    #@38
    .line 2953
    .local v8, safeMediaVolumeEnabled:Z
    if-eqz v8, :cond_5c

    #@3a
    .line 2954
    const/4 v3, 0x3

    #@3b
    .line 2958
    .local v3, persistedState:I
    iget-object v0, p0, Landroid/media/AudioService;->mSafeMediaVolumeState:Ljava/lang/Integer;

    #@3d
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@40
    move-result v0

    #@41
    if-eq v0, v2, :cond_4d

    #@43
    .line 2959
    const/4 v0, 0x3

    #@44
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@47
    move-result-object v0

    #@48
    iput-object v0, p0, Landroid/media/AudioService;->mSafeMediaVolumeState:Ljava/lang/Integer;

    #@4a
    .line 2960
    invoke-direct {p0}, Landroid/media/AudioService;->enforceSafeMediaVolume()V

    #@4d
    .line 2966
    :cond_4d
    :goto_4d
    iput v7, p0, Landroid/media/AudioService;->mMcc:I

    #@4f
    .line 2967
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@51
    const/16 v1, 0x1c

    #@53
    const/4 v2, 0x2

    #@54
    const/4 v4, 0x0

    #@55
    const/4 v5, 0x0

    #@56
    const/4 v6, 0x0

    #@57
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@5a
    .line 2975
    .end local v3           #persistedState:I
    .end local v8           #safeMediaVolumeEnabled:Z
    :cond_5a
    monitor-exit v9

    #@5b
    .line 2976
    return-void

    #@5c
    .line 2963
    .restart local v8       #safeMediaVolumeEnabled:Z
    :cond_5c
    const/4 v3, 0x1

    #@5d
    .line 2964
    .restart local v3       #persistedState:I
    const/4 v0, 0x1

    #@5e
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@61
    move-result-object v0

    #@62
    iput-object v0, p0, Landroid/media/AudioService;->mSafeMediaVolumeState:Ljava/lang/Integer;

    #@64
    goto :goto_4d

    #@65
    .line 2975
    .end local v3           #persistedState:I
    .end local v7           #mcc:I
    .end local v8           #safeMediaVolumeEnabled:Z
    :catchall_65
    move-exception v0

    #@66
    monitor-exit v9
    :try_end_67
    .catchall {:try_start_4 .. :try_end_67} :catchall_65

    #@67
    throw v0
.end method

.method private onNewPlaybackInfoForRcc(III)V
    .registers 10
    .parameter "rccId"
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 6395
    iget-object v3, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@2
    monitor-enter v3

    #@3
    .line 6396
    :try_start_3
    iget-object v2, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@5
    invoke-virtual {v2}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    #@8
    move-result-object v1

    #@9
    .line 6397
    .local v1, stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :cond_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_b1

    #@f
    .line 6398
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Landroid/media/AudioService$RemoteControlStackEntry;

    #@15
    .line 6399
    .local v0, rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    iget v2, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mRccId:I

    #@17
    if-ne v2, p1, :cond_9

    #@19
    .line 6400
    sparse-switch p2, :sswitch_data_b4

    #@1c
    .line 6445
    const-string v2, "AudioService"

    #@1e
    new-instance v4, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string/jumbo v5, "unhandled key "

    #@26
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v4

    #@2a
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v4

    #@2e
    const-string v5, " for RCC "

    #@30
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v4

    #@34
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37
    move-result-object v4

    #@38
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v4

    #@3c
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 6448
    :goto_3f
    monitor-exit v3

    #@40
    .line 6452
    .end local v0           #rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    :goto_40
    return-void

    #@41
    .line 6402
    .restart local v0       #rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    :sswitch_41
    iput p3, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mPlaybackType:I

    #@43
    .line 6403
    invoke-direct {p0}, Landroid/media/AudioService;->postReevaluateRemote()V

    #@46
    goto :goto_3f

    #@47
    .line 6451
    .end local v0           #rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    .end local v1           #stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :catchall_47
    move-exception v2

    #@48
    monitor-exit v3
    :try_end_49
    .catchall {:try_start_3 .. :try_end_49} :catchall_47

    #@49
    throw v2

    #@4a
    .line 6406
    .restart local v0       #rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    .restart local v1       #stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :sswitch_4a
    :try_start_4a
    iput p3, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mPlaybackVolume:I

    #@4c
    .line 6407
    iget-object v4, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@4e
    monitor-enter v4
    :try_end_4f
    .catchall {:try_start_4a .. :try_end_4f} :catchall_47

    #@4f
    .line 6408
    :try_start_4f
    iget-object v2, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@51
    iget v2, v2, Landroid/media/AudioService$RemotePlaybackState;->mRccId:I

    #@53
    if-ne p1, v2, :cond_5e

    #@55
    .line 6409
    iget-object v2, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@57
    iput p3, v2, Landroid/media/AudioService$RemotePlaybackState;->mVolume:I

    #@59
    .line 6410
    iget-object v2, p0, Landroid/media/AudioService;->mVolumePanel:Landroid/view/VolumePanel;

    #@5b
    invoke-virtual {v2}, Landroid/view/VolumePanel;->postHasNewRemotePlaybackInfo()V

    #@5e
    .line 6412
    :cond_5e
    monitor-exit v4

    #@5f
    goto :goto_3f

    #@60
    :catchall_60
    move-exception v2

    #@61
    monitor-exit v4
    :try_end_62
    .catchall {:try_start_4f .. :try_end_62} :catchall_60

    #@62
    :try_start_62
    throw v2

    #@63
    .line 6415
    :sswitch_63
    iput p3, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mPlaybackVolumeMax:I

    #@65
    .line 6416
    iget-object v4, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@67
    monitor-enter v4
    :try_end_68
    .catchall {:try_start_62 .. :try_end_68} :catchall_47

    #@68
    .line 6417
    :try_start_68
    iget-object v2, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@6a
    iget v2, v2, Landroid/media/AudioService$RemotePlaybackState;->mRccId:I

    #@6c
    if-ne p1, v2, :cond_77

    #@6e
    .line 6418
    iget-object v2, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@70
    iput p3, v2, Landroid/media/AudioService$RemotePlaybackState;->mVolumeMax:I

    #@72
    .line 6419
    iget-object v2, p0, Landroid/media/AudioService;->mVolumePanel:Landroid/view/VolumePanel;

    #@74
    invoke-virtual {v2}, Landroid/view/VolumePanel;->postHasNewRemotePlaybackInfo()V

    #@77
    .line 6421
    :cond_77
    monitor-exit v4

    #@78
    goto :goto_3f

    #@79
    :catchall_79
    move-exception v2

    #@7a
    monitor-exit v4
    :try_end_7b
    .catchall {:try_start_68 .. :try_end_7b} :catchall_79

    #@7b
    :try_start_7b
    throw v2

    #@7c
    .line 6424
    :sswitch_7c
    iput p3, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mPlaybackVolumeHandling:I

    #@7e
    .line 6425
    iget-object v4, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@80
    monitor-enter v4
    :try_end_81
    .catchall {:try_start_7b .. :try_end_81} :catchall_47

    #@81
    .line 6426
    :try_start_81
    iget-object v2, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@83
    iget v2, v2, Landroid/media/AudioService$RemotePlaybackState;->mRccId:I

    #@85
    if-ne p1, v2, :cond_90

    #@87
    .line 6427
    iget-object v2, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@89
    iput p3, v2, Landroid/media/AudioService$RemotePlaybackState;->mVolumeHandling:I

    #@8b
    .line 6428
    iget-object v2, p0, Landroid/media/AudioService;->mVolumePanel:Landroid/view/VolumePanel;

    #@8d
    invoke-virtual {v2}, Landroid/view/VolumePanel;->postHasNewRemotePlaybackInfo()V

    #@90
    .line 6430
    :cond_90
    monitor-exit v4

    #@91
    goto :goto_3f

    #@92
    :catchall_92
    move-exception v2

    #@93
    monitor-exit v4
    :try_end_94
    .catchall {:try_start_81 .. :try_end_94} :catchall_92

    #@94
    :try_start_94
    throw v2

    #@95
    .line 6433
    :sswitch_95
    iput p3, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mPlaybackStream:I

    #@97
    goto :goto_3f

    #@98
    .line 6436
    :sswitch_98
    iput p3, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mPlaybackState:I

    #@9a
    .line 6437
    iget-object v4, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@9c
    monitor-enter v4
    :try_end_9d
    .catchall {:try_start_94 .. :try_end_9d} :catchall_47

    #@9d
    .line 6438
    :try_start_9d
    iget-object v2, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@9f
    iget v2, v2, Landroid/media/AudioService$RemotePlaybackState;->mRccId:I

    #@a1
    if-ne p1, v2, :cond_ac

    #@a3
    .line 6439
    invoke-static {p3}, Landroid/media/AudioService;->isPlaystateActive(I)Z

    #@a6
    move-result v2

    #@a7
    iput-boolean v2, p0, Landroid/media/AudioService;->mMainRemoteIsActive:Z

    #@a9
    .line 6440
    invoke-direct {p0}, Landroid/media/AudioService;->postReevaluateRemote()V

    #@ac
    .line 6442
    :cond_ac
    monitor-exit v4

    #@ad
    goto :goto_3f

    #@ae
    :catchall_ae
    move-exception v2

    #@af
    monitor-exit v4
    :try_end_b0
    .catchall {:try_start_9d .. :try_end_b0} :catchall_ae

    #@b0
    :try_start_b0
    throw v2

    #@b1
    .line 6451
    .end local v0           #rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    :cond_b1
    monitor-exit v3
    :try_end_b2
    .catchall {:try_start_b0 .. :try_end_b2} :catchall_47

    #@b2
    goto :goto_40

    #@b3
    .line 6400
    nop

    #@b4
    :sswitch_data_b4
    .sparse-switch
        0x1 -> :sswitch_41
        0x2 -> :sswitch_4a
        0x3 -> :sswitch_63
        0x4 -> :sswitch_7c
        0x5 -> :sswitch_95
        0xff -> :sswitch_98
    .end sparse-switch
.end method

.method private onRcDisplayClear()V
    .registers 6

    #@0
    .prologue
    .line 5931
    iget-object v1, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@2
    monitor-enter v1

    #@3
    .line 5932
    :try_start_3
    iget-object v2, p0, Landroid/media/AudioService;->mCurrentRcLock:Ljava/lang/Object;

    #@5
    monitor-enter v2
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_19

    #@6
    .line 5933
    :try_start_6
    iget v0, p0, Landroid/media/AudioService;->mCurrentRcClientGen:I

    #@8
    add-int/lit8 v0, v0, 0x1

    #@a
    iput v0, p0, Landroid/media/AudioService;->mCurrentRcClientGen:I

    #@c
    .line 5935
    iget v0, p0, Landroid/media/AudioService;->mCurrentRcClientGen:I

    #@e
    const/4 v3, 0x0

    #@f
    const/4 v4, 0x1

    #@10
    invoke-direct {p0, v0, v3, v4}, Landroid/media/AudioService;->setNewRcClient_syncRcsCurrc(ILandroid/app/PendingIntent;Z)V

    #@13
    .line 5937
    monitor-exit v2
    :try_end_14
    .catchall {:try_start_6 .. :try_end_14} :catchall_16

    #@14
    .line 5938
    :try_start_14
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_14 .. :try_end_15} :catchall_19

    #@15
    .line 5939
    return-void

    #@16
    .line 5937
    :catchall_16
    move-exception v0

    #@17
    :try_start_17
    monitor-exit v2
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_16

    #@18
    :try_start_18
    throw v0

    #@19
    .line 5938
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_18 .. :try_end_1b} :catchall_19

    #@1b
    throw v0
.end method

.method private onRcDisplayUpdate(Landroid/media/AudioService$RemoteControlStackEntry;I)V
    .registers 10
    .parameter "rcse"
    .parameter "flags"

    #@0
    .prologue
    .line 5945
    iget-object v2, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@2
    monitor-enter v2

    #@3
    .line 5946
    :try_start_3
    iget-object v3, p0, Landroid/media/AudioService;->mCurrentRcLock:Ljava/lang/Object;

    #@5
    monitor-enter v3
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_50

    #@6
    .line 5947
    :try_start_6
    iget-object v1, p0, Landroid/media/AudioService;->mCurrentRcClient:Landroid/media/IRemoteControlClient;

    #@8
    if-eqz v1, :cond_2d

    #@a
    iget-object v1, p0, Landroid/media/AudioService;->mCurrentRcClient:Landroid/media/IRemoteControlClient;

    #@c
    iget-object v4, p1, Landroid/media/AudioService$RemoteControlStackEntry;->mRcClient:Landroid/media/IRemoteControlClient;

    #@e
    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_2d

    #@14
    .line 5950
    iget v1, p0, Landroid/media/AudioService;->mCurrentRcClientGen:I

    #@16
    add-int/lit8 v1, v1, 0x1

    #@18
    iput v1, p0, Landroid/media/AudioService;->mCurrentRcClientGen:I

    #@1a
    .line 5953
    iget v1, p0, Landroid/media/AudioService;->mCurrentRcClientGen:I

    #@1c
    iget-object v4, p1, Landroid/media/AudioService$RemoteControlStackEntry;->mMediaIntent:Landroid/app/PendingIntent;

    #@1e
    const/4 v5, 0x0

    #@1f
    invoke-direct {p0, v1, v4, v5}, Landroid/media/AudioService;->setNewRcClient_syncRcsCurrc(ILandroid/app/PendingIntent;Z)V
    :try_end_22
    .catchall {:try_start_6 .. :try_end_22} :catchall_4d

    #@22
    .line 5959
    :try_start_22
    iget-object v1, p0, Landroid/media/AudioService;->mCurrentRcClient:Landroid/media/IRemoteControlClient;

    #@24
    iget v4, p0, Landroid/media/AudioService;->mCurrentRcClientGen:I

    #@26
    iget v5, p0, Landroid/media/AudioService;->mArtworkExpectedWidth:I

    #@28
    iget v6, p0, Landroid/media/AudioService;->mArtworkExpectedHeight:I

    #@2a
    invoke-interface {v1, v4, p2, v5, v6}, Landroid/media/IRemoteControlClient;->onInformationRequested(IIII)V
    :try_end_2d
    .catchall {:try_start_22 .. :try_end_2d} :catchall_4d
    .catch Landroid/os/RemoteException; {:try_start_22 .. :try_end_2d} :catch_30

    #@2d
    .line 5970
    :cond_2d
    :goto_2d
    :try_start_2d
    monitor-exit v3
    :try_end_2e
    .catchall {:try_start_2d .. :try_end_2e} :catchall_4d

    #@2e
    .line 5971
    :try_start_2e
    monitor-exit v2
    :try_end_2f
    .catchall {:try_start_2e .. :try_end_2f} :catchall_50

    #@2f
    .line 5972
    return-void

    #@30
    .line 5961
    :catch_30
    move-exception v0

    #@31
    .line 5962
    .local v0, e:Landroid/os/RemoteException;
    :try_start_31
    const-string v1, "AudioService"

    #@33
    new-instance v4, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v5, "Current valid remote client is dead: "

    #@3a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v4

    #@3e
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v4

    #@42
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v4

    #@46
    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    .line 5963
    const/4 v1, 0x0

    #@4a
    iput-object v1, p0, Landroid/media/AudioService;->mCurrentRcClient:Landroid/media/IRemoteControlClient;

    #@4c
    goto :goto_2d

    #@4d
    .line 5970
    .end local v0           #e:Landroid/os/RemoteException;
    :catchall_4d
    move-exception v1

    #@4e
    monitor-exit v3
    :try_end_4f
    .catchall {:try_start_31 .. :try_end_4f} :catchall_4d

    #@4f
    :try_start_4f
    throw v1

    #@50
    .line 5971
    :catchall_50
    move-exception v1

    #@51
    monitor-exit v2
    :try_end_52
    .catchall {:try_start_4f .. :try_end_52} :catchall_50

    #@52
    throw v1
.end method

.method private onReevaluateRemote()V
    .registers 7

    #@0
    .prologue
    .line 6634
    const-string v3, "AudioService"

    #@2
    const-string/jumbo v4, "onReevaluateRemote()"

    #@5
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 6636
    const/4 v0, 0x0

    #@9
    .line 6637
    .local v0, hasRemotePlayback:Z
    iget-object v4, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@b
    monitor-enter v4

    #@c
    .line 6638
    :try_start_c
    iget-object v3, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@e
    invoke-virtual {v3}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    #@11
    move-result-object v2

    #@12
    .line 6639
    .local v2, stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :cond_12
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@15
    move-result v3

    #@16
    if-eqz v3, :cond_24

    #@18
    .line 6640
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1b
    move-result-object v1

    #@1c
    check-cast v1, Landroid/media/AudioService$RemoteControlStackEntry;

    #@1e
    .line 6641
    .local v1, rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    iget v3, v1, Landroid/media/AudioService$RemoteControlStackEntry;->mPlaybackType:I

    #@20
    const/4 v5, 0x1

    #@21
    if-ne v3, v5, :cond_12

    #@23
    .line 6642
    const/4 v0, 0x1

    #@24
    .line 6646
    .end local v1           #rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    :cond_24
    monitor-exit v4
    :try_end_25
    .catchall {:try_start_c .. :try_end_25} :catchall_35

    #@25
    .line 6647
    iget-object v4, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@27
    monitor-enter v4

    #@28
    .line 6648
    :try_start_28
    iget-boolean v3, p0, Landroid/media/AudioService;->mHasRemotePlayback:Z

    #@2a
    if-eq v3, v0, :cond_33

    #@2c
    .line 6649
    iput-boolean v0, p0, Landroid/media/AudioService;->mHasRemotePlayback:Z

    #@2e
    .line 6650
    iget-object v3, p0, Landroid/media/AudioService;->mVolumePanel:Landroid/view/VolumePanel;

    #@30
    invoke-virtual {v3, v0}, Landroid/view/VolumePanel;->postRemoteSliderVisibility(Z)V

    #@33
    .line 6652
    :cond_33
    monitor-exit v4
    :try_end_34
    .catchall {:try_start_28 .. :try_end_34} :catchall_38

    #@34
    .line 6653
    return-void

    #@35
    .line 6646
    .end local v2           #stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :catchall_35
    move-exception v3

    #@36
    :try_start_36
    monitor-exit v4
    :try_end_37
    .catchall {:try_start_36 .. :try_end_37} :catchall_35

    #@37
    throw v3

    #@38
    .line 6652
    .restart local v2       #stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :catchall_38
    move-exception v3

    #@39
    :try_start_39
    monitor-exit v4
    :try_end_3a
    .catchall {:try_start_39 .. :try_end_3a} :catchall_38

    #@3a
    throw v3
.end method

.method private onRegisterVolumeObserverForRcc(ILandroid/media/IRemoteVolumeObserver;)V
    .registers 7
    .parameter "rccId"
    .parameter "rvo"

    #@0
    .prologue
    .line 6461
    iget-object v3, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@2
    monitor-enter v3

    #@3
    .line 6462
    :try_start_3
    iget-object v2, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@5
    invoke-virtual {v2}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    #@8
    move-result-object v1

    #@9
    .line 6463
    .local v1, stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :cond_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_1b

    #@f
    .line 6464
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Landroid/media/AudioService$RemoteControlStackEntry;

    #@15
    .line 6465
    .local v0, rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    iget v2, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mRccId:I

    #@17
    if-ne v2, p1, :cond_9

    #@19
    .line 6466
    iput-object p2, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mRemoteVolumeObs:Landroid/media/IRemoteVolumeObserver;

    #@1b
    .line 6470
    .end local v0           #rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    :cond_1b
    monitor-exit v3

    #@1c
    .line 6471
    return-void

    #@1d
    .line 6470
    .end local v1           #stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :catchall_1d
    move-exception v2

    #@1e
    monitor-exit v3
    :try_end_1f
    .catchall {:try_start_3 .. :try_end_1f} :catchall_1d

    #@1f
    throw v2
.end method

.method private onSendBecomingNoisyIntent()V
    .registers 3

    #@0
    .prologue
    .line 4246
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.media.AUDIO_BECOMING_NOISY"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 4247
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x1000

    #@9
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@c
    .line 4248
    invoke-direct {p0, v0}, Landroid/media/AudioService;->sendBroadcastToAll(Landroid/content/Intent;)V

    #@f
    .line 4250
    return-void
.end method

.method private onSetA2dpConnectionState(Landroid/bluetooth/BluetoothDevice;I)V
    .registers 15
    .parameter "btDevice"
    .parameter "state"

    #@0
    .prologue
    const/4 v3, 0x2

    #@1
    const/4 v8, 0x1

    #@2
    const/4 v1, 0x0

    #@3
    .line 4285
    if-nez p1, :cond_6

    #@5
    .line 4340
    :goto_5
    return-void

    #@6
    .line 4288
    :cond_6
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@9
    move-result-object v7

    #@a
    .line 4289
    .local v7, address:Ljava/lang/String;
    invoke-static {v7}, Landroid/bluetooth/BluetoothAdapter;->checkBluetoothAddress(Ljava/lang/String;)Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_12

    #@10
    .line 4290
    const-string v7, ""

    #@12
    .line 4292
    :cond_12
    iget-object v10, p0, Landroid/media/AudioService;->mConnectedDevices:Ljava/util/HashMap;

    #@14
    monitor-enter v10

    #@15
    .line 4293
    :try_start_15
    iget-object v0, p0, Landroid/media/AudioService;->mConnectedDevices:Ljava/util/HashMap;

    #@17
    const/16 v2, 0x80

    #@19
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@20
    move-result v0

    #@21
    if-eqz v0, :cond_66

    #@23
    iget-object v0, p0, Landroid/media/AudioService;->mConnectedDevices:Ljava/util/HashMap;

    #@25
    const/16 v2, 0x80

    #@27
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2e
    move-result-object v0

    #@2f
    check-cast v0, Ljava/lang/String;

    #@31
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@34
    move-result v0

    #@35
    if-eqz v0, :cond_66

    #@37
    .line 4297
    .local v8, isConnected:Z
    :goto_37
    if-eqz v8, :cond_6f

    #@39
    if-eq p2, v3, :cond_6f

    #@3b
    .line 4298
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->isBluetoothDock()Z

    #@3e
    move-result v0

    #@3f
    if-eqz v0, :cond_68

    #@41
    .line 4299
    if-nez p2, :cond_46

    #@43
    .line 4303
    invoke-direct {p0, v7}, Landroid/media/AudioService;->makeA2dpDeviceUnavailableLater(Ljava/lang/String;)V

    #@46
    .line 4309
    :cond_46
    :goto_46
    iget-object v11, p0, Landroid/media/AudioService;->mCurAudioRoutes:Landroid/media/AudioRoutesInfo;

    #@48
    monitor-enter v11
    :try_end_49
    .catchall {:try_start_15 .. :try_end_49} :catchall_63

    #@49
    .line 4310
    :try_start_49
    iget-object v0, p0, Landroid/media/AudioService;->mCurAudioRoutes:Landroid/media/AudioRoutesInfo;

    #@4b
    iget-object v0, v0, Landroid/media/AudioRoutesInfo;->mBluetoothName:Ljava/lang/CharSequence;

    #@4d
    if-eqz v0, :cond_60

    #@4f
    .line 4311
    iget-object v0, p0, Landroid/media/AudioService;->mCurAudioRoutes:Landroid/media/AudioRoutesInfo;

    #@51
    const/4 v1, 0x0

    #@52
    iput-object v1, v0, Landroid/media/AudioRoutesInfo;->mBluetoothName:Ljava/lang/CharSequence;

    #@54
    .line 4312
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@56
    const/16 v1, 0x10

    #@58
    const/4 v2, 0x1

    #@59
    const/4 v3, 0x0

    #@5a
    const/4 v4, 0x0

    #@5b
    const/4 v5, 0x0

    #@5c
    const/4 v6, 0x0

    #@5d
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@60
    .line 4315
    :cond_60
    monitor-exit v11
    :try_end_61
    .catchall {:try_start_49 .. :try_end_61} :catchall_6c

    #@61
    .line 4339
    :cond_61
    :goto_61
    :try_start_61
    monitor-exit v10

    #@62
    goto :goto_5

    #@63
    .end local v8           #isConnected:Z
    :catchall_63
    move-exception v0

    #@64
    monitor-exit v10
    :try_end_65
    .catchall {:try_start_61 .. :try_end_65} :catchall_63

    #@65
    throw v0

    #@66
    :cond_66
    move v8, v1

    #@67
    .line 4293
    goto :goto_37

    #@68
    .line 4307
    .restart local v8       #isConnected:Z
    :cond_68
    :try_start_68
    invoke-direct {p0, v7}, Landroid/media/AudioService;->makeA2dpDeviceUnavailableNow(Ljava/lang/String;)V
    :try_end_6b
    .catchall {:try_start_68 .. :try_end_6b} :catchall_63

    #@6b
    goto :goto_46

    #@6c
    .line 4315
    :catchall_6c
    move-exception v0

    #@6d
    :try_start_6d
    monitor-exit v11
    :try_end_6e
    .catchall {:try_start_6d .. :try_end_6e} :catchall_6c

    #@6e
    :try_start_6e
    throw v0

    #@6f
    .line 4316
    :cond_6f
    if-nez v8, :cond_61

    #@71
    if-ne p2, v3, :cond_61

    #@73
    .line 4317
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->isBluetoothDock()Z

    #@76
    move-result v0

    #@77
    if-eqz v0, :cond_a7

    #@79
    .line 4319
    invoke-direct {p0}, Landroid/media/AudioService;->cancelA2dpDeviceTimeout()V

    #@7c
    .line 4320
    iput-object v7, p0, Landroid/media/AudioService;->mDockAddress:Ljava/lang/String;

    #@7e
    .line 4329
    :cond_7e
    :goto_7e
    invoke-direct {p0, v7}, Landroid/media/AudioService;->makeA2dpDeviceAvailable(Ljava/lang/String;)V

    #@81
    .line 4330
    iget-object v11, p0, Landroid/media/AudioService;->mCurAudioRoutes:Landroid/media/AudioRoutesInfo;

    #@83
    monitor-enter v11
    :try_end_84
    .catchall {:try_start_6e .. :try_end_84} :catchall_63

    #@84
    .line 4331
    :try_start_84
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAliasName()Ljava/lang/String;

    #@87
    move-result-object v9

    #@88
    .line 4332
    .local v9, name:Ljava/lang/String;
    iget-object v0, p0, Landroid/media/AudioService;->mCurAudioRoutes:Landroid/media/AudioRoutesInfo;

    #@8a
    iget-object v0, v0, Landroid/media/AudioRoutesInfo;->mBluetoothName:Ljava/lang/CharSequence;

    #@8c
    invoke-static {v0, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@8f
    move-result v0

    #@90
    if-nez v0, :cond_a2

    #@92
    .line 4333
    iget-object v0, p0, Landroid/media/AudioService;->mCurAudioRoutes:Landroid/media/AudioRoutesInfo;

    #@94
    iput-object v9, v0, Landroid/media/AudioRoutesInfo;->mBluetoothName:Ljava/lang/CharSequence;

    #@96
    .line 4334
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@98
    const/16 v1, 0x10

    #@9a
    const/4 v2, 0x1

    #@9b
    const/4 v3, 0x0

    #@9c
    const/4 v4, 0x0

    #@9d
    const/4 v5, 0x0

    #@9e
    const/4 v6, 0x0

    #@9f
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@a2
    .line 4337
    :cond_a2
    monitor-exit v11

    #@a3
    goto :goto_61

    #@a4
    .end local v9           #name:Ljava/lang/String;
    :catchall_a4
    move-exception v0

    #@a5
    monitor-exit v11
    :try_end_a6
    .catchall {:try_start_84 .. :try_end_a6} :catchall_a4

    #@a6
    :try_start_a6
    throw v0

    #@a7
    .line 4324
    :cond_a7
    invoke-direct {p0}, Landroid/media/AudioService;->hasScheduledA2dpDockTimeout()Z

    #@aa
    move-result v0

    #@ab
    if-eqz v0, :cond_7e

    #@ad
    .line 4325
    invoke-direct {p0}, Landroid/media/AudioService;->cancelA2dpDeviceTimeout()V

    #@b0
    .line 4326
    iget-object v0, p0, Landroid/media/AudioService;->mDockAddress:Ljava/lang/String;

    #@b2
    invoke-direct {p0, v0}, Landroid/media/AudioService;->makeA2dpDeviceUnavailableNow(Ljava/lang/String;)V
    :try_end_b5
    .catchall {:try_start_a6 .. :try_end_b5} :catchall_63

    #@b5
    goto :goto_7e
.end method

.method private onSetRsxConnectionState(II)V
    .registers 8
    .parameter "available"
    .parameter "address"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 2901
    const v3, -0x7fffff00

    #@5
    if-ne p1, v1, :cond_1c

    #@7
    move v0, v1

    #@8
    :goto_8
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@b
    move-result-object v4

    #@c
    invoke-static {v3, v0, v4}, Landroid/media/AudioSystem;->setDeviceConnectionState(IILjava/lang/String;)I

    #@f
    .line 2905
    const v0, 0x8000

    #@12
    if-ne p1, v1, :cond_1e

    #@14
    :goto_14
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    invoke-static {v0, v1, v2}, Landroid/media/AudioSystem;->setDeviceConnectionState(IILjava/lang/String;)I

    #@1b
    .line 2909
    return-void

    #@1c
    :cond_1c
    move v0, v2

    #@1d
    .line 2901
    goto :goto_8

    #@1e
    :cond_1e
    move v1, v2

    #@1f
    .line 2905
    goto :goto_14
.end method

.method private onSetWiredDeviceConnectionState(IILjava/lang/String;)V
    .registers 13
    .parameter "device"
    .parameter "state"
    .parameter "name"

    #@0
    .prologue
    const/16 v5, 0x8

    #@2
    const/4 v4, 0x4

    #@3
    const/4 v0, 0x1

    #@4
    const/4 v1, 0x0

    #@5
    .line 4456
    iget-object v8, p0, Landroid/media/AudioService;->mConnectedDevices:Ljava/util/HashMap;

    #@7
    monitor-enter v8

    #@8
    .line 4457
    if-nez p2, :cond_29

    #@a
    if-eq p1, v4, :cond_e

    #@c
    if-ne p1, v5, :cond_29

    #@e
    .line 4459
    :cond_e
    const/4 v2, 0x1

    #@f
    :try_start_f
    invoke-virtual {p0, v2}, Landroid/media/AudioService;->setBluetoothA2dpOnInt(Z)V

    #@12
    .line 4462
    iget-object v2, p0, Landroid/media/AudioService;->mSafeMediaVolumeState:Ljava/lang/Integer;

    #@14
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@17
    move-result v2

    #@18
    const/4 v3, 0x3

    #@19
    if-ne v2, v3, :cond_29

    #@1b
    iget-object v2, p0, Landroid/media/AudioService;->mLgSafeMediaVolumeEnabled:Ljava/lang/Boolean;

    #@1d
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    #@20
    move-result v2

    #@21
    if-eqz v2, :cond_29

    #@23
    .line 4464
    iget-object v2, p0, Landroid/media/AudioService;->mVolumePanel:Landroid/view/VolumePanel;

    #@25
    const/4 v3, -0x1

    #@26
    invoke-virtual {v2, v3}, Landroid/view/VolumePanel;->postDismissSafeVolumeWarning(I)V

    #@29
    .line 4468
    :cond_29
    and-int/lit16 v2, p1, 0x6000

    #@2b
    if-eqz v2, :cond_6e

    #@2d
    move v7, v0

    #@2e
    .line 4469
    .local v7, isUsb:Z
    :goto_2e
    if-ne p2, v0, :cond_31

    #@30
    move v1, v0

    #@31
    :cond_31
    if-eqz v7, :cond_70

    #@33
    move-object v0, p3

    #@34
    :goto_34
    invoke-direct {p0, v1, p1, v0}, Landroid/media/AudioService;->handleDeviceConnection(ZILjava/lang/String;)Z

    #@37
    .line 4470
    if-eqz p2, :cond_67

    #@39
    .line 4471
    if-eq p1, v4, :cond_3d

    #@3b
    if-ne p1, v5, :cond_41

    #@3d
    .line 4473
    :cond_3d
    const/4 v0, 0x0

    #@3e
    invoke-virtual {p0, v0}, Landroid/media/AudioService;->setBluetoothA2dpOnInt(Z)V

    #@41
    .line 4475
    :cond_41
    and-int/lit8 v0, p1, 0xc

    #@43
    if-eqz v0, :cond_67

    #@45
    .line 4477
    iget-object v0, p0, Landroid/media/AudioService;->mLgSafeMediaVolumeEnabled:Ljava/lang/Boolean;

    #@47
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    #@4a
    move-result v0

    #@4b
    if-eqz v0, :cond_59

    #@4d
    .line 4478
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@4f
    const/16 v1, 0x1d

    #@51
    const/4 v2, 0x2

    #@52
    const/4 v3, 0x1

    #@53
    const/4 v4, 0x0

    #@54
    const/4 v5, 0x0

    #@55
    const/4 v6, 0x0

    #@56
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@59
    .line 4488
    :cond_59
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@5b
    const/16 v1, 0x18

    #@5d
    const/4 v2, 0x0

    #@5e
    const/4 v3, 0x0

    #@5f
    const/4 v4, 0x0

    #@60
    const/4 v5, 0x0

    #@61
    const v6, 0xea60

    #@64
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@67
    .line 4497
    :cond_67
    if-nez v7, :cond_6c

    #@69
    .line 4498
    invoke-direct {p0, p1, p2, p3}, Landroid/media/AudioService;->sendDeviceConnectionIntent(IILjava/lang/String;)V

    #@6c
    .line 4500
    :cond_6c
    monitor-exit v8

    #@6d
    .line 4501
    return-void

    #@6e
    .end local v7           #isUsb:Z
    :cond_6e
    move v7, v1

    #@6f
    .line 4468
    goto :goto_2e

    #@70
    .line 4469
    .restart local v7       #isUsb:Z
    :cond_70
    const-string v0, ""

    #@72
    goto :goto_34

    #@73
    .line 4500
    .end local v7           #isUsb:Z
    :catchall_73
    move-exception v0

    #@74
    monitor-exit v8
    :try_end_75
    .catchall {:try_start_f .. :try_end_75} :catchall_73

    #@75
    throw v0
.end method

.method private onShowVolumeInfo(I)V
    .registers 8
    .parameter "flag"

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/4 v4, 0x2

    #@2
    const/4 v3, 0x0

    #@3
    .line 7100
    const-string v0, "AudioService"

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string/jumbo v2, "onShowVolumeInfo mediavolume:"

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {p0, v5}, Landroid/media/AudioService;->getStreamVolume(I)I

    #@14
    move-result v2

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, " flag:"

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 7101
    const/4 v0, 0x1

    #@2b
    if-ne p1, v0, :cond_ab

    #@2d
    .line 7104
    invoke-virtual {p0, v3}, Landroid/media/AudioService;->getStreamVolume(I)I

    #@30
    move-result v0

    #@31
    iget v1, p0, Landroid/media/AudioService;->mSafeVoiceVolumeIndex:I

    #@33
    if-le v0, v1, :cond_7f

    #@35
    invoke-virtual {p0}, Landroid/media/AudioService;->isBluetoothScoOn()Z

    #@38
    move-result v0

    #@39
    if-nez v0, :cond_7f

    #@3b
    invoke-virtual {p0}, Landroid/media/AudioService;->isSpeakerphoneOn()Z

    #@3e
    move-result v0

    #@3f
    if-nez v0, :cond_7f

    #@41
    invoke-direct {p0}, Landroid/media/AudioService;->isSafeMediaVolumeDevices()Z

    #@44
    move-result v0

    #@45
    if-eqz v0, :cond_7f

    #@47
    .line 7106
    const-string v0, "AudioService"

    #@49
    new-instance v1, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string/jumbo v2, "onHeadsetVolumeScenario() set default call vol : "

    #@51
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v1

    #@55
    sget-object v2, Landroid/media/AudioManager;->DEFAULT_STREAM_VOLUME:[I

    #@57
    aget v2, v2, v3

    #@59
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v1

    #@5d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v1

    #@61
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    .line 7107
    iget v0, p0, Landroid/media/AudioService;->mSafeVoiceVolumeIndex:I

    #@66
    invoke-virtual {p0, v3, v0, v3}, Landroid/media/AudioService;->setStreamVolume(III)V

    #@69
    .line 7109
    invoke-virtual {p0}, Landroid/media/AudioService;->getMode()I

    #@6c
    move-result v0

    #@6d
    if-ne v0, v4, :cond_7f

    #@6f
    .line 7110
    iget-object v0, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@71
    const v1, 0x20902a6

    #@74
    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@77
    move-result-object v0

    #@78
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@7b
    move-result-object v0

    #@7c
    invoke-direct {p0, v0}, Landroid/media/AudioService;->ToastPopUp(Ljava/lang/CharSequence;)V

    #@7f
    .line 7115
    :cond_7f
    invoke-virtual {p0, v5}, Landroid/media/AudioService;->getStreamVolume(I)I

    #@82
    move-result v0

    #@83
    iget v1, p0, Landroid/media/AudioService;->mSafeMediaVolumeIndex:I

    #@85
    add-int/lit8 v1, v1, 0x5

    #@87
    div-int/lit8 v1, v1, 0xa

    #@89
    if-le v0, v1, :cond_aa

    #@8b
    .line 7116
    iget v0, p0, Landroid/media/AudioService;->mSafeMediaVolumeIndex:I

    #@8d
    add-int/lit8 v0, v0, 0x5

    #@8f
    div-int/lit8 v0, v0, 0xa

    #@91
    invoke-virtual {p0, v5, v0, v3}, Landroid/media/AudioService;->setStreamVolume(III)V

    #@94
    .line 7118
    invoke-virtual {p0}, Landroid/media/AudioService;->getMode()I

    #@97
    move-result v0

    #@98
    if-eq v0, v4, :cond_aa

    #@9a
    .line 7119
    iget-object v0, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@9c
    const v1, 0x20902a7

    #@9f
    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@a2
    move-result-object v0

    #@a3
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@a6
    move-result-object v0

    #@a7
    invoke-direct {p0, v0}, Landroid/media/AudioService;->ToastPopUp(Ljava/lang/CharSequence;)V

    #@aa
    .line 7129
    :cond_aa
    :goto_aa
    return-void

    #@ab
    .line 7122
    :cond_ab
    if-ne p1, v4, :cond_c4

    #@ad
    .line 7123
    invoke-virtual {p0}, Landroid/media/AudioService;->getMode()I

    #@b0
    move-result v0

    #@b1
    if-eq v0, v4, :cond_aa

    #@b3
    .line 7124
    iget-object v0, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@b5
    const v1, 0x20902a8

    #@b8
    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@bb
    move-result-object v0

    #@bc
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@bf
    move-result-object v0

    #@c0
    invoke-direct {p0, v0}, Landroid/media/AudioService;->ToastPopUp(Ljava/lang/CharSequence;)V

    #@c3
    goto :goto_aa

    #@c4
    .line 7126
    :cond_c4
    if-ne p1, v5, :cond_aa

    #@c6
    .line 7127
    iget-object v0, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@c8
    const v1, 0x20902aa

    #@cb
    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@ce
    move-result-object v0

    #@cf
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@d2
    move-result-object v0

    #@d3
    invoke-direct {p0, v0}, Landroid/media/AudioService;->ToastPopUp(Ljava/lang/CharSequence;)V

    #@d6
    goto :goto_aa
.end method

.method private postReevaluateRemote()V
    .registers 8

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 6630
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@3
    const/16 v1, 0x11

    #@5
    const/4 v2, 0x2

    #@6
    const/4 v5, 0x0

    #@7
    move v4, v3

    #@8
    move v6, v3

    #@9
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@c
    .line 6631
    return-void
.end method

.method private pushMediaButtonReceiver(Landroid/app/PendingIntent;Landroid/content/ComponentName;)V
    .registers 10
    .parameter "mediaIntent"
    .parameter "target"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 5816
    iget-object v3, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@3
    invoke-virtual {v3}, Ljava/util/Stack;->empty()Z

    #@6
    move-result v3

    #@7
    if-nez v3, :cond_1a

    #@9
    iget-object v3, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@b
    invoke-virtual {v3}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    #@e
    move-result-object v3

    #@f
    check-cast v3, Landroid/media/AudioService$RemoteControlStackEntry;

    #@11
    iget-object v3, v3, Landroid/media/AudioService$RemoteControlStackEntry;->mMediaIntent:Landroid/app/PendingIntent;

    #@13
    invoke-virtual {v3, p1}, Landroid/app/PendingIntent;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v3

    #@17
    if-eqz v3, :cond_1a

    #@19
    .line 5838
    :goto_19
    return-void

    #@1a
    .line 5819
    :cond_1a
    iget-object v3, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@1c
    invoke-virtual {v3}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    #@1f
    move-result-object v1

    #@20
    .line 5820
    .local v1, stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    const/4 v0, 0x0

    #@21
    .line 5821
    .local v0, rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    const/4 v2, 0x0

    #@22
    .line 5822
    .local v2, wasInsideStack:Z
    :cond_22
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@25
    move-result v3

    #@26
    if-eqz v3, :cond_3a

    #@28
    .line 5823
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2b
    move-result-object v0

    #@2c
    .end local v0           #rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    check-cast v0, Landroid/media/AudioService$RemoteControlStackEntry;

    #@2e
    .line 5824
    .restart local v0       #rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    iget-object v3, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mMediaIntent:Landroid/app/PendingIntent;

    #@30
    invoke-virtual {v3, p1}, Landroid/app/PendingIntent;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v3

    #@34
    if-eqz v3, :cond_22

    #@36
    .line 5825
    const/4 v2, 0x1

    #@37
    .line 5826
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    #@3a
    .line 5830
    :cond_3a
    if-nez v2, :cond_41

    #@3c
    .line 5831
    new-instance v0, Landroid/media/AudioService$RemoteControlStackEntry;

    #@3e
    .end local v0           #rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    invoke-direct {v0, p1, p2}, Landroid/media/AudioService$RemoteControlStackEntry;-><init>(Landroid/app/PendingIntent;Landroid/content/ComponentName;)V

    #@41
    .line 5833
    .restart local v0       #rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    :cond_41
    iget-object v3, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@43
    invoke-virtual {v3, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    #@46
    .line 5836
    iget-object v3, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@48
    iget-object v4, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@4a
    const/16 v5, 0xa

    #@4c
    invoke-virtual {v4, v5, v6, v6, p2}, Landroid/media/AudioService$AudioHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@4f
    move-result-object v4

    #@50
    invoke-virtual {v3, v4}, Landroid/media/AudioService$AudioHandler;->sendMessage(Landroid/os/Message;)Z

    #@53
    goto :goto_19
.end method

.method private queueMsgUnderWakeLock(Landroid/os/Handler;IIILjava/lang/Object;I)V
    .registers 14
    .parameter "handler"
    .parameter "msg"
    .parameter "arg1"
    .parameter "arg2"
    .parameter "obj"
    .parameter "delay"

    #@0
    .prologue
    .line 3211
    iget-object v0, p0, Landroid/media/AudioService;->mMediaEventWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@5
    .line 3212
    const/4 v2, 0x2

    #@6
    move-object v0, p1

    #@7
    move v1, p2

    #@8
    move v3, p3

    #@9
    move v4, p4

    #@a
    move-object v5, p5

    #@b
    move v6, p6

    #@c
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@f
    .line 3213
    return-void
.end method

.method private rcDisplay_startDeathMonitor_syncRcStack()V
    .registers 6

    #@0
    .prologue
    .line 6288
    iget-object v2, p0, Landroid/media/AudioService;->mRcDisplay:Landroid/media/IRemoteControlDisplay;

    #@2
    if-eqz v2, :cond_17

    #@4
    .line 6290
    iget-object v2, p0, Landroid/media/AudioService;->mRcDisplay:Landroid/media/IRemoteControlDisplay;

    #@6
    invoke-interface {v2}, Landroid/media/IRemoteControlDisplay;->asBinder()Landroid/os/IBinder;

    #@9
    move-result-object v0

    #@a
    .line 6291
    .local v0, b:Landroid/os/IBinder;
    new-instance v2, Landroid/media/AudioService$RcDisplayDeathHandler;

    #@c
    invoke-direct {v2, p0, v0}, Landroid/media/AudioService$RcDisplayDeathHandler;-><init>(Landroid/media/AudioService;Landroid/os/IBinder;)V

    #@f
    iput-object v2, p0, Landroid/media/AudioService;->mRcDisplayDeathHandler:Landroid/media/AudioService$RcDisplayDeathHandler;

    #@11
    .line 6293
    :try_start_11
    iget-object v2, p0, Landroid/media/AudioService;->mRcDisplayDeathHandler:Landroid/media/AudioService$RcDisplayDeathHandler;

    #@13
    const/4 v3, 0x0

    #@14
    invoke-interface {v0, v2, v3}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_17
    .catch Landroid/os/RemoteException; {:try_start_11 .. :try_end_17} :catch_18

    #@17
    .line 6300
    .end local v0           #b:Landroid/os/IBinder;
    :cond_17
    :goto_17
    return-void

    #@18
    .line 6294
    .restart local v0       #b:Landroid/os/IBinder;
    :catch_18
    move-exception v1

    #@19
    .line 6296
    .local v1, e:Landroid/os/RemoteException;
    const-string v2, "AudioService"

    #@1b
    new-instance v3, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string/jumbo v4, "registerRemoteControlDisplay() has a dead client "

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 6297
    const/4 v2, 0x0

    #@33
    iput-object v2, p0, Landroid/media/AudioService;->mRcDisplay:Landroid/media/IRemoteControlDisplay;

    #@35
    goto :goto_17
.end method

.method private rcDisplay_stopDeathMonitor_syncRcStack()V
    .registers 2

    #@0
    .prologue
    .line 6281
    iget-object v0, p0, Landroid/media/AudioService;->mRcDisplay:Landroid/media/IRemoteControlDisplay;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 6283
    iget-object v0, p0, Landroid/media/AudioService;->mRcDisplayDeathHandler:Landroid/media/AudioService$RcDisplayDeathHandler;

    #@6
    invoke-virtual {v0}, Landroid/media/AudioService$RcDisplayDeathHandler;->unlinkToRcDisplayDeath()V

    #@9
    .line 6285
    :cond_9
    return-void
.end method

.method private readAudioSettings(Z)V
    .registers 11
    .parameter "userSwitch"

    #@0
    .prologue
    const/4 v8, 0x3

    #@1
    const/4 v7, 0x0

    #@2
    .line 2365
    invoke-direct {p0}, Landroid/media/AudioService;->readPersistedSettings()V

    #@5
    .line 2368
    invoke-static {}, Landroid/media/AudioSystem;->getNumStreamTypes()I

    #@8
    move-result v1

    #@9
    .line 2369
    .local v1, numStreamTypes:I
    const/4 v4, 0x0

    #@a
    .local v4, streamType:I
    :goto_a
    if-ge v4, v1, :cond_60

    #@c
    .line 2370
    iget-object v5, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@e
    aget-object v3, v5, v4

    #@10
    .line 2372
    .local v3, streamState:Landroid/media/AudioService$VolumeStreamState;
    if-eqz p1, :cond_1b

    #@12
    iget-object v5, p0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@14
    aget v5, v5, v4

    #@16
    if-ne v5, v8, :cond_1b

    #@18
    .line 2369
    :goto_18
    add-int/lit8 v4, v4, 0x1

    #@1a
    goto :goto_a

    #@1b
    .line 2376
    :cond_1b
    monitor-enter v3

    #@1c
    .line 2377
    :try_start_1c
    invoke-virtual {v3}, Landroid/media/AudioService$VolumeStreamState;->readSettings()V

    #@1f
    .line 2380
    invoke-static {v3}, Landroid/media/AudioService$VolumeStreamState;->access$700(Landroid/media/AudioService$VolumeStreamState;)I

    #@22
    move-result v5

    #@23
    if-eqz v5, :cond_5b

    #@25
    invoke-virtual {p0, v4}, Landroid/media/AudioService;->isStreamAffectedByMute(I)Z

    #@28
    move-result v5

    #@29
    if-nez v5, :cond_5b

    #@2b
    invoke-direct {p0, v4}, Landroid/media/AudioService;->isStreamMutedByRingerMode(I)Z

    #@2e
    move-result v5

    #@2f
    if-nez v5, :cond_5b

    #@31
    .line 2382
    invoke-static {v3}, Landroid/media/AudioService$VolumeStreamState;->access$2100(Landroid/media/AudioService$VolumeStreamState;)Ljava/util/ArrayList;

    #@34
    move-result-object v5

    #@35
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@38
    move-result v2

    #@39
    .line 2383
    .local v2, size:I
    const/4 v0, 0x0

    #@3a
    .local v0, i:I
    :goto_3a
    if-ge v0, v2, :cond_5b

    #@3c
    .line 2384
    invoke-static {v3}, Landroid/media/AudioService$VolumeStreamState;->access$2100(Landroid/media/AudioService$VolumeStreamState;)Ljava/util/ArrayList;

    #@3f
    move-result-object v5

    #@40
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@43
    move-result-object v5

    #@44
    check-cast v5, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;

    #@46
    const/4 v6, 0x1

    #@47
    invoke-static {v5, v6}, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->access$2202(Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;I)I

    #@4a
    .line 2385
    invoke-static {v3}, Landroid/media/AudioService$VolumeStreamState;->access$2100(Landroid/media/AudioService$VolumeStreamState;)Ljava/util/ArrayList;

    #@4d
    move-result-object v5

    #@4e
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@51
    move-result-object v5

    #@52
    check-cast v5, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;

    #@54
    const/4 v6, 0x0

    #@55
    invoke-virtual {v5, v6}, Landroid/media/AudioService$VolumeStreamState$VolumeDeathHandler;->mute(Z)V

    #@58
    .line 2383
    add-int/lit8 v0, v0, 0x1

    #@5a
    goto :goto_3a

    #@5b
    .line 2388
    .end local v0           #i:I
    .end local v2           #size:I
    :cond_5b
    monitor-exit v3

    #@5c
    goto :goto_18

    #@5d
    :catchall_5d
    move-exception v5

    #@5e
    monitor-exit v3
    :try_end_5f
    .catchall {:try_start_1c .. :try_end_5f} :catchall_5d

    #@5f
    throw v5

    #@60
    .line 2393
    .end local v3           #streamState:Landroid/media/AudioService$VolumeStreamState;
    :cond_60
    invoke-virtual {p0}, Landroid/media/AudioService;->getRingerMode()I

    #@63
    move-result v5

    #@64
    invoke-direct {p0, v5, v7}, Landroid/media/AudioService;->setRingerModeInt(IZ)V

    #@67
    .line 2395
    invoke-direct {p0}, Landroid/media/AudioService;->checkAllAliasStreamVolumes()V

    #@6a
    .line 2397
    iget-object v6, p0, Landroid/media/AudioService;->mSafeMediaVolumeState:Ljava/lang/Integer;

    #@6c
    monitor-enter v6

    #@6d
    .line 2398
    :try_start_6d
    iget-object v5, p0, Landroid/media/AudioService;->mSafeMediaVolumeState:Ljava/lang/Integer;

    #@6f
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    #@72
    move-result v5

    #@73
    if-ne v5, v8, :cond_78

    #@75
    .line 2399
    invoke-direct {p0}, Landroid/media/AudioService;->enforceSafeMediaVolume()V

    #@78
    .line 2401
    :cond_78
    monitor-exit v6

    #@79
    .line 2402
    return-void

    #@7a
    .line 2401
    :catchall_7a
    move-exception v5

    #@7b
    monitor-exit v6
    :try_end_7c
    .catchall {:try_start_6d .. :try_end_7c} :catchall_7a

    #@7c
    throw v5
.end method

.method private readDockAudioSettings(Landroid/content/ContentResolver;)V
    .registers 9
    .parameter "cr"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 791
    const-string v1, "dock_audio_media_enabled"

    #@4
    invoke-static {p1, v1, v6}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@7
    move-result v1

    #@8
    if-ne v1, v0, :cond_27

    #@a
    :goto_a
    iput-boolean v0, p0, Landroid/media/AudioService;->mDockAudioMediaEnabled:Z

    #@c
    .line 794
    iget-boolean v0, p0, Landroid/media/AudioService;->mDockAudioMediaEnabled:Z

    #@e
    if-eqz v0, :cond_29

    #@10
    .line 795
    iget v0, p0, Landroid/media/AudioService;->mBecomingNoisyIntentDevices:I

    #@12
    or-int/lit16 v0, v0, 0x800

    #@14
    iput v0, p0, Landroid/media/AudioService;->mBecomingNoisyIntentDevices:I

    #@16
    .line 800
    :goto_16
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@18
    const/16 v1, 0x9

    #@1a
    const/4 v2, 0x2

    #@1b
    const/4 v3, 0x3

    #@1c
    iget-boolean v4, p0, Landroid/media/AudioService;->mDockAudioMediaEnabled:Z

    #@1e
    if-eqz v4, :cond_30

    #@20
    const/16 v4, 0x8

    #@22
    :goto_22
    const/4 v5, 0x0

    #@23
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@26
    .line 808
    return-void

    #@27
    :cond_27
    move v0, v6

    #@28
    .line 791
    goto :goto_a

    #@29
    .line 797
    :cond_29
    iget v0, p0, Landroid/media/AudioService;->mBecomingNoisyIntentDevices:I

    #@2b
    and-int/lit16 v0, v0, -0x801

    #@2d
    iput v0, p0, Landroid/media/AudioService;->mBecomingNoisyIntentDevices:I

    #@2f
    goto :goto_16

    #@30
    :cond_30
    move v4, v6

    #@31
    .line 800
    goto :goto_22
.end method

.method private readPersistedSettings()V
    .registers 15

    #@0
    .prologue
    const/4 v8, 0x2

    #@1
    const/4 v13, -0x2

    #@2
    const/4 v6, 0x1

    #@3
    const/4 v7, 0x0

    #@4
    .line 811
    iget-object v1, p0, Landroid/media/AudioService;->mContentResolver:Landroid/content/ContentResolver;

    #@6
    .line 813
    .local v1, cr:Landroid/content/ContentResolver;
    const-string/jumbo v9, "mode_ringer"

    #@9
    invoke-static {v1, v9, v8}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@c
    move-result v5

    #@d
    .line 816
    .local v5, ringerModeFromSettings:I
    move v4, v5

    #@e
    .line 819
    .local v4, ringerMode:I
    invoke-static {v4}, Landroid/media/AudioManager;->isValidRingerMode(I)Z

    #@11
    move-result v9

    #@12
    if-nez v9, :cond_15

    #@14
    .line 820
    const/4 v4, 0x2

    #@15
    .line 822
    :cond_15
    if-ne v4, v6, :cond_1c

    #@17
    iget-boolean v9, p0, Landroid/media/AudioService;->mHasVibrator:Z

    #@19
    if-nez v9, :cond_1c

    #@1b
    .line 823
    const/4 v4, 0x0

    #@1c
    .line 825
    :cond_1c
    if-eq v4, v5, :cond_24

    #@1e
    .line 826
    const-string/jumbo v9, "mode_ringer"

    #@21
    invoke-static {v1, v9, v4}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@24
    .line 828
    :cond_24
    iget-object v10, p0, Landroid/media/AudioService;->mSettingsLock:Ljava/lang/Object;

    #@26
    monitor-enter v10

    #@27
    .line 829
    :try_start_27
    iput v4, p0, Landroid/media/AudioService;->mRingerMode:I

    #@29
    .line 834
    const/4 v11, 0x0

    #@2a
    const/4 v12, 0x1

    #@2b
    iget-boolean v9, p0, Landroid/media/AudioService;->mHasVibrator:Z

    #@2d
    if-eqz v9, :cond_ea

    #@2f
    move v9, v8

    #@30
    :goto_30
    invoke-static {v11, v12, v9}, Landroid/media/AudioService;->getValueForVibrateSetting(III)I

    #@33
    move-result v9

    #@34
    iput v9, p0, Landroid/media/AudioService;->mVibrateSetting:I

    #@36
    .line 838
    iget v9, p0, Landroid/media/AudioService;->mVibrateSetting:I

    #@38
    const/4 v11, 0x0

    #@39
    iget-boolean v12, p0, Landroid/media/AudioService;->mHasVibrator:Z

    #@3b
    if-eqz v12, :cond_ed

    #@3d
    :goto_3d
    invoke-static {v9, v11, v8}, Landroid/media/AudioService;->getValueForVibrateSetting(III)I

    #@40
    move-result v8

    #@41
    iput v8, p0, Landroid/media/AudioService;->mVibrateSetting:I

    #@43
    .line 845
    const-string/jumbo v8, "mode_ringer_streams_affected"

    #@46
    const/16 v9, 0xa6

    #@48
    const/4 v11, -0x2

    #@49
    invoke-static {v1, v8, v9, v11}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@4c
    move-result v8

    #@4d
    iput v8, p0, Landroid/media/AudioService;->mRingerModeAffectedStreams:I

    #@4f
    .line 852
    iget v8, p0, Landroid/media/AudioService;->mRingerModeAffectedStreams:I

    #@51
    or-int/lit8 v8, v8, 0x26

    #@53
    iput v8, p0, Landroid/media/AudioService;->mRingerModeAffectedStreams:I

    #@55
    .line 856
    iget-boolean v8, p0, Landroid/media/AudioService;->mVoiceCapable:Z

    #@57
    if-eqz v8, :cond_f0

    #@59
    .line 857
    iget v8, p0, Landroid/media/AudioService;->mRingerModeAffectedStreams:I

    #@5b
    and-int/lit8 v8, v8, -0x9

    #@5d
    iput v8, p0, Landroid/media/AudioService;->mRingerModeAffectedStreams:I

    #@5f
    .line 861
    :goto_5f
    iget-object v8, p0, Landroid/media/AudioService;->mCameraSoundForced:Ljava/lang/Boolean;

    #@61
    monitor-enter v8
    :try_end_62
    .catchall {:try_start_27 .. :try_end_62} :catchall_f8

    #@62
    .line 862
    :try_start_62
    iget-object v9, p0, Landroid/media/AudioService;->mCameraSoundForced:Ljava/lang/Boolean;

    #@64
    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    #@67
    move-result v9

    #@68
    if-eqz v9, :cond_fb

    #@6a
    .line 863
    iget v9, p0, Landroid/media/AudioService;->mRingerModeAffectedStreams:I

    #@6c
    and-int/lit16 v9, v9, -0x81

    #@6e
    iput v9, p0, Landroid/media/AudioService;->mRingerModeAffectedStreams:I

    #@70
    .line 867
    :goto_70
    monitor-exit v8
    :try_end_71
    .catchall {:try_start_62 .. :try_end_71} :catchall_103

    #@71
    .line 870
    :try_start_71
    iget-boolean v8, p0, Landroid/media/AudioService;->mIsSoundException:Z

    #@73
    if-eqz v8, :cond_7b

    #@75
    .line 871
    iget v8, p0, Landroid/media/AudioService;->mRingerModeAffectedStreams:I

    #@77
    and-int/lit8 v8, v8, -0x5

    #@79
    iput v8, p0, Landroid/media/AudioService;->mRingerModeAffectedStreams:I

    #@7b
    .line 875
    :cond_7b
    const-string/jumbo v8, "mode_ringer_streams_affected"

    #@7e
    iget v9, p0, Landroid/media/AudioService;->mRingerModeAffectedStreams:I

    #@80
    const/4 v11, -0x2

    #@81
    invoke-static {v1, v8, v9, v11}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    #@84
    .line 880
    invoke-direct {p0, v1}, Landroid/media/AudioService;->readDockAudioSettings(Landroid/content/ContentResolver;)V

    #@87
    .line 881
    monitor-exit v10
    :try_end_88
    .catchall {:try_start_71 .. :try_end_88} :catchall_f8

    #@88
    .line 883
    const-string/jumbo v8, "mute_streams_affected"

    #@8b
    const/16 v9, 0xe

    #@8d
    invoke-static {v1, v8, v9, v13}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@90
    move-result v8

    #@91
    iput v8, p0, Landroid/media/AudioService;->mMuteAffectedStreams:I

    #@93
    .line 890
    iget v8, p0, Landroid/media/AudioService;->mMuteAffectedStreams:I

    #@95
    or-int/lit16 v8, v8, 0x400

    #@97
    iput v8, p0, Landroid/media/AudioService;->mMuteAffectedStreams:I

    #@99
    .line 892
    const-string/jumbo v8, "volume_master_mute"

    #@9c
    invoke-static {v1, v8, v7, v13}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@9f
    move-result v8

    #@a0
    if-ne v8, v6, :cond_106

    #@a2
    move v2, v6

    #@a3
    .line 894
    .local v2, masterMute:Z
    :goto_a3
    invoke-static {v2}, Landroid/media/AudioSystem;->setMasterMute(Z)I

    #@a6
    .line 895
    invoke-direct {p0, v2}, Landroid/media/AudioService;->broadcastMasterMuteStatus(Z)V

    #@a9
    .line 900
    invoke-direct {p0, v4}, Landroid/media/AudioService;->broadcastRingerMode(I)V

    #@ac
    .line 903
    invoke-direct {p0, v7}, Landroid/media/AudioService;->broadcastVibrateSetting(I)V

    #@af
    .line 904
    invoke-direct {p0, v6}, Landroid/media/AudioService;->broadcastVibrateSetting(I)V

    #@b2
    .line 907
    invoke-direct {p0}, Landroid/media/AudioService;->restoreMediaButtonReceiver()V

    #@b5
    .line 910
    const-string/jumbo v6, "mono_audio_settings"

    #@b8
    invoke-static {v1, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@bb
    move-result v3

    #@bc
    .line 911
    .local v3, monoAudioSettingMode:I
    const-string v6, "balance_change_value"

    #@be
    const/16 v7, 0x1f

    #@c0
    invoke-static {v1, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@c3
    move-result v0

    #@c4
    .line 913
    .local v0, balanceChangeValue:I
    if-lez v3, :cond_108

    #@c6
    .line 914
    const-string v6, "MABL_Enable=enable"

    #@c8
    invoke-static {v6}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    #@cb
    .line 918
    :goto_cb
    new-instance v6, Ljava/lang/StringBuilder;

    #@cd
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@d0
    const-string v7, "MABL_CurLvl="

    #@d2
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v6

    #@d6
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v6

    #@da
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dd
    move-result-object v6

    #@de
    invoke-static {v6}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    #@e1
    .line 920
    invoke-static {v3}, Landroid/media/AudioSystem;->setMABLEnable(I)I

    #@e4
    .line 921
    const/16 v6, 0x3e

    #@e6
    invoke-static {v0, v6}, Landroid/media/AudioSystem;->setMABLControl(II)I

    #@e9
    .line 923
    return-void

    #@ea
    .end local v0           #balanceChangeValue:I
    .end local v2           #masterMute:Z
    .end local v3           #monoAudioSettingMode:I
    :cond_ea
    move v9, v7

    #@eb
    .line 834
    goto/16 :goto_30

    #@ed
    :cond_ed
    move v8, v7

    #@ee
    .line 838
    goto/16 :goto_3d

    #@f0
    .line 859
    :cond_f0
    :try_start_f0
    iget v8, p0, Landroid/media/AudioService;->mRingerModeAffectedStreams:I

    #@f2
    or-int/lit8 v8, v8, 0x8

    #@f4
    iput v8, p0, Landroid/media/AudioService;->mRingerModeAffectedStreams:I

    #@f6
    goto/16 :goto_5f

    #@f8
    .line 881
    :catchall_f8
    move-exception v6

    #@f9
    monitor-exit v10
    :try_end_fa
    .catchall {:try_start_f0 .. :try_end_fa} :catchall_f8

    #@fa
    throw v6

    #@fb
    .line 865
    :cond_fb
    :try_start_fb
    iget v9, p0, Landroid/media/AudioService;->mRingerModeAffectedStreams:I

    #@fd
    or-int/lit16 v9, v9, 0x80

    #@ff
    iput v9, p0, Landroid/media/AudioService;->mRingerModeAffectedStreams:I

    #@101
    goto/16 :goto_70

    #@103
    .line 867
    :catchall_103
    move-exception v6

    #@104
    monitor-exit v8
    :try_end_105
    .catchall {:try_start_fb .. :try_end_105} :catchall_103

    #@105
    :try_start_105
    throw v6
    :try_end_106
    .catchall {:try_start_105 .. :try_end_106} :catchall_f8

    #@106
    :cond_106
    move v2, v7

    #@107
    .line 892
    goto :goto_a3

    #@108
    .line 916
    .restart local v0       #balanceChangeValue:I
    .restart local v2       #masterMute:Z
    .restart local v3       #monoAudioSettingMode:I
    :cond_108
    const-string v6, "MABL_Enable=disable"

    #@10a
    invoke-static {v6}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    #@10d
    goto :goto_cb
.end method

.method private refreshVolumePanel(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 7298
    iget-object v0, p0, Landroid/media/AudioService;->mVolumePanel:Landroid/view/VolumePanel;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 7299
    iget-object v0, p0, Landroid/media/AudioService;->mVolumePanel:Landroid/view/VolumePanel;

    #@6
    invoke-virtual {v0}, Landroid/view/VolumePanel;->destoryVolumePanel()V

    #@9
    .line 7302
    :cond_9
    new-instance v0, Landroid/view/VolumePanel;

    #@b
    invoke-direct {v0, p1, p0}, Landroid/view/VolumePanel;-><init>(Landroid/content/Context;Landroid/media/AudioService;)V

    #@e
    iput-object v0, p0, Landroid/media/AudioService;->mVolumePanel:Landroid/view/VolumePanel;

    #@10
    .line 7303
    return-void
.end method

.method private removeFocusStackEntry(Ljava/lang/String;Z)V
    .registers 8
    .parameter "clientToRemove"
    .parameter "signal"

    #@0
    .prologue
    .line 4969
    iget-object v2, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@2
    invoke-virtual {v2}, Ljava/util/Stack;->empty()Z

    #@5
    move-result v2

    #@6
    if-nez v2, :cond_35

    #@8
    iget-object v2, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@a
    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    #@d
    move-result-object v2

    #@e
    check-cast v2, Landroid/media/AudioService$FocusStackEntry;

    #@10
    iget-object v2, v2, Landroid/media/AudioService$FocusStackEntry;->mClientId:Ljava/lang/String;

    #@12
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v2

    #@16
    if-eqz v2, :cond_35

    #@18
    .line 4972
    iget-object v2, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@1a
    invoke-virtual {v2}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    #@1d
    move-result-object v0

    #@1e
    check-cast v0, Landroid/media/AudioService$FocusStackEntry;

    #@20
    .line 4973
    .local v0, fse:Landroid/media/AudioService$FocusStackEntry;
    invoke-virtual {v0}, Landroid/media/AudioService$FocusStackEntry;->unlinkToDeath()V

    #@23
    .line 4974
    if-eqz p2, :cond_31

    #@25
    .line 4976
    invoke-direct {p0}, Landroid/media/AudioService;->notifyTopOfAudioFocusStack()V

    #@28
    .line 4978
    iget-object v3, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@2a
    monitor-enter v3

    #@2b
    .line 4979
    const/16 v2, 0xf

    #@2d
    :try_start_2d
    invoke-direct {p0, v2}, Landroid/media/AudioService;->checkUpdateRemoteControlDisplay_syncAfRcs(I)V

    #@30
    .line 4980
    monitor-exit v3

    #@31
    .line 4996
    .end local v0           #fse:Landroid/media/AudioService$FocusStackEntry;
    :cond_31
    return-void

    #@32
    .line 4980
    .restart local v0       #fse:Landroid/media/AudioService$FocusStackEntry;
    :catchall_32
    move-exception v2

    #@33
    monitor-exit v3
    :try_end_34
    .catchall {:try_start_2d .. :try_end_34} :catchall_32

    #@34
    throw v2

    #@35
    .line 4985
    .end local v0           #fse:Landroid/media/AudioService$FocusStackEntry;
    :cond_35
    iget-object v2, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@37
    invoke-virtual {v2}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    #@3a
    move-result-object v1

    #@3b
    .line 4986
    .local v1, stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$FocusStackEntry;>;"
    :cond_3b
    :goto_3b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@3e
    move-result v2

    #@3f
    if-eqz v2, :cond_31

    #@41
    .line 4987
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@44
    move-result-object v0

    #@45
    check-cast v0, Landroid/media/AudioService$FocusStackEntry;

    #@47
    .line 4988
    .restart local v0       #fse:Landroid/media/AudioService$FocusStackEntry;
    iget-object v2, v0, Landroid/media/AudioService$FocusStackEntry;->mClientId:Ljava/lang/String;

    #@49
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4c
    move-result v2

    #@4d
    if-eqz v2, :cond_3b

    #@4f
    .line 4989
    const-string v2, "AudioService"

    #@51
    new-instance v3, Ljava/lang/StringBuilder;

    #@53
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    const-string v4, " AudioFocus  abandonAudioFocus(): removing entry for "

    #@58
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v3

    #@5c
    iget-object v4, v0, Landroid/media/AudioService$FocusStackEntry;->mClientId:Ljava/lang/String;

    #@5e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v3

    #@62
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v3

    #@66
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@69
    .line 4991
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    #@6c
    .line 4992
    invoke-virtual {v0}, Landroid/media/AudioService$FocusStackEntry;->unlinkToDeath()V

    #@6f
    goto :goto_3b
.end method

.method private removeFocusStackEntryForClient(Landroid/os/IBinder;)V
    .registers 8
    .parameter "cb"

    #@0
    .prologue
    .line 5005
    iget-object v3, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@2
    invoke-virtual {v3}, Ljava/util/Stack;->isEmpty()Z

    #@5
    move-result v3

    #@6
    if-nez v3, :cond_51

    #@8
    iget-object v3, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@a
    invoke-virtual {v3}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    #@d
    move-result-object v3

    #@e
    check-cast v3, Landroid/media/AudioService$FocusStackEntry;

    #@10
    iget-object v3, v3, Landroid/media/AudioService$FocusStackEntry;->mSourceRef:Landroid/os/IBinder;

    #@12
    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v3

    #@16
    if-eqz v3, :cond_51

    #@18
    const/4 v1, 0x1

    #@19
    .line 5007
    .local v1, isTopOfStackForClientToRemove:Z
    :goto_19
    iget-object v3, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@1b
    invoke-virtual {v3}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    #@1e
    move-result-object v2

    #@1f
    .line 5008
    .local v2, stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$FocusStackEntry;>;"
    :cond_1f
    :goto_1f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@22
    move-result v3

    #@23
    if-eqz v3, :cond_53

    #@25
    .line 5009
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@28
    move-result-object v0

    #@29
    check-cast v0, Landroid/media/AudioService$FocusStackEntry;

    #@2b
    .line 5010
    .local v0, fse:Landroid/media/AudioService$FocusStackEntry;
    iget-object v3, v0, Landroid/media/AudioService$FocusStackEntry;->mSourceRef:Landroid/os/IBinder;

    #@2d
    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v3

    #@31
    if-eqz v3, :cond_1f

    #@33
    .line 5011
    const-string v3, "AudioService"

    #@35
    new-instance v4, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v5, " AudioFocus  abandonAudioFocus(): removing entry for "

    #@3c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v4

    #@40
    iget-object v5, v0, Landroid/media/AudioService$FocusStackEntry;->mClientId:Ljava/lang/String;

    #@42
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v4

    #@46
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v4

    #@4a
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    .line 5013
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    #@50
    goto :goto_1f

    #@51
    .line 5005
    .end local v0           #fse:Landroid/media/AudioService$FocusStackEntry;
    .end local v1           #isTopOfStackForClientToRemove:Z
    .end local v2           #stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$FocusStackEntry;>;"
    :cond_51
    const/4 v1, 0x0

    #@52
    goto :goto_19

    #@53
    .line 5017
    .restart local v1       #isTopOfStackForClientToRemove:Z
    .restart local v2       #stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$FocusStackEntry;>;"
    :cond_53
    if-eqz v1, :cond_61

    #@55
    .line 5020
    invoke-direct {p0}, Landroid/media/AudioService;->notifyTopOfAudioFocusStack()V

    #@58
    .line 5022
    iget-object v4, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@5a
    monitor-enter v4

    #@5b
    .line 5023
    const/16 v3, 0xf

    #@5d
    :try_start_5d
    invoke-direct {p0, v3}, Landroid/media/AudioService;->checkUpdateRemoteControlDisplay_syncAfRcs(I)V

    #@60
    .line 5024
    monitor-exit v4

    #@61
    .line 5026
    :cond_61
    return-void

    #@62
    .line 5024
    :catchall_62
    move-exception v3

    #@63
    monitor-exit v4
    :try_end_64
    .catchall {:try_start_5d .. :try_end_64} :catchall_62

    #@64
    throw v3
.end method

.method private removeMediaButtonReceiver(Landroid/app/PendingIntent;)V
    .registers 5
    .parameter "pi"

    #@0
    .prologue
    .line 5846
    iget-object v2, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@2
    invoke-virtual {v2}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .line 5847
    .local v1, stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_20

    #@c
    .line 5848
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/media/AudioService$RemoteControlStackEntry;

    #@12
    .line 5849
    .local v0, rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    iget-object v2, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mMediaIntent:Landroid/app/PendingIntent;

    #@14
    invoke-virtual {v2, p1}, Landroid/app/PendingIntent;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v2

    #@18
    if-eqz v2, :cond_6

    #@1a
    .line 5850
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    #@1d
    .line 5851
    invoke-virtual {v0}, Landroid/media/AudioService$RemoteControlStackEntry;->unlinkToRcClientDeath()V

    #@20
    .line 5855
    .end local v0           #rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    :cond_20
    return-void
.end method

.method private removeMediaButtonReceiverForPackage(Ljava/lang/String;)V
    .registers 12
    .parameter "packageName"

    #@0
    .prologue
    .line 5758
    iget-object v4, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@2
    monitor-enter v4

    #@3
    .line 5759
    :try_start_3
    iget-object v3, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@5
    invoke-virtual {v3}, Ljava/util/Stack;->empty()Z

    #@8
    move-result v3

    #@9
    if-eqz v3, :cond_d

    #@b
    .line 5760
    monitor-exit v4

    #@c
    .line 5787
    :goto_c
    return-void

    #@d
    .line 5762
    :cond_d
    iget-object v3, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@f
    invoke-virtual {v3}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Landroid/media/AudioService$RemoteControlStackEntry;

    #@15
    .line 5763
    .local v0, oldTop:Landroid/media/AudioService$RemoteControlStackEntry;
    iget-object v3, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@17
    invoke-virtual {v3}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    #@1a
    move-result-object v2

    #@1b
    .line 5765
    .local v2, stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :cond_1b
    :goto_1b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_3d

    #@21
    .line 5766
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@24
    move-result-object v1

    #@25
    check-cast v1, Landroid/media/AudioService$RemoteControlStackEntry;

    #@27
    .line 5767
    .local v1, rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    iget-object v3, v1, Landroid/media/AudioService$RemoteControlStackEntry;->mReceiverComponent:Landroid/content/ComponentName;

    #@29
    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@30
    move-result v3

    #@31
    if-eqz v3, :cond_1b

    #@33
    .line 5769
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    #@36
    .line 5770
    invoke-virtual {v1}, Landroid/media/AudioService$RemoteControlStackEntry;->unlinkToRcClientDeath()V

    #@39
    goto :goto_1b

    #@3a
    .line 5786
    .end local v0           #oldTop:Landroid/media/AudioService$RemoteControlStackEntry;
    .end local v1           #rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    .end local v2           #stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :catchall_3a
    move-exception v3

    #@3b
    monitor-exit v4
    :try_end_3c
    .catchall {:try_start_3 .. :try_end_3c} :catchall_3a

    #@3c
    throw v3

    #@3d
    .line 5773
    .restart local v0       #oldTop:Landroid/media/AudioService$RemoteControlStackEntry;
    .restart local v2       #stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :cond_3d
    :try_start_3d
    iget-object v3, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@3f
    invoke-virtual {v3}, Ljava/util/Stack;->empty()Z

    #@42
    move-result v3

    #@43
    if-eqz v3, :cond_57

    #@45
    .line 5775
    iget-object v3, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@47
    iget-object v5, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@49
    const/16 v6, 0xa

    #@4b
    const/4 v7, 0x0

    #@4c
    const/4 v8, 0x0

    #@4d
    const/4 v9, 0x0

    #@4e
    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/media/AudioService$AudioHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@51
    move-result-object v5

    #@52
    invoke-virtual {v3, v5}, Landroid/media/AudioService$AudioHandler;->sendMessage(Landroid/os/Message;)Z

    #@55
    .line 5786
    :cond_55
    :goto_55
    monitor-exit v4

    #@56
    goto :goto_c

    #@57
    .line 5778
    :cond_57
    iget-object v3, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@59
    invoke-virtual {v3}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    #@5c
    move-result-object v3

    #@5d
    if-eq v0, v3, :cond_55

    #@5f
    .line 5781
    iget-object v5, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@61
    iget-object v6, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@63
    const/16 v7, 0xa

    #@65
    const/4 v8, 0x0

    #@66
    const/4 v9, 0x0

    #@67
    iget-object v3, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@69
    invoke-virtual {v3}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    #@6c
    move-result-object v3

    #@6d
    check-cast v3, Landroid/media/AudioService$RemoteControlStackEntry;

    #@6f
    iget-object v3, v3, Landroid/media/AudioService$RemoteControlStackEntry;->mReceiverComponent:Landroid/content/ComponentName;

    #@71
    invoke-virtual {v6, v7, v8, v9, v3}, Landroid/media/AudioService$AudioHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@74
    move-result-object v3

    #@75
    invoke-virtual {v5, v3}, Landroid/media/AudioService$AudioHandler;->sendMessage(Landroid/os/Message;)Z
    :try_end_78
    .catchall {:try_start_3d .. :try_end_78} :catchall_3a

    #@78
    goto :goto_55
.end method

.method private rescaleIndex(III)I
    .registers 6
    .parameter "index"
    .parameter "srcStream"
    .parameter "dstStream"

    #@0
    .prologue
    .line 926
    iget-object v0, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@2
    aget-object v0, v0, p3

    #@4
    invoke-virtual {v0}, Landroid/media/AudioService$VolumeStreamState;->getMaxIndex()I

    #@7
    move-result v0

    #@8
    mul-int/2addr v0, p1

    #@9
    iget-object v1, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@b
    aget-object v1, v1, p2

    #@d
    invoke-virtual {v1}, Landroid/media/AudioService$VolumeStreamState;->getMaxIndex()I

    #@10
    move-result v1

    #@11
    div-int/lit8 v1, v1, 0x2

    #@13
    add-int/2addr v0, v1

    #@14
    iget-object v1, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@16
    aget-object v1, v1, p2

    #@18
    invoke-virtual {v1}, Landroid/media/AudioService$VolumeStreamState;->getMaxIndex()I

    #@1b
    move-result v1

    #@1c
    div-int/2addr v0, v1

    #@1d
    return v0
.end method

.method private resetBluetoothSco()V
    .registers 4

    #@0
    .prologue
    .line 2747
    iget-object v1, p0, Landroid/media/AudioService;->mScoClients:Ljava/util/ArrayList;

    #@2
    monitor-enter v1

    #@3
    .line 2748
    const/4 v0, 0x0

    #@4
    const/4 v2, 0x0

    #@5
    :try_start_5
    invoke-virtual {p0, v0, v2}, Landroid/media/AudioService;->clearAllScoClients(IZ)V

    #@8
    .line 2749
    const/4 v0, 0x0

    #@9
    iput v0, p0, Landroid/media/AudioService;->mScoAudioState:I

    #@b
    .line 2750
    const/4 v0, 0x0

    #@c
    invoke-direct {p0, v0}, Landroid/media/AudioService;->broadcastScoConnectionState(I)V

    #@f
    .line 2751
    monitor-exit v1

    #@10
    .line 2752
    return-void

    #@11
    .line 2751
    :catchall_11
    move-exception v0

    #@12
    monitor-exit v1
    :try_end_13
    .catchall {:try_start_5 .. :try_end_13} :catchall_11

    #@13
    throw v0
.end method

.method private restoreMasterVolume()V
    .registers 6

    #@0
    .prologue
    .line 1743
    iget-boolean v1, p0, Landroid/media/AudioService;->mUseMasterVolume:Z

    #@2
    if-eqz v1, :cond_18

    #@4
    .line 1744
    iget-object v1, p0, Landroid/media/AudioService;->mContentResolver:Landroid/content/ContentResolver;

    #@6
    const-string/jumbo v2, "volume_master"

    #@9
    const/high16 v3, -0x4080

    #@b
    const/4 v4, -0x2

    #@c
    invoke-static {v1, v2, v3, v4}, Landroid/provider/Settings$System;->getFloatForUser(Landroid/content/ContentResolver;Ljava/lang/String;FI)F

    #@f
    move-result v0

    #@10
    .line 1746
    .local v0, volume:F
    const/4 v1, 0x0

    #@11
    cmpl-float v1, v0, v1

    #@13
    if-ltz v1, :cond_18

    #@15
    .line 1747
    invoke-static {v0}, Landroid/media/AudioSystem;->setMasterVolume(F)I

    #@18
    .line 1750
    .end local v0           #volume:F
    :cond_18
    return-void
.end method

.method private restoreMediaButtonReceiver()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 5794
    iget-object v4, p0, Landroid/media/AudioService;->mContentResolver:Landroid/content/ContentResolver;

    #@3
    const-string/jumbo v5, "media_button_receiver"

    #@6
    const/4 v6, -0x2

    #@7
    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@a
    move-result-object v3

    #@b
    .line 5796
    .local v3, receiverName:Ljava/lang/String;
    if-eqz v3, :cond_2a

    #@d
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    #@10
    move-result v4

    #@11
    if-nez v4, :cond_2a

    #@13
    .line 5797
    invoke-static {v3}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    #@16
    move-result-object v0

    #@17
    .line 5800
    .local v0, eventReceiver:Landroid/content/ComponentName;
    new-instance v1, Landroid/content/Intent;

    #@19
    const-string v4, "android.intent.action.MEDIA_BUTTON"

    #@1b
    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1e
    .line 5802
    .local v1, mediaButtonIntent:Landroid/content/Intent;
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@21
    .line 5803
    iget-object v4, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@23
    invoke-static {v4, v7, v1, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@26
    move-result-object v2

    #@27
    .line 5805
    .local v2, pi:Landroid/app/PendingIntent;
    invoke-virtual {p0, v2, v0}, Landroid/media/AudioService;->registerMediaButtonIntent(Landroid/app/PendingIntent;Landroid/content/ComponentName;)V

    #@2a
    .line 5807
    .end local v0           #eventReceiver:Landroid/content/ComponentName;
    .end local v1           #mediaButtonIntent:Landroid/content/Intent;
    .end local v2           #pi:Landroid/app/PendingIntent;
    :cond_2a
    return-void
.end method

.method private sendBroadcastToAll(Landroid/content/Intent;)V
    .registers 6
    .parameter "intent"

    #@0
    .prologue
    .line 1401
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v0

    #@4
    .line 1403
    .local v0, ident:J
    :try_start_4
    iget-object v2, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@6
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@8
    invoke-virtual {v2, p1, v3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_b
    .catchall {:try_start_4 .. :try_end_b} :catchall_f

    #@b
    .line 1405
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@e
    .line 1407
    return-void

    #@f
    .line 1405
    :catchall_f
    move-exception v2

    #@10
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@13
    throw v2
.end method

.method private sendDeviceConnectionIntent(IILjava/lang/String;)V
    .registers 18
    .parameter "device"
    .parameter "state"
    .parameter "name"

    #@0
    .prologue
    .line 4403
    new-instance v11, Landroid/content/Intent;

    #@2
    invoke-direct {v11}, Landroid/content/Intent;-><init>()V

    #@5
    .line 4405
    .local v11, intent:Landroid/content/Intent;
    const-string/jumbo v1, "state"

    #@8
    move/from16 v0, p2

    #@a
    invoke-virtual {v11, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@d
    .line 4406
    const-string/jumbo v1, "name"

    #@10
    move-object/from16 v0, p3

    #@12
    invoke-virtual {v11, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@15
    .line 4407
    const/high16 v1, 0x4000

    #@17
    invoke-virtual {v11, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@1a
    .line 4409
    const/4 v8, 0x0

    #@1b
    .line 4411
    .local v8, connType:I
    const/4 v1, 0x4

    #@1c
    if-ne p1, v1, :cond_5b

    #@1e
    .line 4412
    const/4 v8, 0x1

    #@1f
    .line 4413
    const-string v1, "android.intent.action.HEADSET_PLUG"

    #@21
    invoke-virtual {v11, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@24
    .line 4414
    const-string/jumbo v1, "microphone"

    #@27
    const/4 v2, 0x1

    #@28
    invoke-virtual {v11, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@2b
    .line 4430
    :cond_2b
    :goto_2b
    iget-object v13, p0, Landroid/media/AudioService;->mCurAudioRoutes:Landroid/media/AudioRoutesInfo;

    #@2d
    monitor-enter v13

    #@2e
    .line 4431
    if-eqz v8, :cond_4d

    #@30
    .line 4432
    :try_start_30
    iget-object v1, p0, Landroid/media/AudioService;->mCurAudioRoutes:Landroid/media/AudioRoutesInfo;

    #@32
    iget v12, v1, Landroid/media/AudioRoutesInfo;->mMainType:I

    #@34
    .line 4433
    .local v12, newConn:I
    if-eqz p2, :cond_8f

    #@36
    .line 4434
    or-int/2addr v12, v8

    #@37
    .line 4438
    :goto_37
    iget-object v1, p0, Landroid/media/AudioService;->mCurAudioRoutes:Landroid/media/AudioRoutesInfo;

    #@39
    iget v1, v1, Landroid/media/AudioRoutesInfo;->mMainType:I

    #@3b
    if-eq v12, v1, :cond_4d

    #@3d
    .line 4439
    iget-object v1, p0, Landroid/media/AudioService;->mCurAudioRoutes:Landroid/media/AudioRoutesInfo;

    #@3f
    iput v12, v1, Landroid/media/AudioRoutesInfo;->mMainType:I

    #@41
    .line 4440
    iget-object v1, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@43
    const/16 v2, 0x10

    #@45
    const/4 v3, 0x1

    #@46
    const/4 v4, 0x0

    #@47
    const/4 v5, 0x0

    #@48
    const/4 v6, 0x0

    #@49
    const/4 v7, 0x0

    #@4a
    invoke-static/range {v1 .. v7}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@4d
    .line 4444
    .end local v12           #newConn:I
    :cond_4d
    monitor-exit v13
    :try_end_4e
    .catchall {:try_start_30 .. :try_end_4e} :catchall_93

    #@4e
    .line 4446
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@51
    move-result-wide v9

    #@52
    .line 4448
    .local v9, ident:J
    const/4 v1, 0x0

    #@53
    const/4 v2, -0x1

    #@54
    :try_start_54
    invoke-static {v11, v1, v2}, Landroid/app/ActivityManagerNative;->broadcastStickyIntent(Landroid/content/Intent;Ljava/lang/String;I)V
    :try_end_57
    .catchall {:try_start_54 .. :try_end_57} :catchall_96

    #@57
    .line 4450
    invoke-static {v9, v10}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@5a
    .line 4452
    return-void

    #@5b
    .line 4415
    .end local v9           #ident:J
    :cond_5b
    const/16 v1, 0x8

    #@5d
    if-ne p1, v1, :cond_6d

    #@5f
    .line 4416
    const/4 v8, 0x2

    #@60
    .line 4417
    const-string v1, "android.intent.action.HEADSET_PLUG"

    #@62
    invoke-virtual {v11, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@65
    .line 4418
    const-string/jumbo v1, "microphone"

    #@68
    const/4 v2, 0x0

    #@69
    invoke-virtual {v11, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@6c
    goto :goto_2b

    #@6d
    .line 4419
    :cond_6d
    const/16 v1, 0x800

    #@6f
    if-ne p1, v1, :cond_78

    #@71
    .line 4420
    const/4 v8, 0x4

    #@72
    .line 4421
    const-string v1, "android.intent.action.ANALOG_AUDIO_DOCK_PLUG"

    #@74
    invoke-virtual {v11, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@77
    goto :goto_2b

    #@78
    .line 4422
    :cond_78
    const/16 v1, 0x1000

    #@7a
    if-ne p1, v1, :cond_83

    #@7c
    .line 4423
    const/4 v8, 0x4

    #@7d
    .line 4424
    const-string v1, "android.intent.action.DIGITAL_AUDIO_DOCK_PLUG"

    #@7f
    invoke-virtual {v11, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@82
    goto :goto_2b

    #@83
    .line 4425
    :cond_83
    const/16 v1, 0x400

    #@85
    if-ne p1, v1, :cond_2b

    #@87
    .line 4426
    const/16 v8, 0x8

    #@89
    .line 4427
    const-string v1, "android.intent.action.HDMI_AUDIO_PLUG"

    #@8b
    invoke-virtual {v11, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@8e
    goto :goto_2b

    #@8f
    .line 4436
    .restart local v12       #newConn:I
    :cond_8f
    xor-int/lit8 v1, v8, -0x1

    #@91
    and-int/2addr v12, v1

    #@92
    goto :goto_37

    #@93
    .line 4444
    .end local v12           #newConn:I
    :catchall_93
    move-exception v1

    #@94
    :try_start_94
    monitor-exit v13
    :try_end_95
    .catchall {:try_start_94 .. :try_end_95} :catchall_93

    #@95
    throw v1

    #@96
    .line 4450
    .restart local v9       #ident:J
    :catchall_96
    move-exception v1

    #@97
    invoke-static {v9, v10}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@9a
    throw v1
.end method

.method private sendMasterMuteUpdate(ZI)V
    .registers 4
    .parameter "muted"
    .parameter "flags"

    #@0
    .prologue
    .line 1464
    iget-object v0, p0, Landroid/media/AudioService;->mVolumePanel:Landroid/view/VolumePanel;

    #@2
    invoke-virtual {v0, p2}, Landroid/view/VolumePanel;->postMasterMuteChanged(I)V

    #@5
    .line 1465
    invoke-direct {p0, p1}, Landroid/media/AudioService;->broadcastMasterMuteStatus(Z)V

    #@8
    .line 1466
    return-void
.end method

.method private sendMasterVolumeUpdate(III)V
    .registers 6
    .parameter "flags"
    .parameter "oldVolume"
    .parameter "newVolume"

    #@0
    .prologue
    .line 1454
    iget-object v1, p0, Landroid/media/AudioService;->mVolumePanel:Landroid/view/VolumePanel;

    #@2
    invoke-virtual {v1, p1}, Landroid/view/VolumePanel;->postMasterVolumeChanged(I)V

    #@5
    .line 1456
    new-instance v0, Landroid/content/Intent;

    #@7
    const-string v1, "android.media.MASTER_VOLUME_CHANGED_ACTION"

    #@9
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@c
    .line 1457
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "android.media.EXTRA_PREV_MASTER_VOLUME_VALUE"

    #@e
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@11
    .line 1458
    const-string v1, "android.media.EXTRA_MASTER_VOLUME_VALUE"

    #@13
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@16
    .line 1459
    invoke-direct {p0, v0}, Landroid/media/AudioService;->sendBroadcastToAll(Landroid/content/Intent;)V

    #@19
    .line 1460
    return-void
.end method

.method private static sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V
    .registers 10
    .parameter "handler"
    .parameter "msg"
    .parameter "existingMsgPolicy"
    .parameter "arg1"
    .parameter "arg2"
    .parameter "obj"
    .parameter "delay"

    #@0
    .prologue
    .line 3218
    if-nez p2, :cond_e

    #@2
    .line 3219
    invoke-virtual {p0, p1}, Landroid/os/Handler;->removeMessages(I)V

    #@5
    .line 3224
    :cond_5
    invoke-virtual {p0, p1, p3, p4, p5}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    int-to-long v1, p6

    #@a
    invoke-virtual {p0, v0, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@d
    .line 3225
    :goto_d
    return-void

    #@e
    .line 3220
    :cond_e
    const/4 v0, 0x1

    #@f
    if-ne p2, v0, :cond_5

    #@11
    invoke-virtual {p0, p1}, Landroid/os/Handler;->hasMessages(I)Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_5

    #@17
    goto :goto_d
.end method

.method private sendSimulatedMediaButtonEvent(Landroid/view/KeyEvent;Z)V
    .registers 5
    .parameter "originalKeyEvent"
    .parameter "needWakeLock"

    #@0
    .prologue
    .line 5404
    const/4 v1, 0x0

    #@1
    invoke-static {p1, v1}, Landroid/view/KeyEvent;->changeAction(Landroid/view/KeyEvent;I)Landroid/view/KeyEvent;

    #@4
    move-result-object v0

    #@5
    .line 5405
    .local v0, keyEvent:Landroid/view/KeyEvent;
    invoke-direct {p0, v0, p2}, Landroid/media/AudioService;->dispatchMediaKeyEvent(Landroid/view/KeyEvent;Z)V

    #@8
    .line 5407
    const/4 v1, 0x1

    #@9
    invoke-static {p1, v1}, Landroid/view/KeyEvent;->changeAction(Landroid/view/KeyEvent;I)Landroid/view/KeyEvent;

    #@c
    move-result-object v0

    #@d
    .line 5408
    invoke-direct {p0, v0, p2}, Landroid/media/AudioService;->dispatchMediaKeyEvent(Landroid/view/KeyEvent;Z)V

    #@10
    .line 5410
    return-void
.end method

.method private sendStickyBroadcastToAll(Landroid/content/Intent;)V
    .registers 6
    .parameter "intent"

    #@0
    .prologue
    .line 1410
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v0

    #@4
    .line 1412
    .local v0, ident:J
    :try_start_4
    iget-object v2, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@6
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@8
    invoke-virtual {v2, p1, v3}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_b
    .catchall {:try_start_4 .. :try_end_b} :catchall_f

    #@b
    .line 1414
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@e
    .line 1416
    return-void

    #@f
    .line 1414
    :catchall_f
    move-exception v2

    #@10
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@13
    throw v2
.end method

.method private sendVolumeUpdate(IIII)V
    .registers 13
    .parameter "streamType"
    .parameter "oldIndex"
    .parameter "index"
    .parameter "flags"

    #@0
    .prologue
    const/4 v6, 0x3

    #@1
    const/4 v5, 0x1

    #@2
    const/4 v4, 0x0

    #@3
    const/4 v2, 0x2

    #@4
    .line 1420
    iget-boolean v0, p0, Landroid/media/AudioService;->mVoiceCapable:Z

    #@6
    if-nez v0, :cond_b

    #@8
    if-ne p1, v2, :cond_b

    #@a
    .line 1421
    const/4 p1, 0x5

    #@b
    .line 1424
    :cond_b
    iget-object v0, p0, Landroid/media/AudioService;->mVolumePanel:Landroid/view/VolumePanel;

    #@d
    invoke-virtual {v0, p1, p4}, Landroid/view/VolumePanel;->postVolumeChanged(II)V

    #@10
    .line 1427
    iget-object v0, p0, Landroid/media/AudioService;->mSafeMediaVolumeState:Ljava/lang/Integer;

    #@12
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@15
    move-result v0

    #@16
    if-eq v0, v6, :cond_9c

    #@18
    iget-object v0, p0, Landroid/media/AudioService;->mLgSafeMediaVolumeEnabled:Ljava/lang/Boolean;

    #@1a
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    #@1d
    move-result v0

    #@1e
    if-eqz v0, :cond_9c

    #@20
    iget-object v0, p0, Landroid/media/AudioService;->mConnectedDevices:Ljava/util/HashMap;

    #@22
    const/4 v1, 0x4

    #@23
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@2a
    move-result v0

    #@2b
    if-nez v0, :cond_3b

    #@2d
    iget-object v0, p0, Landroid/media/AudioService;->mConnectedDevices:Ljava/util/HashMap;

    #@2f
    const/16 v1, 0x8

    #@31
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@34
    move-result-object v1

    #@35
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@38
    move-result v0

    #@39
    if-eqz v0, :cond_9c

    #@3b
    :cond_3b
    invoke-virtual {p0}, Landroid/media/AudioService;->isBluetoothA2dpOn()Z

    #@3e
    move-result v0

    #@3f
    if-nez v0, :cond_9c

    #@41
    .line 1431
    const-string v0, "AudioService"

    #@43
    new-instance v1, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string/jumbo v3, "stream : "

    #@4b
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v1

    #@4f
    iget-object v3, p0, Landroid/media/AudioService;->STREAM_VOLUME_ALIAS:[I

    #@51
    aget v3, v3, p1

    #@53
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@56
    move-result-object v1

    #@57
    const-string v3, ", oldIndex = "

    #@59
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v1

    #@5d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@60
    move-result-object v1

    #@61
    const-string v3, ", newIndex = "

    #@63
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v1

    #@67
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v1

    #@6b
    const-string v3, ", mSafeMediaVolumeIndex = "

    #@6d
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v1

    #@71
    iget v3, p0, Landroid/media/AudioService;->mSafeMediaVolumeIndex:I

    #@73
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@76
    move-result-object v1

    #@77
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v1

    #@7b
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7e
    .line 1433
    iget-object v0, p0, Landroid/media/AudioService;->STREAM_VOLUME_ALIAS:[I

    #@80
    aget v0, v0, p1

    #@82
    if-ne v0, v6, :cond_9c

    #@84
    iget v0, p0, Landroid/media/AudioService;->mSafeMediaVolumeIndex:I

    #@86
    if-gt p2, v0, :cond_9c

    #@88
    iget v0, p0, Landroid/media/AudioService;->mSafeMediaVolumeIndex:I

    #@8a
    if-le p3, v0, :cond_9c

    #@8c
    invoke-static {v5}, Landroid/media/AudioSystem;->getForceUse(I)I

    #@8f
    move-result v0

    #@90
    if-eq v0, v5, :cond_9c

    #@92
    .line 1436
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@94
    const/16 v1, 0x1d

    #@96
    const/4 v5, 0x0

    #@97
    move v3, v2

    #@98
    move v6, v4

    #@99
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@9c
    .line 1441
    :cond_9c
    and-int/lit8 v0, p4, 0x20

    #@9e
    if-nez v0, :cond_c1

    #@a0
    .line 1442
    add-int/lit8 v0, p2, 0x5

    #@a2
    div-int/lit8 p2, v0, 0xa

    #@a4
    .line 1443
    add-int/lit8 v0, p3, 0x5

    #@a6
    div-int/lit8 p3, v0, 0xa

    #@a8
    .line 1444
    new-instance v7, Landroid/content/Intent;

    #@aa
    const-string v0, "android.media.VOLUME_CHANGED_ACTION"

    #@ac
    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@af
    .line 1445
    .local v7, intent:Landroid/content/Intent;
    const-string v0, "android.media.EXTRA_VOLUME_STREAM_TYPE"

    #@b1
    invoke-virtual {v7, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@b4
    .line 1446
    const-string v0, "android.media.EXTRA_VOLUME_STREAM_VALUE"

    #@b6
    invoke-virtual {v7, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@b9
    .line 1447
    const-string v0, "android.media.EXTRA_PREV_VOLUME_STREAM_VALUE"

    #@bb
    invoke-virtual {v7, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@be
    .line 1448
    invoke-direct {p0, v7}, Landroid/media/AudioService;->sendBroadcastToAll(Landroid/content/Intent;)V

    #@c1
    .line 1450
    .end local v7           #intent:Landroid/content/Intent;
    :cond_c1
    return-void
.end method

.method private sendVolumeUpdateToRemote(II)V
    .registers 10
    .parameter "rccId"
    .parameter "direction"

    #@0
    .prologue
    .line 6550
    const-string v4, "AudioService"

    #@2
    new-instance v5, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v6, "sendVolumeUpdateToRemote(rccId="

    #@a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v5

    #@e
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v5

    #@12
    const-string v6, " , dir="

    #@14
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v5

    #@18
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v5

    #@1c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v5

    #@20
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 6551
    if-nez p2, :cond_26

    #@25
    .line 6574
    :cond_25
    :goto_25
    return-void

    #@26
    .line 6555
    :cond_26
    const/4 v2, 0x0

    #@27
    .line 6556
    .local v2, rvo:Landroid/media/IRemoteVolumeObserver;
    iget-object v5, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@29
    monitor-enter v5

    #@2a
    .line 6557
    :try_start_2a
    iget-object v4, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@2c
    invoke-virtual {v4}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    #@2f
    move-result-object v3

    #@30
    .line 6558
    .local v3, stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :cond_30
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@33
    move-result v4

    #@34
    if-eqz v4, :cond_42

    #@36
    .line 6559
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@39
    move-result-object v1

    #@3a
    check-cast v1, Landroid/media/AudioService$RemoteControlStackEntry;

    #@3c
    .line 6561
    .local v1, rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    iget v4, v1, Landroid/media/AudioService$RemoteControlStackEntry;->mRccId:I

    #@3e
    if-ne v4, p1, :cond_30

    #@40
    .line 6562
    iget-object v2, v1, Landroid/media/AudioService$RemoteControlStackEntry;->mRemoteVolumeObs:Landroid/media/IRemoteVolumeObserver;

    #@42
    .line 6566
    .end local v1           #rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    :cond_42
    monitor-exit v5
    :try_end_43
    .catchall {:try_start_2a .. :try_end_43} :catchall_53

    #@43
    .line 6567
    if-eqz v2, :cond_25

    #@45
    .line 6569
    const/4 v4, -0x1

    #@46
    :try_start_46
    invoke-interface {v2, p2, v4}, Landroid/media/IRemoteVolumeObserver;->dispatchRemoteVolumeUpdate(II)V
    :try_end_49
    .catch Landroid/os/RemoteException; {:try_start_46 .. :try_end_49} :catch_4a

    #@49
    goto :goto_25

    #@4a
    .line 6570
    :catch_4a
    move-exception v0

    #@4b
    .line 6571
    .local v0, e:Landroid/os/RemoteException;
    const-string v4, "AudioService"

    #@4d
    const-string v5, "Error dispatching relative volume update"

    #@4f
    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@52
    goto :goto_25

    #@53
    .line 6566
    .end local v0           #e:Landroid/os/RemoteException;
    .end local v3           #stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :catchall_53
    move-exception v4

    #@54
    :try_start_54
    monitor-exit v5
    :try_end_55
    .catchall {:try_start_54 .. :try_end_55} :catchall_53

    #@55
    throw v4
.end method

.method private setNewRcClientGenerationOnClients_syncRcsCurrc(I)V
    .registers 8
    .parameter "newClientGeneration"

    #@0
    .prologue
    .line 5894
    iget-object v3, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@2
    invoke-virtual {v3}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v2

    #@6
    .line 5895
    .local v2, stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :cond_6
    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v3

    #@a
    if-eqz v3, :cond_3e

    #@c
    .line 5896
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Landroid/media/AudioService$RemoteControlStackEntry;

    #@12
    .line 5897
    .local v1, se:Landroid/media/AudioService$RemoteControlStackEntry;
    if-eqz v1, :cond_6

    #@14
    iget-object v3, v1, Landroid/media/AudioService$RemoteControlStackEntry;->mRcClient:Landroid/media/IRemoteControlClient;

    #@16
    if-eqz v3, :cond_6

    #@18
    .line 5899
    :try_start_18
    iget-object v3, v1, Landroid/media/AudioService$RemoteControlStackEntry;->mRcClient:Landroid/media/IRemoteControlClient;

    #@1a
    invoke-interface {v3, p1}, Landroid/media/IRemoteControlClient;->setCurrentClientGenerationId(I)V
    :try_end_1d
    .catch Landroid/os/RemoteException; {:try_start_18 .. :try_end_1d} :catch_1e

    #@1d
    goto :goto_6

    #@1e
    .line 5900
    :catch_1e
    move-exception v0

    #@1f
    .line 5901
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "AudioService"

    #@21
    new-instance v4, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v5, "Dead client in setNewRcClientGenerationOnClients_syncRcsCurrc()"

    #@28
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v4

    #@2c
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v4

    #@30
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v4

    #@34
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 5902
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    #@3a
    .line 5903
    invoke-virtual {v1}, Landroid/media/AudioService$RemoteControlStackEntry;->unlinkToRcClientDeath()V

    #@3d
    goto :goto_6

    #@3e
    .line 5907
    .end local v0           #e:Landroid/os/RemoteException;
    .end local v1           #se:Landroid/media/AudioService$RemoteControlStackEntry;
    :cond_3e
    return-void
.end method

.method private setNewRcClientOnDisplays_syncRcsCurrc(ILandroid/app/PendingIntent;Z)V
    .registers 8
    .parameter "newClientGeneration"
    .parameter "newMediaIntent"
    .parameter "clearing"

    #@0
    .prologue
    .line 5877
    iget-object v1, p0, Landroid/media/AudioService;->mRcDisplay:Landroid/media/IRemoteControlDisplay;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 5879
    :try_start_4
    iget-object v1, p0, Landroid/media/AudioService;->mRcDisplay:Landroid/media/IRemoteControlDisplay;

    #@6
    invoke-interface {v1, p1, p2, p3}, Landroid/media/IRemoteControlDisplay;->setCurrentClientId(ILandroid/app/PendingIntent;Z)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 5888
    :cond_9
    :goto_9
    return-void

    #@a
    .line 5881
    :catch_a
    move-exception v0

    #@b
    .line 5882
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "AudioService"

    #@d
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v3, "Dead display in setNewRcClientOnDisplays_syncRcsCurrc() "

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 5884
    invoke-direct {p0}, Landroid/media/AudioService;->rcDisplay_stopDeathMonitor_syncRcStack()V

    #@26
    .line 5885
    const/4 v1, 0x0

    #@27
    iput-object v1, p0, Landroid/media/AudioService;->mRcDisplay:Landroid/media/IRemoteControlDisplay;

    #@29
    goto :goto_9
.end method

.method private setNewRcClient_syncRcsCurrc(ILandroid/app/PendingIntent;Z)V
    .registers 4
    .parameter "newClientGeneration"
    .parameter "newMediaIntent"
    .parameter "clearing"

    #@0
    .prologue
    .line 5920
    invoke-direct {p0, p1, p2, p3}, Landroid/media/AudioService;->setNewRcClientOnDisplays_syncRcsCurrc(ILandroid/app/PendingIntent;Z)V

    #@3
    .line 5922
    invoke-direct {p0, p1}, Landroid/media/AudioService;->setNewRcClientGenerationOnClients_syncRcsCurrc(I)V

    #@6
    .line 5923
    return-void
.end method

.method private setOrientationForAudioSystem()V
    .registers 3

    #@0
    .prologue
    .line 6730
    iget v0, p0, Landroid/media/AudioService;->mDeviceOrientation:I

    #@2
    packed-switch v0, :pswitch_data_2a

    #@5
    .line 6748
    const-string v0, "AudioService"

    #@7
    const-string v1, "Unknown orientation"

    #@9
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 6750
    :goto_c
    return-void

    #@d
    .line 6733
    :pswitch_d
    const-string/jumbo v0, "orientation=landscape"

    #@10
    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    #@13
    goto :goto_c

    #@14
    .line 6737
    :pswitch_14
    const-string/jumbo v0, "orientation=portrait"

    #@17
    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    #@1a
    goto :goto_c

    #@1b
    .line 6741
    :pswitch_1b
    const-string/jumbo v0, "orientation=square"

    #@1e
    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    #@21
    goto :goto_c

    #@22
    .line 6745
    :pswitch_22
    const-string/jumbo v0, "orientation=undefined"

    #@25
    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    #@28
    goto :goto_c

    #@29
    .line 6730
    nop

    #@2a
    :pswitch_data_2a
    .packed-switch 0x0
        :pswitch_22
        :pswitch_14
        :pswitch_d
        :pswitch_1b
    .end packed-switch
.end method

.method private setRingerModeInt(IZ)V
    .registers 15
    .parameter "ringerMode"
    .parameter "persist"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x2

    #@2
    const/4 v3, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    .line 1688
    iget-object v1, p0, Landroid/media/AudioService;->mSettingsLock:Ljava/lang/Object;

    #@6
    monitor-enter v1

    #@7
    .line 1689
    :try_start_7
    iput p1, p0, Landroid/media/AudioService;->mRingerMode:I

    #@9
    .line 1690
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_7 .. :try_end_a} :catchall_64

    #@a
    .line 1696
    invoke-static {}, Landroid/media/AudioSystem;->getNumStreamTypes()I

    #@d
    move-result v9

    #@e
    .line 1697
    .local v9, numStreamTypes:I
    add-int/lit8 v11, v9, -0x1

    #@10
    .local v11, streamType:I
    :goto_10
    if-ltz v11, :cond_92

    #@12
    .line 1698
    invoke-direct {p0, v11}, Landroid/media/AudioService;->isStreamMutedByRingerMode(I)Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_7b

    #@18
    .line 1699
    invoke-virtual {p0, v11}, Landroid/media/AudioService;->isStreamAffectedByRingerMode(I)Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_20

    #@1e
    if-ne p1, v4, :cond_78

    #@20
    .line 1703
    :cond_20
    iget-boolean v0, p0, Landroid/media/AudioService;->mVoiceCapable:Z

    #@22
    if-eqz v0, :cond_68

    #@24
    iget-object v0, p0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@26
    aget v0, v0, v11

    #@28
    if-ne v0, v4, :cond_68

    #@2a
    .line 1705
    iget-object v0, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@2c
    aget-object v1, v0, v11

    #@2e
    monitor-enter v1

    #@2f
    .line 1706
    :try_start_2f
    iget-object v0, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@31
    aget-object v0, v0, v11

    #@33
    invoke-static {v0}, Landroid/media/AudioService$VolumeStreamState;->access$1300(Landroid/media/AudioService$VolumeStreamState;)Ljava/util/concurrent/ConcurrentHashMap;

    #@36
    move-result-object v0

    #@37
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    #@3a
    move-result-object v10

    #@3b
    .line 1707
    .local v10, set:Ljava/util/Set;
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@3e
    move-result-object v8

    #@3f
    .line 1708
    .local v8, i:Ljava/util/Iterator;
    :cond_3f
    :goto_3f
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@42
    move-result v0

    #@43
    if-eqz v0, :cond_67

    #@45
    .line 1709
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@48
    move-result-object v7

    #@49
    check-cast v7, Ljava/util/Map$Entry;

    #@4b
    .line 1710
    .local v7, entry:Ljava/util/Map$Entry;
    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@4e
    move-result-object v0

    #@4f
    check-cast v0, Ljava/lang/Integer;

    #@51
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@54
    move-result v0

    #@55
    if-nez v0, :cond_3f

    #@57
    .line 1711
    const/16 v0, 0xa

    #@59
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5c
    move-result-object v0

    #@5d
    invoke-interface {v7, v0}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    #@60
    goto :goto_3f

    #@61
    .line 1714
    .end local v7           #entry:Ljava/util/Map$Entry;
    .end local v8           #i:Ljava/util/Iterator;
    .end local v10           #set:Ljava/util/Set;
    :catchall_61
    move-exception v0

    #@62
    monitor-exit v1
    :try_end_63
    .catchall {:try_start_2f .. :try_end_63} :catchall_61

    #@63
    throw v0

    #@64
    .line 1690
    .end local v9           #numStreamTypes:I
    .end local v11           #streamType:I
    :catchall_64
    move-exception v0

    #@65
    :try_start_65
    monitor-exit v1
    :try_end_66
    .catchall {:try_start_65 .. :try_end_66} :catchall_64

    #@66
    throw v0

    #@67
    .line 1714
    .restart local v8       #i:Ljava/util/Iterator;
    .restart local v9       #numStreamTypes:I
    .restart local v10       #set:Ljava/util/Set;
    .restart local v11       #streamType:I
    :cond_67
    :try_start_67
    monitor-exit v1
    :try_end_68
    .catchall {:try_start_67 .. :try_end_68} :catchall_61

    #@68
    .line 1716
    .end local v8           #i:Ljava/util/Iterator;
    .end local v10           #set:Ljava/util/Set;
    :cond_68
    iget-object v0, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@6a
    aget-object v0, v0, v11

    #@6c
    invoke-virtual {v0, v5, v2}, Landroid/media/AudioService$VolumeStreamState;->mute(Landroid/os/IBinder;Z)V

    #@6f
    .line 1717
    iget v0, p0, Landroid/media/AudioService;->mRingerModeMutedStreams:I

    #@71
    shl-int v1, v3, v11

    #@73
    xor-int/lit8 v1, v1, -0x1

    #@75
    and-int/2addr v0, v1

    #@76
    iput v0, p0, Landroid/media/AudioService;->mRingerModeMutedStreams:I

    #@78
    .line 1697
    :cond_78
    :goto_78
    add-int/lit8 v11, v11, -0x1

    #@7a
    goto :goto_10

    #@7b
    .line 1720
    :cond_7b
    invoke-virtual {p0, v11}, Landroid/media/AudioService;->isStreamAffectedByRingerMode(I)Z

    #@7e
    move-result v0

    #@7f
    if-eqz v0, :cond_78

    #@81
    if-eq p1, v4, :cond_78

    #@83
    .line 1722
    iget-object v0, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@85
    aget-object v0, v0, v11

    #@87
    invoke-virtual {v0, v5, v3}, Landroid/media/AudioService$VolumeStreamState;->mute(Landroid/os/IBinder;Z)V

    #@8a
    .line 1723
    iget v0, p0, Landroid/media/AudioService;->mRingerModeMutedStreams:I

    #@8c
    shl-int v1, v3, v11

    #@8e
    or-int/2addr v0, v1

    #@8f
    iput v0, p0, Landroid/media/AudioService;->mRingerModeMutedStreams:I

    #@91
    goto :goto_78

    #@92
    .line 1729
    :cond_92
    iget-boolean v0, p0, Landroid/media/AudioService;->mIsSoundException:Z

    #@94
    if-eqz v0, :cond_bf

    #@96
    .line 1730
    const-string v0, "AudioService"

    #@98
    new-instance v1, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    const-string v3, "AudioSystem.setRingerMode persist: "

    #@9f
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v1

    #@a3
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v1

    #@a7
    const-string v3, " ringerMode: "

    #@a9
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v1

    #@ad
    iget v3, p0, Landroid/media/AudioService;->mRingerMode:I

    #@af
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v1

    #@b3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b6
    move-result-object v1

    #@b7
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ba
    .line 1731
    iget v0, p0, Landroid/media/AudioService;->mRingerMode:I

    #@bc
    invoke-static {v0, v2}, Landroid/media/AudioSystem;->setRingerMode(II)I

    #@bf
    .line 1736
    :cond_bf
    if-eqz p2, :cond_cb

    #@c1
    .line 1737
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@c3
    const/4 v1, 0x3

    #@c4
    const/16 v6, 0x1f4

    #@c6
    move v3, v2

    #@c7
    move v4, v2

    #@c8
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@cb
    .line 1740
    :cond_cb
    return-void
.end method

.method private setSafeMediaVolumeEnabled(Z)V
    .registers 10
    .parameter "on"

    #@0
    .prologue
    const/4 v3, 0x3

    #@1
    const/4 v2, 0x2

    #@2
    .line 6826
    iget-object v7, p0, Landroid/media/AudioService;->mSafeMediaVolumeState:Ljava/lang/Integer;

    #@4
    monitor-enter v7

    #@5
    .line 6827
    :try_start_5
    iget-object v0, p0, Landroid/media/AudioService;->mSafeMediaVolumeState:Ljava/lang/Integer;

    #@7
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_2a

    #@d
    iget-object v0, p0, Landroid/media/AudioService;->mSafeMediaVolumeState:Ljava/lang/Integer;

    #@f
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@12
    move-result v0

    #@13
    const/4 v1, 0x1

    #@14
    if-eq v0, v1, :cond_2a

    #@16
    .line 6829
    if-eqz p1, :cond_2c

    #@18
    iget-object v0, p0, Landroid/media/AudioService;->mSafeMediaVolumeState:Ljava/lang/Integer;

    #@1a
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@1d
    move-result v0

    #@1e
    if-ne v0, v2, :cond_2c

    #@20
    .line 6830
    const/4 v0, 0x3

    #@21
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@24
    move-result-object v0

    #@25
    iput-object v0, p0, Landroid/media/AudioService;->mSafeMediaVolumeState:Ljava/lang/Integer;

    #@27
    .line 6831
    invoke-direct {p0}, Landroid/media/AudioService;->enforceSafeMediaVolume()V

    #@2a
    .line 6844
    :cond_2a
    :goto_2a
    monitor-exit v7

    #@2b
    .line 6845
    return-void

    #@2c
    .line 6832
    :cond_2c
    if-nez p1, :cond_2a

    #@2e
    iget-object v0, p0, Landroid/media/AudioService;->mSafeMediaVolumeState:Ljava/lang/Integer;

    #@30
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@33
    move-result v0

    #@34
    if-ne v0, v3, :cond_2a

    #@36
    .line 6833
    const/4 v0, 0x2

    #@37
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3a
    move-result-object v0

    #@3b
    iput-object v0, p0, Landroid/media/AudioService;->mSafeMediaVolumeState:Ljava/lang/Integer;

    #@3d
    .line 6834
    const/4 v0, 0x0

    #@3e
    iput v0, p0, Landroid/media/AudioService;->mMusicActiveMs:I

    #@40
    .line 6835
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@42
    const/16 v1, 0x18

    #@44
    const/4 v2, 0x0

    #@45
    const/4 v3, 0x0

    #@46
    const/4 v4, 0x0

    #@47
    const/4 v5, 0x0

    #@48
    const v6, 0xea60

    #@4b
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@4e
    goto :goto_2a

    #@4f
    .line 6844
    :catchall_4f
    move-exception v0

    #@50
    monitor-exit v7
    :try_end_51
    .catchall {:try_start_5 .. :try_end_51} :catchall_4f

    #@51
    throw v0
.end method

.method private setStreamVolumeInt(IIIZZ)V
    .registers 13
    .parameter "streamType"
    .parameter "index"
    .parameter "device"
    .parameter "force"
    .parameter "lastAudible"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v2, 0x2

    #@2
    .line 1492
    iget-object v0, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@4
    aget-object v5, v0, p1

    #@6
    .line 1493
    .local v5, streamState:Landroid/media/AudioService$VolumeStreamState;
    const-string v0, "AudioService"

    #@8
    new-instance v3, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string/jumbo v4, "setStreamVolumeInt()... streamType = "

    #@10
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    const-string v4, ", index = "

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    const-string v4, ", device = "

    #@24
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v3

    #@2c
    const-string v4, ", force = "

    #@2e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@35
    move-result-object v3

    #@36
    const-string v4, ", lastAudible = "

    #@38
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v3

    #@3c
    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v3

    #@44
    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 1497
    iget-boolean v0, p0, Landroid/media/AudioService;->mIsSoundException:Z

    #@49
    if-eqz v0, :cond_54

    #@4b
    iget v0, p0, Landroid/media/AudioService;->mRingerMode:I

    #@4d
    if-eq v0, v2, :cond_54

    #@4f
    if-nez p2, :cond_54

    #@51
    if-ne p1, v2, :cond_54

    #@53
    .line 1530
    :cond_53
    :goto_53
    return-void

    #@54
    .line 1504
    :cond_54
    invoke-static {v5}, Landroid/media/AudioService$VolumeStreamState;->access$700(Landroid/media/AudioService$VolumeStreamState;)I

    #@57
    move-result v0

    #@58
    if-eqz v0, :cond_6a

    #@5a
    .line 1506
    if-eqz p2, :cond_53

    #@5c
    .line 1507
    invoke-virtual {v5, p2, p3}, Landroid/media/AudioService$VolumeStreamState;->setLastAudibleIndex(II)V

    #@5f
    .line 1509
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@61
    const/4 v1, 0x1

    #@62
    const/16 v6, 0x1f4

    #@64
    move v3, v2

    #@65
    move v4, p3

    #@66
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@69
    goto :goto_53

    #@6a
    .line 1518
    :cond_6a
    invoke-virtual {v5, p2, p3, p5}, Landroid/media/AudioService$VolumeStreamState;->setIndex(IIZ)Z

    #@6d
    move-result v0

    #@6e
    if-nez v0, :cond_72

    #@70
    if-eqz p4, :cond_53

    #@72
    .line 1521
    :cond_72
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@74
    move v3, p3

    #@75
    move v4, v1

    #@76
    move v6, v1

    #@77
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@7a
    goto :goto_53
.end method

.method private startVoiceBasedInteractions(Z)V
    .registers 10
    .parameter "needWakeLock"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 5457
    const/4 v3, 0x0

    #@3
    .line 5462
    .local v3, voiceIntent:Landroid/content/Intent;
    iget-object v6, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@5
    const-string/jumbo v7, "power"

    #@8
    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@b
    move-result-object v2

    #@c
    check-cast v2, Landroid/os/PowerManager;

    #@e
    .line 5463
    .local v2, pm:Landroid/os/PowerManager;
    iget-object v6, p0, Landroid/media/AudioService;->mKeyguardManager:Landroid/app/KeyguardManager;

    #@10
    if-eqz v6, :cond_45

    #@12
    iget-object v6, p0, Landroid/media/AudioService;->mKeyguardManager:Landroid/app/KeyguardManager;

    #@14
    invoke-virtual {v6}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    #@17
    move-result v6

    #@18
    if-eqz v6, :cond_45

    #@1a
    move v1, v4

    #@1b
    .line 5464
    .local v1, isLocked:Z
    :goto_1b
    if-nez v1, :cond_47

    #@1d
    invoke-virtual {v2}, Landroid/os/PowerManager;->isScreenOn()Z

    #@20
    move-result v6

    #@21
    if-eqz v6, :cond_47

    #@23
    .line 5465
    new-instance v3, Landroid/content/Intent;

    #@25
    .end local v3           #voiceIntent:Landroid/content/Intent;
    const-string v4, "android.speech.action.WEB_SEARCH"

    #@27
    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@2a
    .line 5472
    .restart local v3       #voiceIntent:Landroid/content/Intent;
    :goto_2a
    if-eqz p1, :cond_31

    #@2c
    .line 5473
    iget-object v4, p0, Landroid/media/AudioService;->mMediaEventWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2e
    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@31
    .line 5476
    :cond_31
    if-eqz v3, :cond_3d

    #@33
    .line 5477
    const/high16 v4, 0x1080

    #@35
    :try_start_35
    invoke-virtual {v3, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@38
    .line 5479
    iget-object v4, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@3a
    invoke-virtual {v4, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_3d
    .catchall {:try_start_35 .. :try_end_3d} :catchall_7e
    .catch Landroid/content/ActivityNotFoundException; {:try_start_35 .. :try_end_3d} :catch_60

    #@3d
    .line 5484
    :cond_3d
    if-eqz p1, :cond_44

    #@3f
    .line 5485
    iget-object v4, p0, Landroid/media/AudioService;->mMediaEventWakeLock:Landroid/os/PowerManager$WakeLock;

    #@41
    :goto_41
    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->release()V

    #@44
    .line 5488
    :cond_44
    return-void

    #@45
    .end local v1           #isLocked:Z
    :cond_45
    move v1, v5

    #@46
    .line 5463
    goto :goto_1b

    #@47
    .line 5467
    .restart local v1       #isLocked:Z
    :cond_47
    new-instance v3, Landroid/content/Intent;

    #@49
    .end local v3           #voiceIntent:Landroid/content/Intent;
    const-string v6, "android.speech.action.VOICE_SEARCH_HANDS_FREE"

    #@4b
    invoke-direct {v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@4e
    .line 5468
    .restart local v3       #voiceIntent:Landroid/content/Intent;
    const-string v6, "android.speech.extras.EXTRA_SECURE"

    #@50
    if-eqz v1, :cond_5e

    #@52
    iget-object v7, p0, Landroid/media/AudioService;->mKeyguardManager:Landroid/app/KeyguardManager;

    #@54
    invoke-virtual {v7}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    #@57
    move-result v7

    #@58
    if-eqz v7, :cond_5e

    #@5a
    :goto_5a
    invoke-virtual {v3, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@5d
    goto :goto_2a

    #@5e
    :cond_5e
    move v4, v5

    #@5f
    goto :goto_5a

    #@60
    .line 5481
    :catch_60
    move-exception v0

    #@61
    .line 5482
    .local v0, e:Landroid/content/ActivityNotFoundException;
    :try_start_61
    const-string v4, "AudioService"

    #@63
    new-instance v5, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string v6, "No activity for search: "

    #@6a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v5

    #@6e
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v5

    #@72
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v5

    #@76
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_79
    .catchall {:try_start_61 .. :try_end_79} :catchall_7e

    #@79
    .line 5484
    if-eqz p1, :cond_44

    #@7b
    .line 5485
    iget-object v4, p0, Landroid/media/AudioService;->mMediaEventWakeLock:Landroid/os/PowerManager$WakeLock;

    #@7d
    goto :goto_41

    #@7e
    .line 5484
    .end local v0           #e:Landroid/content/ActivityNotFoundException;
    :catchall_7e
    move-exception v4

    #@7f
    if-eqz p1, :cond_86

    #@81
    .line 5485
    iget-object v5, p0, Landroid/media/AudioService;->mMediaEventWakeLock:Landroid/os/PowerManager$WakeLock;

    #@83
    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->release()V

    #@86
    .line 5484
    :cond_86
    throw v4
.end method

.method private updateRemoteControlDisplay_syncAfRcs(I)V
    .registers 8
    .parameter "infoChangedFlags"

    #@0
    .prologue
    .line 5996
    iget-object v2, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@2
    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Landroid/media/AudioService$RemoteControlStackEntry;

    #@8
    .line 5997
    .local v1, rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    move v0, p1

    #@9
    .line 6000
    .local v0, infoFlagsAboutToBeUsed:I
    iget-object v2, v1, Landroid/media/AudioService$RemoteControlStackEntry;->mRcClient:Landroid/media/IRemoteControlClient;

    #@b
    if-nez v2, :cond_11

    #@d
    .line 6002
    invoke-direct {p0}, Landroid/media/AudioService;->clearRemoteControlDisplay_syncAfRcs()V

    #@10
    .line 6015
    :goto_10
    return-void

    #@11
    .line 6005
    :cond_11
    iget-object v3, p0, Landroid/media/AudioService;->mCurrentRcLock:Ljava/lang/Object;

    #@13
    monitor-enter v3

    #@14
    .line 6006
    :try_start_14
    iget-object v2, v1, Landroid/media/AudioService$RemoteControlStackEntry;->mRcClient:Landroid/media/IRemoteControlClient;

    #@16
    iget-object v4, p0, Landroid/media/AudioService;->mCurrentRcClient:Landroid/media/IRemoteControlClient;

    #@18
    invoke-virtual {v2, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v2

    #@1c
    if-nez v2, :cond_20

    #@1e
    .line 6008
    const/16 v0, 0xf

    #@20
    .line 6010
    :cond_20
    iget-object v2, v1, Landroid/media/AudioService$RemoteControlStackEntry;->mRcClient:Landroid/media/IRemoteControlClient;

    #@22
    iput-object v2, p0, Landroid/media/AudioService;->mCurrentRcClient:Landroid/media/IRemoteControlClient;

    #@24
    .line 6011
    monitor-exit v3
    :try_end_25
    .catchall {:try_start_14 .. :try_end_25} :catchall_34

    #@25
    .line 6013
    iget-object v2, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@27
    iget-object v3, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@29
    const/16 v4, 0xd

    #@2b
    const/4 v5, 0x0

    #@2c
    invoke-virtual {v3, v4, v0, v5, v1}, Landroid/media/AudioService$AudioHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v2, v3}, Landroid/media/AudioService$AudioHandler;->sendMessage(Landroid/os/Message;)Z

    #@33
    goto :goto_10

    #@34
    .line 6011
    :catchall_34
    move-exception v2

    #@35
    :try_start_35
    monitor-exit v3
    :try_end_36
    .catchall {:try_start_35 .. :try_end_36} :catchall_34

    #@36
    throw v2
.end method

.method private updateStreamVolumeAlias(Z)V
    .registers 10
    .parameter "updateVolumes"

    #@0
    .prologue
    const/16 v5, 0x8

    #@2
    const/4 v3, 0x0

    #@3
    .line 764
    iget-boolean v0, p0, Landroid/media/AudioService;->mVoiceCapable:Z

    #@5
    if-eqz v0, :cond_3f

    #@7
    .line 765
    iget-object v0, p0, Landroid/media/AudioService;->STREAM_VOLUME_ALIAS:[I

    #@9
    iput-object v0, p0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@b
    .line 766
    const/4 v7, 0x2

    #@c
    .line 771
    .local v7, dtmfStreamAlias:I
    :goto_c
    invoke-direct {p0}, Landroid/media/AudioService;->isInCommunication()Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_13

    #@12
    .line 772
    const/4 v7, 0x0

    #@13
    .line 774
    :cond_13
    iget-object v0, p0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@15
    aput v7, v0, v5

    #@17
    .line 775
    if-eqz p1, :cond_3e

    #@19
    .line 776
    iget-object v0, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@1b
    aget-object v0, v0, v5

    #@1d
    iget-object v1, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@1f
    aget-object v1, v1, v7

    #@21
    invoke-virtual {v0, v1, v3}, Landroid/media/AudioService$VolumeStreamState;->setAllIndexes(Landroid/media/AudioService$VolumeStreamState;Z)V

    #@24
    .line 778
    iget-object v0, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@26
    aget-object v0, v0, v5

    #@28
    iget-object v1, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@2a
    aget-object v1, v1, v7

    #@2c
    const/4 v2, 0x1

    #@2d
    invoke-virtual {v0, v1, v2}, Landroid/media/AudioService$VolumeStreamState;->setAllIndexes(Landroid/media/AudioService$VolumeStreamState;Z)V

    #@30
    .line 780
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@32
    const/16 v1, 0xe

    #@34
    const/4 v2, 0x2

    #@35
    iget-object v4, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@37
    aget-object v5, v4, v5

    #@39
    move v4, v3

    #@3a
    move v6, v3

    #@3b
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@3e
    .line 787
    :cond_3e
    return-void

    #@3f
    .line 768
    .end local v7           #dtmfStreamAlias:I
    :cond_3f
    iget-object v0, p0, Landroid/media/AudioService;->STREAM_VOLUME_ALIAS_NON_VOICE:[I

    #@41
    iput-object v0, p0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@43
    .line 769
    const/4 v7, 0x3

    #@44
    .restart local v7       #dtmfStreamAlias:I
    goto :goto_c
.end method

.method private waitForAudioHandlerCreation()V
    .registers 4

    #@0
    .prologue
    .line 708
    monitor-enter p0

    #@1
    .line 709
    :goto_1
    :try_start_1
    iget-object v1, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_12

    #@3
    if-nez v1, :cond_15

    #@5
    .line 712
    :try_start_5
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_12
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_8} :catch_9

    #@8
    goto :goto_1

    #@9
    .line 713
    :catch_9
    move-exception v0

    #@a
    .line 714
    .local v0, e:Ljava/lang/InterruptedException;
    :try_start_a
    const-string v1, "AudioService"

    #@c
    const-string v2, "Interrupted while waiting on volume handler."

    #@e
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    goto :goto_1

    #@12
    .line 717
    .end local v0           #e:Ljava/lang/InterruptedException;
    :catchall_12
    move-exception v1

    #@13
    monitor-exit p0
    :try_end_14
    .catchall {:try_start_a .. :try_end_14} :catchall_12

    #@14
    throw v1

    #@15
    :cond_15
    :try_start_15
    monitor-exit p0
    :try_end_16
    .catchall {:try_start_15 .. :try_end_16} :catchall_12

    #@16
    .line 718
    return-void
.end method


# virtual methods
.method public abandonAudioFocus(Landroid/media/IAudioFocusDispatcher;Ljava/lang/String;)I
    .registers 8
    .parameter "fl"
    .parameter "clientId"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 5173
    const-string v1, "AudioService"

    #@3
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v3, " AudioFocus  abandonAudioFocus() from "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 5176
    :try_start_19
    sget-object v2, Landroid/media/AudioService;->mAudioFocusLock:Ljava/lang/Object;

    #@1b
    monitor-enter v2
    :try_end_1c
    .catch Ljava/util/ConcurrentModificationException; {:try_start_19 .. :try_end_1c} :catch_25

    #@1c
    .line 5177
    const/4 v1, 0x1

    #@1d
    :try_start_1d
    invoke-direct {p0, p2, v1}, Landroid/media/AudioService;->removeFocusStackEntry(Ljava/lang/String;Z)V

    #@20
    .line 5178
    monitor-exit v2

    #@21
    .line 5187
    :goto_21
    return v4

    #@22
    .line 5178
    :catchall_22
    move-exception v1

    #@23
    monitor-exit v2
    :try_end_24
    .catchall {:try_start_1d .. :try_end_24} :catchall_22

    #@24
    :try_start_24
    throw v1
    :try_end_25
    .catch Ljava/util/ConcurrentModificationException; {:try_start_24 .. :try_end_25} :catch_25

    #@25
    .line 5179
    :catch_25
    move-exception v0

    #@26
    .line 5183
    .local v0, cme:Ljava/util/ConcurrentModificationException;
    const-string v1, "AudioService"

    #@28
    new-instance v2, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v3, "FATAL EXCEPTION AudioFocus  abandonAudioFocus() caused "

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 5184
    invoke-virtual {v0}, Ljava/util/ConcurrentModificationException;->printStackTrace()V

    #@41
    goto :goto_21
.end method

.method public addMediaButtonEventReceiverForCalls(Landroid/content/ComponentName;)V
    .registers 4
    .parameter "c"

    #@0
    .prologue
    .line 6986
    iget-object v0, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.MODIFY_PHONE_STATE"

    #@4
    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingPermission(Ljava/lang/String;)I

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_12

    #@a
    .line 6988
    const-string v0, "AudioService"

    #@c
    const-string v1, "Invalid permissions to add media button receiver for Calls list"

    #@e
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 6994
    :goto_11
    return-void

    #@12
    .line 6991
    :cond_12
    iget-object v1, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@14
    monitor-enter v1

    #@15
    .line 6992
    :try_start_15
    iget-object v0, p0, Landroid/media/AudioService;->mMediaReceiverForCallsList:Ljava/util/ArrayList;

    #@17
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1a
    .line 6993
    monitor-exit v1

    #@1b
    goto :goto_11

    #@1c
    :catchall_1c
    move-exception v0

    #@1d
    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_15 .. :try_end_1e} :catchall_1c

    #@1e
    throw v0
.end method

.method public adjustLocalOrRemoteStreamVolume(II)V
    .registers 9
    .parameter "streamType"
    .parameter "direction"

    #@0
    .prologue
    const/16 v5, 0xa

    #@2
    const/4 v4, 0x3

    #@3
    const/4 v3, 0x0

    #@4
    .line 941
    const-string v0, "AudioService"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "adjustLocalOrRemoteStreamVolume(dir="

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, ")"

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 942
    invoke-direct {p0, v4}, Landroid/media/AudioService;->checkUpdateRemoteStateIfActive(I)Z

    #@25
    move-result v0

    #@26
    if-eqz v0, :cond_2c

    #@28
    .line 943
    invoke-direct {p0, v4, p2, v3}, Landroid/media/AudioService;->adjustRemoteVolume(III)V

    #@2b
    .line 949
    :cond_2b
    :goto_2b
    return-void

    #@2c
    .line 944
    :cond_2c
    invoke-static {v4, v3}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    #@2f
    move-result v0

    #@30
    if-eqz v0, :cond_36

    #@32
    .line 945
    invoke-virtual {p0, v4, p2, v3}, Landroid/media/AudioService;->adjustStreamVolume(III)V

    #@35
    goto :goto_2b

    #@36
    .line 946
    :cond_36
    invoke-static {v5, v3}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    #@39
    move-result v0

    #@3a
    if-eqz v0, :cond_2b

    #@3c
    .line 947
    invoke-virtual {p0, v5, p2, v3}, Landroid/media/AudioService;->adjustStreamVolume(III)V

    #@3f
    goto :goto_2b
.end method

.method public adjustMasterVolume(II)V
    .registers 10
    .parameter "steps"
    .parameter "flags"

    #@0
    .prologue
    .line 1174
    invoke-direct {p0, p1}, Landroid/media/AudioService;->ensureValidSteps(I)V

    #@3
    .line 1175
    invoke-static {}, Landroid/media/AudioSystem;->getMasterVolume()F

    #@6
    move-result v5

    #@7
    const/high16 v6, 0x42c8

    #@9
    mul-float/2addr v5, v6

    #@a
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    #@d
    move-result v4

    #@e
    .line 1176
    .local v4, volume:I
    const/4 v0, 0x0

    #@f
    .line 1177
    .local v0, delta:I
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    #@12
    move-result v3

    #@13
    .line 1178
    .local v3, numSteps:I
    if-lez p1, :cond_21

    #@15
    const/4 v1, 0x1

    #@16
    .line 1179
    .local v1, direction:I
    :goto_16
    const/4 v2, 0x0

    #@17
    .local v2, i:I
    :goto_17
    if-ge v2, v3, :cond_23

    #@19
    .line 1180
    invoke-direct {p0, v1, v4}, Landroid/media/AudioService;->findVolumeDelta(II)I

    #@1c
    move-result v0

    #@1d
    .line 1181
    add-int/2addr v4, v0

    #@1e
    .line 1179
    add-int/lit8 v2, v2, 0x1

    #@20
    goto :goto_17

    #@21
    .line 1178
    .end local v1           #direction:I
    .end local v2           #i:I
    :cond_21
    const/4 v1, -0x1

    #@22
    goto :goto_16

    #@23
    .line 1185
    .restart local v1       #direction:I
    .restart local v2       #i:I
    :cond_23
    invoke-virtual {p0, v4, p2}, Landroid/media/AudioService;->setMasterVolume(II)V

    #@26
    .line 1186
    return-void
.end method

.method public adjustStreamVolume(III)V
    .registers 29
    .parameter "streamType"
    .parameter "direction"
    .parameter "flags"

    #@0
    .prologue
    .line 1064
    const-string v5, "AudioService"

    #@2
    new-instance v6, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v7, "adjustStreamVolume() stream="

    #@9
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v6

    #@d
    move/from16 v0, p1

    #@f
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v6

    #@13
    const-string v7, ", dir="

    #@15
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v6

    #@19
    move/from16 v0, p2

    #@1b
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v6

    #@1f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v6

    #@23
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 1066
    move-object/from16 v0, p0

    #@28
    move/from16 v1, p2

    #@2a
    invoke-direct {v0, v1}, Landroid/media/AudioService;->ensureValidDirection(I)V

    #@2d
    .line 1067
    invoke-direct/range {p0 .. p1}, Landroid/media/AudioService;->ensureValidStreamType(I)V

    #@30
    .line 1072
    move-object/from16 v0, p0

    #@32
    iget-object v5, v0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@34
    aget v24, v5, p1

    #@36
    .line 1073
    .local v24, streamTypeAlias:I
    move-object/from16 v0, p0

    #@38
    iget-object v5, v0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@3a
    aget-object v10, v5, v24

    #@3c
    .line 1075
    .local v10, streamState:Landroid/media/AudioService$VolumeStreamState;
    move-object/from16 v0, p0

    #@3e
    move/from16 v1, v24

    #@40
    invoke-direct {v0, v1}, Landroid/media/AudioService;->getDeviceForStream(I)I

    #@43
    move-result v9

    #@44
    .line 1077
    .local v9, device:I
    invoke-static {v10}, Landroid/media/AudioService$VolumeStreamState;->access$700(Landroid/media/AudioService$VolumeStreamState;)I

    #@47
    move-result v5

    #@48
    if-eqz v5, :cond_94

    #@4a
    const/4 v5, 0x1

    #@4b
    :goto_4b
    invoke-virtual {v10, v9, v5}, Landroid/media/AudioService$VolumeStreamState;->getIndex(IZ)I

    #@4e
    move-result v19

    #@4f
    .line 1079
    .local v19, aliasIndex:I
    const/16 v18, 0x1

    #@51
    .line 1082
    .local v18, adjustVolume:Z
    const/16 v5, 0xa

    #@53
    move-object/from16 v0, p0

    #@55
    move/from16 v1, p1

    #@57
    move/from16 v2, v24

    #@59
    invoke-direct {v0, v5, v1, v2}, Landroid/media/AudioService;->rescaleIndex(III)I

    #@5c
    move-result v23

    #@5d
    .line 1085
    .local v23, step:I
    move-object/from16 v0, p0

    #@5f
    iget-object v5, v0, Landroid/media/AudioService;->mSafeMediaVolumeState:Ljava/lang/Integer;

    #@61
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    #@64
    move-result v5

    #@65
    const/4 v6, 0x3

    #@66
    if-ne v5, v6, :cond_82

    #@68
    const/4 v5, 0x3

    #@69
    move/from16 v0, v24

    #@6b
    if-ne v0, v5, :cond_82

    #@6d
    move-object/from16 v0, p0

    #@6f
    iget-object v5, v0, Landroid/media/AudioService;->mLgSafeMediaVolumeEnabled:Ljava/lang/Boolean;

    #@71
    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    #@74
    move-result v5

    #@75
    if-eqz v5, :cond_82

    #@77
    .line 1087
    add-int v5, v19, v23

    #@79
    move-object/from16 v0, p0

    #@7b
    move/from16 v1, v24

    #@7d
    move/from16 v2, p2

    #@7f
    invoke-direct {v0, v1, v5, v9, v2}, Landroid/media/AudioService;->checkDisplaySafeMediaVolume(IIII)V

    #@82
    .line 1090
    :cond_82
    const/4 v5, 0x1

    #@83
    move/from16 v0, p2

    #@85
    if-ne v0, v5, :cond_96

    #@87
    add-int v5, v19, v23

    #@89
    move-object/from16 v0, p0

    #@8b
    move/from16 v1, v24

    #@8d
    invoke-direct {v0, v1, v5, v9}, Landroid/media/AudioService;->checkSafeMediaVolume(III)Z

    #@90
    move-result v5

    #@91
    if-nez v5, :cond_96

    #@93
    .line 1170
    :goto_93
    return-void

    #@94
    .line 1077
    .end local v18           #adjustVolume:Z
    .end local v19           #aliasIndex:I
    .end local v23           #step:I
    :cond_94
    const/4 v5, 0x0

    #@95
    goto :goto_4b

    #@96
    .line 1099
    .restart local v18       #adjustVolume:Z
    .restart local v19       #aliasIndex:I
    .restart local v23       #step:I
    :cond_96
    and-int/lit8 p3, p3, -0x21

    #@98
    .line 1100
    const/4 v5, 0x3

    #@99
    move/from16 v0, v24

    #@9b
    if-ne v0, v5, :cond_bd

    #@9d
    and-int/lit8 v5, v9, 0x0

    #@9f
    if-eqz v5, :cond_bd

    #@a1
    .line 1102
    or-int/lit8 p3, p3, 0x20

    #@a3
    .line 1103
    move-object/from16 v0, p0

    #@a5
    iget-object v5, v0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@a7
    aget-object v5, v5, p1

    #@a9
    invoke-virtual {v5}, Landroid/media/AudioService$VolumeStreamState;->getMaxIndex()I

    #@ac
    move-result v20

    #@ad
    .line 1104
    .local v20, index:I
    move/from16 v21, v20

    #@af
    .line 1169
    .local v21, oldIndex:I
    :goto_af
    move-object/from16 v0, p0

    #@b1
    move/from16 v1, p1

    #@b3
    move/from16 v2, v21

    #@b5
    move/from16 v3, v20

    #@b7
    move/from16 v4, p3

    #@b9
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/media/AudioService;->sendVolumeUpdate(IIII)V

    #@bc
    goto :goto_93

    #@bd
    .line 1108
    .end local v20           #index:I
    .end local v21           #oldIndex:I
    :cond_bd
    and-int/lit8 v5, p3, 0x2

    #@bf
    if-nez v5, :cond_c9

    #@c1
    invoke-virtual/range {p0 .. p0}, Landroid/media/AudioService;->getMasterStreamType()I

    #@c4
    move-result v5

    #@c5
    move/from16 v0, v24

    #@c7
    if-ne v0, v5, :cond_e6

    #@c9
    .line 1110
    :cond_c9
    invoke-virtual/range {p0 .. p0}, Landroid/media/AudioService;->getRingerMode()I

    #@cc
    move-result v22

    #@cd
    .line 1112
    .local v22, ringerMode:I
    const/4 v5, 0x1

    #@ce
    move/from16 v0, v22

    #@d0
    if-ne v0, v5, :cond_d4

    #@d2
    .line 1113
    and-int/lit8 p3, p3, -0x11

    #@d4
    .line 1118
    :cond_d4
    move-object/from16 v0, p0

    #@d6
    iget-boolean v5, v0, Landroid/media/AudioService;->mIsSoundProfile:Z

    #@d8
    if-eqz v5, :cond_137

    #@da
    .line 1119
    move-object/from16 v0, p0

    #@dc
    move/from16 v1, v19

    #@de
    move/from16 v2, p2

    #@e0
    move/from16 v3, v23

    #@e2
    invoke-direct {v0, v1, v2, v3}, Landroid/media/AudioService;->checkForSoundProfileChange(III)Z

    #@e5
    move-result v18

    #@e6
    .line 1130
    .end local v22           #ringerMode:I
    :cond_e6
    :goto_e6
    move-object/from16 v0, p0

    #@e8
    iget v5, v0, Landroid/media/AudioService;->mRingerMode:I

    #@ea
    const/4 v6, 0x2

    #@eb
    if-eq v5, v6, :cond_fa

    #@ed
    const/4 v5, 0x5

    #@ee
    move/from16 v0, p1

    #@f0
    if-ne v0, v5, :cond_fa

    #@f2
    .line 1131
    move-object/from16 v0, p0

    #@f4
    iget-boolean v5, v0, Landroid/media/AudioService;->mIsSoundProfile:Z

    #@f6
    if-eqz v5, :cond_fa

    #@f8
    .line 1132
    const/16 v18, 0x0

    #@fa
    .line 1137
    :cond_fa
    move-object/from16 v0, p0

    #@fc
    iget-object v5, v0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@fe
    aget-object v6, v5, p1

    #@100
    move-object/from16 v0, p0

    #@102
    iget-object v5, v0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@104
    aget-object v5, v5, p1

    #@106
    invoke-static {v5}, Landroid/media/AudioService$VolumeStreamState;->access$700(Landroid/media/AudioService$VolumeStreamState;)I

    #@109
    move-result v5

    #@10a
    if-eqz v5, :cond_156

    #@10c
    const/4 v5, 0x1

    #@10d
    :goto_10d
    invoke-virtual {v6, v9, v5}, Landroid/media/AudioService$VolumeStreamState;->getIndex(IZ)I

    #@110
    move-result v21

    #@111
    .line 1140
    .restart local v21       #oldIndex:I
    invoke-static {v10}, Landroid/media/AudioService$VolumeStreamState;->access$700(Landroid/media/AudioService$VolumeStreamState;)I

    #@114
    move-result v5

    #@115
    if-eqz v5, :cond_158

    #@117
    .line 1141
    if-eqz v18, :cond_12a

    #@119
    .line 1144
    mul-int v5, p2, v23

    #@11b
    invoke-virtual {v10, v5, v9}, Landroid/media/AudioService$VolumeStreamState;->adjustLastAudibleIndex(II)V

    #@11e
    .line 1145
    move-object/from16 v0, p0

    #@120
    iget-object v5, v0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@122
    const/4 v6, 0x1

    #@123
    const/4 v7, 0x2

    #@124
    const/4 v8, 0x2

    #@125
    const/16 v11, 0x1f4

    #@127
    invoke-static/range {v5 .. v11}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@12a
    .line 1153
    :cond_12a
    move-object/from16 v0, p0

    #@12c
    iget-object v5, v0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@12e
    aget-object v5, v5, p1

    #@130
    const/4 v6, 0x1

    #@131
    invoke-virtual {v5, v9, v6}, Landroid/media/AudioService$VolumeStreamState;->getIndex(IZ)I

    #@134
    move-result v20

    #@135
    .restart local v20       #index:I
    goto/16 :goto_af

    #@137
    .line 1122
    .end local v20           #index:I
    .end local v21           #oldIndex:I
    .restart local v22       #ringerMode:I
    :cond_137
    move-object/from16 v0, p0

    #@139
    move/from16 v1, v19

    #@13b
    move/from16 v2, p2

    #@13d
    move/from16 v3, v23

    #@13f
    invoke-direct {v0, v1, v2, v3}, Landroid/media/AudioService;->checkForRingerModeChange(III)Z

    #@142
    move-result v18

    #@143
    .line 1123
    invoke-virtual/range {p0 .. p0}, Landroid/media/AudioService;->getMasterStreamType()I

    #@146
    move-result v5

    #@147
    move/from16 v0, v24

    #@149
    if-ne v0, v5, :cond_e6

    #@14b
    move-object/from16 v0, p0

    #@14d
    iget v5, v0, Landroid/media/AudioService;->mRingerMode:I

    #@14f
    if-nez v5, :cond_e6

    #@151
    .line 1125
    const/4 v5, 0x0

    #@152
    invoke-virtual {v10, v5, v9}, Landroid/media/AudioService$VolumeStreamState;->setLastAudibleIndex(II)V

    #@155
    goto :goto_e6

    #@156
    .line 1137
    .end local v22           #ringerMode:I
    :cond_156
    const/4 v5, 0x0

    #@157
    goto :goto_10d

    #@158
    .line 1155
    .restart local v21       #oldIndex:I
    :cond_158
    if-eqz v18, :cond_171

    #@15a
    mul-int v5, p2, v23

    #@15c
    invoke-virtual {v10, v5, v9}, Landroid/media/AudioService$VolumeStreamState;->adjustIndex(II)Z

    #@15f
    move-result v5

    #@160
    if-eqz v5, :cond_171

    #@162
    .line 1158
    move-object/from16 v0, p0

    #@164
    iget-object v11, v0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@166
    const/4 v12, 0x0

    #@167
    const/4 v13, 0x2

    #@168
    const/4 v15, 0x0

    #@169
    const/16 v17, 0x0

    #@16b
    move v14, v9

    #@16c
    move-object/from16 v16, v10

    #@16e
    invoke-static/range {v11 .. v17}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@171
    .line 1166
    :cond_171
    move-object/from16 v0, p0

    #@173
    iget-object v5, v0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@175
    aget-object v5, v5, p1

    #@177
    const/4 v6, 0x0

    #@178
    invoke-virtual {v5, v9, v6}, Landroid/media/AudioService$VolumeStreamState;->getIndex(IZ)I

    #@17b
    move-result v20

    #@17c
    .restart local v20       #index:I
    goto/16 :goto_af
.end method

.method public adjustSuggestedStreamVolume(III)V
    .registers 16
    .parameter "direction"
    .parameter "suggestedStreamType"
    .parameter "flags"

    #@0
    .prologue
    .line 953
    const-string v0, "AudioService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "adjustSuggestedStreamVolume() stream="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 957
    iget-boolean v0, p0, Landroid/media/AudioService;->MirroLinkTestKey:Z

    #@1a
    if-eqz v0, :cond_45

    #@1c
    .line 959
    const/4 v0, 0x1

    #@1d
    if-ne p1, v0, :cond_45

    #@1f
    .line 962
    const-string v0, "AudioService"

    #@21
    const-string/jumbo v1, "wkcp audio volume key"

    #@24
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 963
    const-string/jumbo v0, "isVirtualMirrorLinkDevice"

    #@2a
    invoke-static {v0}, Landroid/media/AudioSystem;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    #@2d
    move-result-object v0

    #@2e
    const-string/jumbo v1, "true"

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@34
    move-result v8

    #@35
    .line 964
    .local v8, isConnected:Z
    if-nez v8, :cond_70

    #@37
    .line 965
    const-string v0, "AudioService"

    #@39
    const-string/jumbo v1, "wkcp audio com.lge.mirrorlink.audio.started"

    #@3c
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 966
    const-string/jumbo v0, "isVirtualMirrorLinkDevice=true"

    #@42
    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    #@45
    .line 977
    .end local v8           #isConnected:Z
    :cond_45
    :goto_45
    iget v0, p0, Landroid/media/AudioService;->mVolumeControlStream:I

    #@47
    const/4 v1, -0x1

    #@48
    if-eq v0, v1, :cond_7f

    #@4a
    .line 978
    iget v10, p0, Landroid/media/AudioService;->mVolumeControlStream:I

    #@4c
    .line 984
    .local v10, streamType:I
    :goto_4c
    iget-object v0, p0, Landroid/media/AudioService;->mKeyguardManager:Landroid/app/KeyguardManager;

    #@4e
    if-eqz v0, :cond_84

    #@50
    iget-object v0, p0, Landroid/media/AudioService;->mKeyguardManager:Landroid/app/KeyguardManager;

    #@52
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    #@55
    move-result v0

    #@56
    if-eqz v0, :cond_84

    #@58
    iget v0, p0, Landroid/media/AudioService;->mMode:I

    #@5a
    const/4 v1, 0x2

    #@5b
    if-eq v0, v1, :cond_84

    #@5d
    iget v0, p0, Landroid/media/AudioService;->mMode:I

    #@5f
    const/4 v1, 0x3

    #@60
    if-eq v0, v1, :cond_84

    #@62
    .line 986
    and-int/lit8 p3, p3, -0x2

    #@64
    .line 987
    const/4 v0, 0x2

    #@65
    if-ne v10, v0, :cond_84

    #@67
    .line 988
    iget-object v0, p0, Landroid/media/AudioService;->mVolumePanel:Landroid/view/VolumePanel;

    #@69
    invoke-virtual {v0}, Landroid/view/VolumePanel;->isShowing()Z

    #@6c
    move-result v0

    #@6d
    if-nez v0, :cond_84

    #@6f
    .line 1060
    :goto_6f
    return-void

    #@70
    .line 969
    .end local v10           #streamType:I
    .restart local v8       #isConnected:Z
    :cond_70
    const-string v0, "AudioService"

    #@72
    const-string/jumbo v1, "wkcp audio com.lge.mirrorlink.audio.stopped"

    #@75
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@78
    .line 970
    const-string/jumbo v0, "isVirtualMirrorLinkDevice=false"

    #@7b
    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    #@7e
    goto :goto_45

    #@7f
    .line 980
    .end local v8           #isConnected:Z
    :cond_7f
    invoke-direct {p0, p2}, Landroid/media/AudioService;->getActiveStreamType(I)I

    #@82
    move-result v10

    #@83
    .restart local v10       #streamType:I
    goto :goto_4c

    #@84
    .line 996
    :cond_84
    invoke-virtual {p0, v10}, Landroid/media/AudioService;->isVirtualMirrorLinkDevicedConnected(I)Z

    #@87
    move-result v0

    #@88
    if-eqz v0, :cond_92

    #@8a
    .line 998
    const-string v0, "AudioService"

    #@8c
    const-string v1, "Speaker the volume is fixed!!!!!"

    #@8e
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@91
    goto :goto_6f

    #@92
    .line 1006
    :cond_92
    const/4 v0, 0x2

    #@93
    if-ne v10, v0, :cond_bb

    #@95
    .line 1007
    :try_start_95
    iget-object v0, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@97
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9a
    move-result-object v0

    #@9b
    const-string/jumbo v1, "quiet_mode_status"

    #@9e
    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    #@a1
    move-result v9

    #@a2
    .line 1008
    .local v9, quietModeStatus:I
    const/4 v0, 0x1

    #@a3
    if-ne v9, v0, :cond_bb

    #@a5
    iget-object v0, p0, Landroid/media/AudioService;->mIsQuietModeSupport:Ljava/lang/Boolean;

    #@a7
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    #@aa
    move-result v0

    #@ab
    if-eqz v0, :cond_bb

    #@ad
    .line 1009
    iget-object v0, p0, Landroid/media/AudioService;->mVolumePanel:Landroid/view/VolumePanel;

    #@af
    invoke-virtual {v0}, Landroid/view/VolumePanel;->postDisplayQuietModeWarning()V
    :try_end_b2
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_95 .. :try_end_b2} :catch_b3

    #@b2
    goto :goto_6f

    #@b3
    .line 1013
    .end local v9           #quietModeStatus:I
    :catch_b3
    move-exception v7

    #@b4
    .line 1014
    .local v7, e:Landroid/provider/Settings$SettingNotFoundException;
    const-string v0, "AudioService"

    #@b6
    const-string v1, "SettingNotFoundException - getDBQuietModeState()"

    #@b8
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@bb
    .line 1019
    .end local v7           #e:Landroid/provider/Settings$SettingNotFoundException;
    :cond_bb
    iget v0, p0, Landroid/media/AudioService;->mMode:I

    #@bd
    const/4 v1, 0x2

    #@be
    if-ne v0, v1, :cond_108

    #@c0
    .line 1020
    const-string/jumbo v0, "tty_mode"

    #@c3
    invoke-static {v0}, Landroid/media/AudioSystem;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    #@c6
    move-result-object v11

    #@c7
    .line 1021
    .local v11, ttyMode:Ljava/lang/String;
    const-string/jumbo v0, "tty_full"

    #@ca
    invoke-virtual {v11, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@cd
    move-result v0

    #@ce
    if-nez v0, :cond_e2

    #@d0
    const-string/jumbo v0, "tty_hco"

    #@d3
    invoke-virtual {v11, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@d6
    move-result v0

    #@d7
    if-nez v0, :cond_e2

    #@d9
    const-string/jumbo v0, "tty_vco"

    #@dc
    invoke-virtual {v11, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@df
    move-result v0

    #@e0
    if-eqz v0, :cond_108

    #@e2
    .line 1022
    :cond_e2
    const-string v0, "AudioService"

    #@e4
    new-instance v1, Ljava/lang/StringBuilder;

    #@e6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e9
    const-string v2, "adjustSuggestedStreamVolume() Cannot adjust the volume during TTY mode : "

    #@eb
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v1

    #@ef
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v1

    #@f3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f6
    move-result-object v1

    #@f7
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@fa
    .line 1023
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@fc
    const/16 v1, 0x1d

    #@fe
    const/4 v2, 0x2

    #@ff
    const/4 v3, 0x3

    #@100
    const/4 v4, 0x0

    #@101
    const/4 v5, 0x0

    #@102
    const/4 v6, 0x0

    #@103
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@106
    goto/16 :goto_6f

    #@108
    .line 1029
    .end local v11           #ttyMode:Ljava/lang/String;
    :cond_108
    const/16 v0, -0xc8

    #@10a
    if-eq v10, v0, :cond_123

    #@10c
    and-int/lit8 v0, p3, 0x4

    #@10e
    if-eqz v0, :cond_123

    #@110
    iget-object v0, p0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@112
    aget v0, v0, v10

    #@114
    const/4 v1, 0x2

    #@115
    if-ne v0, v1, :cond_128

    #@117
    iget-object v0, p0, Landroid/media/AudioService;->mKeyguardManager:Landroid/app/KeyguardManager;

    #@119
    if-eqz v0, :cond_123

    #@11b
    iget-object v0, p0, Landroid/media/AudioService;->mKeyguardManager:Landroid/app/KeyguardManager;

    #@11d
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    #@120
    move-result v0

    #@121
    if-nez v0, :cond_128

    #@123
    :cond_123
    iget v0, p0, Landroid/media/AudioService;->mMode:I

    #@125
    const/4 v1, 0x1

    #@126
    if-ne v0, v1, :cond_12a

    #@128
    .line 1034
    :cond_128
    and-int/lit8 p3, p3, -0x5

    #@12a
    .line 1037
    :cond_12a
    iget-boolean v0, p0, Landroid/media/AudioService;->mIsSoundProfile:Z

    #@12c
    if-eqz v0, :cond_157

    #@12e
    invoke-virtual {p0}, Landroid/media/AudioService;->getMasterStreamType()I

    #@131
    move-result v0

    #@132
    if-ne v10, v0, :cond_157

    #@134
    and-int/lit8 v0, p3, 0x1

    #@136
    if-eqz v0, :cond_157

    #@138
    .line 1038
    const/4 v0, -0x1

    #@139
    if-eq p1, v0, :cond_13e

    #@13b
    const/4 v0, 0x1

    #@13c
    if-ne p1, v0, :cond_152

    #@13e
    .line 1039
    :cond_13e
    iget-object v0, p0, Landroid/media/AudioService;->mVolumePanel:Landroid/view/VolumePanel;

    #@140
    invoke-virtual {v0}, Landroid/view/VolumePanel;->isShowing()Z

    #@143
    move-result v0

    #@144
    if-nez v0, :cond_157

    #@146
    .line 1040
    iget-object v0, p0, Landroid/media/AudioService;->mVolumePanel:Landroid/view/VolumePanel;

    #@148
    invoke-virtual {v0, v10, p3}, Landroid/view/VolumePanel;->postVolumeChanged(II)V

    #@14b
    .line 1041
    iput p1, p0, Landroid/media/AudioService;->mPrevVolDirection:I

    #@14d
    .line 1042
    const/4 v0, 0x1

    #@14e
    iput-boolean v0, p0, Landroid/media/AudioService;->mIsVolumePanelVisible:Z

    #@150
    goto/16 :goto_6f

    #@152
    .line 1046
    :cond_152
    if-nez p1, :cond_157

    #@154
    .line 1047
    const/4 v0, 0x0

    #@155
    iput-boolean v0, p0, Landroid/media/AudioService;->mIsVolumePanelVisible:Z

    #@157
    .line 1052
    :cond_157
    const/16 v0, -0xc8

    #@159
    if-ne v10, v0, :cond_163

    #@15b
    .line 1054
    and-int/lit8 p3, p3, -0x25

    #@15d
    .line 1056
    const/4 v0, 0x3

    #@15e
    invoke-direct {p0, v0, p1, p3}, Landroid/media/AudioService;->adjustRemoteVolume(III)V

    #@161
    goto/16 :goto_6f

    #@163
    .line 1058
    :cond_163
    invoke-virtual {p0, v10, p1, p3}, Landroid/media/AudioService;->adjustStreamVolume(III)V

    #@166
    goto/16 :goto_6f
.end method

.method public adjustVolume(II)V
    .registers 4
    .parameter "direction"
    .parameter "flags"

    #@0
    .prologue
    .line 935
    const/high16 v0, -0x8000

    #@2
    invoke-virtual {p0, p1, v0, p2}, Landroid/media/AudioService;->adjustSuggestedStreamVolume(III)V

    #@5
    .line 936
    return-void
.end method

.method checkAudioSettingsPermission(Ljava/lang/String;)Z
    .registers 5
    .parameter "method"

    #@0
    .prologue
    .line 3228
    iget-object v1, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.MODIFY_AUDIO_SETTINGS"

    #@4
    invoke-virtual {v1, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_c

    #@a
    .line 3230
    const/4 v1, 0x1

    #@b
    .line 3236
    :goto_b
    return v1

    #@c
    .line 3232
    :cond_c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v2, "Audio Settings Permission Denial: "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, " from pid="

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@24
    move-result v2

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    const-string v2, ", uid="

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@32
    move-result v2

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v0

    #@3b
    .line 3235
    .local v0, msg:Ljava/lang/String;
    const-string v1, "AudioService"

    #@3d
    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 3236
    const/4 v1, 0x0

    #@41
    goto :goto_b
.end method

.method public checkPlayConditions(I)Z
    .registers 8
    .parameter "streamType"

    #@0
    .prologue
    const/4 v5, 0x4

    #@1
    const/4 v3, 0x0

    #@2
    const/4 v4, 0x2

    #@3
    .line 7046
    const/4 v0, 0x1

    #@4
    .line 7047
    .local v0, bResult:Z
    packed-switch p1, :pswitch_data_d0

    #@7
    .line 7079
    :cond_7
    :goto_7
    :pswitch_7
    iget v1, p0, Landroid/media/AudioService;->mMode:I

    #@9
    const/4 v2, 0x1

    #@a
    if-ne v1, v2, :cond_d

    #@c
    .line 7080
    const/4 v0, 0x0

    #@d
    .line 7086
    :cond_d
    :goto_d
    invoke-virtual {p0, p1}, Landroid/media/AudioService;->isStreamAffectedByRingerMode(I)Z

    #@10
    move-result v1

    #@11
    if-eqz v1, :cond_18

    #@13
    .line 7087
    iget v1, p0, Landroid/media/AudioService;->mRingerMode:I

    #@15
    if-eq v1, v4, :cond_18

    #@17
    .line 7088
    const/4 v0, 0x0

    #@18
    .line 7091
    :cond_18
    const-string v1, "AudioService"

    #@1a
    new-instance v2, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v3, "checkPlayConditions streamType:"

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    const-string v3, " mMode:"

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    iget v3, p0, Landroid/media/AudioService;->mMode:I

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    const-string v3, " mRingerMode:"

    #@37
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v2

    #@3b
    iget v3, p0, Landroid/media/AudioService;->mRingerMode:I

    #@3d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v2

    #@41
    const-string v3, " result:"

    #@43
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v2

    #@4f
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 7092
    return v0

    #@53
    .line 7049
    :pswitch_53
    iget v1, p0, Landroid/media/AudioService;->mRingerMode:I

    #@55
    if-eq v1, v4, :cond_d

    #@57
    .line 7050
    const/4 v0, 0x0

    #@58
    goto :goto_d

    #@59
    .line 7054
    :pswitch_59
    const-string v1, "audiorecording_state"

    #@5b
    invoke-static {v1}, Landroid/media/AudioSystem;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    #@5e
    move-result-object v1

    #@5f
    const-string/jumbo v2, "on"

    #@62
    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@65
    move-result v1

    #@66
    if-eqz v1, :cond_69

    #@68
    .line 7055
    const/4 v0, 0x0

    #@69
    .line 7058
    :cond_69
    :pswitch_69
    iget v1, p0, Landroid/media/AudioService;->mRingerMode:I

    #@6b
    if-eq v1, v4, :cond_d

    #@6d
    .line 7059
    invoke-static {v5, v3}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    #@70
    move-result v1

    #@71
    if-nez v1, :cond_7a

    #@73
    const/4 v1, 0x7

    #@74
    invoke-static {v1, v3}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    #@77
    move-result v1

    #@78
    if-eqz v1, :cond_7b

    #@7a
    .line 7060
    :cond_7a
    const/4 v0, 0x0

    #@7b
    .line 7064
    :cond_7b
    const-string v1, ""

    #@7d
    invoke-static {v5, v1}, Landroid/media/AudioSystem;->getDeviceConnectionState(ILjava/lang/String;)I

    #@80
    move-result v1

    #@81
    if-nez v1, :cond_d

    #@83
    const/16 v1, 0x8

    #@85
    const-string v2, ""

    #@87
    invoke-static {v1, v2}, Landroid/media/AudioSystem;->getDeviceConnectionState(ILjava/lang/String;)I

    #@8a
    move-result v1

    #@8b
    if-nez v1, :cond_d

    #@8d
    const/16 v1, 0x80

    #@8f
    const-string v2, ""

    #@91
    invoke-static {v1, v2}, Landroid/media/AudioSystem;->getDeviceConnectionState(ILjava/lang/String;)I

    #@94
    move-result v1

    #@95
    if-nez v1, :cond_d

    #@97
    const/4 v1, 0x5

    #@98
    if-ne p1, v1, :cond_d

    #@9a
    .line 7068
    const-string v1, "AudioService"

    #@9c
    new-instance v2, Ljava/lang/StringBuilder;

    #@9e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a1
    const-string v3, "checkPlayConditions ringermode: "

    #@a3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v2

    #@a7
    iget v3, p0, Landroid/media/AudioService;->mRingerMode:I

    #@a9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v2

    #@ad
    const-string v3, " without headset."

    #@af
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v2

    #@b3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b6
    move-result-object v2

    #@b7
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ba
    .line 7069
    const/4 v0, 0x0

    #@bb
    goto/16 :goto_d

    #@bd
    .line 7075
    :pswitch_bd
    const-string v1, "audiorecording_state"

    #@bf
    invoke-static {v1}, Landroid/media/AudioSystem;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    #@c2
    move-result-object v1

    #@c3
    const-string/jumbo v2, "on"

    #@c6
    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@c9
    move-result v1

    #@ca
    if-eqz v1, :cond_7

    #@cc
    .line 7076
    const/4 v0, 0x0

    #@cd
    goto/16 :goto_7

    #@cf
    .line 7047
    nop

    #@d0
    :pswitch_data_d0
    .packed-switch 0x1
        :pswitch_bd
        :pswitch_69
        :pswitch_7
        :pswitch_7
        :pswitch_59
        :pswitch_7
        :pswitch_bd
        :pswitch_53
    .end packed-switch
.end method

.method public clearAllScoClients(IZ)V
    .registers 9
    .parameter "exceptPid"
    .parameter "stopSco"

    #@0
    .prologue
    .line 2689
    iget-object v5, p0, Landroid/media/AudioService;->mScoClients:Ljava/util/ArrayList;

    #@2
    monitor-enter v5

    #@3
    .line 2690
    const/4 v2, 0x0

    #@4
    .line 2691
    .local v2, savedClient:Landroid/media/AudioService$ScoClient;
    :try_start_4
    iget-object v4, p0, Landroid/media/AudioService;->mScoClients:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v3

    #@a
    .line 2692
    .local v3, size:I
    const/4 v1, 0x0

    #@b
    .local v1, i:I
    :goto_b
    if-ge v1, v3, :cond_23

    #@d
    .line 2693
    iget-object v4, p0, Landroid/media/AudioService;->mScoClients:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Landroid/media/AudioService$ScoClient;

    #@15
    .line 2694
    .local v0, cl:Landroid/media/AudioService$ScoClient;
    invoke-virtual {v0}, Landroid/media/AudioService$ScoClient;->getPid()I

    #@18
    move-result v4

    #@19
    if-eq v4, p1, :cond_21

    #@1b
    .line 2695
    invoke-virtual {v0, p2}, Landroid/media/AudioService$ScoClient;->clearCount(Z)V

    #@1e
    .line 2692
    :goto_1e
    add-int/lit8 v1, v1, 0x1

    #@20
    goto :goto_b

    #@21
    .line 2697
    :cond_21
    move-object v2, v0

    #@22
    goto :goto_1e

    #@23
    .line 2700
    .end local v0           #cl:Landroid/media/AudioService$ScoClient;
    :cond_23
    iget-object v4, p0, Landroid/media/AudioService;->mScoClients:Ljava/util/ArrayList;

    #@25
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    #@28
    .line 2701
    if-eqz v2, :cond_2f

    #@2a
    .line 2702
    iget-object v4, p0, Landroid/media/AudioService;->mScoClients:Ljava/util/ArrayList;

    #@2c
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2f
    .line 2704
    :cond_2f
    monitor-exit v5

    #@30
    .line 2705
    return-void

    #@31
    .line 2704
    .end local v1           #i:I
    .end local v3           #size:I
    :catchall_31
    move-exception v4

    #@32
    monitor-exit v5
    :try_end_33
    .catchall {:try_start_4 .. :try_end_33} :catchall_31

    #@33
    throw v4
.end method

.method public disableSafeMediaVolume()V
    .registers 3

    #@0
    .prologue
    .line 6911
    iget-object v1, p0, Landroid/media/AudioService;->mSafeMediaVolumeState:Ljava/lang/Integer;

    #@2
    monitor-enter v1

    #@3
    .line 6912
    const/4 v0, 0x0

    #@4
    :try_start_4
    invoke-direct {p0, v0}, Landroid/media/AudioService;->setSafeMediaVolumeEnabled(Z)V

    #@7
    .line 6913
    monitor-exit v1

    #@8
    .line 6914
    return-void

    #@9
    .line 6913
    :catchall_9
    move-exception v0

    #@a
    monitor-exit v1
    :try_end_b
    .catchall {:try_start_4 .. :try_end_b} :catchall_9

    #@b
    throw v0
.end method

.method public dispatchMediaKeyEvent(Landroid/view/KeyEvent;)V
    .registers 3
    .parameter "keyEvent"

    #@0
    .prologue
    .line 5202
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/media/AudioService;->filterMediaKeyEvent(Landroid/view/KeyEvent;Z)V

    #@4
    .line 5203
    return-void
.end method

.method public dispatchMediaKeyEventUnderWakelock(Landroid/view/KeyEvent;)V
    .registers 3
    .parameter "keyEvent"

    #@0
    .prologue
    .line 5206
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, v0}, Landroid/media/AudioService;->filterMediaKeyEvent(Landroid/view/KeyEvent;Z)V

    #@4
    .line 5207
    return-void
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 7
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 6951
    iget-object v0, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.DUMP"

    #@4
    const-string v2, "AudioService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 6953
    invoke-direct {p0, p2}, Landroid/media/AudioService;->dumpFocusStack(Ljava/io/PrintWriter;)V

    #@c
    .line 6954
    invoke-direct {p0, p2}, Landroid/media/AudioService;->dumpRCStack(Ljava/io/PrintWriter;)V

    #@f
    .line 6955
    invoke-direct {p0, p2}, Landroid/media/AudioService;->dumpRCCStack(Ljava/io/PrintWriter;)V

    #@12
    .line 6956
    invoke-direct {p0, p2}, Landroid/media/AudioService;->dumpStreamStates(Ljava/io/PrintWriter;)V

    #@15
    .line 6957
    invoke-direct {p0, p2}, Landroid/media/AudioService;->dumpRingerMode(Ljava/io/PrintWriter;)V

    #@18
    .line 6958
    const-string v0, "\nAudio routes:"

    #@1a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1d
    .line 6959
    const-string v0, "  mMainType=0x"

    #@1f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@22
    iget-object v0, p0, Landroid/media/AudioService;->mCurAudioRoutes:Landroid/media/AudioRoutesInfo;

    #@24
    iget v0, v0, Landroid/media/AudioRoutesInfo;->mMainType:I

    #@26
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@29
    move-result-object v0

    #@2a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2d
    .line 6960
    const-string v0, "  mBluetoothName="

    #@2f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@32
    iget-object v0, p0, Landroid/media/AudioService;->mCurAudioRoutes:Landroid/media/AudioRoutesInfo;

    #@34
    iget-object v0, v0, Landroid/media/AudioRoutesInfo;->mBluetoothName:Ljava/lang/CharSequence;

    #@36
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@39
    .line 6961
    return-void
.end method

.method public forceVolumeControlStream(ILandroid/os/IBinder;)V
    .registers 6
    .parameter "streamType"
    .parameter "cb"

    #@0
    .prologue
    .line 1316
    iget-object v1, p0, Landroid/media/AudioService;->mForceControlStreamLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 1317
    :try_start_3
    iput p1, p0, Landroid/media/AudioService;->mVolumeControlStream:I

    #@5
    .line 1318
    iget v0, p0, Landroid/media/AudioService;->mVolumeControlStream:I

    #@7
    const/4 v2, -0x1

    #@8
    if-ne v0, v2, :cond_18

    #@a
    .line 1319
    iget-object v0, p0, Landroid/media/AudioService;->mForceControlStreamClient:Landroid/media/AudioService$ForceControlStreamClient;

    #@c
    if-eqz v0, :cond_16

    #@e
    .line 1320
    iget-object v0, p0, Landroid/media/AudioService;->mForceControlStreamClient:Landroid/media/AudioService$ForceControlStreamClient;

    #@10
    invoke-virtual {v0}, Landroid/media/AudioService$ForceControlStreamClient;->release()V

    #@13
    .line 1321
    const/4 v0, 0x0

    #@14
    iput-object v0, p0, Landroid/media/AudioService;->mForceControlStreamClient:Landroid/media/AudioService$ForceControlStreamClient;

    #@16
    .line 1326
    :cond_16
    :goto_16
    monitor-exit v1

    #@17
    .line 1327
    return-void

    #@18
    .line 1324
    :cond_18
    new-instance v0, Landroid/media/AudioService$ForceControlStreamClient;

    #@1a
    invoke-direct {v0, p0, p2}, Landroid/media/AudioService$ForceControlStreamClient;-><init>(Landroid/media/AudioService;Landroid/os/IBinder;)V

    #@1d
    iput-object v0, p0, Landroid/media/AudioService;->mForceControlStreamClient:Landroid/media/AudioService$ForceControlStreamClient;

    #@1f
    goto :goto_16

    #@20
    .line 1326
    :catchall_20
    move-exception v0

    #@21
    monitor-exit v1
    :try_end_22
    .catchall {:try_start_3 .. :try_end_22} :catchall_20

    #@22
    throw v0
.end method

.method public getInCallMode()I
    .registers 2

    #@0
    .prologue
    .line 2145
    iget v0, p0, Landroid/media/AudioService;->mState:I

    #@2
    return v0
.end method

.method public getLastAudibleMasterVolume()I
    .registers 3

    #@0
    .prologue
    .line 1650
    invoke-static {}, Landroid/media/AudioSystem;->getMasterVolume()F

    #@3
    move-result v0

    #@4
    const/high16 v1, 0x42c8

    #@6
    mul-float/2addr v0, v1

    #@7
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    #@a
    move-result v0

    #@b
    return v0
.end method

.method public getLastAudibleStreamVolume(I)I
    .registers 5
    .parameter "streamType"

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    .line 1635
    invoke-direct {p0, p1}, Landroid/media/AudioService;->ensureValidStreamType(I)V

    #@4
    .line 1636
    invoke-direct {p0, p1}, Landroid/media/AudioService;->getDeviceForStream(I)I

    #@7
    move-result v0

    #@8
    .line 1638
    .local v0, device:I
    iget v1, p0, Landroid/media/AudioService;->mRingerMode:I

    #@a
    if-eq v1, v2, :cond_1a

    #@c
    const/16 v1, 0x8

    #@e
    if-eq v0, v1, :cond_13

    #@10
    const/4 v1, 0x4

    #@11
    if-ne v0, v1, :cond_1a

    #@13
    :cond_13
    if-ne p1, v2, :cond_1a

    #@15
    iget-boolean v1, p0, Landroid/media/AudioService;->mIsSoundException:Z

    #@17
    if-eqz v1, :cond_1a

    #@19
    .line 1642
    const/4 v0, 0x2

    #@1a
    .line 1645
    :cond_1a
    iget-object v1, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@1c
    aget-object v1, v1, p1

    #@1e
    const/4 v2, 0x1

    #@1f
    invoke-virtual {v1, v0, v2}, Landroid/media/AudioService$VolumeStreamState;->getIndex(IZ)I

    #@22
    move-result v1

    #@23
    add-int/lit8 v1, v1, 0x5

    #@25
    div-int/lit8 v1, v1, 0xa

    #@27
    return v1
.end method

.method public getMasterMaxVolume()I
    .registers 2

    #@0
    .prologue
    .line 1630
    const/16 v0, 0x64

    #@2
    return v0
.end method

.method public getMasterStreamType()I
    .registers 2

    #@0
    .prologue
    .line 1655
    iget-boolean v0, p0, Landroid/media/AudioService;->mVoiceCapable:Z

    #@2
    if-eqz v0, :cond_6

    #@4
    .line 1656
    const/4 v0, 0x2

    #@5
    .line 1658
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x3

    #@7
    goto :goto_5
.end method

.method public getMasterVolume()I
    .registers 2

    #@0
    .prologue
    .line 1593
    invoke-virtual {p0}, Landroid/media/AudioService;->isMasterMute()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x0

    #@7
    .line 1594
    :goto_7
    return v0

    #@8
    :cond_8
    invoke-virtual {p0}, Landroid/media/AudioService;->getLastAudibleMasterVolume()I

    #@b
    move-result v0

    #@c
    goto :goto_7
.end method

.method public getMode()I
    .registers 2

    #@0
    .prologue
    .line 2140
    iget v0, p0, Landroid/media/AudioService;->mMode:I

    #@2
    return v0
.end method

.method public getRecordHookingState()I
    .registers 4

    #@0
    .prologue
    .line 7291
    const-string v0, "AudioService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getRecordHookingState() state = "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget v2, p0, Landroid/media/AudioService;->mRecordHookingState:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 7292
    iget v0, p0, Landroid/media/AudioService;->mRecordHookingState:I

    #@1c
    return v0
.end method

.method public getRemoteStreamMaxVolume()I
    .registers 4

    #@0
    .prologue
    .line 6577
    iget-object v1, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@2
    monitor-enter v1

    #@3
    .line 6578
    :try_start_3
    iget-object v0, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@5
    iget v0, v0, Landroid/media/AudioService$RemotePlaybackState;->mRccId:I

    #@7
    const/4 v2, -0x1

    #@8
    if-ne v0, v2, :cond_d

    #@a
    .line 6579
    const/4 v0, 0x0

    #@b
    monitor-exit v1

    #@c
    .line 6581
    :goto_c
    return v0

    #@d
    :cond_d
    iget-object v0, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@f
    iget v0, v0, Landroid/media/AudioService$RemotePlaybackState;->mVolumeMax:I

    #@11
    monitor-exit v1

    #@12
    goto :goto_c

    #@13
    .line 6582
    :catchall_13
    move-exception v0

    #@14
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_13

    #@15
    throw v0
.end method

.method public getRemoteStreamVolume()I
    .registers 4

    #@0
    .prologue
    .line 6586
    iget-object v1, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@2
    monitor-enter v1

    #@3
    .line 6587
    :try_start_3
    iget-object v0, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@5
    iget v0, v0, Landroid/media/AudioService$RemotePlaybackState;->mRccId:I

    #@7
    const/4 v2, -0x1

    #@8
    if-ne v0, v2, :cond_d

    #@a
    .line 6588
    const/4 v0, 0x0

    #@b
    monitor-exit v1

    #@c
    .line 6590
    :goto_c
    return v0

    #@d
    :cond_d
    iget-object v0, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@f
    iget v0, v0, Landroid/media/AudioService$RemotePlaybackState;->mVolume:I

    #@11
    monitor-exit v1

    #@12
    goto :goto_c

    #@13
    .line 6591
    :catchall_13
    move-exception v0

    #@14
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_13

    #@15
    throw v0
.end method

.method public getRingerMode()I
    .registers 3

    #@0
    .prologue
    .line 1664
    iget-object v1, p0, Landroid/media/AudioService;->mSettingsLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 1665
    :try_start_3
    iget v0, p0, Landroid/media/AudioService;->mRingerMode:I

    #@5
    monitor-exit v1

    #@6
    return v0

    #@7
    .line 1666
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public getRingtonePlayer()Landroid/media/IRingtonePlayer;
    .registers 2

    #@0
    .prologue
    .line 6771
    iget-object v0, p0, Landroid/media/AudioService;->mRingtonePlayer:Landroid/media/IRingtonePlayer;

    #@2
    return-object v0
.end method

.method public getStreamMaxVolume(I)I
    .registers 3
    .parameter "streamType"

    #@0
    .prologue
    .line 1625
    invoke-direct {p0, p1}, Landroid/media/AudioService;->ensureValidStreamType(I)V

    #@3
    .line 1626
    iget-object v0, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@5
    aget-object v0, v0, p1

    #@7
    invoke-virtual {v0}, Landroid/media/AudioService$VolumeStreamState;->getMaxIndex()I

    #@a
    move-result v0

    #@b
    add-int/lit8 v0, v0, 0x5

    #@d
    div-int/lit8 v0, v0, 0xa

    #@f
    return v0
.end method

.method public getStreamVolume(I)I
    .registers 7
    .parameter "streamType"

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v2, 0x0

    #@2
    .line 1575
    invoke-direct {p0, p1}, Landroid/media/AudioService;->ensureValidStreamType(I)V

    #@5
    .line 1577
    iget-boolean v3, p0, Landroid/media/AudioService;->mIsSoundException:Z

    #@7
    if-eqz v3, :cond_10

    #@9
    iget v3, p0, Landroid/media/AudioService;->mRingerMode:I

    #@b
    if-eq v3, v4, :cond_10

    #@d
    if-ne p1, v4, :cond_10

    #@f
    .line 1589
    :goto_f
    return v2

    #@10
    .line 1580
    :cond_10
    invoke-direct {p0, p1}, Landroid/media/AudioService;->getDeviceForStream(I)I

    #@13
    move-result v0

    #@14
    .line 1583
    .local v0, device:I
    iget-object v3, p0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@16
    aget v3, v3, p1

    #@18
    const/4 v4, 0x3

    #@19
    if-ne v3, v4, :cond_2c

    #@1b
    and-int/lit8 v3, v0, 0x0

    #@1d
    if-eqz v3, :cond_2c

    #@1f
    .line 1585
    iget-object v2, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@21
    aget-object v2, v2, p1

    #@23
    invoke-virtual {v2}, Landroid/media/AudioService$VolumeStreamState;->getMaxIndex()I

    #@26
    move-result v1

    #@27
    .line 1589
    .local v1, index:I
    :goto_27
    add-int/lit8 v2, v1, 0x5

    #@29
    div-int/lit8 v2, v2, 0xa

    #@2b
    goto :goto_f

    #@2c
    .line 1587
    .end local v1           #index:I
    :cond_2c
    iget-object v3, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@2e
    aget-object v3, v3, p1

    #@30
    invoke-virtual {v3, v0, v2}, Landroid/media/AudioService$VolumeStreamState;->getIndex(IZ)I

    #@33
    move-result v1

    #@34
    .restart local v1       #index:I
    goto :goto_27
.end method

.method public getVibrateSetting(I)I
    .registers 4
    .parameter "vibrateType"

    #@0
    .prologue
    .line 1775
    iget-boolean v0, p0, Landroid/media/AudioService;->mHasVibrator:Z

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    .line 1776
    :goto_5
    return v0

    #@6
    :cond_6
    iget v0, p0, Landroid/media/AudioService;->mVibrateSetting:I

    #@8
    mul-int/lit8 v1, p1, 0x2

    #@a
    shr-int/2addr v0, v1

    #@b
    and-int/lit8 v0, v0, 0x3

    #@d
    goto :goto_5
.end method

.method public getVoiceActivationState()I
    .registers 4

    #@0
    .prologue
    .line 7256
    const-string v0, "AudioService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getVoiceActivationState().. mVoiceActivationState = "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget v2, p0, Landroid/media/AudioService;->mVoiceActivationState:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 7257
    iget v0, p0, Landroid/media/AudioService;->mVoiceActivationState:I

    #@1c
    return v0
.end method

.method public isBluetoothA2dpOn()Z
    .registers 3

    #@0
    .prologue
    .line 2481
    iget-object v1, p0, Landroid/media/AudioService;->mBluetoothA2dpEnabledLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 2482
    :try_start_3
    iget-boolean v0, p0, Landroid/media/AudioService;->mBluetoothA2dpEnabled:Z

    #@5
    monitor-exit v1

    #@6
    return v0

    #@7
    .line 2483
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public isBluetoothAGConnected()Z
    .registers 4

    #@0
    .prologue
    .line 2865
    const/4 v0, 0x0

    #@1
    .line 2866
    .local v0, isConnected:Z
    iget-object v1, p0, Landroid/media/AudioService;->mConnectedDevices:Ljava/util/HashMap;

    #@3
    const/16 v2, 0x10

    #@5
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8
    move-result-object v2

    #@9
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@c
    move-result v1

    #@d
    if-nez v1, :cond_2b

    #@f
    iget-object v1, p0, Landroid/media/AudioService;->mConnectedDevices:Ljava/util/HashMap;

    #@11
    const/16 v2, 0x20

    #@13
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@1a
    move-result v1

    #@1b
    if-nez v1, :cond_2b

    #@1d
    iget-object v1, p0, Landroid/media/AudioService;->mConnectedDevices:Ljava/util/HashMap;

    #@1f
    const/16 v2, 0x40

    #@21
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@28
    move-result v1

    #@29
    if-eqz v1, :cond_2d

    #@2b
    .line 2869
    :cond_2b
    const/4 v0, 0x1

    #@2c
    .line 2873
    :goto_2c
    return v0

    #@2d
    .line 2871
    :cond_2d
    const/4 v0, 0x0

    #@2e
    goto :goto_2c
.end method

.method public isBluetoothAVConnected()Z
    .registers 4

    #@0
    .prologue
    .line 2879
    iget-object v1, p0, Landroid/media/AudioService;->mConnectedDevices:Ljava/util/HashMap;

    #@2
    const/16 v2, 0x80

    #@4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7
    move-result-object v2

    #@8
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    .line 2880
    .local v0, isConnected:Z
    return v0
.end method

.method public isBluetoothScoOn()Z
    .registers 3

    #@0
    .prologue
    .line 2465
    iget v0, p0, Landroid/media/AudioService;->mForcedUseForComm:I

    #@2
    const/4 v1, 0x3

    #@3
    if-ne v0, v1, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public isCameraSoundForced()Z
    .registers 3

    #@0
    .prologue
    .line 6929
    iget-object v1, p0, Landroid/media/AudioService;->mCameraSoundForced:Ljava/lang/Boolean;

    #@2
    monitor-enter v1

    #@3
    .line 6930
    :try_start_3
    iget-object v0, p0, Landroid/media/AudioService;->mCameraSoundForced:Ljava/lang/Boolean;

    #@5
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    #@8
    move-result v0

    #@9
    monitor-exit v1

    #@a
    return v0

    #@b
    .line 6931
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method public isMasterMute()Z
    .registers 2

    #@0
    .prologue
    .line 1570
    invoke-static {}, Landroid/media/AudioSystem;->getMasterMute()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public isSpeakerOnForMedia()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 6976
    iget v1, p0, Landroid/media/AudioService;->mForcedUseForMedia:I

    #@3
    if-ne v1, v0, :cond_6

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public isSpeakerphoneOn()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 2422
    iget v1, p0, Landroid/media/AudioService;->mForcedUseForComm:I

    #@3
    if-ne v1, v0, :cond_6

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public isStreamAffectedByMute(I)Z
    .registers 5
    .parameter "streamType"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 3057
    iget v1, p0, Landroid/media/AudioService;->mMuteAffectedStreams:I

    #@3
    shl-int v2, v0, p1

    #@5
    and-int/2addr v1, v2

    #@6
    if-eqz v1, :cond_9

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public isStreamAffectedByRingerMode(I)Z
    .registers 5
    .parameter "streamType"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 3049
    iget v1, p0, Landroid/media/AudioService;->mRingerModeAffectedStreams:I

    #@3
    shl-int v2, v0, p1

    #@5
    and-int/2addr v1, v2

    #@6
    if-eqz v1, :cond_9

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public isStreamMute(I)Z
    .registers 5
    .parameter "streamType"

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    const/4 v0, 0x1

    #@2
    .line 1551
    iget-boolean v1, p0, Landroid/media/AudioService;->mIsSoundException:Z

    #@4
    if-eqz v1, :cond_d

    #@6
    iget v1, p0, Landroid/media/AudioService;->mRingerMode:I

    #@8
    if-eq v1, v2, :cond_d

    #@a
    if-ne p1, v2, :cond_d

    #@c
    .line 1554
    :cond_c
    :goto_c
    return v0

    #@d
    :cond_d
    iget-object v1, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@f
    aget-object v1, v1, p1

    #@11
    invoke-static {v1}, Landroid/media/AudioService$VolumeStreamState;->access$700(Landroid/media/AudioService$VolumeStreamState;)I

    #@14
    move-result v1

    #@15
    if-nez v1, :cond_c

    #@17
    const/4 v0, 0x0

    #@18
    goto :goto_c
.end method

.method public isVirtualMirrorLinkDevicedConnected(I)Z
    .registers 8
    .parameter "streamType"

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    const/4 v1, 0x0

    #@2
    .line 7309
    const/4 v0, 0x0

    #@3
    .line 7311
    .local v0, device:I
    iget-boolean v2, p0, Landroid/media/AudioService;->MirroLinkTestKey:Z

    #@5
    if-nez v2, :cond_16

    #@7
    iget-object v2, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@9
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@c
    move-result-object v2

    #@d
    const v3, 0x206001e

    #@10
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@13
    move-result v2

    #@14
    if-eqz v2, :cond_4c

    #@16
    .line 7313
    :cond_16
    const-string/jumbo v2, "isVirtualMirrorLinkDevice"

    #@19
    invoke-static {v2}, Landroid/media/AudioSystem;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    const-string/jumbo v3, "true"

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@23
    move-result v2

    #@24
    if-eqz v2, :cond_4c

    #@26
    .line 7315
    const-string v2, "AudioService"

    #@28
    new-instance v3, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string/jumbo v4, "wkcp isVirtualMirrorLinkDevicedConnected... streamType > "

    #@30
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v3

    #@3c
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 7316
    invoke-virtual {p0}, Landroid/media/AudioService;->getMode()I

    #@42
    move-result v2

    #@43
    if-eq v2, v5, :cond_4c

    #@45
    invoke-virtual {p0}, Landroid/media/AudioService;->getMode()I

    #@48
    move-result v2

    #@49
    const/4 v3, 0x3

    #@4a
    if-ne v2, v3, :cond_4d

    #@4c
    .line 7333
    :cond_4c
    :goto_4c
    return v1

    #@4d
    .line 7320
    :cond_4d
    invoke-direct {p0, p1}, Landroid/media/AudioService;->getDeviceForStream(I)I

    #@50
    move-result v2

    #@51
    if-ne v2, v5, :cond_4c

    #@53
    .line 7322
    const-string v1, "AudioService"

    #@55
    const-string/jumbo v2, "wkcp isVirtualMirrorLinkDevicedConnected... volume fixed... "

    #@58
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    .line 7323
    const/4 v1, 0x1

    #@5c
    goto :goto_4c
.end method

.method public isWiredHeadsetConnected()Z
    .registers 4

    #@0
    .prologue
    .line 2886
    iget-object v1, p0, Landroid/media/AudioService;->mConnectedDevices:Ljava/util/HashMap;

    #@2
    const/4 v2, 0x4

    #@3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6
    move-result-object v2

    #@7
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@a
    move-result v0

    #@b
    .line 2887
    .local v0, isConnected:Z
    return v0
.end method

.method public loadSoundEffects()Z
    .registers 14

    #@0
    .prologue
    .line 2168
    iget-object v9, p0, Landroid/media/AudioService;->mSoundEffectsLock:Ljava/lang/Object;

    #@2
    monitor-enter v9

    #@3
    .line 2169
    :try_start_3
    iget-boolean v8, p0, Landroid/media/AudioService;->mBootCompleted:Z

    #@5
    if-nez v8, :cond_12

    #@7
    .line 2170
    const-string v8, "AudioService"

    #@9
    const-string/jumbo v10, "loadSoundEffects() called before boot complete"

    #@c
    invoke-static {v8, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 2171
    const/4 v8, 0x0

    #@10
    monitor-exit v9

    #@11
    .line 2273
    :goto_11
    return v8

    #@12
    .line 2174
    :cond_12
    iget-object v8, p0, Landroid/media/AudioService;->mSoundPool:Landroid/media/SoundPool;

    #@14
    if-eqz v8, :cond_1c

    #@16
    .line 2175
    const/4 v8, 0x1

    #@17
    monitor-exit v9

    #@18
    goto :goto_11

    #@19
    .line 2272
    :catchall_19
    move-exception v8

    #@1a
    monitor-exit v9
    :try_end_1b
    .catchall {:try_start_3 .. :try_end_1b} :catchall_19

    #@1b
    throw v8

    #@1c
    .line 2177
    :cond_1c
    :try_start_1c
    new-instance v8, Landroid/media/SoundPool;

    #@1e
    const/4 v10, 0x4

    #@1f
    const/4 v11, 0x1

    #@20
    const/4 v12, 0x0

    #@21
    invoke-direct {v8, v10, v11, v12}, Landroid/media/SoundPool;-><init>(III)V

    #@24
    iput-object v8, p0, Landroid/media/AudioService;->mSoundPool:Landroid/media/SoundPool;
    :try_end_26
    .catchall {:try_start_1c .. :try_end_26} :catchall_19

    #@26
    .line 2180
    const/4 v8, 0x0

    #@27
    :try_start_27
    iput-object v8, p0, Landroid/media/AudioService;->mSoundPoolCallBack:Landroid/media/AudioService$SoundPoolCallback;

    #@29
    .line 2181
    new-instance v8, Landroid/media/AudioService$SoundPoolListenerThread;

    #@2b
    invoke-direct {v8, p0}, Landroid/media/AudioService$SoundPoolListenerThread;-><init>(Landroid/media/AudioService;)V

    #@2e
    iput-object v8, p0, Landroid/media/AudioService;->mSoundPoolListenerThread:Landroid/media/AudioService$SoundPoolListenerThread;

    #@30
    .line 2182
    iget-object v8, p0, Landroid/media/AudioService;->mSoundPoolListenerThread:Landroid/media/AudioService$SoundPoolListenerThread;

    #@32
    invoke-virtual {v8}, Landroid/media/AudioService$SoundPoolListenerThread;->start()V

    #@35
    .line 2184
    iget-object v8, p0, Landroid/media/AudioService;->mSoundEffectsLock:Ljava/lang/Object;

    #@37
    invoke-virtual {v8}, Ljava/lang/Object;->wait()V
    :try_end_3a
    .catchall {:try_start_27 .. :try_end_3a} :catchall_19
    .catch Ljava/lang/InterruptedException; {:try_start_27 .. :try_end_3a} :catch_60

    #@3a
    .line 2189
    :goto_3a
    :try_start_3a
    iget-object v8, p0, Landroid/media/AudioService;->mSoundPoolCallBack:Landroid/media/AudioService$SoundPoolCallback;

    #@3c
    if-nez v8, :cond_69

    #@3e
    .line 2190
    const-string v8, "AudioService"

    #@40
    const-string/jumbo v10, "loadSoundEffects() could not create SoundPool listener or thread"

    #@43
    invoke-static {v8, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 2191
    iget-object v8, p0, Landroid/media/AudioService;->mSoundPoolLooper:Landroid/os/Looper;

    #@48
    if-eqz v8, :cond_52

    #@4a
    .line 2192
    iget-object v8, p0, Landroid/media/AudioService;->mSoundPoolLooper:Landroid/os/Looper;

    #@4c
    invoke-virtual {v8}, Landroid/os/Looper;->quit()V

    #@4f
    .line 2193
    const/4 v8, 0x0

    #@50
    iput-object v8, p0, Landroid/media/AudioService;->mSoundPoolLooper:Landroid/os/Looper;

    #@52
    .line 2195
    :cond_52
    const/4 v8, 0x0

    #@53
    iput-object v8, p0, Landroid/media/AudioService;->mSoundPoolListenerThread:Landroid/media/AudioService$SoundPoolListenerThread;

    #@55
    .line 2196
    iget-object v8, p0, Landroid/media/AudioService;->mSoundPool:Landroid/media/SoundPool;

    #@57
    invoke-virtual {v8}, Landroid/media/SoundPool;->release()V

    #@5a
    .line 2197
    const/4 v8, 0x0

    #@5b
    iput-object v8, p0, Landroid/media/AudioService;->mSoundPool:Landroid/media/SoundPool;

    #@5d
    .line 2198
    const/4 v8, 0x0

    #@5e
    monitor-exit v9

    #@5f
    goto :goto_11

    #@60
    .line 2185
    :catch_60
    move-exception v0

    #@61
    .line 2186
    .local v0, e:Ljava/lang/InterruptedException;
    const-string v8, "AudioService"

    #@63
    const-string v10, "Interrupted while waiting sound pool listener thread."

    #@65
    invoke-static {v8, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@68
    goto :goto_3a

    #@69
    .line 2206
    .end local v0           #e:Ljava/lang/InterruptedException;
    :cond_69
    sget-object v8, Landroid/media/AudioService;->SOUND_EFFECT_FILES:[Ljava/lang/String;

    #@6b
    array-length v8, v8

    #@6c
    new-array v5, v8, [I

    #@6e
    .line 2207
    .local v5, poolId:[I
    const/4 v2, 0x0

    #@6f
    .local v2, fileIdx:I
    :goto_6f
    sget-object v8, Landroid/media/AudioService;->SOUND_EFFECT_FILES:[Ljava/lang/String;

    #@71
    array-length v8, v8

    #@72
    if-ge v2, v8, :cond_7a

    #@74
    .line 2208
    const/4 v8, -0x1

    #@75
    aput v8, v5, v2

    #@77
    .line 2207
    add-int/lit8 v2, v2, 0x1

    #@79
    goto :goto_6f

    #@7a
    .line 2216
    :cond_7a
    const/4 v4, 0x0

    #@7b
    .line 2217
    .local v4, lastSample:I
    const/4 v1, 0x0

    #@7c
    .local v1, effect:I
    :goto_7c
    const/16 v8, 0xb

    #@7e
    if-ge v1, v8, :cond_103

    #@80
    .line 2219
    iget-object v8, p0, Landroid/media/AudioService;->SOUND_EFFECT_FILES_MAP:[[I

    #@82
    aget-object v8, v8, v1

    #@84
    const/4 v10, 0x1

    #@85
    aget v8, v8, v10

    #@87
    if-nez v8, :cond_8c

    #@89
    .line 2217
    :goto_89
    add-int/lit8 v1, v1, 0x1

    #@8b
    goto :goto_7c

    #@8c
    .line 2222
    :cond_8c
    iget-object v8, p0, Landroid/media/AudioService;->SOUND_EFFECT_FILES_MAP:[[I

    #@8e
    aget-object v8, v8, v1

    #@90
    const/4 v10, 0x0

    #@91
    aget v8, v8, v10

    #@93
    aget v8, v5, v8

    #@95
    const/4 v10, -0x1

    #@96
    if-ne v8, v10, :cond_f2

    #@98
    .line 2223
    new-instance v8, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    #@a0
    move-result-object v10

    #@a1
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v8

    #@a5
    const-string v10, "/media/audio/ui/"

    #@a7
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v8

    #@ab
    sget-object v10, Landroid/media/AudioService;->SOUND_EFFECT_FILES:[Ljava/lang/String;

    #@ad
    iget-object v11, p0, Landroid/media/AudioService;->SOUND_EFFECT_FILES_MAP:[[I

    #@af
    aget-object v11, v11, v1

    #@b1
    const/4 v12, 0x0

    #@b2
    aget v11, v11, v12

    #@b4
    aget-object v10, v10, v11

    #@b6
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v8

    #@ba
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bd
    move-result-object v3

    #@be
    .line 2226
    .local v3, filePath:Ljava/lang/String;
    iget-object v8, p0, Landroid/media/AudioService;->mSoundPool:Landroid/media/SoundPool;

    #@c0
    const/4 v10, 0x0

    #@c1
    invoke-virtual {v8, v3, v10}, Landroid/media/SoundPool;->load(Ljava/lang/String;I)I

    #@c4
    move-result v6

    #@c5
    .line 2227
    .local v6, sampleId:I
    if-gtz v6, :cond_e0

    #@c7
    .line 2228
    const-string v8, "AudioService"

    #@c9
    new-instance v10, Ljava/lang/StringBuilder;

    #@cb
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@ce
    const-string v11, "Soundpool could not load file: "

    #@d0
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v10

    #@d4
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v10

    #@d8
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@db
    move-result-object v10

    #@dc
    invoke-static {v8, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@df
    goto :goto_89

    #@e0
    .line 2230
    :cond_e0
    iget-object v8, p0, Landroid/media/AudioService;->SOUND_EFFECT_FILES_MAP:[[I

    #@e2
    aget-object v8, v8, v1

    #@e4
    const/4 v10, 0x1

    #@e5
    aput v6, v8, v10

    #@e7
    .line 2231
    iget-object v8, p0, Landroid/media/AudioService;->SOUND_EFFECT_FILES_MAP:[[I

    #@e9
    aget-object v8, v8, v1

    #@eb
    const/4 v10, 0x0

    #@ec
    aget v8, v8, v10

    #@ee
    aput v6, v5, v8

    #@f0
    .line 2232
    move v4, v6

    #@f1
    goto :goto_89

    #@f2
    .line 2235
    .end local v3           #filePath:Ljava/lang/String;
    .end local v6           #sampleId:I
    :cond_f2
    iget-object v8, p0, Landroid/media/AudioService;->SOUND_EFFECT_FILES_MAP:[[I

    #@f4
    aget-object v8, v8, v1

    #@f6
    const/4 v10, 0x1

    #@f7
    iget-object v11, p0, Landroid/media/AudioService;->SOUND_EFFECT_FILES_MAP:[[I

    #@f9
    aget-object v11, v11, v1

    #@fb
    const/4 v12, 0x0

    #@fc
    aget v11, v11, v12

    #@fe
    aget v11, v5, v11

    #@100
    aput v11, v8, v10

    #@102
    goto :goto_89

    #@103
    .line 2239
    :cond_103
    if-eqz v4, :cond_170

    #@105
    .line 2240
    iget-object v8, p0, Landroid/media/AudioService;->mSoundPoolCallBack:Landroid/media/AudioService$SoundPoolCallback;

    #@107
    invoke-virtual {v8, v4}, Landroid/media/AudioService$SoundPoolCallback;->setLastSample(I)V
    :try_end_10a
    .catchall {:try_start_3a .. :try_end_10a} :catchall_19

    #@10a
    .line 2243
    :try_start_10a
    iget-object v8, p0, Landroid/media/AudioService;->mSoundEffectsLock:Ljava/lang/Object;

    #@10c
    invoke-virtual {v8}, Ljava/lang/Object;->wait()V

    #@10f
    .line 2244
    iget-object v8, p0, Landroid/media/AudioService;->mSoundPoolCallBack:Landroid/media/AudioService$SoundPoolCallback;

    #@111
    invoke-virtual {v8}, Landroid/media/AudioService$SoundPoolCallback;->status()I
    :try_end_114
    .catchall {:try_start_10a .. :try_end_114} :catchall_19
    .catch Ljava/lang/InterruptedException; {:try_start_10a .. :try_end_114} :catch_166

    #@114
    move-result v7

    #@115
    .line 2253
    .local v7, status:I
    :goto_115
    :try_start_115
    iget-object v8, p0, Landroid/media/AudioService;->mSoundPoolLooper:Landroid/os/Looper;

    #@117
    if-eqz v8, :cond_121

    #@119
    .line 2254
    iget-object v8, p0, Landroid/media/AudioService;->mSoundPoolLooper:Landroid/os/Looper;

    #@11b
    invoke-virtual {v8}, Landroid/os/Looper;->quit()V

    #@11e
    .line 2255
    const/4 v8, 0x0

    #@11f
    iput-object v8, p0, Landroid/media/AudioService;->mSoundPoolLooper:Landroid/os/Looper;

    #@121
    .line 2257
    :cond_121
    const/4 v8, 0x0

    #@122
    iput-object v8, p0, Landroid/media/AudioService;->mSoundPoolListenerThread:Landroid/media/AudioService$SoundPoolListenerThread;

    #@124
    .line 2258
    if-eqz v7, :cond_17c

    #@126
    .line 2259
    const-string v10, "AudioService"

    #@128
    new-instance v8, Ljava/lang/StringBuilder;

    #@12a
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@12d
    const-string/jumbo v11, "loadSoundEffects(), Error "

    #@130
    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@133
    move-result-object v11

    #@134
    if-eqz v4, :cond_172

    #@136
    iget-object v8, p0, Landroid/media/AudioService;->mSoundPoolCallBack:Landroid/media/AudioService$SoundPoolCallback;

    #@138
    invoke-virtual {v8}, Landroid/media/AudioService$SoundPoolCallback;->status()I

    #@13b
    move-result v8

    #@13c
    :goto_13c
    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13f
    move-result-object v8

    #@140
    const-string v11, " while loading samples"

    #@142
    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@145
    move-result-object v8

    #@146
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@149
    move-result-object v8

    #@14a
    invoke-static {v10, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@14d
    .line 2263
    const/4 v1, 0x0

    #@14e
    :goto_14e
    const/16 v8, 0xb

    #@150
    if-ge v1, v8, :cond_174

    #@152
    .line 2264
    iget-object v8, p0, Landroid/media/AudioService;->SOUND_EFFECT_FILES_MAP:[[I

    #@154
    aget-object v8, v8, v1

    #@156
    const/4 v10, 0x1

    #@157
    aget v8, v8, v10

    #@159
    if-lez v8, :cond_163

    #@15b
    .line 2265
    iget-object v8, p0, Landroid/media/AudioService;->SOUND_EFFECT_FILES_MAP:[[I

    #@15d
    aget-object v8, v8, v1

    #@15f
    const/4 v10, 0x1

    #@160
    const/4 v11, -0x1

    #@161
    aput v11, v8, v10

    #@163
    .line 2263
    :cond_163
    add-int/lit8 v1, v1, 0x1

    #@165
    goto :goto_14e

    #@166
    .line 2245
    .end local v7           #status:I
    :catch_166
    move-exception v0

    #@167
    .line 2246
    .restart local v0       #e:Ljava/lang/InterruptedException;
    const-string v8, "AudioService"

    #@169
    const-string v10, "Interrupted while waiting sound pool callback."

    #@16b
    invoke-static {v8, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@16e
    .line 2247
    const/4 v7, -0x1

    #@16f
    .line 2248
    .restart local v7       #status:I
    goto :goto_115

    #@170
    .line 2250
    .end local v0           #e:Ljava/lang/InterruptedException;
    .end local v7           #status:I
    :cond_170
    const/4 v7, -0x1

    #@171
    .restart local v7       #status:I
    goto :goto_115

    #@172
    .line 2259
    :cond_172
    const/4 v8, -0x1

    #@173
    goto :goto_13c

    #@174
    .line 2269
    :cond_174
    iget-object v8, p0, Landroid/media/AudioService;->mSoundPool:Landroid/media/SoundPool;

    #@176
    invoke-virtual {v8}, Landroid/media/SoundPool;->release()V

    #@179
    .line 2270
    const/4 v8, 0x0

    #@17a
    iput-object v8, p0, Landroid/media/AudioService;->mSoundPool:Landroid/media/SoundPool;

    #@17c
    .line 2272
    :cond_17c
    monitor-exit v9
    :try_end_17d
    .catchall {:try_start_115 .. :try_end_17d} :catchall_19

    #@17d
    .line 2273
    if-nez v7, :cond_182

    #@17f
    const/4 v8, 0x1

    #@180
    goto/16 :goto_11

    #@182
    :cond_182
    const/4 v8, 0x0

    #@183
    goto/16 :goto_11
.end method

.method public onSendFinished(Landroid/app/PendingIntent;Landroid/content/Intent;ILjava/lang/String;Landroid/os/Bundle;)V
    .registers 7
    .parameter "pendingIntent"
    .parameter "intent"
    .parameter "resultCode"
    .parameter "resultData"
    .parameter "resultExtras"

    #@0
    .prologue
    .line 5500
    const/16 v0, 0x7bc

    #@2
    if-ne p3, v0, :cond_9

    #@4
    .line 5501
    iget-object v0, p0, Landroid/media/AudioService;->mMediaEventWakeLock:Landroid/os/PowerManager$WakeLock;

    #@6
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@9
    .line 5503
    :cond_9
    return-void
.end method

.method public playSoundEffect(I)V
    .registers 9
    .parameter "effectType"

    #@0
    .prologue
    .line 2150
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@2
    const/4 v1, 0x6

    #@3
    const/4 v2, 0x1

    #@4
    const/4 v4, -0x1

    #@5
    const/4 v5, 0x0

    #@6
    const/4 v6, 0x0

    #@7
    move v3, p1

    #@8
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@b
    .line 2152
    return-void
.end method

.method public playSoundEffectVolume(IF)V
    .registers 10
    .parameter "effectType"
    .parameter "volume"

    #@0
    .prologue
    .line 2156
    invoke-virtual {p0}, Landroid/media/AudioService;->loadSoundEffects()Z

    #@3
    .line 2157
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@5
    const/4 v1, 0x6

    #@6
    const/4 v2, 0x1

    #@7
    const/high16 v3, 0x447a

    #@9
    mul-float/2addr v3, p2

    #@a
    float-to-int v4, v3

    #@b
    const/4 v5, 0x0

    #@c
    const/4 v6, 0x0

    #@d
    move v3, p1

    #@e
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@11
    .line 2159
    return-void
.end method

.method public registerMediaButtonEventReceiverForCalls(Landroid/content/ComponentName;)V
    .registers 4
    .parameter "c"

    #@0
    .prologue
    .line 6121
    iget-object v0, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.MODIFY_PHONE_STATE"

    #@4
    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingPermission(Ljava/lang/String;)I

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_12

    #@a
    .line 6123
    const-string v0, "AudioService"

    #@c
    const-string v1, "Invalid permissions to register media button receiver for calls"

    #@e
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 6129
    :goto_11
    return-void

    #@12
    .line 6126
    :cond_12
    iget-object v1, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@14
    monitor-enter v1

    #@15
    .line 6127
    :try_start_15
    iput-object p1, p0, Landroid/media/AudioService;->mMediaReceiverForCalls:Landroid/content/ComponentName;

    #@17
    .line 6128
    monitor-exit v1

    #@18
    goto :goto_11

    #@19
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_15 .. :try_end_1b} :catchall_19

    #@1b
    throw v0
.end method

.method public registerMediaButtonIntent(Landroid/app/PendingIntent;Landroid/content/ComponentName;)V
    .registers 6
    .parameter "mediaIntent"
    .parameter "eventReceiver"

    #@0
    .prologue
    .line 6085
    const-string v0, "AudioService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "  Remote Control   registerMediaButtonIntent() for "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 6087
    sget-object v1, Landroid/media/AudioService;->mAudioFocusLock:Ljava/lang/Object;

    #@1a
    monitor-enter v1

    #@1b
    .line 6088
    :try_start_1b
    iget-object v2, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@1d
    monitor-enter v2
    :try_end_1e
    .catchall {:try_start_1b .. :try_end_1e} :catchall_2c

    #@1e
    .line 6089
    :try_start_1e
    invoke-direct {p0, p1, p2}, Landroid/media/AudioService;->pushMediaButtonReceiver(Landroid/app/PendingIntent;Landroid/content/ComponentName;)V

    #@21
    .line 6091
    const/16 v0, 0xf

    #@23
    invoke-direct {p0, v0}, Landroid/media/AudioService;->checkUpdateRemoteControlDisplay_syncAfRcs(I)V

    #@26
    .line 6092
    monitor-exit v2
    :try_end_27
    .catchall {:try_start_1e .. :try_end_27} :catchall_29

    #@27
    .line 6093
    :try_start_27
    monitor-exit v1
    :try_end_28
    .catchall {:try_start_27 .. :try_end_28} :catchall_2c

    #@28
    .line 6094
    return-void

    #@29
    .line 6092
    :catchall_29
    move-exception v0

    #@2a
    :try_start_2a
    monitor-exit v2
    :try_end_2b
    .catchall {:try_start_2a .. :try_end_2b} :catchall_29

    #@2b
    :try_start_2b
    throw v0

    #@2c
    .line 6093
    :catchall_2c
    move-exception v0

    #@2d
    monitor-exit v1
    :try_end_2e
    .catchall {:try_start_2b .. :try_end_2e} :catchall_2c

    #@2e
    throw v0
.end method

.method public registerRemoteControlClient(Landroid/app/PendingIntent;Landroid/media/IRemoteControlClient;Ljava/lang/String;)I
    .registers 15
    .parameter "mediaIntent"
    .parameter "rcClient"
    .parameter "callingPackageName"

    #@0
    .prologue
    .line 6155
    const/4 v2, -0x1

    #@1
    .line 6156
    .local v2, rccId:I
    sget-object v7, Landroid/media/AudioService;->mAudioFocusLock:Ljava/lang/Object;

    #@3
    monitor-enter v7

    #@4
    .line 6157
    :try_start_4
    iget-object v8, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@6
    monitor-enter v8
    :try_end_7
    .catchall {:try_start_4 .. :try_end_7} :catchall_69

    #@7
    .line 6159
    :try_start_7
    iget-object v6, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@9
    invoke-virtual {v6}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    #@c
    move-result-object v5

    #@d
    .line 6160
    .local v5, stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :cond_d
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@10
    move-result v6

    #@11
    if-eqz v6, :cond_37

    #@13
    .line 6161
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@16
    move-result-object v4

    #@17
    check-cast v4, Landroid/media/AudioService$RemoteControlStackEntry;

    #@19
    .line 6162
    .local v4, rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    iget-object v6, v4, Landroid/media/AudioService$RemoteControlStackEntry;->mMediaIntent:Landroid/app/PendingIntent;

    #@1b
    invoke-virtual {v6, p1}, Landroid/app/PendingIntent;->equals(Ljava/lang/Object;)Z

    #@1e
    move-result v6

    #@1f
    if-eqz v6, :cond_d

    #@21
    .line 6164
    iget-object v6, v4, Landroid/media/AudioService$RemoteControlStackEntry;->mRcClientDeathHandler:Landroid/media/AudioService$RcClientDeathHandler;

    #@23
    if-eqz v6, :cond_28

    #@25
    .line 6166
    invoke-virtual {v4}, Landroid/media/AudioService$RemoteControlStackEntry;->unlinkToRcClientDeath()V

    #@28
    .line 6169
    :cond_28
    iput-object p2, v4, Landroid/media/AudioService$RemoteControlStackEntry;->mRcClient:Landroid/media/IRemoteControlClient;

    #@2a
    .line 6170
    iput-object p3, v4, Landroid/media/AudioService$RemoteControlStackEntry;->mCallingPackageName:Ljava/lang/String;

    #@2c
    .line 6171
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@2f
    move-result v6

    #@30
    iput v6, v4, Landroid/media/AudioService$RemoteControlStackEntry;->mCallingUid:I

    #@32
    .line 6172
    if-nez p2, :cond_45

    #@34
    .line 6174
    invoke-virtual {v4}, Landroid/media/AudioService$RemoteControlStackEntry;->resetPlaybackInfo()V

    #@37
    .line 6206
    .end local v4           #rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    :cond_37
    :goto_37
    invoke-direct {p0, p1}, Landroid/media/AudioService;->isCurrentRcController(Landroid/app/PendingIntent;)Z

    #@3a
    move-result v6

    #@3b
    if-eqz v6, :cond_42

    #@3d
    .line 6207
    const/16 v6, 0xf

    #@3f
    invoke-direct {p0, v6}, Landroid/media/AudioService;->checkUpdateRemoteControlDisplay_syncAfRcs(I)V

    #@42
    .line 6209
    :cond_42
    monitor-exit v8
    :try_end_43
    .catchall {:try_start_7 .. :try_end_43} :catchall_66

    #@43
    .line 6210
    :try_start_43
    monitor-exit v7
    :try_end_44
    .catchall {:try_start_43 .. :try_end_44} :catchall_69

    #@44
    .line 6211
    return v2

    #@45
    .line 6177
    .restart local v4       #rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    :cond_45
    :try_start_45
    iget v2, v4, Landroid/media/AudioService$RemoteControlStackEntry;->mRccId:I

    #@47
    .line 6181
    iget-object v6, p0, Landroid/media/AudioService;->mRcDisplay:Landroid/media/IRemoteControlDisplay;
    :try_end_49
    .catchall {:try_start_45 .. :try_end_49} :catchall_66

    #@49
    if-eqz v6, :cond_52

    #@4b
    .line 6183
    :try_start_4b
    iget-object v6, v4, Landroid/media/AudioService$RemoteControlStackEntry;->mRcClient:Landroid/media/IRemoteControlClient;

    #@4d
    iget-object v9, p0, Landroid/media/AudioService;->mRcDisplay:Landroid/media/IRemoteControlDisplay;

    #@4f
    invoke-interface {v6, v9}, Landroid/media/IRemoteControlClient;->plugRemoteControlDisplay(Landroid/media/IRemoteControlDisplay;)V
    :try_end_52
    .catchall {:try_start_4b .. :try_end_52} :catchall_66
    .catch Landroid/os/RemoteException; {:try_start_4b .. :try_end_52} :catch_6c

    #@52
    .line 6190
    :cond_52
    :goto_52
    :try_start_52
    iget-object v6, v4, Landroid/media/AudioService$RemoteControlStackEntry;->mRcClient:Landroid/media/IRemoteControlClient;

    #@54
    invoke-interface {v6}, Landroid/media/IRemoteControlClient;->asBinder()Landroid/os/IBinder;

    #@57
    move-result-object v0

    #@58
    .line 6191
    .local v0, b:Landroid/os/IBinder;
    new-instance v3, Landroid/media/AudioService$RcClientDeathHandler;

    #@5a
    iget-object v6, v4, Landroid/media/AudioService$RemoteControlStackEntry;->mMediaIntent:Landroid/app/PendingIntent;

    #@5c
    invoke-direct {v3, p0, v0, v6}, Landroid/media/AudioService$RcClientDeathHandler;-><init>(Landroid/media/AudioService;Landroid/os/IBinder;Landroid/app/PendingIntent;)V
    :try_end_5f
    .catchall {:try_start_52 .. :try_end_5f} :catchall_66

    #@5f
    .line 6194
    .local v3, rcdh:Landroid/media/AudioService$RcClientDeathHandler;
    const/4 v6, 0x0

    #@60
    :try_start_60
    invoke-interface {v0, v3, v6}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_63
    .catchall {:try_start_60 .. :try_end_63} :catchall_66
    .catch Landroid/os/RemoteException; {:try_start_60 .. :try_end_63} :catch_89

    #@63
    .line 6200
    :goto_63
    :try_start_63
    iput-object v3, v4, Landroid/media/AudioService$RemoteControlStackEntry;->mRcClientDeathHandler:Landroid/media/AudioService$RcClientDeathHandler;

    #@65
    goto :goto_37

    #@66
    .line 6209
    .end local v0           #b:Landroid/os/IBinder;
    .end local v3           #rcdh:Landroid/media/AudioService$RcClientDeathHandler;
    .end local v4           #rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    .end local v5           #stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :catchall_66
    move-exception v6

    #@67
    monitor-exit v8
    :try_end_68
    .catchall {:try_start_63 .. :try_end_68} :catchall_66

    #@68
    :try_start_68
    throw v6

    #@69
    .line 6210
    :catchall_69
    move-exception v6

    #@6a
    monitor-exit v7
    :try_end_6b
    .catchall {:try_start_68 .. :try_end_6b} :catchall_69

    #@6b
    throw v6

    #@6c
    .line 6184
    .restart local v4       #rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    .restart local v5       #stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :catch_6c
    move-exception v1

    #@6d
    .line 6185
    .local v1, e:Landroid/os/RemoteException;
    :try_start_6d
    const-string v6, "AudioService"

    #@6f
    new-instance v9, Ljava/lang/StringBuilder;

    #@71
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@74
    const-string v10, "Error connecting remote control display to client: "

    #@76
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v9

    #@7a
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v9

    #@7e
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81
    move-result-object v9

    #@82
    invoke-static {v6, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@85
    .line 6186
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@88
    goto :goto_52

    #@89
    .line 6195
    .end local v1           #e:Landroid/os/RemoteException;
    .restart local v0       #b:Landroid/os/IBinder;
    .restart local v3       #rcdh:Landroid/media/AudioService$RcClientDeathHandler;
    :catch_89
    move-exception v1

    #@8a
    .line 6197
    .restart local v1       #e:Landroid/os/RemoteException;
    const-string v6, "AudioService"

    #@8c
    new-instance v9, Ljava/lang/StringBuilder;

    #@8e
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@91
    const-string/jumbo v10, "registerRemoteControlClient() has a dead client "

    #@94
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v9

    #@98
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v9

    #@9c
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9f
    move-result-object v9

    #@a0
    invoke-static {v6, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@a3
    .line 6198
    const/4 v6, 0x0

    #@a4
    iput-object v6, v4, Landroid/media/AudioService$RemoteControlStackEntry;->mRcClient:Landroid/media/IRemoteControlClient;
    :try_end_a6
    .catchall {:try_start_6d .. :try_end_a6} :catchall_66

    #@a6
    goto :goto_63
.end method

.method public registerRemoteControlDisplay(Landroid/media/IRemoteControlDisplay;)V
    .registers 10
    .parameter "rcd"

    #@0
    .prologue
    .line 6311
    sget-object v4, Landroid/media/AudioService;->mAudioFocusLock:Ljava/lang/Object;

    #@2
    monitor-enter v4

    #@3
    .line 6312
    :try_start_3
    iget-object v5, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@5
    monitor-enter v5
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_55

    #@6
    .line 6313
    :try_start_6
    iget-object v3, p0, Landroid/media/AudioService;->mRcDisplay:Landroid/media/IRemoteControlDisplay;

    #@8
    if-eq v3, p1, :cond_c

    #@a
    if-nez p1, :cond_f

    #@c
    .line 6314
    :cond_c
    monitor-exit v5
    :try_end_d
    .catchall {:try_start_6 .. :try_end_d} :catchall_52

    #@d
    :try_start_d
    monitor-exit v4
    :try_end_e
    .catchall {:try_start_d .. :try_end_e} :catchall_55

    #@e
    .line 6342
    :goto_e
    return-void

    #@f
    .line 6317
    :cond_f
    :try_start_f
    invoke-direct {p0}, Landroid/media/AudioService;->rcDisplay_stopDeathMonitor_syncRcStack()V

    #@12
    .line 6318
    iput-object p1, p0, Landroid/media/AudioService;->mRcDisplay:Landroid/media/IRemoteControlDisplay;

    #@14
    .line 6320
    invoke-direct {p0}, Landroid/media/AudioService;->rcDisplay_startDeathMonitor_syncRcStack()V

    #@17
    .line 6325
    iget-object v3, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@19
    invoke-virtual {v3}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    #@1c
    move-result-object v2

    #@1d
    .line 6326
    .local v2, stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :cond_1d
    :goto_1d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@20
    move-result v3

    #@21
    if-eqz v3, :cond_58

    #@23
    .line 6327
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@26
    move-result-object v1

    #@27
    check-cast v1, Landroid/media/AudioService$RemoteControlStackEntry;

    #@29
    .line 6328
    .local v1, rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    iget-object v3, v1, Landroid/media/AudioService$RemoteControlStackEntry;->mRcClient:Landroid/media/IRemoteControlClient;
    :try_end_2b
    .catchall {:try_start_f .. :try_end_2b} :catchall_52

    #@2b
    if-eqz v3, :cond_1d

    #@2d
    .line 6330
    :try_start_2d
    iget-object v3, v1, Landroid/media/AudioService$RemoteControlStackEntry;->mRcClient:Landroid/media/IRemoteControlClient;

    #@2f
    iget-object v6, p0, Landroid/media/AudioService;->mRcDisplay:Landroid/media/IRemoteControlDisplay;

    #@31
    invoke-interface {v3, v6}, Landroid/media/IRemoteControlClient;->plugRemoteControlDisplay(Landroid/media/IRemoteControlDisplay;)V
    :try_end_34
    .catchall {:try_start_2d .. :try_end_34} :catchall_52
    .catch Landroid/os/RemoteException; {:try_start_2d .. :try_end_34} :catch_35

    #@34
    goto :goto_1d

    #@35
    .line 6331
    :catch_35
    move-exception v0

    #@36
    .line 6332
    .local v0, e:Landroid/os/RemoteException;
    :try_start_36
    const-string v3, "AudioService"

    #@38
    new-instance v6, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v7, "Error connecting remote control display to client: "

    #@3f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v6

    #@43
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v6

    #@47
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v6

    #@4b
    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 6333
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@51
    goto :goto_1d

    #@52
    .line 6340
    .end local v0           #e:Landroid/os/RemoteException;
    .end local v1           #rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    .end local v2           #stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :catchall_52
    move-exception v3

    #@53
    monitor-exit v5
    :try_end_54
    .catchall {:try_start_36 .. :try_end_54} :catchall_52

    #@54
    :try_start_54
    throw v3

    #@55
    .line 6341
    :catchall_55
    move-exception v3

    #@56
    monitor-exit v4
    :try_end_57
    .catchall {:try_start_54 .. :try_end_57} :catchall_55

    #@57
    throw v3

    #@58
    .line 6339
    .restart local v2       #stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :cond_58
    const/16 v3, 0xf

    #@5a
    :try_start_5a
    invoke-direct {p0, v3}, Landroid/media/AudioService;->checkUpdateRemoteControlDisplay_syncAfRcs(I)V

    #@5d
    .line 6340
    monitor-exit v5
    :try_end_5e
    .catchall {:try_start_5a .. :try_end_5e} :catchall_52

    #@5e
    .line 6341
    :try_start_5e
    monitor-exit v4
    :try_end_5f
    .catchall {:try_start_5e .. :try_end_5f} :catchall_55

    #@5f
    goto :goto_e
.end method

.method public registerRemoteVolumeObserverForRcc(ILandroid/media/IRemoteVolumeObserver;)V
    .registers 10
    .parameter "rccId"
    .parameter "rvo"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 6455
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@3
    const/16 v1, 0x13

    #@5
    const/4 v2, 0x2

    #@6
    move v3, p1

    #@7
    move-object v5, p2

    #@8
    move v6, v4

    #@9
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@c
    .line 6457
    return-void
.end method

.method public reloadAudioSettings()V
    .registers 2

    #@0
    .prologue
    .line 2360
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Landroid/media/AudioService;->readAudioSettings(Z)V

    #@4
    .line 2361
    return-void
.end method

.method public remoteControlDisplayUsesBitmapSize(Landroid/media/IRemoteControlDisplay;II)V
    .registers 6
    .parameter "rcd"
    .parameter "w"
    .parameter "h"

    #@0
    .prologue
    .line 6379
    iget-object v1, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@2
    monitor-enter v1

    #@3
    .line 6381
    :try_start_3
    iput p2, p0, Landroid/media/AudioService;->mArtworkExpectedWidth:I

    #@5
    .line 6382
    iput p3, p0, Landroid/media/AudioService;->mArtworkExpectedHeight:I

    #@7
    .line 6383
    monitor-exit v1

    #@8
    .line 6384
    return-void

    #@9
    .line 6383
    :catchall_9
    move-exception v0

    #@a
    monitor-exit v1
    :try_end_b
    .catchall {:try_start_3 .. :try_end_b} :catchall_9

    #@b
    throw v0
.end method

.method public removeMediaButtonEventReceiverForCalls(Landroid/content/ComponentName;)V
    .registers 4
    .parameter "c"

    #@0
    .prologue
    .line 7000
    iget-object v0, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.MODIFY_PHONE_STATE"

    #@4
    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingPermission(Ljava/lang/String;)I

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_12

    #@a
    .line 7002
    const-string v0, "AudioService"

    #@c
    const-string v1, "Invalid permissions to remove media button receiver for Calls list"

    #@e
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 7008
    :goto_11
    return-void

    #@12
    .line 7005
    :cond_12
    iget-object v1, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@14
    monitor-enter v1

    #@15
    .line 7006
    :try_start_15
    iget-object v0, p0, Landroid/media/AudioService;->mMediaReceiverForCallsList:Ljava/util/ArrayList;

    #@17
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@1a
    .line 7007
    monitor-exit v1

    #@1b
    goto :goto_11

    #@1c
    :catchall_1c
    move-exception v0

    #@1d
    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_15 .. :try_end_1e} :catchall_1c

    #@1e
    throw v0
.end method

.method public requestAudioFocus(IILandroid/os/IBinder;Landroid/media/IAudioFocusDispatcher;Ljava/lang/String;Ljava/lang/String;)I
    .registers 21
    .parameter "mainStreamType"
    .parameter "focusChangeHint"
    .parameter "cb"
    .parameter "fd"
    .parameter "clientId"
    .parameter "callingPackageName"

    #@0
    .prologue
    .line 5087
    const-string v1, "AudioService"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, " AudioFocus  requestAudioFocus() from "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    move-object/from16 v0, p5

    #@f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    const-string v3, " callingPackageName = "

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    move-object/from16 v0, p6

    #@1b
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 5092
    invoke-interface/range {p3 .. p3}, Landroid/os/IBinder;->pingBinder()Z

    #@29
    move-result v1

    #@2a
    if-nez v1, :cond_35

    #@2c
    .line 5093
    const-string v1, "AudioService"

    #@2e
    const-string v2, " AudioFocus DOA client for requestAudioFocus(), aborting."

    #@30
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 5094
    const/4 v1, 0x0

    #@34
    .line 5168
    :goto_34
    return v1

    #@35
    .line 5098
    :cond_35
    invoke-virtual {p0}, Landroid/media/AudioService;->getRecordHookingState()I

    #@38
    move-result v1

    #@39
    const/4 v2, 0x3

    #@3a
    if-ne v1, v2, :cond_5a

    #@3c
    iget-object v1, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@3e
    invoke-virtual {v1}, Ljava/util/Stack;->empty()Z

    #@41
    move-result v1

    #@42
    if-nez v1, :cond_5a

    #@44
    .line 5099
    const-string v1, "AudioService"

    #@46
    const-string/jumbo v2, "requestAudioFocus() abandon google voice recognition first for guarantee exclusive action"

    #@49
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 5100
    const/4 v2, 0x0

    #@4d
    iget-object v1, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@4f
    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    #@52
    move-result-object v1

    #@53
    check-cast v1, Landroid/media/AudioService$FocusStackEntry;

    #@55
    iget-object v1, v1, Landroid/media/AudioService$FocusStackEntry;->mClientId:Ljava/lang/String;

    #@57
    invoke-virtual {p0, v2, v1}, Landroid/media/AudioService;->abandonAudioFocus(Landroid/media/IAudioFocusDispatcher;Ljava/lang/String;)I

    #@5a
    .line 5104
    :cond_5a
    sget-object v12, Landroid/media/AudioService;->mAudioFocusLock:Ljava/lang/Object;

    #@5c
    monitor-enter v12

    #@5d
    .line 5106
    :try_start_5d
    iget-object v1, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@5f
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@62
    move-result-object v1

    #@63
    const v2, 0x2060020

    #@66
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@69
    move-result v1

    #@6a
    if-nez v1, :cond_78

    #@6c
    invoke-direct {p0}, Landroid/media/AudioService;->canReassignAudioFocus()Z

    #@6f
    move-result v1

    #@70
    if-nez v1, :cond_78

    #@72
    .line 5108
    const/4 v1, 0x0

    #@73
    monitor-exit v12

    #@74
    goto :goto_34

    #@75
    .line 5166
    :catchall_75
    move-exception v1

    #@76
    monitor-exit v12
    :try_end_77
    .catchall {:try_start_5d .. :try_end_77} :catchall_75

    #@77
    throw v1

    #@78
    .line 5112
    :cond_78
    :try_start_78
    move-object/from16 v0, p5

    #@7a
    invoke-direct {p0, p1, v0}, Landroid/media/AudioService;->canReassignAudioFocusFromQchat(ILjava/lang/String;)Z

    #@7d
    move-result v1

    #@7e
    if-nez v1, :cond_83

    #@80
    .line 5113
    const/4 v1, 0x0

    #@81
    monitor-exit v12

    #@82
    goto :goto_34

    #@83
    .line 5119
    :cond_83
    new-instance v7, Landroid/media/AudioService$AudioFocusDeathHandler;

    #@85
    move-object/from16 v0, p3

    #@87
    invoke-direct {v7, p0, v0}, Landroid/media/AudioService$AudioFocusDeathHandler;-><init>(Landroid/media/AudioService;Landroid/os/IBinder;)V
    :try_end_8a
    .catchall {:try_start_78 .. :try_end_8a} :catchall_75

    #@8a
    .line 5121
    .local v7, afdh:Landroid/media/AudioService$AudioFocusDeathHandler;
    const/4 v1, 0x0

    #@8b
    :try_start_8b
    move-object/from16 v0, p3

    #@8d
    invoke-interface {v0, v7, v1}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_90
    .catchall {:try_start_8b .. :try_end_90} :catchall_75
    .catch Landroid/os/RemoteException; {:try_start_8b .. :try_end_90} :catch_c2

    #@90
    .line 5128
    :try_start_90
    iget-object v1, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@92
    invoke-virtual {v1}, Ljava/util/Stack;->empty()Z

    #@95
    move-result v1

    #@96
    if-nez v1, :cond_f2

    #@98
    iget-object v1, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@9a
    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    #@9d
    move-result-object v1

    #@9e
    check-cast v1, Landroid/media/AudioService$FocusStackEntry;

    #@a0
    iget-object v1, v1, Landroid/media/AudioService$FocusStackEntry;->mClientId:Ljava/lang/String;

    #@a2
    move-object/from16 v0, p5

    #@a4
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a7
    move-result v1

    #@a8
    if-eqz v1, :cond_f2

    #@aa
    .line 5131
    iget-object v1, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@ac
    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    #@af
    move-result-object v1

    #@b0
    check-cast v1, Landroid/media/AudioService$FocusStackEntry;

    #@b2
    iget v1, v1, Landroid/media/AudioService$FocusStackEntry;->mFocusChangeType:I

    #@b4
    move/from16 v0, p2

    #@b6
    if-ne v1, v0, :cond_e7

    #@b8
    .line 5134
    const/4 v1, 0x0

    #@b9
    move-object/from16 v0, p3

    #@bb
    invoke-interface {v0, v7, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    #@be
    .line 5135
    const/4 v1, 0x1

    #@bf
    monitor-exit v12

    #@c0
    goto/16 :goto_34

    #@c2
    .line 5122
    :catch_c2
    move-exception v10

    #@c3
    .line 5124
    .local v10, e:Landroid/os/RemoteException;
    const-string v1, "AudioService"

    #@c5
    new-instance v2, Ljava/lang/StringBuilder;

    #@c7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@ca
    const-string v3, "AudioFocus  requestAudioFocus() could not link to "

    #@cc
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v2

    #@d0
    move-object/from16 v0, p3

    #@d2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v2

    #@d6
    const-string v3, " binder death"

    #@d8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@db
    move-result-object v2

    #@dc
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@df
    move-result-object v2

    #@e0
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e3
    .line 5125
    const/4 v1, 0x0

    #@e4
    monitor-exit v12

    #@e5
    goto/16 :goto_34

    #@e7
    .line 5139
    .end local v10           #e:Landroid/os/RemoteException;
    :cond_e7
    iget-object v1, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@e9
    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    #@ec
    move-result-object v11

    #@ed
    check-cast v11, Landroid/media/AudioService$FocusStackEntry;

    #@ef
    .line 5140
    .local v11, fse:Landroid/media/AudioService$FocusStackEntry;
    invoke-virtual {v11}, Landroid/media/AudioService$FocusStackEntry;->unlinkToDeath()V

    #@f2
    .line 5144
    .end local v11           #fse:Landroid/media/AudioService$FocusStackEntry;
    :cond_f2
    iget-object v1, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@f4
    invoke-virtual {v1}, Ljava/util/Stack;->empty()Z

    #@f7
    move-result v1

    #@f8
    if-nez v1, :cond_11f

    #@fa
    iget-object v1, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@fc
    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    #@ff
    move-result-object v1

    #@100
    check-cast v1, Landroid/media/AudioService$FocusStackEntry;

    #@102
    iget-object v1, v1, Landroid/media/AudioService$FocusStackEntry;->mFocusDispatcher:Landroid/media/IAudioFocusDispatcher;
    :try_end_104
    .catchall {:try_start_90 .. :try_end_104} :catchall_75

    #@104
    if-eqz v1, :cond_11f

    #@106
    .line 5146
    :try_start_106
    iget-object v1, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@108
    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    #@10b
    move-result-object v1

    #@10c
    check-cast v1, Landroid/media/AudioService$FocusStackEntry;

    #@10e
    iget-object v2, v1, Landroid/media/AudioService$FocusStackEntry;->mFocusDispatcher:Landroid/media/IAudioFocusDispatcher;

    #@110
    mul-int/lit8 v3, p2, -0x1

    #@112
    iget-object v1, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@114
    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    #@117
    move-result-object v1

    #@118
    check-cast v1, Landroid/media/AudioService$FocusStackEntry;

    #@11a
    iget-object v1, v1, Landroid/media/AudioService$FocusStackEntry;->mClientId:Ljava/lang/String;

    #@11c
    invoke-interface {v2, v3, v1}, Landroid/media/IAudioFocusDispatcher;->dispatchAudioFocusChange(ILjava/lang/String;)V
    :try_end_11f
    .catchall {:try_start_106 .. :try_end_11f} :catchall_75
    .catch Landroid/os/RemoteException; {:try_start_106 .. :try_end_11f} :catch_14b

    #@11f
    .line 5156
    :cond_11f
    :goto_11f
    const/4 v1, 0x0

    #@120
    :try_start_120
    move-object/from16 v0, p5

    #@122
    invoke-direct {p0, v0, v1}, Landroid/media/AudioService;->removeFocusStackEntry(Ljava/lang/String;Z)V

    #@125
    .line 5159
    iget-object v13, p0, Landroid/media/AudioService;->mFocusStack:Ljava/util/Stack;

    #@127
    new-instance v1, Landroid/media/AudioService$FocusStackEntry;

    #@129
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@12c
    move-result v9

    #@12d
    move v2, p1

    #@12e
    move/from16 v3, p2

    #@130
    move-object/from16 v4, p4

    #@132
    move-object/from16 v5, p3

    #@134
    move-object/from16 v6, p5

    #@136
    move-object/from16 v8, p6

    #@138
    invoke-direct/range {v1 .. v9}, Landroid/media/AudioService$FocusStackEntry;-><init>(IILandroid/media/IAudioFocusDispatcher;Landroid/os/IBinder;Ljava/lang/String;Landroid/media/AudioService$AudioFocusDeathHandler;Ljava/lang/String;I)V

    #@13b
    invoke-virtual {v13, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    #@13e
    .line 5163
    iget-object v2, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@140
    monitor-enter v2
    :try_end_141
    .catchall {:try_start_120 .. :try_end_141} :catchall_75

    #@141
    .line 5164
    const/16 v1, 0xf

    #@143
    :try_start_143
    invoke-direct {p0, v1}, Landroid/media/AudioService;->checkUpdateRemoteControlDisplay_syncAfRcs(I)V

    #@146
    .line 5165
    monitor-exit v2
    :try_end_147
    .catchall {:try_start_143 .. :try_end_147} :catchall_168

    #@147
    .line 5166
    :try_start_147
    monitor-exit v12

    #@148
    .line 5168
    const/4 v1, 0x1

    #@149
    goto/16 :goto_34

    #@14b
    .line 5149
    :catch_14b
    move-exception v10

    #@14c
    .line 5150
    .restart local v10       #e:Landroid/os/RemoteException;
    const-string v1, "AudioService"

    #@14e
    new-instance v2, Ljava/lang/StringBuilder;

    #@150
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@153
    const-string v3, " Failure to signal loss of focus due to "

    #@155
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@158
    move-result-object v2

    #@159
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15c
    move-result-object v2

    #@15d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@160
    move-result-object v2

    #@161
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@164
    .line 5151
    invoke-virtual {v10}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_167
    .catchall {:try_start_147 .. :try_end_167} :catchall_75

    #@167
    goto :goto_11f

    #@168
    .line 5165
    .end local v10           #e:Landroid/os/RemoteException;
    :catchall_168
    move-exception v1

    #@169
    :try_start_169
    monitor-exit v2
    :try_end_16a
    .catchall {:try_start_169 .. :try_end_16a} :catchall_168

    #@16a
    :try_start_16a
    throw v1
    :try_end_16b
    .catchall {:try_start_16a .. :try_end_16b} :catchall_75
.end method

.method public sendBroadcastRecordState(I)V
    .registers 6
    .parameter "state"

    #@0
    .prologue
    .line 7235
    const-string v1, "AudioService"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v3, "sendBroadcastRecordState state = "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 7236
    new-instance v0, Landroid/content/Intent;

    #@1b
    const-string v1, "com.lge.media.AUDIORECORD_STATE_CHANGED_ACTION"

    #@1d
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@20
    .line 7237
    .local v0, intentState:Landroid/content/Intent;
    const-string v1, "com.lge.media.EXTRA_AUDIORECORD_STATE"

    #@22
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@25
    .line 7238
    invoke-direct {p0, v0}, Landroid/media/AudioService;->sendBroadcastToAll(Landroid/content/Intent;)V

    #@28
    .line 7239
    return-void
.end method

.method public setBluetoothA2dpDeviceConnectionState(Landroid/bluetooth/BluetoothDevice;I)I
    .registers 11
    .parameter "device"
    .parameter "state"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 3279
    iget-object v7, p0, Landroid/media/AudioService;->mConnectedDevices:Ljava/util/HashMap;

    #@3
    monitor-enter v7

    #@4
    .line 3280
    const/16 v1, 0x80

    #@6
    const/4 v2, 0x2

    #@7
    if-ne p2, v2, :cond_a

    #@9
    const/4 v0, 0x1

    #@a
    :cond_a
    :try_start_a
    invoke-direct {p0, v1, v0}, Landroid/media/AudioService;->checkSendBecomingNoisyIntent(II)I

    #@d
    move-result v6

    #@e
    .line 3282
    .local v6, delay:I
    iget-object v1, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@10
    const/16 v2, 0x16

    #@12
    const/4 v4, 0x0

    #@13
    move-object v0, p0

    #@14
    move v3, p2

    #@15
    move-object v5, p1

    #@16
    invoke-direct/range {v0 .. v6}, Landroid/media/AudioService;->queueMsgUnderWakeLock(Landroid/os/Handler;IIILjava/lang/Object;I)V

    #@19
    .line 3288
    monitor-exit v7

    #@1a
    .line 3289
    return v6

    #@1b
    .line 3288
    .end local v6           #delay:I
    :catchall_1b
    move-exception v0

    #@1c
    monitor-exit v7
    :try_end_1d
    .catchall {:try_start_a .. :try_end_1d} :catchall_1b

    #@1d
    throw v0
.end method

.method public setBluetoothA2dpOn(Z)V
    .registers 10
    .parameter "on"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 2470
    iget-object v7, p0, Landroid/media/AudioService;->mBluetoothA2dpEnabledLock:Ljava/lang/Object;

    #@3
    monitor-enter v7

    #@4
    .line 2471
    :try_start_4
    iput-boolean p1, p0, Landroid/media/AudioService;->mBluetoothA2dpEnabled:Z

    #@6
    .line 2472
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@8
    const/16 v1, 0x14

    #@a
    const/4 v2, 0x2

    #@b
    const/4 v3, 0x1

    #@c
    iget-boolean v5, p0, Landroid/media/AudioService;->mBluetoothA2dpEnabled:Z

    #@e
    if-eqz v5, :cond_17

    #@10
    :goto_10
    const/4 v5, 0x0

    #@11
    const/4 v6, 0x0

    #@12
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@15
    .line 2476
    monitor-exit v7

    #@16
    .line 2477
    return-void

    #@17
    .line 2472
    :cond_17
    const/16 v4, 0xa

    #@19
    goto :goto_10

    #@1a
    .line 2476
    :catchall_1a
    move-exception v0

    #@1b
    monitor-exit v7
    :try_end_1c
    .catchall {:try_start_4 .. :try_end_1c} :catchall_1a

    #@1c
    throw v0
.end method

.method public setBluetoothA2dpOnInt(Z)V
    .registers 5
    .parameter "on"

    #@0
    .prologue
    .line 6755
    iget-object v1, p0, Landroid/media/AudioService;->mBluetoothA2dpEnabledLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 6756
    :try_start_3
    iput-boolean p1, p0, Landroid/media/AudioService;->mBluetoothA2dpEnabled:Z

    #@5
    .line 6757
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@7
    const/16 v2, 0x14

    #@9
    invoke-virtual {v0, v2}, Landroid/media/AudioService$AudioHandler;->removeMessages(I)V

    #@c
    .line 6758
    const/4 v2, 0x1

    #@d
    iget-boolean v0, p0, Landroid/media/AudioService;->mBluetoothA2dpEnabled:Z

    #@f
    if-eqz v0, :cond_17

    #@11
    const/4 v0, 0x0

    #@12
    :goto_12
    invoke-static {v2, v0}, Landroid/media/AudioSystem;->setForceUse(II)I

    #@15
    .line 6760
    monitor-exit v1

    #@16
    .line 6761
    return-void

    #@17
    .line 6758
    :cond_17
    const/16 v0, 0xa

    #@19
    goto :goto_12

    #@1a
    .line 6760
    :catchall_1a
    move-exception v0

    #@1b
    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_1a

    #@1c
    throw v0
.end method

.method public setBluetoothScoOn(Z)V
    .registers 15
    .parameter "on"

    #@0
    .prologue
    const/16 v1, 0x9

    #@2
    const/4 v5, 0x0

    #@3
    const/4 v2, 0x2

    #@4
    const/4 v3, 0x0

    #@5
    .line 2427
    const-string/jumbo v0, "setBluetoothScoOn()"

    #@8
    invoke-virtual {p0, v0}, Landroid/media/AudioService;->checkAudioSettingsPermission(Ljava/lang/String;)Z

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_f

    #@e
    .line 2442
    :cond_e
    :goto_e
    return-void

    #@f
    .line 2430
    :cond_f
    if-eqz p1, :cond_3d

    #@11
    const/4 v0, 0x3

    #@12
    :goto_12
    iput v0, p0, Landroid/media/AudioService;->mForcedUseForComm:I

    #@14
    .line 2432
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@16
    iget v4, p0, Landroid/media/AudioService;->mForcedUseForComm:I

    #@18
    move v6, v3

    #@19
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@1c
    .line 2434
    iget-object v6, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@1e
    iget v10, p0, Landroid/media/AudioService;->mForcedUseForComm:I

    #@20
    move v7, v1

    #@21
    move v8, v2

    #@22
    move v9, v2

    #@23
    move-object v11, v5

    #@24
    move v12, v3

    #@25
    invoke-static/range {v6 .. v12}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@28
    .line 2438
    if-nez p1, :cond_e

    #@2a
    invoke-direct {p0}, Landroid/media/AudioService;->isSafeMediaVolumeDevices()Z

    #@2d
    move-result v0

    #@2e
    if-eqz v0, :cond_e

    #@30
    .line 2439
    iget-object v6, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@32
    const/16 v7, 0x1d

    #@34
    const/4 v9, 0x1

    #@35
    move v8, v2

    #@36
    move v10, v3

    #@37
    move-object v11, v5

    #@38
    move v12, v3

    #@39
    invoke-static/range {v6 .. v12}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@3c
    goto :goto_e

    #@3d
    :cond_3d
    move v0, v3

    #@3e
    .line 2430
    goto :goto_12
.end method

.method public setInCallMode(ILandroid/os/IBinder;)V
    .registers 6
    .parameter "state"
    .parameter "cb"

    #@0
    .prologue
    .line 1895
    const-string/jumbo v1, "setInCallMode()"

    #@3
    invoke-virtual {p0, v1}, Landroid/media/AudioService;->checkAudioSettingsPermission(Ljava/lang/String;)Z

    #@6
    move-result v1

    #@7
    if-nez v1, :cond_a

    #@9
    .line 1909
    :cond_9
    :goto_9
    return-void

    #@a
    .line 1899
    :cond_a
    const/4 v0, 0x0

    #@b
    .line 1900
    .local v0, newModeOwnerPid:I
    iget-object v2, p0, Landroid/media/AudioService;->mSetModeDeathHandlers:Ljava/util/ArrayList;

    #@d
    monitor-enter v2

    #@e
    .line 1901
    :try_start_e
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@11
    move-result v1

    #@12
    invoke-virtual {p0, p1, p2, v1}, Landroid/media/AudioService;->setInCallModeInt(ILandroid/os/IBinder;I)I

    #@15
    move-result v0

    #@16
    .line 1902
    monitor-exit v2
    :try_end_17
    .catchall {:try_start_e .. :try_end_17} :catchall_1d

    #@17
    .line 1906
    if-eqz v0, :cond_9

    #@19
    .line 1907
    invoke-direct {p0, v0}, Landroid/media/AudioService;->disconnectBluetoothSco(I)V

    #@1c
    goto :goto_9

    #@1d
    .line 1902
    :catchall_1d
    move-exception v1

    #@1e
    :try_start_1e
    monitor-exit v2
    :try_end_1f
    .catchall {:try_start_1e .. :try_end_1f} :catchall_1d

    #@1f
    throw v1
.end method

.method setInCallModeInt(ILandroid/os/IBinder;I)I
    .registers 22
    .parameter "state"
    .parameter "cb"
    .parameter "pid"

    #@0
    .prologue
    .line 2015
    const/4 v14, 0x0

    #@1
    .line 2017
    .local v14, newModeOwnerPid:I
    if-eqz p1, :cond_10

    #@3
    .line 2018
    const/4 v13, 0x2

    #@4
    .line 2022
    .local v13, mode:I
    :goto_4
    if-nez p2, :cond_12

    #@6
    .line 2023
    const-string v3, "AudioService"

    #@8
    const-string/jumbo v4, "setModeInt() called with null binder"

    #@b
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    move v15, v14

    #@f
    .line 2099
    .end local v14           #newModeOwnerPid:I
    .local v15, newModeOwnerPid:I
    :goto_f
    return v15

    #@10
    .line 2020
    .end local v13           #mode:I
    .end local v15           #newModeOwnerPid:I
    .restart local v14       #newModeOwnerPid:I
    :cond_10
    const/4 v13, 0x0

    #@11
    .restart local v13       #mode:I
    goto :goto_4

    #@12
    .line 2027
    :cond_12
    const/4 v11, 0x0

    #@13
    .line 2028
    .local v11, hdlr:Landroid/media/AudioService$SetModeDeathHandler;
    move-object/from16 v0, p0

    #@15
    iget-object v3, v0, Landroid/media/AudioService;->mSetModeDeathHandlers:Ljava/util/ArrayList;

    #@17
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@1a
    move-result-object v12

    #@1b
    .line 2029
    .local v12, iter:Ljava/util/Iterator;
    :cond_1b
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_3b

    #@21
    .line 2030
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@24
    move-result-object v10

    #@25
    check-cast v10, Landroid/media/AudioService$SetModeDeathHandler;

    #@27
    .line 2031
    .local v10, h:Landroid/media/AudioService$SetModeDeathHandler;
    invoke-virtual {v10}, Landroid/media/AudioService$SetModeDeathHandler;->getPid()I

    #@2a
    move-result v3

    #@2b
    move/from16 v0, p3

    #@2d
    if-ne v3, v0, :cond_1b

    #@2f
    .line 2032
    move-object v11, v10

    #@30
    .line 2034
    invoke-interface {v12}, Ljava/util/Iterator;->remove()V

    #@33
    .line 2035
    invoke-virtual {v11}, Landroid/media/AudioService$SetModeDeathHandler;->getBinder()Landroid/os/IBinder;

    #@36
    move-result-object v3

    #@37
    const/4 v4, 0x0

    #@38
    invoke-interface {v3, v11, v4}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    #@3b
    .line 2039
    .end local v10           #h:Landroid/media/AudioService$SetModeDeathHandler;
    :cond_3b
    const/16 v16, 0x0

    #@3d
    .line 2041
    .local v16, status:I
    :cond_3d
    if-nez v11, :cond_4a

    #@3f
    .line 2042
    new-instance v11, Landroid/media/AudioService$SetModeDeathHandler;

    #@41
    .end local v11           #hdlr:Landroid/media/AudioService$SetModeDeathHandler;
    move-object/from16 v0, p0

    #@43
    move-object/from16 v1, p2

    #@45
    move/from16 v2, p3

    #@47
    invoke-direct {v11, v0, v1, v2}, Landroid/media/AudioService$SetModeDeathHandler;-><init>(Landroid/media/AudioService;Landroid/os/IBinder;I)V

    #@4a
    .line 2046
    .restart local v11       #hdlr:Landroid/media/AudioService$SetModeDeathHandler;
    :cond_4a
    const/4 v3, 0x0

    #@4b
    :try_start_4b
    move-object/from16 v0, p2

    #@4d
    invoke-interface {v0, v11, v3}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_50
    .catch Landroid/os/RemoteException; {:try_start_4b .. :try_end_50} :catch_e7

    #@50
    .line 2053
    :goto_50
    move-object/from16 v0, p0

    #@52
    iget-object v3, v0, Landroid/media/AudioService;->mSetModeDeathHandlers:Ljava/util/ArrayList;

    #@54
    const/4 v4, 0x0

    #@55
    invoke-virtual {v3, v4, v11}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@58
    .line 2054
    move/from16 v0, p1

    #@5a
    invoke-virtual {v11, v0}, Landroid/media/AudioService$SetModeDeathHandler;->setInCallMode(I)V

    #@5d
    .line 2055
    move-object/from16 v0, p0

    #@5f
    iget v3, v0, Landroid/media/AudioService;->mState:I

    #@61
    move/from16 v0, p1

    #@63
    if-eq v0, v3, :cond_10b

    #@65
    .line 2056
    invoke-static/range {p1 .. p1}, Landroid/media/AudioSystem;->setInCallPhoneState(I)I

    #@68
    move-result v16

    #@69
    .line 2057
    move/from16 v0, p1

    #@6b
    move-object/from16 v1, p0

    #@6d
    iput v0, v1, Landroid/media/AudioService;->mState:I

    #@6f
    .line 2062
    :goto_6f
    move-object/from16 v0, p0

    #@71
    iget v3, v0, Landroid/media/AudioService;->mMode:I

    #@73
    if-eq v13, v3, :cond_121

    #@75
    .line 2063
    if-nez v16, :cond_10f

    #@77
    .line 2065
    move-object/from16 v0, p0

    #@79
    iget v3, v0, Landroid/media/AudioService;->mMode:I

    #@7b
    move-object/from16 v0, p0

    #@7d
    move-object/from16 v1, p2

    #@7f
    invoke-direct {v0, v3, v13, v1}, Landroid/media/AudioService;->handleFocusForCalls(IILandroid/os/IBinder;)V

    #@82
    .line 2066
    move-object/from16 v0, p0

    #@84
    iput v13, v0, Landroid/media/AudioService;->mMode:I

    #@86
    .line 2078
    :goto_86
    if-eqz v16, :cond_92

    #@88
    move-object/from16 v0, p0

    #@8a
    iget-object v3, v0, Landroid/media/AudioService;->mSetModeDeathHandlers:Ljava/util/ArrayList;

    #@8c
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    #@8f
    move-result v3

    #@90
    if-eqz v3, :cond_3d

    #@92
    .line 2080
    :cond_92
    if-nez v16, :cond_e4

    #@94
    .line 2081
    if-eqz v13, :cond_a8

    #@96
    .line 2082
    move-object/from16 v0, p0

    #@98
    iget-object v3, v0, Landroid/media/AudioService;->mSetModeDeathHandlers:Ljava/util/ArrayList;

    #@9a
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    #@9d
    move-result v3

    #@9e
    if-eqz v3, :cond_125

    #@a0
    .line 2083
    const-string v3, "AudioService"

    #@a2
    const-string/jumbo v4, "setMode() different from MODE_NORMAL with empty mode client stack"

    #@a5
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a8
    .line 2088
    :cond_a8
    :goto_a8
    const/high16 v3, -0x8000

    #@aa
    move-object/from16 v0, p0

    #@ac
    invoke-direct {v0, v3}, Landroid/media/AudioService;->getActiveStreamType(I)I

    #@af
    move-result v17

    #@b0
    .line 2089
    .local v17, streamType:I
    const/16 v3, -0xc8

    #@b2
    move/from16 v0, v17

    #@b4
    if-ne v0, v3, :cond_b8

    #@b6
    .line 2091
    const/16 v17, 0x3

    #@b8
    .line 2093
    :cond_b8
    move-object/from16 v0, p0

    #@ba
    move/from16 v1, v17

    #@bc
    invoke-direct {v0, v1}, Landroid/media/AudioService;->getDeviceForStream(I)I

    #@bf
    move-result v6

    #@c0
    .line 2094
    .local v6, device:I
    move-object/from16 v0, p0

    #@c2
    iget-object v3, v0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@c4
    move-object/from16 v0, p0

    #@c6
    iget-object v4, v0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@c8
    aget v4, v4, v17

    #@ca
    aget-object v3, v3, v4

    #@cc
    const/4 v4, 0x0

    #@cd
    invoke-virtual {v3, v6, v4}, Landroid/media/AudioService$VolumeStreamState;->getIndex(IZ)I

    #@d0
    move-result v5

    #@d1
    .line 2095
    .local v5, index:I
    move-object/from16 v0, p0

    #@d3
    iget-object v3, v0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@d5
    aget v4, v3, v17

    #@d7
    const/4 v7, 0x1

    #@d8
    const/4 v8, 0x0

    #@d9
    move-object/from16 v3, p0

    #@db
    invoke-direct/range {v3 .. v8}, Landroid/media/AudioService;->setStreamVolumeInt(IIIZZ)V

    #@de
    .line 2097
    const/4 v3, 0x1

    #@df
    move-object/from16 v0, p0

    #@e1
    invoke-direct {v0, v3}, Landroid/media/AudioService;->updateStreamVolumeAlias(Z)V

    #@e4
    .end local v5           #index:I
    .end local v6           #device:I
    .end local v17           #streamType:I
    :cond_e4
    move v15, v14

    #@e5
    .line 2099
    .end local v14           #newModeOwnerPid:I
    .restart local v15       #newModeOwnerPid:I
    goto/16 :goto_f

    #@e7
    .line 2047
    .end local v15           #newModeOwnerPid:I
    .restart local v14       #newModeOwnerPid:I
    :catch_e7
    move-exception v9

    #@e8
    .line 2049
    .local v9, e:Landroid/os/RemoteException;
    const-string v3, "AudioService"

    #@ea
    new-instance v4, Ljava/lang/StringBuilder;

    #@ec
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@ef
    const-string/jumbo v7, "setMode() could not link to "

    #@f2
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v4

    #@f6
    move-object/from16 v0, p2

    #@f8
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v4

    #@fc
    const-string v7, " binder death"

    #@fe
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@101
    move-result-object v4

    #@102
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@105
    move-result-object v4

    #@106
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@109
    goto/16 :goto_50

    #@10b
    .line 2059
    .end local v9           #e:Landroid/os/RemoteException;
    :cond_10b
    const/16 v16, 0x0

    #@10d
    goto/16 :goto_6f

    #@10f
    .line 2068
    :cond_10f
    if-eqz v11, :cond_11e

    #@111
    .line 2069
    move-object/from16 v0, p0

    #@113
    iget-object v3, v0, Landroid/media/AudioService;->mSetModeDeathHandlers:Ljava/util/ArrayList;

    #@115
    invoke-virtual {v3, v11}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@118
    .line 2070
    const/4 v3, 0x0

    #@119
    move-object/from16 v0, p2

    #@11b
    invoke-interface {v0, v11, v3}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    #@11e
    .line 2073
    :cond_11e
    const/4 v13, 0x0

    #@11f
    goto/16 :goto_86

    #@121
    .line 2076
    :cond_121
    const/16 v16, 0x0

    #@123
    goto/16 :goto_86

    #@125
    .line 2085
    :cond_125
    move-object/from16 v0, p0

    #@127
    iget-object v3, v0, Landroid/media/AudioService;->mSetModeDeathHandlers:Ljava/util/ArrayList;

    #@129
    const/4 v4, 0x0

    #@12a
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12d
    move-result-object v3

    #@12e
    check-cast v3, Landroid/media/AudioService$SetModeDeathHandler;

    #@130
    invoke-virtual {v3}, Landroid/media/AudioService$SetModeDeathHandler;->getPid()I

    #@133
    move-result v14

    #@134
    goto/16 :goto_a8
.end method

.method public setMasterMute(ZILandroid/os/IBinder;)V
    .registers 11
    .parameter "state"
    .parameter "flags"
    .parameter "cb"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1559
    invoke-static {}, Landroid/media/AudioSystem;->getMasterMute()Z

    #@4
    move-result v0

    #@5
    if-eq p1, v0, :cond_1b

    #@7
    .line 1560
    invoke-static {p1}, Landroid/media/AudioSystem;->setMasterMute(Z)I

    #@a
    .line 1562
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@c
    const/16 v1, 0xf

    #@e
    if-eqz p1, :cond_1c

    #@10
    const/4 v3, 0x1

    #@11
    :goto_11
    const/4 v5, 0x0

    #@12
    const/16 v6, 0x1f4

    #@14
    move v4, v2

    #@15
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@18
    .line 1564
    invoke-direct {p0, p1, p2}, Landroid/media/AudioService;->sendMasterMuteUpdate(ZI)V

    #@1b
    .line 1566
    :cond_1b
    return-void

    #@1c
    :cond_1c
    move v3, v2

    #@1d
    .line 1562
    goto :goto_11
.end method

.method public setMasterVolume(II)V
    .registers 5
    .parameter "volume"
    .parameter "flags"

    #@0
    .prologue
    .line 1598
    if-gez p1, :cond_b

    #@2
    .line 1599
    const/4 p1, 0x0

    #@3
    .line 1603
    :cond_3
    :goto_3
    int-to-float v0, p1

    #@4
    const/high16 v1, 0x42c8

    #@6
    div-float/2addr v0, v1

    #@7
    invoke-direct {p0, v0, p2}, Landroid/media/AudioService;->doSetMasterVolume(FI)V

    #@a
    .line 1604
    return-void

    #@b
    .line 1600
    :cond_b
    const/16 v0, 0x64

    #@d
    if-le p1, v0, :cond_3

    #@f
    .line 1601
    const/16 p1, 0x64

    #@11
    goto :goto_3
.end method

.method public setMode(ILandroid/os/IBinder;)V
    .registers 7
    .parameter "mode"
    .parameter "cb"

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    .line 1871
    const-string/jumbo v1, "setMode()"

    #@4
    invoke-virtual {p0, v1}, Landroid/media/AudioService;->checkAudioSettingsPermission(Ljava/lang/String;)Z

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_b

    #@a
    .line 1891
    :cond_a
    :goto_a
    return-void

    #@b
    .line 1875
    :cond_b
    if-lt p1, v3, :cond_a

    #@d
    const/4 v1, 0x4

    #@e
    if-ge p1, v1, :cond_a

    #@10
    .line 1879
    const/4 v0, 0x0

    #@11
    .line 1880
    .local v0, newModeOwnerPid:I
    iget-object v2, p0, Landroid/media/AudioService;->mSetModeDeathHandlers:Ljava/util/ArrayList;

    #@13
    monitor-enter v2

    #@14
    .line 1881
    if-ne p1, v3, :cond_18

    #@16
    .line 1882
    :try_start_16
    iget p1, p0, Landroid/media/AudioService;->mMode:I

    #@18
    .line 1884
    :cond_18
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@1b
    move-result v1

    #@1c
    invoke-virtual {p0, p1, p2, v1}, Landroid/media/AudioService;->setModeInt(ILandroid/os/IBinder;I)I

    #@1f
    move-result v0

    #@20
    .line 1885
    monitor-exit v2
    :try_end_21
    .catchall {:try_start_16 .. :try_end_21} :catchall_27

    #@21
    .line 1888
    if-eqz v0, :cond_a

    #@23
    .line 1889
    invoke-direct {p0, v0}, Landroid/media/AudioService;->disconnectBluetoothSco(I)V

    #@26
    goto :goto_a

    #@27
    .line 1885
    :catchall_27
    move-exception v1

    #@28
    :try_start_28
    monitor-exit v2
    :try_end_29
    .catchall {:try_start_28 .. :try_end_29} :catchall_27

    #@29
    throw v1
.end method

.method setModeInt(ILandroid/os/IBinder;I)I
    .registers 21
    .parameter "mode"
    .parameter "cb"
    .parameter "pid"

    #@0
    .prologue
    .line 1915
    const/4 v13, 0x0

    #@1
    .line 1916
    .local v13, newModeOwnerPid:I
    if-nez p2, :cond_d

    #@3
    .line 1917
    const-string v3, "AudioService"

    #@5
    const-string/jumbo v4, "setModeInt() called with null binder"

    #@8
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    move v14, v13

    #@c
    .line 2011
    .end local v13           #newModeOwnerPid:I
    .local v14, newModeOwnerPid:I
    :goto_c
    return v14

    #@d
    .line 1921
    .end local v14           #newModeOwnerPid:I
    .restart local v13       #newModeOwnerPid:I
    :cond_d
    const/4 v11, 0x0

    #@e
    .line 1922
    .local v11, hdlr:Landroid/media/AudioService$SetModeDeathHandler;
    move-object/from16 v0, p0

    #@10
    iget-object v3, v0, Landroid/media/AudioService;->mSetModeDeathHandlers:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@15
    move-result-object v12

    #@16
    .line 1923
    .local v12, iter:Ljava/util/Iterator;
    :cond_16
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    #@19
    move-result v3

    #@1a
    if-eqz v3, :cond_36

    #@1c
    .line 1924
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1f
    move-result-object v10

    #@20
    check-cast v10, Landroid/media/AudioService$SetModeDeathHandler;

    #@22
    .line 1925
    .local v10, h:Landroid/media/AudioService$SetModeDeathHandler;
    invoke-virtual {v10}, Landroid/media/AudioService$SetModeDeathHandler;->getPid()I

    #@25
    move-result v3

    #@26
    move/from16 v0, p3

    #@28
    if-ne v3, v0, :cond_16

    #@2a
    .line 1926
    move-object v11, v10

    #@2b
    .line 1928
    invoke-interface {v12}, Ljava/util/Iterator;->remove()V

    #@2e
    .line 1929
    invoke-virtual {v11}, Landroid/media/AudioService$SetModeDeathHandler;->getBinder()Landroid/os/IBinder;

    #@31
    move-result-object v3

    #@32
    const/4 v4, 0x0

    #@33
    invoke-interface {v3, v11, v4}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    #@36
    .line 1933
    .end local v10           #h:Landroid/media/AudioService$SetModeDeathHandler;
    :cond_36
    const/4 v15, 0x0

    #@37
    .line 1935
    .local v15, status:I
    :cond_37
    if-nez p1, :cond_f2

    #@39
    .line 1937
    move-object/from16 v0, p0

    #@3b
    iget-object v3, v0, Landroid/media/AudioService;->mSetModeDeathHandlers:Ljava/util/ArrayList;

    #@3d
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    #@40
    move-result v3

    #@41
    if-nez v3, :cond_56

    #@43
    .line 1938
    move-object/from16 v0, p0

    #@45
    iget-object v3, v0, Landroid/media/AudioService;->mSetModeDeathHandlers:Ljava/util/ArrayList;

    #@47
    const/4 v4, 0x0

    #@48
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@4b
    move-result-object v11

    #@4c
    .end local v11           #hdlr:Landroid/media/AudioService$SetModeDeathHandler;
    check-cast v11, Landroid/media/AudioService$SetModeDeathHandler;

    #@4e
    .line 1939
    .restart local v11       #hdlr:Landroid/media/AudioService$SetModeDeathHandler;
    invoke-virtual {v11}, Landroid/media/AudioService$SetModeDeathHandler;->getBinder()Landroid/os/IBinder;

    #@51
    move-result-object p2

    #@52
    .line 1940
    invoke-virtual {v11}, Landroid/media/AudioService$SetModeDeathHandler;->getMode()I

    #@55
    move-result p1

    #@56
    .line 1960
    :cond_56
    :goto_56
    move-object/from16 v0, p0

    #@58
    iget v3, v0, Landroid/media/AudioService;->mMode:I

    #@5a
    move/from16 v0, p1

    #@5c
    if-eq v0, v3, :cond_151

    #@5e
    .line 1962
    const/4 v3, 0x2

    #@5f
    move/from16 v0, p1

    #@61
    if-ne v0, v3, :cond_137

    #@63
    .line 1963
    const/4 v3, 0x1

    #@64
    move-object/from16 v0, p0

    #@66
    iput v3, v0, Landroid/media/AudioService;->mState:I

    #@68
    .line 1969
    :goto_68
    const/4 v3, 0x2

    #@69
    move/from16 v0, p1

    #@6b
    if-eq v0, v3, :cond_72

    #@6d
    const/4 v3, 0x1

    #@6e
    move/from16 v0, p1

    #@70
    if-ne v0, v3, :cond_78

    #@72
    .line 1971
    :cond_72
    const/4 v3, 0x1

    #@73
    move-object/from16 v0, p0

    #@75
    invoke-virtual {v0, v3}, Landroid/media/AudioService;->sendBroadcastRecordState(I)V

    #@78
    .line 1974
    :cond_78
    if-nez p1, :cond_86

    #@7a
    move-object/from16 v0, p0

    #@7c
    iget v3, v0, Landroid/media/AudioService;->mMode:I

    #@7e
    if-eqz v3, :cond_86

    #@80
    .line 1976
    const/4 v3, 0x0

    #@81
    move-object/from16 v0, p0

    #@83
    invoke-virtual {v0, v3}, Landroid/media/AudioService;->sendBroadcastRecordState(I)V

    #@86
    .line 1980
    :cond_86
    invoke-static/range {p1 .. p1}, Landroid/media/AudioSystem;->setPhoneState(I)I

    #@89
    move-result v15

    #@8a
    .line 1981
    if-nez v15, :cond_13e

    #@8c
    .line 1983
    move-object/from16 v0, p0

    #@8e
    iget v3, v0, Landroid/media/AudioService;->mMode:I

    #@90
    move-object/from16 v0, p0

    #@92
    move/from16 v1, p1

    #@94
    move-object/from16 v2, p2

    #@96
    invoke-direct {v0, v3, v1, v2}, Landroid/media/AudioService;->handleFocusForCalls(IILandroid/os/IBinder;)V

    #@99
    .line 1984
    move/from16 v0, p1

    #@9b
    move-object/from16 v1, p0

    #@9d
    iput v0, v1, Landroid/media/AudioService;->mMode:I

    #@9f
    .line 1996
    :goto_9f
    if-eqz v15, :cond_ab

    #@a1
    move-object/from16 v0, p0

    #@a3
    iget-object v3, v0, Landroid/media/AudioService;->mSetModeDeathHandlers:Ljava/util/ArrayList;

    #@a5
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    #@a8
    move-result v3

    #@a9
    if-eqz v3, :cond_37

    #@ab
    .line 1998
    :cond_ab
    if-nez v15, :cond_ef

    #@ad
    .line 1999
    if-eqz p1, :cond_c1

    #@af
    .line 2000
    move-object/from16 v0, p0

    #@b1
    iget-object v3, v0, Landroid/media/AudioService;->mSetModeDeathHandlers:Ljava/util/ArrayList;

    #@b3
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    #@b6
    move-result v3

    #@b7
    if-eqz v3, :cond_154

    #@b9
    .line 2001
    const-string v3, "AudioService"

    #@bb
    const-string/jumbo v4, "setMode() different from MODE_NORMAL with empty mode client stack"

    #@be
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c1
    .line 2006
    :cond_c1
    :goto_c1
    const/high16 v3, -0x8000

    #@c3
    move-object/from16 v0, p0

    #@c5
    invoke-direct {v0, v3}, Landroid/media/AudioService;->getActiveStreamType(I)I

    #@c8
    move-result v16

    #@c9
    .line 2007
    .local v16, streamType:I
    move-object/from16 v0, p0

    #@cb
    move/from16 v1, v16

    #@cd
    invoke-direct {v0, v1}, Landroid/media/AudioService;->getDeviceForStream(I)I

    #@d0
    move-result v6

    #@d1
    .line 2008
    .local v6, device:I
    move-object/from16 v0, p0

    #@d3
    iget-object v3, v0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@d5
    move-object/from16 v0, p0

    #@d7
    iget-object v4, v0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@d9
    aget v4, v4, v16

    #@db
    aget-object v3, v3, v4

    #@dd
    const/4 v4, 0x0

    #@de
    invoke-virtual {v3, v6, v4}, Landroid/media/AudioService$VolumeStreamState;->getIndex(IZ)I

    #@e1
    move-result v5

    #@e2
    .line 2009
    .local v5, index:I
    move-object/from16 v0, p0

    #@e4
    iget-object v3, v0, Landroid/media/AudioService;->STREAM_VOLUME_ALIAS:[I

    #@e6
    aget v4, v3, v16

    #@e8
    const/4 v7, 0x1

    #@e9
    const/4 v8, 0x0

    #@ea
    move-object/from16 v3, p0

    #@ec
    invoke-direct/range {v3 .. v8}, Landroid/media/AudioService;->setStreamVolumeInt(IIIZZ)V

    #@ef
    .end local v5           #index:I
    .end local v6           #device:I
    .end local v16           #streamType:I
    :cond_ef
    move v14, v13

    #@f0
    .line 2011
    .end local v13           #newModeOwnerPid:I
    .restart local v14       #newModeOwnerPid:I
    goto/16 :goto_c

    #@f2
    .line 1943
    .end local v14           #newModeOwnerPid:I
    .restart local v13       #newModeOwnerPid:I
    :cond_f2
    if-nez v11, :cond_ff

    #@f4
    .line 1944
    new-instance v11, Landroid/media/AudioService$SetModeDeathHandler;

    #@f6
    .end local v11           #hdlr:Landroid/media/AudioService$SetModeDeathHandler;
    move-object/from16 v0, p0

    #@f8
    move-object/from16 v1, p2

    #@fa
    move/from16 v2, p3

    #@fc
    invoke-direct {v11, v0, v1, v2}, Landroid/media/AudioService$SetModeDeathHandler;-><init>(Landroid/media/AudioService;Landroid/os/IBinder;I)V

    #@ff
    .line 1948
    .restart local v11       #hdlr:Landroid/media/AudioService$SetModeDeathHandler;
    :cond_ff
    const/4 v3, 0x0

    #@100
    :try_start_100
    move-object/from16 v0, p2

    #@102
    invoke-interface {v0, v11, v3}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_105
    .catch Landroid/os/RemoteException; {:try_start_100 .. :try_end_105} :catch_114

    #@105
    .line 1956
    :goto_105
    move-object/from16 v0, p0

    #@107
    iget-object v3, v0, Landroid/media/AudioService;->mSetModeDeathHandlers:Ljava/util/ArrayList;

    #@109
    const/4 v4, 0x0

    #@10a
    invoke-virtual {v3, v4, v11}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@10d
    .line 1957
    move/from16 v0, p1

    #@10f
    invoke-virtual {v11, v0}, Landroid/media/AudioService$SetModeDeathHandler;->setMode(I)V

    #@112
    goto/16 :goto_56

    #@114
    .line 1949
    :catch_114
    move-exception v9

    #@115
    .line 1951
    .local v9, e:Landroid/os/RemoteException;
    const-string v3, "AudioService"

    #@117
    new-instance v4, Ljava/lang/StringBuilder;

    #@119
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@11c
    const-string/jumbo v7, "setMode() could not link to "

    #@11f
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@122
    move-result-object v4

    #@123
    move-object/from16 v0, p2

    #@125
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@128
    move-result-object v4

    #@129
    const-string v7, " binder death"

    #@12b
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12e
    move-result-object v4

    #@12f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@132
    move-result-object v4

    #@133
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@136
    goto :goto_105

    #@137
    .line 1965
    .end local v9           #e:Landroid/os/RemoteException;
    :cond_137
    const/4 v3, 0x0

    #@138
    move-object/from16 v0, p0

    #@13a
    iput v3, v0, Landroid/media/AudioService;->mState:I

    #@13c
    goto/16 :goto_68

    #@13e
    .line 1986
    :cond_13e
    if-eqz v11, :cond_14d

    #@140
    .line 1987
    move-object/from16 v0, p0

    #@142
    iget-object v3, v0, Landroid/media/AudioService;->mSetModeDeathHandlers:Ljava/util/ArrayList;

    #@144
    invoke-virtual {v3, v11}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@147
    .line 1988
    const/4 v3, 0x0

    #@148
    move-object/from16 v0, p2

    #@14a
    invoke-interface {v0, v11, v3}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    #@14d
    .line 1991
    :cond_14d
    const/16 p1, 0x0

    #@14f
    goto/16 :goto_9f

    #@151
    .line 1994
    :cond_151
    const/4 v15, 0x0

    #@152
    goto/16 :goto_9f

    #@154
    .line 2003
    :cond_154
    move-object/from16 v0, p0

    #@156
    iget-object v3, v0, Landroid/media/AudioService;->mSetModeDeathHandlers:Ljava/util/ArrayList;

    #@158
    const/4 v4, 0x0

    #@159
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@15c
    move-result-object v3

    #@15d
    check-cast v3, Landroid/media/AudioService$SetModeDeathHandler;

    #@15f
    invoke-virtual {v3}, Landroid/media/AudioService$SetModeDeathHandler;->getPid()I

    #@162
    move-result v13

    #@163
    goto/16 :goto_c1
.end method

.method public setPlaybackInfoForRcc(III)V
    .registers 11
    .parameter "rccId"
    .parameter "what"
    .parameter "value"

    #@0
    .prologue
    .line 6387
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@2
    const/16 v1, 0x12

    #@4
    const/4 v2, 0x2

    #@5
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8
    move-result-object v5

    #@9
    const/4 v6, 0x0

    #@a
    move v3, p1

    #@b
    move v4, p2

    #@c
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@f
    .line 6389
    return-void
.end method

.method public setRecordHookingState(II)Z
    .registers 11
    .parameter "state"
    .parameter "flags"

    #@0
    .prologue
    const/16 v7, 0x1f40

    #@2
    const/4 v6, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    .line 7266
    const/4 v0, 0x1

    #@5
    .line 7267
    .local v0, bResult:Z
    const-string v3, "AudioService"

    #@7
    new-instance v4, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string/jumbo v5, "setRecordHookingState() old state("

    #@f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v4

    #@13
    iget v5, p0, Landroid/media/AudioService;->mRecordHookingState:I

    #@15
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    const-string v5, ") new state("

    #@1b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    const-string v5, ") type("

    #@25
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    const-string v5, ")"

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v4

    #@37
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 7268
    if-ltz p1, :cond_3f

    #@3c
    const/4 v3, 0x3

    #@3d
    if-le p1, v3, :cond_48

    #@3f
    .line 7269
    :cond_3f
    const-string v3, "AudioService"

    #@41
    const-string/jumbo v4, "setRecordHookingState() new state is not available"

    #@44
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 7286
    :goto_47
    return v2

    #@48
    .line 7272
    :cond_48
    iput p1, p0, Landroid/media/AudioService;->mRecordHookingState:I

    #@4a
    .line 7273
    iget v3, p0, Landroid/media/AudioService;->mRecordHookingState:I

    #@4c
    if-ne v3, v6, :cond_6a

    #@4e
    .line 7274
    iput p2, p0, Landroid/media/AudioService;->mRecordHookingType:I

    #@50
    .line 7275
    iget v2, p0, Landroid/media/AudioService;->mRecordHookingType:I

    #@52
    invoke-static {v6, v7, v2}, Landroid/media/AudioSystem;->setRecordHookingEnabled(III)Ljava/io/FileDescriptor;

    #@55
    move-result-object v1

    #@56
    .line 7276
    .local v1, fd:Ljava/io/FileDescriptor;
    if-eqz v1, :cond_60

    #@58
    .line 7277
    const/4 v2, 0x2

    #@59
    iget v3, p0, Landroid/media/AudioService;->mRecordHookingType:I

    #@5b
    invoke-virtual {p0, v2, v3}, Landroid/media/AudioService;->setRecordHookingState(II)Z

    #@5e
    .end local v1           #fd:Ljava/io/FileDescriptor;
    :cond_5e
    :goto_5e
    move v2, v0

    #@5f
    .line 7286
    goto :goto_47

    #@60
    .line 7279
    .restart local v1       #fd:Ljava/io/FileDescriptor;
    :cond_60
    const-string v2, "AudioService"

    #@62
    const-string/jumbo v3, "setRecordHookingState() setRecordHookingEnabled failed."

    #@65
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@68
    .line 7280
    const/4 v0, 0x0

    #@69
    goto :goto_5e

    #@6a
    .line 7282
    .end local v1           #fd:Ljava/io/FileDescriptor;
    :cond_6a
    iget v3, p0, Landroid/media/AudioService;->mRecordHookingState:I

    #@6c
    if-nez v3, :cond_5e

    #@6e
    .line 7283
    iput v2, p0, Landroid/media/AudioService;->mRecordHookingType:I

    #@70
    .line 7284
    iget v3, p0, Landroid/media/AudioService;->mRecordHookingType:I

    #@72
    invoke-static {v2, v7, v3}, Landroid/media/AudioSystem;->setRecordHookingEnabled(III)Ljava/io/FileDescriptor;

    #@75
    goto :goto_5e
.end method

.method public setRemoteStreamVolume(I)V
    .registers 10
    .parameter "vol"

    #@0
    .prologue
    .line 6595
    const-string v5, "AudioService"

    #@2
    new-instance v6, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v7, "setRemoteStreamVolume(vol="

    #@a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v6

    #@e
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v6

    #@12
    const-string v7, ")"

    #@14
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v6

    #@18
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v6

    #@1c
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 6596
    const/4 v1, -0x1

    #@20
    .line 6597
    .local v1, rccId:I
    iget-object v6, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@22
    monitor-enter v6

    #@23
    .line 6598
    :try_start_23
    iget-object v5, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@25
    iget v5, v5, Landroid/media/AudioService$RemotePlaybackState;->mRccId:I

    #@27
    const/4 v7, -0x1

    #@28
    if-ne v5, v7, :cond_2c

    #@2a
    .line 6599
    monitor-exit v6

    #@2b
    .line 6622
    :cond_2b
    :goto_2b
    return-void

    #@2c
    .line 6601
    :cond_2c
    iget-object v5, p0, Landroid/media/AudioService;->mMainRemote:Landroid/media/AudioService$RemotePlaybackState;

    #@2e
    iget v1, v5, Landroid/media/AudioService$RemotePlaybackState;->mRccId:I

    #@30
    .line 6602
    monitor-exit v6
    :try_end_31
    .catchall {:try_start_23 .. :try_end_31} :catchall_5e

    #@31
    .line 6603
    const/4 v3, 0x0

    #@32
    .line 6604
    .local v3, rvo:Landroid/media/IRemoteVolumeObserver;
    iget-object v6, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@34
    monitor-enter v6

    #@35
    .line 6605
    :try_start_35
    iget-object v5, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@37
    invoke-virtual {v5}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    #@3a
    move-result-object v4

    #@3b
    .line 6606
    .local v4, stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :cond_3b
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@3e
    move-result v5

    #@3f
    if-eqz v5, :cond_4d

    #@41
    .line 6607
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@44
    move-result-object v2

    #@45
    check-cast v2, Landroid/media/AudioService$RemoteControlStackEntry;

    #@47
    .line 6608
    .local v2, rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    iget v5, v2, Landroid/media/AudioService$RemoteControlStackEntry;->mRccId:I

    #@49
    if-ne v5, v1, :cond_3b

    #@4b
    .line 6610
    iget-object v3, v2, Landroid/media/AudioService$RemoteControlStackEntry;->mRemoteVolumeObs:Landroid/media/IRemoteVolumeObserver;

    #@4d
    .line 6614
    .end local v2           #rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    :cond_4d
    monitor-exit v6
    :try_end_4e
    .catchall {:try_start_35 .. :try_end_4e} :catchall_61

    #@4e
    .line 6615
    if-eqz v3, :cond_2b

    #@50
    .line 6617
    const/4 v5, 0x0

    #@51
    :try_start_51
    invoke-interface {v3, v5, p1}, Landroid/media/IRemoteVolumeObserver;->dispatchRemoteVolumeUpdate(II)V
    :try_end_54
    .catch Landroid/os/RemoteException; {:try_start_51 .. :try_end_54} :catch_55

    #@54
    goto :goto_2b

    #@55
    .line 6618
    :catch_55
    move-exception v0

    #@56
    .line 6619
    .local v0, e:Landroid/os/RemoteException;
    const-string v5, "AudioService"

    #@58
    const-string v6, "Error dispatching absolute volume update"

    #@5a
    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@5d
    goto :goto_2b

    #@5e
    .line 6602
    .end local v0           #e:Landroid/os/RemoteException;
    .end local v3           #rvo:Landroid/media/IRemoteVolumeObserver;
    .end local v4           #stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :catchall_5e
    move-exception v5

    #@5f
    :try_start_5f
    monitor-exit v6
    :try_end_60
    .catchall {:try_start_5f .. :try_end_60} :catchall_5e

    #@60
    throw v5

    #@61
    .line 6614
    .restart local v3       #rvo:Landroid/media/IRemoteVolumeObserver;
    :catchall_61
    move-exception v5

    #@62
    :try_start_62
    monitor-exit v6
    :try_end_63
    .catchall {:try_start_62 .. :try_end_63} :catchall_61

    #@63
    throw v5
.end method

.method public setRemoteSubmixOn(ZI)V
    .registers 10
    .parameter "on"
    .parameter "address"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2893
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@3
    const/16 v1, 0x17

    #@5
    if-eqz p1, :cond_f

    #@7
    const/4 v3, 0x1

    #@8
    :goto_8
    const/4 v5, 0x0

    #@9
    move v4, p2

    #@a
    move v6, v2

    #@b
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@e
    .line 2898
    return-void

    #@f
    :cond_f
    move v3, v2

    #@10
    .line 2893
    goto :goto_8
.end method

.method public setRingerMode(I)V
    .registers 4
    .parameter "ringerMode"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1677
    if-ne p1, v1, :cond_8

    #@3
    iget-boolean v0, p0, Landroid/media/AudioService;->mHasVibrator:Z

    #@5
    if-nez v0, :cond_8

    #@7
    .line 1678
    const/4 p1, 0x0

    #@8
    .line 1680
    :cond_8
    invoke-virtual {p0}, Landroid/media/AudioService;->getRingerMode()I

    #@b
    move-result v0

    #@c
    if-eq p1, v0, :cond_14

    #@e
    .line 1681
    invoke-direct {p0, p1, v1}, Landroid/media/AudioService;->setRingerModeInt(IZ)V

    #@11
    .line 1683
    invoke-direct {p0, p1}, Landroid/media/AudioService;->broadcastRingerMode(I)V

    #@14
    .line 1685
    :cond_14
    return-void
.end method

.method public setRingtonePlayer(Landroid/media/IRingtonePlayer;)V
    .registers 5
    .parameter "player"

    #@0
    .prologue
    .line 6765
    iget-object v0, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.REMOTE_AUDIO_PLAYBACK"

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 6766
    iput-object p1, p0, Landroid/media/AudioService;->mRingtonePlayer:Landroid/media/IRingtonePlayer;

    #@a
    .line 6767
    return-void
.end method

.method public setSpeakerOnForMedia(Z)V
    .registers 9
    .parameter "on"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 6965
    const-string/jumbo v0, "setSpeakerphoneOn()"

    #@5
    invoke-virtual {p0, v0}, Landroid/media/AudioService;->checkAudioSettingsPermission(Ljava/lang/String;)Z

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_c

    #@b
    .line 6972
    :goto_b
    return-void

    #@c
    .line 6969
    :cond_c
    if-eqz p1, :cond_1d

    #@e
    move v0, v3

    #@f
    :goto_f
    iput v0, p0, Landroid/media/AudioService;->mForcedUseForMedia:I

    #@11
    .line 6970
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@13
    const/16 v1, 0x9

    #@15
    const/4 v2, 0x2

    #@16
    iget v4, p0, Landroid/media/AudioService;->mForcedUseForMedia:I

    #@18
    const/4 v5, 0x0

    #@19
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@1c
    goto :goto_b

    #@1d
    :cond_1d
    move v0, v6

    #@1e
    .line 6969
    goto :goto_f
.end method

.method public setSpeakerphoneOn(Z)V
    .registers 15
    .parameter "on"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v2, 0x2

    #@2
    const/4 v9, 0x1

    #@3
    const/4 v3, 0x0

    #@4
    .line 2406
    const-string/jumbo v0, "setSpeakerphoneOn()"

    #@7
    invoke-virtual {p0, v0}, Landroid/media/AudioService;->checkAudioSettingsPermission(Ljava/lang/String;)Z

    #@a
    move-result v0

    #@b
    if-nez v0, :cond_e

    #@d
    .line 2418
    :cond_d
    :goto_d
    return-void

    #@e
    .line 2409
    :cond_e
    if-eqz p1, :cond_31

    #@10
    move v0, v9

    #@11
    :goto_11
    iput v0, p0, Landroid/media/AudioService;->mForcedUseForComm:I

    #@13
    .line 2411
    iget-object v0, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@15
    const/16 v1, 0x9

    #@17
    iget v4, p0, Landroid/media/AudioService;->mForcedUseForComm:I

    #@19
    move v6, v3

    #@1a
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@1d
    .line 2414
    if-nez p1, :cond_d

    #@1f
    invoke-direct {p0}, Landroid/media/AudioService;->isSafeMediaVolumeDevices()Z

    #@22
    move-result v0

    #@23
    if-eqz v0, :cond_d

    #@25
    .line 2415
    iget-object v6, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@27
    const/16 v7, 0x1d

    #@29
    move v8, v2

    #@2a
    move v10, v3

    #@2b
    move-object v11, v5

    #@2c
    move v12, v3

    #@2d
    invoke-static/range {v6 .. v12}, Landroid/media/AudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@30
    goto :goto_d

    #@31
    :cond_31
    move v0, v3

    #@32
    .line 2409
    goto :goto_11
.end method

.method public setStreamMute(IZLandroid/os/IBinder;)V
    .registers 5
    .parameter "streamType"
    .parameter "state"
    .parameter "cb"

    #@0
    .prologue
    .line 1543
    invoke-virtual {p0, p1}, Landroid/media/AudioService;->isStreamAffectedByMute(I)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 1544
    iget-object v0, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@8
    aget-object v0, v0, p1

    #@a
    invoke-virtual {v0, p3, p2}, Landroid/media/AudioService$VolumeStreamState;->mute(Landroid/os/IBinder;Z)V

    #@d
    .line 1546
    :cond_d
    return-void
.end method

.method public setStreamSolo(IZLandroid/os/IBinder;)V
    .registers 6
    .parameter "streamType"
    .parameter "state"
    .parameter "cb"

    #@0
    .prologue
    .line 1534
    const/4 v0, 0x0

    #@1
    .local v0, stream:I
    :goto_1
    iget-object v1, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@3
    array-length v1, v1

    #@4
    if-ge v0, v1, :cond_19

    #@6
    .line 1535
    invoke-virtual {p0, v0}, Landroid/media/AudioService;->isStreamAffectedByMute(I)Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_e

    #@c
    if-ne v0, p1, :cond_11

    #@e
    .line 1534
    :cond_e
    :goto_e
    add-int/lit8 v0, v0, 0x1

    #@10
    goto :goto_1

    #@11
    .line 1537
    :cond_11
    iget-object v1, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@13
    aget-object v1, v1, v0

    #@15
    invoke-virtual {v1, p3, p2}, Landroid/media/AudioService$VolumeStreamState;->mute(Landroid/os/IBinder;Z)V

    #@18
    goto :goto_e

    #@19
    .line 1539
    :cond_19
    return-void
.end method

.method public setStreamVolume(III)V
    .registers 16
    .parameter "streamType"
    .parameter "index"
    .parameter "flags"

    #@0
    .prologue
    .line 1190
    const-string v0, "AudioService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v2, "setStreamVolume() streamType = "

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    const-string v2, ", index = "

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    const-string v2, ", flags = "

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 1191
    invoke-direct {p0, p1}, Landroid/media/AudioService;->ensureValidStreamType(I)V

    #@30
    .line 1192
    iget-object v0, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@32
    iget-object v1, p0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@34
    aget v1, v1, p1

    #@36
    aget-object v11, v0, v1

    #@38
    .line 1195
    .local v11, streamState:Landroid/media/AudioService$VolumeStreamState;
    invoke-virtual {p0, p1}, Landroid/media/AudioService;->isVirtualMirrorLinkDevicedConnected(I)Z

    #@3b
    move-result v0

    #@3c
    if-eqz v0, :cond_46

    #@3e
    .line 1197
    const-string v0, "AudioService"

    #@40
    const-string v1, "Speaker volume is fixed!!!!!"

    #@42
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 1286
    :cond_45
    :goto_45
    return-void

    #@46
    .line 1204
    :cond_46
    const/4 v0, 0x2

    #@47
    if-ne p1, v0, :cond_73

    #@49
    and-int/lit16 v0, p3, 0x80

    #@4b
    if-nez v0, :cond_73

    #@4d
    .line 1205
    :try_start_4d
    iget-object v0, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@4f
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@52
    move-result-object v0

    #@53
    const-string/jumbo v1, "quiet_mode_status"

    #@56
    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    #@59
    move-result v9

    #@5a
    .line 1206
    .local v9, quietModeStatus:I
    const/4 v0, 0x1

    #@5b
    if-ne v9, v0, :cond_73

    #@5d
    iget-object v0, p0, Landroid/media/AudioService;->mIsQuietModeSupport:Ljava/lang/Boolean;

    #@5f
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    #@62
    move-result v0

    #@63
    if-eqz v0, :cond_73

    #@65
    .line 1207
    iget-object v0, p0, Landroid/media/AudioService;->mVolumePanel:Landroid/view/VolumePanel;

    #@67
    invoke-virtual {v0}, Landroid/view/VolumePanel;->postDisplayQuietModeWarning()V
    :try_end_6a
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_4d .. :try_end_6a} :catch_6b

    #@6a
    goto :goto_45

    #@6b
    .line 1211
    .end local v9           #quietModeStatus:I
    :catch_6b
    move-exception v6

    #@6c
    .line 1212
    .local v6, e:Landroid/provider/Settings$SettingNotFoundException;
    const-string v0, "AudioService"

    #@6e
    const-string v1, "SettingNotFoundException - getDBQuietModeState()"

    #@70
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@73
    .line 1216
    .end local v6           #e:Landroid/provider/Settings$SettingNotFoundException;
    :cond_73
    invoke-direct {p0, p1}, Landroid/media/AudioService;->getDeviceForStream(I)I

    #@76
    move-result v3

    #@77
    .line 1219
    .local v3, device:I
    and-int/lit8 p3, p3, -0x21

    #@79
    .line 1220
    iget-object v0, p0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@7b
    aget v0, v0, p1

    #@7d
    const/4 v1, 0x3

    #@7e
    if-ne v0, v1, :cond_93

    #@80
    and-int/lit8 v0, v3, 0x0

    #@82
    if-eqz v0, :cond_93

    #@84
    .line 1222
    or-int/lit8 p3, p3, 0x20

    #@86
    .line 1223
    iget-object v0, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@88
    aget-object v0, v0, p1

    #@8a
    invoke-virtual {v0}, Landroid/media/AudioService$VolumeStreamState;->getMaxIndex()I

    #@8d
    move-result p2

    #@8e
    .line 1224
    move v8, p2

    #@8f
    .line 1285
    .local v8, oldIndex:I
    :goto_8f
    invoke-direct {p0, p1, v8, p2, p3}, Landroid/media/AudioService;->sendVolumeUpdate(IIII)V

    #@92
    goto :goto_45

    #@93
    .line 1227
    .end local v8           #oldIndex:I
    :cond_93
    invoke-static {v11}, Landroid/media/AudioService$VolumeStreamState;->access$700(Landroid/media/AudioService$VolumeStreamState;)I

    #@96
    move-result v0

    #@97
    if-eqz v0, :cond_11d

    #@99
    const/4 v0, 0x1

    #@9a
    :goto_9a
    invoke-virtual {v11, v3, v0}, Landroid/media/AudioService$VolumeStreamState;->getIndex(IZ)I

    #@9d
    move-result v8

    #@9e
    .line 1230
    .restart local v8       #oldIndex:I
    mul-int/lit8 v0, p2, 0xa

    #@a0
    iget-object v1, p0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@a2
    aget v1, v1, p1

    #@a4
    invoke-direct {p0, v0, p1, v1}, Landroid/media/AudioService;->rescaleIndex(III)I

    #@a7
    move-result p2

    #@a8
    .line 1233
    iget-object v0, p0, Landroid/media/AudioService;->mSafeMediaVolumeState:Ljava/lang/Integer;

    #@aa
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@ad
    move-result v0

    #@ae
    const/4 v1, 0x3

    #@af
    if-ne v0, v1, :cond_cc

    #@b1
    iget-object v0, p0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@b3
    aget v0, v0, p1

    #@b5
    const/4 v1, 0x3

    #@b6
    if-ne v0, v1, :cond_cc

    #@b8
    iget v0, p0, Landroid/media/AudioService;->mSafeMediaVolumeIndex:I

    #@ba
    if-ge p2, v0, :cond_cc

    #@bc
    iget-object v0, p0, Landroid/media/AudioService;->mLgSafeMediaVolumeEnabled:Ljava/lang/Boolean;

    #@be
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    #@c1
    move-result v0

    #@c2
    if-eqz v0, :cond_cc

    #@c4
    .line 1236
    iget-object v0, p0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@c6
    aget v0, v0, p1

    #@c8
    const/4 v1, -0x1

    #@c9
    invoke-direct {p0, v0, p2, v3, v1}, Landroid/media/AudioService;->checkDisplaySafeMediaVolume(IIII)V

    #@cc
    .line 1239
    :cond_cc
    iget-object v0, p0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@ce
    aget v0, v0, p1

    #@d0
    invoke-direct {p0, v0, p2, v3}, Landroid/media/AudioService;->checkSafeMediaVolume(III)Z

    #@d3
    move-result v0

    #@d4
    if-eqz v0, :cond_45

    #@d6
    .line 1245
    and-int/lit8 v0, p3, 0x2

    #@d8
    if-nez v0, :cond_e4

    #@da
    iget-object v0, p0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@dc
    aget v0, v0, p1

    #@de
    invoke-virtual {p0}, Landroid/media/AudioService;->getMasterStreamType()I

    #@e1
    move-result v1

    #@e2
    if-ne v0, v1, :cond_fd

    #@e4
    .line 1248
    :cond_e4
    if-nez p2, :cond_137

    #@e6
    .line 1250
    iget-boolean v0, p0, Landroid/media/AudioService;->mIsSoundProfile:Z

    #@e8
    if-eqz v0, :cond_124

    #@ea
    .line 1251
    invoke-virtual {p0}, Landroid/media/AudioService;->getRingerMode()I

    #@ed
    move-result v10

    #@ee
    .line 1252
    .local v10, ringerMode:I
    const/4 v0, 0x2

    #@ef
    if-ne v0, v10, :cond_122

    #@f1
    .line 1253
    iget-boolean v0, p0, Landroid/media/AudioService;->mHasVibrator:Z

    #@f3
    if-eqz v0, :cond_120

    #@f5
    const/4 v7, 0x1

    #@f6
    .line 1274
    .end local v10           #ringerMode:I
    .local v7, newRingerMode:I
    :goto_f6
    and-int/lit16 v0, p3, 0x80

    #@f8
    if-nez v0, :cond_fd

    #@fa
    .line 1275
    invoke-virtual {p0, v7}, Landroid/media/AudioService;->setRingerMode(I)V

    #@fd
    .line 1280
    .end local v7           #newRingerMode:I
    :cond_fd
    iget-object v0, p0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@ff
    aget v1, v0, p1

    #@101
    const/4 v4, 0x0

    #@102
    const/4 v5, 0x1

    #@103
    move-object v0, p0

    #@104
    move v2, p2

    #@105
    invoke-direct/range {v0 .. v5}, Landroid/media/AudioService;->setStreamVolumeInt(IIIZZ)V

    #@108
    .line 1282
    iget-object v0, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@10a
    aget-object v1, v0, p1

    #@10c
    iget-object v0, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@10e
    aget-object v0, v0, p1

    #@110
    invoke-static {v0}, Landroid/media/AudioService$VolumeStreamState;->access$700(Landroid/media/AudioService$VolumeStreamState;)I

    #@113
    move-result v0

    #@114
    if-eqz v0, :cond_139

    #@116
    const/4 v0, 0x1

    #@117
    :goto_117
    invoke-virtual {v1, v3, v0}, Landroid/media/AudioService$VolumeStreamState;->getIndex(IZ)I

    #@11a
    move-result p2

    #@11b
    goto/16 :goto_8f

    #@11d
    .line 1227
    .end local v8           #oldIndex:I
    :cond_11d
    const/4 v0, 0x0

    #@11e
    goto/16 :goto_9a

    #@120
    .line 1253
    .restart local v8       #oldIndex:I
    .restart local v10       #ringerMode:I
    :cond_120
    const/4 v7, 0x0

    #@121
    goto :goto_f6

    #@122
    .line 1257
    :cond_122
    move v7, v10

    #@123
    .restart local v7       #newRingerMode:I
    goto :goto_f6

    #@124
    .line 1260
    .end local v7           #newRingerMode:I
    .end local v10           #ringerMode:I
    :cond_124
    iget-boolean v0, p0, Landroid/media/AudioService;->mHasVibrator:Z

    #@126
    if-eqz v0, :cond_135

    #@128
    const/4 v7, 0x1

    #@129
    .line 1262
    .restart local v7       #newRingerMode:I
    :goto_129
    iget-object v0, p0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@12b
    aget v1, v0, p1

    #@12d
    const/4 v4, 0x0

    #@12e
    const/4 v5, 0x1

    #@12f
    move-object v0, p0

    #@130
    move v2, p2

    #@131
    invoke-direct/range {v0 .. v5}, Landroid/media/AudioService;->setStreamVolumeInt(IIIZZ)V

    #@134
    goto :goto_f6

    #@135
    .line 1260
    .end local v7           #newRingerMode:I
    :cond_135
    const/4 v7, 0x0

    #@136
    goto :goto_129

    #@137
    .line 1270
    :cond_137
    const/4 v7, 0x2

    #@138
    .restart local v7       #newRingerMode:I
    goto :goto_f6

    #@139
    .line 1282
    .end local v7           #newRingerMode:I
    :cond_139
    const/4 v0, 0x0

    #@13a
    goto :goto_117
.end method

.method public setStreamVolumeAll(III)V
    .registers 12
    .parameter "streamType"
    .parameter "index"
    .parameter "flags"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 1292
    invoke-direct {p0, p1}, Landroid/media/AudioService;->ensureValidStreamType(I)V

    #@4
    .line 1293
    iget-object v0, p0, Landroid/media/AudioService;->mStreamStates:[Landroid/media/AudioService$VolumeStreamState;

    #@6
    iget-object v1, p0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@8
    aget v1, v1, p1

    #@a
    aget-object v7, v0, v1

    #@c
    .line 1294
    .local v7, streamState:Landroid/media/AudioService$VolumeStreamState;
    const/4 v3, 0x2

    #@d
    .line 1297
    .local v3, device:I
    const/4 v6, 0x0

    #@e
    .local v6, i:I
    :goto_e
    const/4 v0, 0x4

    #@f
    if-ge v6, v0, :cond_39

    #@11
    .line 1298
    if-nez v6, :cond_2a

    #@13
    .line 1299
    const/4 v3, 0x2

    #@14
    .line 1308
    :cond_14
    :goto_14
    mul-int/lit8 v0, p2, 0xa

    #@16
    iget-object v1, p0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@18
    aget v1, v1, p1

    #@1a
    invoke-direct {p0, v0, p1, v1}, Landroid/media/AudioService;->rescaleIndex(III)I

    #@1d
    move-result v2

    #@1e
    .line 1309
    .local v2, indexTemp:I
    iget-object v0, p0, Landroid/media/AudioService;->mStreamVolumeAlias:[I

    #@20
    aget v1, v0, p1

    #@22
    const/4 v5, 0x0

    #@23
    move-object v0, p0

    #@24
    invoke-direct/range {v0 .. v5}, Landroid/media/AudioService;->setStreamVolumeInt(IIIZZ)V

    #@27
    .line 1297
    add-int/lit8 v6, v6, 0x1

    #@29
    goto :goto_e

    #@2a
    .line 1300
    .end local v2           #indexTemp:I
    :cond_2a
    if-ne v6, v4, :cond_2e

    #@2c
    .line 1301
    const/4 v3, 0x4

    #@2d
    goto :goto_14

    #@2e
    .line 1302
    :cond_2e
    const/4 v0, 0x2

    #@2f
    if-ne v6, v0, :cond_34

    #@31
    .line 1303
    const/16 v3, 0x8

    #@33
    goto :goto_14

    #@34
    .line 1304
    :cond_34
    const/4 v0, 0x3

    #@35
    if-ne v6, v0, :cond_14

    #@37
    .line 1305
    const/4 v3, 0x1

    #@38
    goto :goto_14

    #@39
    .line 1311
    :cond_39
    return-void
.end method

.method public setVibrateSetting(II)V
    .registers 4
    .parameter "vibrateType"
    .parameter "vibrateSetting"

    #@0
    .prologue
    .line 1782
    iget-boolean v0, p0, Landroid/media/AudioService;->mHasVibrator:Z

    #@2
    if-nez v0, :cond_5

    #@4
    .line 1789
    :goto_4
    return-void

    #@5
    .line 1784
    :cond_5
    iget v0, p0, Landroid/media/AudioService;->mVibrateSetting:I

    #@7
    invoke-static {v0, p1, p2}, Landroid/media/AudioService;->getValueForVibrateSetting(III)I

    #@a
    move-result v0

    #@b
    iput v0, p0, Landroid/media/AudioService;->mVibrateSetting:I

    #@d
    .line 1787
    invoke-direct {p0, p1}, Landroid/media/AudioService;->broadcastVibrateSetting(I)V

    #@10
    goto :goto_4
.end method

.method public setVoiceActivationState(I)V
    .registers 5
    .parameter "state"

    #@0
    .prologue
    .line 7244
    const-string v0, "AudioService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v2, "setVoiceActivationState().. state = "

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 7245
    iput p1, p0, Landroid/media/AudioService;->mVoiceActivationState:I

    #@1b
    .line 7246
    iget v0, p0, Landroid/media/AudioService;->mVoiceActivationState:I

    #@1d
    const/4 v1, 0x2

    #@1e
    if-ne v0, v1, :cond_2a

    #@20
    .line 7247
    const-string/jumbo v0, "persist.sys.voice_state"

    #@23
    const-string/jumbo v1, "recording"

    #@26
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@29
    .line 7253
    :goto_29
    return-void

    #@2a
    .line 7248
    :cond_2a
    iget v0, p0, Landroid/media/AudioService;->mVoiceActivationState:I

    #@2c
    const/4 v1, 0x1

    #@2d
    if-ne v0, v1, :cond_39

    #@2f
    .line 7249
    const-string/jumbo v0, "persist.sys.voice_state"

    #@32
    const-string/jumbo v1, "starting"

    #@35
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@38
    goto :goto_29

    #@39
    .line 7251
    :cond_39
    const-string/jumbo v0, "persist.sys.voice_state"

    #@3c
    const-string/jumbo v1, "none"

    #@3f
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@42
    goto :goto_29
.end method

.method public setWiredDeviceConnectionState(IILjava/lang/String;)V
    .registers 12
    .parameter "device"
    .parameter "state"
    .parameter "name"

    #@0
    .prologue
    .line 3261
    const-string v0, "AudioService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v2, "setWiredDeviceConnectionState// device:"

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    const-string v2, " state:"

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    const-string v2, " name:"

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    const-string v2, " volume:"

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    const/4 v2, 0x3

    #@2d
    invoke-virtual {p0, v2}, Landroid/media/AudioService;->getStreamVolume(I)I

    #@30
    move-result v2

    #@31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v1

    #@39
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 3265
    iget-object v7, p0, Landroid/media/AudioService;->mConnectedDevices:Ljava/util/HashMap;

    #@3e
    monitor-enter v7

    #@3f
    .line 3266
    :try_start_3f
    invoke-direct {p0, p1, p2}, Landroid/media/AudioService;->checkSendBecomingNoisyIntent(II)I

    #@42
    move-result v6

    #@43
    .line 3267
    .local v6, delay:I
    iget-object v1, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@45
    const/16 v2, 0x15

    #@47
    move-object v0, p0

    #@48
    move v3, p1

    #@49
    move v4, p2

    #@4a
    move-object v5, p3

    #@4b
    invoke-direct/range {v0 .. v6}, Landroid/media/AudioService;->queueMsgUnderWakeLock(Landroid/os/Handler;IIILjava/lang/Object;I)V

    #@4e
    .line 3273
    monitor-exit v7

    #@4f
    .line 3274
    return-void

    #@50
    .line 3273
    .end local v6           #delay:I
    :catchall_50
    move-exception v0

    #@51
    monitor-exit v7
    :try_end_52
    .catchall {:try_start_3f .. :try_end_52} :catchall_50

    #@52
    throw v0
.end method

.method public shouldVibrate(I)Z
    .registers 5
    .parameter "vibrateType"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1754
    iget-boolean v2, p0, Landroid/media/AudioService;->mHasVibrator:Z

    #@4
    if-nez v2, :cond_7

    #@6
    .line 1769
    :goto_6
    :pswitch_6
    return v1

    #@7
    .line 1756
    :cond_7
    invoke-virtual {p0, p1}, Landroid/media/AudioService;->getVibrateSetting(I)I

    #@a
    move-result v2

    #@b
    packed-switch v2, :pswitch_data_24

    #@e
    goto :goto_6

    #@f
    .line 1759
    :pswitch_f
    invoke-virtual {p0}, Landroid/media/AudioService;->getRingerMode()I

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_17

    #@15
    :goto_15
    move v1, v0

    #@16
    goto :goto_6

    #@17
    :cond_17
    move v0, v1

    #@18
    goto :goto_15

    #@19
    .line 1762
    :pswitch_19
    invoke-virtual {p0}, Landroid/media/AudioService;->getRingerMode()I

    #@1c
    move-result v2

    #@1d
    if-ne v2, v0, :cond_21

    #@1f
    :goto_1f
    move v1, v0

    #@20
    goto :goto_6

    #@21
    :cond_21
    move v0, v1

    #@22
    goto :goto_1f

    #@23
    .line 1756
    nop

    #@24
    :pswitch_data_24
    .packed-switch 0x0
        :pswitch_6
        :pswitch_f
        :pswitch_19
    .end packed-switch
.end method

.method public startBluetoothSco(Landroid/os/IBinder;)V
    .registers 4
    .parameter "cb"

    #@0
    .prologue
    .line 2488
    const-string/jumbo v1, "startBluetoothSco()"

    #@3
    invoke-virtual {p0, v1}, Landroid/media/AudioService;->checkAudioSettingsPermission(Ljava/lang/String;)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_d

    #@9
    iget-boolean v1, p0, Landroid/media/AudioService;->mBootCompleted:Z

    #@b
    if-nez v1, :cond_e

    #@d
    .line 2494
    :cond_d
    :goto_d
    return-void

    #@e
    .line 2492
    :cond_e
    const/4 v1, 0x1

    #@f
    invoke-direct {p0, p1, v1}, Landroid/media/AudioService;->getScoClient(Landroid/os/IBinder;Z)Landroid/media/AudioService$ScoClient;

    #@12
    move-result-object v0

    #@13
    .line 2493
    .local v0, client:Landroid/media/AudioService$ScoClient;
    invoke-virtual {v0}, Landroid/media/AudioService$ScoClient;->incCount()V

    #@16
    goto :goto_d
.end method

.method public startWatchingRoutes(Landroid/media/IAudioRoutesObserver;)Landroid/media/AudioRoutesInfo;
    .registers 5
    .parameter "observer"

    #@0
    .prologue
    .line 6776
    iget-object v2, p0, Landroid/media/AudioService;->mCurAudioRoutes:Landroid/media/AudioRoutesInfo;

    #@2
    monitor-enter v2

    #@3
    .line 6777
    :try_start_3
    new-instance v0, Landroid/media/AudioRoutesInfo;

    #@5
    iget-object v1, p0, Landroid/media/AudioService;->mCurAudioRoutes:Landroid/media/AudioRoutesInfo;

    #@7
    invoke-direct {v0, v1}, Landroid/media/AudioRoutesInfo;-><init>(Landroid/media/AudioRoutesInfo;)V

    #@a
    .line 6778
    .local v0, routes:Landroid/media/AudioRoutesInfo;
    iget-object v1, p0, Landroid/media/AudioService;->mRoutesObservers:Landroid/os/RemoteCallbackList;

    #@c
    invoke-virtual {v1, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    #@f
    .line 6779
    monitor-exit v2

    #@10
    return-object v0

    #@11
    .line 6780
    .end local v0           #routes:Landroid/media/AudioRoutesInfo;
    :catchall_11
    move-exception v1

    #@12
    monitor-exit v2
    :try_end_13
    .catchall {:try_start_3 .. :try_end_13} :catchall_11

    #@13
    throw v1
.end method

.method public stopBluetoothSco(Landroid/os/IBinder;)V
    .registers 4
    .parameter "cb"

    #@0
    .prologue
    .line 2498
    const-string/jumbo v1, "stopBluetoothSco()"

    #@3
    invoke-virtual {p0, v1}, Landroid/media/AudioService;->checkAudioSettingsPermission(Ljava/lang/String;)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_d

    #@9
    iget-boolean v1, p0, Landroid/media/AudioService;->mBootCompleted:Z

    #@b
    if-nez v1, :cond_e

    #@d
    .line 2506
    :cond_d
    :goto_d
    return-void

    #@e
    .line 2502
    :cond_e
    const/4 v1, 0x0

    #@f
    invoke-direct {p0, p1, v1}, Landroid/media/AudioService;->getScoClient(Landroid/os/IBinder;Z)Landroid/media/AudioService$ScoClient;

    #@12
    move-result-object v0

    #@13
    .line 2503
    .local v0, client:Landroid/media/AudioService$ScoClient;
    if-eqz v0, :cond_d

    #@15
    .line 2504
    invoke-virtual {v0}, Landroid/media/AudioService$ScoClient;->decCount()V

    #@18
    goto :goto_d
.end method

.method public unloadSoundEffects()V
    .registers 8

    #@0
    .prologue
    .line 2282
    iget-object v4, p0, Landroid/media/AudioService;->mSoundEffectsLock:Ljava/lang/Object;

    #@2
    monitor-enter v4

    #@3
    .line 2283
    :try_start_3
    iget-object v3, p0, Landroid/media/AudioService;->mSoundPool:Landroid/media/SoundPool;

    #@5
    if-nez v3, :cond_9

    #@7
    .line 2284
    monitor-exit v4

    #@8
    .line 2308
    :goto_8
    return-void

    #@9
    .line 2287
    :cond_9
    iget-object v3, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@b
    const/16 v5, 0x8

    #@d
    invoke-virtual {v3, v5}, Landroid/media/AudioService$AudioHandler;->removeMessages(I)V

    #@10
    .line 2288
    iget-object v3, p0, Landroid/media/AudioService;->mAudioHandler:Landroid/media/AudioService$AudioHandler;

    #@12
    const/4 v5, 0x6

    #@13
    invoke-virtual {v3, v5}, Landroid/media/AudioService$AudioHandler;->removeMessages(I)V

    #@16
    .line 2290
    sget-object v3, Landroid/media/AudioService;->SOUND_EFFECT_FILES:[Ljava/lang/String;

    #@18
    array-length v3, v3

    #@19
    new-array v2, v3, [I

    #@1b
    .line 2291
    .local v2, poolId:[I
    const/4 v1, 0x0

    #@1c
    .local v1, fileIdx:I
    :goto_1c
    sget-object v3, Landroid/media/AudioService;->SOUND_EFFECT_FILES:[Ljava/lang/String;

    #@1e
    array-length v3, v3

    #@1f
    if-ge v1, v3, :cond_27

    #@21
    .line 2292
    const/4 v3, 0x0

    #@22
    aput v3, v2, v1

    #@24
    .line 2291
    add-int/lit8 v1, v1, 0x1

    #@26
    goto :goto_1c

    #@27
    .line 2295
    :cond_27
    const/4 v0, 0x0

    #@28
    .local v0, effect:I
    :goto_28
    const/16 v3, 0xb

    #@2a
    if-ge v0, v3, :cond_65

    #@2c
    .line 2296
    iget-object v3, p0, Landroid/media/AudioService;->SOUND_EFFECT_FILES_MAP:[[I

    #@2e
    aget-object v3, v3, v0

    #@30
    const/4 v5, 0x1

    #@31
    aget v3, v3, v5

    #@33
    if-gtz v3, :cond_38

    #@35
    .line 2295
    :cond_35
    :goto_35
    add-int/lit8 v0, v0, 0x1

    #@37
    goto :goto_28

    #@38
    .line 2299
    :cond_38
    iget-object v3, p0, Landroid/media/AudioService;->SOUND_EFFECT_FILES_MAP:[[I

    #@3a
    aget-object v3, v3, v0

    #@3c
    const/4 v5, 0x0

    #@3d
    aget v3, v3, v5

    #@3f
    aget v3, v2, v3

    #@41
    if-nez v3, :cond_35

    #@43
    .line 2300
    iget-object v3, p0, Landroid/media/AudioService;->mSoundPool:Landroid/media/SoundPool;

    #@45
    iget-object v5, p0, Landroid/media/AudioService;->SOUND_EFFECT_FILES_MAP:[[I

    #@47
    aget-object v5, v5, v0

    #@49
    const/4 v6, 0x1

    #@4a
    aget v5, v5, v6

    #@4c
    invoke-virtual {v3, v5}, Landroid/media/SoundPool;->unload(I)Z

    #@4f
    .line 2301
    iget-object v3, p0, Landroid/media/AudioService;->SOUND_EFFECT_FILES_MAP:[[I

    #@51
    aget-object v3, v3, v0

    #@53
    const/4 v5, 0x1

    #@54
    const/4 v6, -0x1

    #@55
    aput v6, v3, v5

    #@57
    .line 2302
    iget-object v3, p0, Landroid/media/AudioService;->SOUND_EFFECT_FILES_MAP:[[I

    #@59
    aget-object v3, v3, v0

    #@5b
    const/4 v5, 0x0

    #@5c
    aget v3, v3, v5

    #@5e
    const/4 v5, -0x1

    #@5f
    aput v5, v2, v3

    #@61
    goto :goto_35

    #@62
    .line 2307
    .end local v0           #effect:I
    .end local v1           #fileIdx:I
    .end local v2           #poolId:[I
    :catchall_62
    move-exception v3

    #@63
    monitor-exit v4
    :try_end_64
    .catchall {:try_start_3 .. :try_end_64} :catchall_62

    #@64
    throw v3

    #@65
    .line 2305
    .restart local v0       #effect:I
    .restart local v1       #fileIdx:I
    .restart local v2       #poolId:[I
    :cond_65
    :try_start_65
    iget-object v3, p0, Landroid/media/AudioService;->mSoundPool:Landroid/media/SoundPool;

    #@67
    invoke-virtual {v3}, Landroid/media/SoundPool;->release()V

    #@6a
    .line 2306
    const/4 v3, 0x0

    #@6b
    iput-object v3, p0, Landroid/media/AudioService;->mSoundPool:Landroid/media/SoundPool;

    #@6d
    .line 2307
    monitor-exit v4
    :try_end_6e
    .catchall {:try_start_65 .. :try_end_6e} :catchall_62

    #@6e
    goto :goto_8
.end method

.method public unregisterAudioFocusClient(Ljava/lang/String;)V
    .registers 4
    .parameter "clientId"

    #@0
    .prologue
    .line 5192
    sget-object v1, Landroid/media/AudioService;->mAudioFocusLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 5193
    const/4 v0, 0x0

    #@4
    :try_start_4
    invoke-direct {p0, p1, v0}, Landroid/media/AudioService;->removeFocusStackEntry(Ljava/lang/String;Z)V

    #@7
    .line 5194
    monitor-exit v1

    #@8
    .line 5195
    return-void

    #@9
    .line 5194
    :catchall_9
    move-exception v0

    #@a
    monitor-exit v1
    :try_end_b
    .catchall {:try_start_4 .. :try_end_b} :catchall_9

    #@b
    throw v0
.end method

.method public unregisterMediaButtonEventReceiverForCalls()V
    .registers 3

    #@0
    .prologue
    .line 6135
    iget-object v0, p0, Landroid/media/AudioService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.MODIFY_PHONE_STATE"

    #@4
    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingPermission(Ljava/lang/String;)I

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_12

    #@a
    .line 6137
    const-string v0, "AudioService"

    #@c
    const-string v1, "Invalid permissions to unregister media button receiver for calls"

    #@e
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 6143
    :goto_11
    return-void

    #@12
    .line 6140
    :cond_12
    iget-object v1, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@14
    monitor-enter v1

    #@15
    .line 6141
    const/4 v0, 0x0

    #@16
    :try_start_16
    iput-object v0, p0, Landroid/media/AudioService;->mMediaReceiverForCalls:Landroid/content/ComponentName;

    #@18
    .line 6142
    monitor-exit v1

    #@19
    goto :goto_11

    #@1a
    :catchall_1a
    move-exception v0

    #@1b
    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_16 .. :try_end_1c} :catchall_1a

    #@1c
    throw v0
.end method

.method public unregisterMediaButtonIntent(Landroid/app/PendingIntent;Landroid/content/ComponentName;)V
    .registers 7
    .parameter "mediaIntent"
    .parameter "eventReceiver"

    #@0
    .prologue
    .line 6102
    const-string v1, "AudioService"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "  Remote Control   unregisterMediaButtonIntent() for "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 6104
    sget-object v2, Landroid/media/AudioService;->mAudioFocusLock:Ljava/lang/Object;

    #@1a
    monitor-enter v2

    #@1b
    .line 6105
    :try_start_1b
    iget-object v3, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@1d
    monitor-enter v3
    :try_end_1e
    .catchall {:try_start_1b .. :try_end_1e} :catchall_32

    #@1e
    .line 6106
    :try_start_1e
    invoke-direct {p0, p1}, Landroid/media/AudioService;->isCurrentRcController(Landroid/app/PendingIntent;)Z

    #@21
    move-result v0

    #@22
    .line 6107
    .local v0, topOfStackWillChange:Z
    invoke-direct {p0, p1}, Landroid/media/AudioService;->removeMediaButtonReceiver(Landroid/app/PendingIntent;)V

    #@25
    .line 6108
    if-eqz v0, :cond_2c

    #@27
    .line 6110
    const/16 v1, 0xf

    #@29
    invoke-direct {p0, v1}, Landroid/media/AudioService;->checkUpdateRemoteControlDisplay_syncAfRcs(I)V

    #@2c
    .line 6112
    :cond_2c
    monitor-exit v3
    :try_end_2d
    .catchall {:try_start_1e .. :try_end_2d} :catchall_2f

    #@2d
    .line 6113
    :try_start_2d
    monitor-exit v2
    :try_end_2e
    .catchall {:try_start_2d .. :try_end_2e} :catchall_32

    #@2e
    .line 6114
    return-void

    #@2f
    .line 6112
    .end local v0           #topOfStackWillChange:Z
    :catchall_2f
    move-exception v1

    #@30
    :try_start_30
    monitor-exit v3
    :try_end_31
    .catchall {:try_start_30 .. :try_end_31} :catchall_2f

    #@31
    :try_start_31
    throw v1

    #@32
    .line 6113
    :catchall_32
    move-exception v1

    #@33
    monitor-exit v2
    :try_end_34
    .catchall {:try_start_31 .. :try_end_34} :catchall_32

    #@34
    throw v1
.end method

.method public unregisterRemoteControlClient(Landroid/app/PendingIntent;Landroid/media/IRemoteControlClient;)V
    .registers 8
    .parameter "mediaIntent"
    .parameter "rcClient"

    #@0
    .prologue
    .line 6220
    sget-object v3, Landroid/media/AudioService;->mAudioFocusLock:Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 6221
    :try_start_3
    iget-object v4, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@5
    monitor-enter v4
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_35

    #@6
    .line 6222
    :try_start_6
    iget-object v2, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@8
    invoke-virtual {v2}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    #@b
    move-result-object v1

    #@c
    .line 6223
    .local v1, stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :cond_c
    :goto_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@f
    move-result v2

    #@10
    if-eqz v2, :cond_38

    #@12
    .line 6224
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@15
    move-result-object v0

    #@16
    check-cast v0, Landroid/media/AudioService$RemoteControlStackEntry;

    #@18
    .line 6225
    .local v0, rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    iget-object v2, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mMediaIntent:Landroid/app/PendingIntent;

    #@1a
    invoke-virtual {v2, p1}, Landroid/app/PendingIntent;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v2

    #@1e
    if-eqz v2, :cond_c

    #@20
    iget-object v2, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mRcClient:Landroid/media/IRemoteControlClient;

    #@22
    invoke-virtual {p2, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v2

    #@26
    if-eqz v2, :cond_c

    #@28
    .line 6229
    invoke-virtual {v0}, Landroid/media/AudioService$RemoteControlStackEntry;->unlinkToRcClientDeath()V

    #@2b
    .line 6231
    const/4 v2, 0x0

    #@2c
    iput-object v2, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mRcClient:Landroid/media/IRemoteControlClient;

    #@2e
    .line 6232
    const/4 v2, 0x0

    #@2f
    iput-object v2, v0, Landroid/media/AudioService$RemoteControlStackEntry;->mCallingPackageName:Ljava/lang/String;

    #@31
    goto :goto_c

    #@32
    .line 6235
    .end local v0           #rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    .end local v1           #stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :catchall_32
    move-exception v2

    #@33
    monitor-exit v4
    :try_end_34
    .catchall {:try_start_6 .. :try_end_34} :catchall_32

    #@34
    :try_start_34
    throw v2

    #@35
    .line 6236
    :catchall_35
    move-exception v2

    #@36
    monitor-exit v3
    :try_end_37
    .catchall {:try_start_34 .. :try_end_37} :catchall_35

    #@37
    throw v2

    #@38
    .line 6235
    .restart local v1       #stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :cond_38
    :try_start_38
    monitor-exit v4
    :try_end_39
    .catchall {:try_start_38 .. :try_end_39} :catchall_32

    #@39
    .line 6236
    :try_start_39
    monitor-exit v3
    :try_end_3a
    .catchall {:try_start_39 .. :try_end_3a} :catchall_35

    #@3a
    .line 6237
    return-void
.end method

.method public unregisterRemoteControlDisplay(Landroid/media/IRemoteControlDisplay;)V
    .registers 9
    .parameter "rcd"

    #@0
    .prologue
    .line 6352
    iget-object v4, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@2
    monitor-enter v4

    #@3
    .line 6354
    if-eqz p1, :cond_9

    #@5
    :try_start_5
    iget-object v3, p0, Landroid/media/AudioService;->mRcDisplay:Landroid/media/IRemoteControlDisplay;

    #@7
    if-eq p1, v3, :cond_b

    #@9
    .line 6356
    :cond_9
    monitor-exit v4

    #@a
    .line 6376
    :goto_a
    return-void

    #@b
    .line 6359
    :cond_b
    invoke-direct {p0}, Landroid/media/AudioService;->rcDisplay_stopDeathMonitor_syncRcStack()V

    #@e
    .line 6360
    const/4 v3, 0x0

    #@f
    iput-object v3, p0, Landroid/media/AudioService;->mRcDisplay:Landroid/media/IRemoteControlDisplay;

    #@11
    .line 6363
    iget-object v3, p0, Landroid/media/AudioService;->mRCStack:Ljava/util/Stack;

    #@13
    invoke-virtual {v3}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    #@16
    move-result-object v2

    #@17
    .line 6364
    .local v2, stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :cond_17
    :goto_17
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@1a
    move-result v3

    #@1b
    if-eqz v3, :cond_4d

    #@1d
    .line 6365
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@20
    move-result-object v1

    #@21
    check-cast v1, Landroid/media/AudioService$RemoteControlStackEntry;

    #@23
    .line 6366
    .local v1, rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    iget-object v3, v1, Landroid/media/AudioService$RemoteControlStackEntry;->mRcClient:Landroid/media/IRemoteControlClient;
    :try_end_25
    .catchall {:try_start_5 .. :try_end_25} :catchall_4a

    #@25
    if-eqz v3, :cond_17

    #@27
    .line 6368
    :try_start_27
    iget-object v3, v1, Landroid/media/AudioService$RemoteControlStackEntry;->mRcClient:Landroid/media/IRemoteControlClient;

    #@29
    invoke-interface {v3, p1}, Landroid/media/IRemoteControlClient;->unplugRemoteControlDisplay(Landroid/media/IRemoteControlDisplay;)V
    :try_end_2c
    .catchall {:try_start_27 .. :try_end_2c} :catchall_4a
    .catch Landroid/os/RemoteException; {:try_start_27 .. :try_end_2c} :catch_2d

    #@2c
    goto :goto_17

    #@2d
    .line 6369
    :catch_2d
    move-exception v0

    #@2e
    .line 6370
    .local v0, e:Landroid/os/RemoteException;
    :try_start_2e
    const-string v3, "AudioService"

    #@30
    new-instance v5, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v6, "Error disconnecting remote control display to client: "

    #@37
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v5

    #@3b
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v5

    #@3f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v5

    #@43
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 6371
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@49
    goto :goto_17

    #@4a
    .line 6375
    .end local v0           #e:Landroid/os/RemoteException;
    .end local v1           #rcse:Landroid/media/AudioService$RemoteControlStackEntry;
    .end local v2           #stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :catchall_4a
    move-exception v3

    #@4b
    monitor-exit v4
    :try_end_4c
    .catchall {:try_start_2e .. :try_end_4c} :catchall_4a

    #@4c
    throw v3

    #@4d
    .restart local v2       #stackIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/media/AudioService$RemoteControlStackEntry;>;"
    :cond_4d
    :try_start_4d
    monitor-exit v4
    :try_end_4e
    .catchall {:try_start_4d .. :try_end_4e} :catchall_4a

    #@4e
    goto :goto_a
.end method
