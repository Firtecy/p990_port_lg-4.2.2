.class public Landroid/media/MediaRouter$RouteInfo;
.super Ljava/lang/Object;
.source "MediaRouter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/MediaRouter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RouteInfo"
.end annotation


# static fields
.field public static final PLAYBACK_TYPE_LOCAL:I = 0x0

.field public static final PLAYBACK_TYPE_REMOTE:I = 0x1

.field public static final PLAYBACK_VOLUME_FIXED:I = 0x0

.field public static final PLAYBACK_VOLUME_VARIABLE:I = 0x1

.field public static final STATUS_AVAILABLE:I = 0x3

.field public static final STATUS_CONNECTING:I = 0x2

.field public static final STATUS_NONE:I = 0x0

.field public static final STATUS_NOT_AVAILABLE:I = 0x4

.field public static final STATUS_SCANNING:I = 0x1


# instance fields
.field final mCategory:Landroid/media/MediaRouter$RouteCategory;

.field mDeviceAddress:Ljava/lang/String;

.field mEnabled:Z

.field mGroup:Landroid/media/MediaRouter$RouteGroup;

.field mIcon:Landroid/graphics/drawable/Drawable;

.field mName:Ljava/lang/CharSequence;

.field mNameResId:I

.field mPlaybackStream:I

.field mPlaybackType:I

.field mPresentationDisplay:Landroid/view/Display;

.field final mRemoteVolObserver:Landroid/media/IRemoteVolumeObserver$Stub;

.field private mStatus:Ljava/lang/CharSequence;

.field private mStatusCode:I

.field mSupportedTypes:I

.field private mTag:Ljava/lang/Object;

.field mVcb:Landroid/media/MediaRouter$VolumeCallbackInfo;

.field mVolume:I

.field mVolumeHandling:I

.field mVolumeMax:I


# direct methods
.method constructor <init>(Landroid/media/MediaRouter$RouteCategory;)V
    .registers 5
    .parameter "category"

    #@0
    .prologue
    const/16 v2, 0xf

    #@2
    const/4 v1, 0x1

    #@3
    .line 976
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 928
    const/4 v0, 0x0

    #@7
    iput v0, p0, Landroid/media/MediaRouter$RouteInfo;->mPlaybackType:I

    #@9
    .line 929
    iput v2, p0, Landroid/media/MediaRouter$RouteInfo;->mVolumeMax:I

    #@b
    .line 930
    iput v2, p0, Landroid/media/MediaRouter$RouteInfo;->mVolume:I

    #@d
    .line 931
    iput v1, p0, Landroid/media/MediaRouter$RouteInfo;->mVolumeHandling:I

    #@f
    .line 932
    const/4 v0, 0x3

    #@10
    iput v0, p0, Landroid/media/MediaRouter$RouteInfo;->mPlaybackStream:I

    #@12
    .line 937
    iput-boolean v1, p0, Landroid/media/MediaRouter$RouteInfo;->mEnabled:Z

    #@14
    .line 1254
    new-instance v0, Landroid/media/MediaRouter$RouteInfo$1;

    #@16
    invoke-direct {v0, p0}, Landroid/media/MediaRouter$RouteInfo$1;-><init>(Landroid/media/MediaRouter$RouteInfo;)V

    #@19
    iput-object v0, p0, Landroid/media/MediaRouter$RouteInfo;->mRemoteVolObserver:Landroid/media/IRemoteVolumeObserver$Stub;

    #@1b
    .line 977
    iput-object p1, p0, Landroid/media/MediaRouter$RouteInfo;->mCategory:Landroid/media/MediaRouter$RouteCategory;

    #@1d
    .line 978
    return-void
.end method


# virtual methods
.method public getCategory()Landroid/media/MediaRouter$RouteCategory;
    .registers 2

    #@0
    .prologue
    .line 1067
    iget-object v0, p0, Landroid/media/MediaRouter$RouteInfo;->mCategory:Landroid/media/MediaRouter$RouteCategory;

    #@2
    return-object v0
.end method

.method public getGroup()Landroid/media/MediaRouter$RouteGroup;
    .registers 2

    #@0
    .prologue
    .line 1060
    iget-object v0, p0, Landroid/media/MediaRouter$RouteInfo;->mGroup:Landroid/media/MediaRouter$RouteGroup;

    #@2
    return-object v0
.end method

.method public getIconDrawable()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 1077
    iget-object v0, p0, Landroid/media/MediaRouter$RouteInfo;->mIcon:Landroid/graphics/drawable/Drawable;

    #@2
    return-object v0
.end method

.method public getName()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 985
    sget-object v0, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2
    iget-object v0, v0, Landroid/media/MediaRouter$Static;->mResources:Landroid/content/res/Resources;

    #@4
    invoke-virtual {p0, v0}, Landroid/media/MediaRouter$RouteInfo;->getName(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getName(Landroid/content/Context;)Ljava/lang/CharSequence;
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 996
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0}, Landroid/media/MediaRouter$RouteInfo;->getName(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method getName(Landroid/content/res/Resources;)Ljava/lang/CharSequence;
    .registers 3
    .parameter "res"

    #@0
    .prologue
    .line 1000
    iget v0, p0, Landroid/media/MediaRouter$RouteInfo;->mNameResId:I

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 1001
    iget v0, p0, Landroid/media/MediaRouter$RouteInfo;->mNameResId:I

    #@6
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/media/MediaRouter$RouteInfo;->mName:Ljava/lang/CharSequence;

    #@c
    .line 1003
    :goto_c
    return-object v0

    #@d
    :cond_d
    iget-object v0, p0, Landroid/media/MediaRouter$RouteInfo;->mName:Ljava/lang/CharSequence;

    #@f
    goto :goto_c
.end method

.method public getPlaybackStream()I
    .registers 2

    #@0
    .prologue
    .line 1117
    iget v0, p0, Landroid/media/MediaRouter$RouteInfo;->mPlaybackStream:I

    #@2
    return v0
.end method

.method public getPlaybackType()I
    .registers 2

    #@0
    .prologue
    .line 1109
    iget v0, p0, Landroid/media/MediaRouter$RouteInfo;->mPlaybackType:I

    #@2
    return v0
.end method

.method public getPresentationDisplay()Landroid/view/Display;
    .registers 2

    #@0
    .prologue
    .line 1234
    iget-object v0, p0, Landroid/media/MediaRouter$RouteInfo;->mPresentationDisplay:Landroid/view/Display;

    #@2
    return-object v0
.end method

.method public getStatus()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 1011
    iget-object v0, p0, Landroid/media/MediaRouter$RouteInfo;->mStatus:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getStatusCode()I
    .registers 2

    #@0
    .prologue
    .line 1046
    iget v0, p0, Landroid/media/MediaRouter$RouteInfo;->mStatusCode:I

    #@2
    return v0
.end method

.method public getSupportedTypes()I
    .registers 2

    #@0
    .prologue
    .line 1053
    iget v0, p0, Landroid/media/MediaRouter$RouteInfo;->mSupportedTypes:I

    #@2
    return v0
.end method

.method public getTag()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 1101
    iget-object v0, p0, Landroid/media/MediaRouter$RouteInfo;->mTag:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method public getVolume()I
    .registers 5

    #@0
    .prologue
    .line 1128
    iget v2, p0, Landroid/media/MediaRouter$RouteInfo;->mPlaybackType:I

    #@2
    if-nez v2, :cond_19

    #@4
    .line 1129
    const/4 v1, 0x0

    #@5
    .line 1131
    .local v1, vol:I
    :try_start_5
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@7
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mAudioService:Landroid/media/IAudioService;

    #@9
    iget v3, p0, Landroid/media/MediaRouter$RouteInfo;->mPlaybackStream:I

    #@b
    invoke-interface {v2, v3}, Landroid/media/IAudioService;->getStreamVolume(I)I
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_e} :catch_10

    #@e
    move-result v1

    #@f
    .line 1137
    .end local v1           #vol:I
    :goto_f
    return v1

    #@10
    .line 1132
    .restart local v1       #vol:I
    :catch_10
    move-exception v0

    #@11
    .line 1133
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "MediaRouter"

    #@13
    const-string v3, "Error getting local stream volume"

    #@15
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@18
    goto :goto_f

    #@19
    .line 1137
    .end local v0           #e:Landroid/os/RemoteException;
    .end local v1           #vol:I
    :cond_19
    iget v1, p0, Landroid/media/MediaRouter$RouteInfo;->mVolume:I

    #@1b
    goto :goto_f
.end method

.method public getVolumeHandling()I
    .registers 2

    #@0
    .prologue
    .line 1202
    iget v0, p0, Landroid/media/MediaRouter$RouteInfo;->mVolumeHandling:I

    #@2
    return v0
.end method

.method public getVolumeMax()I
    .registers 5

    #@0
    .prologue
    .line 1184
    iget v2, p0, Landroid/media/MediaRouter$RouteInfo;->mPlaybackType:I

    #@2
    if-nez v2, :cond_19

    #@4
    .line 1185
    const/4 v1, 0x0

    #@5
    .line 1187
    .local v1, volMax:I
    :try_start_5
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@7
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mAudioService:Landroid/media/IAudioService;

    #@9
    iget v3, p0, Landroid/media/MediaRouter$RouteInfo;->mPlaybackStream:I

    #@b
    invoke-interface {v2, v3}, Landroid/media/IAudioService;->getStreamMaxVolume(I)I
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_e} :catch_10

    #@e
    move-result v1

    #@f
    .line 1193
    .end local v1           #volMax:I
    :goto_f
    return v1

    #@10
    .line 1188
    .restart local v1       #volMax:I
    :catch_10
    move-exception v0

    #@11
    .line 1189
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "MediaRouter"

    #@13
    const-string v3, "Error getting local stream volume"

    #@15
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@18
    goto :goto_f

    #@19
    .line 1193
    .end local v0           #e:Landroid/os/RemoteException;
    .end local v1           #volMax:I
    :cond_19
    iget v1, p0, Landroid/media/MediaRouter$RouteInfo;->mVolumeMax:I

    #@1b
    goto :goto_f
.end method

.method public isEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 1241
    iget-boolean v0, p0, Landroid/media/MediaRouter$RouteInfo;->mEnabled:Z

    #@2
    return v0
.end method

.method public requestSetVolume(I)V
    .registers 6
    .parameter "volume"

    #@0
    .prologue
    .line 1146
    iget v1, p0, Landroid/media/MediaRouter$RouteInfo;->mPlaybackType:I

    #@2
    if-nez v1, :cond_18

    #@4
    .line 1148
    :try_start_4
    sget-object v1, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@6
    iget-object v1, v1, Landroid/media/MediaRouter$Static;->mAudioService:Landroid/media/IAudioService;

    #@8
    iget v2, p0, Landroid/media/MediaRouter$RouteInfo;->mPlaybackStream:I

    #@a
    const/4 v3, 0x0

    #@b
    invoke-interface {v1, v2, p1, v3}, Landroid/media/IAudioService;->setStreamVolume(III)V
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_e} :catch_f

    #@e
    .line 1157
    :goto_e
    return-void

    #@f
    .line 1149
    :catch_f
    move-exception v0

    #@10
    .line 1150
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "MediaRouter"

    #@12
    const-string v2, "Error setting local stream volume"

    #@14
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@17
    goto :goto_e

    #@18
    .line 1153
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_18
    const-string v1, "MediaRouter"

    #@1a
    new-instance v2, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    const-string v3, ".requestSetVolume(): "

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    const-string v3, "Non-local volume playback on system route? "

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    const-string v3, "Could not request volume change."

    #@39
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v2

    #@41
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    goto :goto_e
.end method

.method public requestUpdateVolume(I)V
    .registers 7
    .parameter "direction"

    #@0
    .prologue
    .line 1164
    iget v2, p0, Landroid/media/MediaRouter$RouteInfo;->mPlaybackType:I

    #@2
    if-nez v2, :cond_2a

    #@4
    .line 1166
    const/4 v2, 0x0

    #@5
    :try_start_5
    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteInfo;->getVolume()I

    #@8
    move-result v3

    #@9
    add-int/2addr v3, p1

    #@a
    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteInfo;->getVolumeMax()I

    #@d
    move-result v4

    #@e
    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    #@11
    move-result v3

    #@12
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    #@15
    move-result v1

    #@16
    .line 1168
    .local v1, volume:I
    sget-object v2, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@18
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mAudioService:Landroid/media/IAudioService;

    #@1a
    iget v3, p0, Landroid/media/MediaRouter$RouteInfo;->mPlaybackStream:I

    #@1c
    const/4 v4, 0x0

    #@1d
    invoke-interface {v2, v3, v1, v4}, Landroid/media/IAudioService;->setStreamVolume(III)V
    :try_end_20
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_20} :catch_21

    #@20
    .line 1177
    .end local v1           #volume:I
    :goto_20
    return-void

    #@21
    .line 1169
    :catch_21
    move-exception v0

    #@22
    .line 1170
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "MediaRouter"

    #@24
    const-string v3, "Error setting local stream volume"

    #@26
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@29
    goto :goto_20

    #@2a
    .line 1173
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_2a
    const-string v2, "MediaRouter"

    #@2c
    new-instance v3, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@34
    move-result-object v4

    #@35
    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    const-string v4, ".requestChangeVolume(): "

    #@3f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    const-string v4, "Non-local volume playback on system route? "

    #@45
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v3

    #@49
    const-string v4, "Could not request volume change."

    #@4b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v3

    #@4f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v3

    #@53
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    goto :goto_20
.end method

.method routeUpdated()V
    .registers 1

    #@0
    .prologue
    .line 1272
    invoke-static {p0}, Landroid/media/MediaRouter;->updateRoute(Landroid/media/MediaRouter$RouteInfo;)V

    #@3
    .line 1273
    return-void
.end method

.method setStatusCode(I)Z
    .registers 4
    .parameter "statusCode"

    #@0
    .prologue
    .line 1019
    iget v1, p0, Landroid/media/MediaRouter$RouteInfo;->mStatusCode:I

    #@2
    if-eq p1, v1, :cond_2a

    #@4
    .line 1020
    iput p1, p0, Landroid/media/MediaRouter$RouteInfo;->mStatusCode:I

    #@6
    .line 1021
    const/4 v0, 0x0

    #@7
    .line 1022
    .local v0, resId:I
    packed-switch p1, :pswitch_data_2c

    #@a
    .line 1036
    :goto_a
    if-eqz v0, :cond_28

    #@c
    sget-object v1, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@e
    iget-object v1, v1, Landroid/media/MediaRouter$Static;->mResources:Landroid/content/res/Resources;

    #@10
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@13
    move-result-object v1

    #@14
    :goto_14
    iput-object v1, p0, Landroid/media/MediaRouter$RouteInfo;->mStatus:Ljava/lang/CharSequence;

    #@16
    .line 1037
    const/4 v1, 0x1

    #@17
    .line 1039
    .end local v0           #resId:I
    :goto_17
    return v1

    #@18
    .line 1024
    .restart local v0       #resId:I
    :pswitch_18
    const v0, 0x104053f

    #@1b
    .line 1025
    goto :goto_a

    #@1c
    .line 1027
    :pswitch_1c
    const v0, 0x1040540

    #@1f
    .line 1028
    goto :goto_a

    #@20
    .line 1030
    :pswitch_20
    const v0, 0x1040541

    #@23
    .line 1031
    goto :goto_a

    #@24
    .line 1033
    :pswitch_24
    const v0, 0x1040542

    #@27
    goto :goto_a

    #@28
    .line 1036
    :cond_28
    const/4 v1, 0x0

    #@29
    goto :goto_14

    #@2a
    .line 1039
    .end local v0           #resId:I
    :cond_2a
    const/4 v1, 0x0

    #@2b
    goto :goto_17

    #@2c
    .line 1022
    :pswitch_data_2c
    .packed-switch 0x1
        :pswitch_18
        :pswitch_1c
        :pswitch_20
        :pswitch_24
    .end packed-switch
.end method

.method setStatusInt(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "status"

    #@0
    .prologue
    .line 1245
    iget-object v0, p0, Landroid/media/MediaRouter$RouteInfo;->mStatus:Ljava/lang/CharSequence;

    #@2
    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_16

    #@8
    .line 1246
    iput-object p1, p0, Landroid/media/MediaRouter$RouteInfo;->mStatus:Ljava/lang/CharSequence;

    #@a
    .line 1247
    iget-object v0, p0, Landroid/media/MediaRouter$RouteInfo;->mGroup:Landroid/media/MediaRouter$RouteGroup;

    #@c
    if-eqz v0, :cond_13

    #@e
    .line 1248
    iget-object v0, p0, Landroid/media/MediaRouter$RouteInfo;->mGroup:Landroid/media/MediaRouter$RouteGroup;

    #@10
    invoke-virtual {v0, p0, p1}, Landroid/media/MediaRouter$RouteGroup;->memberStatusChanged(Landroid/media/MediaRouter$RouteInfo;Ljava/lang/CharSequence;)V

    #@13
    .line 1250
    :cond_13
    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteInfo;->routeUpdated()V

    #@16
    .line 1252
    :cond_16
    return-void
.end method

.method public setTag(Ljava/lang/Object;)V
    .registers 2
    .parameter "tag"

    #@0
    .prologue
    .line 1092
    iput-object p1, p0, Landroid/media/MediaRouter$RouteInfo;->mTag:Ljava/lang/Object;

    #@2
    .line 1093
    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteInfo;->routeUpdated()V

    #@5
    .line 1094
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 1277
    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteInfo;->getSupportedTypes()I

    #@3
    move-result v1

    #@4
    invoke-static {v1}, Landroid/media/MediaRouter;->typesToString(I)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    .line 1278
    .local v0, supportedTypes:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string/jumbo v2, "{ name="

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteInfo;->getName()Ljava/lang/CharSequence;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    const-string v2, ", status="

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteInfo;->getStatus()Ljava/lang/CharSequence;

    #@31
    move-result-object v2

    #@32
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v1

    #@36
    const-string v2, ", category="

    #@38
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v1

    #@3c
    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteInfo;->getCategory()Landroid/media/MediaRouter$RouteCategory;

    #@3f
    move-result-object v2

    #@40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    const-string v2, ", supportedTypes="

    #@46
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v1

    #@4e
    const-string v2, ", presentationDisplay="

    #@50
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v1

    #@54
    iget-object v2, p0, Landroid/media/MediaRouter$RouteInfo;->mPresentationDisplay:Landroid/view/Display;

    #@56
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v1

    #@5a
    const-string/jumbo v2, "}"

    #@5d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v1

    #@61
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v1

    #@65
    return-object v1
.end method
