.class public Landroid/media/MediaRouter$SimpleCallback;
.super Landroid/media/MediaRouter$Callback;
.source "MediaRouter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/MediaRouter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SimpleCallback"
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 1999
    invoke-direct {p0}, Landroid/media/MediaRouter$Callback;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public onRouteAdded(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;)V
    .registers 3
    .parameter "router"
    .parameter "info"

    #@0
    .prologue
    .line 2011
    return-void
.end method

.method public onRouteChanged(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;)V
    .registers 3
    .parameter "router"
    .parameter "info"

    #@0
    .prologue
    .line 2019
    return-void
.end method

.method public onRouteGrouped(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;Landroid/media/MediaRouter$RouteGroup;I)V
    .registers 5
    .parameter "router"
    .parameter "info"
    .parameter "group"
    .parameter "index"

    #@0
    .prologue
    .line 2024
    return-void
.end method

.method public onRouteRemoved(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;)V
    .registers 3
    .parameter "router"
    .parameter "info"

    #@0
    .prologue
    .line 2015
    return-void
.end method

.method public onRouteSelected(Landroid/media/MediaRouter;ILandroid/media/MediaRouter$RouteInfo;)V
    .registers 4
    .parameter "router"
    .parameter "type"
    .parameter "info"

    #@0
    .prologue
    .line 2003
    return-void
.end method

.method public onRouteUngrouped(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;Landroid/media/MediaRouter$RouteGroup;)V
    .registers 4
    .parameter "router"
    .parameter "info"
    .parameter "group"

    #@0
    .prologue
    .line 2028
    return-void
.end method

.method public onRouteUnselected(Landroid/media/MediaRouter;ILandroid/media/MediaRouter$RouteInfo;)V
    .registers 4
    .parameter "router"
    .parameter "type"
    .parameter "info"

    #@0
    .prologue
    .line 2007
    return-void
.end method

.method public onRouteVolumeChanged(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;)V
    .registers 3
    .parameter "router"
    .parameter "info"

    #@0
    .prologue
    .line 2032
    return-void
.end method
