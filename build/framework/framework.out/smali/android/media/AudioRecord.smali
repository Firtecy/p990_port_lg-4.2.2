.class public Landroid/media/AudioRecord;
.super Ljava/lang/Object;
.source "AudioRecord.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/AudioRecord$NativeEventHandler;,
        Landroid/media/AudioRecord$OnRecordPositionUpdateListener;
    }
.end annotation


# static fields
.field private static final AUDIORECORD_ERROR_SETUP_INVALIDCHANNELMASK:I = -0x11

.field private static final AUDIORECORD_ERROR_SETUP_INVALIDFORMAT:I = -0x12

.field private static final AUDIORECORD_ERROR_SETUP_INVALIDSOURCE:I = -0x13

.field private static final AUDIORECORD_ERROR_SETUP_NATIVEINITFAILED:I = -0x14

.field private static final AUDIORECORD_ERROR_SETUP_ZEROFRAMECOUNT:I = -0x10

.field private static final DIVERSION_FILTERS:[Ljava/lang/String; = null

.field public static final ERROR:I = -0x1

.field public static final ERROR_BAD_VALUE:I = -0x2

.field public static final ERROR_INVALID_OPERATION:I = -0x3

.field private static final NATIVE_EVENT_MARKER:I = 0x2

.field private static final NATIVE_EVENT_NEW_POS:I = 0x3

.field private static final NVA_DATA_PIPE:Ljava/lang/String; = "/data/data/com.lge.pa/app_nva/audio"

.field private static final NVA_DEBUG:Z = false

.field private static final NVA_TAG:Ljava/lang/String; = "NaturalVoiceActivation"

.field public static final RECORDSTATE_RECORDING:I = 0x3

.field public static final RECORDSTATE_STOPPED:I = 0x1

.field public static final STATE_INITIALIZED:I = 0x1

.field public static final STATE_UNINITIALIZED:I = 0x0

.field public static final SUCCESS:I = 0x0

.field private static final TAG:Ljava/lang/String; = "AudioRecord-Java"

.field private static final mMemoryFileSize:I = 0x500000


# instance fields
.field private mAudioFormat:I

.field private mAudioManager:Landroid/media/AudioManager;

.field private mChannelConfiguration:I

.field private mChannelCount:I

.field private mChannels:I

.field private mDiverted:Z

.field private mEventHandler:Landroid/media/AudioRecord$NativeEventHandler;

.field private mF:Ljava/io/File;

.field private mFd:Ljava/io/FileDescriptor;

.field private mFos:Ljava/io/FileOutputStream;

.field private mInitializationLooper:Landroid/os/Looper;

.field private mIsRecordHooking:Z

.field private mMf:Landroid/os/MemoryFile;

.field private mNativeBufferSizeInBytes:I

.field private mNativeCallbackCookie:I

.field private mNativeRecorderInJavaObj:I

.field private mPipe:Ljava/io/FileInputStream;

.field private mPositionListener:Landroid/media/AudioRecord$OnRecordPositionUpdateListener;

.field private final mPositionListenerLock:Ljava/lang/Object;

.field private mReadSize:I

.field private final mRecordHookingLock:Ljava/lang/Object;

.field private mRecordSource:I

.field private mRecordingState:I

.field private final mRecordingStateLock:Ljava/lang/Object;

.field private mSampleRate:I

.field private mSessionId:I

.field private mState:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 75
    const/4 v0, 0x2

    #@1
    new-array v0, v0, [Ljava/lang/String;

    #@3
    const/4 v1, 0x0

    #@4
    const-string v2, "com.google.android.voicesearch"

    #@6
    aput-object v2, v0, v1

    #@8
    const/4 v1, 0x1

    #@9
    const-string v2, "com.google.android.speech.audio"

    #@b
    aput-object v2, v0, v1

    #@d
    sput-object v0, Landroid/media/AudioRecord;->DIVERSION_FILTERS:[Ljava/lang/String;

    #@f
    return-void
.end method

.method public constructor <init>(IIIII)V
    .registers 18
    .parameter "audioSource"
    .parameter "sampleRateInHz"
    .parameter "channelConfig"
    .parameter "audioFormat"
    .parameter "bufferSizeInBytes"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 330
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 81
    const/4 v1, 0x0

    #@4
    iput-boolean v1, p0, Landroid/media/AudioRecord;->mDiverted:Z

    #@6
    .line 82
    const/4 v1, 0x0

    #@7
    iput-object v1, p0, Landroid/media/AudioRecord;->mPipe:Ljava/io/FileInputStream;

    #@9
    .line 232
    const/16 v1, 0x5622

    #@b
    iput v1, p0, Landroid/media/AudioRecord;->mSampleRate:I

    #@d
    .line 236
    const/4 v1, 0x1

    #@e
    iput v1, p0, Landroid/media/AudioRecord;->mChannelCount:I

    #@10
    .line 240
    const/16 v1, 0x10

    #@12
    iput v1, p0, Landroid/media/AudioRecord;->mChannels:I

    #@14
    .line 244
    const/16 v1, 0x10

    #@16
    iput v1, p0, Landroid/media/AudioRecord;->mChannelConfiguration:I

    #@18
    .line 250
    const/4 v1, 0x2

    #@19
    iput v1, p0, Landroid/media/AudioRecord;->mAudioFormat:I

    #@1b
    .line 254
    const/4 v1, 0x0

    #@1c
    iput v1, p0, Landroid/media/AudioRecord;->mRecordSource:I

    #@1e
    .line 258
    const/4 v1, 0x0

    #@1f
    iput v1, p0, Landroid/media/AudioRecord;->mState:I

    #@21
    .line 262
    const/4 v1, 0x1

    #@22
    iput v1, p0, Landroid/media/AudioRecord;->mRecordingState:I

    #@24
    .line 266
    new-instance v1, Ljava/lang/Object;

    #@26
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@29
    iput-object v1, p0, Landroid/media/AudioRecord;->mRecordingStateLock:Ljava/lang/Object;

    #@2b
    .line 273
    const/4 v1, 0x0

    #@2c
    iput-object v1, p0, Landroid/media/AudioRecord;->mPositionListener:Landroid/media/AudioRecord$OnRecordPositionUpdateListener;

    #@2e
    .line 277
    new-instance v1, Ljava/lang/Object;

    #@30
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@33
    iput-object v1, p0, Landroid/media/AudioRecord;->mPositionListenerLock:Ljava/lang/Object;

    #@35
    .line 281
    const/4 v1, 0x0

    #@36
    iput-object v1, p0, Landroid/media/AudioRecord;->mEventHandler:Landroid/media/AudioRecord$NativeEventHandler;

    #@38
    .line 285
    const/4 v1, 0x0

    #@39
    iput-object v1, p0, Landroid/media/AudioRecord;->mInitializationLooper:Landroid/os/Looper;

    #@3b
    .line 289
    const/4 v1, 0x0

    #@3c
    iput v1, p0, Landroid/media/AudioRecord;->mNativeBufferSizeInBytes:I

    #@3e
    .line 293
    const/4 v1, 0x0

    #@3f
    iput v1, p0, Landroid/media/AudioRecord;->mSessionId:I

    #@41
    .line 301
    new-instance v1, Ljava/lang/Object;

    #@43
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@46
    iput-object v1, p0, Landroid/media/AudioRecord;->mRecordHookingLock:Ljava/lang/Object;

    #@48
    .line 1143
    const/4 v1, 0x0

    #@49
    iput-boolean v1, p0, Landroid/media/AudioRecord;->mIsRecordHooking:Z

    #@4b
    .line 1144
    const/4 v1, 0x0

    #@4c
    iput v1, p0, Landroid/media/AudioRecord;->mReadSize:I

    #@4e
    .line 1147
    const/4 v1, 0x0

    #@4f
    iput-object v1, p0, Landroid/media/AudioRecord;->mF:Ljava/io/File;

    #@51
    .line 1148
    const/4 v1, 0x0

    #@52
    iput-object v1, p0, Landroid/media/AudioRecord;->mFos:Ljava/io/FileOutputStream;

    #@54
    .line 332
    invoke-direct {p0}, Landroid/media/AudioRecord;->shouldDivert()Z

    #@57
    move-result v1

    #@58
    iput-boolean v1, p0, Landroid/media/AudioRecord;->mDiverted:Z

    #@5a
    .line 336
    iget-boolean v1, p0, Landroid/media/AudioRecord;->mDiverted:Z

    #@5c
    if-eqz v1, :cond_6b

    #@5e
    invoke-direct {p0}, Landroid/media/AudioRecord;->pipeReadable()Z

    #@61
    move-result v1

    #@62
    if-eqz v1, :cond_6b

    #@64
    .line 337
    const/4 v1, 0x1

    #@65
    iput v1, p0, Landroid/media/AudioRecord;->mState:I

    #@67
    .line 338
    const/4 v1, 0x1

    #@68
    iput v1, p0, Landroid/media/AudioRecord;->mRecordingState:I

    #@6a
    .line 399
    :cond_6a
    :goto_6a
    return-void

    #@6b
    .line 344
    :cond_6b
    const/4 v1, 0x0

    #@6c
    iput-boolean v1, p0, Landroid/media/AudioRecord;->mDiverted:Z

    #@6e
    .line 348
    const/4 v1, 0x0

    #@6f
    iput v1, p0, Landroid/media/AudioRecord;->mState:I

    #@71
    .line 349
    const/4 v1, 0x1

    #@72
    iput v1, p0, Landroid/media/AudioRecord;->mRecordingState:I

    #@74
    .line 352
    const-string v1, "AudioRecord-Java"

    #@76
    new-instance v2, Ljava/lang/StringBuilder;

    #@78
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7b
    const-string v3, "AudioRecord() audioSource "

    #@7d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v2

    #@81
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@84
    move-result-object v2

    #@85
    const-string v3, " sampleRateInHz "

    #@87
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v2

    #@8b
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v2

    #@8f
    const-string v3, " channelConfig "

    #@91
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v2

    #@95
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@98
    move-result-object v2

    #@99
    const-string v3, " audioFormat "

    #@9b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v2

    #@9f
    move/from16 v0, p4

    #@a1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v2

    #@a5
    const-string v3, " bufferSizeInBytes "

    #@a7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v2

    #@ab
    move/from16 v0, p5

    #@ad
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v2

    #@b1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b4
    move-result-object v2

    #@b5
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b8
    .line 354
    const-string/jumbo v1, "persist.sys.voice_state"

    #@bb
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@be
    move-result-object v1

    #@bf
    const-string/jumbo v2, "recording"

    #@c2
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c5
    move-result v1

    #@c6
    if-eqz v1, :cond_e1

    #@c8
    .line 356
    :try_start_c8
    invoke-static {}, Landroid/media/AudioManager;->getAudioService()Landroid/media/IAudioService;

    #@cb
    move-result-object v9

    #@cc
    .line 357
    .local v9, audioService:Landroid/media/IAudioService;
    if-eqz v9, :cond_138

    #@ce
    .line 358
    const-string v1, "AudioRecord-Java"

    #@d0
    const-string v2, "AudioRecord() voiceActivation will be start."

    #@d2
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d5
    .line 359
    const/4 v1, 0x1

    #@d6
    invoke-interface {v9, v1}, Landroid/media/IAudioService;->sendBroadcastRecordState(I)V

    #@d9
    .line 360
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@dc
    const-wide/16 v1, 0x1f4

    #@de
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_e1
    .catch Ljava/lang/Exception; {:try_start_c8 .. :try_end_e1} :catch_140

    #@e1
    .line 371
    .end local v9           #audioService:Landroid/media/IAudioService;
    :cond_e1
    :goto_e1
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@e4
    move-result-object v1

    #@e5
    iput-object v1, p0, Landroid/media/AudioRecord;->mInitializationLooper:Landroid/os/Looper;

    #@e7
    if-nez v1, :cond_ef

    #@e9
    .line 372
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    #@ec
    move-result-object v1

    #@ed
    iput-object v1, p0, Landroid/media/AudioRecord;->mInitializationLooper:Landroid/os/Looper;

    #@ef
    .line 375
    :cond_ef
    invoke-direct/range {p0 .. p4}, Landroid/media/AudioRecord;->audioParamCheck(IIII)V

    #@f2
    .line 377
    move/from16 v0, p5

    #@f4
    invoke-direct {p0, v0}, Landroid/media/AudioRecord;->audioBuffSizeCheck(I)V

    #@f7
    .line 379
    invoke-direct {p0, p1, p2}, Landroid/media/AudioRecord;->initRecordHooking(II)Z

    #@fa
    move-result v1

    #@fb
    if-nez v1, :cond_6a

    #@fd
    .line 384
    const/4 v1, 0x1

    #@fe
    new-array v8, v1, [I

    #@100
    .line 385
    .local v8, session:[I
    const/4 v1, 0x0

    #@101
    const/4 v2, 0x0

    #@102
    aput v2, v8, v1

    #@104
    .line 388
    new-instance v2, Ljava/lang/ref/WeakReference;

    #@106
    invoke-direct {v2, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@109
    iget v3, p0, Landroid/media/AudioRecord;->mRecordSource:I

    #@10b
    iget v4, p0, Landroid/media/AudioRecord;->mSampleRate:I

    #@10d
    iget v5, p0, Landroid/media/AudioRecord;->mChannels:I

    #@10f
    iget v6, p0, Landroid/media/AudioRecord;->mAudioFormat:I

    #@111
    iget v7, p0, Landroid/media/AudioRecord;->mNativeBufferSizeInBytes:I

    #@113
    move-object v1, p0

    #@114
    invoke-direct/range {v1 .. v8}, Landroid/media/AudioRecord;->native_setup(Ljava/lang/Object;IIIII[I)I

    #@117
    move-result v11

    #@118
    .line 391
    .local v11, initResult:I
    if-eqz v11, :cond_149

    #@11a
    .line 392
    new-instance v1, Ljava/lang/StringBuilder;

    #@11c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11f
    const-string v2, "Error code "

    #@121
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@124
    move-result-object v1

    #@125
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@128
    move-result-object v1

    #@129
    const-string v2, " when initializing native AudioRecord object."

    #@12b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12e
    move-result-object v1

    #@12f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@132
    move-result-object v1

    #@133
    invoke-static {v1}, Landroid/media/AudioRecord;->loge(Ljava/lang/String;)V

    #@136
    goto/16 :goto_6a

    #@138
    .line 362
    .end local v8           #session:[I
    .end local v11           #initResult:I
    .restart local v9       #audioService:Landroid/media/IAudioService;
    :cond_138
    :try_start_138
    const-string v1, "AudioRecord-Java"

    #@13a
    const-string v2, "AudioRecord() audioService is null."

    #@13c
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_13f
    .catch Ljava/lang/Exception; {:try_start_138 .. :try_end_13f} :catch_140

    #@13f
    goto :goto_e1

    #@140
    .line 364
    .end local v9           #audioService:Landroid/media/IAudioService;
    :catch_140
    move-exception v10

    #@141
    .line 365
    .local v10, e:Ljava/lang/Exception;
    const-string v1, "AudioRecord-Java"

    #@143
    const-string v2, "AudioRecord() "

    #@145
    invoke-static {v1, v2, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@148
    goto :goto_e1

    #@149
    .line 396
    .end local v10           #e:Ljava/lang/Exception;
    .restart local v8       #session:[I
    .restart local v11       #initResult:I
    :cond_149
    const/4 v1, 0x0

    #@14a
    aget v1, v8, v1

    #@14c
    iput v1, p0, Landroid/media/AudioRecord;->mSessionId:I

    #@14e
    .line 398
    const/4 v1, 0x1

    #@14f
    iput v1, p0, Landroid/media/AudioRecord;->mState:I

    #@151
    goto/16 :goto_6a
.end method

.method static synthetic access$000(Landroid/media/AudioRecord;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Landroid/media/AudioRecord;->mPositionListenerLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/media/AudioRecord;)Landroid/media/AudioRecord$OnRecordPositionUpdateListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Landroid/media/AudioRecord;->mPositionListener:Landroid/media/AudioRecord$OnRecordPositionUpdateListener;

    #@2
    return-object v0
.end method

.method private audioBuffSizeCheck(I)V
    .registers 6
    .parameter "audioBufferSize"

    #@0
    .prologue
    .line 491
    iget v2, p0, Landroid/media/AudioRecord;->mAudioFormat:I

    #@2
    const/4 v3, 0x3

    #@3
    if-ne v2, v3, :cond_19

    #@5
    .line 492
    const/4 v0, 0x1

    #@6
    .line 498
    .local v0, bytesPerSample:I
    :goto_6
    iget v2, p0, Landroid/media/AudioRecord;->mChannelCount:I

    #@8
    mul-int v1, v2, v0

    #@a
    .line 499
    .local v1, frameSizeInBytes:I
    rem-int v2, p1, v1

    #@c
    if-nez v2, :cond_11

    #@e
    const/4 v2, 0x1

    #@f
    if-ge p1, v2, :cond_29

    #@11
    .line 500
    :cond_11
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@13
    const-string v3, "Invalid audio buffer size."

    #@15
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@18
    throw v2

    #@19
    .line 493
    .end local v0           #bytesPerSample:I
    .end local v1           #frameSizeInBytes:I
    :cond_19
    iget v2, p0, Landroid/media/AudioRecord;->mAudioFormat:I

    #@1b
    const/16 v3, 0x65

    #@1d
    if-ne v2, v3, :cond_27

    #@1f
    iget v2, p0, Landroid/media/AudioRecord;->mRecordSource:I

    #@21
    const/4 v3, 0x7

    #@22
    if-eq v2, v3, :cond_27

    #@24
    .line 495
    const/16 v0, 0x3d

    #@26
    .restart local v0       #bytesPerSample:I
    goto :goto_6

    #@27
    .line 497
    .end local v0           #bytesPerSample:I
    :cond_27
    const/4 v0, 0x2

    #@28
    .restart local v0       #bytesPerSample:I
    goto :goto_6

    #@29
    .line 503
    .restart local v1       #frameSizeInBytes:I
    :cond_29
    iput p1, p0, Landroid/media/AudioRecord;->mNativeBufferSizeInBytes:I

    #@2b
    .line 504
    return-void
.end method

.method private audioParamCheck(IIII)V
    .registers 8
    .parameter "audioSource"
    .parameter "sampleRateInHz"
    .parameter "channelConfig"
    .parameter "audioFormat"

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    const/4 v1, 0x0

    #@2
    .line 415
    if-ltz p1, :cond_a

    #@4
    invoke-static {}, Landroid/media/MediaRecorder;->getAudioSourceMax()I

    #@7
    move-result v0

    #@8
    if-le p1, v0, :cond_12

    #@a
    .line 417
    :cond_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@c
    const-string v1, "Invalid audio source."

    #@e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0

    #@12
    .line 419
    :cond_12
    iput p1, p0, Landroid/media/AudioRecord;->mRecordSource:I

    #@14
    .line 424
    const/16 v0, 0xfa0

    #@16
    if-lt p2, v0, :cond_1d

    #@18
    const v0, 0xbb80

    #@1b
    if-le p2, v0, :cond_36

    #@1d
    .line 425
    :cond_1d
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1f
    new-instance v1, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    const-string v2, "Hz is not a supported sample rate."

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v1

    #@32
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@35
    throw v0

    #@36
    .line 428
    :cond_36
    iput p2, p0, Landroid/media/AudioRecord;->mSampleRate:I

    #@38
    .line 433
    iput p3, p0, Landroid/media/AudioRecord;->mChannelConfiguration:I

    #@3a
    .line 435
    sparse-switch p3, :sswitch_data_74

    #@3d
    .line 452
    iput v1, p0, Landroid/media/AudioRecord;->mChannelCount:I

    #@3f
    .line 453
    iput v1, p0, Landroid/media/AudioRecord;->mChannels:I

    #@41
    .line 454
    iput v1, p0, Landroid/media/AudioRecord;->mChannelConfiguration:I

    #@43
    .line 455
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@45
    const-string v1, "Unsupported channel configuration."

    #@47
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@4a
    throw v0

    #@4b
    .line 439
    :sswitch_4b
    const/4 v0, 0x1

    #@4c
    iput v0, p0, Landroid/media/AudioRecord;->mChannelCount:I

    #@4e
    .line 440
    const/16 v0, 0x10

    #@50
    iput v0, p0, Landroid/media/AudioRecord;->mChannels:I

    #@52
    .line 460
    :goto_52
    sparse-switch p4, :sswitch_data_8e

    #@55
    .line 474
    iput v1, p0, Landroid/media/AudioRecord;->mAudioFormat:I

    #@57
    .line 475
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@59
    const-string v1, "Unsupported sample encoding. Should be ENCODING_PCM_8BIT or ENCODING_PCM_16BIT."

    #@5b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@5e
    throw v0

    #@5f
    .line 444
    :sswitch_5f
    iput v2, p0, Landroid/media/AudioRecord;->mChannelCount:I

    #@61
    .line 445
    const/16 v0, 0xc

    #@63
    iput v0, p0, Landroid/media/AudioRecord;->mChannels:I

    #@65
    goto :goto_52

    #@66
    .line 448
    :sswitch_66
    const/4 v0, 0x6

    #@67
    iput v0, p0, Landroid/media/AudioRecord;->mChannelCount:I

    #@69
    .line 449
    const/high16 v0, 0x3f

    #@6b
    iput v0, p0, Landroid/media/AudioRecord;->mChannels:I

    #@6d
    goto :goto_52

    #@6e
    .line 462
    :sswitch_6e
    iput v2, p0, Landroid/media/AudioRecord;->mAudioFormat:I

    #@70
    .line 478
    :goto_70
    return-void

    #@71
    .line 471
    :sswitch_71
    iput p4, p0, Landroid/media/AudioRecord;->mAudioFormat:I

    #@73
    goto :goto_70

    #@74
    .line 435
    :sswitch_data_74
    .sparse-switch
        0x1 -> :sswitch_4b
        0x2 -> :sswitch_4b
        0x3 -> :sswitch_5f
        0xc -> :sswitch_5f
        0x10 -> :sswitch_4b
        0x3f0000 -> :sswitch_66
    .end sparse-switch

    #@8e
    .line 460
    :sswitch_data_8e
    .sparse-switch
        0x1 -> :sswitch_6e
        0x2 -> :sswitch_71
        0x3 -> :sswitch_71
        0x64 -> :sswitch_71
        0x65 -> :sswitch_71
        0x66 -> :sswitch_71
        0x67 -> :sswitch_71
        0x68 -> :sswitch_71
    .end sparse-switch
.end method

.method private checkRecordHookingCondition(II)Z
    .registers 9
    .parameter "audioSource"
    .parameter "sampleRateInHz"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1289
    const/4 v2, 0x0

    #@2
    .line 1290
    .local v2, state:I
    invoke-static {}, Landroid/media/AudioManager;->getAudioService()Landroid/media/IAudioService;

    #@5
    move-result-object v1

    #@6
    .line 1293
    .local v1, service:Landroid/media/IAudioService;
    if-eqz v1, :cond_37

    #@8
    .line 1294
    :try_start_8
    invoke-interface {v1}, Landroid/media/IAudioService;->getRecordHookingState()I

    #@b
    move-result v2

    #@c
    .line 1295
    const/4 v4, 0x2

    #@d
    if-eq v2, v4, :cond_17

    #@f
    .line 1296
    const-string v4, "AudioRecord-Java"

    #@11
    const-string v5, "checkRecordHookingCondition() RecordHooking is not started."

    #@13
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 1312
    :goto_16
    return v3

    #@17
    .line 1298
    :cond_17
    const/4 v4, 0x6

    #@18
    if-eq p1, v4, :cond_2b

    #@1a
    .line 1299
    const-string v4, "AudioRecord-Java"

    #@1c
    const-string v5, "checkRecordHookingCondition() audioSource is not recognition."

    #@1e
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_21
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_21} :catch_22

    #@21
    goto :goto_16

    #@22
    .line 1308
    :catch_22
    move-exception v0

    #@23
    .line 1309
    .local v0, e:Landroid/os/RemoteException;
    const-string v4, "AudioRecord-Java"

    #@25
    const-string v5, "checkRecordHookingCondition() Dead object in checkRecordHookingCondition"

    #@27
    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2a
    goto :goto_16

    #@2b
    .line 1301
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_2b
    const/16 v4, 0x1f40

    #@2d
    if-eq p2, v4, :cond_3e

    #@2f
    .line 1302
    :try_start_2f
    const-string v4, "AudioRecord-Java"

    #@31
    const-string v5, "checkRecordHookingCondition() sampleRateInHz is not LGE voice recognition."

    #@33
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    goto :goto_16

    #@37
    .line 1306
    :cond_37
    const-string v4, "AudioRecord-Java"

    #@39
    const-string v5, "checkRecordHookingCondition() AudioRecord() service is null."

    #@3b
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3e
    .catch Landroid/os/RemoteException; {:try_start_2f .. :try_end_3e} :catch_22

    #@3e
    .line 1312
    :cond_3e
    const/4 v3, 0x1

    #@3f
    goto :goto_16
.end method

.method public static getMinBufferSize(III)I
    .registers 8
    .parameter "sampleRateInHz"
    .parameter "channelConfig"
    .parameter "audioFormat"

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    const/4 v2, -0x2

    #@2
    .line 641
    const/4 v0, 0x0

    #@3
    .line 642
    .local v0, channelCount:I
    sparse-switch p1, :sswitch_data_3c

    #@6
    .line 657
    const-string v3, "getMinBufferSize(): Invalid channel configuration."

    #@8
    invoke-static {v3}, Landroid/media/AudioRecord;->loge(Ljava/lang/String;)V

    #@b
    move v1, v2

    #@c
    .line 680
    :cond_c
    :goto_c
    return v1

    #@d
    .line 646
    :sswitch_d
    const/4 v0, 0x1

    #@e
    .line 662
    :goto_e
    const/4 v4, 0x2

    #@f
    if-eq p2, v4, :cond_30

    #@11
    const/16 v4, 0x64

    #@13
    if-eq p2, v4, :cond_30

    #@15
    const/16 v4, 0x65

    #@17
    if-eq p2, v4, :cond_30

    #@19
    const/16 v4, 0x66

    #@1b
    if-eq p2, v4, :cond_30

    #@1d
    const/16 v4, 0x67

    #@1f
    if-eq p2, v4, :cond_30

    #@21
    const/16 v4, 0x68

    #@23
    if-eq p2, v4, :cond_30

    #@25
    .line 668
    const-string v3, "getMinBufferSize(): Invalid audio format."

    #@27
    invoke-static {v3}, Landroid/media/AudioRecord;->loge(Ljava/lang/String;)V

    #@2a
    move v1, v2

    #@2b
    .line 669
    goto :goto_c

    #@2c
    .line 650
    :sswitch_2c
    const/4 v0, 0x2

    #@2d
    .line 651
    goto :goto_e

    #@2e
    .line 653
    :sswitch_2e
    const/4 v0, 0x6

    #@2f
    .line 654
    goto :goto_e

    #@30
    .line 672
    :cond_30
    invoke-static {p0, v0, p2}, Landroid/media/AudioRecord;->native_get_min_buff_size(III)I

    #@33
    move-result v1

    #@34
    .line 673
    .local v1, size:I
    if-nez v1, :cond_38

    #@36
    move v1, v2

    #@37
    .line 674
    goto :goto_c

    #@38
    .line 676
    :cond_38
    if-ne v1, v3, :cond_c

    #@3a
    move v1, v3

    #@3b
    .line 677
    goto :goto_c

    #@3c
    .line 642
    :sswitch_data_3c
    .sparse-switch
        0x1 -> :sswitch_d
        0x2 -> :sswitch_d
        0x3 -> :sswitch_2c
        0xc -> :sswitch_2c
        0x10 -> :sswitch_d
        0x3f0000 -> :sswitch_2e
    .end sparse-switch
.end method

.method private initRecordHooking(II)Z
    .registers 13
    .parameter "audioSource"
    .parameter "sampleRateInHz"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 1152
    invoke-direct {p0, p1, p2}, Landroid/media/AudioRecord;->checkRecordHookingCondition(II)Z

    #@5
    move-result v6

    #@6
    if-nez v6, :cond_9

    #@8
    .line 1189
    :goto_8
    return v4

    #@9
    .line 1156
    :cond_9
    const/4 v6, 0x0

    #@a
    const/4 v7, 0x0

    #@b
    :try_start_b
    invoke-static {v6, v7}, Landroid/media/AudioSystem;->getHeapFD(II)Ljava/io/FileDescriptor;

    #@e
    move-result-object v6

    #@f
    iput-object v6, p0, Landroid/media/AudioRecord;->mFd:Ljava/io/FileDescriptor;

    #@11
    .line 1157
    new-instance v6, Landroid/os/MemoryFile;

    #@13
    iget-object v7, p0, Landroid/media/AudioRecord;->mFd:Ljava/io/FileDescriptor;

    #@15
    const/high16 v8, 0x50

    #@17
    const/4 v9, 0x0

    #@18
    invoke-direct {v6, v7, v8, v9}, Landroid/os/MemoryFile;-><init>(Ljava/io/FileDescriptor;II)V

    #@1b
    iput-object v6, p0, Landroid/media/AudioRecord;->mMf:Landroid/os/MemoryFile;
    :try_end_1d
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_1d} :catch_51

    #@1d
    .line 1172
    iput-boolean v5, p0, Landroid/media/AudioRecord;->mIsRecordHooking:Z

    #@1f
    .line 1173
    iput v5, p0, Landroid/media/AudioRecord;->mState:I

    #@21
    .line 1174
    const-string v6, "AudioRecHook_HookingSize"

    #@23
    invoke-static {v6}, Landroid/media/AudioSystem;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    #@26
    move-result-object v6

    #@27
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@2a
    move-result v6

    #@2b
    iput v6, p0, Landroid/media/AudioRecord;->mReadSize:I

    #@2d
    .line 1176
    const-string v6, "Audio record hooking is initialized!!"

    #@2f
    invoke-static {v6}, Landroid/media/AudioRecord;->logd(Ljava/lang/String;)V

    #@32
    .line 1179
    const-string/jumbo v6, "persist.sys.record_dump"

    #@35
    invoke-static {v6, v4}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@38
    move-result v4

    #@39
    if-eqz v4, :cond_4f

    #@3b
    .line 1180
    new-instance v2, Ljava/io/File;

    #@3d
    const-string v4, "/data/"

    #@3f
    const-string v6, "dump_audio_record-java.pcm"

    #@41
    invoke-direct {v2, v4, v6}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@44
    .line 1182
    .local v2, mF:Ljava/io/File;
    :try_start_44
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    #@47
    .line 1183
    new-instance v4, Ljava/io/FileOutputStream;

    #@49
    const/4 v6, 0x1

    #@4a
    invoke-direct {v4, v2, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    #@4d
    iput-object v4, p0, Landroid/media/AudioRecord;->mFos:Ljava/io/FileOutputStream;
    :try_end_4f
    .catch Ljava/lang/Exception; {:try_start_44 .. :try_end_4f} :catch_85

    #@4f
    .end local v2           #mF:Ljava/io/File;
    :cond_4f
    :goto_4f
    move v4, v5

    #@50
    .line 1189
    goto :goto_8

    #@51
    .line 1158
    :catch_51
    move-exception v0

    #@52
    .line 1159
    .local v0, e:Ljava/lang/Exception;
    new-instance v5, Ljava/lang/StringBuilder;

    #@54
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@57
    const-string v6, "initRecordHooking() Exception = "

    #@59
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v5

    #@5d
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v5

    #@61
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v5

    #@65
    invoke-static {v5}, Landroid/media/AudioRecord;->loge(Ljava/lang/String;)V

    #@68
    .line 1160
    invoke-static {}, Landroid/media/AudioManager;->getAudioService()Landroid/media/IAudioService;

    #@6b
    move-result-object v3

    #@6c
    .line 1162
    .local v3, service:Landroid/media/IAudioService;
    if-eqz v3, :cond_7d

    #@6e
    .line 1163
    const/4 v5, 0x0

    #@6f
    const/4 v6, 0x0

    #@70
    :try_start_70
    invoke-interface {v3, v5, v6}, Landroid/media/IAudioService;->setRecordHookingState(II)Z
    :try_end_73
    .catch Ljava/lang/Exception; {:try_start_70 .. :try_end_73} :catch_74

    #@73
    goto :goto_8

    #@74
    .line 1167
    :catch_74
    move-exception v1

    #@75
    .line 1168
    .local v1, ex:Ljava/lang/Exception;
    const-string v5, "AudioRecord-Java"

    #@77
    const-string v6, "initRecordHooking() Dead object in initRecordHooking"

    #@79
    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@7c
    goto :goto_8

    #@7d
    .line 1165
    .end local v1           #ex:Ljava/lang/Exception;
    :cond_7d
    :try_start_7d
    const-string v5, "AudioRecord-Java"

    #@7f
    const-string v6, "initRecordHooking() AudioRecord() service is null."

    #@81
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_84
    .catch Ljava/lang/Exception; {:try_start_7d .. :try_end_84} :catch_74

    #@84
    goto :goto_8

    #@85
    .line 1184
    .end local v0           #e:Ljava/lang/Exception;
    .end local v3           #service:Landroid/media/IAudioService;
    .restart local v2       #mF:Ljava/io/File;
    :catch_85
    move-exception v0

    #@86
    .line 1185
    .restart local v0       #e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@89
    goto :goto_4f
.end method

.method private static logd(Ljava/lang/String;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 1130
    const-string v0, "AudioRecord-Java"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[ android.media.AudioRecord ] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1131
    return-void
.end method

.method private static loge(Ljava/lang/String;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 1134
    const-string v0, "AudioRecord-Java"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[ android.media.AudioRecord ] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1135
    return-void
.end method

.method private final native native_finalize()V
.end method

.method private final native native_get_marker_pos()I
.end method

.method private static final native native_get_min_buff_size(III)I
.end method

.method private final native native_get_pos_update_period()I
.end method

.method private final native native_read_in_byte_array([BII)I
.end method

.method private final native native_read_in_direct_buffer(Ljava/lang/Object;I)I
.end method

.method private final native native_read_in_short_array([SII)I
.end method

.method private final native native_release()V
.end method

.method private final native native_set_marker_pos(I)I
.end method

.method private final native native_set_pos_update_period(I)I
.end method

.method private final native native_setup(Ljava/lang/Object;IIIII[I)I
.end method

.method private final native native_start(II)I
.end method

.method private final native native_stop()V
.end method

.method private pipeClose()V
    .registers 3

    #@0
    .prologue
    .line 129
    :try_start_0
    iget-object v1, p0, Landroid/media/AudioRecord;->mPipe:Ljava/io/FileInputStream;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 130
    iget-object v1, p0, Landroid/media/AudioRecord;->mPipe:Ljava/io/FileInputStream;

    #@6
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_9} :catch_d

    #@9
    .line 134
    :cond_9
    :goto_9
    const/4 v1, 0x0

    #@a
    iput-object v1, p0, Landroid/media/AudioRecord;->mPipe:Ljava/io/FileInputStream;

    #@c
    .line 135
    return-void

    #@d
    .line 131
    :catch_d
    move-exception v0

    #@e
    .line 132
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@11
    goto :goto_9
.end method

.method private pipeOpen()Z
    .registers 4

    #@0
    .prologue
    .line 106
    iget-object v1, p0, Landroid/media/AudioRecord;->mPipe:Ljava/io/FileInputStream;

    #@2
    if-eqz v1, :cond_7

    #@4
    .line 110
    invoke-direct {p0}, Landroid/media/AudioRecord;->pipeClose()V

    #@7
    .line 113
    :cond_7
    :try_start_7
    new-instance v1, Ljava/io/FileInputStream;

    #@9
    const-string v2, "/data/data/com.lge.pa/app_nva/audio"

    #@b
    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    #@e
    iput-object v1, p0, Landroid/media/AudioRecord;->mPipe:Ljava/io/FileInputStream;
    :try_end_10
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_10} :catch_12

    #@10
    .line 122
    const/4 v1, 0x1

    #@11
    :goto_11
    return v1

    #@12
    .line 117
    :catch_12
    move-exception v0

    #@13
    .line 118
    .local v0, e:Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    #@16
    .line 119
    const/4 v1, 0x0

    #@17
    iput-object v1, p0, Landroid/media/AudioRecord;->mPipe:Ljava/io/FileInputStream;

    #@19
    .line 120
    const/4 v1, 0x0

    #@1a
    goto :goto_11
.end method

.method private pipeReadable()Z
    .registers 3

    #@0
    .prologue
    .line 100
    new-instance v0, Ljava/io/File;

    #@2
    const-string v1, "/data/data/com.lge.pa/app_nva/audio"

    #@4
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@7
    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    #@a
    move-result v0

    #@b
    return v0
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .registers 8
    .parameter "audiorecord_ref"
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"
    .parameter "obj"

    #@0
    .prologue
    .line 1077
    check-cast p0, Ljava/lang/ref/WeakReference;

    #@2
    .end local p0
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Landroid/media/AudioRecord;

    #@8
    .line 1078
    .local v1, recorder:Landroid/media/AudioRecord;
    if-nez v1, :cond_b

    #@a
    .line 1088
    :cond_a
    :goto_a
    return-void

    #@b
    .line 1082
    :cond_b
    iget-object v2, v1, Landroid/media/AudioRecord;->mEventHandler:Landroid/media/AudioRecord$NativeEventHandler;

    #@d
    if-eqz v2, :cond_a

    #@f
    .line 1083
    iget-object v2, v1, Landroid/media/AudioRecord;->mEventHandler:Landroid/media/AudioRecord$NativeEventHandler;

    #@11
    invoke-virtual {v2, p1, p2, p3, p4}, Landroid/media/AudioRecord$NativeEventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@14
    move-result-object v0

    #@15
    .line 1085
    .local v0, m:Landroid/os/Message;
    iget-object v2, v1, Landroid/media/AudioRecord;->mEventHandler:Landroid/media/AudioRecord$NativeEventHandler;

    #@17
    invoke-virtual {v2, v0}, Landroid/media/AudioRecord$NativeEventHandler;->sendMessage(Landroid/os/Message;)Z

    #@1a
    goto :goto_a
.end method

.method private read([BIII)I
    .registers 16
    .parameter "audioData"
    .parameter "offsetInBytes"
    .parameter "sizeInBytes"
    .parameter "flag"

    #@0
    .prologue
    .line 1193
    const/4 v4, 0x0

    #@1
    .line 1194
    .local v4, ret:I
    iget-object v8, p0, Landroid/media/AudioRecord;->mRecordHookingLock:Ljava/lang/Object;

    #@3
    monitor-enter v8

    #@4
    .line 1195
    :try_start_4
    iget v7, p0, Landroid/media/AudioRecord;->mState:I

    #@6
    const/4 v9, 0x1

    #@7
    if-eq v7, v9, :cond_c

    #@9
    .line 1196
    const/4 v7, -0x3

    #@a
    monitor-exit v8

    #@b
    .line 1247
    :goto_b
    return v7

    #@c
    .line 1199
    :cond_c
    if-eqz p1, :cond_17

    #@e
    if-ltz p2, :cond_17

    #@10
    if-ltz p3, :cond_17

    #@12
    add-int v7, p2, p3

    #@14
    array-length v9, p1

    #@15
    if-le v7, v9, :cond_1d

    #@17
    .line 1201
    :cond_17
    const/4 v7, -0x2

    #@18
    monitor-exit v8

    #@19
    goto :goto_b

    #@1a
    .line 1233
    :catchall_1a
    move-exception v7

    #@1b
    monitor-exit v8
    :try_end_1c
    .catchall {:try_start_4 .. :try_end_1c} :catchall_1a

    #@1c
    throw v7

    #@1d
    .line 1204
    :cond_1d
    :try_start_1d
    iget v7, p0, Landroid/media/AudioRecord;->mReadSize:I

    #@1f
    add-int/2addr v7, p3

    #@20
    const/high16 v9, 0x50

    #@22
    if-le v7, v9, :cond_4f

    #@24
    .line 1205
    new-instance v7, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v9, "end of memory file. sizeInByte set "

    #@2b
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v7

    #@2f
    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v7

    #@33
    const-string v9, " to "

    #@35
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v7

    #@39
    const/high16 v9, 0x50

    #@3b
    iget v10, p0, Landroid/media/AudioRecord;->mReadSize:I

    #@3d
    rem-int/2addr v9, v10

    #@3e
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v7

    #@42
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v7

    #@46
    invoke-static {v7}, Landroid/media/AudioRecord;->logd(Ljava/lang/String;)V

    #@49
    .line 1206
    const/high16 v7, 0x50

    #@4b
    iget v9, p0, Landroid/media/AudioRecord;->mReadSize:I

    #@4d
    rem-int p3, v7, v9

    #@4f
    .line 1209
    :cond_4f
    const-string v7, "AudioRecHook_TotalHookingSize"

    #@51
    invoke-static {v7}, Landroid/media/AudioSystem;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    #@54
    move-result-object v7

    #@55
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@58
    move-result v6

    #@59
    .line 1210
    .local v6, totalReadSize:I
    const/4 v5, 0x0

    #@5a
    .line 1211
    .local v5, totalDumpSize:I
    const/4 v2, 0x0

    #@5b
    .local v2, i:I
    :goto_5b
    const/4 v7, 0x5

    #@5c
    if-ge v2, v7, :cond_e2

    #@5e
    .line 1212
    const-string v7, "AudioRecHook_ReadSize"

    #@60
    invoke-static {v7}, Landroid/media/AudioSystem;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    #@63
    move-result-object v7

    #@64
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_67
    .catchall {:try_start_1d .. :try_end_67} :catchall_1a

    #@67
    move-result v5

    #@68
    .line 1213
    add-int v7, v6, p3

    #@6a
    if-le v5, v7, :cond_12d

    #@6c
    .line 1215
    :try_start_6c
    iget-object v7, p0, Landroid/media/AudioRecord;->mMf:Landroid/os/MemoryFile;

    #@6e
    iget v9, p0, Landroid/media/AudioRecord;->mReadSize:I

    #@70
    invoke-virtual {v7, p1, v9, p2, p3}, Landroid/os/MemoryFile;->readBytes([BIII)I

    #@73
    move-result v4

    #@74
    .line 1216
    iget v7, p0, Landroid/media/AudioRecord;->mReadSize:I

    #@76
    add-int/2addr v7, v4

    #@77
    iput v7, p0, Landroid/media/AudioRecord;->mReadSize:I

    #@79
    .line 1217
    new-instance v7, Ljava/lang/StringBuilder;

    #@7b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@7e
    const-string/jumbo v9, "read hooking data. audioData.length = "

    #@81
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v7

    #@85
    array-length v9, p1

    #@86
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@89
    move-result-object v7

    #@8a
    const-string v9, " offsetInBytes = "

    #@8c
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v7

    #@90
    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@93
    move-result-object v7

    #@94
    const-string v9, " sizeInBytes = "

    #@96
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v7

    #@9a
    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v7

    #@9e
    const-string v9, " ret = "

    #@a0
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v7

    #@a4
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v7

    #@a8
    const-string v9, " mReadSize = "

    #@aa
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v7

    #@ae
    iget v9, p0, Landroid/media/AudioRecord;->mReadSize:I

    #@b0
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v7

    #@b4
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b7
    move-result-object v7

    #@b8
    invoke-static {v7}, Landroid/media/AudioRecord;->logd(Ljava/lang/String;)V

    #@bb
    .line 1218
    new-instance v7, Ljava/lang/StringBuilder;

    #@bd
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@c0
    const-string v9, "AudioRecHook_HookingSize="

    #@c2
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v7

    #@c6
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v7

    #@ca
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cd
    move-result-object v7

    #@ce
    invoke-static {v7}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    #@d1
    .line 1219
    iget v7, p0, Landroid/media/AudioRecord;->mReadSize:I

    #@d3
    const/high16 v9, 0x50

    #@d5
    if-ne v7, v9, :cond_da

    #@d7
    .line 1220
    const/4 v7, 0x0

    #@d8
    iput v7, p0, Landroid/media/AudioRecord;->mReadSize:I

    #@da
    .line 1221
    :cond_da
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@dd
    const-wide/16 v9, 0xa

    #@df
    invoke-static {v9, v10}, Ljava/lang/Thread;->sleep(J)V
    :try_end_e2
    .catchall {:try_start_6c .. :try_end_e2} :catchall_1a
    .catch Ljava/lang/Exception; {:try_start_6c .. :try_end_e2} :catch_112

    #@e2
    .line 1233
    :cond_e2
    :goto_e2
    :try_start_e2
    monitor-exit v8
    :try_end_e3
    .catchall {:try_start_e2 .. :try_end_e3} :catchall_1a

    #@e3
    .line 1235
    const-string/jumbo v7, "persist.sys.record_dump"

    #@e6
    const/4 v8, 0x0

    #@e7
    invoke-static {v7, v8}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@ea
    move-result v7

    #@eb
    if-eqz v7, :cond_10f

    #@ed
    .line 1237
    :try_start_ed
    iget-object v7, p0, Landroid/media/AudioRecord;->mFos:Ljava/io/FileOutputStream;

    #@ef
    invoke-virtual {v7, p1, p2, p3}, Ljava/io/FileOutputStream;->write([BII)V

    #@f2
    .line 1238
    new-instance v7, Ljava/lang/StringBuilder;

    #@f4
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@f7
    const-string/jumbo v8, "read() save to /data/dump_audio_record-java.pcm, (size:"

    #@fa
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fd
    move-result-object v7

    #@fe
    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@101
    move-result-object v7

    #@102
    const-string v8, ")"

    #@104
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@107
    move-result-object v7

    #@108
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10b
    move-result-object v7

    #@10c
    invoke-static {v7}, Landroid/media/AudioRecord;->logd(Ljava/lang/String;)V
    :try_end_10f
    .catch Ljava/io/FileNotFoundException; {:try_start_ed .. :try_end_10f} :catch_14f
    .catch Ljava/io/IOException; {:try_start_ed .. :try_end_10f} :catch_167

    #@10f
    :cond_10f
    :goto_10f
    move v7, v4

    #@110
    .line 1247
    goto/16 :goto_b

    #@112
    .line 1222
    :catch_112
    move-exception v0

    #@113
    .line 1223
    .local v0, e:Ljava/lang/Exception;
    :try_start_113
    const-string v7, "AudioRecord-Java"

    #@115
    new-instance v9, Ljava/lang/StringBuilder;

    #@117
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@11a
    const-string/jumbo v10, "read fail Exception = "

    #@11d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@120
    move-result-object v9

    #@121
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@124
    move-result-object v9

    #@125
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@128
    move-result-object v9

    #@129
    invoke-static {v7, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@12c
    goto :goto_e2

    #@12d
    .line 1227
    .end local v0           #e:Ljava/lang/Exception;
    :cond_12d
    new-instance v7, Ljava/lang/StringBuilder;

    #@12f
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@132
    const-string v9, "Hooking size(From AudioFlinger) is not enough. Waitting for 20msec. retry = "

    #@134
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@137
    move-result-object v7

    #@138
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13b
    move-result-object v7

    #@13c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13f
    move-result-object v7

    #@140
    invoke-static {v7}, Landroid/media/AudioRecord;->logd(Ljava/lang/String;)V
    :try_end_143
    .catchall {:try_start_113 .. :try_end_143} :catchall_1a

    #@143
    .line 1229
    :try_start_143
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@146
    const-wide/16 v9, 0x14

    #@148
    invoke-static {v9, v10}, Ljava/lang/Thread;->sleep(J)V
    :try_end_14b
    .catchall {:try_start_143 .. :try_end_14b} :catchall_1a
    .catch Ljava/lang/InterruptedException; {:try_start_143 .. :try_end_14b} :catch_17f

    #@14b
    .line 1211
    :goto_14b
    add-int/lit8 v2, v2, 0x1

    #@14d
    goto/16 :goto_5b

    #@14f
    .line 1239
    :catch_14f
    move-exception v1

    #@150
    .line 1240
    .local v1, fe:Ljava/io/FileNotFoundException;
    new-instance v7, Ljava/lang/StringBuilder;

    #@152
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@155
    const-string v8, "dump write fail!! fe = "

    #@157
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15a
    move-result-object v7

    #@15b
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15e
    move-result-object v7

    #@15f
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@162
    move-result-object v7

    #@163
    invoke-static {v7}, Landroid/media/AudioRecord;->loge(Ljava/lang/String;)V

    #@166
    goto :goto_10f

    #@167
    .line 1241
    .end local v1           #fe:Ljava/io/FileNotFoundException;
    :catch_167
    move-exception v3

    #@168
    .line 1242
    .local v3, ie:Ljava/io/IOException;
    new-instance v7, Ljava/lang/StringBuilder;

    #@16a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@16d
    const-string v8, "dump write fail!! ie = "

    #@16f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@172
    move-result-object v7

    #@173
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@176
    move-result-object v7

    #@177
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17a
    move-result-object v7

    #@17b
    invoke-static {v7}, Landroid/media/AudioRecord;->loge(Ljava/lang/String;)V

    #@17e
    goto :goto_10f

    #@17f
    .line 1230
    .end local v3           #ie:Ljava/io/IOException;
    :catch_17f
    move-exception v7

    #@180
    goto :goto_14b
.end method

.method private readPipe([BII)I
    .registers 7
    .parameter "audioData"
    .parameter "offsetInBytes"
    .parameter "sizeInBytes"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 137
    iget-object v2, p0, Landroid/media/AudioRecord;->mPipe:Ljava/io/FileInputStream;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 146
    :goto_5
    return v1

    #@6
    .line 142
    :cond_6
    :try_start_6
    iget-object v2, p0, Landroid/media/AudioRecord;->mPipe:Ljava/io/FileInputStream;

    #@8
    invoke-virtual {v2, p1, p2, p3}, Ljava/io/FileInputStream;->read([BII)I
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_b} :catch_d

    #@b
    move-result v1

    #@c
    .line 143
    .local v1, nReaded:I
    goto :goto_5

    #@d
    .line 144
    .end local v1           #nReaded:I
    :catch_d
    move-exception v0

    #@e
    .line 145
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@11
    goto :goto_5
.end method

.method private releaseRecordHooking()Z
    .registers 7

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1251
    iget-boolean v3, p0, Landroid/media/AudioRecord;->mIsRecordHooking:Z

    #@3
    if-nez v3, :cond_6

    #@5
    .line 1285
    :goto_5
    return v2

    #@6
    .line 1254
    :cond_6
    const-string/jumbo v3, "releaseRecordHooking() release for recordHooking"

    #@9
    invoke-static {v3}, Landroid/media/AudioRecord;->logd(Ljava/lang/String;)V

    #@c
    .line 1256
    iget-object v3, p0, Landroid/media/AudioRecord;->mRecordHookingLock:Ljava/lang/Object;

    #@e
    monitor-enter v3

    #@f
    .line 1257
    :try_start_f
    invoke-static {}, Landroid/media/AudioManager;->getAudioService()Landroid/media/IAudioService;
    :try_end_12
    .catchall {:try_start_f .. :try_end_12} :catchall_55

    #@12
    move-result-object v1

    #@13
    .line 1259
    .local v1, service:Landroid/media/IAudioService;
    if-eqz v1, :cond_41

    #@15
    .line 1260
    :try_start_15
    invoke-interface {v1}, Landroid/media/IAudioService;->getRecordHookingState()I

    #@18
    move-result v4

    #@19
    const/4 v5, 0x3

    #@1a
    if-ne v4, v5, :cond_21

    #@1c
    .line 1261
    const/4 v4, 0x2

    #@1d
    const/4 v5, 0x0

    #@1e
    invoke-interface {v1, v4, v5}, Landroid/media/IAudioService;->setRecordHookingState(II)Z
    :try_end_21
    .catchall {:try_start_15 .. :try_end_21} :catchall_55
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_21} :catch_4a

    #@21
    .line 1270
    :cond_21
    :goto_21
    :try_start_21
    iget-object v4, p0, Landroid/media/AudioRecord;->mMf:Landroid/os/MemoryFile;

    #@23
    if-eqz v4, :cond_2a

    #@25
    .line 1271
    iget-object v4, p0, Landroid/media/AudioRecord;->mMf:Landroid/os/MemoryFile;

    #@27
    invoke-virtual {v4}, Landroid/os/MemoryFile;->close()V

    #@2a
    .line 1273
    :cond_2a
    const/4 v4, 0x0

    #@2b
    iput-boolean v4, p0, Landroid/media/AudioRecord;->mIsRecordHooking:Z

    #@2d
    .line 1274
    const/4 v4, 0x0

    #@2e
    iput v4, p0, Landroid/media/AudioRecord;->mState:I

    #@30
    .line 1275
    monitor-exit v3
    :try_end_31
    .catchall {:try_start_21 .. :try_end_31} :catchall_55

    #@31
    .line 1277
    const-string/jumbo v3, "persist.sys.record_dump"

    #@34
    invoke-static {v3, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@37
    move-result v2

    #@38
    if-eqz v2, :cond_3f

    #@3a
    .line 1279
    :try_start_3a
    iget-object v2, p0, Landroid/media/AudioRecord;->mFos:Ljava/io/FileOutputStream;

    #@3c
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_3f
    .catch Ljava/lang/Exception; {:try_start_3a .. :try_end_3f} :catch_58

    #@3f
    .line 1285
    :cond_3f
    :goto_3f
    const/4 v2, 0x1

    #@40
    goto :goto_5

    #@41
    .line 1264
    :cond_41
    :try_start_41
    const-string v4, "AudioRecord-Java"

    #@43
    const-string/jumbo v5, "releaseRecordHooking() AudioRecord() service is null."

    #@46
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_49
    .catchall {:try_start_41 .. :try_end_49} :catchall_55
    .catch Landroid/os/RemoteException; {:try_start_41 .. :try_end_49} :catch_4a

    #@49
    goto :goto_21

    #@4a
    .line 1266
    :catch_4a
    move-exception v0

    #@4b
    .line 1267
    .local v0, e:Landroid/os/RemoteException;
    :try_start_4b
    const-string v4, "AudioRecord-Java"

    #@4d
    const-string/jumbo v5, "releaseRecordHooking() Dead object in releaseRecordHooking"

    #@50
    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@53
    .line 1268
    monitor-exit v3

    #@54
    goto :goto_5

    #@55
    .line 1275
    .end local v0           #e:Landroid/os/RemoteException;
    .end local v1           #service:Landroid/media/IAudioService;
    :catchall_55
    move-exception v2

    #@56
    monitor-exit v3
    :try_end_57
    .catchall {:try_start_4b .. :try_end_57} :catchall_55

    #@57
    throw v2

    #@58
    .line 1280
    .restart local v1       #service:Landroid/media/IAudioService;
    :catch_58
    move-exception v0

    #@59
    .line 1281
    .local v0, e:Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string/jumbo v3, "mFos close fail!! e = "

    #@61
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v2

    #@65
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v2

    #@69
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v2

    #@6d
    invoke-static {v2}, Landroid/media/AudioRecord;->loge(Ljava/lang/String;)V

    #@70
    goto :goto_3f
.end method

.method private shouldDivert()Z
    .registers 8

    #@0
    .prologue
    .line 85
    new-instance v5, Ljava/lang/Throwable;

    #@2
    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    #@5
    invoke-virtual {v5}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    #@8
    move-result-object v5

    #@9
    const/4 v6, 0x2

    #@a
    aget-object v5, v5, v6

    #@c
    invoke-virtual {v5}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    #@f
    move-result-object v2

    #@10
    .line 89
    .local v2, fromClass:Ljava/lang/String;
    sget-object v0, Landroid/media/AudioRecord;->DIVERSION_FILTERS:[Ljava/lang/String;

    #@12
    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    #@13
    .local v4, len$:I
    const/4 v3, 0x0

    #@14
    .local v3, i$:I
    :goto_14
    if-ge v3, v4, :cond_23

    #@16
    aget-object v1, v0, v3

    #@18
    .line 90
    .local v1, f:Ljava/lang/String;
    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@1b
    move-result v5

    #@1c
    if-eqz v5, :cond_20

    #@1e
    .line 91
    const/4 v5, 0x1

    #@1f
    .line 94
    .end local v1           #f:Ljava/lang/String;
    :goto_1f
    return v5

    #@20
    .line 89
    .restart local v1       #f:Ljava/lang/String;
    :cond_20
    add-int/lit8 v3, v3, 0x1

    #@22
    goto :goto_14

    #@23
    .line 94
    .end local v1           #f:Ljava/lang/String;
    :cond_23
    const/4 v5, 0x0

    #@24
    goto :goto_1f
.end method


# virtual methods
.method protected finalize()V
    .registers 1

    #@0
    .prologue
    .line 539
    invoke-direct {p0}, Landroid/media/AudioRecord;->native_finalize()V

    #@3
    .line 540
    return-void
.end method

.method public getAudioFormat()I
    .registers 2

    #@0
    .prologue
    .line 566
    iget v0, p0, Landroid/media/AudioRecord;->mAudioFormat:I

    #@2
    return v0
.end method

.method public getAudioSessionId()I
    .registers 2

    #@0
    .prologue
    .line 690
    iget v0, p0, Landroid/media/AudioRecord;->mSessionId:I

    #@2
    return v0
.end method

.method public getAudioSource()I
    .registers 2

    #@0
    .prologue
    .line 558
    iget v0, p0, Landroid/media/AudioRecord;->mRecordSource:I

    #@2
    return v0
.end method

.method public getChannelConfiguration()I
    .registers 2

    #@0
    .prologue
    .line 575
    iget v0, p0, Landroid/media/AudioRecord;->mChannelConfiguration:I

    #@2
    return v0
.end method

.method public getChannelCount()I
    .registers 2

    #@0
    .prologue
    .line 582
    iget v0, p0, Landroid/media/AudioRecord;->mChannelCount:I

    #@2
    return v0
.end method

.method public getNotificationMarkerPosition()I
    .registers 2

    #@0
    .prologue
    .line 610
    invoke-direct {p0}, Landroid/media/AudioRecord;->native_get_marker_pos()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getPositionNotificationPeriod()I
    .registers 2

    #@0
    .prologue
    .line 617
    invoke-direct {p0}, Landroid/media/AudioRecord;->native_get_pos_update_period()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getRecordingState()I
    .registers 2

    #@0
    .prologue
    .line 603
    iget v0, p0, Landroid/media/AudioRecord;->mRecordingState:I

    #@2
    return v0
.end method

.method public getSampleRate()I
    .registers 2

    #@0
    .prologue
    .line 550
    iget v0, p0, Landroid/media/AudioRecord;->mSampleRate:I

    #@2
    return v0
.end method

.method public getState()I
    .registers 2

    #@0
    .prologue
    .line 594
    iget v0, p0, Landroid/media/AudioRecord;->mState:I

    #@2
    return v0
.end method

.method public read(Ljava/nio/ByteBuffer;I)I
    .registers 5
    .parameter "audioBuffer"
    .parameter "sizeInBytes"

    #@0
    .prologue
    .line 923
    iget v0, p0, Landroid/media/AudioRecord;->mState:I

    #@2
    const/4 v1, 0x1

    #@3
    if-eq v0, v1, :cond_7

    #@5
    .line 924
    const/4 v0, -0x3

    #@6
    .line 931
    :goto_6
    return v0

    #@7
    .line 927
    :cond_7
    if-eqz p1, :cond_b

    #@9
    if-gez p2, :cond_d

    #@b
    .line 928
    :cond_b
    const/4 v0, -0x2

    #@c
    goto :goto_6

    #@d
    .line 931
    :cond_d
    invoke-direct {p0, p1, p2}, Landroid/media/AudioRecord;->native_read_in_direct_buffer(Ljava/lang/Object;I)I

    #@10
    move-result v0

    #@11
    goto :goto_6
.end method

.method public read([BII)I
    .registers 6
    .parameter "audioData"
    .parameter "offsetInBytes"
    .parameter "sizeInBytes"

    #@0
    .prologue
    .line 858
    iget-boolean v0, p0, Landroid/media/AudioRecord;->mIsRecordHooking:Z

    #@2
    if-eqz v0, :cond_21

    #@4
    .line 859
    const-string v0, "audiorecording_state"

    #@6
    invoke-static {v0}, Landroid/media/AudioSystem;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    const-string/jumbo v1, "on"

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_19

    #@13
    .line 860
    const/4 v0, 0x0

    #@14
    invoke-direct {p0, p1, p2, p3, v0}, Landroid/media/AudioRecord;->read([BIII)I

    #@17
    move-result v0

    #@18
    .line 884
    :goto_18
    return v0

    #@19
    .line 862
    :cond_19
    const-string/jumbo v0, "read() RecordHooking is enable. But recordThread is not active state."

    #@1c
    invoke-static {v0}, Landroid/media/AudioRecord;->logd(Ljava/lang/String;)V

    #@1f
    .line 863
    const/4 v0, -0x1

    #@20
    goto :goto_18

    #@21
    .line 868
    :cond_21
    iget v0, p0, Landroid/media/AudioRecord;->mState:I

    #@23
    const/4 v1, 0x1

    #@24
    if-eq v0, v1, :cond_28

    #@26
    .line 869
    const/4 v0, -0x3

    #@27
    goto :goto_18

    #@28
    .line 872
    :cond_28
    if-eqz p1, :cond_33

    #@2a
    if-ltz p2, :cond_33

    #@2c
    if-ltz p3, :cond_33

    #@2e
    add-int v0, p2, p3

    #@30
    array-length v1, p1

    #@31
    if-le v0, v1, :cond_35

    #@33
    .line 874
    :cond_33
    const/4 v0, -0x2

    #@34
    goto :goto_18

    #@35
    .line 879
    :cond_35
    iget-boolean v0, p0, Landroid/media/AudioRecord;->mDiverted:Z

    #@37
    if-eqz v0, :cond_3e

    #@39
    .line 880
    invoke-direct {p0, p1, p2, p3}, Landroid/media/AudioRecord;->readPipe([BII)I

    #@3c
    move-result v0

    #@3d
    goto :goto_18

    #@3e
    .line 884
    :cond_3e
    invoke-direct {p0, p1, p2, p3}, Landroid/media/AudioRecord;->native_read_in_byte_array([BII)I

    #@41
    move-result v0

    #@42
    goto :goto_18
.end method

.method public read([SII)I
    .registers 6
    .parameter "audioData"
    .parameter "offsetInShorts"
    .parameter "sizeInShorts"

    #@0
    .prologue
    .line 899
    iget v0, p0, Landroid/media/AudioRecord;->mState:I

    #@2
    const/4 v1, 0x1

    #@3
    if-eq v0, v1, :cond_7

    #@5
    .line 900
    const/4 v0, -0x3

    #@6
    .line 908
    :goto_6
    return v0

    #@7
    .line 903
    :cond_7
    if-eqz p1, :cond_12

    #@9
    if-ltz p2, :cond_12

    #@b
    if-ltz p3, :cond_12

    #@d
    add-int v0, p2, p3

    #@f
    array-length v1, p1

    #@10
    if-le v0, v1, :cond_14

    #@12
    .line 905
    :cond_12
    const/4 v0, -0x2

    #@13
    goto :goto_6

    #@14
    .line 908
    :cond_14
    invoke-direct {p0, p1, p2, p3}, Landroid/media/AudioRecord;->native_read_in_short_array([SII)I

    #@17
    move-result v0

    #@18
    goto :goto_6
.end method

.method public release()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 515
    :try_start_1
    invoke-virtual {p0}, Landroid/media/AudioRecord;->stop()V
    :try_end_4
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_4} :catch_18

    #@4
    .line 520
    :goto_4
    invoke-direct {p0}, Landroid/media/AudioRecord;->releaseRecordHooking()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_b

    #@a
    .line 534
    :goto_a
    return-void

    #@b
    .line 525
    :cond_b
    iget-boolean v0, p0, Landroid/media/AudioRecord;->mDiverted:Z

    #@d
    if-eqz v0, :cond_14

    #@f
    .line 526
    iput-boolean v1, p0, Landroid/media/AudioRecord;->mDiverted:Z

    #@11
    .line 533
    :goto_11
    iput v1, p0, Landroid/media/AudioRecord;->mState:I

    #@13
    goto :goto_a

    #@14
    .line 528
    :cond_14
    invoke-direct {p0}, Landroid/media/AudioRecord;->native_release()V

    #@17
    goto :goto_11

    #@18
    .line 516
    :catch_18
    move-exception v0

    #@19
    goto :goto_4
.end method

.method public setNotificationMarkerPosition(I)I
    .registers 3
    .parameter "markerInFrames"

    #@0
    .prologue
    .line 985
    invoke-direct {p0, p1}, Landroid/media/AudioRecord;->native_set_marker_pos(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public setPositionNotificationPeriod(I)I
    .registers 3
    .parameter "periodInFrames"

    #@0
    .prologue
    .line 997
    invoke-direct {p0, p1}, Landroid/media/AudioRecord;->native_set_pos_update_period(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public setRecordPositionUpdateListener(Landroid/media/AudioRecord$OnRecordPositionUpdateListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 944
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/media/AudioRecord;->setRecordPositionUpdateListener(Landroid/media/AudioRecord$OnRecordPositionUpdateListener;Landroid/os/Handler;)V

    #@4
    .line 945
    return-void
.end method

.method public setRecordPositionUpdateListener(Landroid/media/AudioRecord$OnRecordPositionUpdateListener;Landroid/os/Handler;)V
    .registers 6
    .parameter "listener"
    .parameter "handler"

    #@0
    .prologue
    .line 957
    iget-object v1, p0, Landroid/media/AudioRecord;->mPositionListenerLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 959
    :try_start_3
    iput-object p1, p0, Landroid/media/AudioRecord;->mPositionListener:Landroid/media/AudioRecord$OnRecordPositionUpdateListener;

    #@5
    .line 961
    if-eqz p1, :cond_23

    #@7
    .line 962
    if-eqz p2, :cond_16

    #@9
    .line 963
    new-instance v0, Landroid/media/AudioRecord$NativeEventHandler;

    #@b
    invoke-virtual {p2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@e
    move-result-object v2

    #@f
    invoke-direct {v0, p0, p0, v2}, Landroid/media/AudioRecord$NativeEventHandler;-><init>(Landroid/media/AudioRecord;Landroid/media/AudioRecord;Landroid/os/Looper;)V

    #@12
    iput-object v0, p0, Landroid/media/AudioRecord;->mEventHandler:Landroid/media/AudioRecord$NativeEventHandler;

    #@14
    .line 971
    :goto_14
    monitor-exit v1

    #@15
    .line 973
    return-void

    #@16
    .line 966
    :cond_16
    new-instance v0, Landroid/media/AudioRecord$NativeEventHandler;

    #@18
    iget-object v2, p0, Landroid/media/AudioRecord;->mInitializationLooper:Landroid/os/Looper;

    #@1a
    invoke-direct {v0, p0, p0, v2}, Landroid/media/AudioRecord$NativeEventHandler;-><init>(Landroid/media/AudioRecord;Landroid/media/AudioRecord;Landroid/os/Looper;)V

    #@1d
    iput-object v0, p0, Landroid/media/AudioRecord;->mEventHandler:Landroid/media/AudioRecord$NativeEventHandler;

    #@1f
    goto :goto_14

    #@20
    .line 971
    :catchall_20
    move-exception v0

    #@21
    monitor-exit v1
    :try_end_22
    .catchall {:try_start_3 .. :try_end_22} :catchall_20

    #@22
    throw v0

    #@23
    .line 969
    :cond_23
    const/4 v0, 0x0

    #@24
    :try_start_24
    iput-object v0, p0, Landroid/media/AudioRecord;->mEventHandler:Landroid/media/AudioRecord$NativeEventHandler;
    :try_end_26
    .catchall {:try_start_24 .. :try_end_26} :catchall_20

    #@26
    goto :goto_14
.end method

.method public startRecording()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v1, 0x3

    #@2
    .line 702
    iget v0, p0, Landroid/media/AudioRecord;->mState:I

    #@4
    if-eq v0, v3, :cond_f

    #@6
    .line 703
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    const-string/jumbo v1, "startRecording() called on an uninitialized AudioRecord."

    #@b
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 707
    :cond_f
    iget-boolean v0, p0, Landroid/media/AudioRecord;->mIsRecordHooking:Z

    #@11
    if-eqz v0, :cond_1c

    #@13
    .line 708
    const-string/jumbo v0, "startRecording()"

    #@16
    invoke-static {v0}, Landroid/media/AudioRecord;->logd(Ljava/lang/String;)V

    #@19
    .line 709
    iput v1, p0, Landroid/media/AudioRecord;->mRecordingState:I

    #@1b
    .line 737
    :cond_1b
    :goto_1b
    return-void

    #@1c
    .line 714
    :cond_1c
    iget-object v1, p0, Landroid/media/AudioRecord;->mRecordingStateLock:Ljava/lang/Object;

    #@1e
    monitor-enter v1

    #@1f
    .line 717
    :try_start_1f
    iget-boolean v0, p0, Landroid/media/AudioRecord;->mDiverted:Z

    #@21
    if-eqz v0, :cond_43

    #@23
    .line 718
    invoke-direct {p0}, Landroid/media/AudioRecord;->pipeOpen()Z

    #@26
    move-result v0

    #@27
    if-eqz v0, :cond_2c

    #@29
    .line 719
    const/4 v0, 0x3

    #@2a
    iput v0, p0, Landroid/media/AudioRecord;->mRecordingState:I

    #@2c
    .line 728
    :cond_2c
    :goto_2c
    monitor-exit v1
    :try_end_2d
    .catchall {:try_start_1f .. :try_end_2d} :catchall_4f

    #@2d
    .line 731
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@30
    move-result-object v0

    #@31
    if-eqz v0, :cond_1b

    #@33
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@36
    move-result-object v0

    #@37
    invoke-interface {v0}, Lcom/lge/cappuccino/IMdm;->checkAllowMicrophone()Z

    #@3a
    move-result v0

    #@3b
    if-eqz v0, :cond_1b

    #@3d
    .line 733
    invoke-direct {p0}, Landroid/media/AudioRecord;->native_stop()V

    #@40
    .line 734
    iput v3, p0, Landroid/media/AudioRecord;->mRecordingState:I

    #@42
    goto :goto_1b

    #@43
    .line 722
    :cond_43
    const/4 v0, 0x0

    #@44
    const/4 v2, 0x0

    #@45
    :try_start_45
    invoke-direct {p0, v0, v2}, Landroid/media/AudioRecord;->native_start(II)I

    #@48
    move-result v0

    #@49
    if-nez v0, :cond_2c

    #@4b
    .line 723
    const/4 v0, 0x3

    #@4c
    iput v0, p0, Landroid/media/AudioRecord;->mRecordingState:I

    #@4e
    goto :goto_2c

    #@4f
    .line 728
    :catchall_4f
    move-exception v0

    #@50
    monitor-exit v1
    :try_end_51
    .catchall {:try_start_45 .. :try_end_51} :catchall_4f

    #@51
    throw v0
.end method

.method public startRecording(Landroid/media/MediaSyncEvent;)V
    .registers 8
    .parameter "syncEvent"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x3

    #@2
    .line 748
    iget v2, p0, Landroid/media/AudioRecord;->mState:I

    #@4
    if-eq v2, v5, :cond_f

    #@6
    .line 749
    new-instance v2, Ljava/lang/IllegalStateException;

    #@8
    const-string/jumbo v3, "startRecording() called on an uninitialized AudioRecord."

    #@b
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@e
    throw v2

    #@f
    .line 753
    :cond_f
    iget-boolean v2, p0, Landroid/media/AudioRecord;->mIsRecordHooking:Z

    #@11
    if-eqz v2, :cond_3a

    #@13
    .line 754
    const-string/jumbo v2, "startRecording() syncEvent"

    #@16
    invoke-static {v2}, Landroid/media/AudioRecord;->logd(Ljava/lang/String;)V

    #@19
    .line 755
    invoke-static {}, Landroid/media/AudioManager;->getAudioService()Landroid/media/IAudioService;

    #@1c
    move-result-object v1

    #@1d
    .line 757
    .local v1, service:Landroid/media/IAudioService;
    if-eqz v1, :cond_27

    #@1f
    .line 758
    const/4 v2, 0x3

    #@20
    const/4 v3, 0x0

    #@21
    :try_start_21
    invoke-interface {v1, v2, v3}, Landroid/media/IAudioService;->setRecordHookingState(II)Z
    :try_end_24
    .catch Landroid/os/RemoteException; {:try_start_21 .. :try_end_24} :catch_30

    #@24
    .line 766
    :goto_24
    iput v4, p0, Landroid/media/AudioRecord;->mRecordingState:I

    #@26
    .line 794
    .end local v1           #service:Landroid/media/IAudioService;
    :cond_26
    :goto_26
    return-void

    #@27
    .line 760
    .restart local v1       #service:Landroid/media/IAudioService;
    :cond_27
    :try_start_27
    const-string v2, "AudioRecord-Java"

    #@29
    const-string/jumbo v3, "releaseRecordHooking() AudioRecord() service is null."

    #@2c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2f
    .catch Landroid/os/RemoteException; {:try_start_27 .. :try_end_2f} :catch_30

    #@2f
    goto :goto_24

    #@30
    .line 762
    :catch_30
    move-exception v0

    #@31
    .line 763
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "AudioRecord-Java"

    #@33
    const-string/jumbo v3, "releaseRecordHooking() Dead object in releaseRecordHooking"

    #@36
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@39
    goto :goto_26

    #@3a
    .line 771
    .end local v0           #e:Landroid/os/RemoteException;
    .end local v1           #service:Landroid/media/IAudioService;
    :cond_3a
    iget-object v3, p0, Landroid/media/AudioRecord;->mRecordingStateLock:Ljava/lang/Object;

    #@3c
    monitor-enter v3

    #@3d
    .line 774
    :try_start_3d
    iget-boolean v2, p0, Landroid/media/AudioRecord;->mDiverted:Z

    #@3f
    if-eqz v2, :cond_61

    #@41
    .line 775
    invoke-direct {p0}, Landroid/media/AudioRecord;->pipeOpen()Z

    #@44
    move-result v2

    #@45
    if-eqz v2, :cond_4a

    #@47
    .line 776
    const/4 v2, 0x3

    #@48
    iput v2, p0, Landroid/media/AudioRecord;->mRecordingState:I

    #@4a
    .line 785
    :cond_4a
    :goto_4a
    monitor-exit v3
    :try_end_4b
    .catchall {:try_start_3d .. :try_end_4b} :catchall_73

    #@4b
    .line 788
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@4e
    move-result-object v2

    #@4f
    if-eqz v2, :cond_26

    #@51
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@54
    move-result-object v2

    #@55
    invoke-interface {v2}, Lcom/lge/cappuccino/IMdm;->checkAllowMicrophone()Z

    #@58
    move-result v2

    #@59
    if-eqz v2, :cond_26

    #@5b
    .line 790
    invoke-direct {p0}, Landroid/media/AudioRecord;->native_stop()V

    #@5e
    .line 791
    iput v5, p0, Landroid/media/AudioRecord;->mRecordingState:I

    #@60
    goto :goto_26

    #@61
    .line 780
    :cond_61
    :try_start_61
    invoke-virtual {p1}, Landroid/media/MediaSyncEvent;->getType()I

    #@64
    move-result v2

    #@65
    invoke-virtual {p1}, Landroid/media/MediaSyncEvent;->getAudioSessionId()I

    #@68
    move-result v4

    #@69
    invoke-direct {p0, v2, v4}, Landroid/media/AudioRecord;->native_start(II)I

    #@6c
    move-result v2

    #@6d
    if-nez v2, :cond_4a

    #@6f
    .line 781
    const/4 v2, 0x3

    #@70
    iput v2, p0, Landroid/media/AudioRecord;->mRecordingState:I

    #@72
    goto :goto_4a

    #@73
    .line 785
    :catchall_73
    move-exception v2

    #@74
    monitor-exit v3
    :try_end_75
    .catchall {:try_start_61 .. :try_end_75} :catchall_73

    #@75
    throw v2
.end method

.method public stop()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 802
    iget v2, p0, Landroid/media/AudioRecord;->mState:I

    #@3
    if-eq v2, v3, :cond_e

    #@5
    .line 803
    new-instance v2, Ljava/lang/IllegalStateException;

    #@7
    const-string/jumbo v3, "stop() called on an uninitialized AudioRecord."

    #@a
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v2

    #@e
    .line 806
    :cond_e
    iget-boolean v2, p0, Landroid/media/AudioRecord;->mIsRecordHooking:Z

    #@10
    if-eqz v2, :cond_15

    #@12
    .line 807
    iput v3, p0, Landroid/media/AudioRecord;->mRecordingState:I

    #@14
    .line 840
    :goto_14
    return-void

    #@15
    .line 813
    :cond_15
    const-string v2, "AudioRecord-Java"

    #@17
    const-string/jumbo v3, "stop()"

    #@1a
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 814
    const-string/jumbo v2, "persist.sys.voice_state"

    #@20
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    const-string/jumbo v3, "recording"

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a
    move-result v2

    #@2b
    if-nez v2, :cond_3f

    #@2d
    .line 816
    :try_start_2d
    invoke-static {}, Landroid/media/AudioManager;->getAudioService()Landroid/media/IAudioService;

    #@30
    move-result-object v0

    #@31
    .line 817
    .local v0, audioService:Landroid/media/IAudioService;
    if-eqz v0, :cond_51

    #@33
    .line 818
    const-string v2, "AudioRecord-Java"

    #@35
    const-string/jumbo v3, "stop() VoiceActivation will be stop."

    #@38
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 819
    const/4 v2, 0x0

    #@3c
    invoke-interface {v0, v2}, Landroid/media/IAudioService;->sendBroadcastRecordState(I)V
    :try_end_3f
    .catch Ljava/lang/Exception; {:try_start_2d .. :try_end_3f} :catch_5a

    #@3f
    .line 830
    .end local v0           #audioService:Landroid/media/IAudioService;
    :cond_3f
    :goto_3f
    iget-object v3, p0, Landroid/media/AudioRecord;->mRecordingStateLock:Ljava/lang/Object;

    #@41
    monitor-enter v3

    #@42
    .line 833
    :try_start_42
    iget-boolean v2, p0, Landroid/media/AudioRecord;->mDiverted:Z

    #@44
    if-eqz v2, :cond_64

    #@46
    .line 834
    invoke-direct {p0}, Landroid/media/AudioRecord;->pipeClose()V

    #@49
    .line 838
    :goto_49
    const/4 v2, 0x1

    #@4a
    iput v2, p0, Landroid/media/AudioRecord;->mRecordingState:I

    #@4c
    .line 839
    monitor-exit v3

    #@4d
    goto :goto_14

    #@4e
    :catchall_4e
    move-exception v2

    #@4f
    monitor-exit v3
    :try_end_50
    .catchall {:try_start_42 .. :try_end_50} :catchall_4e

    #@50
    throw v2

    #@51
    .line 821
    .restart local v0       #audioService:Landroid/media/IAudioService;
    :cond_51
    :try_start_51
    const-string v2, "AudioRecord-Java"

    #@53
    const-string/jumbo v3, "stop() audioService is null!"

    #@56
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_59
    .catch Ljava/lang/Exception; {:try_start_51 .. :try_end_59} :catch_5a

    #@59
    goto :goto_3f

    #@5a
    .line 823
    .end local v0           #audioService:Landroid/media/IAudioService;
    :catch_5a
    move-exception v1

    #@5b
    .line 824
    .local v1, e:Ljava/lang/Exception;
    const-string v2, "AudioRecord-Java"

    #@5d
    const-string/jumbo v3, "stop() "

    #@60
    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@63
    goto :goto_3f

    #@64
    .line 836
    .end local v1           #e:Ljava/lang/Exception;
    :cond_64
    :try_start_64
    invoke-direct {p0}, Landroid/media/AudioRecord;->native_stop()V
    :try_end_67
    .catchall {:try_start_64 .. :try_end_67} :catchall_4e

    #@67
    goto :goto_49
.end method
