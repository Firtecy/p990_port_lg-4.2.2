.class Landroid/media/RemoteDisplay$1;
.super Ljava/lang/Object;
.source "RemoteDisplay.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/media/RemoteDisplay;->notifyDisplayConnected(Landroid/view/Surface;III)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/media/RemoteDisplay;

.field final synthetic val$flags:I

.field final synthetic val$height:I

.field final synthetic val$surface:Landroid/view/Surface;

.field final synthetic val$width:I


# direct methods
.method constructor <init>(Landroid/media/RemoteDisplay;Landroid/view/Surface;III)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 117
    iput-object p1, p0, Landroid/media/RemoteDisplay$1;->this$0:Landroid/media/RemoteDisplay;

    #@2
    iput-object p2, p0, Landroid/media/RemoteDisplay$1;->val$surface:Landroid/view/Surface;

    #@4
    iput p3, p0, Landroid/media/RemoteDisplay$1;->val$width:I

    #@6
    iput p4, p0, Landroid/media/RemoteDisplay$1;->val$height:I

    #@8
    iput p5, p0, Landroid/media/RemoteDisplay$1;->val$flags:I

    #@a
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@d
    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    #@0
    .prologue
    .line 120
    iget-object v0, p0, Landroid/media/RemoteDisplay$1;->this$0:Landroid/media/RemoteDisplay;

    #@2
    invoke-static {v0}, Landroid/media/RemoteDisplay;->access$000(Landroid/media/RemoteDisplay;)Landroid/media/RemoteDisplay$Listener;

    #@5
    move-result-object v0

    #@6
    iget-object v1, p0, Landroid/media/RemoteDisplay$1;->val$surface:Landroid/view/Surface;

    #@8
    iget v2, p0, Landroid/media/RemoteDisplay$1;->val$width:I

    #@a
    iget v3, p0, Landroid/media/RemoteDisplay$1;->val$height:I

    #@c
    iget v4, p0, Landroid/media/RemoteDisplay$1;->val$flags:I

    #@e
    invoke-interface {v0, v1, v2, v3, v4}, Landroid/media/RemoteDisplay$Listener;->onDisplayConnected(Landroid/view/Surface;III)V

    #@11
    .line 121
    return-void
.end method
