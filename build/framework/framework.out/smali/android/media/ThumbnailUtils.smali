.class public Landroid/media/ThumbnailUtils;
.super Ljava/lang/Object;
.source "ThumbnailUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/ThumbnailUtils$1;,
        Landroid/media/ThumbnailUtils$SizedThumbnailBitmap;
    }
.end annotation


# static fields
.field private static final MAX_NUM_PIXELS_MICRO_THUMBNAIL:I = 0x4000

.field private static final MAX_NUM_PIXELS_THUMBNAIL:I = 0x30000

.field private static final OPTIONS_NONE:I = 0x0

.field public static final OPTIONS_RECYCLE_INPUT:I = 0x2

.field private static final OPTIONS_SCALE_UP:I = 0x1

.field private static final TAG:Ljava/lang/String; = "ThumbnailUtils"

.field public static final TARGET_SIZE_MICRO_THUMBNAIL:I = 0x60

.field public static final TARGET_SIZE_MINI_THUMBNAIL:I = 0x140

.field private static final UNCONSTRAINED:I = -0x1


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 46
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 466
    return-void
.end method

.method private static closeSilently(Landroid/os/ParcelFileDescriptor;)V
    .registers 2
    .parameter "c"

    #@0
    .prologue
    .line 342
    if-nez p0, :cond_3

    #@2
    .line 348
    :goto_2
    return-void

    #@3
    .line 344
    :cond_3
    :try_start_3
    invoke-virtual {p0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_6} :catch_7

    #@6
    goto :goto_2

    #@7
    .line 345
    :catch_7
    move-exception v0

    #@8
    goto :goto_2
.end method

.method private static computeInitialSampleSize(Landroid/graphics/BitmapFactory$Options;II)I
    .registers 15
    .parameter "options"
    .parameter "minSideLength"
    .parameter "maxNumOfPixels"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v11, -0x1

    #@2
    .line 276
    iget v7, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    #@4
    int-to-double v4, v7

    #@5
    .line 277
    .local v4, w:D
    iget v7, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    #@7
    int-to-double v0, v7

    #@8
    .line 279
    .local v0, h:D
    if-ne p2, v11, :cond_12

    #@a
    move v2, v6

    #@b
    .line 281
    .local v2, lowerBound:I
    :goto_b
    if-ne p1, v11, :cond_20

    #@d
    const/16 v3, 0x80

    #@f
    .line 285
    .local v3, upperBound:I
    :goto_f
    if-ge v3, v2, :cond_34

    #@11
    .line 296
    .end local v2           #lowerBound:I
    :cond_11
    :goto_11
    return v2

    #@12
    .line 279
    .end local v3           #upperBound:I
    :cond_12
    mul-double v7, v4, v0

    #@14
    int-to-double v9, p2

    #@15
    div-double/2addr v7, v9

    #@16
    invoke-static {v7, v8}, Ljava/lang/Math;->sqrt(D)D

    #@19
    move-result-wide v7

    #@1a
    invoke-static {v7, v8}, Ljava/lang/Math;->ceil(D)D

    #@1d
    move-result-wide v7

    #@1e
    double-to-int v2, v7

    #@1f
    goto :goto_b

    #@20
    .line 281
    .restart local v2       #lowerBound:I
    :cond_20
    int-to-double v7, p1

    #@21
    div-double v7, v4, v7

    #@23
    invoke-static {v7, v8}, Ljava/lang/Math;->floor(D)D

    #@26
    move-result-wide v7

    #@27
    int-to-double v9, p1

    #@28
    div-double v9, v0, v9

    #@2a
    invoke-static {v9, v10}, Ljava/lang/Math;->floor(D)D

    #@2d
    move-result-wide v9

    #@2e
    invoke-static {v7, v8, v9, v10}, Ljava/lang/Math;->min(DD)D

    #@31
    move-result-wide v7

    #@32
    double-to-int v3, v7

    #@33
    goto :goto_f

    #@34
    .line 290
    .restart local v3       #upperBound:I
    :cond_34
    if-ne p2, v11, :cond_3a

    #@36
    if-ne p1, v11, :cond_3a

    #@38
    move v2, v6

    #@39
    .line 292
    goto :goto_11

    #@3a
    .line 293
    :cond_3a
    if-eq p1, v11, :cond_11

    #@3c
    move v2, v3

    #@3d
    .line 296
    goto :goto_11
.end method

.method private static computeSampleSize(Landroid/graphics/BitmapFactory$Options;II)I
    .registers 6
    .parameter "options"
    .parameter "minSideLength"
    .parameter "maxNumOfPixels"

    #@0
    .prologue
    .line 258
    invoke-static {p0, p1, p2}, Landroid/media/ThumbnailUtils;->computeInitialSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    #@3
    move-result v0

    #@4
    .line 262
    .local v0, initialSize:I
    const/16 v2, 0x8

    #@6
    if-gt v0, v2, :cond_e

    #@8
    .line 263
    const/4 v1, 0x1

    #@9
    .line 264
    .local v1, roundedSize:I
    :goto_9
    if-ge v1, v0, :cond_14

    #@b
    .line 265
    shl-int/lit8 v1, v1, 0x1

    #@d
    goto :goto_9

    #@e
    .line 268
    .end local v1           #roundedSize:I
    :cond_e
    add-int/lit8 v2, v0, 0x7

    #@10
    div-int/lit8 v2, v2, 0x8

    #@12
    mul-int/lit8 v1, v2, 0x8

    #@14
    .line 271
    .restart local v1       #roundedSize:I
    :cond_14
    return v1
.end method

.method public static createImageThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    .registers 18
    .parameter "filePath"
    .parameter "kind"

    #@0
    .prologue
    .line 91
    const/4 v13, 0x1

    #@1
    move/from16 v0, p1

    #@3
    if-ne v0, v13, :cond_5a

    #@5
    const/4 v12, 0x1

    #@6
    .line 92
    .local v12, wantMini:Z
    :goto_6
    if-eqz v12, :cond_5c

    #@8
    const/16 v11, 0x140

    #@a
    .line 95
    .local v11, targetSize:I
    :goto_a
    if-eqz v12, :cond_5f

    #@c
    const/high16 v5, 0x3

    #@e
    .line 98
    .local v5, maxPixels:I
    :goto_e
    new-instance v8, Landroid/media/ThumbnailUtils$SizedThumbnailBitmap;

    #@10
    const/4 v13, 0x0

    #@11
    invoke-direct {v8, v13}, Landroid/media/ThumbnailUtils$SizedThumbnailBitmap;-><init>(Landroid/media/ThumbnailUtils$1;)V

    #@14
    .line 99
    .local v8, sizedThumbnailBitmap:Landroid/media/ThumbnailUtils$SizedThumbnailBitmap;
    const/4 v1, 0x0

    #@15
    .line 100
    .local v1, bitmap:Landroid/graphics/Bitmap;
    invoke-static/range {p0 .. p0}, Landroid/media/MediaFile;->getFileType(Ljava/lang/String;)Landroid/media/MediaFile$MediaFileType;

    #@18
    move-result-object v4

    #@19
    .line 101
    .local v4, fileType:Landroid/media/MediaFile$MediaFileType;
    if-eqz v4, :cond_28

    #@1b
    iget v13, v4, Landroid/media/MediaFile$MediaFileType;->fileType:I

    #@1d
    const/16 v14, 0x20

    #@1f
    if-ne v13, v14, :cond_28

    #@21
    .line 102
    move-object/from16 v0, p0

    #@23
    invoke-static {v0, v11, v5, v8}, Landroid/media/ThumbnailUtils;->createThumbnailFromEXIF(Ljava/lang/String;IILandroid/media/ThumbnailUtils$SizedThumbnailBitmap;)V

    #@26
    .line 103
    iget-object v1, v8, Landroid/media/ThumbnailUtils$SizedThumbnailBitmap;->mBitmap:Landroid/graphics/Bitmap;

    #@28
    .line 106
    :cond_28
    if-nez v1, :cond_85

    #@2a
    .line 107
    const/4 v9, 0x0

    #@2b
    .line 109
    .local v9, stream:Ljava/io/FileInputStream;
    :try_start_2b
    new-instance v10, Ljava/io/FileInputStream;

    #@2d
    move-object/from16 v0, p0

    #@2f
    invoke-direct {v10, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_32
    .catchall {:try_start_2b .. :try_end_32} :catchall_e5
    .catch Ljava/io/IOException; {:try_start_2b .. :try_end_32} :catch_9e
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2b .. :try_end_32} :catch_b5

    #@32
    .line 110
    .end local v9           #stream:Ljava/io/FileInputStream;
    .local v10, stream:Ljava/io/FileInputStream;
    :try_start_32
    invoke-virtual {v10}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    #@35
    move-result-object v3

    #@36
    .line 111
    .local v3, fd:Ljava/io/FileDescriptor;
    new-instance v7, Landroid/graphics/BitmapFactory$Options;

    #@38
    invoke-direct {v7}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    #@3b
    .line 112
    .local v7, options:Landroid/graphics/BitmapFactory$Options;
    const/4 v13, 0x1

    #@3c
    iput v13, v7, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    #@3e
    .line 113
    const/4 v13, 0x1

    #@3f
    iput-boolean v13, v7, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    #@41
    .line 114
    const/4 v13, 0x0

    #@42
    invoke-static {v3, v13, v7}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@45
    .line 115
    iget-boolean v13, v7, Landroid/graphics/BitmapFactory$Options;->mCancel:Z

    #@47
    if-nez v13, :cond_53

    #@49
    iget v13, v7, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    #@4b
    const/4 v14, -0x1

    #@4c
    if-eq v13, v14, :cond_53

    #@4e
    iget v13, v7, Landroid/graphics/BitmapFactory$Options;->outHeight:I
    :try_end_50
    .catchall {:try_start_32 .. :try_end_50} :catchall_f5
    .catch Ljava/io/IOException; {:try_start_32 .. :try_end_50} :catch_fb
    .catch Ljava/lang/OutOfMemoryError; {:try_start_32 .. :try_end_50} :catch_f8

    #@50
    const/4 v14, -0x1

    #@51
    if-ne v13, v14, :cond_6b

    #@53
    .line 117
    :cond_53
    const/4 v13, 0x0

    #@54
    .line 132
    if-eqz v10, :cond_59

    #@56
    .line 133
    :try_start_56
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_59
    .catch Ljava/io/IOException; {:try_start_56 .. :try_end_59} :catch_62

    #@59
    .line 148
    .end local v3           #fd:Ljava/io/FileDescriptor;
    .end local v7           #options:Landroid/graphics/BitmapFactory$Options;
    .end local v10           #stream:Ljava/io/FileInputStream;
    :cond_59
    :goto_59
    return-object v13

    #@5a
    .line 91
    .end local v1           #bitmap:Landroid/graphics/Bitmap;
    .end local v4           #fileType:Landroid/media/MediaFile$MediaFileType;
    .end local v5           #maxPixels:I
    .end local v8           #sizedThumbnailBitmap:Landroid/media/ThumbnailUtils$SizedThumbnailBitmap;
    .end local v11           #targetSize:I
    .end local v12           #wantMini:Z
    :cond_5a
    const/4 v12, 0x0

    #@5b
    goto :goto_6

    #@5c
    .line 92
    .restart local v12       #wantMini:Z
    :cond_5c
    const/16 v11, 0x60

    #@5e
    goto :goto_a

    #@5f
    .line 95
    .restart local v11       #targetSize:I
    :cond_5f
    const/16 v5, 0x4000

    #@61
    goto :goto_e

    #@62
    .line 135
    .restart local v1       #bitmap:Landroid/graphics/Bitmap;
    .restart local v3       #fd:Ljava/io/FileDescriptor;
    .restart local v4       #fileType:Landroid/media/MediaFile$MediaFileType;
    .restart local v5       #maxPixels:I
    .restart local v7       #options:Landroid/graphics/BitmapFactory$Options;
    .restart local v8       #sizedThumbnailBitmap:Landroid/media/ThumbnailUtils$SizedThumbnailBitmap;
    .restart local v10       #stream:Ljava/io/FileInputStream;
    :catch_62
    move-exception v2

    #@63
    .line 136
    .local v2, ex:Ljava/io/IOException;
    const-string v14, "ThumbnailUtils"

    #@65
    const-string v15, ""

    #@67
    invoke-static {v14, v15, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@6a
    goto :goto_59

    #@6b
    .line 119
    .end local v2           #ex:Ljava/io/IOException;
    :cond_6b
    :try_start_6b
    invoke-static {v7, v11, v5}, Landroid/media/ThumbnailUtils;->computeSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    #@6e
    move-result v13

    #@6f
    iput v13, v7, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    #@71
    .line 121
    const/4 v13, 0x0

    #@72
    iput-boolean v13, v7, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    #@74
    .line 123
    const/4 v13, 0x0

    #@75
    iput-boolean v13, v7, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    #@77
    .line 124
    sget-object v13, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@79
    iput-object v13, v7, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    #@7b
    .line 125
    const/4 v13, 0x0

    #@7c
    invoke-static {v3, v13, v7}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_7f
    .catchall {:try_start_6b .. :try_end_7f} :catchall_f5
    .catch Ljava/io/IOException; {:try_start_6b .. :try_end_7f} :catch_fb
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6b .. :try_end_7f} :catch_f8

    #@7f
    move-result-object v1

    #@80
    .line 132
    if-eqz v10, :cond_85

    #@82
    .line 133
    :try_start_82
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_85
    .catch Ljava/io/IOException; {:try_start_82 .. :try_end_85} :catch_95

    #@85
    .line 142
    .end local v3           #fd:Ljava/io/FileDescriptor;
    .end local v7           #options:Landroid/graphics/BitmapFactory$Options;
    .end local v10           #stream:Ljava/io/FileInputStream;
    :cond_85
    :goto_85
    const/4 v13, 0x3

    #@86
    move/from16 v0, p1

    #@88
    if-ne v0, v13, :cond_93

    #@8a
    .line 144
    const/16 v13, 0x60

    #@8c
    const/16 v14, 0x60

    #@8e
    const/4 v15, 0x2

    #@8f
    invoke-static {v1, v13, v14, v15}, Landroid/media/ThumbnailUtils;->extractThumbnail(Landroid/graphics/Bitmap;III)Landroid/graphics/Bitmap;

    #@92
    move-result-object v1

    #@93
    :cond_93
    move-object v13, v1

    #@94
    .line 148
    goto :goto_59

    #@95
    .line 135
    .restart local v3       #fd:Ljava/io/FileDescriptor;
    .restart local v7       #options:Landroid/graphics/BitmapFactory$Options;
    .restart local v10       #stream:Ljava/io/FileInputStream;
    :catch_95
    move-exception v2

    #@96
    .line 136
    .restart local v2       #ex:Ljava/io/IOException;
    const-string v13, "ThumbnailUtils"

    #@98
    const-string v14, ""

    #@9a
    invoke-static {v13, v14, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@9d
    goto :goto_85

    #@9e
    .line 126
    .end local v2           #ex:Ljava/io/IOException;
    .end local v3           #fd:Ljava/io/FileDescriptor;
    .end local v7           #options:Landroid/graphics/BitmapFactory$Options;
    .end local v10           #stream:Ljava/io/FileInputStream;
    .restart local v9       #stream:Ljava/io/FileInputStream;
    :catch_9e
    move-exception v2

    #@9f
    .line 127
    .restart local v2       #ex:Ljava/io/IOException;
    :goto_9f
    :try_start_9f
    const-string v13, "ThumbnailUtils"

    #@a1
    const-string v14, ""

    #@a3
    invoke-static {v13, v14, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_a6
    .catchall {:try_start_9f .. :try_end_a6} :catchall_e5

    #@a6
    .line 132
    if-eqz v9, :cond_85

    #@a8
    .line 133
    :try_start_a8
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_ab
    .catch Ljava/io/IOException; {:try_start_a8 .. :try_end_ab} :catch_ac

    #@ab
    goto :goto_85

    #@ac
    .line 135
    :catch_ac
    move-exception v2

    #@ad
    .line 136
    const-string v13, "ThumbnailUtils"

    #@af
    const-string v14, ""

    #@b1
    invoke-static {v13, v14, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@b4
    goto :goto_85

    #@b5
    .line 128
    .end local v2           #ex:Ljava/io/IOException;
    :catch_b5
    move-exception v6

    #@b6
    .line 129
    .local v6, oom:Ljava/lang/OutOfMemoryError;
    :goto_b6
    :try_start_b6
    const-string v13, "ThumbnailUtils"

    #@b8
    new-instance v14, Ljava/lang/StringBuilder;

    #@ba
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@bd
    const-string v15, "Unable to decode file "

    #@bf
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v14

    #@c3
    move-object/from16 v0, p0

    #@c5
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v14

    #@c9
    const-string v15, ". OutOfMemoryError."

    #@cb
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v14

    #@cf
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d2
    move-result-object v14

    #@d3
    invoke-static {v13, v14, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_d6
    .catchall {:try_start_b6 .. :try_end_d6} :catchall_e5

    #@d6
    .line 132
    if-eqz v9, :cond_85

    #@d8
    .line 133
    :try_start_d8
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_db
    .catch Ljava/io/IOException; {:try_start_d8 .. :try_end_db} :catch_dc

    #@db
    goto :goto_85

    #@dc
    .line 135
    :catch_dc
    move-exception v2

    #@dd
    .line 136
    .restart local v2       #ex:Ljava/io/IOException;
    const-string v13, "ThumbnailUtils"

    #@df
    const-string v14, ""

    #@e1
    invoke-static {v13, v14, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e4
    goto :goto_85

    #@e5
    .line 131
    .end local v2           #ex:Ljava/io/IOException;
    .end local v6           #oom:Ljava/lang/OutOfMemoryError;
    :catchall_e5
    move-exception v13

    #@e6
    .line 132
    :goto_e6
    if-eqz v9, :cond_eb

    #@e8
    .line 133
    :try_start_e8
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_eb
    .catch Ljava/io/IOException; {:try_start_e8 .. :try_end_eb} :catch_ec

    #@eb
    .line 137
    :cond_eb
    :goto_eb
    throw v13

    #@ec
    .line 135
    :catch_ec
    move-exception v2

    #@ed
    .line 136
    .restart local v2       #ex:Ljava/io/IOException;
    const-string v14, "ThumbnailUtils"

    #@ef
    const-string v15, ""

    #@f1
    invoke-static {v14, v15, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f4
    goto :goto_eb

    #@f5
    .line 131
    .end local v2           #ex:Ljava/io/IOException;
    .end local v9           #stream:Ljava/io/FileInputStream;
    .restart local v10       #stream:Ljava/io/FileInputStream;
    :catchall_f5
    move-exception v13

    #@f6
    move-object v9, v10

    #@f7
    .end local v10           #stream:Ljava/io/FileInputStream;
    .restart local v9       #stream:Ljava/io/FileInputStream;
    goto :goto_e6

    #@f8
    .line 128
    .end local v9           #stream:Ljava/io/FileInputStream;
    .restart local v10       #stream:Ljava/io/FileInputStream;
    :catch_f8
    move-exception v6

    #@f9
    move-object v9, v10

    #@fa
    .end local v10           #stream:Ljava/io/FileInputStream;
    .restart local v9       #stream:Ljava/io/FileInputStream;
    goto :goto_b6

    #@fb
    .line 126
    .end local v9           #stream:Ljava/io/FileInputStream;
    .restart local v10       #stream:Ljava/io/FileInputStream;
    :catch_fb
    move-exception v2

    #@fc
    move-object v9, v10

    #@fd
    .end local v10           #stream:Ljava/io/FileInputStream;
    .restart local v9       #stream:Ljava/io/FileInputStream;
    goto :goto_9f
.end method

.method private static createThumbnailFromEXIF(Ljava/lang/String;IILandroid/media/ThumbnailUtils$SizedThumbnailBitmap;)V
    .registers 16
    .parameter "filePath"
    .parameter "targetSize"
    .parameter "maxPixels"
    .parameter "sizedThumbBitmap"

    #@0
    .prologue
    .line 480
    if-nez p0, :cond_3

    #@2
    .line 536
    :cond_2
    :goto_2
    return-void

    #@3
    .line 485
    :cond_3
    const/4 v1, 0x0

    #@4
    .line 486
    .local v1, exif:Landroid/media/ExifInterface;
    const/4 v6, 0x0

    #@5
    .line 488
    .local v6, thumbData:[B
    :try_start_5
    new-instance v2, Landroid/media/ExifInterface;

    #@7
    invoke-direct {v2, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_a} :catch_99

    #@a
    .line 489
    .end local v1           #exif:Landroid/media/ExifInterface;
    .local v2, exif:Landroid/media/ExifInterface;
    if-eqz v2, :cond_40

    #@c
    .line 490
    :try_start_c
    invoke-virtual {v2}, Landroid/media/ExifInterface;->getThumbnail()[B
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_f} :catch_75

    #@f
    move-result-object v6

    #@10
    :goto_10
    move-object v1, v2

    #@11
    .line 502
    .end local v2           #exif:Landroid/media/ExifInterface;
    .restart local v1       #exif:Landroid/media/ExifInterface;
    :goto_11
    if-eqz v6, :cond_7d

    #@13
    .line 504
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    #@15
    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    #@18
    .line 505
    .local v3, exifOptions:Landroid/graphics/BitmapFactory$Options;
    const/4 v8, 0x1

    #@19
    iput-boolean v8, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    #@1b
    .line 506
    const/4 v8, 0x0

    #@1c
    array-length v9, v6

    #@1d
    invoke-static {v6, v8, v9, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@20
    .line 507
    invoke-static {v3, p1, p2}, Landroid/media/ThumbnailUtils;->computeSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    #@23
    move-result v8

    #@24
    iput v8, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    #@26
    .line 509
    iget v7, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    #@28
    .line 510
    .local v7, width:I
    iget v5, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    #@2a
    .line 511
    .local v5, height:I
    const/4 v8, 0x0

    #@2b
    iput-boolean v8, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    #@2d
    .line 513
    const/4 v8, 0x0

    #@2e
    array-length v9, v6

    #@2f
    invoke-static {v6, v8, v9, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@32
    move-result-object v8

    #@33
    iput-object v8, p3, Landroid/media/ThumbnailUtils$SizedThumbnailBitmap;->mBitmap:Landroid/graphics/Bitmap;

    #@35
    .line 515
    iget-object v8, p3, Landroid/media/ThumbnailUtils$SizedThumbnailBitmap;->mBitmap:Landroid/graphics/Bitmap;

    #@37
    if-eqz v8, :cond_2

    #@39
    .line 516
    iput-object v6, p3, Landroid/media/ThumbnailUtils$SizedThumbnailBitmap;->mThumbnailData:[B

    #@3b
    .line 517
    iput v7, p3, Landroid/media/ThumbnailUtils$SizedThumbnailBitmap;->mThumbnailWidth:I

    #@3d
    .line 518
    iput v5, p3, Landroid/media/ThumbnailUtils$SizedThumbnailBitmap;->mThumbnailHeight:I

    #@3f
    goto :goto_2

    #@40
    .line 494
    .end local v1           #exif:Landroid/media/ExifInterface;
    .end local v3           #exifOptions:Landroid/graphics/BitmapFactory$Options;
    .end local v5           #height:I
    .end local v7           #width:I
    .restart local v2       #exif:Landroid/media/ExifInterface;
    :cond_40
    :try_start_40
    const-string v8, "ThumbnailUtils"

    #@42
    new-instance v9, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v10, "["

    #@49
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v9

    #@4d
    new-instance v10, Ljava/lang/Throwable;

    #@4f
    invoke-direct {v10}, Ljava/lang/Throwable;-><init>()V

    #@52
    invoke-virtual {v10}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    #@55
    move-result-object v10

    #@56
    const/4 v11, 0x0

    #@57
    aget-object v10, v10, v11

    #@59
    invoke-virtual {v10}, Ljava/lang/StackTraceElement;->getLineNumber()I

    #@5c
    move-result v10

    #@5d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@60
    move-result-object v9

    #@61
    const-string v10, "] "

    #@63
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v9

    #@67
    const-string v10, "EXIF NULL!"

    #@69
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v9

    #@6d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v9

    #@71
    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_74
    .catch Ljava/io/IOException; {:try_start_40 .. :try_end_74} :catch_75

    #@74
    goto :goto_10

    #@75
    .line 496
    :catch_75
    move-exception v0

    #@76
    move-object v1, v2

    #@77
    .line 497
    .end local v2           #exif:Landroid/media/ExifInterface;
    .local v0, ex:Ljava/io/IOException;
    .restart local v1       #exif:Landroid/media/ExifInterface;
    :goto_77
    const-string v8, "ThumbnailUtils"

    #@79
    invoke-static {v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@7c
    goto :goto_11

    #@7d
    .line 523
    .end local v0           #ex:Ljava/io/IOException;
    :cond_7d
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    #@7f
    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    #@82
    .line 525
    .local v4, fullOptions:Landroid/graphics/BitmapFactory$Options;
    const/4 v8, 0x1

    #@83
    iput-boolean v8, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    #@85
    .line 526
    invoke-static {p0, v4}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@88
    .line 527
    invoke-static {v4, p1, p2}, Landroid/media/ThumbnailUtils;->computeSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    #@8b
    move-result v8

    #@8c
    iput v8, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    #@8e
    .line 528
    const/4 v8, 0x0

    #@8f
    iput-boolean v8, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    #@91
    .line 530
    invoke-static {p0, v4}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@94
    move-result-object v8

    #@95
    iput-object v8, p3, Landroid/media/ThumbnailUtils$SizedThumbnailBitmap;->mBitmap:Landroid/graphics/Bitmap;

    #@97
    goto/16 :goto_2

    #@99
    .line 496
    .end local v4           #fullOptions:Landroid/graphics/BitmapFactory$Options;
    :catch_99
    move-exception v0

    #@9a
    goto :goto_77
.end method

.method public static createVideoThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    .registers 14
    .parameter "filePath"
    .parameter "kind"

    #@0
    .prologue
    const/16 v11, 0x60

    #@2
    const/4 v10, 0x1

    #@3
    .line 159
    const/4 v0, 0x0

    #@4
    .line 160
    .local v0, bitmap:Landroid/graphics/Bitmap;
    new-instance v4, Landroid/media/MediaMetadataRetriever;

    #@6
    invoke-direct {v4}, Landroid/media/MediaMetadataRetriever;-><init>()V

    #@9
    .line 162
    .local v4, retriever:Landroid/media/MediaMetadataRetriever;
    :try_start_9
    invoke-virtual {v4, p0}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    #@c
    .line 163
    const-wide/16 v8, -0x1

    #@e
    invoke-virtual {v4, v8, v9}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(J)Landroid/graphics/Bitmap;
    :try_end_11
    .catchall {:try_start_9 .. :try_end_11} :catchall_27
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_11} :catch_19
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_11} :catch_20

    #@11
    move-result-object v0

    #@12
    .line 170
    :try_start_12
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_15
    .catch Ljava/lang/RuntimeException; {:try_start_12 .. :try_end_15} :catch_5e

    #@15
    .line 176
    :goto_15
    if-nez v0, :cond_2c

    #@17
    const/4 v8, 0x0

    #@18
    .line 195
    :goto_18
    return-object v8

    #@19
    .line 164
    :catch_19
    move-exception v8

    #@1a
    .line 170
    :try_start_1a
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_1d
    .catch Ljava/lang/RuntimeException; {:try_start_1a .. :try_end_1d} :catch_1e

    #@1d
    goto :goto_15

    #@1e
    .line 171
    :catch_1e
    move-exception v8

    #@1f
    goto :goto_15

    #@20
    .line 166
    :catch_20
    move-exception v8

    #@21
    .line 170
    :try_start_21
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_24
    .catch Ljava/lang/RuntimeException; {:try_start_21 .. :try_end_24} :catch_25

    #@24
    goto :goto_15

    #@25
    .line 171
    :catch_25
    move-exception v8

    #@26
    goto :goto_15

    #@27
    .line 169
    :catchall_27
    move-exception v8

    #@28
    .line 170
    :try_start_28
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_2b
    .catch Ljava/lang/RuntimeException; {:try_start_28 .. :try_end_2b} :catch_60

    #@2b
    .line 173
    :goto_2b
    throw v8

    #@2c
    .line 178
    :cond_2c
    if-ne p1, v10, :cond_55

    #@2e
    .line 180
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    #@31
    move-result v7

    #@32
    .line 181
    .local v7, width:I
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    #@35
    move-result v2

    #@36
    .line 182
    .local v2, height:I
    invoke-static {v7, v2}, Ljava/lang/Math;->max(II)I

    #@39
    move-result v3

    #@3a
    .line 183
    .local v3, max:I
    const/16 v8, 0x200

    #@3c
    if-le v3, v8, :cond_53

    #@3e
    .line 184
    const/high16 v8, 0x4400

    #@40
    int-to-float v9, v3

    #@41
    div-float v5, v8, v9

    #@43
    .line 185
    .local v5, scale:F
    int-to-float v8, v7

    #@44
    mul-float/2addr v8, v5

    #@45
    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    #@48
    move-result v6

    #@49
    .line 186
    .local v6, w:I
    int-to-float v8, v2

    #@4a
    mul-float/2addr v8, v5

    #@4b
    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    #@4e
    move-result v1

    #@4f
    .line 187
    .local v1, h:I
    invoke-static {v0, v6, v1, v10}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    #@52
    move-result-object v0

    #@53
    .end local v1           #h:I
    .end local v2           #height:I
    .end local v3           #max:I
    .end local v5           #scale:F
    .end local v6           #w:I
    .end local v7           #width:I
    :cond_53
    :goto_53
    move-object v8, v0

    #@54
    .line 195
    goto :goto_18

    #@55
    .line 189
    :cond_55
    const/4 v8, 0x3

    #@56
    if-ne p1, v8, :cond_53

    #@58
    .line 190
    const/4 v8, 0x2

    #@59
    invoke-static {v0, v11, v11, v8}, Landroid/media/ThumbnailUtils;->extractThumbnail(Landroid/graphics/Bitmap;III)Landroid/graphics/Bitmap;

    #@5c
    move-result-object v0

    #@5d
    goto :goto_53

    #@5e
    .line 171
    :catch_5e
    move-exception v8

    #@5f
    goto :goto_15

    #@60
    :catch_60
    move-exception v9

    #@61
    goto :goto_2b
.end method

.method public static extractThumbnail(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .registers 4
    .parameter "source"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 207
    const/4 v0, 0x0

    #@1
    invoke-static {p0, p1, p2, v0}, Landroid/media/ThumbnailUtils;->extractThumbnail(Landroid/graphics/Bitmap;III)Landroid/graphics/Bitmap;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static extractThumbnail(Landroid/graphics/Bitmap;III)Landroid/graphics/Bitmap;
    .registers 9
    .parameter "source"
    .parameter "width"
    .parameter "height"
    .parameter "options"

    #@0
    .prologue
    .line 220
    if-nez p0, :cond_4

    #@2
    .line 221
    const/4 v2, 0x0

    #@3
    .line 234
    :goto_3
    return-object v2

    #@4
    .line 225
    :cond_4
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    #@7
    move-result v3

    #@8
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    #@b
    move-result v4

    #@c
    if-ge v3, v4, :cond_25

    #@e
    .line 226
    int-to-float v3, p1

    #@f
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    #@12
    move-result v4

    #@13
    int-to-float v4, v4

    #@14
    div-float v1, v3, v4

    #@16
    .line 230
    .local v1, scale:F
    :goto_16
    new-instance v0, Landroid/graphics/Matrix;

    #@18
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    #@1b
    .line 231
    .local v0, matrix:Landroid/graphics/Matrix;
    invoke-virtual {v0, v1, v1}, Landroid/graphics/Matrix;->setScale(FF)V

    #@1e
    .line 232
    or-int/lit8 v3, p3, 0x1

    #@20
    invoke-static {v0, p0, p1, p2, v3}, Landroid/media/ThumbnailUtils;->transform(Landroid/graphics/Matrix;Landroid/graphics/Bitmap;III)Landroid/graphics/Bitmap;

    #@23
    move-result-object v2

    #@24
    .line 234
    .local v2, thumbnail:Landroid/graphics/Bitmap;
    goto :goto_3

    #@25
    .line 228
    .end local v0           #matrix:Landroid/graphics/Matrix;
    .end local v1           #scale:F
    .end local v2           #thumbnail:Landroid/graphics/Bitmap;
    :cond_25
    int-to-float v3, p2

    #@26
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    #@29
    move-result v4

    #@2a
    int-to-float v4, v4

    #@2b
    div-float v1, v3, v4

    #@2d
    .restart local v1       #scale:F
    goto :goto_16
.end method

.method private static makeBitmap(IILandroid/net/Uri;Landroid/content/ContentResolver;Landroid/os/ParcelFileDescriptor;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .registers 13
    .parameter "minSideLength"
    .parameter "maxNumOfPixels"
    .parameter "uri"
    .parameter "cr"
    .parameter "pfd"
    .parameter "options"

    #@0
    .prologue
    const/4 v6, -0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 311
    const/4 v0, 0x0

    #@3
    .line 313
    .local v0, b:Landroid/graphics/Bitmap;
    if-nez p4, :cond_9

    #@5
    :try_start_5
    invoke-static {p2, p3}, Landroid/media/ThumbnailUtils;->makeInputStream(Landroid/net/Uri;Landroid/content/ContentResolver;)Landroid/os/ParcelFileDescriptor;
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_5b
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_8} :catch_4f

    #@8
    move-result-object p4

    #@9
    .line 314
    :cond_9
    if-nez p4, :cond_f

    #@b
    .line 336
    invoke-static {p4}, Landroid/media/ThumbnailUtils;->closeSilently(Landroid/os/ParcelFileDescriptor;)V

    #@e
    .line 338
    :goto_e
    return-object v4

    #@f
    .line 315
    :cond_f
    if-nez p5, :cond_17

    #@11
    :try_start_11
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    #@13
    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    #@16
    .end local p5
    .local v3, options:Landroid/graphics/BitmapFactory$Options;
    move-object p5, v3

    #@17
    .line 317
    .end local v3           #options:Landroid/graphics/BitmapFactory$Options;
    .restart local p5
    :cond_17
    invoke-virtual {p4}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@1a
    move-result-object v2

    #@1b
    .line 318
    .local v2, fd:Ljava/io/FileDescriptor;
    const/4 v5, 0x1

    #@1c
    iput v5, p5, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    #@1e
    .line 319
    const/4 v5, 0x1

    #@1f
    iput-boolean v5, p5, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    #@21
    .line 320
    const/4 v5, 0x0

    #@22
    invoke-static {v2, v5, p5}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@25
    .line 321
    iget-boolean v5, p5, Landroid/graphics/BitmapFactory$Options;->mCancel:Z

    #@27
    if-nez v5, :cond_31

    #@29
    iget v5, p5, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    #@2b
    if-eq v5, v6, :cond_31

    #@2d
    iget v5, p5, Landroid/graphics/BitmapFactory$Options;->outHeight:I
    :try_end_2f
    .catchall {:try_start_11 .. :try_end_2f} :catchall_5b
    .catch Ljava/lang/OutOfMemoryError; {:try_start_11 .. :try_end_2f} :catch_4f

    #@2f
    if-ne v5, v6, :cond_35

    #@31
    .line 336
    :cond_31
    invoke-static {p4}, Landroid/media/ThumbnailUtils;->closeSilently(Landroid/os/ParcelFileDescriptor;)V

    #@34
    goto :goto_e

    #@35
    .line 325
    :cond_35
    :try_start_35
    invoke-static {p5, p0, p1}, Landroid/media/ThumbnailUtils;->computeSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    #@38
    move-result v5

    #@39
    iput v5, p5, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    #@3b
    .line 327
    const/4 v5, 0x0

    #@3c
    iput-boolean v5, p5, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    #@3e
    .line 329
    const/4 v5, 0x0

    #@3f
    iput-boolean v5, p5, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    #@41
    .line 330
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@43
    iput-object v5, p5, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    #@45
    .line 331
    const/4 v5, 0x0

    #@46
    invoke-static {v2, v5, p5}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_49
    .catchall {:try_start_35 .. :try_end_49} :catchall_5b
    .catch Ljava/lang/OutOfMemoryError; {:try_start_35 .. :try_end_49} :catch_4f

    #@49
    move-result-object v0

    #@4a
    .line 336
    invoke-static {p4}, Landroid/media/ThumbnailUtils;->closeSilently(Landroid/os/ParcelFileDescriptor;)V

    #@4d
    move-object v4, v0

    #@4e
    .line 338
    goto :goto_e

    #@4f
    .line 332
    .end local v2           #fd:Ljava/io/FileDescriptor;
    :catch_4f
    move-exception v1

    #@50
    .line 333
    .local v1, ex:Ljava/lang/OutOfMemoryError;
    :try_start_50
    const-string v5, "ThumbnailUtils"

    #@52
    const-string v6, "Got oom exception "

    #@54
    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_57
    .catchall {:try_start_50 .. :try_end_57} :catchall_5b

    #@57
    .line 336
    invoke-static {p4}, Landroid/media/ThumbnailUtils;->closeSilently(Landroid/os/ParcelFileDescriptor;)V

    #@5a
    goto :goto_e

    #@5b
    .end local v1           #ex:Ljava/lang/OutOfMemoryError;
    :catchall_5b
    move-exception v4

    #@5c
    invoke-static {p4}, Landroid/media/ThumbnailUtils;->closeSilently(Landroid/os/ParcelFileDescriptor;)V

    #@5f
    throw v4
.end method

.method private static makeInputStream(Landroid/net/Uri;Landroid/content/ContentResolver;)Landroid/os/ParcelFileDescriptor;
    .registers 4
    .parameter "uri"
    .parameter "cr"

    #@0
    .prologue
    .line 353
    :try_start_0
    const-string/jumbo v1, "r"

    #@3
    invoke-virtual {p1, p0, v1}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_6} :catch_8

    #@6
    move-result-object v1

    #@7
    .line 355
    :goto_7
    return-object v1

    #@8
    .line 354
    :catch_8
    move-exception v0

    #@9
    .line 355
    .local v0, ex:Ljava/io/IOException;
    const/4 v1, 0x0

    #@a
    goto :goto_7
.end method

.method private static transform(Landroid/graphics/Matrix;Landroid/graphics/Bitmap;III)Landroid/graphics/Bitmap;
    .registers 35
    .parameter "scaler"
    .parameter "source"
    .parameter "targetWidth"
    .parameter "targetHeight"
    .parameter "options"

    #@0
    .prologue
    .line 367
    and-int/lit8 v3, p4, 0x1

    #@2
    if-eqz v3, :cond_91

    #@4
    const/16 v27, 0x1

    #@6
    .line 368
    .local v27, scaleUp:Z
    :goto_6
    and-int/lit8 v3, p4, 0x2

    #@8
    if-eqz v3, :cond_95

    #@a
    const/16 v25, 0x1

    #@c
    .line 370
    .local v25, recycle:Z
    :goto_c
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    #@f
    move-result v3

    #@10
    sub-int v16, v3, p2

    #@12
    .line 371
    .local v16, deltaX:I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    #@15
    move-result v3

    #@16
    sub-int v18, v3, p3

    #@18
    .line 372
    .local v18, deltaY:I
    if-nez v27, :cond_99

    #@1a
    if-ltz v16, :cond_1e

    #@1c
    if-gez v18, :cond_99

    #@1e
    .line 379
    :cond_1e
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@20
    move/from16 v0, p2

    #@22
    move/from16 v1, p3

    #@24
    invoke-static {v0, v1, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    #@27
    move-result-object v11

    #@28
    .line 381
    .local v11, b2:Landroid/graphics/Bitmap;
    new-instance v15, Landroid/graphics/Canvas;

    #@2a
    invoke-direct {v15, v11}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    #@2d
    .line 383
    .local v15, c:Landroid/graphics/Canvas;
    const/4 v3, 0x0

    #@2e
    div-int/lit8 v4, v16, 0x2

    #@30
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    #@33
    move-result v17

    #@34
    .line 384
    .local v17, deltaXHalf:I
    const/4 v3, 0x0

    #@35
    div-int/lit8 v4, v18, 0x2

    #@37
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    #@3a
    move-result v19

    #@3b
    .line 385
    .local v19, deltaYHalf:I
    new-instance v28, Landroid/graphics/Rect;

    #@3d
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    #@40
    move-result v3

    #@41
    move/from16 v0, p2

    #@43
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    #@46
    move-result v3

    #@47
    add-int v3, v3, v17

    #@49
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    #@4c
    move-result v4

    #@4d
    move/from16 v0, p3

    #@4f
    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    #@52
    move-result v4

    #@53
    add-int v4, v4, v19

    #@55
    move-object/from16 v0, v28

    #@57
    move/from16 v1, v17

    #@59
    move/from16 v2, v19

    #@5b
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    #@5e
    .line 390
    .local v28, src:Landroid/graphics/Rect;
    invoke-virtual/range {v28 .. v28}, Landroid/graphics/Rect;->width()I

    #@61
    move-result v3

    #@62
    sub-int v3, p2, v3

    #@64
    div-int/lit8 v21, v3, 0x2

    #@66
    .line 391
    .local v21, dstX:I
    invoke-virtual/range {v28 .. v28}, Landroid/graphics/Rect;->height()I

    #@69
    move-result v3

    #@6a
    sub-int v3, p3, v3

    #@6c
    div-int/lit8 v22, v3, 0x2

    #@6e
    .line 392
    .local v22, dstY:I
    new-instance v20, Landroid/graphics/Rect;

    #@70
    sub-int v3, p2, v21

    #@72
    sub-int v4, p3, v22

    #@74
    move-object/from16 v0, v20

    #@76
    move/from16 v1, v21

    #@78
    move/from16 v2, v22

    #@7a
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    #@7d
    .line 397
    .local v20, dst:Landroid/graphics/Rect;
    const/4 v3, 0x0

    #@7e
    move-object/from16 v0, p1

    #@80
    move-object/from16 v1, v28

    #@82
    move-object/from16 v2, v20

    #@84
    invoke-virtual {v15, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    #@87
    .line 398
    if-eqz v25, :cond_8c

    #@89
    .line 399
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->recycle()V

    #@8c
    .line 401
    :cond_8c
    const/4 v3, 0x0

    #@8d
    invoke-virtual {v15, v3}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    #@90
    .line 455
    .end local v15           #c:Landroid/graphics/Canvas;
    .end local v17           #deltaXHalf:I
    .end local v19           #deltaYHalf:I
    .end local v20           #dst:Landroid/graphics/Rect;
    .end local v21           #dstX:I
    .end local v22           #dstY:I
    .end local v28           #src:Landroid/graphics/Rect;
    :cond_90
    :goto_90
    return-object v11

    #@91
    .line 367
    .end local v11           #b2:Landroid/graphics/Bitmap;
    .end local v16           #deltaX:I
    .end local v18           #deltaY:I
    .end local v25           #recycle:Z
    .end local v27           #scaleUp:Z
    :cond_91
    const/16 v27, 0x0

    #@93
    goto/16 :goto_6

    #@95
    .line 368
    .restart local v27       #scaleUp:Z
    :cond_95
    const/16 v25, 0x0

    #@97
    goto/16 :goto_c

    #@99
    .line 404
    .restart local v16       #deltaX:I
    .restart local v18       #deltaY:I
    .restart local v25       #recycle:Z
    :cond_99
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    #@9c
    move-result v3

    #@9d
    int-to-float v14, v3

    #@9e
    .line 405
    .local v14, bitmapWidthF:F
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    #@a1
    move-result v3

    #@a2
    int-to-float v13, v3

    #@a3
    .line 407
    .local v13, bitmapHeightF:F
    div-float v12, v14, v13

    #@a5
    .line 408
    .local v12, bitmapAspect:F
    move/from16 v0, p2

    #@a7
    int-to-float v3, v0

    #@a8
    move/from16 v0, p3

    #@aa
    int-to-float v4, v0

    #@ab
    div-float v29, v3, v4

    #@ad
    .line 410
    .local v29, viewAspect:F
    cmpl-float v3, v12, v29

    #@af
    if-lez v3, :cond_11c

    #@b1
    .line 411
    move/from16 v0, p3

    #@b3
    int-to-float v3, v0

    #@b4
    div-float v26, v3, v13

    #@b6
    .line 412
    .local v26, scale:F
    const v3, 0x3f666666

    #@b9
    cmpg-float v3, v26, v3

    #@bb
    if-ltz v3, :cond_c3

    #@bd
    const/high16 v3, 0x3f80

    #@bf
    cmpl-float v3, v26, v3

    #@c1
    if-lez v3, :cond_119

    #@c3
    .line 413
    :cond_c3
    move-object/from16 v0, p0

    #@c5
    move/from16 v1, v26

    #@c7
    move/from16 v2, v26

    #@c9
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    #@cc
    .line 427
    :goto_cc
    if-eqz p0, :cond_13b

    #@ce
    .line 429
    const/4 v4, 0x0

    #@cf
    const/4 v5, 0x0

    #@d0
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    #@d3
    move-result v6

    #@d4
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    #@d7
    move-result v7

    #@d8
    const/4 v9, 0x1

    #@d9
    move-object/from16 v3, p1

    #@db
    move-object/from16 v8, p0

    #@dd
    invoke-static/range {v3 .. v9}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    #@e0
    move-result-object v10

    #@e1
    .line 435
    .local v10, b1:Landroid/graphics/Bitmap;
    :goto_e1
    if-eqz v25, :cond_ea

    #@e3
    move-object/from16 v0, p1

    #@e5
    if-eq v10, v0, :cond_ea

    #@e7
    .line 436
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->recycle()V

    #@ea
    .line 439
    :cond_ea
    const/4 v3, 0x0

    #@eb
    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    #@ee
    move-result v4

    #@ef
    sub-int v4, v4, p2

    #@f1
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    #@f4
    move-result v23

    #@f5
    .line 440
    .local v23, dx1:I
    const/4 v3, 0x0

    #@f6
    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    #@f9
    move-result v4

    #@fa
    sub-int v4, v4, p3

    #@fc
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    #@ff
    move-result v24

    #@100
    .line 442
    .local v24, dy1:I
    div-int/lit8 v3, v23, 0x2

    #@102
    div-int/lit8 v4, v24, 0x2

    #@104
    move/from16 v0, p2

    #@106
    move/from16 v1, p3

    #@108
    invoke-static {v10, v3, v4, v0, v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    #@10b
    move-result-object v11

    #@10c
    .line 449
    .restart local v11       #b2:Landroid/graphics/Bitmap;
    if-eq v11, v10, :cond_90

    #@10e
    .line 450
    if-nez v25, :cond_114

    #@110
    move-object/from16 v0, p1

    #@112
    if-eq v10, v0, :cond_90

    #@114
    .line 451
    :cond_114
    invoke-virtual {v10}, Landroid/graphics/Bitmap;->recycle()V

    #@117
    goto/16 :goto_90

    #@119
    .line 415
    .end local v10           #b1:Landroid/graphics/Bitmap;
    .end local v11           #b2:Landroid/graphics/Bitmap;
    .end local v23           #dx1:I
    .end local v24           #dy1:I
    :cond_119
    const/16 p0, 0x0

    #@11b
    goto :goto_cc

    #@11c
    .line 418
    .end local v26           #scale:F
    :cond_11c
    move/from16 v0, p2

    #@11e
    int-to-float v3, v0

    #@11f
    div-float v26, v3, v14

    #@121
    .line 419
    .restart local v26       #scale:F
    const v3, 0x3f666666

    #@124
    cmpg-float v3, v26, v3

    #@126
    if-ltz v3, :cond_12e

    #@128
    const/high16 v3, 0x3f80

    #@12a
    cmpl-float v3, v26, v3

    #@12c
    if-lez v3, :cond_138

    #@12e
    .line 420
    :cond_12e
    move-object/from16 v0, p0

    #@130
    move/from16 v1, v26

    #@132
    move/from16 v2, v26

    #@134
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    #@137
    goto :goto_cc

    #@138
    .line 422
    :cond_138
    const/16 p0, 0x0

    #@13a
    goto :goto_cc

    #@13b
    .line 432
    :cond_13b
    move-object/from16 v10, p1

    #@13d
    .restart local v10       #b1:Landroid/graphics/Bitmap;
    goto :goto_e1
.end method
