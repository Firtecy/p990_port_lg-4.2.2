.class public Landroid/media/MediaMetadataRetriever;
.super Ljava/lang/Object;
.source "MediaMetadataRetriever.java"


# static fields
.field private static final EMBEDDED_PICTURE_TYPE_ANY:I = 0xffff

.field public static final METADATA_KEY_ALBUM:I = 0x1

.field public static final METADATA_KEY_ALBUMARTIST:I = 0xd

.field public static final METADATA_KEY_ARTIST:I = 0x2

.field public static final METADATA_KEY_AUTHOR:I = 0x3

.field public static final METADATA_KEY_BITRATE:I = 0x14

.field public static final METADATA_KEY_CD_TRACK_NUMBER:I = 0x0

.field public static final METADATA_KEY_COMPILATION:I = 0xf

.field public static final METADATA_KEY_COMPOSER:I = 0x4

.field public static final METADATA_KEY_DATE:I = 0x5

.field public static final METADATA_KEY_DISC_NUMBER:I = 0xe

.field public static final METADATA_KEY_DURATION:I = 0x9

.field public static final METADATA_KEY_GENRE:I = 0x6

.field public static final METADATA_KEY_HAS_AUDIO:I = 0x10

.field public static final METADATA_KEY_HAS_VIDEO:I = 0x11

.field public static final METADATA_KEY_IS_DRM:I = 0x16

.field public static final METADATA_KEY_LGE_VIDEO_CONTENT:I = 0x64

.field public static final METADATA_KEY_LOCATION:I = 0x17

.field public static final METADATA_KEY_MIMETYPE:I = 0xc

.field public static final METADATA_KEY_NUM_TRACKS:I = 0xa

.field public static final METADATA_KEY_TIMED_TEXT_LANGUAGES:I = 0x15

.field public static final METADATA_KEY_TITLE:I = 0x7

.field public static final METADATA_KEY_VIDEO_HEIGHT:I = 0x13

.field public static final METADATA_KEY_VIDEO_ROTATION:I = 0x18

.field public static final METADATA_KEY_VIDEO_WIDTH:I = 0x12

.field public static final METADATA_KEY_WRITER:I = 0xb

.field public static final METADATA_KEY_YEAR:I = 0x8

.field public static final OPTION_CLOSEST:I = 0x3

.field public static final OPTION_CLOSEST_SYNC:I = 0x2

.field public static final OPTION_NEXT_SYNC:I = 0x1

.field public static final OPTION_PREVIOUS_SYNC:I


# instance fields
.field private mNativeContext:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 39
    const-string/jumbo v0, "media_jni"

    #@3
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@6
    .line 40
    invoke-static {}, Landroid/media/MediaMetadataRetriever;->native_init()V

    #@9
    .line 41
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 49
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 50
    invoke-direct {p0}, Landroid/media/MediaMetadataRetriever;->native_setup()V

    #@6
    .line 51
    return-void
.end method

.method private native _getFrameAtTime(JI)Landroid/graphics/Bitmap;
.end method

.method private native _setDataSource(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation
.end method

.method private native getEmbeddedPicture(I)[B
.end method

.method private final native native_finalize()V
.end method

.method private static native native_init()V
.end method

.method private native native_setup()V
.end method


# virtual methods
.method public native extractMetadata(I)Ljava/lang/String;
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 317
    :try_start_0
    invoke-direct {p0}, Landroid/media/MediaMetadataRetriever;->native_finalize()V
    :try_end_3
    .catchall {:try_start_0 .. :try_end_3} :catchall_7

    #@3
    .line 319
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@6
    .line 321
    return-void

    #@7
    .line 319
    :catchall_7
    move-exception v0

    #@8
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@b
    throw v0
.end method

.method public getEmbeddedPicture()[B
    .registers 2

    #@0
    .prologue
    .line 299
    const v0, 0xffff

    #@3
    invoke-direct {p0, v0}, Landroid/media/MediaMetadataRetriever;->getEmbeddedPicture(I)[B

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getFrameAtTime()Landroid/graphics/Bitmap;
    .registers 4

    #@0
    .prologue
    .line 285
    const-wide/16 v0, -0x1

    #@2
    const/4 v2, 0x2

    #@3
    invoke-virtual {p0, v0, v1, v2}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(JI)Landroid/graphics/Bitmap;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getFrameAtTime(J)Landroid/graphics/Bitmap;
    .registers 4
    .parameter "timeUs"

    #@0
    .prologue
    .line 267
    const/4 v0, 0x2

    #@1
    invoke-virtual {p0, p1, p2, v0}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(JI)Landroid/graphics/Bitmap;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public getFrameAtTime(JI)Landroid/graphics/Bitmap;
    .registers 7
    .parameter "timeUs"
    .parameter "option"

    #@0
    .prologue
    .line 238
    if-ltz p3, :cond_5

    #@2
    const/4 v0, 0x3

    #@3
    if-le p3, v0, :cond_1e

    #@5
    .line 240
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "Unsupported option: "

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v0

    #@1e
    .line 243
    :cond_1e
    invoke-direct {p0, p1, p2, p3}, Landroid/media/MediaMetadataRetriever;->_getFrameAtTime(JI)Landroid/graphics/Bitmap;

    #@21
    move-result-object v0

    #@22
    return-object v0
.end method

.method public native release()V
.end method

.method public setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    .registers 13
    .parameter "context"
    .parameter "uri"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 149
    if-nez p2, :cond_8

    #@2
    .line 150
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@7
    throw v0

    #@8
    .line 153
    :cond_8
    invoke-virtual {p2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@b
    move-result-object v9

    #@c
    .line 154
    .local v9, scheme:Ljava/lang/String;
    if-eqz v9, :cond_16

    #@e
    const-string v0, "file"

    #@10
    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v0

    #@14
    if-eqz v0, :cond_1e

    #@16
    .line 155
    :cond_16
    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {p0, v0}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    #@1d
    .line 193
    :cond_1d
    :goto_1d
    return-void

    #@1e
    .line 159
    :cond_1e
    const/4 v7, 0x0

    #@1f
    .line 161
    .local v7, fd:Landroid/content/res/AssetFileDescriptor;
    :try_start_1f
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
    :try_end_22
    .catchall {:try_start_1f .. :try_end_22} :catchall_47
    .catch Ljava/lang/SecurityException; {:try_start_1f .. :try_end_22} :catch_32

    #@22
    move-result-object v8

    #@23
    .line 163
    .local v8, resolver:Landroid/content/ContentResolver;
    :try_start_23
    const-string/jumbo v0, "r"

    #@26
    invoke-virtual {v8, p2, v0}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    :try_end_29
    .catchall {:try_start_23 .. :try_end_29} :catchall_47
    .catch Ljava/io/FileNotFoundException; {:try_start_23 .. :try_end_29} :catch_40
    .catch Ljava/lang/SecurityException; {:try_start_23 .. :try_end_29} :catch_32

    #@29
    move-result-object v7

    #@2a
    .line 167
    if-nez v7, :cond_4e

    #@2c
    .line 168
    :try_start_2c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@2e
    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@31
    throw v0
    :try_end_32
    .catchall {:try_start_2c .. :try_end_32} :catchall_47
    .catch Ljava/lang/SecurityException; {:try_start_2c .. :try_end_32} :catch_32

    #@32
    .line 183
    .end local v8           #resolver:Landroid/content/ContentResolver;
    :catch_32
    move-exception v0

    #@33
    .line 186
    if-eqz v7, :cond_38

    #@35
    .line 187
    :try_start_35
    invoke-virtual {v7}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_38
    .catch Ljava/io/IOException; {:try_start_35 .. :try_end_38} :catch_80

    #@38
    .line 192
    :cond_38
    :goto_38
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@3b
    move-result-object v0

    #@3c
    invoke-virtual {p0, v0}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    #@3f
    goto :goto_1d

    #@40
    .line 164
    .restart local v8       #resolver:Landroid/content/ContentResolver;
    :catch_40
    move-exception v6

    #@41
    .line 165
    .local v6, e:Ljava/io/FileNotFoundException;
    :try_start_41
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@43
    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@46
    throw v0
    :try_end_47
    .catchall {:try_start_41 .. :try_end_47} :catchall_47
    .catch Ljava/lang/SecurityException; {:try_start_41 .. :try_end_47} :catch_32

    #@47
    .line 185
    .end local v6           #e:Ljava/io/FileNotFoundException;
    .end local v8           #resolver:Landroid/content/ContentResolver;
    :catchall_47
    move-exception v0

    #@48
    .line 186
    if-eqz v7, :cond_4d

    #@4a
    .line 187
    :try_start_4a
    invoke-virtual {v7}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_4d
    .catch Ljava/io/IOException; {:try_start_4a .. :try_end_4d} :catch_82

    #@4d
    .line 190
    :cond_4d
    :goto_4d
    throw v0

    #@4e
    .line 170
    .restart local v8       #resolver:Landroid/content/ContentResolver;
    :cond_4e
    :try_start_4e
    invoke-virtual {v7}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@51
    move-result-object v1

    #@52
    .line 171
    .local v1, descriptor:Ljava/io/FileDescriptor;
    invoke-virtual {v1}, Ljava/io/FileDescriptor;->valid()Z

    #@55
    move-result v0

    #@56
    if-nez v0, :cond_5e

    #@58
    .line 172
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@5a
    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@5d
    throw v0

    #@5e
    .line 177
    :cond_5e
    invoke-virtual {v7}, Landroid/content/res/AssetFileDescriptor;->getDeclaredLength()J

    #@61
    move-result-wide v2

    #@62
    const-wide/16 v4, 0x0

    #@64
    cmp-long v0, v2, v4

    #@66
    if-gez v0, :cond_73

    #@68
    .line 178
    invoke-virtual {p0, v1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/io/FileDescriptor;)V
    :try_end_6b
    .catchall {:try_start_4e .. :try_end_6b} :catchall_47
    .catch Ljava/lang/SecurityException; {:try_start_4e .. :try_end_6b} :catch_32

    #@6b
    .line 186
    :goto_6b
    if-eqz v7, :cond_1d

    #@6d
    .line 187
    :try_start_6d
    invoke-virtual {v7}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_70
    .catch Ljava/io/IOException; {:try_start_6d .. :try_end_70} :catch_71

    #@70
    goto :goto_1d

    #@71
    .line 189
    :catch_71
    move-exception v0

    #@72
    goto :goto_1d

    #@73
    .line 180
    :cond_73
    :try_start_73
    invoke-virtual {v7}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    #@76
    move-result-wide v2

    #@77
    invoke-virtual {v7}, Landroid/content/res/AssetFileDescriptor;->getDeclaredLength()J

    #@7a
    move-result-wide v4

    #@7b
    move-object v0, p0

    #@7c
    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/io/FileDescriptor;JJ)V
    :try_end_7f
    .catchall {:try_start_73 .. :try_end_7f} :catchall_47
    .catch Ljava/lang/SecurityException; {:try_start_73 .. :try_end_7f} :catch_32

    #@7f
    goto :goto_6b

    #@80
    .line 189
    .end local v1           #descriptor:Ljava/io/FileDescriptor;
    .end local v8           #resolver:Landroid/content/ContentResolver;
    :catch_80
    move-exception v0

    #@81
    goto :goto_38

    #@82
    :catch_82
    move-exception v2

    #@83
    goto :goto_4d
.end method

.method public setDataSource(Ljava/io/FileDescriptor;)V
    .registers 8
    .parameter "fd"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 134
    const-wide/16 v2, 0x0

    #@2
    const-wide v4, 0x7ffffffffffffffL

    #@7
    move-object v0, p0

    #@8
    move-object v1, p1

    #@9
    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/io/FileDescriptor;JJ)V

    #@c
    .line 135
    return-void
.end method

.method public native setDataSource(Ljava/io/FileDescriptor;JJ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation
.end method

.method public setDataSource(Ljava/lang/String;)V
    .registers 12
    .parameter "path"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 62
    const/4 v8, 0x0

    #@1
    .line 64
    .local v8, is:Ljava/io/FileInputStream;
    :try_start_1
    new-instance v9, Ljava/io/FileInputStream;

    #@3
    invoke-direct {v9, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_6} :catch_1b
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_6} :catch_22

    #@6
    .line 65
    .end local v8           #is:Ljava/io/FileInputStream;
    .local v9, is:Ljava/io/FileInputStream;
    :try_start_6
    invoke-virtual {v9}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    #@9
    move-result-object v1

    #@a
    .line 66
    .local v1, fd:Ljava/io/FileDescriptor;
    const-wide/16 v2, 0x0

    #@c
    const-wide v4, 0x7ffffffffffffffL

    #@11
    move-object v0, p0

    #@12
    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/io/FileDescriptor;JJ)V
    :try_end_15
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_15} :catch_2e
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_15} :catch_2b

    #@15
    .line 74
    if-eqz v9, :cond_1a

    #@17
    .line 75
    :try_start_17
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_1a} :catch_29

    #@1a
    .line 78
    :cond_1a
    :goto_1a
    return-void

    #@1b
    .line 67
    .end local v1           #fd:Ljava/io/FileDescriptor;
    .end local v9           #is:Ljava/io/FileInputStream;
    .restart local v8       #is:Ljava/io/FileInputStream;
    :catch_1b
    move-exception v6

    #@1c
    .line 68
    .local v6, fileEx:Ljava/io/FileNotFoundException;
    :goto_1c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1e
    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@21
    throw v0

    #@22
    .line 69
    .end local v6           #fileEx:Ljava/io/FileNotFoundException;
    :catch_22
    move-exception v7

    #@23
    .line 70
    .local v7, ioEx:Ljava/io/IOException;
    :goto_23
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@25
    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@28
    throw v0

    #@29
    .line 77
    .end local v7           #ioEx:Ljava/io/IOException;
    .end local v8           #is:Ljava/io/FileInputStream;
    .restart local v1       #fd:Ljava/io/FileDescriptor;
    .restart local v9       #is:Ljava/io/FileInputStream;
    :catch_29
    move-exception v0

    #@2a
    goto :goto_1a

    #@2b
    .line 69
    .end local v1           #fd:Ljava/io/FileDescriptor;
    :catch_2b
    move-exception v7

    #@2c
    move-object v8, v9

    #@2d
    .end local v9           #is:Ljava/io/FileInputStream;
    .restart local v8       #is:Ljava/io/FileInputStream;
    goto :goto_23

    #@2e
    .line 67
    .end local v8           #is:Ljava/io/FileInputStream;
    .restart local v9       #is:Ljava/io/FileInputStream;
    :catch_2e
    move-exception v6

    #@2f
    move-object v8, v9

    #@30
    .end local v9           #is:Ljava/io/FileInputStream;
    .restart local v8       #is:Ljava/io/FileInputStream;
    goto :goto_1c
.end method

.method public setDataSource(Ljava/lang/String;Ljava/util/Map;)V
    .registers 9
    .parameter "uri"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 91
    .local p2, headers:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v1, 0x0

    #@1
    .line 92
    .local v1, i:I
    invoke-interface {p2}, Ljava/util/Map;->size()I

    #@4
    move-result v5

    #@5
    new-array v3, v5, [Ljava/lang/String;

    #@7
    .line 93
    .local v3, keys:[Ljava/lang/String;
    invoke-interface {p2}, Ljava/util/Map;->size()I

    #@a
    move-result v5

    #@b
    new-array v4, v5, [Ljava/lang/String;

    #@d
    .line 94
    .local v4, values:[Ljava/lang/String;
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@10
    move-result-object v5

    #@11
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@14
    move-result-object v2

    #@15
    .local v2, i$:Ljava/util/Iterator;
    :goto_15
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@18
    move-result v5

    #@19
    if-eqz v5, :cond_34

    #@1b
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1e
    move-result-object v0

    #@1f
    check-cast v0, Ljava/util/Map$Entry;

    #@21
    .line 95
    .local v0, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@24
    move-result-object v5

    #@25
    check-cast v5, Ljava/lang/String;

    #@27
    aput-object v5, v3, v1

    #@29
    .line 96
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@2c
    move-result-object v5

    #@2d
    check-cast v5, Ljava/lang/String;

    #@2f
    aput-object v5, v4, v1

    #@31
    .line 97
    add-int/lit8 v1, v1, 0x1

    #@33
    goto :goto_15

    #@34
    .line 99
    .end local v0           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_34
    invoke-direct {p0, p1, v3, v4}, Landroid/media/MediaMetadataRetriever;->_setDataSource(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    #@37
    .line 100
    return-void
.end method
