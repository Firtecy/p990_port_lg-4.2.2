.class Landroid/media/SmartRingtone$3;
.super Ljava/lang/Object;
.source "SmartRingtone.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/media/SmartRingtone;->onNoiseEstimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/media/SmartRingtone;


# direct methods
.method constructor <init>(Landroid/media/SmartRingtone;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 196
    iput-object p1, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 15

    #@0
    .prologue
    .line 199
    iget-object v10, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@2
    iget-object v10, v10, Landroid/media/SmartRingtone;->arec:Landroid/media/AudioRecord;

    #@4
    invoke-virtual {v10}, Landroid/media/AudioRecord;->getRecordingState()I

    #@7
    move-result v10

    #@8
    const/4 v11, 0x1

    #@9
    if-ne v10, v11, :cond_4a

    #@b
    .line 200
    invoke-static {}, Landroid/media/SmartRingtone;->access$000()Ljava/lang/String;

    #@e
    move-result-object v10

    #@f
    const-string v11, "arec startRecording cannot started"

    #@11
    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 201
    iget-object v10, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@16
    iget-object v10, v10, Landroid/media/SmartRingtone;->arec:Landroid/media/AudioRecord;

    #@18
    invoke-virtual {v10}, Landroid/media/AudioRecord;->stop()V

    #@1b
    .line 202
    invoke-static {}, Landroid/media/SmartRingtone;->access$000()Ljava/lang/String;

    #@1e
    move-result-object v10

    #@1f
    new-instance v11, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v12, "arec.stop() getRecordingState = "

    #@26
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v11

    #@2a
    iget-object v12, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@2c
    iget-object v12, v12, Landroid/media/SmartRingtone;->arec:Landroid/media/AudioRecord;

    #@2e
    invoke-virtual {v12}, Landroid/media/AudioRecord;->getRecordingState()I

    #@31
    move-result v12

    #@32
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v11

    #@36
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v11

    #@3a
    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 203
    iget-object v10, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@3f
    iget-object v10, v10, Landroid/media/SmartRingtone;->arec:Landroid/media/AudioRecord;

    #@41
    invoke-virtual {v10}, Landroid/media/AudioRecord;->release()V

    #@44
    .line 204
    iget-object v10, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@46
    const/4 v11, 0x0

    #@47
    iput-object v11, v10, Landroid/media/SmartRingtone;->arec:Landroid/media/AudioRecord;

    #@49
    .line 284
    :cond_49
    :goto_49
    return-void

    #@4a
    .line 208
    :cond_4a
    const/16 v10, 0xf

    #@4c
    new-array v7, v10, [I

    #@4e
    .line 209
    .local v7, tempAverage:[I
    const/4 v8, 0x0

    #@4f
    .line 210
    .local v8, totalFrameCount:I
    const/4 v9, 0x0

    #@50
    .line 216
    .local v9, validFrameCount:I
    const/4 v1, 0x0

    #@51
    .line 217
    .local v1, filtInBuf:S
    const/4 v3, 0x0

    #@52
    .line 219
    .local v3, filtOutBuf:S
    :goto_52
    :try_start_52
    iget-object v10, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@54
    invoke-static {v10}, Landroid/media/SmartRingtone;->access$900(Landroid/media/SmartRingtone;)Z

    #@57
    move-result v10

    #@58
    if-nez v10, :cond_19f

    #@5a
    const/16 v10, 0xf

    #@5c
    if-ge v9, v10, :cond_19f

    #@5e
    .line 220
    const/4 v10, 0x0

    #@5f
    aput v10, v7, v9

    #@61
    .line 221
    iget-object v10, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@63
    iget-object v10, v10, Landroid/media/SmartRingtone;->arec:Landroid/media/AudioRecord;

    #@65
    iget-object v11, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@67
    iget-object v11, v11, Landroid/media/SmartRingtone;->buffer:[S

    #@69
    const/4 v12, 0x0

    #@6a
    iget-object v13, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@6c
    iget v13, v13, Landroid/media/SmartRingtone;->buffersize:I

    #@6e
    invoke-virtual {v10, v11, v12, v13}, Landroid/media/AudioRecord;->read([SII)I

    #@71
    .line 224
    const/4 v10, 0x1

    #@72
    if-lt v8, v10, :cond_19b

    #@74
    .line 225
    const/4 v5, 0x0

    #@75
    .local v5, j:I
    :goto_75
    iget-object v10, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@77
    iget v10, v10, Landroid/media/SmartRingtone;->buffersize:I

    #@79
    if-ge v5, v10, :cond_190

    #@7b
    .line 228
    iget-object v10, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@7d
    iget-object v10, v10, Landroid/media/SmartRingtone;->buffer:[S

    #@7f
    aget-short v10, v10, v5

    #@81
    invoke-static {}, Landroid/media/SmartRingtone;->access$1000()[S

    #@84
    move-result-object v11

    #@85
    const/4 v12, 0x0

    #@86
    aget-short v11, v11, v12

    #@88
    mul-int/2addr v10, v11

    #@89
    invoke-static {}, Landroid/media/SmartRingtone;->access$1000()[S

    #@8c
    move-result-object v11

    #@8d
    const/4 v12, 0x1

    #@8e
    aget-short v11, v11, v12

    #@90
    mul-int/2addr v11, v1

    #@91
    add-int/2addr v10, v11

    #@92
    mul-int/lit16 v11, v3, -0x60e4

    #@94
    sub-int v2, v10, v11

    #@96
    .line 229
    .local v2, filtOut:I
    iget-object v10, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@98
    iget-object v10, v10, Landroid/media/SmartRingtone;->buffer:[S

    #@9a
    aget-short v1, v10, v5

    #@9c
    .line 230
    shr-int/lit8 v10, v2, 0xf

    #@9e
    int-to-short v3, v10

    #@9f
    .line 232
    if-ltz v3, :cond_a9

    #@a1
    .line 233
    aget v10, v7, v9

    #@a3
    add-int/2addr v10, v3

    #@a4
    aput v10, v7, v9

    #@a6
    .line 225
    :goto_a6
    add-int/lit8 v5, v5, 0x1

    #@a8
    goto :goto_75

    #@a9
    .line 235
    :cond_a9
    aget v10, v7, v9

    #@ab
    neg-int v11, v3

    #@ac
    add-int/2addr v10, v11

    #@ad
    aput v10, v7, v9
    :try_end_af
    .catchall {:try_start_52 .. :try_end_af} :catchall_250
    .catch Ljava/lang/Exception; {:try_start_52 .. :try_end_af} :catch_b0

    #@af
    goto :goto_a6

    #@b0
    .line 261
    .end local v2           #filtOut:I
    .end local v5           #j:I
    :catch_b0
    move-exception v0

    #@b1
    .line 262
    .local v0, ex:Ljava/lang/Exception;
    :try_start_b1
    invoke-static {}, Landroid/media/SmartRingtone;->access$000()Ljava/lang/String;

    #@b4
    move-result-object v10

    #@b5
    const-string/jumbo v11, "smart ringtone caught "

    #@b8
    invoke-static {v10, v11, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_bb
    .catchall {:try_start_b1 .. :try_end_bb} :catchall_250

    #@bb
    .line 264
    iget-object v10, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@bd
    iget-object v10, v10, Landroid/media/SmartRingtone;->arec:Landroid/media/AudioRecord;

    #@bf
    invoke-virtual {v10}, Landroid/media/AudioRecord;->stop()V

    #@c2
    .line 265
    invoke-static {}, Landroid/media/SmartRingtone;->access$800()Z

    #@c5
    move-result v10

    #@c6
    if-eqz v10, :cond_ea

    #@c8
    invoke-static {}, Landroid/media/SmartRingtone;->access$000()Ljava/lang/String;

    #@cb
    move-result-object v10

    #@cc
    new-instance v11, Ljava/lang/StringBuilder;

    #@ce
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@d1
    const-string v12, "arec.stop() getRecordingState = "

    #@d3
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v11

    #@d7
    iget-object v12, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@d9
    iget-object v12, v12, Landroid/media/SmartRingtone;->arec:Landroid/media/AudioRecord;

    #@db
    invoke-virtual {v12}, Landroid/media/AudioRecord;->getRecordingState()I

    #@de
    move-result v12

    #@df
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v11

    #@e3
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e6
    move-result-object v11

    #@e7
    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@ea
    .line 266
    :cond_ea
    iget-object v10, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@ec
    iget-object v10, v10, Landroid/media/SmartRingtone;->arec:Landroid/media/AudioRecord;

    #@ee
    invoke-virtual {v10}, Landroid/media/AudioRecord;->release()V

    #@f1
    .line 267
    invoke-static {}, Landroid/media/SmartRingtone;->access$800()Z

    #@f4
    move-result v10

    #@f5
    if-eqz v10, :cond_119

    #@f7
    invoke-static {}, Landroid/media/SmartRingtone;->access$000()Ljava/lang/String;

    #@fa
    move-result-object v10

    #@fb
    new-instance v11, Ljava/lang/StringBuilder;

    #@fd
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@100
    const-string v12, "arec.release() getState = "

    #@102
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@105
    move-result-object v11

    #@106
    iget-object v12, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@108
    iget-object v12, v12, Landroid/media/SmartRingtone;->arec:Landroid/media/AudioRecord;

    #@10a
    invoke-virtual {v12}, Landroid/media/AudioRecord;->getState()I

    #@10d
    move-result v12

    #@10e
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@111
    move-result-object v11

    #@112
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@115
    move-result-object v11

    #@116
    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@119
    .line 268
    :cond_119
    iget-object v10, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@11b
    .end local v0           #ex:Ljava/lang/Exception;
    :goto_11b
    const/4 v11, 0x0

    #@11c
    iput-object v11, v10, Landroid/media/SmartRingtone;->arec:Landroid/media/AudioRecord;

    #@11e
    .line 271
    iget-object v10, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@120
    invoke-static {v10}, Landroid/media/SmartRingtone;->access$400(Landroid/media/SmartRingtone;)I

    #@123
    move-result v10

    #@124
    const/4 v11, 0x2

    #@125
    if-ne v10, v11, :cond_2b5

    #@127
    iget-object v10, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@129
    invoke-static {v10}, Landroid/media/SmartRingtone;->access$500(Landroid/media/SmartRingtone;)Landroid/media/AudioManager;

    #@12c
    move-result-object v10

    #@12d
    invoke-virtual {v10}, Landroid/media/AudioManager;->getRingerMode()I

    #@130
    move-result v10

    #@131
    const/4 v11, 0x2

    #@132
    if-ne v10, v11, :cond_2b5

    #@134
    iget-object v10, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@136
    invoke-static {v10}, Landroid/media/SmartRingtone;->access$500(Landroid/media/SmartRingtone;)Landroid/media/AudioManager;

    #@139
    move-result-object v10

    #@13a
    iget-object v11, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@13c
    invoke-static {v11}, Landroid/media/SmartRingtone;->access$400(Landroid/media/SmartRingtone;)I

    #@13f
    move-result v11

    #@140
    invoke-virtual {v10, v11}, Landroid/media/AudioManager;->getDevicesForStream(I)I

    #@143
    move-result v10

    #@144
    const/4 v11, 0x2

    #@145
    if-ne v10, v11, :cond_2b5

    #@147
    .line 274
    iget-object v10, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@149
    invoke-static {}, Landroid/media/SmartRingtone;->access$1100()I

    #@14c
    move-result v11

    #@14d
    invoke-static {v10, v11}, Landroid/media/SmartRingtone;->access$1200(Landroid/media/SmartRingtone;I)V

    #@150
    .line 275
    invoke-static {}, Landroid/media/SmartRingtone;->access$000()Ljava/lang/String;

    #@153
    move-result-object v10

    #@154
    new-instance v11, Ljava/lang/StringBuilder;

    #@156
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@159
    const-string v12, "calculateSmartRingtoneLevel: mSmartRingtoneLevel = "

    #@15b
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15e
    move-result-object v11

    #@15f
    iget-object v12, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@161
    invoke-static {v12}, Landroid/media/SmartRingtone;->access$200(Landroid/media/SmartRingtone;)I

    #@164
    move-result v12

    #@165
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@168
    move-result-object v11

    #@169
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16c
    move-result-object v11

    #@16d
    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@170
    .line 277
    iget-object v10, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@172
    invoke-static {v10}, Landroid/media/SmartRingtone;->access$1300(Landroid/media/SmartRingtone;)Landroid/os/Handler;

    #@175
    move-result-object v10

    #@176
    if-eqz v10, :cond_49

    #@178
    .line 278
    invoke-static {}, Landroid/media/SmartRingtone;->access$000()Ljava/lang/String;

    #@17b
    move-result-object v10

    #@17c
    const-string/jumbo v11, "mDelayedVolumeUpHandler.sendEmptyMessageDelayed"

    #@17f
    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@182
    .line 279
    iget-object v10, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@184
    invoke-static {v10}, Landroid/media/SmartRingtone;->access$1300(Landroid/media/SmartRingtone;)Landroid/os/Handler;

    #@187
    move-result-object v10

    #@188
    const/4 v11, 0x0

    #@189
    const-wide/16 v12, 0xdac

    #@18b
    invoke-virtual {v10, v11, v12, v13}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@18e
    goto/16 :goto_49

    #@190
    .line 238
    .restart local v5       #j:I
    :cond_190
    :try_start_190
    aget v10, v7, v9

    #@192
    iget-object v11, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@194
    iget v11, v11, Landroid/media/SmartRingtone;->buffersize:I

    #@196
    div-int/2addr v10, v11

    #@197
    aput v10, v7, v9

    #@199
    .line 240
    add-int/lit8 v9, v9, 0x1

    #@19b
    .line 243
    .end local v5           #j:I
    :cond_19b
    add-int/lit8 v8, v8, 0x1

    #@19d
    goto/16 :goto_52

    #@19f
    .line 246
    :cond_19f
    if-eqz v9, :cond_1c0

    #@1a1
    .line 247
    invoke-static {}, Landroid/media/SmartRingtone;->access$1100()I

    #@1a4
    move-result v6

    #@1a5
    .line 249
    .local v6, prevNoiseAverage:I
    const/4 v10, 0x0

    #@1a6
    invoke-static {v10}, Landroid/media/SmartRingtone;->access$1102(I)I

    #@1a9
    .line 250
    const/4 v4, 0x0

    #@1aa
    .local v4, i:I
    :goto_1aa
    if-ge v4, v9, :cond_1b4

    #@1ac
    .line 251
    aget v10, v7, v4

    #@1ae
    invoke-static {v10}, Landroid/media/SmartRingtone;->access$1112(I)I

    #@1b1
    .line 250
    add-int/lit8 v4, v4, 0x1

    #@1b3
    goto :goto_1aa

    #@1b4
    .line 253
    :cond_1b4
    invoke-static {v9}, Landroid/media/SmartRingtone;->access$1136(I)I

    #@1b7
    .line 255
    invoke-static {}, Landroid/media/SmartRingtone;->access$1100()I

    #@1ba
    move-result v10

    #@1bb
    if-nez v10, :cond_1c0

    #@1bd
    .line 256
    invoke-static {v6}, Landroid/media/SmartRingtone;->access$1102(I)I

    #@1c0
    .line 259
    .end local v4           #i:I
    .end local v6           #prevNoiseAverage:I
    :cond_1c0
    invoke-static {}, Landroid/media/SmartRingtone;->access$800()Z

    #@1c3
    move-result v10

    #@1c4
    if-eqz v10, :cond_1ee

    #@1c6
    invoke-static {}, Landroid/media/SmartRingtone;->access$000()Ljava/lang/String;

    #@1c9
    move-result-object v10

    #@1ca
    new-instance v11, Ljava/lang/StringBuilder;

    #@1cc
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@1cf
    const-string v12, " mNoiseAverage = "

    #@1d1
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d4
    move-result-object v11

    #@1d5
    invoke-static {}, Landroid/media/SmartRingtone;->access$1100()I

    #@1d8
    move-result v12

    #@1d9
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1dc
    move-result-object v11

    #@1dd
    const-string v12, " frameCount = "

    #@1df
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e2
    move-result-object v11

    #@1e3
    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e6
    move-result-object v11

    #@1e7
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ea
    move-result-object v11

    #@1eb
    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1ee
    .catchall {:try_start_190 .. :try_end_1ee} :catchall_250
    .catch Ljava/lang/Exception; {:try_start_190 .. :try_end_1ee} :catch_b0

    #@1ee
    .line 264
    :cond_1ee
    iget-object v10, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@1f0
    iget-object v10, v10, Landroid/media/SmartRingtone;->arec:Landroid/media/AudioRecord;

    #@1f2
    invoke-virtual {v10}, Landroid/media/AudioRecord;->stop()V

    #@1f5
    .line 265
    invoke-static {}, Landroid/media/SmartRingtone;->access$800()Z

    #@1f8
    move-result v10

    #@1f9
    if-eqz v10, :cond_21d

    #@1fb
    invoke-static {}, Landroid/media/SmartRingtone;->access$000()Ljava/lang/String;

    #@1fe
    move-result-object v10

    #@1ff
    new-instance v11, Ljava/lang/StringBuilder;

    #@201
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@204
    const-string v12, "arec.stop() getRecordingState = "

    #@206
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@209
    move-result-object v11

    #@20a
    iget-object v12, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@20c
    iget-object v12, v12, Landroid/media/SmartRingtone;->arec:Landroid/media/AudioRecord;

    #@20e
    invoke-virtual {v12}, Landroid/media/AudioRecord;->getRecordingState()I

    #@211
    move-result v12

    #@212
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@215
    move-result-object v11

    #@216
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@219
    move-result-object v11

    #@21a
    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@21d
    .line 266
    :cond_21d
    iget-object v10, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@21f
    iget-object v10, v10, Landroid/media/SmartRingtone;->arec:Landroid/media/AudioRecord;

    #@221
    invoke-virtual {v10}, Landroid/media/AudioRecord;->release()V

    #@224
    .line 267
    invoke-static {}, Landroid/media/SmartRingtone;->access$800()Z

    #@227
    move-result v10

    #@228
    if-eqz v10, :cond_24c

    #@22a
    invoke-static {}, Landroid/media/SmartRingtone;->access$000()Ljava/lang/String;

    #@22d
    move-result-object v10

    #@22e
    new-instance v11, Ljava/lang/StringBuilder;

    #@230
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@233
    const-string v12, "arec.release() getState = "

    #@235
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@238
    move-result-object v11

    #@239
    iget-object v12, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@23b
    iget-object v12, v12, Landroid/media/SmartRingtone;->arec:Landroid/media/AudioRecord;

    #@23d
    invoke-virtual {v12}, Landroid/media/AudioRecord;->getState()I

    #@240
    move-result v12

    #@241
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@244
    move-result-object v11

    #@245
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@248
    move-result-object v11

    #@249
    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@24c
    .line 268
    :cond_24c
    iget-object v10, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@24e
    goto/16 :goto_11b

    #@250
    .line 264
    :catchall_250
    move-exception v10

    #@251
    iget-object v11, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@253
    iget-object v11, v11, Landroid/media/SmartRingtone;->arec:Landroid/media/AudioRecord;

    #@255
    invoke-virtual {v11}, Landroid/media/AudioRecord;->stop()V

    #@258
    .line 265
    invoke-static {}, Landroid/media/SmartRingtone;->access$800()Z

    #@25b
    move-result v11

    #@25c
    if-eqz v11, :cond_280

    #@25e
    invoke-static {}, Landroid/media/SmartRingtone;->access$000()Ljava/lang/String;

    #@261
    move-result-object v11

    #@262
    new-instance v12, Ljava/lang/StringBuilder;

    #@264
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@267
    const-string v13, "arec.stop() getRecordingState = "

    #@269
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26c
    move-result-object v12

    #@26d
    iget-object v13, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@26f
    iget-object v13, v13, Landroid/media/SmartRingtone;->arec:Landroid/media/AudioRecord;

    #@271
    invoke-virtual {v13}, Landroid/media/AudioRecord;->getRecordingState()I

    #@274
    move-result v13

    #@275
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@278
    move-result-object v12

    #@279
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27c
    move-result-object v12

    #@27d
    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@280
    .line 266
    :cond_280
    iget-object v11, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@282
    iget-object v11, v11, Landroid/media/SmartRingtone;->arec:Landroid/media/AudioRecord;

    #@284
    invoke-virtual {v11}, Landroid/media/AudioRecord;->release()V

    #@287
    .line 267
    invoke-static {}, Landroid/media/SmartRingtone;->access$800()Z

    #@28a
    move-result v11

    #@28b
    if-eqz v11, :cond_2af

    #@28d
    invoke-static {}, Landroid/media/SmartRingtone;->access$000()Ljava/lang/String;

    #@290
    move-result-object v11

    #@291
    new-instance v12, Ljava/lang/StringBuilder;

    #@293
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@296
    const-string v13, "arec.release() getState = "

    #@298
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29b
    move-result-object v12

    #@29c
    iget-object v13, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@29e
    iget-object v13, v13, Landroid/media/SmartRingtone;->arec:Landroid/media/AudioRecord;

    #@2a0
    invoke-virtual {v13}, Landroid/media/AudioRecord;->getState()I

    #@2a3
    move-result v13

    #@2a4
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a7
    move-result-object v12

    #@2a8
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2ab
    move-result-object v12

    #@2ac
    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2af
    .line 268
    :cond_2af
    iget-object v11, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@2b1
    const/4 v12, 0x0

    #@2b2
    iput-object v12, v11, Landroid/media/SmartRingtone;->arec:Landroid/media/AudioRecord;

    #@2b4
    .line 264
    throw v10

    #@2b5
    .line 282
    :cond_2b5
    iget-object v10, p0, Landroid/media/SmartRingtone$3;->this$0:Landroid/media/SmartRingtone;

    #@2b7
    const/4 v11, 0x0

    #@2b8
    invoke-static {v10, v11}, Landroid/media/SmartRingtone;->access$202(Landroid/media/SmartRingtone;I)I

    #@2bb
    goto/16 :goto_49
.end method
