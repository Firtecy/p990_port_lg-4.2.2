.class Landroid/media/IMediaScannerService$Stub$Proxy;
.super Ljava/lang/Object;
.source "IMediaScannerService.java"

# interfaces
.implements Landroid/media/IMediaScannerService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/IMediaScannerService$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 79
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 80
    iput-object p1, p0, Landroid/media/IMediaScannerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 81
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 84
    iget-object v0, p0, Landroid/media/IMediaScannerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 88
    const-string v0, "android.media.IMediaScannerService"

    #@2
    return-object v0
.end method

.method public requestScanFile(Ljava/lang/String;Ljava/lang/String;Landroid/media/IMediaScannerListener;)V
    .registers 9
    .parameter "path"
    .parameter "mimeType"
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 100
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 101
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 103
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.media.IMediaScannerService"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 104
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 105
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@13
    .line 106
    if-eqz p3, :cond_2d

    #@15
    invoke-interface {p3}, Landroid/media/IMediaScannerListener;->asBinder()Landroid/os/IBinder;

    #@18
    move-result-object v2

    #@19
    :goto_19
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@1c
    .line 107
    iget-object v2, p0, Landroid/media/IMediaScannerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1e
    const/4 v3, 0x1

    #@1f
    const/4 v4, 0x0

    #@20
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@23
    .line 108
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_26
    .catchall {:try_start_8 .. :try_end_26} :catchall_2f

    #@26
    .line 111
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 112
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 114
    return-void

    #@2d
    .line 106
    :cond_2d
    const/4 v2, 0x0

    #@2e
    goto :goto_19

    #@2f
    .line 111
    :catchall_2f
    move-exception v2

    #@30
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@33
    .line 112
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@36
    throw v2
.end method

.method public scanFile(Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "path"
    .parameter "mimeType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 124
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 125
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 127
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.media.IMediaScannerService"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 128
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 129
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@13
    .line 130
    iget-object v2, p0, Landroid/media/IMediaScannerService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/4 v3, 0x2

    #@16
    const/4 v4, 0x0

    #@17
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1a
    .line 131
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1d
    .catchall {:try_start_8 .. :try_end_1d} :catchall_24

    #@1d
    .line 134
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 135
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 137
    return-void

    #@24
    .line 134
    :catchall_24
    move-exception v2

    #@25
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 135
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    throw v2
.end method
