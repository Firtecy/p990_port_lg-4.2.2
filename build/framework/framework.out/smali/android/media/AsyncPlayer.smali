.class public Landroid/media/AsyncPlayer;
.super Ljava/lang/Object;
.source "AsyncPlayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/AsyncPlayer$1;,
        Landroid/media/AsyncPlayer$Thread;,
        Landroid/media/AsyncPlayer$Command;
    }
.end annotation


# static fields
.field private static final PLAY:I = 0x1

.field private static final STOP:I = 0x2

.field private static final mDebug:Z


# instance fields
.field private final mCmdQueue:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Landroid/media/AsyncPlayer$Command;",
            ">;"
        }
    .end annotation
.end field

.field private mPlayer:Landroid/media/MediaPlayer;

.field private mState:I

.field private mTag:Ljava/lang/String;

.field private mThread:Landroid/media/AsyncPlayer$Thread;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "tag"

    #@0
    .prologue
    .line 144
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 52
    new-instance v0, Ljava/util/LinkedList;

    #@5
    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    #@8
    iput-object v0, p0, Landroid/media/AsyncPlayer;->mCmdQueue:Ljava/util/LinkedList;

    #@a
    .line 137
    const/4 v0, 0x2

    #@b
    iput v0, p0, Landroid/media/AsyncPlayer;->mState:I

    #@d
    .line 145
    if-eqz p1, :cond_12

    #@f
    .line 146
    iput-object p1, p0, Landroid/media/AsyncPlayer;->mTag:Ljava/lang/String;

    #@11
    .line 150
    :goto_11
    return-void

    #@12
    .line 148
    :cond_12
    const-string v0, "AsyncPlayer"

    #@14
    iput-object v0, p0, Landroid/media/AsyncPlayer;->mTag:Ljava/lang/String;

    #@16
    goto :goto_11
.end method

.method static synthetic access$000(Landroid/media/AsyncPlayer;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 33
    iget-object v0, p0, Landroid/media/AsyncPlayer;->mTag:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/media/AsyncPlayer;)Ljava/util/LinkedList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 33
    iget-object v0, p0, Landroid/media/AsyncPlayer;->mCmdQueue:Ljava/util/LinkedList;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/media/AsyncPlayer;Landroid/media/AsyncPlayer$Command;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 33
    invoke-direct {p0, p1}, Landroid/media/AsyncPlayer;->startSound(Landroid/media/AsyncPlayer$Command;)V

    #@3
    return-void
.end method

.method static synthetic access$300(Landroid/media/AsyncPlayer;)Landroid/media/MediaPlayer;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 33
    iget-object v0, p0, Landroid/media/AsyncPlayer;->mPlayer:Landroid/media/MediaPlayer;

    #@2
    return-object v0
.end method

.method static synthetic access$302(Landroid/media/AsyncPlayer;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 33
    iput-object p1, p0, Landroid/media/AsyncPlayer;->mPlayer:Landroid/media/MediaPlayer;

    #@2
    return-object p1
.end method

.method static synthetic access$402(Landroid/media/AsyncPlayer;Landroid/media/AsyncPlayer$Thread;)Landroid/media/AsyncPlayer$Thread;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 33
    iput-object p1, p0, Landroid/media/AsyncPlayer;->mThread:Landroid/media/AsyncPlayer$Thread;

    #@2
    return-object p1
.end method

.method static synthetic access$500(Landroid/media/AsyncPlayer;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 33
    invoke-direct {p0}, Landroid/media/AsyncPlayer;->releaseWakeLock()V

    #@3
    return-void
.end method

.method private acquireWakeLock()V
    .registers 2

    #@0
    .prologue
    .line 239
    iget-object v0, p0, Landroid/media/AsyncPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 240
    iget-object v0, p0, Landroid/media/AsyncPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@6
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@9
    .line 242
    :cond_9
    return-void
.end method

.method private enqueueLocked(Landroid/media/AsyncPlayer$Command;)V
    .registers 3
    .parameter "cmd"

    #@0
    .prologue
    .line 206
    iget-object v0, p0, Landroid/media/AsyncPlayer;->mCmdQueue:Ljava/util/LinkedList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    #@5
    .line 207
    iget-object v0, p0, Landroid/media/AsyncPlayer;->mThread:Landroid/media/AsyncPlayer$Thread;

    #@7
    if-nez v0, :cond_18

    #@9
    .line 208
    invoke-direct {p0}, Landroid/media/AsyncPlayer;->acquireWakeLock()V

    #@c
    .line 209
    new-instance v0, Landroid/media/AsyncPlayer$Thread;

    #@e
    invoke-direct {v0, p0}, Landroid/media/AsyncPlayer$Thread;-><init>(Landroid/media/AsyncPlayer;)V

    #@11
    iput-object v0, p0, Landroid/media/AsyncPlayer;->mThread:Landroid/media/AsyncPlayer$Thread;

    #@13
    .line 210
    iget-object v0, p0, Landroid/media/AsyncPlayer;->mThread:Landroid/media/AsyncPlayer$Thread;

    #@15
    invoke-virtual {v0}, Landroid/media/AsyncPlayer$Thread;->start()V

    #@18
    .line 212
    :cond_18
    return-void
.end method

.method private releaseWakeLock()V
    .registers 2

    #@0
    .prologue
    .line 245
    iget-object v0, p0, Landroid/media/AsyncPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 246
    iget-object v0, p0, Landroid/media/AsyncPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@6
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@9
    .line 248
    :cond_9
    return-void
.end method

.method private startSound(Landroid/media/AsyncPlayer$Command;)V
    .registers 10
    .parameter "cmd"

    #@0
    .prologue
    .line 60
    :try_start_0
    new-instance v3, Landroid/media/MediaPlayer;

    #@2
    invoke-direct {v3}, Landroid/media/MediaPlayer;-><init>()V

    #@5
    .line 61
    .local v3, player:Landroid/media/MediaPlayer;
    iget v4, p1, Landroid/media/AsyncPlayer$Command;->stream:I

    #@7
    invoke-virtual {v3, v4}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    #@a
    .line 62
    iget-object v4, p1, Landroid/media/AsyncPlayer$Command;->context:Landroid/content/Context;

    #@c
    iget-object v5, p1, Landroid/media/AsyncPlayer$Command;->uri:Landroid/net/Uri;

    #@e
    invoke-virtual {v3, v4, v5}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    #@11
    .line 63
    iget-boolean v4, p1, Landroid/media/AsyncPlayer$Command;->looping:Z

    #@13
    invoke-virtual {v3, v4}, Landroid/media/MediaPlayer;->setLooping(Z)V

    #@16
    .line 64
    invoke-virtual {v3}, Landroid/media/MediaPlayer;->prepare()V

    #@19
    .line 65
    invoke-virtual {v3}, Landroid/media/MediaPlayer;->start()V

    #@1c
    .line 66
    iget-object v4, p0, Landroid/media/AsyncPlayer;->mPlayer:Landroid/media/MediaPlayer;

    #@1e
    if-eqz v4, :cond_25

    #@20
    .line 67
    iget-object v4, p0, Landroid/media/AsyncPlayer;->mPlayer:Landroid/media/MediaPlayer;

    #@22
    invoke-virtual {v4}, Landroid/media/MediaPlayer;->release()V

    #@25
    .line 69
    :cond_25
    iput-object v3, p0, Landroid/media/AsyncPlayer;->mPlayer:Landroid/media/MediaPlayer;

    #@27
    .line 70
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@2a
    move-result-wide v4

    #@2b
    iget-wide v6, p1, Landroid/media/AsyncPlayer$Command;->requestTime:J

    #@2d
    sub-long v0, v4, v6

    #@2f
    .line 71
    .local v0, delay:J
    const-wide/16 v4, 0x3e8

    #@31
    cmp-long v4, v0, v4

    #@33
    if-lez v4, :cond_54

    #@35
    .line 72
    iget-object v4, p0, Landroid/media/AsyncPlayer;->mTag:Ljava/lang/String;

    #@37
    new-instance v5, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v6, "Notification sound delayed by "

    #@3e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v5

    #@42
    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@45
    move-result-object v5

    #@46
    const-string/jumbo v6, "msecs"

    #@49
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v5

    #@4d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v5

    #@51
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_54
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_54} :catch_55

    #@54
    .line 78
    .end local v0           #delay:J
    .end local v3           #player:Landroid/media/MediaPlayer;
    :cond_54
    :goto_54
    return-void

    #@55
    .line 75
    :catch_55
    move-exception v2

    #@56
    .line 76
    .local v2, e:Ljava/lang/Exception;
    iget-object v4, p0, Landroid/media/AsyncPlayer;->mTag:Ljava/lang/String;

    #@58
    new-instance v5, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string v6, "error loading sound for "

    #@5f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v5

    #@63
    iget-object v6, p1, Landroid/media/AsyncPlayer$Command;->uri:Landroid/net/Uri;

    #@65
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v5

    #@69
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v5

    #@6d
    invoke-static {v4, v5, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@70
    goto :goto_54
.end method


# virtual methods
.method public play(Landroid/content/Context;Landroid/net/Uri;ZI)V
    .registers 10
    .parameter "context"
    .parameter "uri"
    .parameter "looping"
    .parameter "stream"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 166
    new-instance v1, Landroid/media/AsyncPlayer$Command;

    #@3
    const/4 v2, 0x0

    #@4
    invoke-direct {v1, v2}, Landroid/media/AsyncPlayer$Command;-><init>(Landroid/media/AsyncPlayer$1;)V

    #@7
    .line 167
    .local v1, cmd:Landroid/media/AsyncPlayer$Command;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@a
    move-result-wide v2

    #@b
    iput-wide v2, v1, Landroid/media/AsyncPlayer$Command;->requestTime:J

    #@d
    .line 170
    const-string v2, "audio"

    #@f
    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Landroid/media/AudioManager;

    #@15
    .line 171
    .local v0, au:Landroid/media/AudioManager;
    invoke-virtual {v0, p4}, Landroid/media/AudioManager;->checkPlayConditions(I)Z

    #@18
    move-result v2

    #@19
    if-eqz v2, :cond_30

    #@1b
    .line 172
    iput v4, v1, Landroid/media/AsyncPlayer$Command;->code:I

    #@1d
    .line 177
    :goto_1d
    iput-object p1, v1, Landroid/media/AsyncPlayer$Command;->context:Landroid/content/Context;

    #@1f
    .line 178
    iput-object p2, v1, Landroid/media/AsyncPlayer$Command;->uri:Landroid/net/Uri;

    #@21
    .line 179
    iput-boolean p3, v1, Landroid/media/AsyncPlayer$Command;->looping:Z

    #@23
    .line 180
    iput p4, v1, Landroid/media/AsyncPlayer$Command;->stream:I

    #@25
    .line 181
    iget-object v3, p0, Landroid/media/AsyncPlayer;->mCmdQueue:Ljava/util/LinkedList;

    #@27
    monitor-enter v3

    #@28
    .line 182
    :try_start_28
    invoke-direct {p0, v1}, Landroid/media/AsyncPlayer;->enqueueLocked(Landroid/media/AsyncPlayer$Command;)V

    #@2b
    .line 183
    const/4 v2, 0x1

    #@2c
    iput v2, p0, Landroid/media/AsyncPlayer;->mState:I

    #@2e
    .line 184
    monitor-exit v3
    :try_end_2f
    .catchall {:try_start_28 .. :try_end_2f} :catchall_34

    #@2f
    .line 185
    return-void

    #@30
    .line 174
    :cond_30
    const/4 v2, 0x2

    #@31
    iput v2, v1, Landroid/media/AsyncPlayer$Command;->code:I

    #@33
    goto :goto_1d

    #@34
    .line 184
    :catchall_34
    move-exception v2

    #@35
    :try_start_35
    monitor-exit v3
    :try_end_36
    .catchall {:try_start_35 .. :try_end_36} :catchall_34

    #@36
    throw v2
.end method

.method public setUsesWakeLock(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    .line 228
    iget-object v1, p0, Landroid/media/AsyncPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    if-nez v1, :cond_8

    #@4
    iget-object v1, p0, Landroid/media/AsyncPlayer;->mThread:Landroid/media/AsyncPlayer$Thread;

    #@6
    if-eqz v1, :cond_2f

    #@8
    .line 231
    :cond_8
    new-instance v1, Ljava/lang/RuntimeException;

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "assertion failed mWakeLock="

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    iget-object v3, p0, Landroid/media/AsyncPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    const-string v3, " mThread="

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    iget-object v3, p0, Landroid/media/AsyncPlayer;->mThread:Landroid/media/AsyncPlayer$Thread;

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@2e
    throw v1

    #@2f
    .line 234
    :cond_2f
    const-string/jumbo v1, "power"

    #@32
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@35
    move-result-object v0

    #@36
    check-cast v0, Landroid/os/PowerManager;

    #@38
    .line 235
    .local v0, pm:Landroid/os/PowerManager;
    const/4 v1, 0x1

    #@39
    iget-object v2, p0, Landroid/media/AsyncPlayer;->mTag:Ljava/lang/String;

    #@3b
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@3e
    move-result-object v1

    #@3f
    iput-object v1, p0, Landroid/media/AsyncPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@41
    .line 236
    return-void
.end method

.method public stop()V
    .registers 6

    #@0
    .prologue
    const/4 v3, 0x2

    #@1
    .line 192
    iget-object v2, p0, Landroid/media/AsyncPlayer;->mCmdQueue:Ljava/util/LinkedList;

    #@3
    monitor-enter v2

    #@4
    .line 195
    :try_start_4
    iget v1, p0, Landroid/media/AsyncPlayer;->mState:I

    #@6
    if-eq v1, v3, :cond_1d

    #@8
    .line 196
    new-instance v0, Landroid/media/AsyncPlayer$Command;

    #@a
    const/4 v1, 0x0

    #@b
    invoke-direct {v0, v1}, Landroid/media/AsyncPlayer$Command;-><init>(Landroid/media/AsyncPlayer$1;)V

    #@e
    .line 197
    .local v0, cmd:Landroid/media/AsyncPlayer$Command;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@11
    move-result-wide v3

    #@12
    iput-wide v3, v0, Landroid/media/AsyncPlayer$Command;->requestTime:J

    #@14
    .line 198
    const/4 v1, 0x2

    #@15
    iput v1, v0, Landroid/media/AsyncPlayer$Command;->code:I

    #@17
    .line 199
    invoke-direct {p0, v0}, Landroid/media/AsyncPlayer;->enqueueLocked(Landroid/media/AsyncPlayer$Command;)V

    #@1a
    .line 200
    const/4 v1, 0x2

    #@1b
    iput v1, p0, Landroid/media/AsyncPlayer;->mState:I

    #@1d
    .line 202
    .end local v0           #cmd:Landroid/media/AsyncPlayer$Command;
    :cond_1d
    monitor-exit v2

    #@1e
    .line 203
    return-void

    #@1f
    .line 202
    :catchall_1f
    move-exception v1

    #@20
    monitor-exit v2
    :try_end_21
    .catchall {:try_start_4 .. :try_end_21} :catchall_1f

    #@21
    throw v1
.end method
