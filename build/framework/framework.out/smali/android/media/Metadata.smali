.class public Landroid/media/Metadata;
.super Ljava/lang/Object;
.source "Metadata.java"


# static fields
.field public static final ALBUM:I = 0x8

.field public static final ALBUM_ART:I = 0x12

.field public static final ANY:I = 0x0

.field public static final ARTIST:I = 0x9

.field public static final AUDIO_BIT_RATE:I = 0x15

.field public static final AUDIO_CODEC:I = 0x1a

.field public static final AUDIO_SAMPLE_RATE:I = 0x17

.field public static final AUTHOR:I = 0xa

.field public static final BIT_RATE:I = 0x14

.field public static final BOOLEAN_VAL:I = 0x3

.field public static final BYTE_ARRAY_VAL:I = 0x7

.field public static final CD_TRACK_MAX:I = 0x10

.field public static final CD_TRACK_NUM:I = 0xf

.field public static final COMMENT:I = 0x6

.field public static final COMPOSER:I = 0xb

.field public static final COPYRIGHT:I = 0x7

.field public static final DATE:I = 0xd

.field public static final DATE_VAL:I = 0x6

.field public static final DOUBLE_VAL:I = 0x5

.field public static final DRM_CRIPPLED:I = 0x1f

.field public static final DURATION:I = 0xe

.field private static final FIRST_CUSTOM:I = 0x2000

.field public static final GENRE:I = 0xc

.field public static final INTEGER_VAL:I = 0x2

.field private static final LAST_SYSTEM:I = 0x1f

.field private static final LAST_TYPE:I = 0x7

.field public static final LONG_VAL:I = 0x4

.field public static final MATCH_ALL:Ljava/util/Set; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final MATCH_NONE:Ljava/util/Set; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final MIME_TYPE:I = 0x19

.field public static final NUM_TRACKS:I = 0x1e

.field public static final PAUSE_AVAILABLE:I = 0x1

.field public static final RATING:I = 0x11

.field public static final SEEK_AVAILABLE:I = 0x4

.field public static final SEEK_BACKWARD_AVAILABLE:I = 0x2

.field public static final SEEK_FORWARD_AVAILABLE:I = 0x3

.field public static final STRING_VAL:I = 0x1

.field private static final TAG:Ljava/lang/String; = "media.Metadata"

.field public static final TITLE:I = 0x5

.field public static final VIDEO_BIT_RATE:I = 0x16

.field public static final VIDEO_CODEC:I = 0x1b

.field public static final VIDEO_FRAME:I = 0x13

.field public static final VIDEO_FRAME_RATE:I = 0x18

.field public static final VIDEO_HEIGHT:I = 0x1c

.field public static final VIDEO_WIDTH:I = 0x1d

.field private static final kInt32Size:I = 0x4

.field private static final kMetaHeaderSize:I = 0x8

.field private static final kMetaMarker:I = 0x4d455441

.field private static final kRecordHeaderSize:I = 0xc


# instance fields
.field private final mKeyToPosMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mParcel:Landroid/os/Parcel;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 207
    sget-object v0, Ljava/util/Collections;->EMPTY_SET:Ljava/util/Set;

    #@2
    sput-object v0, Landroid/media/Metadata;->MATCH_NONE:Ljava/util/Set;

    #@4
    .line 211
    const/4 v0, 0x0

    #@5
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8
    move-result-object v0

    #@9
    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    #@c
    move-result-object v0

    #@d
    sput-object v0, Landroid/media/Metadata;->MATCH_ALL:Ljava/util/Set;

    #@f
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 266
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 260
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Landroid/media/Metadata;->mKeyToPosMap:Ljava/util/HashMap;

    #@a
    .line 266
    return-void
.end method

.method private checkMetadataId(I)Z
    .registers 5
    .parameter "val"

    #@0
    .prologue
    .line 525
    if-lez p1, :cond_a

    #@2
    const/16 v0, 0x1f

    #@4
    if-ge v0, p1, :cond_25

    #@6
    const/16 v0, 0x2000

    #@8
    if-ge p1, v0, :cond_25

    #@a
    .line 526
    :cond_a
    const-string/jumbo v0, "media.Metadata"

    #@d
    new-instance v1, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v2, "Invalid metadata ID "

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 527
    const/4 v0, 0x0

    #@24
    .line 529
    :goto_24
    return v0

    #@25
    :cond_25
    const/4 v0, 0x1

    #@26
    goto :goto_24
.end method

.method private checkType(II)V
    .registers 8
    .parameter "key"
    .parameter "expectedType"

    #@0
    .prologue
    .line 536
    iget-object v2, p0, Landroid/media/Metadata;->mKeyToPosMap:Ljava/util/HashMap;

    #@2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v3

    #@6
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v2

    #@a
    check-cast v2, Ljava/lang/Integer;

    #@c
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@f
    move-result v0

    #@10
    .line 538
    .local v0, pos:I
    iget-object v2, p0, Landroid/media/Metadata;->mParcel:Landroid/os/Parcel;

    #@12
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    #@15
    .line 540
    iget-object v2, p0, Landroid/media/Metadata;->mParcel:Landroid/os/Parcel;

    #@17
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    #@1a
    move-result v1

    #@1b
    .line 541
    .local v1, type:I
    if-eq v1, p2, :cond_40

    #@1d
    .line 542
    new-instance v2, Ljava/lang/IllegalStateException;

    #@1f
    new-instance v3, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v4, "Wrong type "

    #@26
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v3

    #@2a
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v3

    #@2e
    const-string v4, " but got "

    #@30
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v3

    #@3c
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@3f
    throw v2

    #@40
    .line 544
    :cond_40
    return-void
.end method

.method public static firstCustomId()I
    .registers 1

    #@0
    .prologue
    .line 511
    const/16 v0, 0x2000

    #@2
    return v0
.end method

.method public static lastSytemId()I
    .registers 1

    #@0
    .prologue
    .line 505
    const/16 v0, 0x1f

    #@2
    return v0
.end method

.method public static lastType()I
    .registers 1

    #@0
    .prologue
    .line 517
    const/4 v0, 0x7

    #@1
    return v0
.end method

.method private scanAllRecords(Landroid/os/Parcel;I)Z
    .registers 13
    .parameter "parcel"
    .parameter "bytesLeft"

    #@0
    .prologue
    const/16 v9, 0xc

    #@2
    .line 293
    const/4 v3, 0x0

    #@3
    .line 294
    .local v3, recCount:I
    const/4 v0, 0x0

    #@4
    .line 296
    .local v0, error:Z
    iget-object v6, p0, Landroid/media/Metadata;->mKeyToPosMap:Ljava/util/HashMap;

    #@6
    invoke-virtual {v6}, Ljava/util/HashMap;->clear()V

    #@9
    .line 297
    :goto_9
    if-le p2, v9, :cond_1e

    #@b
    .line 298
    invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I

    #@e
    move-result v5

    #@f
    .line 300
    .local v5, start:I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@12
    move-result v4

    #@13
    .line 302
    .local v4, size:I
    if-gt v4, v9, :cond_42

    #@15
    .line 303
    const-string/jumbo v6, "media.Metadata"

    #@18
    const-string v7, "Record is too short"

    #@1a
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 304
    const/4 v0, 0x1

    #@1e
    .line 340
    .end local v4           #size:I
    .end local v5           #start:I
    :cond_1e
    :goto_1e
    if-nez p2, :cond_22

    #@20
    if-eqz v0, :cond_a3

    #@22
    .line 341
    :cond_22
    const-string/jumbo v6, "media.Metadata"

    #@25
    new-instance v7, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v8, "Ran out of data or error on record "

    #@2c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v7

    #@30
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    move-result-object v7

    #@34
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v7

    #@38
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 342
    iget-object v6, p0, Landroid/media/Metadata;->mKeyToPosMap:Ljava/util/HashMap;

    #@3d
    invoke-virtual {v6}, Ljava/util/HashMap;->clear()V

    #@40
    .line 343
    const/4 v6, 0x0

    #@41
    .line 345
    :goto_41
    return v6

    #@42
    .line 309
    .restart local v4       #size:I
    .restart local v5       #start:I
    :cond_42
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@45
    move-result v1

    #@46
    .line 310
    .local v1, metadataId:I
    invoke-direct {p0, v1}, Landroid/media/Metadata;->checkMetadataId(I)Z

    #@49
    move-result v6

    #@4a
    if-nez v6, :cond_4e

    #@4c
    .line 311
    const/4 v0, 0x1

    #@4d
    .line 312
    goto :goto_1e

    #@4e
    .line 318
    :cond_4e
    iget-object v6, p0, Landroid/media/Metadata;->mKeyToPosMap:Ljava/util/HashMap;

    #@50
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@53
    move-result-object v7

    #@54
    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@57
    move-result v6

    #@58
    if-eqz v6, :cond_64

    #@5a
    .line 319
    const-string/jumbo v6, "media.Metadata"

    #@5d
    const-string v7, "Duplicate metadata ID found"

    #@5f
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    .line 320
    const/4 v0, 0x1

    #@63
    .line 321
    goto :goto_1e

    #@64
    .line 324
    :cond_64
    iget-object v6, p0, Landroid/media/Metadata;->mKeyToPosMap:Ljava/util/HashMap;

    #@66
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@69
    move-result-object v7

    #@6a
    invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I

    #@6d
    move-result v8

    #@6e
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@71
    move-result-object v8

    #@72
    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@75
    .line 327
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@78
    move-result v2

    #@79
    .line 328
    .local v2, metadataType:I
    if-lez v2, :cond_7e

    #@7b
    const/4 v6, 0x7

    #@7c
    if-le v2, v6, :cond_99

    #@7e
    .line 329
    :cond_7e
    const-string/jumbo v6, "media.Metadata"

    #@81
    new-instance v7, Ljava/lang/StringBuilder;

    #@83
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@86
    const-string v8, "Invalid metadata type "

    #@88
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v7

    #@8c
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v7

    #@90
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@93
    move-result-object v7

    #@94
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@97
    .line 330
    const/4 v0, 0x1

    #@98
    .line 331
    goto :goto_1e

    #@99
    .line 335
    :cond_99
    add-int v6, v5, v4

    #@9b
    invoke-virtual {p1, v6}, Landroid/os/Parcel;->setDataPosition(I)V

    #@9e
    .line 336
    sub-int/2addr p2, v4

    #@9f
    .line 337
    add-int/lit8 v3, v3, 0x1

    #@a1
    .line 338
    goto/16 :goto_9

    #@a3
    .line 345
    .end local v1           #metadataId:I
    .end local v2           #metadataType:I
    .end local v4           #size:I
    .end local v5           #start:I
    :cond_a3
    const/4 v6, 0x1

    #@a4
    goto :goto_41
.end method


# virtual methods
.method public getBoolean(I)Z
    .registers 4
    .parameter "key"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 451
    const/4 v1, 0x3

    #@2
    invoke-direct {p0, p1, v1}, Landroid/media/Metadata;->checkType(II)V

    #@5
    .line 452
    iget-object v1, p0, Landroid/media/Metadata;->mParcel:Landroid/os/Parcel;

    #@7
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@a
    move-result v1

    #@b
    if-ne v1, v0, :cond_e

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public getByteArray(I)[B
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 477
    const/4 v0, 0x7

    #@1
    invoke-direct {p0, p1, v0}, Landroid/media/Metadata;->checkType(II)V

    #@4
    .line 478
    iget-object v0, p0, Landroid/media/Metadata;->mParcel:Landroid/os/Parcel;

    #@6
    invoke-virtual {v0}, Landroid/os/Parcel;->createByteArray()[B

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public getDate(I)Ljava/util/Date;
    .registers 8
    .parameter "key"

    #@0
    .prologue
    .line 485
    const/4 v5, 0x6

    #@1
    invoke-direct {p0, p1, v5}, Landroid/media/Metadata;->checkType(II)V

    #@4
    .line 486
    iget-object v5, p0, Landroid/media/Metadata;->mParcel:Landroid/os/Parcel;

    #@6
    invoke-virtual {v5}, Landroid/os/Parcel;->readLong()J

    #@9
    move-result-wide v1

    #@a
    .line 487
    .local v1, timeSinceEpoch:J
    iget-object v5, p0, Landroid/media/Metadata;->mParcel:Landroid/os/Parcel;

    #@c
    invoke-virtual {v5}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@f
    move-result-object v3

    #@10
    .line 489
    .local v3, timeZone:Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@13
    move-result v5

    #@14
    if-nez v5, :cond_1c

    #@16
    .line 490
    new-instance v5, Ljava/util/Date;

    #@18
    invoke-direct {v5, v1, v2}, Ljava/util/Date;-><init>(J)V

    #@1b
    .line 496
    :goto_1b
    return-object v5

    #@1c
    .line 492
    :cond_1c
    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    #@1f
    move-result-object v4

    #@20
    .line 493
    .local v4, tz:Ljava/util/TimeZone;
    invoke-static {v4}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    #@23
    move-result-object v0

    #@24
    .line 495
    .local v0, cal:Ljava/util/Calendar;
    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@27
    .line 496
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    #@2a
    move-result-object v5

    #@2b
    goto :goto_1b
.end method

.method public getDouble(I)D
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 469
    const/4 v0, 0x5

    #@1
    invoke-direct {p0, p1, v0}, Landroid/media/Metadata;->checkType(II)V

    #@4
    .line 470
    iget-object v0, p0, Landroid/media/Metadata;->mParcel:Landroid/os/Parcel;

    #@6
    invoke-virtual {v0}, Landroid/os/Parcel;->readDouble()D

    #@9
    move-result-wide v0

    #@a
    return-wide v0
.end method

.method public getInt(I)I
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 443
    const/4 v0, 0x2

    #@1
    invoke-direct {p0, p1, v0}, Landroid/media/Metadata;->checkType(II)V

    #@4
    .line 444
    iget-object v0, p0, Landroid/media/Metadata;->mParcel:Landroid/os/Parcel;

    #@6
    invoke-virtual {v0}, Landroid/os/Parcel;->readInt()I

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public getLong(I)J
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 459
    const/4 v0, 0x4

    #@1
    invoke-direct {p0, p1, v0}, Landroid/media/Metadata;->checkType(II)V

    #@4
    .line 462
    iget-object v0, p0, Landroid/media/Metadata;->mParcel:Landroid/os/Parcel;

    #@6
    invoke-virtual {v0}, Landroid/os/Parcel;->readLong()J

    #@9
    move-result-wide v0

    #@a
    return-wide v0
.end method

.method public getString(I)Ljava/lang/String;
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 435
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, v0}, Landroid/media/Metadata;->checkType(II)V

    #@4
    .line 436
    iget-object v0, p0, Landroid/media/Metadata;->mParcel:Landroid/os/Parcel;

    #@6
    invoke-virtual {v0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public has(I)Z
    .registers 5
    .parameter "metadataId"

    #@0
    .prologue
    .line 421
    invoke-direct {p0, p1}, Landroid/media/Metadata;->checkMetadataId(I)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_1f

    #@6
    .line 422
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Invalid key: "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v0

    #@1f
    .line 424
    :cond_1f
    iget-object v0, p0, Landroid/media/Metadata;->mKeyToPosMap:Ljava/util/HashMap;

    #@21
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@28
    move-result v0

    #@29
    return v0
.end method

.method public keySet()Ljava/util/Set;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 414
    iget-object v0, p0, Landroid/media/Metadata;->mKeyToPosMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public parse(Landroid/os/Parcel;)Z
    .registers 9
    .parameter "parcel"

    #@0
    .prologue
    const/16 v5, 0x8

    #@2
    const/4 v3, 0x0

    #@3
    .line 378
    invoke-virtual {p1}, Landroid/os/Parcel;->dataAvail()I

    #@6
    move-result v4

    #@7
    if-ge v4, v5, :cond_27

    #@9
    .line 379
    const-string/jumbo v4, "media.Metadata"

    #@c
    new-instance v5, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v6, "Not enough data "

    #@13
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v5

    #@17
    invoke-virtual {p1}, Landroid/os/Parcel;->dataAvail()I

    #@1a
    move-result v6

    #@1b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v5

    #@1f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v5

    #@23
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 407
    :goto_26
    return v3

    #@27
    .line 383
    :cond_27
    invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I

    #@2a
    move-result v1

    #@2b
    .line 384
    .local v1, pin:I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2e
    move-result v2

    #@2f
    .line 387
    .local v2, size:I
    invoke-virtual {p1}, Landroid/os/Parcel;->dataAvail()I

    #@32
    move-result v4

    #@33
    add-int/lit8 v4, v4, 0x4

    #@35
    if-lt v4, v2, :cond_39

    #@37
    if-ge v2, v5, :cond_6e

    #@39
    .line 388
    :cond_39
    const-string/jumbo v4, "media.Metadata"

    #@3c
    new-instance v5, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v6, "Bad size "

    #@43
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v5

    #@47
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v5

    #@4b
    const-string v6, " avail "

    #@4d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v5

    #@51
    invoke-virtual {p1}, Landroid/os/Parcel;->dataAvail()I

    #@54
    move-result v6

    #@55
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@58
    move-result-object v5

    #@59
    const-string v6, " position "

    #@5b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v5

    #@5f
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@62
    move-result-object v5

    #@63
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v5

    #@67
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    .line 389
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    #@6d
    goto :goto_26

    #@6e
    .line 394
    :cond_6e
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@71
    move-result v0

    #@72
    .line 395
    .local v0, kShouldBeMetaMarker:I
    const v4, 0x4d455441

    #@75
    if-eq v0, v4, :cond_98

    #@77
    .line 396
    const-string/jumbo v4, "media.Metadata"

    #@7a
    new-instance v5, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    const-string v6, "Marker missing "

    #@81
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v5

    #@85
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@88
    move-result-object v6

    #@89
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v5

    #@8d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@90
    move-result-object v5

    #@91
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@94
    .line 397
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    #@97
    goto :goto_26

    #@98
    .line 402
    :cond_98
    add-int/lit8 v4, v2, -0x8

    #@9a
    invoke-direct {p0, p1, v4}, Landroid/media/Metadata;->scanAllRecords(Landroid/os/Parcel;I)Z

    #@9d
    move-result v4

    #@9e
    if-nez v4, :cond_a4

    #@a0
    .line 403
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    #@a3
    goto :goto_26

    #@a4
    .line 406
    :cond_a4
    iput-object p1, p0, Landroid/media/Metadata;->mParcel:Landroid/os/Parcel;

    #@a6
    .line 407
    const/4 v3, 0x1

    #@a7
    goto/16 :goto_26
.end method
