.class Landroid/media/AudioService$ForceControlStreamClient;
.super Ljava/lang/Object;
.source "AudioService.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/AudioService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ForceControlStreamClient"
.end annotation


# instance fields
.field private mCb:Landroid/os/IBinder;

.field final synthetic this$0:Landroid/media/AudioService;


# direct methods
.method constructor <init>(Landroid/media/AudioService;Landroid/os/IBinder;)V
    .registers 7
    .parameter
    .parameter "cb"

    #@0
    .prologue
    .line 1332
    iput-object p1, p0, Landroid/media/AudioService$ForceControlStreamClient;->this$0:Landroid/media/AudioService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 1333
    if-eqz p2, :cond_b

    #@7
    .line 1335
    const/4 v1, 0x0

    #@8
    :try_start_8
    invoke-interface {p2, p0, v1}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_b} :catch_e

    #@b
    .line 1342
    :cond_b
    :goto_b
    iput-object p2, p0, Landroid/media/AudioService$ForceControlStreamClient;->mCb:Landroid/os/IBinder;

    #@d
    .line 1343
    return-void

    #@e
    .line 1336
    :catch_e
    move-exception v0

    #@f
    .line 1338
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "AudioService"

    #@11
    new-instance v2, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v3, "ForceControlStreamClient() could not link to "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    const-string v3, " binder death"

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 1339
    const/4 p2, 0x0

    #@2e
    goto :goto_b
.end method


# virtual methods
.method public binderDied()V
    .registers 4

    #@0
    .prologue
    .line 1346
    iget-object v0, p0, Landroid/media/AudioService$ForceControlStreamClient;->this$0:Landroid/media/AudioService;

    #@2
    invoke-static {v0}, Landroid/media/AudioService;->access$1000(Landroid/media/AudioService;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    monitor-enter v1

    #@7
    .line 1347
    :try_start_7
    const-string v0, "AudioService"

    #@9
    const-string v2, "SCO client died"

    #@b
    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1348
    iget-object v0, p0, Landroid/media/AudioService$ForceControlStreamClient;->this$0:Landroid/media/AudioService;

    #@10
    invoke-static {v0}, Landroid/media/AudioService;->access$1100(Landroid/media/AudioService;)Landroid/media/AudioService$ForceControlStreamClient;

    #@13
    move-result-object v0

    #@14
    if-eq v0, p0, :cond_20

    #@16
    .line 1349
    const-string v0, "AudioService"

    #@18
    const-string/jumbo v2, "unregistered control stream client died"

    #@1b
    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 1354
    :goto_1e
    monitor-exit v1

    #@1f
    .line 1355
    return-void

    #@20
    .line 1351
    :cond_20
    iget-object v0, p0, Landroid/media/AudioService$ForceControlStreamClient;->this$0:Landroid/media/AudioService;

    #@22
    const/4 v2, 0x0

    #@23
    invoke-static {v0, v2}, Landroid/media/AudioService;->access$1102(Landroid/media/AudioService;Landroid/media/AudioService$ForceControlStreamClient;)Landroid/media/AudioService$ForceControlStreamClient;

    #@26
    .line 1352
    iget-object v0, p0, Landroid/media/AudioService$ForceControlStreamClient;->this$0:Landroid/media/AudioService;

    #@28
    const/4 v2, -0x1

    #@29
    invoke-static {v0, v2}, Landroid/media/AudioService;->access$1202(Landroid/media/AudioService;I)I

    #@2c
    goto :goto_1e

    #@2d
    .line 1354
    :catchall_2d
    move-exception v0

    #@2e
    monitor-exit v1
    :try_end_2f
    .catchall {:try_start_7 .. :try_end_2f} :catchall_2d

    #@2f
    throw v0
.end method

.method public release()V
    .registers 3

    #@0
    .prologue
    .line 1358
    iget-object v0, p0, Landroid/media/AudioService$ForceControlStreamClient;->mCb:Landroid/os/IBinder;

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 1359
    iget-object v0, p0, Landroid/media/AudioService$ForceControlStreamClient;->mCb:Landroid/os/IBinder;

    #@6
    const/4 v1, 0x0

    #@7
    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    #@a
    .line 1360
    const/4 v0, 0x0

    #@b
    iput-object v0, p0, Landroid/media/AudioService$ForceControlStreamClient;->mCb:Landroid/os/IBinder;

    #@d
    .line 1362
    :cond_d
    return-void
.end method
