.class public final Landroid/media/RemoteDisplay;
.super Ljava/lang/Object;
.source "RemoteDisplay.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/RemoteDisplay$Listener;
    }
.end annotation


# static fields
.field public static final DISPLAY_ERROR_CONNECTION_DROPPED:I = 0x2

.field public static final DISPLAY_ERROR_UNKOWN:I = 0x1

.field public static final DISPLAY_FLAG_SECURE:I = 0x1


# instance fields
.field private final mGuard:Ldalvik/system/CloseGuard;

.field private final mHandler:Landroid/os/Handler;

.field private final mListener:Landroid/media/RemoteDisplay$Listener;

.field private mPtr:I


# direct methods
.method private constructor <init>(Landroid/media/RemoteDisplay$Listener;Landroid/os/Handler;)V
    .registers 4
    .parameter "listener"
    .parameter "handler"

    #@0
    .prologue
    .line 46
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 37
    invoke-static {}, Ldalvik/system/CloseGuard;->get()Ldalvik/system/CloseGuard;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/media/RemoteDisplay;->mGuard:Ldalvik/system/CloseGuard;

    #@9
    .line 47
    iput-object p1, p0, Landroid/media/RemoteDisplay;->mListener:Landroid/media/RemoteDisplay$Listener;

    #@b
    .line 48
    iput-object p2, p0, Landroid/media/RemoteDisplay;->mHandler:Landroid/os/Handler;

    #@d
    .line 49
    return-void
.end method

.method static synthetic access$000(Landroid/media/RemoteDisplay;)Landroid/media/RemoteDisplay$Listener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 29
    iget-object v0, p0, Landroid/media/RemoteDisplay;->mListener:Landroid/media/RemoteDisplay$Listener;

    #@2
    return-object v0
.end method

.method private dispose(Z)V
    .registers 3
    .parameter "finalized"

    #@0
    .prologue
    .line 91
    iget v0, p0, Landroid/media/RemoteDisplay;->mPtr:I

    #@2
    if-eqz v0, :cond_17

    #@4
    .line 92
    iget-object v0, p0, Landroid/media/RemoteDisplay;->mGuard:Ldalvik/system/CloseGuard;

    #@6
    if-eqz v0, :cond_f

    #@8
    .line 93
    if-eqz p1, :cond_18

    #@a
    .line 94
    iget-object v0, p0, Landroid/media/RemoteDisplay;->mGuard:Ldalvik/system/CloseGuard;

    #@c
    invoke-virtual {v0}, Ldalvik/system/CloseGuard;->warnIfOpen()V

    #@f
    .line 100
    :cond_f
    :goto_f
    iget v0, p0, Landroid/media/RemoteDisplay;->mPtr:I

    #@11
    invoke-direct {p0, v0}, Landroid/media/RemoteDisplay;->nativeDispose(I)V

    #@14
    .line 101
    const/4 v0, 0x0

    #@15
    iput v0, p0, Landroid/media/RemoteDisplay;->mPtr:I

    #@17
    .line 103
    :cond_17
    return-void

    #@18
    .line 96
    :cond_18
    iget-object v0, p0, Landroid/media/RemoteDisplay;->mGuard:Ldalvik/system/CloseGuard;

    #@1a
    invoke-virtual {v0}, Ldalvik/system/CloseGuard;->close()V

    #@1d
    goto :goto_f
.end method

.method public static listen(Ljava/lang/String;Landroid/media/RemoteDisplay$Listener;Landroid/os/Handler;)Landroid/media/RemoteDisplay;
    .registers 6
    .parameter "iface"
    .parameter "listener"
    .parameter "handler"

    #@0
    .prologue
    .line 68
    if-nez p0, :cond_a

    #@2
    .line 69
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v2, "iface must not be null"

    #@6
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v1

    #@a
    .line 71
    :cond_a
    if-nez p1, :cond_15

    #@c
    .line 72
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@e
    const-string/jumbo v2, "listener must not be null"

    #@11
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@14
    throw v1

    #@15
    .line 74
    :cond_15
    if-nez p2, :cond_1f

    #@17
    .line 75
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@19
    const-string v2, "handler must not be null"

    #@1b
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v1

    #@1f
    .line 78
    :cond_1f
    new-instance v0, Landroid/media/RemoteDisplay;

    #@21
    invoke-direct {v0, p1, p2}, Landroid/media/RemoteDisplay;-><init>(Landroid/media/RemoteDisplay$Listener;Landroid/os/Handler;)V

    #@24
    .line 79
    .local v0, display:Landroid/media/RemoteDisplay;
    invoke-direct {v0, p0}, Landroid/media/RemoteDisplay;->startListening(Ljava/lang/String;)V

    #@27
    .line 80
    return-object v0
.end method

.method private native nativeDispose(I)V
.end method

.method private native nativeListen(Ljava/lang/String;)I
.end method

.method private notifyDisplayConnected(Landroid/view/Surface;III)V
    .registers 12
    .parameter "surface"
    .parameter "width"
    .parameter "height"
    .parameter "flags"

    #@0
    .prologue
    .line 117
    iget-object v6, p0, Landroid/media/RemoteDisplay;->mHandler:Landroid/os/Handler;

    #@2
    new-instance v0, Landroid/media/RemoteDisplay$1;

    #@4
    move-object v1, p0

    #@5
    move-object v2, p1

    #@6
    move v3, p2

    #@7
    move v4, p3

    #@8
    move v5, p4

    #@9
    invoke-direct/range {v0 .. v5}, Landroid/media/RemoteDisplay$1;-><init>(Landroid/media/RemoteDisplay;Landroid/view/Surface;III)V

    #@c
    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@f
    .line 123
    return-void
.end method

.method private notifyDisplayDisconnected()V
    .registers 3

    #@0
    .prologue
    .line 127
    iget-object v0, p0, Landroid/media/RemoteDisplay;->mHandler:Landroid/os/Handler;

    #@2
    new-instance v1, Landroid/media/RemoteDisplay$2;

    #@4
    invoke-direct {v1, p0}, Landroid/media/RemoteDisplay$2;-><init>(Landroid/media/RemoteDisplay;)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@a
    .line 133
    return-void
.end method

.method private notifyDisplayError(I)V
    .registers 4
    .parameter "error"

    #@0
    .prologue
    .line 137
    iget-object v0, p0, Landroid/media/RemoteDisplay;->mHandler:Landroid/os/Handler;

    #@2
    new-instance v1, Landroid/media/RemoteDisplay$3;

    #@4
    invoke-direct {v1, p0, p1}, Landroid/media/RemoteDisplay$3;-><init>(Landroid/media/RemoteDisplay;I)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@a
    .line 143
    return-void
.end method

.method private startListening(Ljava/lang/String;)V
    .registers 5
    .parameter "iface"

    #@0
    .prologue
    .line 106
    invoke-direct {p0, p1}, Landroid/media/RemoteDisplay;->nativeListen(Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    iput v0, p0, Landroid/media/RemoteDisplay;->mPtr:I

    #@6
    .line 107
    iget v0, p0, Landroid/media/RemoteDisplay;->mPtr:I

    #@8
    if-nez v0, :cond_29

    #@a
    .line 108
    new-instance v0, Ljava/lang/IllegalStateException;

    #@c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v2, "Could not start listening for remote display connection on \""

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, "\""

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@28
    throw v0

    #@29
    .line 111
    :cond_29
    iget-object v0, p0, Landroid/media/RemoteDisplay;->mGuard:Ldalvik/system/CloseGuard;

    #@2b
    const-string v1, "dispose"

    #@2d
    invoke-virtual {v0, v1}, Ldalvik/system/CloseGuard;->open(Ljava/lang/String;)V

    #@30
    .line 112
    return-void
.end method


# virtual methods
.method public dispose()V
    .registers 2

    #@0
    .prologue
    .line 87
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Landroid/media/RemoteDisplay;->dispose(Z)V

    #@4
    .line 88
    return-void
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 54
    const/4 v0, 0x1

    #@1
    :try_start_1
    invoke-direct {p0, v0}, Landroid/media/RemoteDisplay;->dispose(Z)V
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_8

    #@4
    .line 56
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@7
    .line 58
    return-void

    #@8
    .line 56
    :catchall_8
    move-exception v0

    #@9
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@c
    throw v0
.end method
