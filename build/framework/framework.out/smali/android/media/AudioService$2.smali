.class Landroid/media/AudioService$2;
.super Landroid/content/BroadcastReceiver;
.source "AudioService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/AudioService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/media/AudioService;


# direct methods
.method constructor <init>(Landroid/media/AudioService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 524
    iput-object p1, p0, Landroid/media/AudioService$2;->this$0:Landroid/media/AudioService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 15
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    const/4 v6, 0x0

    #@2
    .line 527
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@5
    move-result-object v7

    #@6
    .line 528
    .local v7, action:Ljava/lang/String;
    const-string v0, "AudioService"

    #@8
    const-string v1, "UseEarpieceReceiver() set force use earpiece device."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 529
    const-string v0, "com.lge.media.SET_FORCE_USE_EARPIECE"

    #@f
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_80

    #@15
    iget-object v0, p0, Landroid/media/AudioService$2;->this$0:Landroid/media/AudioService;

    #@17
    invoke-virtual {v0}, Landroid/media/AudioService;->isBluetoothA2dpOn()Z

    #@1a
    move-result v0

    #@1b
    if-nez v0, :cond_80

    #@1d
    .line 531
    const/4 v9, 0x0

    #@1e
    .line 532
    .local v9, devices:I
    iget-object v0, p0, Landroid/media/AudioService$2;->this$0:Landroid/media/AudioService;

    #@20
    invoke-static {v0}, Landroid/media/AudioService;->access$400(Landroid/media/AudioService;)Ljava/util/HashMap;

    #@23
    move-result-object v0

    #@24
    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@27
    move-result-object v0

    #@28
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@2b
    move-result-object v10

    #@2c
    .local v10, i$:Ljava/util/Iterator;
    :cond_2c
    :goto_2c
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@2f
    move-result v0

    #@30
    if-eqz v0, :cond_45

    #@32
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@35
    move-result-object v0

    #@36
    check-cast v0, Ljava/lang/Integer;

    #@38
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@3b
    move-result v8

    #@3c
    .line 533
    .local v8, dev:I
    iget-object v0, p0, Landroid/media/AudioService$2;->this$0:Landroid/media/AudioService;

    #@3e
    iget v0, v0, Landroid/media/AudioService;->mBecomingNoisyIntentDevices:I

    #@40
    and-int/2addr v0, v8

    #@41
    if-eqz v0, :cond_2c

    #@43
    .line 534
    or-int/2addr v9, v8

    #@44
    goto :goto_2c

    #@45
    .line 537
    .end local v8           #dev:I
    :cond_45
    if-nez v9, :cond_80

    #@47
    .line 538
    const-string v0, "com.lge.media.EXTRA_USE_EARPIECE_ON"

    #@49
    invoke-virtual {p2, v0, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@4c
    move-result v11

    #@4d
    .line 539
    .local v11, useEarpieceOn:I
    const-string v0, "AudioService"

    #@4f
    new-instance v1, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    const-string v3, "UseEarpieceReceiver() Set force use earpiece device. useEarpieceOn = "

    #@56
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v1

    #@5a
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v1

    #@5e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v1

    #@62
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@65
    .line 540
    iget-object v1, p0, Landroid/media/AudioService$2;->this$0:Landroid/media/AudioService;

    #@67
    if-nez v11, :cond_81

    #@69
    move v0, v6

    #@6a
    :goto_6a
    invoke-static {v1, v0}, Landroid/media/AudioService;->access$502(Landroid/media/AudioService;I)I

    #@6d
    .line 541
    iget-object v0, p0, Landroid/media/AudioService$2;->this$0:Landroid/media/AudioService;

    #@6f
    invoke-static {v0}, Landroid/media/AudioService;->access$100(Landroid/media/AudioService;)Landroid/media/AudioService$AudioHandler;

    #@72
    move-result-object v0

    #@73
    const/16 v1, 0x9

    #@75
    const/4 v3, 0x1

    #@76
    iget-object v4, p0, Landroid/media/AudioService$2;->this$0:Landroid/media/AudioService;

    #@78
    invoke-static {v4}, Landroid/media/AudioService;->access$500(Landroid/media/AudioService;)I

    #@7b
    move-result v4

    #@7c
    const/4 v5, 0x0

    #@7d
    invoke-static/range {v0 .. v6}, Landroid/media/AudioService;->access$200(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    #@80
    .line 545
    .end local v9           #devices:I
    .end local v10           #i$:Ljava/util/Iterator;
    .end local v11           #useEarpieceOn:I
    :cond_80
    return-void

    #@81
    .restart local v9       #devices:I
    .restart local v10       #i$:Ljava/util/Iterator;
    .restart local v11       #useEarpieceOn:I
    :cond_81
    move v0, v2

    #@82
    .line 540
    goto :goto_6a
.end method
