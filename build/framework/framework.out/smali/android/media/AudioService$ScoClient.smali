.class Landroid/media/AudioService$ScoClient;
.super Ljava/lang/Object;
.source "AudioService.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/AudioService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScoClient"
.end annotation


# instance fields
.field private mCb:Landroid/os/IBinder;

.field private mCreatorPid:I

.field private mStartcount:I

.field final synthetic this$0:Landroid/media/AudioService;


# direct methods
.method constructor <init>(Landroid/media/AudioService;Landroid/os/IBinder;)V
    .registers 4
    .parameter
    .parameter "cb"

    #@0
    .prologue
    .line 2514
    iput-object p1, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 2515
    iput-object p2, p0, Landroid/media/AudioService$ScoClient;->mCb:Landroid/os/IBinder;

    #@7
    .line 2516
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@a
    move-result v0

    #@b
    iput v0, p0, Landroid/media/AudioService$ScoClient;->mCreatorPid:I

    #@d
    .line 2517
    const/4 v0, 0x0

    #@e
    iput v0, p0, Landroid/media/AudioService$ScoClient;->mStartcount:I

    #@10
    .line 2518
    return-void
.end method

.method private requestScoState(I)V
    .registers 7
    .parameter "state"

    #@0
    .prologue
    const/4 v4, 0x5

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v1, 0x3

    #@3
    const/4 v2, 0x0

    #@4
    .line 2606
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@6
    invoke-static {v0}, Landroid/media/AudioService;->access$2400(Landroid/media/AudioService;)V

    #@9
    .line 2607
    invoke-virtual {p0}, Landroid/media/AudioService$ScoClient;->totalCount()I

    #@c
    move-result v0

    #@d
    if-nez v0, :cond_82

    #@f
    .line 2608
    const/16 v0, 0xc

    #@11
    if-ne p1, v0, :cond_b0

    #@13
    .line 2611
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@15
    const/4 v1, 0x2

    #@16
    invoke-static {v0, v1}, Landroid/media/AudioService;->access$2500(Landroid/media/AudioService;I)V

    #@19
    .line 2614
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@1b
    invoke-static {v0}, Landroid/media/AudioService;->access$1400(Landroid/media/AudioService;)Ljava/util/ArrayList;

    #@1e
    move-result-object v1

    #@1f
    monitor-enter v1

    #@20
    .line 2615
    :try_start_20
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@22
    invoke-static {v0}, Landroid/media/AudioService;->access$1400(Landroid/media/AudioService;)Ljava/util/ArrayList;

    #@25
    move-result-object v0

    #@26
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    #@29
    move-result v0

    #@2a
    if-nez v0, :cond_41

    #@2c
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@2e
    invoke-static {v0}, Landroid/media/AudioService;->access$1400(Landroid/media/AudioService;)Ljava/util/ArrayList;

    #@31
    move-result-object v0

    #@32
    const/4 v2, 0x0

    #@33
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@36
    move-result-object v0

    #@37
    check-cast v0, Landroid/media/AudioService$SetModeDeathHandler;

    #@39
    invoke-virtual {v0}, Landroid/media/AudioService$SetModeDeathHandler;->getPid()I

    #@3c
    move-result v0

    #@3d
    iget v2, p0, Landroid/media/AudioService$ScoClient;->mCreatorPid:I

    #@3f
    if-ne v0, v2, :cond_a9

    #@41
    :cond_41
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@43
    invoke-static {v0}, Landroid/media/AudioService;->access$2600(Landroid/media/AudioService;)I

    #@46
    move-result v0

    #@47
    if-eqz v0, :cond_51

    #@49
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@4b
    invoke-static {v0}, Landroid/media/AudioService;->access$2600(Landroid/media/AudioService;)I

    #@4e
    move-result v0

    #@4f
    if-ne v0, v4, :cond_a9

    #@51
    .line 2619
    :cond_51
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@53
    invoke-static {v0}, Landroid/media/AudioService;->access$2600(Landroid/media/AudioService;)I

    #@56
    move-result v0

    #@57
    if-nez v0, :cond_9c

    #@59
    .line 2620
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@5b
    invoke-static {v0}, Landroid/media/AudioService;->access$2700(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothHeadset;

    #@5e
    move-result-object v0

    #@5f
    if-eqz v0, :cond_8d

    #@61
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@63
    invoke-static {v0}, Landroid/media/AudioService;->access$2800(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothDevice;

    #@66
    move-result-object v0

    #@67
    if-eqz v0, :cond_8d

    #@69
    .line 2621
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@6b
    invoke-static {v0}, Landroid/media/AudioService;->access$2700(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothHeadset;

    #@6e
    move-result-object v0

    #@6f
    iget-object v2, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@71
    invoke-static {v2}, Landroid/media/AudioService;->access$2800(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothDevice;

    #@74
    move-result-object v2

    #@75
    invoke-virtual {v0, v2}, Landroid/bluetooth/BluetoothHeadset;->startScoUsingVirtualVoiceCall(Landroid/bluetooth/BluetoothDevice;)Z

    #@78
    move-result v0

    #@79
    if-eqz v0, :cond_83

    #@7b
    .line 2623
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@7d
    const/4 v2, 0x3

    #@7e
    invoke-static {v0, v2}, Landroid/media/AudioService;->access$2602(Landroid/media/AudioService;I)I

    #@81
    .line 2638
    :cond_81
    :goto_81
    monitor-exit v1

    #@82
    .line 2659
    :cond_82
    :goto_82
    return-void

    #@83
    .line 2625
    :cond_83
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@85
    const/4 v2, 0x0

    #@86
    invoke-static {v0, v2}, Landroid/media/AudioService;->access$2500(Landroid/media/AudioService;I)V

    #@89
    goto :goto_81

    #@8a
    .line 2638
    :catchall_8a
    move-exception v0

    #@8b
    monitor-exit v1
    :try_end_8c
    .catchall {:try_start_20 .. :try_end_8c} :catchall_8a

    #@8c
    throw v0

    #@8d
    .line 2628
    :cond_8d
    :try_start_8d
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@8f
    invoke-static {v0}, Landroid/media/AudioService;->access$2900(Landroid/media/AudioService;)Z

    #@92
    move-result v0

    #@93
    if-eqz v0, :cond_81

    #@95
    .line 2629
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@97
    const/4 v2, 0x1

    #@98
    invoke-static {v0, v2}, Landroid/media/AudioService;->access$2602(Landroid/media/AudioService;I)I

    #@9b
    goto :goto_81

    #@9c
    .line 2632
    :cond_9c
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@9e
    const/4 v2, 0x3

    #@9f
    invoke-static {v0, v2}, Landroid/media/AudioService;->access$2602(Landroid/media/AudioService;I)I

    #@a2
    .line 2633
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@a4
    const/4 v2, 0x1

    #@a5
    invoke-static {v0, v2}, Landroid/media/AudioService;->access$2500(Landroid/media/AudioService;I)V

    #@a8
    goto :goto_81

    #@a9
    .line 2636
    :cond_a9
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@ab
    const/4 v2, 0x0

    #@ac
    invoke-static {v0, v2}, Landroid/media/AudioService;->access$2500(Landroid/media/AudioService;I)V
    :try_end_af
    .catchall {:try_start_8d .. :try_end_af} :catchall_8a

    #@af
    goto :goto_81

    #@b0
    .line 2639
    :cond_b0
    const/16 v0, 0xa

    #@b2
    if-ne p1, v0, :cond_82

    #@b4
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@b6
    invoke-static {v0}, Landroid/media/AudioService;->access$2600(Landroid/media/AudioService;)I

    #@b9
    move-result v0

    #@ba
    if-eq v0, v1, :cond_c4

    #@bc
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@be
    invoke-static {v0}, Landroid/media/AudioService;->access$2600(Landroid/media/AudioService;)I

    #@c1
    move-result v0

    #@c2
    if-ne v0, v3, :cond_82

    #@c4
    .line 2642
    :cond_c4
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@c6
    invoke-static {v0}, Landroid/media/AudioService;->access$2600(Landroid/media/AudioService;)I

    #@c9
    move-result v0

    #@ca
    if-ne v0, v1, :cond_108

    #@cc
    .line 2643
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@ce
    invoke-static {v0}, Landroid/media/AudioService;->access$2700(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothHeadset;

    #@d1
    move-result-object v0

    #@d2
    if-eqz v0, :cond_f9

    #@d4
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@d6
    invoke-static {v0}, Landroid/media/AudioService;->access$2800(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothDevice;

    #@d9
    move-result-object v0

    #@da
    if-eqz v0, :cond_f9

    #@dc
    .line 2644
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@de
    invoke-static {v0}, Landroid/media/AudioService;->access$2700(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothHeadset;

    #@e1
    move-result-object v0

    #@e2
    iget-object v1, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@e4
    invoke-static {v1}, Landroid/media/AudioService;->access$2800(Landroid/media/AudioService;)Landroid/bluetooth/BluetoothDevice;

    #@e7
    move-result-object v1

    #@e8
    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothHeadset;->stopScoUsingVirtualVoiceCall(Landroid/bluetooth/BluetoothDevice;)Z

    #@eb
    move-result v0

    #@ec
    if-nez v0, :cond_82

    #@ee
    .line 2646
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@f0
    invoke-static {v0, v2}, Landroid/media/AudioService;->access$2602(Landroid/media/AudioService;I)I

    #@f3
    .line 2647
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@f5
    invoke-static {v0, v2}, Landroid/media/AudioService;->access$2500(Landroid/media/AudioService;I)V

    #@f8
    goto :goto_82

    #@f9
    .line 2650
    :cond_f9
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@fb
    invoke-static {v0}, Landroid/media/AudioService;->access$2900(Landroid/media/AudioService;)Z

    #@fe
    move-result v0

    #@ff
    if-eqz v0, :cond_82

    #@101
    .line 2651
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@103
    invoke-static {v0, v4}, Landroid/media/AudioService;->access$2602(Landroid/media/AudioService;I)I

    #@106
    goto/16 :goto_82

    #@108
    .line 2654
    :cond_108
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@10a
    invoke-static {v0, v2}, Landroid/media/AudioService;->access$2602(Landroid/media/AudioService;I)I

    #@10d
    .line 2655
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@10f
    invoke-static {v0, v2}, Landroid/media/AudioService;->access$2500(Landroid/media/AudioService;I)V

    #@112
    goto/16 :goto_82
.end method


# virtual methods
.method public binderDied()V
    .registers 5

    #@0
    .prologue
    .line 2521
    iget-object v1, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@2
    invoke-static {v1}, Landroid/media/AudioService;->access$2300(Landroid/media/AudioService;)Ljava/util/ArrayList;

    #@5
    move-result-object v2

    #@6
    monitor-enter v2

    #@7
    .line 2522
    :try_start_7
    const-string v1, "AudioService"

    #@9
    const-string v3, "SCO client died"

    #@b
    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 2523
    iget-object v1, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@10
    invoke-static {v1}, Landroid/media/AudioService;->access$2300(Landroid/media/AudioService;)Ljava/util/ArrayList;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    #@17
    move-result v0

    #@18
    .line 2524
    .local v0, index:I
    if-gez v0, :cond_24

    #@1a
    .line 2525
    const-string v1, "AudioService"

    #@1c
    const-string/jumbo v3, "unregistered SCO client died"

    #@1f
    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 2530
    :goto_22
    monitor-exit v2

    #@23
    .line 2531
    return-void

    #@24
    .line 2527
    :cond_24
    const/4 v1, 0x1

    #@25
    invoke-virtual {p0, v1}, Landroid/media/AudioService$ScoClient;->clearCount(Z)V

    #@28
    .line 2528
    iget-object v1, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@2a
    invoke-static {v1}, Landroid/media/AudioService;->access$2300(Landroid/media/AudioService;)Ljava/util/ArrayList;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@31
    goto :goto_22

    #@32
    .line 2530
    .end local v0           #index:I
    :catchall_32
    move-exception v1

    #@33
    monitor-exit v2
    :try_end_34
    .catchall {:try_start_7 .. :try_end_34} :catchall_32

    #@34
    throw v1
.end method

.method public clearCount(Z)V
    .registers 7
    .parameter "stopSco"

    #@0
    .prologue
    .line 2567
    iget-object v1, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@2
    invoke-static {v1}, Landroid/media/AudioService;->access$2300(Landroid/media/AudioService;)Ljava/util/ArrayList;

    #@5
    move-result-object v2

    #@6
    monitor-enter v2

    #@7
    .line 2568
    :try_start_7
    iget v1, p0, Landroid/media/AudioService$ScoClient;->mStartcount:I
    :try_end_9
    .catchall {:try_start_7 .. :try_end_9} :catchall_3f

    #@9
    if-eqz v1, :cond_11

    #@b
    .line 2570
    :try_start_b
    iget-object v1, p0, Landroid/media/AudioService$ScoClient;->mCb:Landroid/os/IBinder;

    #@d
    const/4 v3, 0x0

    #@e
    invoke-interface {v1, p0, v3}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_11
    .catchall {:try_start_b .. :try_end_11} :catchall_3f
    .catch Ljava/util/NoSuchElementException; {:try_start_b .. :try_end_11} :catch_1d

    #@11
    .line 2575
    :cond_11
    :goto_11
    const/4 v1, 0x0

    #@12
    :try_start_12
    iput v1, p0, Landroid/media/AudioService$ScoClient;->mStartcount:I

    #@14
    .line 2576
    if-eqz p1, :cond_1b

    #@16
    .line 2577
    const/16 v1, 0xa

    #@18
    invoke-direct {p0, v1}, Landroid/media/AudioService$ScoClient;->requestScoState(I)V

    #@1b
    .line 2579
    :cond_1b
    monitor-exit v2

    #@1c
    .line 2580
    return-void

    #@1d
    .line 2571
    :catch_1d
    move-exception v0

    #@1e
    .line 2572
    .local v0, e:Ljava/util/NoSuchElementException;
    const-string v1, "AudioService"

    #@20
    new-instance v3, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v4, "clearCount() mStartcount: "

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    iget v4, p0, Landroid/media/AudioService$ScoClient;->mStartcount:I

    #@2d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    const-string v4, " != 0 but not registered to binder"

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v3

    #@3b
    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    goto :goto_11

    #@3f
    .line 2579
    .end local v0           #e:Ljava/util/NoSuchElementException;
    :catchall_3f
    move-exception v1

    #@40
    monitor-exit v2
    :try_end_41
    .catchall {:try_start_12 .. :try_end_41} :catchall_3f

    #@41
    throw v1
.end method

.method public decCount()V
    .registers 5

    #@0
    .prologue
    .line 2549
    iget-object v1, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@2
    invoke-static {v1}, Landroid/media/AudioService;->access$2300(Landroid/media/AudioService;)Ljava/util/ArrayList;

    #@5
    move-result-object v2

    #@6
    monitor-enter v2

    #@7
    .line 2550
    :try_start_7
    iget v1, p0, Landroid/media/AudioService$ScoClient;->mStartcount:I

    #@9
    if-nez v1, :cond_14

    #@b
    .line 2551
    const-string v1, "AudioService"

    #@d
    const-string v3, "ScoClient.decCount() already 0"

    #@f
    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 2563
    :goto_12
    monitor-exit v2

    #@13
    .line 2564
    return-void

    #@14
    .line 2553
    :cond_14
    iget v1, p0, Landroid/media/AudioService$ScoClient;->mStartcount:I

    #@16
    add-int/lit8 v1, v1, -0x1

    #@18
    iput v1, p0, Landroid/media/AudioService$ScoClient;->mStartcount:I

    #@1a
    .line 2554
    iget v1, p0, Landroid/media/AudioService$ScoClient;->mStartcount:I
    :try_end_1c
    .catchall {:try_start_7 .. :try_end_1c} :catchall_2a

    #@1c
    if-nez v1, :cond_24

    #@1e
    .line 2556
    :try_start_1e
    iget-object v1, p0, Landroid/media/AudioService$ScoClient;->mCb:Landroid/os/IBinder;

    #@20
    const/4 v3, 0x0

    #@21
    invoke-interface {v1, p0, v3}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_24
    .catchall {:try_start_1e .. :try_end_24} :catchall_2a
    .catch Ljava/util/NoSuchElementException; {:try_start_1e .. :try_end_24} :catch_2d

    #@24
    .line 2561
    :cond_24
    :goto_24
    const/16 v1, 0xa

    #@26
    :try_start_26
    invoke-direct {p0, v1}, Landroid/media/AudioService$ScoClient;->requestScoState(I)V

    #@29
    goto :goto_12

    #@2a
    .line 2563
    :catchall_2a
    move-exception v1

    #@2b
    monitor-exit v2
    :try_end_2c
    .catchall {:try_start_26 .. :try_end_2c} :catchall_2a

    #@2c
    throw v1

    #@2d
    .line 2557
    :catch_2d
    move-exception v0

    #@2e
    .line 2558
    .local v0, e:Ljava/util/NoSuchElementException;
    :try_start_2e
    const-string v1, "AudioService"

    #@30
    const-string v3, "decCount() going to 0 but not registered to binder"

    #@32
    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_35
    .catchall {:try_start_2e .. :try_end_35} :catchall_2a

    #@35
    goto :goto_24
.end method

.method public getBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 2587
    iget-object v0, p0, Landroid/media/AudioService$ScoClient;->mCb:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getCount()I
    .registers 2

    #@0
    .prologue
    .line 2583
    iget v0, p0, Landroid/media/AudioService$ScoClient;->mStartcount:I

    #@2
    return v0
.end method

.method public getPid()I
    .registers 2

    #@0
    .prologue
    .line 2591
    iget v0, p0, Landroid/media/AudioService$ScoClient;->mCreatorPid:I

    #@2
    return v0
.end method

.method public incCount()V
    .registers 6

    #@0
    .prologue
    .line 2534
    iget-object v1, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@2
    invoke-static {v1}, Landroid/media/AudioService;->access$2300(Landroid/media/AudioService;)Ljava/util/ArrayList;

    #@5
    move-result-object v2

    #@6
    monitor-enter v2

    #@7
    .line 2535
    const/16 v1, 0xc

    #@9
    :try_start_9
    invoke-direct {p0, v1}, Landroid/media/AudioService$ScoClient;->requestScoState(I)V

    #@c
    .line 2536
    iget v1, p0, Landroid/media/AudioService$ScoClient;->mStartcount:I
    :try_end_e
    .catchall {:try_start_9 .. :try_end_e} :catchall_40

    #@e
    if-nez v1, :cond_16

    #@10
    .line 2538
    :try_start_10
    iget-object v1, p0, Landroid/media/AudioService$ScoClient;->mCb:Landroid/os/IBinder;

    #@12
    const/4 v3, 0x0

    #@13
    invoke-interface {v1, p0, v3}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_16
    .catchall {:try_start_10 .. :try_end_16} :catchall_40
    .catch Landroid/os/RemoteException; {:try_start_10 .. :try_end_16} :catch_1e

    #@16
    .line 2544
    :cond_16
    :goto_16
    :try_start_16
    iget v1, p0, Landroid/media/AudioService$ScoClient;->mStartcount:I

    #@18
    add-int/lit8 v1, v1, 0x1

    #@1a
    iput v1, p0, Landroid/media/AudioService$ScoClient;->mStartcount:I

    #@1c
    .line 2545
    monitor-exit v2

    #@1d
    .line 2546
    return-void

    #@1e
    .line 2539
    :catch_1e
    move-exception v0

    #@1f
    .line 2541
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "AudioService"

    #@21
    new-instance v3, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v4, "ScoClient  incCount() could not link to "

    #@28
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v3

    #@2c
    iget-object v4, p0, Landroid/media/AudioService$ScoClient;->mCb:Landroid/os/IBinder;

    #@2e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    const-string v4, " binder death"

    #@34
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v3

    #@3c
    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    goto :goto_16

    #@40
    .line 2545
    .end local v0           #e:Landroid/os/RemoteException;
    :catchall_40
    move-exception v1

    #@41
    monitor-exit v2
    :try_end_42
    .catchall {:try_start_16 .. :try_end_42} :catchall_40

    #@42
    throw v1
.end method

.method public totalCount()I
    .registers 6

    #@0
    .prologue
    .line 2595
    iget-object v3, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@2
    invoke-static {v3}, Landroid/media/AudioService;->access$2300(Landroid/media/AudioService;)Ljava/util/ArrayList;

    #@5
    move-result-object v4

    #@6
    monitor-enter v4

    #@7
    .line 2596
    const/4 v0, 0x0

    #@8
    .line 2597
    .local v0, count:I
    :try_start_8
    iget-object v3, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@a
    invoke-static {v3}, Landroid/media/AudioService;->access$2300(Landroid/media/AudioService;)Ljava/util/ArrayList;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@11
    move-result v2

    #@12
    .line 2598
    .local v2, size:I
    const/4 v1, 0x0

    #@13
    .local v1, i:I
    :goto_13
    if-ge v1, v2, :cond_29

    #@15
    .line 2599
    iget-object v3, p0, Landroid/media/AudioService$ScoClient;->this$0:Landroid/media/AudioService;

    #@17
    invoke-static {v3}, Landroid/media/AudioService;->access$2300(Landroid/media/AudioService;)Ljava/util/ArrayList;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1e
    move-result-object v3

    #@1f
    check-cast v3, Landroid/media/AudioService$ScoClient;

    #@21
    invoke-virtual {v3}, Landroid/media/AudioService$ScoClient;->getCount()I

    #@24
    move-result v3

    #@25
    add-int/2addr v0, v3

    #@26
    .line 2598
    add-int/lit8 v1, v1, 0x1

    #@28
    goto :goto_13

    #@29
    .line 2601
    :cond_29
    monitor-exit v4

    #@2a
    return v0

    #@2b
    .line 2602
    .end local v1           #i:I
    .end local v2           #size:I
    :catchall_2b
    move-exception v3

    #@2c
    monitor-exit v4
    :try_end_2d
    .catchall {:try_start_8 .. :try_end_2d} :catchall_2b

    #@2d
    throw v3
.end method
