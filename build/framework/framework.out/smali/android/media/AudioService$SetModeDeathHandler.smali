.class Landroid/media/AudioService$SetModeDeathHandler;
.super Ljava/lang/Object;
.source "AudioService.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/AudioService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SetModeDeathHandler"
.end annotation


# instance fields
.field private mCb:Landroid/os/IBinder;

.field private mMode:I

.field private mPid:I

.field private mState:I

.field final synthetic this$0:Landroid/media/AudioService;


# direct methods
.method constructor <init>(Landroid/media/AudioService;Landroid/os/IBinder;I)V
    .registers 5
    .parameter
    .parameter "cb"
    .parameter "pid"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1813
    iput-object p1, p0, Landroid/media/AudioService$SetModeDeathHandler;->this$0:Landroid/media/AudioService;

    #@3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 1810
    iput v0, p0, Landroid/media/AudioService$SetModeDeathHandler;->mMode:I

    #@8
    .line 1814
    iput-object p2, p0, Landroid/media/AudioService$SetModeDeathHandler;->mCb:Landroid/os/IBinder;

    #@a
    .line 1815
    iput p3, p0, Landroid/media/AudioService$SetModeDeathHandler;->mPid:I

    #@c
    .line 1816
    iput v0, p0, Landroid/media/AudioService$SetModeDeathHandler;->mState:I

    #@e
    .line 1817
    return-void
.end method


# virtual methods
.method public binderDied()V
    .registers 8

    #@0
    .prologue
    .line 1820
    const/4 v1, 0x0

    #@1
    .line 1821
    .local v1, newModeOwnerPid:I
    iget-object v2, p0, Landroid/media/AudioService$SetModeDeathHandler;->this$0:Landroid/media/AudioService;

    #@3
    invoke-static {v2}, Landroid/media/AudioService;->access$1400(Landroid/media/AudioService;)Ljava/util/ArrayList;

    #@6
    move-result-object v3

    #@7
    monitor-enter v3

    #@8
    .line 1822
    :try_start_8
    const-string v2, "AudioService"

    #@a
    const-string/jumbo v4, "setMode() client died"

    #@d
    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 1823
    iget-object v2, p0, Landroid/media/AudioService$SetModeDeathHandler;->this$0:Landroid/media/AudioService;

    #@12
    invoke-static {v2}, Landroid/media/AudioService;->access$1400(Landroid/media/AudioService;)Ljava/util/ArrayList;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2, p0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    #@19
    move-result v0

    #@1a
    .line 1824
    .local v0, index:I
    if-gez v0, :cond_2d

    #@1c
    .line 1825
    const-string v2, "AudioService"

    #@1e
    const-string/jumbo v4, "unregistered setMode() client died"

    #@21
    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 1829
    :goto_24
    monitor-exit v3
    :try_end_25
    .catchall {:try_start_8 .. :try_end_25} :catchall_39

    #@25
    .line 1832
    if-eqz v1, :cond_2c

    #@27
    .line 1833
    iget-object v2, p0, Landroid/media/AudioService$SetModeDeathHandler;->this$0:Landroid/media/AudioService;

    #@29
    invoke-static {v2, v1}, Landroid/media/AudioService;->access$1500(Landroid/media/AudioService;I)V

    #@2c
    .line 1835
    :cond_2c
    return-void

    #@2d
    .line 1827
    :cond_2d
    :try_start_2d
    iget-object v2, p0, Landroid/media/AudioService$SetModeDeathHandler;->this$0:Landroid/media/AudioService;

    #@2f
    const/4 v4, 0x0

    #@30
    iget-object v5, p0, Landroid/media/AudioService$SetModeDeathHandler;->mCb:Landroid/os/IBinder;

    #@32
    iget v6, p0, Landroid/media/AudioService$SetModeDeathHandler;->mPid:I

    #@34
    invoke-virtual {v2, v4, v5, v6}, Landroid/media/AudioService;->setModeInt(ILandroid/os/IBinder;I)I

    #@37
    move-result v1

    #@38
    goto :goto_24

    #@39
    .line 1829
    .end local v0           #index:I
    :catchall_39
    move-exception v2

    #@3a
    monitor-exit v3
    :try_end_3b
    .catchall {:try_start_2d .. :try_end_3b} :catchall_39

    #@3b
    throw v2
.end method

.method public getBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 1865
    iget-object v0, p0, Landroid/media/AudioService$SetModeDeathHandler;->mCb:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getInCallMode()I
    .registers 2

    #@0
    .prologue
    .line 1861
    iget v0, p0, Landroid/media/AudioService$SetModeDeathHandler;->mState:I

    #@2
    return v0
.end method

.method public getMode()I
    .registers 2

    #@0
    .prologue
    .line 1857
    iget v0, p0, Landroid/media/AudioService$SetModeDeathHandler;->mMode:I

    #@2
    return v0
.end method

.method public getPid()I
    .registers 2

    #@0
    .prologue
    .line 1838
    iget v0, p0, Landroid/media/AudioService$SetModeDeathHandler;->mPid:I

    #@2
    return v0
.end method

.method public setInCallMode(I)V
    .registers 3
    .parameter "state"

    #@0
    .prologue
    .line 1852
    const/4 v0, 0x2

    #@1
    iput v0, p0, Landroid/media/AudioService$SetModeDeathHandler;->mMode:I

    #@3
    .line 1853
    iput p1, p0, Landroid/media/AudioService$SetModeDeathHandler;->mState:I

    #@5
    .line 1854
    return-void
.end method

.method public setMode(I)V
    .registers 3
    .parameter "mode"

    #@0
    .prologue
    .line 1842
    iput p1, p0, Landroid/media/AudioService$SetModeDeathHandler;->mMode:I

    #@2
    .line 1843
    const/4 v0, 0x2

    #@3
    if-ne p1, v0, :cond_9

    #@5
    .line 1844
    const/4 v0, 0x1

    #@6
    iput v0, p0, Landroid/media/AudioService$SetModeDeathHandler;->mState:I

    #@8
    .line 1849
    :goto_8
    return-void

    #@9
    .line 1847
    :cond_9
    const/4 v0, 0x0

    #@a
    iput v0, p0, Landroid/media/AudioService$SetModeDeathHandler;->mState:I

    #@c
    goto :goto_8
.end method
