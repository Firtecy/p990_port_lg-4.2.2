.class public Landroid/media/MiniThumbFile;
.super Ljava/lang/Object;
.source "MiniThumbFile.java"


# static fields
.field public static final BYTES_PER_MINTHUMB:I = 0x2710

.field private static final HEADER_SIZE:I = 0xd

.field private static final MINI_THUMB_DATA_FILE_VERSION:I = 0x3

.field private static final TAG:Ljava/lang/String; = "MiniThumbFile"

.field private static final sThumbFiles:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Landroid/media/MiniThumbFile;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mBuffer:Ljava/nio/ByteBuffer;

.field private mChannel:Ljava/nio/channels/FileChannel;

.field mCr:Landroid/content/ContentResolver;

.field private mMiniThumbFile:Ljava/io/RandomAccessFile;

.field mOrigColumnName:Ljava/lang/String;

.field private mThumbUri:Landroid/net/Uri;

.field private mUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 64
    new-instance v0, Ljava/util/Hashtable;

    #@2
    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    #@5
    sput-object v0, Landroid/media/MiniThumbFile;->sThumbFiles:Ljava/util/Hashtable;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;)V
    .registers 5
    .parameter "uri"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 145
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 146
    iput-object p1, p0, Landroid/media/MiniThumbFile;->mUri:Landroid/net/Uri;

    #@6
    .line 147
    const/16 v0, 0x2710

    #@8
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    #@b
    move-result-object v0

    #@c
    iput-object v0, p0, Landroid/media/MiniThumbFile;->mBuffer:Ljava/nio/ByteBuffer;

    #@e
    .line 149
    const-string/jumbo v0, "video"

    #@11
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@14
    move-result-object v1

    #@15
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v0

    #@1d
    if-eqz v0, :cond_3a

    #@1f
    sget-object v0, Landroid/provider/MediaStore$Video$Thumbnails;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    #@21
    :goto_21
    iput-object v0, p0, Landroid/media/MiniThumbFile;->mThumbUri:Landroid/net/Uri;

    #@23
    .line 152
    const-string/jumbo v0, "video"

    #@26
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@29
    move-result-object v1

    #@2a
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@31
    move-result v0

    #@32
    if-eqz v0, :cond_3d

    #@34
    const-string/jumbo v0, "video_id"

    #@37
    :goto_37
    iput-object v0, p0, Landroid/media/MiniThumbFile;->mOrigColumnName:Ljava/lang/String;

    #@39
    .line 156
    return-void

    #@3a
    .line 149
    :cond_3a
    sget-object v0, Landroid/provider/MediaStore$Images$Thumbnails;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    #@3c
    goto :goto_21

    #@3d
    .line 152
    :cond_3d
    const-string v0, "image_id"

    #@3f
    goto :goto_37
.end method

.method private getNewID(J)J
    .registers 13
    .parameter "id"

    #@0
    .prologue
    const-wide/16 v7, 0x0

    #@2
    const/4 v4, 0x0

    #@3
    const/4 v9, 0x0

    #@4
    .line 318
    iget-object v0, p0, Landroid/media/MiniThumbFile;->mCr:Landroid/content/ContentResolver;

    #@6
    if-nez v0, :cond_a

    #@8
    move-wide v0, v7

    #@9
    .line 326
    :goto_9
    return-wide v0

    #@a
    .line 319
    :cond_a
    iget-object v0, p0, Landroid/media/MiniThumbFile;->mCr:Landroid/content/ContentResolver;

    #@c
    iget-object v1, p0, Landroid/media/MiniThumbFile;->mThumbUri:Landroid/net/Uri;

    #@e
    const/4 v2, 0x1

    #@f
    new-array v2, v2, [Ljava/lang/String;

    #@11
    const-string v3, "_id"

    #@13
    aput-object v3, v2, v9

    #@15
    new-instance v3, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    iget-object v5, p0, Landroid/media/MiniThumbFile;->mOrigColumnName:Ljava/lang/String;

    #@1c
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    const-string v5, " = "

    #@22
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@29
    move-result-object v3

    #@2a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v3

    #@2e
    move-object v5, v4

    #@2f
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@32
    move-result-object v6

    #@33
    .line 320
    .local v6, c:Landroid/database/Cursor;
    if-eqz v6, :cond_43

    #@35
    .line 321
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@38
    move-result v0

    #@39
    if-eqz v0, :cond_40

    #@3b
    .line 322
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getLong(I)J

    #@3e
    move-result-wide v0

    #@3f
    goto :goto_9

    #@40
    .line 324
    :cond_40
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@43
    :cond_43
    move-wide v0, v7

    #@44
    .line 326
    goto :goto_9
.end method

.method public static declared-synchronized instance(Landroid/net/Uri;)Landroid/media/MiniThumbFile;
    .registers 6
    .parameter "uri"

    #@0
    .prologue
    .line 84
    const-class v3, Landroid/media/MiniThumbFile;

    #@2
    monitor-enter v3

    #@3
    :try_start_3
    invoke-virtual {p0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@6
    move-result-object v2

    #@7
    const/4 v4, 0x1

    #@8
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@b
    move-result-object v1

    #@c
    check-cast v1, Ljava/lang/String;

    #@e
    .line 85
    .local v1, type:Ljava/lang/String;
    sget-object v2, Landroid/media/MiniThumbFile;->sThumbFiles:Ljava/util/Hashtable;

    #@10
    invoke-virtual {v2, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Landroid/media/MiniThumbFile;

    #@16
    .line 87
    .local v0, file:Landroid/media/MiniThumbFile;
    if-nez v0, :cond_3f

    #@18
    .line 88
    new-instance v0, Landroid/media/MiniThumbFile;

    #@1a
    .end local v0           #file:Landroid/media/MiniThumbFile;
    new-instance v2, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v4, "content://media/external/"

    #@21
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    const-string v4, "/media"

    #@2b
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v2

    #@33
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@36
    move-result-object v2

    #@37
    invoke-direct {v0, v2}, Landroid/media/MiniThumbFile;-><init>(Landroid/net/Uri;)V

    #@3a
    .line 90
    .restart local v0       #file:Landroid/media/MiniThumbFile;
    sget-object v2, Landroid/media/MiniThumbFile;->sThumbFiles:Ljava/util/Hashtable;

    #@3c
    invoke-virtual {v2, v1, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3f
    .catchall {:try_start_3 .. :try_end_3f} :catchall_41

    #@3f
    .line 93
    :cond_3f
    monitor-exit v3

    #@40
    return-object v0

    #@41
    .line 84
    .end local v0           #file:Landroid/media/MiniThumbFile;
    .end local v1           #type:Ljava/lang/String;
    :catchall_41
    move-exception v2

    #@42
    monitor-exit v3

    #@43
    throw v2
.end method

.method private miniThumbDataFile()Ljava/io/RandomAccessFile;
    .registers 8

    #@0
    .prologue
    .line 116
    iget-object v4, p0, Landroid/media/MiniThumbFile;->mMiniThumbFile:Ljava/io/RandomAccessFile;

    #@2
    if-nez v4, :cond_58

    #@4
    .line 117
    invoke-direct {p0}, Landroid/media/MiniThumbFile;->removeOldFile()V

    #@7
    .line 118
    const/4 v4, 0x3

    #@8
    invoke-direct {p0, v4}, Landroid/media/MiniThumbFile;->randomAccessFilePath(I)Ljava/lang/String;

    #@b
    move-result-object v3

    #@c
    .line 119
    .local v3, path:Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    #@e
    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@11
    invoke-virtual {v4}, Ljava/io/File;->getParentFile()Ljava/io/File;

    #@14
    move-result-object v0

    #@15
    .line 120
    .local v0, directory:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    #@18
    move-result v4

    #@19
    if-nez v4, :cond_3d

    #@1b
    .line 121
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    #@1e
    move-result v4

    #@1f
    if-nez v4, :cond_3d

    #@21
    .line 122
    const-string v4, "MiniThumbFile"

    #@23
    new-instance v5, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v6, "Unable to create .thumbnails directory "

    #@2a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v5

    #@2e
    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    #@31
    move-result-object v6

    #@32
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v5

    #@36
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v5

    #@3a
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 126
    :cond_3d
    new-instance v2, Ljava/io/File;

    #@3f
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@42
    .line 128
    .local v2, f:Ljava/io/File;
    :try_start_42
    new-instance v4, Ljava/io/RandomAccessFile;

    #@44
    const-string/jumbo v5, "rw"

    #@47
    invoke-direct {v4, v2, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@4a
    iput-object v4, p0, Landroid/media/MiniThumbFile;->mMiniThumbFile:Ljava/io/RandomAccessFile;
    :try_end_4c
    .catch Ljava/io/IOException; {:try_start_42 .. :try_end_4c} :catch_5b

    #@4c
    .line 138
    :goto_4c
    iget-object v4, p0, Landroid/media/MiniThumbFile;->mMiniThumbFile:Ljava/io/RandomAccessFile;

    #@4e
    if-eqz v4, :cond_58

    #@50
    .line 139
    iget-object v4, p0, Landroid/media/MiniThumbFile;->mMiniThumbFile:Ljava/io/RandomAccessFile;

    #@52
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    #@55
    move-result-object v4

    #@56
    iput-object v4, p0, Landroid/media/MiniThumbFile;->mChannel:Ljava/nio/channels/FileChannel;

    #@58
    .line 142
    .end local v0           #directory:Ljava/io/File;
    .end local v2           #f:Ljava/io/File;
    .end local v3           #path:Ljava/lang/String;
    :cond_58
    iget-object v4, p0, Landroid/media/MiniThumbFile;->mMiniThumbFile:Ljava/io/RandomAccessFile;

    #@5a
    return-object v4

    #@5b
    .line 129
    .restart local v0       #directory:Ljava/io/File;
    .restart local v2       #f:Ljava/io/File;
    .restart local v3       #path:Ljava/lang/String;
    :catch_5b
    move-exception v1

    #@5c
    .line 133
    .local v1, ex:Ljava/io/IOException;
    :try_start_5c
    new-instance v4, Ljava/io/RandomAccessFile;

    #@5e
    const-string/jumbo v5, "r"

    #@61
    invoke-direct {v4, v2, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@64
    iput-object v4, p0, Landroid/media/MiniThumbFile;->mMiniThumbFile:Ljava/io/RandomAccessFile;
    :try_end_66
    .catch Ljava/io/IOException; {:try_start_5c .. :try_end_66} :catch_67

    #@66
    goto :goto_4c

    #@67
    .line 134
    :catch_67
    move-exception v4

    #@68
    goto :goto_4c
.end method

.method private randomAccessFilePath(I)Ljava/lang/String;
    .registers 5
    .parameter "version"

    #@0
    .prologue
    .line 97
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    #@8
    move-result-object v2

    #@9
    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, "/DCIM/.thumbnails"

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    .line 100
    .local v0, directoryName:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    const-string v2, "/.thumbdata"

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    const-string v2, "-"

    #@30
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    iget-object v2, p0, Landroid/media/MiniThumbFile;->mUri:Landroid/net/Uri;

    #@36
    invoke-virtual {v2}, Landroid/net/Uri;->hashCode()I

    #@39
    move-result v2

    #@3a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v1

    #@3e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v1

    #@42
    return-object v1
.end method

.method private removeOldFile()V
    .registers 4

    #@0
    .prologue
    .line 104
    const/4 v2, 0x2

    #@1
    invoke-direct {p0, v2}, Landroid/media/MiniThumbFile;->randomAccessFilePath(I)Ljava/lang/String;

    #@4
    move-result-object v1

    #@5
    .line 105
    .local v1, oldPath:Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    #@7
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@a
    .line 106
    .local v0, oldFile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_13

    #@10
    .line 108
    :try_start_10
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_13
    .catch Ljava/lang/SecurityException; {:try_start_10 .. :try_end_13} :catch_14

    #@13
    .line 113
    :cond_13
    :goto_13
    return-void

    #@14
    .line 109
    :catch_14
    move-exception v2

    #@15
    goto :goto_13
.end method

.method public static declared-synchronized reset()V
    .registers 4

    #@0
    .prologue
    .line 77
    const-class v3, Landroid/media/MiniThumbFile;

    #@2
    monitor-enter v3

    #@3
    :try_start_3
    sget-object v2, Landroid/media/MiniThumbFile;->sThumbFiles:Ljava/util/Hashtable;

    #@5
    invoke-virtual {v2}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    #@8
    move-result-object v2

    #@9
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@c
    move-result-object v1

    #@d
    .local v1, i$:Ljava/util/Iterator;
    :goto_d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_20

    #@13
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@16
    move-result-object v0

    #@17
    check-cast v0, Landroid/media/MiniThumbFile;

    #@19
    .line 78
    .local v0, file:Landroid/media/MiniThumbFile;
    invoke-virtual {v0}, Landroid/media/MiniThumbFile;->deactivate()V
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_1d

    #@1c
    goto :goto_d

    #@1d
    .line 77
    .end local v0           #file:Landroid/media/MiniThumbFile;
    :catchall_1d
    move-exception v2

    #@1e
    monitor-exit v3

    #@1f
    throw v2

    #@20
    .line 80
    :cond_20
    :try_start_20
    sget-object v2, Landroid/media/MiniThumbFile;->sThumbFiles:Ljava/util/Hashtable;

    #@22
    invoke-virtual {v2}, Ljava/util/Hashtable;->clear()V
    :try_end_25
    .catchall {:try_start_20 .. :try_end_25} :catchall_1d

    #@25
    .line 81
    monitor-exit v3

    #@26
    return-void
.end method


# virtual methods
.method public declared-synchronized deactivate()V
    .registers 2

    #@0
    .prologue
    .line 159
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/media/MiniThumbFile;->mMiniThumbFile:Ljava/io/RandomAccessFile;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_f

    #@3
    if-eqz v0, :cond_d

    #@5
    .line 161
    :try_start_5
    iget-object v0, p0, Landroid/media/MiniThumbFile;->mMiniThumbFile:Ljava/io/RandomAccessFile;

    #@7
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    #@a
    .line 162
    const/4 v0, 0x0

    #@b
    iput-object v0, p0, Landroid/media/MiniThumbFile;->mMiniThumbFile:Ljava/io/RandomAccessFile;
    :try_end_d
    .catchall {:try_start_5 .. :try_end_d} :catchall_f
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_d} :catch_12

    #@d
    .line 167
    :cond_d
    :goto_d
    monitor-exit p0

    #@e
    return-void

    #@f
    .line 159
    :catchall_f
    move-exception v0

    #@10
    monitor-exit p0

    #@11
    throw v0

    #@12
    .line 163
    :catch_12
    move-exception v0

    #@13
    goto :goto_d
.end method

.method public declared-synchronized getMagic(J)J
    .registers 16
    .parameter "id"

    #@0
    .prologue
    const/16 v12, 0x9

    #@2
    const/4 v11, 0x1

    #@3
    const-wide/16 v9, 0x0

    #@5
    .line 175
    monitor-enter p0

    #@6
    :try_start_6
    invoke-direct {p0}, Landroid/media/MiniThumbFile;->miniThumbDataFile()Ljava/io/RandomAccessFile;

    #@9
    move-result-object v8

    #@a
    .line 176
    .local v8, r:Ljava/io/RandomAccessFile;
    if-eqz v8, :cond_5c

    #@c
    .line 179
    invoke-direct {p0, p1, p2}, Landroid/media/MiniThumbFile;->getNewID(J)J
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_a4

    #@f
    move-result-wide p1

    #@10
    .line 180
    cmp-long v0, p1, v9

    #@12
    if-nez v0, :cond_17

    #@14
    move-wide v3, v9

    #@15
    .line 213
    :cond_15
    :goto_15
    monitor-exit p0

    #@16
    return-wide v3

    #@17
    .line 183
    :cond_17
    const-wide/16 v3, 0x2710

    #@19
    mul-long v1, p1, v3

    #@1b
    .line 184
    .local v1, pos:J
    const/4 v7, 0x0

    #@1c
    .line 186
    .local v7, lock:Ljava/nio/channels/FileLock;
    :try_start_1c
    iget-object v0, p0, Landroid/media/MiniThumbFile;->mBuffer:Ljava/nio/ByteBuffer;

    #@1e
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    #@21
    .line 187
    iget-object v0, p0, Landroid/media/MiniThumbFile;->mBuffer:Ljava/nio/ByteBuffer;

    #@23
    const/16 v3, 0x9

    #@25
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    #@28
    .line 189
    iget-object v0, p0, Landroid/media/MiniThumbFile;->mChannel:Ljava/nio/channels/FileChannel;

    #@2a
    const-wide/16 v3, 0x9

    #@2c
    const/4 v5, 0x1

    #@2d
    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->lock(JJZ)Ljava/nio/channels/FileLock;

    #@30
    move-result-object v7

    #@31
    .line 192
    iget-object v0, p0, Landroid/media/MiniThumbFile;->mChannel:Ljava/nio/channels/FileChannel;

    #@33
    iget-object v3, p0, Landroid/media/MiniThumbFile;->mBuffer:Ljava/nio/ByteBuffer;

    #@35
    invoke-virtual {v0, v3, v1, v2}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;J)I

    #@38
    move-result v0

    #@39
    if-ne v0, v12, :cond_57

    #@3b
    .line 193
    iget-object v0, p0, Landroid/media/MiniThumbFile;->mBuffer:Ljava/nio/ByteBuffer;

    #@3d
    const/4 v3, 0x0

    #@3e
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    #@41
    .line 194
    iget-object v0, p0, Landroid/media/MiniThumbFile;->mBuffer:Ljava/nio/ByteBuffer;

    #@43
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    #@46
    move-result v0

    #@47
    if-ne v0, v11, :cond_57

    #@49
    .line 195
    iget-object v0, p0, Landroid/media/MiniThumbFile;->mBuffer:Ljava/nio/ByteBuffer;

    #@4b
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getLong()J
    :try_end_4e
    .catchall {:try_start_1c .. :try_end_4e} :catchall_9d
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_4e} :catch_5e
    .catch Ljava/lang/RuntimeException; {:try_start_1c .. :try_end_4e} :catch_6e

    #@4e
    move-result-wide v3

    #@4f
    .line 206
    if-eqz v7, :cond_15

    #@51
    :try_start_51
    invoke-virtual {v7}, Ljava/nio/channels/FileLock;->release()V
    :try_end_54
    .catchall {:try_start_51 .. :try_end_54} :catchall_a4
    .catch Ljava/io/IOException; {:try_start_51 .. :try_end_54} :catch_55

    #@54
    goto :goto_15

    #@55
    .line 208
    :catch_55
    move-exception v0

    #@56
    goto :goto_15

    #@57
    .line 206
    :cond_57
    if-eqz v7, :cond_5c

    #@59
    :try_start_59
    invoke-virtual {v7}, Ljava/nio/channels/FileLock;->release()V
    :try_end_5c
    .catchall {:try_start_59 .. :try_end_5c} :catchall_a4
    .catch Ljava/io/IOException; {:try_start_59 .. :try_end_5c} :catch_a7

    #@5c
    .end local v1           #pos:J
    .end local v7           #lock:Ljava/nio/channels/FileLock;
    :cond_5c
    :goto_5c
    move-wide v3, v9

    #@5d
    .line 213
    goto :goto_15

    #@5e
    .line 198
    .restart local v1       #pos:J
    .restart local v7       #lock:Ljava/nio/channels/FileLock;
    :catch_5e
    move-exception v6

    #@5f
    .line 199
    .local v6, ex:Ljava/io/IOException;
    :try_start_5f
    const-string v0, "MiniThumbFile"

    #@61
    const-string v3, "Got exception checking file magic: "

    #@63
    invoke-static {v0, v3, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_66
    .catchall {:try_start_5f .. :try_end_66} :catchall_9d

    #@66
    .line 206
    if-eqz v7, :cond_5c

    #@68
    :try_start_68
    invoke-virtual {v7}, Ljava/nio/channels/FileLock;->release()V
    :try_end_6b
    .catchall {:try_start_68 .. :try_end_6b} :catchall_a4
    .catch Ljava/io/IOException; {:try_start_68 .. :try_end_6b} :catch_6c

    #@6b
    goto :goto_5c

    #@6c
    .line 208
    :catch_6c
    move-exception v0

    #@6d
    goto :goto_5c

    #@6e
    .line 200
    .end local v6           #ex:Ljava/io/IOException;
    :catch_6e
    move-exception v6

    #@6f
    .line 202
    .local v6, ex:Ljava/lang/RuntimeException;
    :try_start_6f
    const-string v0, "MiniThumbFile"

    #@71
    new-instance v3, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v4, "Got exception when reading magic, id = "

    #@78
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v3

    #@7c
    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v3

    #@80
    const-string v4, ", disk full or mount read-only? "

    #@82
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v3

    #@86
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@89
    move-result-object v4

    #@8a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v3

    #@8e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@91
    move-result-object v3

    #@92
    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_95
    .catchall {:try_start_6f .. :try_end_95} :catchall_9d

    #@95
    .line 206
    if-eqz v7, :cond_5c

    #@97
    :try_start_97
    invoke-virtual {v7}, Ljava/nio/channels/FileLock;->release()V
    :try_end_9a
    .catchall {:try_start_97 .. :try_end_9a} :catchall_a4
    .catch Ljava/io/IOException; {:try_start_97 .. :try_end_9a} :catch_9b

    #@9a
    goto :goto_5c

    #@9b
    .line 208
    :catch_9b
    move-exception v0

    #@9c
    goto :goto_5c

    #@9d
    .line 205
    .end local v6           #ex:Ljava/lang/RuntimeException;
    :catchall_9d
    move-exception v0

    #@9e
    .line 206
    if-eqz v7, :cond_a3

    #@a0
    :try_start_a0
    invoke-virtual {v7}, Ljava/nio/channels/FileLock;->release()V
    :try_end_a3
    .catchall {:try_start_a0 .. :try_end_a3} :catchall_a4
    .catch Ljava/io/IOException; {:try_start_a0 .. :try_end_a3} :catch_a9

    #@a3
    .line 210
    :cond_a3
    :goto_a3
    :try_start_a3
    throw v0
    :try_end_a4
    .catchall {:try_start_a3 .. :try_end_a4} :catchall_a4

    #@a4
    .line 175
    .end local v1           #pos:J
    .end local v7           #lock:Ljava/nio/channels/FileLock;
    .end local v8           #r:Ljava/io/RandomAccessFile;
    :catchall_a4
    move-exception v0

    #@a5
    monitor-exit p0

    #@a6
    throw v0

    #@a7
    .line 208
    .restart local v1       #pos:J
    .restart local v7       #lock:Ljava/nio/channels/FileLock;
    .restart local v8       #r:Ljava/io/RandomAccessFile;
    :catch_a7
    move-exception v0

    #@a8
    goto :goto_5c

    #@a9
    :catch_a9
    move-exception v3

    #@aa
    goto :goto_a3
.end method

.method public declared-synchronized getMiniThumbFromFile(J[B)[B
    .registers 20
    .parameter "id"
    .parameter "data"

    #@0
    .prologue
    .line 270
    monitor-enter p0

    #@1
    :try_start_1
    invoke-direct/range {p0 .. p0}, Landroid/media/MiniThumbFile;->miniThumbDataFile()Ljava/io/RandomAccessFile;
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_e7

    #@4
    move-result-object v14

    #@5
    .line 271
    .local v14, r:Ljava/io/RandomAccessFile;
    if-nez v14, :cond_b

    #@7
    const/16 p3, 0x0

    #@9
    .line 309
    .end local p3
    :cond_9
    :goto_9
    monitor-exit p0

    #@a
    return-object p3

    #@b
    .line 274
    .restart local p3
    :cond_b
    :try_start_b
    invoke-direct/range {p0 .. p2}, Landroid/media/MiniThumbFile;->getNewID(J)J
    :try_end_e
    .catchall {:try_start_b .. :try_end_e} :catchall_e7

    #@e
    move-result-wide p1

    #@f
    .line 275
    const-wide/16 v5, 0x0

    #@11
    cmp-long v2, p1, v5

    #@13
    if-nez v2, :cond_18

    #@15
    const/16 p3, 0x0

    #@17
    goto :goto_9

    #@18
    .line 278
    :cond_18
    const-wide/16 v5, 0x2710

    #@1a
    mul-long v3, p1, v5

    #@1c
    .line 279
    .local v3, pos:J
    const/4 v11, 0x0

    #@1d
    .line 281
    .local v11, lock:Ljava/nio/channels/FileLock;
    :try_start_1d
    move-object/from16 v0, p0

    #@1f
    iget-object v2, v0, Landroid/media/MiniThumbFile;->mBuffer:Ljava/nio/ByteBuffer;

    #@21
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    #@24
    .line 282
    move-object/from16 v0, p0

    #@26
    iget-object v2, v0, Landroid/media/MiniThumbFile;->mChannel:Ljava/nio/channels/FileChannel;

    #@28
    const-wide/16 v5, 0x2710

    #@2a
    const/4 v7, 0x1

    #@2b
    invoke-virtual/range {v2 .. v7}, Ljava/nio/channels/FileChannel;->lock(JJZ)Ljava/nio/channels/FileLock;

    #@2e
    move-result-object v11

    #@2f
    .line 283
    move-object/from16 v0, p0

    #@31
    iget-object v2, v0, Landroid/media/MiniThumbFile;->mChannel:Ljava/nio/channels/FileChannel;

    #@33
    move-object/from16 v0, p0

    #@35
    iget-object v5, v0, Landroid/media/MiniThumbFile;->mBuffer:Ljava/nio/ByteBuffer;

    #@37
    invoke-virtual {v2, v5, v3, v4}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;J)I

    #@3a
    move-result v15

    #@3b
    .line 284
    .local v15, size:I
    const/16 v2, 0xd

    #@3d
    if-le v15, v2, :cond_7a

    #@3f
    .line 285
    move-object/from16 v0, p0

    #@41
    iget-object v2, v0, Landroid/media/MiniThumbFile;->mBuffer:Ljava/nio/ByteBuffer;

    #@43
    const/4 v5, 0x0

    #@44
    invoke-virtual {v2, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    #@47
    .line 286
    move-object/from16 v0, p0

    #@49
    iget-object v2, v0, Landroid/media/MiniThumbFile;->mBuffer:Ljava/nio/ByteBuffer;

    #@4b
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->get()B

    #@4e
    move-result v9

    #@4f
    .line 287
    .local v9, flag:B
    move-object/from16 v0, p0

    #@51
    iget-object v2, v0, Landroid/media/MiniThumbFile;->mBuffer:Ljava/nio/ByteBuffer;

    #@53
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getLong()J

    #@56
    move-result-wide v12

    #@57
    .line 288
    .local v12, magic:J
    move-object/from16 v0, p0

    #@59
    iget-object v2, v0, Landroid/media/MiniThumbFile;->mBuffer:Ljava/nio/ByteBuffer;

    #@5b
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I

    #@5e
    move-result v10

    #@5f
    .line 290
    .local v10, length:I
    add-int/lit8 v2, v10, 0xd

    #@61
    if-lt v15, v2, :cond_7a

    #@63
    move-object/from16 v0, p3

    #@65
    array-length v2, v0

    #@66
    if-lt v2, v10, :cond_7a

    #@68
    .line 291
    move-object/from16 v0, p0

    #@6a
    iget-object v2, v0, Landroid/media/MiniThumbFile;->mBuffer:Ljava/nio/ByteBuffer;

    #@6c
    const/4 v5, 0x0

    #@6d
    move-object/from16 v0, p3

    #@6f
    invoke-virtual {v2, v0, v5, v10}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;
    :try_end_72
    .catchall {:try_start_1d .. :try_end_72} :catchall_e0
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_72} :catch_82
    .catch Ljava/lang/RuntimeException; {:try_start_1d .. :try_end_72} :catch_af

    #@72
    .line 303
    if-eqz v11, :cond_9

    #@74
    :try_start_74
    invoke-virtual {v11}, Ljava/nio/channels/FileLock;->release()V
    :try_end_77
    .catchall {:try_start_74 .. :try_end_77} :catchall_e7
    .catch Ljava/io/IOException; {:try_start_74 .. :try_end_77} :catch_78

    #@77
    goto :goto_9

    #@78
    .line 305
    :catch_78
    move-exception v2

    #@79
    goto :goto_9

    #@7a
    .line 303
    .end local v9           #flag:B
    .end local v10           #length:I
    .end local v12           #magic:J
    :cond_7a
    if-eqz v11, :cond_7f

    #@7c
    :try_start_7c
    invoke-virtual {v11}, Ljava/nio/channels/FileLock;->release()V
    :try_end_7f
    .catchall {:try_start_7c .. :try_end_7f} :catchall_e7
    .catch Ljava/io/IOException; {:try_start_7c .. :try_end_7f} :catch_ea

    #@7f
    .line 309
    .end local v15           #size:I
    :cond_7f
    :goto_7f
    const/16 p3, 0x0

    #@81
    goto :goto_9

    #@82
    .line 295
    :catch_82
    move-exception v8

    #@83
    .line 296
    .local v8, ex:Ljava/io/IOException;
    :try_start_83
    const-string v2, "MiniThumbFile"

    #@85
    new-instance v5, Ljava/lang/StringBuilder;

    #@87
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@8a
    const-string v6, "got exception when reading thumbnail id="

    #@8c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v5

    #@90
    move-wide/from16 v0, p1

    #@92
    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@95
    move-result-object v5

    #@96
    const-string v6, ", exception: "

    #@98
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v5

    #@9c
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v5

    #@a0
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a3
    move-result-object v5

    #@a4
    invoke-static {v2, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a7
    .catchall {:try_start_83 .. :try_end_a7} :catchall_e0

    #@a7
    .line 303
    if-eqz v11, :cond_7f

    #@a9
    :try_start_a9
    invoke-virtual {v11}, Ljava/nio/channels/FileLock;->release()V
    :try_end_ac
    .catchall {:try_start_a9 .. :try_end_ac} :catchall_e7
    .catch Ljava/io/IOException; {:try_start_a9 .. :try_end_ac} :catch_ad

    #@ac
    goto :goto_7f

    #@ad
    .line 305
    :catch_ad
    move-exception v2

    #@ae
    goto :goto_7f

    #@af
    .line 297
    .end local v8           #ex:Ljava/io/IOException;
    :catch_af
    move-exception v8

    #@b0
    .line 299
    .local v8, ex:Ljava/lang/RuntimeException;
    :try_start_b0
    const-string v2, "MiniThumbFile"

    #@b2
    new-instance v5, Ljava/lang/StringBuilder;

    #@b4
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@b7
    const-string v6, "Got exception when reading thumbnail, id = "

    #@b9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v5

    #@bd
    move-wide/from16 v0, p1

    #@bf
    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v5

    #@c3
    const-string v6, ", disk full or mount read-only? "

    #@c5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v5

    #@c9
    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@cc
    move-result-object v6

    #@cd
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v5

    #@d1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d4
    move-result-object v5

    #@d5
    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d8
    .catchall {:try_start_b0 .. :try_end_d8} :catchall_e0

    #@d8
    .line 303
    if-eqz v11, :cond_7f

    #@da
    :try_start_da
    invoke-virtual {v11}, Ljava/nio/channels/FileLock;->release()V
    :try_end_dd
    .catchall {:try_start_da .. :try_end_dd} :catchall_e7
    .catch Ljava/io/IOException; {:try_start_da .. :try_end_dd} :catch_de

    #@dd
    goto :goto_7f

    #@de
    .line 305
    :catch_de
    move-exception v2

    #@df
    goto :goto_7f

    #@e0
    .line 302
    .end local v8           #ex:Ljava/lang/RuntimeException;
    :catchall_e0
    move-exception v2

    #@e1
    .line 303
    if-eqz v11, :cond_e6

    #@e3
    :try_start_e3
    invoke-virtual {v11}, Ljava/nio/channels/FileLock;->release()V
    :try_end_e6
    .catchall {:try_start_e3 .. :try_end_e6} :catchall_e7
    .catch Ljava/io/IOException; {:try_start_e3 .. :try_end_e6} :catch_ec

    #@e6
    .line 307
    :cond_e6
    :goto_e6
    :try_start_e6
    throw v2
    :try_end_e7
    .catchall {:try_start_e6 .. :try_end_e7} :catchall_e7

    #@e7
    .line 270
    .end local v3           #pos:J
    .end local v11           #lock:Ljava/nio/channels/FileLock;
    .end local v14           #r:Ljava/io/RandomAccessFile;
    :catchall_e7
    move-exception v2

    #@e8
    monitor-exit p0

    #@e9
    throw v2

    #@ea
    .line 305
    .restart local v3       #pos:J
    .restart local v11       #lock:Ljava/nio/channels/FileLock;
    .restart local v14       #r:Ljava/io/RandomAccessFile;
    .restart local v15       #size:I
    :catch_ea
    move-exception v2

    #@eb
    goto :goto_7f

    #@ec
    .end local v15           #size:I
    :catch_ec
    move-exception v5

    #@ed
    goto :goto_e6
.end method

.method public declared-synchronized saveMiniThumbToFile([BJJ)V
    .registers 15
    .parameter "data"
    .parameter "id"
    .parameter "magic"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 218
    monitor-enter p0

    #@1
    :try_start_1
    invoke-direct {p0}, Landroid/media/MiniThumbFile;->miniThumbDataFile()Ljava/io/RandomAccessFile;
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_86

    #@4
    move-result-object v8

    #@5
    .line 219
    .local v8, r:Ljava/io/RandomAccessFile;
    if-nez v8, :cond_9

    #@7
    .line 260
    :cond_7
    :goto_7
    monitor-exit p0

    #@8
    return-void

    #@9
    .line 222
    :cond_9
    :try_start_9
    invoke-direct {p0, p2, p3}, Landroid/media/MiniThumbFile;->getNewID(J)J
    :try_end_c
    .catchall {:try_start_9 .. :try_end_c} :catchall_86

    #@c
    move-result-wide p2

    #@d
    .line 223
    const-wide/16 v3, 0x0

    #@f
    cmp-long v0, p2, v3

    #@11
    if-eqz v0, :cond_7

    #@13
    .line 226
    const-wide/16 v3, 0x2710

    #@15
    mul-long v1, p2, v3

    #@17
    .line 227
    .local v1, pos:J
    const/4 v7, 0x0

    #@18
    .line 229
    .local v7, lock:Ljava/nio/channels/FileLock;
    if-eqz p1, :cond_57

    #@1a
    .line 230
    :try_start_1a
    array-length v0, p1
    :try_end_1b
    .catchall {:try_start_1a .. :try_end_1b} :catchall_7f
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1b} :catch_5f
    .catch Ljava/lang/RuntimeException; {:try_start_1a .. :try_end_1b} :catch_89

    #@1b
    const/16 v3, 0x2703

    #@1d
    if-le v0, v3, :cond_27

    #@1f
    .line 254
    if-eqz v7, :cond_7

    #@21
    :try_start_21
    #Replaced unresolvable odex instruction with a throw
    throw v7
    #invoke-virtual-quick {v7}, vtable@0x10
    :try_end_24
    .catchall {:try_start_21 .. :try_end_24} :catchall_86
    .catch Ljava/io/IOException; {:try_start_21 .. :try_end_24} :catch_25

    #@24
    goto :goto_7

    #@25
    .line 256
    :catch_25
    move-exception v0

    #@26
    goto :goto_7

    #@27
    .line 234
    :cond_27
    :try_start_27
    iget-object v0, p0, Landroid/media/MiniThumbFile;->mBuffer:Ljava/nio/ByteBuffer;

    #@29
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    #@2c
    .line 235
    iget-object v0, p0, Landroid/media/MiniThumbFile;->mBuffer:Ljava/nio/ByteBuffer;

    #@2e
    const/4 v3, 0x1

    #@2f
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@32
    .line 236
    iget-object v0, p0, Landroid/media/MiniThumbFile;->mBuffer:Ljava/nio/ByteBuffer;

    #@34
    invoke-virtual {v0, p4, p5}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    #@37
    .line 237
    iget-object v0, p0, Landroid/media/MiniThumbFile;->mBuffer:Ljava/nio/ByteBuffer;

    #@39
    array-length v3, p1

    #@3a
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@3d
    .line 238
    iget-object v0, p0, Landroid/media/MiniThumbFile;->mBuffer:Ljava/nio/ByteBuffer;

    #@3f
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@42
    .line 239
    iget-object v0, p0, Landroid/media/MiniThumbFile;->mBuffer:Ljava/nio/ByteBuffer;

    #@44
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    #@47
    .line 241
    iget-object v0, p0, Landroid/media/MiniThumbFile;->mChannel:Ljava/nio/channels/FileChannel;

    #@49
    const-wide/16 v3, 0x2710

    #@4b
    const/4 v5, 0x0

    #@4c
    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->lock(JJZ)Ljava/nio/channels/FileLock;

    #@4f
    move-result-object v7

    #@50
    .line 242
    iget-object v0, p0, Landroid/media/MiniThumbFile;->mChannel:Ljava/nio/channels/FileChannel;

    #@52
    iget-object v3, p0, Landroid/media/MiniThumbFile;->mBuffer:Ljava/nio/ByteBuffer;

    #@54
    invoke-virtual {v0, v3, v1, v2}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;J)I
    :try_end_57
    .catchall {:try_start_27 .. :try_end_57} :catchall_7f
    .catch Ljava/io/IOException; {:try_start_27 .. :try_end_57} :catch_5f
    .catch Ljava/lang/RuntimeException; {:try_start_27 .. :try_end_57} :catch_89

    #@57
    .line 254
    :cond_57
    if-eqz v7, :cond_7

    #@59
    :try_start_59
    invoke-virtual {v7}, Ljava/nio/channels/FileLock;->release()V
    :try_end_5c
    .catchall {:try_start_59 .. :try_end_5c} :catchall_86
    .catch Ljava/io/IOException; {:try_start_59 .. :try_end_5c} :catch_5d

    #@5c
    goto :goto_7

    #@5d
    .line 256
    :catch_5d
    move-exception v0

    #@5e
    goto :goto_7

    #@5f
    .line 244
    :catch_5f
    move-exception v6

    #@60
    .line 245
    .local v6, ex:Ljava/io/IOException;
    :try_start_60
    const-string v0, "MiniThumbFile"

    #@62
    new-instance v3, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    const-string v4, "couldn\'t save mini thumbnail data for "

    #@69
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v3

    #@6d
    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@70
    move-result-object v3

    #@71
    const-string v4, "; "

    #@73
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v3

    #@77
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v3

    #@7b
    invoke-static {v0, v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@7e
    .line 247
    throw v6
    :try_end_7f
    .catchall {:try_start_60 .. :try_end_7f} :catchall_7f

    #@7f
    .line 253
    .end local v6           #ex:Ljava/io/IOException;
    :catchall_7f
    move-exception v0

    #@80
    .line 254
    if-eqz v7, :cond_85

    #@82
    :try_start_82
    invoke-virtual {v7}, Ljava/nio/channels/FileLock;->release()V
    :try_end_85
    .catchall {:try_start_82 .. :try_end_85} :catchall_86
    .catch Ljava/io/IOException; {:try_start_82 .. :try_end_85} :catch_ba

    #@85
    .line 258
    :cond_85
    :goto_85
    :try_start_85
    throw v0
    :try_end_86
    .catchall {:try_start_85 .. :try_end_86} :catchall_86

    #@86
    .line 218
    .end local v1           #pos:J
    .end local v7           #lock:Ljava/nio/channels/FileLock;
    .end local v8           #r:Ljava/io/RandomAccessFile;
    :catchall_86
    move-exception v0

    #@87
    monitor-exit p0

    #@88
    throw v0

    #@89
    .line 248
    .restart local v1       #pos:J
    .restart local v7       #lock:Ljava/nio/channels/FileLock;
    .restart local v8       #r:Ljava/io/RandomAccessFile;
    :catch_89
    move-exception v6

    #@8a
    .line 250
    .local v6, ex:Ljava/lang/RuntimeException;
    :try_start_8a
    const-string v0, "MiniThumbFile"

    #@8c
    new-instance v3, Ljava/lang/StringBuilder;

    #@8e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@91
    const-string v4, "couldn\'t save mini thumbnail data for "

    #@93
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v3

    #@97
    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v3

    #@9b
    const-string v4, "; disk full or mount read-only? "

    #@9d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v3

    #@a1
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@a4
    move-result-object v4

    #@a5
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v3

    #@a9
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ac
    move-result-object v3

    #@ad
    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b0
    .catchall {:try_start_8a .. :try_end_b0} :catchall_7f

    #@b0
    .line 254
    if-eqz v7, :cond_7

    #@b2
    :try_start_b2
    invoke-virtual {v7}, Ljava/nio/channels/FileLock;->release()V
    :try_end_b5
    .catchall {:try_start_b2 .. :try_end_b5} :catchall_86
    .catch Ljava/io/IOException; {:try_start_b2 .. :try_end_b5} :catch_b7

    #@b5
    goto/16 :goto_7

    #@b7
    .line 256
    :catch_b7
    move-exception v0

    #@b8
    goto/16 :goto_7

    #@ba
    .end local v6           #ex:Ljava/lang/RuntimeException;
    :catch_ba
    move-exception v3

    #@bb
    goto :goto_85
.end method

.method public setCR(Landroid/content/ContentResolver;)V
    .registers 2
    .parameter "cr"

    #@0
    .prologue
    .line 314
    iput-object p1, p0, Landroid/media/MiniThumbFile;->mCr:Landroid/content/ContentResolver;

    #@2
    .line 315
    return-void
.end method
