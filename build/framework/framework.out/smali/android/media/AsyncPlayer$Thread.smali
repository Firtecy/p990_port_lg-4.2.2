.class final Landroid/media/AsyncPlayer$Thread;
.super Ljava/lang/Thread;
.source "AsyncPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/AsyncPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "Thread"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/media/AsyncPlayer;


# direct methods
.method constructor <init>(Landroid/media/AsyncPlayer;)V
    .registers 4
    .parameter

    #@0
    .prologue
    .line 81
    iput-object p1, p0, Landroid/media/AsyncPlayer$Thread;->this$0:Landroid/media/AsyncPlayer;

    #@2
    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v1, "AsyncPlayer-"

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    invoke-static {p1}, Landroid/media/AsyncPlayer;->access$000(Landroid/media/AsyncPlayer;)Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    #@1c
    .line 83
    return-void
.end method


# virtual methods
.method public run()V
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 87
    :goto_1
    const/4 v1, 0x0

    #@2
    .line 89
    .local v1, cmd:Landroid/media/AsyncPlayer$Command;
    iget-object v4, p0, Landroid/media/AsyncPlayer$Thread;->this$0:Landroid/media/AsyncPlayer;

    #@4
    invoke-static {v4}, Landroid/media/AsyncPlayer;->access$100(Landroid/media/AsyncPlayer;)Ljava/util/LinkedList;

    #@7
    move-result-object v5

    #@8
    monitor-enter v5

    #@9
    .line 91
    :try_start_9
    iget-object v4, p0, Landroid/media/AsyncPlayer$Thread;->this$0:Landroid/media/AsyncPlayer;

    #@b
    invoke-static {v4}, Landroid/media/AsyncPlayer;->access$100(Landroid/media/AsyncPlayer;)Ljava/util/LinkedList;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {v4}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    #@12
    move-result-object v4

    #@13
    move-object v0, v4

    #@14
    check-cast v0, Landroid/media/AsyncPlayer$Command;

    #@16
    move-object v1, v0

    #@17
    .line 92
    monitor-exit v5
    :try_end_18
    .catchall {:try_start_9 .. :try_end_18} :catchall_3d

    #@18
    .line 94
    iget v4, v1, Landroid/media/AsyncPlayer$Command;->code:I

    #@1a
    packed-switch v4, :pswitch_data_aa

    #@1d
    .line 115
    :goto_1d
    iget-object v4, p0, Landroid/media/AsyncPlayer$Thread;->this$0:Landroid/media/AsyncPlayer;

    #@1f
    invoke-static {v4}, Landroid/media/AsyncPlayer;->access$100(Landroid/media/AsyncPlayer;)Ljava/util/LinkedList;

    #@22
    move-result-object v5

    #@23
    monitor-enter v5

    #@24
    .line 116
    :try_start_24
    iget-object v4, p0, Landroid/media/AsyncPlayer$Thread;->this$0:Landroid/media/AsyncPlayer;

    #@26
    invoke-static {v4}, Landroid/media/AsyncPlayer;->access$100(Landroid/media/AsyncPlayer;)Ljava/util/LinkedList;

    #@29
    move-result-object v4

    #@2a
    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    #@2d
    move-result v4

    #@2e
    if-nez v4, :cond_a4

    #@30
    .line 121
    iget-object v4, p0, Landroid/media/AsyncPlayer$Thread;->this$0:Landroid/media/AsyncPlayer;

    #@32
    const/4 v6, 0x0

    #@33
    invoke-static {v4, v6}, Landroid/media/AsyncPlayer;->access$402(Landroid/media/AsyncPlayer;Landroid/media/AsyncPlayer$Thread;)Landroid/media/AsyncPlayer$Thread;

    #@36
    .line 122
    iget-object v4, p0, Landroid/media/AsyncPlayer$Thread;->this$0:Landroid/media/AsyncPlayer;

    #@38
    invoke-static {v4}, Landroid/media/AsyncPlayer;->access$500(Landroid/media/AsyncPlayer;)V

    #@3b
    .line 123
    monitor-exit v5
    :try_end_3c
    .catchall {:try_start_24 .. :try_end_3c} :catchall_a7

    #@3c
    return-void

    #@3d
    .line 92
    :catchall_3d
    move-exception v4

    #@3e
    :try_start_3e
    monitor-exit v5
    :try_end_3f
    .catchall {:try_start_3e .. :try_end_3f} :catchall_3d

    #@3f
    throw v4

    #@40
    .line 97
    :pswitch_40
    iget-object v4, p0, Landroid/media/AsyncPlayer$Thread;->this$0:Landroid/media/AsyncPlayer;

    #@42
    invoke-static {v4, v1}, Landroid/media/AsyncPlayer;->access$200(Landroid/media/AsyncPlayer;Landroid/media/AsyncPlayer$Command;)V

    #@45
    goto :goto_1d

    #@46
    .line 101
    :pswitch_46
    iget-object v4, p0, Landroid/media/AsyncPlayer$Thread;->this$0:Landroid/media/AsyncPlayer;

    #@48
    invoke-static {v4}, Landroid/media/AsyncPlayer;->access$300(Landroid/media/AsyncPlayer;)Landroid/media/MediaPlayer;

    #@4b
    move-result-object v4

    #@4c
    if-eqz v4, :cond_97

    #@4e
    .line 102
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@51
    move-result-wide v4

    #@52
    iget-wide v6, v1, Landroid/media/AsyncPlayer$Command;->requestTime:J

    #@54
    sub-long v2, v4, v6

    #@56
    .line 103
    .local v2, delay:J
    const-wide/16 v4, 0x3e8

    #@58
    cmp-long v4, v2, v4

    #@5a
    if-lez v4, :cond_7f

    #@5c
    .line 104
    iget-object v4, p0, Landroid/media/AsyncPlayer$Thread;->this$0:Landroid/media/AsyncPlayer;

    #@5e
    invoke-static {v4}, Landroid/media/AsyncPlayer;->access$000(Landroid/media/AsyncPlayer;)Ljava/lang/String;

    #@61
    move-result-object v4

    #@62
    new-instance v5, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    const-string v6, "Notification stop delayed by "

    #@69
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v5

    #@6d
    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@70
    move-result-object v5

    #@71
    const-string/jumbo v6, "msecs"

    #@74
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v5

    #@78
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v5

    #@7c
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7f
    .line 106
    :cond_7f
    iget-object v4, p0, Landroid/media/AsyncPlayer$Thread;->this$0:Landroid/media/AsyncPlayer;

    #@81
    invoke-static {v4}, Landroid/media/AsyncPlayer;->access$300(Landroid/media/AsyncPlayer;)Landroid/media/MediaPlayer;

    #@84
    move-result-object v4

    #@85
    invoke-virtual {v4}, Landroid/media/MediaPlayer;->stop()V

    #@88
    .line 107
    iget-object v4, p0, Landroid/media/AsyncPlayer$Thread;->this$0:Landroid/media/AsyncPlayer;

    #@8a
    invoke-static {v4}, Landroid/media/AsyncPlayer;->access$300(Landroid/media/AsyncPlayer;)Landroid/media/MediaPlayer;

    #@8d
    move-result-object v4

    #@8e
    invoke-virtual {v4}, Landroid/media/MediaPlayer;->release()V

    #@91
    .line 108
    iget-object v4, p0, Landroid/media/AsyncPlayer$Thread;->this$0:Landroid/media/AsyncPlayer;

    #@93
    invoke-static {v4, v8}, Landroid/media/AsyncPlayer;->access$302(Landroid/media/AsyncPlayer;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;

    #@96
    goto :goto_1d

    #@97
    .line 110
    .end local v2           #delay:J
    :cond_97
    iget-object v4, p0, Landroid/media/AsyncPlayer$Thread;->this$0:Landroid/media/AsyncPlayer;

    #@99
    invoke-static {v4}, Landroid/media/AsyncPlayer;->access$000(Landroid/media/AsyncPlayer;)Ljava/lang/String;

    #@9c
    move-result-object v4

    #@9d
    const-string v5, "STOP command without a player"

    #@9f
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@a2
    goto/16 :goto_1d

    #@a4
    .line 125
    :cond_a4
    :try_start_a4
    monitor-exit v5

    #@a5
    goto/16 :goto_1

    #@a7
    :catchall_a7
    move-exception v4

    #@a8
    monitor-exit v5
    :try_end_a9
    .catchall {:try_start_a4 .. :try_end_a9} :catchall_a7

    #@a9
    throw v4

    #@aa
    .line 94
    :pswitch_data_aa
    .packed-switch 0x1
        :pswitch_40
        :pswitch_46
    .end packed-switch
.end method
