.class final Landroid/media/AudioService$SoundPoolCallback;
.super Ljava/lang/Object;
.source "AudioService.java"

# interfaces
.implements Landroid/media/SoundPool$OnLoadCompleteListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/AudioService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SoundPoolCallback"
.end annotation


# instance fields
.field mLastSample:I

.field mStatus:I

.field final synthetic this$0:Landroid/media/AudioService;


# direct methods
.method private constructor <init>(Landroid/media/AudioService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 2332
    iput-object p1, p0, Landroid/media/AudioService$SoundPoolCallback;->this$0:Landroid/media/AudioService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/media/AudioService;Landroid/media/AudioService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2332
    invoke-direct {p0, p1}, Landroid/media/AudioService$SoundPoolCallback;-><init>(Landroid/media/AudioService;)V

    #@3
    return-void
.end method


# virtual methods
.method public onLoadComplete(Landroid/media/SoundPool;II)V
    .registers 6
    .parameter "soundPool"
    .parameter "sampleId"
    .parameter "status"

    #@0
    .prologue
    .line 2347
    iget-object v0, p0, Landroid/media/AudioService$SoundPoolCallback;->this$0:Landroid/media/AudioService;

    #@2
    invoke-static {v0}, Landroid/media/AudioService;->access$1700(Landroid/media/AudioService;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    monitor-enter v1

    #@7
    .line 2348
    if-eqz p3, :cond_b

    #@9
    .line 2349
    :try_start_9
    iput p3, p0, Landroid/media/AudioService$SoundPoolCallback;->mStatus:I

    #@b
    .line 2351
    :cond_b
    iget v0, p0, Landroid/media/AudioService$SoundPoolCallback;->mLastSample:I

    #@d
    if-ne p2, v0, :cond_18

    #@f
    .line 2352
    iget-object v0, p0, Landroid/media/AudioService$SoundPoolCallback;->this$0:Landroid/media/AudioService;

    #@11
    invoke-static {v0}, Landroid/media/AudioService;->access$1700(Landroid/media/AudioService;)Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    #@18
    .line 2354
    :cond_18
    monitor-exit v1

    #@19
    .line 2355
    return-void

    #@1a
    .line 2354
    :catchall_1a
    move-exception v0

    #@1b
    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_9 .. :try_end_1c} :catchall_1a

    #@1c
    throw v0
.end method

.method public setLastSample(I)V
    .registers 2
    .parameter "sample"

    #@0
    .prologue
    .line 2343
    iput p1, p0, Landroid/media/AudioService$SoundPoolCallback;->mLastSample:I

    #@2
    .line 2344
    return-void
.end method

.method public status()I
    .registers 2

    #@0
    .prologue
    .line 2339
    iget v0, p0, Landroid/media/AudioService$SoundPoolCallback;->mStatus:I

    #@2
    return v0
.end method
