.class Landroid/media/MediaScanner$MyMediaScannerClient;
.super Ljava/lang/Object;
.source "MediaScanner.java"

# interfaces
.implements Landroid/media/MediaScannerClient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/MediaScanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyMediaScannerClient"
.end annotation


# instance fields
.field private mAlbum:Ljava/lang/String;

.field private mAlbumArtist:Ljava/lang/String;

.field private mArtist:Ljava/lang/String;

.field private mCompilation:I

.field private mComposer:Ljava/lang/String;

.field private mDuration:I

.field private mFileSize:J

.field private mFileType:I

.field private mGenre:Ljava/lang/String;

.field private mHeight:I

.field private mIsDrm:Z

.field private mIsLGEVdieo:I

.field private mLastModified:J

.field private mMimeType:Ljava/lang/String;

.field private mNoMedia:Z

.field private mPath:Ljava/lang/String;

.field private mProtectedType:I

.field private mTitle:Ljava/lang/String;

.field private mTrack:I

.field private mWidth:I

.field private mWriter:Ljava/lang/String;

.field private mYear:I

.field private mlatitude:F

.field private mlongitude:F

.field private mparseLatLonSuccess:Z

.field final synthetic this$0:Landroid/media/MediaScanner;


# direct methods
.method private constructor <init>(Landroid/media/MediaScanner;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 448
    iput-object p1, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/media/MediaScanner;Landroid/media/MediaScanner$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 448
    invoke-direct {p0, p1}, Landroid/media/MediaScanner$MyMediaScannerClient;-><init>(Landroid/media/MediaScanner;)V

    #@3
    return-void
.end method

.method private convertGenreCode(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 7
    .parameter "input"
    .parameter "expected"

    #@0
    .prologue
    .line 829
    invoke-virtual {p0, p1}, Landroid/media/MediaScanner$MyMediaScannerClient;->getGenreName(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 830
    .local v0, output:Ljava/lang/String;
    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_c

    #@a
    .line 831
    const/4 v1, 0x1

    #@b
    .line 834
    :goto_b
    return v1

    #@c
    .line 833
    :cond_c
    const-string v1, "MediaScanner"

    #@e
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "\'"

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    const-string v3, "\' -> \'"

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    const-string v3, "\', expected \'"

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    const-string v3, "\'"

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 834
    const/4 v1, 0x0

    #@3f
    goto :goto_b
.end method

.method private convertRationalLatLonToFloat(Ljava/lang/String;Z)Z
    .registers 10
    .parameter "rationalString"
    .parameter "isLat"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 730
    const/4 v0, 0x0

    #@3
    .line 731
    .local v0, Ref:I
    const/4 v2, 0x0

    #@4
    .line 732
    .local v2, result:F
    const/4 v1, 0x0

    #@5
    .line 734
    .local v1, length:I
    if-nez p1, :cond_8

    #@7
    .line 762
    :cond_7
    :goto_7
    return v3

    #@8
    .line 737
    :cond_8
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@b
    move-result v1

    #@c
    .line 739
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    #@f
    move-result v5

    #@10
    const/16 v6, 0x2b

    #@12
    if-ne v5, v6, :cond_6d

    #@14
    .line 740
    const/4 v0, 0x1

    #@15
    .line 748
    :goto_15
    invoke-virtual {p1, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    #@1c
    move-result v2

    #@1d
    .line 749
    const-string v3, "MediaScanner"

    #@1f
    new-instance v5, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v6, "convertRational LatLonToFloat float = "

    #@26
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v5

    #@2a
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v5

    #@2e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v5

    #@32
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 751
    if-ne p2, v4, :cond_77

    #@37
    .line 752
    const-string v3, "MediaScanner"

    #@39
    new-instance v5, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v6, "convertRational LatLonToFloat isLat = "

    #@40
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v5

    #@44
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v5

    #@48
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v5

    #@4c
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 753
    int-to-float v3, v0

    #@50
    mul-float/2addr v3, v2

    #@51
    iput v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mlatitude:F

    #@53
    .line 760
    :goto_53
    const-string v3, "MediaScanner"

    #@55
    new-instance v5, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v6, "convertRational LatLonToFloat succeed, "

    #@5c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v5

    #@60
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@63
    move-result-object v5

    #@64
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v5

    #@68
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6b
    move v3, v4

    #@6c
    .line 762
    goto :goto_7

    #@6d
    .line 742
    :cond_6d
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    #@70
    move-result v5

    #@71
    const/16 v6, 0x2d

    #@73
    if-ne v5, v6, :cond_7

    #@75
    .line 743
    const/4 v0, -0x1

    #@76
    goto :goto_15

    #@77
    .line 756
    :cond_77
    const-string v3, "MediaScanner"

    #@79
    new-instance v5, Ljava/lang/StringBuilder;

    #@7b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@7e
    const-string v6, "convertRational LatLonToFloat isLon = "

    #@80
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v5

    #@84
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v5

    #@88
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8b
    move-result-object v5

    #@8c
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8f
    .line 757
    int-to-float v3, v0

    #@90
    mul-float/2addr v3, v2

    #@91
    iput v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mlongitude:F

    #@93
    goto :goto_53
.end method

.method private doesPathHaveFilename(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 8
    .parameter "path"
    .parameter "filename"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1321
    sget-char v3, Ljava/io/File;->separatorChar:C

    #@3
    invoke-virtual {p1, v3}, Ljava/lang/String;->lastIndexOf(I)I

    #@6
    move-result v3

    #@7
    add-int/lit8 v1, v3, 0x1

    #@9
    .line 1322
    .local v1, pathFilenameStart:I
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    #@c
    move-result v0

    #@d
    .line 1323
    .local v0, filenameLength:I
    invoke-virtual {p1, v1, p2, v2, v0}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    #@10
    move-result v3

    #@11
    if-eqz v3, :cond_1c

    #@13
    add-int v3, v1, v0

    #@15
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@18
    move-result v4

    #@19
    if-ne v3, v4, :cond_1c

    #@1b
    const/4 v2, 0x1

    #@1c
    :cond_1c
    return v2
.end method

.method private endFile(Landroid/media/MediaScanner$FileEntry;ZZZZZZ)Landroid/net/Uri;
    .registers 45
    .parameter "entry"
    .parameter "ringtones"
    .parameter "notifications"
    .parameter "alarms"
    .parameter "music"
    .parameter "podcasts"
    .parameter "skipOpen"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1021
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->mArtist:Ljava/lang/String;

    #@4
    move-object/from16 v33, v0

    #@6
    if-eqz v33, :cond_14

    #@8
    move-object/from16 v0, p0

    #@a
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->mArtist:Ljava/lang/String;

    #@c
    move-object/from16 v33, v0

    #@e
    invoke-virtual/range {v33 .. v33}, Ljava/lang/String;->length()I

    #@11
    move-result v33

    #@12
    if-nez v33, :cond_20

    #@14
    .line 1022
    :cond_14
    move-object/from16 v0, p0

    #@16
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->mAlbumArtist:Ljava/lang/String;

    #@18
    move-object/from16 v33, v0

    #@1a
    move-object/from16 v0, v33

    #@1c
    move-object/from16 v1, p0

    #@1e
    iput-object v0, v1, Landroid/media/MediaScanner$MyMediaScannerClient;->mArtist:Ljava/lang/String;

    #@20
    .line 1025
    :cond_20
    move-object/from16 v0, p0

    #@22
    iget v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->mFileType:I

    #@24
    move/from16 v33, v0

    #@26
    if-eqz v33, :cond_5a

    #@28
    .line 1026
    move-object/from16 v0, p0

    #@2a
    iget-boolean v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->mIsDrm:Z

    #@2c
    move/from16 v33, v0

    #@2e
    if-nez v33, :cond_3e

    #@30
    move-object/from16 v0, p0

    #@32
    iget v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->mProtectedType:I

    #@34
    move/from16 v33, v0

    #@36
    const/16 v34, 0x1

    #@38
    move/from16 v0, v33

    #@3a
    move/from16 v1, v34

    #@3c
    if-ne v0, v1, :cond_350

    #@3e
    .line 1027
    :cond_3e
    const/16 v33, 0x1

    #@40
    move/from16 v0, v33

    #@42
    move-object/from16 v1, p0

    #@44
    iput v0, v1, Landroid/media/MediaScanner$MyMediaScannerClient;->mProtectedType:I

    #@46
    .line 1037
    :cond_46
    :goto_46
    move-object/from16 v0, p0

    #@48
    iget v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->mFileType:I

    #@4a
    move/from16 v33, v0

    #@4c
    invoke-static/range {v33 .. v33}, Landroid/media/MediaFile;->isTDMBFileType(I)Z

    #@4f
    move-result v33

    #@50
    if-eqz v33, :cond_5a

    #@52
    .line 1038
    const/16 v33, 0x2

    #@54
    move/from16 v0, v33

    #@56
    move-object/from16 v1, p0

    #@58
    iput v0, v1, Landroid/media/MediaScanner$MyMediaScannerClient;->mProtectedType:I

    #@5a
    .line 1043
    :cond_5a
    invoke-direct/range {p0 .. p0}, Landroid/media/MediaScanner$MyMediaScannerClient;->toValues()Landroid/content/ContentValues;

    #@5d
    move-result-object v32

    #@5e
    .line 1044
    .local v32, values:Landroid/content/ContentValues;
    const-string/jumbo v33, "title"

    #@61
    invoke-virtual/range {v32 .. v33}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    #@64
    move-result-object v31

    #@65
    .line 1045
    .local v31, title:Ljava/lang/String;
    if-eqz v31, :cond_71

    #@67
    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@6a
    move-result-object v33

    #@6b
    invoke-static/range {v33 .. v33}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@6e
    move-result v33

    #@6f
    if-eqz v33, :cond_87

    #@71
    .line 1046
    :cond_71
    const-string v33, "_data"

    #@73
    invoke-virtual/range {v32 .. v33}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    #@76
    move-result-object v33

    #@77
    invoke-static/range {v33 .. v33}, Landroid/media/MediaFile;->getFileTitle(Ljava/lang/String;)Ljava/lang/String;

    #@7a
    move-result-object v31

    #@7b
    .line 1047
    const-string/jumbo v33, "title"

    #@7e
    move-object/from16 v0, v32

    #@80
    move-object/from16 v1, v33

    #@82
    move-object/from16 v2, v31

    #@84
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@87
    .line 1049
    :cond_87
    const-string v33, "album"

    #@89
    invoke-virtual/range {v32 .. v33}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    #@8c
    move-result-object v5

    #@8d
    .line 1050
    .local v5, album:Ljava/lang/String;
    const-string v33, "<unknown>"

    #@8f
    move-object/from16 v0, v33

    #@91
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@94
    move-result v33

    #@95
    if-eqz v33, :cond_d0

    #@97
    .line 1051
    const-string v33, "_data"

    #@99
    invoke-virtual/range {v32 .. v33}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    #@9c
    move-result-object v5

    #@9d
    .line 1053
    const/16 v33, 0x2f

    #@9f
    move/from16 v0, v33

    #@a1
    invoke-virtual {v5, v0}, Ljava/lang/String;->lastIndexOf(I)I

    #@a4
    move-result v16

    #@a5
    .line 1054
    .local v16, lastSlash:I
    if-ltz v16, :cond_d0

    #@a7
    .line 1055
    const/16 v22, 0x0

    #@a9
    .line 1057
    .local v22, previousSlash:I
    :goto_a9
    const/16 v33, 0x2f

    #@ab
    add-int/lit8 v34, v22, 0x1

    #@ad
    move/from16 v0, v33

    #@af
    move/from16 v1, v34

    #@b1
    invoke-virtual {v5, v0, v1}, Ljava/lang/String;->indexOf(II)I

    #@b4
    move-result v14

    #@b5
    .line 1058
    .local v14, idx:I
    if-ltz v14, :cond_bb

    #@b7
    move/from16 v0, v16

    #@b9
    if-lt v14, v0, :cond_37e

    #@bb
    .line 1063
    :cond_bb
    if-eqz v22, :cond_d0

    #@bd
    .line 1064
    add-int/lit8 v33, v22, 0x1

    #@bf
    move/from16 v0, v33

    #@c1
    move/from16 v1, v16

    #@c3
    invoke-virtual {v5, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@c6
    move-result-object v5

    #@c7
    .line 1065
    const-string v33, "album"

    #@c9
    move-object/from16 v0, v32

    #@cb
    move-object/from16 v1, v33

    #@cd
    invoke-virtual {v0, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@d0
    .line 1070
    .end local v14           #idx:I
    .end local v16           #lastSlash:I
    .end local v22           #previousSlash:I
    :cond_d0
    const/4 v9, 0x0

    #@d1
    .line 1071
    .local v9, drmImage:Z
    sget-boolean v33, Lcom/lge/config/ConfigBuildFlags;->CAPP_DRM:Z

    #@d3
    if-eqz v33, :cond_13e

    #@d5
    .line 1072
    move-object/from16 v0, p0

    #@d7
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->mMimeType:Ljava/lang/String;

    #@d9
    move-object/from16 v33, v0

    #@db
    invoke-static/range {v33 .. v33}, Landroid/media/MediaFile;->getFileTypeForMimeType(Ljava/lang/String;)I

    #@de
    move-result v12

    #@df
    .line 1073
    .local v12, fileType:I
    move-object/from16 v0, p0

    #@e1
    iget v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->mFileType:I

    #@e3
    move/from16 v33, v0

    #@e5
    invoke-static/range {v33 .. v33}, Landroid/media/MediaFile;->isImageFileType(I)Z

    #@e8
    move-result v33

    #@e9
    if-eqz v33, :cond_382

    #@eb
    move-object/from16 v0, p0

    #@ed
    iget v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->mFileType:I

    #@ef
    move/from16 v33, v0

    #@f1
    const/16 v34, 0x20

    #@f3
    move/from16 v0, v33

    #@f5
    move/from16 v1, v34

    #@f7
    if-ne v0, v1, :cond_382

    #@f9
    .line 1074
    const/16 v33, 0x34

    #@fb
    move/from16 v0, v33

    #@fd
    if-lt v12, v0, :cond_106

    #@ff
    const/16 v33, 0x39

    #@101
    move/from16 v0, v33

    #@103
    if-gt v12, v0, :cond_106

    #@105
    .line 1075
    const/4 v9, 0x1

    #@106
    .line 1092
    :cond_106
    :goto_106
    const/16 v33, 0x34

    #@108
    move/from16 v0, v33

    #@10a
    if-ne v12, v0, :cond_13e

    #@10c
    .line 1093
    const/16 v33, 0xc

    #@10e
    invoke-static/range {v33 .. v33}, Lcom/lge/lgdrm/DrmManager;->isSupportedAgent(I)Z

    #@111
    move-result v33

    #@112
    if-eqz v33, :cond_13e

    #@114
    move-object/from16 v0, p0

    #@116
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->mPath:Ljava/lang/String;

    #@118
    move-object/from16 v33, v0

    #@11a
    invoke-static/range {v33 .. v33}, Lcom/lge/lgdrm/DrmManager;->isDRM(Ljava/lang/String;)I

    #@11d
    move-result v33

    #@11e
    const/16 v34, 0x31

    #@120
    move/from16 v0, v33

    #@122
    move/from16 v1, v34

    #@124
    if-ne v0, v1, :cond_13e

    #@126
    .line 1095
    move-object/from16 v0, p0

    #@128
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->mPath:Ljava/lang/String;

    #@12a
    move-object/from16 v33, v0

    #@12c
    invoke-static/range {v33 .. v33}, Lcom/lge/lgdrm/DrmFwExt$MediaFile;->getFileTypeFromDrm(Ljava/lang/String;)Lcom/lge/lgdrm/DrmFwExt$MediaFile$MediaFileType;

    #@12f
    move-result-object v18

    #@130
    .line 1096
    .local v18, mediaFileType:Lcom/lge/lgdrm/DrmFwExt$MediaFile$MediaFileType;
    if-eqz v18, :cond_13e

    #@132
    .line 1098
    const-string/jumbo v33, "mime_type"

    #@135
    move-object/from16 v0, v18

    #@137
    iget-object v0, v0, Lcom/lge/lgdrm/DrmFwExt$MediaFile$MediaFileType;->mimeType:Ljava/lang/String;

    #@139
    move-object/from16 v34, v0

    #@13b
    invoke-virtual/range {v32 .. v34}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@13e
    .line 1105
    .end local v12           #fileType:I
    .end local v18           #mediaFileType:Lcom/lge/lgdrm/DrmFwExt$MediaFile$MediaFileType;
    :cond_13e
    move-object/from16 v0, p1

    #@140
    iget-wide v0, v0, Landroid/media/MediaScanner$FileEntry;->mRowId:J

    #@142
    move-wide/from16 v25, v0

    #@144
    .line 1106
    .local v25, rowId:J
    move-object/from16 v0, p0

    #@146
    iget v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->mFileType:I

    #@148
    move/from16 v33, v0

    #@14a
    invoke-static/range {v33 .. v33}, Landroid/media/MediaFile;->isAudioFileType(I)Z

    #@14d
    move-result v33

    #@14e
    if-eqz v33, :cond_3b5

    #@150
    const-wide/16 v33, 0x0

    #@152
    cmp-long v33, v25, v33

    #@154
    if-eqz v33, :cond_162

    #@156
    move-object/from16 v0, p0

    #@158
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@15a
    move-object/from16 v33, v0

    #@15c
    invoke-static/range {v33 .. v33}, Landroid/media/MediaScanner;->access$700(Landroid/media/MediaScanner;)I

    #@15f
    move-result v33

    #@160
    if-eqz v33, :cond_3b5

    #@162
    .line 1111
    :cond_162
    const-string/jumbo v33, "is_ringtone"

    #@165
    invoke-static/range {p2 .. p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@168
    move-result-object v34

    #@169
    invoke-virtual/range {v32 .. v34}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@16c
    .line 1112
    const-string/jumbo v33, "is_notification"

    #@16f
    invoke-static/range {p3 .. p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@172
    move-result-object v34

    #@173
    invoke-virtual/range {v32 .. v34}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@176
    .line 1113
    const-string/jumbo v33, "is_alarm"

    #@179
    invoke-static/range {p4 .. p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@17c
    move-result-object v34

    #@17d
    invoke-virtual/range {v32 .. v34}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@180
    .line 1114
    const-string/jumbo v33, "is_music"

    #@183
    invoke-static/range {p5 .. p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@186
    move-result-object v34

    #@187
    invoke-virtual/range {v32 .. v34}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@18a
    .line 1115
    const-string/jumbo v33, "is_podcast"

    #@18d
    invoke-static/range {p6 .. p6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@190
    move-result-object v34

    #@191
    invoke-virtual/range {v32 .. v34}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@194
    .line 1175
    :cond_194
    :goto_194
    move-object/from16 v0, p0

    #@196
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@198
    move-object/from16 v33, v0

    #@19a
    invoke-static/range {v33 .. v33}, Landroid/media/MediaScanner;->access$1500(Landroid/media/MediaScanner;)Landroid/net/Uri;

    #@19d
    move-result-object v28

    #@19e
    .line 1176
    .local v28, tableUri:Landroid/net/Uri;
    move-object/from16 v0, p0

    #@1a0
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@1a2
    move-object/from16 v33, v0

    #@1a4
    invoke-static/range {v33 .. v33}, Landroid/media/MediaScanner;->access$1600(Landroid/media/MediaScanner;)Landroid/media/MediaInserter;

    #@1a7
    move-result-object v15

    #@1a8
    .line 1177
    .local v15, inserter:Landroid/media/MediaInserter;
    move-object/from16 v0, p0

    #@1aa
    iget-boolean v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->mNoMedia:Z

    #@1ac
    move/from16 v33, v0

    #@1ae
    if-nez v33, :cond_1c6

    #@1b0
    .line 1178
    move-object/from16 v0, p0

    #@1b2
    iget v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->mFileType:I

    #@1b4
    move/from16 v33, v0

    #@1b6
    invoke-static/range {v33 .. v33}, Landroid/media/MediaFile;->isVideoFileType(I)Z

    #@1b9
    move-result v33

    #@1ba
    if-eqz v33, :cond_477

    #@1bc
    .line 1179
    move-object/from16 v0, p0

    #@1be
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@1c0
    move-object/from16 v33, v0

    #@1c2
    invoke-static/range {v33 .. v33}, Landroid/media/MediaScanner;->access$1700(Landroid/media/MediaScanner;)Landroid/net/Uri;

    #@1c5
    move-result-object v28

    #@1c6
    .line 1186
    :cond_1c6
    :goto_1c6
    const/16 v23, 0x0

    #@1c8
    .line 1187
    .local v23, result:Landroid/net/Uri;
    const/16 v20, 0x0

    #@1ca
    .line 1188
    .local v20, needToSetSettings:Z
    const-wide/16 v33, 0x0

    #@1cc
    cmp-long v33, v25, v33

    #@1ce
    if-nez v33, :cond_543

    #@1d0
    .line 1189
    move-object/from16 v0, p0

    #@1d2
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@1d4
    move-object/from16 v33, v0

    #@1d6
    invoke-static/range {v33 .. v33}, Landroid/media/MediaScanner;->access$700(Landroid/media/MediaScanner;)I

    #@1d9
    move-result v33

    #@1da
    if-eqz v33, :cond_1f0

    #@1dc
    .line 1190
    const-string/jumbo v33, "media_scanner_new_object_id"

    #@1df
    move-object/from16 v0, p0

    #@1e1
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@1e3
    move-object/from16 v34, v0

    #@1e5
    invoke-static/range {v34 .. v34}, Landroid/media/MediaScanner;->access$700(Landroid/media/MediaScanner;)I

    #@1e8
    move-result v34

    #@1e9
    invoke-static/range {v34 .. v34}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1ec
    move-result-object v34

    #@1ed
    invoke-virtual/range {v32 .. v34}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@1f0
    .line 1192
    :cond_1f0
    move-object/from16 v0, p0

    #@1f2
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@1f4
    move-object/from16 v33, v0

    #@1f6
    invoke-static/range {v33 .. v33}, Landroid/media/MediaScanner;->access$1500(Landroid/media/MediaScanner;)Landroid/net/Uri;

    #@1f9
    move-result-object v33

    #@1fa
    move-object/from16 v0, v28

    #@1fc
    move-object/from16 v1, v33

    #@1fe
    if-ne v0, v1, :cond_21f

    #@200
    .line 1193
    move-object/from16 v0, p1

    #@202
    iget v13, v0, Landroid/media/MediaScanner$FileEntry;->mFormat:I

    #@204
    .line 1194
    .local v13, format:I
    if-nez v13, :cond_216

    #@206
    .line 1195
    move-object/from16 v0, p1

    #@208
    iget-object v0, v0, Landroid/media/MediaScanner$FileEntry;->mPath:Ljava/lang/String;

    #@20a
    move-object/from16 v33, v0

    #@20c
    move-object/from16 v0, p0

    #@20e
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->mMimeType:Ljava/lang/String;

    #@210
    move-object/from16 v34, v0

    #@212
    invoke-static/range {v33 .. v34}, Landroid/media/MediaFile;->getFormatCode(Ljava/lang/String;Ljava/lang/String;)I

    #@215
    move-result v13

    #@216
    .line 1197
    :cond_216
    const-string v33, "format"

    #@218
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@21b
    move-result-object v34

    #@21c
    invoke-virtual/range {v32 .. v34}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@21f
    .line 1202
    .end local v13           #format:I
    :cond_21f
    move-object/from16 v0, p0

    #@221
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@223
    move-object/from16 v33, v0

    #@225
    invoke-static/range {v33 .. v33}, Landroid/media/MediaScanner;->access$2000(Landroid/media/MediaScanner;)Z

    #@228
    move-result v33

    #@229
    if-eqz v33, :cond_267

    #@22b
    .line 1203
    if-eqz p3, :cond_4a7

    #@22d
    move-object/from16 v0, p0

    #@22f
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@231
    move-object/from16 v33, v0

    #@233
    invoke-static/range {v33 .. v33}, Landroid/media/MediaScanner;->access$2100(Landroid/media/MediaScanner;)Z

    #@236
    move-result v33

    #@237
    if-nez v33, :cond_4a7

    #@239
    .line 1204
    move-object/from16 v0, p0

    #@23b
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@23d
    move-object/from16 v33, v0

    #@23f
    invoke-static/range {v33 .. v33}, Landroid/media/MediaScanner;->access$2200(Landroid/media/MediaScanner;)Ljava/lang/String;

    #@242
    move-result-object v33

    #@243
    invoke-static/range {v33 .. v33}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@246
    move-result v33

    #@247
    if-nez v33, :cond_265

    #@249
    move-object/from16 v0, p1

    #@24b
    iget-object v0, v0, Landroid/media/MediaScanner$FileEntry;->mPath:Ljava/lang/String;

    #@24d
    move-object/from16 v33, v0

    #@24f
    move-object/from16 v0, p0

    #@251
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@253
    move-object/from16 v34, v0

    #@255
    invoke-static/range {v34 .. v34}, Landroid/media/MediaScanner;->access$2200(Landroid/media/MediaScanner;)Ljava/lang/String;

    #@258
    move-result-object v34

    #@259
    move-object/from16 v0, p0

    #@25b
    move-object/from16 v1, v33

    #@25d
    move-object/from16 v2, v34

    #@25f
    invoke-direct {v0, v1, v2}, Landroid/media/MediaScanner$MyMediaScannerClient;->doesPathHaveFilename(Ljava/lang/String;Ljava/lang/String;)Z

    #@262
    move-result v33

    #@263
    if-eqz v33, :cond_267

    #@265
    .line 1206
    :cond_265
    const/16 v20, 0x1

    #@267
    .line 1226
    :cond_267
    :goto_267
    if-eqz v15, :cond_26b

    #@269
    if-eqz v20, :cond_523

    #@26b
    .line 1227
    :cond_26b
    move-object/from16 v0, p0

    #@26d
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@26f
    move-object/from16 v33, v0

    #@271
    invoke-static/range {v33 .. v33}, Landroid/media/MediaScanner;->access$2700(Landroid/media/MediaScanner;)Landroid/content/IContentProvider;

    #@274
    move-result-object v33

    #@275
    move-object/from16 v0, v33

    #@277
    move-object/from16 v1, v28

    #@279
    move-object/from16 v2, v32

    #@27b
    invoke-interface {v0, v1, v2}, Landroid/content/IContentProvider;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@27e
    move-result-object v23

    #@27f
    .line 1234
    :goto_27f
    if-eqz v23, :cond_28b

    #@281
    .line 1235
    invoke-static/range {v23 .. v23}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    #@284
    move-result-wide v25

    #@285
    .line 1236
    move-wide/from16 v0, v25

    #@287
    move-object/from16 v2, p1

    #@289
    iput-wide v0, v2, Landroid/media/MediaScanner$FileEntry;->mRowId:J

    #@28b
    .line 1276
    :cond_28b
    :goto_28b
    if-eqz v20, :cond_2bd

    #@28d
    .line 1277
    if-eqz p3, :cond_5e4

    #@28f
    .line 1278
    const-string/jumbo v33, "notification_sound"

    #@292
    move-object/from16 v0, p0

    #@294
    move-object/from16 v1, v33

    #@296
    move-object/from16 v2, v28

    #@298
    move-wide/from16 v3, v25

    #@29a
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/media/MediaScanner$MyMediaScannerClient;->setSettingIfNotSet(Ljava/lang/String;Landroid/net/Uri;J)V

    #@29d
    .line 1280
    const-string/jumbo v33, "notification_sound_sim2"

    #@2a0
    move-object/from16 v0, p0

    #@2a2
    move-object/from16 v1, v33

    #@2a4
    move-object/from16 v2, v28

    #@2a6
    move-wide/from16 v3, v25

    #@2a8
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/media/MediaScanner$MyMediaScannerClient;->setSettingIfNotSet(Ljava/lang/String;Landroid/net/Uri;J)V

    #@2ab
    .line 1281
    const-string v33, "MediaScanner"

    #@2ad
    const-string v34, "Set a default 2nd sim noti sound"

    #@2af
    invoke-static/range {v33 .. v34}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2b2
    .line 1283
    move-object/from16 v0, p0

    #@2b4
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@2b6
    move-object/from16 v33, v0

    #@2b8
    const/16 v34, 0x1

    #@2ba
    invoke-static/range {v33 .. v34}, Landroid/media/MediaScanner;->access$2102(Landroid/media/MediaScanner;Z)Z

    #@2bd
    .line 1301
    :cond_2bd
    :goto_2bd
    move-object/from16 v0, p0

    #@2bf
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@2c1
    move-object/from16 v33, v0

    #@2c3
    invoke-static/range {v33 .. v33}, Landroid/media/MediaScanner;->access$1400(Landroid/media/MediaScanner;)Landroid/content/Context;

    #@2c6
    move-result-object v33

    #@2c7
    invoke-virtual/range {v33 .. v33}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@2ca
    move-result-object v33

    #@2cb
    const v34, 0x206001c

    #@2ce
    invoke-virtual/range {v33 .. v34}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@2d1
    move-result v7

    #@2d2
    .line 1302
    .local v7, chameleonSupported:Z
    const-string/jumbo v33, "ro.config.ringtone"

    #@2d5
    invoke-static/range {v33 .. v33}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@2d8
    move-result-object v24

    #@2d9
    .line 1303
    .local v24, roRingtone:Ljava/lang/String;
    const-string v6, "default_ringer.mp3"

    #@2db
    .line 1305
    .local v6, carrierRingtone:Ljava/lang/String;
    const-wide/16 v33, 0x0

    #@2dd
    cmp-long v33, v25, v33

    #@2df
    if-eqz v33, :cond_34f

    #@2e1
    .line 1306
    move-object/from16 v0, p0

    #@2e3
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@2e5
    move-object/from16 v33, v0

    #@2e7
    invoke-static/range {v33 .. v33}, Landroid/media/MediaScanner;->access$1400(Landroid/media/MediaScanner;)Landroid/content/Context;

    #@2ea
    move-result-object v33

    #@2eb
    invoke-virtual/range {v33 .. v33}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2ee
    move-result-object v33

    #@2ef
    const-string/jumbo v34, "ringtone"

    #@2f2
    invoke-static/range {v33 .. v34}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@2f5
    move-result-object v33

    #@2f6
    if-eqz v33, :cond_34f

    #@2f8
    .line 1307
    if-eqz v7, :cond_34f

    #@2fa
    move-object/from16 v0, v24

    #@2fc
    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@2ff
    move-result v33

    #@300
    if-eqz v33, :cond_34f

    #@302
    move-object/from16 v0, p0

    #@304
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@306
    move-object/from16 v33, v0

    #@308
    invoke-static/range {v33 .. v33}, Landroid/media/MediaScanner;->access$1400(Landroid/media/MediaScanner;)Landroid/content/Context;

    #@30b
    move-result-object v33

    #@30c
    invoke-virtual/range {v33 .. v33}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@30f
    move-result-object v33

    #@310
    const-string/jumbo v34, "ringtone"

    #@313
    invoke-static/range {v33 .. v34}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@316
    move-result-object v33

    #@317
    const-string v34, "content://media/internal/audio/media/99999"

    #@319
    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@31c
    move-result v33

    #@31d
    if-eqz v33, :cond_34f

    #@31f
    .line 1309
    move-object/from16 v0, p1

    #@321
    iget-object v0, v0, Landroid/media/MediaScanner$FileEntry;->mPath:Ljava/lang/String;

    #@323
    move-object/from16 v33, v0

    #@325
    move-object/from16 v0, p0

    #@327
    move-object/from16 v1, v33

    #@329
    invoke-direct {v0, v1, v6}, Landroid/media/MediaScanner$MyMediaScannerClient;->doesPathHaveFilename(Ljava/lang/String;Ljava/lang/String;)Z

    #@32c
    move-result v33

    #@32d
    if-eqz v33, :cond_34f

    #@32f
    .line 1310
    move-object/from16 v0, p0

    #@331
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@333
    move-object/from16 v33, v0

    #@335
    invoke-static/range {v33 .. v33}, Landroid/media/MediaScanner;->access$1400(Landroid/media/MediaScanner;)Landroid/content/Context;

    #@338
    move-result-object v33

    #@339
    invoke-virtual/range {v33 .. v33}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@33c
    move-result-object v33

    #@33d
    const-string/jumbo v34, "ringtone"

    #@340
    move-object/from16 v0, v28

    #@342
    move-wide/from16 v1, v25

    #@344
    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@347
    move-result-object v35

    #@348
    invoke-virtual/range {v35 .. v35}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@34b
    move-result-object v35

    #@34c
    invoke-static/range {v33 .. v35}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@34f
    .line 1317
    :cond_34f
    return-object v23

    #@350
    .line 1029
    .end local v5           #album:Ljava/lang/String;
    .end local v6           #carrierRingtone:Ljava/lang/String;
    .end local v7           #chameleonSupported:Z
    .end local v9           #drmImage:Z
    .end local v15           #inserter:Landroid/media/MediaInserter;
    .end local v20           #needToSetSettings:Z
    .end local v23           #result:Landroid/net/Uri;
    .end local v24           #roRingtone:Ljava/lang/String;
    .end local v25           #rowId:J
    .end local v28           #tableUri:Landroid/net/Uri;
    .end local v31           #title:Ljava/lang/String;
    .end local v32           #values:Landroid/content/ContentValues;
    :cond_350
    move-object/from16 v0, p0

    #@352
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->mPath:Ljava/lang/String;

    #@354
    move-object/from16 v33, v0

    #@356
    invoke-static/range {v33 .. v33}, Landroid/media/MediaFile;->getFileType(Ljava/lang/String;)Landroid/media/MediaFile$MediaFileType;

    #@359
    move-result-object v18

    #@35a
    .line 1030
    .local v18, mediaFileType:Landroid/media/MediaFile$MediaFileType;
    if-eqz v18, :cond_46

    #@35c
    .line 1031
    move-object/from16 v0, p0

    #@35e
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@360
    move-object/from16 v33, v0

    #@362
    invoke-static/range {v33 .. v33}, Landroid/media/MediaScanner;->access$200(Landroid/media/MediaScanner;)Z

    #@365
    move-result v33

    #@366
    if-eqz v33, :cond_46

    #@368
    move-object/from16 v0, v18

    #@36a
    iget v0, v0, Landroid/media/MediaFile$MediaFileType;->fileType:I

    #@36c
    move/from16 v33, v0

    #@36e
    invoke-static/range {v33 .. v33}, Landroid/media/MediaFile;->isDrmFileType(I)Z

    #@371
    move-result v33

    #@372
    if-eqz v33, :cond_46

    #@374
    .line 1033
    const/16 v33, 0x1

    #@376
    move/from16 v0, v33

    #@378
    move-object/from16 v1, p0

    #@37a
    iput v0, v1, Landroid/media/MediaScanner$MyMediaScannerClient;->mProtectedType:I

    #@37c
    goto/16 :goto_46

    #@37e
    .line 1061
    .end local v18           #mediaFileType:Landroid/media/MediaFile$MediaFileType;
    .restart local v5       #album:Ljava/lang/String;
    .restart local v14       #idx:I
    .restart local v16       #lastSlash:I
    .restart local v22       #previousSlash:I
    .restart local v31       #title:Ljava/lang/String;
    .restart local v32       #values:Landroid/content/ContentValues;
    :cond_37e
    move/from16 v22, v14

    #@380
    .line 1062
    goto/16 :goto_a9

    #@382
    .line 1077
    .end local v14           #idx:I
    .end local v16           #lastSlash:I
    .end local v22           #previousSlash:I
    .restart local v9       #drmImage:Z
    .restart local v12       #fileType:I
    :cond_382
    move-object/from16 v0, p0

    #@384
    iget v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->mFileType:I

    #@386
    move/from16 v33, v0

    #@388
    invoke-static/range {v33 .. v33}, Landroid/media/MediaFile;->isAudioFileType(I)Z

    #@38b
    move-result v33

    #@38c
    if-eqz v33, :cond_106

    #@38e
    .line 1078
    const/16 v33, 0x34

    #@390
    move/from16 v0, v33

    #@392
    if-lt v12, v0, :cond_106

    #@394
    const/16 v33, 0x39

    #@396
    move/from16 v0, v33

    #@398
    if-gt v12, v0, :cond_106

    #@39a
    .line 1085
    move-object/from16 v0, p0

    #@39c
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@39e
    move-object/from16 v33, v0

    #@3a0
    invoke-static/range {v33 .. v33}, Landroid/media/MediaScanner;->access$1400(Landroid/media/MediaScanner;)Landroid/content/Context;

    #@3a3
    move-result-object v33

    #@3a4
    invoke-virtual/range {v33 .. v33}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3a7
    move-result-object v33

    #@3a8
    const v34, 0x206001c

    #@3ab
    invoke-virtual/range {v33 .. v34}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@3ae
    move-result v27

    #@3af
    .line 1086
    .local v27, sprintSupported:Z
    if-eqz v27, :cond_106

    #@3b1
    .line 1087
    const/16 p2, 0x1

    #@3b3
    goto/16 :goto_106

    #@3b5
    .line 1116
    .end local v12           #fileType:I
    .end local v27           #sprintSupported:Z
    .restart local v25       #rowId:J
    :cond_3b5
    move-object/from16 v0, p0

    #@3b7
    iget v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->mFileType:I

    #@3b9
    move/from16 v33, v0

    #@3bb
    const/16 v34, 0x20

    #@3bd
    move/from16 v0, v33

    #@3bf
    move/from16 v1, v34

    #@3c1
    if-ne v0, v1, :cond_194

    #@3c3
    move-object/from16 v0, p0

    #@3c5
    iget-boolean v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->mNoMedia:Z

    #@3c7
    move/from16 v33, v0

    #@3c9
    if-nez v33, :cond_194

    #@3cb
    if-nez v9, :cond_194

    #@3cd
    .line 1121
    const/4 v10, 0x0

    #@3ce
    .line 1124
    .local v10, exif:Landroid/media/ExifInterface;
    if-nez p7, :cond_3de

    #@3d0
    .line 1125
    :try_start_3d0
    new-instance v11, Landroid/media/ExifInterface;

    #@3d2
    move-object/from16 v0, p1

    #@3d4
    iget-object v0, v0, Landroid/media/MediaScanner$FileEntry;->mPath:Ljava/lang/String;

    #@3d6
    move-object/from16 v33, v0

    #@3d8
    move-object/from16 v0, v33

    #@3da
    invoke-direct {v11, v0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_3dd
    .catch Ljava/io/IOException; {:try_start_3d0 .. :try_end_3dd} :catch_647

    #@3dd
    .end local v10           #exif:Landroid/media/ExifInterface;
    .local v11, exif:Landroid/media/ExifInterface;
    move-object v10, v11

    #@3de
    .line 1130
    .end local v11           #exif:Landroid/media/ExifInterface;
    .restart local v10       #exif:Landroid/media/ExifInterface;
    :cond_3de
    :goto_3de
    if-eqz v10, :cond_194

    #@3e0
    .line 1131
    const/16 v33, 0x2

    #@3e2
    move/from16 v0, v33

    #@3e4
    new-array v0, v0, [F

    #@3e6
    move-object/from16 v17, v0

    #@3e8
    .line 1132
    .local v17, latlng:[F
    move-object/from16 v0, v17

    #@3ea
    invoke-virtual {v10, v0}, Landroid/media/ExifInterface;->getLatLong([F)Z

    #@3ed
    move-result v33

    #@3ee
    if-eqz v33, :cond_40c

    #@3f0
    .line 1133
    const-string/jumbo v33, "latitude"

    #@3f3
    const/16 v34, 0x0

    #@3f5
    aget v34, v17, v34

    #@3f7
    invoke-static/range {v34 .. v34}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@3fa
    move-result-object v34

    #@3fb
    invoke-virtual/range {v32 .. v34}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    #@3fe
    .line 1134
    const-string/jumbo v33, "longitude"

    #@401
    const/16 v34, 0x1

    #@403
    aget v34, v17, v34

    #@405
    invoke-static/range {v34 .. v34}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@408
    move-result-object v34

    #@409
    invoke-virtual/range {v32 .. v34}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    #@40c
    .line 1137
    :cond_40c
    invoke-virtual {v10}, Landroid/media/ExifInterface;->getGpsDateTime()J

    #@40f
    move-result-wide v29

    #@410
    .line 1138
    .local v29, time:J
    const-wide/16 v33, -0x1

    #@412
    cmp-long v33, v29, v33

    #@414
    if-eqz v33, :cond_443

    #@416
    .line 1139
    const-string v33, "datetaken"

    #@418
    invoke-static/range {v29 .. v30}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@41b
    move-result-object v34

    #@41c
    invoke-virtual/range {v32 .. v34}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@41f
    .line 1151
    :cond_41f
    :goto_41f
    const-string v33, "Orientation"

    #@421
    const/16 v34, -0x1

    #@423
    move-object/from16 v0, v33

    #@425
    move/from16 v1, v34

    #@427
    invoke-virtual {v10, v0, v1}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    #@42a
    move-result v21

    #@42b
    .line 1153
    .local v21, orientation:I
    const/16 v33, -0x1

    #@42d
    move/from16 v0, v21

    #@42f
    move/from16 v1, v33

    #@431
    if-eq v0, v1, :cond_194

    #@433
    .line 1156
    packed-switch v21, :pswitch_data_64a

    #@436
    .line 1167
    :pswitch_436
    const/4 v8, 0x0

    #@437
    .line 1170
    .local v8, degree:I
    :goto_437
    const-string/jumbo v33, "orientation"

    #@43a
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@43d
    move-result-object v34

    #@43e
    invoke-virtual/range {v32 .. v34}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@441
    goto/16 :goto_194

    #@443
    .line 1145
    .end local v8           #degree:I
    .end local v21           #orientation:I
    :cond_443
    invoke-virtual {v10}, Landroid/media/ExifInterface;->getDateTime()J

    #@446
    move-result-wide v29

    #@447
    .line 1146
    const-wide/16 v33, -0x1

    #@449
    cmp-long v33, v29, v33

    #@44b
    if-eqz v33, :cond_41f

    #@44d
    move-object/from16 v0, p0

    #@44f
    iget-wide v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->mLastModified:J

    #@451
    move-wide/from16 v33, v0

    #@453
    const-wide/16 v35, 0x3e8

    #@455
    mul-long v33, v33, v35

    #@457
    sub-long v33, v33, v29

    #@459
    invoke-static/range {v33 .. v34}, Ljava/lang/Math;->abs(J)J

    #@45c
    move-result-wide v33

    #@45d
    const-wide/32 v35, 0x5265c00

    #@460
    cmp-long v33, v33, v35

    #@462
    if-ltz v33, :cond_41f

    #@464
    .line 1147
    const-string v33, "datetaken"

    #@466
    invoke-static/range {v29 .. v30}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@469
    move-result-object v34

    #@46a
    invoke-virtual/range {v32 .. v34}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@46d
    goto :goto_41f

    #@46e
    .line 1158
    .restart local v21       #orientation:I
    :pswitch_46e
    const/16 v8, 0x5a

    #@470
    .line 1159
    .restart local v8       #degree:I
    goto :goto_437

    #@471
    .line 1161
    .end local v8           #degree:I
    :pswitch_471
    const/16 v8, 0xb4

    #@473
    .line 1162
    .restart local v8       #degree:I
    goto :goto_437

    #@474
    .line 1164
    .end local v8           #degree:I
    :pswitch_474
    const/16 v8, 0x10e

    #@476
    .line 1165
    .restart local v8       #degree:I
    goto :goto_437

    #@477
    .line 1180
    .end local v8           #degree:I
    .end local v10           #exif:Landroid/media/ExifInterface;
    .end local v17           #latlng:[F
    .end local v21           #orientation:I
    .end local v29           #time:J
    .restart local v15       #inserter:Landroid/media/MediaInserter;
    .restart local v28       #tableUri:Landroid/net/Uri;
    :cond_477
    move-object/from16 v0, p0

    #@479
    iget v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->mFileType:I

    #@47b
    move/from16 v33, v0

    #@47d
    invoke-static/range {v33 .. v33}, Landroid/media/MediaFile;->isImageFileType(I)Z

    #@480
    move-result v33

    #@481
    if-eqz v33, :cond_48f

    #@483
    .line 1181
    move-object/from16 v0, p0

    #@485
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@487
    move-object/from16 v33, v0

    #@489
    invoke-static/range {v33 .. v33}, Landroid/media/MediaScanner;->access$1800(Landroid/media/MediaScanner;)Landroid/net/Uri;

    #@48c
    move-result-object v28

    #@48d
    goto/16 :goto_1c6

    #@48f
    .line 1182
    :cond_48f
    move-object/from16 v0, p0

    #@491
    iget v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->mFileType:I

    #@493
    move/from16 v33, v0

    #@495
    invoke-static/range {v33 .. v33}, Landroid/media/MediaFile;->isAudioFileType(I)Z

    #@498
    move-result v33

    #@499
    if-eqz v33, :cond_1c6

    #@49b
    .line 1183
    move-object/from16 v0, p0

    #@49d
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@49f
    move-object/from16 v33, v0

    #@4a1
    invoke-static/range {v33 .. v33}, Landroid/media/MediaScanner;->access$1900(Landroid/media/MediaScanner;)Landroid/net/Uri;

    #@4a4
    move-result-object v28

    #@4a5
    goto/16 :goto_1c6

    #@4a7
    .line 1208
    .restart local v20       #needToSetSettings:Z
    .restart local v23       #result:Landroid/net/Uri;
    :cond_4a7
    if-eqz p2, :cond_4e5

    #@4a9
    move-object/from16 v0, p0

    #@4ab
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@4ad
    move-object/from16 v33, v0

    #@4af
    invoke-static/range {v33 .. v33}, Landroid/media/MediaScanner;->access$2300(Landroid/media/MediaScanner;)Z

    #@4b2
    move-result v33

    #@4b3
    if-nez v33, :cond_4e5

    #@4b5
    .line 1209
    move-object/from16 v0, p0

    #@4b7
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@4b9
    move-object/from16 v33, v0

    #@4bb
    invoke-static/range {v33 .. v33}, Landroid/media/MediaScanner;->access$2400(Landroid/media/MediaScanner;)Ljava/lang/String;

    #@4be
    move-result-object v33

    #@4bf
    invoke-static/range {v33 .. v33}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@4c2
    move-result v33

    #@4c3
    if-nez v33, :cond_4e1

    #@4c5
    move-object/from16 v0, p1

    #@4c7
    iget-object v0, v0, Landroid/media/MediaScanner$FileEntry;->mPath:Ljava/lang/String;

    #@4c9
    move-object/from16 v33, v0

    #@4cb
    move-object/from16 v0, p0

    #@4cd
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@4cf
    move-object/from16 v34, v0

    #@4d1
    invoke-static/range {v34 .. v34}, Landroid/media/MediaScanner;->access$2400(Landroid/media/MediaScanner;)Ljava/lang/String;

    #@4d4
    move-result-object v34

    #@4d5
    move-object/from16 v0, p0

    #@4d7
    move-object/from16 v1, v33

    #@4d9
    move-object/from16 v2, v34

    #@4db
    invoke-direct {v0, v1, v2}, Landroid/media/MediaScanner$MyMediaScannerClient;->doesPathHaveFilename(Ljava/lang/String;Ljava/lang/String;)Z

    #@4de
    move-result v33

    #@4df
    if-eqz v33, :cond_267

    #@4e1
    .line 1211
    :cond_4e1
    const/16 v20, 0x1

    #@4e3
    goto/16 :goto_267

    #@4e5
    .line 1213
    :cond_4e5
    if-eqz p4, :cond_267

    #@4e7
    move-object/from16 v0, p0

    #@4e9
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@4eb
    move-object/from16 v33, v0

    #@4ed
    invoke-static/range {v33 .. v33}, Landroid/media/MediaScanner;->access$2500(Landroid/media/MediaScanner;)Z

    #@4f0
    move-result v33

    #@4f1
    if-nez v33, :cond_267

    #@4f3
    .line 1214
    move-object/from16 v0, p0

    #@4f5
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@4f7
    move-object/from16 v33, v0

    #@4f9
    invoke-static/range {v33 .. v33}, Landroid/media/MediaScanner;->access$2600(Landroid/media/MediaScanner;)Ljava/lang/String;

    #@4fc
    move-result-object v33

    #@4fd
    invoke-static/range {v33 .. v33}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@500
    move-result v33

    #@501
    if-nez v33, :cond_51f

    #@503
    move-object/from16 v0, p1

    #@505
    iget-object v0, v0, Landroid/media/MediaScanner$FileEntry;->mPath:Ljava/lang/String;

    #@507
    move-object/from16 v33, v0

    #@509
    move-object/from16 v0, p0

    #@50b
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@50d
    move-object/from16 v34, v0

    #@50f
    invoke-static/range {v34 .. v34}, Landroid/media/MediaScanner;->access$2600(Landroid/media/MediaScanner;)Ljava/lang/String;

    #@512
    move-result-object v34

    #@513
    move-object/from16 v0, p0

    #@515
    move-object/from16 v1, v33

    #@517
    move-object/from16 v2, v34

    #@519
    invoke-direct {v0, v1, v2}, Landroid/media/MediaScanner$MyMediaScannerClient;->doesPathHaveFilename(Ljava/lang/String;Ljava/lang/String;)Z

    #@51c
    move-result v33

    #@51d
    if-eqz v33, :cond_267

    #@51f
    .line 1216
    :cond_51f
    const/16 v20, 0x1

    #@521
    goto/16 :goto_267

    #@523
    .line 1228
    :cond_523
    move-object/from16 v0, p1

    #@525
    iget v0, v0, Landroid/media/MediaScanner$FileEntry;->mFormat:I

    #@527
    move/from16 v33, v0

    #@529
    const/16 v34, 0x3001

    #@52b
    move/from16 v0, v33

    #@52d
    move/from16 v1, v34

    #@52f
    if-ne v0, v1, :cond_53a

    #@531
    .line 1229
    move-object/from16 v0, v28

    #@533
    move-object/from16 v1, v32

    #@535
    invoke-virtual {v15, v0, v1}, Landroid/media/MediaInserter;->insertwithPriority(Landroid/net/Uri;Landroid/content/ContentValues;)V

    #@538
    goto/16 :goto_27f

    #@53a
    .line 1231
    :cond_53a
    move-object/from16 v0, v28

    #@53c
    move-object/from16 v1, v32

    #@53e
    invoke-virtual {v15, v0, v1}, Landroid/media/MediaInserter;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)V

    #@541
    goto/16 :goto_27f

    #@543
    .line 1240
    :cond_543
    move-object/from16 v0, v28

    #@545
    move-wide/from16 v1, v25

    #@547
    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@54a
    move-result-object v23

    #@54b
    .line 1243
    const-string v33, "_data"

    #@54d
    invoke-virtual/range {v32 .. v33}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    #@550
    .line 1245
    const/16 v19, 0x0

    #@552
    .line 1246
    .local v19, mediaType:I
    move-object/from16 v0, p1

    #@554
    iget-object v0, v0, Landroid/media/MediaScanner$FileEntry;->mPath:Ljava/lang/String;

    #@556
    move-object/from16 v33, v0

    #@558
    invoke-static/range {v33 .. v33}, Landroid/media/MediaScanner;->isNoMediaPath(Ljava/lang/String;)Z

    #@55b
    move-result v33

    #@55c
    if-nez v33, :cond_5ac

    #@55e
    .line 1247
    move-object/from16 v0, p0

    #@560
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->mMimeType:Ljava/lang/String;

    #@562
    move-object/from16 v33, v0

    #@564
    invoke-static/range {v33 .. v33}, Landroid/media/MediaFile;->getFileTypeForMimeType(Ljava/lang/String;)I

    #@567
    move-result v12

    #@568
    .line 1249
    .restart local v12       #fileType:I
    if-nez v12, :cond_57a

    #@56a
    .line 1250
    move-object/from16 v0, p1

    #@56c
    iget-object v0, v0, Landroid/media/MediaScanner$FileEntry;->mPath:Ljava/lang/String;

    #@56e
    move-object/from16 v33, v0

    #@570
    invoke-static/range {v33 .. v33}, Landroid/media/MediaFile;->getFileType(Ljava/lang/String;)Landroid/media/MediaFile$MediaFileType;

    #@573
    move-result-object v18

    #@574
    .line 1251
    .restart local v18       #mediaFileType:Landroid/media/MediaFile$MediaFileType;
    if-eqz v18, :cond_57a

    #@576
    .line 1252
    move-object/from16 v0, v18

    #@578
    iget v12, v0, Landroid/media/MediaFile$MediaFileType;->fileType:I

    #@57a
    .line 1258
    .end local v18           #mediaFileType:Landroid/media/MediaFile$MediaFileType;
    :cond_57a
    move-object/from16 v0, p0

    #@57c
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@57e
    move-object/from16 v33, v0

    #@580
    invoke-static/range {v33 .. v33}, Landroid/media/MediaScanner;->access$200(Landroid/media/MediaScanner;)Z

    #@583
    move-result v33

    #@584
    if-eqz v33, :cond_59a

    #@586
    invoke-static {v12}, Landroid/media/MediaFile;->isDrmFileType(I)Z

    #@589
    move-result v33

    #@58a
    if-eqz v33, :cond_59a

    #@58c
    .line 1259
    move-object/from16 v0, p1

    #@58e
    iget-object v0, v0, Landroid/media/MediaScanner$FileEntry;->mPath:Ljava/lang/String;

    #@590
    move-object/from16 v33, v0

    #@592
    move-object/from16 v0, p0

    #@594
    move-object/from16 v1, v33

    #@596
    invoke-direct {v0, v1}, Landroid/media/MediaScanner$MyMediaScannerClient;->getFileTypeFromDrm(Ljava/lang/String;)I

    #@599
    move-result v12

    #@59a
    .line 1262
    :cond_59a
    invoke-static {v12}, Landroid/media/MediaFile;->isAudioFileType(I)Z

    #@59d
    move-result v33

    #@59e
    if-eqz v33, :cond_5c9

    #@5a0
    .line 1263
    const/16 v19, 0x2

    #@5a2
    .line 1271
    :cond_5a2
    :goto_5a2
    const-string/jumbo v33, "media_type"

    #@5a5
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5a8
    move-result-object v34

    #@5a9
    invoke-virtual/range {v32 .. v34}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@5ac
    .line 1273
    .end local v12           #fileType:I
    :cond_5ac
    move-object/from16 v0, p0

    #@5ae
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@5b0
    move-object/from16 v33, v0

    #@5b2
    invoke-static/range {v33 .. v33}, Landroid/media/MediaScanner;->access$2700(Landroid/media/MediaScanner;)Landroid/content/IContentProvider;

    #@5b5
    move-result-object v33

    #@5b6
    const/16 v34, 0x0

    #@5b8
    const/16 v35, 0x0

    #@5ba
    move-object/from16 v0, v33

    #@5bc
    move-object/from16 v1, v23

    #@5be
    move-object/from16 v2, v32

    #@5c0
    move-object/from16 v3, v34

    #@5c2
    move-object/from16 v4, v35

    #@5c4
    invoke-interface {v0, v1, v2, v3, v4}, Landroid/content/IContentProvider;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@5c7
    goto/16 :goto_28b

    #@5c9
    .line 1264
    .restart local v12       #fileType:I
    :cond_5c9
    invoke-static {v12}, Landroid/media/MediaFile;->isVideoFileType(I)Z

    #@5cc
    move-result v33

    #@5cd
    if-eqz v33, :cond_5d2

    #@5cf
    .line 1265
    const/16 v19, 0x3

    #@5d1
    goto :goto_5a2

    #@5d2
    .line 1266
    :cond_5d2
    invoke-static {v12}, Landroid/media/MediaFile;->isImageFileType(I)Z

    #@5d5
    move-result v33

    #@5d6
    if-eqz v33, :cond_5db

    #@5d8
    .line 1267
    const/16 v19, 0x1

    #@5da
    goto :goto_5a2

    #@5db
    .line 1268
    :cond_5db
    invoke-static {v12}, Landroid/media/MediaFile;->isPlayListFileType(I)Z

    #@5de
    move-result v33

    #@5df
    if-eqz v33, :cond_5a2

    #@5e1
    .line 1269
    const/16 v19, 0x4

    #@5e3
    goto :goto_5a2

    #@5e4
    .line 1284
    .end local v12           #fileType:I
    .end local v19           #mediaType:I
    :cond_5e4
    if-eqz p2, :cond_62b

    #@5e6
    .line 1285
    const-string/jumbo v33, "ringtone"

    #@5e9
    move-object/from16 v0, p0

    #@5eb
    move-object/from16 v1, v33

    #@5ed
    move-object/from16 v2, v28

    #@5ef
    move-wide/from16 v3, v25

    #@5f1
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/media/MediaScanner$MyMediaScannerClient;->setSettingIfNotSet(Ljava/lang/String;Landroid/net/Uri;J)V

    #@5f4
    .line 1287
    const-string/jumbo v33, "ringtone_sim2"

    #@5f7
    move-object/from16 v0, p0

    #@5f9
    move-object/from16 v1, v33

    #@5fb
    move-object/from16 v2, v28

    #@5fd
    move-wide/from16 v3, v25

    #@5ff
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/media/MediaScanner$MyMediaScannerClient;->setSettingIfNotSet(Ljava/lang/String;Landroid/net/Uri;J)V

    #@602
    .line 1288
    const-string v33, "MediaScanner"

    #@604
    const-string v34, "Set a default 2nd sim ringtone"

    #@606
    invoke-static/range {v33 .. v34}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@609
    .line 1289
    const-string/jumbo v33, "ringtone_videocall"

    #@60c
    move-object/from16 v0, p0

    #@60e
    move-object/from16 v1, v33

    #@610
    move-object/from16 v2, v28

    #@612
    move-wide/from16 v3, v25

    #@614
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/media/MediaScanner$MyMediaScannerClient;->setSettingIfNotSet(Ljava/lang/String;Landroid/net/Uri;J)V

    #@617
    .line 1290
    const-string v33, "MediaScanner"

    #@619
    const-string v34, "Set a default video call ringtone for KT"

    #@61b
    invoke-static/range {v33 .. v34}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@61e
    .line 1292
    move-object/from16 v0, p0

    #@620
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@622
    move-object/from16 v33, v0

    #@624
    const/16 v34, 0x1

    #@626
    invoke-static/range {v33 .. v34}, Landroid/media/MediaScanner;->access$2302(Landroid/media/MediaScanner;Z)Z

    #@629
    goto/16 :goto_2bd

    #@62b
    .line 1293
    :cond_62b
    if-eqz p4, :cond_2bd

    #@62d
    .line 1294
    const-string v33, "alarm_alert"

    #@62f
    move-object/from16 v0, p0

    #@631
    move-object/from16 v1, v33

    #@633
    move-object/from16 v2, v28

    #@635
    move-wide/from16 v3, v25

    #@637
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/media/MediaScanner$MyMediaScannerClient;->setSettingIfNotSet(Ljava/lang/String;Landroid/net/Uri;J)V

    #@63a
    .line 1295
    move-object/from16 v0, p0

    #@63c
    iget-object v0, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@63e
    move-object/from16 v33, v0

    #@640
    const/16 v34, 0x1

    #@642
    invoke-static/range {v33 .. v34}, Landroid/media/MediaScanner;->access$2502(Landroid/media/MediaScanner;Z)Z

    #@645
    goto/16 :goto_2bd

    #@647
    .line 1127
    .end local v15           #inserter:Landroid/media/MediaInserter;
    .end local v20           #needToSetSettings:Z
    .end local v23           #result:Landroid/net/Uri;
    .end local v28           #tableUri:Landroid/net/Uri;
    .restart local v10       #exif:Landroid/media/ExifInterface;
    :catch_647
    move-exception v33

    #@648
    goto/16 :goto_3de

    #@64a
    .line 1156
    :pswitch_data_64a
    .packed-switch 0x3
        :pswitch_471
        :pswitch_436
        :pswitch_436
        :pswitch_46e
        :pswitch_436
        :pswitch_474
    .end packed-switch
.end method

.method private getFileTypeFromDrm(Ljava/lang/String;)I
    .registers 8
    .parameter "path"

    #@0
    .prologue
    .line 1340
    iget-object v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@2
    invoke-static {v3}, Landroid/media/MediaScanner;->access$200(Landroid/media/MediaScanner;)Z

    #@5
    move-result v3

    #@6
    if-nez v3, :cond_a

    #@8
    .line 1341
    const/4 v2, 0x0

    #@9
    .line 1369
    :cond_9
    :goto_9
    return v2

    #@a
    .line 1344
    :cond_a
    const/4 v2, 0x0

    #@b
    .line 1347
    .local v2, resultFileType:I
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_DRM:Z

    #@d
    if-eqz v3, :cond_28

    #@f
    .line 1348
    invoke-static {p1}, Lcom/lge/lgdrm/DrmFwExt$MediaFile;->getFileTypeFromDrm(Ljava/lang/String;)Lcom/lge/lgdrm/DrmFwExt$MediaFile$MediaFileType;

    #@12
    move-result-object v1

    #@13
    .line 1349
    .local v1, mediaFileType:Lcom/lge/lgdrm/DrmFwExt$MediaFile$MediaFileType;
    if-eqz v1, :cond_18

    #@15
    .line 1350
    iget v2, v1, Lcom/lge/lgdrm/DrmFwExt$MediaFile$MediaFileType;->fileType:I

    #@17
    goto :goto_9

    #@18
    .line 1352
    :cond_18
    const-string v3, "drm.service.enabled"

    #@1a
    const-string v4, "false"

    #@1c
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    const-string v4, "false"

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v3

    #@26
    if-nez v3, :cond_9

    #@28
    .line 1358
    .end local v1           #mediaFileType:Lcom/lge/lgdrm/DrmFwExt$MediaFile$MediaFileType;
    :cond_28
    iget-object v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@2a
    invoke-static {v3}, Landroid/media/MediaScanner;->access$2800(Landroid/media/MediaScanner;)Landroid/drm/DrmManagerClient;

    #@2d
    move-result-object v3

    #@2e
    if-nez v3, :cond_40

    #@30
    .line 1359
    iget-object v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@32
    new-instance v4, Landroid/drm/DrmManagerClient;

    #@34
    iget-object v5, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@36
    invoke-static {v5}, Landroid/media/MediaScanner;->access$1400(Landroid/media/MediaScanner;)Landroid/content/Context;

    #@39
    move-result-object v5

    #@3a
    invoke-direct {v4, v5}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    #@3d
    invoke-static {v3, v4}, Landroid/media/MediaScanner;->access$2802(Landroid/media/MediaScanner;Landroid/drm/DrmManagerClient;)Landroid/drm/DrmManagerClient;

    #@40
    .line 1362
    :cond_40
    iget-object v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@42
    invoke-static {v3}, Landroid/media/MediaScanner;->access$2800(Landroid/media/MediaScanner;)Landroid/drm/DrmManagerClient;

    #@45
    move-result-object v3

    #@46
    const/4 v4, 0x0

    #@47
    invoke-virtual {v3, p1, v4}, Landroid/drm/DrmManagerClient;->canHandle(Ljava/lang/String;Ljava/lang/String;)Z

    #@4a
    move-result v3

    #@4b
    if-eqz v3, :cond_9

    #@4d
    .line 1363
    iget-object v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@4f
    invoke-static {v3}, Landroid/media/MediaScanner;->access$2800(Landroid/media/MediaScanner;)Landroid/drm/DrmManagerClient;

    #@52
    move-result-object v3

    #@53
    invoke-virtual {v3, p1}, Landroid/drm/DrmManagerClient;->getOriginalMimeType(Ljava/lang/String;)Ljava/lang/String;

    #@56
    move-result-object v0

    #@57
    .line 1364
    .local v0, drmMimetype:Ljava/lang/String;
    if-eqz v0, :cond_9

    #@59
    .line 1365
    iput-object v0, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mMimeType:Ljava/lang/String;

    #@5b
    .line 1366
    invoke-static {v0}, Landroid/media/MediaFile;->getFileTypeForMimeType(Ljava/lang/String;)I

    #@5e
    move-result v2

    #@5f
    goto :goto_9
.end method

.method private parseSubstring(Ljava/lang/String;II)I
    .registers 12
    .parameter "s"
    .parameter "start"
    .parameter "defaultValue"

    #@0
    .prologue
    const/16 v7, 0x39

    #@2
    const/16 v6, 0x30

    #@4
    .line 677
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@7
    move-result v1

    #@8
    .line 678
    .local v1, length:I
    if-ne p2, v1, :cond_b

    #@a
    .line 691
    .end local p3
    :goto_a
    return p3

    #@b
    .line 680
    .restart local p3
    :cond_b
    add-int/lit8 v3, p2, 0x1

    #@d
    .end local p2
    .local v3, start:I
    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    #@10
    move-result v0

    #@11
    .line 682
    .local v0, ch:C
    if-lt v0, v6, :cond_15

    #@13
    if-le v0, v7, :cond_17

    #@15
    :cond_15
    move p2, v3

    #@16
    .end local v3           #start:I
    .restart local p2
    goto :goto_a

    #@17
    .line 684
    .end local p2
    .restart local v3       #start:I
    :cond_17
    add-int/lit8 v2, v0, -0x30

    #@19
    .line 685
    .local v2, result:I
    :goto_19
    if-ge v3, v1, :cond_2f

    #@1b
    .line 686
    add-int/lit8 p2, v3, 0x1

    #@1d
    .end local v3           #start:I
    .restart local p2
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    #@20
    move-result v0

    #@21
    .line 687
    if-lt v0, v6, :cond_25

    #@23
    if-le v0, v7, :cond_27

    #@25
    :cond_25
    move p3, v2

    #@26
    goto :goto_a

    #@27
    .line 688
    :cond_27
    mul-int/lit8 v4, v2, 0xa

    #@29
    add-int/lit8 v5, v0, -0x30

    #@2b
    add-int v2, v4, v5

    #@2d
    move v3, p2

    #@2e
    .end local p2
    .restart local v3       #start:I
    goto :goto_19

    #@2f
    :cond_2f
    move p2, v3

    #@30
    .end local v3           #start:I
    .restart local p2
    move p3, v2

    #@31
    .line 691
    goto :goto_a
.end method

.method private processImageFile(Ljava/lang/String;)V
    .registers 4
    .parameter "path"

    #@0
    .prologue
    .line 910
    :try_start_0
    iget-object v0, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@2
    invoke-static {v0}, Landroid/media/MediaScanner;->access$1300(Landroid/media/MediaScanner;)Landroid/graphics/BitmapFactory$Options;

    #@5
    move-result-object v0

    #@6
    const/4 v1, 0x0

    #@7
    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    #@9
    .line 911
    iget-object v0, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@b
    invoke-static {v0}, Landroid/media/MediaScanner;->access$1300(Landroid/media/MediaScanner;)Landroid/graphics/BitmapFactory$Options;

    #@e
    move-result-object v0

    #@f
    const/4 v1, 0x0

    #@10
    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    #@12
    .line 912
    iget-object v0, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@14
    invoke-static {v0}, Landroid/media/MediaScanner;->access$1300(Landroid/media/MediaScanner;)Landroid/graphics/BitmapFactory$Options;

    #@17
    move-result-object v0

    #@18
    invoke-static {p1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@1b
    .line 913
    iget-object v0, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@1d
    invoke-static {v0}, Landroid/media/MediaScanner;->access$1300(Landroid/media/MediaScanner;)Landroid/graphics/BitmapFactory$Options;

    #@20
    move-result-object v0

    #@21
    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    #@23
    iput v0, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mWidth:I

    #@25
    .line 914
    iget-object v0, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@27
    invoke-static {v0}, Landroid/media/MediaScanner;->access$1300(Landroid/media/MediaScanner;)Landroid/graphics/BitmapFactory$Options;

    #@2a
    move-result-object v0

    #@2b
    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    #@2d
    iput v0, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mHeight:I
    :try_end_2f
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_2f} :catch_30

    #@2f
    .line 918
    :goto_2f
    return-void

    #@30
    .line 915
    :catch_30
    move-exception v0

    #@31
    goto :goto_2f
.end method

.method private setSettingIfNotSet(Ljava/lang/String;Landroid/net/Uri;J)V
    .registers 8
    .parameter "settingName"
    .parameter "uri"
    .parameter "rowId"

    #@0
    .prologue
    .line 1329
    iget-object v1, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@2
    invoke-static {v1}, Landroid/media/MediaScanner;->access$1400(Landroid/media/MediaScanner;)Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9
    move-result-object v1

    #@a
    invoke-static {v1, p1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    .line 1332
    .local v0, existingSettingValue:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_29

    #@14
    .line 1334
    iget-object v1, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@16
    invoke-static {v1}, Landroid/media/MediaScanner;->access$1400(Landroid/media/MediaScanner;)Landroid/content/Context;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1d
    move-result-object v1

    #@1e
    invoke-static {p2, p3, p4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    invoke-static {v1, p1, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@29
    .line 1337
    :cond_29
    return-void
.end method

.method private splitLatLon(Ljava/lang/String;Z)Ljava/lang/String;
    .registers 9
    .parameter "rationalString"
    .parameter "isLat"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 700
    const/4 v0, 0x0

    #@2
    .line 702
    .local v0, length:I
    if-nez p1, :cond_6

    #@4
    move-object v1, v2

    #@5
    .line 725
    :goto_5
    return-object v1

    #@6
    .line 705
    :cond_6
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@9
    move-result v0

    #@a
    .line 706
    const/4 v1, 0x0

    #@b
    .line 707
    .local v1, result:Ljava/lang/String;
    const-string v3, "MediaScanner"

    #@d
    new-instance v4, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v5, "convertRational LatLonToString rationalString = "

    #@14
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v4

    #@20
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 709
    const/16 v3, 0x12

    #@25
    if-lt v0, v3, :cond_29

    #@27
    move-object v1, v2

    #@28
    .line 710
    goto :goto_5

    #@29
    .line 712
    :cond_29
    div-int/lit8 v3, v0, 0x2

    #@2b
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    #@2e
    move-result v3

    #@2f
    const/16 v4, 0x2b

    #@31
    if-eq v3, v4, :cond_3d

    #@33
    div-int/lit8 v3, v0, 0x2

    #@35
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    #@38
    move-result v3

    #@39
    const/16 v4, 0x2d

    #@3b
    if-ne v3, v4, :cond_48

    #@3d
    .line 718
    :cond_3d
    const/4 v2, 0x1

    #@3e
    if-ne p2, v2, :cond_4a

    #@40
    .line 719
    const/4 v2, 0x0

    #@41
    div-int/lit8 v3, v0, 0x2

    #@43
    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@46
    move-result-object v1

    #@47
    goto :goto_5

    #@48
    :cond_48
    move-object v1, v2

    #@49
    .line 715
    goto :goto_5

    #@4a
    .line 722
    :cond_4a
    div-int/lit8 v2, v0, 0x2

    #@4c
    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@4f
    move-result-object v1

    #@50
    goto :goto_5
.end method

.method private testGenreNameConverter()V
    .registers 3

    #@0
    .prologue
    .line 838
    const-string v0, "2"

    #@2
    const-string v1, "Country"

    #@4
    invoke-direct {p0, v0, v1}, Landroid/media/MediaScanner$MyMediaScannerClient;->convertGenreCode(Ljava/lang/String;Ljava/lang/String;)Z

    #@7
    .line 839
    const-string v0, "(2)"

    #@9
    const-string v1, "Country"

    #@b
    invoke-direct {p0, v0, v1}, Landroid/media/MediaScanner$MyMediaScannerClient;->convertGenreCode(Ljava/lang/String;Ljava/lang/String;)Z

    #@e
    .line 840
    const-string v0, "(2"

    #@10
    const-string v1, "(2"

    #@12
    invoke-direct {p0, v0, v1}, Landroid/media/MediaScanner$MyMediaScannerClient;->convertGenreCode(Ljava/lang/String;Ljava/lang/String;)Z

    #@15
    .line 841
    const-string v0, "2 Foo"

    #@17
    const-string v1, "Country"

    #@19
    invoke-direct {p0, v0, v1}, Landroid/media/MediaScanner$MyMediaScannerClient;->convertGenreCode(Ljava/lang/String;Ljava/lang/String;)Z

    #@1c
    .line 842
    const-string v0, "(2) Foo"

    #@1e
    const-string v1, "Country"

    #@20
    invoke-direct {p0, v0, v1}, Landroid/media/MediaScanner$MyMediaScannerClient;->convertGenreCode(Ljava/lang/String;Ljava/lang/String;)Z

    #@23
    .line 843
    const-string v0, "(2 Foo"

    #@25
    const-string v1, "(2 Foo"

    #@27
    invoke-direct {p0, v0, v1}, Landroid/media/MediaScanner$MyMediaScannerClient;->convertGenreCode(Ljava/lang/String;Ljava/lang/String;)Z

    #@2a
    .line 844
    const-string v0, "2Foo"

    #@2c
    const-string v1, "2Foo"

    #@2e
    invoke-direct {p0, v0, v1}, Landroid/media/MediaScanner$MyMediaScannerClient;->convertGenreCode(Ljava/lang/String;Ljava/lang/String;)Z

    #@31
    .line 845
    const-string v0, "(2)Foo"

    #@33
    const-string v1, "Country"

    #@35
    invoke-direct {p0, v0, v1}, Landroid/media/MediaScanner$MyMediaScannerClient;->convertGenreCode(Ljava/lang/String;Ljava/lang/String;)Z

    #@38
    .line 846
    const-string v0, "200 Foo"

    #@3a
    const-string v1, "Foo"

    #@3c
    invoke-direct {p0, v0, v1}, Landroid/media/MediaScanner$MyMediaScannerClient;->convertGenreCode(Ljava/lang/String;Ljava/lang/String;)Z

    #@3f
    .line 847
    const-string v0, "(200) Foo"

    #@41
    const-string v1, "Foo"

    #@43
    invoke-direct {p0, v0, v1}, Landroid/media/MediaScanner$MyMediaScannerClient;->convertGenreCode(Ljava/lang/String;Ljava/lang/String;)Z

    #@46
    .line 848
    const-string v0, "200Foo"

    #@48
    const-string v1, "200Foo"

    #@4a
    invoke-direct {p0, v0, v1}, Landroid/media/MediaScanner$MyMediaScannerClient;->convertGenreCode(Ljava/lang/String;Ljava/lang/String;)Z

    #@4d
    .line 849
    const-string v0, "(200)Foo"

    #@4f
    const-string v1, "Foo"

    #@51
    invoke-direct {p0, v0, v1}, Landroid/media/MediaScanner$MyMediaScannerClient;->convertGenreCode(Ljava/lang/String;Ljava/lang/String;)Z

    #@54
    .line 850
    const-string v0, "200)Foo"

    #@56
    const-string v1, "200)Foo"

    #@58
    invoke-direct {p0, v0, v1}, Landroid/media/MediaScanner$MyMediaScannerClient;->convertGenreCode(Ljava/lang/String;Ljava/lang/String;)Z

    #@5b
    .line 851
    const-string v0, "200) Foo"

    #@5d
    const-string v1, "200) Foo"

    #@5f
    invoke-direct {p0, v0, v1}, Landroid/media/MediaScanner$MyMediaScannerClient;->convertGenreCode(Ljava/lang/String;Ljava/lang/String;)Z

    #@62
    .line 852
    return-void
.end method

.method private toValues()Landroid/content/ContentValues;
    .registers 6

    #@0
    .prologue
    .line 953
    new-instance v0, Landroid/content/ContentValues;

    #@2
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    #@5
    .line 955
    .local v0, map:Landroid/content/ContentValues;
    const-string v2, "_data"

    #@7
    iget-object v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mPath:Ljava/lang/String;

    #@9
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@c
    .line 956
    const-string/jumbo v2, "title"

    #@f
    iget-object v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mTitle:Ljava/lang/String;

    #@11
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@14
    .line 957
    const-string v2, "date_modified"

    #@16
    iget-wide v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mLastModified:J

    #@18
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@1f
    .line 958
    const-string v2, "_size"

    #@21
    iget-wide v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mFileSize:J

    #@23
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@2a
    .line 959
    const-string/jumbo v2, "mime_type"

    #@2d
    iget-object v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mMimeType:Ljava/lang/String;

    #@2f
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@32
    .line 960
    const-string/jumbo v2, "is_drm"

    #@35
    iget-boolean v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mIsDrm:Z

    #@37
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@3e
    .line 962
    const/4 v1, 0x0

    #@3f
    .line 963
    .local v1, resolution:Ljava/lang/String;
    iget v2, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mWidth:I

    #@41
    if-lez v2, :cond_7a

    #@43
    iget v2, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mHeight:I

    #@45
    if-lez v2, :cond_7a

    #@47
    .line 964
    const-string/jumbo v2, "width"

    #@4a
    iget v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mWidth:I

    #@4c
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4f
    move-result-object v3

    #@50
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@53
    .line 965
    const-string v2, "height"

    #@55
    iget v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mHeight:I

    #@57
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5a
    move-result-object v3

    #@5b
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@5e
    .line 966
    new-instance v2, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    iget v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mWidth:I

    #@65
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@68
    move-result-object v2

    #@69
    const-string/jumbo v3, "x"

    #@6c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v2

    #@70
    iget v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mHeight:I

    #@72
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@75
    move-result-object v2

    #@76
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v1

    #@7a
    .line 970
    :cond_7a
    const-string/jumbo v2, "protected_type"

    #@7d
    iget v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mProtectedType:I

    #@7f
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@82
    move-result-object v3

    #@83
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@86
    .line 972
    iget-boolean v2, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mNoMedia:Z

    #@88
    if-nez v2, :cond_e7

    #@8a
    .line 973
    iget v2, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mFileType:I

    #@8c
    invoke-static {v2}, Landroid/media/MediaFile;->isVideoFileType(I)Z

    #@8f
    move-result v2

    #@90
    if-eqz v2, :cond_ee

    #@92
    .line 975
    iget-boolean v2, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mparseLatLonSuccess:Z

    #@94
    if-eqz v2, :cond_ae

    #@96
    .line 976
    const-string/jumbo v2, "latitude"

    #@99
    iget v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mlatitude:F

    #@9b
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@9e
    move-result-object v3

    #@9f
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    #@a2
    .line 977
    const-string/jumbo v2, "longitude"

    #@a5
    iget v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mlongitude:F

    #@a7
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@aa
    move-result-object v3

    #@ab
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    #@ae
    .line 980
    :cond_ae
    const-string v3, "artist"

    #@b0
    iget-object v2, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mArtist:Ljava/lang/String;

    #@b2
    if-eqz v2, :cond_e8

    #@b4
    iget-object v2, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mArtist:Ljava/lang/String;

    #@b6
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@b9
    move-result v2

    #@ba
    if-lez v2, :cond_e8

    #@bc
    iget-object v2, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mArtist:Ljava/lang/String;

    #@be
    :goto_be
    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@c1
    .line 982
    const-string v3, "album"

    #@c3
    iget-object v2, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mAlbum:Ljava/lang/String;

    #@c5
    if-eqz v2, :cond_eb

    #@c7
    iget-object v2, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mAlbum:Ljava/lang/String;

    #@c9
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@cc
    move-result v2

    #@cd
    if-lez v2, :cond_eb

    #@cf
    iget-object v2, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mAlbum:Ljava/lang/String;

    #@d1
    :goto_d1
    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@d4
    .line 984
    const-string v2, "duration"

    #@d6
    iget v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mDuration:I

    #@d8
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@db
    move-result-object v3

    #@dc
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@df
    .line 985
    if-eqz v1, :cond_e7

    #@e1
    .line 986
    const-string/jumbo v2, "resolution"

    #@e4
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@e7
    .line 1010
    :cond_e7
    :goto_e7
    return-object v0

    #@e8
    .line 980
    :cond_e8
    const-string v2, "<unknown>"

    #@ea
    goto :goto_be

    #@eb
    .line 982
    :cond_eb
    const-string v2, "<unknown>"

    #@ed
    goto :goto_d1

    #@ee
    .line 991
    :cond_ee
    iget v2, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mFileType:I

    #@f0
    invoke-static {v2}, Landroid/media/MediaFile;->isImageFileType(I)Z

    #@f3
    move-result v2

    #@f4
    if-nez v2, :cond_e7

    #@f6
    .line 993
    iget v2, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mFileType:I

    #@f8
    invoke-static {v2}, Landroid/media/MediaFile;->isAudioFileType(I)Z

    #@fb
    move-result v2

    #@fc
    if-eqz v2, :cond_e7

    #@fe
    .line 994
    const-string v3, "artist"

    #@100
    iget-object v2, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mArtist:Ljava/lang/String;

    #@102
    if-eqz v2, :cond_179

    #@104
    iget-object v2, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mArtist:Ljava/lang/String;

    #@106
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@109
    move-result v2

    #@10a
    if-lez v2, :cond_179

    #@10c
    iget-object v2, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mArtist:Ljava/lang/String;

    #@10e
    :goto_10e
    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@111
    .line 996
    const-string v3, "album_artist"

    #@113
    iget-object v2, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mAlbumArtist:Ljava/lang/String;

    #@115
    if-eqz v2, :cond_17c

    #@117
    iget-object v2, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mAlbumArtist:Ljava/lang/String;

    #@119
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@11c
    move-result v2

    #@11d
    if-lez v2, :cond_17c

    #@11f
    iget-object v2, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mAlbumArtist:Ljava/lang/String;

    #@121
    :goto_121
    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@124
    .line 998
    const-string v3, "album"

    #@126
    iget-object v2, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mAlbum:Ljava/lang/String;

    #@128
    if-eqz v2, :cond_17e

    #@12a
    iget-object v2, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mAlbum:Ljava/lang/String;

    #@12c
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@12f
    move-result v2

    #@130
    if-lez v2, :cond_17e

    #@132
    iget-object v2, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mAlbum:Ljava/lang/String;

    #@134
    :goto_134
    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@137
    .line 1000
    const-string v2, "composer"

    #@139
    iget-object v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mComposer:Ljava/lang/String;

    #@13b
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@13e
    .line 1001
    const-string v2, "genre"

    #@140
    iget-object v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mGenre:Ljava/lang/String;

    #@142
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@145
    .line 1002
    iget v2, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mYear:I

    #@147
    if-eqz v2, :cond_155

    #@149
    .line 1003
    const-string/jumbo v2, "year"

    #@14c
    iget v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mYear:I

    #@14e
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@151
    move-result-object v3

    #@152
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@155
    .line 1005
    :cond_155
    const-string/jumbo v2, "track"

    #@158
    iget v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mTrack:I

    #@15a
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@15d
    move-result-object v3

    #@15e
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@161
    .line 1006
    const-string v2, "duration"

    #@163
    iget v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mDuration:I

    #@165
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@168
    move-result-object v3

    #@169
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@16c
    .line 1007
    const-string v2, "compilation"

    #@16e
    iget v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mCompilation:I

    #@170
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@173
    move-result-object v3

    #@174
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@177
    goto/16 :goto_e7

    #@179
    .line 994
    :cond_179
    const-string v2, "<unknown>"

    #@17b
    goto :goto_10e

    #@17c
    .line 996
    :cond_17c
    const/4 v2, 0x0

    #@17d
    goto :goto_121

    #@17e
    .line 998
    :cond_17e
    const-string v2, "<unknown>"

    #@180
    goto :goto_134
.end method


# virtual methods
.method public beginFile(Ljava/lang/String;Ljava/lang/String;JJZZ)Landroid/media/MediaScanner$FileEntry;
    .registers 24
    .parameter "path"
    .parameter "mimeType"
    .parameter "lastModified"
    .parameter "fileSize"
    .parameter "isDirectory"
    .parameter "noMedia"

    #@0
    .prologue
    .line 484
    move-object/from16 v0, p2

    #@2
    iput-object v0, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mMimeType:Ljava/lang/String;

    #@4
    .line 485
    const/4 v3, 0x0

    #@5
    iput v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mFileType:I

    #@7
    .line 486
    move-wide/from16 v0, p5

    #@9
    iput-wide v0, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mFileSize:J

    #@b
    .line 488
    if-nez p7, :cond_4f

    #@d
    .line 489
    if-nez p8, :cond_17

    #@f
    invoke-static/range {p1 .. p1}, Landroid/media/MediaScanner;->access$100(Ljava/lang/String;)Z

    #@12
    move-result v3

    #@13
    if-eqz v3, :cond_17

    #@15
    .line 490
    const/16 p8, 0x1

    #@17
    .line 492
    :cond_17
    move/from16 v0, p8

    #@19
    iput-boolean v0, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mNoMedia:Z

    #@1b
    .line 495
    if-eqz p2, :cond_23

    #@1d
    .line 496
    invoke-static/range {p2 .. p2}, Landroid/media/MediaFile;->getFileTypeForMimeType(Ljava/lang/String;)I

    #@20
    move-result v3

    #@21
    iput v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mFileType:I

    #@23
    .line 500
    :cond_23
    iget v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mFileType:I

    #@25
    if-nez v3, :cond_39

    #@27
    .line 501
    invoke-static/range {p1 .. p1}, Landroid/media/MediaFile;->getFileType(Ljava/lang/String;)Landroid/media/MediaFile$MediaFileType;

    #@2a
    move-result-object v13

    #@2b
    .line 502
    .local v13, mediaFileType:Landroid/media/MediaFile$MediaFileType;
    if-eqz v13, :cond_39

    #@2d
    .line 503
    iget v3, v13, Landroid/media/MediaFile$MediaFileType;->fileType:I

    #@2f
    iput v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mFileType:I

    #@31
    .line 504
    iget-object v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mMimeType:Ljava/lang/String;

    #@33
    if-nez v3, :cond_39

    #@35
    .line 505
    iget-object v3, v13, Landroid/media/MediaFile$MediaFileType;->mimeType:Ljava/lang/String;

    #@37
    iput-object v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mMimeType:Ljava/lang/String;

    #@39
    .line 510
    .end local v13           #mediaFileType:Landroid/media/MediaFile$MediaFileType;
    :cond_39
    iget-object v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@3b
    invoke-static {v3}, Landroid/media/MediaScanner;->access$200(Landroid/media/MediaScanner;)Z

    #@3e
    move-result v3

    #@3f
    if-eqz v3, :cond_4f

    #@41
    iget v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mFileType:I

    #@43
    invoke-static {v3}, Landroid/media/MediaFile;->isDrmFileType(I)Z

    #@46
    move-result v3

    #@47
    if-eqz v3, :cond_4f

    #@49
    .line 511
    invoke-direct/range {p0 .. p1}, Landroid/media/MediaScanner$MyMediaScannerClient;->getFileTypeFromDrm(Ljava/lang/String;)I

    #@4c
    move-result v3

    #@4d
    iput v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mFileType:I

    #@4f
    .line 516
    :cond_4f
    move-object/from16 v11, p1

    #@51
    .line 517
    .local v11, key:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@54
    move-result-object v12

    #@55
    .line 519
    .local v12, lowpath:Ljava/lang/String;
    iget-object v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@57
    invoke-static {v3}, Landroid/media/MediaScanner;->access$300(Landroid/media/MediaScanner;)Z

    #@5a
    move-result v3

    #@5b
    if-eqz v3, :cond_61

    #@5d
    .line 520
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@60
    move-result-object v11

    #@61
    .line 522
    :cond_61
    const-string v3, "/dcim/"

    #@63
    invoke-virtual {v12, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@66
    move-result v3

    #@67
    if-gtz v3, :cond_71

    #@69
    const-string v3, "/my_sounds/"

    #@6b
    invoke-virtual {v12, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@6e
    move-result v3

    #@6f
    if-lez v3, :cond_b4

    #@71
    .line 523
    :cond_71
    iget-object v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@73
    move-object/from16 v0, p1

    #@75
    invoke-virtual {v3, v0}, Landroid/media/MediaScanner;->makeEntryFor(Ljava/lang/String;)Landroid/media/MediaScanner$FileEntry;

    #@78
    move-result-object v2

    #@79
    .line 529
    .local v2, entry:Landroid/media/MediaScanner$FileEntry;
    :goto_79
    if-eqz v2, :cond_c1

    #@7b
    iget-wide v3, v2, Landroid/media/MediaScanner$FileEntry;->mLastModified:J

    #@7d
    sub-long v9, p3, v3

    #@7f
    .line 530
    .local v9, delta:J
    :goto_7f
    const-wide/16 v3, 0x1

    #@81
    cmp-long v3, v9, v3

    #@83
    if-gtz v3, :cond_8b

    #@85
    const-wide/16 v3, -0x1

    #@87
    cmp-long v3, v9, v3

    #@89
    if-gez v3, :cond_c4

    #@8b
    :cond_8b
    const/4 v14, 0x1

    #@8c
    .line 531
    .local v14, wasModified:Z
    :goto_8c
    if-eqz v2, :cond_90

    #@8e
    if-eqz v14, :cond_99

    #@90
    .line 532
    :cond_90
    if-eqz v14, :cond_c6

    #@92
    .line 533
    move-wide/from16 v0, p3

    #@94
    iput-wide v0, v2, Landroid/media/MediaScanner$FileEntry;->mLastModified:J

    #@96
    .line 538
    :goto_96
    const/4 v3, 0x1

    #@97
    iput-boolean v3, v2, Landroid/media/MediaScanner$FileEntry;->mLastModifiedChanged:Z

    #@99
    .line 541
    :cond_99
    iget-object v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@9b
    invoke-static {v3}, Landroid/media/MediaScanner;->access$500(Landroid/media/MediaScanner;)Z

    #@9e
    move-result v3

    #@9f
    if-eqz v3, :cond_d8

    #@a1
    iget v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mFileType:I

    #@a3
    invoke-static {v3}, Landroid/media/MediaFile;->isPlayListFileType(I)Z

    #@a6
    move-result v3

    #@a7
    if-eqz v3, :cond_d8

    #@a9
    .line 542
    iget-object v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@ab
    invoke-static {v3}, Landroid/media/MediaScanner;->access$600(Landroid/media/MediaScanner;)Ljava/util/ArrayList;

    #@ae
    move-result-object v3

    #@af
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b2
    .line 544
    const/4 v2, 0x0

    #@b3
    .line 575
    .end local v2           #entry:Landroid/media/MediaScanner$FileEntry;
    :goto_b3
    return-object v2

    #@b4
    .line 525
    .end local v9           #delta:J
    .end local v14           #wasModified:Z
    :cond_b4
    iget-object v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@b6
    invoke-static {v3}, Landroid/media/MediaScanner;->access$400(Landroid/media/MediaScanner;)Ljava/util/HashMap;

    #@b9
    move-result-object v3

    #@ba
    invoke-virtual {v3, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@bd
    move-result-object v2

    #@be
    check-cast v2, Landroid/media/MediaScanner$FileEntry;

    #@c0
    .restart local v2       #entry:Landroid/media/MediaScanner$FileEntry;
    goto :goto_79

    #@c1
    .line 529
    :cond_c1
    const-wide/16 v9, 0x0

    #@c3
    goto :goto_7f

    #@c4
    .line 530
    .restart local v9       #delta:J
    :cond_c4
    const/4 v14, 0x0

    #@c5
    goto :goto_8c

    #@c6
    .line 535
    .restart local v14       #wasModified:Z
    :cond_c6
    new-instance v2, Landroid/media/MediaScanner$FileEntry;

    #@c8
    .end local v2           #entry:Landroid/media/MediaScanner$FileEntry;
    const-wide/16 v3, 0x0

    #@ca
    if-eqz p7, :cond_d6

    #@cc
    const/16 v8, 0x3001

    #@ce
    :goto_ce
    move-object/from16 v5, p1

    #@d0
    move-wide/from16 v6, p3

    #@d2
    invoke-direct/range {v2 .. v8}, Landroid/media/MediaScanner$FileEntry;-><init>(JLjava/lang/String;JI)V

    #@d5
    .restart local v2       #entry:Landroid/media/MediaScanner$FileEntry;
    goto :goto_96

    #@d6
    .end local v2           #entry:Landroid/media/MediaScanner$FileEntry;
    :cond_d6
    const/4 v8, 0x0

    #@d7
    goto :goto_ce

    #@d8
    .line 548
    .restart local v2       #entry:Landroid/media/MediaScanner$FileEntry;
    :cond_d8
    const/4 v3, 0x0

    #@d9
    iput-object v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mArtist:Ljava/lang/String;

    #@db
    .line 549
    const/4 v3, 0x0

    #@dc
    iput-object v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mAlbumArtist:Ljava/lang/String;

    #@de
    .line 550
    const/4 v3, 0x0

    #@df
    iput-object v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mAlbum:Ljava/lang/String;

    #@e1
    .line 551
    const/4 v3, 0x0

    #@e2
    iput-object v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mTitle:Ljava/lang/String;

    #@e4
    .line 552
    const/4 v3, 0x0

    #@e5
    iput-object v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mComposer:Ljava/lang/String;

    #@e7
    .line 553
    const/4 v3, 0x0

    #@e8
    iput-object v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mGenre:Ljava/lang/String;

    #@ea
    .line 554
    const/4 v3, 0x0

    #@eb
    iput v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mTrack:I

    #@ed
    .line 555
    const/4 v3, 0x0

    #@ee
    iput v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mYear:I

    #@f0
    .line 556
    const/4 v3, 0x0

    #@f1
    iput v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mDuration:I

    #@f3
    .line 557
    move-object/from16 v0, p1

    #@f5
    iput-object v0, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mPath:Ljava/lang/String;

    #@f7
    .line 558
    move-wide/from16 v0, p3

    #@f9
    iput-wide v0, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mLastModified:J

    #@fb
    .line 559
    const/4 v3, 0x0

    #@fc
    iput-object v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mWriter:Ljava/lang/String;

    #@fe
    .line 560
    const/4 v3, 0x0

    #@ff
    iput v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mCompilation:I

    #@101
    .line 561
    const/4 v3, 0x0

    #@102
    iput-boolean v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mIsDrm:Z

    #@104
    .line 562
    const/4 v3, 0x0

    #@105
    iput v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mWidth:I

    #@107
    .line 563
    const/4 v3, 0x0

    #@108
    iput v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mHeight:I

    #@10a
    .line 565
    const/4 v3, 0x0

    #@10b
    iput v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mProtectedType:I

    #@10d
    .line 568
    const/4 v3, 0x0

    #@10e
    iput-boolean v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mparseLatLonSuccess:Z

    #@110
    .line 569
    const/4 v3, 0x0

    #@111
    iput v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mlatitude:F

    #@113
    .line 570
    const/4 v3, 0x0

    #@114
    iput v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mlongitude:F

    #@116
    .line 573
    const/4 v3, 0x0

    #@117
    iput v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mIsLGEVdieo:I

    #@119
    goto :goto_b3
.end method

.method public doScanFile(Ljava/lang/String;Ljava/lang/String;JJZZZZ)Landroid/net/Uri;
    .registers 32
    .parameter "path"
    .parameter "mimeType"
    .parameter "lastModified"
    .parameter "fileSize"
    .parameter "isDirectory"
    .parameter "scanAlways"
    .parameter "noMedia"
    .parameter "skipOpen"

    #@0
    .prologue
    .line 594
    const/16 v19, 0x0

    #@2
    .local v19, result:Landroid/net/Uri;
    move-object/from16 v3, p0

    #@4
    move-object/from16 v4, p1

    #@6
    move-object/from16 v5, p2

    #@8
    move-wide/from16 v6, p3

    #@a
    move-wide/from16 v8, p5

    #@c
    move/from16 v10, p7

    #@e
    move/from16 v11, p9

    #@10
    .line 597
    :try_start_10
    invoke-virtual/range {v3 .. v11}, Landroid/media/MediaScanner$MyMediaScannerClient;->beginFile(Ljava/lang/String;Ljava/lang/String;JJZZ)Landroid/media/MediaScanner$FileEntry;

    #@13
    move-result-object v4

    #@14
    .line 603
    .local v4, entry:Landroid/media/MediaScanner$FileEntry;
    move-object/from16 v0, p0

    #@16
    iget-object v3, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@18
    invoke-static {v3}, Landroid/media/MediaScanner;->access$700(Landroid/media/MediaScanner;)I

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_22

    #@1e
    .line 604
    const-wide/16 v10, 0x0

    #@20
    iput-wide v10, v4, Landroid/media/MediaScanner$FileEntry;->mRowId:J

    #@22
    .line 607
    :cond_22
    if-eqz v4, :cond_38

    #@24
    iget-boolean v3, v4, Landroid/media/MediaScanner$FileEntry;->mLastModifiedChanged:Z

    #@26
    if-nez v3, :cond_2a

    #@28
    if-eqz p8, :cond_38

    #@2a
    .line 608
    :cond_2a
    if-eqz p9, :cond_39

    #@2c
    .line 610
    const/4 v5, 0x0

    #@2d
    const/4 v6, 0x0

    #@2e
    const/4 v7, 0x0

    #@2f
    const/4 v8, 0x0

    #@30
    const/4 v9, 0x0

    #@31
    const/4 v10, 0x0

    #@32
    move-object/from16 v3, p0

    #@34
    invoke-direct/range {v3 .. v10}, Landroid/media/MediaScanner$MyMediaScannerClient;->endFile(Landroid/media/MediaScanner$FileEntry;ZZZZZZ)Landroid/net/Uri;

    #@37
    move-result-object v19

    #@38
    .line 673
    .end local v4           #entry:Landroid/media/MediaScanner$FileEntry;
    :cond_38
    :goto_38
    return-object v19

    #@39
    .line 613
    .restart local v4       #entry:Landroid/media/MediaScanner$FileEntry;
    :cond_39
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@3c
    move-result-object v18

    #@3d
    .line 614
    .local v18, lowpath:Ljava/lang/String;
    const-string v3, "/ringtones/"

    #@3f
    move-object/from16 v0, v18

    #@41
    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@44
    move-result v3

    #@45
    if-lez v3, :cond_152

    #@47
    const/4 v5, 0x1

    #@48
    .line 615
    .local v5, ringtones:Z
    :goto_48
    const-string v3, "/notifications/"

    #@4a
    move-object/from16 v0, v18

    #@4c
    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@4f
    move-result v3

    #@50
    if-lez v3, :cond_155

    #@52
    const/4 v6, 0x1

    #@53
    .line 616
    .local v6, notifications:Z
    :goto_53
    const-string v3, "/alarms/"

    #@55
    move-object/from16 v0, v18

    #@57
    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@5a
    move-result v3

    #@5b
    if-lez v3, :cond_158

    #@5d
    const/4 v7, 0x1

    #@5e
    .line 617
    .local v7, alarms:Z
    :goto_5e
    const-string v3, "/podcasts/"

    #@60
    move-object/from16 v0, v18

    #@62
    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@65
    move-result v3

    #@66
    if-lez v3, :cond_15b

    #@68
    const/4 v9, 0x1

    #@69
    .line 620
    .local v9, podcasts:Z
    :goto_69
    const-string v3, "/my_sounds/"

    #@6b
    move-object/from16 v0, v18

    #@6d
    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@70
    move-result v3

    #@71
    if-lez v3, :cond_15e

    #@73
    const-string v3, "3GP"

    #@75
    const-string v10, "."

    #@77
    move-object/from16 v0, p1

    #@79
    invoke-virtual {v0, v10}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    #@7c
    move-result v10

    #@7d
    add-int/lit8 v10, v10, 0x1

    #@7f
    move-object/from16 v0, p1

    #@81
    invoke-virtual {v0, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@84
    move-result-object v10

    #@85
    invoke-virtual {v10}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@88
    move-result-object v10

    #@89
    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8c
    move-result v3

    #@8d
    if-nez v3, :cond_ab

    #@8f
    const-string v3, "AMR"

    #@91
    const-string v10, "."

    #@93
    move-object/from16 v0, p1

    #@95
    invoke-virtual {v0, v10}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    #@98
    move-result v10

    #@99
    add-int/lit8 v10, v10, 0x1

    #@9b
    move-object/from16 v0, p1

    #@9d
    invoke-virtual {v0, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@a0
    move-result-object v10

    #@a1
    invoke-virtual {v10}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@a4
    move-result-object v10

    #@a5
    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a8
    move-result v3

    #@a9
    if-eqz v3, :cond_15e

    #@ab
    :cond_ab
    const/16 v20, 0x1

    #@ad
    .line 625
    .local v20, voicerecording:Z
    :goto_ad
    const-string v3, "/music/"

    #@af
    move-object/from16 v0, v18

    #@b1
    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@b4
    move-result v3

    #@b5
    if-gtz v3, :cond_c1

    #@b7
    if-nez v5, :cond_162

    #@b9
    if-nez v6, :cond_162

    #@bb
    if-nez v7, :cond_162

    #@bd
    if-nez v9, :cond_162

    #@bf
    if-nez v20, :cond_162

    #@c1
    :cond_c1
    const/4 v8, 0x1

    #@c2
    .line 633
    .local v8, music:Z
    :goto_c2
    move-object/from16 v0, p0

    #@c4
    iget v3, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->mFileType:I

    #@c6
    invoke-static {v3}, Landroid/media/MediaFile;->isAudioFileType(I)Z

    #@c9
    move-result v15

    #@ca
    .line 634
    .local v15, isaudio:Z
    move-object/from16 v0, p0

    #@cc
    iget v3, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->mFileType:I

    #@ce
    invoke-static {v3}, Landroid/media/MediaFile;->isVideoFileType(I)Z

    #@d1
    move-result v17

    #@d2
    .line 635
    .local v17, isvideo:Z
    move-object/from16 v0, p0

    #@d4
    iget v3, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->mFileType:I

    #@d6
    invoke-static {v3}, Landroid/media/MediaFile;->isImageFileType(I)Z

    #@d9
    move-result v16

    #@da
    .line 637
    .local v16, isimage:Z
    if-nez v15, :cond_e0

    #@dc
    if-nez v17, :cond_e0

    #@de
    if-eqz v16, :cond_12e

    #@e0
    .line 638
    :cond_e0
    move-object/from16 v0, p0

    #@e2
    iget-object v3, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@e4
    invoke-static {v3}, Landroid/media/MediaScanner;->access$800(Landroid/media/MediaScanner;)Z

    #@e7
    move-result v3

    #@e8
    if-eqz v3, :cond_12e

    #@ea
    move-object/from16 v0, p0

    #@ec
    iget-object v3, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@ee
    invoke-static {v3}, Landroid/media/MediaScanner;->access$900(Landroid/media/MediaScanner;)Ljava/lang/String;

    #@f1
    move-result-object v3

    #@f2
    move-object/from16 v0, p1

    #@f4
    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@f7
    move-result v3

    #@f8
    if-eqz v3, :cond_12e

    #@fa
    .line 640
    new-instance v3, Ljava/lang/StringBuilder;

    #@fc
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@ff
    invoke-static {}, Landroid/os/Environment;->getMediaStorageDirectory()Ljava/io/File;

    #@102
    move-result-object v10

    #@103
    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@106
    move-result-object v3

    #@107
    move-object/from16 v0, p0

    #@109
    iget-object v10, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@10b
    invoke-static {v10}, Landroid/media/MediaScanner;->access$900(Landroid/media/MediaScanner;)Ljava/lang/String;

    #@10e
    move-result-object v10

    #@10f
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    #@112
    move-result v10

    #@113
    move-object/from16 v0, p1

    #@115
    invoke-virtual {v0, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@118
    move-result-object v10

    #@119
    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v3

    #@11d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@120
    move-result-object v12

    #@121
    .line 642
    .local v12, directPath:Ljava/lang/String;
    new-instance v14, Ljava/io/File;

    #@123
    invoke-direct {v14, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@126
    .line 643
    .local v14, f:Ljava/io/File;
    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    #@129
    move-result v3

    #@12a
    if-eqz v3, :cond_12e

    #@12c
    .line 644
    move-object/from16 p1, v12

    #@12e
    .line 650
    .end local v12           #directPath:Ljava/lang/String;
    .end local v14           #f:Ljava/io/File;
    :cond_12e
    if-nez v15, :cond_132

    #@130
    if-eqz v17, :cond_141

    #@132
    .line 652
    :cond_132
    if-nez p10, :cond_141

    #@134
    .line 653
    move-object/from16 v0, p0

    #@136
    iget-object v3, v0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@138
    move-object/from16 v0, p1

    #@13a
    move-object/from16 v1, p2

    #@13c
    move-object/from16 v2, p0

    #@13e
    invoke-static {v3, v0, v1, v2}, Landroid/media/MediaScanner;->access$1000(Landroid/media/MediaScanner;Ljava/lang/String;Ljava/lang/String;Landroid/media/MediaScannerClient;)V

    #@141
    .line 657
    :cond_141
    if-eqz v16, :cond_148

    #@143
    .line 658
    if-nez p10, :cond_148

    #@145
    .line 659
    invoke-direct/range {p0 .. p1}, Landroid/media/MediaScanner$MyMediaScannerClient;->processImageFile(Ljava/lang/String;)V

    #@148
    :cond_148
    move-object/from16 v3, p0

    #@14a
    move/from16 v10, p10

    #@14c
    .line 664
    invoke-direct/range {v3 .. v10}, Landroid/media/MediaScanner$MyMediaScannerClient;->endFile(Landroid/media/MediaScanner$FileEntry;ZZZZZZ)Landroid/net/Uri;
    :try_end_14f
    .catch Landroid/os/RemoteException; {:try_start_10 .. :try_end_14f} :catch_165

    #@14f
    move-result-object v19

    #@150
    goto/16 :goto_38

    #@152
    .line 614
    .end local v5           #ringtones:Z
    .end local v6           #notifications:Z
    .end local v7           #alarms:Z
    .end local v8           #music:Z
    .end local v9           #podcasts:Z
    .end local v15           #isaudio:Z
    .end local v16           #isimage:Z
    .end local v17           #isvideo:Z
    .end local v20           #voicerecording:Z
    :cond_152
    const/4 v5, 0x0

    #@153
    goto/16 :goto_48

    #@155
    .line 615
    .restart local v5       #ringtones:Z
    :cond_155
    const/4 v6, 0x0

    #@156
    goto/16 :goto_53

    #@158
    .line 616
    .restart local v6       #notifications:Z
    :cond_158
    const/4 v7, 0x0

    #@159
    goto/16 :goto_5e

    #@15b
    .line 617
    .restart local v7       #alarms:Z
    :cond_15b
    const/4 v9, 0x0

    #@15c
    goto/16 :goto_69

    #@15e
    .line 620
    .restart local v9       #podcasts:Z
    :cond_15e
    const/16 v20, 0x0

    #@160
    goto/16 :goto_ad

    #@162
    .line 625
    .restart local v20       #voicerecording:Z
    :cond_162
    const/4 v8, 0x0

    #@163
    goto/16 :goto_c2

    #@165
    .line 668
    .end local v4           #entry:Landroid/media/MediaScanner$FileEntry;
    .end local v5           #ringtones:Z
    .end local v6           #notifications:Z
    .end local v7           #alarms:Z
    .end local v9           #podcasts:Z
    .end local v18           #lowpath:Ljava/lang/String;
    .end local v20           #voicerecording:Z
    :catch_165
    move-exception v13

    #@166
    .line 669
    .local v13, e:Landroid/os/RemoteException;
    const-string v3, "MediaScanner"

    #@168
    const-string v10, "RemoteException in MediaScanner.scanFile()"

    #@16a
    invoke-static {v3, v10, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16d
    goto/16 :goto_38
.end method

.method public getGenreName(Ljava/lang/String;)Ljava/lang/String;
    .registers 13
    .parameter "genreTagValue"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/16 v10, 0xff

    #@3
    const/16 v9, 0x29

    #@5
    .line 856
    if-nez p1, :cond_8

    #@7
    .line 905
    :cond_7
    :goto_7
    return-object v7

    #@8
    .line 859
    :cond_8
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@b
    move-result v4

    #@c
    .line 861
    .local v4, length:I
    if-lez v4, :cond_78

    #@e
    .line 862
    const/4 v6, 0x0

    #@f
    .line 863
    .local v6, parenthesized:Z
    new-instance v5, Ljava/lang/StringBuffer;

    #@11
    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    #@14
    .line 864
    .local v5, number:Ljava/lang/StringBuffer;
    const/4 v3, 0x0

    #@15
    .line 865
    .local v3, i:I
    :goto_15
    if-ge v3, v4, :cond_2f

    #@17
    .line 866
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    #@1a
    move-result v0

    #@1b
    .line 867
    .local v0, c:C
    if-nez v3, :cond_25

    #@1d
    const/16 v8, 0x28

    #@1f
    if-ne v0, v8, :cond_25

    #@21
    .line 868
    const/4 v6, 0x1

    #@22
    .line 865
    :goto_22
    add-int/lit8 v3, v3, 0x1

    #@24
    goto :goto_15

    #@25
    .line 869
    :cond_25
    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    #@28
    move-result v8

    #@29
    if-eqz v8, :cond_2f

    #@2b
    .line 870
    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    #@2e
    goto :goto_22

    #@2f
    .line 875
    .end local v0           #c:C
    :cond_2f
    if-ge v3, v4, :cond_59

    #@31
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    #@34
    move-result v1

    #@35
    .line 876
    .local v1, charAfterNumber:C
    :goto_35
    if-eqz v6, :cond_39

    #@37
    if-eq v1, v9, :cond_41

    #@39
    :cond_39
    if-nez v6, :cond_78

    #@3b
    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    #@3e
    move-result v8

    #@3f
    if-eqz v8, :cond_78

    #@41
    .line 879
    :cond_41
    :try_start_41
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@44
    move-result-object v8

    #@45
    invoke-static {v8}, Ljava/lang/Short;->parseShort(Ljava/lang/String;)S

    #@48
    move-result v2

    #@49
    .line 880
    .local v2, genreIndex:S
    if-ltz v2, :cond_78

    #@4b
    .line 881
    invoke-static {}, Landroid/media/MediaScanner;->access$1200()[Ljava/lang/String;

    #@4e
    move-result-object v8

    #@4f
    array-length v8, v8

    #@50
    if-ge v2, v8, :cond_5c

    #@52
    .line 882
    invoke-static {}, Landroid/media/MediaScanner;->access$1200()[Ljava/lang/String;

    #@55
    move-result-object v8

    #@56
    aget-object v7, v8, v2

    #@58
    goto :goto_7

    #@59
    .line 875
    .end local v1           #charAfterNumber:C
    .end local v2           #genreIndex:S
    :cond_59
    const/16 v1, 0x20

    #@5b
    goto :goto_35

    #@5c
    .line 883
    .restart local v1       #charAfterNumber:C
    .restart local v2       #genreIndex:S
    :cond_5c
    if-eq v2, v10, :cond_7

    #@5e
    .line 885
    if-ge v2, v10, :cond_7a

    #@60
    add-int/lit8 v8, v3, 0x1

    #@62
    if-ge v8, v4, :cond_7a

    #@64
    .line 888
    if-eqz v6, :cond_6a

    #@66
    if-ne v1, v9, :cond_6a

    #@68
    .line 889
    add-int/lit8 v3, v3, 0x1

    #@6a
    .line 891
    :cond_6a
    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@6d
    move-result-object v8

    #@6e
    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@71
    move-result-object v7

    #@72
    .line 892
    .local v7, ret:Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@75
    move-result v8

    #@76
    if-nez v8, :cond_7

    #@78
    .end local v1           #charAfterNumber:C
    .end local v2           #genreIndex:S
    .end local v3           #i:I
    .end local v5           #number:Ljava/lang/StringBuffer;
    .end local v6           #parenthesized:Z
    .end local v7           #ret:Ljava/lang/String;
    :cond_78
    :goto_78
    move-object v7, p1

    #@79
    .line 905
    goto :goto_7

    #@7a
    .line 897
    .restart local v1       #charAfterNumber:C
    .restart local v2       #genreIndex:S
    .restart local v3       #i:I
    .restart local v5       #number:Ljava/lang/StringBuffer;
    .restart local v6       #parenthesized:Z
    :cond_7a
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_7d
    .catch Ljava/lang/NumberFormatException; {:try_start_41 .. :try_end_7d} :catch_7f

    #@7d
    move-result-object v7

    #@7e
    goto :goto_7

    #@7f
    .line 900
    .end local v2           #genreIndex:S
    :catch_7f
    move-exception v8

    #@80
    goto :goto_78
.end method

.method public handleStringTag(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 767
    const-string/jumbo v3, "title"

    #@5
    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@8
    move-result v3

    #@9
    if-nez v3, :cond_14

    #@b
    const-string/jumbo v3, "title;"

    #@e
    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@11
    move-result v3

    #@12
    if-eqz v3, :cond_17

    #@14
    .line 771
    :cond_14
    iput-object p2, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mTitle:Ljava/lang/String;

    #@16
    .line 826
    :cond_16
    :goto_16
    return-void

    #@17
    .line 772
    :cond_17
    const-string v3, "artist"

    #@19
    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1c
    move-result v3

    #@1d
    if-nez v3, :cond_27

    #@1f
    const-string v3, "artist;"

    #@21
    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@24
    move-result v3

    #@25
    if-eqz v3, :cond_2e

    #@27
    .line 773
    :cond_27
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    iput-object v1, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mArtist:Ljava/lang/String;

    #@2d
    goto :goto_16

    #@2e
    .line 774
    :cond_2e
    const-string v3, "albumartist"

    #@30
    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@33
    move-result v3

    #@34
    if-nez v3, :cond_4e

    #@36
    const-string v3, "albumartist;"

    #@38
    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@3b
    move-result v3

    #@3c
    if-nez v3, :cond_4e

    #@3e
    const-string v3, "band"

    #@40
    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@43
    move-result v3

    #@44
    if-nez v3, :cond_4e

    #@46
    const-string v3, "band;"

    #@48
    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@4b
    move-result v3

    #@4c
    if-eqz v3, :cond_55

    #@4e
    .line 776
    :cond_4e
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@51
    move-result-object v1

    #@52
    iput-object v1, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mAlbumArtist:Ljava/lang/String;

    #@54
    goto :goto_16

    #@55
    .line 777
    :cond_55
    const-string v3, "album"

    #@57
    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@5a
    move-result v3

    #@5b
    if-nez v3, :cond_65

    #@5d
    const-string v3, "album;"

    #@5f
    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@62
    move-result v3

    #@63
    if-eqz v3, :cond_6c

    #@65
    .line 778
    :cond_65
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@68
    move-result-object v1

    #@69
    iput-object v1, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mAlbum:Ljava/lang/String;

    #@6b
    goto :goto_16

    #@6c
    .line 779
    :cond_6c
    const-string v3, "composer"

    #@6e
    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@71
    move-result v3

    #@72
    if-nez v3, :cond_7c

    #@74
    const-string v3, "composer;"

    #@76
    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@79
    move-result v3

    #@7a
    if-eqz v3, :cond_83

    #@7c
    .line 780
    :cond_7c
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@7f
    move-result-object v1

    #@80
    iput-object v1, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mComposer:Ljava/lang/String;

    #@82
    goto :goto_16

    #@83
    .line 781
    :cond_83
    iget-object v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->this$0:Landroid/media/MediaScanner;

    #@85
    invoke-static {v3}, Landroid/media/MediaScanner;->access$1100(Landroid/media/MediaScanner;)Z

    #@88
    move-result v3

    #@89
    if-eqz v3, :cond_a3

    #@8b
    const-string v3, "genre"

    #@8d
    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@90
    move-result v3

    #@91
    if-nez v3, :cond_9b

    #@93
    const-string v3, "genre;"

    #@95
    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@98
    move-result v3

    #@99
    if-eqz v3, :cond_a3

    #@9b
    .line 783
    :cond_9b
    invoke-virtual {p0, p2}, Landroid/media/MediaScanner$MyMediaScannerClient;->getGenreName(Ljava/lang/String;)Ljava/lang/String;

    #@9e
    move-result-object v1

    #@9f
    iput-object v1, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mGenre:Ljava/lang/String;

    #@a1
    goto/16 :goto_16

    #@a3
    .line 784
    :cond_a3
    const-string/jumbo v3, "year"

    #@a6
    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@a9
    move-result v3

    #@aa
    if-nez v3, :cond_b5

    #@ac
    const-string/jumbo v3, "year;"

    #@af
    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@b2
    move-result v3

    #@b3
    if-eqz v3, :cond_bd

    #@b5
    .line 785
    :cond_b5
    invoke-direct {p0, p2, v2, v2}, Landroid/media/MediaScanner$MyMediaScannerClient;->parseSubstring(Ljava/lang/String;II)I

    #@b8
    move-result v1

    #@b9
    iput v1, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mYear:I

    #@bb
    goto/16 :goto_16

    #@bd
    .line 786
    :cond_bd
    const-string/jumbo v3, "tracknumber"

    #@c0
    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@c3
    move-result v3

    #@c4
    if-nez v3, :cond_cf

    #@c6
    const-string/jumbo v3, "tracknumber;"

    #@c9
    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@cc
    move-result v3

    #@cd
    if-eqz v3, :cond_de

    #@cf
    .line 789
    :cond_cf
    invoke-direct {p0, p2, v2, v2}, Landroid/media/MediaScanner$MyMediaScannerClient;->parseSubstring(Ljava/lang/String;II)I

    #@d2
    move-result v0

    #@d3
    .line 790
    .local v0, num:I
    iget v1, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mTrack:I

    #@d5
    div-int/lit16 v1, v1, 0x3e8

    #@d7
    mul-int/lit16 v1, v1, 0x3e8

    #@d9
    add-int/2addr v1, v0

    #@da
    iput v1, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mTrack:I

    #@dc
    goto/16 :goto_16

    #@de
    .line 791
    .end local v0           #num:I
    :cond_de
    const-string v3, "discnumber"

    #@e0
    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@e3
    move-result v3

    #@e4
    if-nez v3, :cond_f8

    #@e6
    const-string/jumbo v3, "set"

    #@e9
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ec
    move-result v3

    #@ed
    if-nez v3, :cond_f8

    #@ef
    const-string/jumbo v3, "set;"

    #@f2
    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@f5
    move-result v3

    #@f6
    if-eqz v3, :cond_107

    #@f8
    .line 795
    :cond_f8
    invoke-direct {p0, p2, v2, v2}, Landroid/media/MediaScanner$MyMediaScannerClient;->parseSubstring(Ljava/lang/String;II)I

    #@fb
    move-result v0

    #@fc
    .line 796
    .restart local v0       #num:I
    mul-int/lit16 v1, v0, 0x3e8

    #@fe
    iget v2, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mTrack:I

    #@100
    rem-int/lit16 v2, v2, 0x3e8

    #@102
    add-int/2addr v1, v2

    #@103
    iput v1, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mTrack:I

    #@105
    goto/16 :goto_16

    #@107
    .line 797
    .end local v0           #num:I
    :cond_107
    const-string v3, "duration"

    #@109
    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@10c
    move-result v3

    #@10d
    if-eqz v3, :cond_117

    #@10f
    .line 798
    invoke-direct {p0, p2, v2, v2}, Landroid/media/MediaScanner$MyMediaScannerClient;->parseSubstring(Ljava/lang/String;II)I

    #@112
    move-result v1

    #@113
    iput v1, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mDuration:I

    #@115
    goto/16 :goto_16

    #@117
    .line 799
    :cond_117
    const-string/jumbo v3, "writer"

    #@11a
    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@11d
    move-result v3

    #@11e
    if-nez v3, :cond_129

    #@120
    const-string/jumbo v3, "writer;"

    #@123
    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@126
    move-result v3

    #@127
    if-eqz v3, :cond_131

    #@129
    .line 800
    :cond_129
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@12c
    move-result-object v1

    #@12d
    iput-object v1, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mWriter:Ljava/lang/String;

    #@12f
    goto/16 :goto_16

    #@131
    .line 801
    :cond_131
    const-string v3, "compilation"

    #@133
    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@136
    move-result v3

    #@137
    if-eqz v3, :cond_141

    #@139
    .line 802
    invoke-direct {p0, p2, v2, v2}, Landroid/media/MediaScanner$MyMediaScannerClient;->parseSubstring(Ljava/lang/String;II)I

    #@13c
    move-result v1

    #@13d
    iput v1, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mCompilation:I

    #@13f
    goto/16 :goto_16

    #@141
    .line 803
    :cond_141
    const-string/jumbo v3, "isdrm"

    #@144
    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@147
    move-result v3

    #@148
    if-eqz v3, :cond_156

    #@14a
    .line 804
    invoke-direct {p0, p2, v2, v2}, Landroid/media/MediaScanner$MyMediaScannerClient;->parseSubstring(Ljava/lang/String;II)I

    #@14d
    move-result v3

    #@14e
    if-ne v3, v1, :cond_154

    #@150
    :goto_150
    iput-boolean v1, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mIsDrm:Z

    #@152
    goto/16 :goto_16

    #@154
    :cond_154
    move v1, v2

    #@155
    goto :goto_150

    #@156
    .line 805
    :cond_156
    const-string/jumbo v3, "width"

    #@159
    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@15c
    move-result v3

    #@15d
    if-eqz v3, :cond_167

    #@15f
    .line 806
    invoke-direct {p0, p2, v2, v2}, Landroid/media/MediaScanner$MyMediaScannerClient;->parseSubstring(Ljava/lang/String;II)I

    #@162
    move-result v1

    #@163
    iput v1, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mWidth:I

    #@165
    goto/16 :goto_16

    #@167
    .line 807
    :cond_167
    const-string v3, "height"

    #@169
    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@16c
    move-result v3

    #@16d
    if-eqz v3, :cond_177

    #@16f
    .line 808
    invoke-direct {p0, p2, v2, v2}, Landroid/media/MediaScanner$MyMediaScannerClient;->parseSubstring(Ljava/lang/String;II)I

    #@172
    move-result v1

    #@173
    iput v1, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mHeight:I

    #@175
    goto/16 :goto_16

    #@177
    .line 811
    :cond_177
    const-string/jumbo v3, "location"

    #@17a
    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@17d
    move-result v3

    #@17e
    if-eqz v3, :cond_16

    #@180
    .line 812
    invoke-direct {p0, p2, v1}, Landroid/media/MediaScanner$MyMediaScannerClient;->splitLatLon(Ljava/lang/String;Z)Ljava/lang/String;

    #@183
    move-result-object v3

    #@184
    invoke-direct {p0, v3, v1}, Landroid/media/MediaScanner$MyMediaScannerClient;->convertRationalLatLonToFloat(Ljava/lang/String;Z)Z

    #@187
    move-result v3

    #@188
    if-eqz v3, :cond_16

    #@18a
    invoke-direct {p0, p2, v2}, Landroid/media/MediaScanner$MyMediaScannerClient;->splitLatLon(Ljava/lang/String;Z)Ljava/lang/String;

    #@18d
    move-result-object v3

    #@18e
    invoke-direct {p0, v3, v2}, Landroid/media/MediaScanner$MyMediaScannerClient;->convertRationalLatLonToFloat(Ljava/lang/String;Z)Z

    #@191
    move-result v2

    #@192
    if-eqz v2, :cond_16

    #@194
    .line 814
    iput-boolean v1, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mparseLatLonSuccess:Z

    #@196
    goto/16 :goto_16
.end method

.method public scanFile(Ljava/lang/String;JJZZZ)V
    .registers 20
    .parameter "path"
    .parameter "lastModified"
    .parameter "fileSize"
    .parameter "isDirectory"
    .parameter "noMedia"
    .parameter "skipOpen"

    #@0
    .prologue
    .line 586
    const/4 v2, 0x0

    #@1
    const/4 v8, 0x0

    #@2
    move-object v0, p0

    #@3
    move-object v1, p1

    #@4
    move-wide v3, p2

    #@5
    move-wide v5, p4

    #@6
    move/from16 v7, p6

    #@8
    move/from16 v9, p7

    #@a
    move/from16 v10, p8

    #@c
    invoke-virtual/range {v0 .. v10}, Landroid/media/MediaScanner$MyMediaScannerClient;->doScanFile(Ljava/lang/String;Ljava/lang/String;JJZZZZ)Landroid/net/Uri;

    #@f
    .line 588
    return-void
.end method

.method public setMimeType(Ljava/lang/String;)V
    .registers 6
    .parameter "mimeType"

    #@0
    .prologue
    .line 921
    const-string v2, "audio/mp4"

    #@2
    iget-object v3, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mMimeType:Ljava/lang/String;

    #@4
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_14

    #@a
    const-string/jumbo v2, "video"

    #@d
    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_14

    #@13
    .line 944
    :goto_13
    return-void

    #@14
    .line 929
    :cond_14
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_DRM:Z

    #@16
    if-eqz v2, :cond_40

    #@18
    .line 930
    iget-object v2, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mMimeType:Ljava/lang/String;

    #@1a
    invoke-static {v2}, Landroid/media/MediaFile;->getFileTypeForMimeType(Ljava/lang/String;)I

    #@1d
    move-result v1

    #@1e
    .line 932
    .local v1, fileType:I
    const/16 v2, 0x34

    #@20
    if-lt v1, v2, :cond_26

    #@22
    const/16 v2, 0x39

    #@24
    if-le v1, v2, :cond_2f

    #@26
    .line 933
    :cond_26
    iput-object p1, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mMimeType:Ljava/lang/String;

    #@28
    .line 943
    .end local v1           #fileType:I
    :cond_28
    :goto_28
    invoke-static {p1}, Landroid/media/MediaFile;->getFileTypeForMimeType(Ljava/lang/String;)I

    #@2b
    move-result v2

    #@2c
    iput v2, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mFileType:I

    #@2e
    goto :goto_13

    #@2f
    .line 935
    .restart local v1       #fileType:I
    :cond_2f
    iget-object v2, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mPath:Ljava/lang/String;

    #@31
    invoke-static {v2}, Lcom/lge/lgdrm/DrmManager;->isDRM(Ljava/lang/String;)I

    #@34
    move-result v0

    #@35
    .line 936
    .local v0, drmType:I
    const/16 v2, 0x501

    #@37
    if-eq v0, v2, :cond_3d

    #@39
    const/16 v2, 0x1800

    #@3b
    if-ne v0, v2, :cond_28

    #@3d
    .line 937
    :cond_3d
    iput-object p1, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mMimeType:Ljava/lang/String;

    #@3f
    goto :goto_28

    #@40
    .line 942
    .end local v0           #drmType:I
    .end local v1           #fileType:I
    :cond_40
    iput-object p1, p0, Landroid/media/MediaScanner$MyMediaScannerClient;->mMimeType:Ljava/lang/String;

    #@42
    goto :goto_28
.end method
