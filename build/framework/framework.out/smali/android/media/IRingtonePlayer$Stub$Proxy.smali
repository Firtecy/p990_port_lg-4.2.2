.class Landroid/media/IRingtonePlayer$Stub$Proxy;
.super Ljava/lang/Object;
.source "IRingtonePlayer.java"

# interfaces
.implements Landroid/media/IRingtonePlayer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/IRingtonePlayer$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 124
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 125
    iput-object p1, p0, Landroid/media/IRingtonePlayer$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 126
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 129
    iget-object v0, p0, Landroid/media/IRingtonePlayer$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 133
    const-string v0, "android.media.IRingtonePlayer"

    #@2
    return-object v0
.end method

.method public isPlaying(Landroid/os/IBinder;)Z
    .registers 8
    .parameter "token"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 176
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 177
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 180
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "android.media.IRingtonePlayer"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 181
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@11
    .line 182
    iget-object v3, p0, Landroid/media/IRingtonePlayer$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/4 v4, 0x3

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 183
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 184
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1e
    .catchall {:try_start_9 .. :try_end_1e} :catchall_29

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_22

    #@21
    const/4 v2, 0x1

    #@22
    .line 187
    .local v2, _result:Z
    :cond_22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 188
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 190
    return v2

    #@29
    .line 187
    .end local v2           #_result:Z
    :catchall_29
    move-exception v3

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 188
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v3
.end method

.method public play(Landroid/os/IBinder;Landroid/net/Uri;I)V
    .registers 9
    .parameter "token"
    .parameter "uri"
    .parameter "streamType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 138
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 139
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 141
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.media.IRingtonePlayer"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 142
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 143
    if-eqz p2, :cond_2e

    #@12
    .line 144
    const/4 v2, 0x1

    #@13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 145
    const/4 v2, 0x0

    #@17
    invoke-virtual {p2, v0, v2}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    #@1a
    .line 150
    :goto_1a
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 151
    iget-object v2, p0, Landroid/media/IRingtonePlayer$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1f
    const/4 v3, 0x1

    #@20
    const/4 v4, 0x0

    #@21
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@24
    .line 152
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_27
    .catchall {:try_start_8 .. :try_end_27} :catchall_33

    #@27
    .line 155
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 156
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 158
    return-void

    #@2e
    .line 148
    :cond_2e
    const/4 v2, 0x0

    #@2f
    :try_start_2f
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_32
    .catchall {:try_start_2f .. :try_end_32} :catchall_33

    #@32
    goto :goto_1a

    #@33
    .line 155
    :catchall_33
    move-exception v2

    #@34
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@37
    .line 156
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3a
    throw v2
.end method

.method public playAsync(Landroid/net/Uri;Landroid/os/UserHandle;ZI)V
    .registers 10
    .parameter "uri"
    .parameter "user"
    .parameter "looping"
    .parameter "streamType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 195
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 196
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 198
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "android.media.IRingtonePlayer"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 199
    if-eqz p1, :cond_3c

    #@11
    .line 200
    const/4 v4, 0x1

    #@12
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 201
    const/4 v4, 0x0

    #@16
    invoke-virtual {p1, v0, v4}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    #@19
    .line 206
    :goto_19
    if-eqz p2, :cond_49

    #@1b
    .line 207
    const/4 v4, 0x1

    #@1c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 208
    const/4 v4, 0x0

    #@20
    invoke-virtual {p2, v0, v4}, Landroid/os/UserHandle;->writeToParcel(Landroid/os/Parcel;I)V

    #@23
    .line 213
    :goto_23
    if-eqz p3, :cond_4e

    #@25
    :goto_25
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@28
    .line 214
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@2b
    .line 215
    iget-object v2, p0, Landroid/media/IRingtonePlayer$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2d
    const/4 v3, 0x4

    #@2e
    const/4 v4, 0x0

    #@2f
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@32
    .line 216
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_35
    .catchall {:try_start_a .. :try_end_35} :catchall_41

    #@35
    .line 219
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@38
    .line 220
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3b
    .line 222
    return-void

    #@3c
    .line 204
    :cond_3c
    const/4 v4, 0x0

    #@3d
    :try_start_3d
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_40
    .catchall {:try_start_3d .. :try_end_40} :catchall_41

    #@40
    goto :goto_19

    #@41
    .line 219
    :catchall_41
    move-exception v2

    #@42
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@45
    .line 220
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@48
    throw v2

    #@49
    .line 211
    :cond_49
    const/4 v4, 0x0

    #@4a
    :try_start_4a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_4d
    .catchall {:try_start_4a .. :try_end_4d} :catchall_41

    #@4d
    goto :goto_23

    #@4e
    :cond_4e
    move v2, v3

    #@4f
    .line 213
    goto :goto_25
.end method

.method public stop(Landroid/os/IBinder;)V
    .registers 7
    .parameter "token"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 161
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 162
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 164
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.media.IRingtonePlayer"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 165
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 166
    iget-object v2, p0, Landroid/media/IRingtonePlayer$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v3, 0x2

    #@13
    const/4 v4, 0x0

    #@14
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 167
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_21

    #@1a
    .line 170
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 171
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 173
    return-void

    #@21
    .line 170
    :catchall_21
    move-exception v2

    #@22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 171
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    throw v2
.end method

.method public stopAsync()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 225
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 226
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 228
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.media.IRingtonePlayer"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 229
    iget-object v2, p0, Landroid/media/IRingtonePlayer$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v3, 0x5

    #@10
    const/4 v4, 0x0

    #@11
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 230
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_17
    .catchall {:try_start_8 .. :try_end_17} :catchall_1e

    #@17
    .line 233
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 234
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 236
    return-void

    #@1e
    .line 233
    :catchall_1e
    move-exception v2

    #@1f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 234
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    throw v2
.end method
