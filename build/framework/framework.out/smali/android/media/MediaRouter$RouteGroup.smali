.class public Landroid/media/MediaRouter$RouteGroup;
.super Landroid/media/MediaRouter$RouteInfo;
.source "MediaRouter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/MediaRouter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RouteGroup"
.end annotation


# instance fields
.field final mRoutes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/media/MediaRouter$RouteInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mUpdateName:Z


# direct methods
.method constructor <init>(Landroid/media/MediaRouter$RouteCategory;)V
    .registers 3
    .parameter "category"

    #@0
    .prologue
    .line 1514
    invoke-direct {p0, p1}, Landroid/media/MediaRouter$RouteInfo;-><init>(Landroid/media/MediaRouter$RouteCategory;)V

    #@3
    .line 1510
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Landroid/media/MediaRouter$RouteGroup;->mRoutes:Ljava/util/ArrayList;

    #@a
    .line 1515
    iput-object p0, p0, Landroid/media/MediaRouter$RouteInfo;->mGroup:Landroid/media/MediaRouter$RouteGroup;

    #@c
    .line 1516
    const/4 v0, 0x0

    #@d
    iput v0, p0, Landroid/media/MediaRouter$RouteInfo;->mVolumeHandling:I

    #@f
    .line 1517
    return-void
.end method


# virtual methods
.method public addRoute(Landroid/media/MediaRouter$RouteInfo;)V
    .registers 6
    .parameter "route"

    #@0
    .prologue
    .line 1530
    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getGroup()Landroid/media/MediaRouter$RouteGroup;

    #@3
    move-result-object v1

    #@4
    if-eqz v1, :cond_25

    #@6
    .line 1531
    new-instance v1, Ljava/lang/IllegalStateException;

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "Route "

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    const-string v3, " is already part of a group."

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@24
    throw v1

    #@25
    .line 1533
    :cond_25
    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getCategory()Landroid/media/MediaRouter$RouteCategory;

    #@28
    move-result-object v1

    #@29
    iget-object v2, p0, Landroid/media/MediaRouter$RouteInfo;->mCategory:Landroid/media/MediaRouter$RouteCategory;

    #@2b
    if-eq v1, v2, :cond_5c

    #@2d
    .line 1534
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@2f
    new-instance v2, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v3, "Route cannot be added to a group with a different category. (Route category="

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getCategory()Landroid/media/MediaRouter$RouteCategory;

    #@3d
    move-result-object v3

    #@3e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    const-string v3, " group category="

    #@44
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    iget-object v3, p0, Landroid/media/MediaRouter$RouteInfo;->mCategory:Landroid/media/MediaRouter$RouteCategory;

    #@4a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v2

    #@4e
    const-string v3, ")"

    #@50
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v2

    #@54
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v2

    #@58
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@5b
    throw v1

    #@5c
    .line 1539
    :cond_5c
    iget-object v1, p0, Landroid/media/MediaRouter$RouteGroup;->mRoutes:Ljava/util/ArrayList;

    #@5e
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@61
    move-result v0

    #@62
    .line 1540
    .local v0, at:I
    iget-object v1, p0, Landroid/media/MediaRouter$RouteGroup;->mRoutes:Ljava/util/ArrayList;

    #@64
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@67
    .line 1541
    iput-object p0, p1, Landroid/media/MediaRouter$RouteInfo;->mGroup:Landroid/media/MediaRouter$RouteGroup;

    #@69
    .line 1542
    const/4 v1, 0x1

    #@6a
    iput-boolean v1, p0, Landroid/media/MediaRouter$RouteGroup;->mUpdateName:Z

    #@6c
    .line 1543
    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteGroup;->updateVolume()V

    #@6f
    .line 1544
    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteGroup;->routeUpdated()V

    #@72
    .line 1545
    invoke-static {p1, p0, v0}, Landroid/media/MediaRouter;->dispatchRouteGrouped(Landroid/media/MediaRouter$RouteInfo;Landroid/media/MediaRouter$RouteGroup;I)V

    #@75
    .line 1546
    return-void
.end method

.method public addRoute(Landroid/media/MediaRouter$RouteInfo;I)V
    .registers 6
    .parameter "route"
    .parameter "insertAt"

    #@0
    .prologue
    .line 1555
    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getGroup()Landroid/media/MediaRouter$RouteGroup;

    #@3
    move-result-object v0

    #@4
    if-eqz v0, :cond_25

    #@6
    .line 1556
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Route "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, " is already part of a group."

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@24
    throw v0

    #@25
    .line 1558
    :cond_25
    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getCategory()Landroid/media/MediaRouter$RouteCategory;

    #@28
    move-result-object v0

    #@29
    iget-object v1, p0, Landroid/media/MediaRouter$RouteInfo;->mCategory:Landroid/media/MediaRouter$RouteCategory;

    #@2b
    if-eq v0, v1, :cond_5c

    #@2d
    .line 1559
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@2f
    new-instance v1, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v2, "Route cannot be added to a group with a different category. (Route category="

    #@36
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v1

    #@3a
    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getCategory()Landroid/media/MediaRouter$RouteCategory;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v1

    #@42
    const-string v2, " group category="

    #@44
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v1

    #@48
    iget-object v2, p0, Landroid/media/MediaRouter$RouteInfo;->mCategory:Landroid/media/MediaRouter$RouteCategory;

    #@4a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v1

    #@4e
    const-string v2, ")"

    #@50
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v1

    #@54
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v1

    #@58
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@5b
    throw v0

    #@5c
    .line 1564
    :cond_5c
    iget-object v0, p0, Landroid/media/MediaRouter$RouteGroup;->mRoutes:Ljava/util/ArrayList;

    #@5e
    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@61
    .line 1565
    iput-object p0, p1, Landroid/media/MediaRouter$RouteInfo;->mGroup:Landroid/media/MediaRouter$RouteGroup;

    #@63
    .line 1566
    const/4 v0, 0x1

    #@64
    iput-boolean v0, p0, Landroid/media/MediaRouter$RouteGroup;->mUpdateName:Z

    #@66
    .line 1567
    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteGroup;->updateVolume()V

    #@69
    .line 1568
    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteGroup;->routeUpdated()V

    #@6c
    .line 1569
    invoke-static {p1, p0, p2}, Landroid/media/MediaRouter;->dispatchRouteGrouped(Landroid/media/MediaRouter$RouteInfo;Landroid/media/MediaRouter$RouteGroup;I)V

    #@6f
    .line 1570
    return-void
.end method

.method getName(Landroid/content/res/Resources;)Ljava/lang/CharSequence;
    .registers 3
    .parameter "res"

    #@0
    .prologue
    .line 1520
    iget-boolean v0, p0, Landroid/media/MediaRouter$RouteGroup;->mUpdateName:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteGroup;->updateName()V

    #@7
    .line 1521
    :cond_7
    invoke-super {p0, p1}, Landroid/media/MediaRouter$RouteInfo;->getName(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public getRouteAt(I)Landroid/media/MediaRouter$RouteInfo;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 1618
    iget-object v0, p0, Landroid/media/MediaRouter$RouteGroup;->mRoutes:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    #@8
    return-object v0
.end method

.method public getRouteCount()I
    .registers 2

    #@0
    .prologue
    .line 1608
    iget-object v0, p0, Landroid/media/MediaRouter$RouteGroup;->mRoutes:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method memberNameChanged(Landroid/media/MediaRouter$RouteInfo;Ljava/lang/CharSequence;)V
    .registers 4
    .parameter "info"
    .parameter "name"

    #@0
    .prologue
    .line 1685
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/media/MediaRouter$RouteGroup;->mUpdateName:Z

    #@3
    .line 1686
    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteGroup;->routeUpdated()V

    #@6
    .line 1687
    return-void
.end method

.method memberStatusChanged(Landroid/media/MediaRouter$RouteInfo;Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "info"
    .parameter "status"

    #@0
    .prologue
    .line 1690
    invoke-virtual {p0, p2}, Landroid/media/MediaRouter$RouteGroup;->setStatusInt(Ljava/lang/CharSequence;)V

    #@3
    .line 1691
    return-void
.end method

.method memberVolumeChanged(Landroid/media/MediaRouter$RouteInfo;)V
    .registers 2
    .parameter "info"

    #@0
    .prologue
    .line 1694
    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteGroup;->updateVolume()V

    #@3
    .line 1695
    return-void
.end method

.method public removeRoute(I)V
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 1596
    iget-object v1, p0, Landroid/media/MediaRouter$RouteGroup;->mRoutes:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    #@8
    .line 1597
    .local v0, route:Landroid/media/MediaRouter$RouteInfo;
    const/4 v1, 0x0

    #@9
    iput-object v1, v0, Landroid/media/MediaRouter$RouteInfo;->mGroup:Landroid/media/MediaRouter$RouteGroup;

    #@b
    .line 1598
    const/4 v1, 0x1

    #@c
    iput-boolean v1, p0, Landroid/media/MediaRouter$RouteGroup;->mUpdateName:Z

    #@e
    .line 1599
    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteGroup;->updateVolume()V

    #@11
    .line 1600
    invoke-static {v0, p0}, Landroid/media/MediaRouter;->dispatchRouteUngrouped(Landroid/media/MediaRouter$RouteInfo;Landroid/media/MediaRouter$RouteGroup;)V

    #@14
    .line 1601
    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteGroup;->routeUpdated()V

    #@17
    .line 1602
    return-void
.end method

.method public removeRoute(Landroid/media/MediaRouter$RouteInfo;)V
    .registers 5
    .parameter "route"

    #@0
    .prologue
    .line 1578
    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getGroup()Landroid/media/MediaRouter$RouteGroup;

    #@3
    move-result-object v0

    #@4
    if-eq v0, p0, :cond_25

    #@6
    .line 1579
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Route "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, " is not a member of this group."

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@24
    throw v0

    #@25
    .line 1582
    :cond_25
    iget-object v0, p0, Landroid/media/MediaRouter$RouteGroup;->mRoutes:Ljava/util/ArrayList;

    #@27
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@2a
    .line 1583
    const/4 v0, 0x0

    #@2b
    iput-object v0, p1, Landroid/media/MediaRouter$RouteInfo;->mGroup:Landroid/media/MediaRouter$RouteGroup;

    #@2d
    .line 1584
    const/4 v0, 0x1

    #@2e
    iput-boolean v0, p0, Landroid/media/MediaRouter$RouteGroup;->mUpdateName:Z

    #@30
    .line 1585
    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteGroup;->updateVolume()V

    #@33
    .line 1586
    invoke-static {p1, p0}, Landroid/media/MediaRouter;->dispatchRouteUngrouped(Landroid/media/MediaRouter$RouteInfo;Landroid/media/MediaRouter$RouteGroup;)V

    #@36
    .line 1587
    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteGroup;->routeUpdated()V

    #@39
    .line 1588
    return-void
.end method

.method public requestSetVolume(I)V
    .registers 10
    .parameter "volume"

    #@0
    .prologue
    .line 1643
    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteGroup;->getVolumeMax()I

    #@3
    move-result v1

    #@4
    .line 1644
    .local v1, maxVol:I
    if-nez v1, :cond_7

    #@6
    .line 1659
    :cond_6
    :goto_6
    return-void

    #@7
    .line 1648
    :cond_7
    int-to-float v6, p1

    #@8
    int-to-float v7, v1

    #@9
    div-float v5, v6, v7

    #@b
    .line 1649
    .local v5, scaledVolume:F
    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteGroup;->getRouteCount()I

    #@e
    move-result v3

    #@f
    .line 1650
    .local v3, routeCount:I
    const/4 v0, 0x0

    #@10
    .local v0, i:I
    :goto_10
    if-ge v0, v3, :cond_23

    #@12
    .line 1651
    invoke-virtual {p0, v0}, Landroid/media/MediaRouter$RouteGroup;->getRouteAt(I)Landroid/media/MediaRouter$RouteInfo;

    #@15
    move-result-object v2

    #@16
    .line 1652
    .local v2, route:Landroid/media/MediaRouter$RouteInfo;
    invoke-virtual {v2}, Landroid/media/MediaRouter$RouteInfo;->getVolumeMax()I

    #@19
    move-result v6

    #@1a
    int-to-float v6, v6

    #@1b
    mul-float/2addr v6, v5

    #@1c
    float-to-int v4, v6

    #@1d
    .line 1653
    .local v4, routeVol:I
    invoke-virtual {v2, v4}, Landroid/media/MediaRouter$RouteInfo;->requestSetVolume(I)V

    #@20
    .line 1650
    add-int/lit8 v0, v0, 0x1

    #@22
    goto :goto_10

    #@23
    .line 1655
    .end local v2           #route:Landroid/media/MediaRouter$RouteInfo;
    .end local v4           #routeVol:I
    :cond_23
    iget v6, p0, Landroid/media/MediaRouter$RouteInfo;->mVolume:I

    #@25
    if-eq p1, v6, :cond_6

    #@27
    .line 1656
    iput p1, p0, Landroid/media/MediaRouter$RouteInfo;->mVolume:I

    #@29
    .line 1657
    invoke-static {p0}, Landroid/media/MediaRouter;->dispatchRouteVolumeChanged(Landroid/media/MediaRouter$RouteInfo;)V

    #@2c
    goto :goto_6
.end method

.method public requestUpdateVolume(I)V
    .registers 9
    .parameter "direction"

    #@0
    .prologue
    .line 1663
    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteGroup;->getVolumeMax()I

    #@3
    move-result v1

    #@4
    .line 1664
    .local v1, maxVol:I
    if-nez v1, :cond_7

    #@6
    .line 1682
    :cond_6
    :goto_6
    return-void

    #@7
    .line 1668
    :cond_7
    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteGroup;->getRouteCount()I

    #@a
    move-result v3

    #@b
    .line 1669
    .local v3, routeCount:I
    const/4 v5, 0x0

    #@c
    .line 1670
    .local v5, volume:I
    const/4 v0, 0x0

    #@d
    .local v0, i:I
    :goto_d
    if-ge v0, v3, :cond_20

    #@f
    .line 1671
    invoke-virtual {p0, v0}, Landroid/media/MediaRouter$RouteGroup;->getRouteAt(I)Landroid/media/MediaRouter$RouteInfo;

    #@12
    move-result-object v2

    #@13
    .line 1672
    .local v2, route:Landroid/media/MediaRouter$RouteInfo;
    invoke-virtual {v2, p1}, Landroid/media/MediaRouter$RouteInfo;->requestUpdateVolume(I)V

    #@16
    .line 1673
    invoke-virtual {v2}, Landroid/media/MediaRouter$RouteInfo;->getVolume()I

    #@19
    move-result v4

    #@1a
    .line 1674
    .local v4, routeVol:I
    if-le v4, v5, :cond_1d

    #@1c
    .line 1675
    move v5, v4

    #@1d
    .line 1670
    :cond_1d
    add-int/lit8 v0, v0, 0x1

    #@1f
    goto :goto_d

    #@20
    .line 1678
    .end local v2           #route:Landroid/media/MediaRouter$RouteInfo;
    .end local v4           #routeVol:I
    :cond_20
    iget v6, p0, Landroid/media/MediaRouter$RouteInfo;->mVolume:I

    #@22
    if-eq v5, v6, :cond_6

    #@24
    .line 1679
    iput v5, p0, Landroid/media/MediaRouter$RouteInfo;->mVolume:I

    #@26
    .line 1680
    invoke-static {p0}, Landroid/media/MediaRouter;->dispatchRouteVolumeChanged(Landroid/media/MediaRouter$RouteInfo;)V

    #@29
    goto :goto_6
.end method

.method routeUpdated()V
    .registers 12

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v10, 0x0

    #@2
    .line 1715
    const/4 v7, 0x0

    #@3
    .line 1716
    .local v7, types:I
    iget-object v8, p0, Landroid/media/MediaRouter$RouteGroup;->mRoutes:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v0

    #@9
    .line 1717
    .local v0, count:I
    if-nez v0, :cond_f

    #@b
    .line 1719
    invoke-static {p0}, Landroid/media/MediaRouter;->removeRoute(Landroid/media/MediaRouter$RouteInfo;)V

    #@e
    .line 1742
    :goto_e
    return-void

    #@f
    .line 1723
    :cond_f
    const/4 v4, 0x0

    #@10
    .line 1724
    .local v4, maxVolume:I
    const/4 v3, 0x1

    #@11
    .line 1725
    .local v3, isLocal:Z
    const/4 v2, 0x1

    #@12
    .line 1726
    .local v2, isFixedVolume:Z
    const/4 v1, 0x0

    #@13
    .local v1, i:I
    :goto_13
    if-ge v1, v0, :cond_3e

    #@15
    .line 1727
    iget-object v8, p0, Landroid/media/MediaRouter$RouteGroup;->mRoutes:Ljava/util/ArrayList;

    #@17
    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1a
    move-result-object v5

    #@1b
    check-cast v5, Landroid/media/MediaRouter$RouteInfo;

    #@1d
    .line 1728
    .local v5, route:Landroid/media/MediaRouter$RouteInfo;
    iget v8, v5, Landroid/media/MediaRouter$RouteInfo;->mSupportedTypes:I

    #@1f
    or-int/2addr v7, v8

    #@20
    .line 1729
    invoke-virtual {v5}, Landroid/media/MediaRouter$RouteInfo;->getVolumeMax()I

    #@23
    move-result v6

    #@24
    .line 1730
    .local v6, routeMaxVolume:I
    if-le v6, v4, :cond_27

    #@26
    .line 1731
    move v4, v6

    #@27
    .line 1733
    :cond_27
    invoke-virtual {v5}, Landroid/media/MediaRouter$RouteInfo;->getPlaybackType()I

    #@2a
    move-result v8

    #@2b
    if-nez v8, :cond_3a

    #@2d
    move v8, v9

    #@2e
    :goto_2e
    and-int/2addr v3, v8

    #@2f
    .line 1734
    invoke-virtual {v5}, Landroid/media/MediaRouter$RouteInfo;->getVolumeHandling()I

    #@32
    move-result v8

    #@33
    if-nez v8, :cond_3c

    #@35
    move v8, v9

    #@36
    :goto_36
    and-int/2addr v2, v8

    #@37
    .line 1726
    add-int/lit8 v1, v1, 0x1

    #@39
    goto :goto_13

    #@3a
    :cond_3a
    move v8, v10

    #@3b
    .line 1733
    goto :goto_2e

    #@3c
    :cond_3c
    move v8, v10

    #@3d
    .line 1734
    goto :goto_36

    #@3e
    .line 1736
    .end local v5           #route:Landroid/media/MediaRouter$RouteInfo;
    .end local v6           #routeMaxVolume:I
    :cond_3e
    if-eqz v3, :cond_60

    #@40
    move v8, v10

    #@41
    :goto_41
    iput v8, p0, Landroid/media/MediaRouter$RouteInfo;->mPlaybackType:I

    #@43
    .line 1737
    if-eqz v2, :cond_62

    #@45
    move v8, v10

    #@46
    :goto_46
    iput v8, p0, Landroid/media/MediaRouter$RouteInfo;->mVolumeHandling:I

    #@48
    .line 1738
    iput v7, p0, Landroid/media/MediaRouter$RouteInfo;->mSupportedTypes:I

    #@4a
    .line 1739
    iput v4, p0, Landroid/media/MediaRouter$RouteInfo;->mVolumeMax:I

    #@4c
    .line 1740
    if-ne v0, v9, :cond_64

    #@4e
    iget-object v8, p0, Landroid/media/MediaRouter$RouteGroup;->mRoutes:Ljava/util/ArrayList;

    #@50
    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@53
    move-result-object v8

    #@54
    check-cast v8, Landroid/media/MediaRouter$RouteInfo;

    #@56
    invoke-virtual {v8}, Landroid/media/MediaRouter$RouteInfo;->getIconDrawable()Landroid/graphics/drawable/Drawable;

    #@59
    move-result-object v8

    #@5a
    :goto_5a
    iput-object v8, p0, Landroid/media/MediaRouter$RouteInfo;->mIcon:Landroid/graphics/drawable/Drawable;

    #@5c
    .line 1741
    invoke-super {p0}, Landroid/media/MediaRouter$RouteInfo;->routeUpdated()V

    #@5f
    goto :goto_e

    #@60
    :cond_60
    move v8, v9

    #@61
    .line 1736
    goto :goto_41

    #@62
    :cond_62
    move v8, v9

    #@63
    .line 1737
    goto :goto_46

    #@64
    .line 1740
    :cond_64
    const/4 v8, 0x0

    #@65
    goto :goto_5a
.end method

.method public setIconDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 2
    .parameter "icon"

    #@0
    .prologue
    .line 1628
    iput-object p1, p0, Landroid/media/MediaRouter$RouteInfo;->mIcon:Landroid/graphics/drawable/Drawable;

    #@2
    .line 1629
    return-void
.end method

.method public setIconResource(I)V
    .registers 3
    .parameter "resId"

    #@0
    .prologue
    .line 1638
    sget-object v0, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    #@2
    iget-object v0, v0, Landroid/media/MediaRouter$Static;->mResources:Landroid/content/res/Resources;

    #@4
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {p0, v0}, Landroid/media/MediaRouter$RouteGroup;->setIconDrawable(Landroid/graphics/drawable/Drawable;)V

    #@b
    .line 1639
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 1759
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-super {p0}, Landroid/media/MediaRouter$RouteInfo;->toString()Ljava/lang/String;

    #@5
    move-result-object v3

    #@6
    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@9
    .line 1760
    .local v2, sb:Ljava/lang/StringBuilder;
    const/16 v3, 0x5b

    #@b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@e
    .line 1761
    iget-object v3, p0, Landroid/media/MediaRouter$RouteGroup;->mRoutes:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@13
    move-result v0

    #@14
    .line 1762
    .local v0, count:I
    const/4 v1, 0x0

    #@15
    .local v1, i:I
    :goto_15
    if-ge v1, v0, :cond_2a

    #@17
    .line 1763
    if-lez v1, :cond_1e

    #@19
    const-string v3, ", "

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    .line 1764
    :cond_1e
    iget-object v3, p0, Landroid/media/MediaRouter$RouteGroup;->mRoutes:Ljava/util/ArrayList;

    #@20
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@27
    .line 1762
    add-int/lit8 v1, v1, 0x1

    #@29
    goto :goto_15

    #@2a
    .line 1766
    :cond_2a
    const/16 v3, 0x5d

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2f
    .line 1767
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v3

    #@33
    return-object v3
.end method

.method updateName()V
    .registers 6

    #@0
    .prologue
    .line 1745
    new-instance v3, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 1746
    .local v3, sb:Ljava/lang/StringBuilder;
    iget-object v4, p0, Landroid/media/MediaRouter$RouteGroup;->mRoutes:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v0

    #@b
    .line 1747
    .local v0, count:I
    const/4 v1, 0x0

    #@c
    .local v1, i:I
    :goto_c
    if-ge v1, v0, :cond_25

    #@e
    .line 1748
    iget-object v4, p0, Landroid/media/MediaRouter$RouteGroup;->mRoutes:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v2

    #@14
    check-cast v2, Landroid/media/MediaRouter$RouteInfo;

    #@16
    .line 1750
    .local v2, info:Landroid/media/MediaRouter$RouteInfo;
    if-lez v1, :cond_1d

    #@18
    const-string v4, ", "

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    .line 1751
    :cond_1d
    iget-object v4, v2, Landroid/media/MediaRouter$RouteInfo;->mName:Ljava/lang/CharSequence;

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@22
    .line 1747
    add-int/lit8 v1, v1, 0x1

    #@24
    goto :goto_c

    #@25
    .line 1753
    .end local v2           #info:Landroid/media/MediaRouter$RouteInfo;
    :cond_25
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v4

    #@29
    iput-object v4, p0, Landroid/media/MediaRouter$RouteInfo;->mName:Ljava/lang/CharSequence;

    #@2b
    .line 1754
    const/4 v4, 0x0

    #@2c
    iput-boolean v4, p0, Landroid/media/MediaRouter$RouteGroup;->mUpdateName:Z

    #@2e
    .line 1755
    return-void
.end method

.method updateVolume()V
    .registers 6

    #@0
    .prologue
    .line 1699
    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteGroup;->getRouteCount()I

    #@3
    move-result v1

    #@4
    .line 1700
    .local v1, routeCount:I
    const/4 v3, 0x0

    #@5
    .line 1701
    .local v3, volume:I
    const/4 v0, 0x0

    #@6
    .local v0, i:I
    :goto_6
    if-ge v0, v1, :cond_16

    #@8
    .line 1702
    invoke-virtual {p0, v0}, Landroid/media/MediaRouter$RouteGroup;->getRouteAt(I)Landroid/media/MediaRouter$RouteInfo;

    #@b
    move-result-object v4

    #@c
    invoke-virtual {v4}, Landroid/media/MediaRouter$RouteInfo;->getVolume()I

    #@f
    move-result v2

    #@10
    .line 1703
    .local v2, routeVol:I
    if-le v2, v3, :cond_13

    #@12
    .line 1704
    move v3, v2

    #@13
    .line 1701
    :cond_13
    add-int/lit8 v0, v0, 0x1

    #@15
    goto :goto_6

    #@16
    .line 1707
    .end local v2           #routeVol:I
    :cond_16
    iget v4, p0, Landroid/media/MediaRouter$RouteInfo;->mVolume:I

    #@18
    if-eq v3, v4, :cond_1f

    #@1a
    .line 1708
    iput v3, p0, Landroid/media/MediaRouter$RouteInfo;->mVolume:I

    #@1c
    .line 1709
    invoke-static {p0}, Landroid/media/MediaRouter;->dispatchRouteVolumeChanged(Landroid/media/MediaRouter$RouteInfo;)V

    #@1f
    .line 1711
    :cond_1f
    return-void
.end method
