.class public final Landroid/media/ResampleInputStream;
.super Ljava/io/InputStream;
.source "ResampleInputStream.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ResampleInputStream"

.field private static final mFirLength:I = 0x1d


# instance fields
.field private mBuf:[B

.field private mBufCount:I

.field private mInputStream:Ljava/io/InputStream;

.field private final mOneByte:[B

.field private final mRateIn:I

.field private final mRateOut:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 32
    const-string/jumbo v0, "media_jni"

    #@3
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@6
    .line 33
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;II)V
    .registers 6
    .parameter "inputStream"
    .parameter "rateIn"
    .parameter "rateOut"

    #@0
    .prologue
    .line 61
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    #@3
    .line 52
    const/4 v0, 0x1

    #@4
    new-array v0, v0, [B

    #@6
    iput-object v0, p0, Landroid/media/ResampleInputStream;->mOneByte:[B

    #@8
    .line 63
    mul-int/lit8 v0, p3, 0x2

    #@a
    if-eq p2, v0, :cond_15

    #@c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@e
    const-string/jumbo v1, "only support 2:1 at the moment"

    #@11
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@14
    throw v0

    #@15
    .line 64
    :cond_15
    const/4 p2, 0x2

    #@16
    .line 65
    const/4 p3, 0x1

    #@17
    .line 67
    iput-object p1, p0, Landroid/media/ResampleInputStream;->mInputStream:Ljava/io/InputStream;

    #@19
    .line 68
    iput p2, p0, Landroid/media/ResampleInputStream;->mRateIn:I

    #@1b
    .line 69
    iput p3, p0, Landroid/media/ResampleInputStream;->mRateOut:I

    #@1d
    .line 70
    return-void
.end method

.method private static native fir21([BI[BII)V
.end method


# virtual methods
.method public close()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 132
    :try_start_1
    iget-object v0, p0, Landroid/media/ResampleInputStream;->mInputStream:Ljava/io/InputStream;

    #@3
    if-eqz v0, :cond_a

    #@5
    iget-object v0, p0, Landroid/media/ResampleInputStream;->mInputStream:Ljava/io/InputStream;

    #@7
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_d

    #@a
    .line 134
    :cond_a
    iput-object v1, p0, Landroid/media/ResampleInputStream;->mInputStream:Ljava/io/InputStream;

    #@c
    .line 136
    return-void

    #@d
    .line 134
    :catchall_d
    move-exception v0

    #@e
    iput-object v1, p0, Landroid/media/ResampleInputStream;->mInputStream:Ljava/io/InputStream;

    #@10
    throw v0
.end method

.method protected finalize()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 140
    iget-object v0, p0, Landroid/media/ResampleInputStream;->mInputStream:Ljava/io/InputStream;

    #@2
    if-eqz v0, :cond_10

    #@4
    .line 141
    invoke-virtual {p0}, Landroid/media/ResampleInputStream;->close()V

    #@7
    .line 142
    new-instance v0, Ljava/lang/IllegalStateException;

    #@9
    const-string/jumbo v1, "someone forgot to close ResampleInputStream"

    #@c
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 144
    :cond_10
    return-void
.end method

.method public read()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 74
    iget-object v1, p0, Landroid/media/ResampleInputStream;->mOneByte:[B

    #@4
    invoke-virtual {p0, v1, v2, v3}, Landroid/media/ResampleInputStream;->read([BII)I

    #@7
    move-result v0

    #@8
    .line 75
    .local v0, rtn:I
    if-ne v0, v3, :cond_11

    #@a
    iget-object v1, p0, Landroid/media/ResampleInputStream;->mOneByte:[B

    #@c
    aget-byte v1, v1, v2

    #@e
    and-int/lit16 v1, v1, 0xff

    #@10
    :goto_10
    return v1

    #@11
    :cond_11
    const/4 v1, -0x1

    #@12
    goto :goto_10
.end method

.method public read([B)I
    .registers 4
    .parameter "b"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 80
    const/4 v0, 0x0

    #@1
    array-length v1, p1

    #@2
    invoke-virtual {p0, p1, v0, v1}, Landroid/media/ResampleInputStream;->read([BII)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public read([BII)I
    .registers 16
    .parameter "b"
    .parameter "offset"
    .parameter "length"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, -0x1

    #@1
    const/4 v11, 0x0

    #@2
    .line 85
    iget-object v6, p0, Landroid/media/ResampleInputStream;->mInputStream:Ljava/io/InputStream;

    #@4
    if-nez v6, :cond_f

    #@6
    new-instance v5, Ljava/lang/IllegalStateException;

    #@8
    const-string/jumbo v6, "not open"

    #@b
    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@e
    throw v5

    #@f
    .line 88
    :cond_f
    div-int/lit8 v6, p3, 0x2

    #@11
    iget v7, p0, Landroid/media/ResampleInputStream;->mRateIn:I

    #@13
    mul-int/2addr v6, v7

    #@14
    iget v7, p0, Landroid/media/ResampleInputStream;->mRateOut:I

    #@16
    div-int/2addr v6, v7

    #@17
    add-int/lit8 v6, v6, 0x1d

    #@19
    mul-int/lit8 v4, v6, 0x2

    #@1b
    .line 89
    .local v4, nIn:I
    iget-object v6, p0, Landroid/media/ResampleInputStream;->mBuf:[B

    #@1d
    if-nez v6, :cond_58

    #@1f
    .line 90
    new-array v6, v4, [B

    #@21
    iput-object v6, p0, Landroid/media/ResampleInputStream;->mBuf:[B

    #@23
    .line 99
    :cond_23
    :goto_23
    iget v6, p0, Landroid/media/ResampleInputStream;->mBufCount:I

    #@25
    div-int/lit8 v6, v6, 0x2

    #@27
    add-int/lit8 v6, v6, -0x1d

    #@29
    iget v7, p0, Landroid/media/ResampleInputStream;->mRateOut:I

    #@2b
    mul-int/2addr v6, v7

    #@2c
    iget v7, p0, Landroid/media/ResampleInputStream;->mRateIn:I

    #@2e
    div-int/2addr v6, v7

    #@2f
    mul-int/lit8 v1, v6, 0x2

    #@31
    .line 100
    .local v1, len:I
    if-lez v1, :cond_6e

    #@33
    .line 101
    if-ge v1, p3, :cond_69

    #@35
    move p3, v1

    #@36
    .line 111
    :goto_36
    iget-object v5, p0, Landroid/media/ResampleInputStream;->mBuf:[B

    #@38
    div-int/lit8 v6, p3, 0x2

    #@3a
    invoke-static {v5, v11, p1, p2, v6}, Landroid/media/ResampleInputStream;->fir21([BI[BII)V

    #@3d
    .line 114
    iget v5, p0, Landroid/media/ResampleInputStream;->mRateIn:I

    #@3f
    mul-int/2addr v5, p3

    #@40
    iget v6, p0, Landroid/media/ResampleInputStream;->mRateOut:I

    #@42
    div-int v3, v5, v6

    #@44
    .line 115
    .local v3, nFwd:I
    iget v5, p0, Landroid/media/ResampleInputStream;->mBufCount:I

    #@46
    sub-int/2addr v5, v3

    #@47
    iput v5, p0, Landroid/media/ResampleInputStream;->mBufCount:I

    #@49
    .line 116
    iget v5, p0, Landroid/media/ResampleInputStream;->mBufCount:I

    #@4b
    if-lez v5, :cond_56

    #@4d
    iget-object v5, p0, Landroid/media/ResampleInputStream;->mBuf:[B

    #@4f
    iget-object v6, p0, Landroid/media/ResampleInputStream;->mBuf:[B

    #@51
    iget v7, p0, Landroid/media/ResampleInputStream;->mBufCount:I

    #@53
    invoke-static {v5, v3, v6, v11, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@56
    :cond_56
    move v5, p3

    #@57
    .line 118
    .end local v3           #nFwd:I
    :cond_57
    return v5

    #@58
    .line 91
    .end local v1           #len:I
    :cond_58
    iget-object v6, p0, Landroid/media/ResampleInputStream;->mBuf:[B

    #@5a
    array-length v6, v6

    #@5b
    if-le v4, v6, :cond_23

    #@5d
    .line 92
    new-array v0, v4, [B

    #@5f
    .line 93
    .local v0, bf:[B
    iget-object v6, p0, Landroid/media/ResampleInputStream;->mBuf:[B

    #@61
    iget v7, p0, Landroid/media/ResampleInputStream;->mBufCount:I

    #@63
    invoke-static {v6, v11, v0, v11, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@66
    .line 94
    iput-object v0, p0, Landroid/media/ResampleInputStream;->mBuf:[B

    #@68
    goto :goto_23

    #@69
    .line 101
    .end local v0           #bf:[B
    .restart local v1       #len:I
    :cond_69
    div-int/lit8 v5, p3, 0x2

    #@6b
    mul-int/lit8 p3, v5, 0x2

    #@6d
    goto :goto_36

    #@6e
    .line 105
    :cond_6e
    iget-object v6, p0, Landroid/media/ResampleInputStream;->mInputStream:Ljava/io/InputStream;

    #@70
    iget-object v7, p0, Landroid/media/ResampleInputStream;->mBuf:[B

    #@72
    iget v8, p0, Landroid/media/ResampleInputStream;->mBufCount:I

    #@74
    iget-object v9, p0, Landroid/media/ResampleInputStream;->mBuf:[B

    #@76
    array-length v9, v9

    #@77
    iget v10, p0, Landroid/media/ResampleInputStream;->mBufCount:I

    #@79
    sub-int/2addr v9, v10

    #@7a
    invoke-virtual {v6, v7, v8, v9}, Ljava/io/InputStream;->read([BII)I

    #@7d
    move-result v2

    #@7e
    .line 106
    .local v2, n:I
    if-eq v2, v5, :cond_57

    #@80
    .line 107
    iget v6, p0, Landroid/media/ResampleInputStream;->mBufCount:I

    #@82
    add-int/2addr v6, v2

    #@83
    iput v6, p0, Landroid/media/ResampleInputStream;->mBufCount:I

    #@85
    goto :goto_23
.end method
