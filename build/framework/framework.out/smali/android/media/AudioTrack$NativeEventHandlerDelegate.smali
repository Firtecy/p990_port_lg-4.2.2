.class Landroid/media/AudioTrack$NativeEventHandlerDelegate;
.super Ljava/lang/Object;
.source "AudioTrack.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/AudioTrack;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NativeEventHandlerDelegate"
.end annotation


# instance fields
.field private final mAudioTrack:Landroid/media/AudioTrack;

.field private final mHandler:Landroid/os/Handler;

.field final synthetic this$0:Landroid/media/AudioTrack;


# direct methods
.method constructor <init>(Landroid/media/AudioTrack;Landroid/media/AudioTrack;Landroid/os/Handler;)V
    .registers 6
    .parameter
    .parameter "track"
    .parameter "handler"

    #@0
    .prologue
    .line 1141
    iput-object p1, p0, Landroid/media/AudioTrack$NativeEventHandlerDelegate;->this$0:Landroid/media/AudioTrack;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 1142
    iput-object p2, p0, Landroid/media/AudioTrack$NativeEventHandlerDelegate;->mAudioTrack:Landroid/media/AudioTrack;

    #@7
    .line 1145
    if-eqz p3, :cond_17

    #@9
    .line 1146
    invoke-virtual {p3}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@c
    move-result-object v0

    #@d
    .line 1153
    .local v0, looper:Landroid/os/Looper;
    :goto_d
    if-eqz v0, :cond_1c

    #@f
    .line 1155
    new-instance v1, Landroid/media/AudioTrack$NativeEventHandlerDelegate$1;

    #@11
    invoke-direct {v1, p0, v0, p1}, Landroid/media/AudioTrack$NativeEventHandlerDelegate$1;-><init>(Landroid/media/AudioTrack$NativeEventHandlerDelegate;Landroid/os/Looper;Landroid/media/AudioTrack;)V

    #@14
    iput-object v1, p0, Landroid/media/AudioTrack$NativeEventHandlerDelegate;->mHandler:Landroid/os/Handler;

    #@16
    .line 1186
    :goto_16
    return-void

    #@17
    .line 1149
    .end local v0           #looper:Landroid/os/Looper;
    :cond_17
    invoke-static {p1}, Landroid/media/AudioTrack;->access$000(Landroid/media/AudioTrack;)Landroid/os/Looper;

    #@1a
    move-result-object v0

    #@1b
    .restart local v0       #looper:Landroid/os/Looper;
    goto :goto_d

    #@1c
    .line 1184
    :cond_1c
    const/4 v1, 0x0

    #@1d
    iput-object v1, p0, Landroid/media/AudioTrack$NativeEventHandlerDelegate;->mHandler:Landroid/os/Handler;

    #@1f
    goto :goto_16
.end method

.method static synthetic access$100(Landroid/media/AudioTrack$NativeEventHandlerDelegate;)Landroid/media/AudioTrack;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1137
    iget-object v0, p0, Landroid/media/AudioTrack$NativeEventHandlerDelegate;->mAudioTrack:Landroid/media/AudioTrack;

    #@2
    return-object v0
.end method


# virtual methods
.method getHandler()Landroid/os/Handler;
    .registers 2

    #@0
    .prologue
    .line 1189
    iget-object v0, p0, Landroid/media/AudioTrack$NativeEventHandlerDelegate;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method
