.class Landroid/media/MediaScanner$WplHandler;
.super Ljava/lang/Object;
.source "MediaScanner.java"

# interfaces
.implements Landroid/sax/ElementListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/MediaScanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "WplHandler"
.end annotation


# instance fields
.field final handler:Lorg/xml/sax/ContentHandler;

.field playListDirectory:Ljava/lang/String;

.field final synthetic this$0:Landroid/media/MediaScanner;


# direct methods
.method public constructor <init>(Landroid/media/MediaScanner;Ljava/lang/String;Landroid/net/Uri;Landroid/database/Cursor;)V
    .registers 10
    .parameter
    .parameter "playListDirectory"
    .parameter "uri"
    .parameter "fileList"

    #@0
    .prologue
    .line 2142
    iput-object p1, p0, Landroid/media/MediaScanner$WplHandler;->this$0:Landroid/media/MediaScanner;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 2143
    iput-object p2, p0, Landroid/media/MediaScanner$WplHandler;->playListDirectory:Ljava/lang/String;

    #@7
    .line 2145
    new-instance v2, Landroid/sax/RootElement;

    #@9
    const-string/jumbo v4, "smil"

    #@c
    invoke-direct {v2, v4}, Landroid/sax/RootElement;-><init>(Ljava/lang/String;)V

    #@f
    .line 2146
    .local v2, root:Landroid/sax/RootElement;
    const-string v4, "body"

    #@11
    invoke-virtual {v2, v4}, Landroid/sax/RootElement;->getChild(Ljava/lang/String;)Landroid/sax/Element;

    #@14
    move-result-object v0

    #@15
    .line 2147
    .local v0, body:Landroid/sax/Element;
    const-string/jumbo v4, "seq"

    #@18
    invoke-virtual {v0, v4}, Landroid/sax/Element;->getChild(Ljava/lang/String;)Landroid/sax/Element;

    #@1b
    move-result-object v3

    #@1c
    .line 2148
    .local v3, seq:Landroid/sax/Element;
    const-string/jumbo v4, "media"

    #@1f
    invoke-virtual {v3, v4}, Landroid/sax/Element;->getChild(Ljava/lang/String;)Landroid/sax/Element;

    #@22
    move-result-object v1

    #@23
    .line 2149
    .local v1, media:Landroid/sax/Element;
    invoke-virtual {v1, p0}, Landroid/sax/Element;->setElementListener(Landroid/sax/ElementListener;)V

    #@26
    .line 2151
    invoke-virtual {v2}, Landroid/sax/RootElement;->getContentHandler()Lorg/xml/sax/ContentHandler;

    #@29
    move-result-object v4

    #@2a
    iput-object v4, p0, Landroid/media/MediaScanner$WplHandler;->handler:Lorg/xml/sax/ContentHandler;

    #@2c
    .line 2152
    return-void
.end method


# virtual methods
.method public end()V
    .registers 1

    #@0
    .prologue
    .line 2164
    return-void
.end method

.method getContentHandler()Lorg/xml/sax/ContentHandler;
    .registers 2

    #@0
    .prologue
    .line 2167
    iget-object v0, p0, Landroid/media/MediaScanner$WplHandler;->handler:Lorg/xml/sax/ContentHandler;

    #@2
    return-object v0
.end method

.method public start(Lorg/xml/sax/Attributes;)V
    .registers 5
    .parameter "attributes"

    #@0
    .prologue
    .line 2156
    const-string v1, ""

    #@2
    const-string/jumbo v2, "src"

    #@5
    invoke-interface {p1, v1, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    .line 2157
    .local v0, path:Ljava/lang/String;
    if-eqz v0, :cond_12

    #@b
    .line 2158
    iget-object v1, p0, Landroid/media/MediaScanner$WplHandler;->this$0:Landroid/media/MediaScanner;

    #@d
    iget-object v2, p0, Landroid/media/MediaScanner$WplHandler;->playListDirectory:Ljava/lang/String;

    #@f
    invoke-static {v1, v0, v2}, Landroid/media/MediaScanner;->access$3000(Landroid/media/MediaScanner;Ljava/lang/String;Ljava/lang/String;)V

    #@12
    .line 2160
    :cond_12
    return-void
.end method
