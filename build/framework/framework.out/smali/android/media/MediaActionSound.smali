.class public Landroid/media/MediaActionSound;
.super Ljava/lang/Object;
.source "MediaActionSound.java"


# static fields
.field public static final FOCUS_COMPLETE:I = 0x1

.field private static final NUM_MEDIA_SOUND_STREAMS:I = 0x1

.field public static final SHUTTER_CLICK:I = 0x0

.field private static final SOUND_FILES:[Ljava/lang/String; = null

.field private static final SOUND_NOT_LOADED:I = -0x1

.field public static final START_VIDEO_RECORDING:I = 0x2

.field public static final STOP_VIDEO_RECORDING:I = 0x3

.field private static final TAG:Ljava/lang/String; = "MediaActionSound"


# instance fields
.field private mLoadCompleteListener:Landroid/media/SoundPool$OnLoadCompleteListener;

.field private mSoundIdToPlay:I

.field private mSoundIds:[I

.field private mSoundPool:Landroid/media/SoundPool;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 49
    const/4 v0, 0x4

    #@1
    new-array v0, v0, [Ljava/lang/String;

    #@3
    const/4 v1, 0x0

    #@4
    const-string v2, "/system/media/audio/ui/camera_click.ogg"

    #@6
    aput-object v2, v0, v1

    #@8
    const/4 v1, 0x1

    #@9
    const-string v2, "/system/media/audio/ui/camera_focus.ogg"

    #@b
    aput-object v2, v0, v1

    #@d
    const/4 v1, 0x2

    #@e
    const-string v2, "/system/media/audio/ui/VideoRecord.ogg"

    #@10
    aput-object v2, v0, v1

    #@12
    const/4 v1, 0x3

    #@13
    const-string v2, "/system/media/audio/ui/VideoRecord.ogg"

    #@15
    aput-object v2, v0, v1

    #@17
    sput-object v0, Landroid/media/MediaActionSound;->SOUND_FILES:[Ljava/lang/String;

    #@19
    return-void
.end method

.method public constructor <init>()V
    .registers 7

    #@0
    .prologue
    const/4 v5, -0x1

    #@1
    .line 96
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 171
    new-instance v1, Landroid/media/MediaActionSound$1;

    #@6
    invoke-direct {v1, p0}, Landroid/media/MediaActionSound$1;-><init>(Landroid/media/MediaActionSound;)V

    #@9
    iput-object v1, p0, Landroid/media/MediaActionSound;->mLoadCompleteListener:Landroid/media/SoundPool$OnLoadCompleteListener;

    #@b
    .line 97
    new-instance v1, Landroid/media/SoundPool;

    #@d
    const/4 v2, 0x1

    #@e
    const/4 v3, 0x7

    #@f
    const/4 v4, 0x0

    #@10
    invoke-direct {v1, v2, v3, v4}, Landroid/media/SoundPool;-><init>(III)V

    #@13
    iput-object v1, p0, Landroid/media/MediaActionSound;->mSoundPool:Landroid/media/SoundPool;

    #@15
    .line 99
    iget-object v1, p0, Landroid/media/MediaActionSound;->mSoundPool:Landroid/media/SoundPool;

    #@17
    iget-object v2, p0, Landroid/media/MediaActionSound;->mLoadCompleteListener:Landroid/media/SoundPool$OnLoadCompleteListener;

    #@19
    invoke-virtual {v1, v2}, Landroid/media/SoundPool;->setOnLoadCompleteListener(Landroid/media/SoundPool$OnLoadCompleteListener;)V

    #@1c
    .line 100
    sget-object v1, Landroid/media/MediaActionSound;->SOUND_FILES:[Ljava/lang/String;

    #@1e
    array-length v1, v1

    #@1f
    new-array v1, v1, [I

    #@21
    iput-object v1, p0, Landroid/media/MediaActionSound;->mSoundIds:[I

    #@23
    .line 101
    const/4 v0, 0x0

    #@24
    .local v0, i:I
    :goto_24
    iget-object v1, p0, Landroid/media/MediaActionSound;->mSoundIds:[I

    #@26
    array-length v1, v1

    #@27
    if-ge v0, v1, :cond_30

    #@29
    .line 102
    iget-object v1, p0, Landroid/media/MediaActionSound;->mSoundIds:[I

    #@2b
    aput v5, v1, v0

    #@2d
    .line 101
    add-int/lit8 v0, v0, 0x1

    #@2f
    goto :goto_24

    #@30
    .line 104
    :cond_30
    iput v5, p0, Landroid/media/MediaActionSound;->mSoundIdToPlay:I

    #@32
    .line 105
    return-void
.end method

.method static synthetic access$000(Landroid/media/MediaActionSound;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 42
    iget v0, p0, Landroid/media/MediaActionSound;->mSoundIdToPlay:I

    #@2
    return v0
.end method

.method static synthetic access$002(Landroid/media/MediaActionSound;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 42
    iput p1, p0, Landroid/media/MediaActionSound;->mSoundIdToPlay:I

    #@2
    return p1
.end method


# virtual methods
.method public declared-synchronized load(I)V
    .registers 6
    .parameter "soundName"

    #@0
    .prologue
    .line 120
    monitor-enter p0

    #@1
    if-ltz p1, :cond_8

    #@3
    :try_start_3
    sget-object v0, Landroid/media/MediaActionSound;->SOUND_FILES:[Ljava/lang/String;

    #@5
    array-length v0, v0

    #@6
    if-lt p1, v0, :cond_24

    #@8
    .line 121
    :cond_8
    new-instance v0, Ljava/lang/RuntimeException;

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "Unknown sound requested: "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@20
    throw v0
    :try_end_21
    .catchall {:try_start_3 .. :try_end_21} :catchall_21

    #@21
    .line 120
    :catchall_21
    move-exception v0

    #@22
    monitor-exit p0

    #@23
    throw v0

    #@24
    .line 123
    :cond_24
    :try_start_24
    iget-object v0, p0, Landroid/media/MediaActionSound;->mSoundIds:[I

    #@26
    aget v0, v0, p1

    #@28
    const/4 v1, -0x1

    #@29
    if-ne v0, v1, :cond_3a

    #@2b
    .line 124
    iget-object v0, p0, Landroid/media/MediaActionSound;->mSoundIds:[I

    #@2d
    iget-object v1, p0, Landroid/media/MediaActionSound;->mSoundPool:Landroid/media/SoundPool;

    #@2f
    sget-object v2, Landroid/media/MediaActionSound;->SOUND_FILES:[Ljava/lang/String;

    #@31
    aget-object v2, v2, p1

    #@33
    const/4 v3, 0x1

    #@34
    invoke-virtual {v1, v2, v3}, Landroid/media/SoundPool;->load(Ljava/lang/String;I)I

    #@37
    move-result v1

    #@38
    aput v1, v0, p1
    :try_end_3a
    .catchall {:try_start_24 .. :try_end_3a} :catchall_21

    #@3a
    .line 127
    :cond_3a
    monitor-exit p0

    #@3b
    return-void
.end method

.method public declared-synchronized play(I)V
    .registers 9
    .parameter "soundName"

    #@0
    .prologue
    .line 159
    monitor-enter p0

    #@1
    if-ltz p1, :cond_8

    #@3
    :try_start_3
    sget-object v0, Landroid/media/MediaActionSound;->SOUND_FILES:[Ljava/lang/String;

    #@5
    array-length v0, v0

    #@6
    if-lt p1, v0, :cond_24

    #@8
    .line 160
    :cond_8
    new-instance v0, Ljava/lang/RuntimeException;

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "Unknown sound requested: "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@20
    throw v0
    :try_end_21
    .catchall {:try_start_3 .. :try_end_21} :catchall_21

    #@21
    .line 159
    :catchall_21
    move-exception v0

    #@22
    monitor-exit p0

    #@23
    throw v0

    #@24
    .line 162
    :cond_24
    :try_start_24
    iget-object v0, p0, Landroid/media/MediaActionSound;->mSoundIds:[I

    #@26
    aget v0, v0, p1

    #@28
    const/4 v1, -0x1

    #@29
    if-ne v0, v1, :cond_40

    #@2b
    .line 163
    iget-object v0, p0, Landroid/media/MediaActionSound;->mSoundPool:Landroid/media/SoundPool;

    #@2d
    sget-object v1, Landroid/media/MediaActionSound;->SOUND_FILES:[Ljava/lang/String;

    #@2f
    aget-object v1, v1, p1

    #@31
    const/4 v2, 0x1

    #@32
    invoke-virtual {v0, v1, v2}, Landroid/media/SoundPool;->load(Ljava/lang/String;I)I

    #@35
    move-result v0

    #@36
    iput v0, p0, Landroid/media/MediaActionSound;->mSoundIdToPlay:I

    #@38
    .line 165
    iget-object v0, p0, Landroid/media/MediaActionSound;->mSoundIds:[I

    #@3a
    iget v1, p0, Landroid/media/MediaActionSound;->mSoundIdToPlay:I

    #@3c
    aput v1, v0, p1
    :try_end_3e
    .catchall {:try_start_24 .. :try_end_3e} :catchall_21

    #@3e
    .line 169
    :goto_3e
    monitor-exit p0

    #@3f
    return-void

    #@40
    .line 167
    :cond_40
    :try_start_40
    iget-object v0, p0, Landroid/media/MediaActionSound;->mSoundPool:Landroid/media/SoundPool;

    #@42
    iget-object v1, p0, Landroid/media/MediaActionSound;->mSoundIds:[I

    #@44
    aget v1, v1, p1

    #@46
    const/high16 v2, 0x3f80

    #@48
    const/high16 v3, 0x3f80

    #@4a
    const/4 v4, 0x0

    #@4b
    const/4 v5, 0x0

    #@4c
    const/high16 v6, 0x3f80

    #@4e
    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I
    :try_end_51
    .catchall {:try_start_40 .. :try_end_51} :catchall_21

    #@51
    goto :goto_3e
.end method

.method public release()V
    .registers 2

    #@0
    .prologue
    .line 193
    iget-object v0, p0, Landroid/media/MediaActionSound;->mSoundPool:Landroid/media/SoundPool;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 194
    iget-object v0, p0, Landroid/media/MediaActionSound;->mSoundPool:Landroid/media/SoundPool;

    #@6
    invoke-virtual {v0}, Landroid/media/SoundPool;->release()V

    #@9
    .line 195
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Landroid/media/MediaActionSound;->mSoundPool:Landroid/media/SoundPool;

    #@c
    .line 197
    :cond_c
    return-void
.end method
