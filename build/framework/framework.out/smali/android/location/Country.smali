.class public Landroid/location/Country;
.super Ljava/lang/Object;
.source "Country.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final COUNTRY_SOURCE_LOCALE:I = 0x3

.field public static final COUNTRY_SOURCE_LOCATION:I = 0x1

.field public static final COUNTRY_SOURCE_NETWORK:I = 0x0

.field public static final COUNTRY_SOURCE_SIM:I = 0x2

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/location/Country;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mCountryIso:Ljava/lang/String;

.field private mHashCode:I

.field private final mSource:I

.field private final mTimestamp:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 136
    new-instance v0, Landroid/location/Country$1;

    #@2
    invoke-direct {v0}, Landroid/location/Country$1;-><init>()V

    #@5
    sput-object v0, Landroid/location/Country;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/location/Country;)V
    .registers 4
    .parameter "country"

    #@0
    .prologue
    .line 101
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 102
    iget-object v0, p1, Landroid/location/Country;->mCountryIso:Ljava/lang/String;

    #@5
    iput-object v0, p0, Landroid/location/Country;->mCountryIso:Ljava/lang/String;

    #@7
    .line 103
    iget v0, p1, Landroid/location/Country;->mSource:I

    #@9
    iput v0, p0, Landroid/location/Country;->mSource:I

    #@b
    .line 104
    iget-wide v0, p1, Landroid/location/Country;->mTimestamp:J

    #@d
    iput-wide v0, p0, Landroid/location/Country;->mTimestamp:J

    #@f
    .line 105
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .registers 5
    .parameter "countryIso"
    .parameter "source"

    #@0
    .prologue
    .line 81
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 82
    if-eqz p1, :cond_a

    #@5
    if-ltz p2, :cond_a

    #@7
    const/4 v0, 0x3

    #@8
    if-le p2, v0, :cond_10

    #@a
    .line 84
    :cond_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@c
    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@f
    throw v0

    #@10
    .line 86
    :cond_10
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    #@12
    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    iput-object v0, p0, Landroid/location/Country;->mCountryIso:Ljava/lang/String;

    #@18
    .line 87
    iput p2, p0, Landroid/location/Country;->mSource:I

    #@1a
    .line 88
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@1d
    move-result-wide v0

    #@1e
    iput-wide v0, p0, Landroid/location/Country;->mTimestamp:J

    #@20
    .line 89
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IJ)V
    .registers 6
    .parameter "countryIso"
    .parameter "source"
    .parameter "timestamp"

    #@0
    .prologue
    .line 91
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 92
    if-eqz p1, :cond_a

    #@5
    if-ltz p2, :cond_a

    #@7
    const/4 v0, 0x3

    #@8
    if-le p2, v0, :cond_10

    #@a
    .line 94
    :cond_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@c
    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@f
    throw v0

    #@10
    .line 96
    :cond_10
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    #@12
    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    iput-object v0, p0, Landroid/location/Country;->mCountryIso:Ljava/lang/String;

    #@18
    .line 97
    iput p2, p0, Landroid/location/Country;->mSource:I

    #@1a
    .line 98
    iput-wide p3, p0, Landroid/location/Country;->mTimestamp:J

    #@1c
    .line 99
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IJLandroid/location/Country$1;)V
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/location/Country;-><init>(Ljava/lang/String;IJ)V

    #@3
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 147
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "object"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 163
    if-ne p1, p0, :cond_5

    #@4
    .line 171
    :cond_4
    :goto_4
    return v1

    #@5
    .line 166
    :cond_5
    instance-of v3, p1, Landroid/location/Country;

    #@7
    if-eqz v3, :cond_22

    #@9
    move-object v0, p1

    #@a
    .line 167
    check-cast v0, Landroid/location/Country;

    #@c
    .line 169
    .local v0, c:Landroid/location/Country;
    iget-object v3, p0, Landroid/location/Country;->mCountryIso:Ljava/lang/String;

    #@e
    invoke-virtual {v0}, Landroid/location/Country;->getCountryIso()Ljava/lang/String;

    #@11
    move-result-object v4

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v3

    #@16
    if-eqz v3, :cond_20

    #@18
    iget v3, p0, Landroid/location/Country;->mSource:I

    #@1a
    invoke-virtual {v0}, Landroid/location/Country;->getSource()I

    #@1d
    move-result v4

    #@1e
    if-eq v3, v4, :cond_4

    #@20
    :cond_20
    move v1, v2

    #@21
    goto :goto_4

    #@22
    .end local v0           #c:Landroid/location/Country;
    :cond_22
    move v1, v2

    #@23
    .line 171
    goto :goto_4
.end method

.method public equalsIgnoreSource(Landroid/location/Country;)Z
    .registers 4
    .parameter "country"

    #@0
    .prologue
    .line 195
    if-eqz p1, :cond_10

    #@2
    iget-object v0, p0, Landroid/location/Country;->mCountryIso:Ljava/lang/String;

    #@4
    invoke-virtual {p1}, Landroid/location/Country;->getCountryIso()Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_10

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method public final getCountryIso()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Landroid/location/Country;->mCountryIso:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public final getSource()I
    .registers 2

    #@0
    .prologue
    .line 125
    iget v0, p0, Landroid/location/Country;->mSource:I

    #@2
    return v0
.end method

.method public final getTimestamp()J
    .registers 3

    #@0
    .prologue
    .line 133
    iget-wide v0, p0, Landroid/location/Country;->mTimestamp:J

    #@2
    return-wide v0
.end method

.method public hashCode()I
    .registers 4

    #@0
    .prologue
    .line 176
    iget v0, p0, Landroid/location/Country;->mHashCode:I

    #@2
    .line 177
    .local v0, hash:I
    if-nez v0, :cond_16

    #@4
    .line 178
    const/16 v0, 0x11

    #@6
    .line 179
    iget-object v1, p0, Landroid/location/Country;->mCountryIso:Ljava/lang/String;

    #@8
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    #@b
    move-result v1

    #@c
    add-int/lit16 v0, v1, 0xdd

    #@e
    .line 180
    mul-int/lit8 v1, v0, 0xd

    #@10
    iget v2, p0, Landroid/location/Country;->mSource:I

    #@12
    add-int v0, v1, v2

    #@14
    .line 181
    iput v0, p0, Landroid/location/Country;->mHashCode:I

    #@16
    .line 183
    :cond_16
    iget v1, p0, Landroid/location/Country;->mHashCode:I

    #@18
    return v1
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 200
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "Country {ISO="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Landroid/location/Country;->mCountryIso:Ljava/lang/String;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", source="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Landroid/location/Country;->mSource:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ", time="

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget-wide v1, p0, Landroid/location/Country;->mTimestamp:J

    #@25
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string/jumbo v1, "}"

    #@2c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v0

    #@30
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v0

    #@34
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "parcel"
    .parameter "flags"

    #@0
    .prologue
    .line 151
    iget-object v0, p0, Landroid/location/Country;->mCountryIso:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5
    .line 152
    iget v0, p0, Landroid/location/Country;->mSource:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 153
    iget-wide v0, p0, Landroid/location/Country;->mTimestamp:J

    #@c
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@f
    .line 154
    return-void
.end method
