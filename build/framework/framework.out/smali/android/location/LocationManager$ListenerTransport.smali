.class Landroid/location/LocationManager$ListenerTransport;
.super Landroid/location/ILocationListener$Stub;
.source "LocationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/location/LocationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ListenerTransport"
.end annotation


# static fields
.field private static final TYPE_LOCATION_CHANGED:I = 0x1

.field private static final TYPE_PROVIDER_DISABLED:I = 0x4

.field private static final TYPE_PROVIDER_ENABLED:I = 0x3

.field private static final TYPE_STATUS_CHANGED:I = 0x2


# instance fields
.field private mListener:Landroid/location/LocationListener;

.field private final mListenerHandler:Landroid/os/Handler;

.field final synthetic this$0:Landroid/location/LocationManager;


# direct methods
.method constructor <init>(Landroid/location/LocationManager;Landroid/location/LocationListener;Landroid/os/Looper;)V
    .registers 5
    .parameter
    .parameter "listener"
    .parameter "looper"

    #@0
    .prologue
    .line 193
    iput-object p1, p0, Landroid/location/LocationManager$ListenerTransport;->this$0:Landroid/location/LocationManager;

    #@2
    invoke-direct {p0}, Landroid/location/ILocationListener$Stub;-><init>()V

    #@5
    .line 194
    iput-object p2, p0, Landroid/location/LocationManager$ListenerTransport;->mListener:Landroid/location/LocationListener;

    #@7
    .line 196
    if-nez p3, :cond_11

    #@9
    .line 197
    new-instance v0, Landroid/location/LocationManager$ListenerTransport$1;

    #@b
    invoke-direct {v0, p0, p1}, Landroid/location/LocationManager$ListenerTransport$1;-><init>(Landroid/location/LocationManager$ListenerTransport;Landroid/location/LocationManager;)V

    #@e
    iput-object v0, p0, Landroid/location/LocationManager$ListenerTransport;->mListenerHandler:Landroid/os/Handler;

    #@10
    .line 211
    :goto_10
    return-void

    #@11
    .line 204
    :cond_11
    new-instance v0, Landroid/location/LocationManager$ListenerTransport$2;

    #@13
    invoke-direct {v0, p0, p3, p1}, Landroid/location/LocationManager$ListenerTransport$2;-><init>(Landroid/location/LocationManager$ListenerTransport;Landroid/os/Looper;Landroid/location/LocationManager;)V

    #@16
    iput-object v0, p0, Landroid/location/LocationManager$ListenerTransport;->mListenerHandler:Landroid/os/Handler;

    #@18
    goto :goto_10
.end method

.method private _handleMessage(Landroid/os/Message;)V
    .registers 10
    .parameter "msg"

    #@0
    .prologue
    .line 252
    iget v6, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v6, :pswitch_data_5a

    #@5
    .line 272
    :goto_5
    :try_start_5
    iget-object v6, p0, Landroid/location/LocationManager$ListenerTransport;->this$0:Landroid/location/LocationManager;

    #@7
    invoke-static {v6}, Landroid/location/LocationManager;->access$100(Landroid/location/LocationManager;)Landroid/location/ILocationManager;

    #@a
    move-result-object v6

    #@b
    invoke-interface {v6, p0}, Landroid/location/ILocationManager;->locationCallbackFinished(Landroid/location/ILocationListener;)V
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_e} :catch_50

    #@e
    .line 276
    :goto_e
    return-void

    #@f
    .line 254
    :pswitch_f
    new-instance v3, Landroid/location/Location;

    #@11
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@13
    check-cast v6, Landroid/location/Location;

    #@15
    invoke-direct {v3, v6}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    #@18
    .line 255
    .local v3, location:Landroid/location/Location;
    iget-object v6, p0, Landroid/location/LocationManager$ListenerTransport;->mListener:Landroid/location/LocationListener;

    #@1a
    invoke-interface {v6, v3}, Landroid/location/LocationListener;->onLocationChanged(Landroid/location/Location;)V

    #@1d
    goto :goto_5

    #@1e
    .line 258
    .end local v3           #location:Landroid/location/Location;
    :pswitch_1e
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@20
    check-cast v0, Landroid/os/Bundle;

    #@22
    .line 259
    .local v0, b:Landroid/os/Bundle;
    const-string/jumbo v6, "provider"

    #@25
    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@28
    move-result-object v4

    #@29
    .line 260
    .local v4, provider:Ljava/lang/String;
    const-string/jumbo v6, "status"

    #@2c
    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@2f
    move-result v5

    #@30
    .line 261
    .local v5, status:I
    const-string v6, "extras"

    #@32
    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    #@35
    move-result-object v2

    #@36
    .line 262
    .local v2, extras:Landroid/os/Bundle;
    iget-object v6, p0, Landroid/location/LocationManager$ListenerTransport;->mListener:Landroid/location/LocationListener;

    #@38
    invoke-interface {v6, v4, v5, v2}, Landroid/location/LocationListener;->onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V

    #@3b
    goto :goto_5

    #@3c
    .line 265
    .end local v0           #b:Landroid/os/Bundle;
    .end local v2           #extras:Landroid/os/Bundle;
    .end local v4           #provider:Ljava/lang/String;
    .end local v5           #status:I
    :pswitch_3c
    iget-object v7, p0, Landroid/location/LocationManager$ListenerTransport;->mListener:Landroid/location/LocationListener;

    #@3e
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@40
    check-cast v6, Ljava/lang/String;

    #@42
    invoke-interface {v7, v6}, Landroid/location/LocationListener;->onProviderEnabled(Ljava/lang/String;)V

    #@45
    goto :goto_5

    #@46
    .line 268
    :pswitch_46
    iget-object v7, p0, Landroid/location/LocationManager$ListenerTransport;->mListener:Landroid/location/LocationListener;

    #@48
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4a
    check-cast v6, Ljava/lang/String;

    #@4c
    invoke-interface {v7, v6}, Landroid/location/LocationListener;->onProviderDisabled(Ljava/lang/String;)V

    #@4f
    goto :goto_5

    #@50
    .line 273
    :catch_50
    move-exception v1

    #@51
    .line 274
    .local v1, e:Landroid/os/RemoteException;
    const-string v6, "LocationManager"

    #@53
    const-string/jumbo v7, "locationCallbackFinished: RemoteException"

    #@56
    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@59
    goto :goto_e

    #@5a
    .line 252
    :pswitch_data_5a
    .packed-switch 0x1
        :pswitch_f
        :pswitch_1e
        :pswitch_3c
        :pswitch_46
    .end packed-switch
.end method

.method static synthetic access$000(Landroid/location/LocationManager$ListenerTransport;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 184
    invoke-direct {p0, p1}, Landroid/location/LocationManager$ListenerTransport;->_handleMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .registers 4
    .parameter "location"

    #@0
    .prologue
    .line 215
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 216
    .local v0, msg:Landroid/os/Message;
    const/4 v1, 0x1

    #@5
    iput v1, v0, Landroid/os/Message;->what:I

    #@7
    .line 217
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9
    .line 218
    iget-object v1, p0, Landroid/location/LocationManager$ListenerTransport;->mListenerHandler:Landroid/os/Handler;

    #@b
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@e
    .line 219
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .registers 4
    .parameter "provider"

    #@0
    .prologue
    .line 245
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 246
    .local v0, msg:Landroid/os/Message;
    const/4 v1, 0x4

    #@5
    iput v1, v0, Landroid/os/Message;->what:I

    #@7
    .line 247
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9
    .line 248
    iget-object v1, p0, Landroid/location/LocationManager$ListenerTransport;->mListenerHandler:Landroid/os/Handler;

    #@b
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@e
    .line 249
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .registers 4
    .parameter "provider"

    #@0
    .prologue
    .line 237
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 238
    .local v0, msg:Landroid/os/Message;
    const/4 v1, 0x3

    #@5
    iput v1, v0, Landroid/os/Message;->what:I

    #@7
    .line 239
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9
    .line 240
    iget-object v1, p0, Landroid/location/LocationManager$ListenerTransport;->mListenerHandler:Landroid/os/Handler;

    #@b
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@e
    .line 241
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .registers 7
    .parameter "provider"
    .parameter "status"
    .parameter "extras"

    #@0
    .prologue
    .line 223
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v1

    #@4
    .line 224
    .local v1, msg:Landroid/os/Message;
    const/4 v2, 0x2

    #@5
    iput v2, v1, Landroid/os/Message;->what:I

    #@7
    .line 225
    new-instance v0, Landroid/os/Bundle;

    #@9
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@c
    .line 226
    .local v0, b:Landroid/os/Bundle;
    const-string/jumbo v2, "provider"

    #@f
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@12
    .line 227
    const-string/jumbo v2, "status"

    #@15
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@18
    .line 228
    if-eqz p3, :cond_1f

    #@1a
    .line 229
    const-string v2, "extras"

    #@1c
    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    #@1f
    .line 231
    :cond_1f
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@21
    .line 232
    iget-object v2, p0, Landroid/location/LocationManager$ListenerTransport;->mListenerHandler:Landroid/os/Handler;

    #@23
    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@26
    .line 233
    return-void
.end method
