.class public Landroid/location/Address;
.super Ljava/lang/Object;
.source "Address.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/location/Address;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAddressLines:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mAdminArea:Ljava/lang/String;

.field private mCountryCode:Ljava/lang/String;

.field private mCountryName:Ljava/lang/String;

.field private mExtras:Landroid/os/Bundle;

.field private mFeatureName:Ljava/lang/String;

.field private mHasLatitude:Z

.field private mHasLongitude:Z

.field private mLatitude:D

.field private mLocale:Ljava/util/Locale;

.field private mLocality:Ljava/lang/String;

.field private mLongitude:D

.field private mMaxAddressLineIndex:I

.field private mPhone:Ljava/lang/String;

.field private mPostalCode:Ljava/lang/String;

.field private mPremises:Ljava/lang/String;

.field private mSubAdminArea:Ljava/lang/String;

.field private mSubLocality:Ljava/lang/String;

.field private mSubThoroughfare:Ljava/lang/String;

.field private mThoroughfare:Ljava/lang/String;

.field private mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 475
    new-instance v0, Landroid/location/Address$1;

    #@2
    invoke-direct {v0}, Landroid/location/Address$1;-><init>()V

    #@5
    sput-object v0, Landroid/location/Address;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Ljava/util/Locale;)V
    .registers 4
    .parameter "locale"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 63
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 40
    const/4 v0, -0x1

    #@5
    iput v0, p0, Landroid/location/Address;->mMaxAddressLineIndex:I

    #@7
    .line 53
    iput-boolean v1, p0, Landroid/location/Address;->mHasLatitude:Z

    #@9
    .line 54
    iput-boolean v1, p0, Landroid/location/Address;->mHasLongitude:Z

    #@b
    .line 57
    const/4 v0, 0x0

    #@c
    iput-object v0, p0, Landroid/location/Address;->mExtras:Landroid/os/Bundle;

    #@e
    .line 64
    iput-object p1, p0, Landroid/location/Address;->mLocale:Ljava/util/Locale;

    #@10
    .line 65
    return-void
.end method

.method static synthetic access$000(Landroid/location/Address;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 34
    iget-object v0, p0, Landroid/location/Address;->mAddressLines:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$002(Landroid/location/Address;Ljava/util/HashMap;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput-object p1, p0, Landroid/location/Address;->mAddressLines:Ljava/util/HashMap;

    #@2
    return-object p1
.end method

.method static synthetic access$100(Landroid/location/Address;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 34
    iget v0, p0, Landroid/location/Address;->mMaxAddressLineIndex:I

    #@2
    return v0
.end method

.method static synthetic access$1002(Landroid/location/Address;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput-object p1, p0, Landroid/location/Address;->mPostalCode:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$102(Landroid/location/Address;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput p1, p0, Landroid/location/Address;->mMaxAddressLineIndex:I

    #@2
    return p1
.end method

.method static synthetic access$1102(Landroid/location/Address;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput-object p1, p0, Landroid/location/Address;->mCountryCode:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$1202(Landroid/location/Address;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput-object p1, p0, Landroid/location/Address;->mCountryName:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$1300(Landroid/location/Address;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 34
    iget-boolean v0, p0, Landroid/location/Address;->mHasLatitude:Z

    #@2
    return v0
.end method

.method static synthetic access$1302(Landroid/location/Address;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput-boolean p1, p0, Landroid/location/Address;->mHasLatitude:Z

    #@2
    return p1
.end method

.method static synthetic access$1402(Landroid/location/Address;D)D
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput-wide p1, p0, Landroid/location/Address;->mLatitude:D

    #@2
    return-wide p1
.end method

.method static synthetic access$1500(Landroid/location/Address;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 34
    iget-boolean v0, p0, Landroid/location/Address;->mHasLongitude:Z

    #@2
    return v0
.end method

.method static synthetic access$1502(Landroid/location/Address;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput-boolean p1, p0, Landroid/location/Address;->mHasLongitude:Z

    #@2
    return p1
.end method

.method static synthetic access$1602(Landroid/location/Address;D)D
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput-wide p1, p0, Landroid/location/Address;->mLongitude:D

    #@2
    return-wide p1
.end method

.method static synthetic access$1702(Landroid/location/Address;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput-object p1, p0, Landroid/location/Address;->mPhone:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$1802(Landroid/location/Address;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput-object p1, p0, Landroid/location/Address;->mUrl:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$1902(Landroid/location/Address;Landroid/os/Bundle;)Landroid/os/Bundle;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput-object p1, p0, Landroid/location/Address;->mExtras:Landroid/os/Bundle;

    #@2
    return-object p1
.end method

.method static synthetic access$202(Landroid/location/Address;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput-object p1, p0, Landroid/location/Address;->mFeatureName:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$302(Landroid/location/Address;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput-object p1, p0, Landroid/location/Address;->mAdminArea:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$402(Landroid/location/Address;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput-object p1, p0, Landroid/location/Address;->mSubAdminArea:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$502(Landroid/location/Address;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput-object p1, p0, Landroid/location/Address;->mLocality:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$602(Landroid/location/Address;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput-object p1, p0, Landroid/location/Address;->mSubLocality:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$702(Landroid/location/Address;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput-object p1, p0, Landroid/location/Address;->mThoroughfare:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$802(Landroid/location/Address;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput-object p1, p0, Landroid/location/Address;->mSubThoroughfare:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$902(Landroid/location/Address;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput-object p1, p0, Landroid/location/Address;->mPremises:Ljava/lang/String;

    #@2
    return-object p1
.end method


# virtual methods
.method public clearLatitude()V
    .registers 2

    #@0
    .prologue
    .line 321
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/location/Address;->mHasLatitude:Z

    #@3
    .line 322
    return-void
.end method

.method public clearLongitude()V
    .registers 2

    #@0
    .prologue
    .line 358
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/location/Address;->mHasLongitude:Z

    #@3
    .line 359
    return-void
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 530
    iget-object v0, p0, Landroid/location/Address;->mExtras:Landroid/os/Bundle;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/location/Address;->mExtras:Landroid/os/Bundle;

    #@6
    invoke-virtual {v0}, Landroid/os/Bundle;->describeContents()I

    #@9
    move-result v0

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getAddressLine(I)Ljava/lang/String;
    .registers 5
    .parameter "index"

    #@0
    .prologue
    .line 89
    if-gez p1, :cond_21

    #@2
    .line 90
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v2, "index = "

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, " < 0"

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@20
    throw v0

    #@21
    .line 92
    :cond_21
    iget-object v0, p0, Landroid/location/Address;->mAddressLines:Ljava/util/HashMap;

    #@23
    if-nez v0, :cond_27

    #@25
    const/4 v0, 0x0

    #@26
    :goto_26
    return-object v0

    #@27
    :cond_27
    iget-object v0, p0, Landroid/location/Address;->mAddressLines:Ljava/util/HashMap;

    #@29
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@30
    move-result-object v0

    #@31
    check-cast v0, Ljava/lang/String;

    #@33
    goto :goto_26
.end method

.method public getAdminArea()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 141
    iget-object v0, p0, Landroid/location/Address;->mAdminArea:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getCountryCode()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 260
    iget-object v0, p0, Landroid/location/Address;->mCountryCode:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getCountryName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 276
    iget-object v0, p0, Landroid/location/Address;->mCountryName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getExtras()Landroid/os/Bundle;
    .registers 2

    #@0
    .prologue
    .line 410
    iget-object v0, p0, Landroid/location/Address;->mExtras:Landroid/os/Bundle;

    #@2
    return-object v0
.end method

.method public getFeatureName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 126
    iget-object v0, p0, Landroid/location/Address;->mFeatureName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getLatitude()D
    .registers 3

    #@0
    .prologue
    .line 302
    iget-boolean v0, p0, Landroid/location/Address;->mHasLatitude:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 303
    iget-wide v0, p0, Landroid/location/Address;->mLatitude:D

    #@6
    return-wide v0

    #@7
    .line 305
    :cond_7
    new-instance v0, Ljava/lang/IllegalStateException;

    #@9
    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    #@c
    throw v0
.end method

.method public getLocale()Ljava/util/Locale;
    .registers 2

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Landroid/location/Address;->mLocale:Ljava/util/Locale;

    #@2
    return-object v0
.end method

.method public getLocality()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 170
    iget-object v0, p0, Landroid/location/Address;->mLocality:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getLongitude()D
    .registers 3

    #@0
    .prologue
    .line 339
    iget-boolean v0, p0, Landroid/location/Address;->mHasLongitude:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 340
    iget-wide v0, p0, Landroid/location/Address;->mLongitude:D

    #@6
    return-wide v0

    #@7
    .line 342
    :cond_7
    new-instance v0, Ljava/lang/IllegalStateException;

    #@9
    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    #@c
    throw v0
.end method

.method public getMaxAddressLineIndex()I
    .registers 2

    #@0
    .prologue
    .line 79
    iget v0, p0, Landroid/location/Address;->mMaxAddressLineIndex:I

    #@2
    return v0
.end method

.method public getPhone()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 369
    iget-object v0, p0, Landroid/location/Address;->mPhone:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getPostalCode()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 244
    iget-object v0, p0, Landroid/location/Address;->mPostalCode:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getPremises()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 229
    iget-object v0, p0, Landroid/location/Address;->mPremises:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getSubAdminArea()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 156
    iget-object v0, p0, Landroid/location/Address;->mSubAdminArea:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getSubLocality()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 185
    iget-object v0, p0, Landroid/location/Address;->mSubLocality:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getSubThoroughfare()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 215
    iget-object v0, p0, Landroid/location/Address;->mSubThoroughfare:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getThoroughfare()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 200
    iget-object v0, p0, Landroid/location/Address;->mThoroughfare:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 384
    iget-object v0, p0, Landroid/location/Address;->mUrl:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public hasLatitude()Z
    .registers 2

    #@0
    .prologue
    .line 292
    iget-boolean v0, p0, Landroid/location/Address;->mHasLatitude:Z

    #@2
    return v0
.end method

.method public hasLongitude()Z
    .registers 2

    #@0
    .prologue
    .line 329
    iget-boolean v0, p0, Landroid/location/Address;->mHasLongitude:Z

    #@2
    return v0
.end method

.method public setAddressLine(ILjava/lang/String;)V
    .registers 8
    .parameter "index"
    .parameter "line"

    #@0
    .prologue
    .line 102
    if-gez p1, :cond_21

    #@2
    .line 103
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@4
    new-instance v3, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v4, "index = "

    #@b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v3

    #@f
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    const-string v4, " < 0"

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@20
    throw v2

    #@21
    .line 105
    :cond_21
    iget-object v2, p0, Landroid/location/Address;->mAddressLines:Ljava/util/HashMap;

    #@23
    if-nez v2, :cond_2c

    #@25
    .line 106
    new-instance v2, Ljava/util/HashMap;

    #@27
    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    #@2a
    iput-object v2, p0, Landroid/location/Address;->mAddressLines:Ljava/util/HashMap;

    #@2c
    .line 108
    :cond_2c
    iget-object v2, p0, Landroid/location/Address;->mAddressLines:Ljava/util/HashMap;

    #@2e
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v2, v3, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@35
    .line 110
    if-nez p2, :cond_5d

    #@37
    .line 112
    const/4 v2, -0x1

    #@38
    iput v2, p0, Landroid/location/Address;->mMaxAddressLineIndex:I

    #@3a
    .line 113
    iget-object v2, p0, Landroid/location/Address;->mAddressLines:Ljava/util/HashMap;

    #@3c
    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@3f
    move-result-object v2

    #@40
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@43
    move-result-object v1

    #@44
    .local v1, i$:Ljava/util/Iterator;
    :goto_44
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@47
    move-result v2

    #@48
    if-eqz v2, :cond_65

    #@4a
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@4d
    move-result-object v0

    #@4e
    check-cast v0, Ljava/lang/Integer;

    #@50
    .line 114
    .local v0, i:Ljava/lang/Integer;
    iget v2, p0, Landroid/location/Address;->mMaxAddressLineIndex:I

    #@52
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@55
    move-result v3

    #@56
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    #@59
    move-result v2

    #@5a
    iput v2, p0, Landroid/location/Address;->mMaxAddressLineIndex:I

    #@5c
    goto :goto_44

    #@5d
    .line 117
    .end local v0           #i:Ljava/lang/Integer;
    .end local v1           #i$:Ljava/util/Iterator;
    :cond_5d
    iget v2, p0, Landroid/location/Address;->mMaxAddressLineIndex:I

    #@5f
    invoke-static {v2, p1}, Ljava/lang/Math;->max(II)I

    #@62
    move-result v2

    #@63
    iput v2, p0, Landroid/location/Address;->mMaxAddressLineIndex:I

    #@65
    .line 119
    :cond_65
    return-void
.end method

.method public setAdminArea(Ljava/lang/String;)V
    .registers 2
    .parameter "adminArea"

    #@0
    .prologue
    .line 148
    iput-object p1, p0, Landroid/location/Address;->mAdminArea:Ljava/lang/String;

    #@2
    .line 149
    return-void
.end method

.method public setCountryCode(Ljava/lang/String;)V
    .registers 2
    .parameter "countryCode"

    #@0
    .prologue
    .line 268
    iput-object p1, p0, Landroid/location/Address;->mCountryCode:Ljava/lang/String;

    #@2
    .line 269
    return-void
.end method

.method public setCountryName(Ljava/lang/String;)V
    .registers 2
    .parameter "countryName"

    #@0
    .prologue
    .line 284
    iput-object p1, p0, Landroid/location/Address;->mCountryName:Ljava/lang/String;

    #@2
    .line 285
    return-void
.end method

.method public setExtras(Landroid/os/Bundle;)V
    .registers 3
    .parameter "extras"

    #@0
    .prologue
    .line 418
    if-nez p1, :cond_6

    #@2
    const/4 v0, 0x0

    #@3
    :goto_3
    iput-object v0, p0, Landroid/location/Address;->mExtras:Landroid/os/Bundle;

    #@5
    .line 419
    return-void

    #@6
    .line 418
    :cond_6
    new-instance v0, Landroid/os/Bundle;

    #@8
    invoke-direct {v0, p1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@b
    goto :goto_3
.end method

.method public setFeatureName(Ljava/lang/String;)V
    .registers 2
    .parameter "featureName"

    #@0
    .prologue
    .line 133
    iput-object p1, p0, Landroid/location/Address;->mFeatureName:Ljava/lang/String;

    #@2
    .line 134
    return-void
.end method

.method public setLatitude(D)V
    .registers 4
    .parameter "latitude"

    #@0
    .prologue
    .line 313
    iput-wide p1, p0, Landroid/location/Address;->mLatitude:D

    #@2
    .line 314
    const/4 v0, 0x1

    #@3
    iput-boolean v0, p0, Landroid/location/Address;->mHasLatitude:Z

    #@5
    .line 315
    return-void
.end method

.method public setLocality(Ljava/lang/String;)V
    .registers 2
    .parameter "locality"

    #@0
    .prologue
    .line 177
    iput-object p1, p0, Landroid/location/Address;->mLocality:Ljava/lang/String;

    #@2
    .line 178
    return-void
.end method

.method public setLongitude(D)V
    .registers 4
    .parameter "longitude"

    #@0
    .prologue
    .line 350
    iput-wide p1, p0, Landroid/location/Address;->mLongitude:D

    #@2
    .line 351
    const/4 v0, 0x1

    #@3
    iput-boolean v0, p0, Landroid/location/Address;->mHasLongitude:Z

    #@5
    .line 352
    return-void
.end method

.method public setPhone(Ljava/lang/String;)V
    .registers 2
    .parameter "phone"

    #@0
    .prologue
    .line 376
    iput-object p1, p0, Landroid/location/Address;->mPhone:Ljava/lang/String;

    #@2
    .line 377
    return-void
.end method

.method public setPostalCode(Ljava/lang/String;)V
    .registers 2
    .parameter "postalCode"

    #@0
    .prologue
    .line 252
    iput-object p1, p0, Landroid/location/Address;->mPostalCode:Ljava/lang/String;

    #@2
    .line 253
    return-void
.end method

.method public setPremises(Ljava/lang/String;)V
    .registers 2
    .parameter "premises"

    #@0
    .prologue
    .line 236
    iput-object p1, p0, Landroid/location/Address;->mPremises:Ljava/lang/String;

    #@2
    .line 237
    return-void
.end method

.method public setSubAdminArea(Ljava/lang/String;)V
    .registers 2
    .parameter "subAdminArea"

    #@0
    .prologue
    .line 163
    iput-object p1, p0, Landroid/location/Address;->mSubAdminArea:Ljava/lang/String;

    #@2
    .line 164
    return-void
.end method

.method public setSubLocality(Ljava/lang/String;)V
    .registers 2
    .parameter "sublocality"

    #@0
    .prologue
    .line 192
    iput-object p1, p0, Landroid/location/Address;->mSubLocality:Ljava/lang/String;

    #@2
    .line 193
    return-void
.end method

.method public setSubThoroughfare(Ljava/lang/String;)V
    .registers 2
    .parameter "subthoroughfare"

    #@0
    .prologue
    .line 222
    iput-object p1, p0, Landroid/location/Address;->mSubThoroughfare:Ljava/lang/String;

    #@2
    .line 223
    return-void
.end method

.method public setThoroughfare(Ljava/lang/String;)V
    .registers 2
    .parameter "thoroughfare"

    #@0
    .prologue
    .line 207
    iput-object p1, p0, Landroid/location/Address;->mThoroughfare:Ljava/lang/String;

    #@2
    .line 208
    return-void
.end method

.method public setUrl(Ljava/lang/String;)V
    .registers 2
    .parameter "Url"

    #@0
    .prologue
    .line 391
    iput-object p1, p0, Landroid/location/Address;->mUrl:Ljava/lang/String;

    #@2
    .line 392
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 8

    #@0
    .prologue
    const/16 v6, 0x5d

    #@2
    const/16 v5, 0x22

    #@4
    .line 423
    new-instance v2, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    .line 424
    .local v2, sb:Ljava/lang/StringBuilder;
    const-string v3, "Address[addressLines=["

    #@b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    .line 425
    const/4 v0, 0x0

    #@f
    .local v0, i:I
    :goto_f
    iget v3, p0, Landroid/location/Address;->mMaxAddressLineIndex:I

    #@11
    if-gt v0, v3, :cond_43

    #@13
    .line 426
    if-lez v0, :cond_1a

    #@15
    .line 427
    const/16 v3, 0x2c

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1a
    .line 429
    :cond_1a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    .line 430
    const/16 v3, 0x3a

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@22
    .line 431
    iget-object v3, p0, Landroid/location/Address;->mAddressLines:Ljava/util/HashMap;

    #@24
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@27
    move-result-object v4

    #@28
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2b
    move-result-object v1

    #@2c
    check-cast v1, Ljava/lang/String;

    #@2e
    .line 432
    .local v1, line:Ljava/lang/String;
    if-nez v1, :cond_39

    #@30
    .line 433
    const-string/jumbo v3, "null"

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    .line 425
    :goto_36
    add-int/lit8 v0, v0, 0x1

    #@38
    goto :goto_f

    #@39
    .line 435
    :cond_39
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@3c
    .line 436
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    .line 437
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@42
    goto :goto_36

    #@43
    .line 440
    .end local v1           #line:Ljava/lang/String;
    :cond_43
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@46
    .line 441
    const-string v3, ",feature="

    #@48
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    .line 442
    iget-object v3, p0, Landroid/location/Address;->mFeatureName:Ljava/lang/String;

    #@4d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    .line 443
    const-string v3, ",admin="

    #@52
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    .line 444
    iget-object v3, p0, Landroid/location/Address;->mAdminArea:Ljava/lang/String;

    #@57
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    .line 445
    const-string v3, ",sub-admin="

    #@5c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    .line 446
    iget-object v3, p0, Landroid/location/Address;->mSubAdminArea:Ljava/lang/String;

    #@61
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    .line 447
    const-string v3, ",locality="

    #@66
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    .line 448
    iget-object v3, p0, Landroid/location/Address;->mLocality:Ljava/lang/String;

    #@6b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    .line 449
    const-string v3, ",thoroughfare="

    #@70
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    .line 450
    iget-object v3, p0, Landroid/location/Address;->mThoroughfare:Ljava/lang/String;

    #@75
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    .line 451
    const-string v3, ",postalCode="

    #@7a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    .line 452
    iget-object v3, p0, Landroid/location/Address;->mPostalCode:Ljava/lang/String;

    #@7f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    .line 453
    const-string v3, ",countryCode="

    #@84
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    .line 454
    iget-object v3, p0, Landroid/location/Address;->mCountryCode:Ljava/lang/String;

    #@89
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    .line 455
    const-string v3, ",countryName="

    #@8e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    .line 456
    iget-object v3, p0, Landroid/location/Address;->mCountryName:Ljava/lang/String;

    #@93
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    .line 457
    const-string v3, ",hasLatitude="

    #@98
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    .line 458
    iget-boolean v3, p0, Landroid/location/Address;->mHasLatitude:Z

    #@9d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@a0
    .line 459
    const-string v3, ",latitude="

    #@a2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    .line 460
    iget-wide v3, p0, Landroid/location/Address;->mLatitude:D

    #@a7
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@aa
    .line 461
    const-string v3, ",hasLongitude="

    #@ac
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    .line 462
    iget-boolean v3, p0, Landroid/location/Address;->mHasLongitude:Z

    #@b1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@b4
    .line 463
    const-string v3, ",longitude="

    #@b6
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    .line 464
    iget-wide v3, p0, Landroid/location/Address;->mLongitude:D

    #@bb
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@be
    .line 465
    const-string v3, ",phone="

    #@c0
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    .line 466
    iget-object v3, p0, Landroid/location/Address;->mPhone:Ljava/lang/String;

    #@c5
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    .line 467
    const-string v3, ",url="

    #@ca
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    .line 468
    iget-object v3, p0, Landroid/location/Address;->mUrl:Ljava/lang/String;

    #@cf
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    .line 469
    const-string v3, ",extras="

    #@d4
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    .line 470
    iget-object v3, p0, Landroid/location/Address;->mExtras:Landroid/os/Bundle;

    #@d9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@dc
    .line 471
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@df
    .line 472
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e2
    move-result-object v3

    #@e3
    return-object v3
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 11
    .parameter "parcel"
    .parameter "flags"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 534
    iget-object v3, p0, Landroid/location/Address;->mLocale:Ljava/util/Locale;

    #@4
    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    #@7
    move-result-object v3

    #@8
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@b
    .line 535
    iget-object v3, p0, Landroid/location/Address;->mLocale:Ljava/util/Locale;

    #@d
    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 536
    iget-object v3, p0, Landroid/location/Address;->mAddressLines:Ljava/util/HashMap;

    #@16
    if-nez v3, :cond_83

    #@18
    .line 537
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@1b
    .line 546
    :cond_1b
    iget-object v3, p0, Landroid/location/Address;->mFeatureName:Ljava/lang/String;

    #@1d
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@20
    .line 547
    iget-object v3, p0, Landroid/location/Address;->mAdminArea:Ljava/lang/String;

    #@22
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@25
    .line 548
    iget-object v3, p0, Landroid/location/Address;->mSubAdminArea:Ljava/lang/String;

    #@27
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2a
    .line 549
    iget-object v3, p0, Landroid/location/Address;->mLocality:Ljava/lang/String;

    #@2c
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2f
    .line 550
    iget-object v3, p0, Landroid/location/Address;->mSubLocality:Ljava/lang/String;

    #@31
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@34
    .line 551
    iget-object v3, p0, Landroid/location/Address;->mThoroughfare:Ljava/lang/String;

    #@36
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@39
    .line 552
    iget-object v3, p0, Landroid/location/Address;->mSubThoroughfare:Ljava/lang/String;

    #@3b
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@3e
    .line 553
    iget-object v3, p0, Landroid/location/Address;->mPremises:Ljava/lang/String;

    #@40
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@43
    .line 554
    iget-object v3, p0, Landroid/location/Address;->mPostalCode:Ljava/lang/String;

    #@45
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@48
    .line 555
    iget-object v3, p0, Landroid/location/Address;->mCountryCode:Ljava/lang/String;

    #@4a
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@4d
    .line 556
    iget-object v3, p0, Landroid/location/Address;->mCountryName:Ljava/lang/String;

    #@4f
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@52
    .line 557
    iget-boolean v3, p0, Landroid/location/Address;->mHasLatitude:Z

    #@54
    if-eqz v3, :cond_b7

    #@56
    move v3, v4

    #@57
    :goto_57
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@5a
    .line 558
    iget-boolean v3, p0, Landroid/location/Address;->mHasLatitude:Z

    #@5c
    if-eqz v3, :cond_63

    #@5e
    .line 559
    iget-wide v6, p0, Landroid/location/Address;->mLatitude:D

    #@60
    invoke-virtual {p1, v6, v7}, Landroid/os/Parcel;->writeDouble(D)V

    #@63
    .line 561
    :cond_63
    iget-boolean v3, p0, Landroid/location/Address;->mHasLongitude:Z

    #@65
    if-eqz v3, :cond_b9

    #@67
    :goto_67
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@6a
    .line 562
    iget-boolean v3, p0, Landroid/location/Address;->mHasLongitude:Z

    #@6c
    if-eqz v3, :cond_73

    #@6e
    .line 563
    iget-wide v3, p0, Landroid/location/Address;->mLongitude:D

    #@70
    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeDouble(D)V

    #@73
    .line 565
    :cond_73
    iget-object v3, p0, Landroid/location/Address;->mPhone:Ljava/lang/String;

    #@75
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@78
    .line 566
    iget-object v3, p0, Landroid/location/Address;->mUrl:Ljava/lang/String;

    #@7a
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@7d
    .line 567
    iget-object v3, p0, Landroid/location/Address;->mExtras:Landroid/os/Bundle;

    #@7f
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    #@82
    .line 568
    return-void

    #@83
    .line 539
    :cond_83
    iget-object v3, p0, Landroid/location/Address;->mAddressLines:Ljava/util/HashMap;

    #@85
    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@88
    move-result-object v1

    #@89
    .line 540
    .local v1, entries:Ljava/util/Set;,"Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;>;"
    invoke-interface {v1}, Ljava/util/Set;->size()I

    #@8c
    move-result v3

    #@8d
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@90
    .line 541
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@93
    move-result-object v2

    #@94
    .local v2, i$:Ljava/util/Iterator;
    :goto_94
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@97
    move-result v3

    #@98
    if-eqz v3, :cond_1b

    #@9a
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@9d
    move-result-object v0

    #@9e
    check-cast v0, Ljava/util/Map$Entry;

    #@a0
    .line 542
    .local v0, e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@a3
    move-result-object v3

    #@a4
    check-cast v3, Ljava/lang/Integer;

    #@a6
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@a9
    move-result v3

    #@aa
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@ad
    .line 543
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@b0
    move-result-object v3

    #@b1
    check-cast v3, Ljava/lang/String;

    #@b3
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@b6
    goto :goto_94

    #@b7
    .end local v0           #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    .end local v1           #entries:Ljava/util/Set;,"Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;>;"
    .end local v2           #i$:Ljava/util/Iterator;
    :cond_b7
    move v3, v5

    #@b8
    .line 557
    goto :goto_57

    #@b9
    :cond_b9
    move v4, v5

    #@ba
    .line 561
    goto :goto_67
.end method
