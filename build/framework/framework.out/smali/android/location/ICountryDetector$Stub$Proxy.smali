.class Landroid/location/ICountryDetector$Stub$Proxy;
.super Ljava/lang/Object;
.source "ICountryDetector.java"

# interfaces
.implements Landroid/location/ICountryDetector;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/location/ICountryDetector$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 89
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 90
    iput-object p1, p0, Landroid/location/ICountryDetector$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 91
    return-void
.end method


# virtual methods
.method public addCountryListener(Landroid/location/ICountryListener;)V
    .registers 7
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 131
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 132
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 134
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.location.ICountryDetector"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 135
    if-eqz p1, :cond_27

    #@f
    invoke-interface {p1}, Landroid/location/ICountryListener;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 136
    iget-object v2, p0, Landroid/location/ICountryDetector$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/4 v3, 0x2

    #@19
    const/4 v4, 0x0

    #@1a
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1d
    .line 137
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_20
    .catchall {:try_start_8 .. :try_end_20} :catchall_29

    #@20
    .line 140
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 141
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 143
    return-void

    #@27
    .line 135
    :cond_27
    const/4 v2, 0x0

    #@28
    goto :goto_13

    #@29
    .line 140
    :catchall_29
    move-exception v2

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 141
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v2
.end method

.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 94
    iget-object v0, p0, Landroid/location/ICountryDetector$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public detectCountry()Landroid/location/Country;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 106
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 107
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 110
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.location.ICountryDetector"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 111
    iget-object v3, p0, Landroid/location/ICountryDetector$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x1

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 112
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 113
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1a
    move-result v3

    #@1b
    if-eqz v3, :cond_2c

    #@1d
    .line 114
    sget-object v3, Landroid/location/Country;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1f
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@22
    move-result-object v2

    #@23
    check-cast v2, Landroid/location/Country;
    :try_end_25
    .catchall {:try_start_8 .. :try_end_25} :catchall_2e

    #@25
    .line 121
    .local v2, _result:Landroid/location/Country;
    :goto_25
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 122
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 124
    return-object v2

    #@2c
    .line 117
    .end local v2           #_result:Landroid/location/Country;
    :cond_2c
    const/4 v2, 0x0

    #@2d
    .restart local v2       #_result:Landroid/location/Country;
    goto :goto_25

    #@2e
    .line 121
    .end local v2           #_result:Landroid/location/Country;
    :catchall_2e
    move-exception v3

    #@2f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 122
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    throw v3
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 98
    const-string v0, "android.location.ICountryDetector"

    #@2
    return-object v0
.end method

.method public removeCountryListener(Landroid/location/ICountryListener;)V
    .registers 7
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 149
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 150
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 152
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.location.ICountryDetector"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 153
    if-eqz p1, :cond_27

    #@f
    invoke-interface {p1}, Landroid/location/ICountryListener;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 154
    iget-object v2, p0, Landroid/location/ICountryDetector$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/4 v3, 0x3

    #@19
    const/4 v4, 0x0

    #@1a
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1d
    .line 155
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_20
    .catchall {:try_start_8 .. :try_end_20} :catchall_29

    #@20
    .line 158
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 159
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 161
    return-void

    #@27
    .line 153
    :cond_27
    const/4 v2, 0x0

    #@28
    goto :goto_13

    #@29
    .line 158
    :catchall_29
    move-exception v2

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 159
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v2
.end method
