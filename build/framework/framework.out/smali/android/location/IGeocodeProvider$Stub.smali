.class public abstract Landroid/location/IGeocodeProvider$Stub;
.super Landroid/os/Binder;
.source "IGeocodeProvider.java"

# interfaces
.implements Landroid/location/IGeocodeProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/location/IGeocodeProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/location/IGeocodeProvider$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.location.IGeocodeProvider"

.field static final TRANSACTION_getFromLocation:I = 0x1

.field static final TRANSACTION_getFromLocationName:I = 0x2


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "android.location.IGeocodeProvider"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/location/IGeocodeProvider$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/location/IGeocodeProvider;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "android.location.IGeocodeProvider"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/location/IGeocodeProvider;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Landroid/location/IGeocodeProvider;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Landroid/location/IGeocodeProvider$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/location/IGeocodeProvider$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 29
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 43
    sparse-switch p1, :sswitch_data_a6

    #@3
    .line 105
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v2

    #@7
    :goto_7
    return v2

    #@8
    .line 47
    :sswitch_8
    const-string v2, "android.location.IGeocodeProvider"

    #@a
    move-object/from16 v0, p3

    #@c
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 48
    const/4 v2, 0x1

    #@10
    goto :goto_7

    #@11
    .line 52
    :sswitch_11
    const-string v2, "android.location.IGeocodeProvider"

    #@13
    move-object/from16 v0, p2

    #@15
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18
    .line 54
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readDouble()D

    #@1b
    move-result-wide v3

    #@1c
    .line 56
    .local v3, _arg0:D
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readDouble()D

    #@1f
    move-result-wide v5

    #@20
    .line 58
    .local v5, _arg1:D
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@23
    move-result v7

    #@24
    .line 60
    .local v7, _arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@27
    move-result v2

    #@28
    if-eqz v2, :cond_50

    #@2a
    .line 61
    sget-object v2, Landroid/location/GeocoderParams;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2c
    move-object/from16 v0, p2

    #@2e
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@31
    move-result-object v8

    #@32
    check-cast v8, Landroid/location/GeocoderParams;

    #@34
    .line 67
    .local v8, _arg3:Landroid/location/GeocoderParams;
    :goto_34
    new-instance v9, Ljava/util/ArrayList;

    #@36
    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    #@39
    .local v9, _arg4:Ljava/util/List;,"Ljava/util/List<Landroid/location/Address;>;"
    move-object/from16 v2, p0

    #@3b
    .line 68
    invoke-virtual/range {v2 .. v9}, Landroid/location/IGeocodeProvider$Stub;->getFromLocation(DDILandroid/location/GeocoderParams;Ljava/util/List;)Ljava/lang/String;

    #@3e
    move-result-object v23

    #@3f
    .line 69
    .local v23, _result:Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@42
    .line 70
    move-object/from16 v0, p3

    #@44
    move-object/from16 v1, v23

    #@46
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@49
    .line 71
    move-object/from16 v0, p3

    #@4b
    invoke-virtual {v0, v9}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@4e
    .line 72
    const/4 v2, 0x1

    #@4f
    goto :goto_7

    #@50
    .line 64
    .end local v8           #_arg3:Landroid/location/GeocoderParams;
    .end local v9           #_arg4:Ljava/util/List;,"Ljava/util/List<Landroid/location/Address;>;"
    .end local v23           #_result:Ljava/lang/String;
    :cond_50
    const/4 v8, 0x0

    #@51
    .restart local v8       #_arg3:Landroid/location/GeocoderParams;
    goto :goto_34

    #@52
    .line 76
    .end local v3           #_arg0:D
    .end local v5           #_arg1:D
    .end local v7           #_arg2:I
    .end local v8           #_arg3:Landroid/location/GeocoderParams;
    :sswitch_52
    const-string v2, "android.location.IGeocodeProvider"

    #@54
    move-object/from16 v0, p2

    #@56
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@59
    .line 78
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5c
    move-result-object v3

    #@5d
    .line 80
    .local v3, _arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readDouble()D

    #@60
    move-result-wide v5

    #@61
    .line 82
    .restart local v5       #_arg1:D
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readDouble()D

    #@64
    move-result-wide v14

    #@65
    .line 84
    .local v14, _arg2:D
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readDouble()D

    #@68
    move-result-wide v16

    #@69
    .line 86
    .local v16, _arg3:D
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readDouble()D

    #@6c
    move-result-wide v18

    #@6d
    .line 88
    .local v18, _arg4:D
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@70
    move-result v20

    #@71
    .line 90
    .local v20, _arg5:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@74
    move-result v2

    #@75
    if-eqz v2, :cond_a2

    #@77
    .line 91
    sget-object v2, Landroid/location/GeocoderParams;->CREATOR:Landroid/os/Parcelable$Creator;

    #@79
    move-object/from16 v0, p2

    #@7b
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@7e
    move-result-object v21

    #@7f
    check-cast v21, Landroid/location/GeocoderParams;

    #@81
    .line 97
    .local v21, _arg6:Landroid/location/GeocoderParams;
    :goto_81
    new-instance v22, Ljava/util/ArrayList;

    #@83
    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    #@86
    .local v22, _arg7:Ljava/util/List;,"Ljava/util/List<Landroid/location/Address;>;"
    move-object/from16 v10, p0

    #@88
    move-object v11, v3

    #@89
    move-wide v12, v5

    #@8a
    .line 98
    invoke-virtual/range {v10 .. v22}, Landroid/location/IGeocodeProvider$Stub;->getFromLocationName(Ljava/lang/String;DDDDILandroid/location/GeocoderParams;Ljava/util/List;)Ljava/lang/String;

    #@8d
    move-result-object v23

    #@8e
    .line 99
    .restart local v23       #_result:Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@91
    .line 100
    move-object/from16 v0, p3

    #@93
    move-object/from16 v1, v23

    #@95
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@98
    .line 101
    move-object/from16 v0, p3

    #@9a
    move-object/from16 v1, v22

    #@9c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@9f
    .line 102
    const/4 v2, 0x1

    #@a0
    goto/16 :goto_7

    #@a2
    .line 94
    .end local v21           #_arg6:Landroid/location/GeocoderParams;
    .end local v22           #_arg7:Ljava/util/List;,"Ljava/util/List<Landroid/location/Address;>;"
    .end local v23           #_result:Ljava/lang/String;
    :cond_a2
    const/16 v21, 0x0

    #@a4
    .restart local v21       #_arg6:Landroid/location/GeocoderParams;
    goto :goto_81

    #@a5
    .line 43
    nop

    #@a6
    :sswitch_data_a6
    .sparse-switch
        0x1 -> :sswitch_11
        0x2 -> :sswitch_52
        0x5f4e5446 -> :sswitch_8
    .end sparse-switch
.end method
