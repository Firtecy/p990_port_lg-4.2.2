.class final Landroid/location/GpsStatus$SatelliteIterator;
.super Ljava/lang/Object;
.source "GpsStatus.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/location/GpsStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SatelliteIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Landroid/location/GpsSatellite;",
        ">;"
    }
.end annotation


# instance fields
.field mIndex:I

.field private mSatellites:[Landroid/location/GpsSatellite;

.field final synthetic this$0:Landroid/location/GpsStatus;


# direct methods
.method constructor <init>(Landroid/location/GpsStatus;[Landroid/location/GpsSatellite;)V
    .registers 4
    .parameter
    .parameter "satellites"

    #@0
    .prologue
    .line 39
    iput-object p1, p0, Landroid/location/GpsStatus$SatelliteIterator;->this$0:Landroid/location/GpsStatus;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 37
    const/4 v0, 0x0

    #@6
    iput v0, p0, Landroid/location/GpsStatus$SatelliteIterator;->mIndex:I

    #@8
    .line 40
    iput-object p2, p0, Landroid/location/GpsStatus$SatelliteIterator;->mSatellites:[Landroid/location/GpsSatellite;

    #@a
    .line 41
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .registers 3

    #@0
    .prologue
    .line 44
    iget v0, p0, Landroid/location/GpsStatus$SatelliteIterator;->mIndex:I

    #@2
    .local v0, i:I
    :goto_2
    iget-object v1, p0, Landroid/location/GpsStatus$SatelliteIterator;->mSatellites:[Landroid/location/GpsSatellite;

    #@4
    array-length v1, v1

    #@5
    if-ge v0, v1, :cond_14

    #@7
    .line 45
    iget-object v1, p0, Landroid/location/GpsStatus$SatelliteIterator;->mSatellites:[Landroid/location/GpsSatellite;

    #@9
    aget-object v1, v1, v0

    #@b
    iget-boolean v1, v1, Landroid/location/GpsSatellite;->mValid:Z

    #@d
    if-eqz v1, :cond_11

    #@f
    .line 46
    const/4 v1, 0x1

    #@10
    .line 49
    :goto_10
    return v1

    #@11
    .line 44
    :cond_11
    add-int/lit8 v0, v0, 0x1

    #@13
    goto :goto_2

    #@14
    .line 49
    :cond_14
    const/4 v1, 0x0

    #@15
    goto :goto_10
.end method

.method public next()Landroid/location/GpsSatellite;
    .registers 5

    #@0
    .prologue
    .line 53
    :cond_0
    iget v1, p0, Landroid/location/GpsStatus$SatelliteIterator;->mIndex:I

    #@2
    iget-object v2, p0, Landroid/location/GpsStatus$SatelliteIterator;->mSatellites:[Landroid/location/GpsSatellite;

    #@4
    array-length v2, v2

    #@5
    if-ge v1, v2, :cond_16

    #@7
    .line 54
    iget-object v1, p0, Landroid/location/GpsStatus$SatelliteIterator;->mSatellites:[Landroid/location/GpsSatellite;

    #@9
    iget v2, p0, Landroid/location/GpsStatus$SatelliteIterator;->mIndex:I

    #@b
    add-int/lit8 v3, v2, 0x1

    #@d
    iput v3, p0, Landroid/location/GpsStatus$SatelliteIterator;->mIndex:I

    #@f
    aget-object v0, v1, v2

    #@11
    .line 55
    .local v0, satellite:Landroid/location/GpsSatellite;
    iget-boolean v1, v0, Landroid/location/GpsSatellite;->mValid:Z

    #@13
    if-eqz v1, :cond_0

    #@15
    .line 56
    return-object v0

    #@16
    .line 59
    .end local v0           #satellite:Landroid/location/GpsSatellite;
    :cond_16
    new-instance v1, Ljava/util/NoSuchElementException;

    #@18
    invoke-direct {v1}, Ljava/util/NoSuchElementException;-><init>()V

    #@1b
    throw v1
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 34
    invoke-virtual {p0}, Landroid/location/GpsStatus$SatelliteIterator;->next()Landroid/location/GpsSatellite;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public remove()V
    .registers 2

    #@0
    .prologue
    .line 63
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@5
    throw v0
.end method
