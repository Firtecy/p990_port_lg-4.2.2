.class public abstract Landroid/location/ILocationManager$Stub;
.super Landroid/os/Binder;
.source "ILocationManager.java"

# interfaces
.implements Landroid/location/ILocationManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/location/ILocationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/location/ILocationManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.location.ILocationManager"

.field static final TRANSACTION_addGpsStatusListener:I = 0x6

.field static final TRANSACTION_addTestProvider:I = 0x12

.field static final TRANSACTION_clearTestProviderEnabled:I = 0x17

.field static final TRANSACTION_clearTestProviderLocation:I = 0x15

.field static final TRANSACTION_clearTestProviderStatus:I = 0x19

.field static final TRANSACTION_geocoderIsPresent:I = 0x8

.field static final TRANSACTION_getAllProviders:I = 0xc

.field static final TRANSACTION_getBestProvider:I = 0xe

.field static final TRANSACTION_getFromLocation:I = 0x9

.field static final TRANSACTION_getFromLocationName:I = 0xa

.field static final TRANSACTION_getLastLocation:I = 0x5

.field static final TRANSACTION_getProviderProperties:I = 0x10

.field static final TRANSACTION_getProviders:I = 0xd

.field static final TRANSACTION_isProviderEnabled:I = 0x11

.field static final TRANSACTION_locationCallbackFinished:I = 0x1c

.field static final TRANSACTION_providerMeetsCriteria:I = 0xf

.field static final TRANSACTION_removeGeofence:I = 0x4

.field static final TRANSACTION_removeGpsStatusListener:I = 0x7

.field static final TRANSACTION_removeTestProvider:I = 0x13

.field static final TRANSACTION_removeUpdates:I = 0x2

.field static final TRANSACTION_reportLocation:I = 0x1b

.field static final TRANSACTION_requestGeofence:I = 0x3

.field static final TRANSACTION_requestLocationUpdates:I = 0x1

.field static final TRANSACTION_sendExtraCommand:I = 0x1a

.field static final TRANSACTION_sendNiResponse:I = 0xb

.field static final TRANSACTION_setTestProviderEnabled:I = 0x16

.field static final TRANSACTION_setTestProviderLocation:I = 0x14

.field static final TRANSACTION_setTestProviderStatus:I = 0x18


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "android.location.ILocationManager"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/location/ILocationManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/location/ILocationManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "android.location.ILocationManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/location/ILocationManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Landroid/location/ILocationManager;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Landroid/location/ILocationManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/location/ILocationManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 36
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 43
    sparse-switch p1, :sswitch_data_4f2

    #@3
    .line 498
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v2

    #@7
    :goto_7
    return v2

    #@8
    .line 47
    :sswitch_8
    const-string v2, "android.location.ILocationManager"

    #@a
    move-object/from16 v0, p3

    #@c
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 48
    const/4 v2, 0x1

    #@10
    goto :goto_7

    #@11
    .line 52
    :sswitch_11
    const-string v2, "android.location.ILocationManager"

    #@13
    move-object/from16 v0, p2

    #@15
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18
    .line 54
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v2

    #@1c
    if-eqz v2, :cond_4e

    #@1e
    .line 55
    sget-object v2, Landroid/location/LocationRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    #@20
    move-object/from16 v0, p2

    #@22
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@25
    move-result-object v3

    #@26
    check-cast v3, Landroid/location/LocationRequest;

    #@28
    .line 61
    .local v3, _arg0:Landroid/location/LocationRequest;
    :goto_28
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@2b
    move-result-object v2

    #@2c
    invoke-static {v2}, Landroid/location/ILocationListener$Stub;->asInterface(Landroid/os/IBinder;)Landroid/location/ILocationListener;

    #@2f
    move-result-object v5

    #@30
    .line 63
    .local v5, _arg1:Landroid/location/ILocationListener;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@33
    move-result v2

    #@34
    if-eqz v2, :cond_50

    #@36
    .line 64
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@38
    move-object/from16 v0, p2

    #@3a
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@3d
    move-result-object v7

    #@3e
    check-cast v7, Landroid/app/PendingIntent;

    #@40
    .line 70
    .local v7, _arg2:Landroid/app/PendingIntent;
    :goto_40
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@43
    move-result-object v8

    #@44
    .line 71
    .local v8, _arg3:Ljava/lang/String;
    move-object/from16 v0, p0

    #@46
    invoke-virtual {v0, v3, v5, v7, v8}, Landroid/location/ILocationManager$Stub;->requestLocationUpdates(Landroid/location/LocationRequest;Landroid/location/ILocationListener;Landroid/app/PendingIntent;Ljava/lang/String;)V

    #@49
    .line 72
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@4c
    .line 73
    const/4 v2, 0x1

    #@4d
    goto :goto_7

    #@4e
    .line 58
    .end local v3           #_arg0:Landroid/location/LocationRequest;
    .end local v5           #_arg1:Landroid/location/ILocationListener;
    .end local v7           #_arg2:Landroid/app/PendingIntent;
    .end local v8           #_arg3:Ljava/lang/String;
    :cond_4e
    const/4 v3, 0x0

    #@4f
    .restart local v3       #_arg0:Landroid/location/LocationRequest;
    goto :goto_28

    #@50
    .line 67
    .restart local v5       #_arg1:Landroid/location/ILocationListener;
    :cond_50
    const/4 v7, 0x0

    #@51
    .restart local v7       #_arg2:Landroid/app/PendingIntent;
    goto :goto_40

    #@52
    .line 77
    .end local v3           #_arg0:Landroid/location/LocationRequest;
    .end local v5           #_arg1:Landroid/location/ILocationListener;
    .end local v7           #_arg2:Landroid/app/PendingIntent;
    :sswitch_52
    const-string v2, "android.location.ILocationManager"

    #@54
    move-object/from16 v0, p2

    #@56
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@59
    .line 79
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@5c
    move-result-object v2

    #@5d
    invoke-static {v2}, Landroid/location/ILocationListener$Stub;->asInterface(Landroid/os/IBinder;)Landroid/location/ILocationListener;

    #@60
    move-result-object v3

    #@61
    .line 81
    .local v3, _arg0:Landroid/location/ILocationListener;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@64
    move-result v2

    #@65
    if-eqz v2, :cond_7f

    #@67
    .line 82
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@69
    move-object/from16 v0, p2

    #@6b
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@6e
    move-result-object v5

    #@6f
    check-cast v5, Landroid/app/PendingIntent;

    #@71
    .line 88
    .local v5, _arg1:Landroid/app/PendingIntent;
    :goto_71
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@74
    move-result-object v7

    #@75
    .line 89
    .local v7, _arg2:Ljava/lang/String;
    move-object/from16 v0, p0

    #@77
    invoke-virtual {v0, v3, v5, v7}, Landroid/location/ILocationManager$Stub;->removeUpdates(Landroid/location/ILocationListener;Landroid/app/PendingIntent;Ljava/lang/String;)V

    #@7a
    .line 90
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@7d
    .line 91
    const/4 v2, 0x1

    #@7e
    goto :goto_7

    #@7f
    .line 85
    .end local v5           #_arg1:Landroid/app/PendingIntent;
    .end local v7           #_arg2:Ljava/lang/String;
    :cond_7f
    const/4 v5, 0x0

    #@80
    .restart local v5       #_arg1:Landroid/app/PendingIntent;
    goto :goto_71

    #@81
    .line 95
    .end local v3           #_arg0:Landroid/location/ILocationListener;
    .end local v5           #_arg1:Landroid/app/PendingIntent;
    :sswitch_81
    const-string v2, "android.location.ILocationManager"

    #@83
    move-object/from16 v0, p2

    #@85
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@88
    .line 97
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@8b
    move-result v2

    #@8c
    if-eqz v2, :cond_c7

    #@8e
    .line 98
    sget-object v2, Landroid/location/LocationRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    #@90
    move-object/from16 v0, p2

    #@92
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@95
    move-result-object v3

    #@96
    check-cast v3, Landroid/location/LocationRequest;

    #@98
    .line 104
    .local v3, _arg0:Landroid/location/LocationRequest;
    :goto_98
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@9b
    move-result v2

    #@9c
    if-eqz v2, :cond_c9

    #@9e
    .line 105
    sget-object v2, Landroid/location/Geofence;->CREATOR:Landroid/os/Parcelable$Creator;

    #@a0
    move-object/from16 v0, p2

    #@a2
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@a5
    move-result-object v5

    #@a6
    check-cast v5, Landroid/location/Geofence;

    #@a8
    .line 111
    .local v5, _arg1:Landroid/location/Geofence;
    :goto_a8
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@ab
    move-result v2

    #@ac
    if-eqz v2, :cond_cb

    #@ae
    .line 112
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@b0
    move-object/from16 v0, p2

    #@b2
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@b5
    move-result-object v7

    #@b6
    check-cast v7, Landroid/app/PendingIntent;

    #@b8
    .line 118
    .local v7, _arg2:Landroid/app/PendingIntent;
    :goto_b8
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@bb
    move-result-object v8

    #@bc
    .line 119
    .restart local v8       #_arg3:Ljava/lang/String;
    move-object/from16 v0, p0

    #@be
    invoke-virtual {v0, v3, v5, v7, v8}, Landroid/location/ILocationManager$Stub;->requestGeofence(Landroid/location/LocationRequest;Landroid/location/Geofence;Landroid/app/PendingIntent;Ljava/lang/String;)V

    #@c1
    .line 120
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@c4
    .line 121
    const/4 v2, 0x1

    #@c5
    goto/16 :goto_7

    #@c7
    .line 101
    .end local v3           #_arg0:Landroid/location/LocationRequest;
    .end local v5           #_arg1:Landroid/location/Geofence;
    .end local v7           #_arg2:Landroid/app/PendingIntent;
    .end local v8           #_arg3:Ljava/lang/String;
    :cond_c7
    const/4 v3, 0x0

    #@c8
    .restart local v3       #_arg0:Landroid/location/LocationRequest;
    goto :goto_98

    #@c9
    .line 108
    :cond_c9
    const/4 v5, 0x0

    #@ca
    .restart local v5       #_arg1:Landroid/location/Geofence;
    goto :goto_a8

    #@cb
    .line 115
    :cond_cb
    const/4 v7, 0x0

    #@cc
    .restart local v7       #_arg2:Landroid/app/PendingIntent;
    goto :goto_b8

    #@cd
    .line 125
    .end local v3           #_arg0:Landroid/location/LocationRequest;
    .end local v5           #_arg1:Landroid/location/Geofence;
    .end local v7           #_arg2:Landroid/app/PendingIntent;
    :sswitch_cd
    const-string v2, "android.location.ILocationManager"

    #@cf
    move-object/from16 v0, p2

    #@d1
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d4
    .line 127
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@d7
    move-result v2

    #@d8
    if-eqz v2, :cond_103

    #@da
    .line 128
    sget-object v2, Landroid/location/Geofence;->CREATOR:Landroid/os/Parcelable$Creator;

    #@dc
    move-object/from16 v0, p2

    #@de
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@e1
    move-result-object v3

    #@e2
    check-cast v3, Landroid/location/Geofence;

    #@e4
    .line 134
    .local v3, _arg0:Landroid/location/Geofence;
    :goto_e4
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@e7
    move-result v2

    #@e8
    if-eqz v2, :cond_105

    #@ea
    .line 135
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@ec
    move-object/from16 v0, p2

    #@ee
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@f1
    move-result-object v5

    #@f2
    check-cast v5, Landroid/app/PendingIntent;

    #@f4
    .line 141
    .local v5, _arg1:Landroid/app/PendingIntent;
    :goto_f4
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@f7
    move-result-object v7

    #@f8
    .line 142
    .local v7, _arg2:Ljava/lang/String;
    move-object/from16 v0, p0

    #@fa
    invoke-virtual {v0, v3, v5, v7}, Landroid/location/ILocationManager$Stub;->removeGeofence(Landroid/location/Geofence;Landroid/app/PendingIntent;Ljava/lang/String;)V

    #@fd
    .line 143
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@100
    .line 144
    const/4 v2, 0x1

    #@101
    goto/16 :goto_7

    #@103
    .line 131
    .end local v3           #_arg0:Landroid/location/Geofence;
    .end local v5           #_arg1:Landroid/app/PendingIntent;
    .end local v7           #_arg2:Ljava/lang/String;
    :cond_103
    const/4 v3, 0x0

    #@104
    .restart local v3       #_arg0:Landroid/location/Geofence;
    goto :goto_e4

    #@105
    .line 138
    :cond_105
    const/4 v5, 0x0

    #@106
    .restart local v5       #_arg1:Landroid/app/PendingIntent;
    goto :goto_f4

    #@107
    .line 148
    .end local v3           #_arg0:Landroid/location/Geofence;
    .end local v5           #_arg1:Landroid/app/PendingIntent;
    :sswitch_107
    const-string v2, "android.location.ILocationManager"

    #@109
    move-object/from16 v0, p2

    #@10b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@10e
    .line 150
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@111
    move-result v2

    #@112
    if-eqz v2, :cond_13e

    #@114
    .line 151
    sget-object v2, Landroid/location/LocationRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    #@116
    move-object/from16 v0, p2

    #@118
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@11b
    move-result-object v3

    #@11c
    check-cast v3, Landroid/location/LocationRequest;

    #@11e
    .line 157
    .local v3, _arg0:Landroid/location/LocationRequest;
    :goto_11e
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@121
    move-result-object v5

    #@122
    .line 158
    .local v5, _arg1:Ljava/lang/String;
    move-object/from16 v0, p0

    #@124
    invoke-virtual {v0, v3, v5}, Landroid/location/ILocationManager$Stub;->getLastLocation(Landroid/location/LocationRequest;Ljava/lang/String;)Landroid/location/Location;

    #@127
    move-result-object v29

    #@128
    .line 159
    .local v29, _result:Landroid/location/Location;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@12b
    .line 160
    if-eqz v29, :cond_140

    #@12d
    .line 161
    const/4 v2, 0x1

    #@12e
    move-object/from16 v0, p3

    #@130
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@133
    .line 162
    const/4 v2, 0x1

    #@134
    move-object/from16 v0, v29

    #@136
    move-object/from16 v1, p3

    #@138
    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->writeToParcel(Landroid/os/Parcel;I)V

    #@13b
    .line 167
    :goto_13b
    const/4 v2, 0x1

    #@13c
    goto/16 :goto_7

    #@13e
    .line 154
    .end local v3           #_arg0:Landroid/location/LocationRequest;
    .end local v5           #_arg1:Ljava/lang/String;
    .end local v29           #_result:Landroid/location/Location;
    :cond_13e
    const/4 v3, 0x0

    #@13f
    .restart local v3       #_arg0:Landroid/location/LocationRequest;
    goto :goto_11e

    #@140
    .line 165
    .restart local v5       #_arg1:Ljava/lang/String;
    .restart local v29       #_result:Landroid/location/Location;
    :cond_140
    const/4 v2, 0x0

    #@141
    move-object/from16 v0, p3

    #@143
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@146
    goto :goto_13b

    #@147
    .line 171
    .end local v3           #_arg0:Landroid/location/LocationRequest;
    .end local v5           #_arg1:Ljava/lang/String;
    .end local v29           #_result:Landroid/location/Location;
    :sswitch_147
    const-string v2, "android.location.ILocationManager"

    #@149
    move-object/from16 v0, p2

    #@14b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14e
    .line 173
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@151
    move-result-object v2

    #@152
    invoke-static {v2}, Landroid/location/IGpsStatusListener$Stub;->asInterface(Landroid/os/IBinder;)Landroid/location/IGpsStatusListener;

    #@155
    move-result-object v3

    #@156
    .line 174
    .local v3, _arg0:Landroid/location/IGpsStatusListener;
    move-object/from16 v0, p0

    #@158
    invoke-virtual {v0, v3}, Landroid/location/ILocationManager$Stub;->addGpsStatusListener(Landroid/location/IGpsStatusListener;)Z

    #@15b
    move-result v29

    #@15c
    .line 175
    .local v29, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@15f
    .line 176
    if-eqz v29, :cond_16a

    #@161
    const/4 v2, 0x1

    #@162
    :goto_162
    move-object/from16 v0, p3

    #@164
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@167
    .line 177
    const/4 v2, 0x1

    #@168
    goto/16 :goto_7

    #@16a
    .line 176
    :cond_16a
    const/4 v2, 0x0

    #@16b
    goto :goto_162

    #@16c
    .line 181
    .end local v3           #_arg0:Landroid/location/IGpsStatusListener;
    .end local v29           #_result:Z
    :sswitch_16c
    const-string v2, "android.location.ILocationManager"

    #@16e
    move-object/from16 v0, p2

    #@170
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@173
    .line 183
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@176
    move-result-object v2

    #@177
    invoke-static {v2}, Landroid/location/IGpsStatusListener$Stub;->asInterface(Landroid/os/IBinder;)Landroid/location/IGpsStatusListener;

    #@17a
    move-result-object v3

    #@17b
    .line 184
    .restart local v3       #_arg0:Landroid/location/IGpsStatusListener;
    move-object/from16 v0, p0

    #@17d
    invoke-virtual {v0, v3}, Landroid/location/ILocationManager$Stub;->removeGpsStatusListener(Landroid/location/IGpsStatusListener;)V

    #@180
    .line 185
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@183
    .line 186
    const/4 v2, 0x1

    #@184
    goto/16 :goto_7

    #@186
    .line 190
    .end local v3           #_arg0:Landroid/location/IGpsStatusListener;
    :sswitch_186
    const-string v2, "android.location.ILocationManager"

    #@188
    move-object/from16 v0, p2

    #@18a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18d
    .line 191
    invoke-virtual/range {p0 .. p0}, Landroid/location/ILocationManager$Stub;->geocoderIsPresent()Z

    #@190
    move-result v29

    #@191
    .line 192
    .restart local v29       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@194
    .line 193
    if-eqz v29, :cond_19f

    #@196
    const/4 v2, 0x1

    #@197
    :goto_197
    move-object/from16 v0, p3

    #@199
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@19c
    .line 194
    const/4 v2, 0x1

    #@19d
    goto/16 :goto_7

    #@19f
    .line 193
    :cond_19f
    const/4 v2, 0x0

    #@1a0
    goto :goto_197

    #@1a1
    .line 198
    .end local v29           #_result:Z
    :sswitch_1a1
    const-string v2, "android.location.ILocationManager"

    #@1a3
    move-object/from16 v0, p2

    #@1a5
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1a8
    .line 200
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readDouble()D

    #@1ab
    move-result-wide v3

    #@1ac
    .line 202
    .local v3, _arg0:D
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readDouble()D

    #@1af
    move-result-wide v5

    #@1b0
    .line 204
    .local v5, _arg1:D
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1b3
    move-result v7

    #@1b4
    .line 206
    .local v7, _arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1b7
    move-result v2

    #@1b8
    if-eqz v2, :cond_1e1

    #@1ba
    .line 207
    sget-object v2, Landroid/location/GeocoderParams;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1bc
    move-object/from16 v0, p2

    #@1be
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1c1
    move-result-object v8

    #@1c2
    check-cast v8, Landroid/location/GeocoderParams;

    #@1c4
    .line 213
    .local v8, _arg3:Landroid/location/GeocoderParams;
    :goto_1c4
    new-instance v9, Ljava/util/ArrayList;

    #@1c6
    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    #@1c9
    .local v9, _arg4:Ljava/util/List;,"Ljava/util/List<Landroid/location/Address;>;"
    move-object/from16 v2, p0

    #@1cb
    .line 214
    invoke-virtual/range {v2 .. v9}, Landroid/location/ILocationManager$Stub;->getFromLocation(DDILandroid/location/GeocoderParams;Ljava/util/List;)Ljava/lang/String;

    #@1ce
    move-result-object v29

    #@1cf
    .line 215
    .local v29, _result:Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1d2
    .line 216
    move-object/from16 v0, p3

    #@1d4
    move-object/from16 v1, v29

    #@1d6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1d9
    .line 217
    move-object/from16 v0, p3

    #@1db
    invoke-virtual {v0, v9}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@1de
    .line 218
    const/4 v2, 0x1

    #@1df
    goto/16 :goto_7

    #@1e1
    .line 210
    .end local v8           #_arg3:Landroid/location/GeocoderParams;
    .end local v9           #_arg4:Ljava/util/List;,"Ljava/util/List<Landroid/location/Address;>;"
    .end local v29           #_result:Ljava/lang/String;
    :cond_1e1
    const/4 v8, 0x0

    #@1e2
    .restart local v8       #_arg3:Landroid/location/GeocoderParams;
    goto :goto_1c4

    #@1e3
    .line 222
    .end local v3           #_arg0:D
    .end local v5           #_arg1:D
    .end local v7           #_arg2:I
    .end local v8           #_arg3:Landroid/location/GeocoderParams;
    :sswitch_1e3
    const-string v2, "android.location.ILocationManager"

    #@1e5
    move-object/from16 v0, p2

    #@1e7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1ea
    .line 224
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1ed
    move-result-object v3

    #@1ee
    .line 226
    .local v3, _arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readDouble()D

    #@1f1
    move-result-wide v5

    #@1f2
    .line 228
    .restart local v5       #_arg1:D
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readDouble()D

    #@1f5
    move-result-wide v14

    #@1f6
    .line 230
    .local v14, _arg2:D
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readDouble()D

    #@1f9
    move-result-wide v16

    #@1fa
    .line 232
    .local v16, _arg3:D
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readDouble()D

    #@1fd
    move-result-wide v18

    #@1fe
    .line 234
    .local v18, _arg4:D
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@201
    move-result v20

    #@202
    .line 236
    .local v20, _arg5:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@205
    move-result v2

    #@206
    if-eqz v2, :cond_233

    #@208
    .line 237
    sget-object v2, Landroid/location/GeocoderParams;->CREATOR:Landroid/os/Parcelable$Creator;

    #@20a
    move-object/from16 v0, p2

    #@20c
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@20f
    move-result-object v21

    #@210
    check-cast v21, Landroid/location/GeocoderParams;

    #@212
    .line 243
    .local v21, _arg6:Landroid/location/GeocoderParams;
    :goto_212
    new-instance v22, Ljava/util/ArrayList;

    #@214
    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    #@217
    .local v22, _arg7:Ljava/util/List;,"Ljava/util/List<Landroid/location/Address;>;"
    move-object/from16 v10, p0

    #@219
    move-object v11, v3

    #@21a
    move-wide v12, v5

    #@21b
    .line 244
    invoke-virtual/range {v10 .. v22}, Landroid/location/ILocationManager$Stub;->getFromLocationName(Ljava/lang/String;DDDDILandroid/location/GeocoderParams;Ljava/util/List;)Ljava/lang/String;

    #@21e
    move-result-object v29

    #@21f
    .line 245
    .restart local v29       #_result:Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@222
    .line 246
    move-object/from16 v0, p3

    #@224
    move-object/from16 v1, v29

    #@226
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@229
    .line 247
    move-object/from16 v0, p3

    #@22b
    move-object/from16 v1, v22

    #@22d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@230
    .line 248
    const/4 v2, 0x1

    #@231
    goto/16 :goto_7

    #@233
    .line 240
    .end local v21           #_arg6:Landroid/location/GeocoderParams;
    .end local v22           #_arg7:Ljava/util/List;,"Ljava/util/List<Landroid/location/Address;>;"
    .end local v29           #_result:Ljava/lang/String;
    :cond_233
    const/16 v21, 0x0

    #@235
    .restart local v21       #_arg6:Landroid/location/GeocoderParams;
    goto :goto_212

    #@236
    .line 252
    .end local v3           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:D
    .end local v14           #_arg2:D
    .end local v16           #_arg3:D
    .end local v18           #_arg4:D
    .end local v20           #_arg5:I
    .end local v21           #_arg6:Landroid/location/GeocoderParams;
    :sswitch_236
    const-string v2, "android.location.ILocationManager"

    #@238
    move-object/from16 v0, p2

    #@23a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@23d
    .line 254
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@240
    move-result v3

    #@241
    .line 256
    .local v3, _arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@244
    move-result v5

    #@245
    .line 257
    .local v5, _arg1:I
    move-object/from16 v0, p0

    #@247
    invoke-virtual {v0, v3, v5}, Landroid/location/ILocationManager$Stub;->sendNiResponse(II)Z

    #@24a
    move-result v29

    #@24b
    .line 258
    .local v29, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@24e
    .line 259
    if-eqz v29, :cond_259

    #@250
    const/4 v2, 0x1

    #@251
    :goto_251
    move-object/from16 v0, p3

    #@253
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@256
    .line 260
    const/4 v2, 0x1

    #@257
    goto/16 :goto_7

    #@259
    .line 259
    :cond_259
    const/4 v2, 0x0

    #@25a
    goto :goto_251

    #@25b
    .line 264
    .end local v3           #_arg0:I
    .end local v5           #_arg1:I
    .end local v29           #_result:Z
    :sswitch_25b
    const-string v2, "android.location.ILocationManager"

    #@25d
    move-object/from16 v0, p2

    #@25f
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@262
    .line 265
    invoke-virtual/range {p0 .. p0}, Landroid/location/ILocationManager$Stub;->getAllProviders()Ljava/util/List;

    #@265
    move-result-object v30

    #@266
    .line 266
    .local v30, _result:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@269
    .line 267
    move-object/from16 v0, p3

    #@26b
    move-object/from16 v1, v30

    #@26d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    #@270
    .line 268
    const/4 v2, 0x1

    #@271
    goto/16 :goto_7

    #@273
    .line 272
    .end local v30           #_result:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_273
    const-string v2, "android.location.ILocationManager"

    #@275
    move-object/from16 v0, p2

    #@277
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@27a
    .line 274
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@27d
    move-result v2

    #@27e
    if-eqz v2, :cond_2a4

    #@280
    .line 275
    sget-object v2, Landroid/location/Criteria;->CREATOR:Landroid/os/Parcelable$Creator;

    #@282
    move-object/from16 v0, p2

    #@284
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@287
    move-result-object v3

    #@288
    check-cast v3, Landroid/location/Criteria;

    #@28a
    .line 281
    .local v3, _arg0:Landroid/location/Criteria;
    :goto_28a
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@28d
    move-result v2

    #@28e
    if-eqz v2, :cond_2a6

    #@290
    const/4 v5, 0x1

    #@291
    .line 282
    .local v5, _arg1:Z
    :goto_291
    move-object/from16 v0, p0

    #@293
    invoke-virtual {v0, v3, v5}, Landroid/location/ILocationManager$Stub;->getProviders(Landroid/location/Criteria;Z)Ljava/util/List;

    #@296
    move-result-object v30

    #@297
    .line 283
    .restart local v30       #_result:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@29a
    .line 284
    move-object/from16 v0, p3

    #@29c
    move-object/from16 v1, v30

    #@29e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    #@2a1
    .line 285
    const/4 v2, 0x1

    #@2a2
    goto/16 :goto_7

    #@2a4
    .line 278
    .end local v3           #_arg0:Landroid/location/Criteria;
    .end local v5           #_arg1:Z
    .end local v30           #_result:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :cond_2a4
    const/4 v3, 0x0

    #@2a5
    .restart local v3       #_arg0:Landroid/location/Criteria;
    goto :goto_28a

    #@2a6
    .line 281
    :cond_2a6
    const/4 v5, 0x0

    #@2a7
    goto :goto_291

    #@2a8
    .line 289
    .end local v3           #_arg0:Landroid/location/Criteria;
    :sswitch_2a8
    const-string v2, "android.location.ILocationManager"

    #@2aa
    move-object/from16 v0, p2

    #@2ac
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2af
    .line 291
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2b2
    move-result v2

    #@2b3
    if-eqz v2, :cond_2d9

    #@2b5
    .line 292
    sget-object v2, Landroid/location/Criteria;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2b7
    move-object/from16 v0, p2

    #@2b9
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2bc
    move-result-object v3

    #@2bd
    check-cast v3, Landroid/location/Criteria;

    #@2bf
    .line 298
    .restart local v3       #_arg0:Landroid/location/Criteria;
    :goto_2bf
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2c2
    move-result v2

    #@2c3
    if-eqz v2, :cond_2db

    #@2c5
    const/4 v5, 0x1

    #@2c6
    .line 299
    .restart local v5       #_arg1:Z
    :goto_2c6
    move-object/from16 v0, p0

    #@2c8
    invoke-virtual {v0, v3, v5}, Landroid/location/ILocationManager$Stub;->getBestProvider(Landroid/location/Criteria;Z)Ljava/lang/String;

    #@2cb
    move-result-object v29

    #@2cc
    .line 300
    .local v29, _result:Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2cf
    .line 301
    move-object/from16 v0, p3

    #@2d1
    move-object/from16 v1, v29

    #@2d3
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2d6
    .line 302
    const/4 v2, 0x1

    #@2d7
    goto/16 :goto_7

    #@2d9
    .line 295
    .end local v3           #_arg0:Landroid/location/Criteria;
    .end local v5           #_arg1:Z
    .end local v29           #_result:Ljava/lang/String;
    :cond_2d9
    const/4 v3, 0x0

    #@2da
    .restart local v3       #_arg0:Landroid/location/Criteria;
    goto :goto_2bf

    #@2db
    .line 298
    :cond_2db
    const/4 v5, 0x0

    #@2dc
    goto :goto_2c6

    #@2dd
    .line 306
    .end local v3           #_arg0:Landroid/location/Criteria;
    :sswitch_2dd
    const-string v2, "android.location.ILocationManager"

    #@2df
    move-object/from16 v0, p2

    #@2e1
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2e4
    .line 308
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2e7
    move-result-object v3

    #@2e8
    .line 310
    .local v3, _arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2eb
    move-result v2

    #@2ec
    if-eqz v2, :cond_30c

    #@2ee
    .line 311
    sget-object v2, Landroid/location/Criteria;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2f0
    move-object/from16 v0, p2

    #@2f2
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2f5
    move-result-object v5

    #@2f6
    check-cast v5, Landroid/location/Criteria;

    #@2f8
    .line 316
    .local v5, _arg1:Landroid/location/Criteria;
    :goto_2f8
    move-object/from16 v0, p0

    #@2fa
    invoke-virtual {v0, v3, v5}, Landroid/location/ILocationManager$Stub;->providerMeetsCriteria(Ljava/lang/String;Landroid/location/Criteria;)Z

    #@2fd
    move-result v29

    #@2fe
    .line 317
    .local v29, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@301
    .line 318
    if-eqz v29, :cond_30e

    #@303
    const/4 v2, 0x1

    #@304
    :goto_304
    move-object/from16 v0, p3

    #@306
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@309
    .line 319
    const/4 v2, 0x1

    #@30a
    goto/16 :goto_7

    #@30c
    .line 314
    .end local v5           #_arg1:Landroid/location/Criteria;
    .end local v29           #_result:Z
    :cond_30c
    const/4 v5, 0x0

    #@30d
    .restart local v5       #_arg1:Landroid/location/Criteria;
    goto :goto_2f8

    #@30e
    .line 318
    .restart local v29       #_result:Z
    :cond_30e
    const/4 v2, 0x0

    #@30f
    goto :goto_304

    #@310
    .line 323
    .end local v3           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:Landroid/location/Criteria;
    .end local v29           #_result:Z
    :sswitch_310
    const-string v2, "android.location.ILocationManager"

    #@312
    move-object/from16 v0, p2

    #@314
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@317
    .line 325
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@31a
    move-result-object v3

    #@31b
    .line 326
    .restart local v3       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@31d
    invoke-virtual {v0, v3}, Landroid/location/ILocationManager$Stub;->getProviderProperties(Ljava/lang/String;)Lcom/android/internal/location/ProviderProperties;

    #@320
    move-result-object v29

    #@321
    .line 327
    .local v29, _result:Lcom/android/internal/location/ProviderProperties;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@324
    .line 328
    if-eqz v29, :cond_337

    #@326
    .line 329
    const/4 v2, 0x1

    #@327
    move-object/from16 v0, p3

    #@329
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@32c
    .line 330
    const/4 v2, 0x1

    #@32d
    move-object/from16 v0, v29

    #@32f
    move-object/from16 v1, p3

    #@331
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/location/ProviderProperties;->writeToParcel(Landroid/os/Parcel;I)V

    #@334
    .line 335
    :goto_334
    const/4 v2, 0x1

    #@335
    goto/16 :goto_7

    #@337
    .line 333
    :cond_337
    const/4 v2, 0x0

    #@338
    move-object/from16 v0, p3

    #@33a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@33d
    goto :goto_334

    #@33e
    .line 339
    .end local v3           #_arg0:Ljava/lang/String;
    .end local v29           #_result:Lcom/android/internal/location/ProviderProperties;
    :sswitch_33e
    const-string v2, "android.location.ILocationManager"

    #@340
    move-object/from16 v0, p2

    #@342
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@345
    .line 341
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@348
    move-result-object v3

    #@349
    .line 342
    .restart local v3       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@34b
    invoke-virtual {v0, v3}, Landroid/location/ILocationManager$Stub;->isProviderEnabled(Ljava/lang/String;)Z

    #@34e
    move-result v29

    #@34f
    .line 343
    .local v29, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@352
    .line 344
    if-eqz v29, :cond_35d

    #@354
    const/4 v2, 0x1

    #@355
    :goto_355
    move-object/from16 v0, p3

    #@357
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@35a
    .line 345
    const/4 v2, 0x1

    #@35b
    goto/16 :goto_7

    #@35d
    .line 344
    :cond_35d
    const/4 v2, 0x0

    #@35e
    goto :goto_355

    #@35f
    .line 349
    .end local v3           #_arg0:Ljava/lang/String;
    .end local v29           #_result:Z
    :sswitch_35f
    const-string v2, "android.location.ILocationManager"

    #@361
    move-object/from16 v0, p2

    #@363
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@366
    .line 351
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@369
    move-result-object v3

    #@36a
    .line 353
    .restart local v3       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@36d
    move-result v2

    #@36e
    if-eqz v2, :cond_385

    #@370
    .line 354
    sget-object v2, Lcom/android/internal/location/ProviderProperties;->CREATOR:Landroid/os/Parcelable$Creator;

    #@372
    move-object/from16 v0, p2

    #@374
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@377
    move-result-object v5

    #@378
    check-cast v5, Lcom/android/internal/location/ProviderProperties;

    #@37a
    .line 359
    .local v5, _arg1:Lcom/android/internal/location/ProviderProperties;
    :goto_37a
    move-object/from16 v0, p0

    #@37c
    invoke-virtual {v0, v3, v5}, Landroid/location/ILocationManager$Stub;->addTestProvider(Ljava/lang/String;Lcom/android/internal/location/ProviderProperties;)V

    #@37f
    .line 360
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@382
    .line 361
    const/4 v2, 0x1

    #@383
    goto/16 :goto_7

    #@385
    .line 357
    .end local v5           #_arg1:Lcom/android/internal/location/ProviderProperties;
    :cond_385
    const/4 v5, 0x0

    #@386
    .restart local v5       #_arg1:Lcom/android/internal/location/ProviderProperties;
    goto :goto_37a

    #@387
    .line 365
    .end local v3           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:Lcom/android/internal/location/ProviderProperties;
    :sswitch_387
    const-string v2, "android.location.ILocationManager"

    #@389
    move-object/from16 v0, p2

    #@38b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@38e
    .line 367
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@391
    move-result-object v3

    #@392
    .line 368
    .restart local v3       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@394
    invoke-virtual {v0, v3}, Landroid/location/ILocationManager$Stub;->removeTestProvider(Ljava/lang/String;)V

    #@397
    .line 369
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@39a
    .line 370
    const/4 v2, 0x1

    #@39b
    goto/16 :goto_7

    #@39d
    .line 374
    .end local v3           #_arg0:Ljava/lang/String;
    :sswitch_39d
    const-string v2, "android.location.ILocationManager"

    #@39f
    move-object/from16 v0, p2

    #@3a1
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3a4
    .line 376
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3a7
    move-result-object v3

    #@3a8
    .line 378
    .restart local v3       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3ab
    move-result v2

    #@3ac
    if-eqz v2, :cond_3c3

    #@3ae
    .line 379
    sget-object v2, Landroid/location/Location;->CREATOR:Landroid/os/Parcelable$Creator;

    #@3b0
    move-object/from16 v0, p2

    #@3b2
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@3b5
    move-result-object v5

    #@3b6
    check-cast v5, Landroid/location/Location;

    #@3b8
    .line 384
    .local v5, _arg1:Landroid/location/Location;
    :goto_3b8
    move-object/from16 v0, p0

    #@3ba
    invoke-virtual {v0, v3, v5}, Landroid/location/ILocationManager$Stub;->setTestProviderLocation(Ljava/lang/String;Landroid/location/Location;)V

    #@3bd
    .line 385
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3c0
    .line 386
    const/4 v2, 0x1

    #@3c1
    goto/16 :goto_7

    #@3c3
    .line 382
    .end local v5           #_arg1:Landroid/location/Location;
    :cond_3c3
    const/4 v5, 0x0

    #@3c4
    .restart local v5       #_arg1:Landroid/location/Location;
    goto :goto_3b8

    #@3c5
    .line 390
    .end local v3           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:Landroid/location/Location;
    :sswitch_3c5
    const-string v2, "android.location.ILocationManager"

    #@3c7
    move-object/from16 v0, p2

    #@3c9
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3cc
    .line 392
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3cf
    move-result-object v3

    #@3d0
    .line 393
    .restart local v3       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@3d2
    invoke-virtual {v0, v3}, Landroid/location/ILocationManager$Stub;->clearTestProviderLocation(Ljava/lang/String;)V

    #@3d5
    .line 394
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3d8
    .line 395
    const/4 v2, 0x1

    #@3d9
    goto/16 :goto_7

    #@3db
    .line 399
    .end local v3           #_arg0:Ljava/lang/String;
    :sswitch_3db
    const-string v2, "android.location.ILocationManager"

    #@3dd
    move-object/from16 v0, p2

    #@3df
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3e2
    .line 401
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3e5
    move-result-object v3

    #@3e6
    .line 403
    .restart local v3       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3e9
    move-result v2

    #@3ea
    if-eqz v2, :cond_3f8

    #@3ec
    const/4 v5, 0x1

    #@3ed
    .line 404
    .local v5, _arg1:Z
    :goto_3ed
    move-object/from16 v0, p0

    #@3ef
    invoke-virtual {v0, v3, v5}, Landroid/location/ILocationManager$Stub;->setTestProviderEnabled(Ljava/lang/String;Z)V

    #@3f2
    .line 405
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3f5
    .line 406
    const/4 v2, 0x1

    #@3f6
    goto/16 :goto_7

    #@3f8
    .line 403
    .end local v5           #_arg1:Z
    :cond_3f8
    const/4 v5, 0x0

    #@3f9
    goto :goto_3ed

    #@3fa
    .line 410
    .end local v3           #_arg0:Ljava/lang/String;
    :sswitch_3fa
    const-string v2, "android.location.ILocationManager"

    #@3fc
    move-object/from16 v0, p2

    #@3fe
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@401
    .line 412
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@404
    move-result-object v3

    #@405
    .line 413
    .restart local v3       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@407
    invoke-virtual {v0, v3}, Landroid/location/ILocationManager$Stub;->clearTestProviderEnabled(Ljava/lang/String;)V

    #@40a
    .line 414
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@40d
    .line 415
    const/4 v2, 0x1

    #@40e
    goto/16 :goto_7

    #@410
    .line 419
    .end local v3           #_arg0:Ljava/lang/String;
    :sswitch_410
    const-string v2, "android.location.ILocationManager"

    #@412
    move-object/from16 v0, p2

    #@414
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@417
    .line 421
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@41a
    move-result-object v3

    #@41b
    .line 423
    .restart local v3       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@41e
    move-result v5

    #@41f
    .line 425
    .local v5, _arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@422
    move-result v2

    #@423
    if-eqz v2, :cond_446

    #@425
    .line 426
    sget-object v2, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@427
    move-object/from16 v0, p2

    #@429
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@42c
    move-result-object v7

    #@42d
    check-cast v7, Landroid/os/Bundle;

    #@42f
    .line 432
    .local v7, _arg2:Landroid/os/Bundle;
    :goto_42f
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@432
    move-result-wide v16

    #@433
    .local v16, _arg3:J
    move-object/from16 v23, p0

    #@435
    move-object/from16 v24, v3

    #@437
    move/from16 v25, v5

    #@439
    move-object/from16 v26, v7

    #@43b
    move-wide/from16 v27, v16

    #@43d
    .line 433
    invoke-virtual/range {v23 .. v28}, Landroid/location/ILocationManager$Stub;->setTestProviderStatus(Ljava/lang/String;ILandroid/os/Bundle;J)V

    #@440
    .line 434
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@443
    .line 435
    const/4 v2, 0x1

    #@444
    goto/16 :goto_7

    #@446
    .line 429
    .end local v7           #_arg2:Landroid/os/Bundle;
    .end local v16           #_arg3:J
    :cond_446
    const/4 v7, 0x0

    #@447
    .restart local v7       #_arg2:Landroid/os/Bundle;
    goto :goto_42f

    #@448
    .line 439
    .end local v3           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:I
    .end local v7           #_arg2:Landroid/os/Bundle;
    :sswitch_448
    const-string v2, "android.location.ILocationManager"

    #@44a
    move-object/from16 v0, p2

    #@44c
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@44f
    .line 441
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@452
    move-result-object v3

    #@453
    .line 442
    .restart local v3       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@455
    invoke-virtual {v0, v3}, Landroid/location/ILocationManager$Stub;->clearTestProviderStatus(Ljava/lang/String;)V

    #@458
    .line 443
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@45b
    .line 444
    const/4 v2, 0x1

    #@45c
    goto/16 :goto_7

    #@45e
    .line 448
    .end local v3           #_arg0:Ljava/lang/String;
    :sswitch_45e
    const-string v2, "android.location.ILocationManager"

    #@460
    move-object/from16 v0, p2

    #@462
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@465
    .line 450
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@468
    move-result-object v3

    #@469
    .line 452
    .restart local v3       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@46c
    move-result-object v5

    #@46d
    .line 454
    .local v5, _arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@470
    move-result v2

    #@471
    if-eqz v2, :cond_49f

    #@473
    .line 455
    sget-object v2, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@475
    move-object/from16 v0, p2

    #@477
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@47a
    move-result-object v7

    #@47b
    check-cast v7, Landroid/os/Bundle;

    #@47d
    .line 460
    .restart local v7       #_arg2:Landroid/os/Bundle;
    :goto_47d
    move-object/from16 v0, p0

    #@47f
    invoke-virtual {v0, v3, v5, v7}, Landroid/location/ILocationManager$Stub;->sendExtraCommand(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Z

    #@482
    move-result v29

    #@483
    .line 461
    .restart local v29       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@486
    .line 462
    if-eqz v29, :cond_4a1

    #@488
    const/4 v2, 0x1

    #@489
    :goto_489
    move-object/from16 v0, p3

    #@48b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@48e
    .line 463
    if-eqz v7, :cond_4a3

    #@490
    .line 464
    const/4 v2, 0x1

    #@491
    move-object/from16 v0, p3

    #@493
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@496
    .line 465
    const/4 v2, 0x1

    #@497
    move-object/from16 v0, p3

    #@499
    invoke-virtual {v7, v0, v2}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@49c
    .line 470
    :goto_49c
    const/4 v2, 0x1

    #@49d
    goto/16 :goto_7

    #@49f
    .line 458
    .end local v7           #_arg2:Landroid/os/Bundle;
    .end local v29           #_result:Z
    :cond_49f
    const/4 v7, 0x0

    #@4a0
    .restart local v7       #_arg2:Landroid/os/Bundle;
    goto :goto_47d

    #@4a1
    .line 462
    .restart local v29       #_result:Z
    :cond_4a1
    const/4 v2, 0x0

    #@4a2
    goto :goto_489

    #@4a3
    .line 468
    :cond_4a3
    const/4 v2, 0x0

    #@4a4
    move-object/from16 v0, p3

    #@4a6
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@4a9
    goto :goto_49c

    #@4aa
    .line 474
    .end local v3           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:Ljava/lang/String;
    .end local v7           #_arg2:Landroid/os/Bundle;
    .end local v29           #_result:Z
    :sswitch_4aa
    const-string v2, "android.location.ILocationManager"

    #@4ac
    move-object/from16 v0, p2

    #@4ae
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4b1
    .line 476
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@4b4
    move-result v2

    #@4b5
    if-eqz v2, :cond_4d3

    #@4b7
    .line 477
    sget-object v2, Landroid/location/Location;->CREATOR:Landroid/os/Parcelable$Creator;

    #@4b9
    move-object/from16 v0, p2

    #@4bb
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@4be
    move-result-object v3

    #@4bf
    check-cast v3, Landroid/location/Location;

    #@4c1
    .line 483
    .local v3, _arg0:Landroid/location/Location;
    :goto_4c1
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@4c4
    move-result v2

    #@4c5
    if-eqz v2, :cond_4d5

    #@4c7
    const/4 v5, 0x1

    #@4c8
    .line 484
    .local v5, _arg1:Z
    :goto_4c8
    move-object/from16 v0, p0

    #@4ca
    invoke-virtual {v0, v3, v5}, Landroid/location/ILocationManager$Stub;->reportLocation(Landroid/location/Location;Z)V

    #@4cd
    .line 485
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@4d0
    .line 486
    const/4 v2, 0x1

    #@4d1
    goto/16 :goto_7

    #@4d3
    .line 480
    .end local v3           #_arg0:Landroid/location/Location;
    .end local v5           #_arg1:Z
    :cond_4d3
    const/4 v3, 0x0

    #@4d4
    .restart local v3       #_arg0:Landroid/location/Location;
    goto :goto_4c1

    #@4d5
    .line 483
    :cond_4d5
    const/4 v5, 0x0

    #@4d6
    goto :goto_4c8

    #@4d7
    .line 490
    .end local v3           #_arg0:Landroid/location/Location;
    :sswitch_4d7
    const-string v2, "android.location.ILocationManager"

    #@4d9
    move-object/from16 v0, p2

    #@4db
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4de
    .line 492
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@4e1
    move-result-object v2

    #@4e2
    invoke-static {v2}, Landroid/location/ILocationListener$Stub;->asInterface(Landroid/os/IBinder;)Landroid/location/ILocationListener;

    #@4e5
    move-result-object v3

    #@4e6
    .line 493
    .local v3, _arg0:Landroid/location/ILocationListener;
    move-object/from16 v0, p0

    #@4e8
    invoke-virtual {v0, v3}, Landroid/location/ILocationManager$Stub;->locationCallbackFinished(Landroid/location/ILocationListener;)V

    #@4eb
    .line 494
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@4ee
    .line 495
    const/4 v2, 0x1

    #@4ef
    goto/16 :goto_7

    #@4f1
    .line 43
    nop

    #@4f2
    :sswitch_data_4f2
    .sparse-switch
        0x1 -> :sswitch_11
        0x2 -> :sswitch_52
        0x3 -> :sswitch_81
        0x4 -> :sswitch_cd
        0x5 -> :sswitch_107
        0x6 -> :sswitch_147
        0x7 -> :sswitch_16c
        0x8 -> :sswitch_186
        0x9 -> :sswitch_1a1
        0xa -> :sswitch_1e3
        0xb -> :sswitch_236
        0xc -> :sswitch_25b
        0xd -> :sswitch_273
        0xe -> :sswitch_2a8
        0xf -> :sswitch_2dd
        0x10 -> :sswitch_310
        0x11 -> :sswitch_33e
        0x12 -> :sswitch_35f
        0x13 -> :sswitch_387
        0x14 -> :sswitch_39d
        0x15 -> :sswitch_3c5
        0x16 -> :sswitch_3db
        0x17 -> :sswitch_3fa
        0x18 -> :sswitch_410
        0x19 -> :sswitch_448
        0x1a -> :sswitch_45e
        0x1b -> :sswitch_4aa
        0x1c -> :sswitch_4d7
        0x5f4e5446 -> :sswitch_8
    .end sparse-switch
.end method
