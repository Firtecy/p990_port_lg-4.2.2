.class public final Landroid/location/GpsSatellite;
.super Ljava/lang/Object;
.source "GpsSatellite.java"


# instance fields
.field mAzimuth:F

.field mElevation:F

.field mHasAlmanac:Z

.field mHasEphemeris:Z

.field mPrn:I

.field mSnr:F

.field mUsedInFix:Z

.field mValid:Z


# direct methods
.method constructor <init>(I)V
    .registers 2
    .parameter "prn"

    #@0
    .prologue
    .line 34
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 35
    iput p1, p0, Landroid/location/GpsSatellite;->mPrn:I

    #@5
    .line 36
    return-void
.end method


# virtual methods
.method public getAzimuth()F
    .registers 2

    #@0
    .prologue
    .line 87
    iget v0, p0, Landroid/location/GpsSatellite;->mAzimuth:F

    #@2
    return v0
.end method

.method public getElevation()F
    .registers 2

    #@0
    .prologue
    .line 77
    iget v0, p0, Landroid/location/GpsSatellite;->mElevation:F

    #@2
    return v0
.end method

.method public getPrn()I
    .registers 2

    #@0
    .prologue
    .line 58
    iget v0, p0, Landroid/location/GpsSatellite;->mPrn:I

    #@2
    return v0
.end method

.method public getSnr()F
    .registers 2

    #@0
    .prologue
    .line 67
    iget v0, p0, Landroid/location/GpsSatellite;->mSnr:F

    #@2
    return v0
.end method

.method public hasAlmanac()Z
    .registers 2

    #@0
    .prologue
    .line 105
    iget-boolean v0, p0, Landroid/location/GpsSatellite;->mHasAlmanac:Z

    #@2
    return v0
.end method

.method public hasEphemeris()Z
    .registers 2

    #@0
    .prologue
    .line 96
    iget-boolean v0, p0, Landroid/location/GpsSatellite;->mHasEphemeris:Z

    #@2
    return v0
.end method

.method setStatus(Landroid/location/GpsSatellite;)V
    .registers 3
    .parameter "satellite"

    #@0
    .prologue
    .line 43
    iget-boolean v0, p1, Landroid/location/GpsSatellite;->mValid:Z

    #@2
    iput-boolean v0, p0, Landroid/location/GpsSatellite;->mValid:Z

    #@4
    .line 44
    iget-boolean v0, p1, Landroid/location/GpsSatellite;->mHasEphemeris:Z

    #@6
    iput-boolean v0, p0, Landroid/location/GpsSatellite;->mHasEphemeris:Z

    #@8
    .line 45
    iget-boolean v0, p1, Landroid/location/GpsSatellite;->mHasAlmanac:Z

    #@a
    iput-boolean v0, p0, Landroid/location/GpsSatellite;->mHasAlmanac:Z

    #@c
    .line 46
    iget-boolean v0, p1, Landroid/location/GpsSatellite;->mUsedInFix:Z

    #@e
    iput-boolean v0, p0, Landroid/location/GpsSatellite;->mUsedInFix:Z

    #@10
    .line 47
    iget v0, p1, Landroid/location/GpsSatellite;->mSnr:F

    #@12
    iput v0, p0, Landroid/location/GpsSatellite;->mSnr:F

    #@14
    .line 48
    iget v0, p1, Landroid/location/GpsSatellite;->mElevation:F

    #@16
    iput v0, p0, Landroid/location/GpsSatellite;->mElevation:F

    #@18
    .line 49
    iget v0, p1, Landroid/location/GpsSatellite;->mAzimuth:F

    #@1a
    iput v0, p0, Landroid/location/GpsSatellite;->mAzimuth:F

    #@1c
    .line 50
    return-void
.end method

.method public usedInFix()Z
    .registers 2

    #@0
    .prologue
    .line 115
    iget-boolean v0, p0, Landroid/location/GpsSatellite;->mUsedInFix:Z

    #@2
    return v0
.end method
