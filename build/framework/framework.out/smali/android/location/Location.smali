.class public Landroid/location/Location;
.super Ljava/lang/Object;
.source "Location.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end field

.field public static final EXTRA_COARSE_LOCATION:Ljava/lang/String; = "coarseLocation"

.field public static final EXTRA_NO_GPS_LOCATION:Ljava/lang/String; = "noGPSLocation"

.field public static final FORMAT_DEGREES:I = 0x0

.field public static final FORMAT_MINUTES:I = 0x1

.field public static final FORMAT_SECONDS:I = 0x2


# instance fields
.field private mAccuracy:F

.field private mAltitude:D

.field private mBearing:F

.field private mDistance:F

.field private mElapsedRealtimeNanos:J

.field private mExtras:Landroid/os/Bundle;

.field private mHasAccuracy:Z

.field private mHasAltitude:Z

.field private mHasBearing:Z

.field private mHasSpeed:Z

.field private mInitialBearing:F

.field private mLat1:D

.field private mLat2:D

.field private mLatitude:D

.field private mLon1:D

.field private mLon2:D

.field private mLongitude:D

.field private mProvider:Ljava/lang/String;

.field private final mResults:[F

.field private mSpeed:F

.field private mTime:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 855
    new-instance v0, Landroid/location/Location$1;

    #@2
    invoke-direct {v0}, Landroid/location/Location$1;-><init>()V

    #@5
    sput-object v0, Landroid/location/Location;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/location/Location;)V
    .registers 8
    .parameter "l"

    #@0
    .prologue
    const-wide/16 v4, 0x0

    #@2
    const/4 v0, 0x0

    #@3
    const/4 v3, 0x0

    #@4
    const-wide/16 v1, 0x0

    #@6
    .line 121
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@9
    .line 81
    iput-wide v4, p0, Landroid/location/Location;->mTime:J

    #@b
    .line 82
    iput-wide v4, p0, Landroid/location/Location;->mElapsedRealtimeNanos:J

    #@d
    .line 83
    iput-wide v1, p0, Landroid/location/Location;->mLatitude:D

    #@f
    .line 84
    iput-wide v1, p0, Landroid/location/Location;->mLongitude:D

    #@11
    .line 85
    iput-boolean v0, p0, Landroid/location/Location;->mHasAltitude:Z

    #@13
    .line 86
    iput-wide v1, p0, Landroid/location/Location;->mAltitude:D

    #@15
    .line 87
    iput-boolean v0, p0, Landroid/location/Location;->mHasSpeed:Z

    #@17
    .line 88
    iput v3, p0, Landroid/location/Location;->mSpeed:F

    #@19
    .line 89
    iput-boolean v0, p0, Landroid/location/Location;->mHasBearing:Z

    #@1b
    .line 90
    iput v3, p0, Landroid/location/Location;->mBearing:F

    #@1d
    .line 91
    iput-boolean v0, p0, Landroid/location/Location;->mHasAccuracy:Z

    #@1f
    .line 92
    iput v3, p0, Landroid/location/Location;->mAccuracy:F

    #@21
    .line 93
    const/4 v0, 0x0

    #@22
    iput-object v0, p0, Landroid/location/Location;->mExtras:Landroid/os/Bundle;

    #@24
    .line 97
    iput-wide v1, p0, Landroid/location/Location;->mLat1:D

    #@26
    .line 98
    iput-wide v1, p0, Landroid/location/Location;->mLon1:D

    #@28
    .line 99
    iput-wide v1, p0, Landroid/location/Location;->mLat2:D

    #@2a
    .line 100
    iput-wide v1, p0, Landroid/location/Location;->mLon2:D

    #@2c
    .line 101
    iput v3, p0, Landroid/location/Location;->mDistance:F

    #@2e
    .line 102
    iput v3, p0, Landroid/location/Location;->mInitialBearing:F

    #@30
    .line 104
    const/4 v0, 0x2

    #@31
    new-array v0, v0, [F

    #@33
    iput-object v0, p0, Landroid/location/Location;->mResults:[F

    #@35
    .line 122
    invoke-virtual {p0, p1}, Landroid/location/Location;->set(Landroid/location/Location;)V

    #@38
    .line 123
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 8
    .parameter "provider"

    #@0
    .prologue
    const-wide/16 v4, 0x0

    #@2
    const/4 v0, 0x0

    #@3
    const/4 v3, 0x0

    #@4
    const-wide/16 v1, 0x0

    #@6
    .line 114
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@9
    .line 81
    iput-wide v4, p0, Landroid/location/Location;->mTime:J

    #@b
    .line 82
    iput-wide v4, p0, Landroid/location/Location;->mElapsedRealtimeNanos:J

    #@d
    .line 83
    iput-wide v1, p0, Landroid/location/Location;->mLatitude:D

    #@f
    .line 84
    iput-wide v1, p0, Landroid/location/Location;->mLongitude:D

    #@11
    .line 85
    iput-boolean v0, p0, Landroid/location/Location;->mHasAltitude:Z

    #@13
    .line 86
    iput-wide v1, p0, Landroid/location/Location;->mAltitude:D

    #@15
    .line 87
    iput-boolean v0, p0, Landroid/location/Location;->mHasSpeed:Z

    #@17
    .line 88
    iput v3, p0, Landroid/location/Location;->mSpeed:F

    #@19
    .line 89
    iput-boolean v0, p0, Landroid/location/Location;->mHasBearing:Z

    #@1b
    .line 90
    iput v3, p0, Landroid/location/Location;->mBearing:F

    #@1d
    .line 91
    iput-boolean v0, p0, Landroid/location/Location;->mHasAccuracy:Z

    #@1f
    .line 92
    iput v3, p0, Landroid/location/Location;->mAccuracy:F

    #@21
    .line 93
    const/4 v0, 0x0

    #@22
    iput-object v0, p0, Landroid/location/Location;->mExtras:Landroid/os/Bundle;

    #@24
    .line 97
    iput-wide v1, p0, Landroid/location/Location;->mLat1:D

    #@26
    .line 98
    iput-wide v1, p0, Landroid/location/Location;->mLon1:D

    #@28
    .line 99
    iput-wide v1, p0, Landroid/location/Location;->mLat2:D

    #@2a
    .line 100
    iput-wide v1, p0, Landroid/location/Location;->mLon2:D

    #@2c
    .line 101
    iput v3, p0, Landroid/location/Location;->mDistance:F

    #@2e
    .line 102
    iput v3, p0, Landroid/location/Location;->mInitialBearing:F

    #@30
    .line 104
    const/4 v0, 0x2

    #@31
    new-array v0, v0, [F

    #@33
    iput-object v0, p0, Landroid/location/Location;->mResults:[F

    #@35
    .line 115
    iput-object p1, p0, Landroid/location/Location;->mProvider:Ljava/lang/String;

    #@37
    .line 116
    return-void
.end method

.method static synthetic access$002(Landroid/location/Location;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 40
    iput-wide p1, p0, Landroid/location/Location;->mTime:J

    #@2
    return-wide p1
.end method

.method static synthetic access$1002(Landroid/location/Location;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 40
    iput-boolean p1, p0, Landroid/location/Location;->mHasAccuracy:Z

    #@2
    return p1
.end method

.method static synthetic access$102(Landroid/location/Location;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 40
    iput-wide p1, p0, Landroid/location/Location;->mElapsedRealtimeNanos:J

    #@2
    return-wide p1
.end method

.method static synthetic access$1102(Landroid/location/Location;F)F
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 40
    iput p1, p0, Landroid/location/Location;->mAccuracy:F

    #@2
    return p1
.end method

.method static synthetic access$1202(Landroid/location/Location;Landroid/os/Bundle;)Landroid/os/Bundle;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 40
    iput-object p1, p0, Landroid/location/Location;->mExtras:Landroid/os/Bundle;

    #@2
    return-object p1
.end method

.method static synthetic access$202(Landroid/location/Location;D)D
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 40
    iput-wide p1, p0, Landroid/location/Location;->mLatitude:D

    #@2
    return-wide p1
.end method

.method static synthetic access$302(Landroid/location/Location;D)D
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 40
    iput-wide p1, p0, Landroid/location/Location;->mLongitude:D

    #@2
    return-wide p1
.end method

.method static synthetic access$402(Landroid/location/Location;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 40
    iput-boolean p1, p0, Landroid/location/Location;->mHasAltitude:Z

    #@2
    return p1
.end method

.method static synthetic access$502(Landroid/location/Location;D)D
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 40
    iput-wide p1, p0, Landroid/location/Location;->mAltitude:D

    #@2
    return-wide p1
.end method

.method static synthetic access$602(Landroid/location/Location;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 40
    iput-boolean p1, p0, Landroid/location/Location;->mHasSpeed:Z

    #@2
    return p1
.end method

.method static synthetic access$702(Landroid/location/Location;F)F
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 40
    iput p1, p0, Landroid/location/Location;->mSpeed:F

    #@2
    return p1
.end method

.method static synthetic access$802(Landroid/location/Location;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 40
    iput-boolean p1, p0, Landroid/location/Location;->mHasBearing:Z

    #@2
    return p1
.end method

.method static synthetic access$902(Landroid/location/Location;F)F
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 40
    iput p1, p0, Landroid/location/Location;->mBearing:F

    #@2
    return p1
.end method

.method private static computeDistanceAndBearing(DDDD[F)V
    .registers 96
    .parameter "lat1"
    .parameter "lon1"
    .parameter "lat2"
    .parameter "lon2"
    .parameter "results"

    #@0
    .prologue
    .line 289
    const/16 v12, 0x14

    #@2
    .line 291
    .local v12, MAXITERS:I
    const-wide v75, 0x3f91df46a2529d39L

    #@7
    mul-double p0, p0, v75

    #@9
    .line 292
    const-wide v75, 0x3f91df46a2529d39L

    #@e
    mul-double p4, p4, v75

    #@10
    .line 293
    const-wide v75, 0x3f91df46a2529d39L

    #@15
    mul-double p2, p2, v75

    #@17
    .line 294
    const-wide v75, 0x3f91df46a2529d39L

    #@1c
    mul-double p6, p6, v75

    #@1e
    .line 296
    const-wide v17, 0x415854a640000000L

    #@23
    .line 297
    .local v17, a:D
    const-wide v21, 0x41583fc4141bda51L

    #@28
    .line 298
    .local v21, b:D
    sub-double v75, v17, v21

    #@2a
    div-double v44, v75, v17

    #@2c
    .line 299
    .local v44, f:D
    mul-double v75, v17, v17

    #@2e
    mul-double v77, v21, v21

    #@30
    sub-double v75, v75, v77

    #@32
    mul-double v77, v21, v21

    #@34
    div-double v19, v75, v77

    #@36
    .line 301
    .local v19, aSqMinusBSqOverBSq:D
    sub-double v10, p6, p2

    #@38
    .line 302
    .local v10, L:D
    const-wide/16 v4, 0x0

    #@3a
    .line 303
    .local v4, A:D
    const-wide/high16 v75, 0x3ff0

    #@3c
    sub-double v75, v75, v44

    #@3e
    invoke-static/range {p0 .. p1}, Ljava/lang/Math;->tan(D)D

    #@41
    move-result-wide v77

    #@42
    mul-double v75, v75, v77

    #@44
    invoke-static/range {v75 .. v76}, Ljava/lang/Math;->atan(D)D

    #@47
    move-result-wide v13

    #@48
    .line 304
    .local v13, U1:D
    const-wide/high16 v75, 0x3ff0

    #@4a
    sub-double v75, v75, v44

    #@4c
    invoke-static/range {p4 .. p5}, Ljava/lang/Math;->tan(D)D

    #@4f
    move-result-wide v77

    #@50
    mul-double v75, v75, v77

    #@52
    invoke-static/range {v75 .. v76}, Ljava/lang/Math;->atan(D)D

    #@55
    move-result-wide v15

    #@56
    .line 306
    .local v15, U2:D
    invoke-static {v13, v14}, Ljava/lang/Math;->cos(D)D

    #@59
    move-result-wide v33

    #@5a
    .line 307
    .local v33, cosU1:D
    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->cos(D)D

    #@5d
    move-result-wide v37

    #@5e
    .line 308
    .local v37, cosU2:D
    invoke-static {v13, v14}, Ljava/lang/Math;->sin(D)D

    #@61
    move-result-wide v63

    #@62
    .line 309
    .local v63, sinU1:D
    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->sin(D)D

    #@65
    move-result-wide v67

    #@66
    .line 310
    .local v67, sinU2:D
    mul-double v35, v33, v37

    #@68
    .line 311
    .local v35, cosU1cosU2:D
    mul-double v65, v63, v67

    #@6a
    .line 313
    .local v65, sinU1sinU2:D
    const-wide/16 v53, 0x0

    #@6c
    .line 314
    .local v53, sigma:D
    const-wide/16 v41, 0x0

    #@6e
    .line 315
    .local v41, deltaSigma:D
    const-wide/16 v31, 0x0

    #@70
    .line 316
    .local v31, cosSqAlpha:D
    const-wide/16 v23, 0x0

    #@72
    .line 317
    .local v23, cos2SM:D
    const-wide/16 v29, 0x0

    #@74
    .line 318
    .local v29, cosSigma:D
    const-wide/16 v59, 0x0

    #@76
    .line 319
    .local v59, sinSigma:D
    const-wide/16 v27, 0x0

    #@78
    .line 320
    .local v27, cosLambda:D
    const-wide/16 v57, 0x0

    #@7a
    .line 322
    .local v57, sinLambda:D
    move-wide/from16 v49, v10

    #@7c
    .line 323
    .local v49, lambda:D
    const/16 v48, 0x0

    #@7e
    .local v48, iter:I
    :goto_7e
    move/from16 v0, v48

    #@80
    if-ge v0, v12, :cond_184

    #@82
    .line 324
    move-wide/from16 v51, v49

    #@84
    .line 325
    .local v51, lambdaOrig:D
    invoke-static/range {v49 .. v50}, Ljava/lang/Math;->cos(D)D

    #@87
    move-result-wide v27

    #@88
    .line 326
    invoke-static/range {v49 .. v50}, Ljava/lang/Math;->sin(D)D

    #@8b
    move-result-wide v57

    #@8c
    .line 327
    mul-double v69, v37, v57

    #@8e
    .line 328
    .local v69, t1:D
    mul-double v75, v33, v67

    #@90
    mul-double v77, v63, v37

    #@92
    mul-double v77, v77, v27

    #@94
    sub-double v71, v75, v77

    #@96
    .line 329
    .local v71, t2:D
    mul-double v75, v69, v69

    #@98
    mul-double v77, v71, v71

    #@9a
    add-double v61, v75, v77

    #@9c
    .line 330
    .local v61, sinSqSigma:D
    invoke-static/range {v61 .. v62}, Ljava/lang/Math;->sqrt(D)D

    #@9f
    move-result-wide v59

    #@a0
    .line 331
    mul-double v75, v35, v27

    #@a2
    add-double v29, v65, v75

    #@a4
    .line 332
    move-wide/from16 v0, v59

    #@a6
    move-wide/from16 v2, v29

    #@a8
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    #@ab
    move-result-wide v53

    #@ac
    .line 333
    const-wide/16 v75, 0x0

    #@ae
    cmpl-double v75, v59, v75

    #@b0
    if-nez v75, :cond_203

    #@b2
    const-wide/16 v55, 0x0

    #@b4
    .line 335
    .local v55, sinAlpha:D
    :goto_b4
    const-wide/high16 v75, 0x3ff0

    #@b6
    mul-double v77, v55, v55

    #@b8
    sub-double v31, v75, v77

    #@ba
    .line 336
    const-wide/16 v75, 0x0

    #@bc
    cmpl-double v75, v31, v75

    #@be
    if-nez v75, :cond_209

    #@c0
    const-wide/16 v23, 0x0

    #@c2
    .line 339
    :goto_c2
    mul-double v73, v31, v19

    #@c4
    .line 340
    .local v73, uSquared:D
    const-wide/high16 v75, 0x3ff0

    #@c6
    const-wide/high16 v77, 0x40d0

    #@c8
    div-double v77, v73, v77

    #@ca
    const-wide/high16 v79, 0x40b0

    #@cc
    const-wide/high16 v81, -0x3f78

    #@ce
    const-wide/high16 v83, 0x4074

    #@d0
    const-wide v85, 0x4065e00000000000L

    #@d5
    mul-double v85, v85, v73

    #@d7
    sub-double v83, v83, v85

    #@d9
    mul-double v83, v83, v73

    #@db
    add-double v81, v81, v83

    #@dd
    mul-double v81, v81, v73

    #@df
    add-double v79, v79, v81

    #@e1
    mul-double v77, v77, v79

    #@e3
    add-double v4, v75, v77

    #@e5
    .line 343
    const-wide/high16 v75, 0x4090

    #@e7
    div-double v75, v73, v75

    #@e9
    const-wide/high16 v77, 0x4070

    #@eb
    const-wide/high16 v79, -0x3fa0

    #@ed
    const-wide v81, 0x4052800000000000L

    #@f2
    const-wide v83, 0x4047800000000000L

    #@f7
    mul-double v83, v83, v73

    #@f9
    sub-double v81, v81, v83

    #@fb
    mul-double v81, v81, v73

    #@fd
    add-double v79, v79, v81

    #@ff
    mul-double v79, v79, v73

    #@101
    add-double v77, v77, v79

    #@103
    mul-double v6, v75, v77

    #@105
    .line 346
    .local v6, B:D
    const-wide/high16 v75, 0x4030

    #@107
    div-double v75, v44, v75

    #@109
    mul-double v75, v75, v31

    #@10b
    const-wide/high16 v77, 0x4010

    #@10d
    const-wide/high16 v79, 0x4010

    #@10f
    const-wide/high16 v81, 0x4008

    #@111
    mul-double v81, v81, v31

    #@113
    sub-double v79, v79, v81

    #@115
    mul-double v79, v79, v44

    #@117
    add-double v77, v77, v79

    #@119
    mul-double v8, v75, v77

    #@11b
    .line 349
    .local v8, C:D
    mul-double v25, v23, v23

    #@11d
    .line 350
    .local v25, cos2SMSq:D
    mul-double v75, v6, v59

    #@11f
    const-wide/high16 v77, 0x4010

    #@121
    div-double v77, v6, v77

    #@123
    const-wide/high16 v79, -0x4010

    #@125
    const-wide/high16 v81, 0x4000

    #@127
    mul-double v81, v81, v25

    #@129
    add-double v79, v79, v81

    #@12b
    mul-double v79, v79, v29

    #@12d
    const-wide/high16 v81, 0x4018

    #@12f
    div-double v81, v6, v81

    #@131
    mul-double v81, v81, v23

    #@133
    const-wide/high16 v83, -0x3ff8

    #@135
    const-wide/high16 v85, 0x4010

    #@137
    mul-double v85, v85, v59

    #@139
    mul-double v85, v85, v59

    #@13b
    add-double v83, v83, v85

    #@13d
    mul-double v81, v81, v83

    #@13f
    const-wide/high16 v83, -0x3ff8

    #@141
    const-wide/high16 v85, 0x4010

    #@143
    mul-double v85, v85, v25

    #@145
    add-double v83, v83, v85

    #@147
    mul-double v81, v81, v83

    #@149
    sub-double v79, v79, v81

    #@14b
    mul-double v77, v77, v79

    #@14d
    add-double v77, v77, v23

    #@14f
    mul-double v41, v75, v77

    #@151
    .line 357
    const-wide/high16 v75, 0x3ff0

    #@153
    sub-double v75, v75, v8

    #@155
    mul-double v75, v75, v44

    #@157
    mul-double v75, v75, v55

    #@159
    mul-double v77, v8, v59

    #@15b
    mul-double v79, v8, v29

    #@15d
    const-wide/high16 v81, -0x4010

    #@15f
    const-wide/high16 v83, 0x4000

    #@161
    mul-double v83, v83, v23

    #@163
    mul-double v83, v83, v23

    #@165
    add-double v81, v81, v83

    #@167
    mul-double v79, v79, v81

    #@169
    add-double v79, v79, v23

    #@16b
    mul-double v77, v77, v79

    #@16d
    add-double v77, v77, v53

    #@16f
    mul-double v75, v75, v77

    #@171
    add-double v49, v10, v75

    #@173
    .line 363
    sub-double v75, v49, v51

    #@175
    div-double v39, v75, v49

    #@177
    .line 364
    .local v39, delta:D
    invoke-static/range {v39 .. v40}, Ljava/lang/Math;->abs(D)D

    #@17a
    move-result-wide v75

    #@17b
    const-wide v77, 0x3d719799812dea11L

    #@180
    cmpg-double v75, v75, v77

    #@182
    if-gez v75, :cond_213

    #@184
    .line 369
    .end local v6           #B:D
    .end local v8           #C:D
    .end local v25           #cos2SMSq:D
    .end local v39           #delta:D
    .end local v51           #lambdaOrig:D
    .end local v55           #sinAlpha:D
    .end local v61           #sinSqSigma:D
    .end local v69           #t1:D
    .end local v71           #t2:D
    .end local v73           #uSquared:D
    :cond_184
    mul-double v75, v21, v4

    #@186
    sub-double v77, v53, v41

    #@188
    mul-double v75, v75, v77

    #@18a
    move-wide/from16 v0, v75

    #@18c
    double-to-float v0, v0

    #@18d
    move/from16 v43, v0

    #@18f
    .line 370
    .local v43, distance:F
    const/16 v75, 0x0

    #@191
    aput v43, p8, v75

    #@193
    .line 371
    move-object/from16 v0, p8

    #@195
    array-length v0, v0

    #@196
    move/from16 v75, v0

    #@198
    const/16 v76, 0x1

    #@19a
    move/from16 v0, v75

    #@19c
    move/from16 v1, v76

    #@19e
    if-le v0, v1, :cond_202

    #@1a0
    .line 372
    mul-double v75, v37, v57

    #@1a2
    mul-double v77, v33, v67

    #@1a4
    mul-double v79, v63, v37

    #@1a6
    mul-double v79, v79, v27

    #@1a8
    sub-double v77, v77, v79

    #@1aa
    invoke-static/range {v75 .. v78}, Ljava/lang/Math;->atan2(DD)D

    #@1ad
    move-result-wide v75

    #@1ae
    move-wide/from16 v0, v75

    #@1b0
    double-to-float v0, v0

    #@1b1
    move/from16 v47, v0

    #@1b3
    .line 374
    .local v47, initialBearing:F
    move/from16 v0, v47

    #@1b5
    float-to-double v0, v0

    #@1b6
    move-wide/from16 v75, v0

    #@1b8
    const-wide v77, 0x404ca5dc1a63c1f8L

    #@1bd
    mul-double v75, v75, v77

    #@1bf
    move-wide/from16 v0, v75

    #@1c1
    double-to-float v0, v0

    #@1c2
    move/from16 v47, v0

    #@1c4
    .line 375
    const/16 v75, 0x1

    #@1c6
    aput v47, p8, v75

    #@1c8
    .line 376
    move-object/from16 v0, p8

    #@1ca
    array-length v0, v0

    #@1cb
    move/from16 v75, v0

    #@1cd
    const/16 v76, 0x2

    #@1cf
    move/from16 v0, v75

    #@1d1
    move/from16 v1, v76

    #@1d3
    if-le v0, v1, :cond_202

    #@1d5
    .line 377
    mul-double v75, v33, v57

    #@1d7
    move-wide/from16 v0, v63

    #@1d9
    neg-double v0, v0

    #@1da
    move-wide/from16 v77, v0

    #@1dc
    mul-double v77, v77, v37

    #@1de
    mul-double v79, v33, v67

    #@1e0
    mul-double v79, v79, v27

    #@1e2
    add-double v77, v77, v79

    #@1e4
    invoke-static/range {v75 .. v78}, Ljava/lang/Math;->atan2(DD)D

    #@1e7
    move-result-wide v75

    #@1e8
    move-wide/from16 v0, v75

    #@1ea
    double-to-float v0, v0

    #@1eb
    move/from16 v46, v0

    #@1ed
    .line 379
    .local v46, finalBearing:F
    move/from16 v0, v46

    #@1ef
    float-to-double v0, v0

    #@1f0
    move-wide/from16 v75, v0

    #@1f2
    const-wide v77, 0x404ca5dc1a63c1f8L

    #@1f7
    mul-double v75, v75, v77

    #@1f9
    move-wide/from16 v0, v75

    #@1fb
    double-to-float v0, v0

    #@1fc
    move/from16 v46, v0

    #@1fe
    .line 380
    const/16 v75, 0x2

    #@200
    aput v46, p8, v75

    #@202
    .line 383
    .end local v46           #finalBearing:F
    .end local v47           #initialBearing:F
    :cond_202
    return-void

    #@203
    .line 333
    .end local v43           #distance:F
    .restart local v51       #lambdaOrig:D
    .restart local v61       #sinSqSigma:D
    .restart local v69       #t1:D
    .restart local v71       #t2:D
    :cond_203
    mul-double v75, v35, v57

    #@205
    div-double v55, v75, v59

    #@207
    goto/16 :goto_b4

    #@209
    .line 336
    .restart local v55       #sinAlpha:D
    :cond_209
    const-wide/high16 v75, 0x4000

    #@20b
    mul-double v75, v75, v65

    #@20d
    div-double v75, v75, v31

    #@20f
    sub-double v23, v29, v75

    #@211
    goto/16 :goto_c2

    #@213
    .line 323
    .restart local v6       #B:D
    .restart local v8       #C:D
    .restart local v25       #cos2SMSq:D
    .restart local v39       #delta:D
    .restart local v73       #uSquared:D
    :cond_213
    add-int/lit8 v48, v48, 0x1

    #@215
    goto/16 :goto_7e
.end method

.method public static convert(Ljava/lang/String;)D
    .registers 22
    .parameter "coordinate"

    #@0
    .prologue
    .line 224
    if-nez p0, :cond_a

    #@2
    .line 225
    new-instance v17, Ljava/lang/NullPointerException;

    #@4
    const-string v18, "coordinate"

    #@6
    invoke-direct/range {v17 .. v18}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@9
    throw v17

    #@a
    .line 228
    :cond_a
    const/4 v8, 0x0

    #@b
    .line 229
    .local v8, negative:Z
    const/16 v17, 0x0

    #@d
    move-object/from16 v0, p0

    #@f
    move/from16 v1, v17

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    #@14
    move-result v17

    #@15
    const/16 v18, 0x2d

    #@17
    move/from16 v0, v17

    #@19
    move/from16 v1, v18

    #@1b
    if-ne v0, v1, :cond_28

    #@1d
    .line 230
    const/16 v17, 0x1

    #@1f
    move-object/from16 v0, p0

    #@21
    move/from16 v1, v17

    #@23
    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@26
    move-result-object p0

    #@27
    .line 231
    const/4 v8, 0x1

    #@28
    .line 234
    :cond_28
    new-instance v13, Ljava/util/StringTokenizer;

    #@2a
    const-string v17, ":"

    #@2c
    move-object/from16 v0, p0

    #@2e
    move-object/from16 v1, v17

    #@30
    invoke-direct {v13, v0, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@33
    .line 235
    .local v13, st:Ljava/util/StringTokenizer;
    invoke-virtual {v13}, Ljava/util/StringTokenizer;->countTokens()I

    #@36
    move-result v14

    #@37
    .line 236
    .local v14, tokens:I
    const/16 v17, 0x1

    #@39
    move/from16 v0, v17

    #@3b
    if-ge v14, v0, :cond_5a

    #@3d
    .line 237
    new-instance v17, Ljava/lang/IllegalArgumentException;

    #@3f
    new-instance v18, Ljava/lang/StringBuilder;

    #@41
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v19, "coordinate="

    #@46
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v18

    #@4a
    move-object/from16 v0, v18

    #@4c
    move-object/from16 v1, p0

    #@4e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v18

    #@52
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v18

    #@56
    invoke-direct/range {v17 .. v18}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@59
    throw v17

    #@5a
    .line 240
    :cond_5a
    :try_start_5a
    invoke-virtual {v13}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@5d
    move-result-object v3

    #@5e
    .line 242
    .local v3, degrees:Ljava/lang/String;
    const/16 v17, 0x1

    #@60
    move/from16 v0, v17

    #@62
    if-ne v14, v0, :cond_71

    #@64
    .line 243
    invoke-static {v3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    #@67
    move-result-wide v15

    #@68
    .line 244
    .local v15, val:D
    if-eqz v8, :cond_6e

    #@6a
    neg-double v0, v15

    #@6b
    move-wide/from16 v17, v0

    #@6d
    .line 278
    :goto_6d
    return-wide v17

    #@6e
    :cond_6e
    move-wide/from16 v17, v15

    #@70
    .line 244
    goto :goto_6d

    #@71
    .line 247
    .end local v15           #val:D
    :cond_71
    invoke-virtual {v13}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@74
    move-result-object v7

    #@75
    .line 248
    .local v7, minutes:Ljava/lang/String;
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@78
    move-result v2

    #@79
    .line 250
    .local v2, deg:I
    const-wide/16 v10, 0x0

    #@7b
    .line 252
    .local v10, sec:D
    invoke-virtual {v13}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    #@7e
    move-result v17

    #@7f
    if-eqz v17, :cond_f1

    #@81
    .line 253
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@84
    move-result v17

    #@85
    move/from16 v0, v17

    #@87
    int-to-double v5, v0

    #@88
    .line 254
    .local v5, min:D
    invoke-virtual {v13}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@8b
    move-result-object v12

    #@8c
    .line 255
    .local v12, seconds:Ljava/lang/String;
    invoke-static {v12}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    #@8f
    move-result-wide v10

    #@90
    .line 260
    .end local v12           #seconds:Ljava/lang/String;
    :goto_90
    if-eqz v8, :cond_f6

    #@92
    const/16 v17, 0xb4

    #@94
    move/from16 v0, v17

    #@96
    if-ne v2, v0, :cond_f6

    #@98
    const-wide/16 v17, 0x0

    #@9a
    cmpl-double v17, v5, v17

    #@9c
    if-nez v17, :cond_f6

    #@9e
    const-wide/16 v17, 0x0

    #@a0
    cmpl-double v17, v10, v17

    #@a2
    if-nez v17, :cond_f6

    #@a4
    const/4 v4, 0x1

    #@a5
    .line 264
    .local v4, isNegative180:Z
    :goto_a5
    int-to-double v0, v2

    #@a6
    move-wide/from16 v17, v0

    #@a8
    const-wide/16 v19, 0x0

    #@aa
    cmpg-double v17, v17, v19

    #@ac
    if-ltz v17, :cond_b6

    #@ae
    const/16 v17, 0xb3

    #@b0
    move/from16 v0, v17

    #@b2
    if-le v2, v0, :cond_f8

    #@b4
    if-nez v4, :cond_f8

    #@b6
    .line 265
    :cond_b6
    new-instance v17, Ljava/lang/IllegalArgumentException;

    #@b8
    new-instance v18, Ljava/lang/StringBuilder;

    #@ba
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@bd
    const-string v19, "coordinate="

    #@bf
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v18

    #@c3
    move-object/from16 v0, v18

    #@c5
    move-object/from16 v1, p0

    #@c7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v18

    #@cb
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ce
    move-result-object v18

    #@cf
    invoke-direct/range {v17 .. v18}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d2
    throw v17
    :try_end_d3
    .catch Ljava/lang/NumberFormatException; {:try_start_5a .. :try_end_d3} :catch_d3

    #@d3
    .line 279
    .end local v2           #deg:I
    .end local v3           #degrees:Ljava/lang/String;
    .end local v4           #isNegative180:Z
    .end local v5           #min:D
    .end local v7           #minutes:Ljava/lang/String;
    .end local v10           #sec:D
    :catch_d3
    move-exception v9

    #@d4
    .line 280
    .local v9, nfe:Ljava/lang/NumberFormatException;
    new-instance v17, Ljava/lang/IllegalArgumentException;

    #@d6
    new-instance v18, Ljava/lang/StringBuilder;

    #@d8
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@db
    const-string v19, "coordinate="

    #@dd
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e0
    move-result-object v18

    #@e1
    move-object/from16 v0, v18

    #@e3
    move-object/from16 v1, p0

    #@e5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v18

    #@e9
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ec
    move-result-object v18

    #@ed
    invoke-direct/range {v17 .. v18}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f0
    throw v17

    #@f1
    .line 257
    .end local v9           #nfe:Ljava/lang/NumberFormatException;
    .restart local v2       #deg:I
    .restart local v3       #degrees:Ljava/lang/String;
    .restart local v7       #minutes:Ljava/lang/String;
    .restart local v10       #sec:D
    :cond_f1
    :try_start_f1
    invoke-static {v7}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    #@f4
    move-result-wide v5

    #@f5
    .restart local v5       #min:D
    goto :goto_90

    #@f6
    .line 260
    :cond_f6
    const/4 v4, 0x0

    #@f7
    goto :goto_a5

    #@f8
    .line 267
    .restart local v4       #isNegative180:Z
    :cond_f8
    const-wide/16 v17, 0x0

    #@fa
    cmpg-double v17, v5, v17

    #@fc
    if-ltz v17, :cond_107

    #@fe
    const-wide v17, 0x404d800000000000L

    #@103
    cmpl-double v17, v5, v17

    #@105
    if-lez v17, :cond_124

    #@107
    .line 268
    :cond_107
    new-instance v17, Ljava/lang/IllegalArgumentException;

    #@109
    new-instance v18, Ljava/lang/StringBuilder;

    #@10b
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@10e
    const-string v19, "coordinate="

    #@110
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@113
    move-result-object v18

    #@114
    move-object/from16 v0, v18

    #@116
    move-object/from16 v1, p0

    #@118
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v18

    #@11c
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11f
    move-result-object v18

    #@120
    invoke-direct/range {v17 .. v18}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@123
    throw v17

    #@124
    .line 271
    :cond_124
    const-wide/16 v17, 0x0

    #@126
    cmpg-double v17, v10, v17

    #@128
    if-ltz v17, :cond_133

    #@12a
    const-wide v17, 0x404d800000000000L

    #@12f
    cmpl-double v17, v10, v17

    #@131
    if-lez v17, :cond_150

    #@133
    .line 272
    :cond_133
    new-instance v17, Ljava/lang/IllegalArgumentException;

    #@135
    new-instance v18, Ljava/lang/StringBuilder;

    #@137
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@13a
    const-string v19, "coordinate="

    #@13c
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13f
    move-result-object v18

    #@140
    move-object/from16 v0, v18

    #@142
    move-object/from16 v1, p0

    #@144
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@147
    move-result-object v18

    #@148
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14b
    move-result-object v18

    #@14c
    invoke-direct/range {v17 .. v18}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@14f
    throw v17
    :try_end_150
    .catch Ljava/lang/NumberFormatException; {:try_start_f1 .. :try_end_150} :catch_d3

    #@150
    .line 276
    :cond_150
    int-to-double v0, v2

    #@151
    move-wide/from16 v17, v0

    #@153
    const-wide v19, 0x40ac200000000000L

    #@158
    mul-double v17, v17, v19

    #@15a
    const-wide/high16 v19, 0x404e

    #@15c
    mul-double v19, v19, v5

    #@15e
    add-double v17, v17, v19

    #@160
    add-double v15, v17, v10

    #@162
    .line 277
    .restart local v15       #val:D
    const-wide v17, 0x40ac200000000000L

    #@167
    div-double v15, v15, v17

    #@169
    .line 278
    if-eqz v8, :cond_170

    #@16b
    neg-double v0, v15

    #@16c
    move-wide/from16 v17, v0

    #@16e
    goto/16 :goto_6d

    #@170
    :cond_170
    move-wide/from16 v17, v15

    #@172
    goto/16 :goto_6d
.end method

.method public static convert(DI)Ljava/lang/String;
    .registers 14
    .parameter "coordinate"
    .parameter "outputType"

    #@0
    .prologue
    const/16 v10, 0x3a

    #@2
    const/4 v9, 0x1

    #@3
    const-wide/high16 v7, 0x404e

    #@5
    const/4 v6, 0x2

    #@6
    .line 176
    const-wide v4, -0x3f99800000000000L

    #@b
    cmpg-double v4, p0, v4

    #@d
    if-ltz v4, :cond_1e

    #@f
    const-wide v4, 0x4066800000000000L

    #@14
    cmpl-double v4, p0, v4

    #@16
    if-gtz v4, :cond_1e

    #@18
    invoke-static {p0, p1}, Ljava/lang/Double;->isNaN(D)Z

    #@1b
    move-result v4

    #@1c
    if-eqz v4, :cond_37

    #@1e
    .line 178
    :cond_1e
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@20
    new-instance v5, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v6, "coordinate="

    #@27
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v5

    #@2b
    invoke-virtual {v5, p0, p1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v5

    #@2f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v5

    #@33
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@36
    throw v4

    #@37
    .line 180
    :cond_37
    if-eqz p2, :cond_57

    #@39
    if-eq p2, v9, :cond_57

    #@3b
    if-eq p2, v6, :cond_57

    #@3d
    .line 183
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@3f
    new-instance v5, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string/jumbo v6, "outputType="

    #@47
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v5

    #@4b
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v5

    #@4f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v5

    #@53
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@56
    throw v4

    #@57
    .line 186
    :cond_57
    new-instance v3, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    .line 189
    .local v3, sb:Ljava/lang/StringBuilder;
    const-wide/16 v4, 0x0

    #@5e
    cmpg-double v4, p0, v4

    #@60
    if-gez v4, :cond_68

    #@62
    .line 190
    const/16 v4, 0x2d

    #@64
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@67
    .line 191
    neg-double p0, p0

    #@68
    .line 194
    :cond_68
    new-instance v1, Ljava/text/DecimalFormat;

    #@6a
    const-string v4, "###.#####"

    #@6c
    invoke-direct {v1, v4}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    #@6f
    .line 195
    .local v1, df:Ljava/text/DecimalFormat;
    if-eq p2, v9, :cond_73

    #@71
    if-ne p2, v6, :cond_91

    #@73
    .line 196
    :cond_73
    invoke-static {p0, p1}, Ljava/lang/Math;->floor(D)D

    #@76
    move-result-wide v4

    #@77
    double-to-int v0, v4

    #@78
    .line 197
    .local v0, degrees:I
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7b
    .line 198
    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@7e
    .line 199
    int-to-double v4, v0

    #@7f
    sub-double/2addr p0, v4

    #@80
    .line 200
    mul-double/2addr p0, v7

    #@81
    .line 201
    if-ne p2, v6, :cond_91

    #@83
    .line 202
    invoke-static {p0, p1}, Ljava/lang/Math;->floor(D)D

    #@86
    move-result-wide v4

    #@87
    double-to-int v2, v4

    #@88
    .line 203
    .local v2, minutes:I
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8b
    .line 204
    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@8e
    .line 205
    int-to-double v4, v2

    #@8f
    sub-double/2addr p0, v4

    #@90
    .line 206
    mul-double/2addr p0, v7

    #@91
    .line 209
    .end local v0           #degrees:I
    .end local v2           #minutes:I
    :cond_91
    invoke-virtual {v1, p0, p1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    #@94
    move-result-object v4

    #@95
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    .line 210
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9b
    move-result-object v4

    #@9c
    return-object v4
.end method

.method public static distanceBetween(DDDD[F)V
    .registers 11
    .parameter "startLatitude"
    .parameter "startLongitude"
    .parameter "endLatitude"
    .parameter "endLongitude"
    .parameter "results"

    #@0
    .prologue
    .line 405
    if-eqz p8, :cond_6

    #@2
    array-length v0, p8

    #@3
    const/4 v1, 0x1

    #@4
    if-ge v0, v1, :cond_f

    #@6
    .line 406
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    const-string/jumbo v1, "results is null or has length < 1"

    #@b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 408
    :cond_f
    invoke-static/range {p0 .. p8}, Landroid/location/Location;->computeDistanceAndBearing(DDDD[F)V

    #@12
    .line 410
    return-void
.end method


# virtual methods
.method public bearingTo(Landroid/location/Location;)F
    .registers 12
    .parameter "dest"

    #@0
    .prologue
    .line 449
    iget-object v9, p0, Landroid/location/Location;->mResults:[F

    #@2
    monitor-enter v9

    #@3
    .line 451
    :try_start_3
    iget-wide v0, p0, Landroid/location/Location;->mLatitude:D

    #@5
    iget-wide v2, p0, Landroid/location/Location;->mLat1:D

    #@7
    cmpl-double v0, v0, v2

    #@9
    if-nez v0, :cond_23

    #@b
    iget-wide v0, p0, Landroid/location/Location;->mLongitude:D

    #@d
    iget-wide v2, p0, Landroid/location/Location;->mLon1:D

    #@f
    cmpl-double v0, v0, v2

    #@11
    if-nez v0, :cond_23

    #@13
    iget-wide v0, p1, Landroid/location/Location;->mLatitude:D

    #@15
    iget-wide v2, p0, Landroid/location/Location;->mLat2:D

    #@17
    cmpl-double v0, v0, v2

    #@19
    if-nez v0, :cond_23

    #@1b
    iget-wide v0, p1, Landroid/location/Location;->mLongitude:D

    #@1d
    iget-wide v2, p0, Landroid/location/Location;->mLon2:D

    #@1f
    cmpl-double v0, v0, v2

    #@21
    if-eqz v0, :cond_4e

    #@23
    .line 453
    :cond_23
    iget-wide v0, p0, Landroid/location/Location;->mLatitude:D

    #@25
    iget-wide v2, p0, Landroid/location/Location;->mLongitude:D

    #@27
    iget-wide v4, p1, Landroid/location/Location;->mLatitude:D

    #@29
    iget-wide v6, p1, Landroid/location/Location;->mLongitude:D

    #@2b
    iget-object v8, p0, Landroid/location/Location;->mResults:[F

    #@2d
    invoke-static/range {v0 .. v8}, Landroid/location/Location;->computeDistanceAndBearing(DDDD[F)V

    #@30
    .line 455
    iget-wide v0, p0, Landroid/location/Location;->mLatitude:D

    #@32
    iput-wide v0, p0, Landroid/location/Location;->mLat1:D

    #@34
    .line 456
    iget-wide v0, p0, Landroid/location/Location;->mLongitude:D

    #@36
    iput-wide v0, p0, Landroid/location/Location;->mLon1:D

    #@38
    .line 457
    iget-wide v0, p1, Landroid/location/Location;->mLatitude:D

    #@3a
    iput-wide v0, p0, Landroid/location/Location;->mLat2:D

    #@3c
    .line 458
    iget-wide v0, p1, Landroid/location/Location;->mLongitude:D

    #@3e
    iput-wide v0, p0, Landroid/location/Location;->mLon2:D

    #@40
    .line 459
    iget-object v0, p0, Landroid/location/Location;->mResults:[F

    #@42
    const/4 v1, 0x0

    #@43
    aget v0, v0, v1

    #@45
    iput v0, p0, Landroid/location/Location;->mDistance:F

    #@47
    .line 460
    iget-object v0, p0, Landroid/location/Location;->mResults:[F

    #@49
    const/4 v1, 0x1

    #@4a
    aget v0, v0, v1

    #@4c
    iput v0, p0, Landroid/location/Location;->mInitialBearing:F

    #@4e
    .line 462
    :cond_4e
    iget v0, p0, Landroid/location/Location;->mInitialBearing:F

    #@50
    monitor-exit v9

    #@51
    return v0

    #@52
    .line 463
    :catchall_52
    move-exception v0

    #@53
    monitor-exit v9
    :try_end_54
    .catchall {:try_start_3 .. :try_end_54} :catchall_52

    #@54
    throw v0
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 885
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public distanceTo(Landroid/location/Location;)F
    .registers 12
    .parameter "dest"

    #@0
    .prologue
    .line 422
    iget-object v9, p0, Landroid/location/Location;->mResults:[F

    #@2
    monitor-enter v9

    #@3
    .line 423
    :try_start_3
    iget-wide v0, p0, Landroid/location/Location;->mLatitude:D

    #@5
    iget-wide v2, p0, Landroid/location/Location;->mLat1:D

    #@7
    cmpl-double v0, v0, v2

    #@9
    if-nez v0, :cond_23

    #@b
    iget-wide v0, p0, Landroid/location/Location;->mLongitude:D

    #@d
    iget-wide v2, p0, Landroid/location/Location;->mLon1:D

    #@f
    cmpl-double v0, v0, v2

    #@11
    if-nez v0, :cond_23

    #@13
    iget-wide v0, p1, Landroid/location/Location;->mLatitude:D

    #@15
    iget-wide v2, p0, Landroid/location/Location;->mLat2:D

    #@17
    cmpl-double v0, v0, v2

    #@19
    if-nez v0, :cond_23

    #@1b
    iget-wide v0, p1, Landroid/location/Location;->mLongitude:D

    #@1d
    iget-wide v2, p0, Landroid/location/Location;->mLon2:D

    #@1f
    cmpl-double v0, v0, v2

    #@21
    if-eqz v0, :cond_4e

    #@23
    .line 425
    :cond_23
    iget-wide v0, p0, Landroid/location/Location;->mLatitude:D

    #@25
    iget-wide v2, p0, Landroid/location/Location;->mLongitude:D

    #@27
    iget-wide v4, p1, Landroid/location/Location;->mLatitude:D

    #@29
    iget-wide v6, p1, Landroid/location/Location;->mLongitude:D

    #@2b
    iget-object v8, p0, Landroid/location/Location;->mResults:[F

    #@2d
    invoke-static/range {v0 .. v8}, Landroid/location/Location;->computeDistanceAndBearing(DDDD[F)V

    #@30
    .line 427
    iget-wide v0, p0, Landroid/location/Location;->mLatitude:D

    #@32
    iput-wide v0, p0, Landroid/location/Location;->mLat1:D

    #@34
    .line 428
    iget-wide v0, p0, Landroid/location/Location;->mLongitude:D

    #@36
    iput-wide v0, p0, Landroid/location/Location;->mLon1:D

    #@38
    .line 429
    iget-wide v0, p1, Landroid/location/Location;->mLatitude:D

    #@3a
    iput-wide v0, p0, Landroid/location/Location;->mLat2:D

    #@3c
    .line 430
    iget-wide v0, p1, Landroid/location/Location;->mLongitude:D

    #@3e
    iput-wide v0, p0, Landroid/location/Location;->mLon2:D

    #@40
    .line 431
    iget-object v0, p0, Landroid/location/Location;->mResults:[F

    #@42
    const/4 v1, 0x0

    #@43
    aget v0, v0, v1

    #@45
    iput v0, p0, Landroid/location/Location;->mDistance:F

    #@47
    .line 432
    iget-object v0, p0, Landroid/location/Location;->mResults:[F

    #@49
    const/4 v1, 0x1

    #@4a
    aget v0, v0, v1

    #@4c
    iput v0, p0, Landroid/location/Location;->mInitialBearing:F

    #@4e
    .line 434
    :cond_4e
    iget v0, p0, Landroid/location/Location;->mDistance:F

    #@50
    monitor-exit v9

    #@51
    return v0

    #@52
    .line 435
    :catchall_52
    move-exception v0

    #@53
    monitor-exit v9
    :try_end_54
    .catchall {:try_start_3 .. :try_end_54} :catchall_52

    #@54
    throw v0
.end method

.method public dump(Landroid/util/Printer;Ljava/lang/String;)V
    .registers 5
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    .line 852
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p0}, Landroid/location/Location;->toString()Ljava/lang/String;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@18
    .line 853
    return-void
.end method

.method public getAccuracy()F
    .registers 2

    #@0
    .prologue
    .line 732
    iget v0, p0, Landroid/location/Location;->mAccuracy:F

    #@2
    return v0
.end method

.method public getAltitude()D
    .registers 3

    #@0
    .prologue
    .line 588
    iget-wide v0, p0, Landroid/location/Location;->mAltitude:D

    #@2
    return-wide v0
.end method

.method public getBearing()F
    .registers 2

    #@0
    .prologue
    .line 666
    iget v0, p0, Landroid/location/Location;->mBearing:F

    #@2
    return v0
.end method

.method public getElapsedRealtimeNanos()J
    .registers 3

    #@0
    .prologue
    .line 529
    iget-wide v0, p0, Landroid/location/Location;->mElapsedRealtimeNanos:J

    #@2
    return-wide v0
.end method

.method public getExtraLocation(Ljava/lang/String;)Landroid/location/Location;
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 915
    iget-object v1, p0, Landroid/location/Location;->mExtras:Landroid/os/Bundle;

    #@2
    if-eqz v1, :cond_11

    #@4
    .line 916
    iget-object v1, p0, Landroid/location/Location;->mExtras:Landroid/os/Bundle;

    #@6
    invoke-virtual {v1, p1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@9
    move-result-object v0

    #@a
    .line 917
    .local v0, value:Landroid/os/Parcelable;
    instance-of v1, v0, Landroid/location/Location;

    #@c
    if-eqz v1, :cond_11

    #@e
    .line 918
    check-cast v0, Landroid/location/Location;

    #@10
    .line 921
    .end local v0           #value:Landroid/os/Parcelable;
    :goto_10
    return-object v0

    #@11
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_10
.end method

.method public getExtras()Landroid/os/Bundle;
    .registers 2

    #@0
    .prologue
    .line 812
    iget-object v0, p0, Landroid/location/Location;->mExtras:Landroid/os/Bundle;

    #@2
    return-object v0
.end method

.method public getLatitude()D
    .registers 3

    #@0
    .prologue
    .line 548
    iget-wide v0, p0, Landroid/location/Location;->mLatitude:D

    #@2
    return-wide v0
.end method

.method public getLongitude()D
    .registers 3

    #@0
    .prologue
    .line 565
    iget-wide v0, p0, Landroid/location/Location;->mLongitude:D

    #@2
    return-wide v0
.end method

.method public getProvider()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 472
    iget-object v0, p0, Landroid/location/Location;->mProvider:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getSpeed()F
    .registers 2

    #@0
    .prologue
    .line 625
    iget v0, p0, Landroid/location/Location;->mSpeed:F

    #@2
    return v0
.end method

.method public getTime()J
    .registers 3

    #@0
    .prologue
    .line 500
    iget-wide v0, p0, Landroid/location/Location;->mTime:J

    #@2
    return-wide v0
.end method

.method public hasAccuracy()Z
    .registers 2

    #@0
    .prologue
    .line 706
    iget-boolean v0, p0, Landroid/location/Location;->mHasAccuracy:Z

    #@2
    return v0
.end method

.method public hasAltitude()Z
    .registers 2

    #@0
    .prologue
    .line 579
    iget-boolean v0, p0, Landroid/location/Location;->mHasAltitude:Z

    #@2
    return v0
.end method

.method public hasBearing()Z
    .registers 2

    #@0
    .prologue
    .line 653
    iget-boolean v0, p0, Landroid/location/Location;->mHasBearing:Z

    #@2
    return v0
.end method

.method public hasSpeed()Z
    .registers 2

    #@0
    .prologue
    .line 616
    iget-boolean v0, p0, Landroid/location/Location;->mHasSpeed:Z

    #@2
    return v0
.end method

.method public isComplete()Z
    .registers 6

    #@0
    .prologue
    const-wide/16 v3, 0x0

    #@2
    const/4 v0, 0x0

    #@3
    .line 771
    iget-object v1, p0, Landroid/location/Location;->mProvider:Ljava/lang/String;

    #@5
    if-nez v1, :cond_8

    #@7
    .line 775
    :cond_7
    :goto_7
    return v0

    #@8
    .line 772
    :cond_8
    iget-boolean v1, p0, Landroid/location/Location;->mHasAccuracy:Z

    #@a
    if-eqz v1, :cond_7

    #@c
    .line 773
    iget-wide v1, p0, Landroid/location/Location;->mTime:J

    #@e
    cmp-long v1, v1, v3

    #@10
    if-eqz v1, :cond_7

    #@12
    .line 774
    iget-wide v1, p0, Landroid/location/Location;->mElapsedRealtimeNanos:J

    #@14
    cmp-long v1, v1, v3

    #@16
    if-eqz v1, :cond_7

    #@18
    .line 775
    const/4 v0, 0x1

    #@19
    goto :goto_7
.end method

.method public makeComplete()V
    .registers 5

    #@0
    .prologue
    const-wide/16 v2, 0x0

    #@2
    .line 788
    iget-object v0, p0, Landroid/location/Location;->mProvider:Ljava/lang/String;

    #@4
    if-nez v0, :cond_a

    #@6
    const-string v0, "?"

    #@8
    iput-object v0, p0, Landroid/location/Location;->mProvider:Ljava/lang/String;

    #@a
    .line 789
    :cond_a
    iget-boolean v0, p0, Landroid/location/Location;->mHasAccuracy:Z

    #@c
    if-nez v0, :cond_15

    #@e
    .line 790
    const/4 v0, 0x1

    #@f
    iput-boolean v0, p0, Landroid/location/Location;->mHasAccuracy:Z

    #@11
    .line 791
    const/high16 v0, 0x42c8

    #@13
    iput v0, p0, Landroid/location/Location;->mAccuracy:F

    #@15
    .line 793
    :cond_15
    iget-wide v0, p0, Landroid/location/Location;->mTime:J

    #@17
    cmp-long v0, v0, v2

    #@19
    if-nez v0, :cond_21

    #@1b
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@1e
    move-result-wide v0

    #@1f
    iput-wide v0, p0, Landroid/location/Location;->mTime:J

    #@21
    .line 794
    :cond_21
    iget-wide v0, p0, Landroid/location/Location;->mElapsedRealtimeNanos:J

    #@23
    cmp-long v0, v0, v2

    #@25
    if-nez v0, :cond_2d

    #@27
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    #@2a
    move-result-wide v0

    #@2b
    iput-wide v0, p0, Landroid/location/Location;->mElapsedRealtimeNanos:J

    #@2d
    .line 795
    :cond_2d
    return-void
.end method

.method public removeAccuracy()V
    .registers 2

    #@0
    .prologue
    .line 754
    const/4 v0, 0x0

    #@1
    iput v0, p0, Landroid/location/Location;->mAccuracy:F

    #@3
    .line 755
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Landroid/location/Location;->mHasAccuracy:Z

    #@6
    .line 756
    return-void
.end method

.method public removeAltitude()V
    .registers 3

    #@0
    .prologue
    .line 608
    const-wide/16 v0, 0x0

    #@2
    iput-wide v0, p0, Landroid/location/Location;->mAltitude:D

    #@4
    .line 609
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Landroid/location/Location;->mHasAltitude:Z

    #@7
    .line 610
    return-void
.end method

.method public removeBearing()V
    .registers 2

    #@0
    .prologue
    .line 695
    const/4 v0, 0x0

    #@1
    iput v0, p0, Landroid/location/Location;->mBearing:F

    #@3
    .line 696
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Landroid/location/Location;->mHasBearing:Z

    #@6
    .line 697
    return-void
.end method

.method public removeSpeed()V
    .registers 2

    #@0
    .prologue
    .line 645
    const/4 v0, 0x0

    #@1
    iput v0, p0, Landroid/location/Location;->mSpeed:F

    #@3
    .line 646
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Landroid/location/Location;->mHasSpeed:Z

    #@6
    .line 647
    return-void
.end method

.method public reset()V
    .registers 8

    #@0
    .prologue
    const-wide/16 v5, 0x0

    #@2
    const/4 v4, 0x0

    #@3
    const/4 v3, 0x0

    #@4
    const-wide/16 v1, 0x0

    #@6
    const/4 v0, 0x0

    #@7
    .line 149
    iput-object v4, p0, Landroid/location/Location;->mProvider:Ljava/lang/String;

    #@9
    .line 150
    iput-wide v5, p0, Landroid/location/Location;->mTime:J

    #@b
    .line 151
    iput-wide v5, p0, Landroid/location/Location;->mElapsedRealtimeNanos:J

    #@d
    .line 152
    iput-wide v1, p0, Landroid/location/Location;->mLatitude:D

    #@f
    .line 153
    iput-wide v1, p0, Landroid/location/Location;->mLongitude:D

    #@11
    .line 154
    iput-boolean v0, p0, Landroid/location/Location;->mHasAltitude:Z

    #@13
    .line 155
    iput-wide v1, p0, Landroid/location/Location;->mAltitude:D

    #@15
    .line 156
    iput-boolean v0, p0, Landroid/location/Location;->mHasSpeed:Z

    #@17
    .line 157
    iput v3, p0, Landroid/location/Location;->mSpeed:F

    #@19
    .line 158
    iput-boolean v0, p0, Landroid/location/Location;->mHasBearing:Z

    #@1b
    .line 159
    iput v3, p0, Landroid/location/Location;->mBearing:F

    #@1d
    .line 160
    iput-boolean v0, p0, Landroid/location/Location;->mHasAccuracy:Z

    #@1f
    .line 161
    iput v3, p0, Landroid/location/Location;->mAccuracy:F

    #@21
    .line 162
    iput-object v4, p0, Landroid/location/Location;->mExtras:Landroid/os/Bundle;

    #@23
    .line 163
    return-void
.end method

.method public set(Landroid/location/Location;)V
    .registers 4
    .parameter "l"

    #@0
    .prologue
    .line 129
    iget-object v0, p1, Landroid/location/Location;->mProvider:Ljava/lang/String;

    #@2
    iput-object v0, p0, Landroid/location/Location;->mProvider:Ljava/lang/String;

    #@4
    .line 130
    iget-wide v0, p1, Landroid/location/Location;->mTime:J

    #@6
    iput-wide v0, p0, Landroid/location/Location;->mTime:J

    #@8
    .line 131
    iget-wide v0, p1, Landroid/location/Location;->mElapsedRealtimeNanos:J

    #@a
    iput-wide v0, p0, Landroid/location/Location;->mElapsedRealtimeNanos:J

    #@c
    .line 132
    iget-wide v0, p1, Landroid/location/Location;->mLatitude:D

    #@e
    iput-wide v0, p0, Landroid/location/Location;->mLatitude:D

    #@10
    .line 133
    iget-wide v0, p1, Landroid/location/Location;->mLongitude:D

    #@12
    iput-wide v0, p0, Landroid/location/Location;->mLongitude:D

    #@14
    .line 134
    iget-boolean v0, p1, Landroid/location/Location;->mHasAltitude:Z

    #@16
    iput-boolean v0, p0, Landroid/location/Location;->mHasAltitude:Z

    #@18
    .line 135
    iget-wide v0, p1, Landroid/location/Location;->mAltitude:D

    #@1a
    iput-wide v0, p0, Landroid/location/Location;->mAltitude:D

    #@1c
    .line 136
    iget-boolean v0, p1, Landroid/location/Location;->mHasSpeed:Z

    #@1e
    iput-boolean v0, p0, Landroid/location/Location;->mHasSpeed:Z

    #@20
    .line 137
    iget v0, p1, Landroid/location/Location;->mSpeed:F

    #@22
    iput v0, p0, Landroid/location/Location;->mSpeed:F

    #@24
    .line 138
    iget-boolean v0, p1, Landroid/location/Location;->mHasBearing:Z

    #@26
    iput-boolean v0, p0, Landroid/location/Location;->mHasBearing:Z

    #@28
    .line 139
    iget v0, p1, Landroid/location/Location;->mBearing:F

    #@2a
    iput v0, p0, Landroid/location/Location;->mBearing:F

    #@2c
    .line 140
    iget-boolean v0, p1, Landroid/location/Location;->mHasAccuracy:Z

    #@2e
    iput-boolean v0, p0, Landroid/location/Location;->mHasAccuracy:Z

    #@30
    .line 141
    iget v0, p1, Landroid/location/Location;->mAccuracy:F

    #@32
    iput v0, p0, Landroid/location/Location;->mAccuracy:F

    #@34
    .line 142
    iget-object v0, p1, Landroid/location/Location;->mExtras:Landroid/os/Bundle;

    #@36
    if-nez v0, :cond_3c

    #@38
    const/4 v0, 0x0

    #@39
    :goto_39
    iput-object v0, p0, Landroid/location/Location;->mExtras:Landroid/os/Bundle;

    #@3b
    .line 143
    return-void

    #@3c
    .line 142
    :cond_3c
    new-instance v0, Landroid/os/Bundle;

    #@3e
    iget-object v1, p1, Landroid/location/Location;->mExtras:Landroid/os/Bundle;

    #@40
    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@43
    goto :goto_39
.end method

.method public setAccuracy(F)V
    .registers 3
    .parameter "accuracy"

    #@0
    .prologue
    .line 743
    iput p1, p0, Landroid/location/Location;->mAccuracy:F

    #@2
    .line 744
    const/4 v0, 0x1

    #@3
    iput-boolean v0, p0, Landroid/location/Location;->mHasAccuracy:Z

    #@5
    .line 745
    return-void
.end method

.method public setAltitude(D)V
    .registers 4
    .parameter "altitude"

    #@0
    .prologue
    .line 597
    iput-wide p1, p0, Landroid/location/Location;->mAltitude:D

    #@2
    .line 598
    const/4 v0, 0x1

    #@3
    iput-boolean v0, p0, Landroid/location/Location;->mHasAltitude:Z

    #@5
    .line 599
    return-void
.end method

.method public setBearing(F)V
    .registers 4
    .parameter "bearing"

    #@0
    .prologue
    const/high16 v1, 0x43b4

    #@2
    .line 678
    :goto_2
    const/4 v0, 0x0

    #@3
    cmpg-float v0, p1, v0

    #@5
    if-gez v0, :cond_9

    #@7
    .line 679
    add-float/2addr p1, v1

    #@8
    goto :goto_2

    #@9
    .line 681
    :cond_9
    :goto_9
    cmpl-float v0, p1, v1

    #@b
    if-ltz v0, :cond_f

    #@d
    .line 682
    sub-float/2addr p1, v1

    #@e
    goto :goto_9

    #@f
    .line 684
    :cond_f
    iput p1, p0, Landroid/location/Location;->mBearing:F

    #@11
    .line 685
    const/4 v0, 0x1

    #@12
    iput-boolean v0, p0, Landroid/location/Location;->mHasBearing:Z

    #@14
    .line 686
    return-void
.end method

.method public setElapsedRealtimeNanos(J)V
    .registers 3
    .parameter "time"

    #@0
    .prologue
    .line 538
    iput-wide p1, p0, Landroid/location/Location;->mElapsedRealtimeNanos:J

    #@2
    .line 539
    return-void
.end method

.method public setExtraLocation(Ljava/lang/String;Landroid/location/Location;)V
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 932
    iget-object v0, p0, Landroid/location/Location;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 933
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/location/Location;->mExtras:Landroid/os/Bundle;

    #@b
    .line 935
    :cond_b
    iget-object v0, p0, Landroid/location/Location;->mExtras:Landroid/os/Bundle;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@10
    .line 936
    return-void
.end method

.method public setExtras(Landroid/os/Bundle;)V
    .registers 3
    .parameter "extras"

    #@0
    .prologue
    .line 820
    if-nez p1, :cond_6

    #@2
    const/4 v0, 0x0

    #@3
    :goto_3
    iput-object v0, p0, Landroid/location/Location;->mExtras:Landroid/os/Bundle;

    #@5
    .line 821
    return-void

    #@6
    .line 820
    :cond_6
    new-instance v0, Landroid/os/Bundle;

    #@8
    invoke-direct {v0, p1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@b
    goto :goto_3
.end method

.method public setLatitude(D)V
    .registers 3
    .parameter "latitude"

    #@0
    .prologue
    .line 555
    iput-wide p1, p0, Landroid/location/Location;->mLatitude:D

    #@2
    .line 556
    return-void
.end method

.method public setLongitude(D)V
    .registers 3
    .parameter "longitude"

    #@0
    .prologue
    .line 572
    iput-wide p1, p0, Landroid/location/Location;->mLongitude:D

    #@2
    .line 573
    return-void
.end method

.method public setProvider(Ljava/lang/String;)V
    .registers 2
    .parameter "provider"

    #@0
    .prologue
    .line 479
    iput-object p1, p0, Landroid/location/Location;->mProvider:Ljava/lang/String;

    #@2
    .line 480
    return-void
.end method

.method public setSpeed(F)V
    .registers 3
    .parameter "speed"

    #@0
    .prologue
    .line 634
    iput p1, p0, Landroid/location/Location;->mSpeed:F

    #@2
    .line 635
    const/4 v0, 0x1

    #@3
    iput-boolean v0, p0, Landroid/location/Location;->mHasSpeed:Z

    #@5
    .line 636
    return-void
.end method

.method public setTime(J)V
    .registers 3
    .parameter "time"

    #@0
    .prologue
    .line 510
    iput-wide p1, p0, Landroid/location/Location;->mTime:J

    #@2
    .line 511
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 10

    #@0
    .prologue
    const-wide/16 v7, 0x0

    #@2
    const/4 v6, 0x1

    #@3
    const/4 v5, 0x0

    #@4
    .line 825
    new-instance v0, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    .line 826
    .local v0, s:Ljava/lang/StringBuilder;
    const-string v1, "Location["

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    .line 827
    iget-object v1, p0, Landroid/location/Location;->mProvider:Ljava/lang/String;

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    .line 828
    const-string v1, " %.6f,%.6f"

    #@15
    const/4 v2, 0x2

    #@16
    new-array v2, v2, [Ljava/lang/Object;

    #@18
    iget-wide v3, p0, Landroid/location/Location;->mLatitude:D

    #@1a
    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    #@1d
    move-result-object v3

    #@1e
    aput-object v3, v2, v5

    #@20
    iget-wide v3, p0, Landroid/location/Location;->mLongitude:D

    #@22
    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    #@25
    move-result-object v3

    #@26
    aput-object v3, v2, v6

    #@28
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    .line 829
    iget-boolean v1, p0, Landroid/location/Location;->mHasAccuracy:Z

    #@31
    if-eqz v1, :cond_a8

    #@33
    const-string v1, " acc=%.0f"

    #@35
    new-array v2, v6, [Ljava/lang/Object;

    #@37
    iget v3, p0, Landroid/location/Location;->mAccuracy:F

    #@39
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@3c
    move-result-object v3

    #@3d
    aput-object v3, v2, v5

    #@3f
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@42
    move-result-object v1

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    .line 831
    :goto_46
    iget-wide v1, p0, Landroid/location/Location;->mTime:J

    #@48
    cmp-long v1, v1, v7

    #@4a
    if-nez v1, :cond_51

    #@4c
    .line 832
    const-string v1, " t=?!?"

    #@4e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    .line 834
    :cond_51
    iget-wide v1, p0, Landroid/location/Location;->mElapsedRealtimeNanos:J

    #@53
    cmp-long v1, v1, v7

    #@55
    if-nez v1, :cond_ae

    #@57
    .line 835
    const-string v1, " et=?!?"

    #@59
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    .line 840
    :goto_5c
    iget-boolean v1, p0, Landroid/location/Location;->mHasAltitude:Z

    #@5e
    if-eqz v1, :cond_6b

    #@60
    const-string v1, " alt="

    #@62
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v1

    #@66
    iget-wide v2, p0, Landroid/location/Location;->mAltitude:D

    #@68
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@6b
    .line 841
    :cond_6b
    iget-boolean v1, p0, Landroid/location/Location;->mHasSpeed:Z

    #@6d
    if-eqz v1, :cond_7a

    #@6f
    const-string v1, " vel="

    #@71
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v1

    #@75
    iget v2, p0, Landroid/location/Location;->mSpeed:F

    #@77
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@7a
    .line 842
    :cond_7a
    iget-boolean v1, p0, Landroid/location/Location;->mHasBearing:Z

    #@7c
    if-eqz v1, :cond_89

    #@7e
    const-string v1, " bear="

    #@80
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v1

    #@84
    iget v2, p0, Landroid/location/Location;->mBearing:F

    #@86
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@89
    .line 844
    :cond_89
    iget-object v1, p0, Landroid/location/Location;->mExtras:Landroid/os/Bundle;

    #@8b
    if-eqz v1, :cond_9e

    #@8d
    .line 845
    const-string v1, " {"

    #@8f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v1

    #@93
    iget-object v2, p0, Landroid/location/Location;->mExtras:Landroid/os/Bundle;

    #@95
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v1

    #@99
    const/16 v2, 0x7d

    #@9b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@9e
    .line 847
    :cond_9e
    const/16 v1, 0x5d

    #@a0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@a3
    .line 848
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a6
    move-result-object v1

    #@a7
    return-object v1

    #@a8
    .line 830
    :cond_a8
    const-string v1, " acc=???"

    #@aa
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    goto :goto_46

    #@ae
    .line 837
    :cond_ae
    const-string v1, " et="

    #@b0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    .line 838
    iget-wide v1, p0, Landroid/location/Location;->mElapsedRealtimeNanos:J

    #@b5
    const-wide/32 v3, 0xf4240

    #@b8
    div-long/2addr v1, v3

    #@b9
    invoke-static {v1, v2, v0}, Landroid/util/TimeUtils;->formatDuration(JLjava/lang/StringBuilder;)V

    #@bc
    goto :goto_5c
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 8
    .parameter "parcel"
    .parameter "flags"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 890
    iget-object v0, p0, Landroid/location/Location;->mProvider:Ljava/lang/String;

    #@4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@7
    .line 891
    iget-wide v3, p0, Landroid/location/Location;->mTime:J

    #@9
    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    #@c
    .line 892
    iget-wide v3, p0, Landroid/location/Location;->mElapsedRealtimeNanos:J

    #@e
    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    #@11
    .line 893
    iget-wide v3, p0, Landroid/location/Location;->mLatitude:D

    #@13
    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeDouble(D)V

    #@16
    .line 894
    iget-wide v3, p0, Landroid/location/Location;->mLongitude:D

    #@18
    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeDouble(D)V

    #@1b
    .line 895
    iget-boolean v0, p0, Landroid/location/Location;->mHasAltitude:Z

    #@1d
    if-eqz v0, :cond_54

    #@1f
    move v0, v1

    #@20
    :goto_20
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    .line 896
    iget-wide v3, p0, Landroid/location/Location;->mAltitude:D

    #@25
    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeDouble(D)V

    #@28
    .line 897
    iget-boolean v0, p0, Landroid/location/Location;->mHasSpeed:Z

    #@2a
    if-eqz v0, :cond_56

    #@2c
    move v0, v1

    #@2d
    :goto_2d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@30
    .line 898
    iget v0, p0, Landroid/location/Location;->mSpeed:F

    #@32
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@35
    .line 899
    iget-boolean v0, p0, Landroid/location/Location;->mHasBearing:Z

    #@37
    if-eqz v0, :cond_58

    #@39
    move v0, v1

    #@3a
    :goto_3a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@3d
    .line 900
    iget v0, p0, Landroid/location/Location;->mBearing:F

    #@3f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@42
    .line 901
    iget-boolean v0, p0, Landroid/location/Location;->mHasAccuracy:Z

    #@44
    if-eqz v0, :cond_5a

    #@46
    :goto_46
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@49
    .line 902
    iget v0, p0, Landroid/location/Location;->mAccuracy:F

    #@4b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@4e
    .line 903
    iget-object v0, p0, Landroid/location/Location;->mExtras:Landroid/os/Bundle;

    #@50
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    #@53
    .line 904
    return-void

    #@54
    :cond_54
    move v0, v2

    #@55
    .line 895
    goto :goto_20

    #@56
    :cond_56
    move v0, v2

    #@57
    .line 897
    goto :goto_2d

    #@58
    :cond_58
    move v0, v2

    #@59
    .line 899
    goto :goto_3a

    #@5a
    :cond_5a
    move v1, v2

    #@5b
    .line 901
    goto :goto_46
.end method
