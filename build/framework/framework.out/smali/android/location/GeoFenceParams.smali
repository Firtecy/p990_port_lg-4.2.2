.class public Landroid/location/GeoFenceParams;
.super Ljava/lang/Object;
.source "GeoFenceParams.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/location/GeoFenceParams;",
            ">;"
        }
    .end annotation
.end field

.field public static final ENTERING:I = 0x1

.field public static final LEAVING:I = 0x2


# instance fields
.field public final mExpiration:J

.field public final mIntent:Landroid/app/PendingIntent;

.field public final mLatitude:D

.field public final mLongitude:D

.field public final mPackageName:Ljava/lang/String;

.field public final mRadius:F

.field public final mUid:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 44
    new-instance v0, Landroid/location/GeoFenceParams$1;

    #@2
    invoke-direct {v0}, Landroid/location/GeoFenceParams$1;-><init>()V

    #@5
    sput-object v0, Landroid/location/GeoFenceParams;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(DDFJLandroid/app/PendingIntent;Ljava/lang/String;)V
    .registers 21
    .parameter "lat"
    .parameter "lon"
    .parameter "r"
    .parameter "expire"
    .parameter "intent"
    .parameter "packageName"

    #@0
    .prologue
    .line 57
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v1

    #@4
    move-object v0, p0

    #@5
    move-wide v2, p1

    #@6
    move-wide v4, p3

    #@7
    move/from16 v6, p5

    #@9
    move-wide/from16 v7, p6

    #@b
    move-object/from16 v9, p8

    #@d
    move-object/from16 v10, p9

    #@f
    invoke-direct/range {v0 .. v10}, Landroid/location/GeoFenceParams;-><init>(IDDFJLandroid/app/PendingIntent;Ljava/lang/String;)V

    #@12
    .line 58
    return-void
.end method

.method public constructor <init>(IDDFJLandroid/app/PendingIntent;Ljava/lang/String;)V
    .registers 11
    .parameter "uid"
    .parameter "lat"
    .parameter "lon"
    .parameter "r"
    .parameter "expire"
    .parameter "intent"
    .parameter "packageName"

    #@0
    .prologue
    .line 61
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 62
    iput p1, p0, Landroid/location/GeoFenceParams;->mUid:I

    #@5
    .line 63
    iput-wide p2, p0, Landroid/location/GeoFenceParams;->mLatitude:D

    #@7
    .line 64
    iput-wide p4, p0, Landroid/location/GeoFenceParams;->mLongitude:D

    #@9
    .line 65
    iput p6, p0, Landroid/location/GeoFenceParams;->mRadius:F

    #@b
    .line 66
    iput-wide p7, p0, Landroid/location/GeoFenceParams;->mExpiration:J

    #@d
    .line 67
    iput-object p9, p0, Landroid/location/GeoFenceParams;->mIntent:Landroid/app/PendingIntent;

    #@f
    .line 68
    iput-object p10, p0, Landroid/location/GeoFenceParams;->mPackageName:Ljava/lang/String;

    #@11
    .line 69
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "in"

    #@0
    .prologue
    .line 71
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/location/GeoFenceParams;->mUid:I

    #@9
    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    #@c
    move-result-wide v0

    #@d
    iput-wide v0, p0, Landroid/location/GeoFenceParams;->mLatitude:D

    #@f
    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    #@12
    move-result-wide v0

    #@13
    iput-wide v0, p0, Landroid/location/GeoFenceParams;->mLongitude:D

    #@15
    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@18
    move-result v0

    #@19
    iput v0, p0, Landroid/location/GeoFenceParams;->mRadius:F

    #@1b
    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@1e
    move-result-wide v0

    #@1f
    iput-wide v0, p0, Landroid/location/GeoFenceParams;->mExpiration:J

    #@21
    .line 77
    const/4 v0, 0x0

    #@22
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    #@25
    move-result-object v0

    #@26
    check-cast v0, Landroid/app/PendingIntent;

    #@28
    iput-object v0, p0, Landroid/location/GeoFenceParams;->mIntent:Landroid/app/PendingIntent;

    #@2a
    .line 78
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2d
    move-result-object v0

    #@2e
    iput-object v0, p0, Landroid/location/GeoFenceParams;->mPackageName:Ljava/lang/String;

    #@30
    .line 79
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/location/GeoFenceParams$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 33
    invoke-direct {p0, p1}, Landroid/location/GeoFenceParams;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 83
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .registers 6
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@14
    .line 129
    new-instance v0, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string/jumbo v1, "mLatitude="

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v0

    #@24
    iget-wide v1, p0, Landroid/location/GeoFenceParams;->mLatitude:D

    #@26
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@29
    move-result-object v0

    #@2a
    const-string v1, " mLongitude="

    #@2c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v0

    #@30
    iget-wide v1, p0, Landroid/location/GeoFenceParams;->mLongitude:D

    #@32
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@35
    move-result-object v0

    #@36
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v0

    #@3a
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3d
    .line 130
    new-instance v0, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v0

    #@46
    const-string/jumbo v1, "mRadius="

    #@49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    iget v1, p0, Landroid/location/GeoFenceParams;->mRadius:F

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@52
    move-result-object v0

    #@53
    const-string v1, " mExpiration="

    #@55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v0

    #@59
    iget-wide v1, p0, Landroid/location/GeoFenceParams;->mExpiration:J

    #@5b
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v0

    #@5f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v0

    #@63
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@66
    .line 131
    return-void
.end method

.method public getCallerUid()I
    .registers 2

    #@0
    .prologue
    .line 124
    iget v0, p0, Landroid/location/GeoFenceParams;->mUid:I

    #@2
    return v0
.end method

.method public getExpiration()J
    .registers 3

    #@0
    .prologue
    .line 116
    iget-wide v0, p0, Landroid/location/GeoFenceParams;->mExpiration:J

    #@2
    return-wide v0
.end method

.method public getIntent()Landroid/app/PendingIntent;
    .registers 2

    #@0
    .prologue
    .line 120
    iget-object v0, p0, Landroid/location/GeoFenceParams;->mIntent:Landroid/app/PendingIntent;

    #@2
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 100
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "GeoFenceParams:\n\tmUid - "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 101
    iget v1, p0, Landroid/location/GeoFenceParams;->mUid:I

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f
    .line 102
    const-string v1, "\n\tmLatitide - "

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    .line 103
    iget-wide v1, p0, Landroid/location/GeoFenceParams;->mLatitude:D

    #@16
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@19
    .line 104
    const-string v1, "\n\tmLongitude - "

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    .line 105
    iget-wide v1, p0, Landroid/location/GeoFenceParams;->mLongitude:D

    #@20
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@23
    .line 106
    const-string v1, "\n\tmRadius - "

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    .line 107
    iget v1, p0, Landroid/location/GeoFenceParams;->mRadius:F

    #@2a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@2d
    .line 108
    const-string v1, "\n\tmExpiration - "

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    .line 109
    iget-wide v1, p0, Landroid/location/GeoFenceParams;->mExpiration:J

    #@34
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@37
    .line 110
    const-string v1, "\n\tmIntent - "

    #@39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    .line 111
    iget-object v1, p0, Landroid/location/GeoFenceParams;->mIntent:Landroid/app/PendingIntent;

    #@3e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@41
    .line 112
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v1

    #@45
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 88
    iget v0, p0, Landroid/location/GeoFenceParams;->mUid:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 89
    iget-wide v0, p0, Landroid/location/GeoFenceParams;->mLatitude:D

    #@7
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    #@a
    .line 90
    iget-wide v0, p0, Landroid/location/GeoFenceParams;->mLongitude:D

    #@c
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    #@f
    .line 91
    iget v0, p0, Landroid/location/GeoFenceParams;->mRadius:F

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@14
    .line 92
    iget-wide v0, p0, Landroid/location/GeoFenceParams;->mExpiration:J

    #@16
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@19
    .line 93
    iget-object v0, p0, Landroid/location/GeoFenceParams;->mIntent:Landroid/app/PendingIntent;

    #@1b
    const/4 v1, 0x0

    #@1c
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    #@1f
    .line 94
    iget-object v0, p0, Landroid/location/GeoFenceParams;->mPackageName:Ljava/lang/String;

    #@21
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@24
    .line 95
    return-void
.end method
