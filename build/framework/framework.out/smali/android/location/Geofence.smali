.class public final Landroid/location/Geofence;
.super Ljava/lang/Object;
.source "Geofence.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/location/Geofence;",
            ">;"
        }
    .end annotation
.end field

.field public static final TYPE_HORIZONTAL_CIRCLE:I = 0x1


# instance fields
.field private final mLatitude:D

.field private final mLongitude:D

.field private final mRadius:F

.field private final mType:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 101
    new-instance v0, Landroid/location/Geofence$1;

    #@2
    invoke-direct {v0}, Landroid/location/Geofence$1;-><init>()V

    #@5
    sput-object v0, Landroid/location/Geofence;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method private constructor <init>(DDF)V
    .registers 7
    .parameter "latitude"
    .parameter "longitude"
    .parameter "radius"

    #@0
    .prologue
    .line 51
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 52
    invoke-static {p5}, Landroid/location/Geofence;->checkRadius(F)V

    #@6
    .line 53
    invoke-static {p1, p2, p3, p4}, Landroid/location/Geofence;->checkLatLong(DD)V

    #@9
    .line 54
    const/4 v0, 0x1

    #@a
    iput v0, p0, Landroid/location/Geofence;->mType:I

    #@c
    .line 55
    iput-wide p1, p0, Landroid/location/Geofence;->mLatitude:D

    #@e
    .line 56
    iput-wide p3, p0, Landroid/location/Geofence;->mLongitude:D

    #@10
    .line 57
    iput p5, p0, Landroid/location/Geofence;->mRadius:F

    #@12
    .line 58
    return-void
.end method

.method static synthetic access$000(I)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 29
    invoke-static {p0}, Landroid/location/Geofence;->checkType(I)V

    #@3
    return-void
.end method

.method private static checkLatLong(DD)V
    .registers 7
    .parameter "latitude"
    .parameter "longitude"

    #@0
    .prologue
    .line 87
    const-wide v0, 0x4056800000000000L

    #@5
    cmpl-double v0, p0, v0

    #@7
    if-gtz v0, :cond_12

    #@9
    const-wide v0, -0x3fa9800000000000L

    #@e
    cmpg-double v0, p0, v0

    #@10
    if-gez v0, :cond_2b

    #@12
    .line 88
    :cond_12
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@14
    new-instance v1, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v2, "invalid latitude: "

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2a
    throw v0

    #@2b
    .line 90
    :cond_2b
    const-wide v0, 0x4066800000000000L

    #@30
    cmpl-double v0, p2, v0

    #@32
    if-gtz v0, :cond_3d

    #@34
    const-wide v0, -0x3f99800000000000L

    #@39
    cmpg-double v0, p2, v0

    #@3b
    if-gez v0, :cond_56

    #@3d
    .line 91
    :cond_3d
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@3f
    new-instance v1, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v2, "invalid longitude: "

    #@46
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v1

    #@4e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v1

    #@52
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@55
    throw v0

    #@56
    .line 93
    :cond_56
    return-void
.end method

.method private static checkRadius(F)V
    .registers 4
    .parameter "radius"

    #@0
    .prologue
    .line 81
    const/4 v0, 0x0

    #@1
    cmpg-float v0, p0, v0

    #@3
    if-gtz v0, :cond_1e

    #@5
    .line 82
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "invalid radius: "

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v0

    #@1e
    .line 84
    :cond_1e
    return-void
.end method

.method private static checkType(I)V
    .registers 4
    .parameter "type"

    #@0
    .prologue
    .line 96
    const/4 v0, 0x1

    #@1
    if-eq p0, v0, :cond_1c

    #@3
    .line 97
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "invalid type: "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 99
    :cond_1c
    return-void
.end method

.method public static createCircle(DDF)Landroid/location/Geofence;
    .registers 11
    .parameter "latitude"
    .parameter "longitude"
    .parameter "radius"

    #@0
    .prologue
    .line 48
    new-instance v0, Landroid/location/Geofence;

    #@2
    move-wide v1, p0

    #@3
    move-wide v3, p2

    #@4
    move v5, p4

    #@5
    invoke-direct/range {v0 .. v5}, Landroid/location/Geofence;-><init>(DDF)V

    #@8
    return-object v0
.end method

.method private static typeToString(I)Ljava/lang/String;
    .registers 2
    .parameter "type"

    #@0
    .prologue
    .line 131
    packed-switch p0, :pswitch_data_c

    #@3
    .line 135
    invoke-static {p0}, Landroid/location/Geofence;->checkType(I)V

    #@6
    .line 136
    const/4 v0, 0x0

    #@7
    :goto_7
    return-object v0

    #@8
    .line 133
    :pswitch_8
    const-string v0, "CIRCLE"

    #@a
    goto :goto_7

    #@b
    .line 131
    nop

    #@c
    :pswitch_data_c
    .packed-switch 0x1
        :pswitch_8
    .end packed-switch
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 119
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 9
    .parameter "obj"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 165
    if-ne p0, p1, :cond_5

    #@4
    .line 180
    :cond_4
    :goto_4
    return v1

    #@5
    .line 167
    :cond_5
    if-nez p1, :cond_9

    #@7
    move v1, v2

    #@8
    .line 168
    goto :goto_4

    #@9
    .line 169
    :cond_9
    instance-of v3, p1, Landroid/location/Geofence;

    #@b
    if-nez v3, :cond_f

    #@d
    move v1, v2

    #@e
    .line 170
    goto :goto_4

    #@f
    :cond_f
    move-object v0, p1

    #@10
    .line 171
    check-cast v0, Landroid/location/Geofence;

    #@12
    .line 172
    .local v0, other:Landroid/location/Geofence;
    iget v3, p0, Landroid/location/Geofence;->mRadius:F

    #@14
    iget v4, v0, Landroid/location/Geofence;->mRadius:F

    #@16
    cmpl-float v3, v3, v4

    #@18
    if-eqz v3, :cond_1c

    #@1a
    move v1, v2

    #@1b
    .line 173
    goto :goto_4

    #@1c
    .line 174
    :cond_1c
    iget-wide v3, p0, Landroid/location/Geofence;->mLatitude:D

    #@1e
    iget-wide v5, v0, Landroid/location/Geofence;->mLatitude:D

    #@20
    cmpl-double v3, v3, v5

    #@22
    if-eqz v3, :cond_26

    #@24
    move v1, v2

    #@25
    .line 175
    goto :goto_4

    #@26
    .line 176
    :cond_26
    iget-wide v3, p0, Landroid/location/Geofence;->mLongitude:D

    #@28
    iget-wide v5, v0, Landroid/location/Geofence;->mLongitude:D

    #@2a
    cmpl-double v3, v3, v5

    #@2c
    if-eqz v3, :cond_30

    #@2e
    move v1, v2

    #@2f
    .line 177
    goto :goto_4

    #@30
    .line 178
    :cond_30
    iget v3, p0, Landroid/location/Geofence;->mType:I

    #@32
    iget v4, v0, Landroid/location/Geofence;->mType:I

    #@34
    if-eq v3, v4, :cond_4

    #@36
    move v1, v2

    #@37
    .line 179
    goto :goto_4
.end method

.method public getLatitude()D
    .registers 3

    #@0
    .prologue
    .line 67
    iget-wide v0, p0, Landroid/location/Geofence;->mLatitude:D

    #@2
    return-wide v0
.end method

.method public getLongitude()D
    .registers 3

    #@0
    .prologue
    .line 72
    iget-wide v0, p0, Landroid/location/Geofence;->mLongitude:D

    #@2
    return-wide v0
.end method

.method public getRadius()F
    .registers 2

    #@0
    .prologue
    .line 77
    iget v0, p0, Landroid/location/Geofence;->mRadius:F

    #@2
    return v0
.end method

.method public getType()I
    .registers 2

    #@0
    .prologue
    .line 62
    iget v0, p0, Landroid/location/Geofence;->mType:I

    #@2
    return v0
.end method

.method public hashCode()I
    .registers 8

    #@0
    .prologue
    const/16 v6, 0x20

    #@2
    .line 148
    const/16 v0, 0x1f

    #@4
    .line 149
    .local v0, prime:I
    const/4 v1, 0x1

    #@5
    .line 151
    .local v1, result:I
    iget-wide v4, p0, Landroid/location/Geofence;->mLatitude:D

    #@7
    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    #@a
    move-result-wide v2

    #@b
    .line 152
    .local v2, temp:J
    ushr-long v4, v2, v6

    #@d
    xor-long/2addr v4, v2

    #@e
    long-to-int v4, v4

    #@f
    add-int/lit8 v1, v4, 0x1f

    #@11
    .line 153
    iget-wide v4, p0, Landroid/location/Geofence;->mLongitude:D

    #@13
    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    #@16
    move-result-wide v2

    #@17
    .line 154
    mul-int/lit8 v4, v1, 0x1f

    #@19
    ushr-long v5, v2, v6

    #@1b
    xor-long/2addr v5, v2

    #@1c
    long-to-int v5, v5

    #@1d
    add-int v1, v4, v5

    #@1f
    .line 155
    mul-int/lit8 v4, v1, 0x1f

    #@21
    iget v5, p0, Landroid/location/Geofence;->mRadius:F

    #@23
    invoke-static {v5}, Ljava/lang/Float;->floatToIntBits(F)I

    #@26
    move-result v5

    #@27
    add-int v1, v4, v5

    #@29
    .line 156
    mul-int/lit8 v4, v1, 0x1f

    #@2b
    iget v5, p0, Landroid/location/Geofence;->mType:I

    #@2d
    add-int v1, v4, v5

    #@2f
    .line 157
    return v1
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 142
    const-string v0, "Geofence[%s %.6f, %.6f %.0fm]"

    #@2
    const/4 v1, 0x4

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    iget v3, p0, Landroid/location/Geofence;->mType:I

    #@8
    invoke-static {v3}, Landroid/location/Geofence;->typeToString(I)Ljava/lang/String;

    #@b
    move-result-object v3

    #@c
    aput-object v3, v1, v2

    #@e
    const/4 v2, 0x1

    #@f
    iget-wide v3, p0, Landroid/location/Geofence;->mLatitude:D

    #@11
    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    #@14
    move-result-object v3

    #@15
    aput-object v3, v1, v2

    #@17
    const/4 v2, 0x2

    #@18
    iget-wide v3, p0, Landroid/location/Geofence;->mLongitude:D

    #@1a
    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    #@1d
    move-result-object v3

    #@1e
    aput-object v3, v1, v2

    #@20
    const/4 v2, 0x3

    #@21
    iget v3, p0, Landroid/location/Geofence;->mRadius:F

    #@23
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@26
    move-result-object v3

    #@27
    aput-object v3, v1, v2

    #@29
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2c
    move-result-object v0

    #@2d
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "parcel"
    .parameter "flags"

    #@0
    .prologue
    .line 124
    iget v0, p0, Landroid/location/Geofence;->mType:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 125
    iget-wide v0, p0, Landroid/location/Geofence;->mLatitude:D

    #@7
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    #@a
    .line 126
    iget-wide v0, p0, Landroid/location/Geofence;->mLongitude:D

    #@c
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    #@f
    .line 127
    iget v0, p0, Landroid/location/Geofence;->mRadius:F

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@14
    .line 128
    return-void
.end method
