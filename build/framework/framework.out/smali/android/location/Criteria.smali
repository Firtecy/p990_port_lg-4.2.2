.class public Landroid/location/Criteria;
.super Ljava/lang/Object;
.source "Criteria.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final ACCURACY_COARSE:I = 0x2

.field public static final ACCURACY_FINE:I = 0x1

.field public static final ACCURACY_HIGH:I = 0x3

.field public static final ACCURACY_LOW:I = 0x1

.field public static final ACCURACY_MEDIUM:I = 0x2

.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/location/Criteria;",
            ">;"
        }
    .end annotation
.end field

.field public static final NO_REQUIREMENT:I = 0x0

.field public static final POWER_HIGH:I = 0x3

.field public static final POWER_LOW:I = 0x1

.field public static final POWER_MEDIUM:I = 0x2


# instance fields
.field private mAltitudeRequired:Z

.field private mBearingAccuracy:I

.field private mBearingRequired:Z

.field private mCostAllowed:Z

.field private mHorizontalAccuracy:I

.field private mPowerRequirement:I

.field private mSpeedAccuracy:I

.field private mSpeedRequired:Z

.field private mVerticalAccuracy:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 327
    new-instance v0, Landroid/location/Criteria$1;

    #@2
    invoke-direct {v0}, Landroid/location/Criteria$1;-><init>()V

    #@5
    sput-object v0, Landroid/location/Criteria;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 100
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 84
    iput v0, p0, Landroid/location/Criteria;->mHorizontalAccuracy:I

    #@6
    .line 85
    iput v0, p0, Landroid/location/Criteria;->mVerticalAccuracy:I

    #@8
    .line 86
    iput v0, p0, Landroid/location/Criteria;->mSpeedAccuracy:I

    #@a
    .line 87
    iput v0, p0, Landroid/location/Criteria;->mBearingAccuracy:I

    #@c
    .line 88
    iput v0, p0, Landroid/location/Criteria;->mPowerRequirement:I

    #@e
    .line 89
    iput-boolean v0, p0, Landroid/location/Criteria;->mAltitudeRequired:Z

    #@10
    .line 90
    iput-boolean v0, p0, Landroid/location/Criteria;->mBearingRequired:Z

    #@12
    .line 91
    iput-boolean v0, p0, Landroid/location/Criteria;->mSpeedRequired:Z

    #@14
    .line 92
    iput-boolean v0, p0, Landroid/location/Criteria;->mCostAllowed:Z

    #@16
    .line 100
    return-void
.end method

.method public constructor <init>(Landroid/location/Criteria;)V
    .registers 3
    .parameter "criteria"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 105
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 84
    iput v0, p0, Landroid/location/Criteria;->mHorizontalAccuracy:I

    #@6
    .line 85
    iput v0, p0, Landroid/location/Criteria;->mVerticalAccuracy:I

    #@8
    .line 86
    iput v0, p0, Landroid/location/Criteria;->mSpeedAccuracy:I

    #@a
    .line 87
    iput v0, p0, Landroid/location/Criteria;->mBearingAccuracy:I

    #@c
    .line 88
    iput v0, p0, Landroid/location/Criteria;->mPowerRequirement:I

    #@e
    .line 89
    iput-boolean v0, p0, Landroid/location/Criteria;->mAltitudeRequired:Z

    #@10
    .line 90
    iput-boolean v0, p0, Landroid/location/Criteria;->mBearingRequired:Z

    #@12
    .line 91
    iput-boolean v0, p0, Landroid/location/Criteria;->mSpeedRequired:Z

    #@14
    .line 92
    iput-boolean v0, p0, Landroid/location/Criteria;->mCostAllowed:Z

    #@16
    .line 106
    iget v0, p1, Landroid/location/Criteria;->mHorizontalAccuracy:I

    #@18
    iput v0, p0, Landroid/location/Criteria;->mHorizontalAccuracy:I

    #@1a
    .line 107
    iget v0, p1, Landroid/location/Criteria;->mVerticalAccuracy:I

    #@1c
    iput v0, p0, Landroid/location/Criteria;->mVerticalAccuracy:I

    #@1e
    .line 108
    iget v0, p1, Landroid/location/Criteria;->mSpeedAccuracy:I

    #@20
    iput v0, p0, Landroid/location/Criteria;->mSpeedAccuracy:I

    #@22
    .line 109
    iget v0, p1, Landroid/location/Criteria;->mBearingAccuracy:I

    #@24
    iput v0, p0, Landroid/location/Criteria;->mBearingAccuracy:I

    #@26
    .line 110
    iget v0, p1, Landroid/location/Criteria;->mPowerRequirement:I

    #@28
    iput v0, p0, Landroid/location/Criteria;->mPowerRequirement:I

    #@2a
    .line 111
    iget-boolean v0, p1, Landroid/location/Criteria;->mAltitudeRequired:Z

    #@2c
    iput-boolean v0, p0, Landroid/location/Criteria;->mAltitudeRequired:Z

    #@2e
    .line 112
    iget-boolean v0, p1, Landroid/location/Criteria;->mBearingRequired:Z

    #@30
    iput-boolean v0, p0, Landroid/location/Criteria;->mBearingRequired:Z

    #@32
    .line 113
    iget-boolean v0, p1, Landroid/location/Criteria;->mSpeedRequired:Z

    #@34
    iput-boolean v0, p0, Landroid/location/Criteria;->mSpeedRequired:Z

    #@36
    .line 114
    iget-boolean v0, p1, Landroid/location/Criteria;->mCostAllowed:Z

    #@38
    iput-boolean v0, p0, Landroid/location/Criteria;->mCostAllowed:Z

    #@3a
    .line 115
    return-void
.end method

.method static synthetic access$002(Landroid/location/Criteria;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 28
    iput p1, p0, Landroid/location/Criteria;->mHorizontalAccuracy:I

    #@2
    return p1
.end method

.method static synthetic access$102(Landroid/location/Criteria;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 28
    iput p1, p0, Landroid/location/Criteria;->mVerticalAccuracy:I

    #@2
    return p1
.end method

.method static synthetic access$202(Landroid/location/Criteria;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 28
    iput p1, p0, Landroid/location/Criteria;->mSpeedAccuracy:I

    #@2
    return p1
.end method

.method static synthetic access$302(Landroid/location/Criteria;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 28
    iput p1, p0, Landroid/location/Criteria;->mBearingAccuracy:I

    #@2
    return p1
.end method

.method static synthetic access$402(Landroid/location/Criteria;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 28
    iput p1, p0, Landroid/location/Criteria;->mPowerRequirement:I

    #@2
    return p1
.end method

.method static synthetic access$502(Landroid/location/Criteria;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 28
    iput-boolean p1, p0, Landroid/location/Criteria;->mAltitudeRequired:Z

    #@2
    return p1
.end method

.method static synthetic access$602(Landroid/location/Criteria;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 28
    iput-boolean p1, p0, Landroid/location/Criteria;->mBearingRequired:Z

    #@2
    return p1
.end method

.method static synthetic access$702(Landroid/location/Criteria;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 28
    iput-boolean p1, p0, Landroid/location/Criteria;->mSpeedRequired:Z

    #@2
    return p1
.end method

.method static synthetic access$802(Landroid/location/Criteria;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 28
    iput-boolean p1, p0, Landroid/location/Criteria;->mCostAllowed:Z

    #@2
    return p1
.end method

.method private static accuracyToString(I)Ljava/lang/String;
    .registers 2
    .parameter "accuracy"

    #@0
    .prologue
    .line 384
    packed-switch p0, :pswitch_data_12

    #@3
    .line 394
    const-string v0, "???"

    #@5
    :goto_5
    return-object v0

    #@6
    .line 386
    :pswitch_6
    const-string v0, "---"

    #@8
    goto :goto_5

    #@9
    .line 388
    :pswitch_9
    const-string v0, "HIGH"

    #@b
    goto :goto_5

    #@c
    .line 390
    :pswitch_c
    const-string v0, "MEDIUM"

    #@e
    goto :goto_5

    #@f
    .line 392
    :pswitch_f
    const-string v0, "LOW"

    #@11
    goto :goto_5

    #@12
    .line 384
    :pswitch_data_12
    .packed-switch 0x0
        :pswitch_6
        :pswitch_f
        :pswitch_c
        :pswitch_9
    .end packed-switch
.end method

.method private static powerToString(I)Ljava/lang/String;
    .registers 2
    .parameter "power"

    #@0
    .prologue
    .line 369
    packed-switch p0, :pswitch_data_12

    #@3
    .line 379
    const-string v0, "???"

    #@5
    :goto_5
    return-object v0

    #@6
    .line 371
    :pswitch_6
    const-string v0, "NO_REQ"

    #@8
    goto :goto_5

    #@9
    .line 373
    :pswitch_9
    const-string v0, "LOW"

    #@b
    goto :goto_5

    #@c
    .line 375
    :pswitch_c
    const-string v0, "MEDIUM"

    #@e
    goto :goto_5

    #@f
    .line 377
    :pswitch_f
    const-string v0, "HIGH"

    #@11
    goto :goto_5

    #@12
    .line 369
    :pswitch_data_12
    .packed-switch 0x0
        :pswitch_6
        :pswitch_9
        :pswitch_c
        :pswitch_f
    .end packed-switch
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 352
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getAccuracy()I
    .registers 3

    #@0
    .prologue
    .line 238
    iget v0, p0, Landroid/location/Criteria;->mHorizontalAccuracy:I

    #@2
    const/4 v1, 0x3

    #@3
    if-lt v0, v1, :cond_7

    #@5
    .line 239
    const/4 v0, 0x1

    #@6
    .line 241
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x2

    #@8
    goto :goto_6
.end method

.method public getBearingAccuracy()I
    .registers 2

    #@0
    .prologue
    .line 210
    iget v0, p0, Landroid/location/Criteria;->mBearingAccuracy:I

    #@2
    return v0
.end method

.method public getHorizontalAccuracy()I
    .registers 2

    #@0
    .prologue
    .line 138
    iget v0, p0, Landroid/location/Criteria;->mHorizontalAccuracy:I

    #@2
    return v0
.end method

.method public getPowerRequirement()I
    .registers 2

    #@0
    .prologue
    .line 262
    iget v0, p0, Landroid/location/Criteria;->mPowerRequirement:I

    #@2
    return v0
.end method

.method public getSpeedAccuracy()I
    .registers 2

    #@0
    .prologue
    .line 186
    iget v0, p0, Landroid/location/Criteria;->mSpeedAccuracy:I

    #@2
    return v0
.end method

.method public getVerticalAccuracy()I
    .registers 2

    #@0
    .prologue
    .line 162
    iget v0, p0, Landroid/location/Criteria;->mVerticalAccuracy:I

    #@2
    return v0
.end method

.method public isAltitudeRequired()Z
    .registers 2

    #@0
    .prologue
    .line 292
    iget-boolean v0, p0, Landroid/location/Criteria;->mAltitudeRequired:Z

    #@2
    return v0
.end method

.method public isBearingRequired()Z
    .registers 2

    #@0
    .prologue
    .line 324
    iget-boolean v0, p0, Landroid/location/Criteria;->mBearingRequired:Z

    #@2
    return v0
.end method

.method public isCostAllowed()Z
    .registers 2

    #@0
    .prologue
    .line 276
    iget-boolean v0, p0, Landroid/location/Criteria;->mCostAllowed:Z

    #@2
    return v0
.end method

.method public isSpeedRequired()Z
    .registers 2

    #@0
    .prologue
    .line 308
    iget-boolean v0, p0, Landroid/location/Criteria;->mSpeedRequired:Z

    #@2
    return v0
.end method

.method public setAccuracy(I)V
    .registers 5
    .parameter "accuracy"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 222
    if-ltz p1, :cond_6

    #@3
    const/4 v0, 0x2

    #@4
    if-le p1, v0, :cond_1f

    #@6
    .line 223
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "accuracy="

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v0

    #@1f
    .line 225
    :cond_1f
    if-ne p1, v1, :cond_25

    #@21
    .line 226
    const/4 v0, 0x3

    #@22
    iput v0, p0, Landroid/location/Criteria;->mHorizontalAccuracy:I

    #@24
    .line 230
    :goto_24
    return-void

    #@25
    .line 228
    :cond_25
    iput v1, p0, Landroid/location/Criteria;->mHorizontalAccuracy:I

    #@27
    goto :goto_24
.end method

.method public setAltitudeRequired(Z)V
    .registers 2
    .parameter "altitudeRequired"

    #@0
    .prologue
    .line 284
    iput-boolean p1, p0, Landroid/location/Criteria;->mAltitudeRequired:Z

    #@2
    .line 285
    return-void
.end method

.method public setBearingAccuracy(I)V
    .registers 5
    .parameter "accuracy"

    #@0
    .prologue
    .line 198
    if-ltz p1, :cond_5

    #@2
    const/4 v0, 0x3

    #@3
    if-le p1, v0, :cond_1e

    #@5
    .line 199
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "accuracy="

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v0

    #@1e
    .line 201
    :cond_1e
    iput p1, p0, Landroid/location/Criteria;->mBearingAccuracy:I

    #@20
    .line 202
    return-void
.end method

.method public setBearingRequired(Z)V
    .registers 2
    .parameter "bearingRequired"

    #@0
    .prologue
    .line 316
    iput-boolean p1, p0, Landroid/location/Criteria;->mBearingRequired:Z

    #@2
    .line 317
    return-void
.end method

.method public setCostAllowed(Z)V
    .registers 2
    .parameter "costAllowed"

    #@0
    .prologue
    .line 269
    iput-boolean p1, p0, Landroid/location/Criteria;->mCostAllowed:Z

    #@2
    .line 270
    return-void
.end method

.method public setHorizontalAccuracy(I)V
    .registers 5
    .parameter "accuracy"

    #@0
    .prologue
    .line 126
    if-ltz p1, :cond_5

    #@2
    const/4 v0, 0x3

    #@3
    if-le p1, v0, :cond_1e

    #@5
    .line 127
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "accuracy="

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v0

    #@1e
    .line 129
    :cond_1e
    iput p1, p0, Landroid/location/Criteria;->mHorizontalAccuracy:I

    #@20
    .line 130
    return-void
.end method

.method public setPowerRequirement(I)V
    .registers 5
    .parameter "level"

    #@0
    .prologue
    .line 251
    if-ltz p1, :cond_5

    #@2
    const/4 v0, 0x3

    #@3
    if-le p1, v0, :cond_1f

    #@5
    .line 252
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string/jumbo v2, "level="

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v0

    #@1f
    .line 254
    :cond_1f
    iput p1, p0, Landroid/location/Criteria;->mPowerRequirement:I

    #@21
    .line 255
    return-void
.end method

.method public setSpeedAccuracy(I)V
    .registers 5
    .parameter "accuracy"

    #@0
    .prologue
    .line 174
    if-ltz p1, :cond_5

    #@2
    const/4 v0, 0x3

    #@3
    if-le p1, v0, :cond_1e

    #@5
    .line 175
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "accuracy="

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v0

    #@1e
    .line 177
    :cond_1e
    iput p1, p0, Landroid/location/Criteria;->mSpeedAccuracy:I

    #@20
    .line 178
    return-void
.end method

.method public setSpeedRequired(Z)V
    .registers 2
    .parameter "speedRequired"

    #@0
    .prologue
    .line 300
    iput-boolean p1, p0, Landroid/location/Criteria;->mSpeedRequired:Z

    #@2
    .line 301
    return-void
.end method

.method public setVerticalAccuracy(I)V
    .registers 5
    .parameter "accuracy"

    #@0
    .prologue
    .line 150
    if-ltz p1, :cond_5

    #@2
    const/4 v0, 0x3

    #@3
    if-le p1, v0, :cond_1e

    #@5
    .line 151
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "accuracy="

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v0

    #@1e
    .line 153
    :cond_1e
    iput p1, p0, Landroid/location/Criteria;->mVerticalAccuracy:I

    #@20
    .line 154
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 400
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 401
    .local v0, s:Ljava/lang/StringBuilder;
    const-string v1, "Criteria[power="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    iget v2, p0, Landroid/location/Criteria;->mPowerRequirement:I

    #@d
    invoke-static {v2}, Landroid/location/Criteria;->powerToString(I)Ljava/lang/String;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    .line 402
    const-string v1, " acc="

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    iget v2, p0, Landroid/location/Criteria;->mHorizontalAccuracy:I

    #@1c
    invoke-static {v2}, Landroid/location/Criteria;->accuracyToString(I)Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    .line 403
    const/16 v1, 0x5d

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@28
    .line 404
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v1

    #@2c
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "parcel"
    .parameter "flags"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 357
    iget v0, p0, Landroid/location/Criteria;->mHorizontalAccuracy:I

    #@4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@7
    .line 358
    iget v0, p0, Landroid/location/Criteria;->mVerticalAccuracy:I

    #@9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 359
    iget v0, p0, Landroid/location/Criteria;->mSpeedAccuracy:I

    #@e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 360
    iget v0, p0, Landroid/location/Criteria;->mBearingAccuracy:I

    #@13
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 361
    iget v0, p0, Landroid/location/Criteria;->mPowerRequirement:I

    #@18
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1b
    .line 362
    iget-boolean v0, p0, Landroid/location/Criteria;->mAltitudeRequired:Z

    #@1d
    if-eqz v0, :cond_3b

    #@1f
    move v0, v1

    #@20
    :goto_20
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    .line 363
    iget-boolean v0, p0, Landroid/location/Criteria;->mBearingRequired:Z

    #@25
    if-eqz v0, :cond_3d

    #@27
    move v0, v1

    #@28
    :goto_28
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2b
    .line 364
    iget-boolean v0, p0, Landroid/location/Criteria;->mSpeedRequired:Z

    #@2d
    if-eqz v0, :cond_3f

    #@2f
    move v0, v1

    #@30
    :goto_30
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@33
    .line 365
    iget-boolean v0, p0, Landroid/location/Criteria;->mCostAllowed:Z

    #@35
    if-eqz v0, :cond_41

    #@37
    :goto_37
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@3a
    .line 366
    return-void

    #@3b
    :cond_3b
    move v0, v2

    #@3c
    .line 362
    goto :goto_20

    #@3d
    :cond_3d
    move v0, v2

    #@3e
    .line 363
    goto :goto_28

    #@3f
    :cond_3f
    move v0, v2

    #@40
    .line 364
    goto :goto_30

    #@41
    :cond_41
    move v1, v2

    #@42
    .line 365
    goto :goto_37
.end method
