.class public final Landroid/location/GpsStatus;
.super Ljava/lang/Object;
.source "GpsStatus.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/location/GpsStatus$NmeaListener;,
        Landroid/location/GpsStatus$Listener;,
        Landroid/location/GpsStatus$SatelliteIterator;
    }
.end annotation


# static fields
.field public static final GPS_EVENT_FIRST_FIX:I = 0x3

.field public static final GPS_EVENT_SATELLITE_STATUS:I = 0x4

.field public static final GPS_EVENT_STARTED:I = 0x1

.field public static final GPS_EVENT_STOPPED:I = 0x2

.field private static final NUM_SATELLITES:I = 0xff


# instance fields
.field private mSatelliteList:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<",
            "Landroid/location/GpsSatellite;",
            ">;"
        }
    .end annotation
.end field

.field private mSatellites:[Landroid/location/GpsSatellite;

.field private mTimeToFirstFix:I


# direct methods
.method constructor <init>()V
    .registers 5

    #@0
    .prologue
    .line 130
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 32
    const/16 v1, 0xff

    #@5
    new-array v1, v1, [Landroid/location/GpsSatellite;

    #@7
    iput-object v1, p0, Landroid/location/GpsStatus;->mSatellites:[Landroid/location/GpsSatellite;

    #@9
    .line 67
    new-instance v1, Landroid/location/GpsStatus$1;

    #@b
    invoke-direct {v1, p0}, Landroid/location/GpsStatus$1;-><init>(Landroid/location/GpsStatus;)V

    #@e
    iput-object v1, p0, Landroid/location/GpsStatus;->mSatelliteList:Ljava/lang/Iterable;

    #@10
    .line 131
    const/4 v0, 0x0

    #@11
    .local v0, i:I
    :goto_11
    iget-object v1, p0, Landroid/location/GpsStatus;->mSatellites:[Landroid/location/GpsSatellite;

    #@13
    array-length v1, v1

    #@14
    if-ge v0, v1, :cond_24

    #@16
    .line 132
    iget-object v1, p0, Landroid/location/GpsStatus;->mSatellites:[Landroid/location/GpsSatellite;

    #@18
    new-instance v2, Landroid/location/GpsSatellite;

    #@1a
    add-int/lit8 v3, v0, 0x1

    #@1c
    invoke-direct {v2, v3}, Landroid/location/GpsSatellite;-><init>(I)V

    #@1f
    aput-object v2, v1, v0

    #@21
    .line 131
    add-int/lit8 v0, v0, 0x1

    #@23
    goto :goto_11

    #@24
    .line 134
    :cond_24
    return-void
.end method

.method static synthetic access$000(Landroid/location/GpsStatus;)[Landroid/location/GpsSatellite;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 27
    iget-object v0, p0, Landroid/location/GpsStatus;->mSatellites:[Landroid/location/GpsSatellite;

    #@2
    return-object v0
.end method


# virtual methods
.method public getMaxSatellites()I
    .registers 2

    #@0
    .prologue
    .line 212
    const/16 v0, 0xff

    #@2
    return v0
.end method

.method public getSatellites()Ljava/lang/Iterable;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Landroid/location/GpsSatellite;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 202
    iget-object v0, p0, Landroid/location/GpsStatus;->mSatelliteList:Ljava/lang/Iterable;

    #@2
    return-object v0
.end method

.method public getTimeToFirstFix()I
    .registers 2

    #@0
    .prologue
    .line 192
    iget v0, p0, Landroid/location/GpsStatus;->mTimeToFirstFix:I

    #@2
    return v0
.end method

.method declared-synchronized setStatus(I[I[F[F[FIII)V
    .registers 15
    .parameter "svCount"
    .parameter "prns"
    .parameter "snrs"
    .parameter "elevations"
    .parameter "azimuths"
    .parameter "ephemerisMask"
    .parameter "almanacMask"
    .parameter "usedInFixMask"

    #@0
    .prologue
    .line 146
    monitor-enter p0

    #@1
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :goto_2
    :try_start_2
    iget-object v4, p0, Landroid/location/GpsStatus;->mSatellites:[Landroid/location/GpsSatellite;

    #@4
    array-length v4, v4

    #@5
    if-ge v0, v4, :cond_11

    #@7
    .line 147
    iget-object v4, p0, Landroid/location/GpsStatus;->mSatellites:[Landroid/location/GpsSatellite;

    #@9
    aget-object v4, v4, v0

    #@b
    const/4 v5, 0x0

    #@c
    iput-boolean v5, v4, Landroid/location/GpsSatellite;->mValid:Z

    #@e
    .line 146
    add-int/lit8 v0, v0, 0x1

    #@10
    goto :goto_2

    #@11
    .line 150
    :cond_11
    const/4 v0, 0x0

    #@12
    :goto_12
    if-ge v0, p1, :cond_53

    #@14
    .line 151
    aget v4, p2, v0

    #@16
    add-int/lit8 v1, v4, -0x1

    #@18
    .line 152
    .local v1, prn:I
    const/4 v4, 0x1

    #@19
    shl-int v2, v4, v1

    #@1b
    .line 153
    .local v2, prnShift:I
    if-ltz v1, :cond_4a

    #@1d
    iget-object v4, p0, Landroid/location/GpsStatus;->mSatellites:[Landroid/location/GpsSatellite;

    #@1f
    array-length v4, v4

    #@20
    if-ge v1, v4, :cond_4a

    #@22
    .line 154
    iget-object v4, p0, Landroid/location/GpsStatus;->mSatellites:[Landroid/location/GpsSatellite;

    #@24
    aget-object v3, v4, v1

    #@26
    .line 156
    .local v3, satellite:Landroid/location/GpsSatellite;
    const/4 v4, 0x1

    #@27
    iput-boolean v4, v3, Landroid/location/GpsSatellite;->mValid:Z

    #@29
    .line 157
    aget v4, p3, v0

    #@2b
    iput v4, v3, Landroid/location/GpsSatellite;->mSnr:F

    #@2d
    .line 158
    aget v4, p4, v0

    #@2f
    iput v4, v3, Landroid/location/GpsSatellite;->mElevation:F

    #@31
    .line 159
    aget v4, p5, v0

    #@33
    iput v4, v3, Landroid/location/GpsSatellite;->mAzimuth:F

    #@35
    .line 160
    and-int v4, p6, v2

    #@37
    if-eqz v4, :cond_4d

    #@39
    const/4 v4, 0x1

    #@3a
    :goto_3a
    iput-boolean v4, v3, Landroid/location/GpsSatellite;->mHasEphemeris:Z

    #@3c
    .line 161
    and-int v4, p7, v2

    #@3e
    if-eqz v4, :cond_4f

    #@40
    const/4 v4, 0x1

    #@41
    :goto_41
    iput-boolean v4, v3, Landroid/location/GpsSatellite;->mHasAlmanac:Z

    #@43
    .line 162
    and-int v4, p8, v2

    #@45
    if-eqz v4, :cond_51

    #@47
    const/4 v4, 0x1

    #@48
    :goto_48
    iput-boolean v4, v3, Landroid/location/GpsSatellite;->mUsedInFix:Z
    :try_end_4a
    .catchall {:try_start_2 .. :try_end_4a} :catchall_55

    #@4a
    .line 150
    .end local v3           #satellite:Landroid/location/GpsSatellite;
    :cond_4a
    add-int/lit8 v0, v0, 0x1

    #@4c
    goto :goto_12

    #@4d
    .line 160
    .restart local v3       #satellite:Landroid/location/GpsSatellite;
    :cond_4d
    const/4 v4, 0x0

    #@4e
    goto :goto_3a

    #@4f
    .line 161
    :cond_4f
    const/4 v4, 0x0

    #@50
    goto :goto_41

    #@51
    .line 162
    :cond_51
    const/4 v4, 0x0

    #@52
    goto :goto_48

    #@53
    .line 165
    .end local v1           #prn:I
    .end local v2           #prnShift:I
    .end local v3           #satellite:Landroid/location/GpsSatellite;
    :cond_53
    monitor-exit p0

    #@54
    return-void

    #@55
    .line 146
    :catchall_55
    move-exception v4

    #@56
    monitor-exit p0

    #@57
    throw v4
.end method

.method setStatus(Landroid/location/GpsStatus;)V
    .registers 5
    .parameter "status"

    #@0
    .prologue
    .line 174
    invoke-virtual {p1}, Landroid/location/GpsStatus;->getTimeToFirstFix()I

    #@3
    move-result v1

    #@4
    iput v1, p0, Landroid/location/GpsStatus;->mTimeToFirstFix:I

    #@6
    .line 176
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    iget-object v1, p0, Landroid/location/GpsStatus;->mSatellites:[Landroid/location/GpsSatellite;

    #@9
    array-length v1, v1

    #@a
    if-ge v0, v1, :cond_1a

    #@c
    .line 177
    iget-object v1, p0, Landroid/location/GpsStatus;->mSatellites:[Landroid/location/GpsSatellite;

    #@e
    aget-object v1, v1, v0

    #@10
    iget-object v2, p1, Landroid/location/GpsStatus;->mSatellites:[Landroid/location/GpsSatellite;

    #@12
    aget-object v2, v2, v0

    #@14
    invoke-virtual {v1, v2}, Landroid/location/GpsSatellite;->setStatus(Landroid/location/GpsSatellite;)V

    #@17
    .line 176
    add-int/lit8 v0, v0, 0x1

    #@19
    goto :goto_7

    #@1a
    .line 179
    :cond_1a
    return-void
.end method

.method setTimeToFirstFix(I)V
    .registers 2
    .parameter "ttff"

    #@0
    .prologue
    .line 182
    iput p1, p0, Landroid/location/GpsStatus;->mTimeToFirstFix:I

    #@2
    .line 183
    return-void
.end method
