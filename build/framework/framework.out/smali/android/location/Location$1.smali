.class final Landroid/location/Location$1;
.super Ljava/lang/Object;
.source "Location.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/location/Location;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Landroid/location/Location;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 856
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Landroid/location/Location;
    .registers 9
    .parameter "in"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 859
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    .line 860
    .local v1, provider:Ljava/lang/String;
    new-instance v0, Landroid/location/Location;

    #@8
    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    #@b
    .line 861
    .local v0, l:Landroid/location/Location;
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@e
    move-result-wide v5

    #@f
    invoke-static {v0, v5, v6}, Landroid/location/Location;->access$002(Landroid/location/Location;J)J

    #@12
    .line 862
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@15
    move-result-wide v5

    #@16
    invoke-static {v0, v5, v6}, Landroid/location/Location;->access$102(Landroid/location/Location;J)J

    #@19
    .line 863
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    #@1c
    move-result-wide v5

    #@1d
    invoke-static {v0, v5, v6}, Landroid/location/Location;->access$202(Landroid/location/Location;D)D

    #@20
    .line 864
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    #@23
    move-result-wide v5

    #@24
    invoke-static {v0, v5, v6}, Landroid/location/Location;->access$302(Landroid/location/Location;D)D

    #@27
    .line 865
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2a
    move-result v2

    #@2b
    if-eqz v2, :cond_72

    #@2d
    move v2, v3

    #@2e
    :goto_2e
    invoke-static {v0, v2}, Landroid/location/Location;->access$402(Landroid/location/Location;Z)Z

    #@31
    .line 866
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    #@34
    move-result-wide v5

    #@35
    invoke-static {v0, v5, v6}, Landroid/location/Location;->access$502(Landroid/location/Location;D)D

    #@38
    .line 867
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3b
    move-result v2

    #@3c
    if-eqz v2, :cond_74

    #@3e
    move v2, v3

    #@3f
    :goto_3f
    invoke-static {v0, v2}, Landroid/location/Location;->access$602(Landroid/location/Location;Z)Z

    #@42
    .line 868
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@45
    move-result v2

    #@46
    invoke-static {v0, v2}, Landroid/location/Location;->access$702(Landroid/location/Location;F)F

    #@49
    .line 869
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@4c
    move-result v2

    #@4d
    if-eqz v2, :cond_76

    #@4f
    move v2, v3

    #@50
    :goto_50
    invoke-static {v0, v2}, Landroid/location/Location;->access$802(Landroid/location/Location;Z)Z

    #@53
    .line 870
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@56
    move-result v2

    #@57
    invoke-static {v0, v2}, Landroid/location/Location;->access$902(Landroid/location/Location;F)F

    #@5a
    .line 871
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@5d
    move-result v2

    #@5e
    if-eqz v2, :cond_78

    #@60
    :goto_60
    invoke-static {v0, v3}, Landroid/location/Location;->access$1002(Landroid/location/Location;Z)Z

    #@63
    .line 872
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@66
    move-result v2

    #@67
    invoke-static {v0, v2}, Landroid/location/Location;->access$1102(Landroid/location/Location;F)F

    #@6a
    .line 873
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    #@6d
    move-result-object v2

    #@6e
    invoke-static {v0, v2}, Landroid/location/Location;->access$1202(Landroid/location/Location;Landroid/os/Bundle;)Landroid/os/Bundle;

    #@71
    .line 874
    return-object v0

    #@72
    :cond_72
    move v2, v4

    #@73
    .line 865
    goto :goto_2e

    #@74
    :cond_74
    move v2, v4

    #@75
    .line 867
    goto :goto_3f

    #@76
    :cond_76
    move v2, v4

    #@77
    .line 869
    goto :goto_50

    #@78
    :cond_78
    move v3, v4

    #@79
    .line 871
    goto :goto_60
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 856
    invoke-virtual {p0, p1}, Landroid/location/Location$1;->createFromParcel(Landroid/os/Parcel;)Landroid/location/Location;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Landroid/location/Location;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 879
    new-array v0, p1, [Landroid/location/Location;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 856
    invoke-virtual {p0, p1}, Landroid/location/Location$1;->newArray(I)[Landroid/location/Location;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
