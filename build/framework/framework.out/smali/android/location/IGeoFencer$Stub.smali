.class public abstract Landroid/location/IGeoFencer$Stub;
.super Landroid/os/Binder;
.source "IGeoFencer.java"

# interfaces
.implements Landroid/location/IGeoFencer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/location/IGeoFencer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/location/IGeoFencer$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.location.IGeoFencer"

.field static final TRANSACTION_clearGeoFence:I = 0x2

.field static final TRANSACTION_clearGeoFenceUser:I = 0x3

.field static final TRANSACTION_setGeoFence:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 18
    const-string v0, "android.location.IGeoFencer"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/location/IGeoFencer$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 19
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/location/IGeoFencer;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 26
    if-nez p0, :cond_4

    #@2
    .line 27
    const/4 v0, 0x0

    #@3
    .line 33
    :goto_3
    return-object v0

    #@4
    .line 29
    :cond_4
    const-string v1, "android.location.IGeoFencer"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 30
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/location/IGeoFencer;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 31
    check-cast v0, Landroid/location/IGeoFencer;

    #@12
    goto :goto_3

    #@13
    .line 33
    :cond_13
    new-instance v0, Landroid/location/IGeoFencer$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/location/IGeoFencer$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 10
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 41
    sparse-switch p1, :sswitch_data_68

    #@4
    .line 91
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v4

    #@8
    :goto_8
    return v4

    #@9
    .line 45
    :sswitch_9
    const-string v3, "android.location.IGeoFencer"

    #@b
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 50
    :sswitch_f
    const-string v3, "android.location.IGeoFencer"

    #@11
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@17
    move-result-object v0

    #@18
    .line 54
    .local v0, _arg0:Landroid/os/IBinder;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_34

    #@1e
    .line 55
    sget-object v3, Landroid/location/GeoFenceParams;->CREATOR:Landroid/os/Parcelable$Creator;

    #@20
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@23
    move-result-object v1

    #@24
    check-cast v1, Landroid/location/GeoFenceParams;

    #@26
    .line 60
    .local v1, _arg1:Landroid/location/GeoFenceParams;
    :goto_26
    invoke-virtual {p0, v0, v1}, Landroid/location/IGeoFencer$Stub;->setGeoFence(Landroid/os/IBinder;Landroid/location/GeoFenceParams;)Z

    #@29
    move-result v2

    #@2a
    .line 61
    .local v2, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2d
    .line 62
    if-eqz v2, :cond_36

    #@2f
    move v3, v4

    #@30
    :goto_30
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@33
    goto :goto_8

    #@34
    .line 58
    .end local v1           #_arg1:Landroid/location/GeoFenceParams;
    .end local v2           #_result:Z
    :cond_34
    const/4 v1, 0x0

    #@35
    .restart local v1       #_arg1:Landroid/location/GeoFenceParams;
    goto :goto_26

    #@36
    .line 62
    .restart local v2       #_result:Z
    :cond_36
    const/4 v3, 0x0

    #@37
    goto :goto_30

    #@38
    .line 67
    .end local v0           #_arg0:Landroid/os/IBinder;
    .end local v1           #_arg1:Landroid/location/GeoFenceParams;
    .end local v2           #_result:Z
    :sswitch_38
    const-string v3, "android.location.IGeoFencer"

    #@3a
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3d
    .line 69
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@40
    move-result-object v0

    #@41
    .line 71
    .restart local v0       #_arg0:Landroid/os/IBinder;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@44
    move-result v3

    #@45
    if-eqz v3, :cond_56

    #@47
    .line 72
    sget-object v3, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@49
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@4c
    move-result-object v1

    #@4d
    check-cast v1, Landroid/app/PendingIntent;

    #@4f
    .line 77
    .local v1, _arg1:Landroid/app/PendingIntent;
    :goto_4f
    invoke-virtual {p0, v0, v1}, Landroid/location/IGeoFencer$Stub;->clearGeoFence(Landroid/os/IBinder;Landroid/app/PendingIntent;)V

    #@52
    .line 78
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@55
    goto :goto_8

    #@56
    .line 75
    .end local v1           #_arg1:Landroid/app/PendingIntent;
    :cond_56
    const/4 v1, 0x0

    #@57
    .restart local v1       #_arg1:Landroid/app/PendingIntent;
    goto :goto_4f

    #@58
    .line 83
    .end local v0           #_arg0:Landroid/os/IBinder;
    .end local v1           #_arg1:Landroid/app/PendingIntent;
    :sswitch_58
    const-string v3, "android.location.IGeoFencer"

    #@5a
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5d
    .line 85
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@60
    move-result v0

    #@61
    .line 86
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Landroid/location/IGeoFencer$Stub;->clearGeoFenceUser(I)V

    #@64
    .line 87
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@67
    goto :goto_8

    #@68
    .line 41
    :sswitch_data_68
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_38
        0x3 -> :sswitch_58
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
