.class public final Landroid/location/LocationRequest;
.super Ljava/lang/Object;
.source "LocationRequest.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final ACCURACY_BLOCK:I = 0x66

.field public static final ACCURACY_CITY:I = 0x68

.field public static final ACCURACY_FINE:I = 0x64

.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/location/LocationRequest;",
            ">;"
        }
    .end annotation
.end field

.field private static final FASTEST_INTERVAL_FACTOR:D = 6.0

.field public static final POWER_HIGH:I = 0xcb

.field public static final POWER_LOW:I = 0xc9

.field public static final POWER_NONE:I = 0xc8


# instance fields
.field private mExpireAt:J

.field private mExplicitFastestInterval:Z

.field private mFastestInterval:J

.field private mInterval:J

.field private mNumUpdates:I

.field private mProvider:Ljava/lang/String;

.field private mQuality:I

.field private mSmallestDisplacement:F


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 528
    new-instance v0, Landroid/location/LocationRequest$1;

    #@2
    invoke-direct {v0}, Landroid/location/LocationRequest$1;-><init>()V

    #@5
    sput-object v0, Landroid/location/LocationRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 5

    #@0
    .prologue
    .line 224
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 141
    const/16 v0, 0xc9

    #@5
    iput v0, p0, Landroid/location/LocationRequest;->mQuality:I

    #@7
    .line 142
    const-wide/32 v0, 0x36ee80

    #@a
    iput-wide v0, p0, Landroid/location/LocationRequest;->mInterval:J

    #@c
    .line 143
    iget-wide v0, p0, Landroid/location/LocationRequest;->mInterval:J

    #@e
    long-to-double v0, v0

    #@f
    const-wide/high16 v2, 0x4018

    #@11
    div-double/2addr v0, v2

    #@12
    double-to-long v0, v0

    #@13
    iput-wide v0, p0, Landroid/location/LocationRequest;->mFastestInterval:J

    #@15
    .line 144
    const/4 v0, 0x0

    #@16
    iput-boolean v0, p0, Landroid/location/LocationRequest;->mExplicitFastestInterval:Z

    #@18
    .line 145
    const-wide v0, 0x7fffffffffffffffL

    #@1d
    iput-wide v0, p0, Landroid/location/LocationRequest;->mExpireAt:J

    #@1f
    .line 146
    const v0, 0x7fffffff

    #@22
    iput v0, p0, Landroid/location/LocationRequest;->mNumUpdates:I

    #@24
    .line 147
    const/4 v0, 0x0

    #@25
    iput v0, p0, Landroid/location/LocationRequest;->mSmallestDisplacement:F

    #@27
    .line 149
    const-string v0, "fused"

    #@29
    iput-object v0, p0, Landroid/location/LocationRequest;->mProvider:Ljava/lang/String;

    #@2b
    .line 224
    return-void
.end method

.method public constructor <init>(Landroid/location/LocationRequest;)V
    .registers 6
    .parameter "src"

    #@0
    .prologue
    .line 227
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 141
    const/16 v0, 0xc9

    #@5
    iput v0, p0, Landroid/location/LocationRequest;->mQuality:I

    #@7
    .line 142
    const-wide/32 v0, 0x36ee80

    #@a
    iput-wide v0, p0, Landroid/location/LocationRequest;->mInterval:J

    #@c
    .line 143
    iget-wide v0, p0, Landroid/location/LocationRequest;->mInterval:J

    #@e
    long-to-double v0, v0

    #@f
    const-wide/high16 v2, 0x4018

    #@11
    div-double/2addr v0, v2

    #@12
    double-to-long v0, v0

    #@13
    iput-wide v0, p0, Landroid/location/LocationRequest;->mFastestInterval:J

    #@15
    .line 144
    const/4 v0, 0x0

    #@16
    iput-boolean v0, p0, Landroid/location/LocationRequest;->mExplicitFastestInterval:Z

    #@18
    .line 145
    const-wide v0, 0x7fffffffffffffffL

    #@1d
    iput-wide v0, p0, Landroid/location/LocationRequest;->mExpireAt:J

    #@1f
    .line 146
    const v0, 0x7fffffff

    #@22
    iput v0, p0, Landroid/location/LocationRequest;->mNumUpdates:I

    #@24
    .line 147
    const/4 v0, 0x0

    #@25
    iput v0, p0, Landroid/location/LocationRequest;->mSmallestDisplacement:F

    #@27
    .line 149
    const-string v0, "fused"

    #@29
    iput-object v0, p0, Landroid/location/LocationRequest;->mProvider:Ljava/lang/String;

    #@2b
    .line 228
    iget v0, p1, Landroid/location/LocationRequest;->mQuality:I

    #@2d
    iput v0, p0, Landroid/location/LocationRequest;->mQuality:I

    #@2f
    .line 229
    iget-wide v0, p1, Landroid/location/LocationRequest;->mInterval:J

    #@31
    iput-wide v0, p0, Landroid/location/LocationRequest;->mInterval:J

    #@33
    .line 230
    iget-wide v0, p1, Landroid/location/LocationRequest;->mFastestInterval:J

    #@35
    iput-wide v0, p0, Landroid/location/LocationRequest;->mFastestInterval:J

    #@37
    .line 231
    iget-boolean v0, p1, Landroid/location/LocationRequest;->mExplicitFastestInterval:Z

    #@39
    iput-boolean v0, p0, Landroid/location/LocationRequest;->mExplicitFastestInterval:Z

    #@3b
    .line 232
    iget-wide v0, p1, Landroid/location/LocationRequest;->mExpireAt:J

    #@3d
    iput-wide v0, p0, Landroid/location/LocationRequest;->mExpireAt:J

    #@3f
    .line 233
    iget v0, p1, Landroid/location/LocationRequest;->mNumUpdates:I

    #@41
    iput v0, p0, Landroid/location/LocationRequest;->mNumUpdates:I

    #@43
    .line 234
    iget v0, p1, Landroid/location/LocationRequest;->mSmallestDisplacement:F

    #@45
    iput v0, p0, Landroid/location/LocationRequest;->mSmallestDisplacement:F

    #@47
    .line 235
    iget-object v0, p1, Landroid/location/LocationRequest;->mProvider:Ljava/lang/String;

    #@49
    iput-object v0, p0, Landroid/location/LocationRequest;->mProvider:Ljava/lang/String;

    #@4b
    .line 236
    return-void
.end method

.method private static checkDisplacement(F)V
    .registers 4
    .parameter "meters"

    #@0
    .prologue
    .line 517
    const/4 v0, 0x0

    #@1
    cmpg-float v0, p0, v0

    #@3
    if-gez v0, :cond_1e

    #@5
    .line 518
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "invalid displacement: "

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v0

    #@1e
    .line 520
    :cond_1e
    return-void
.end method

.method private static checkInterval(J)V
    .registers 5
    .parameter "millis"

    #@0
    .prologue
    .line 497
    const-wide/16 v0, 0x0

    #@2
    cmp-long v0, p0, v0

    #@4
    if-gez v0, :cond_1f

    #@6
    .line 498
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "invalid interval: "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v0

    #@1f
    .line 500
    :cond_1f
    return-void
.end method

.method private static checkProvider(Ljava/lang/String;)V
    .registers 4
    .parameter "name"

    #@0
    .prologue
    .line 523
    if-nez p0, :cond_1b

    #@2
    .line 524
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v2, "invalid provider: "

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v0

    #@1b
    .line 526
    :cond_1b
    return-void
.end method

.method private static checkQuality(I)V
    .registers 4
    .parameter "quality"

    #@0
    .prologue
    .line 503
    sparse-switch p0, :sswitch_data_1e

    #@3
    .line 512
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "invalid quality: "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 514
    :sswitch_1c
    return-void

    #@1d
    .line 503
    nop

    #@1e
    :sswitch_data_1e
    .sparse-switch
        0x64 -> :sswitch_1c
        0x66 -> :sswitch_1c
        0x68 -> :sswitch_1c
        0xc8 -> :sswitch_1c
        0xc9 -> :sswitch_1c
        0xcb -> :sswitch_1c
    .end sparse-switch
.end method

.method public static create()Landroid/location/LocationRequest;
    .registers 1

    #@0
    .prologue
    .line 161
    new-instance v0, Landroid/location/LocationRequest;

    #@2
    invoke-direct {v0}, Landroid/location/LocationRequest;-><init>()V

    #@5
    .line 162
    .local v0, request:Landroid/location/LocationRequest;
    return-object v0
.end method

.method public static createFromDeprecatedCriteria(Landroid/location/Criteria;JFZ)Landroid/location/LocationRequest;
    .registers 9
    .parameter "criteria"
    .parameter "minTime"
    .parameter "minDistance"
    .parameter "singleShot"

    #@0
    .prologue
    .line 193
    const-wide/16 v2, 0x0

    #@2
    cmp-long v2, p1, v2

    #@4
    if-gez v2, :cond_8

    #@6
    const-wide/16 p1, 0x0

    #@8
    .line 194
    :cond_8
    const/4 v2, 0x0

    #@9
    cmpg-float v2, p3, v2

    #@b
    if-gez v2, :cond_e

    #@d
    const/4 p3, 0x0

    #@e
    .line 197
    :cond_e
    invoke-virtual {p0}, Landroid/location/Criteria;->getAccuracy()I

    #@11
    move-result v2

    #@12
    packed-switch v2, :pswitch_data_44

    #@15
    .line 205
    invoke-virtual {p0}, Landroid/location/Criteria;->getPowerRequirement()I

    #@18
    move-result v2

    #@19
    packed-switch v2, :pswitch_data_4c

    #@1c
    .line 209
    :goto_1c
    const/16 v0, 0xc9

    #@1e
    .line 214
    .local v0, quality:I
    :goto_1e
    new-instance v2, Landroid/location/LocationRequest;

    #@20
    invoke-direct {v2}, Landroid/location/LocationRequest;-><init>()V

    #@23
    invoke-virtual {v2, v0}, Landroid/location/LocationRequest;->setQuality(I)Landroid/location/LocationRequest;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2, p1, p2}, Landroid/location/LocationRequest;->setInterval(J)Landroid/location/LocationRequest;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2, p1, p2}, Landroid/location/LocationRequest;->setFastestInterval(J)Landroid/location/LocationRequest;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2, p3}, Landroid/location/LocationRequest;->setSmallestDisplacement(F)Landroid/location/LocationRequest;

    #@32
    move-result-object v1

    #@33
    .line 219
    .local v1, request:Landroid/location/LocationRequest;
    if-eqz p4, :cond_39

    #@35
    const/4 v2, 0x1

    #@36
    invoke-virtual {v1, v2}, Landroid/location/LocationRequest;->setNumUpdates(I)Landroid/location/LocationRequest;

    #@39
    .line 220
    :cond_39
    return-object v1

    #@3a
    .line 199
    .end local v0           #quality:I
    .end local v1           #request:Landroid/location/LocationRequest;
    :pswitch_3a
    const/16 v0, 0x66

    #@3c
    .line 200
    .restart local v0       #quality:I
    goto :goto_1e

    #@3d
    .line 202
    .end local v0           #quality:I
    :pswitch_3d
    const/16 v0, 0x64

    #@3f
    .line 203
    .restart local v0       #quality:I
    goto :goto_1e

    #@40
    .line 207
    .end local v0           #quality:I
    :pswitch_40
    const/16 v0, 0xcb

    #@42
    .restart local v0       #quality:I
    goto :goto_1c

    #@43
    .line 197
    nop

    #@44
    :pswitch_data_44
    .packed-switch 0x1
        :pswitch_3d
        :pswitch_3a
    .end packed-switch

    #@4c
    .line 205
    :pswitch_data_4c
    .packed-switch 0x3
        :pswitch_40
    .end packed-switch
.end method

.method public static createFromDeprecatedProvider(Ljava/lang/String;JFZ)Landroid/location/LocationRequest;
    .registers 9
    .parameter "provider"
    .parameter "minTime"
    .parameter "minDistance"
    .parameter "singleShot"

    #@0
    .prologue
    .line 168
    const-wide/16 v2, 0x0

    #@2
    cmp-long v2, p1, v2

    #@4
    if-gez v2, :cond_8

    #@6
    const-wide/16 p1, 0x0

    #@8
    .line 169
    :cond_8
    const/4 v2, 0x0

    #@9
    cmpg-float v2, p3, v2

    #@b
    if-gez v2, :cond_e

    #@d
    const/4 p3, 0x0

    #@e
    .line 172
    :cond_e
    const-string/jumbo v2, "passive"

    #@11
    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_39

    #@17
    .line 173
    const/16 v0, 0xc8

    #@19
    .line 180
    .local v0, quality:I
    :goto_19
    new-instance v2, Landroid/location/LocationRequest;

    #@1b
    invoke-direct {v2}, Landroid/location/LocationRequest;-><init>()V

    #@1e
    invoke-virtual {v2, p0}, Landroid/location/LocationRequest;->setProvider(Ljava/lang/String;)Landroid/location/LocationRequest;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v2, v0}, Landroid/location/LocationRequest;->setQuality(I)Landroid/location/LocationRequest;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2, p1, p2}, Landroid/location/LocationRequest;->setInterval(J)Landroid/location/LocationRequest;

    #@29
    move-result-object v2

    #@2a
    invoke-virtual {v2, p1, p2}, Landroid/location/LocationRequest;->setFastestInterval(J)Landroid/location/LocationRequest;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v2, p3}, Landroid/location/LocationRequest;->setSmallestDisplacement(F)Landroid/location/LocationRequest;

    #@31
    move-result-object v1

    #@32
    .line 186
    .local v1, request:Landroid/location/LocationRequest;
    if-eqz p4, :cond_38

    #@34
    const/4 v2, 0x1

    #@35
    invoke-virtual {v1, v2}, Landroid/location/LocationRequest;->setNumUpdates(I)Landroid/location/LocationRequest;

    #@38
    .line 187
    :cond_38
    return-object v1

    #@39
    .line 174
    .end local v0           #quality:I
    .end local v1           #request:Landroid/location/LocationRequest;
    :cond_39
    const-string v2, "gps"

    #@3b
    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3e
    move-result v2

    #@3f
    if-eqz v2, :cond_44

    #@41
    .line 175
    const/16 v0, 0x64

    #@43
    .restart local v0       #quality:I
    goto :goto_19

    #@44
    .line 177
    .end local v0           #quality:I
    :cond_44
    const/16 v0, 0xc9

    #@46
    .restart local v0       #quality:I
    goto :goto_19
.end method

.method public static qualityToString(I)Ljava/lang/String;
    .registers 2
    .parameter "quality"

    #@0
    .prologue
    .line 567
    sparse-switch p0, :sswitch_data_18

    #@3
    .line 581
    const-string v0, "???"

    #@5
    :goto_5
    return-object v0

    #@6
    .line 569
    :sswitch_6
    const-string v0, "ACCURACY_FINE"

    #@8
    goto :goto_5

    #@9
    .line 571
    :sswitch_9
    const-string v0, "ACCURACY_BLOCK"

    #@b
    goto :goto_5

    #@c
    .line 573
    :sswitch_c
    const-string v0, "ACCURACY_CITY"

    #@e
    goto :goto_5

    #@f
    .line 575
    :sswitch_f
    const-string v0, "POWER_NONE"

    #@11
    goto :goto_5

    #@12
    .line 577
    :sswitch_12
    const-string v0, "POWER_LOW"

    #@14
    goto :goto_5

    #@15
    .line 579
    :sswitch_15
    const-string v0, "POWER_HIGH"

    #@17
    goto :goto_5

    #@18
    .line 567
    :sswitch_data_18
    .sparse-switch
        0x64 -> :sswitch_6
        0x66 -> :sswitch_9
        0x68 -> :sswitch_c
        0xc8 -> :sswitch_f
        0xc9 -> :sswitch_12
        0xcb -> :sswitch_15
    .end sparse-switch
.end method


# virtual methods
.method public decrementNumUpdates()V
    .registers 3

    #@0
    .prologue
    .line 463
    iget v0, p0, Landroid/location/LocationRequest;->mNumUpdates:I

    #@2
    const v1, 0x7fffffff

    #@5
    if-eq v0, v1, :cond_d

    #@7
    .line 464
    iget v0, p0, Landroid/location/LocationRequest;->mNumUpdates:I

    #@9
    add-int/lit8 v0, v0, -0x1

    #@b
    iput v0, p0, Landroid/location/LocationRequest;->mNumUpdates:I

    #@d
    .line 466
    :cond_d
    iget v0, p0, Landroid/location/LocationRequest;->mNumUpdates:I

    #@f
    if-gez v0, :cond_14

    #@11
    .line 467
    const/4 v0, 0x0

    #@12
    iput v0, p0, Landroid/location/LocationRequest;->mNumUpdates:I

    #@14
    .line 469
    :cond_14
    return-void
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 551
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getExpireAt()J
    .registers 3

    #@0
    .prologue
    .line 428
    iget-wide v0, p0, Landroid/location/LocationRequest;->mExpireAt:J

    #@2
    return-wide v0
.end method

.method public getFastestInterval()J
    .registers 3

    #@0
    .prologue
    .line 366
    iget-wide v0, p0, Landroid/location/LocationRequest;->mFastestInterval:J

    #@2
    return-wide v0
.end method

.method public getInterval()J
    .registers 3

    #@0
    .prologue
    .line 317
    iget-wide v0, p0, Landroid/location/LocationRequest;->mInterval:J

    #@2
    return-wide v0
.end method

.method public getNumUpdates()I
    .registers 2

    #@0
    .prologue
    .line 458
    iget v0, p0, Landroid/location/LocationRequest;->mNumUpdates:I

    #@2
    return v0
.end method

.method public getProvider()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 481
    iget-object v0, p0, Landroid/location/LocationRequest;->mProvider:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getQuality()I
    .registers 2

    #@0
    .prologue
    .line 271
    iget v0, p0, Landroid/location/LocationRequest;->mQuality:I

    #@2
    return v0
.end method

.method public getSmallestDisplacement()F
    .registers 2

    #@0
    .prologue
    .line 493
    iget v0, p0, Landroid/location/LocationRequest;->mSmallestDisplacement:F

    #@2
    return v0
.end method

.method public setExpireAt(J)Landroid/location/LocationRequest;
    .registers 7
    .parameter "millis"

    #@0
    .prologue
    const-wide/16 v2, 0x0

    #@2
    .line 414
    iput-wide p1, p0, Landroid/location/LocationRequest;->mExpireAt:J

    #@4
    .line 415
    iget-wide v0, p0, Landroid/location/LocationRequest;->mExpireAt:J

    #@6
    cmp-long v0, v0, v2

    #@8
    if-gez v0, :cond_c

    #@a
    iput-wide v2, p0, Landroid/location/LocationRequest;->mExpireAt:J

    #@c
    .line 416
    :cond_c
    return-object p0
.end method

.method public setExpireIn(J)Landroid/location/LocationRequest;
    .registers 11
    .parameter "millis"

    #@0
    .prologue
    const-wide v6, 0x7fffffffffffffffL

    #@5
    const-wide/16 v4, 0x0

    #@7
    .line 386
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@a
    move-result-wide v0

    #@b
    .line 389
    .local v0, elapsedRealtime:J
    sub-long v2, v6, v0

    #@d
    cmp-long v2, p1, v2

    #@f
    if-lez v2, :cond_1c

    #@11
    .line 390
    iput-wide v6, p0, Landroid/location/LocationRequest;->mExpireAt:J

    #@13
    .line 395
    :goto_13
    iget-wide v2, p0, Landroid/location/LocationRequest;->mExpireAt:J

    #@15
    cmp-long v2, v2, v4

    #@17
    if-gez v2, :cond_1b

    #@19
    iput-wide v4, p0, Landroid/location/LocationRequest;->mExpireAt:J

    #@1b
    .line 396
    :cond_1b
    return-object p0

    #@1c
    .line 392
    :cond_1c
    add-long v2, p1, v0

    #@1e
    iput-wide v2, p0, Landroid/location/LocationRequest;->mExpireAt:J

    #@20
    goto :goto_13
.end method

.method public setFastestInterval(J)Landroid/location/LocationRequest;
    .registers 4
    .parameter "millis"

    #@0
    .prologue
    .line 350
    invoke-static {p1, p2}, Landroid/location/LocationRequest;->checkInterval(J)V

    #@3
    .line 351
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/location/LocationRequest;->mExplicitFastestInterval:Z

    #@6
    .line 352
    iput-wide p1, p0, Landroid/location/LocationRequest;->mFastestInterval:J

    #@8
    .line 353
    return-object p0
.end method

.method public setInterval(J)Landroid/location/LocationRequest;
    .registers 7
    .parameter "millis"

    #@0
    .prologue
    .line 303
    invoke-static {p1, p2}, Landroid/location/LocationRequest;->checkInterval(J)V

    #@3
    .line 304
    iput-wide p1, p0, Landroid/location/LocationRequest;->mInterval:J

    #@5
    .line 305
    iget-boolean v0, p0, Landroid/location/LocationRequest;->mExplicitFastestInterval:Z

    #@7
    if-nez v0, :cond_12

    #@9
    .line 306
    iget-wide v0, p0, Landroid/location/LocationRequest;->mInterval:J

    #@b
    long-to-double v0, v0

    #@c
    const-wide/high16 v2, 0x4018

    #@e
    div-double/2addr v0, v2

    #@f
    double-to-long v0, v0

    #@10
    iput-wide v0, p0, Landroid/location/LocationRequest;->mFastestInterval:J

    #@12
    .line 308
    :cond_12
    return-object p0
.end method

.method public setNumUpdates(I)Landroid/location/LocationRequest;
    .registers 5
    .parameter "numUpdates"

    #@0
    .prologue
    .line 445
    if-gtz p1, :cond_1b

    #@2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v2, "invalid numUpdates: "

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v0

    #@1b
    .line 446
    :cond_1b
    iput p1, p0, Landroid/location/LocationRequest;->mNumUpdates:I

    #@1d
    .line 447
    return-object p0
.end method

.method public setProvider(Ljava/lang/String;)Landroid/location/LocationRequest;
    .registers 2
    .parameter "provider"

    #@0
    .prologue
    .line 474
    invoke-static {p1}, Landroid/location/LocationRequest;->checkProvider(Ljava/lang/String;)V

    #@3
    .line 475
    iput-object p1, p0, Landroid/location/LocationRequest;->mProvider:Ljava/lang/String;

    #@5
    .line 476
    return-object p0
.end method

.method public setQuality(I)Landroid/location/LocationRequest;
    .registers 2
    .parameter "quality"

    #@0
    .prologue
    .line 260
    invoke-static {p1}, Landroid/location/LocationRequest;->checkQuality(I)V

    #@3
    .line 261
    iput p1, p0, Landroid/location/LocationRequest;->mQuality:I

    #@5
    .line 262
    return-object p0
.end method

.method public setSmallestDisplacement(F)Landroid/location/LocationRequest;
    .registers 2
    .parameter "meters"

    #@0
    .prologue
    .line 486
    invoke-static {p1}, Landroid/location/LocationRequest;->checkDisplacement(F)V

    #@3
    .line 487
    iput p1, p0, Landroid/location/LocationRequest;->mSmallestDisplacement:F

    #@5
    .line 488
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .registers 8

    #@0
    .prologue
    .line 587
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 588
    .local v2, s:Ljava/lang/StringBuilder;
    const-string v3, "Request["

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v3

    #@b
    iget v4, p0, Landroid/location/LocationRequest;->mQuality:I

    #@d
    invoke-static {v4}, Landroid/location/LocationRequest;->qualityToString(I)Ljava/lang/String;

    #@10
    move-result-object v4

    #@11
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    .line 589
    iget-object v3, p0, Landroid/location/LocationRequest;->mProvider:Ljava/lang/String;

    #@16
    if-eqz v3, :cond_23

    #@18
    const/16 v3, 0x20

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    iget-object v4, p0, Landroid/location/LocationRequest;->mProvider:Ljava/lang/String;

    #@20
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    .line 590
    :cond_23
    iget v3, p0, Landroid/location/LocationRequest;->mQuality:I

    #@25
    const/16 v4, 0xc8

    #@27
    if-eq v3, v4, :cond_33

    #@29
    .line 591
    const-string v3, " requested="

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    .line 592
    iget-wide v3, p0, Landroid/location/LocationRequest;->mInterval:J

    #@30
    invoke-static {v3, v4, v2}, Landroid/util/TimeUtils;->formatDuration(JLjava/lang/StringBuilder;)V

    #@33
    .line 594
    :cond_33
    const-string v3, " fastest="

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    .line 595
    iget-wide v3, p0, Landroid/location/LocationRequest;->mFastestInterval:J

    #@3a
    invoke-static {v3, v4, v2}, Landroid/util/TimeUtils;->formatDuration(JLjava/lang/StringBuilder;)V

    #@3d
    .line 596
    iget-wide v3, p0, Landroid/location/LocationRequest;->mExpireAt:J

    #@3f
    const-wide v5, 0x7fffffffffffffffL

    #@44
    cmp-long v3, v3, v5

    #@46
    if-eqz v3, :cond_58

    #@48
    .line 597
    iget-wide v3, p0, Landroid/location/LocationRequest;->mExpireAt:J

    #@4a
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@4d
    move-result-wide v5

    #@4e
    sub-long v0, v3, v5

    #@50
    .line 598
    .local v0, expireIn:J
    const-string v3, " expireIn="

    #@52
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    .line 599
    invoke-static {v0, v1, v2}, Landroid/util/TimeUtils;->formatDuration(JLjava/lang/StringBuilder;)V

    #@58
    .line 601
    .end local v0           #expireIn:J
    :cond_58
    iget v3, p0, Landroid/location/LocationRequest;->mNumUpdates:I

    #@5a
    const v4, 0x7fffffff

    #@5d
    if-eq v3, v4, :cond_6a

    #@5f
    .line 602
    const-string v3, " num="

    #@61
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v3

    #@65
    iget v4, p0, Landroid/location/LocationRequest;->mNumUpdates:I

    #@67
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6a
    .line 604
    :cond_6a
    const/16 v3, 0x5d

    #@6c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@6f
    .line 605
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v3

    #@73
    return-object v3
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "parcel"
    .parameter "flags"

    #@0
    .prologue
    .line 556
    iget v0, p0, Landroid/location/LocationRequest;->mQuality:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 557
    iget-wide v0, p0, Landroid/location/LocationRequest;->mFastestInterval:J

    #@7
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@a
    .line 558
    iget-wide v0, p0, Landroid/location/LocationRequest;->mInterval:J

    #@c
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@f
    .line 559
    iget-wide v0, p0, Landroid/location/LocationRequest;->mExpireAt:J

    #@11
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@14
    .line 560
    iget v0, p0, Landroid/location/LocationRequest;->mNumUpdates:I

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 561
    iget v0, p0, Landroid/location/LocationRequest;->mSmallestDisplacement:F

    #@1b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@1e
    .line 562
    iget-object v0, p0, Landroid/location/LocationRequest;->mProvider:Ljava/lang/String;

    #@20
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@23
    .line 563
    return-void
.end method
