.class public Landroid/location/LocationProvider;
.super Ljava/lang/Object;
.source "LocationProvider.java"


# static fields
.field public static final AVAILABLE:I = 0x2

.field public static final BAD_CHARS_REGEX:Ljava/lang/String; = "[^a-zA-Z0-9]"

.field public static final OUT_OF_SERVICE:I = 0x0

.field public static final TEMPORARILY_UNAVAILABLE:I = 0x1


# instance fields
.field private final mName:Ljava/lang/String;

.field private final mProperties:Lcom/android/internal/location/ProviderProperties;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/android/internal/location/ProviderProperties;)V
    .registers 6
    .parameter "name"
    .parameter "properties"

    #@0
    .prologue
    .line 59
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 60
    const-string v0, "[^a-zA-Z0-9]"

    #@5
    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_25

    #@b
    .line 61
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@d
    new-instance v1, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string/jumbo v2, "provider name contains illegal character: "

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@24
    throw v0

    #@25
    .line 63
    :cond_25
    iput-object p1, p0, Landroid/location/LocationProvider;->mName:Ljava/lang/String;

    #@27
    .line 64
    iput-object p2, p0, Landroid/location/LocationProvider;->mProperties:Lcom/android/internal/location/ProviderProperties;

    #@29
    .line 65
    return-void
.end method

.method public static propertiesMeetCriteria(Ljava/lang/String;Lcom/android/internal/location/ProviderProperties;Landroid/location/Criteria;)Z
    .registers 6
    .parameter "name"
    .parameter "properties"
    .parameter "criteria"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 87
    const-string/jumbo v1, "passive"

    #@4
    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_b

    #@a
    .line 117
    :cond_a
    :goto_a
    return v0

    #@b
    .line 91
    :cond_b
    if-eqz p1, :cond_a

    #@d
    .line 97
    invoke-virtual {p2}, Landroid/location/Criteria;->getAccuracy()I

    #@10
    move-result v1

    #@11
    if-eqz v1, :cond_1b

    #@13
    invoke-virtual {p2}, Landroid/location/Criteria;->getAccuracy()I

    #@16
    move-result v1

    #@17
    iget v2, p1, Lcom/android/internal/location/ProviderProperties;->mAccuracy:I

    #@19
    if-lt v1, v2, :cond_a

    #@1b
    .line 101
    :cond_1b
    invoke-virtual {p2}, Landroid/location/Criteria;->getPowerRequirement()I

    #@1e
    move-result v1

    #@1f
    if-eqz v1, :cond_29

    #@21
    invoke-virtual {p2}, Landroid/location/Criteria;->getPowerRequirement()I

    #@24
    move-result v1

    #@25
    iget v2, p1, Lcom/android/internal/location/ProviderProperties;->mPowerRequirement:I

    #@27
    if-lt v1, v2, :cond_a

    #@29
    .line 105
    :cond_29
    invoke-virtual {p2}, Landroid/location/Criteria;->isAltitudeRequired()Z

    #@2c
    move-result v1

    #@2d
    if-eqz v1, :cond_33

    #@2f
    iget-boolean v1, p1, Lcom/android/internal/location/ProviderProperties;->mSupportsAltitude:Z

    #@31
    if-eqz v1, :cond_a

    #@33
    .line 108
    :cond_33
    invoke-virtual {p2}, Landroid/location/Criteria;->isSpeedRequired()Z

    #@36
    move-result v1

    #@37
    if-eqz v1, :cond_3d

    #@39
    iget-boolean v1, p1, Lcom/android/internal/location/ProviderProperties;->mSupportsSpeed:Z

    #@3b
    if-eqz v1, :cond_a

    #@3d
    .line 111
    :cond_3d
    invoke-virtual {p2}, Landroid/location/Criteria;->isBearingRequired()Z

    #@40
    move-result v1

    #@41
    if-eqz v1, :cond_47

    #@43
    iget-boolean v1, p1, Lcom/android/internal/location/ProviderProperties;->mSupportsBearing:Z

    #@45
    if-eqz v1, :cond_a

    #@47
    .line 114
    :cond_47
    invoke-virtual {p2}, Landroid/location/Criteria;->isCostAllowed()Z

    #@4a
    move-result v1

    #@4b
    if-nez v1, :cond_51

    #@4d
    iget-boolean v1, p1, Lcom/android/internal/location/ProviderProperties;->mHasMonetaryCost:Z

    #@4f
    if-nez v1, :cond_a

    #@51
    .line 117
    :cond_51
    const/4 v0, 0x1

    #@52
    goto :goto_a
.end method


# virtual methods
.method public getAccuracy()I
    .registers 2

    #@0
    .prologue
    .line 203
    iget-object v0, p0, Landroid/location/LocationProvider;->mProperties:Lcom/android/internal/location/ProviderProperties;

    #@2
    iget v0, v0, Lcom/android/internal/location/ProviderProperties;->mAccuracy:I

    #@4
    return v0
.end method

.method public getName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Landroid/location/LocationProvider;->mName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getPowerRequirement()I
    .registers 2

    #@0
    .prologue
    .line 192
    iget-object v0, p0, Landroid/location/LocationProvider;->mProperties:Lcom/android/internal/location/ProviderProperties;

    #@2
    iget v0, v0, Lcom/android/internal/location/ProviderProperties;->mPowerRequirement:I

    #@4
    return v0
.end method

.method public hasMonetaryCost()Z
    .registers 2

    #@0
    .prologue
    .line 152
    iget-object v0, p0, Landroid/location/LocationProvider;->mProperties:Lcom/android/internal/location/ProviderProperties;

    #@2
    iget-boolean v0, v0, Lcom/android/internal/location/ProviderProperties;->mHasMonetaryCost:Z

    #@4
    return v0
.end method

.method public meetsCriteria(Landroid/location/Criteria;)Z
    .registers 4
    .parameter "criteria"

    #@0
    .prologue
    .line 79
    iget-object v0, p0, Landroid/location/LocationProvider;->mName:Ljava/lang/String;

    #@2
    iget-object v1, p0, Landroid/location/LocationProvider;->mProperties:Lcom/android/internal/location/ProviderProperties;

    #@4
    invoke-static {v0, v1, p1}, Landroid/location/LocationProvider;->propertiesMeetCriteria(Ljava/lang/String;Lcom/android/internal/location/ProviderProperties;Landroid/location/Criteria;)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public requiresCell()Z
    .registers 2

    #@0
    .prologue
    .line 143
    iget-object v0, p0, Landroid/location/LocationProvider;->mProperties:Lcom/android/internal/location/ProviderProperties;

    #@2
    iget-boolean v0, v0, Lcom/android/internal/location/ProviderProperties;->mRequiresCell:Z

    #@4
    return v0
.end method

.method public requiresNetwork()Z
    .registers 2

    #@0
    .prologue
    .line 125
    iget-object v0, p0, Landroid/location/LocationProvider;->mProperties:Lcom/android/internal/location/ProviderProperties;

    #@2
    iget-boolean v0, v0, Lcom/android/internal/location/ProviderProperties;->mRequiresNetwork:Z

    #@4
    return v0
.end method

.method public requiresSatellite()Z
    .registers 2

    #@0
    .prologue
    .line 134
    iget-object v0, p0, Landroid/location/LocationProvider;->mProperties:Lcom/android/internal/location/ProviderProperties;

    #@2
    iget-boolean v0, v0, Lcom/android/internal/location/ProviderProperties;->mRequiresSatellite:Z

    #@4
    return v0
.end method

.method public supportsAltitude()Z
    .registers 2

    #@0
    .prologue
    .line 162
    iget-object v0, p0, Landroid/location/LocationProvider;->mProperties:Lcom/android/internal/location/ProviderProperties;

    #@2
    iget-boolean v0, v0, Lcom/android/internal/location/ProviderProperties;->mSupportsAltitude:Z

    #@4
    return v0
.end method

.method public supportsBearing()Z
    .registers 2

    #@0
    .prologue
    .line 182
    iget-object v0, p0, Landroid/location/LocationProvider;->mProperties:Lcom/android/internal/location/ProviderProperties;

    #@2
    iget-boolean v0, v0, Lcom/android/internal/location/ProviderProperties;->mSupportsBearing:Z

    #@4
    return v0
.end method

.method public supportsSpeed()Z
    .registers 2

    #@0
    .prologue
    .line 172
    iget-object v0, p0, Landroid/location/LocationProvider;->mProperties:Lcom/android/internal/location/ProviderProperties;

    #@2
    iget-boolean v0, v0, Lcom/android/internal/location/ProviderProperties;->mSupportsSpeed:Z

    #@4
    return v0
.end method
