.class public Landroid/filterpacks/base/GLTextureTarget;
.super Landroid/filterfw/core/Filter;
.source "GLTextureTarget.java"


# instance fields
.field private mTexId:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "texId"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 40
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@3
    .line 41
    return-void
.end method


# virtual methods
.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 9
    .parameter "context"

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    .line 51
    const-string v3, "frame"

    #@3
    invoke-virtual {p0, v3}, Landroid/filterpacks/base/GLTextureTarget;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@6
    move-result-object v2

    #@7
    .line 53
    .local v2, input:Landroid/filterfw/core/Frame;
    invoke-virtual {v2}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@a
    move-result-object v3

    #@b
    invoke-virtual {v3}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@e
    move-result v3

    #@f
    invoke-virtual {v2}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@12
    move-result-object v4

    #@13
    invoke-virtual {v4}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@16
    move-result v4

    #@17
    invoke-static {v3, v4, v5, v5}, Landroid/filterfw/format/ImageFormat;->create(IIII)Landroid/filterfw/core/MutableFrameFormat;

    #@1a
    move-result-object v0

    #@1b
    .line 58
    .local v0, format:Landroid/filterfw/core/FrameFormat;
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@1e
    move-result-object v3

    #@1f
    const/16 v4, 0x64

    #@21
    iget v5, p0, Landroid/filterpacks/base/GLTextureTarget;->mTexId:I

    #@23
    int-to-long v5, v5

    #@24
    invoke-virtual {v3, v0, v4, v5, v6}, Landroid/filterfw/core/FrameManager;->newBoundFrame(Landroid/filterfw/core/FrameFormat;IJ)Landroid/filterfw/core/Frame;

    #@27
    move-result-object v1

    #@28
    .line 61
    .local v1, frame:Landroid/filterfw/core/Frame;
    invoke-virtual {v1, v2}, Landroid/filterfw/core/Frame;->setDataFromFrame(Landroid/filterfw/core/Frame;)V

    #@2b
    .line 62
    invoke-virtual {v1}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@2e
    .line 63
    return-void
.end method

.method public setupPorts()V
    .registers 3

    #@0
    .prologue
    .line 45
    const-string v0, "frame"

    #@2
    const/4 v1, 0x3

    #@3
    invoke-static {v1}, Landroid/filterfw/format/ImageFormat;->create(I)Landroid/filterfw/core/MutableFrameFormat;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/base/GLTextureTarget;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@a
    .line 46
    return-void
.end method
