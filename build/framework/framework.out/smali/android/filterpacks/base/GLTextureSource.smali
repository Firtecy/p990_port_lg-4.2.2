.class public Landroid/filterpacks/base/GLTextureSource;
.super Landroid/filterfw/core/Filter;
.source "GLTextureSource.java"


# instance fields
.field private mFrame:Landroid/filterfw/core/Frame;

.field private mHeight:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "height"
    .end annotation
.end field

.field private mRepeatFrame:Z
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "repeatFrame"
    .end annotation
.end field

.field private mTexId:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "texId"
    .end annotation
.end field

.field private mTimestamp:J
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "timestamp"
    .end annotation
.end field

.field private mWidth:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "width"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter "name"

    #@0
    .prologue
    .line 59
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@3
    .line 45
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Landroid/filterpacks/base/GLTextureSource;->mRepeatFrame:Z

    #@6
    .line 53
    const-wide/16 v0, -0x1

    #@8
    iput-wide v0, p0, Landroid/filterpacks/base/GLTextureSource;->mTimestamp:J

    #@a
    .line 60
    return-void
.end method


# virtual methods
.method public fieldPortValueUpdated(Ljava/lang/String;Landroid/filterfw/core/FilterContext;)V
    .registers 4
    .parameter "name"
    .parameter "context"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Landroid/filterpacks/base/GLTextureSource;->mFrame:Landroid/filterfw/core/Frame;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 72
    iget-object v0, p0, Landroid/filterpacks/base/GLTextureSource;->mFrame:Landroid/filterfw/core/Frame;

    #@6
    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@9
    .line 73
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Landroid/filterpacks/base/GLTextureSource;->mFrame:Landroid/filterfw/core/Frame;

    #@c
    .line 75
    :cond_c
    return-void
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 7
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x3

    #@1
    .line 80
    iget-object v1, p0, Landroid/filterpacks/base/GLTextureSource;->mFrame:Landroid/filterfw/core/Frame;

    #@3
    if-nez v1, :cond_23

    #@5
    .line 81
    iget v1, p0, Landroid/filterpacks/base/GLTextureSource;->mWidth:I

    #@7
    iget v2, p0, Landroid/filterpacks/base/GLTextureSource;->mHeight:I

    #@9
    invoke-static {v1, v2, v3, v3}, Landroid/filterfw/format/ImageFormat;->create(IIII)Landroid/filterfw/core/MutableFrameFormat;

    #@c
    move-result-object v0

    #@d
    .line 84
    .local v0, outputFormat:Landroid/filterfw/core/FrameFormat;
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@10
    move-result-object v1

    #@11
    const/16 v2, 0x64

    #@13
    iget v3, p0, Landroid/filterpacks/base/GLTextureSource;->mTexId:I

    #@15
    int-to-long v3, v3

    #@16
    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/filterfw/core/FrameManager;->newBoundFrame(Landroid/filterfw/core/FrameFormat;IJ)Landroid/filterfw/core/Frame;

    #@19
    move-result-object v1

    #@1a
    iput-object v1, p0, Landroid/filterpacks/base/GLTextureSource;->mFrame:Landroid/filterfw/core/Frame;

    #@1c
    .line 87
    iget-object v1, p0, Landroid/filterpacks/base/GLTextureSource;->mFrame:Landroid/filterfw/core/Frame;

    #@1e
    iget-wide v2, p0, Landroid/filterpacks/base/GLTextureSource;->mTimestamp:J

    #@20
    invoke-virtual {v1, v2, v3}, Landroid/filterfw/core/Frame;->setTimestamp(J)V

    #@23
    .line 91
    .end local v0           #outputFormat:Landroid/filterfw/core/FrameFormat;
    :cond_23
    const-string v1, "frame"

    #@25
    iget-object v2, p0, Landroid/filterpacks/base/GLTextureSource;->mFrame:Landroid/filterfw/core/Frame;

    #@27
    invoke-virtual {p0, v1, v2}, Landroid/filterpacks/base/GLTextureSource;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@2a
    .line 93
    iget-boolean v1, p0, Landroid/filterpacks/base/GLTextureSource;->mRepeatFrame:Z

    #@2c
    if-nez v1, :cond_33

    #@2e
    .line 95
    const-string v1, "frame"

    #@30
    invoke-virtual {p0, v1}, Landroid/filterpacks/base/GLTextureSource;->closeOutputPort(Ljava/lang/String;)V

    #@33
    .line 97
    :cond_33
    return-void
.end method

.method public setupPorts()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x3

    #@1
    .line 64
    const-string v0, "frame"

    #@3
    invoke-static {v1, v1}, Landroid/filterfw/format/ImageFormat;->create(II)Landroid/filterfw/core/MutableFrameFormat;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/base/GLTextureSource;->addOutputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@a
    .line 66
    return-void
.end method

.method public tearDown(Landroid/filterfw/core/FilterContext;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 101
    iget-object v0, p0, Landroid/filterpacks/base/GLTextureSource;->mFrame:Landroid/filterfw/core/Frame;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 102
    iget-object v0, p0, Landroid/filterpacks/base/GLTextureSource;->mFrame:Landroid/filterfw/core/Frame;

    #@6
    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@9
    .line 104
    :cond_9
    return-void
.end method
