.class public Landroid/filterpacks/base/InputStreamSource;
.super Landroid/filterfw/core/Filter;
.source "InputStreamSource.java"


# instance fields
.field private mInputStream:Ljava/io/InputStream;
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "stream"
    .end annotation
.end field

.field private mOutputFormat:Landroid/filterfw/core/MutableFrameFormat;
    .annotation runtime Landroid/filterfw/core/GenerateFinalPort;
        hasDefault = true
        name = "format"
    .end annotation
.end field

.field private mTarget:Ljava/lang/String;
    .annotation runtime Landroid/filterfw/core/GenerateFinalPort;
        name = "target"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 50
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@3
    .line 46
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Landroid/filterpacks/base/InputStreamSource;->mOutputFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@6
    .line 51
    return-void
.end method


# virtual methods
.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 12
    .parameter "context"

    #@0
    .prologue
    .line 64
    const/4 v5, 0x0

    #@1
    .line 65
    .local v5, fileSize:I
    const/4 v1, 0x0

    #@2
    .line 69
    .local v1, byteBuffer:Ljava/nio/ByteBuffer;
    :try_start_2
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    #@4
    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@7
    .line 70
    .local v2, byteStream:Ljava/io/ByteArrayOutputStream;
    const/16 v7, 0x400

    #@9
    new-array v0, v7, [B

    #@b
    .line 72
    .local v0, buffer:[B
    :goto_b
    iget-object v7, p0, Landroid/filterpacks/base/InputStreamSource;->mInputStream:Ljava/io/InputStream;

    #@d
    invoke-virtual {v7, v0}, Ljava/io/InputStream;->read([B)I

    #@10
    move-result v3

    #@11
    .local v3, bytesRead:I
    if-lez v3, :cond_19

    #@13
    .line 73
    const/4 v7, 0x0

    #@14
    invoke-virtual {v2, v0, v7, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@17
    .line 74
    add-int/2addr v5, v3

    #@18
    goto :goto_b

    #@19
    .line 76
    :cond_19
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@1c
    move-result-object v7

    #@1d
    invoke-static {v7}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;
    :try_end_20
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_20} :catch_41

    #@20
    move-result-object v1

    #@21
    .line 83
    iget-object v7, p0, Landroid/filterpacks/base/InputStreamSource;->mOutputFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@23
    invoke-virtual {v7, v5}, Landroid/filterfw/core/MutableFrameFormat;->setDimensions(I)V

    #@26
    .line 84
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@29
    move-result-object v7

    #@2a
    iget-object v8, p0, Landroid/filterpacks/base/InputStreamSource;->mOutputFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@2c
    invoke-virtual {v7, v8}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@2f
    move-result-object v6

    #@30
    .line 85
    .local v6, output:Landroid/filterfw/core/Frame;
    invoke-virtual {v6, v1}, Landroid/filterfw/core/Frame;->setData(Ljava/nio/ByteBuffer;)V

    #@33
    .line 88
    const-string v7, "data"

    #@35
    invoke-virtual {p0, v7, v6}, Landroid/filterpacks/base/InputStreamSource;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@38
    .line 91
    invoke-virtual {v6}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@3b
    .line 94
    const-string v7, "data"

    #@3d
    invoke-virtual {p0, v7}, Landroid/filterpacks/base/InputStreamSource;->closeOutputPort(Ljava/lang/String;)V

    #@40
    .line 95
    return-void

    #@41
    .line 77
    .end local v0           #buffer:[B
    .end local v2           #byteStream:Ljava/io/ByteArrayOutputStream;
    .end local v3           #bytesRead:I
    .end local v6           #output:Landroid/filterfw/core/Frame;
    :catch_41
    move-exception v4

    #@42
    .line 78
    .local v4, exception:Ljava/io/IOException;
    new-instance v7, Ljava/lang/RuntimeException;

    #@44
    new-instance v8, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v9, "InputStreamSource: Could not read stream: "

    #@4b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v8

    #@4f
    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    #@52
    move-result-object v9

    #@53
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v8

    #@57
    const-string v9, "!"

    #@59
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v8

    #@5d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v8

    #@61
    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@64
    throw v7
.end method

.method public setupPorts()V
    .registers 4

    #@0
    .prologue
    .line 55
    iget-object v1, p0, Landroid/filterpacks/base/InputStreamSource;->mTarget:Ljava/lang/String;

    #@2
    invoke-static {v1}, Landroid/filterfw/core/FrameFormat;->readTargetString(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    .line 56
    .local v0, target:I
    iget-object v1, p0, Landroid/filterpacks/base/InputStreamSource;->mOutputFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@8
    if-nez v1, :cond_10

    #@a
    .line 57
    invoke-static {v0}, Landroid/filterfw/format/PrimitiveFormat;->createByteFormat(I)Landroid/filterfw/core/MutableFrameFormat;

    #@d
    move-result-object v1

    #@e
    iput-object v1, p0, Landroid/filterpacks/base/InputStreamSource;->mOutputFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@10
    .line 59
    :cond_10
    const-string v1, "data"

    #@12
    iget-object v2, p0, Landroid/filterpacks/base/InputStreamSource;->mOutputFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@14
    invoke-virtual {p0, v1, v2}, Landroid/filterpacks/base/InputStreamSource;->addOutputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@17
    .line 60
    return-void
.end method
