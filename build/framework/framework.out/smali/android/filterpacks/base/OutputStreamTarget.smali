.class public Landroid/filterpacks/base/OutputStreamTarget;
.super Landroid/filterfw/core/Filter;
.source "OutputStreamTarget.java"


# instance fields
.field private mOutputStream:Ljava/io/OutputStream;
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "stream"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 39
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@3
    .line 40
    return-void
.end method


# virtual methods
.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 10
    .parameter "context"

    #@0
    .prologue
    .line 49
    const-string v4, "data"

    #@2
    invoke-virtual {p0, v4}, Landroid/filterpacks/base/OutputStreamTarget;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@5
    move-result-object v2

    #@6
    .line 52
    .local v2, input:Landroid/filterfw/core/Frame;
    invoke-virtual {v2}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@9
    move-result-object v4

    #@a
    invoke-virtual {v4}, Landroid/filterfw/core/FrameFormat;->getObjectClass()Ljava/lang/Class;

    #@d
    move-result-object v4

    #@e
    const-class v5, Ljava/lang/String;

    #@10
    if-ne v4, v5, :cond_34

    #@12
    .line 53
    invoke-virtual {v2}, Landroid/filterfw/core/Frame;->getObjectValue()Ljava/lang/Object;

    #@15
    move-result-object v3

    #@16
    check-cast v3, Ljava/lang/String;

    #@18
    .line 54
    .local v3, stringVal:Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    #@1b
    move-result-object v4

    #@1c
    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    #@1f
    move-result-object v0

    #@20
    .line 59
    .end local v3           #stringVal:Ljava/lang/String;
    .local v0, data:Ljava/nio/ByteBuffer;
    :goto_20
    :try_start_20
    iget-object v4, p0, Landroid/filterpacks/base/OutputStreamTarget;->mOutputStream:Ljava/io/OutputStream;

    #@22
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    #@25
    move-result-object v5

    #@26
    const/4 v6, 0x0

    #@27
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    #@2a
    move-result v7

    #@2b
    invoke-virtual {v4, v5, v6, v7}, Ljava/io/OutputStream;->write([BII)V

    #@2e
    .line 60
    iget-object v4, p0, Landroid/filterpacks/base/OutputStreamTarget;->mOutputStream:Ljava/io/OutputStream;

    #@30
    invoke-virtual {v4}, Ljava/io/OutputStream;->flush()V
    :try_end_33
    .catch Ljava/io/IOException; {:try_start_20 .. :try_end_33} :catch_39

    #@33
    .line 65
    return-void

    #@34
    .line 56
    .end local v0           #data:Ljava/nio/ByteBuffer;
    :cond_34
    invoke-virtual {v2}, Landroid/filterfw/core/Frame;->getData()Ljava/nio/ByteBuffer;

    #@37
    move-result-object v0

    #@38
    .restart local v0       #data:Ljava/nio/ByteBuffer;
    goto :goto_20

    #@39
    .line 61
    :catch_39
    move-exception v1

    #@3a
    .line 62
    .local v1, exception:Ljava/io/IOException;
    new-instance v4, Ljava/lang/RuntimeException;

    #@3c
    new-instance v5, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v6, "OutputStreamTarget: Could not write to stream: "

    #@43
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v5

    #@47
    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    #@4a
    move-result-object v6

    #@4b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v5

    #@4f
    const-string v6, "!"

    #@51
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v5

    #@55
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v5

    #@59
    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@5c
    throw v4
.end method

.method public setupPorts()V
    .registers 2

    #@0
    .prologue
    .line 44
    const-string v0, "data"

    #@2
    invoke-virtual {p0, v0}, Landroid/filterpacks/base/OutputStreamTarget;->addInputPort(Ljava/lang/String;)V

    #@5
    .line 45
    return-void
.end method
