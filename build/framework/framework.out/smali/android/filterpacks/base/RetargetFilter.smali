.class public Landroid/filterpacks/base/RetargetFilter;
.super Landroid/filterfw/core/Filter;
.source "RetargetFilter.java"


# instance fields
.field private mOutputFormat:Landroid/filterfw/core/MutableFrameFormat;

.field private mTarget:I

.field private mTargetString:Ljava/lang/String;
    .annotation runtime Landroid/filterfw/core/GenerateFinalPort;
        hasDefault = false
        name = "target"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 41
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@3
    .line 38
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/filterpacks/base/RetargetFilter;->mTarget:I

    #@6
    .line 42
    return-void
.end method


# virtual methods
.method public getOutputFormat(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .registers 5
    .parameter "portName"
    .parameter "inputFormat"

    #@0
    .prologue
    .line 56
    invoke-virtual {p2}, Landroid/filterfw/core/FrameFormat;->mutableCopy()Landroid/filterfw/core/MutableFrameFormat;

    #@3
    move-result-object v0

    #@4
    .line 57
    .local v0, retargeted:Landroid/filterfw/core/MutableFrameFormat;
    iget v1, p0, Landroid/filterpacks/base/RetargetFilter;->mTarget:I

    #@6
    invoke-virtual {v0, v1}, Landroid/filterfw/core/MutableFrameFormat;->setTarget(I)V

    #@9
    .line 58
    return-object v0
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    .line 64
    const-string v2, "frame"

    #@2
    invoke-virtual {p0, v2}, Landroid/filterpacks/base/RetargetFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@5
    move-result-object v0

    #@6
    .line 67
    .local v0, input:Landroid/filterfw/core/Frame;
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@9
    move-result-object v2

    #@a
    iget v3, p0, Landroid/filterpacks/base/RetargetFilter;->mTarget:I

    #@c
    invoke-virtual {v2, v0, v3}, Landroid/filterfw/core/FrameManager;->duplicateFrameToTarget(Landroid/filterfw/core/Frame;I)Landroid/filterfw/core/Frame;

    #@f
    move-result-object v1

    #@10
    .line 70
    .local v1, output:Landroid/filterfw/core/Frame;
    const-string v2, "frame"

    #@12
    invoke-virtual {p0, v2, v1}, Landroid/filterpacks/base/RetargetFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@15
    .line 73
    invoke-virtual {v1}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@18
    .line 74
    return-void
.end method

.method public setupPorts()V
    .registers 3

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Landroid/filterpacks/base/RetargetFilter;->mTargetString:Ljava/lang/String;

    #@2
    invoke-static {v0}, Landroid/filterfw/core/FrameFormat;->readTargetString(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    iput v0, p0, Landroid/filterpacks/base/RetargetFilter;->mTarget:I

    #@8
    .line 50
    const-string v0, "frame"

    #@a
    invoke-virtual {p0, v0}, Landroid/filterpacks/base/RetargetFilter;->addInputPort(Ljava/lang/String;)V

    #@d
    .line 51
    const-string v0, "frame"

    #@f
    const-string v1, "frame"

    #@11
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/base/RetargetFilter;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    #@14
    .line 52
    return-void
.end method
