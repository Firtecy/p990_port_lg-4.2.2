.class public Landroid/filterpacks/imageproc/VignetteFilter;
.super Landroid/filterfw/core/Filter;
.source "VignetteFilter.java"


# instance fields
.field private mHeight:I

.field private mProgram:Landroid/filterfw/core/Program;

.field private mScale:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "scale"
    .end annotation
.end field

.field private final mShade:F

.field private final mSlope:F

.field private mTarget:I

.field private mTileSize:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "tile_size"
    .end annotation
.end field

.field private final mVignetteShader:Ljava/lang/String;

.field private mWidth:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter "name"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 66
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@4
    .line 33
    const/4 v0, 0x0

    #@5
    iput v0, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mScale:F

    #@7
    .line 36
    const/16 v0, 0x280

    #@9
    iput v0, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mTileSize:I

    #@b
    .line 41
    iput v1, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mWidth:I

    #@d
    .line 42
    iput v1, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mHeight:I

    #@f
    .line 43
    iput v1, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mTarget:I

    #@11
    .line 45
    const/high16 v0, 0x41a0

    #@13
    iput v0, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mSlope:F

    #@15
    .line 46
    const v0, 0x3f59999a

    #@18
    iput v0, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mShade:F

    #@1a
    .line 48
    const-string/jumbo v0, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform float range;\nuniform float inv_max_dist;\nuniform float shade;\nuniform vec2 scale;\nvarying vec2 v_texcoord;\nvoid main() {\n  const float slope = 20.0;\n  vec2 coord = v_texcoord - vec2(0.5, 0.5);\n  float dist = length(coord * scale);\n  float lumen = shade / (1.0 + exp((dist * inv_max_dist - range) * slope)) + (1.0 - shade);\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  gl_FragColor = vec4(color.rgb * lumen, color.a);\n}\n"

    #@1d
    iput-object v0, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mVignetteShader:Ljava/lang/String;

    #@1f
    .line 67
    return-void
.end method

.method private initParameters()V
    .registers 8

    #@0
    .prologue
    const/high16 v6, 0x3f80

    #@2
    const/4 v5, 0x1

    #@3
    const/4 v4, 0x0

    #@4
    .line 96
    iget-object v2, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mProgram:Landroid/filterfw/core/Program;

    #@6
    if-eqz v2, :cond_58

    #@8
    .line 97
    const/4 v2, 0x2

    #@9
    new-array v1, v2, [F

    #@b
    .line 98
    .local v1, scale:[F
    iget v2, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mWidth:I

    #@d
    iget v3, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mHeight:I

    #@f
    if-le v2, v3, :cond_59

    #@11
    .line 99
    aput v6, v1, v4

    #@13
    .line 100
    iget v2, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mHeight:I

    #@15
    int-to-float v2, v2

    #@16
    iget v3, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mWidth:I

    #@18
    int-to-float v3, v3

    #@19
    div-float/2addr v2, v3

    #@1a
    aput v2, v1, v5

    #@1c
    .line 105
    :goto_1c
    aget v2, v1, v4

    #@1e
    aget v3, v1, v4

    #@20
    mul-float/2addr v2, v3

    #@21
    aget v3, v1, v5

    #@23
    aget v4, v1, v5

    #@25
    mul-float/2addr v3, v4

    #@26
    add-float/2addr v2, v3

    #@27
    float-to-double v2, v2

    #@28
    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    #@2b
    move-result-wide v2

    #@2c
    double-to-float v2, v2

    #@2d
    const/high16 v3, 0x3f00

    #@2f
    mul-float v0, v2, v3

    #@31
    .line 106
    .local v0, max_dist:F
    iget-object v2, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mProgram:Landroid/filterfw/core/Program;

    #@33
    const-string/jumbo v3, "scale"

    #@36
    invoke-virtual {v2, v3, v1}, Landroid/filterfw/core/Program;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@39
    .line 107
    iget-object v2, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mProgram:Landroid/filterfw/core/Program;

    #@3b
    const-string v3, "inv_max_dist"

    #@3d
    div-float v4, v6, v0

    #@3f
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@42
    move-result-object v4

    #@43
    invoke-virtual {v2, v3, v4}, Landroid/filterfw/core/Program;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@46
    .line 108
    iget-object v2, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mProgram:Landroid/filterfw/core/Program;

    #@48
    const-string/jumbo v3, "shade"

    #@4b
    const v4, 0x3f59999a

    #@4e
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@51
    move-result-object v4

    #@52
    invoke-virtual {v2, v3, v4}, Landroid/filterfw/core/Program;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@55
    .line 110
    invoke-direct {p0}, Landroid/filterpacks/imageproc/VignetteFilter;->updateParameters()V

    #@58
    .line 112
    .end local v0           #max_dist:F
    .end local v1           #scale:[F
    :cond_58
    return-void

    #@59
    .line 102
    .restart local v1       #scale:[F
    :cond_59
    iget v2, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mWidth:I

    #@5b
    int-to-float v2, v2

    #@5c
    iget v3, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mHeight:I

    #@5e
    int-to-float v3, v3

    #@5f
    div-float/2addr v2, v3

    #@60
    aput v2, v1, v4

    #@62
    .line 103
    aput v6, v1, v5

    #@64
    goto :goto_1c
.end method

.method private updateParameters()V
    .registers 6

    #@0
    .prologue
    .line 118
    iget-object v0, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mProgram:Landroid/filterfw/core/Program;

    #@2
    const-string/jumbo v1, "range"

    #@5
    const v2, 0x3fa66666

    #@8
    iget v3, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mScale:F

    #@a
    float-to-double v3, v3

    #@b
    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    #@e
    move-result-wide v3

    #@f
    double-to-float v3, v3

    #@10
    const v4, 0x3f333333

    #@13
    mul-float/2addr v3, v4

    #@14
    sub-float/2addr v2, v3

    #@15
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/Program;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@1c
    .line 119
    return-void
.end method


# virtual methods
.method public fieldPortValueUpdated(Ljava/lang/String;Landroid/filterfw/core/FilterContext;)V
    .registers 4
    .parameter "name"
    .parameter "context"

    #@0
    .prologue
    .line 123
    iget-object v0, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mProgram:Landroid/filterfw/core/Program;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 124
    invoke-direct {p0}, Landroid/filterpacks/imageproc/VignetteFilter;->updateParameters()V

    #@7
    .line 126
    :cond_7
    return-void
.end method

.method public getOutputFormat(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .registers 3
    .parameter "portName"
    .parameter "inputFormat"

    #@0
    .prologue
    .line 77
    return-object p2
.end method

.method public initProgram(Landroid/filterfw/core/FilterContext;I)V
    .registers 7
    .parameter "context"
    .parameter "target"

    #@0
    .prologue
    .line 81
    packed-switch p2, :pswitch_data_34

    #@3
    .line 89
    new-instance v1, Ljava/lang/RuntimeException;

    #@5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v3, "Filter Sharpen does not support frames of target "

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    const-string v3, "!"

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@21
    throw v1

    #@22
    .line 83
    :pswitch_22
    new-instance v0, Landroid/filterfw/core/ShaderProgram;

    #@24
    const-string/jumbo v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform float range;\nuniform float inv_max_dist;\nuniform float shade;\nuniform vec2 scale;\nvarying vec2 v_texcoord;\nvoid main() {\n  const float slope = 20.0;\n  vec2 coord = v_texcoord - vec2(0.5, 0.5);\n  float dist = length(coord * scale);\n  float lumen = shade / (1.0 + exp((dist * inv_max_dist - range) * slope)) + (1.0 - shade);\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  gl_FragColor = vec4(color.rgb * lumen, color.a);\n}\n"

    #@27
    invoke-direct {v0, p1, v1}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;)V

    #@2a
    .line 84
    .local v0, shaderProgram:Landroid/filterfw/core/ShaderProgram;
    iget v1, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mTileSize:I

    #@2c
    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setMaximumTileSize(I)V

    #@2f
    .line 85
    iput-object v0, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mProgram:Landroid/filterfw/core/Program;

    #@31
    .line 92
    iput p2, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mTarget:I

    #@33
    .line 93
    return-void

    #@34
    .line 81
    :pswitch_data_34
    .packed-switch 0x3
        :pswitch_22
    .end packed-switch
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 7
    .parameter "context"

    #@0
    .prologue
    .line 131
    const-string v3, "image"

    #@2
    invoke-virtual {p0, v3}, Landroid/filterpacks/imageproc/VignetteFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@5
    move-result-object v0

    #@6
    .line 132
    .local v0, input:Landroid/filterfw/core/Frame;
    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@9
    move-result-object v1

    #@a
    .line 135
    .local v1, inputFormat:Landroid/filterfw/core/FrameFormat;
    iget-object v3, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mProgram:Landroid/filterfw/core/Program;

    #@c
    if-eqz v3, :cond_16

    #@e
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@11
    move-result v3

    #@12
    iget v4, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mTarget:I

    #@14
    if-eq v3, v4, :cond_1d

    #@16
    .line 136
    :cond_16
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@19
    move-result v3

    #@1a
    invoke-virtual {p0, p1, v3}, Landroid/filterpacks/imageproc/VignetteFilter;->initProgram(Landroid/filterfw/core/FilterContext;I)V

    #@1d
    .line 140
    :cond_1d
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@20
    move-result v3

    #@21
    iget v4, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mWidth:I

    #@23
    if-ne v3, v4, :cond_2d

    #@25
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@28
    move-result v3

    #@29
    iget v4, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mHeight:I

    #@2b
    if-eq v3, v4, :cond_3c

    #@2d
    .line 141
    :cond_2d
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@30
    move-result v3

    #@31
    iput v3, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mWidth:I

    #@33
    .line 142
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@36
    move-result v3

    #@37
    iput v3, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mHeight:I

    #@39
    .line 143
    invoke-direct {p0}, Landroid/filterpacks/imageproc/VignetteFilter;->initParameters()V

    #@3c
    .line 147
    :cond_3c
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v3, v1}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@43
    move-result-object v2

    #@44
    .line 150
    .local v2, output:Landroid/filterfw/core/Frame;
    iget-object v3, p0, Landroid/filterpacks/imageproc/VignetteFilter;->mProgram:Landroid/filterfw/core/Program;

    #@46
    invoke-virtual {v3, v0, v2}, Landroid/filterfw/core/Program;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@49
    .line 153
    const-string v3, "image"

    #@4b
    invoke-virtual {p0, v3, v2}, Landroid/filterpacks/imageproc/VignetteFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@4e
    .line 156
    invoke-virtual {v2}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@51
    .line 157
    return-void
.end method

.method public setupPorts()V
    .registers 3

    #@0
    .prologue
    .line 71
    const-string v0, "image"

    #@2
    const/4 v1, 0x3

    #@3
    invoke-static {v1}, Landroid/filterfw/format/ImageFormat;->create(I)Landroid/filterfw/core/MutableFrameFormat;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/VignetteFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@a
    .line 72
    const-string v0, "image"

    #@c
    const-string v1, "image"

    #@e
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/VignetteFilter;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 73
    return-void
.end method
