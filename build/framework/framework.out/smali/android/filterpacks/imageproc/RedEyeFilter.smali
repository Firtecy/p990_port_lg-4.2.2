.class public Landroid/filterpacks/imageproc/RedEyeFilter;
.super Landroid/filterfw/core/Filter;
.source "RedEyeFilter.java"


# static fields
.field private static final DEFAULT_RED_INTENSITY:F = 1.3f

.field private static final MIN_RADIUS:F = 10.0f

.field private static final RADIUS_RATIO:F = 0.06f


# instance fields
.field private final mCanvas:Landroid/graphics/Canvas;

.field private mCenters:[F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "centers"
    .end annotation
.end field

.field private mHeight:I

.field private final mPaint:Landroid/graphics/Paint;

.field private mProgram:Landroid/filterfw/core/Program;

.field private mRadius:F

.field private mRedEyeBitmap:Landroid/graphics/Bitmap;

.field private mRedEyeFrame:Landroid/filterfw/core/Frame;

.field private final mRedEyeShader:Ljava/lang/String;

.field private mTarget:I

.field private mTileSize:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "tile_size"
    .end annotation
.end field

.field private mWidth:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter "name"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 86
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@4
    .line 49
    const/16 v0, 0x280

    #@6
    iput v0, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mTileSize:I

    #@8
    .line 55
    new-instance v0, Landroid/graphics/Canvas;

    #@a
    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    #@d
    iput-object v0, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mCanvas:Landroid/graphics/Canvas;

    #@f
    .line 56
    new-instance v0, Landroid/graphics/Paint;

    #@11
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    #@14
    iput-object v0, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mPaint:Landroid/graphics/Paint;

    #@16
    .line 60
    iput v1, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mWidth:I

    #@18
    .line 61
    iput v1, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mHeight:I

    #@1a
    .line 64
    iput v1, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mTarget:I

    #@1c
    .line 66
    const-string/jumbo v0, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform float intensity;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  vec4 mask = texture2D(tex_sampler_1, v_texcoord);\n  if (mask.a > 0.0) {\n    float green_blue = color.g + color.b;\n    float red_intensity = color.r / green_blue;\n    if (red_intensity > intensity) {\n      color.r = 0.5 * green_blue;\n    }\n  }\n  gl_FragColor = color;\n}\n"

    #@1f
    iput-object v0, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mRedEyeShader:Ljava/lang/String;

    #@21
    .line 87
    return-void
.end method

.method private createRedEyeFrame(Landroid/filterfw/core/FilterContext;)V
    .registers 13
    .parameter "context"

    #@0
    .prologue
    const/4 v10, 0x3

    #@1
    .line 159
    iget v5, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mWidth:I

    #@3
    div-int/lit8 v1, v5, 0x2

    #@5
    .line 160
    .local v1, bitmapWidth:I
    iget v5, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mHeight:I

    #@7
    div-int/lit8 v0, v5, 0x2

    #@9
    .line 162
    .local v0, bitmapHeight:I
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@b
    invoke-static {v1, v0, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    #@e
    move-result-object v4

    #@f
    .line 163
    .local v4, redEyeBitmap:Landroid/graphics/Bitmap;
    iget-object v5, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mCanvas:Landroid/graphics/Canvas;

    #@11
    invoke-virtual {v5, v4}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    #@14
    .line 164
    iget-object v5, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mPaint:Landroid/graphics/Paint;

    #@16
    const/4 v6, -0x1

    #@17
    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    #@1a
    .line 165
    const/high16 v5, 0x4120

    #@1c
    const v6, 0x3d75c28f

    #@1f
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    #@22
    move-result v7

    #@23
    int-to-float v7, v7

    #@24
    mul-float/2addr v6, v7

    #@25
    invoke-static {v5, v6}, Ljava/lang/Math;->max(FF)F

    #@28
    move-result v5

    #@29
    iput v5, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mRadius:F

    #@2b
    .line 167
    const/4 v3, 0x0

    #@2c
    .local v3, i:I
    :goto_2c
    iget-object v5, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mCenters:[F

    #@2e
    array-length v5, v5

    #@2f
    if-ge v3, v5, :cond_4b

    #@31
    .line 168
    iget-object v5, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mCanvas:Landroid/graphics/Canvas;

    #@33
    iget-object v6, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mCenters:[F

    #@35
    aget v6, v6, v3

    #@37
    int-to-float v7, v1

    #@38
    mul-float/2addr v6, v7

    #@39
    iget-object v7, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mCenters:[F

    #@3b
    add-int/lit8 v8, v3, 0x1

    #@3d
    aget v7, v7, v8

    #@3f
    int-to-float v8, v0

    #@40
    mul-float/2addr v7, v8

    #@41
    iget v8, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mRadius:F

    #@43
    iget-object v9, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mPaint:Landroid/graphics/Paint;

    #@45
    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    #@48
    .line 167
    add-int/lit8 v3, v3, 0x2

    #@4a
    goto :goto_2c

    #@4b
    .line 172
    :cond_4b
    invoke-static {v1, v0, v10, v10}, Landroid/filterfw/format/ImageFormat;->create(IIII)Landroid/filterfw/core/MutableFrameFormat;

    #@4e
    move-result-object v2

    #@4f
    .line 175
    .local v2, format:Landroid/filterfw/core/FrameFormat;
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@52
    move-result-object v5

    #@53
    invoke-virtual {v5, v2}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@56
    move-result-object v5

    #@57
    iput-object v5, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mRedEyeFrame:Landroid/filterfw/core/Frame;

    #@59
    .line 176
    iget-object v5, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mRedEyeFrame:Landroid/filterfw/core/Frame;

    #@5b
    invoke-virtual {v5, v4}, Landroid/filterfw/core/Frame;->setBitmap(Landroid/graphics/Bitmap;)V

    #@5e
    .line 177
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    #@61
    .line 178
    return-void
.end method

.method private updateProgramParams()V
    .registers 3

    #@0
    .prologue
    .line 181
    iget-object v0, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mCenters:[F

    #@2
    array-length v0, v0

    #@3
    rem-int/lit8 v0, v0, 0x2

    #@5
    const/4 v1, 0x1

    #@6
    if-ne v0, v1, :cond_10

    #@8
    .line 182
    new-instance v0, Ljava/lang/RuntimeException;

    #@a
    const-string v1, "The size of center array must be even."

    #@c
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 184
    :cond_10
    return-void
.end method


# virtual methods
.method public fieldPortValueUpdated(Ljava/lang/String;Landroid/filterfw/core/FilterContext;)V
    .registers 4
    .parameter "name"
    .parameter "context"

    #@0
    .prologue
    .line 153
    iget-object v0, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mProgram:Landroid/filterfw/core/Program;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 154
    invoke-direct {p0}, Landroid/filterpacks/imageproc/RedEyeFilter;->updateProgramParams()V

    #@7
    .line 156
    :cond_7
    return-void
.end method

.method public getOutputFormat(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .registers 3
    .parameter "portName"
    .parameter "inputFormat"

    #@0
    .prologue
    .line 97
    return-object p2
.end method

.method public initProgram(Landroid/filterfw/core/FilterContext;I)V
    .registers 7
    .parameter "context"
    .parameter "target"

    #@0
    .prologue
    .line 101
    packed-switch p2, :pswitch_data_42

    #@3
    .line 109
    new-instance v1, Ljava/lang/RuntimeException;

    #@5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v3, "Filter RedEye does not support frames of target "

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    const-string v3, "!"

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@21
    throw v1

    #@22
    .line 103
    :pswitch_22
    new-instance v0, Landroid/filterfw/core/ShaderProgram;

    #@24
    const-string/jumbo v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform float intensity;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  vec4 mask = texture2D(tex_sampler_1, v_texcoord);\n  if (mask.a > 0.0) {\n    float green_blue = color.g + color.b;\n    float red_intensity = color.r / green_blue;\n    if (red_intensity > intensity) {\n      color.r = 0.5 * green_blue;\n    }\n  }\n  gl_FragColor = color;\n}\n"

    #@27
    invoke-direct {v0, p1, v1}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;)V

    #@2a
    .line 104
    .local v0, shaderProgram:Landroid/filterfw/core/ShaderProgram;
    iget v1, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mTileSize:I

    #@2c
    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setMaximumTileSize(I)V

    #@2f
    .line 105
    iput-object v0, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mProgram:Landroid/filterfw/core/Program;

    #@31
    .line 106
    iget-object v1, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mProgram:Landroid/filterfw/core/Program;

    #@33
    const-string v2, "intensity"

    #@35
    const v3, 0x3fa66666

    #@38
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@3b
    move-result-object v3

    #@3c
    invoke-virtual {v1, v2, v3}, Landroid/filterfw/core/Program;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@3f
    .line 112
    iput p2, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mTarget:I

    #@41
    .line 113
    return-void

    #@42
    .line 101
    :pswitch_data_42
    .packed-switch 0x3
        :pswitch_22
    .end packed-switch
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 8
    .parameter "context"

    #@0
    .prologue
    .line 118
    const-string v4, "image"

    #@2
    invoke-virtual {p0, v4}, Landroid/filterpacks/imageproc/RedEyeFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@5
    move-result-object v0

    #@6
    .line 119
    .local v0, input:Landroid/filterfw/core/Frame;
    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@9
    move-result-object v1

    #@a
    .line 122
    .local v1, inputFormat:Landroid/filterfw/core/FrameFormat;
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@d
    move-result-object v4

    #@e
    invoke-virtual {v4, v1}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@11
    move-result-object v3

    #@12
    .line 125
    .local v3, output:Landroid/filterfw/core/Frame;
    iget-object v4, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mProgram:Landroid/filterfw/core/Program;

    #@14
    if-eqz v4, :cond_1e

    #@16
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@19
    move-result v4

    #@1a
    iget v5, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mTarget:I

    #@1c
    if-eq v4, v5, :cond_25

    #@1e
    .line 126
    :cond_1e
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@21
    move-result v4

    #@22
    invoke-virtual {p0, p1, v4}, Landroid/filterpacks/imageproc/RedEyeFilter;->initProgram(Landroid/filterfw/core/FilterContext;I)V

    #@25
    .line 130
    :cond_25
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@28
    move-result v4

    #@29
    iget v5, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mWidth:I

    #@2b
    if-ne v4, v5, :cond_35

    #@2d
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@30
    move-result v4

    #@31
    iget v5, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mHeight:I

    #@33
    if-eq v4, v5, :cond_41

    #@35
    .line 131
    :cond_35
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@38
    move-result v4

    #@39
    iput v4, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mWidth:I

    #@3b
    .line 132
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@3e
    move-result v4

    #@3f
    iput v4, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mHeight:I

    #@41
    .line 134
    :cond_41
    invoke-direct {p0, p1}, Landroid/filterpacks/imageproc/RedEyeFilter;->createRedEyeFrame(Landroid/filterfw/core/FilterContext;)V

    #@44
    .line 137
    const/4 v4, 0x2

    #@45
    new-array v2, v4, [Landroid/filterfw/core/Frame;

    #@47
    const/4 v4, 0x0

    #@48
    aput-object v0, v2, v4

    #@4a
    const/4 v4, 0x1

    #@4b
    iget-object v5, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mRedEyeFrame:Landroid/filterfw/core/Frame;

    #@4d
    aput-object v5, v2, v4

    #@4f
    .line 138
    .local v2, inputs:[Landroid/filterfw/core/Frame;
    iget-object v4, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mProgram:Landroid/filterfw/core/Program;

    #@51
    invoke-virtual {v4, v2, v3}, Landroid/filterfw/core/Program;->process([Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@54
    .line 141
    const-string v4, "image"

    #@56
    invoke-virtual {p0, v4, v3}, Landroid/filterpacks/imageproc/RedEyeFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@59
    .line 144
    invoke-virtual {v3}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@5c
    .line 147
    iget-object v4, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mRedEyeFrame:Landroid/filterfw/core/Frame;

    #@5e
    invoke-virtual {v4}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@61
    .line 148
    const/4 v4, 0x0

    #@62
    iput-object v4, p0, Landroid/filterpacks/imageproc/RedEyeFilter;->mRedEyeFrame:Landroid/filterfw/core/Frame;

    #@64
    .line 149
    return-void
.end method

.method public setupPorts()V
    .registers 3

    #@0
    .prologue
    .line 91
    const-string v0, "image"

    #@2
    const/4 v1, 0x3

    #@3
    invoke-static {v1}, Landroid/filterfw/format/ImageFormat;->create(I)Landroid/filterfw/core/MutableFrameFormat;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/RedEyeFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@a
    .line 92
    const-string v0, "image"

    #@c
    const-string v1, "image"

    #@e
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/RedEyeFilter;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 93
    return-void
.end method
