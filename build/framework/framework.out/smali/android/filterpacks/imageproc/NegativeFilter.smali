.class public Landroid/filterpacks/imageproc/NegativeFilter;
.super Landroid/filterfw/core/Filter;
.source "NegativeFilter.java"


# instance fields
.field private final mNegativeShader:Ljava/lang/String;

.field private mProgram:Landroid/filterfw/core/Program;

.field private mTarget:I

.field private mTileSize:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "tile_size"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 50
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@3
    .line 34
    const/16 v0, 0x280

    #@5
    iput v0, p0, Landroid/filterpacks/imageproc/NegativeFilter;->mTileSize:I

    #@7
    .line 38
    const/4 v0, 0x0

    #@8
    iput v0, p0, Landroid/filterpacks/imageproc/NegativeFilter;->mTarget:I

    #@a
    .line 40
    const-string/jumbo v0, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  gl_FragColor = vec4(1.0 - color.rgb, color.a);\n}\n"

    #@d
    iput-object v0, p0, Landroid/filterpacks/imageproc/NegativeFilter;->mNegativeShader:Ljava/lang/String;

    #@f
    .line 51
    return-void
.end method


# virtual methods
.method public getOutputFormat(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .registers 3
    .parameter "portName"
    .parameter "inputFormat"

    #@0
    .prologue
    .line 61
    return-object p2
.end method

.method public initProgram(Landroid/filterfw/core/FilterContext;I)V
    .registers 7
    .parameter "context"
    .parameter "target"

    #@0
    .prologue
    .line 65
    packed-switch p2, :pswitch_data_34

    #@3
    .line 73
    new-instance v1, Ljava/lang/RuntimeException;

    #@5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v3, "Filter Sharpen does not support frames of target "

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    const-string v3, "!"

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@21
    throw v1

    #@22
    .line 67
    :pswitch_22
    new-instance v0, Landroid/filterfw/core/ShaderProgram;

    #@24
    const-string/jumbo v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  gl_FragColor = vec4(1.0 - color.rgb, color.a);\n}\n"

    #@27
    invoke-direct {v0, p1, v1}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;)V

    #@2a
    .line 68
    .local v0, shaderProgram:Landroid/filterfw/core/ShaderProgram;
    iget v1, p0, Landroid/filterpacks/imageproc/NegativeFilter;->mTileSize:I

    #@2c
    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setMaximumTileSize(I)V

    #@2f
    .line 69
    iput-object v0, p0, Landroid/filterpacks/imageproc/NegativeFilter;->mProgram:Landroid/filterfw/core/Program;

    #@31
    .line 76
    iput p2, p0, Landroid/filterpacks/imageproc/NegativeFilter;->mTarget:I

    #@33
    .line 77
    return-void

    #@34
    .line 65
    :pswitch_data_34
    .packed-switch 0x3
        :pswitch_22
    .end packed-switch
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 7
    .parameter "context"

    #@0
    .prologue
    .line 82
    const-string v3, "image"

    #@2
    invoke-virtual {p0, v3}, Landroid/filterpacks/imageproc/NegativeFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@5
    move-result-object v0

    #@6
    .line 83
    .local v0, input:Landroid/filterfw/core/Frame;
    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@9
    move-result-object v1

    #@a
    .line 86
    .local v1, inputFormat:Landroid/filterfw/core/FrameFormat;
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v3, v1}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@11
    move-result-object v2

    #@12
    .line 89
    .local v2, output:Landroid/filterfw/core/Frame;
    iget-object v3, p0, Landroid/filterpacks/imageproc/NegativeFilter;->mProgram:Landroid/filterfw/core/Program;

    #@14
    if-eqz v3, :cond_1e

    #@16
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@19
    move-result v3

    #@1a
    iget v4, p0, Landroid/filterpacks/imageproc/NegativeFilter;->mTarget:I

    #@1c
    if-eq v3, v4, :cond_25

    #@1e
    .line 90
    :cond_1e
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@21
    move-result v3

    #@22
    invoke-virtual {p0, p1, v3}, Landroid/filterpacks/imageproc/NegativeFilter;->initProgram(Landroid/filterfw/core/FilterContext;I)V

    #@25
    .line 94
    :cond_25
    iget-object v3, p0, Landroid/filterpacks/imageproc/NegativeFilter;->mProgram:Landroid/filterfw/core/Program;

    #@27
    invoke-virtual {v3, v0, v2}, Landroid/filterfw/core/Program;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@2a
    .line 97
    const-string v3, "image"

    #@2c
    invoke-virtual {p0, v3, v2}, Landroid/filterpacks/imageproc/NegativeFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@2f
    .line 100
    invoke-virtual {v2}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@32
    .line 101
    return-void
.end method

.method public setupPorts()V
    .registers 3

    #@0
    .prologue
    .line 55
    const-string v0, "image"

    #@2
    const/4 v1, 0x3

    #@3
    invoke-static {v1}, Landroid/filterfw/format/ImageFormat;->create(I)Landroid/filterfw/core/MutableFrameFormat;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/NegativeFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@a
    .line 56
    const-string v0, "image"

    #@c
    const-string v1, "image"

    #@e
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/NegativeFilter;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 57
    return-void
.end method
