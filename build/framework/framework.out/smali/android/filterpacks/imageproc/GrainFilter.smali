.class public Landroid/filterpacks/imageproc/GrainFilter;
.super Landroid/filterfw/core/Filter;
.source "GrainFilter.java"


# static fields
.field private static final RAND_THRESHOLD:I = 0x80


# instance fields
.field private mGrainProgram:Landroid/filterfw/core/Program;

.field private final mGrainShader:Ljava/lang/String;

.field private mHeight:I

.field private mNoiseProgram:Landroid/filterfw/core/Program;

.field private final mNoiseShader:Ljava/lang/String;

.field private mRandom:Ljava/util/Random;

.field private mScale:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "strength"
    .end annotation
.end field

.field private mTarget:I

.field private mTileSize:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "tile_size"
    .end annotation
.end field

.field private mWidth:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 5
    .parameter "name"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 97
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@4
    .line 40
    const/4 v0, 0x0

    #@5
    iput v0, p0, Landroid/filterpacks/imageproc/GrainFilter;->mScale:F

    #@7
    .line 43
    const/16 v0, 0x280

    #@9
    iput v0, p0, Landroid/filterpacks/imageproc/GrainFilter;->mTileSize:I

    #@b
    .line 49
    iput v1, p0, Landroid/filterpacks/imageproc/GrainFilter;->mWidth:I

    #@d
    .line 50
    iput v1, p0, Landroid/filterpacks/imageproc/GrainFilter;->mHeight:I

    #@f
    .line 51
    iput v1, p0, Landroid/filterpacks/imageproc/GrainFilter;->mTarget:I

    #@11
    .line 55
    const-string/jumbo v0, "precision mediump float;\nuniform vec2 seed;\nvarying vec2 v_texcoord;\nfloat rand(vec2 loc) {\n  float theta1 = dot(loc, vec2(0.9898, 0.233));\n  float theta2 = dot(loc, vec2(12.0, 78.0));\n  float value = cos(theta1) * sin(theta2) + sin(theta1) * cos(theta2);\n  float temp = mod(197.0 * value, 1.0) + value;\n  float part1 = mod(220.0 * temp, 1.0) + temp;\n  float part2 = value * 0.5453;\n  float part3 = cos(theta1 + theta2) * 0.43758;\n  return fract(part1 + part2 + part3);\n}\nvoid main() {\n  gl_FragColor = vec4(rand(v_texcoord + seed), 0.0, 0.0, 1.0);\n}\n"

    #@14
    iput-object v0, p0, Landroid/filterpacks/imageproc/GrainFilter;->mNoiseShader:Ljava/lang/String;

    #@16
    .line 74
    const-string/jumbo v0, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform float scale;\nuniform float stepX;\nuniform float stepY;\nvarying vec2 v_texcoord;\nvoid main() {\n  float noise = texture2D(tex_sampler_1, v_texcoord + vec2(-stepX, -stepY)).r * 0.224;\n  noise += texture2D(tex_sampler_1, v_texcoord + vec2(-stepX, stepY)).r * 0.224;\n  noise += texture2D(tex_sampler_1, v_texcoord + vec2(stepX, -stepY)).r * 0.224;\n  noise += texture2D(tex_sampler_1, v_texcoord + vec2(stepX, stepY)).r * 0.224;\n  noise += 0.4448;\n  noise *= scale;\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  float energy = 0.33333 * color.r + 0.33333 * color.g + 0.33333 * color.b;\n  float mask = (1.0 - sqrt(energy));\n  float weight = 1.0 - 1.333 * mask * noise;\n  gl_FragColor = vec4(color.rgb * weight, color.a);\n}\n"

    #@19
    iput-object v0, p0, Landroid/filterpacks/imageproc/GrainFilter;->mGrainShader:Ljava/lang/String;

    #@1b
    .line 98
    new-instance v0, Ljava/util/Random;

    #@1d
    new-instance v1, Ljava/util/Date;

    #@1f
    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    #@22
    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    #@25
    move-result-wide v1

    #@26
    invoke-direct {v0, v1, v2}, Ljava/util/Random;-><init>(J)V

    #@29
    iput-object v0, p0, Landroid/filterpacks/imageproc/GrainFilter;->mRandom:Ljava/util/Random;

    #@2b
    .line 99
    return-void
.end method

.method private updateFrameSize(II)V
    .registers 7
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    const/high16 v3, 0x3f00

    #@2
    .line 139
    iput p1, p0, Landroid/filterpacks/imageproc/GrainFilter;->mWidth:I

    #@4
    .line 140
    iput p2, p0, Landroid/filterpacks/imageproc/GrainFilter;->mHeight:I

    #@6
    .line 142
    iget-object v0, p0, Landroid/filterpacks/imageproc/GrainFilter;->mGrainProgram:Landroid/filterfw/core/Program;

    #@8
    if-eqz v0, :cond_2f

    #@a
    .line 143
    iget-object v0, p0, Landroid/filterpacks/imageproc/GrainFilter;->mGrainProgram:Landroid/filterfw/core/Program;

    #@c
    const-string/jumbo v1, "stepX"

    #@f
    iget v2, p0, Landroid/filterpacks/imageproc/GrainFilter;->mWidth:I

    #@11
    int-to-float v2, v2

    #@12
    div-float v2, v3, v2

    #@14
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/Program;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@1b
    .line 144
    iget-object v0, p0, Landroid/filterpacks/imageproc/GrainFilter;->mGrainProgram:Landroid/filterfw/core/Program;

    #@1d
    const-string/jumbo v1, "stepY"

    #@20
    iget v2, p0, Landroid/filterpacks/imageproc/GrainFilter;->mHeight:I

    #@22
    int-to-float v2, v2

    #@23
    div-float v2, v3, v2

    #@25
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/Program;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@2c
    .line 145
    invoke-direct {p0}, Landroid/filterpacks/imageproc/GrainFilter;->updateParameters()V

    #@2f
    .line 147
    :cond_2f
    return-void
.end method

.method private updateParameters()V
    .registers 5

    #@0
    .prologue
    .line 132
    const/4 v1, 0x2

    #@1
    new-array v0, v1, [F

    #@3
    const/4 v1, 0x0

    #@4
    iget-object v2, p0, Landroid/filterpacks/imageproc/GrainFilter;->mRandom:Ljava/util/Random;

    #@6
    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    #@9
    move-result v2

    #@a
    aput v2, v0, v1

    #@c
    const/4 v1, 0x1

    #@d
    iget-object v2, p0, Landroid/filterpacks/imageproc/GrainFilter;->mRandom:Ljava/util/Random;

    #@f
    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    #@12
    move-result v2

    #@13
    aput v2, v0, v1

    #@15
    .line 133
    .local v0, seed:[F
    iget-object v1, p0, Landroid/filterpacks/imageproc/GrainFilter;->mNoiseProgram:Landroid/filterfw/core/Program;

    #@17
    const-string/jumbo v2, "seed"

    #@1a
    invoke-virtual {v1, v2, v0}, Landroid/filterfw/core/Program;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@1d
    .line 135
    iget-object v1, p0, Landroid/filterpacks/imageproc/GrainFilter;->mGrainProgram:Landroid/filterfw/core/Program;

    #@1f
    const-string/jumbo v2, "scale"

    #@22
    iget v3, p0, Landroid/filterpacks/imageproc/GrainFilter;->mScale:F

    #@24
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v1, v2, v3}, Landroid/filterfw/core/Program;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@2b
    .line 136
    return-void
.end method


# virtual methods
.method public fieldPortValueUpdated(Ljava/lang/String;Landroid/filterfw/core/FilterContext;)V
    .registers 4
    .parameter "name"
    .parameter "context"

    #@0
    .prologue
    .line 151
    iget-object v0, p0, Landroid/filterpacks/imageproc/GrainFilter;->mGrainProgram:Landroid/filterfw/core/Program;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/filterpacks/imageproc/GrainFilter;->mNoiseProgram:Landroid/filterfw/core/Program;

    #@6
    if-eqz v0, :cond_b

    #@8
    .line 152
    invoke-direct {p0}, Landroid/filterpacks/imageproc/GrainFilter;->updateParameters()V

    #@b
    .line 154
    :cond_b
    return-void
.end method

.method public getOutputFormat(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .registers 3
    .parameter "portName"
    .parameter "inputFormat"

    #@0
    .prologue
    .line 109
    return-object p2
.end method

.method public initProgram(Landroid/filterfw/core/FilterContext;I)V
    .registers 7
    .parameter "context"
    .parameter "target"

    #@0
    .prologue
    .line 113
    packed-switch p2, :pswitch_data_44

    #@3
    .line 125
    new-instance v1, Ljava/lang/RuntimeException;

    #@5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v3, "Filter Sharpen does not support frames of target "

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    const-string v3, "!"

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@21
    throw v1

    #@22
    .line 115
    :pswitch_22
    new-instance v0, Landroid/filterfw/core/ShaderProgram;

    #@24
    const-string/jumbo v1, "precision mediump float;\nuniform vec2 seed;\nvarying vec2 v_texcoord;\nfloat rand(vec2 loc) {\n  float theta1 = dot(loc, vec2(0.9898, 0.233));\n  float theta2 = dot(loc, vec2(12.0, 78.0));\n  float value = cos(theta1) * sin(theta2) + sin(theta1) * cos(theta2);\n  float temp = mod(197.0 * value, 1.0) + value;\n  float part1 = mod(220.0 * temp, 1.0) + temp;\n  float part2 = value * 0.5453;\n  float part3 = cos(theta1 + theta2) * 0.43758;\n  return fract(part1 + part2 + part3);\n}\nvoid main() {\n  gl_FragColor = vec4(rand(v_texcoord + seed), 0.0, 0.0, 1.0);\n}\n"

    #@27
    invoke-direct {v0, p1, v1}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;)V

    #@2a
    .line 116
    .local v0, shaderProgram:Landroid/filterfw/core/ShaderProgram;
    iget v1, p0, Landroid/filterpacks/imageproc/GrainFilter;->mTileSize:I

    #@2c
    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setMaximumTileSize(I)V

    #@2f
    .line 117
    iput-object v0, p0, Landroid/filterpacks/imageproc/GrainFilter;->mNoiseProgram:Landroid/filterfw/core/Program;

    #@31
    .line 119
    new-instance v0, Landroid/filterfw/core/ShaderProgram;

    #@33
    .end local v0           #shaderProgram:Landroid/filterfw/core/ShaderProgram;
    const-string/jumbo v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform float scale;\nuniform float stepX;\nuniform float stepY;\nvarying vec2 v_texcoord;\nvoid main() {\n  float noise = texture2D(tex_sampler_1, v_texcoord + vec2(-stepX, -stepY)).r * 0.224;\n  noise += texture2D(tex_sampler_1, v_texcoord + vec2(-stepX, stepY)).r * 0.224;\n  noise += texture2D(tex_sampler_1, v_texcoord + vec2(stepX, -stepY)).r * 0.224;\n  noise += texture2D(tex_sampler_1, v_texcoord + vec2(stepX, stepY)).r * 0.224;\n  noise += 0.4448;\n  noise *= scale;\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  float energy = 0.33333 * color.r + 0.33333 * color.g + 0.33333 * color.b;\n  float mask = (1.0 - sqrt(energy));\n  float weight = 1.0 - 1.333 * mask * noise;\n  gl_FragColor = vec4(color.rgb * weight, color.a);\n}\n"

    #@36
    invoke-direct {v0, p1, v1}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;)V

    #@39
    .line 120
    .restart local v0       #shaderProgram:Landroid/filterfw/core/ShaderProgram;
    iget v1, p0, Landroid/filterpacks/imageproc/GrainFilter;->mTileSize:I

    #@3b
    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setMaximumTileSize(I)V

    #@3e
    .line 121
    iput-object v0, p0, Landroid/filterpacks/imageproc/GrainFilter;->mGrainProgram:Landroid/filterfw/core/Program;

    #@40
    .line 128
    iput p2, p0, Landroid/filterpacks/imageproc/GrainFilter;->mTarget:I

    #@42
    .line 129
    return-void

    #@43
    .line 113
    nop

    #@44
    :pswitch_data_44
    .packed-switch 0x3
        :pswitch_22
    .end packed-switch
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 13
    .parameter "context"

    #@0
    .prologue
    const/4 v10, 0x3

    #@1
    const/4 v9, 0x0

    #@2
    .line 159
    const-string v7, "image"

    #@4
    invoke-virtual {p0, v7}, Landroid/filterpacks/imageproc/GrainFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@7
    move-result-object v1

    #@8
    .line 160
    .local v1, input:Landroid/filterfw/core/Frame;
    invoke-virtual {v1}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@b
    move-result-object v2

    #@c
    .line 162
    .local v2, inputFormat:Landroid/filterfw/core/FrameFormat;
    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@f
    move-result v7

    #@10
    div-int/lit8 v7, v7, 0x2

    #@12
    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@15
    move-result v8

    #@16
    div-int/lit8 v8, v8, 0x2

    #@18
    invoke-static {v7, v8, v10, v10}, Landroid/filterfw/format/ImageFormat;->create(IIII)Landroid/filterfw/core/MutableFrameFormat;

    #@1b
    move-result-object v4

    #@1c
    .line 168
    .local v4, noiseFormat:Landroid/filterfw/core/FrameFormat;
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@1f
    move-result-object v7

    #@20
    invoke-virtual {v7, v2}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@23
    move-result-object v5

    #@24
    .line 171
    .local v5, noiseFrame:Landroid/filterfw/core/Frame;
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@27
    move-result-object v7

    #@28
    invoke-virtual {v7, v2}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@2b
    move-result-object v6

    #@2c
    .line 174
    .local v6, output:Landroid/filterfw/core/Frame;
    iget-object v7, p0, Landroid/filterpacks/imageproc/GrainFilter;->mNoiseProgram:Landroid/filterfw/core/Program;

    #@2e
    if-eqz v7, :cond_3c

    #@30
    iget-object v7, p0, Landroid/filterpacks/imageproc/GrainFilter;->mGrainProgram:Landroid/filterfw/core/Program;

    #@32
    if-eqz v7, :cond_3c

    #@34
    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@37
    move-result v7

    #@38
    iget v8, p0, Landroid/filterpacks/imageproc/GrainFilter;->mTarget:I

    #@3a
    if-eq v7, v8, :cond_46

    #@3c
    .line 175
    :cond_3c
    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@3f
    move-result v7

    #@40
    invoke-virtual {p0, p1, v7}, Landroid/filterpacks/imageproc/GrainFilter;->initProgram(Landroid/filterfw/core/FilterContext;I)V

    #@43
    .line 176
    invoke-direct {p0}, Landroid/filterpacks/imageproc/GrainFilter;->updateParameters()V

    #@46
    .line 180
    :cond_46
    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@49
    move-result v7

    #@4a
    iget v8, p0, Landroid/filterpacks/imageproc/GrainFilter;->mWidth:I

    #@4c
    if-ne v7, v8, :cond_56

    #@4e
    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@51
    move-result v7

    #@52
    iget v8, p0, Landroid/filterpacks/imageproc/GrainFilter;->mHeight:I

    #@54
    if-eq v7, v8, :cond_61

    #@56
    .line 181
    :cond_56
    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@59
    move-result v7

    #@5a
    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@5d
    move-result v8

    #@5e
    invoke-direct {p0, v7, v8}, Landroid/filterpacks/imageproc/GrainFilter;->updateFrameSize(II)V

    #@61
    .line 184
    :cond_61
    new-array v0, v9, [Landroid/filterfw/core/Frame;

    #@63
    .line 185
    .local v0, empty:[Landroid/filterfw/core/Frame;
    iget-object v7, p0, Landroid/filterpacks/imageproc/GrainFilter;->mNoiseProgram:Landroid/filterfw/core/Program;

    #@65
    invoke-virtual {v7, v0, v5}, Landroid/filterfw/core/Program;->process([Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@68
    .line 188
    const/4 v7, 0x2

    #@69
    new-array v3, v7, [Landroid/filterfw/core/Frame;

    #@6b
    aput-object v1, v3, v9

    #@6d
    const/4 v7, 0x1

    #@6e
    aput-object v5, v3, v7

    #@70
    .line 189
    .local v3, inputs:[Landroid/filterfw/core/Frame;
    iget-object v7, p0, Landroid/filterpacks/imageproc/GrainFilter;->mGrainProgram:Landroid/filterfw/core/Program;

    #@72
    invoke-virtual {v7, v3, v6}, Landroid/filterfw/core/Program;->process([Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@75
    .line 192
    const-string v7, "image"

    #@77
    invoke-virtual {p0, v7, v6}, Landroid/filterpacks/imageproc/GrainFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@7a
    .line 195
    invoke-virtual {v6}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@7d
    .line 196
    invoke-virtual {v5}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@80
    .line 197
    return-void
.end method

.method public setupPorts()V
    .registers 3

    #@0
    .prologue
    .line 103
    const-string v0, "image"

    #@2
    const/4 v1, 0x3

    #@3
    invoke-static {v1}, Landroid/filterfw/format/ImageFormat;->create(I)Landroid/filterfw/core/MutableFrameFormat;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/GrainFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@a
    .line 104
    const-string v0, "image"

    #@c
    const-string v1, "image"

    #@e
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/GrainFilter;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 105
    return-void
.end method
