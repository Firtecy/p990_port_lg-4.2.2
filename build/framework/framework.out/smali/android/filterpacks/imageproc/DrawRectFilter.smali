.class public Landroid/filterpacks/imageproc/DrawRectFilter;
.super Landroid/filterfw/core/Filter;
.source "DrawRectFilter.java"


# instance fields
.field private mColorBlue:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "colorBlue"
    .end annotation
.end field

.field private mColorGreen:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "colorGreen"
    .end annotation
.end field

.field private mColorRed:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "colorRed"
    .end annotation
.end field

.field private final mFixedColorFragmentShader:Ljava/lang/String;

.field private mProgram:Landroid/filterfw/core/ShaderProgram;

.field private final mVertexShader:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "name"

    #@0
    .prologue
    const v0, 0x3f4ccccd

    #@3
    .line 67
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@6
    .line 41
    iput v0, p0, Landroid/filterpacks/imageproc/DrawRectFilter;->mColorRed:F

    #@8
    .line 44
    iput v0, p0, Landroid/filterpacks/imageproc/DrawRectFilter;->mColorGreen:F

    #@a
    .line 47
    const/4 v0, 0x0

    #@b
    iput v0, p0, Landroid/filterpacks/imageproc/DrawRectFilter;->mColorBlue:F

    #@d
    .line 50
    const-string v0, "attribute vec4 aPosition;\nvoid main() {\n  gl_Position = aPosition;\n}\n"

    #@f
    iput-object v0, p0, Landroid/filterpacks/imageproc/DrawRectFilter;->mVertexShader:Ljava/lang/String;

    #@11
    .line 56
    const-string/jumbo v0, "precision mediump float;\nuniform vec4 color;\nvoid main() {\n  gl_FragColor = color;\n}\n"

    #@14
    iput-object v0, p0, Landroid/filterpacks/imageproc/DrawRectFilter;->mFixedColorFragmentShader:Ljava/lang/String;

    #@16
    .line 68
    return-void
.end method

.method private renderBox(Landroid/filterfw/geometry/Quad;)V
    .registers 11
    .parameter "box"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/high16 v8, 0x3f80

    #@3
    const/4 v7, 0x0

    #@4
    const/4 v6, 0x4

    #@5
    const/4 v5, 0x2

    #@6
    .line 113
    const/4 v0, 0x4

    #@7
    .line 116
    .local v0, FLOAT_SIZE:I
    new-array v1, v6, [F

    #@9
    iget v3, p0, Landroid/filterpacks/imageproc/DrawRectFilter;->mColorRed:F

    #@b
    aput v3, v1, v7

    #@d
    iget v3, p0, Landroid/filterpacks/imageproc/DrawRectFilter;->mColorGreen:F

    #@f
    aput v3, v1, v4

    #@11
    iget v3, p0, Landroid/filterpacks/imageproc/DrawRectFilter;->mColorBlue:F

    #@13
    aput v3, v1, v5

    #@15
    const/4 v3, 0x3

    #@16
    aput v8, v1, v3

    #@18
    .line 117
    .local v1, color:[F
    const/16 v3, 0x8

    #@1a
    new-array v2, v3, [F

    #@1c
    iget-object v3, p1, Landroid/filterfw/geometry/Quad;->p0:Landroid/filterfw/geometry/Point;

    #@1e
    iget v3, v3, Landroid/filterfw/geometry/Point;->x:F

    #@20
    aput v3, v2, v7

    #@22
    iget-object v3, p1, Landroid/filterfw/geometry/Quad;->p0:Landroid/filterfw/geometry/Point;

    #@24
    iget v3, v3, Landroid/filterfw/geometry/Point;->y:F

    #@26
    aput v3, v2, v4

    #@28
    iget-object v3, p1, Landroid/filterfw/geometry/Quad;->p1:Landroid/filterfw/geometry/Point;

    #@2a
    iget v3, v3, Landroid/filterfw/geometry/Point;->x:F

    #@2c
    aput v3, v2, v5

    #@2e
    const/4 v3, 0x3

    #@2f
    iget-object v4, p1, Landroid/filterfw/geometry/Quad;->p1:Landroid/filterfw/geometry/Point;

    #@31
    iget v4, v4, Landroid/filterfw/geometry/Point;->y:F

    #@33
    aput v4, v2, v3

    #@35
    iget-object v3, p1, Landroid/filterfw/geometry/Quad;->p3:Landroid/filterfw/geometry/Point;

    #@37
    iget v3, v3, Landroid/filterfw/geometry/Point;->x:F

    #@39
    aput v3, v2, v6

    #@3b
    const/4 v3, 0x5

    #@3c
    iget-object v4, p1, Landroid/filterfw/geometry/Quad;->p3:Landroid/filterfw/geometry/Point;

    #@3e
    iget v4, v4, Landroid/filterfw/geometry/Point;->y:F

    #@40
    aput v4, v2, v3

    #@42
    const/4 v3, 0x6

    #@43
    iget-object v4, p1, Landroid/filterfw/geometry/Quad;->p2:Landroid/filterfw/geometry/Point;

    #@45
    iget v4, v4, Landroid/filterfw/geometry/Point;->x:F

    #@47
    aput v4, v2, v3

    #@49
    const/4 v3, 0x7

    #@4a
    iget-object v4, p1, Landroid/filterfw/geometry/Quad;->p2:Landroid/filterfw/geometry/Point;

    #@4c
    iget v4, v4, Landroid/filterfw/geometry/Point;->y:F

    #@4e
    aput v4, v2, v3

    #@50
    .line 123
    .local v2, vertexValues:[F
    iget-object v3, p0, Landroid/filterpacks/imageproc/DrawRectFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@52
    const-string v4, "color"

    #@54
    invoke-virtual {v3, v4, v1}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@57
    .line 124
    iget-object v3, p0, Landroid/filterpacks/imageproc/DrawRectFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@59
    const-string v4, "aPosition"

    #@5b
    invoke-virtual {v3, v4, v2, v5}, Landroid/filterfw/core/ShaderProgram;->setAttributeValues(Ljava/lang/String;[FI)V

    #@5e
    .line 125
    iget-object v3, p0, Landroid/filterpacks/imageproc/DrawRectFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@60
    invoke-virtual {v3, v6}, Landroid/filterfw/core/ShaderProgram;->setVertexCount(I)V

    #@63
    .line 128
    iget-object v3, p0, Landroid/filterpacks/imageproc/DrawRectFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@65
    invoke-virtual {v3}, Landroid/filterfw/core/ShaderProgram;->beginDrawing()V

    #@68
    .line 129
    invoke-static {v8}, Landroid/opengl/GLES20;->glLineWidth(F)V

    #@6b
    .line 130
    invoke-static {v5, v7, v6}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    #@6e
    .line 131
    return-void
.end method


# virtual methods
.method public getOutputFormat(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .registers 3
    .parameter "portName"
    .parameter "inputFormat"

    #@0
    .prologue
    .line 80
    return-object p2
.end method

.method public prepare(Landroid/filterfw/core/FilterContext;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 85
    new-instance v0, Landroid/filterfw/core/ShaderProgram;

    #@2
    const-string v1, "attribute vec4 aPosition;\nvoid main() {\n  gl_Position = aPosition;\n}\n"

    #@4
    const-string/jumbo v2, "precision mediump float;\nuniform vec4 color;\nvoid main() {\n  gl_FragColor = color;\n}\n"

    #@7
    invoke-direct {v0, p1, v1, v2}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;Ljava/lang/String;)V

    #@a
    iput-object v0, p0, Landroid/filterpacks/imageproc/DrawRectFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@c
    .line 86
    return-void
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 8
    .parameter "env"

    #@0
    .prologue
    const/high16 v5, -0x4080

    #@2
    .line 91
    const-string v4, "image"

    #@4
    invoke-virtual {p0, v4}, Landroid/filterpacks/imageproc/DrawRectFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@7
    move-result-object v2

    #@8
    .line 92
    .local v2, imageFrame:Landroid/filterfw/core/Frame;
    const-string v4, "box"

    #@a
    invoke-virtual {p0, v4}, Landroid/filterpacks/imageproc/DrawRectFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@d
    move-result-object v1

    #@e
    .line 95
    .local v1, boxFrame:Landroid/filterfw/core/Frame;
    invoke-virtual {v1}, Landroid/filterfw/core/Frame;->getObjectValue()Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Landroid/filterfw/geometry/Quad;

    #@14
    .line 96
    .local v0, box:Landroid/filterfw/geometry/Quad;
    const/high16 v4, 0x4000

    #@16
    invoke-virtual {v0, v4}, Landroid/filterfw/geometry/Quad;->scaled(F)Landroid/filterfw/geometry/Quad;

    #@19
    move-result-object v4

    #@1a
    invoke-virtual {v4, v5, v5}, Landroid/filterfw/geometry/Quad;->translated(FF)Landroid/filterfw/geometry/Quad;

    #@1d
    move-result-object v0

    #@1e
    .line 99
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@21
    move-result-object v4

    #@22
    invoke-virtual {v4, v2}, Landroid/filterfw/core/FrameManager;->duplicateFrame(Landroid/filterfw/core/Frame;)Landroid/filterfw/core/Frame;

    #@25
    move-result-object v3

    #@26
    check-cast v3, Landroid/filterfw/core/GLFrame;

    #@28
    .line 102
    .local v3, output:Landroid/filterfw/core/GLFrame;
    invoke-virtual {v3}, Landroid/filterfw/core/GLFrame;->focus()V

    #@2b
    .line 103
    invoke-direct {p0, v0}, Landroid/filterpacks/imageproc/DrawRectFilter;->renderBox(Landroid/filterfw/geometry/Quad;)V

    #@2e
    .line 106
    const-string v4, "image"

    #@30
    invoke-virtual {p0, v4, v3}, Landroid/filterpacks/imageproc/DrawRectFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@33
    .line 109
    invoke-virtual {v3}, Landroid/filterfw/core/GLFrame;->release()Landroid/filterfw/core/Frame;

    #@36
    .line 110
    return-void
.end method

.method public setupPorts()V
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x3

    #@1
    .line 72
    const-string v0, "image"

    #@3
    invoke-static {v1, v1}, Landroid/filterfw/format/ImageFormat;->create(II)Landroid/filterfw/core/MutableFrameFormat;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/DrawRectFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@a
    .line 74
    const-string v0, "box"

    #@c
    const-class v1, Landroid/filterfw/geometry/Quad;

    #@e
    const/4 v2, 0x1

    #@f
    invoke-static {v1, v2}, Landroid/filterfw/format/ObjectFormat;->fromClass(Ljava/lang/Class;I)Landroid/filterfw/core/MutableFrameFormat;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/DrawRectFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@16
    .line 75
    const-string v0, "image"

    #@18
    const-string v1, "image"

    #@1a
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/DrawRectFilter;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    .line 76
    return-void
.end method
