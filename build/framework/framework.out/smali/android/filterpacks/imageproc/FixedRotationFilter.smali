.class public Landroid/filterpacks/imageproc/FixedRotationFilter;
.super Landroid/filterfw/core/Filter;
.source "FixedRotationFilter.java"


# instance fields
.field private mProgram:Landroid/filterfw/core/ShaderProgram;

.field private mRotation:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "rotation"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 44
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@3
    .line 38
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/filterpacks/imageproc/FixedRotationFilter;->mRotation:I

    #@6
    .line 41
    const/4 v0, 0x0

    #@7
    iput-object v0, p0, Landroid/filterpacks/imageproc/FixedRotationFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@9
    .line 45
    return-void
.end method


# virtual methods
.method public getOutputFormat(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .registers 3
    .parameter "portName"
    .parameter "inputFormat"

    #@0
    .prologue
    .line 56
    return-object p2
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 16
    .parameter "context"

    #@0
    .prologue
    const/high16 v13, 0x3f80

    #@2
    const/4 v12, 0x0

    #@3
    .line 61
    const-string v11, "image"

    #@5
    invoke-virtual {p0, v11}, Landroid/filterpacks/imageproc/FixedRotationFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@8
    move-result-object v1

    #@9
    .line 62
    .local v1, input:Landroid/filterfw/core/Frame;
    iget v11, p0, Landroid/filterpacks/imageproc/FixedRotationFilter;->mRotation:I

    #@b
    if-nez v11, :cond_13

    #@d
    .line 63
    const-string v11, "image"

    #@f
    invoke-virtual {p0, v11, v1}, Landroid/filterpacks/imageproc/FixedRotationFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@12
    .line 111
    :goto_12
    return-void

    #@13
    .line 66
    :cond_13
    invoke-virtual {v1}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@16
    move-result-object v2

    #@17
    .line 69
    .local v2, inputFormat:Landroid/filterfw/core/FrameFormat;
    iget-object v11, p0, Landroid/filterpacks/imageproc/FixedRotationFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@19
    if-nez v11, :cond_21

    #@1b
    .line 70
    invoke-static {p1}, Landroid/filterfw/core/ShaderProgram;->createIdentity(Landroid/filterfw/core/FilterContext;)Landroid/filterfw/core/ShaderProgram;

    #@1e
    move-result-object v11

    #@1f
    iput-object v11, p0, Landroid/filterpacks/imageproc/FixedRotationFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@21
    .line 72
    :cond_21
    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->mutableCopy()Landroid/filterfw/core/MutableFrameFormat;

    #@24
    move-result-object v4

    #@25
    .line 73
    .local v4, outputFormat:Landroid/filterfw/core/MutableFrameFormat;
    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@28
    move-result v10

    #@29
    .line 74
    .local v10, width:I
    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@2c
    move-result v0

    #@2d
    .line 75
    .local v0, height:I
    new-instance v5, Landroid/filterfw/geometry/Point;

    #@2f
    invoke-direct {v5, v12, v12}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    #@32
    .line 76
    .local v5, p1:Landroid/filterfw/geometry/Point;
    new-instance v6, Landroid/filterfw/geometry/Point;

    #@34
    invoke-direct {v6, v13, v12}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    #@37
    .line 77
    .local v6, p2:Landroid/filterfw/geometry/Point;
    new-instance v7, Landroid/filterfw/geometry/Point;

    #@39
    invoke-direct {v7, v12, v13}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    #@3c
    .line 78
    .local v7, p3:Landroid/filterfw/geometry/Point;
    new-instance v8, Landroid/filterfw/geometry/Point;

    #@3e
    invoke-direct {v8, v13, v13}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    #@41
    .line 80
    .local v8, p4:Landroid/filterfw/geometry/Point;
    iget v11, p0, Landroid/filterpacks/imageproc/FixedRotationFilter;->mRotation:I

    #@43
    int-to-float v11, v11

    #@44
    const/high16 v12, 0x42b4

    #@46
    div-float/2addr v11, v12

    #@47
    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    #@4a
    move-result v11

    #@4b
    rem-int/lit8 v11, v11, 0x4

    #@4d
    packed-switch v11, :pswitch_data_88

    #@50
    .line 94
    new-instance v9, Landroid/filterfw/geometry/Quad;

    #@52
    invoke-direct {v9, v5, v6, v7, v8}, Landroid/filterfw/geometry/Quad;-><init>(Landroid/filterfw/geometry/Point;Landroid/filterfw/geometry/Point;Landroid/filterfw/geometry/Point;Landroid/filterfw/geometry/Point;)V

    #@55
    .line 98
    .local v9, sourceRegion:Landroid/filterfw/geometry/Quad;
    :goto_55
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@58
    move-result-object v11

    #@59
    invoke-virtual {v11, v4}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@5c
    move-result-object v3

    #@5d
    .line 101
    .local v3, output:Landroid/filterfw/core/Frame;
    iget-object v11, p0, Landroid/filterpacks/imageproc/FixedRotationFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@5f
    invoke-virtual {v11, v9}, Landroid/filterfw/core/ShaderProgram;->setSourceRegion(Landroid/filterfw/geometry/Quad;)V

    #@62
    .line 104
    iget-object v11, p0, Landroid/filterpacks/imageproc/FixedRotationFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@64
    invoke-virtual {v11, v1, v3}, Landroid/filterfw/core/ShaderProgram;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@67
    .line 107
    const-string v11, "image"

    #@69
    invoke-virtual {p0, v11, v3}, Landroid/filterpacks/imageproc/FixedRotationFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@6c
    .line 110
    invoke-virtual {v3}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@6f
    goto :goto_12

    #@70
    .line 82
    .end local v3           #output:Landroid/filterfw/core/Frame;
    .end local v9           #sourceRegion:Landroid/filterfw/geometry/Quad;
    :pswitch_70
    new-instance v9, Landroid/filterfw/geometry/Quad;

    #@72
    invoke-direct {v9, v7, v5, v8, v6}, Landroid/filterfw/geometry/Quad;-><init>(Landroid/filterfw/geometry/Point;Landroid/filterfw/geometry/Point;Landroid/filterfw/geometry/Point;Landroid/filterfw/geometry/Point;)V

    #@75
    .line 83
    .restart local v9       #sourceRegion:Landroid/filterfw/geometry/Quad;
    invoke-virtual {v4, v0, v10}, Landroid/filterfw/core/MutableFrameFormat;->setDimensions(II)V

    #@78
    goto :goto_55

    #@79
    .line 86
    .end local v9           #sourceRegion:Landroid/filterfw/geometry/Quad;
    :pswitch_79
    new-instance v9, Landroid/filterfw/geometry/Quad;

    #@7b
    invoke-direct {v9, v8, v7, v6, v5}, Landroid/filterfw/geometry/Quad;-><init>(Landroid/filterfw/geometry/Point;Landroid/filterfw/geometry/Point;Landroid/filterfw/geometry/Point;Landroid/filterfw/geometry/Point;)V

    #@7e
    .line 87
    .restart local v9       #sourceRegion:Landroid/filterfw/geometry/Quad;
    goto :goto_55

    #@7f
    .line 89
    .end local v9           #sourceRegion:Landroid/filterfw/geometry/Quad;
    :pswitch_7f
    new-instance v9, Landroid/filterfw/geometry/Quad;

    #@81
    invoke-direct {v9, v6, v8, v5, v7}, Landroid/filterfw/geometry/Quad;-><init>(Landroid/filterfw/geometry/Point;Landroid/filterfw/geometry/Point;Landroid/filterfw/geometry/Point;Landroid/filterfw/geometry/Point;)V

    #@84
    .line 90
    .restart local v9       #sourceRegion:Landroid/filterfw/geometry/Quad;
    invoke-virtual {v4, v0, v10}, Landroid/filterfw/core/MutableFrameFormat;->setDimensions(II)V

    #@87
    goto :goto_55

    #@88
    .line 80
    :pswitch_data_88
    .packed-switch 0x1
        :pswitch_70
        :pswitch_79
        :pswitch_7f
    .end packed-switch
.end method

.method public setupPorts()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x3

    #@1
    .line 49
    const-string v0, "image"

    #@3
    invoke-static {v1, v1}, Landroid/filterfw/format/ImageFormat;->create(II)Landroid/filterfw/core/MutableFrameFormat;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/FixedRotationFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@a
    .line 51
    const-string v0, "image"

    #@c
    const-string v1, "image"

    #@e
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/FixedRotationFilter;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 52
    return-void
.end method
