.class public Landroid/filterpacks/imageproc/FisheyeFilter;
.super Landroid/filterfw/core/Filter;
.source "FisheyeFilter.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "FisheyeFilter"

.field private static final mFisheyeShader:Ljava/lang/String; = "precision highp float;\nuniform sampler2D tex_sampler_0;\nuniform vec2 scale;\nuniform float alpha;\nuniform float radius2;\nuniform float factor;\nvarying vec2 v_texcoord;\nvoid main() {\n  const float m_pi_2 = 1.570963;\n  vec2 coord = v_texcoord - vec2(0.5, 0.5);\n  float dist = length(coord * scale);\n  float radian = m_pi_2 - atan(alpha * sqrt(radius2 - dist * dist), dist);\n  float scalar = radian * factor / dist;\n  vec2 new_coord = coord * scalar + vec2(0.5, 0.5);\n  gl_FragColor = texture2D(tex_sampler_0, new_coord);\n}\n"


# instance fields
.field private mHeight:I

.field private mProgram:Landroid/filterfw/core/Program;

.field private mScale:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "scale"
    .end annotation
.end field

.field private mTarget:I

.field private mTileSize:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "tile_size"
    .end annotation
.end field

.field private mWidth:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter "name"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 79
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@4
    .line 46
    const/4 v0, 0x0

    #@5
    iput v0, p0, Landroid/filterpacks/imageproc/FisheyeFilter;->mScale:F

    #@7
    .line 49
    const/16 v0, 0x280

    #@9
    iput v0, p0, Landroid/filterpacks/imageproc/FisheyeFilter;->mTileSize:I

    #@b
    .line 54
    iput v1, p0, Landroid/filterpacks/imageproc/FisheyeFilter;->mWidth:I

    #@d
    .line 55
    iput v1, p0, Landroid/filterpacks/imageproc/FisheyeFilter;->mHeight:I

    #@f
    .line 56
    iput v1, p0, Landroid/filterpacks/imageproc/FisheyeFilter;->mTarget:I

    #@11
    .line 80
    return-void
.end method

.method private updateFrameSize(II)V
    .registers 3
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 145
    iput p1, p0, Landroid/filterpacks/imageproc/FisheyeFilter;->mWidth:I

    #@2
    .line 146
    iput p2, p0, Landroid/filterpacks/imageproc/FisheyeFilter;->mHeight:I

    #@4
    .line 148
    invoke-direct {p0}, Landroid/filterpacks/imageproc/FisheyeFilter;->updateProgramParams()V

    #@7
    .line 149
    return-void
.end method

.method private updateProgramParams()V
    .registers 15

    #@0
    .prologue
    const/high16 v13, 0x3f80

    #@2
    const/4 v12, 0x1

    #@3
    const/4 v11, 0x0

    #@4
    .line 152
    const v5, 0x40490fdb

    #@7
    .line 153
    .local v5, pi:F
    const/4 v9, 0x2

    #@8
    new-array v8, v9, [F

    #@a
    .line 154
    .local v8, scale:[F
    iget v9, p0, Landroid/filterpacks/imageproc/FisheyeFilter;->mWidth:I

    #@c
    iget v10, p0, Landroid/filterpacks/imageproc/FisheyeFilter;->mHeight:I

    #@e
    if-le v9, v10, :cond_83

    #@10
    .line 155
    aput v13, v8, v11

    #@12
    .line 156
    iget v9, p0, Landroid/filterpacks/imageproc/FisheyeFilter;->mHeight:I

    #@14
    int-to-float v9, v9

    #@15
    iget v10, p0, Landroid/filterpacks/imageproc/FisheyeFilter;->mWidth:I

    #@17
    int-to-float v10, v10

    #@18
    div-float/2addr v9, v10

    #@19
    aput v9, v8, v12

    #@1b
    .line 161
    :goto_1b
    iget v9, p0, Landroid/filterpacks/imageproc/FisheyeFilter;->mScale:F

    #@1d
    const/high16 v10, 0x4000

    #@1f
    mul-float/2addr v9, v10

    #@20
    const/high16 v10, 0x3f40

    #@22
    add-float v0, v9, v10

    #@24
    .line 162
    .local v0, alpha:F
    const/high16 v9, 0x3e80

    #@26
    aget v10, v8, v11

    #@28
    aget v11, v8, v11

    #@2a
    mul-float/2addr v10, v11

    #@2b
    aget v11, v8, v12

    #@2d
    aget v12, v8, v12

    #@2f
    mul-float/2addr v11, v12

    #@30
    add-float/2addr v10, v11

    #@31
    mul-float v2, v9, v10

    #@33
    .line 163
    .local v2, bound2:F
    float-to-double v9, v2

    #@34
    invoke-static {v9, v10}, Ljava/lang/Math;->sqrt(D)D

    #@37
    move-result-wide v9

    #@38
    double-to-float v1, v9

    #@39
    .line 164
    .local v1, bound:F
    const v9, 0x3f933333

    #@3c
    mul-float v6, v9, v1

    #@3e
    .line 165
    .local v6, radius:F
    mul-float v7, v6, v6

    #@40
    .line 166
    .local v7, radius2:F
    const v9, 0x3fc90fdb

    #@43
    div-float v10, v0, v1

    #@45
    sub-float v11, v7, v2

    #@47
    float-to-double v11, v11

    #@48
    invoke-static {v11, v12}, Ljava/lang/Math;->sqrt(D)D

    #@4b
    move-result-wide v11

    #@4c
    double-to-float v11, v11

    #@4d
    mul-float/2addr v10, v11

    #@4e
    float-to-double v10, v10

    #@4f
    invoke-static {v10, v11}, Ljava/lang/Math;->atan(D)D

    #@52
    move-result-wide v10

    #@53
    double-to-float v10, v10

    #@54
    sub-float v4, v9, v10

    #@56
    .line 168
    .local v4, max_radian:F
    div-float v3, v1, v4

    #@58
    .line 170
    .local v3, factor:F
    iget-object v9, p0, Landroid/filterpacks/imageproc/FisheyeFilter;->mProgram:Landroid/filterfw/core/Program;

    #@5a
    const-string/jumbo v10, "scale"

    #@5d
    invoke-virtual {v9, v10, v8}, Landroid/filterfw/core/Program;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@60
    .line 171
    iget-object v9, p0, Landroid/filterpacks/imageproc/FisheyeFilter;->mProgram:Landroid/filterfw/core/Program;

    #@62
    const-string/jumbo v10, "radius2"

    #@65
    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@68
    move-result-object v11

    #@69
    invoke-virtual {v9, v10, v11}, Landroid/filterfw/core/Program;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@6c
    .line 172
    iget-object v9, p0, Landroid/filterpacks/imageproc/FisheyeFilter;->mProgram:Landroid/filterfw/core/Program;

    #@6e
    const-string v10, "factor"

    #@70
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@73
    move-result-object v11

    #@74
    invoke-virtual {v9, v10, v11}, Landroid/filterfw/core/Program;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@77
    .line 173
    iget-object v9, p0, Landroid/filterpacks/imageproc/FisheyeFilter;->mProgram:Landroid/filterfw/core/Program;

    #@79
    const-string v10, "alpha"

    #@7b
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@7e
    move-result-object v11

    #@7f
    invoke-virtual {v9, v10, v11}, Landroid/filterfw/core/Program;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@82
    .line 174
    return-void

    #@83
    .line 158
    .end local v0           #alpha:F
    .end local v1           #bound:F
    .end local v2           #bound2:F
    .end local v3           #factor:F
    .end local v4           #max_radian:F
    .end local v6           #radius:F
    .end local v7           #radius2:F
    :cond_83
    iget v9, p0, Landroid/filterpacks/imageproc/FisheyeFilter;->mWidth:I

    #@85
    int-to-float v9, v9

    #@86
    iget v10, p0, Landroid/filterpacks/imageproc/FisheyeFilter;->mHeight:I

    #@88
    int-to-float v10, v10

    #@89
    div-float/2addr v9, v10

    #@8a
    aput v9, v8, v11

    #@8c
    .line 159
    aput v13, v8, v12

    #@8e
    goto :goto_1b
.end method


# virtual methods
.method public fieldPortValueUpdated(Ljava/lang/String;Landroid/filterfw/core/FilterContext;)V
    .registers 4
    .parameter "name"
    .parameter "context"

    #@0
    .prologue
    .line 139
    iget-object v0, p0, Landroid/filterpacks/imageproc/FisheyeFilter;->mProgram:Landroid/filterfw/core/Program;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 140
    invoke-direct {p0}, Landroid/filterpacks/imageproc/FisheyeFilter;->updateProgramParams()V

    #@7
    .line 142
    :cond_7
    return-void
.end method

.method public getOutputFormat(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .registers 3
    .parameter "portName"
    .parameter "inputFormat"

    #@0
    .prologue
    .line 90
    return-object p2
.end method

.method public initProgram(Landroid/filterfw/core/FilterContext;I)V
    .registers 7
    .parameter "context"
    .parameter "target"

    #@0
    .prologue
    .line 94
    packed-switch p2, :pswitch_data_34

    #@3
    .line 102
    new-instance v1, Ljava/lang/RuntimeException;

    #@5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v3, "Filter FisheyeFilter does not support frames of target "

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    const-string v3, "!"

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@21
    throw v1

    #@22
    .line 96
    :pswitch_22
    new-instance v0, Landroid/filterfw/core/ShaderProgram;

    #@24
    const-string/jumbo v1, "precision highp float;\nuniform sampler2D tex_sampler_0;\nuniform vec2 scale;\nuniform float alpha;\nuniform float radius2;\nuniform float factor;\nvarying vec2 v_texcoord;\nvoid main() {\n  const float m_pi_2 = 1.570963;\n  vec2 coord = v_texcoord - vec2(0.5, 0.5);\n  float dist = length(coord * scale);\n  float radian = m_pi_2 - atan(alpha * sqrt(radius2 - dist * dist), dist);\n  float scalar = radian * factor / dist;\n  vec2 new_coord = coord * scalar + vec2(0.5, 0.5);\n  gl_FragColor = texture2D(tex_sampler_0, new_coord);\n}\n"

    #@27
    invoke-direct {v0, p1, v1}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;)V

    #@2a
    .line 97
    .local v0, shaderProgram:Landroid/filterfw/core/ShaderProgram;
    iget v1, p0, Landroid/filterpacks/imageproc/FisheyeFilter;->mTileSize:I

    #@2c
    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setMaximumTileSize(I)V

    #@2f
    .line 98
    iput-object v0, p0, Landroid/filterpacks/imageproc/FisheyeFilter;->mProgram:Landroid/filterfw/core/Program;

    #@31
    .line 105
    iput p2, p0, Landroid/filterpacks/imageproc/FisheyeFilter;->mTarget:I

    #@33
    .line 106
    return-void

    #@34
    .line 94
    :pswitch_data_34
    .packed-switch 0x3
        :pswitch_22
    .end packed-switch
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 7
    .parameter "context"

    #@0
    .prologue
    .line 111
    const-string v3, "image"

    #@2
    invoke-virtual {p0, v3}, Landroid/filterpacks/imageproc/FisheyeFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@5
    move-result-object v0

    #@6
    .line 112
    .local v0, input:Landroid/filterfw/core/Frame;
    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@9
    move-result-object v1

    #@a
    .line 115
    .local v1, inputFormat:Landroid/filterfw/core/FrameFormat;
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v3, v1}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@11
    move-result-object v2

    #@12
    .line 118
    .local v2, output:Landroid/filterfw/core/Frame;
    iget-object v3, p0, Landroid/filterpacks/imageproc/FisheyeFilter;->mProgram:Landroid/filterfw/core/Program;

    #@14
    if-eqz v3, :cond_1e

    #@16
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@19
    move-result v3

    #@1a
    iget v4, p0, Landroid/filterpacks/imageproc/FisheyeFilter;->mTarget:I

    #@1c
    if-eq v3, v4, :cond_25

    #@1e
    .line 119
    :cond_1e
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@21
    move-result v3

    #@22
    invoke-virtual {p0, p1, v3}, Landroid/filterpacks/imageproc/FisheyeFilter;->initProgram(Landroid/filterfw/core/FilterContext;I)V

    #@25
    .line 123
    :cond_25
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@28
    move-result v3

    #@29
    iget v4, p0, Landroid/filterpacks/imageproc/FisheyeFilter;->mWidth:I

    #@2b
    if-ne v3, v4, :cond_35

    #@2d
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@30
    move-result v3

    #@31
    iget v4, p0, Landroid/filterpacks/imageproc/FisheyeFilter;->mHeight:I

    #@33
    if-eq v3, v4, :cond_40

    #@35
    .line 124
    :cond_35
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@38
    move-result v3

    #@39
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@3c
    move-result v4

    #@3d
    invoke-direct {p0, v3, v4}, Landroid/filterpacks/imageproc/FisheyeFilter;->updateFrameSize(II)V

    #@40
    .line 128
    :cond_40
    iget-object v3, p0, Landroid/filterpacks/imageproc/FisheyeFilter;->mProgram:Landroid/filterfw/core/Program;

    #@42
    invoke-virtual {v3, v0, v2}, Landroid/filterfw/core/Program;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@45
    .line 131
    const-string v3, "image"

    #@47
    invoke-virtual {p0, v3, v2}, Landroid/filterpacks/imageproc/FisheyeFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@4a
    .line 134
    invoke-virtual {v2}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@4d
    .line 135
    return-void
.end method

.method public setupPorts()V
    .registers 3

    #@0
    .prologue
    .line 84
    const-string v0, "image"

    #@2
    const/4 v1, 0x3

    #@3
    invoke-static {v1}, Landroid/filterfw/format/ImageFormat;->create(I)Landroid/filterfw/core/MutableFrameFormat;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/FisheyeFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@a
    .line 85
    const-string v0, "image"

    #@c
    const-string v1, "image"

    #@e
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/FisheyeFilter;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 86
    return-void
.end method
