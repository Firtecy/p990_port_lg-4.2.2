.class public Landroid/filterpacks/imageproc/DuotoneFilter;
.super Landroid/filterfw/core/Filter;
.source "DuotoneFilter.java"


# instance fields
.field private final mDuotoneShader:Ljava/lang/String;

.field private mFirstColor:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "first_color"
    .end annotation
.end field

.field private mProgram:Landroid/filterfw/core/Program;

.field private mSecondColor:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "second_color"
    .end annotation
.end field

.field private mTarget:I

.field private mTileSize:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "tile_size"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 60
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@3
    .line 34
    const/high16 v0, -0x1

    #@5
    iput v0, p0, Landroid/filterpacks/imageproc/DuotoneFilter;->mFirstColor:I

    #@7
    .line 37
    const/16 v0, -0x100

    #@9
    iput v0, p0, Landroid/filterpacks/imageproc/DuotoneFilter;->mSecondColor:I

    #@b
    .line 40
    const/16 v0, 0x280

    #@d
    iput v0, p0, Landroid/filterpacks/imageproc/DuotoneFilter;->mTileSize:I

    #@f
    .line 44
    const/4 v0, 0x0

    #@10
    iput v0, p0, Landroid/filterpacks/imageproc/DuotoneFilter;->mTarget:I

    #@12
    .line 46
    const-string/jumbo v0, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform vec3 first;\nuniform vec3 second;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  float energy = (color.r + color.g + color.b) * 0.3333;\n  vec3 new_color = (1.0 - energy) * first + energy * second;\n  gl_FragColor = vec4(new_color.rgb, color.a);\n}\n"

    #@15
    iput-object v0, p0, Landroid/filterpacks/imageproc/DuotoneFilter;->mDuotoneShader:Ljava/lang/String;

    #@17
    .line 61
    return-void
.end method

.method private updateParameters()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x3

    #@1
    const/4 v6, 0x2

    #@2
    const/4 v5, 0x1

    #@3
    const/4 v4, 0x0

    #@4
    const/high16 v3, 0x437f

    #@6
    .line 115
    new-array v0, v7, [F

    #@8
    iget v2, p0, Landroid/filterpacks/imageproc/DuotoneFilter;->mFirstColor:I

    #@a
    invoke-static {v2}, Landroid/graphics/Color;->red(I)I

    #@d
    move-result v2

    #@e
    int-to-float v2, v2

    #@f
    div-float/2addr v2, v3

    #@10
    aput v2, v0, v4

    #@12
    iget v2, p0, Landroid/filterpacks/imageproc/DuotoneFilter;->mFirstColor:I

    #@14
    invoke-static {v2}, Landroid/graphics/Color;->green(I)I

    #@17
    move-result v2

    #@18
    int-to-float v2, v2

    #@19
    div-float/2addr v2, v3

    #@1a
    aput v2, v0, v5

    #@1c
    iget v2, p0, Landroid/filterpacks/imageproc/DuotoneFilter;->mFirstColor:I

    #@1e
    invoke-static {v2}, Landroid/graphics/Color;->blue(I)I

    #@21
    move-result v2

    #@22
    int-to-float v2, v2

    #@23
    div-float/2addr v2, v3

    #@24
    aput v2, v0, v6

    #@26
    .line 118
    .local v0, first:[F
    new-array v1, v7, [F

    #@28
    iget v2, p0, Landroid/filterpacks/imageproc/DuotoneFilter;->mSecondColor:I

    #@2a
    invoke-static {v2}, Landroid/graphics/Color;->red(I)I

    #@2d
    move-result v2

    #@2e
    int-to-float v2, v2

    #@2f
    div-float/2addr v2, v3

    #@30
    aput v2, v1, v4

    #@32
    iget v2, p0, Landroid/filterpacks/imageproc/DuotoneFilter;->mSecondColor:I

    #@34
    invoke-static {v2}, Landroid/graphics/Color;->green(I)I

    #@37
    move-result v2

    #@38
    int-to-float v2, v2

    #@39
    div-float/2addr v2, v3

    #@3a
    aput v2, v1, v5

    #@3c
    iget v2, p0, Landroid/filterpacks/imageproc/DuotoneFilter;->mSecondColor:I

    #@3e
    invoke-static {v2}, Landroid/graphics/Color;->blue(I)I

    #@41
    move-result v2

    #@42
    int-to-float v2, v2

    #@43
    div-float/2addr v2, v3

    #@44
    aput v2, v1, v6

    #@46
    .line 122
    .local v1, second:[F
    iget-object v2, p0, Landroid/filterpacks/imageproc/DuotoneFilter;->mProgram:Landroid/filterfw/core/Program;

    #@48
    const-string v3, "first"

    #@4a
    invoke-virtual {v2, v3, v0}, Landroid/filterfw/core/Program;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@4d
    .line 123
    iget-object v2, p0, Landroid/filterpacks/imageproc/DuotoneFilter;->mProgram:Landroid/filterfw/core/Program;

    #@4f
    const-string/jumbo v3, "second"

    #@52
    invoke-virtual {v2, v3, v1}, Landroid/filterfw/core/Program;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@55
    .line 124
    return-void
.end method


# virtual methods
.method public getOutputFormat(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .registers 3
    .parameter "portName"
    .parameter "inputFormat"

    #@0
    .prologue
    .line 71
    return-object p2
.end method

.method public initProgram(Landroid/filterfw/core/FilterContext;I)V
    .registers 7
    .parameter "context"
    .parameter "target"

    #@0
    .prologue
    .line 75
    packed-switch p2, :pswitch_data_34

    #@3
    .line 83
    new-instance v1, Ljava/lang/RuntimeException;

    #@5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v3, "Filter Duotone does not support frames of target "

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    const-string v3, "!"

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@21
    throw v1

    #@22
    .line 77
    :pswitch_22
    new-instance v0, Landroid/filterfw/core/ShaderProgram;

    #@24
    const-string/jumbo v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform vec3 first;\nuniform vec3 second;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  float energy = (color.r + color.g + color.b) * 0.3333;\n  vec3 new_color = (1.0 - energy) * first + energy * second;\n  gl_FragColor = vec4(new_color.rgb, color.a);\n}\n"

    #@27
    invoke-direct {v0, p1, v1}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;)V

    #@2a
    .line 78
    .local v0, shaderProgram:Landroid/filterfw/core/ShaderProgram;
    iget v1, p0, Landroid/filterpacks/imageproc/DuotoneFilter;->mTileSize:I

    #@2c
    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setMaximumTileSize(I)V

    #@2f
    .line 79
    iput-object v0, p0, Landroid/filterpacks/imageproc/DuotoneFilter;->mProgram:Landroid/filterfw/core/Program;

    #@31
    .line 86
    iput p2, p0, Landroid/filterpacks/imageproc/DuotoneFilter;->mTarget:I

    #@33
    .line 87
    return-void

    #@34
    .line 75
    :pswitch_data_34
    .packed-switch 0x3
        :pswitch_22
    .end packed-switch
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 7
    .parameter "context"

    #@0
    .prologue
    .line 92
    const-string v3, "image"

    #@2
    invoke-virtual {p0, v3}, Landroid/filterpacks/imageproc/DuotoneFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@5
    move-result-object v0

    #@6
    .line 93
    .local v0, input:Landroid/filterfw/core/Frame;
    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@9
    move-result-object v1

    #@a
    .line 96
    .local v1, inputFormat:Landroid/filterfw/core/FrameFormat;
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v3, v1}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@11
    move-result-object v2

    #@12
    .line 99
    .local v2, output:Landroid/filterfw/core/Frame;
    iget-object v3, p0, Landroid/filterpacks/imageproc/DuotoneFilter;->mProgram:Landroid/filterfw/core/Program;

    #@14
    if-eqz v3, :cond_1e

    #@16
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@19
    move-result v3

    #@1a
    iget v4, p0, Landroid/filterpacks/imageproc/DuotoneFilter;->mTarget:I

    #@1c
    if-eq v3, v4, :cond_25

    #@1e
    .line 100
    :cond_1e
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@21
    move-result v3

    #@22
    invoke-virtual {p0, p1, v3}, Landroid/filterpacks/imageproc/DuotoneFilter;->initProgram(Landroid/filterfw/core/FilterContext;I)V

    #@25
    .line 102
    :cond_25
    invoke-direct {p0}, Landroid/filterpacks/imageproc/DuotoneFilter;->updateParameters()V

    #@28
    .line 105
    iget-object v3, p0, Landroid/filterpacks/imageproc/DuotoneFilter;->mProgram:Landroid/filterfw/core/Program;

    #@2a
    invoke-virtual {v3, v0, v2}, Landroid/filterfw/core/Program;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@2d
    .line 108
    const-string v3, "image"

    #@2f
    invoke-virtual {p0, v3, v2}, Landroid/filterpacks/imageproc/DuotoneFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@32
    .line 111
    invoke-virtual {v2}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@35
    .line 112
    return-void
.end method

.method public setupPorts()V
    .registers 3

    #@0
    .prologue
    .line 65
    const-string v0, "image"

    #@2
    const/4 v1, 0x3

    #@3
    invoke-static {v1}, Landroid/filterfw/format/ImageFormat;->create(I)Landroid/filterfw/core/MutableFrameFormat;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/DuotoneFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@a
    .line 66
    const-string v0, "image"

    #@c
    const-string v1, "image"

    #@e
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/DuotoneFilter;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 67
    return-void
.end method
