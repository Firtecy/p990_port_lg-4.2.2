.class public Landroid/filterpacks/imageproc/CropRectFilter;
.super Landroid/filterfw/core/Filter;
.source "CropRectFilter.java"


# instance fields
.field private mHeight:I

.field private mOutputHeight:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "height"
    .end annotation
.end field

.field private mOutputWidth:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "width"
    .end annotation
.end field

.field private mProgram:Landroid/filterfw/core/Program;

.field private mTarget:I

.field private mTileSize:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "tile_size"
    .end annotation
.end field

.field private mWidth:I

.field private mXorigin:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "xorigin"
    .end annotation
.end field

.field private mYorigin:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "yorigin"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter "name"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 61
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@4
    .line 50
    const/16 v0, 0x280

    #@6
    iput v0, p0, Landroid/filterpacks/imageproc/CropRectFilter;->mTileSize:I

    #@8
    .line 55
    iput v1, p0, Landroid/filterpacks/imageproc/CropRectFilter;->mWidth:I

    #@a
    .line 56
    iput v1, p0, Landroid/filterpacks/imageproc/CropRectFilter;->mHeight:I

    #@c
    .line 58
    iput v1, p0, Landroid/filterpacks/imageproc/CropRectFilter;->mTarget:I

    #@e
    .line 62
    return-void
.end method


# virtual methods
.method public fieldPortValueUpdated(Ljava/lang/String;Landroid/filterfw/core/FilterContext;)V
    .registers 5
    .parameter "name"
    .parameter "context"

    #@0
    .prologue
    .line 87
    iget-object v0, p0, Landroid/filterpacks/imageproc/CropRectFilter;->mProgram:Landroid/filterfw/core/Program;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 88
    iget v0, p0, Landroid/filterpacks/imageproc/CropRectFilter;->mWidth:I

    #@6
    iget v1, p0, Landroid/filterpacks/imageproc/CropRectFilter;->mHeight:I

    #@8
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/CropRectFilter;->updateSourceRect(II)V

    #@b
    .line 90
    :cond_b
    return-void
.end method

.method public initProgram(Landroid/filterfw/core/FilterContext;I)V
    .registers 7
    .parameter "context"
    .parameter "target"

    #@0
    .prologue
    .line 71
    packed-switch p2, :pswitch_data_30

    #@3
    .line 79
    new-instance v1, Ljava/lang/RuntimeException;

    #@5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v3, "Filter Sharpen does not support frames of target "

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    const-string v3, "!"

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@21
    throw v1

    #@22
    .line 73
    :pswitch_22
    invoke-static {p1}, Landroid/filterfw/core/ShaderProgram;->createIdentity(Landroid/filterfw/core/FilterContext;)Landroid/filterfw/core/ShaderProgram;

    #@25
    move-result-object v0

    #@26
    .line 74
    .local v0, shaderProgram:Landroid/filterfw/core/ShaderProgram;
    iget v1, p0, Landroid/filterpacks/imageproc/CropRectFilter;->mTileSize:I

    #@28
    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setMaximumTileSize(I)V

    #@2b
    .line 75
    iput-object v0, p0, Landroid/filterpacks/imageproc/CropRectFilter;->mProgram:Landroid/filterfw/core/Program;

    #@2d
    .line 82
    iput p2, p0, Landroid/filterpacks/imageproc/CropRectFilter;->mTarget:I

    #@2f
    .line 83
    return-void

    #@30
    .line 71
    :pswitch_data_30
    .packed-switch 0x3
        :pswitch_22
    .end packed-switch
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 9
    .parameter "context"

    #@0
    .prologue
    const/4 v6, 0x3

    #@1
    .line 95
    const-string v4, "image"

    #@3
    invoke-virtual {p0, v4}, Landroid/filterpacks/imageproc/CropRectFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@6
    move-result-object v0

    #@7
    .line 96
    .local v0, input:Landroid/filterfw/core/Frame;
    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@a
    move-result-object v1

    #@b
    .line 99
    .local v1, inputFormat:Landroid/filterfw/core/FrameFormat;
    iget v4, p0, Landroid/filterpacks/imageproc/CropRectFilter;->mOutputWidth:I

    #@d
    iget v5, p0, Landroid/filterpacks/imageproc/CropRectFilter;->mOutputHeight:I

    #@f
    invoke-static {v4, v5, v6, v6}, Landroid/filterfw/format/ImageFormat;->create(IIII)Landroid/filterfw/core/MutableFrameFormat;

    #@12
    move-result-object v3

    #@13
    .line 102
    .local v3, outputFormat:Landroid/filterfw/core/FrameFormat;
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4, v3}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@1a
    move-result-object v2

    #@1b
    .line 105
    .local v2, output:Landroid/filterfw/core/Frame;
    iget-object v4, p0, Landroid/filterpacks/imageproc/CropRectFilter;->mProgram:Landroid/filterfw/core/Program;

    #@1d
    if-eqz v4, :cond_27

    #@1f
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@22
    move-result v4

    #@23
    iget v5, p0, Landroid/filterpacks/imageproc/CropRectFilter;->mTarget:I

    #@25
    if-eq v4, v5, :cond_2e

    #@27
    .line 106
    :cond_27
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@2a
    move-result v4

    #@2b
    invoke-virtual {p0, p1, v4}, Landroid/filterpacks/imageproc/CropRectFilter;->initProgram(Landroid/filterfw/core/FilterContext;I)V

    #@2e
    .line 110
    :cond_2e
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@31
    move-result v4

    #@32
    iget v5, p0, Landroid/filterpacks/imageproc/CropRectFilter;->mWidth:I

    #@34
    if-ne v4, v5, :cond_3e

    #@36
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@39
    move-result v4

    #@3a
    iget v5, p0, Landroid/filterpacks/imageproc/CropRectFilter;->mHeight:I

    #@3c
    if-eq v4, v5, :cond_49

    #@3e
    .line 111
    :cond_3e
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@41
    move-result v4

    #@42
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@45
    move-result v5

    #@46
    invoke-virtual {p0, v4, v5}, Landroid/filterpacks/imageproc/CropRectFilter;->updateSourceRect(II)V

    #@49
    .line 115
    :cond_49
    iget-object v4, p0, Landroid/filterpacks/imageproc/CropRectFilter;->mProgram:Landroid/filterfw/core/Program;

    #@4b
    invoke-virtual {v4, v0, v2}, Landroid/filterfw/core/Program;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@4e
    .line 118
    const-string v4, "image"

    #@50
    invoke-virtual {p0, v4, v2}, Landroid/filterpacks/imageproc/CropRectFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@53
    .line 121
    invoke-virtual {v2}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@56
    .line 122
    return-void
.end method

.method public setupPorts()V
    .registers 3

    #@0
    .prologue
    .line 66
    const-string v0, "image"

    #@2
    const/4 v1, 0x3

    #@3
    invoke-static {v1}, Landroid/filterfw/format/ImageFormat;->create(I)Landroid/filterfw/core/MutableFrameFormat;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/CropRectFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@a
    .line 67
    const-string v0, "image"

    #@c
    const-string v1, "image"

    #@e
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/CropRectFilter;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 68
    return-void
.end method

.method updateSourceRect(II)V
    .registers 9
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 125
    iput p1, p0, Landroid/filterpacks/imageproc/CropRectFilter;->mWidth:I

    #@2
    .line 126
    iput p2, p0, Landroid/filterpacks/imageproc/CropRectFilter;->mHeight:I

    #@4
    .line 136
    iget-object v0, p0, Landroid/filterpacks/imageproc/CropRectFilter;->mProgram:Landroid/filterfw/core/Program;

    #@6
    check-cast v0, Landroid/filterfw/core/ShaderProgram;

    #@8
    iget v1, p0, Landroid/filterpacks/imageproc/CropRectFilter;->mXorigin:I

    #@a
    int-to-float v1, v1

    #@b
    iget v2, p0, Landroid/filterpacks/imageproc/CropRectFilter;->mWidth:I

    #@d
    int-to-float v2, v2

    #@e
    div-float/2addr v1, v2

    #@f
    iget v2, p0, Landroid/filterpacks/imageproc/CropRectFilter;->mYorigin:I

    #@11
    int-to-float v2, v2

    #@12
    iget v3, p0, Landroid/filterpacks/imageproc/CropRectFilter;->mHeight:I

    #@14
    int-to-float v3, v3

    #@15
    div-float/2addr v2, v3

    #@16
    iget v3, p0, Landroid/filterpacks/imageproc/CropRectFilter;->mOutputWidth:I

    #@18
    int-to-float v3, v3

    #@19
    iget v4, p0, Landroid/filterpacks/imageproc/CropRectFilter;->mWidth:I

    #@1b
    int-to-float v4, v4

    #@1c
    div-float/2addr v3, v4

    #@1d
    iget v4, p0, Landroid/filterpacks/imageproc/CropRectFilter;->mOutputHeight:I

    #@1f
    int-to-float v4, v4

    #@20
    iget v5, p0, Landroid/filterpacks/imageproc/CropRectFilter;->mHeight:I

    #@22
    int-to-float v5, v5

    #@23
    div-float/2addr v4, v5

    #@24
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/filterfw/core/ShaderProgram;->setSourceRect(FFFF)V

    #@27
    .line 140
    return-void
.end method
