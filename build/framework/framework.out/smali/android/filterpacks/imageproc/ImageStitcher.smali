.class public Landroid/filterpacks/imageproc/ImageStitcher;
.super Landroid/filterfw/core/Filter;
.source "ImageStitcher.java"


# instance fields
.field private mImageHeight:I

.field private mImageWidth:I

.field private mInputHeight:I

.field private mInputWidth:I

.field private mOutputFrame:Landroid/filterfw/core/Frame;

.field private mPadSize:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "padSize"
    .end annotation
.end field

.field private mProgram:Landroid/filterfw/core/Program;

.field private mSliceHeight:I

.field private mSliceIndex:I

.field private mSliceWidth:I

.field private mXSlices:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "xSlices"
    .end annotation
.end field

.field private mYSlices:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "ySlices"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 59
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@3
    .line 60
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mSliceIndex:I

    #@6
    .line 61
    return-void
.end method

.method private calcOutputFormatForInput(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .registers 5
    .parameter "format"

    #@0
    .prologue
    .line 76
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->mutableCopy()Landroid/filterfw/core/MutableFrameFormat;

    #@3
    move-result-object v0

    #@4
    .line 78
    .local v0, outputFormat:Landroid/filterfw/core/MutableFrameFormat;
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@7
    move-result v1

    #@8
    iput v1, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mInputWidth:I

    #@a
    .line 79
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@d
    move-result v1

    #@e
    iput v1, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mInputHeight:I

    #@10
    .line 81
    iget v1, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mInputWidth:I

    #@12
    iget v2, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mPadSize:I

    #@14
    mul-int/lit8 v2, v2, 0x2

    #@16
    sub-int/2addr v1, v2

    #@17
    iput v1, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mSliceWidth:I

    #@19
    .line 82
    iget v1, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mInputHeight:I

    #@1b
    iget v2, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mPadSize:I

    #@1d
    mul-int/lit8 v2, v2, 0x2

    #@1f
    sub-int/2addr v1, v2

    #@20
    iput v1, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mSliceHeight:I

    #@22
    .line 84
    iget v1, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mSliceWidth:I

    #@24
    iget v2, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mXSlices:I

    #@26
    mul-int/2addr v1, v2

    #@27
    iput v1, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mImageWidth:I

    #@29
    .line 85
    iget v1, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mSliceHeight:I

    #@2b
    iget v2, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mYSlices:I

    #@2d
    mul-int/2addr v1, v2

    #@2e
    iput v1, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mImageHeight:I

    #@30
    .line 87
    iget v1, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mImageWidth:I

    #@32
    iget v2, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mImageHeight:I

    #@34
    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/MutableFrameFormat;->setDimensions(II)V

    #@37
    .line 88
    return-object v0
.end method


# virtual methods
.method public getOutputFormat(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .registers 3
    .parameter "portName"
    .parameter "inputFormat"

    #@0
    .prologue
    .line 72
    return-object p2
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 15
    .parameter "context"

    #@0
    .prologue
    .line 94
    const-string v8, "image"

    #@2
    invoke-virtual {p0, v8}, Landroid/filterpacks/imageproc/ImageStitcher;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@5
    move-result-object v1

    #@6
    .line 95
    .local v1, input:Landroid/filterfw/core/Frame;
    invoke-virtual {v1}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@9
    move-result-object v0

    #@a
    .line 98
    .local v0, format:Landroid/filterfw/core/FrameFormat;
    iget v8, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mSliceIndex:I

    #@c
    if-nez v8, :cond_ae

    #@e
    .line 99
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@11
    move-result-object v8

    #@12
    invoke-direct {p0, v0}, Landroid/filterpacks/imageproc/ImageStitcher;->calcOutputFormatForInput(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;

    #@15
    move-result-object v9

    #@16
    invoke-virtual {v8, v9}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@19
    move-result-object v8

    #@1a
    iput-object v8, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mOutputFrame:Landroid/filterfw/core/Frame;

    #@1c
    .line 109
    :cond_1c
    iget-object v8, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mProgram:Landroid/filterfw/core/Program;

    #@1e
    if-nez v8, :cond_26

    #@20
    .line 110
    invoke-static {p1}, Landroid/filterfw/core/ShaderProgram;->createIdentity(Landroid/filterfw/core/FilterContext;)Landroid/filterfw/core/ShaderProgram;

    #@23
    move-result-object v8

    #@24
    iput-object v8, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mProgram:Landroid/filterfw/core/Program;

    #@26
    .line 114
    :cond_26
    iget v8, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mPadSize:I

    #@28
    int-to-float v8, v8

    #@29
    iget v9, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mInputWidth:I

    #@2b
    int-to-float v9, v9

    #@2c
    div-float v6, v8, v9

    #@2e
    .line 115
    .local v6, x0:F
    iget v8, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mPadSize:I

    #@30
    int-to-float v8, v8

    #@31
    iget v9, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mInputHeight:I

    #@33
    int-to-float v9, v9

    #@34
    div-float v7, v8, v9

    #@36
    .line 117
    .local v7, y0:F
    iget v8, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mSliceIndex:I

    #@38
    iget v9, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mXSlices:I

    #@3a
    rem-int/2addr v8, v9

    #@3b
    iget v9, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mSliceWidth:I

    #@3d
    mul-int v3, v8, v9

    #@3f
    .line 118
    .local v3, outputOffsetX:I
    iget v8, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mSliceIndex:I

    #@41
    iget v9, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mXSlices:I

    #@43
    div-int/2addr v8, v9

    #@44
    iget v9, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mSliceHeight:I

    #@46
    mul-int v4, v8, v9

    #@48
    .line 120
    .local v4, outputOffsetY:I
    iget v8, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mSliceWidth:I

    #@4a
    iget v9, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mImageWidth:I

    #@4c
    sub-int/2addr v9, v3

    #@4d
    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    #@50
    move-result v8

    #@51
    int-to-float v5, v8

    #@52
    .line 121
    .local v5, outputWidth:F
    iget v8, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mSliceHeight:I

    #@54
    iget v9, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mImageHeight:I

    #@56
    sub-int/2addr v9, v4

    #@57
    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    #@5a
    move-result v8

    #@5b
    int-to-float v2, v8

    #@5c
    .line 124
    .local v2, outputHeight:F
    iget-object v8, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mProgram:Landroid/filterfw/core/Program;

    #@5e
    check-cast v8, Landroid/filterfw/core/ShaderProgram;

    #@60
    iget v9, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mInputWidth:I

    #@62
    int-to-float v9, v9

    #@63
    div-float v9, v5, v9

    #@65
    iget v10, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mInputHeight:I

    #@67
    int-to-float v10, v10

    #@68
    div-float v10, v2, v10

    #@6a
    invoke-virtual {v8, v6, v7, v9, v10}, Landroid/filterfw/core/ShaderProgram;->setSourceRect(FFFF)V

    #@6d
    .line 128
    iget-object v8, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mProgram:Landroid/filterfw/core/Program;

    #@6f
    check-cast v8, Landroid/filterfw/core/ShaderProgram;

    #@71
    int-to-float v9, v3

    #@72
    iget v10, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mImageWidth:I

    #@74
    int-to-float v10, v10

    #@75
    div-float/2addr v9, v10

    #@76
    int-to-float v10, v4

    #@77
    iget v11, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mImageHeight:I

    #@79
    int-to-float v11, v11

    #@7a
    div-float/2addr v10, v11

    #@7b
    iget v11, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mImageWidth:I

    #@7d
    int-to-float v11, v11

    #@7e
    div-float v11, v5, v11

    #@80
    iget v12, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mImageHeight:I

    #@82
    int-to-float v12, v12

    #@83
    div-float v12, v2, v12

    #@85
    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/filterfw/core/ShaderProgram;->setTargetRect(FFFF)V

    #@88
    .line 134
    iget-object v8, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mProgram:Landroid/filterfw/core/Program;

    #@8a
    iget-object v9, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mOutputFrame:Landroid/filterfw/core/Frame;

    #@8c
    invoke-virtual {v8, v1, v9}, Landroid/filterfw/core/Program;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@8f
    .line 135
    iget v8, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mSliceIndex:I

    #@91
    add-int/lit8 v8, v8, 0x1

    #@93
    iput v8, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mSliceIndex:I

    #@95
    .line 138
    iget v8, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mSliceIndex:I

    #@97
    iget v9, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mXSlices:I

    #@99
    iget v10, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mYSlices:I

    #@9b
    mul-int/2addr v9, v10

    #@9c
    if-ne v8, v9, :cond_ad

    #@9e
    .line 139
    const-string v8, "image"

    #@a0
    iget-object v9, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mOutputFrame:Landroid/filterfw/core/Frame;

    #@a2
    invoke-virtual {p0, v8, v9}, Landroid/filterpacks/imageproc/ImageStitcher;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@a5
    .line 140
    iget-object v8, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mOutputFrame:Landroid/filterfw/core/Frame;

    #@a7
    invoke-virtual {v8}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@aa
    .line 141
    const/4 v8, 0x0

    #@ab
    iput v8, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mSliceIndex:I

    #@ad
    .line 143
    :cond_ad
    return-void

    #@ae
    .line 101
    .end local v2           #outputHeight:F
    .end local v3           #outputOffsetX:I
    .end local v4           #outputOffsetY:I
    .end local v5           #outputWidth:F
    .end local v6           #x0:F
    .end local v7           #y0:F
    :cond_ae
    invoke-virtual {v0}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@b1
    move-result v8

    #@b2
    iget v9, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mInputWidth:I

    #@b4
    if-ne v8, v9, :cond_be

    #@b6
    invoke-virtual {v0}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@b9
    move-result v8

    #@ba
    iget v9, p0, Landroid/filterpacks/imageproc/ImageStitcher;->mInputHeight:I

    #@bc
    if-eq v8, v9, :cond_1c

    #@be
    .line 104
    :cond_be
    new-instance v8, Ljava/lang/RuntimeException;

    #@c0
    const-string v9, "Image size should not change."

    #@c2
    invoke-direct {v8, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@c5
    throw v8
.end method

.method public setupPorts()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x3

    #@1
    .line 65
    const-string v0, "image"

    #@3
    invoke-static {v1, v1}, Landroid/filterfw/format/ImageFormat;->create(II)Landroid/filterfw/core/MutableFrameFormat;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/ImageStitcher;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@a
    .line 67
    const-string v0, "image"

    #@c
    const-string v1, "image"

    #@e
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/ImageStitcher;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 68
    return-void
.end method
