.class public Landroid/filterpacks/imageproc/CropFilter;
.super Landroid/filterfw/core/Filter;
.source "CropFilter.java"


# instance fields
.field private mFillBlack:Z
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "fillblack"
    .end annotation
.end field

.field private final mFragShader:Ljava/lang/String;

.field private mLastFormat:Landroid/filterfw/core/FrameFormat;

.field private mOutputHeight:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "oheight"
    .end annotation
.end field

.field private mOutputWidth:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "owidth"
    .end annotation
.end field

.field private mProgram:Landroid/filterfw/core/Program;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter "name"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 56
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@4
    .line 44
    const/4 v0, 0x0

    #@5
    iput-object v0, p0, Landroid/filterpacks/imageproc/CropFilter;->mLastFormat:Landroid/filterfw/core/FrameFormat;

    #@7
    .line 46
    iput v1, p0, Landroid/filterpacks/imageproc/CropFilter;->mOutputWidth:I

    #@9
    .line 49
    iput v1, p0, Landroid/filterpacks/imageproc/CropFilter;->mOutputHeight:I

    #@b
    .line 52
    const/4 v0, 0x0

    #@c
    iput-boolean v0, p0, Landroid/filterpacks/imageproc/CropFilter;->mFillBlack:Z

    #@e
    .line 59
    const-string/jumbo v0, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  const vec2 lo = vec2(0.0, 0.0);\n  const vec2 hi = vec2(1.0, 1.0);\n  const vec4 black = vec4(0.0, 0.0, 0.0, 1.0);\n  bool out_of_bounds =\n    any(lessThan(v_texcoord, lo)) ||\n    any(greaterThan(v_texcoord, hi));\n  if (out_of_bounds) {\n    gl_FragColor = black;\n  } else {\n    gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n  }\n}\n"

    #@11
    iput-object v0, p0, Landroid/filterpacks/imageproc/CropFilter;->mFragShader:Ljava/lang/String;

    #@13
    .line 57
    return-void
.end method


# virtual methods
.method protected createProgram(Landroid/filterfw/core/FilterContext;Landroid/filterfw/core/FrameFormat;)V
    .registers 6
    .parameter "context"
    .parameter "format"

    #@0
    .prologue
    .line 95
    iget-object v0, p0, Landroid/filterpacks/imageproc/CropFilter;->mLastFormat:Landroid/filterfw/core/FrameFormat;

    #@2
    if-eqz v0, :cond_11

    #@4
    iget-object v0, p0, Landroid/filterpacks/imageproc/CropFilter;->mLastFormat:Landroid/filterfw/core/FrameFormat;

    #@6
    invoke-virtual {v0}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@9
    move-result v0

    #@a
    invoke-virtual {p2}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@d
    move-result v1

    #@e
    if-ne v0, v1, :cond_11

    #@10
    .line 110
    :cond_10
    return-void

    #@11
    .line 96
    :cond_11
    iput-object p2, p0, Landroid/filterpacks/imageproc/CropFilter;->mLastFormat:Landroid/filterfw/core/FrameFormat;

    #@13
    .line 97
    const/4 v0, 0x0

    #@14
    iput-object v0, p0, Landroid/filterpacks/imageproc/CropFilter;->mProgram:Landroid/filterfw/core/Program;

    #@16
    .line 98
    invoke-virtual {p2}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@19
    move-result v0

    #@1a
    packed-switch v0, :pswitch_data_56

    #@1d
    .line 107
    :goto_1d
    iget-object v0, p0, Landroid/filterpacks/imageproc/CropFilter;->mProgram:Landroid/filterfw/core/Program;

    #@1f
    if-nez v0, :cond_10

    #@21
    .line 108
    new-instance v0, Ljava/lang/RuntimeException;

    #@23
    new-instance v1, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v2, "Could not create a program for crop filter "

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    const-string v2, "!"

    #@34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v1

    #@3c
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@3f
    throw v0

    #@40
    .line 100
    :pswitch_40
    iget-boolean v0, p0, Landroid/filterpacks/imageproc/CropFilter;->mFillBlack:Z

    #@42
    if-eqz v0, :cond_4f

    #@44
    .line 101
    new-instance v0, Landroid/filterfw/core/ShaderProgram;

    #@46
    const-string/jumbo v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  const vec2 lo = vec2(0.0, 0.0);\n  const vec2 hi = vec2(1.0, 1.0);\n  const vec4 black = vec4(0.0, 0.0, 0.0, 1.0);\n  bool out_of_bounds =\n    any(lessThan(v_texcoord, lo)) ||\n    any(greaterThan(v_texcoord, hi));\n  if (out_of_bounds) {\n    gl_FragColor = black;\n  } else {\n    gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n  }\n}\n"

    #@49
    invoke-direct {v0, p1, v1}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;)V

    #@4c
    iput-object v0, p0, Landroid/filterpacks/imageproc/CropFilter;->mProgram:Landroid/filterfw/core/Program;

    #@4e
    goto :goto_1d

    #@4f
    .line 103
    :cond_4f
    invoke-static {p1}, Landroid/filterfw/core/ShaderProgram;->createIdentity(Landroid/filterfw/core/FilterContext;)Landroid/filterfw/core/ShaderProgram;

    #@52
    move-result-object v0

    #@53
    iput-object v0, p0, Landroid/filterpacks/imageproc/CropFilter;->mProgram:Landroid/filterfw/core/Program;

    #@55
    goto :goto_1d

    #@56
    .line 98
    :pswitch_data_56
    .packed-switch 0x3
        :pswitch_40
    .end packed-switch
.end method

.method public getOutputFormat(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .registers 5
    .parameter "portName"
    .parameter "inputFormat"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 88
    invoke-virtual {p2}, Landroid/filterfw/core/FrameFormat;->mutableCopy()Landroid/filterfw/core/MutableFrameFormat;

    #@4
    move-result-object v0

    #@5
    .line 89
    .local v0, outputFormat:Landroid/filterfw/core/MutableFrameFormat;
    invoke-virtual {v0, v1, v1}, Landroid/filterfw/core/MutableFrameFormat;->setDimensions(II)V

    #@8
    .line 90
    return-object v0
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 11
    .parameter "env"

    #@0
    .prologue
    const/4 v8, -0x1

    #@1
    .line 115
    const-string v6, "image"

    #@3
    invoke-virtual {p0, v6}, Landroid/filterpacks/imageproc/CropFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@6
    move-result-object v2

    #@7
    .line 116
    .local v2, imageFrame:Landroid/filterfw/core/Frame;
    const-string v6, "box"

    #@9
    invoke-virtual {p0, v6}, Landroid/filterpacks/imageproc/CropFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@c
    move-result-object v1

    #@d
    .line 118
    .local v1, boxFrame:Landroid/filterfw/core/Frame;
    invoke-virtual {v2}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@10
    move-result-object v6

    #@11
    invoke-virtual {p0, p1, v6}, Landroid/filterpacks/imageproc/CropFilter;->createProgram(Landroid/filterfw/core/FilterContext;Landroid/filterfw/core/FrameFormat;)V

    #@14
    .line 121
    invoke-virtual {v1}, Landroid/filterfw/core/Frame;->getObjectValue()Ljava/lang/Object;

    #@17
    move-result-object v0

    #@18
    check-cast v0, Landroid/filterfw/geometry/Quad;

    #@1a
    .line 124
    .local v0, box:Landroid/filterfw/geometry/Quad;
    invoke-virtual {v2}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@1d
    move-result-object v6

    #@1e
    invoke-virtual {v6}, Landroid/filterfw/core/FrameFormat;->mutableCopy()Landroid/filterfw/core/MutableFrameFormat;

    #@21
    move-result-object v4

    #@22
    .line 125
    .local v4, outputFormat:Landroid/filterfw/core/MutableFrameFormat;
    iget v6, p0, Landroid/filterpacks/imageproc/CropFilter;->mOutputWidth:I

    #@24
    if-ne v6, v8, :cond_58

    #@26
    invoke-virtual {v4}, Landroid/filterfw/core/MutableFrameFormat;->getWidth()I

    #@29
    move-result v6

    #@2a
    :goto_2a
    iget v7, p0, Landroid/filterpacks/imageproc/CropFilter;->mOutputHeight:I

    #@2c
    if-ne v7, v8, :cond_5b

    #@2e
    invoke-virtual {v4}, Landroid/filterfw/core/MutableFrameFormat;->getHeight()I

    #@31
    move-result v7

    #@32
    :goto_32
    invoke-virtual {v4, v6, v7}, Landroid/filterfw/core/MutableFrameFormat;->setDimensions(II)V

    #@35
    .line 129
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@38
    move-result-object v6

    #@39
    invoke-virtual {v6, v4}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@3c
    move-result-object v3

    #@3d
    .line 132
    .local v3, output:Landroid/filterfw/core/Frame;
    iget-object v6, p0, Landroid/filterpacks/imageproc/CropFilter;->mProgram:Landroid/filterfw/core/Program;

    #@3f
    instance-of v6, v6, Landroid/filterfw/core/ShaderProgram;

    #@41
    if-eqz v6, :cond_4a

    #@43
    .line 133
    iget-object v5, p0, Landroid/filterpacks/imageproc/CropFilter;->mProgram:Landroid/filterfw/core/Program;

    #@45
    check-cast v5, Landroid/filterfw/core/ShaderProgram;

    #@47
    .line 134
    .local v5, shaderProgram:Landroid/filterfw/core/ShaderProgram;
    invoke-virtual {v5, v0}, Landroid/filterfw/core/ShaderProgram;->setSourceRegion(Landroid/filterfw/geometry/Quad;)V

    #@4a
    .line 137
    .end local v5           #shaderProgram:Landroid/filterfw/core/ShaderProgram;
    :cond_4a
    iget-object v6, p0, Landroid/filterpacks/imageproc/CropFilter;->mProgram:Landroid/filterfw/core/Program;

    #@4c
    invoke-virtual {v6, v2, v3}, Landroid/filterfw/core/Program;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@4f
    .line 140
    const-string v6, "image"

    #@51
    invoke-virtual {p0, v6, v3}, Landroid/filterpacks/imageproc/CropFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@54
    .line 143
    invoke-virtual {v3}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@57
    .line 144
    return-void

    #@58
    .line 125
    .end local v3           #output:Landroid/filterfw/core/Frame;
    :cond_58
    iget v6, p0, Landroid/filterpacks/imageproc/CropFilter;->mOutputWidth:I

    #@5a
    goto :goto_2a

    #@5b
    :cond_5b
    iget v7, p0, Landroid/filterpacks/imageproc/CropFilter;->mOutputHeight:I

    #@5d
    goto :goto_32
.end method

.method public setupPorts()V
    .registers 4

    #@0
    .prologue
    .line 79
    const-string v0, "image"

    #@2
    const/4 v1, 0x3

    #@3
    invoke-static {v1}, Landroid/filterfw/format/ImageFormat;->create(I)Landroid/filterfw/core/MutableFrameFormat;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/CropFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@a
    .line 80
    const-string v0, "box"

    #@c
    const-class v1, Landroid/filterfw/geometry/Quad;

    #@e
    const/4 v2, 0x1

    #@f
    invoke-static {v1, v2}, Landroid/filterfw/format/ObjectFormat;->fromClass(Ljava/lang/Class;I)Landroid/filterfw/core/MutableFrameFormat;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/CropFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@16
    .line 81
    const-string v0, "image"

    #@18
    const-string v1, "image"

    #@1a
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/CropFilter;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    .line 82
    return-void
.end method
