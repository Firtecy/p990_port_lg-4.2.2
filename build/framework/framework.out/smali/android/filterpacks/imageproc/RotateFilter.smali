.class public Landroid/filterpacks/imageproc/RotateFilter;
.super Landroid/filterfw/core/Filter;
.source "RotateFilter.java"


# instance fields
.field private mAngle:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "angle"
    .end annotation
.end field

.field private mHeight:I

.field private mOutputHeight:I

.field private mOutputWidth:I

.field private mProgram:Landroid/filterfw/core/Program;

.field private mTarget:I

.field private mTileSize:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "tile_size"
    .end annotation
.end field

.field private mWidth:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter "name"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 57
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@4
    .line 44
    const/16 v0, 0x280

    #@6
    iput v0, p0, Landroid/filterpacks/imageproc/RotateFilter;->mTileSize:I

    #@8
    .line 49
    iput v1, p0, Landroid/filterpacks/imageproc/RotateFilter;->mWidth:I

    #@a
    .line 50
    iput v1, p0, Landroid/filterpacks/imageproc/RotateFilter;->mHeight:I

    #@c
    .line 51
    iput v1, p0, Landroid/filterpacks/imageproc/RotateFilter;->mTarget:I

    #@e
    .line 58
    return-void
.end method

.method private updateParameters()V
    .registers 12

    #@0
    .prologue
    const/high16 v8, -0x4080

    #@2
    const/high16 v10, 0x3f00

    #@4
    const/high16 v7, 0x3f80

    #@6
    .line 130
    iget v9, p0, Landroid/filterpacks/imageproc/RotateFilter;->mAngle:I

    #@8
    rem-int/lit8 v9, v9, 0x5a

    #@a
    if-nez v9, :cond_72

    #@c
    .line 131
    iget v9, p0, Landroid/filterpacks/imageproc/RotateFilter;->mAngle:I

    #@e
    rem-int/lit16 v9, v9, 0xb4

    #@10
    if-nez v9, :cond_5d

    #@12
    .line 132
    const/4 v2, 0x0

    #@13
    .line 133
    .local v2, sinTheta:F
    iget v9, p0, Landroid/filterpacks/imageproc/RotateFilter;->mAngle:I

    #@15
    rem-int/lit16 v9, v9, 0x168

    #@17
    if-nez v9, :cond_5b

    #@19
    move v0, v7

    #@1a
    .line 145
    .local v0, cosTheta:F
    :goto_1a
    new-instance v3, Landroid/filterfw/geometry/Point;

    #@1c
    neg-float v8, v0

    #@1d
    add-float/2addr v8, v2

    #@1e
    add-float/2addr v8, v7

    #@1f
    mul-float/2addr v8, v10

    #@20
    neg-float v9, v2

    #@21
    sub-float/2addr v9, v0

    #@22
    add-float/2addr v9, v7

    #@23
    mul-float/2addr v9, v10

    #@24
    invoke-direct {v3, v8, v9}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    #@27
    .line 147
    .local v3, x0:Landroid/filterfw/geometry/Point;
    new-instance v4, Landroid/filterfw/geometry/Point;

    #@29
    add-float v8, v0, v2

    #@2b
    add-float/2addr v8, v7

    #@2c
    mul-float/2addr v8, v10

    #@2d
    sub-float v9, v2, v0

    #@2f
    add-float/2addr v9, v7

    #@30
    mul-float/2addr v9, v10

    #@31
    invoke-direct {v4, v8, v9}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    #@34
    .line 149
    .local v4, x1:Landroid/filterfw/geometry/Point;
    new-instance v5, Landroid/filterfw/geometry/Point;

    #@36
    neg-float v8, v0

    #@37
    sub-float/2addr v8, v2

    #@38
    add-float/2addr v8, v7

    #@39
    mul-float/2addr v8, v10

    #@3a
    neg-float v9, v2

    #@3b
    add-float/2addr v9, v0

    #@3c
    add-float/2addr v9, v7

    #@3d
    mul-float/2addr v9, v10

    #@3e
    invoke-direct {v5, v8, v9}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    #@41
    .line 151
    .local v5, x2:Landroid/filterfw/geometry/Point;
    new-instance v6, Landroid/filterfw/geometry/Point;

    #@43
    sub-float v8, v0, v2

    #@45
    add-float/2addr v8, v7

    #@46
    mul-float/2addr v8, v10

    #@47
    add-float v9, v2, v0

    #@49
    add-float/2addr v7, v9

    #@4a
    mul-float/2addr v7, v10

    #@4b
    invoke-direct {v6, v8, v7}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    #@4e
    .line 153
    .local v6, x3:Landroid/filterfw/geometry/Point;
    new-instance v1, Landroid/filterfw/geometry/Quad;

    #@50
    invoke-direct {v1, v3, v4, v5, v6}, Landroid/filterfw/geometry/Quad;-><init>(Landroid/filterfw/geometry/Point;Landroid/filterfw/geometry/Point;Landroid/filterfw/geometry/Point;Landroid/filterfw/geometry/Point;)V

    #@53
    .line 154
    .local v1, quad:Landroid/filterfw/geometry/Quad;
    iget-object v7, p0, Landroid/filterpacks/imageproc/RotateFilter;->mProgram:Landroid/filterfw/core/Program;

    #@55
    check-cast v7, Landroid/filterfw/core/ShaderProgram;

    #@57
    invoke-virtual {v7, v1}, Landroid/filterfw/core/ShaderProgram;->setTargetRegion(Landroid/filterfw/geometry/Quad;)V

    #@5a
    .line 155
    return-void

    #@5b
    .end local v0           #cosTheta:F
    .end local v1           #quad:Landroid/filterfw/geometry/Quad;
    .end local v3           #x0:Landroid/filterfw/geometry/Point;
    .end local v4           #x1:Landroid/filterfw/geometry/Point;
    .end local v5           #x2:Landroid/filterfw/geometry/Point;
    .end local v6           #x3:Landroid/filterfw/geometry/Point;
    :cond_5b
    move v0, v8

    #@5c
    .line 133
    goto :goto_1a

    #@5d
    .line 135
    .end local v2           #sinTheta:F
    :cond_5d
    const/4 v0, 0x0

    #@5e
    .line 136
    .restart local v0       #cosTheta:F
    iget v9, p0, Landroid/filterpacks/imageproc/RotateFilter;->mAngle:I

    #@60
    add-int/lit8 v9, v9, 0x5a

    #@62
    rem-int/lit16 v9, v9, 0x168

    #@64
    if-nez v9, :cond_70

    #@66
    move v2, v8

    #@67
    .line 138
    .restart local v2       #sinTheta:F
    :goto_67
    iget v8, p0, Landroid/filterpacks/imageproc/RotateFilter;->mHeight:I

    #@69
    iput v8, p0, Landroid/filterpacks/imageproc/RotateFilter;->mOutputWidth:I

    #@6b
    .line 139
    iget v8, p0, Landroid/filterpacks/imageproc/RotateFilter;->mWidth:I

    #@6d
    iput v8, p0, Landroid/filterpacks/imageproc/RotateFilter;->mOutputHeight:I

    #@6f
    goto :goto_1a

    #@70
    .end local v2           #sinTheta:F
    :cond_70
    move v2, v7

    #@71
    .line 136
    goto :goto_67

    #@72
    .line 142
    .end local v0           #cosTheta:F
    :cond_72
    new-instance v7, Ljava/lang/RuntimeException;

    #@74
    const-string v8, "degree has to be multiply of 90."

    #@76
    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@79
    throw v7
.end method


# virtual methods
.method public fieldPortValueUpdated(Ljava/lang/String;Landroid/filterfw/core/FilterContext;)V
    .registers 4
    .parameter "name"
    .parameter "context"

    #@0
    .prologue
    .line 84
    iget-object v0, p0, Landroid/filterpacks/imageproc/RotateFilter;->mProgram:Landroid/filterfw/core/Program;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 85
    invoke-direct {p0}, Landroid/filterpacks/imageproc/RotateFilter;->updateParameters()V

    #@7
    .line 87
    :cond_7
    return-void
.end method

.method public initProgram(Landroid/filterfw/core/FilterContext;I)V
    .registers 7
    .parameter "context"
    .parameter "target"

    #@0
    .prologue
    .line 67
    packed-switch p2, :pswitch_data_34

    #@3
    .line 76
    new-instance v1, Ljava/lang/RuntimeException;

    #@5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v3, "Filter Sharpen does not support frames of target "

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    const-string v3, "!"

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@21
    throw v1

    #@22
    .line 69
    :pswitch_22
    invoke-static {p1}, Landroid/filterfw/core/ShaderProgram;->createIdentity(Landroid/filterfw/core/FilterContext;)Landroid/filterfw/core/ShaderProgram;

    #@25
    move-result-object v0

    #@26
    .line 70
    .local v0, shaderProgram:Landroid/filterfw/core/ShaderProgram;
    iget v1, p0, Landroid/filterpacks/imageproc/RotateFilter;->mTileSize:I

    #@28
    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setMaximumTileSize(I)V

    #@2b
    .line 71
    const/4 v1, 0x1

    #@2c
    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setClearsOutput(Z)V

    #@2f
    .line 72
    iput-object v0, p0, Landroid/filterpacks/imageproc/RotateFilter;->mProgram:Landroid/filterfw/core/Program;

    #@31
    .line 79
    iput p2, p0, Landroid/filterpacks/imageproc/RotateFilter;->mTarget:I

    #@33
    .line 80
    return-void

    #@34
    .line 67
    :pswitch_data_34
    .packed-switch 0x3
        :pswitch_22
    .end packed-switch
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 9
    .parameter "context"

    #@0
    .prologue
    const/4 v6, 0x3

    #@1
    .line 92
    const-string v4, "image"

    #@3
    invoke-virtual {p0, v4}, Landroid/filterpacks/imageproc/RotateFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@6
    move-result-object v0

    #@7
    .line 93
    .local v0, input:Landroid/filterfw/core/Frame;
    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@a
    move-result-object v1

    #@b
    .line 96
    .local v1, inputFormat:Landroid/filterfw/core/FrameFormat;
    iget-object v4, p0, Landroid/filterpacks/imageproc/RotateFilter;->mProgram:Landroid/filterfw/core/Program;

    #@d
    if-eqz v4, :cond_17

    #@f
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@12
    move-result v4

    #@13
    iget v5, p0, Landroid/filterpacks/imageproc/RotateFilter;->mTarget:I

    #@15
    if-eq v4, v5, :cond_1e

    #@17
    .line 97
    :cond_17
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@1a
    move-result v4

    #@1b
    invoke-virtual {p0, p1, v4}, Landroid/filterpacks/imageproc/RotateFilter;->initProgram(Landroid/filterfw/core/FilterContext;I)V

    #@1e
    .line 100
    :cond_1e
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@21
    move-result v4

    #@22
    iget v5, p0, Landroid/filterpacks/imageproc/RotateFilter;->mWidth:I

    #@24
    if-ne v4, v5, :cond_2e

    #@26
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@29
    move-result v4

    #@2a
    iget v5, p0, Landroid/filterpacks/imageproc/RotateFilter;->mHeight:I

    #@2c
    if-eq v4, v5, :cond_45

    #@2e
    .line 101
    :cond_2e
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@31
    move-result v4

    #@32
    iput v4, p0, Landroid/filterpacks/imageproc/RotateFilter;->mWidth:I

    #@34
    .line 102
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@37
    move-result v4

    #@38
    iput v4, p0, Landroid/filterpacks/imageproc/RotateFilter;->mHeight:I

    #@3a
    .line 103
    iget v4, p0, Landroid/filterpacks/imageproc/RotateFilter;->mWidth:I

    #@3c
    iput v4, p0, Landroid/filterpacks/imageproc/RotateFilter;->mOutputWidth:I

    #@3e
    .line 104
    iget v4, p0, Landroid/filterpacks/imageproc/RotateFilter;->mHeight:I

    #@40
    iput v4, p0, Landroid/filterpacks/imageproc/RotateFilter;->mOutputHeight:I

    #@42
    .line 106
    invoke-direct {p0}, Landroid/filterpacks/imageproc/RotateFilter;->updateParameters()V

    #@45
    .line 110
    :cond_45
    iget v4, p0, Landroid/filterpacks/imageproc/RotateFilter;->mOutputWidth:I

    #@47
    iget v5, p0, Landroid/filterpacks/imageproc/RotateFilter;->mOutputHeight:I

    #@49
    invoke-static {v4, v5, v6, v6}, Landroid/filterfw/format/ImageFormat;->create(IIII)Landroid/filterfw/core/MutableFrameFormat;

    #@4c
    move-result-object v3

    #@4d
    .line 114
    .local v3, outputFormat:Landroid/filterfw/core/FrameFormat;
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@50
    move-result-object v4

    #@51
    invoke-virtual {v4, v3}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@54
    move-result-object v2

    #@55
    .line 117
    .local v2, output:Landroid/filterfw/core/Frame;
    iget-object v4, p0, Landroid/filterpacks/imageproc/RotateFilter;->mProgram:Landroid/filterfw/core/Program;

    #@57
    invoke-virtual {v4, v0, v2}, Landroid/filterfw/core/Program;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@5a
    .line 120
    const-string v4, "image"

    #@5c
    invoke-virtual {p0, v4, v2}, Landroid/filterpacks/imageproc/RotateFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@5f
    .line 123
    invoke-virtual {v2}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@62
    .line 124
    return-void
.end method

.method public setupPorts()V
    .registers 3

    #@0
    .prologue
    .line 62
    const-string v0, "image"

    #@2
    const/4 v1, 0x3

    #@3
    invoke-static {v1}, Landroid/filterfw/format/ImageFormat;->create(I)Landroid/filterfw/core/MutableFrameFormat;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/RotateFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@a
    .line 63
    const-string v0, "image"

    #@c
    const-string v1, "image"

    #@e
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/RotateFilter;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 64
    return-void
.end method
