.class public abstract Landroid/filterpacks/imageproc/ImageCombineFilter;
.super Landroid/filterfw/core/Filter;
.source "ImageCombineFilter.java"


# instance fields
.field protected mCurrentTarget:I

.field protected mInputNames:[Ljava/lang/String;

.field protected mOutputName:Ljava/lang/String;

.field protected mParameterName:Ljava/lang/String;

.field protected mProgram:Landroid/filterfw/core/Program;


# direct methods
.method public constructor <init>(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "name"
    .parameter "inputNames"
    .parameter "outputName"
    .parameter "parameterName"

    #@0
    .prologue
    .line 50
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@3
    .line 44
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/filterpacks/imageproc/ImageCombineFilter;->mCurrentTarget:I

    #@6
    .line 51
    iput-object p2, p0, Landroid/filterpacks/imageproc/ImageCombineFilter;->mInputNames:[Ljava/lang/String;

    #@8
    .line 52
    iput-object p3, p0, Landroid/filterpacks/imageproc/ImageCombineFilter;->mOutputName:Ljava/lang/String;

    #@a
    .line 53
    iput-object p4, p0, Landroid/filterpacks/imageproc/ImageCombineFilter;->mParameterName:Ljava/lang/String;

    #@c
    .line 54
    return-void
.end method

.method private assertAllInputTargetsMatch()V
    .registers 9

    #@0
    .prologue
    .line 78
    iget-object v5, p0, Landroid/filterpacks/imageproc/ImageCombineFilter;->mInputNames:[Ljava/lang/String;

    #@2
    const/4 v6, 0x0

    #@3
    aget-object v5, v5, v6

    #@5
    invoke-virtual {p0, v5}, Landroid/filterpacks/imageproc/ImageCombineFilter;->getInputFormat(Ljava/lang/String;)Landroid/filterfw/core/FrameFormat;

    #@8
    move-result-object v5

    #@9
    invoke-virtual {v5}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@c
    move-result v4

    #@d
    .line 79
    .local v4, target:I
    iget-object v0, p0, Landroid/filterpacks/imageproc/ImageCombineFilter;->mInputNames:[Ljava/lang/String;

    #@f
    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    #@10
    .local v3, len$:I
    const/4 v1, 0x0

    #@11
    .local v1, i$:I
    :goto_11
    if-ge v1, v3, :cond_41

    #@13
    aget-object v2, v0, v1

    #@15
    .line 80
    .local v2, inputName:Ljava/lang/String;
    invoke-virtual {p0, v2}, Landroid/filterpacks/imageproc/ImageCombineFilter;->getInputFormat(Ljava/lang/String;)Landroid/filterfw/core/FrameFormat;

    #@18
    move-result-object v5

    #@19
    invoke-virtual {v5}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@1c
    move-result v5

    #@1d
    if-eq v4, v5, :cond_3e

    #@1f
    .line 81
    new-instance v5, Ljava/lang/RuntimeException;

    #@21
    new-instance v6, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v7, "Type mismatch of input formats in filter "

    #@28
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v6

    #@2c
    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v6

    #@30
    const-string v7, ". All input frames must have the same target!"

    #@32
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v6

    #@36
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v6

    #@3a
    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@3d
    throw v5

    #@3e
    .line 79
    :cond_3e
    add-int/lit8 v1, v1, 0x1

    #@40
    goto :goto_11

    #@41
    .line 85
    .end local v2           #inputName:Ljava/lang/String;
    :cond_41
    return-void
.end method


# virtual methods
.method protected abstract getNativeProgram(Landroid/filterfw/core/FilterContext;)Landroid/filterfw/core/Program;
.end method

.method public getOutputFormat(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .registers 3
    .parameter "portName"
    .parameter "inputFormat"

    #@0
    .prologue
    .line 74
    return-object p2
.end method

.method protected abstract getShaderProgram(Landroid/filterfw/core/FilterContext;)Landroid/filterfw/core/Program;
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 13
    .parameter "context"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    .line 90
    const/4 v1, 0x0

    #@2
    .line 91
    .local v1, i:I
    iget-object v8, p0, Landroid/filterpacks/imageproc/ImageCombineFilter;->mInputNames:[Ljava/lang/String;

    #@4
    array-length v8, v8

    #@5
    new-array v5, v8, [Landroid/filterfw/core/Frame;

    #@7
    .line 92
    .local v5, inputs:[Landroid/filterfw/core/Frame;
    iget-object v0, p0, Landroid/filterpacks/imageproc/ImageCombineFilter;->mInputNames:[Ljava/lang/String;

    #@9
    .local v0, arr$:[Ljava/lang/String;
    array-length v6, v0

    #@a
    .local v6, len$:I
    const/4 v3, 0x0

    #@b
    .local v3, i$:I
    move v2, v1

    #@c
    .end local v1           #i:I
    .local v2, i:I
    :goto_c
    if-ge v3, v6, :cond_1c

    #@e
    aget-object v4, v0, v3

    #@10
    .line 93
    .local v4, inputName:Ljava/lang/String;
    add-int/lit8 v1, v2, 0x1

    #@12
    .end local v2           #i:I
    .restart local v1       #i:I
    invoke-virtual {p0, v4}, Landroid/filterpacks/imageproc/ImageCombineFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@15
    move-result-object v8

    #@16
    aput-object v8, v5, v2

    #@18
    .line 92
    add-int/lit8 v3, v3, 0x1

    #@1a
    move v2, v1

    #@1b
    .end local v1           #i:I
    .restart local v2       #i:I
    goto :goto_c

    #@1c
    .line 97
    .end local v4           #inputName:Ljava/lang/String;
    :cond_1c
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@1f
    move-result-object v8

    #@20
    aget-object v9, v5, v10

    #@22
    invoke-virtual {v9}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@25
    move-result-object v9

    #@26
    invoke-virtual {v8, v9}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@29
    move-result-object v7

    #@2a
    .line 100
    .local v7, output:Landroid/filterfw/core/Frame;
    aget-object v8, v5, v10

    #@2c
    invoke-virtual {v8}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@2f
    move-result-object v8

    #@30
    invoke-virtual {v8}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@33
    move-result v8

    #@34
    invoke-virtual {p0, v8, p1}, Landroid/filterpacks/imageproc/ImageCombineFilter;->updateProgramWithTarget(ILandroid/filterfw/core/FilterContext;)V

    #@37
    .line 103
    iget-object v8, p0, Landroid/filterpacks/imageproc/ImageCombineFilter;->mProgram:Landroid/filterfw/core/Program;

    #@39
    invoke-virtual {v8, v5, v7}, Landroid/filterfw/core/Program;->process([Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@3c
    .line 106
    iget-object v8, p0, Landroid/filterpacks/imageproc/ImageCombineFilter;->mOutputName:Ljava/lang/String;

    #@3e
    invoke-virtual {p0, v8, v7}, Landroid/filterpacks/imageproc/ImageCombineFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@41
    .line 109
    invoke-virtual {v7}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@44
    .line 110
    return-void
.end method

.method public setupPorts()V
    .registers 13

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    .line 58
    iget-object v0, p0, Landroid/filterpacks/imageproc/ImageCombineFilter;->mParameterName:Ljava/lang/String;

    #@3
    if-eqz v0, :cond_19

    #@5
    .line 60
    :try_start_5
    const-class v0, Landroid/filterpacks/imageproc/ImageCombineFilter;

    #@7
    const-string/jumbo v1, "mProgram"

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@d
    move-result-object v3

    #@e
    .line 61
    .local v3, programField:Ljava/lang/reflect/Field;
    iget-object v1, p0, Landroid/filterpacks/imageproc/ImageCombineFilter;->mParameterName:Ljava/lang/String;

    #@10
    iget-object v2, p0, Landroid/filterpacks/imageproc/ImageCombineFilter;->mParameterName:Ljava/lang/String;

    #@12
    sget-object v4, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    #@14
    const/4 v5, 0x0

    #@15
    move-object v0, p0

    #@16
    invoke-virtual/range {v0 .. v5}, Landroid/filterpacks/imageproc/ImageCombineFilter;->addProgramPort(Ljava/lang/String;Ljava/lang/String;Ljava/lang/reflect/Field;Ljava/lang/Class;Z)V
    :try_end_19
    .catch Ljava/lang/NoSuchFieldException; {:try_start_5 .. :try_end_19} :catch_2c

    #@19
    .line 66
    .end local v3           #programField:Ljava/lang/reflect/Field;
    :cond_19
    iget-object v6, p0, Landroid/filterpacks/imageproc/ImageCombineFilter;->mInputNames:[Ljava/lang/String;

    #@1b
    .local v6, arr$:[Ljava/lang/String;
    array-length v10, v6

    #@1c
    .local v10, len$:I
    const/4 v8, 0x0

    #@1d
    .local v8, i$:I
    :goto_1d
    if-ge v8, v10, :cond_35

    #@1f
    aget-object v9, v6, v8

    #@21
    .line 67
    .local v9, inputName:Ljava/lang/String;
    const/4 v0, 0x3

    #@22
    invoke-static {v0}, Landroid/filterfw/format/ImageFormat;->create(I)Landroid/filterfw/core/MutableFrameFormat;

    #@25
    move-result-object v0

    #@26
    invoke-virtual {p0, v9, v0}, Landroid/filterpacks/imageproc/ImageCombineFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@29
    .line 66
    add-int/lit8 v8, v8, 0x1

    #@2b
    goto :goto_1d

    #@2c
    .line 62
    .end local v6           #arr$:[Ljava/lang/String;
    .end local v8           #i$:I
    .end local v9           #inputName:Ljava/lang/String;
    .end local v10           #len$:I
    :catch_2c
    move-exception v7

    #@2d
    .line 63
    .local v7, e:Ljava/lang/NoSuchFieldException;
    new-instance v0, Ljava/lang/RuntimeException;

    #@2f
    const-string v1, "Internal Error: mProgram field not found!"

    #@31
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@34
    throw v0

    #@35
    .line 69
    .end local v7           #e:Ljava/lang/NoSuchFieldException;
    .restart local v6       #arr$:[Ljava/lang/String;
    .restart local v8       #i$:I
    .restart local v10       #len$:I
    :cond_35
    iget-object v0, p0, Landroid/filterpacks/imageproc/ImageCombineFilter;->mOutputName:Ljava/lang/String;

    #@37
    iget-object v1, p0, Landroid/filterpacks/imageproc/ImageCombineFilter;->mInputNames:[Ljava/lang/String;

    #@39
    aget-object v1, v1, v11

    #@3b
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/ImageCombineFilter;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    #@3e
    .line 70
    return-void
.end method

.method protected updateProgramWithTarget(ILandroid/filterfw/core/FilterContext;)V
    .registers 6
    .parameter "target"
    .parameter "context"

    #@0
    .prologue
    .line 113
    iget v0, p0, Landroid/filterpacks/imageproc/ImageCombineFilter;->mCurrentTarget:I

    #@2
    if-eq p1, v0, :cond_42

    #@4
    .line 114
    packed-switch p1, :pswitch_data_44

    #@7
    .line 124
    const/4 v0, 0x0

    #@8
    iput-object v0, p0, Landroid/filterpacks/imageproc/ImageCombineFilter;->mProgram:Landroid/filterfw/core/Program;

    #@a
    .line 127
    :goto_a
    iget-object v0, p0, Landroid/filterpacks/imageproc/ImageCombineFilter;->mProgram:Landroid/filterfw/core/Program;

    #@c
    if-nez v0, :cond_3b

    #@e
    .line 128
    new-instance v0, Ljava/lang/RuntimeException;

    #@10
    new-instance v1, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v2, "Could not create a program for image filter "

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    const-string v2, "!"

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v0

    #@2d
    .line 116
    :pswitch_2d
    invoke-virtual {p0, p2}, Landroid/filterpacks/imageproc/ImageCombineFilter;->getNativeProgram(Landroid/filterfw/core/FilterContext;)Landroid/filterfw/core/Program;

    #@30
    move-result-object v0

    #@31
    iput-object v0, p0, Landroid/filterpacks/imageproc/ImageCombineFilter;->mProgram:Landroid/filterfw/core/Program;

    #@33
    goto :goto_a

    #@34
    .line 120
    :pswitch_34
    invoke-virtual {p0, p2}, Landroid/filterpacks/imageproc/ImageCombineFilter;->getShaderProgram(Landroid/filterfw/core/FilterContext;)Landroid/filterfw/core/Program;

    #@37
    move-result-object v0

    #@38
    iput-object v0, p0, Landroid/filterpacks/imageproc/ImageCombineFilter;->mProgram:Landroid/filterfw/core/Program;

    #@3a
    goto :goto_a

    #@3b
    .line 131
    :cond_3b
    iget-object v0, p0, Landroid/filterpacks/imageproc/ImageCombineFilter;->mProgram:Landroid/filterfw/core/Program;

    #@3d
    invoke-virtual {p0, v0, p2}, Landroid/filterpacks/imageproc/ImageCombineFilter;->initProgramInputs(Landroid/filterfw/core/Program;Landroid/filterfw/core/FilterContext;)V

    #@40
    .line 132
    iput p1, p0, Landroid/filterpacks/imageproc/ImageCombineFilter;->mCurrentTarget:I

    #@42
    .line 134
    :cond_42
    return-void

    #@43
    .line 114
    nop

    #@44
    :pswitch_data_44
    .packed-switch 0x2
        :pswitch_2d
        :pswitch_34
    .end packed-switch
.end method
