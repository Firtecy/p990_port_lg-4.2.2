.class public Landroid/filterpacks/imageproc/ImageSlicer;
.super Landroid/filterfw/core/Filter;
.source "ImageSlicer.java"


# instance fields
.field private mInputHeight:I

.field private mInputWidth:I

.field private mOriginalFrame:Landroid/filterfw/core/Frame;

.field private mOutputHeight:I

.field private mOutputWidth:I

.field private mPadSize:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "padSize"
    .end annotation
.end field

.field private mProgram:Landroid/filterfw/core/Program;

.field private mSliceHeight:I

.field private mSliceIndex:I

.field private mSliceWidth:I

.field private mXSlices:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "xSlices"
    .end annotation
.end field

.field private mYSlices:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "ySlices"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 59
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@3
    .line 60
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mSliceIndex:I

    #@6
    .line 61
    return-void
.end method

.method private calcOutputFormatForInput(Landroid/filterfw/core/Frame;)V
    .registers 4
    .parameter "frame"

    #@0
    .prologue
    .line 78
    invoke-virtual {p1}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@7
    move-result v0

    #@8
    iput v0, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mInputWidth:I

    #@a
    .line 79
    invoke-virtual {p1}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@d
    move-result-object v0

    #@e
    invoke-virtual {v0}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@11
    move-result v0

    #@12
    iput v0, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mInputHeight:I

    #@14
    .line 81
    iget v0, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mInputWidth:I

    #@16
    iget v1, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mXSlices:I

    #@18
    add-int/2addr v0, v1

    #@19
    add-int/lit8 v0, v0, -0x1

    #@1b
    iget v1, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mXSlices:I

    #@1d
    div-int/2addr v0, v1

    #@1e
    iput v0, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mSliceWidth:I

    #@20
    .line 82
    iget v0, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mInputHeight:I

    #@22
    iget v1, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mYSlices:I

    #@24
    add-int/2addr v0, v1

    #@25
    add-int/lit8 v0, v0, -0x1

    #@27
    iget v1, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mYSlices:I

    #@29
    div-int/2addr v0, v1

    #@2a
    iput v0, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mSliceHeight:I

    #@2c
    .line 84
    iget v0, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mSliceWidth:I

    #@2e
    iget v1, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mPadSize:I

    #@30
    mul-int/lit8 v1, v1, 0x2

    #@32
    add-int/2addr v0, v1

    #@33
    iput v0, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mOutputWidth:I

    #@35
    .line 85
    iget v0, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mSliceHeight:I

    #@37
    iget v1, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mPadSize:I

    #@39
    mul-int/lit8 v1, v1, 0x2

    #@3b
    add-int/2addr v0, v1

    #@3c
    iput v0, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mOutputHeight:I

    #@3e
    .line 86
    return-void
.end method


# virtual methods
.method public getOutputFormat(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .registers 3
    .parameter "portName"
    .parameter "inputFormat"

    #@0
    .prologue
    .line 72
    return-object p2
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 14
    .parameter "context"

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    .line 93
    iget v7, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mSliceIndex:I

    #@3
    if-nez v7, :cond_12

    #@5
    .line 94
    const-string v7, "image"

    #@7
    invoke-virtual {p0, v7}, Landroid/filterpacks/imageproc/ImageSlicer;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@a
    move-result-object v7

    #@b
    iput-object v7, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mOriginalFrame:Landroid/filterfw/core/Frame;

    #@d
    .line 95
    iget-object v7, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mOriginalFrame:Landroid/filterfw/core/Frame;

    #@f
    invoke-direct {p0, v7}, Landroid/filterpacks/imageproc/ImageSlicer;->calcOutputFormatForInput(Landroid/filterfw/core/Frame;)V

    #@12
    .line 98
    :cond_12
    iget-object v7, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mOriginalFrame:Landroid/filterfw/core/Frame;

    #@14
    invoke-virtual {v7}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@17
    move-result-object v0

    #@18
    .line 99
    .local v0, inputFormat:Landroid/filterfw/core/FrameFormat;
    invoke-virtual {v0}, Landroid/filterfw/core/FrameFormat;->mutableCopy()Landroid/filterfw/core/MutableFrameFormat;

    #@1b
    move-result-object v2

    #@1c
    .line 100
    .local v2, outputFormat:Landroid/filterfw/core/MutableFrameFormat;
    iget v7, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mOutputWidth:I

    #@1e
    iget v8, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mOutputHeight:I

    #@20
    invoke-virtual {v2, v7, v8}, Landroid/filterfw/core/MutableFrameFormat;->setDimensions(II)V

    #@23
    .line 103
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@26
    move-result-object v7

    #@27
    invoke-virtual {v7, v2}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@2a
    move-result-object v1

    #@2b
    .line 106
    .local v1, output:Landroid/filterfw/core/Frame;
    iget-object v7, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mProgram:Landroid/filterfw/core/Program;

    #@2d
    if-nez v7, :cond_35

    #@2f
    .line 107
    invoke-static {p1}, Landroid/filterfw/core/ShaderProgram;->createIdentity(Landroid/filterfw/core/FilterContext;)Landroid/filterfw/core/ShaderProgram;

    #@32
    move-result-object v7

    #@33
    iput-object v7, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mProgram:Landroid/filterfw/core/Program;

    #@35
    .line 111
    :cond_35
    iget v7, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mSliceIndex:I

    #@37
    iget v8, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mXSlices:I

    #@39
    rem-int v4, v7, v8

    #@3b
    .line 112
    .local v4, xSliceIndex:I
    iget v7, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mSliceIndex:I

    #@3d
    iget v8, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mXSlices:I

    #@3f
    div-int v6, v7, v8

    #@41
    .line 115
    .local v6, ySliceIndex:I
    iget v7, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mSliceWidth:I

    #@43
    mul-int/2addr v7, v4

    #@44
    iget v8, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mPadSize:I

    #@46
    sub-int/2addr v7, v8

    #@47
    int-to-float v7, v7

    #@48
    iget v8, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mInputWidth:I

    #@4a
    int-to-float v8, v8

    #@4b
    div-float v3, v7, v8

    #@4d
    .line 116
    .local v3, x0:F
    iget v7, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mSliceHeight:I

    #@4f
    mul-int/2addr v7, v6

    #@50
    iget v8, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mPadSize:I

    #@52
    sub-int/2addr v7, v8

    #@53
    int-to-float v7, v7

    #@54
    iget v8, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mInputHeight:I

    #@56
    int-to-float v8, v8

    #@57
    div-float v5, v7, v8

    #@59
    .line 118
    .local v5, y0:F
    iget-object v7, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mProgram:Landroid/filterfw/core/Program;

    #@5b
    check-cast v7, Landroid/filterfw/core/ShaderProgram;

    #@5d
    iget v8, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mOutputWidth:I

    #@5f
    int-to-float v8, v8

    #@60
    iget v9, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mInputWidth:I

    #@62
    int-to-float v9, v9

    #@63
    div-float/2addr v8, v9

    #@64
    iget v9, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mOutputHeight:I

    #@66
    int-to-float v9, v9

    #@67
    iget v10, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mInputHeight:I

    #@69
    int-to-float v10, v10

    #@6a
    div-float/2addr v9, v10

    #@6b
    invoke-virtual {v7, v3, v5, v8, v9}, Landroid/filterfw/core/ShaderProgram;->setSourceRect(FFFF)V

    #@6e
    .line 123
    iget-object v7, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mProgram:Landroid/filterfw/core/Program;

    #@70
    iget-object v8, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mOriginalFrame:Landroid/filterfw/core/Frame;

    #@72
    invoke-virtual {v7, v8, v1}, Landroid/filterfw/core/Program;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@75
    .line 124
    iget v7, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mSliceIndex:I

    #@77
    add-int/lit8 v7, v7, 0x1

    #@79
    iput v7, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mSliceIndex:I

    #@7b
    .line 126
    iget v7, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mSliceIndex:I

    #@7d
    iget v8, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mXSlices:I

    #@7f
    iget v9, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mYSlices:I

    #@81
    mul-int/2addr v8, v9

    #@82
    if-ne v7, v8, :cond_9a

    #@84
    .line 127
    iput v11, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mSliceIndex:I

    #@86
    .line 128
    iget-object v7, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mOriginalFrame:Landroid/filterfw/core/Frame;

    #@88
    invoke-virtual {v7}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@8b
    .line 129
    const-string v7, "image"

    #@8d
    const/4 v8, 0x1

    #@8e
    invoke-virtual {p0, v7, v8}, Landroid/filterpacks/imageproc/ImageSlicer;->setWaitsOnInputPort(Ljava/lang/String;Z)V

    #@91
    .line 137
    :goto_91
    const-string v7, "image"

    #@93
    invoke-virtual {p0, v7, v1}, Landroid/filterpacks/imageproc/ImageSlicer;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@96
    .line 140
    invoke-virtual {v1}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@99
    .line 141
    return-void

    #@9a
    .line 132
    :cond_9a
    iget-object v7, p0, Landroid/filterpacks/imageproc/ImageSlicer;->mOriginalFrame:Landroid/filterfw/core/Frame;

    #@9c
    invoke-virtual {v7}, Landroid/filterfw/core/Frame;->retain()Landroid/filterfw/core/Frame;

    #@9f
    .line 133
    const-string v7, "image"

    #@a1
    invoke-virtual {p0, v7, v11}, Landroid/filterpacks/imageproc/ImageSlicer;->setWaitsOnInputPort(Ljava/lang/String;Z)V

    #@a4
    goto :goto_91
.end method

.method public setupPorts()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x3

    #@1
    .line 65
    const-string v0, "image"

    #@3
    invoke-static {v1, v1}, Landroid/filterfw/format/ImageFormat;->create(II)Landroid/filterfw/core/MutableFrameFormat;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/ImageSlicer;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@a
    .line 67
    const-string v0, "image"

    #@c
    const-string v1, "image"

    #@e
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/ImageSlicer;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 68
    return-void
.end method
