.class public Landroid/filterpacks/imageproc/DocumentaryFilter;
.super Landroid/filterfw/core/Filter;
.source "DocumentaryFilter.java"


# instance fields
.field private final mDocumentaryShader:Ljava/lang/String;

.field private mHeight:I

.field private mProgram:Landroid/filterfw/core/Program;

.field private mRandom:Ljava/util/Random;

.field private mTarget:I

.field private mTileSize:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "tile_size"
    .end annotation
.end field

.field private mWidth:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 6
    .parameter "name"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 83
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@4
    .line 36
    const/16 v1, 0x280

    #@6
    iput v1, p0, Landroid/filterpacks/imageproc/DocumentaryFilter;->mTileSize:I

    #@8
    .line 42
    iput v2, p0, Landroid/filterpacks/imageproc/DocumentaryFilter;->mWidth:I

    #@a
    .line 43
    iput v2, p0, Landroid/filterpacks/imageproc/DocumentaryFilter;->mHeight:I

    #@c
    .line 44
    iput v2, p0, Landroid/filterpacks/imageproc/DocumentaryFilter;->mTarget:I

    #@e
    .line 46
    const-string/jumbo v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform vec2 seed;\nuniform float stepsize;\nuniform float inv_max_dist;\nuniform vec2 scale;\nvarying vec2 v_texcoord;\nfloat rand(vec2 loc) {\n  float theta1 = dot(loc, vec2(0.9898, 0.233));\n  float theta2 = dot(loc, vec2(12.0, 78.0));\n  float value = cos(theta1) * sin(theta2) + sin(theta1) * cos(theta2);\n  float temp = mod(197.0 * value, 1.0) + value;\n  float part1 = mod(220.0 * temp, 1.0) + temp;\n  float part2 = value * 0.5453;\n  float part3 = cos(theta1 + theta2) * 0.43758;\n  return fract(part1 + part2 + part3);\n}\nvoid main() {\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  float dither = rand(v_texcoord + seed);\n  vec3 xform = clamp(2.0 * color.rgb, 0.0, 1.0);\n  vec3 temp = clamp(2.0 * (color.rgb + stepsize), 0.0, 1.0);\n  vec3 new_color = clamp(xform + (temp - xform) * (dither - 0.5), 0.0, 1.0);\n  float gray = dot(new_color, vec3(0.299, 0.587, 0.114));\n  new_color = vec3(gray, gray, gray);\n  vec2 coord = v_texcoord - vec2(0.5, 0.5);\n  float dist = length(coord * scale);\n  float lumen = 0.85 / (1.0 + exp((dist * inv_max_dist - 0.83) * 20.0)) + 0.15;\n  gl_FragColor = vec4(new_color * lumen, color.a);\n}\n"

    #@11
    iput-object v1, p0, Landroid/filterpacks/imageproc/DocumentaryFilter;->mDocumentaryShader:Ljava/lang/String;

    #@13
    .line 84
    new-instance v0, Ljava/util/Date;

    #@15
    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    #@18
    .line 85
    .local v0, date:Ljava/util/Date;
    new-instance v1, Ljava/util/Random;

    #@1a
    new-instance v2, Ljava/util/Date;

    #@1c
    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    #@1f
    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    #@22
    move-result-wide v2

    #@23
    invoke-direct {v1, v2, v3}, Ljava/util/Random;-><init>(J)V

    #@26
    iput-object v1, p0, Landroid/filterpacks/imageproc/DocumentaryFilter;->mRandom:Ljava/util/Random;

    #@28
    .line 86
    return-void
.end method

.method private initParameters()V
    .registers 11

    #@0
    .prologue
    const/4 v9, 0x2

    #@1
    const/high16 v8, 0x3f80

    #@3
    const/4 v7, 0x1

    #@4
    const/4 v6, 0x0

    #@5
    .line 146
    iget-object v3, p0, Landroid/filterpacks/imageproc/DocumentaryFilter;->mProgram:Landroid/filterfw/core/Program;

    #@7
    if-eqz v3, :cond_6f

    #@9
    .line 147
    new-array v1, v9, [F

    #@b
    .line 148
    .local v1, scale:[F
    iget v3, p0, Landroid/filterpacks/imageproc/DocumentaryFilter;->mWidth:I

    #@d
    iget v4, p0, Landroid/filterpacks/imageproc/DocumentaryFilter;->mHeight:I

    #@f
    if-le v3, v4, :cond_70

    #@11
    .line 149
    aput v8, v1, v6

    #@13
    .line 150
    iget v3, p0, Landroid/filterpacks/imageproc/DocumentaryFilter;->mHeight:I

    #@15
    int-to-float v3, v3

    #@16
    iget v4, p0, Landroid/filterpacks/imageproc/DocumentaryFilter;->mWidth:I

    #@18
    int-to-float v4, v4

    #@19
    div-float/2addr v3, v4

    #@1a
    aput v3, v1, v7

    #@1c
    .line 155
    :goto_1c
    aget v3, v1, v6

    #@1e
    aget v4, v1, v6

    #@20
    mul-float/2addr v3, v4

    #@21
    aget v4, v1, v7

    #@23
    aget v5, v1, v7

    #@25
    mul-float/2addr v4, v5

    #@26
    add-float/2addr v3, v4

    #@27
    float-to-double v3, v3

    #@28
    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    #@2b
    move-result-wide v3

    #@2c
    double-to-float v3, v3

    #@2d
    const/high16 v4, 0x3f00

    #@2f
    mul-float v0, v3, v4

    #@31
    .line 157
    .local v0, max_dist:F
    iget-object v3, p0, Landroid/filterpacks/imageproc/DocumentaryFilter;->mProgram:Landroid/filterfw/core/Program;

    #@33
    const-string/jumbo v4, "scale"

    #@36
    invoke-virtual {v3, v4, v1}, Landroid/filterfw/core/Program;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@39
    .line 158
    iget-object v3, p0, Landroid/filterpacks/imageproc/DocumentaryFilter;->mProgram:Landroid/filterfw/core/Program;

    #@3b
    const-string v4, "inv_max_dist"

    #@3d
    div-float v5, v8, v0

    #@3f
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@42
    move-result-object v5

    #@43
    invoke-virtual {v3, v4, v5}, Landroid/filterfw/core/Program;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@46
    .line 159
    iget-object v3, p0, Landroid/filterpacks/imageproc/DocumentaryFilter;->mProgram:Landroid/filterfw/core/Program;

    #@48
    const-string/jumbo v4, "stepsize"

    #@4b
    const v5, 0x3b808081

    #@4e
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@51
    move-result-object v5

    #@52
    invoke-virtual {v3, v4, v5}, Landroid/filterfw/core/Program;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@55
    .line 161
    new-array v2, v9, [F

    #@57
    iget-object v3, p0, Landroid/filterpacks/imageproc/DocumentaryFilter;->mRandom:Ljava/util/Random;

    #@59
    invoke-virtual {v3}, Ljava/util/Random;->nextFloat()F

    #@5c
    move-result v3

    #@5d
    aput v3, v2, v6

    #@5f
    iget-object v3, p0, Landroid/filterpacks/imageproc/DocumentaryFilter;->mRandom:Ljava/util/Random;

    #@61
    invoke-virtual {v3}, Ljava/util/Random;->nextFloat()F

    #@64
    move-result v3

    #@65
    aput v3, v2, v7

    #@67
    .line 162
    .local v2, seed:[F
    iget-object v3, p0, Landroid/filterpacks/imageproc/DocumentaryFilter;->mProgram:Landroid/filterfw/core/Program;

    #@69
    const-string/jumbo v4, "seed"

    #@6c
    invoke-virtual {v3, v4, v2}, Landroid/filterfw/core/Program;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@6f
    .line 164
    .end local v0           #max_dist:F
    .end local v1           #scale:[F
    .end local v2           #seed:[F
    :cond_6f
    return-void

    #@70
    .line 152
    .restart local v1       #scale:[F
    :cond_70
    iget v3, p0, Landroid/filterpacks/imageproc/DocumentaryFilter;->mWidth:I

    #@72
    int-to-float v3, v3

    #@73
    iget v4, p0, Landroid/filterpacks/imageproc/DocumentaryFilter;->mHeight:I

    #@75
    int-to-float v4, v4

    #@76
    div-float/2addr v3, v4

    #@77
    aput v3, v1, v6

    #@79
    .line 153
    aput v8, v1, v7

    #@7b
    goto :goto_1c
.end method


# virtual methods
.method public getOutputFormat(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .registers 3
    .parameter "portName"
    .parameter "inputFormat"

    #@0
    .prologue
    .line 96
    return-object p2
.end method

.method public initProgram(Landroid/filterfw/core/FilterContext;I)V
    .registers 7
    .parameter "context"
    .parameter "target"

    #@0
    .prologue
    .line 100
    packed-switch p2, :pswitch_data_34

    #@3
    .line 108
    new-instance v1, Ljava/lang/RuntimeException;

    #@5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v3, "Filter Sharpen does not support frames of target "

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    const-string v3, "!"

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@21
    throw v1

    #@22
    .line 102
    :pswitch_22
    new-instance v0, Landroid/filterfw/core/ShaderProgram;

    #@24
    const-string/jumbo v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform vec2 seed;\nuniform float stepsize;\nuniform float inv_max_dist;\nuniform vec2 scale;\nvarying vec2 v_texcoord;\nfloat rand(vec2 loc) {\n  float theta1 = dot(loc, vec2(0.9898, 0.233));\n  float theta2 = dot(loc, vec2(12.0, 78.0));\n  float value = cos(theta1) * sin(theta2) + sin(theta1) * cos(theta2);\n  float temp = mod(197.0 * value, 1.0) + value;\n  float part1 = mod(220.0 * temp, 1.0) + temp;\n  float part2 = value * 0.5453;\n  float part3 = cos(theta1 + theta2) * 0.43758;\n  return fract(part1 + part2 + part3);\n}\nvoid main() {\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  float dither = rand(v_texcoord + seed);\n  vec3 xform = clamp(2.0 * color.rgb, 0.0, 1.0);\n  vec3 temp = clamp(2.0 * (color.rgb + stepsize), 0.0, 1.0);\n  vec3 new_color = clamp(xform + (temp - xform) * (dither - 0.5), 0.0, 1.0);\n  float gray = dot(new_color, vec3(0.299, 0.587, 0.114));\n  new_color = vec3(gray, gray, gray);\n  vec2 coord = v_texcoord - vec2(0.5, 0.5);\n  float dist = length(coord * scale);\n  float lumen = 0.85 / (1.0 + exp((dist * inv_max_dist - 0.83) * 20.0)) + 0.15;\n  gl_FragColor = vec4(new_color * lumen, color.a);\n}\n"

    #@27
    invoke-direct {v0, p1, v1}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;)V

    #@2a
    .line 103
    .local v0, shaderProgram:Landroid/filterfw/core/ShaderProgram;
    iget v1, p0, Landroid/filterpacks/imageproc/DocumentaryFilter;->mTileSize:I

    #@2c
    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setMaximumTileSize(I)V

    #@2f
    .line 104
    iput-object v0, p0, Landroid/filterpacks/imageproc/DocumentaryFilter;->mProgram:Landroid/filterfw/core/Program;

    #@31
    .line 111
    iput p2, p0, Landroid/filterpacks/imageproc/DocumentaryFilter;->mTarget:I

    #@33
    .line 112
    return-void

    #@34
    .line 100
    :pswitch_data_34
    .packed-switch 0x3
        :pswitch_22
    .end packed-switch
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 7
    .parameter "context"

    #@0
    .prologue
    .line 117
    const-string v3, "image"

    #@2
    invoke-virtual {p0, v3}, Landroid/filterpacks/imageproc/DocumentaryFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@5
    move-result-object v0

    #@6
    .line 118
    .local v0, input:Landroid/filterfw/core/Frame;
    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@9
    move-result-object v1

    #@a
    .line 121
    .local v1, inputFormat:Landroid/filterfw/core/FrameFormat;
    iget-object v3, p0, Landroid/filterpacks/imageproc/DocumentaryFilter;->mProgram:Landroid/filterfw/core/Program;

    #@c
    if-eqz v3, :cond_16

    #@e
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@11
    move-result v3

    #@12
    iget v4, p0, Landroid/filterpacks/imageproc/DocumentaryFilter;->mTarget:I

    #@14
    if-eq v3, v4, :cond_1d

    #@16
    .line 122
    :cond_16
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@19
    move-result v3

    #@1a
    invoke-virtual {p0, p1, v3}, Landroid/filterpacks/imageproc/DocumentaryFilter;->initProgram(Landroid/filterfw/core/FilterContext;I)V

    #@1d
    .line 126
    :cond_1d
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@20
    move-result v3

    #@21
    iget v4, p0, Landroid/filterpacks/imageproc/DocumentaryFilter;->mWidth:I

    #@23
    if-ne v3, v4, :cond_2d

    #@25
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@28
    move-result v3

    #@29
    iget v4, p0, Landroid/filterpacks/imageproc/DocumentaryFilter;->mHeight:I

    #@2b
    if-eq v3, v4, :cond_3c

    #@2d
    .line 127
    :cond_2d
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@30
    move-result v3

    #@31
    iput v3, p0, Landroid/filterpacks/imageproc/DocumentaryFilter;->mWidth:I

    #@33
    .line 128
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@36
    move-result v3

    #@37
    iput v3, p0, Landroid/filterpacks/imageproc/DocumentaryFilter;->mHeight:I

    #@39
    .line 129
    invoke-direct {p0}, Landroid/filterpacks/imageproc/DocumentaryFilter;->initParameters()V

    #@3c
    .line 133
    :cond_3c
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v3, v1}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@43
    move-result-object v2

    #@44
    .line 136
    .local v2, output:Landroid/filterfw/core/Frame;
    iget-object v3, p0, Landroid/filterpacks/imageproc/DocumentaryFilter;->mProgram:Landroid/filterfw/core/Program;

    #@46
    invoke-virtual {v3, v0, v2}, Landroid/filterfw/core/Program;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@49
    .line 139
    const-string v3, "image"

    #@4b
    invoke-virtual {p0, v3, v2}, Landroid/filterpacks/imageproc/DocumentaryFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@4e
    .line 142
    invoke-virtual {v2}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@51
    .line 143
    return-void
.end method

.method public setupPorts()V
    .registers 3

    #@0
    .prologue
    .line 90
    const-string v0, "image"

    #@2
    const/4 v1, 0x3

    #@3
    invoke-static {v1}, Landroid/filterfw/format/ImageFormat;->create(I)Landroid/filterfw/core/MutableFrameFormat;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/DocumentaryFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@a
    .line 91
    const-string v0, "image"

    #@c
    const-string v1, "image"

    #@e
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/DocumentaryFilter;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 92
    return-void
.end method
