.class public Landroid/filterpacks/imageproc/ResizeFilter;
.super Landroid/filterfw/core/Filter;
.source "ResizeFilter.java"


# instance fields
.field private mGenerateMipMap:Z
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "generateMipMap"
    .end annotation
.end field

.field private mInputChannels:I

.field private mKeepAspectRatio:Z
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "keepAspectRatio"
    .end annotation
.end field

.field private mLastFormat:Landroid/filterfw/core/FrameFormat;

.field private mOHeight:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "oheight"
    .end annotation
.end field

.field private mOWidth:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "owidth"
    .end annotation
.end field

.field private mOutputFormat:Landroid/filterfw/core/MutableFrameFormat;

.field private mProgram:Landroid/filterfw/core/Program;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "name"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 57
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@4
    .line 45
    iput-boolean v0, p0, Landroid/filterpacks/imageproc/ResizeFilter;->mKeepAspectRatio:Z

    #@6
    .line 47
    iput-boolean v0, p0, Landroid/filterpacks/imageproc/ResizeFilter;->mGenerateMipMap:Z

    #@8
    .line 51
    const/4 v0, 0x0

    #@9
    iput-object v0, p0, Landroid/filterpacks/imageproc/ResizeFilter;->mLastFormat:Landroid/filterfw/core/FrameFormat;

    #@b
    .line 58
    return-void
.end method


# virtual methods
.method protected createProgram(Landroid/filterfw/core/FilterContext;Landroid/filterfw/core/FrameFormat;)V
    .registers 6
    .parameter "context"
    .parameter "format"

    #@0
    .prologue
    .line 72
    iget-object v1, p0, Landroid/filterpacks/imageproc/ResizeFilter;->mLastFormat:Landroid/filterfw/core/FrameFormat;

    #@2
    if-eqz v1, :cond_11

    #@4
    iget-object v1, p0, Landroid/filterpacks/imageproc/ResizeFilter;->mLastFormat:Landroid/filterfw/core/FrameFormat;

    #@6
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@9
    move-result v1

    #@a
    invoke-virtual {p2}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@d
    move-result v2

    #@e
    if-ne v1, v2, :cond_11

    #@10
    .line 87
    :goto_10
    return-void

    #@11
    .line 73
    :cond_11
    iput-object p2, p0, Landroid/filterpacks/imageproc/ResizeFilter;->mLastFormat:Landroid/filterfw/core/FrameFormat;

    #@13
    .line 74
    invoke-virtual {p2}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@16
    move-result v1

    #@17
    packed-switch v1, :pswitch_data_32

    #@1a
    .line 85
    new-instance v1, Ljava/lang/RuntimeException;

    #@1c
    const-string v2, "ResizeFilter could not create suitable program!"

    #@1e
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@21
    throw v1

    #@22
    .line 76
    :pswitch_22
    new-instance v1, Ljava/lang/RuntimeException;

    #@24
    const-string v2, "Native ResizeFilter not implemented yet!"

    #@26
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@29
    throw v1

    #@2a
    .line 80
    :pswitch_2a
    invoke-static {p1}, Landroid/filterfw/core/ShaderProgram;->createIdentity(Landroid/filterfw/core/FilterContext;)Landroid/filterfw/core/ShaderProgram;

    #@2d
    move-result-object v0

    #@2e
    .line 81
    .local v0, prog:Landroid/filterfw/core/ShaderProgram;
    iput-object v0, p0, Landroid/filterpacks/imageproc/ResizeFilter;->mProgram:Landroid/filterfw/core/Program;

    #@30
    goto :goto_10

    #@31
    .line 74
    nop

    #@32
    :pswitch_data_32
    .packed-switch 0x2
        :pswitch_22
        :pswitch_2a
    .end packed-switch
.end method

.method public getOutputFormat(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .registers 3
    .parameter "portName"
    .parameter "inputFormat"

    #@0
    .prologue
    .line 68
    return-object p2
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 9
    .parameter "env"

    #@0
    .prologue
    .line 91
    const-string v5, "image"

    #@2
    invoke-virtual {p0, v5}, Landroid/filterpacks/imageproc/ResizeFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@5
    move-result-object v0

    #@6
    .line 92
    .local v0, input:Landroid/filterfw/core/Frame;
    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@9
    move-result-object v5

    #@a
    invoke-virtual {p0, p1, v5}, Landroid/filterpacks/imageproc/ResizeFilter;->createProgram(Landroid/filterfw/core/FilterContext;Landroid/filterfw/core/FrameFormat;)V

    #@d
    .line 95
    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@10
    move-result-object v5

    #@11
    invoke-virtual {v5}, Landroid/filterfw/core/FrameFormat;->mutableCopy()Landroid/filterfw/core/MutableFrameFormat;

    #@14
    move-result-object v4

    #@15
    .line 96
    .local v4, outputFormat:Landroid/filterfw/core/MutableFrameFormat;
    iget-boolean v5, p0, Landroid/filterpacks/imageproc/ResizeFilter;->mKeepAspectRatio:Z

    #@17
    if-eqz v5, :cond_2b

    #@19
    .line 97
    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@1c
    move-result-object v1

    #@1d
    .line 98
    .local v1, inputFormat:Landroid/filterfw/core/FrameFormat;
    iget v5, p0, Landroid/filterpacks/imageproc/ResizeFilter;->mOWidth:I

    #@1f
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@22
    move-result v6

    #@23
    mul-int/2addr v5, v6

    #@24
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@27
    move-result v6

    #@28
    div-int/2addr v5, v6

    #@29
    iput v5, p0, Landroid/filterpacks/imageproc/ResizeFilter;->mOHeight:I

    #@2b
    .line 100
    .end local v1           #inputFormat:Landroid/filterfw/core/FrameFormat;
    :cond_2b
    iget v5, p0, Landroid/filterpacks/imageproc/ResizeFilter;->mOWidth:I

    #@2d
    iget v6, p0, Landroid/filterpacks/imageproc/ResizeFilter;->mOHeight:I

    #@2f
    invoke-virtual {v4, v5, v6}, Landroid/filterfw/core/MutableFrameFormat;->setDimensions(II)V

    #@32
    .line 101
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@35
    move-result-object v5

    #@36
    invoke-virtual {v5, v4}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@39
    move-result-object v3

    #@3a
    .line 104
    .local v3, output:Landroid/filterfw/core/Frame;
    iget-boolean v5, p0, Landroid/filterpacks/imageproc/ResizeFilter;->mGenerateMipMap:Z

    #@3c
    if-eqz v5, :cond_6a

    #@3e
    .line 105
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@41
    move-result-object v5

    #@42
    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@45
    move-result-object v6

    #@46
    invoke-virtual {v5, v6}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@49
    move-result-object v2

    #@4a
    check-cast v2, Landroid/filterfw/core/GLFrame;

    #@4c
    .line 106
    .local v2, mipmapped:Landroid/filterfw/core/GLFrame;
    const/16 v5, 0x2801

    #@4e
    const/16 v6, 0x2701

    #@50
    invoke-virtual {v2, v5, v6}, Landroid/filterfw/core/GLFrame;->setTextureParameter(II)V

    #@53
    .line 108
    invoke-virtual {v2, v0}, Landroid/filterfw/core/GLFrame;->setDataFromFrame(Landroid/filterfw/core/Frame;)V

    #@56
    .line 109
    invoke-virtual {v2}, Landroid/filterfw/core/GLFrame;->generateMipMap()V

    #@59
    .line 110
    iget-object v5, p0, Landroid/filterpacks/imageproc/ResizeFilter;->mProgram:Landroid/filterfw/core/Program;

    #@5b
    invoke-virtual {v5, v2, v3}, Landroid/filterfw/core/Program;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@5e
    .line 111
    invoke-virtual {v2}, Landroid/filterfw/core/GLFrame;->release()Landroid/filterfw/core/Frame;

    #@61
    .line 117
    .end local v2           #mipmapped:Landroid/filterfw/core/GLFrame;
    :goto_61
    const-string v5, "image"

    #@63
    invoke-virtual {p0, v5, v3}, Landroid/filterpacks/imageproc/ResizeFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@66
    .line 120
    invoke-virtual {v3}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@69
    .line 121
    return-void

    #@6a
    .line 113
    :cond_6a
    iget-object v5, p0, Landroid/filterpacks/imageproc/ResizeFilter;->mProgram:Landroid/filterfw/core/Program;

    #@6c
    invoke-virtual {v5, v0, v3}, Landroid/filterfw/core/Program;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@6f
    goto :goto_61
.end method

.method public setupPorts()V
    .registers 3

    #@0
    .prologue
    .line 62
    const-string v0, "image"

    #@2
    const/4 v1, 0x3

    #@3
    invoke-static {v1}, Landroid/filterfw/format/ImageFormat;->create(I)Landroid/filterfw/core/MutableFrameFormat;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/ResizeFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@a
    .line 63
    const-string v0, "image"

    #@c
    const-string v1, "image"

    #@e
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/ResizeFilter;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 64
    return-void
.end method
