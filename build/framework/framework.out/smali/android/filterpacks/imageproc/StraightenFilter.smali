.class public Landroid/filterpacks/imageproc/StraightenFilter;
.super Landroid/filterfw/core/Filter;
.source "StraightenFilter.java"


# static fields
.field private static final DEGREE_TO_RADIAN:F = 0.017453292f


# instance fields
.field private mAngle:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "angle"
    .end annotation
.end field

.field private mHeight:I

.field private mMaxAngle:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "maxAngle"
    .end annotation
.end field

.field private mProgram:Landroid/filterfw/core/Program;

.field private mTarget:I

.field private mTileSize:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "tile_size"
    .end annotation
.end field

.field private mWidth:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter "name"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 59
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@4
    .line 41
    const/4 v0, 0x0

    #@5
    iput v0, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mAngle:F

    #@7
    .line 44
    const/high16 v0, 0x4234

    #@9
    iput v0, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mMaxAngle:F

    #@b
    .line 47
    const/16 v0, 0x280

    #@d
    iput v0, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mTileSize:I

    #@f
    .line 52
    iput v1, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mWidth:I

    #@11
    .line 53
    iput v1, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mHeight:I

    #@13
    .line 54
    iput v1, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mTarget:I

    #@15
    .line 60
    return-void
.end method

.method private updateParameters()V
    .registers 16

    #@0
    .prologue
    const/high16 v10, 0x42b4

    #@2
    const v14, 0x3c8efa35

    #@5
    const/high16 v13, 0x3f00

    #@7
    .line 121
    iget v11, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mAngle:F

    #@9
    mul-float/2addr v11, v14

    #@a
    float-to-double v11, v11

    #@b
    invoke-static {v11, v12}, Ljava/lang/Math;->cos(D)D

    #@e
    move-result-wide v11

    #@f
    double-to-float v0, v11

    #@10
    .line 122
    .local v0, cosTheta:F
    iget v11, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mAngle:F

    #@12
    mul-float/2addr v11, v14

    #@13
    float-to-double v11, v11

    #@14
    invoke-static {v11, v12}, Ljava/lang/Math;->sin(D)D

    #@17
    move-result-wide v11

    #@18
    double-to-float v9, v11

    #@19
    .line 124
    .local v9, sinTheta:F
    iget v11, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mMaxAngle:F

    #@1b
    const/4 v12, 0x0

    #@1c
    cmpg-float v11, v11, v12

    #@1e
    if-gtz v11, :cond_28

    #@20
    .line 125
    new-instance v10, Ljava/lang/RuntimeException;

    #@22
    const-string v11, "Max angle is out of range (0-180)."

    #@24
    invoke-direct {v10, v11}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@27
    throw v10

    #@28
    .line 126
    :cond_28
    iget v11, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mMaxAngle:F

    #@2a
    cmpl-float v11, v11, v10

    #@2c
    if-lez v11, :cond_117

    #@2e
    :goto_2e
    iput v10, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mMaxAngle:F

    #@30
    .line 128
    new-instance v3, Landroid/filterfw/geometry/Point;

    #@32
    neg-float v10, v0

    #@33
    iget v11, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mWidth:I

    #@35
    int-to-float v11, v11

    #@36
    mul-float/2addr v10, v11

    #@37
    iget v11, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mHeight:I

    #@39
    int-to-float v11, v11

    #@3a
    mul-float/2addr v11, v9

    #@3b
    add-float/2addr v10, v11

    #@3c
    neg-float v11, v9

    #@3d
    iget v12, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mWidth:I

    #@3f
    int-to-float v12, v12

    #@40
    mul-float/2addr v11, v12

    #@41
    iget v12, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mHeight:I

    #@43
    int-to-float v12, v12

    #@44
    mul-float/2addr v12, v0

    #@45
    sub-float/2addr v11, v12

    #@46
    invoke-direct {v3, v10, v11}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    #@49
    .line 131
    .local v3, p0:Landroid/filterfw/geometry/Point;
    new-instance v4, Landroid/filterfw/geometry/Point;

    #@4b
    iget v10, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mWidth:I

    #@4d
    int-to-float v10, v10

    #@4e
    mul-float/2addr v10, v0

    #@4f
    iget v11, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mHeight:I

    #@51
    int-to-float v11, v11

    #@52
    mul-float/2addr v11, v9

    #@53
    add-float/2addr v10, v11

    #@54
    iget v11, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mWidth:I

    #@56
    int-to-float v11, v11

    #@57
    mul-float/2addr v11, v9

    #@58
    iget v12, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mHeight:I

    #@5a
    int-to-float v12, v12

    #@5b
    mul-float/2addr v12, v0

    #@5c
    sub-float/2addr v11, v12

    #@5d
    invoke-direct {v4, v10, v11}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    #@60
    .line 134
    .local v4, p1:Landroid/filterfw/geometry/Point;
    new-instance v5, Landroid/filterfw/geometry/Point;

    #@62
    neg-float v10, v0

    #@63
    iget v11, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mWidth:I

    #@65
    int-to-float v11, v11

    #@66
    mul-float/2addr v10, v11

    #@67
    iget v11, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mHeight:I

    #@69
    int-to-float v11, v11

    #@6a
    mul-float/2addr v11, v9

    #@6b
    sub-float/2addr v10, v11

    #@6c
    neg-float v11, v9

    #@6d
    iget v12, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mWidth:I

    #@6f
    int-to-float v12, v12

    #@70
    mul-float/2addr v11, v12

    #@71
    iget v12, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mHeight:I

    #@73
    int-to-float v12, v12

    #@74
    mul-float/2addr v12, v0

    #@75
    add-float/2addr v11, v12

    #@76
    invoke-direct {v5, v10, v11}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    #@79
    .line 137
    .local v5, p2:Landroid/filterfw/geometry/Point;
    new-instance v6, Landroid/filterfw/geometry/Point;

    #@7b
    iget v10, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mWidth:I

    #@7d
    int-to-float v10, v10

    #@7e
    mul-float/2addr v10, v0

    #@7f
    iget v11, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mHeight:I

    #@81
    int-to-float v11, v11

    #@82
    mul-float/2addr v11, v9

    #@83
    sub-float/2addr v10, v11

    #@84
    iget v11, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mWidth:I

    #@86
    int-to-float v11, v11

    #@87
    mul-float/2addr v11, v9

    #@88
    iget v12, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mHeight:I

    #@8a
    int-to-float v12, v12

    #@8b
    mul-float/2addr v12, v0

    #@8c
    add-float/2addr v11, v12

    #@8d
    invoke-direct {v6, v10, v11}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    #@90
    .line 140
    .local v6, p3:Landroid/filterfw/geometry/Point;
    iget v10, v3, Landroid/filterfw/geometry/Point;->x:F

    #@92
    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    #@95
    move-result v10

    #@96
    iget v11, v4, Landroid/filterfw/geometry/Point;->x:F

    #@98
    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    #@9b
    move-result v11

    #@9c
    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    #@9f
    move-result v2

    #@a0
    .line 141
    .local v2, maxWidth:F
    iget v10, v3, Landroid/filterfw/geometry/Point;->y:F

    #@a2
    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    #@a5
    move-result v10

    #@a6
    iget v11, v4, Landroid/filterfw/geometry/Point;->y:F

    #@a8
    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    #@ab
    move-result v11

    #@ac
    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    #@af
    move-result v1

    #@b0
    .line 143
    .local v1, maxHeight:F
    iget v10, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mWidth:I

    #@b2
    int-to-float v10, v10

    #@b3
    div-float/2addr v10, v2

    #@b4
    iget v11, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mHeight:I

    #@b6
    int-to-float v11, v11

    #@b7
    div-float/2addr v11, v1

    #@b8
    invoke-static {v10, v11}, Ljava/lang/Math;->min(FF)F

    #@bb
    move-result v10

    #@bc
    mul-float v8, v13, v10

    #@be
    .line 146
    .local v8, scale:F
    iget v10, v3, Landroid/filterfw/geometry/Point;->x:F

    #@c0
    mul-float/2addr v10, v8

    #@c1
    iget v11, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mWidth:I

    #@c3
    int-to-float v11, v11

    #@c4
    div-float/2addr v10, v11

    #@c5
    add-float/2addr v10, v13

    #@c6
    iget v11, v3, Landroid/filterfw/geometry/Point;->y:F

    #@c8
    mul-float/2addr v11, v8

    #@c9
    iget v12, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mHeight:I

    #@cb
    int-to-float v12, v12

    #@cc
    div-float/2addr v11, v12

    #@cd
    add-float/2addr v11, v13

    #@ce
    invoke-virtual {v3, v10, v11}, Landroid/filterfw/geometry/Point;->set(FF)V

    #@d1
    .line 147
    iget v10, v4, Landroid/filterfw/geometry/Point;->x:F

    #@d3
    mul-float/2addr v10, v8

    #@d4
    iget v11, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mWidth:I

    #@d6
    int-to-float v11, v11

    #@d7
    div-float/2addr v10, v11

    #@d8
    add-float/2addr v10, v13

    #@d9
    iget v11, v4, Landroid/filterfw/geometry/Point;->y:F

    #@db
    mul-float/2addr v11, v8

    #@dc
    iget v12, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mHeight:I

    #@de
    int-to-float v12, v12

    #@df
    div-float/2addr v11, v12

    #@e0
    add-float/2addr v11, v13

    #@e1
    invoke-virtual {v4, v10, v11}, Landroid/filterfw/geometry/Point;->set(FF)V

    #@e4
    .line 148
    iget v10, v5, Landroid/filterfw/geometry/Point;->x:F

    #@e6
    mul-float/2addr v10, v8

    #@e7
    iget v11, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mWidth:I

    #@e9
    int-to-float v11, v11

    #@ea
    div-float/2addr v10, v11

    #@eb
    add-float/2addr v10, v13

    #@ec
    iget v11, v5, Landroid/filterfw/geometry/Point;->y:F

    #@ee
    mul-float/2addr v11, v8

    #@ef
    iget v12, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mHeight:I

    #@f1
    int-to-float v12, v12

    #@f2
    div-float/2addr v11, v12

    #@f3
    add-float/2addr v11, v13

    #@f4
    invoke-virtual {v5, v10, v11}, Landroid/filterfw/geometry/Point;->set(FF)V

    #@f7
    .line 149
    iget v10, v6, Landroid/filterfw/geometry/Point;->x:F

    #@f9
    mul-float/2addr v10, v8

    #@fa
    iget v11, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mWidth:I

    #@fc
    int-to-float v11, v11

    #@fd
    div-float/2addr v10, v11

    #@fe
    add-float/2addr v10, v13

    #@ff
    iget v11, v6, Landroid/filterfw/geometry/Point;->y:F

    #@101
    mul-float/2addr v11, v8

    #@102
    iget v12, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mHeight:I

    #@104
    int-to-float v12, v12

    #@105
    div-float/2addr v11, v12

    #@106
    add-float/2addr v11, v13

    #@107
    invoke-virtual {v6, v10, v11}, Landroid/filterfw/geometry/Point;->set(FF)V

    #@10a
    .line 151
    new-instance v7, Landroid/filterfw/geometry/Quad;

    #@10c
    invoke-direct {v7, v3, v4, v5, v6}, Landroid/filterfw/geometry/Quad;-><init>(Landroid/filterfw/geometry/Point;Landroid/filterfw/geometry/Point;Landroid/filterfw/geometry/Point;Landroid/filterfw/geometry/Point;)V

    #@10f
    .line 152
    .local v7, quad:Landroid/filterfw/geometry/Quad;
    iget-object v10, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mProgram:Landroid/filterfw/core/Program;

    #@111
    check-cast v10, Landroid/filterfw/core/ShaderProgram;

    #@113
    invoke-virtual {v10, v7}, Landroid/filterfw/core/ShaderProgram;->setSourceRegion(Landroid/filterfw/geometry/Quad;)V

    #@116
    .line 153
    return-void

    #@117
    .line 126
    .end local v1           #maxHeight:F
    .end local v2           #maxWidth:F
    .end local v3           #p0:Landroid/filterfw/geometry/Point;
    .end local v4           #p1:Landroid/filterfw/geometry/Point;
    .end local v5           #p2:Landroid/filterfw/geometry/Point;
    .end local v6           #p3:Landroid/filterfw/geometry/Point;
    .end local v7           #quad:Landroid/filterfw/geometry/Quad;
    .end local v8           #scale:F
    :cond_117
    iget v10, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mMaxAngle:F

    #@119
    goto/16 :goto_2e
.end method


# virtual methods
.method public fieldPortValueUpdated(Ljava/lang/String;Landroid/filterfw/core/FilterContext;)V
    .registers 4
    .parameter "name"
    .parameter "context"

    #@0
    .prologue
    .line 85
    iget-object v0, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mProgram:Landroid/filterfw/core/Program;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 86
    invoke-direct {p0}, Landroid/filterpacks/imageproc/StraightenFilter;->updateParameters()V

    #@7
    .line 88
    :cond_7
    return-void
.end method

.method public initProgram(Landroid/filterfw/core/FilterContext;I)V
    .registers 7
    .parameter "context"
    .parameter "target"

    #@0
    .prologue
    .line 69
    packed-switch p2, :pswitch_data_30

    #@3
    .line 77
    new-instance v1, Ljava/lang/RuntimeException;

    #@5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v3, "Filter Sharpen does not support frames of target "

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    const-string v3, "!"

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@21
    throw v1

    #@22
    .line 71
    :pswitch_22
    invoke-static {p1}, Landroid/filterfw/core/ShaderProgram;->createIdentity(Landroid/filterfw/core/FilterContext;)Landroid/filterfw/core/ShaderProgram;

    #@25
    move-result-object v0

    #@26
    .line 72
    .local v0, shaderProgram:Landroid/filterfw/core/ShaderProgram;
    iget v1, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mTileSize:I

    #@28
    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setMaximumTileSize(I)V

    #@2b
    .line 73
    iput-object v0, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mProgram:Landroid/filterfw/core/Program;

    #@2d
    .line 80
    iput p2, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mTarget:I

    #@2f
    .line 81
    return-void

    #@30
    .line 69
    :pswitch_data_30
    .packed-switch 0x3
        :pswitch_22
    .end packed-switch
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 7
    .parameter "context"

    #@0
    .prologue
    .line 93
    const-string v3, "image"

    #@2
    invoke-virtual {p0, v3}, Landroid/filterpacks/imageproc/StraightenFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@5
    move-result-object v0

    #@6
    .line 94
    .local v0, input:Landroid/filterfw/core/Frame;
    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@9
    move-result-object v1

    #@a
    .line 97
    .local v1, inputFormat:Landroid/filterfw/core/FrameFormat;
    iget-object v3, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mProgram:Landroid/filterfw/core/Program;

    #@c
    if-eqz v3, :cond_16

    #@e
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@11
    move-result v3

    #@12
    iget v4, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mTarget:I

    #@14
    if-eq v3, v4, :cond_1d

    #@16
    .line 98
    :cond_16
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@19
    move-result v3

    #@1a
    invoke-virtual {p0, p1, v3}, Landroid/filterpacks/imageproc/StraightenFilter;->initProgram(Landroid/filterfw/core/FilterContext;I)V

    #@1d
    .line 102
    :cond_1d
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@20
    move-result v3

    #@21
    iget v4, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mWidth:I

    #@23
    if-ne v3, v4, :cond_2d

    #@25
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@28
    move-result v3

    #@29
    iget v4, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mHeight:I

    #@2b
    if-eq v3, v4, :cond_3c

    #@2d
    .line 103
    :cond_2d
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@30
    move-result v3

    #@31
    iput v3, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mWidth:I

    #@33
    .line 104
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@36
    move-result v3

    #@37
    iput v3, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mHeight:I

    #@39
    .line 105
    invoke-direct {p0}, Landroid/filterpacks/imageproc/StraightenFilter;->updateParameters()V

    #@3c
    .line 108
    :cond_3c
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v3, v1}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@43
    move-result-object v2

    #@44
    .line 111
    .local v2, output:Landroid/filterfw/core/Frame;
    iget-object v3, p0, Landroid/filterpacks/imageproc/StraightenFilter;->mProgram:Landroid/filterfw/core/Program;

    #@46
    invoke-virtual {v3, v0, v2}, Landroid/filterfw/core/Program;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@49
    .line 114
    const-string v3, "image"

    #@4b
    invoke-virtual {p0, v3, v2}, Landroid/filterpacks/imageproc/StraightenFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@4e
    .line 117
    invoke-virtual {v2}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@51
    .line 118
    return-void
.end method

.method public setupPorts()V
    .registers 3

    #@0
    .prologue
    .line 64
    const-string v0, "image"

    #@2
    const/4 v1, 0x3

    #@3
    invoke-static {v1}, Landroid/filterfw/format/ImageFormat;->create(I)Landroid/filterfw/core/MutableFrameFormat;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/StraightenFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@a
    .line 65
    const-string v0, "image"

    #@c
    const-string v1, "image"

    #@e
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/StraightenFilter;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 66
    return-void
.end method
