.class public Landroid/filterpacks/imageproc/ToPackedGrayFilter;
.super Landroid/filterfw/core/Filter;
.source "ToPackedGrayFilter.java"


# instance fields
.field private final mColorToPackedGrayShader:Ljava/lang/String;

.field private mKeepAspectRatio:Z
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "keepAspectRatio"
    .end annotation
.end field

.field private mOHeight:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "oheight"
    .end annotation
.end field

.field private mOWidth:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "owidth"
    .end annotation
.end field

.field private mProgram:Landroid/filterfw/core/Program;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "name"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 62
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@4
    .line 38
    iput v0, p0, Landroid/filterpacks/imageproc/ToPackedGrayFilter;->mOWidth:I

    #@6
    .line 40
    iput v0, p0, Landroid/filterpacks/imageproc/ToPackedGrayFilter;->mOHeight:I

    #@8
    .line 42
    iput-boolean v0, p0, Landroid/filterpacks/imageproc/ToPackedGrayFilter;->mKeepAspectRatio:Z

    #@a
    .line 47
    const-string/jumbo v0, "precision mediump float;\nconst vec4 coeff_y = vec4(0.299, 0.587, 0.114, 0);\nuniform sampler2D tex_sampler_0;\nuniform float pix_stride;\nvarying vec2 v_texcoord;\nvoid main() {\n  for (int i = 0; i < 4; ++i) {\n    vec4 p = texture2D(tex_sampler_0,\n                       v_texcoord + vec2(pix_stride * float(i), 0.0));\n    gl_FragColor[i] = dot(p, coeff_y);\n  }\n}\n"

    #@d
    iput-object v0, p0, Landroid/filterpacks/imageproc/ToPackedGrayFilter;->mColorToPackedGrayShader:Ljava/lang/String;

    #@f
    .line 63
    return-void
.end method

.method private checkOutputDimensions(II)V
    .registers 6
    .parameter "outputWidth"
    .parameter "outputHeight"

    #@0
    .prologue
    .line 78
    if-lez p1, :cond_4

    #@2
    if-gtz p2, :cond_27

    #@4
    .line 79
    :cond_4
    new-instance v0, Ljava/lang/RuntimeException;

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "Invalid output dimensions: "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, " "

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@26
    throw v0

    #@27
    .line 82
    :cond_27
    return-void
.end method

.method private convertInputFormat(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .registers 8
    .parameter "inputFormat"

    #@0
    .prologue
    const/4 v4, 0x4

    #@1
    .line 85
    iget v2, p0, Landroid/filterpacks/imageproc/ToPackedGrayFilter;->mOWidth:I

    #@3
    .line 86
    .local v2, ow:I
    iget v1, p0, Landroid/filterpacks/imageproc/ToPackedGrayFilter;->mOHeight:I

    #@5
    .line 87
    .local v1, oh:I
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@8
    move-result v3

    #@9
    .line 88
    .local v3, w:I
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@c
    move-result v0

    #@d
    .line 89
    .local v0, h:I
    iget v5, p0, Landroid/filterpacks/imageproc/ToPackedGrayFilter;->mOWidth:I

    #@f
    if-nez v5, :cond_12

    #@11
    .line 90
    move v2, v3

    #@12
    .line 92
    :cond_12
    iget v5, p0, Landroid/filterpacks/imageproc/ToPackedGrayFilter;->mOHeight:I

    #@14
    if-nez v5, :cond_17

    #@16
    .line 93
    move v1, v0

    #@17
    .line 95
    :cond_17
    iget-boolean v5, p0, Landroid/filterpacks/imageproc/ToPackedGrayFilter;->mKeepAspectRatio:Z

    #@19
    if-eqz v5, :cond_25

    #@1b
    .line 98
    if-le v3, v0, :cond_31

    #@1d
    .line 99
    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    #@20
    move-result v2

    #@21
    .line 100
    mul-int v5, v2, v0

    #@23
    div-int v1, v5, v3

    #@25
    .line 106
    :cond_25
    :goto_25
    if-lez v2, :cond_3a

    #@27
    if-ge v2, v4, :cond_3a

    #@29
    move v2, v4

    #@2a
    .line 107
    :goto_2a
    const/4 v4, 0x1

    #@2b
    const/4 v5, 0x2

    #@2c
    invoke-static {v2, v1, v4, v5}, Landroid/filterfw/format/ImageFormat;->create(IIII)Landroid/filterfw/core/MutableFrameFormat;

    #@2f
    move-result-object v4

    #@30
    return-object v4

    #@31
    .line 102
    :cond_31
    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    #@34
    move-result v1

    #@35
    .line 103
    mul-int v5, v1, v3

    #@37
    div-int v2, v5, v0

    #@39
    goto :goto_25

    #@3a
    .line 106
    :cond_3a
    div-int/lit8 v4, v2, 0x4

    #@3c
    mul-int/lit8 v2, v4, 0x4

    #@3e
    goto :goto_2a
.end method


# virtual methods
.method public getOutputFormat(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .registers 4
    .parameter "portName"
    .parameter "inputFormat"

    #@0
    .prologue
    .line 74
    invoke-direct {p0, p2}, Landroid/filterpacks/imageproc/ToPackedGrayFilter;->convertInputFormat(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public prepare(Landroid/filterfw/core/FilterContext;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 114
    new-instance v0, Landroid/filterfw/core/ShaderProgram;

    #@2
    const-string/jumbo v1, "precision mediump float;\nconst vec4 coeff_y = vec4(0.299, 0.587, 0.114, 0);\nuniform sampler2D tex_sampler_0;\nuniform float pix_stride;\nvarying vec2 v_texcoord;\nvoid main() {\n  for (int i = 0; i < 4; ++i) {\n    vec4 p = texture2D(tex_sampler_0,\n                       v_texcoord + vec2(pix_stride * float(i), 0.0));\n    gl_FragColor[i] = dot(p, coeff_y);\n  }\n}\n"

    #@5
    invoke-direct {v0, p1, v1}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;)V

    #@8
    iput-object v0, p0, Landroid/filterpacks/imageproc/ToPackedGrayFilter;->mProgram:Landroid/filterfw/core/Program;

    #@a
    .line 115
    return-void
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 14
    .parameter "context"

    #@0
    .prologue
    .line 119
    const-string v8, "image"

    #@2
    invoke-virtual {p0, v8}, Landroid/filterpacks/imageproc/ToPackedGrayFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@5
    move-result-object v0

    #@6
    .line 120
    .local v0, input:Landroid/filterfw/core/Frame;
    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@9
    move-result-object v1

    #@a
    .line 121
    .local v1, inputFormat:Landroid/filterfw/core/FrameFormat;
    invoke-direct {p0, v1}, Landroid/filterpacks/imageproc/ToPackedGrayFilter;->convertInputFormat(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;

    #@d
    move-result-object v4

    #@e
    .line 122
    .local v4, outputFormat:Landroid/filterfw/core/FrameFormat;
    invoke-virtual {v4}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@11
    move-result v5

    #@12
    .line 123
    .local v5, ow:I
    invoke-virtual {v4}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@15
    move-result v2

    #@16
    .line 124
    .local v2, oh:I
    invoke-direct {p0, v5, v2}, Landroid/filterpacks/imageproc/ToPackedGrayFilter;->checkOutputDimensions(II)V

    #@19
    .line 125
    iget-object v8, p0, Landroid/filterpacks/imageproc/ToPackedGrayFilter;->mProgram:Landroid/filterfw/core/Program;

    #@1b
    const-string/jumbo v9, "pix_stride"

    #@1e
    const/high16 v10, 0x3f80

    #@20
    int-to-float v11, v5

    #@21
    div-float/2addr v10, v11

    #@22
    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@25
    move-result-object v10

    #@26
    invoke-virtual {v8, v9, v10}, Landroid/filterfw/core/Program;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@29
    .line 128
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->mutableCopy()Landroid/filterfw/core/MutableFrameFormat;

    #@2c
    move-result-object v7

    #@2d
    .line 129
    .local v7, tempFrameFormat:Landroid/filterfw/core/MutableFrameFormat;
    div-int/lit8 v8, v5, 0x4

    #@2f
    invoke-virtual {v7, v8, v2}, Landroid/filterfw/core/MutableFrameFormat;->setDimensions(II)V

    #@32
    .line 130
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@35
    move-result-object v8

    #@36
    invoke-virtual {v8, v7}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@39
    move-result-object v6

    #@3a
    .line 131
    .local v6, temp:Landroid/filterfw/core/Frame;
    iget-object v8, p0, Landroid/filterpacks/imageproc/ToPackedGrayFilter;->mProgram:Landroid/filterfw/core/Program;

    #@3c
    invoke-virtual {v8, v0, v6}, Landroid/filterfw/core/Program;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@3f
    .line 134
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@42
    move-result-object v8

    #@43
    invoke-virtual {v8, v4}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@46
    move-result-object v3

    #@47
    .line 135
    .local v3, output:Landroid/filterfw/core/Frame;
    invoke-virtual {v3, v6}, Landroid/filterfw/core/Frame;->setDataFromFrame(Landroid/filterfw/core/Frame;)V

    #@4a
    .line 136
    invoke-virtual {v6}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@4d
    .line 139
    const-string v8, "image"

    #@4f
    invoke-virtual {p0, v8, v3}, Landroid/filterpacks/imageproc/ToPackedGrayFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@52
    .line 140
    invoke-virtual {v3}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@55
    .line 141
    return-void
.end method

.method public setupPorts()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x3

    #@1
    .line 67
    const-string v0, "image"

    #@3
    invoke-static {v1, v1}, Landroid/filterfw/format/ImageFormat;->create(II)Landroid/filterfw/core/MutableFrameFormat;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/ToPackedGrayFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@a
    .line 69
    const-string v0, "image"

    #@c
    const-string v1, "image"

    #@e
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/imageproc/ToPackedGrayFilter;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 70
    return-void
.end method
