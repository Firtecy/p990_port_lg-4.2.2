.class public Landroid/filterpacks/imageproc/ToRGBAFilter;
.super Landroid/filterfw/core/Filter;
.source "ToRGBAFilter.java"


# instance fields
.field private mInputBPP:I

.field private mLastFormat:Landroid/filterfw/core/FrameFormat;

.field private mProgram:Landroid/filterfw/core/Program;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 44
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@3
    .line 41
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Landroid/filterpacks/imageproc/ToRGBAFilter;->mLastFormat:Landroid/filterfw/core/FrameFormat;

    #@6
    .line 45
    return-void
.end method


# virtual methods
.method public createProgram(Landroid/filterfw/core/FilterContext;Landroid/filterfw/core/FrameFormat;)V
    .registers 6
    .parameter "context"
    .parameter "format"

    #@0
    .prologue
    .line 69
    invoke-virtual {p2}, Landroid/filterfw/core/FrameFormat;->getBytesPerSample()I

    #@3
    move-result v0

    #@4
    iput v0, p0, Landroid/filterpacks/imageproc/ToRGBAFilter;->mInputBPP:I

    #@6
    .line 70
    iget-object v0, p0, Landroid/filterpacks/imageproc/ToRGBAFilter;->mLastFormat:Landroid/filterfw/core/FrameFormat;

    #@8
    if-eqz v0, :cond_15

    #@a
    iget-object v0, p0, Landroid/filterpacks/imageproc/ToRGBAFilter;->mLastFormat:Landroid/filterfw/core/FrameFormat;

    #@c
    invoke-virtual {v0}, Landroid/filterfw/core/FrameFormat;->getBytesPerSample()I

    #@f
    move-result v0

    #@10
    iget v1, p0, Landroid/filterpacks/imageproc/ToRGBAFilter;->mInputBPP:I

    #@12
    if-ne v0, v1, :cond_15

    #@14
    .line 82
    :goto_14
    return-void

    #@15
    .line 71
    :cond_15
    iput-object p2, p0, Landroid/filterpacks/imageproc/ToRGBAFilter;->mLastFormat:Landroid/filterfw/core/FrameFormat;

    #@17
    .line 72
    iget v0, p0, Landroid/filterpacks/imageproc/ToRGBAFilter;->mInputBPP:I

    #@19
    packed-switch v0, :pswitch_data_56

    #@1c
    .line 80
    :pswitch_1c
    new-instance v0, Ljava/lang/RuntimeException;

    #@1e
    new-instance v1, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v2, "Unsupported BytesPerPixel: "

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    iget v2, p0, Landroid/filterpacks/imageproc/ToRGBAFilter;->mInputBPP:I

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    const-string v2, "!"

    #@31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v1

    #@39
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@3c
    throw v0

    #@3d
    .line 74
    :pswitch_3d
    new-instance v0, Landroid/filterfw/core/NativeProgram;

    #@3f
    const-string v1, "filterpack_imageproc"

    #@41
    const-string v2, "gray_to_rgba"

    #@43
    invoke-direct {v0, v1, v2}, Landroid/filterfw/core/NativeProgram;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@46
    iput-object v0, p0, Landroid/filterpacks/imageproc/ToRGBAFilter;->mProgram:Landroid/filterfw/core/Program;

    #@48
    goto :goto_14

    #@49
    .line 77
    :pswitch_49
    new-instance v0, Landroid/filterfw/core/NativeProgram;

    #@4b
    const-string v1, "filterpack_imageproc"

    #@4d
    const-string/jumbo v2, "rgb_to_rgba"

    #@50
    invoke-direct {v0, v1, v2}, Landroid/filterfw/core/NativeProgram;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@53
    iput-object v0, p0, Landroid/filterpacks/imageproc/ToRGBAFilter;->mProgram:Landroid/filterfw/core/Program;

    #@55
    goto :goto_14

    #@56
    .line 72
    :pswitch_data_56
    .packed-switch 0x1
        :pswitch_3d
        :pswitch_1c
        :pswitch_49
    .end packed-switch
.end method

.method public getConvertedFormat(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .registers 5
    .parameter "format"

    #@0
    .prologue
    .line 62
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->mutableCopy()Landroid/filterfw/core/MutableFrameFormat;

    #@3
    move-result-object v0

    #@4
    .line 63
    .local v0, result:Landroid/filterfw/core/MutableFrameFormat;
    const-string v1, "colorspace"

    #@6
    const/4 v2, 0x3

    #@7
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/MutableFrameFormat;->setMetaValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@e
    .line 64
    const/4 v1, 0x4

    #@f
    invoke-virtual {v0, v1}, Landroid/filterfw/core/MutableFrameFormat;->setBytesPerSample(I)V

    #@12
    .line 65
    return-object v0
.end method

.method public getOutputFormat(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .registers 4
    .parameter "portName"
    .parameter "inputFormat"

    #@0
    .prologue
    .line 58
    invoke-virtual {p0, p2}, Landroid/filterpacks/imageproc/ToRGBAFilter;->getConvertedFormat(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    .line 87
    const-string v2, "image"

    #@2
    invoke-virtual {p0, v2}, Landroid/filterpacks/imageproc/ToRGBAFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@5
    move-result-object v0

    #@6
    .line 88
    .local v0, input:Landroid/filterfw/core/Frame;
    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@9
    move-result-object v2

    #@a
    invoke-virtual {p0, p1, v2}, Landroid/filterpacks/imageproc/ToRGBAFilter;->createProgram(Landroid/filterfw/core/FilterContext;Landroid/filterfw/core/FrameFormat;)V

    #@d
    .line 91
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@14
    move-result-object v3

    #@15
    invoke-virtual {p0, v3}, Landroid/filterpacks/imageproc/ToRGBAFilter;->getConvertedFormat(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v2, v3}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@1c
    move-result-object v1

    #@1d
    .line 94
    .local v1, output:Landroid/filterfw/core/Frame;
    iget-object v2, p0, Landroid/filterpacks/imageproc/ToRGBAFilter;->mProgram:Landroid/filterfw/core/Program;

    #@1f
    invoke-virtual {v2, v0, v1}, Landroid/filterfw/core/Program;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@22
    .line 97
    const-string v2, "image"

    #@24
    invoke-virtual {p0, v2, v1}, Landroid/filterpacks/imageproc/ToRGBAFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@27
    .line 100
    invoke-virtual {v1}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@2a
    .line 101
    return-void
.end method

.method public setupPorts()V
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x2

    #@1
    .line 49
    new-instance v0, Landroid/filterfw/core/MutableFrameFormat;

    #@3
    invoke-direct {v0, v1, v1}, Landroid/filterfw/core/MutableFrameFormat;-><init>(II)V

    #@6
    .line 51
    .local v0, mask:Landroid/filterfw/core/MutableFrameFormat;
    invoke-virtual {v0, v1}, Landroid/filterfw/core/MutableFrameFormat;->setDimensionCount(I)V

    #@9
    .line 52
    const-string v1, "image"

    #@b
    invoke-virtual {p0, v1, v0}, Landroid/filterpacks/imageproc/ToRGBAFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@e
    .line 53
    const-string v1, "image"

    #@10
    const-string v2, "image"

    #@12
    invoke-virtual {p0, v1, v2}, Landroid/filterpacks/imageproc/ToRGBAFilter;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    #@15
    .line 54
    return-void
.end method
