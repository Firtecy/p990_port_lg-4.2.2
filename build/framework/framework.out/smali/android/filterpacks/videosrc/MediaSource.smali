.class public Landroid/filterpacks/videosrc/MediaSource;
.super Landroid/filterfw/core/Filter;
.source "MediaSource.java"


# static fields
.field private static final NEWFRAME_TIMEOUT:I = 0x64

.field private static final NEWFRAME_TIMEOUT_REPEAT:I = 0xa

.field private static final PREP_TIMEOUT:I = 0x64

.field private static final PREP_TIMEOUT_REPEAT:I = 0x64

.field private static final TAG:Ljava/lang/String; = "MediaSource"

.field private static final mSourceCoords_0:[F

.field private static final mSourceCoords_180:[F

.field private static final mSourceCoords_270:[F

.field private static final mSourceCoords_90:[F


# instance fields
.field private mCompleted:Z

.field private mContext:Landroid/content/Context;
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "context"
    .end annotation
.end field

.field private mError:Z

.field private mFrameExtractor:Landroid/filterfw/core/ShaderProgram;

.field private final mFrameShader:Ljava/lang/String;

.field private mGotSize:Z

.field private mHeight:I

.field private final mLogVerbose:Z

.field private mLooping:Z
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "loop"
    .end annotation
.end field

.field private mMediaFrame:Landroid/filterfw/core/GLFrame;

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mNewFrameAvailable:Z

.field private mOrientation:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "orientation"
    .end annotation
.end field

.field private mOrientationUpdated:Z

.field private mOutputFormat:Landroid/filterfw/core/MutableFrameFormat;

.field private mPaused:Z

.field private mPlaying:Z

.field private mPrepared:Z

.field private mSelectedIsUrl:Z
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "sourceIsUrl"
    .end annotation
.end field

.field private mSourceAsset:Landroid/content/res/AssetFileDescriptor;
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "sourceAsset"
    .end annotation
.end field

.field private mSourceUrl:Ljava/lang/String;
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "sourceUrl"
    .end annotation
.end field

.field private mSurfaceTexture:Landroid/graphics/SurfaceTexture;

.field private mVolume:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "volume"
    .end annotation
.end field

.field private mWaitForNewFrame:Z
    .annotation runtime Landroid/filterfw/core/GenerateFinalPort;
        hasDefault = true
        name = "waitForNewFrame"
    .end annotation
.end field

.field private mWidth:I

.field private onCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private onErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private onMediaFrameAvailableListener:Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;

.field private onPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field private onVideoSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/16 v1, 0x10

    #@2
    .line 144
    new-array v0, v1, [F

    #@4
    fill-array-data v0, :array_20

    #@7
    sput-object v0, Landroid/filterpacks/videosrc/MediaSource;->mSourceCoords_0:[F

    #@9
    .line 148
    new-array v0, v1, [F

    #@b
    fill-array-data v0, :array_44

    #@e
    sput-object v0, Landroid/filterpacks/videosrc/MediaSource;->mSourceCoords_270:[F

    #@10
    .line 152
    new-array v0, v1, [F

    #@12
    fill-array-data v0, :array_68

    #@15
    sput-object v0, Landroid/filterpacks/videosrc/MediaSource;->mSourceCoords_180:[F

    #@17
    .line 156
    new-array v0, v1, [F

    #@19
    fill-array-data v0, :array_8c

    #@1c
    sput-object v0, Landroid/filterpacks/videosrc/MediaSource;->mSourceCoords_90:[F

    #@1e
    return-void

    #@1f
    .line 144
    nop

    #@20
    :array_20
    .array-data 0x4
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data

    #@44
    .line 148
    :array_44
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data

    #@68
    .line 152
    :array_68
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data

    #@8c
    .line 156
    :array_8c
    .array-data 0x4
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 6
    .parameter "name"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    const/4 v1, 0x0

    #@3
    .line 174
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@6
    .line 61
    const-string v0, ""

    #@8
    iput-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mSourceUrl:Ljava/lang/String;

    #@a
    .line 65
    iput-object v3, p0, Landroid/filterpacks/videosrc/MediaSource;->mSourceAsset:Landroid/content/res/AssetFileDescriptor;

    #@c
    .line 71
    iput-object v3, p0, Landroid/filterpacks/videosrc/MediaSource;->mContext:Landroid/content/Context;

    #@e
    .line 77
    iput-boolean v1, p0, Landroid/filterpacks/videosrc/MediaSource;->mSelectedIsUrl:Z

    #@10
    .line 84
    iput-boolean v2, p0, Landroid/filterpacks/videosrc/MediaSource;->mWaitForNewFrame:Z

    #@12
    .line 90
    iput-boolean v2, p0, Landroid/filterpacks/videosrc/MediaSource;->mLooping:Z

    #@14
    .line 96
    const/4 v0, 0x0

    #@15
    iput v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mVolume:F

    #@17
    .line 102
    iput v1, p0, Landroid/filterpacks/videosrc/MediaSource;->mOrientation:I

    #@19
    .line 121
    const-string v0, "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nuniform samplerExternalOES tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n"

    #@1b
    iput-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mFrameShader:Ljava/lang/String;

    #@1d
    .line 533
    new-instance v0, Landroid/filterpacks/videosrc/MediaSource$1;

    #@1f
    invoke-direct {v0, p0}, Landroid/filterpacks/videosrc/MediaSource$1;-><init>(Landroid/filterpacks/videosrc/MediaSource;)V

    #@22
    iput-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->onVideoSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    #@24
    .line 558
    new-instance v0, Landroid/filterpacks/videosrc/MediaSource$2;

    #@26
    invoke-direct {v0, p0}, Landroid/filterpacks/videosrc/MediaSource$2;-><init>(Landroid/filterpacks/videosrc/MediaSource;)V

    #@29
    iput-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->onPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    #@2b
    .line 569
    new-instance v0, Landroid/filterpacks/videosrc/MediaSource$3;

    #@2d
    invoke-direct {v0, p0}, Landroid/filterpacks/videosrc/MediaSource$3;-><init>(Landroid/filterpacks/videosrc/MediaSource;)V

    #@30
    iput-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->onCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    #@32
    .line 579
    new-instance v0, Landroid/filterpacks/videosrc/MediaSource$4;

    #@34
    invoke-direct {v0, p0}, Landroid/filterpacks/videosrc/MediaSource$4;-><init>(Landroid/filterpacks/videosrc/MediaSource;)V

    #@37
    iput-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->onErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    #@39
    .line 591
    new-instance v0, Landroid/filterpacks/videosrc/MediaSource$5;

    #@3b
    invoke-direct {v0, p0}, Landroid/filterpacks/videosrc/MediaSource$5;-><init>(Landroid/filterpacks/videosrc/MediaSource;)V

    #@3e
    iput-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->onMediaFrameAvailableListener:Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;

    #@40
    .line 175
    iput-boolean v1, p0, Landroid/filterpacks/videosrc/MediaSource;->mNewFrameAvailable:Z

    #@42
    .line 177
    const-string v0, "MediaSource"

    #@44
    const/4 v1, 0x2

    #@45
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@48
    move-result v0

    #@49
    iput-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mLogVerbose:Z

    #@4b
    .line 178
    return-void
.end method

.method static synthetic access$000(Landroid/filterpacks/videosrc/MediaSource;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 54
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mLogVerbose:Z

    #@2
    return v0
.end method

.method static synthetic access$100(Landroid/filterpacks/videosrc/MediaSource;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 54
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mGotSize:Z

    #@2
    return v0
.end method

.method static synthetic access$102(Landroid/filterpacks/videosrc/MediaSource;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 54
    iput-boolean p1, p0, Landroid/filterpacks/videosrc/MediaSource;->mGotSize:Z

    #@2
    return p1
.end method

.method static synthetic access$200(Landroid/filterpacks/videosrc/MediaSource;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 54
    iget v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mOrientation:I

    #@2
    return v0
.end method

.method static synthetic access$300(Landroid/filterpacks/videosrc/MediaSource;)Landroid/filterfw/core/MutableFrameFormat;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 54
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mOutputFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@2
    return-object v0
.end method

.method static synthetic access$402(Landroid/filterpacks/videosrc/MediaSource;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 54
    iput p1, p0, Landroid/filterpacks/videosrc/MediaSource;->mWidth:I

    #@2
    return p1
.end method

.method static synthetic access$502(Landroid/filterpacks/videosrc/MediaSource;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 54
    iput p1, p0, Landroid/filterpacks/videosrc/MediaSource;->mHeight:I

    #@2
    return p1
.end method

.method static synthetic access$602(Landroid/filterpacks/videosrc/MediaSource;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 54
    iput-boolean p1, p0, Landroid/filterpacks/videosrc/MediaSource;->mPrepared:Z

    #@2
    return p1
.end method

.method static synthetic access$702(Landroid/filterpacks/videosrc/MediaSource;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 54
    iput-boolean p1, p0, Landroid/filterpacks/videosrc/MediaSource;->mCompleted:Z

    #@2
    return p1
.end method

.method static synthetic access$802(Landroid/filterpacks/videosrc/MediaSource;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 54
    iput-boolean p1, p0, Landroid/filterpacks/videosrc/MediaSource;->mError:Z

    #@2
    return p1
.end method

.method static synthetic access$902(Landroid/filterpacks/videosrc/MediaSource;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 54
    iput-boolean p1, p0, Landroid/filterpacks/videosrc/MediaSource;->mNewFrameAvailable:Z

    #@2
    return p1
.end method

.method private createFormats()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x3

    #@1
    .line 188
    invoke-static {v0, v0}, Landroid/filterfw/format/ImageFormat;->create(II)Landroid/filterfw/core/MutableFrameFormat;

    #@4
    move-result-object v0

    #@5
    iput-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mOutputFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@7
    .line 190
    return-void
.end method

.method private declared-synchronized setupMediaPlayer(Z)Z
    .registers 11
    .parameter "useUrl"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    .line 454
    monitor-enter p0

    #@2
    const/4 v0, 0x0

    #@3
    :try_start_3
    iput-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mPrepared:Z

    #@5
    .line 455
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mGotSize:Z

    #@8
    .line 456
    const/4 v0, 0x0

    #@9
    iput-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mPlaying:Z

    #@b
    .line 457
    const/4 v0, 0x0

    #@c
    iput-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mPaused:Z

    #@e
    .line 458
    const/4 v0, 0x0

    #@f
    iput-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mCompleted:Z

    #@11
    .line 459
    const/4 v0, 0x0

    #@12
    iput-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mError:Z

    #@14
    .line 460
    const/4 v0, 0x0

    #@15
    iput-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mNewFrameAvailable:Z

    #@17
    .line 462
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mLogVerbose:Z

    #@19
    if-eqz v0, :cond_22

    #@1b
    const-string v0, "MediaSource"

    #@1d
    const-string v1, "Setting up playback."

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 464
    :cond_22
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@24
    if-eqz v0, :cond_45

    #@26
    .line 466
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mLogVerbose:Z

    #@28
    if-eqz v0, :cond_31

    #@2a
    const-string v0, "MediaSource"

    #@2c
    const-string v1, "Resetting existing MediaPlayer."

    #@2e
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 467
    :cond_31
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@33
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    #@36
    .line 474
    :goto_36
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@38
    if-nez v0, :cond_58

    #@3a
    .line 475
    new-instance v0, Ljava/lang/RuntimeException;

    #@3c
    const-string v1, "Unable to create a MediaPlayer!"

    #@3e
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@41
    throw v0
    :try_end_42
    .catchall {:try_start_3 .. :try_end_42} :catchall_42

    #@42
    .line 454
    :catchall_42
    move-exception v0

    #@43
    monitor-exit p0

    #@44
    throw v0

    #@45
    .line 470
    :cond_45
    :try_start_45
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mLogVerbose:Z

    #@47
    if-eqz v0, :cond_50

    #@49
    const-string v0, "MediaSource"

    #@4b
    const-string v1, "Creating new MediaPlayer."

    #@4d
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    .line 471
    :cond_50
    new-instance v0, Landroid/media/MediaPlayer;

    #@52
    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    #@55
    iput-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;
    :try_end_57
    .catchall {:try_start_45 .. :try_end_57} :catchall_42

    #@57
    goto :goto_36

    #@58
    .line 480
    :cond_58
    if-eqz p1, :cond_108

    #@5a
    .line 481
    :try_start_5a
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mLogVerbose:Z

    #@5c
    if-eqz v0, :cond_78

    #@5e
    const-string v0, "MediaSource"

    #@60
    new-instance v1, Ljava/lang/StringBuilder;

    #@62
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@65
    const-string v2, "Setting MediaPlayer source to URI "

    #@67
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v1

    #@6b
    iget-object v2, p0, Landroid/filterpacks/videosrc/MediaSource;->mSourceUrl:Ljava/lang/String;

    #@6d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v1

    #@71
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v1

    #@75
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@78
    .line 482
    :cond_78
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mContext:Landroid/content/Context;

    #@7a
    if-nez v0, :cond_d7

    #@7c
    .line 483
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@7e
    iget-object v1, p0, Landroid/filterpacks/videosrc/MediaSource;->mSourceUrl:Ljava/lang/String;

    #@80
    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V
    :try_end_83
    .catchall {:try_start_5a .. :try_end_83} :catchall_42
    .catch Ljava/io/IOException; {:try_start_5a .. :try_end_83} :catch_e9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5a .. :try_end_83} :catch_13f

    #@83
    .line 509
    :goto_83
    :try_start_83
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@85
    iget-boolean v1, p0, Landroid/filterpacks/videosrc/MediaSource;->mLooping:Z

    #@87
    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    #@8a
    .line 510
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@8c
    iget v1, p0, Landroid/filterpacks/videosrc/MediaSource;->mVolume:F

    #@8e
    iget v2, p0, Landroid/filterpacks/videosrc/MediaSource;->mVolume:F

    #@90
    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setVolume(FF)V

    #@93
    .line 513
    new-instance v7, Landroid/view/Surface;

    #@95
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@97
    invoke-direct {v7, v0}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    #@9a
    .line 514
    .local v7, surface:Landroid/view/Surface;
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@9c
    invoke-virtual {v0, v7}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    #@9f
    .line 515
    invoke-virtual {v7}, Landroid/view/Surface;->release()V

    #@a2
    .line 519
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@a4
    iget-object v1, p0, Landroid/filterpacks/videosrc/MediaSource;->onVideoSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    #@a6
    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    #@a9
    .line 520
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@ab
    iget-object v1, p0, Landroid/filterpacks/videosrc/MediaSource;->onPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    #@ad
    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    #@b0
    .line 521
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@b2
    iget-object v1, p0, Landroid/filterpacks/videosrc/MediaSource;->onCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    #@b4
    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    #@b7
    .line 522
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@b9
    iget-object v1, p0, Landroid/filterpacks/videosrc/MediaSource;->onErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    #@bb
    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    #@be
    .line 525
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@c0
    iget-object v1, p0, Landroid/filterpacks/videosrc/MediaSource;->onMediaFrameAvailableListener:Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;

    #@c2
    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    #@c5
    .line 527
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mLogVerbose:Z

    #@c7
    if-eqz v0, :cond_d0

    #@c9
    const-string v0, "MediaSource"

    #@cb
    const-string v1, "Preparing MediaPlayer."

    #@cd
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@d0
    .line 528
    :cond_d0
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@d2
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_d5
    .catchall {:try_start_83 .. :try_end_d5} :catchall_42

    #@d5
    .line 530
    monitor-exit p0

    #@d6
    return v8

    #@d7
    .line 485
    .end local v7           #surface:Landroid/view/Surface;
    :cond_d7
    :try_start_d7
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@d9
    iget-object v1, p0, Landroid/filterpacks/videosrc/MediaSource;->mContext:Landroid/content/Context;

    #@db
    iget-object v2, p0, Landroid/filterpacks/videosrc/MediaSource;->mSourceUrl:Ljava/lang/String;

    #@dd
    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    #@e0
    move-result-object v2

    #@e1
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@e4
    move-result-object v2

    #@e5
    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    :try_end_e8
    .catchall {:try_start_d7 .. :try_end_e8} :catchall_42
    .catch Ljava/io/IOException; {:try_start_d7 .. :try_end_e8} :catch_e9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_d7 .. :try_end_e8} :catch_13f

    #@e8
    goto :goto_83

    #@e9
    .line 491
    :catch_e9
    move-exception v6

    #@ea
    .line 492
    .local v6, e:Ljava/io/IOException;
    :try_start_ea
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@ec
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    #@ef
    .line 493
    const/4 v0, 0x0

    #@f0
    iput-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@f2
    .line 494
    if-eqz p1, :cond_15e

    #@f4
    .line 495
    new-instance v0, Ljava/lang/RuntimeException;

    #@f6
    const-string v1, "Unable to set MediaPlayer to URL %s!"

    #@f8
    const/4 v2, 0x1

    #@f9
    new-array v2, v2, [Ljava/lang/Object;

    #@fb
    const/4 v3, 0x0

    #@fc
    iget-object v4, p0, Landroid/filterpacks/videosrc/MediaSource;->mSourceUrl:Ljava/lang/String;

    #@fe
    aput-object v4, v2, v3

    #@100
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@103
    move-result-object v1

    #@104
    invoke-direct {v0, v1, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@107
    throw v0
    :try_end_108
    .catchall {:try_start_ea .. :try_end_108} :catchall_42

    #@108
    .line 488
    .end local v6           #e:Ljava/io/IOException;
    :cond_108
    :try_start_108
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mLogVerbose:Z

    #@10a
    if-eqz v0, :cond_126

    #@10c
    const-string v0, "MediaSource"

    #@10e
    new-instance v1, Ljava/lang/StringBuilder;

    #@110
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@113
    const-string v2, "Setting MediaPlayer source to asset "

    #@115
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@118
    move-result-object v1

    #@119
    iget-object v2, p0, Landroid/filterpacks/videosrc/MediaSource;->mSourceAsset:Landroid/content/res/AssetFileDescriptor;

    #@11b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11e
    move-result-object v1

    #@11f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@122
    move-result-object v1

    #@123
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@126
    .line 489
    :cond_126
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@128
    iget-object v1, p0, Landroid/filterpacks/videosrc/MediaSource;->mSourceAsset:Landroid/content/res/AssetFileDescriptor;

    #@12a
    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@12d
    move-result-object v1

    #@12e
    iget-object v2, p0, Landroid/filterpacks/videosrc/MediaSource;->mSourceAsset:Landroid/content/res/AssetFileDescriptor;

    #@130
    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    #@133
    move-result-wide v2

    #@134
    iget-object v4, p0, Landroid/filterpacks/videosrc/MediaSource;->mSourceAsset:Landroid/content/res/AssetFileDescriptor;

    #@136
    invoke-virtual {v4}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    #@139
    move-result-wide v4

    #@13a
    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V
    :try_end_13d
    .catchall {:try_start_108 .. :try_end_13d} :catchall_42
    .catch Ljava/io/IOException; {:try_start_108 .. :try_end_13d} :catch_e9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_108 .. :try_end_13d} :catch_13f

    #@13d
    goto/16 :goto_83

    #@13f
    .line 499
    :catch_13f
    move-exception v6

    #@140
    .line 500
    .local v6, e:Ljava/lang/IllegalArgumentException;
    :try_start_140
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@142
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    #@145
    .line 501
    const/4 v0, 0x0

    #@146
    iput-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@148
    .line 502
    if-eqz p1, :cond_172

    #@14a
    .line 503
    new-instance v0, Ljava/lang/RuntimeException;

    #@14c
    const-string v1, "Unable to set MediaPlayer to URL %s!"

    #@14e
    const/4 v2, 0x1

    #@14f
    new-array v2, v2, [Ljava/lang/Object;

    #@151
    const/4 v3, 0x0

    #@152
    iget-object v4, p0, Landroid/filterpacks/videosrc/MediaSource;->mSourceUrl:Ljava/lang/String;

    #@154
    aput-object v4, v2, v3

    #@156
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@159
    move-result-object v1

    #@15a
    invoke-direct {v0, v1, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@15d
    throw v0

    #@15e
    .line 497
    .local v6, e:Ljava/io/IOException;
    :cond_15e
    new-instance v0, Ljava/lang/RuntimeException;

    #@160
    const-string v1, "Unable to set MediaPlayer to asset %s!"

    #@162
    const/4 v2, 0x1

    #@163
    new-array v2, v2, [Ljava/lang/Object;

    #@165
    const/4 v3, 0x0

    #@166
    iget-object v4, p0, Landroid/filterpacks/videosrc/MediaSource;->mSourceAsset:Landroid/content/res/AssetFileDescriptor;

    #@168
    aput-object v4, v2, v3

    #@16a
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@16d
    move-result-object v1

    #@16e
    invoke-direct {v0, v1, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@171
    throw v0

    #@172
    .line 505
    .local v6, e:Ljava/lang/IllegalArgumentException;
    :cond_172
    new-instance v0, Ljava/lang/RuntimeException;

    #@174
    const-string v1, "Unable to set MediaPlayer to asset %s!"

    #@176
    const/4 v2, 0x1

    #@177
    new-array v2, v2, [Ljava/lang/Object;

    #@179
    const/4 v3, 0x0

    #@17a
    iget-object v4, p0, Landroid/filterpacks/videosrc/MediaSource;->mSourceAsset:Landroid/content/res/AssetFileDescriptor;

    #@17c
    aput-object v4, v2, v3

    #@17e
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@181
    move-result-object v1

    #@182
    invoke-direct {v0, v1, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@185
    throw v0
    :try_end_186
    .catchall {:try_start_140 .. :try_end_186} :catchall_42
.end method


# virtual methods
.method public close(Landroid/filterfw/core/FilterContext;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 366
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@4
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_f

    #@a
    .line 367
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@c
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    #@f
    .line 369
    :cond_f
    iput-boolean v1, p0, Landroid/filterpacks/videosrc/MediaSource;->mPrepared:Z

    #@11
    .line 370
    iput-boolean v1, p0, Landroid/filterpacks/videosrc/MediaSource;->mGotSize:Z

    #@13
    .line 371
    iput-boolean v1, p0, Landroid/filterpacks/videosrc/MediaSource;->mPlaying:Z

    #@15
    .line 372
    iput-boolean v1, p0, Landroid/filterpacks/videosrc/MediaSource;->mPaused:Z

    #@17
    .line 373
    iput-boolean v1, p0, Landroid/filterpacks/videosrc/MediaSource;->mCompleted:Z

    #@19
    .line 374
    iput-boolean v1, p0, Landroid/filterpacks/videosrc/MediaSource;->mError:Z

    #@1b
    .line 375
    iput-boolean v1, p0, Landroid/filterpacks/videosrc/MediaSource;->mNewFrameAvailable:Z

    #@1d
    .line 377
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@1f
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    #@22
    .line 378
    iput-object v2, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@24
    .line 379
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@26
    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    #@29
    .line 380
    iput-object v2, p0, Landroid/filterpacks/videosrc/MediaSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@2b
    .line 381
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mLogVerbose:Z

    #@2d
    if-eqz v0, :cond_36

    #@2f
    const-string v0, "MediaSource"

    #@31
    const-string v1, "MediaSource closed"

    #@33
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 382
    :cond_36
    return-void
.end method

.method public fieldPortValueUpdated(Ljava/lang/String;Landroid/filterfw/core/FilterContext;)V
    .registers 6
    .parameter "name"
    .parameter "context"

    #@0
    .prologue
    .line 399
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mLogVerbose:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    const-string v0, "MediaSource"

    #@6
    const-string v1, "Parameter update"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 400
    :cond_b
    const-string/jumbo v0, "sourceUrl"

    #@e
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_2f

    #@14
    .line 401
    invoke-virtual {p0}, Landroid/filterpacks/videosrc/MediaSource;->isOpen()Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_2e

    #@1a
    .line 402
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mLogVerbose:Z

    #@1c
    if-eqz v0, :cond_25

    #@1e
    const-string v0, "MediaSource"

    #@20
    const-string v1, "Opening new source URL"

    #@22
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 403
    :cond_25
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mSelectedIsUrl:Z

    #@27
    if-eqz v0, :cond_2e

    #@29
    .line 404
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mSelectedIsUrl:Z

    #@2b
    invoke-direct {p0, v0}, Landroid/filterpacks/videosrc/MediaSource;->setupMediaPlayer(Z)Z

    #@2e
    .line 439
    :cond_2e
    :goto_2e
    return-void

    #@2f
    .line 407
    :cond_2f
    const-string/jumbo v0, "sourceAsset"

    #@32
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@35
    move-result v0

    #@36
    if-eqz v0, :cond_53

    #@38
    .line 408
    invoke-virtual {p0}, Landroid/filterpacks/videosrc/MediaSource;->isOpen()Z

    #@3b
    move-result v0

    #@3c
    if-eqz v0, :cond_2e

    #@3e
    .line 409
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mLogVerbose:Z

    #@40
    if-eqz v0, :cond_49

    #@42
    const-string v0, "MediaSource"

    #@44
    const-string v1, "Opening new source FD"

    #@46
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    .line 410
    :cond_49
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mSelectedIsUrl:Z

    #@4b
    if-nez v0, :cond_2e

    #@4d
    .line 411
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mSelectedIsUrl:Z

    #@4f
    invoke-direct {p0, v0}, Landroid/filterpacks/videosrc/MediaSource;->setupMediaPlayer(Z)Z

    #@52
    goto :goto_2e

    #@53
    .line 414
    :cond_53
    const-string/jumbo v0, "loop"

    #@56
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@59
    move-result v0

    #@5a
    if-eqz v0, :cond_6a

    #@5c
    .line 415
    invoke-virtual {p0}, Landroid/filterpacks/videosrc/MediaSource;->isOpen()Z

    #@5f
    move-result v0

    #@60
    if-eqz v0, :cond_2e

    #@62
    .line 416
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@64
    iget-boolean v1, p0, Landroid/filterpacks/videosrc/MediaSource;->mLooping:Z

    #@66
    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    #@69
    goto :goto_2e

    #@6a
    .line 418
    :cond_6a
    const-string/jumbo v0, "sourceIsUrl"

    #@6d
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@70
    move-result v0

    #@71
    if-eqz v0, :cond_9a

    #@73
    .line 419
    invoke-virtual {p0}, Landroid/filterpacks/videosrc/MediaSource;->isOpen()Z

    #@76
    move-result v0

    #@77
    if-eqz v0, :cond_2e

    #@79
    .line 420
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mSelectedIsUrl:Z

    #@7b
    if-eqz v0, :cond_8e

    #@7d
    .line 421
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mLogVerbose:Z

    #@7f
    if-eqz v0, :cond_88

    #@81
    const-string v0, "MediaSource"

    #@83
    const-string v1, "Opening new source URL"

    #@85
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@88
    .line 425
    :cond_88
    :goto_88
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mSelectedIsUrl:Z

    #@8a
    invoke-direct {p0, v0}, Landroid/filterpacks/videosrc/MediaSource;->setupMediaPlayer(Z)Z

    #@8d
    goto :goto_2e

    #@8e
    .line 423
    :cond_8e
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mLogVerbose:Z

    #@90
    if-eqz v0, :cond_88

    #@92
    const-string v0, "MediaSource"

    #@94
    const-string v1, "Opening new source Asset"

    #@96
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@99
    goto :goto_88

    #@9a
    .line 427
    :cond_9a
    const-string/jumbo v0, "volume"

    #@9d
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a0
    move-result v0

    #@a1
    if-eqz v0, :cond_b4

    #@a3
    .line 428
    invoke-virtual {p0}, Landroid/filterpacks/videosrc/MediaSource;->isOpen()Z

    #@a6
    move-result v0

    #@a7
    if-eqz v0, :cond_2e

    #@a9
    .line 429
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@ab
    iget v1, p0, Landroid/filterpacks/videosrc/MediaSource;->mVolume:F

    #@ad
    iget v2, p0, Landroid/filterpacks/videosrc/MediaSource;->mVolume:F

    #@af
    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setVolume(FF)V

    #@b2
    goto/16 :goto_2e

    #@b4
    .line 431
    :cond_b4
    const-string/jumbo v0, "orientation"

    #@b7
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ba
    move-result v0

    #@bb
    if-eqz v0, :cond_2e

    #@bd
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mGotSize:Z

    #@bf
    if-eqz v0, :cond_2e

    #@c1
    .line 432
    iget v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mOrientation:I

    #@c3
    if-eqz v0, :cond_cb

    #@c5
    iget v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mOrientation:I

    #@c7
    const/16 v1, 0xb4

    #@c9
    if-ne v0, v1, :cond_d9

    #@cb
    .line 433
    :cond_cb
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mOutputFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@cd
    iget v1, p0, Landroid/filterpacks/videosrc/MediaSource;->mWidth:I

    #@cf
    iget v2, p0, Landroid/filterpacks/videosrc/MediaSource;->mHeight:I

    #@d1
    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/MutableFrameFormat;->setDimensions(II)V

    #@d4
    .line 437
    :goto_d4
    const/4 v0, 0x1

    #@d5
    iput-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mOrientationUpdated:Z

    #@d7
    goto/16 :goto_2e

    #@d9
    .line 435
    :cond_d9
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mOutputFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@db
    iget v1, p0, Landroid/filterpacks/videosrc/MediaSource;->mHeight:I

    #@dd
    iget v2, p0, Landroid/filterpacks/videosrc/MediaSource;->mWidth:I

    #@df
    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/MutableFrameFormat;->setDimensions(II)V

    #@e2
    goto :goto_d4
.end method

.method public open(Landroid/filterfw/core/FilterContext;)V
    .registers 7
    .parameter "context"

    #@0
    .prologue
    .line 206
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mLogVerbose:Z

    #@2
    if-eqz v0, :cond_29

    #@4
    .line 207
    const-string v0, "MediaSource"

    #@6
    const-string v1, "Opening MediaSource"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 208
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mSelectedIsUrl:Z

    #@d
    if-eqz v0, :cond_58

    #@f
    .line 209
    const-string v0, "MediaSource"

    #@11
    new-instance v1, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v2, "Current URL is "

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    iget-object v2, p0, Landroid/filterpacks/videosrc/MediaSource;->mSourceUrl:Ljava/lang/String;

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v1

    #@26
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 215
    :cond_29
    :goto_29
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@2c
    move-result-object v0

    #@2d
    iget-object v1, p0, Landroid/filterpacks/videosrc/MediaSource;->mOutputFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@2f
    const/16 v2, 0x68

    #@31
    const-wide/16 v3, 0x0

    #@33
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/filterfw/core/FrameManager;->newBoundFrame(Landroid/filterfw/core/FrameFormat;IJ)Landroid/filterfw/core/Frame;

    #@36
    move-result-object v0

    #@37
    check-cast v0, Landroid/filterfw/core/GLFrame;

    #@39
    iput-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaFrame:Landroid/filterfw/core/GLFrame;

    #@3b
    .line 220
    new-instance v0, Landroid/graphics/SurfaceTexture;

    #@3d
    iget-object v1, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaFrame:Landroid/filterfw/core/GLFrame;

    #@3f
    invoke-virtual {v1}, Landroid/filterfw/core/GLFrame;->getTextureId()I

    #@42
    move-result v1

    #@43
    invoke-direct {v0, v1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    #@46
    iput-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@48
    .line 222
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mSelectedIsUrl:Z

    #@4a
    invoke-direct {p0, v0}, Landroid/filterpacks/videosrc/MediaSource;->setupMediaPlayer(Z)Z

    #@4d
    move-result v0

    #@4e
    if-nez v0, :cond_60

    #@50
    .line 223
    new-instance v0, Ljava/lang/RuntimeException;

    #@52
    const-string v1, "Error setting up MediaPlayer!"

    #@54
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@57
    throw v0

    #@58
    .line 211
    :cond_58
    const-string v0, "MediaSource"

    #@5a
    const-string v1, "Current source is Asset!"

    #@5c
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    goto :goto_29

    #@60
    .line 225
    :cond_60
    return-void
.end method

.method public declared-synchronized pauseVideo(Z)V
    .registers 3
    .parameter "pauseState"

    #@0
    .prologue
    .line 442
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p0}, Landroid/filterpacks/videosrc/MediaSource;->isOpen()Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_12

    #@7
    .line 443
    if-eqz p1, :cond_16

    #@9
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mPaused:Z

    #@b
    if-nez v0, :cond_16

    #@d
    .line 444
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@f
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    #@12
    .line 449
    :cond_12
    :goto_12
    iput-boolean p1, p0, Landroid/filterpacks/videosrc/MediaSource;->mPaused:Z
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_22

    #@14
    .line 450
    monitor-exit p0

    #@15
    return-void

    #@16
    .line 445
    :cond_16
    if-nez p1, :cond_12

    #@18
    :try_start_18
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mPaused:Z

    #@1a
    if-eqz v0, :cond_12

    #@1c
    .line 446
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@1e
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V
    :try_end_21
    .catchall {:try_start_18 .. :try_end_21} :catchall_22

    #@21
    goto :goto_12

    #@22
    .line 442
    :catchall_22
    move-exception v0

    #@23
    monitor-exit p0

    #@24
    throw v0
.end method

.method protected prepare(Landroid/filterfw/core/FilterContext;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    const/high16 v3, 0x3f80

    #@2
    .line 194
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mLogVerbose:Z

    #@4
    if-eqz v0, :cond_d

    #@6
    const-string v0, "MediaSource"

    #@8
    const-string v1, "Preparing MediaSource"

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 196
    :cond_d
    new-instance v0, Landroid/filterfw/core/ShaderProgram;

    #@f
    const-string v1, "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nuniform samplerExternalOES tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n"

    #@11
    invoke-direct {v0, p1, v1}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;)V

    #@14
    iput-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mFrameExtractor:Landroid/filterfw/core/ShaderProgram;

    #@16
    .line 199
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mFrameExtractor:Landroid/filterfw/core/ShaderProgram;

    #@18
    const/4 v1, 0x0

    #@19
    const/high16 v2, -0x4080

    #@1b
    invoke-virtual {v0, v1, v3, v3, v2}, Landroid/filterfw/core/ShaderProgram;->setSourceRect(FFFF)V

    #@1e
    .line 201
    invoke-direct {p0}, Landroid/filterpacks/videosrc/MediaSource;->createFormats()V

    #@21
    .line 202
    return-void
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 22
    .parameter "context"

    #@0
    .prologue
    .line 230
    move-object/from16 v0, p0

    #@2
    iget-boolean v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mLogVerbose:Z

    #@4
    if-eqz v3, :cond_d

    #@6
    const-string v3, "MediaSource"

    #@8
    const-string v5, "Processing new frame"

    #@a
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 232
    :cond_d
    move-object/from16 v0, p0

    #@f
    iget-object v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@11
    if-nez v3, :cond_1b

    #@13
    .line 234
    new-instance v3, Ljava/lang/NullPointerException;

    #@15
    const-string v5, "Unexpected null media player!"

    #@17
    invoke-direct {v3, v5}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v3

    #@1b
    .line 237
    :cond_1b
    move-object/from16 v0, p0

    #@1d
    iget-boolean v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mError:Z

    #@1f
    if-eqz v3, :cond_3b

    #@21
    .line 239
    move-object/from16 v0, p0

    #@23
    iget-object v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@25
    if-eqz v3, :cond_33

    #@27
    .line 241
    move-object/from16 v0, p0

    #@29
    iget-object v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@2b
    invoke-virtual {v3}, Landroid/media/MediaPlayer;->release()V

    #@2e
    .line 242
    const/4 v3, 0x0

    #@2f
    move-object/from16 v0, p0

    #@31
    iput-object v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@33
    .line 244
    :cond_33
    new-instance v3, Ljava/lang/RuntimeException;

    #@35
    const-string v5, "MediaPlayer happen ERROR"

    #@37
    invoke-direct {v3, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@3a
    throw v3

    #@3b
    .line 247
    :cond_3b
    move-object/from16 v0, p0

    #@3d
    iget-boolean v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mCompleted:Z

    #@3f
    if-eqz v3, :cond_4a

    #@41
    .line 249
    const-string/jumbo v3, "video"

    #@44
    move-object/from16 v0, p0

    #@46
    invoke-virtual {v0, v3}, Landroid/filterpacks/videosrc/MediaSource;->closeOutputPort(Ljava/lang/String;)V

    #@49
    .line 362
    :goto_49
    return-void

    #@4a
    .line 253
    :cond_4a
    move-object/from16 v0, p0

    #@4c
    iget-boolean v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mPlaying:Z

    #@4e
    if-nez v3, :cond_ac

    #@50
    .line 254
    const/16 v19, 0x0

    #@52
    .line 255
    .local v19, waitCount:I
    move-object/from16 v0, p0

    #@54
    iget-boolean v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mLogVerbose:Z

    #@56
    if-eqz v3, :cond_5f

    #@58
    const-string v3, "MediaSource"

    #@5a
    const-string v5, "Waiting for preparation to complete"

    #@5c
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    .line 256
    :cond_5f
    move-object/from16 v0, p0

    #@61
    iget-boolean v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mGotSize:Z

    #@63
    if-eqz v3, :cond_6b

    #@65
    move-object/from16 v0, p0

    #@67
    iget-boolean v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mPrepared:Z

    #@69
    if-nez v3, :cond_98

    #@6b
    .line 258
    :cond_6b
    const-wide/16 v5, 0x64

    #@6d
    :try_start_6d
    move-object/from16 v0, p0

    #@6f
    invoke-virtual {v0, v5, v6}, Ljava/lang/Object;->wait(J)V
    :try_end_72
    .catch Ljava/lang/InterruptedException; {:try_start_6d .. :try_end_72} :catch_27c

    #@72
    .line 262
    :goto_72
    move-object/from16 v0, p0

    #@74
    iget-boolean v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mCompleted:Z

    #@76
    if-eqz v3, :cond_81

    #@78
    .line 264
    const-string/jumbo v3, "video"

    #@7b
    move-object/from16 v0, p0

    #@7d
    invoke-virtual {v0, v3}, Landroid/filterpacks/videosrc/MediaSource;->closeOutputPort(Ljava/lang/String;)V

    #@80
    goto :goto_49

    #@81
    .line 267
    :cond_81
    add-int/lit8 v19, v19, 0x1

    #@83
    .line 268
    const/16 v3, 0x64

    #@85
    move/from16 v0, v19

    #@87
    if-ne v0, v3, :cond_5f

    #@89
    .line 269
    move-object/from16 v0, p0

    #@8b
    iget-object v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@8d
    invoke-virtual {v3}, Landroid/media/MediaPlayer;->release()V

    #@90
    .line 270
    new-instance v3, Ljava/lang/RuntimeException;

    #@92
    const-string v5, "MediaPlayer timed out while preparing!"

    #@94
    invoke-direct {v3, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@97
    throw v3

    #@98
    .line 273
    :cond_98
    move-object/from16 v0, p0

    #@9a
    iget-boolean v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mLogVerbose:Z

    #@9c
    if-eqz v3, :cond_a5

    #@9e
    const-string v3, "MediaSource"

    #@a0
    const-string v5, "Starting playback"

    #@a2
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@a5
    .line 274
    :cond_a5
    move-object/from16 v0, p0

    #@a7
    iget-object v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@a9
    invoke-virtual {v3}, Landroid/media/MediaPlayer;->start()V

    #@ac
    .line 279
    .end local v19           #waitCount:I
    :cond_ac
    move-object/from16 v0, p0

    #@ae
    iget-boolean v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mPaused:Z

    #@b0
    if-eqz v3, :cond_b8

    #@b2
    move-object/from16 v0, p0

    #@b4
    iget-boolean v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mPlaying:Z

    #@b6
    if-nez v3, :cond_128

    #@b8
    .line 280
    :cond_b8
    move-object/from16 v0, p0

    #@ba
    iget-boolean v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mWaitForNewFrame:Z

    #@bc
    if-eqz v3, :cond_11c

    #@be
    .line 281
    move-object/from16 v0, p0

    #@c0
    iget-boolean v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mLogVerbose:Z

    #@c2
    if-eqz v3, :cond_cb

    #@c4
    const-string v3, "MediaSource"

    #@c6
    const-string v5, "Waiting for new frame"

    #@c8
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@cb
    .line 283
    :cond_cb
    const/16 v19, 0x0

    #@cd
    .line 284
    .restart local v19       #waitCount:I
    :goto_cd
    move-object/from16 v0, p0

    #@cf
    iget-boolean v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mNewFrameAvailable:Z

    #@d1
    if-nez v3, :cond_10a

    #@d3
    .line 285
    const/16 v3, 0xa

    #@d5
    move/from16 v0, v19

    #@d7
    if-ne v0, v3, :cond_f1

    #@d9
    .line 286
    move-object/from16 v0, p0

    #@db
    iget-boolean v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mCompleted:Z

    #@dd
    if-eqz v3, :cond_e9

    #@df
    .line 288
    const-string/jumbo v3, "video"

    #@e2
    move-object/from16 v0, p0

    #@e4
    invoke-virtual {v0, v3}, Landroid/filterpacks/videosrc/MediaSource;->closeOutputPort(Ljava/lang/String;)V

    #@e7
    goto/16 :goto_49

    #@e9
    .line 291
    :cond_e9
    new-instance v3, Ljava/lang/RuntimeException;

    #@eb
    const-string v5, "Timeout waiting for new frame!"

    #@ed
    invoke-direct {v3, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@f0
    throw v3

    #@f1
    .line 295
    :cond_f1
    const-wide/16 v5, 0x64

    #@f3
    :try_start_f3
    move-object/from16 v0, p0

    #@f5
    invoke-virtual {v0, v5, v6}, Ljava/lang/Object;->wait(J)V
    :try_end_f8
    .catch Ljava/lang/InterruptedException; {:try_start_f3 .. :try_end_f8} :catch_fb

    #@f8
    .line 300
    :cond_f8
    :goto_f8
    add-int/lit8 v19, v19, 0x1

    #@fa
    goto :goto_cd

    #@fb
    .line 296
    :catch_fb
    move-exception v14

    #@fc
    .line 297
    .local v14, e:Ljava/lang/InterruptedException;
    move-object/from16 v0, p0

    #@fe
    iget-boolean v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mLogVerbose:Z

    #@100
    if-eqz v3, :cond_f8

    #@102
    const-string v3, "MediaSource"

    #@104
    const-string v5, "interrupted"

    #@106
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@109
    goto :goto_f8

    #@10a
    .line 302
    .end local v14           #e:Ljava/lang/InterruptedException;
    :cond_10a
    const/4 v3, 0x0

    #@10b
    move-object/from16 v0, p0

    #@10d
    iput-boolean v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mNewFrameAvailable:Z

    #@10f
    .line 303
    move-object/from16 v0, p0

    #@111
    iget-boolean v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mLogVerbose:Z

    #@113
    if-eqz v3, :cond_11c

    #@115
    const-string v3, "MediaSource"

    #@117
    const-string v5, "Got new frame"

    #@119
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@11c
    .line 306
    .end local v19           #waitCount:I
    :cond_11c
    move-object/from16 v0, p0

    #@11e
    iget-object v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@120
    invoke-virtual {v3}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    #@123
    .line 307
    const/4 v3, 0x1

    #@124
    move-object/from16 v0, p0

    #@126
    iput-boolean v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mOrientationUpdated:Z

    #@128
    .line 309
    :cond_128
    move-object/from16 v0, p0

    #@12a
    iget-boolean v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mOrientationUpdated:Z

    #@12c
    if-eqz v3, :cond_1fb

    #@12e
    .line 310
    const/16 v3, 0x10

    #@130
    new-array v4, v3, [F

    #@132
    .line 311
    .local v4, surfaceTransform:[F
    move-object/from16 v0, p0

    #@134
    iget-object v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@136
    invoke-virtual {v3, v4}, Landroid/graphics/SurfaceTexture;->getTransformMatrix([F)V

    #@139
    .line 313
    const/16 v3, 0x10

    #@13b
    new-array v2, v3, [F

    #@13d
    .line 314
    .local v2, sourceCoords:[F
    move-object/from16 v0, p0

    #@13f
    iget v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mOrientation:I

    #@141
    sparse-switch v3, :sswitch_data_280

    #@144
    .line 317
    const/4 v3, 0x0

    #@145
    const/4 v5, 0x0

    #@146
    sget-object v6, Landroid/filterpacks/videosrc/MediaSource;->mSourceCoords_0:[F

    #@148
    const/4 v7, 0x0

    #@149
    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    #@14c
    .line 337
    :goto_14c
    move-object/from16 v0, p0

    #@14e
    iget-boolean v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mLogVerbose:Z

    #@150
    if-eqz v3, :cond_1d3

    #@152
    .line 338
    const-string v3, "MediaSource"

    #@154
    new-instance v5, Ljava/lang/StringBuilder;

    #@156
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@159
    const-string v6, "OrientationHint = "

    #@15b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15e
    move-result-object v5

    #@15f
    move-object/from16 v0, p0

    #@161
    iget v6, v0, Landroid/filterpacks/videosrc/MediaSource;->mOrientation:I

    #@163
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@166
    move-result-object v5

    #@167
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16a
    move-result-object v5

    #@16b
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@16e
    .line 339
    const-string v3, "SetSourceRegion: %.2f, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f"

    #@170
    const/16 v5, 0x8

    #@172
    new-array v5, v5, [Ljava/lang/Object;

    #@174
    const/4 v6, 0x0

    #@175
    const/4 v7, 0x4

    #@176
    aget v7, v2, v7

    #@178
    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@17b
    move-result-object v7

    #@17c
    aput-object v7, v5, v6

    #@17e
    const/4 v6, 0x1

    #@17f
    const/4 v7, 0x5

    #@180
    aget v7, v2, v7

    #@182
    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@185
    move-result-object v7

    #@186
    aput-object v7, v5, v6

    #@188
    const/4 v6, 0x2

    #@189
    const/4 v7, 0x0

    #@18a
    aget v7, v2, v7

    #@18c
    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@18f
    move-result-object v7

    #@190
    aput-object v7, v5, v6

    #@192
    const/4 v6, 0x3

    #@193
    const/4 v7, 0x1

    #@194
    aget v7, v2, v7

    #@196
    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@199
    move-result-object v7

    #@19a
    aput-object v7, v5, v6

    #@19c
    const/4 v6, 0x4

    #@19d
    const/16 v7, 0xc

    #@19f
    aget v7, v2, v7

    #@1a1
    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@1a4
    move-result-object v7

    #@1a5
    aput-object v7, v5, v6

    #@1a7
    const/4 v6, 0x5

    #@1a8
    const/16 v7, 0xd

    #@1aa
    aget v7, v2, v7

    #@1ac
    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@1af
    move-result-object v7

    #@1b0
    aput-object v7, v5, v6

    #@1b2
    const/4 v6, 0x6

    #@1b3
    const/16 v7, 0x8

    #@1b5
    aget v7, v2, v7

    #@1b7
    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@1ba
    move-result-object v7

    #@1bb
    aput-object v7, v5, v6

    #@1bd
    const/4 v6, 0x7

    #@1be
    const/16 v7, 0x9

    #@1c0
    aget v7, v2, v7

    #@1c2
    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@1c5
    move-result-object v7

    #@1c6
    aput-object v7, v5, v6

    #@1c8
    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@1cb
    move-result-object v16

    #@1cc
    .line 342
    .local v16, temp:Ljava/lang/String;
    const-string v3, "MediaSource"

    #@1ce
    move-object/from16 v0, v16

    #@1d0
    invoke-static {v3, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1d3
    .line 344
    .end local v16           #temp:Ljava/lang/String;
    :cond_1d3
    move-object/from16 v0, p0

    #@1d5
    iget-object v5, v0, Landroid/filterpacks/videosrc/MediaSource;->mFrameExtractor:Landroid/filterfw/core/ShaderProgram;

    #@1d7
    const/4 v3, 0x4

    #@1d8
    aget v6, v2, v3

    #@1da
    const/4 v3, 0x5

    #@1db
    aget v7, v2, v3

    #@1dd
    const/4 v3, 0x0

    #@1de
    aget v8, v2, v3

    #@1e0
    const/4 v3, 0x1

    #@1e1
    aget v9, v2, v3

    #@1e3
    const/16 v3, 0xc

    #@1e5
    aget v10, v2, v3

    #@1e7
    const/16 v3, 0xd

    #@1e9
    aget v11, v2, v3

    #@1eb
    const/16 v3, 0x8

    #@1ed
    aget v12, v2, v3

    #@1ef
    const/16 v3, 0x9

    #@1f1
    aget v13, v2, v3

    #@1f3
    invoke-virtual/range {v5 .. v13}, Landroid/filterfw/core/ShaderProgram;->setSourceRegion(FFFFFFFF)Z

    #@1f6
    .line 348
    const/4 v3, 0x0

    #@1f7
    move-object/from16 v0, p0

    #@1f9
    iput-boolean v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mOrientationUpdated:Z

    #@1fb
    .line 351
    .end local v2           #sourceCoords:[F
    .end local v4           #surfaceTransform:[F
    :cond_1fb
    invoke-virtual/range {p1 .. p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@1fe
    move-result-object v3

    #@1ff
    move-object/from16 v0, p0

    #@201
    iget-object v5, v0, Landroid/filterpacks/videosrc/MediaSource;->mOutputFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@203
    invoke-virtual {v3, v5}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@206
    move-result-object v15

    #@207
    .line 352
    .local v15, output:Landroid/filterfw/core/Frame;
    move-object/from16 v0, p0

    #@209
    iget-object v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mFrameExtractor:Landroid/filterfw/core/ShaderProgram;

    #@20b
    move-object/from16 v0, p0

    #@20d
    iget-object v5, v0, Landroid/filterpacks/videosrc/MediaSource;->mMediaFrame:Landroid/filterfw/core/GLFrame;

    #@20f
    invoke-virtual {v3, v5, v15}, Landroid/filterfw/core/ShaderProgram;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@212
    .line 354
    move-object/from16 v0, p0

    #@214
    iget-object v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@216
    invoke-virtual {v3}, Landroid/graphics/SurfaceTexture;->getTimestamp()J

    #@219
    move-result-wide v17

    #@21a
    .line 355
    .local v17, timestamp:J
    move-object/from16 v0, p0

    #@21c
    iget-boolean v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mLogVerbose:Z

    #@21e
    if-eqz v3, :cond_247

    #@220
    const-string v3, "MediaSource"

    #@222
    new-instance v5, Ljava/lang/StringBuilder;

    #@224
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@227
    const-string v6, "Timestamp: "

    #@229
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22c
    move-result-object v5

    #@22d
    move-wide/from16 v0, v17

    #@22f
    long-to-double v6, v0

    #@230
    const-wide v8, 0x41cdcd6500000000L

    #@235
    div-double/2addr v6, v8

    #@236
    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@239
    move-result-object v5

    #@23a
    const-string v6, " s"

    #@23c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23f
    move-result-object v5

    #@240
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@243
    move-result-object v5

    #@244
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@247
    .line 356
    :cond_247
    move-wide/from16 v0, v17

    #@249
    invoke-virtual {v15, v0, v1}, Landroid/filterfw/core/Frame;->setTimestamp(J)V

    #@24c
    .line 358
    const-string/jumbo v3, "video"

    #@24f
    move-object/from16 v0, p0

    #@251
    invoke-virtual {v0, v3, v15}, Landroid/filterpacks/videosrc/MediaSource;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@254
    .line 359
    invoke-virtual {v15}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@257
    .line 361
    const/4 v3, 0x1

    #@258
    move-object/from16 v0, p0

    #@25a
    iput-boolean v3, v0, Landroid/filterpacks/videosrc/MediaSource;->mPlaying:Z

    #@25c
    goto/16 :goto_49

    #@25e
    .line 322
    .end local v15           #output:Landroid/filterfw/core/Frame;
    .end local v17           #timestamp:J
    .restart local v2       #sourceCoords:[F
    .restart local v4       #surfaceTransform:[F
    :sswitch_25e
    const/4 v3, 0x0

    #@25f
    const/4 v5, 0x0

    #@260
    sget-object v6, Landroid/filterpacks/videosrc/MediaSource;->mSourceCoords_90:[F

    #@262
    const/4 v7, 0x0

    #@263
    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    #@266
    goto/16 :goto_14c

    #@268
    .line 327
    :sswitch_268
    const/4 v3, 0x0

    #@269
    const/4 v5, 0x0

    #@26a
    sget-object v6, Landroid/filterpacks/videosrc/MediaSource;->mSourceCoords_180:[F

    #@26c
    const/4 v7, 0x0

    #@26d
    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    #@270
    goto/16 :goto_14c

    #@272
    .line 332
    :sswitch_272
    const/4 v3, 0x0

    #@273
    const/4 v5, 0x0

    #@274
    sget-object v6, Landroid/filterpacks/videosrc/MediaSource;->mSourceCoords_270:[F

    #@276
    const/4 v7, 0x0

    #@277
    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    #@27a
    goto/16 :goto_14c

    #@27c
    .line 259
    .end local v2           #sourceCoords:[F
    .end local v4           #surfaceTransform:[F
    .restart local v19       #waitCount:I
    :catch_27c
    move-exception v3

    #@27d
    goto/16 :goto_72

    #@27f
    .line 314
    nop

    #@280
    :sswitch_data_280
    .sparse-switch
        0x5a -> :sswitch_25e
        0xb4 -> :sswitch_268
        0x10e -> :sswitch_272
    .end sparse-switch
.end method

.method public setupPorts()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x3

    #@1
    .line 183
    const-string/jumbo v0, "video"

    #@4
    invoke-static {v1, v1}, Landroid/filterfw/format/ImageFormat;->create(II)Landroid/filterfw/core/MutableFrameFormat;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/videosrc/MediaSource;->addOutputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@b
    .line 185
    return-void
.end method

.method public tearDown(Landroid/filterfw/core/FilterContext;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 386
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaFrame:Landroid/filterfw/core/GLFrame;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 387
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource;->mMediaFrame:Landroid/filterfw/core/GLFrame;

    #@6
    invoke-virtual {v0}, Landroid/filterfw/core/GLFrame;->release()Landroid/filterfw/core/Frame;

    #@9
    .line 389
    :cond_9
    return-void
.end method
