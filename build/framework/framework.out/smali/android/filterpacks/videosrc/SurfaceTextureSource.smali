.class public Landroid/filterpacks/videosrc/SurfaceTextureSource;
.super Landroid/filterfw/core/Filter;
.source "SurfaceTextureSource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/filterpacks/videosrc/SurfaceTextureSource$SurfaceTextureSourceListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SurfaceTextureSource"

.field private static final mLogVerbose:Z

.field private static final mSourceCoords:[F


# instance fields
.field private mCloseOnTimeout:Z
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "closeOnTimeout"
    .end annotation
.end field

.field private mFirstFrame:Z

.field private mFrameExtractor:Landroid/filterfw/core/ShaderProgram;

.field private mFrameTransform:[F

.field private mHeight:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "height"
    .end annotation
.end field

.field private mMappedCoords:[F

.field private mMediaFrame:Landroid/filterfw/core/GLFrame;

.field private mNewFrameAvailable:Landroid/os/ConditionVariable;

.field private mOutputFormat:Landroid/filterfw/core/MutableFrameFormat;

.field private final mRenderShader:Ljava/lang/String;

.field private mSourceListener:Landroid/filterpacks/videosrc/SurfaceTextureSource$SurfaceTextureSourceListener;
    .annotation runtime Landroid/filterfw/core/GenerateFinalPort;
        name = "sourceListener"
    .end annotation
.end field

.field private mSurfaceTexture:Landroid/graphics/SurfaceTexture;

.field private mWaitForNewFrame:Z
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "waitForNewFrame"
    .end annotation
.end field

.field private mWaitTimeout:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "waitTimeout"
    .end annotation
.end field

.field private mWidth:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "width"
    .end annotation
.end field

.field private onFrameAvailableListener:Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 125
    const/16 v0, 0x10

    #@2
    new-array v0, v0, [F

    #@4
    fill-array-data v0, :array_14

    #@7
    sput-object v0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mSourceCoords:[F

    #@9
    .line 142
    const-string v0, "SurfaceTextureSource"

    #@b
    const/4 v1, 0x2

    #@c
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@f
    move-result v0

    #@10
    sput-boolean v0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mLogVerbose:Z

    #@12
    return-void

    #@13
    .line 125
    nop

    #@14
    :array_14
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter "name"

    #@0
    .prologue
    const/16 v1, 0x10

    #@2
    .line 145
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@5
    .line 98
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mWaitForNewFrame:Z

    #@8
    .line 104
    const/16 v0, 0x3e8

    #@a
    iput v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mWaitTimeout:I

    #@c
    .line 110
    const/4 v0, 0x0

    #@d
    iput-boolean v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mCloseOnTimeout:Z

    #@f
    .line 130
    const-string v0, "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nuniform samplerExternalOES tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n"

    #@11
    iput-object v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mRenderShader:Ljava/lang/String;

    #@13
    .line 258
    new-instance v0, Landroid/filterpacks/videosrc/SurfaceTextureSource$1;

    #@15
    invoke-direct {v0, p0}, Landroid/filterpacks/videosrc/SurfaceTextureSource$1;-><init>(Landroid/filterpacks/videosrc/SurfaceTextureSource;)V

    #@18
    iput-object v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->onFrameAvailableListener:Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;

    #@1a
    .line 146
    new-instance v0, Landroid/os/ConditionVariable;

    #@1c
    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    #@1f
    iput-object v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mNewFrameAvailable:Landroid/os/ConditionVariable;

    #@21
    .line 147
    new-array v0, v1, [F

    #@23
    iput-object v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mFrameTransform:[F

    #@25
    .line 148
    new-array v0, v1, [F

    #@27
    iput-object v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mMappedCoords:[F

    #@29
    .line 149
    return-void
.end method

.method static synthetic access$000()Z
    .registers 1

    #@0
    .prologue
    .line 62
    sget-boolean v0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mLogVerbose:Z

    #@2
    return v0
.end method

.method static synthetic access$100(Landroid/filterpacks/videosrc/SurfaceTextureSource;)Landroid/os/ConditionVariable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mNewFrameAvailable:Landroid/os/ConditionVariable;

    #@2
    return-object v0
.end method

.method private createFormats()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x3

    #@1
    .line 159
    iget v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mWidth:I

    #@3
    iget v1, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mHeight:I

    #@5
    invoke-static {v0, v1, v2, v2}, Landroid/filterfw/format/ImageFormat;->create(IIII)Landroid/filterfw/core/MutableFrameFormat;

    #@8
    move-result-object v0

    #@9
    iput-object v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mOutputFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@b
    .line 162
    return-void
.end method


# virtual methods
.method public close(Landroid/filterfw/core/FilterContext;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 238
    sget-boolean v0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mLogVerbose:Z

    #@3
    if-eqz v0, :cond_c

    #@5
    const-string v0, "SurfaceTextureSource"

    #@7
    const-string v1, "SurfaceTextureSource closed"

    #@9
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 239
    :cond_c
    iget-object v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mSourceListener:Landroid/filterpacks/videosrc/SurfaceTextureSource$SurfaceTextureSourceListener;

    #@e
    invoke-interface {v0, v2}, Landroid/filterpacks/videosrc/SurfaceTextureSource$SurfaceTextureSourceListener;->onSurfaceTextureSourceReady(Landroid/graphics/SurfaceTexture;)V

    #@11
    .line 240
    iget-object v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@13
    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    #@16
    .line 241
    iput-object v2, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@18
    .line 242
    return-void
.end method

.method public fieldPortValueUpdated(Ljava/lang/String;Landroid/filterfw/core/FilterContext;)V
    .registers 6
    .parameter "name"
    .parameter "context"

    #@0
    .prologue
    .line 253
    const-string/jumbo v0, "width"

    #@3
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_11

    #@9
    const-string v0, "height"

    #@b
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_1a

    #@11
    .line 254
    :cond_11
    iget-object v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mOutputFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@13
    iget v1, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mWidth:I

    #@15
    iget v2, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mHeight:I

    #@17
    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/MutableFrameFormat;->setDimensions(II)V

    #@1a
    .line 256
    :cond_1a
    return-void
.end method

.method public open(Landroid/filterfw/core/FilterContext;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 181
    sget-boolean v0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mLogVerbose:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    const-string v0, "SurfaceTextureSource"

    #@6
    const-string v1, "Opening SurfaceTextureSource"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 183
    :cond_b
    new-instance v0, Landroid/graphics/SurfaceTexture;

    #@d
    iget-object v1, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mMediaFrame:Landroid/filterfw/core/GLFrame;

    #@f
    invoke-virtual {v1}, Landroid/filterfw/core/GLFrame;->getTextureId()I

    #@12
    move-result v1

    #@13
    invoke-direct {v0, v1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    #@16
    iput-object v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@18
    .line 185
    iget-object v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@1a
    iget-object v1, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->onFrameAvailableListener:Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;

    #@1c
    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    #@1f
    .line 187
    iget-object v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mSourceListener:Landroid/filterpacks/videosrc/SurfaceTextureSource$SurfaceTextureSourceListener;

    #@21
    iget-object v1, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@23
    invoke-interface {v0, v1}, Landroid/filterpacks/videosrc/SurfaceTextureSource$SurfaceTextureSourceListener;->onSurfaceTextureSourceReady(Landroid/graphics/SurfaceTexture;)V

    #@26
    .line 188
    const/4 v0, 0x1

    #@27
    iput-boolean v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mFirstFrame:Z

    #@29
    .line 189
    return-void
.end method

.method protected prepare(Landroid/filterfw/core/FilterContext;)V
    .registers 7
    .parameter "context"

    #@0
    .prologue
    .line 166
    sget-boolean v0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mLogVerbose:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    const-string v0, "SurfaceTextureSource"

    #@6
    const-string v1, "Preparing SurfaceTextureSource"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 168
    :cond_b
    invoke-direct {p0}, Landroid/filterpacks/videosrc/SurfaceTextureSource;->createFormats()V

    #@e
    .line 171
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@11
    move-result-object v0

    #@12
    iget-object v1, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mOutputFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@14
    const/16 v2, 0x68

    #@16
    const-wide/16 v3, 0x0

    #@18
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/filterfw/core/FrameManager;->newBoundFrame(Landroid/filterfw/core/FrameFormat;IJ)Landroid/filterfw/core/Frame;

    #@1b
    move-result-object v0

    #@1c
    check-cast v0, Landroid/filterfw/core/GLFrame;

    #@1e
    iput-object v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mMediaFrame:Landroid/filterfw/core/GLFrame;

    #@20
    .line 176
    new-instance v0, Landroid/filterfw/core/ShaderProgram;

    #@22
    const-string v1, "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nuniform samplerExternalOES tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n"

    #@24
    invoke-direct {v0, p1, v1}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;)V

    #@27
    iput-object v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mFrameExtractor:Landroid/filterfw/core/ShaderProgram;

    #@29
    .line 177
    return-void
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 14
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 193
    sget-boolean v0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mLogVerbose:Z

    #@3
    if-eqz v0, :cond_c

    #@5
    const-string v0, "SurfaceTextureSource"

    #@7
    const-string v2, "Processing new frame"

    #@9
    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 196
    :cond_c
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mWaitForNewFrame:Z

    #@e
    if-nez v0, :cond_14

    #@10
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mFirstFrame:Z

    #@12
    if-eqz v0, :cond_4d

    #@14
    .line 198
    :cond_14
    iget v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mWaitTimeout:I

    #@16
    if-eqz v0, :cond_41

    #@18
    .line 199
    iget-object v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mNewFrameAvailable:Landroid/os/ConditionVariable;

    #@1a
    iget v2, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mWaitTimeout:I

    #@1c
    int-to-long v2, v2

    #@1d
    invoke-virtual {v0, v2, v3}, Landroid/os/ConditionVariable;->block(J)Z

    #@20
    move-result v9

    #@21
    .line 200
    .local v9, gotNewFrame:Z
    if-nez v9, :cond_46

    #@23
    .line 201
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mCloseOnTimeout:Z

    #@25
    if-nez v0, :cond_2f

    #@27
    .line 202
    new-instance v0, Ljava/lang/RuntimeException;

    #@29
    const-string v1, "Timeout waiting for new frame"

    #@2b
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@2e
    throw v0

    #@2f
    .line 204
    :cond_2f
    sget-boolean v0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mLogVerbose:Z

    #@31
    if-eqz v0, :cond_3a

    #@33
    const-string v0, "SurfaceTextureSource"

    #@35
    const-string v1, "Timeout waiting for a new frame. Closing."

    #@37
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 205
    :cond_3a
    const-string/jumbo v0, "video"

    #@3d
    invoke-virtual {p0, v0}, Landroid/filterpacks/videosrc/SurfaceTextureSource;->closeOutputPort(Ljava/lang/String;)V

    #@40
    .line 234
    .end local v9           #gotNewFrame:Z
    :goto_40
    return-void

    #@41
    .line 210
    :cond_41
    iget-object v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mNewFrameAvailable:Landroid/os/ConditionVariable;

    #@43
    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    #@46
    .line 212
    :cond_46
    iget-object v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mNewFrameAvailable:Landroid/os/ConditionVariable;

    #@48
    invoke-virtual {v0}, Landroid/os/ConditionVariable;->close()V

    #@4b
    .line 213
    iput-boolean v1, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mFirstFrame:Z

    #@4d
    .line 216
    :cond_4d
    iget-object v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@4f
    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    #@52
    .line 218
    iget-object v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@54
    iget-object v2, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mFrameTransform:[F

    #@56
    invoke-virtual {v0, v2}, Landroid/graphics/SurfaceTexture;->getTransformMatrix([F)V

    #@59
    .line 219
    iget-object v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mMappedCoords:[F

    #@5b
    iget-object v2, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mFrameTransform:[F

    #@5d
    sget-object v4, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mSourceCoords:[F

    #@5f
    move v3, v1

    #@60
    move v5, v1

    #@61
    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    #@64
    .line 222
    iget-object v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mFrameExtractor:Landroid/filterfw/core/ShaderProgram;

    #@66
    iget-object v2, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mMappedCoords:[F

    #@68
    aget v1, v2, v1

    #@6a
    iget-object v2, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mMappedCoords:[F

    #@6c
    const/4 v3, 0x1

    #@6d
    aget v2, v2, v3

    #@6f
    iget-object v3, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mMappedCoords:[F

    #@71
    const/4 v4, 0x4

    #@72
    aget v3, v3, v4

    #@74
    iget-object v4, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mMappedCoords:[F

    #@76
    const/4 v5, 0x5

    #@77
    aget v4, v4, v5

    #@79
    iget-object v5, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mMappedCoords:[F

    #@7b
    const/16 v6, 0x8

    #@7d
    aget v5, v5, v6

    #@7f
    iget-object v6, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mMappedCoords:[F

    #@81
    const/16 v7, 0x9

    #@83
    aget v6, v6, v7

    #@85
    iget-object v7, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mMappedCoords:[F

    #@87
    const/16 v8, 0xc

    #@89
    aget v7, v7, v8

    #@8b
    iget-object v8, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mMappedCoords:[F

    #@8d
    const/16 v11, 0xd

    #@8f
    aget v8, v8, v11

    #@91
    invoke-virtual/range {v0 .. v8}, Landroid/filterfw/core/ShaderProgram;->setSourceRegion(FFFFFFFF)Z

    #@94
    .line 227
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@97
    move-result-object v0

    #@98
    iget-object v1, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mOutputFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@9a
    invoke-virtual {v0, v1}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@9d
    move-result-object v10

    #@9e
    .line 228
    .local v10, output:Landroid/filterfw/core/Frame;
    iget-object v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mFrameExtractor:Landroid/filterfw/core/ShaderProgram;

    #@a0
    iget-object v1, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mMediaFrame:Landroid/filterfw/core/GLFrame;

    #@a2
    invoke-virtual {v0, v1, v10}, Landroid/filterfw/core/ShaderProgram;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@a5
    .line 230
    iget-object v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@a7
    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->getTimestamp()J

    #@aa
    move-result-wide v0

    #@ab
    invoke-virtual {v10, v0, v1}, Landroid/filterfw/core/Frame;->setTimestamp(J)V

    #@ae
    .line 232
    const-string/jumbo v0, "video"

    #@b1
    invoke-virtual {p0, v0, v10}, Landroid/filterpacks/videosrc/SurfaceTextureSource;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@b4
    .line 233
    invoke-virtual {v10}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@b7
    goto :goto_40
.end method

.method public setupPorts()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x3

    #@1
    .line 154
    const-string/jumbo v0, "video"

    #@4
    invoke-static {v1, v1}, Landroid/filterfw/format/ImageFormat;->create(II)Landroid/filterfw/core/MutableFrameFormat;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/videosrc/SurfaceTextureSource;->addOutputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@b
    .line 156
    return-void
.end method

.method public tearDown(Landroid/filterfw/core/FilterContext;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 246
    iget-object v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mMediaFrame:Landroid/filterfw/core/GLFrame;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 247
    iget-object v0, p0, Landroid/filterpacks/videosrc/SurfaceTextureSource;->mMediaFrame:Landroid/filterfw/core/GLFrame;

    #@6
    invoke-virtual {v0}, Landroid/filterfw/core/GLFrame;->release()Landroid/filterfw/core/Frame;

    #@9
    .line 249
    :cond_9
    return-void
.end method
