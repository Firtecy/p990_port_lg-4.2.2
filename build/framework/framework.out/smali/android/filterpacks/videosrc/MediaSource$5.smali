.class Landroid/filterpacks/videosrc/MediaSource$5;
.super Ljava/lang/Object;
.source "MediaSource.java"

# interfaces
.implements Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/filterpacks/videosrc/MediaSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/filterpacks/videosrc/MediaSource;


# direct methods
.method constructor <init>(Landroid/filterpacks/videosrc/MediaSource;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 592
    iput-object p1, p0, Landroid/filterpacks/videosrc/MediaSource$5;->this$0:Landroid/filterpacks/videosrc/MediaSource;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onFrameAvailable(Landroid/graphics/SurfaceTexture;)V
    .registers 5
    .parameter "surfaceTexture"

    #@0
    .prologue
    .line 594
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource$5;->this$0:Landroid/filterpacks/videosrc/MediaSource;

    #@2
    invoke-static {v0}, Landroid/filterpacks/videosrc/MediaSource;->access$000(Landroid/filterpacks/videosrc/MediaSource;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_f

    #@8
    const-string v0, "MediaSource"

    #@a
    const-string v1, "New frame from media player"

    #@c
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 595
    :cond_f
    iget-object v1, p0, Landroid/filterpacks/videosrc/MediaSource$5;->this$0:Landroid/filterpacks/videosrc/MediaSource;

    #@11
    monitor-enter v1

    #@12
    .line 596
    :try_start_12
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource$5;->this$0:Landroid/filterpacks/videosrc/MediaSource;

    #@14
    invoke-static {v0}, Landroid/filterpacks/videosrc/MediaSource;->access$000(Landroid/filterpacks/videosrc/MediaSource;)Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_21

    #@1a
    const-string v0, "MediaSource"

    #@1c
    const-string v2, "New frame: notify"

    #@1e
    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 597
    :cond_21
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource$5;->this$0:Landroid/filterpacks/videosrc/MediaSource;

    #@23
    const/4 v2, 0x1

    #@24
    invoke-static {v0, v2}, Landroid/filterpacks/videosrc/MediaSource;->access$902(Landroid/filterpacks/videosrc/MediaSource;Z)Z

    #@27
    .line 598
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource$5;->this$0:Landroid/filterpacks/videosrc/MediaSource;

    #@29
    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    #@2c
    .line 599
    iget-object v0, p0, Landroid/filterpacks/videosrc/MediaSource$5;->this$0:Landroid/filterpacks/videosrc/MediaSource;

    #@2e
    invoke-static {v0}, Landroid/filterpacks/videosrc/MediaSource;->access$000(Landroid/filterpacks/videosrc/MediaSource;)Z

    #@31
    move-result v0

    #@32
    if-eqz v0, :cond_3b

    #@34
    const-string v0, "MediaSource"

    #@36
    const-string v2, "New frame: notify done"

    #@38
    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 600
    :cond_3b
    monitor-exit v1

    #@3c
    .line 601
    return-void

    #@3d
    .line 600
    :catchall_3d
    move-exception v0

    #@3e
    monitor-exit v1
    :try_end_3f
    .catchall {:try_start_12 .. :try_end_3f} :catchall_3d

    #@3f
    throw v0
.end method
