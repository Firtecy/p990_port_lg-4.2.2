.class public Landroid/filterpacks/videosrc/CameraSource;
.super Landroid/filterfw/core/Filter;
.source "CameraSource.java"


# static fields
.field private static final NEWFRAME_TIMEOUT:I = 0x64

.field private static final NEWFRAME_TIMEOUT_REPEAT:I = 0xa

.field private static final TAG:Ljava/lang/String; = "CameraSource"

.field private static mCamera:Landroid/hardware/Camera; = null

.field private static final mFrameShader:Ljava/lang/String; = "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nuniform samplerExternalOES tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n"

.field private static final mSourceCoords:[F


# instance fields
.field private CameraAutoFocusOnCafCallback:Landroid/hardware/Camera$AutoFocusCallback;

.field private mCameraFrame:Landroid/filterfw/core/GLFrame;

.field private mCameraId:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "id"
    .end annotation
.end field

.field private mCameraParameters:Landroid/hardware/Camera$Parameters;

.field private mCameraTransform:[F

.field private mFocusmode:Ljava/lang/String;
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "focusmode"
    .end annotation
.end field

.field private mFps:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "framerate"
    .end annotation
.end field

.field private mFrameExtractor:Landroid/filterfw/core/ShaderProgram;

.field private mHeight:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "height"
    .end annotation
.end field

.field private final mLogVerbose:Z

.field private mMappedCoords:[F

.field private mNewFrameAvailable:Z

.field private mOrientation:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "orientationDegree"
    .end annotation
.end field

.field private mOutputFormat:Landroid/filterfw/core/MutableFrameFormat;

.field private mSurfaceTexture:Landroid/graphics/SurfaceTexture;

.field private mWaitForNewFrame:Z
    .annotation runtime Landroid/filterfw/core/GenerateFinalPort;
        hasDefault = true
        name = "waitForNewFrame"
    .end annotation
.end field

.field private mWidth:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "width"
    .end annotation
.end field

.field private onCameraFrameAvailableListener:Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 98
    const/16 v0, 0x10

    #@2
    new-array v0, v0, [F

    #@4
    fill-array-data v0, :array_a

    #@7
    sput-object v0, Landroid/filterpacks/videosrc/CameraSource;->mSourceCoords:[F

    #@9
    return-void

    #@a
    :array_a
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 5
    .parameter "name"

    #@0
    .prologue
    const/16 v2, 0x10

    #@2
    const/4 v1, 0x0

    #@3
    .line 123
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@6
    .line 55
    iput v1, p0, Landroid/filterpacks/videosrc/CameraSource;->mCameraId:I

    #@8
    .line 59
    const/16 v0, 0x140

    #@a
    iput v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mWidth:I

    #@c
    .line 63
    const/16 v0, 0xf0

    #@e
    iput v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mHeight:I

    #@10
    .line 67
    const/16 v0, 0x1e

    #@12
    iput v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mFps:I

    #@14
    .line 75
    const/4 v0, 0x1

    #@15
    iput-boolean v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mWaitForNewFrame:Z

    #@17
    .line 79
    iput v1, p0, Landroid/filterpacks/videosrc/CameraSource;->mOrientation:I

    #@19
    .line 82
    const-string v0, "auto"

    #@1b
    iput-object v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mFocusmode:Ljava/lang/String;

    #@1d
    .line 372
    new-instance v0, Landroid/filterpacks/videosrc/CameraSource$1;

    #@1f
    invoke-direct {v0, p0}, Landroid/filterpacks/videosrc/CameraSource$1;-><init>(Landroid/filterpacks/videosrc/CameraSource;)V

    #@22
    iput-object v0, p0, Landroid/filterpacks/videosrc/CameraSource;->onCameraFrameAvailableListener:Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;

    #@24
    .line 396
    new-instance v0, Landroid/filterpacks/videosrc/CameraSource$2;

    #@26
    invoke-direct {v0, p0}, Landroid/filterpacks/videosrc/CameraSource$2;-><init>(Landroid/filterpacks/videosrc/CameraSource;)V

    #@29
    iput-object v0, p0, Landroid/filterpacks/videosrc/CameraSource;->CameraAutoFocusOnCafCallback:Landroid/hardware/Camera$AutoFocusCallback;

    #@2b
    .line 124
    new-array v0, v2, [F

    #@2d
    iput-object v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mCameraTransform:[F

    #@2f
    .line 125
    new-array v0, v2, [F

    #@31
    iput-object v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mMappedCoords:[F

    #@33
    .line 127
    const-string v0, "CameraSource"

    #@35
    const/4 v1, 0x2

    #@36
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@39
    move-result v0

    #@3a
    iput-boolean v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mLogVerbose:Z

    #@3c
    .line 128
    return-void
.end method

.method static synthetic access$000(Landroid/filterpacks/videosrc/CameraSource;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mLogVerbose:Z

    #@2
    return v0
.end method

.method static synthetic access$102(Landroid/filterpacks/videosrc/CameraSource;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 50
    iput-boolean p1, p0, Landroid/filterpacks/videosrc/CameraSource;->mNewFrameAvailable:Z

    #@2
    return p1
.end method

.method private createFormats()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x3

    #@1
    .line 138
    iget v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mWidth:I

    #@3
    iget v1, p0, Landroid/filterpacks/videosrc/CameraSource;->mHeight:I

    #@5
    invoke-static {v0, v1, v2, v2}, Landroid/filterfw/format/ImageFormat;->create(IIII)Landroid/filterfw/core/MutableFrameFormat;

    #@8
    move-result-object v0

    #@9
    iput-object v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mOutputFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@b
    .line 141
    return-void
.end method

.method private findClosestFpsRange(ILandroid/hardware/Camera$Parameters;)[I
    .registers 14
    .parameter "fps"
    .parameter "params"

    #@0
    .prologue
    const-wide v9, 0x408f400000000000L

    #@5
    const/4 v8, 0x1

    #@6
    const/4 v7, 0x0

    #@7
    .line 350
    invoke-virtual {p2}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewFpsRange()Ljava/util/List;

    #@a
    move-result-object v3

    #@b
    .line 351
    .local v3, supportedFpsRanges:Ljava/util/List;,"Ljava/util/List<[I>;"
    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, [I

    #@11
    .line 352
    .local v0, closestRange:[I
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@14
    move-result-object v1

    #@15
    .local v1, i$:Ljava/util/Iterator;
    :cond_15
    :goto_15
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@18
    move-result v4

    #@19
    if-eqz v4, :cond_3b

    #@1b
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1e
    move-result-object v2

    #@1f
    check-cast v2, [I

    #@21
    .line 353
    .local v2, range:[I
    aget v4, v2, v7

    #@23
    mul-int/lit16 v5, p1, 0x3e8

    #@25
    if-ge v4, v5, :cond_15

    #@27
    aget v4, v2, v8

    #@29
    mul-int/lit16 v5, p1, 0x3e8

    #@2b
    if-le v4, v5, :cond_15

    #@2d
    aget v4, v2, v7

    #@2f
    aget v5, v0, v7

    #@31
    if-le v4, v5, :cond_15

    #@33
    aget v4, v2, v8

    #@35
    aget v5, v0, v8

    #@37
    if-ge v4, v5, :cond_15

    #@39
    .line 359
    move-object v0, v2

    #@3a
    goto :goto_15

    #@3b
    .line 362
    .end local v2           #range:[I
    :cond_3b
    iget-boolean v4, p0, Landroid/filterpacks/videosrc/CameraSource;->mLogVerbose:Z

    #@3d
    if-eqz v4, :cond_79

    #@3f
    const-string v4, "CameraSource"

    #@41
    new-instance v5, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v6, "Requested fps: "

    #@48
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v5

    #@4c
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v5

    #@50
    const-string v6, ".Closest frame rate range: ["

    #@52
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v5

    #@56
    aget v6, v0, v7

    #@58
    int-to-double v6, v6

    #@59
    div-double/2addr v6, v9

    #@5a
    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v5

    #@5e
    const-string v6, ","

    #@60
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v5

    #@64
    aget v6, v0, v8

    #@66
    int-to-double v6, v6

    #@67
    div-double/2addr v6, v9

    #@68
    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v5

    #@6c
    const-string v6, "]"

    #@6e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v5

    #@72
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v5

    #@76
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@79
    .line 369
    :cond_79
    return-object v0
.end method

.method private findClosestSize(IILandroid/hardware/Camera$Parameters;)[I
    .registers 16
    .parameter "width"
    .parameter "height"
    .parameter "parameters"

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    .line 310
    invoke-virtual {p3}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    #@4
    move-result-object v4

    #@5
    .line 311
    .local v4, previewSizes:Ljava/util/List;,"Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    const/4 v2, -0x1

    #@6
    .line 312
    .local v2, closestWidth:I
    const/4 v0, -0x1

    #@7
    .line 313
    .local v0, closestHeight:I
    invoke-interface {v4, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@a
    move-result-object v8

    #@b
    check-cast v8, Landroid/hardware/Camera$Size;

    #@d
    iget v7, v8, Landroid/hardware/Camera$Size;->width:I

    #@f
    .line 314
    .local v7, smallestWidth:I
    invoke-interface {v4, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v8

    #@13
    check-cast v8, Landroid/hardware/Camera$Size;

    #@15
    iget v6, v8, Landroid/hardware/Camera$Size;->height:I

    #@17
    .line 315
    .local v6, smallestHeight:I
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@1a
    move-result-object v3

    #@1b
    .local v3, i$:Ljava/util/Iterator;
    :cond_1b
    :goto_1b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@1e
    move-result v8

    #@1f
    if-eqz v8, :cond_48

    #@21
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@24
    move-result-object v5

    #@25
    check-cast v5, Landroid/hardware/Camera$Size;

    #@27
    .line 320
    .local v5, size:Landroid/hardware/Camera$Size;
    iget v8, v5, Landroid/hardware/Camera$Size;->width:I

    #@29
    if-gt v8, p1, :cond_3b

    #@2b
    iget v8, v5, Landroid/hardware/Camera$Size;->height:I

    #@2d
    if-gt v8, p2, :cond_3b

    #@2f
    iget v8, v5, Landroid/hardware/Camera$Size;->width:I

    #@31
    if-lt v8, v2, :cond_3b

    #@33
    iget v8, v5, Landroid/hardware/Camera$Size;->height:I

    #@35
    if-lt v8, v0, :cond_3b

    #@37
    .line 324
    iget v2, v5, Landroid/hardware/Camera$Size;->width:I

    #@39
    .line 325
    iget v0, v5, Landroid/hardware/Camera$Size;->height:I

    #@3b
    .line 327
    :cond_3b
    iget v8, v5, Landroid/hardware/Camera$Size;->width:I

    #@3d
    if-ge v8, v7, :cond_1b

    #@3f
    iget v8, v5, Landroid/hardware/Camera$Size;->height:I

    #@41
    if-ge v8, v6, :cond_1b

    #@43
    .line 329
    iget v7, v5, Landroid/hardware/Camera$Size;->width:I

    #@45
    .line 330
    iget v6, v5, Landroid/hardware/Camera$Size;->height:I

    #@47
    goto :goto_1b

    #@48
    .line 333
    .end local v5           #size:Landroid/hardware/Camera$Size;
    :cond_48
    const/4 v8, -0x1

    #@49
    if-ne v2, v8, :cond_4d

    #@4b
    .line 335
    move v2, v7

    #@4c
    .line 336
    move v0, v6

    #@4d
    .line 339
    :cond_4d
    iget-boolean v8, p0, Landroid/filterpacks/videosrc/CameraSource;->mLogVerbose:Z

    #@4f
    if-eqz v8, :cond_8d

    #@51
    .line 340
    const-string v8, "CameraSource"

    #@53
    new-instance v9, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v10, "Requested resolution: ("

    #@5a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v9

    #@5e
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@61
    move-result-object v9

    #@62
    const-string v10, ", "

    #@64
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v9

    #@68
    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v9

    #@6c
    const-string v10, "). Closest match: ("

    #@6e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v9

    #@72
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@75
    move-result-object v9

    #@76
    const-string v10, ", "

    #@78
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v9

    #@7c
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v9

    #@80
    const-string v10, ")."

    #@82
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v9

    #@86
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v9

    #@8a
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@8d
    .line 345
    :cond_8d
    const/4 v8, 0x2

    #@8e
    new-array v1, v8, [I

    #@90
    aput v2, v1, v11

    #@92
    const/4 v8, 0x1

    #@93
    aput v0, v1, v8

    #@95
    .line 346
    .local v1, closestSize:[I
    return-object v1
.end method

.method public static manualStopPreview()V
    .registers 1

    #@0
    .prologue
    .line 387
    sget-object v0, Landroid/filterpacks/videosrc/CameraSource;->mCamera:Landroid/hardware/Camera;

    #@2
    if-eqz v0, :cond_11

    #@4
    .line 388
    sget-object v0, Landroid/filterpacks/videosrc/CameraSource;->mCamera:Landroid/hardware/Camera;

    #@6
    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    #@9
    .line 389
    sget-object v0, Landroid/filterpacks/videosrc/CameraSource;->mCamera:Landroid/hardware/Camera;

    #@b
    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    #@e
    .line 390
    const/4 v0, 0x0

    #@f
    sput-object v0, Landroid/filterpacks/videosrc/CameraSource;->mCamera:Landroid/hardware/Camera;

    #@11
    .line 392
    :cond_11
    return-void
.end method


# virtual methods
.method public close(Landroid/filterfw/core/FilterContext;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 235
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mLogVerbose:Z

    #@3
    if-eqz v0, :cond_c

    #@5
    const-string v0, "CameraSource"

    #@7
    const-string v1, "Closing"

    #@9
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 237
    :cond_c
    sget-object v0, Landroid/filterpacks/videosrc/CameraSource;->mCamera:Landroid/hardware/Camera;

    #@e
    if-eqz v0, :cond_17

    #@10
    .line 238
    sget-object v0, Landroid/filterpacks/videosrc/CameraSource;->mCamera:Landroid/hardware/Camera;

    #@12
    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    #@15
    .line 239
    sput-object v2, Landroid/filterpacks/videosrc/CameraSource;->mCamera:Landroid/hardware/Camera;

    #@17
    .line 241
    :cond_17
    iget-object v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@19
    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    #@1c
    .line 242
    iput-object v2, p0, Landroid/filterpacks/videosrc/CameraSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@1e
    .line 243
    return-void
.end method

.method public fieldPortValueUpdated(Ljava/lang/String;Landroid/filterfw/core/FilterContext;)V
    .registers 7
    .parameter "name"
    .parameter "context"

    #@0
    .prologue
    .line 254
    const-string v1, "framerate"

    #@2
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_25

    #@8
    .line 255
    invoke-virtual {p0}, Landroid/filterpacks/videosrc/CameraSource;->getCameraParameters()Landroid/hardware/Camera$Parameters;

    #@b
    .line 256
    iget v1, p0, Landroid/filterpacks/videosrc/CameraSource;->mFps:I

    #@d
    iget-object v2, p0, Landroid/filterpacks/videosrc/CameraSource;->mCameraParameters:Landroid/hardware/Camera$Parameters;

    #@f
    invoke-direct {p0, v1, v2}, Landroid/filterpacks/videosrc/CameraSource;->findClosestFpsRange(ILandroid/hardware/Camera$Parameters;)[I

    #@12
    move-result-object v0

    #@13
    .line 257
    .local v0, closestRange:[I
    iget-object v1, p0, Landroid/filterpacks/videosrc/CameraSource;->mCameraParameters:Landroid/hardware/Camera$Parameters;

    #@15
    const/4 v2, 0x0

    #@16
    aget v2, v0, v2

    #@18
    const/4 v3, 0x1

    #@19
    aget v3, v0, v3

    #@1b
    invoke-virtual {v1, v2, v3}, Landroid/hardware/Camera$Parameters;->setPreviewFpsRange(II)V

    #@1e
    .line 259
    sget-object v1, Landroid/filterpacks/videosrc/CameraSource;->mCamera:Landroid/hardware/Camera;

    #@20
    iget-object v2, p0, Landroid/filterpacks/videosrc/CameraSource;->mCameraParameters:Landroid/hardware/Camera$Parameters;

    #@22
    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    #@25
    .line 261
    .end local v0           #closestRange:[I
    :cond_25
    return-void
.end method

.method public declared-synchronized getCameraParameters()Landroid/hardware/Camera$Parameters;
    .registers 7

    #@0
    .prologue
    .line 264
    monitor-enter p0

    #@1
    const/4 v0, 0x0

    #@2
    .line 265
    .local v0, closeCamera:Z
    :try_start_2
    iget-object v3, p0, Landroid/filterpacks/videosrc/CameraSource;->mCameraParameters:Landroid/hardware/Camera$Parameters;

    #@4
    if-nez v3, :cond_25

    #@6
    .line 266
    sget-object v3, Landroid/filterpacks/videosrc/CameraSource;->mCamera:Landroid/hardware/Camera;

    #@8
    if-nez v3, :cond_13

    #@a
    .line 267
    iget v3, p0, Landroid/filterpacks/videosrc/CameraSource;->mCameraId:I

    #@c
    invoke-static {v3}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    #@f
    move-result-object v3

    #@10
    sput-object v3, Landroid/filterpacks/videosrc/CameraSource;->mCamera:Landroid/hardware/Camera;

    #@12
    .line 268
    const/4 v0, 0x1

    #@13
    .line 270
    :cond_13
    sget-object v3, Landroid/filterpacks/videosrc/CameraSource;->mCamera:Landroid/hardware/Camera;

    #@15
    invoke-virtual {v3}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    #@18
    move-result-object v3

    #@19
    iput-object v3, p0, Landroid/filterpacks/videosrc/CameraSource;->mCameraParameters:Landroid/hardware/Camera$Parameters;

    #@1b
    .line 272
    if-eqz v0, :cond_25

    #@1d
    .line 273
    sget-object v3, Landroid/filterpacks/videosrc/CameraSource;->mCamera:Landroid/hardware/Camera;

    #@1f
    invoke-virtual {v3}, Landroid/hardware/Camera;->release()V

    #@22
    .line 274
    const/4 v3, 0x0

    #@23
    sput-object v3, Landroid/filterpacks/videosrc/CameraSource;->mCamera:Landroid/hardware/Camera;

    #@25
    .line 278
    :cond_25
    iget v3, p0, Landroid/filterpacks/videosrc/CameraSource;->mWidth:I

    #@27
    iget v4, p0, Landroid/filterpacks/videosrc/CameraSource;->mHeight:I

    #@29
    iget-object v5, p0, Landroid/filterpacks/videosrc/CameraSource;->mCameraParameters:Landroid/hardware/Camera$Parameters;

    #@2b
    invoke-direct {p0, v3, v4, v5}, Landroid/filterpacks/videosrc/CameraSource;->findClosestSize(IILandroid/hardware/Camera$Parameters;)[I

    #@2e
    move-result-object v2

    #@2f
    .line 279
    .local v2, closestSize:[I
    const/4 v3, 0x0

    #@30
    aget v3, v2, v3

    #@32
    iput v3, p0, Landroid/filterpacks/videosrc/CameraSource;->mWidth:I

    #@34
    .line 280
    const/4 v3, 0x1

    #@35
    aget v3, v2, v3

    #@37
    iput v3, p0, Landroid/filterpacks/videosrc/CameraSource;->mHeight:I

    #@39
    .line 281
    iget-object v3, p0, Landroid/filterpacks/videosrc/CameraSource;->mCameraParameters:Landroid/hardware/Camera$Parameters;

    #@3b
    iget v4, p0, Landroid/filterpacks/videosrc/CameraSource;->mWidth:I

    #@3d
    iget v5, p0, Landroid/filterpacks/videosrc/CameraSource;->mHeight:I

    #@3f
    invoke-virtual {v3, v4, v5}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    #@42
    .line 283
    iget v3, p0, Landroid/filterpacks/videosrc/CameraSource;->mFps:I

    #@44
    iget-object v4, p0, Landroid/filterpacks/videosrc/CameraSource;->mCameraParameters:Landroid/hardware/Camera$Parameters;

    #@46
    invoke-direct {p0, v3, v4}, Landroid/filterpacks/videosrc/CameraSource;->findClosestFpsRange(ILandroid/hardware/Camera$Parameters;)[I

    #@49
    move-result-object v1

    #@4a
    .line 285
    .local v1, closestRange:[I
    iget-object v3, p0, Landroid/filterpacks/videosrc/CameraSource;->mCameraParameters:Landroid/hardware/Camera$Parameters;

    #@4c
    const/4 v4, 0x0

    #@4d
    aget v4, v1, v4

    #@4f
    const/4 v5, 0x1

    #@50
    aget v5, v1, v5

    #@52
    invoke-virtual {v3, v4, v5}, Landroid/hardware/Camera$Parameters;->setPreviewFpsRange(II)V

    #@55
    .line 289
    iget v3, p0, Landroid/filterpacks/videosrc/CameraSource;->mCameraId:I

    #@57
    if-nez v3, :cond_60

    #@59
    .line 290
    iget-object v3, p0, Landroid/filterpacks/videosrc/CameraSource;->mCameraParameters:Landroid/hardware/Camera$Parameters;

    #@5b
    iget-object v4, p0, Landroid/filterpacks/videosrc/CameraSource;->mFocusmode:Ljava/lang/String;

    #@5d
    invoke-virtual {v3, v4}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    #@60
    .line 294
    :cond_60
    sget-object v3, Landroid/filterpacks/videosrc/CameraSource;->mCamera:Landroid/hardware/Camera;

    #@62
    iget v4, p0, Landroid/filterpacks/videosrc/CameraSource;->mOrientation:I

    #@64
    invoke-virtual {v3, v4}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    #@67
    .line 297
    iget-object v3, p0, Landroid/filterpacks/videosrc/CameraSource;->mCameraParameters:Landroid/hardware/Camera$Parameters;
    :try_end_69
    .catchall {:try_start_2 .. :try_end_69} :catchall_6b

    #@69
    monitor-exit p0

    #@6a
    return-object v3

    #@6b
    .line 264
    .end local v1           #closestRange:[I
    .end local v2           #closestSize:[I
    :catchall_6b
    move-exception v3

    #@6c
    monitor-exit p0

    #@6d
    throw v3
.end method

.method public open(Landroid/filterfw/core/FilterContext;)V
    .registers 8
    .parameter "context"

    #@0
    .prologue
    .line 152
    iget-boolean v1, p0, Landroid/filterpacks/videosrc/CameraSource;->mLogVerbose:Z

    #@2
    if-eqz v1, :cond_b

    #@4
    const-string v1, "CameraSource"

    #@6
    const-string v2, "Opening"

    #@8
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 154
    :cond_b
    iget v1, p0, Landroid/filterpacks/videosrc/CameraSource;->mCameraId:I

    #@d
    invoke-static {v1}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    #@10
    move-result-object v1

    #@11
    sput-object v1, Landroid/filterpacks/videosrc/CameraSource;->mCamera:Landroid/hardware/Camera;

    #@13
    .line 157
    invoke-virtual {p0}, Landroid/filterpacks/videosrc/CameraSource;->getCameraParameters()Landroid/hardware/Camera$Parameters;

    #@16
    .line 158
    sget-object v1, Landroid/filterpacks/videosrc/CameraSource;->mCamera:Landroid/hardware/Camera;

    #@18
    iget-object v2, p0, Landroid/filterpacks/videosrc/CameraSource;->mCameraParameters:Landroid/hardware/Camera$Parameters;

    #@1a
    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    #@1d
    .line 161
    invoke-direct {p0}, Landroid/filterpacks/videosrc/CameraSource;->createFormats()V

    #@20
    .line 164
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@23
    move-result-object v1

    #@24
    iget-object v2, p0, Landroid/filterpacks/videosrc/CameraSource;->mOutputFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@26
    const/16 v3, 0x68

    #@28
    const-wide/16 v4, 0x0

    #@2a
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/filterfw/core/FrameManager;->newBoundFrame(Landroid/filterfw/core/FrameFormat;IJ)Landroid/filterfw/core/Frame;

    #@2d
    move-result-object v1

    #@2e
    check-cast v1, Landroid/filterfw/core/GLFrame;

    #@30
    iput-object v1, p0, Landroid/filterpacks/videosrc/CameraSource;->mCameraFrame:Landroid/filterfw/core/GLFrame;

    #@32
    .line 167
    new-instance v1, Landroid/graphics/SurfaceTexture;

    #@34
    iget-object v2, p0, Landroid/filterpacks/videosrc/CameraSource;->mCameraFrame:Landroid/filterfw/core/GLFrame;

    #@36
    invoke-virtual {v2}, Landroid/filterfw/core/GLFrame;->getTextureId()I

    #@39
    move-result v2

    #@3a
    invoke-direct {v1, v2}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    #@3d
    iput-object v1, p0, Landroid/filterpacks/videosrc/CameraSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@3f
    .line 169
    :try_start_3f
    sget-object v1, Landroid/filterpacks/videosrc/CameraSource;->mCamera:Landroid/hardware/Camera;

    #@41
    iget-object v2, p0, Landroid/filterpacks/videosrc/CameraSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@43
    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V
    :try_end_46
    .catch Ljava/io/IOException; {:try_start_3f .. :try_end_46} :catch_5d

    #@46
    .line 176
    iget-object v1, p0, Landroid/filterpacks/videosrc/CameraSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@48
    iget-object v2, p0, Landroid/filterpacks/videosrc/CameraSource;->onCameraFrameAvailableListener:Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;

    #@4a
    invoke-virtual {v1, v2}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    #@4d
    .line 178
    const/4 v1, 0x0

    #@4e
    iput-boolean v1, p0, Landroid/filterpacks/videosrc/CameraSource;->mNewFrameAvailable:Z

    #@50
    .line 179
    sget-object v1, Landroid/filterpacks/videosrc/CameraSource;->mCamera:Landroid/hardware/Camera;

    #@52
    invoke-virtual {v1}, Landroid/hardware/Camera;->startPreview()V

    #@55
    .line 181
    sget-object v1, Landroid/filterpacks/videosrc/CameraSource;->mCamera:Landroid/hardware/Camera;

    #@57
    iget-object v2, p0, Landroid/filterpacks/videosrc/CameraSource;->CameraAutoFocusOnCafCallback:Landroid/hardware/Camera$AutoFocusCallback;

    #@59
    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    #@5c
    .line 184
    return-void

    #@5d
    .line 170
    :catch_5d
    move-exception v0

    #@5e
    .line 171
    .local v0, e:Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@60
    new-instance v2, Ljava/lang/StringBuilder;

    #@62
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@65
    const-string v3, "Could not bind camera surface texture: "

    #@67
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v2

    #@6b
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    #@6e
    move-result-object v3

    #@6f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v2

    #@73
    const-string v3, "!"

    #@75
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v2

    #@79
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v2

    #@7d
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@80
    throw v1
.end method

.method public prepare(Landroid/filterfw/core/FilterContext;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 145
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mLogVerbose:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    const-string v0, "CameraSource"

    #@6
    const-string v1, "Preparing"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 147
    :cond_b
    new-instance v0, Landroid/filterfw/core/ShaderProgram;

    #@d
    const-string v1, "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nuniform samplerExternalOES tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n"

    #@f
    invoke-direct {v0, p1, v1}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;)V

    #@12
    iput-object v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mFrameExtractor:Landroid/filterfw/core/ShaderProgram;

    #@14
    .line 148
    return-void
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 17
    .parameter "context"

    #@0
    .prologue
    .line 188
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mLogVerbose:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    const-string v0, "CameraSource"

    #@6
    const-string v1, "Processing new frame"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 190
    :cond_b
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mWaitForNewFrame:Z

    #@d
    if-eqz v0, :cond_41

    #@f
    .line 191
    const/4 v13, 0x0

    #@10
    .line 192
    .local v13, waitCount:I
    :cond_10
    :goto_10
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mNewFrameAvailable:Z

    #@12
    if-nez v0, :cond_33

    #@14
    .line 193
    const/16 v0, 0xa

    #@16
    if-ne v13, v0, :cond_20

    #@18
    .line 194
    new-instance v0, Ljava/lang/RuntimeException;

    #@1a
    const-string v1, "Timeout waiting for new frame"

    #@1c
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0

    #@20
    .line 197
    :cond_20
    const-wide/16 v0, 0x64

    #@22
    :try_start_22
    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_25
    .catch Ljava/lang/InterruptedException; {:try_start_22 .. :try_end_25} :catch_26

    #@25
    goto :goto_10

    #@26
    .line 198
    :catch_26
    move-exception v9

    #@27
    .line 199
    .local v9, e:Ljava/lang/InterruptedException;
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mLogVerbose:Z

    #@29
    if-eqz v0, :cond_10

    #@2b
    const-string v0, "CameraSource"

    #@2d
    const-string v1, "Interrupted while waiting for new frame"

    #@2f
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    goto :goto_10

    #@33
    .line 202
    .end local v9           #e:Ljava/lang/InterruptedException;
    :cond_33
    const/4 v0, 0x0

    #@34
    iput-boolean v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mNewFrameAvailable:Z

    #@36
    .line 203
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mLogVerbose:Z

    #@38
    if-eqz v0, :cond_41

    #@3a
    const-string v0, "CameraSource"

    #@3c
    const-string v1, "Got new frame"

    #@3e
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 206
    .end local v13           #waitCount:I
    :cond_41
    iget-object v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@43
    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    #@46
    .line 208
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mLogVerbose:Z

    #@48
    if-eqz v0, :cond_66

    #@4a
    const-string v0, "CameraSource"

    #@4c
    new-instance v1, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v2, "Using frame extractor in thread: "

    #@53
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v1

    #@57
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@5a
    move-result-object v2

    #@5b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v1

    #@5f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v1

    #@63
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    .line 209
    :cond_66
    iget-object v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@68
    iget-object v1, p0, Landroid/filterpacks/videosrc/CameraSource;->mCameraTransform:[F

    #@6a
    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->getTransformMatrix([F)V

    #@6d
    .line 210
    iget-object v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mMappedCoords:[F

    #@6f
    const/4 v1, 0x0

    #@70
    iget-object v2, p0, Landroid/filterpacks/videosrc/CameraSource;->mCameraTransform:[F

    #@72
    const/4 v3, 0x0

    #@73
    sget-object v4, Landroid/filterpacks/videosrc/CameraSource;->mSourceCoords:[F

    #@75
    const/4 v5, 0x0

    #@76
    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    #@79
    .line 213
    iget-object v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mFrameExtractor:Landroid/filterfw/core/ShaderProgram;

    #@7b
    iget-object v1, p0, Landroid/filterpacks/videosrc/CameraSource;->mMappedCoords:[F

    #@7d
    const/4 v2, 0x0

    #@7e
    aget v1, v1, v2

    #@80
    iget-object v2, p0, Landroid/filterpacks/videosrc/CameraSource;->mMappedCoords:[F

    #@82
    const/4 v3, 0x1

    #@83
    aget v2, v2, v3

    #@85
    iget-object v3, p0, Landroid/filterpacks/videosrc/CameraSource;->mMappedCoords:[F

    #@87
    const/4 v4, 0x4

    #@88
    aget v3, v3, v4

    #@8a
    iget-object v4, p0, Landroid/filterpacks/videosrc/CameraSource;->mMappedCoords:[F

    #@8c
    const/4 v5, 0x5

    #@8d
    aget v4, v4, v5

    #@8f
    iget-object v5, p0, Landroid/filterpacks/videosrc/CameraSource;->mMappedCoords:[F

    #@91
    const/16 v6, 0x8

    #@93
    aget v5, v5, v6

    #@95
    iget-object v6, p0, Landroid/filterpacks/videosrc/CameraSource;->mMappedCoords:[F

    #@97
    const/16 v7, 0x9

    #@99
    aget v6, v6, v7

    #@9b
    iget-object v7, p0, Landroid/filterpacks/videosrc/CameraSource;->mMappedCoords:[F

    #@9d
    const/16 v8, 0xc

    #@9f
    aget v7, v7, v8

    #@a1
    iget-object v8, p0, Landroid/filterpacks/videosrc/CameraSource;->mMappedCoords:[F

    #@a3
    const/16 v14, 0xd

    #@a5
    aget v8, v8, v14

    #@a7
    invoke-virtual/range {v0 .. v8}, Landroid/filterfw/core/ShaderProgram;->setSourceRegion(FFFFFFFF)Z

    #@aa
    .line 218
    invoke-virtual/range {p1 .. p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@ad
    move-result-object v0

    #@ae
    iget-object v1, p0, Landroid/filterpacks/videosrc/CameraSource;->mOutputFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@b0
    invoke-virtual {v0, v1}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@b3
    move-result-object v10

    #@b4
    .line 219
    .local v10, output:Landroid/filterfw/core/Frame;
    iget-object v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mFrameExtractor:Landroid/filterfw/core/ShaderProgram;

    #@b6
    iget-object v1, p0, Landroid/filterpacks/videosrc/CameraSource;->mCameraFrame:Landroid/filterfw/core/GLFrame;

    #@b8
    invoke-virtual {v0, v1, v10}, Landroid/filterfw/core/ShaderProgram;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@bb
    .line 221
    iget-object v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    #@bd
    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->getTimestamp()J

    #@c0
    move-result-wide v11

    #@c1
    .line 222
    .local v11, timestamp:J
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mLogVerbose:Z

    #@c3
    if-eqz v0, :cond_ea

    #@c5
    const-string v0, "CameraSource"

    #@c7
    new-instance v1, Ljava/lang/StringBuilder;

    #@c9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@cc
    const-string v2, "Timestamp: "

    #@ce
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v1

    #@d2
    long-to-double v2, v11

    #@d3
    const-wide v4, 0x41cdcd6500000000L

    #@d8
    div-double/2addr v2, v4

    #@d9
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v1

    #@dd
    const-string v2, " s"

    #@df
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v1

    #@e3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e6
    move-result-object v1

    #@e7
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@ea
    .line 223
    :cond_ea
    invoke-virtual {v10, v11, v12}, Landroid/filterfw/core/Frame;->setTimestamp(J)V

    #@ed
    .line 225
    const-string/jumbo v0, "video"

    #@f0
    invoke-virtual {p0, v0, v10}, Landroid/filterpacks/videosrc/CameraSource;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@f3
    .line 228
    invoke-virtual {v10}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@f6
    .line 230
    iget-boolean v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mLogVerbose:Z

    #@f8
    if-eqz v0, :cond_101

    #@fa
    const-string v0, "CameraSource"

    #@fc
    const-string v1, "Done processing new frame"

    #@fe
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@101
    .line 231
    :cond_101
    return-void
.end method

.method public declared-synchronized setCameraParameters(Landroid/hardware/Camera$Parameters;)V
    .registers 4
    .parameter "params"

    #@0
    .prologue
    .line 302
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mWidth:I

    #@3
    iget v1, p0, Landroid/filterpacks/videosrc/CameraSource;->mHeight:I

    #@5
    invoke-virtual {p1, v0, v1}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    #@8
    .line 303
    iput-object p1, p0, Landroid/filterpacks/videosrc/CameraSource;->mCameraParameters:Landroid/hardware/Camera$Parameters;

    #@a
    .line 304
    invoke-virtual {p0}, Landroid/filterpacks/videosrc/CameraSource;->isOpen()Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_17

    #@10
    .line 305
    sget-object v0, Landroid/filterpacks/videosrc/CameraSource;->mCamera:Landroid/hardware/Camera;

    #@12
    iget-object v1, p0, Landroid/filterpacks/videosrc/CameraSource;->mCameraParameters:Landroid/hardware/Camera$Parameters;

    #@14
    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_17
    .catchall {:try_start_1 .. :try_end_17} :catchall_19

    #@17
    .line 307
    :cond_17
    monitor-exit p0

    #@18
    return-void

    #@19
    .line 302
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit p0

    #@1b
    throw v0
.end method

.method public setupPorts()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x3

    #@1
    .line 133
    const-string/jumbo v0, "video"

    #@4
    invoke-static {v1, v1}, Landroid/filterfw/format/ImageFormat;->create(II)Landroid/filterfw/core/MutableFrameFormat;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/videosrc/CameraSource;->addOutputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@b
    .line 135
    return-void
.end method

.method public tearDown(Landroid/filterfw/core/FilterContext;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 247
    iget-object v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mCameraFrame:Landroid/filterfw/core/GLFrame;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 248
    iget-object v0, p0, Landroid/filterpacks/videosrc/CameraSource;->mCameraFrame:Landroid/filterfw/core/GLFrame;

    #@6
    invoke-virtual {v0}, Landroid/filterfw/core/GLFrame;->release()Landroid/filterfw/core/Frame;

    #@9
    .line 250
    :cond_9
    return-void
.end method
