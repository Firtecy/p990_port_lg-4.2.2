.class public Landroid/filterpacks/performance/ThroughputFilter;
.super Landroid/filterfw/core/Filter;
.source "ThroughputFilter.java"


# instance fields
.field private mLastTime:J

.field private mOutputFormat:Landroid/filterfw/core/FrameFormat;

.field private mPeriod:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "period"
    .end annotation
.end field

.field private mPeriodFrameCount:I

.field private mTotalFrameCount:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 5
    .parameter "name"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 44
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@4
    .line 33
    const/4 v0, 0x5

    #@5
    iput v0, p0, Landroid/filterpacks/performance/ThroughputFilter;->mPeriod:I

    #@7
    .line 36
    const-wide/16 v0, 0x0

    #@9
    iput-wide v0, p0, Landroid/filterpacks/performance/ThroughputFilter;->mLastTime:J

    #@b
    .line 38
    iput v2, p0, Landroid/filterpacks/performance/ThroughputFilter;->mTotalFrameCount:I

    #@d
    .line 39
    iput v2, p0, Landroid/filterpacks/performance/ThroughputFilter;->mPeriodFrameCount:I

    #@f
    .line 45
    return-void
.end method


# virtual methods
.method public getOutputFormat(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .registers 3
    .parameter "portName"
    .parameter "inputFormat"

    #@0
    .prologue
    .line 60
    return-object p2
.end method

.method public open(Landroid/filterfw/core/FilterContext;)V
    .registers 4
    .parameter "env"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 65
    iput v0, p0, Landroid/filterpacks/performance/ThroughputFilter;->mTotalFrameCount:I

    #@3
    .line 66
    iput v0, p0, Landroid/filterpacks/performance/ThroughputFilter;->mPeriodFrameCount:I

    #@5
    .line 67
    const-wide/16 v0, 0x0

    #@7
    iput-wide v0, p0, Landroid/filterpacks/performance/ThroughputFilter;->mLastTime:J

    #@9
    .line 68
    return-void
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 13
    .parameter "context"

    #@0
    .prologue
    .line 73
    const-string v7, "frame"

    #@2
    invoke-virtual {p0, v7}, Landroid/filterpacks/performance/ThroughputFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@5
    move-result-object v2

    #@6
    .line 74
    .local v2, input:Landroid/filterfw/core/Frame;
    const-string v7, "frame"

    #@8
    invoke-virtual {p0, v7, v2}, Landroid/filterpacks/performance/ThroughputFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@b
    .line 77
    iget v7, p0, Landroid/filterpacks/performance/ThroughputFilter;->mTotalFrameCount:I

    #@d
    add-int/lit8 v7, v7, 0x1

    #@f
    iput v7, p0, Landroid/filterpacks/performance/ThroughputFilter;->mTotalFrameCount:I

    #@11
    .line 78
    iget v7, p0, Landroid/filterpacks/performance/ThroughputFilter;->mPeriodFrameCount:I

    #@13
    add-int/lit8 v7, v7, 0x1

    #@15
    iput v7, p0, Landroid/filterpacks/performance/ThroughputFilter;->mPeriodFrameCount:I

    #@17
    .line 81
    iget-wide v7, p0, Landroid/filterpacks/performance/ThroughputFilter;->mLastTime:J

    #@19
    const-wide/16 v9, 0x0

    #@1b
    cmp-long v7, v7, v9

    #@1d
    if-nez v7, :cond_25

    #@1f
    .line 82
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@22
    move-result-wide v7

    #@23
    iput-wide v7, p0, Landroid/filterpacks/performance/ThroughputFilter;->mLastTime:J

    #@25
    .line 84
    :cond_25
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@28
    move-result-wide v0

    #@29
    .line 87
    .local v0, curTime:J
    iget-wide v7, p0, Landroid/filterpacks/performance/ThroughputFilter;->mLastTime:J

    #@2b
    sub-long v7, v0, v7

    #@2d
    iget v9, p0, Landroid/filterpacks/performance/ThroughputFilter;->mPeriod:I

    #@2f
    mul-int/lit16 v9, v9, 0x3e8

    #@31
    int-to-long v9, v9

    #@32
    cmp-long v7, v7, v9

    #@34
    if-ltz v7, :cond_67

    #@36
    .line 88
    invoke-virtual {v2}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@39
    move-result-object v3

    #@3a
    .line 89
    .local v3, inputFormat:Landroid/filterfw/core/FrameFormat;
    invoke-virtual {v3}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@3d
    move-result v7

    #@3e
    invoke-virtual {v3}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@41
    move-result v8

    #@42
    mul-int v4, v7, v8

    #@44
    .line 90
    .local v4, pixelCount:I
    new-instance v5, Landroid/filterpacks/performance/Throughput;

    #@46
    iget v7, p0, Landroid/filterpacks/performance/ThroughputFilter;->mTotalFrameCount:I

    #@48
    iget v8, p0, Landroid/filterpacks/performance/ThroughputFilter;->mPeriodFrameCount:I

    #@4a
    iget v9, p0, Landroid/filterpacks/performance/ThroughputFilter;->mPeriod:I

    #@4c
    invoke-direct {v5, v7, v8, v9, v4}, Landroid/filterpacks/performance/Throughput;-><init>(IIII)V

    #@4f
    .line 94
    .local v5, throughput:Landroid/filterpacks/performance/Throughput;
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@52
    move-result-object v7

    #@53
    iget-object v8, p0, Landroid/filterpacks/performance/ThroughputFilter;->mOutputFormat:Landroid/filterfw/core/FrameFormat;

    #@55
    invoke-virtual {v7, v8}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@58
    move-result-object v6

    #@59
    .line 95
    .local v6, throughputFrame:Landroid/filterfw/core/Frame;
    invoke-virtual {v6, v5}, Landroid/filterfw/core/Frame;->setObjectValue(Ljava/lang/Object;)V

    #@5c
    .line 96
    const-string/jumbo v7, "throughput"

    #@5f
    invoke-virtual {p0, v7, v6}, Landroid/filterpacks/performance/ThroughputFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@62
    .line 97
    iput-wide v0, p0, Landroid/filterpacks/performance/ThroughputFilter;->mLastTime:J

    #@64
    .line 98
    const/4 v7, 0x0

    #@65
    iput v7, p0, Landroid/filterpacks/performance/ThroughputFilter;->mPeriodFrameCount:I

    #@67
    .line 100
    .end local v3           #inputFormat:Landroid/filterfw/core/FrameFormat;
    .end local v4           #pixelCount:I
    .end local v5           #throughput:Landroid/filterpacks/performance/Throughput;
    .end local v6           #throughputFrame:Landroid/filterfw/core/Frame;
    :cond_67
    return-void
.end method

.method public setupPorts()V
    .registers 3

    #@0
    .prologue
    .line 50
    const-string v0, "frame"

    #@2
    invoke-virtual {p0, v0}, Landroid/filterpacks/performance/ThroughputFilter;->addInputPort(Ljava/lang/String;)V

    #@5
    .line 53
    const-class v0, Landroid/filterpacks/performance/Throughput;

    #@7
    const/4 v1, 0x1

    #@8
    invoke-static {v0, v1}, Landroid/filterfw/format/ObjectFormat;->fromClass(Ljava/lang/Class;I)Landroid/filterfw/core/MutableFrameFormat;

    #@b
    move-result-object v0

    #@c
    iput-object v0, p0, Landroid/filterpacks/performance/ThroughputFilter;->mOutputFormat:Landroid/filterfw/core/FrameFormat;

    #@e
    .line 54
    const-string v0, "frame"

    #@10
    const-string v1, "frame"

    #@12
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/performance/ThroughputFilter;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    #@15
    .line 55
    const-string/jumbo v0, "throughput"

    #@18
    iget-object v1, p0, Landroid/filterpacks/performance/ThroughputFilter;->mOutputFormat:Landroid/filterfw/core/FrameFormat;

    #@1a
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/performance/ThroughputFilter;->addOutputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@1d
    .line 56
    return-void
.end method
