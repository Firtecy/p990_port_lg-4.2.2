.class public Landroid/filterpacks/videosink/MediaEncoderFilter;
.super Landroid/filterfw/core/Filter;
.source "MediaEncoderFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/filterpacks/videosink/MediaEncoderFilter$OnRecordingDoneListener;
    }
.end annotation


# static fields
.field private static final NO_AUDIO_SOURCE:I = -0x1

.field private static final TAG:Ljava/lang/String; = "MediaEncoderFilter"


# instance fields
.field private mAudioSource:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "audioSource"
    .end annotation
.end field

.field private mCaptureTimeLapse:Z

.field private mErrorListener:Landroid/media/MediaRecorder$OnErrorListener;
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "errorListener"
    .end annotation
.end field

.field private mFd:Ljava/io/FileDescriptor;
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "outputFileDescriptor"
    .end annotation
.end field

.field private mFps:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "framerate"
    .end annotation
.end field

.field private mHeight:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "height"
    .end annotation
.end field

.field private mInfoListener:Landroid/media/MediaRecorder$OnInfoListener;
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "infoListener"
    .end annotation
.end field

.field private mIsRecStart:Z

.field private mLastTimeLapseFrameRealTimestampNs:J

.field private mLogVerbose:Z

.field private mMaxDurationMs:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "maxDurationMs"
    .end annotation
.end field

.field private mMaxFileSize:J
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "maxFileSize"
    .end annotation
.end field

.field private mMediaRecorder:Landroid/media/MediaRecorder;

.field private mNumFramesEncoded:I

.field private mOrientationHint:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "orientationHint"
    .end annotation
.end field

.field private mOutputFile:Ljava/lang/String;
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "outputFile"
    .end annotation
.end field

.field private mOutputFormat:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "outputFormat"
    .end annotation
.end field

.field private mPauseResumeRecording:Z
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "pauseNresumeRecording"
    .end annotation
.end field

.field private mProfile:Landroid/media/CamcorderProfile;
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "recordingProfile"
    .end annotation
.end field

.field private mProgram:Landroid/filterfw/core/ShaderProgram;

.field private mRecording:Z
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "recording"
    .end annotation
.end field

.field private mRecordingActive:Z

.field private mRecordingDoneListener:Landroid/filterpacks/videosink/MediaEncoderFilter$OnRecordingDoneListener;
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "recordingDoneListener"
    .end annotation
.end field

.field private mScreen:Landroid/filterfw/core/GLFrame;

.field private mSourceRegion:Landroid/filterfw/geometry/Quad;
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "inputRegion"
    .end annotation
.end field

.field private mSurfaceId:I

.field private mTimeBetweenTimeLapseFrameCaptureUs:J
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "timelapseRecordingIntervalUs"
    .end annotation
.end field

.field private mTimestampNs:J

.field private mVideoEncoder:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "videoEncoder"
    .end annotation
.end field

.field private mVideoEncoderBitrate:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "videoEncoderBitrate"
    .end annotation
.end field

.field private mWidth:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "width"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 14
    .parameter "name"

    #@0
    .prologue
    const-wide/16 v10, 0x0

    #@2
    const/high16 v9, 0x3f80

    #@4
    const/4 v8, 0x0

    #@5
    const/4 v7, 0x0

    #@6
    const/4 v6, 0x0

    #@7
    .line 209
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@a
    .line 60
    const/4 v4, 0x1

    #@b
    iput-boolean v4, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mRecording:Z

    #@d
    .line 64
    new-instance v4, Ljava/lang/String;

    #@f
    const-string v5, "/sdcard/MediaEncoderOut.mp4"

    #@11
    invoke-direct {v4, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@14
    iput-object v4, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mOutputFile:Ljava/lang/String;

    #@16
    .line 68
    iput-object v7, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mFd:Ljava/io/FileDescriptor;

    #@18
    .line 74
    const/4 v4, -0x1

    #@19
    iput v4, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mAudioSource:I

    #@1b
    .line 81
    iput-object v7, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mInfoListener:Landroid/media/MediaRecorder$OnInfoListener;

    #@1d
    .line 88
    iput-object v7, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mErrorListener:Landroid/media/MediaRecorder$OnErrorListener;

    #@1f
    .line 94
    iput-object v7, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mRecordingDoneListener:Landroid/filterpacks/videosink/MediaEncoderFilter$OnRecordingDoneListener;

    #@21
    .line 101
    iput v6, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mOrientationHint:I

    #@23
    .line 108
    iput-object v7, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mProfile:Landroid/media/CamcorderProfile;

    #@25
    .line 113
    iput v6, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mWidth:I

    #@27
    .line 118
    iput v6, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mHeight:I

    #@29
    .line 123
    const/16 v4, 0x1e

    #@2b
    iput v4, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mFps:I

    #@2d
    .line 129
    const/4 v4, 0x2

    #@2e
    iput v4, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mOutputFormat:I

    #@30
    .line 135
    const/4 v4, 0x2

    #@31
    iput v4, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mVideoEncoder:I

    #@33
    .line 141
    const v4, 0x1e8480

    #@36
    iput v4, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mVideoEncoderBitrate:I

    #@38
    .line 156
    iput-wide v10, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMaxFileSize:J

    #@3a
    .line 162
    iput v6, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMaxDurationMs:I

    #@3c
    .line 168
    iput-wide v10, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mTimeBetweenTimeLapseFrameCaptureUs:J

    #@3e
    .line 172
    iput-boolean v6, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mPauseResumeRecording:Z

    #@40
    .line 184
    iput-boolean v6, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mRecordingActive:Z

    #@42
    .line 185
    iput-wide v10, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mTimestampNs:J

    #@44
    .line 186
    iput-wide v10, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mLastTimeLapseFrameRealTimestampNs:J

    #@46
    .line 187
    iput v6, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mNumFramesEncoded:I

    #@48
    .line 190
    iput-boolean v6, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mCaptureTimeLapse:Z

    #@4a
    .line 196
    iput-boolean v6, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mIsRecStart:Z

    #@4c
    .line 210
    new-instance v0, Landroid/filterfw/geometry/Point;

    #@4e
    invoke-direct {v0, v8, v8}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    #@51
    .line 211
    .local v0, bl:Landroid/filterfw/geometry/Point;
    new-instance v1, Landroid/filterfw/geometry/Point;

    #@53
    invoke-direct {v1, v9, v8}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    #@56
    .line 212
    .local v1, br:Landroid/filterfw/geometry/Point;
    new-instance v2, Landroid/filterfw/geometry/Point;

    #@58
    invoke-direct {v2, v8, v9}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    #@5b
    .line 213
    .local v2, tl:Landroid/filterfw/geometry/Point;
    new-instance v3, Landroid/filterfw/geometry/Point;

    #@5d
    invoke-direct {v3, v9, v9}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    #@60
    .line 214
    .local v3, tr:Landroid/filterfw/geometry/Point;
    new-instance v4, Landroid/filterfw/geometry/Quad;

    #@62
    invoke-direct {v4, v0, v1, v2, v3}, Landroid/filterfw/geometry/Quad;-><init>(Landroid/filterfw/geometry/Point;Landroid/filterfw/geometry/Point;Landroid/filterfw/geometry/Point;Landroid/filterfw/geometry/Point;)V

    #@65
    iput-object v4, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mSourceRegion:Landroid/filterfw/geometry/Quad;

    #@67
    .line 215
    const-string v4, "MediaEncoderFilter"

    #@69
    const/4 v5, 0x2

    #@6a
    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6d
    move-result v4

    #@6e
    iput-boolean v4, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mLogVerbose:Z

    #@70
    .line 216
    return-void
.end method

.method private startRecording(Landroid/filterfw/core/FilterContext;)V
    .registers 13
    .parameter "context"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 346
    iget-boolean v5, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mLogVerbose:Z

    #@4
    if-eqz v5, :cond_d

    #@6
    const-string v5, "MediaEncoderFilter"

    #@8
    const-string v8, "Starting recording"

    #@a
    invoke-static {v5, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 349
    :cond_d
    new-instance v2, Landroid/filterfw/core/MutableFrameFormat;

    #@f
    const/4 v5, 0x2

    #@10
    const/4 v8, 0x3

    #@11
    invoke-direct {v2, v5, v8}, Landroid/filterfw/core/MutableFrameFormat;-><init>(II)V

    #@14
    .line 351
    .local v2, screenFormat:Landroid/filterfw/core/MutableFrameFormat;
    const/4 v5, 0x4

    #@15
    invoke-virtual {v2, v5}, Landroid/filterfw/core/MutableFrameFormat;->setBytesPerSample(I)V

    #@18
    .line 354
    iget v5, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mWidth:I

    #@1a
    if-lez v5, :cond_9e

    #@1c
    iget v5, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mHeight:I

    #@1e
    if-lez v5, :cond_9e

    #@20
    move v4, v6

    #@21
    .line 357
    .local v4, widthHeightSpecified:Z
    :goto_21
    iget-object v5, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mProfile:Landroid/media/CamcorderProfile;

    #@23
    if-eqz v5, :cond_a0

    #@25
    if-nez v4, :cond_a0

    #@27
    .line 358
    iget-object v5, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mProfile:Landroid/media/CamcorderProfile;

    #@29
    iget v3, v5, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    #@2b
    .line 359
    .local v3, width:I
    iget-object v5, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mProfile:Landroid/media/CamcorderProfile;

    #@2d
    iget v1, v5, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    #@2f
    .line 364
    .local v1, height:I
    :goto_2f
    invoke-virtual {v2, v3, v1}, Landroid/filterfw/core/MutableFrameFormat;->setDimensions(II)V

    #@32
    .line 365
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@35
    move-result-object v5

    #@36
    const/16 v8, 0x65

    #@38
    const-wide/16 v9, 0x0

    #@3a
    invoke-virtual {v5, v2, v8, v9, v10}, Landroid/filterfw/core/FrameManager;->newBoundFrame(Landroid/filterfw/core/FrameFormat;IJ)Landroid/filterfw/core/Frame;

    #@3d
    move-result-object v5

    #@3e
    check-cast v5, Landroid/filterfw/core/GLFrame;

    #@40
    iput-object v5, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mScreen:Landroid/filterfw/core/GLFrame;

    #@42
    .line 370
    new-instance v5, Landroid/media/MediaRecorder;

    #@44
    invoke-direct {v5}, Landroid/media/MediaRecorder;-><init>()V

    #@47
    iput-object v5, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@49
    .line 371
    invoke-direct {p0}, Landroid/filterpacks/videosink/MediaEncoderFilter;->updateMediaRecorderParams()V

    #@4c
    .line 374
    :try_start_4c
    iget-object v5, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@4e
    invoke-virtual {v5}, Landroid/media/MediaRecorder;->prepare()V
    :try_end_51
    .catch Ljava/lang/IllegalStateException; {:try_start_4c .. :try_end_51} :catch_a5
    .catch Ljava/io/IOException; {:try_start_4c .. :try_end_51} :catch_a7
    .catch Ljava/lang/Exception; {:try_start_4c .. :try_end_51} :catch_b0

    #@51
    .line 389
    iput-boolean v6, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mIsRecStart:Z

    #@53
    .line 390
    iget-boolean v5, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mLogVerbose:Z

    #@55
    if-eqz v5, :cond_71

    #@57
    const-string v5, "MediaEncoderFilter"

    #@59
    new-instance v8, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v9, "MediaEncoderFilter set mPauseResumeRecording = false / mPauseResumeRecording = "

    #@60
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v8

    #@64
    iget-boolean v9, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mPauseResumeRecording:Z

    #@66
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@69
    move-result-object v8

    #@6a
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v8

    #@6e
    invoke-static {v5, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@71
    .line 393
    :cond_71
    iget-object v5, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@73
    invoke-virtual {v5}, Landroid/media/MediaRecorder;->start()V

    #@76
    .line 394
    iget-boolean v5, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mLogVerbose:Z

    #@78
    if-eqz v5, :cond_81

    #@7a
    const-string v5, "MediaEncoderFilter"

    #@7c
    const-string v8, "Open: registering surface from Mediarecorder"

    #@7e
    invoke-static {v5, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    .line 395
    :cond_81
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getGLEnvironment()Landroid/filterfw/core/GLEnvironment;

    #@84
    move-result-object v5

    #@85
    iget-object v8, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@87
    invoke-virtual {v5, v8}, Landroid/filterfw/core/GLEnvironment;->registerSurfaceFromMediaRecorder(Landroid/media/MediaRecorder;)I

    #@8a
    move-result v5

    #@8b
    iput v5, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mSurfaceId:I

    #@8d
    .line 397
    iput v7, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mNumFramesEncoded:I

    #@8f
    .line 398
    iput-boolean v6, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mRecordingActive:Z

    #@91
    .line 403
    const-string v5, "MediaEncoderFilter"

    #@93
    const-string v6, " mMediaRecorder.setAudioZoomExceptionCase"

    #@95
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@98
    .line 404
    iget-object v5, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@9a
    invoke-virtual {v5}, Landroid/media/MediaRecorder;->setAudioZoomExceptionCase()V

    #@9d
    .line 406
    return-void

    #@9e
    .end local v1           #height:I
    .end local v3           #width:I
    .end local v4           #widthHeightSpecified:Z
    :cond_9e
    move v4, v7

    #@9f
    .line 354
    goto :goto_21

    #@a0
    .line 361
    .restart local v4       #widthHeightSpecified:Z
    :cond_a0
    iget v3, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mWidth:I

    #@a2
    .line 362
    .restart local v3       #width:I
    iget v1, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mHeight:I

    #@a4
    .restart local v1       #height:I
    goto :goto_2f

    #@a5
    .line 375
    :catch_a5
    move-exception v0

    #@a6
    .line 376
    .local v0, e:Ljava/lang/IllegalStateException;
    throw v0

    #@a7
    .line 377
    .end local v0           #e:Ljava/lang/IllegalStateException;
    :catch_a7
    move-exception v0

    #@a8
    .line 378
    .local v0, e:Ljava/io/IOException;
    new-instance v5, Ljava/lang/RuntimeException;

    #@aa
    const-string v6, "IOException inMediaRecorder.prepare()!"

    #@ac
    invoke-direct {v5, v6, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@af
    throw v5

    #@b0
    .line 380
    .end local v0           #e:Ljava/io/IOException;
    :catch_b0
    move-exception v0

    #@b1
    .line 381
    .local v0, e:Ljava/lang/Exception;
    new-instance v5, Ljava/lang/RuntimeException;

    #@b3
    const-string v6, "Unknown Exception inMediaRecorder.prepare()!"

    #@b5
    invoke-direct {v5, v6, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@b8
    throw v5
.end method

.method private stopRecording(Landroid/filterfw/core/FilterContext;)V
    .registers 10
    .parameter "context"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x0

    #@2
    .line 485
    iget-boolean v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mLogVerbose:Z

    #@4
    if-eqz v2, :cond_d

    #@6
    const-string v2, "MediaEncoderFilter"

    #@8
    const-string v3, "Stopping recording"

    #@a
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 487
    :cond_d
    iput-boolean v6, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mRecordingActive:Z

    #@f
    .line 488
    iput v6, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mNumFramesEncoded:I

    #@11
    .line 489
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getGLEnvironment()Landroid/filterfw/core/GLEnvironment;

    #@14
    move-result-object v1

    #@15
    .line 495
    .local v1, glEnv:Landroid/filterfw/core/GLEnvironment;
    iget-boolean v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mLogVerbose:Z

    #@17
    if-eqz v2, :cond_2f

    #@19
    const-string v2, "MediaEncoderFilter"

    #@1b
    const-string v3, "Unregistering surface %d"

    #@1d
    const/4 v4, 0x1

    #@1e
    new-array v4, v4, [Ljava/lang/Object;

    #@20
    iget v5, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mSurfaceId:I

    #@22
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@25
    move-result-object v5

    #@26
    aput-object v5, v4, v6

    #@28
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2b
    move-result-object v3

    #@2c
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 496
    :cond_2f
    iget v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mSurfaceId:I

    #@31
    invoke-virtual {v1, v2}, Landroid/filterfw/core/GLEnvironment;->unregisterSurfaceId(I)V

    #@34
    .line 498
    :try_start_34
    iget-object v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@36
    invoke-virtual {v2}, Landroid/media/MediaRecorder;->stop()V
    :try_end_39
    .catch Ljava/lang/RuntimeException; {:try_start_34 .. :try_end_39} :catch_53

    #@39
    .line 503
    iput-boolean v6, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mIsRecStart:Z

    #@3b
    .line 505
    iget-object v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@3d
    invoke-virtual {v2}, Landroid/media/MediaRecorder;->release()V

    #@40
    .line 506
    iput-object v7, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@42
    .line 508
    iget-object v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mScreen:Landroid/filterfw/core/GLFrame;

    #@44
    invoke-virtual {v2}, Landroid/filterfw/core/GLFrame;->release()Landroid/filterfw/core/Frame;

    #@47
    .line 509
    iput-object v7, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mScreen:Landroid/filterfw/core/GLFrame;

    #@49
    .line 514
    iget-object v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mRecordingDoneListener:Landroid/filterpacks/videosink/MediaEncoderFilter$OnRecordingDoneListener;

    #@4b
    if-eqz v2, :cond_52

    #@4d
    .line 515
    iget-object v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mRecordingDoneListener:Landroid/filterpacks/videosink/MediaEncoderFilter$OnRecordingDoneListener;

    #@4f
    invoke-interface {v2}, Landroid/filterpacks/videosink/MediaEncoderFilter$OnRecordingDoneListener;->onRecordingDone()V

    #@52
    .line 517
    :cond_52
    return-void

    #@53
    .line 499
    :catch_53
    move-exception v0

    #@54
    .line 500
    .local v0, e:Ljava/lang/RuntimeException;
    new-instance v2, Landroid/filterpacks/videosink/MediaRecorderStopException;

    #@56
    const-string v3, "MediaRecorder.stop() failed!"

    #@58
    invoke-direct {v2, v3, v0}, Landroid/filterpacks/videosink/MediaRecorderStopException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@5b
    throw v2
.end method

.method private updateMediaRecorderParams()V
    .registers 8

    #@0
    .prologue
    const/4 v6, -0x1

    #@1
    .line 271
    iget-wide v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mTimeBetweenTimeLapseFrameCaptureUs:J

    #@3
    const-wide/16 v4, 0x0

    #@5
    cmp-long v2, v2, v4

    #@7
    if-lez v2, :cond_66

    #@9
    const/4 v2, 0x1

    #@a
    :goto_a
    iput-boolean v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mCaptureTimeLapse:Z

    #@c
    .line 272
    const/4 v0, 0x2

    #@d
    .line 273
    .local v0, GRALLOC_BUFFER:I
    iget-object v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@f
    const/4 v3, 0x2

    #@10
    invoke-virtual {v2, v3}, Landroid/media/MediaRecorder;->setVideoSource(I)V

    #@13
    .line 274
    iget-boolean v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mCaptureTimeLapse:Z

    #@15
    if-nez v2, :cond_22

    #@17
    iget v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mAudioSource:I

    #@19
    if-eq v2, v6, :cond_22

    #@1b
    .line 275
    iget-object v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@1d
    iget v3, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mAudioSource:I

    #@1f
    invoke-virtual {v2, v3}, Landroid/media/MediaRecorder;->setAudioSource(I)V

    #@22
    .line 277
    :cond_22
    iget-object v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mProfile:Landroid/media/CamcorderProfile;

    #@24
    if-eqz v2, :cond_96

    #@26
    .line 279
    iget-object v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mProfile:Landroid/media/CamcorderProfile;

    #@28
    iget v2, v2, Landroid/media/CamcorderProfile;->videoFrameRate:I

    #@2a
    iput v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mFps:I

    #@2c
    .line 281
    iget v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mAudioSource:I

    #@2e
    if-eq v2, v6, :cond_68

    #@30
    .line 282
    iget-object v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@32
    iget-object v3, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mProfile:Landroid/media/CamcorderProfile;

    #@34
    invoke-virtual {v2, v3}, Landroid/media/MediaRecorder;->setProfile(Landroid/media/CamcorderProfile;)V

    #@37
    .line 307
    :goto_37
    iget-object v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@39
    iget v3, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mOrientationHint:I

    #@3b
    invoke-virtual {v2, v3}, Landroid/media/MediaRecorder;->setOrientationHint(I)V

    #@3e
    .line 308
    iget-object v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@40
    iget-object v3, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mInfoListener:Landroid/media/MediaRecorder$OnInfoListener;

    #@42
    invoke-virtual {v2, v3}, Landroid/media/MediaRecorder;->setOnInfoListener(Landroid/media/MediaRecorder$OnInfoListener;)V

    #@45
    .line 309
    iget-object v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@47
    iget-object v3, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mErrorListener:Landroid/media/MediaRecorder$OnErrorListener;

    #@49
    invoke-virtual {v2, v3}, Landroid/media/MediaRecorder;->setOnErrorListener(Landroid/media/MediaRecorder$OnErrorListener;)V

    #@4c
    .line 310
    iget-object v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mFd:Ljava/io/FileDescriptor;

    #@4e
    if-eqz v2, :cond_bd

    #@50
    .line 311
    iget-object v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@52
    iget-object v3, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mFd:Ljava/io/FileDescriptor;

    #@54
    invoke-virtual {v2, v3}, Landroid/media/MediaRecorder;->setOutputFile(Ljava/io/FileDescriptor;)V

    #@57
    .line 316
    :goto_57
    :try_start_57
    iget-object v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@59
    iget-wide v3, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMaxFileSize:J

    #@5b
    invoke-virtual {v2, v3, v4}, Landroid/media/MediaRecorder;->setMaxFileSize(J)V
    :try_end_5e
    .catch Ljava/lang/Exception; {:try_start_57 .. :try_end_5e} :catch_c5

    #@5e
    .line 326
    :goto_5e
    iget-object v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@60
    iget v3, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMaxDurationMs:I

    #@62
    invoke-virtual {v2, v3}, Landroid/media/MediaRecorder;->setMaxDuration(I)V

    #@65
    .line 327
    return-void

    #@66
    .line 271
    .end local v0           #GRALLOC_BUFFER:I
    :cond_66
    const/4 v2, 0x0

    #@67
    goto :goto_a

    #@68
    .line 285
    .restart local v0       #GRALLOC_BUFFER:I
    :cond_68
    iget-object v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@6a
    iget v3, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mOutputFormat:I

    #@6c
    invoke-virtual {v2, v3}, Landroid/media/MediaRecorder;->setOutputFormat(I)V

    #@6f
    .line 286
    iget-object v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@71
    iget v3, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mVideoEncoder:I

    #@73
    invoke-virtual {v2, v3}, Landroid/media/MediaRecorder;->setVideoEncoder(I)V

    #@76
    .line 289
    iget v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mWidth:I

    #@78
    if-lez v2, :cond_87

    #@7a
    iget v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mHeight:I

    #@7c
    if-lez v2, :cond_87

    #@7e
    .line 290
    iget-object v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@80
    iget v3, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mWidth:I

    #@82
    iget v4, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mHeight:I

    #@84
    invoke-virtual {v2, v3, v4}, Landroid/media/MediaRecorder;->setVideoSize(II)V

    #@87
    .line 292
    :cond_87
    iget-object v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@89
    iget v3, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mVideoEncoderBitrate:I

    #@8b
    invoke-virtual {v2, v3}, Landroid/media/MediaRecorder;->setVideoEncodingBitRate(I)V

    #@8e
    .line 293
    iget-object v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@90
    iget v3, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mFps:I

    #@92
    invoke-virtual {v2, v3}, Landroid/media/MediaRecorder;->setVideoFrameRate(I)V

    #@95
    goto :goto_37

    #@96
    .line 299
    :cond_96
    iget-object v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@98
    iget v3, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mOutputFormat:I

    #@9a
    invoke-virtual {v2, v3}, Landroid/media/MediaRecorder;->setOutputFormat(I)V

    #@9d
    .line 300
    iget-object v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@9f
    iget v3, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mVideoEncoder:I

    #@a1
    invoke-virtual {v2, v3}, Landroid/media/MediaRecorder;->setVideoEncoder(I)V

    #@a4
    .line 301
    iget-object v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@a6
    iget v3, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mWidth:I

    #@a8
    iget v4, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mHeight:I

    #@aa
    invoke-virtual {v2, v3, v4}, Landroid/media/MediaRecorder;->setVideoSize(II)V

    #@ad
    .line 303
    iget-object v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@af
    iget v3, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mVideoEncoderBitrate:I

    #@b1
    invoke-virtual {v2, v3}, Landroid/media/MediaRecorder;->setVideoEncodingBitRate(I)V

    #@b4
    .line 305
    iget-object v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@b6
    iget v3, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mFps:I

    #@b8
    invoke-virtual {v2, v3}, Landroid/media/MediaRecorder;->setVideoFrameRate(I)V

    #@bb
    goto/16 :goto_37

    #@bd
    .line 313
    :cond_bd
    iget-object v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@bf
    iget-object v3, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mOutputFile:Ljava/lang/String;

    #@c1
    invoke-virtual {v2, v3}, Landroid/media/MediaRecorder;->setOutputFile(Ljava/lang/String;)V

    #@c4
    goto :goto_57

    #@c5
    .line 317
    :catch_c5
    move-exception v1

    #@c6
    .line 323
    .local v1, e:Ljava/lang/Exception;
    const-string v2, "MediaEncoderFilter"

    #@c8
    new-instance v3, Ljava/lang/StringBuilder;

    #@ca
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@cd
    const-string v4, "Setting maxFileSize on MediaRecorder unsuccessful! "

    #@cf
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v3

    #@d3
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@d6
    move-result-object v4

    #@d7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@da
    move-result-object v3

    #@db
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@de
    move-result-object v3

    #@df
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e2
    goto/16 :goto_5e
.end method

.method private updateSourceRegion()V
    .registers 3

    #@0
    .prologue
    .line 259
    new-instance v0, Landroid/filterfw/geometry/Quad;

    #@2
    invoke-direct {v0}, Landroid/filterfw/geometry/Quad;-><init>()V

    #@5
    .line 260
    .local v0, flippedRegion:Landroid/filterfw/geometry/Quad;
    iget-object v1, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mSourceRegion:Landroid/filterfw/geometry/Quad;

    #@7
    iget-object v1, v1, Landroid/filterfw/geometry/Quad;->p2:Landroid/filterfw/geometry/Point;

    #@9
    iput-object v1, v0, Landroid/filterfw/geometry/Quad;->p0:Landroid/filterfw/geometry/Point;

    #@b
    .line 261
    iget-object v1, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mSourceRegion:Landroid/filterfw/geometry/Quad;

    #@d
    iget-object v1, v1, Landroid/filterfw/geometry/Quad;->p3:Landroid/filterfw/geometry/Point;

    #@f
    iput-object v1, v0, Landroid/filterfw/geometry/Quad;->p1:Landroid/filterfw/geometry/Point;

    #@11
    .line 262
    iget-object v1, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mSourceRegion:Landroid/filterfw/geometry/Quad;

    #@13
    iget-object v1, v1, Landroid/filterfw/geometry/Quad;->p0:Landroid/filterfw/geometry/Point;

    #@15
    iput-object v1, v0, Landroid/filterfw/geometry/Quad;->p2:Landroid/filterfw/geometry/Point;

    #@17
    .line 263
    iget-object v1, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mSourceRegion:Landroid/filterfw/geometry/Quad;

    #@19
    iget-object v1, v1, Landroid/filterfw/geometry/Quad;->p1:Landroid/filterfw/geometry/Point;

    #@1b
    iput-object v1, v0, Landroid/filterfw/geometry/Quad;->p3:Landroid/filterfw/geometry/Point;

    #@1d
    .line 264
    iget-object v1, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@1f
    invoke-virtual {v1, v0}, Landroid/filterfw/core/ShaderProgram;->setSourceRegion(Landroid/filterfw/geometry/Quad;)V

    #@22
    .line 265
    return-void
.end method


# virtual methods
.method public close(Landroid/filterfw/core/FilterContext;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 521
    iget-boolean v0, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mLogVerbose:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    const-string v0, "MediaEncoderFilter"

    #@6
    const-string v1, "Closing"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 522
    :cond_b
    iget-boolean v0, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mRecordingActive:Z

    #@d
    if-eqz v0, :cond_12

    #@f
    invoke-direct {p0, p1}, Landroid/filterpacks/videosink/MediaEncoderFilter;->stopRecording(Landroid/filterfw/core/FilterContext;)V

    #@12
    .line 523
    :cond_12
    return-void
.end method

.method public fieldPortValueUpdated(Ljava/lang/String;Landroid/filterfw/core/FilterContext;)V
    .registers 6
    .parameter "name"
    .parameter "context"

    #@0
    .prologue
    .line 227
    iget-boolean v0, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mLogVerbose:Z

    #@2
    if-eqz v0, :cond_22

    #@4
    const-string v0, "MediaEncoderFilter"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "Port "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, " has been updated"

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 228
    :cond_22
    const-string/jumbo v0, "recording"

    #@25
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v0

    #@29
    if-eqz v0, :cond_2c

    #@2b
    .line 255
    :cond_2b
    :goto_2b
    return-void

    #@2c
    .line 229
    :cond_2c
    const-string v0, "inputRegion"

    #@2e
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@31
    move-result v0

    #@32
    if-eqz v0, :cond_3e

    #@34
    .line 230
    invoke-virtual {p0}, Landroid/filterpacks/videosink/MediaEncoderFilter;->isOpen()Z

    #@37
    move-result v0

    #@38
    if-eqz v0, :cond_2b

    #@3a
    invoke-direct {p0}, Landroid/filterpacks/videosink/MediaEncoderFilter;->updateSourceRegion()V

    #@3d
    goto :goto_2b

    #@3e
    .line 235
    :cond_3e
    const-string/jumbo v0, "pauseNresumeRecording"

    #@41
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@44
    move-result v0

    #@45
    if-eqz v0, :cond_91

    #@47
    iget-boolean v0, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mIsRecStart:Z

    #@49
    const/4 v1, 0x1

    #@4a
    if-ne v0, v1, :cond_91

    #@4c
    .line 236
    iget-boolean v0, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mLogVerbose:Z

    #@4e
    if-eqz v0, :cond_6b

    #@50
    const-string v0, "MediaEncoderFilter"

    #@52
    new-instance v1, Ljava/lang/StringBuilder;

    #@54
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@57
    const-string/jumbo v2, "pauseNresumeRecording = "

    #@5a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v1

    #@5e
    iget-boolean v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mPauseResumeRecording:Z

    #@60
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@63
    move-result-object v1

    #@64
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v1

    #@68
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@6b
    .line 237
    :cond_6b
    iget-boolean v0, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mPauseResumeRecording:Z

    #@6d
    if-eqz v0, :cond_80

    #@6f
    .line 238
    iget-boolean v0, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mLogVerbose:Z

    #@71
    if-eqz v0, :cond_7a

    #@73
    const-string v0, "MediaEncoderFilter"

    #@75
    const-string v1, "MediaEncoderFilter call mMediaRecorder.pause()"

    #@77
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    .line 239
    :cond_7a
    iget-object v0, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@7c
    invoke-virtual {v0}, Landroid/media/MediaRecorder;->pause()V

    #@7f
    goto :goto_2b

    #@80
    .line 242
    :cond_80
    iget-boolean v0, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mLogVerbose:Z

    #@82
    if-eqz v0, :cond_8b

    #@84
    const-string v0, "MediaEncoderFilter"

    #@86
    const-string v1, "MediaEncoderFilter call mMediaRecorder.resume()"

    #@88
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@8b
    .line 243
    :cond_8b
    iget-object v0, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@8d
    invoke-virtual {v0}, Landroid/media/MediaRecorder;->resume()V

    #@90
    goto :goto_2b

    #@91
    .line 251
    :cond_91
    invoke-virtual {p0}, Landroid/filterpacks/videosink/MediaEncoderFilter;->isOpen()Z

    #@94
    move-result v0

    #@95
    if-eqz v0, :cond_2b

    #@97
    iget-boolean v0, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mRecordingActive:Z

    #@99
    if-eqz v0, :cond_2b

    #@9b
    .line 252
    new-instance v0, Ljava/lang/RuntimeException;

    #@9d
    const-string v1, "Cannot change recording parameters when the filter is recording!"

    #@9f
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@a2
    throw v0
.end method

.method public open(Landroid/filterfw/core/FilterContext;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 340
    iget-boolean v0, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mLogVerbose:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    const-string v0, "MediaEncoderFilter"

    #@6
    const-string v1, "Opening"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 341
    :cond_b
    invoke-direct {p0}, Landroid/filterpacks/videosink/MediaEncoderFilter;->updateSourceRegion()V

    #@e
    .line 342
    iget-boolean v0, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mRecording:Z

    #@10
    if-eqz v0, :cond_15

    #@12
    invoke-direct {p0, p1}, Landroid/filterpacks/videosink/MediaEncoderFilter;->startRecording(Landroid/filterfw/core/FilterContext;)V

    #@15
    .line 343
    :cond_15
    return-void
.end method

.method public prepare(Landroid/filterfw/core/FilterContext;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 331
    iget-boolean v0, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mLogVerbose:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    const-string v0, "MediaEncoderFilter"

    #@6
    const-string v1, "Preparing"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 333
    :cond_b
    invoke-static {p1}, Landroid/filterfw/core/ShaderProgram;->createIdentity(Landroid/filterfw/core/FilterContext;)Landroid/filterfw/core/ShaderProgram;

    #@e
    move-result-object v0

    #@f
    iput-object v0, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@11
    .line 335
    const/4 v0, 0x0

    #@12
    iput-boolean v0, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mRecordingActive:Z

    #@14
    .line 336
    return-void
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    .line 448
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getGLEnvironment()Landroid/filterfw/core/GLEnvironment;

    #@3
    move-result-object v0

    #@4
    .line 450
    .local v0, glEnv:Landroid/filterfw/core/GLEnvironment;
    const-string/jumbo v2, "videoframe"

    #@7
    invoke-virtual {p0, v2}, Landroid/filterpacks/videosink/MediaEncoderFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@a
    move-result-object v1

    #@b
    .line 453
    .local v1, input:Landroid/filterfw/core/Frame;
    iget-boolean v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mRecordingActive:Z

    #@d
    if-nez v2, :cond_16

    #@f
    iget-boolean v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mRecording:Z

    #@11
    if-eqz v2, :cond_16

    #@13
    .line 454
    invoke-direct {p0, p1}, Landroid/filterpacks/videosink/MediaEncoderFilter;->startRecording(Landroid/filterfw/core/FilterContext;)V

    #@16
    .line 457
    :cond_16
    iget-boolean v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mRecordingActive:Z

    #@18
    if-eqz v2, :cond_21

    #@1a
    iget-boolean v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mRecording:Z

    #@1c
    if-nez v2, :cond_21

    #@1e
    .line 458
    invoke-direct {p0, p1}, Landroid/filterpacks/videosink/MediaEncoderFilter;->stopRecording(Landroid/filterfw/core/FilterContext;)V

    #@21
    .line 461
    :cond_21
    iget-boolean v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mRecordingActive:Z

    #@23
    if-nez v2, :cond_26

    #@25
    .line 482
    :cond_25
    :goto_25
    return-void

    #@26
    .line 463
    :cond_26
    iget-boolean v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mCaptureTimeLapse:Z

    #@28
    if-eqz v2, :cond_4f

    #@2a
    .line 464
    invoke-virtual {v1}, Landroid/filterfw/core/Frame;->getTimestamp()J

    #@2d
    move-result-wide v2

    #@2e
    invoke-virtual {p0, v2, v3}, Landroid/filterpacks/videosink/MediaEncoderFilter;->skipFrameAndModifyTimestamp(J)Z

    #@31
    move-result v2

    #@32
    if-nez v2, :cond_25

    #@34
    .line 472
    :goto_34
    iget v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mSurfaceId:I

    #@36
    invoke-virtual {v0, v2}, Landroid/filterfw/core/GLEnvironment;->activateSurfaceWithId(I)V

    #@39
    .line 475
    iget-object v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@3b
    iget-object v3, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mScreen:Landroid/filterfw/core/GLFrame;

    #@3d
    invoke-virtual {v2, v1, v3}, Landroid/filterfw/core/ShaderProgram;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@40
    .line 478
    iget-wide v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mTimestampNs:J

    #@42
    invoke-virtual {v0, v2, v3}, Landroid/filterfw/core/GLEnvironment;->setSurfaceTimestamp(J)V

    #@45
    .line 480
    invoke-virtual {v0}, Landroid/filterfw/core/GLEnvironment;->swapBuffers()V

    #@48
    .line 481
    iget v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mNumFramesEncoded:I

    #@4a
    add-int/lit8 v2, v2, 0x1

    #@4c
    iput v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mNumFramesEncoded:I

    #@4e
    goto :goto_25

    #@4f
    .line 468
    :cond_4f
    invoke-virtual {v1}, Landroid/filterfw/core/Frame;->getTimestamp()J

    #@52
    move-result-wide v2

    #@53
    iput-wide v2, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mTimestampNs:J

    #@55
    goto :goto_34
.end method

.method public setupPorts()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x3

    #@1
    .line 221
    const-string/jumbo v0, "videoframe"

    #@4
    invoke-static {v1, v1}, Landroid/filterfw/format/ImageFormat;->create(II)Landroid/filterfw/core/MutableFrameFormat;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/videosink/MediaEncoderFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@b
    .line 223
    return-void
.end method

.method public skipFrameAndModifyTimestamp(J)Z
    .registers 12
    .parameter "timestampNs"

    #@0
    .prologue
    const-wide/32 v7, 0x3b9aca00

    #@3
    const/4 v0, 0x0

    #@4
    .line 410
    iget v1, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mNumFramesEncoded:I

    #@6
    if-nez v1, :cond_38

    #@8
    .line 411
    iput-wide p1, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mLastTimeLapseFrameRealTimestampNs:J

    #@a
    .line 412
    iput-wide p1, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mTimestampNs:J

    #@c
    .line 413
    iget-boolean v1, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mLogVerbose:Z

    #@e
    if-eqz v1, :cond_37

    #@10
    const-string v1, "MediaEncoderFilter"

    #@12
    new-instance v2, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string/jumbo v3, "timelapse: FIRST frame, last real t= "

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    iget-wide v3, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mLastTimeLapseFrameRealTimestampNs:J

    #@20
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    const-string v3, ", setting t = "

    #@26
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    iget-wide v3, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mTimestampNs:J

    #@2c
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v2

    #@34
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 442
    :cond_37
    :goto_37
    return v0

    #@38
    .line 422
    :cond_38
    iget v1, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mNumFramesEncoded:I

    #@3a
    const/4 v2, 0x2

    #@3b
    if-lt v1, v2, :cond_57

    #@3d
    iget-wide v1, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mLastTimeLapseFrameRealTimestampNs:J

    #@3f
    const-wide/16 v3, 0x3e8

    #@41
    iget-wide v5, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mTimeBetweenTimeLapseFrameCaptureUs:J

    #@43
    mul-long/2addr v3, v5

    #@44
    add-long/2addr v1, v3

    #@45
    cmp-long v1, p1, v1

    #@47
    if-gez v1, :cond_57

    #@49
    .line 427
    iget-boolean v0, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mLogVerbose:Z

    #@4b
    if-eqz v0, :cond_55

    #@4d
    const-string v0, "MediaEncoderFilter"

    #@4f
    const-string/jumbo v1, "timelapse: skipping intermediate frame"

    #@52
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    .line 428
    :cond_55
    const/4 v0, 0x1

    #@56
    goto :goto_37

    #@57
    .line 434
    :cond_57
    iget-boolean v1, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mLogVerbose:Z

    #@59
    if-eqz v1, :cond_8c

    #@5b
    const-string v1, "MediaEncoderFilter"

    #@5d
    new-instance v2, Ljava/lang/StringBuilder;

    #@5f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@62
    const-string/jumbo v3, "timelapse: encoding frame, Timestamp t = "

    #@65
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v2

    #@69
    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v2

    #@6d
    const-string v3, ", last real t= "

    #@6f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v2

    #@73
    iget-wide v3, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mLastTimeLapseFrameRealTimestampNs:J

    #@75
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@78
    move-result-object v2

    #@79
    const-string v3, ", interval = "

    #@7b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v2

    #@7f
    iget-wide v3, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mTimeBetweenTimeLapseFrameCaptureUs:J

    #@81
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@84
    move-result-object v2

    #@85
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@88
    move-result-object v2

    #@89
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@8c
    .line 437
    :cond_8c
    iput-wide p1, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mLastTimeLapseFrameRealTimestampNs:J

    #@8e
    .line 438
    iget-wide v1, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mTimestampNs:J

    #@90
    iget v3, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mFps:I

    #@92
    int-to-long v3, v3

    #@93
    div-long v3, v7, v3

    #@95
    add-long/2addr v1, v3

    #@96
    iput-wide v1, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mTimestampNs:J

    #@98
    .line 439
    iget-boolean v1, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mLogVerbose:Z

    #@9a
    if-eqz v1, :cond_37

    #@9c
    const-string v1, "MediaEncoderFilter"

    #@9e
    new-instance v2, Ljava/lang/StringBuilder;

    #@a0
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a3
    const-string/jumbo v3, "timelapse: encoding frame, setting t = "

    #@a6
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v2

    #@aa
    iget-wide v3, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mTimestampNs:J

    #@ac
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@af
    move-result-object v2

    #@b0
    const-string v3, ", delta t = "

    #@b2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v2

    #@b6
    iget v3, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mFps:I

    #@b8
    int-to-long v3, v3

    #@b9
    div-long v3, v7, v3

    #@bb
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@be
    move-result-object v2

    #@bf
    const-string v3, ", fps = "

    #@c1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v2

    #@c5
    iget v3, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mFps:I

    #@c7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v2

    #@cb
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ce
    move-result-object v2

    #@cf
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@d2
    goto/16 :goto_37
.end method

.method public tearDown(Landroid/filterfw/core/FilterContext;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 529
    iget-object v0, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 530
    iget-object v0, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mMediaRecorder:Landroid/media/MediaRecorder;

    #@6
    invoke-virtual {v0}, Landroid/media/MediaRecorder;->release()V

    #@9
    .line 532
    :cond_9
    iget-object v0, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mScreen:Landroid/filterfw/core/GLFrame;

    #@b
    if-eqz v0, :cond_12

    #@d
    .line 533
    iget-object v0, p0, Landroid/filterpacks/videosink/MediaEncoderFilter;->mScreen:Landroid/filterfw/core/GLFrame;

    #@f
    invoke-virtual {v0}, Landroid/filterfw/core/GLFrame;->release()Landroid/filterfw/core/Frame;

    #@12
    .line 536
    :cond_12
    return-void
.end method
