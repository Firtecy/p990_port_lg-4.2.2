.class public Landroid/filterpacks/ui/SurfaceTargetFilter;
.super Landroid/filterfw/core/Filter;
.source "SurfaceTargetFilter.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SurfaceRenderFilter"


# instance fields
.field private final RENDERMODE_FILL_CROP:I

.field private final RENDERMODE_FIT:I

.field private final RENDERMODE_STRETCH:I

.field private mAspectRatio:F

.field private mGlEnv:Landroid/filterfw/core/GLEnvironment;

.field private mLogVerbose:Z

.field private mProgram:Landroid/filterfw/core/ShaderProgram;

.field private mRenderMode:I

.field private mRenderModeString:Ljava/lang/String;
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "renderMode"
    .end annotation
.end field

.field private mScreen:Landroid/filterfw/core/GLFrame;

.field private mScreenHeight:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "oheight"
    .end annotation
.end field

.field private mScreenWidth:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "owidth"
    .end annotation
.end field

.field private mSurface:Landroid/view/Surface;
    .annotation runtime Landroid/filterfw/core/GenerateFinalPort;
        name = "surface"
    .end annotation
.end field

.field private mSurfaceId:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 5
    .parameter "name"

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    const/4 v1, 0x1

    #@2
    .line 91
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@5
    .line 49
    const/4 v0, 0x0

    #@6
    iput v0, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->RENDERMODE_STRETCH:I

    #@8
    .line 50
    iput v1, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->RENDERMODE_FIT:I

    #@a
    .line 51
    iput v2, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->RENDERMODE_FILL_CROP:I

    #@c
    .line 82
    iput v1, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mRenderMode:I

    #@e
    .line 83
    const/high16 v0, 0x3f80

    #@10
    iput v0, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mAspectRatio:F

    #@12
    .line 85
    const/4 v0, -0x1

    #@13
    iput v0, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mSurfaceId:I

    #@15
    .line 93
    const-string v0, "SurfaceRenderFilter"

    #@17
    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@1a
    move-result v0

    #@1b
    iput-boolean v0, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mLogVerbose:Z

    #@1d
    .line 94
    return-void
.end method

.method private registerSurface()V
    .registers 4

    #@0
    .prologue
    .line 245
    iget-object v0, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mGlEnv:Landroid/filterfw/core/GLEnvironment;

    #@2
    iget-object v1, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mSurface:Landroid/view/Surface;

    #@4
    invoke-virtual {v0, v1}, Landroid/filterfw/core/GLEnvironment;->registerSurface(Landroid/view/Surface;)I

    #@7
    move-result v0

    #@8
    iput v0, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mSurfaceId:I

    #@a
    .line 246
    iget v0, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mSurfaceId:I

    #@c
    if-gez v0, :cond_29

    #@e
    .line 247
    new-instance v0, Ljava/lang/RuntimeException;

    #@10
    new-instance v1, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v2, "Could not register Surface: "

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    iget-object v2, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mSurface:Landroid/view/Surface;

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@28
    throw v0

    #@29
    .line 249
    :cond_29
    return-void
.end method

.method private unregisterSurface()V
    .registers 3

    #@0
    .prologue
    .line 252
    iget v0, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mSurfaceId:I

    #@2
    if-lez v0, :cond_b

    #@4
    .line 253
    iget-object v0, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mGlEnv:Landroid/filterfw/core/GLEnvironment;

    #@6
    iget v1, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mSurfaceId:I

    #@8
    invoke-virtual {v0, v1}, Landroid/filterfw/core/GLEnvironment;->unregisterSurfaceId(I)V

    #@b
    .line 255
    :cond_b
    return-void
.end method

.method private updateTargetRect()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/high16 v4, 0x3f00

    #@3
    const/high16 v5, 0x3f80

    #@5
    .line 210
    iget v2, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mScreenWidth:I

    #@7
    if-lez v2, :cond_22

    #@9
    iget v2, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mScreenHeight:I

    #@b
    if-lez v2, :cond_22

    #@d
    iget-object v2, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@f
    if-eqz v2, :cond_22

    #@11
    .line 211
    iget v2, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mScreenWidth:I

    #@13
    int-to-float v2, v2

    #@14
    iget v3, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mScreenHeight:I

    #@16
    int-to-float v3, v3

    #@17
    div-float v1, v2, v3

    #@19
    .line 212
    .local v1, screenAspectRatio:F
    iget v2, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mAspectRatio:F

    #@1b
    div-float v0, v1, v2

    #@1d
    .line 214
    .local v0, relativeAspectRatio:F
    iget v2, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mRenderMode:I

    #@1f
    packed-switch v2, :pswitch_data_5e

    #@22
    .line 242
    .end local v0           #relativeAspectRatio:F
    .end local v1           #screenAspectRatio:F
    :cond_22
    :goto_22
    return-void

    #@23
    .line 216
    .restart local v0       #relativeAspectRatio:F
    .restart local v1       #screenAspectRatio:F
    :pswitch_23
    iget-object v2, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@25
    invoke-virtual {v2, v6, v6, v5, v5}, Landroid/filterfw/core/ShaderProgram;->setTargetRect(FFFF)V

    #@28
    goto :goto_22

    #@29
    .line 219
    :pswitch_29
    cmpl-float v2, v0, v5

    #@2b
    if-lez v2, :cond_39

    #@2d
    .line 221
    iget-object v2, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@2f
    div-float v3, v4, v0

    #@31
    sub-float v3, v4, v3

    #@33
    div-float v4, v5, v0

    #@35
    invoke-virtual {v2, v3, v6, v4, v5}, Landroid/filterfw/core/ShaderProgram;->setTargetRect(FFFF)V

    #@38
    goto :goto_22

    #@39
    .line 225
    :cond_39
    iget-object v2, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@3b
    mul-float v3, v4, v0

    #@3d
    sub-float v3, v4, v3

    #@3f
    invoke-virtual {v2, v6, v3, v5, v0}, Landroid/filterfw/core/ShaderProgram;->setTargetRect(FFFF)V

    #@42
    goto :goto_22

    #@43
    .line 230
    :pswitch_43
    cmpl-float v2, v0, v5

    #@45
    if-lez v2, :cond_51

    #@47
    .line 232
    iget-object v2, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@49
    mul-float v3, v4, v0

    #@4b
    sub-float v3, v4, v3

    #@4d
    invoke-virtual {v2, v6, v3, v5, v0}, Landroid/filterfw/core/ShaderProgram;->setTargetRect(FFFF)V

    #@50
    goto :goto_22

    #@51
    .line 236
    :cond_51
    iget-object v2, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@53
    div-float v3, v4, v0

    #@55
    sub-float v3, v4, v3

    #@57
    div-float v4, v5, v0

    #@59
    invoke-virtual {v2, v3, v6, v4, v5}, Landroid/filterfw/core/ShaderProgram;->setTargetRect(FFFF)V

    #@5c
    goto :goto_22

    #@5d
    .line 214
    nop

    #@5e
    :pswitch_data_5e
    .packed-switch 0x0
        :pswitch_23
        :pswitch_29
        :pswitch_43
    .end packed-switch
.end method


# virtual methods
.method public close(Landroid/filterfw/core/FilterContext;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 199
    invoke-direct {p0}, Landroid/filterpacks/ui/SurfaceTargetFilter;->unregisterSurface()V

    #@3
    .line 200
    return-void
.end method

.method public fieldPortValueUpdated(Ljava/lang/String;Landroid/filterfw/core/FilterContext;)V
    .registers 7
    .parameter "name"
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 193
    iget-object v0, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mScreen:Landroid/filterfw/core/GLFrame;

    #@3
    iget v1, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mScreenWidth:I

    #@5
    iget v2, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mScreenHeight:I

    #@7
    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/filterfw/core/GLFrame;->setViewport(IIII)V

    #@a
    .line 194
    invoke-direct {p0}, Landroid/filterpacks/ui/SurfaceTargetFilter;->updateTargetRect()V

    #@d
    .line 195
    return-void
.end method

.method public open(Landroid/filterfw/core/FilterContext;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 147
    invoke-direct {p0}, Landroid/filterpacks/ui/SurfaceTargetFilter;->registerSurface()V

    #@3
    .line 148
    return-void
.end method

.method public prepare(Landroid/filterfw/core/FilterContext;)V
    .registers 8
    .parameter "context"

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/high16 v4, 0x3f80

    #@3
    const/4 v3, 0x0

    #@4
    .line 124
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getGLEnvironment()Landroid/filterfw/core/GLEnvironment;

    #@7
    move-result-object v1

    #@8
    iput-object v1, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mGlEnv:Landroid/filterfw/core/GLEnvironment;

    #@a
    .line 128
    invoke-static {p1}, Landroid/filterfw/core/ShaderProgram;->createIdentity(Landroid/filterfw/core/FilterContext;)Landroid/filterfw/core/ShaderProgram;

    #@d
    move-result-object v1

    #@e
    iput-object v1, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@10
    .line 129
    iget-object v1, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@12
    const/high16 v2, -0x4080

    #@14
    invoke-virtual {v1, v3, v4, v4, v2}, Landroid/filterfw/core/ShaderProgram;->setSourceRect(FFFF)V

    #@17
    .line 130
    iget-object v1, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@19
    const/4 v2, 0x1

    #@1a
    invoke-virtual {v1, v2}, Landroid/filterfw/core/ShaderProgram;->setClearsOutput(Z)V

    #@1d
    .line 131
    iget-object v1, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@1f
    invoke-virtual {v1, v3, v3, v3}, Landroid/filterfw/core/ShaderProgram;->setClearColor(FFF)V

    #@22
    .line 133
    iget v1, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mScreenWidth:I

    #@24
    iget v2, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mScreenHeight:I

    #@26
    invoke-static {v1, v2, v5, v5}, Landroid/filterfw/format/ImageFormat;->create(IIII)Landroid/filterfw/core/MutableFrameFormat;

    #@29
    move-result-object v0

    #@2a
    .line 137
    .local v0, screenFormat:Landroid/filterfw/core/MutableFrameFormat;
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@2d
    move-result-object v1

    #@2e
    const/16 v2, 0x65

    #@30
    const-wide/16 v3, 0x0

    #@32
    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/filterfw/core/FrameManager;->newBoundFrame(Landroid/filterfw/core/FrameFormat;IJ)Landroid/filterfw/core/Frame;

    #@35
    move-result-object v1

    #@36
    check-cast v1, Landroid/filterfw/core/GLFrame;

    #@38
    iput-object v1, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mScreen:Landroid/filterfw/core/GLFrame;

    #@3a
    .line 142
    invoke-virtual {p0}, Landroid/filterpacks/ui/SurfaceTargetFilter;->updateRenderMode()V

    #@3d
    .line 143
    return-void
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 11
    .parameter "context"

    #@0
    .prologue
    const/4 v8, 0x3

    #@1
    .line 152
    iget-boolean v5, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mLogVerbose:Z

    #@3
    if-eqz v5, :cond_c

    #@5
    const-string v5, "SurfaceRenderFilter"

    #@7
    const-string v6, "Starting frame processing"

    #@9
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 155
    :cond_c
    const-string v5, "frame"

    #@e
    invoke-virtual {p0, v5}, Landroid/filterpacks/ui/SurfaceTargetFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@11
    move-result-object v3

    #@12
    .line 156
    .local v3, input:Landroid/filterfw/core/Frame;
    const/4 v0, 0x0

    #@13
    .line 158
    .local v0, createdFrame:Z
    invoke-virtual {v3}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@16
    move-result-object v5

    #@17
    invoke-virtual {v5}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@1a
    move-result v5

    #@1b
    int-to-float v5, v5

    #@1c
    invoke-virtual {v3}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@1f
    move-result-object v6

    #@20
    invoke-virtual {v6}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@23
    move-result v6

    #@24
    int-to-float v6, v6

    #@25
    div-float v1, v5, v6

    #@27
    .line 159
    .local v1, currentAspectRatio:F
    iget v5, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mAspectRatio:F

    #@29
    cmpl-float v5, v1, v5

    #@2b
    if-eqz v5, :cond_5a

    #@2d
    .line 160
    iget-boolean v5, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mLogVerbose:Z

    #@2f
    if-eqz v5, :cond_55

    #@31
    const-string v5, "SurfaceRenderFilter"

    #@33
    new-instance v6, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v7, "New aspect ratio: "

    #@3a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v6

    #@3e
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@41
    move-result-object v6

    #@42
    const-string v7, ", previously: "

    #@44
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v6

    #@48
    iget v7, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mAspectRatio:F

    #@4a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v6

    #@4e
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v6

    #@52
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    .line 161
    :cond_55
    iput v1, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mAspectRatio:F

    #@57
    .line 162
    invoke-direct {p0}, Landroid/filterpacks/ui/SurfaceTargetFilter;->updateTargetRect()V

    #@5a
    .line 166
    :cond_5a
    const/4 v2, 0x0

    #@5b
    .line 167
    .local v2, gpuFrame:Landroid/filterfw/core/Frame;
    iget-boolean v5, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mLogVerbose:Z

    #@5d
    if-eqz v5, :cond_7b

    #@5f
    const-string v5, "SurfaceRenderFilter"

    #@61
    new-instance v6, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    const-string v7, "Got input format: "

    #@68
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v6

    #@6c
    invoke-virtual {v3}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@6f
    move-result-object v7

    #@70
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v6

    #@74
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v6

    #@78
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7b
    .line 168
    :cond_7b
    invoke-virtual {v3}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@7e
    move-result-object v5

    #@7f
    invoke-virtual {v5}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@82
    move-result v4

    #@83
    .line 169
    .local v4, target:I
    if-eq v4, v8, :cond_a7

    #@85
    .line 170
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@88
    move-result-object v5

    #@89
    invoke-virtual {v5, v3, v8}, Landroid/filterfw/core/FrameManager;->duplicateFrameToTarget(Landroid/filterfw/core/Frame;I)Landroid/filterfw/core/Frame;

    #@8c
    move-result-object v2

    #@8d
    .line 172
    const/4 v0, 0x1

    #@8e
    .line 178
    :goto_8e
    iget-object v5, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mGlEnv:Landroid/filterfw/core/GLEnvironment;

    #@90
    iget v6, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mSurfaceId:I

    #@92
    invoke-virtual {v5, v6}, Landroid/filterfw/core/GLEnvironment;->activateSurfaceWithId(I)V

    #@95
    .line 181
    iget-object v5, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@97
    iget-object v6, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mScreen:Landroid/filterfw/core/GLFrame;

    #@99
    invoke-virtual {v5, v2, v6}, Landroid/filterfw/core/ShaderProgram;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@9c
    .line 184
    iget-object v5, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mGlEnv:Landroid/filterfw/core/GLEnvironment;

    #@9e
    invoke-virtual {v5}, Landroid/filterfw/core/GLEnvironment;->swapBuffers()V

    #@a1
    .line 186
    if-eqz v0, :cond_a6

    #@a3
    .line 187
    invoke-virtual {v2}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@a6
    .line 189
    :cond_a6
    return-void

    #@a7
    .line 174
    :cond_a7
    move-object v2, v3

    #@a8
    goto :goto_8e
.end method

.method public setupPorts()V
    .registers 3

    #@0
    .prologue
    .line 99
    iget-object v0, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mSurface:Landroid/view/Surface;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 100
    new-instance v0, Ljava/lang/RuntimeException;

    #@6
    const-string v1, "NULL Surface passed to SurfaceTargetFilter"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 104
    :cond_c
    const-string v0, "frame"

    #@e
    const/4 v1, 0x3

    #@f
    invoke-static {v1}, Landroid/filterfw/format/ImageFormat;->create(I)Landroid/filterfw/core/MutableFrameFormat;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/ui/SurfaceTargetFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@16
    .line 105
    return-void
.end method

.method public tearDown(Landroid/filterfw/core/FilterContext;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 204
    iget-object v0, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mScreen:Landroid/filterfw/core/GLFrame;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 205
    iget-object v0, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mScreen:Landroid/filterfw/core/GLFrame;

    #@6
    invoke-virtual {v0}, Landroid/filterfw/core/GLFrame;->release()Landroid/filterfw/core/Frame;

    #@9
    .line 207
    :cond_9
    return-void
.end method

.method public updateRenderMode()V
    .registers 4

    #@0
    .prologue
    .line 108
    iget-object v0, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mRenderModeString:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_12

    #@4
    .line 109
    iget-object v0, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mRenderModeString:Ljava/lang/String;

    #@6
    const-string/jumbo v1, "stretch"

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_16

    #@f
    .line 110
    const/4 v0, 0x0

    #@10
    iput v0, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mRenderMode:I

    #@12
    .line 119
    :cond_12
    :goto_12
    invoke-direct {p0}, Landroid/filterpacks/ui/SurfaceTargetFilter;->updateTargetRect()V

    #@15
    .line 120
    return-void

    #@16
    .line 111
    :cond_16
    iget-object v0, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mRenderModeString:Ljava/lang/String;

    #@18
    const-string v1, "fit"

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v0

    #@1e
    if-eqz v0, :cond_24

    #@20
    .line 112
    const/4 v0, 0x1

    #@21
    iput v0, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mRenderMode:I

    #@23
    goto :goto_12

    #@24
    .line 113
    :cond_24
    iget-object v0, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mRenderModeString:Ljava/lang/String;

    #@26
    const-string v1, "fill_crop"

    #@28
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v0

    #@2c
    if-eqz v0, :cond_32

    #@2e
    .line 114
    const/4 v0, 0x2

    #@2f
    iput v0, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mRenderMode:I

    #@31
    goto :goto_12

    #@32
    .line 116
    :cond_32
    new-instance v0, Ljava/lang/RuntimeException;

    #@34
    new-instance v1, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v2, "Unknown render mode \'"

    #@3b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v1

    #@3f
    iget-object v2, p0, Landroid/filterpacks/ui/SurfaceTargetFilter;->mRenderModeString:Ljava/lang/String;

    #@41
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v1

    #@45
    const-string v2, "\'!"

    #@47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v1

    #@4b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v1

    #@4f
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@52
    throw v0
.end method
