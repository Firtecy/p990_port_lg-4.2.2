.class public Landroid/filterpacks/ui/SurfaceRenderFilter;
.super Landroid/filterfw/core/Filter;
.source "SurfaceRenderFilter.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# static fields
.field private static final TAG:Ljava/lang/String; = "SurfaceRenderFilter"


# instance fields
.field private final RENDERMODE_FILL_CROP:I

.field private final RENDERMODE_FIT:I

.field private final RENDERMODE_STRETCH:I

.field private mAspectRatio:F

.field private mIsBound:Z

.field private mLogVerbose:Z

.field private mProgram:Landroid/filterfw/core/ShaderProgram;

.field private mRenderMode:I

.field private mRenderModeString:Ljava/lang/String;
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "renderMode"
    .end annotation
.end field

.field private mScreen:Landroid/filterfw/core/GLFrame;

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mSurfaceView:Landroid/filterfw/core/FilterSurfaceView;
    .annotation runtime Landroid/filterfw/core/GenerateFinalPort;
        name = "surfaceView"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 5
    .parameter "name"

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    const/4 v1, 0x1

    #@2
    const/4 v0, 0x0

    #@3
    .line 85
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@6
    .line 50
    iput v0, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->RENDERMODE_STRETCH:I

    #@8
    .line 51
    iput v1, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->RENDERMODE_FIT:I

    #@a
    .line 52
    iput v2, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->RENDERMODE_FILL_CROP:I

    #@c
    .line 71
    iput-boolean v0, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mIsBound:Z

    #@e
    .line 75
    iput v1, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mRenderMode:I

    #@10
    .line 76
    const/high16 v0, 0x3f80

    #@12
    iput v0, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mAspectRatio:F

    #@14
    .line 87
    const-string v0, "SurfaceRenderFilter"

    #@16
    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@19
    move-result v0

    #@1a
    iput-boolean v0, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mLogVerbose:Z

    #@1c
    .line 88
    return-void
.end method

.method private updateTargetRect()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/high16 v4, 0x3f00

    #@3
    const/high16 v5, 0x3f80

    #@5
    .line 242
    iget v2, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mScreenWidth:I

    #@7
    if-lez v2, :cond_22

    #@9
    iget v2, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mScreenHeight:I

    #@b
    if-lez v2, :cond_22

    #@d
    iget-object v2, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@f
    if-eqz v2, :cond_22

    #@11
    .line 243
    iget v2, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mScreenWidth:I

    #@13
    int-to-float v2, v2

    #@14
    iget v3, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mScreenHeight:I

    #@16
    int-to-float v3, v3

    #@17
    div-float v1, v2, v3

    #@19
    .line 244
    .local v1, screenAspectRatio:F
    iget v2, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mAspectRatio:F

    #@1b
    div-float v0, v1, v2

    #@1d
    .line 246
    .local v0, relativeAspectRatio:F
    iget v2, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mRenderMode:I

    #@1f
    packed-switch v2, :pswitch_data_5e

    #@22
    .line 274
    .end local v0           #relativeAspectRatio:F
    .end local v1           #screenAspectRatio:F
    :cond_22
    :goto_22
    return-void

    #@23
    .line 248
    .restart local v0       #relativeAspectRatio:F
    .restart local v1       #screenAspectRatio:F
    :pswitch_23
    iget-object v2, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@25
    invoke-virtual {v2, v6, v6, v5, v5}, Landroid/filterfw/core/ShaderProgram;->setTargetRect(FFFF)V

    #@28
    goto :goto_22

    #@29
    .line 251
    :pswitch_29
    cmpl-float v2, v0, v5

    #@2b
    if-lez v2, :cond_39

    #@2d
    .line 253
    iget-object v2, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@2f
    div-float v3, v4, v0

    #@31
    sub-float v3, v4, v3

    #@33
    div-float v4, v5, v0

    #@35
    invoke-virtual {v2, v3, v6, v4, v5}, Landroid/filterfw/core/ShaderProgram;->setTargetRect(FFFF)V

    #@38
    goto :goto_22

    #@39
    .line 257
    :cond_39
    iget-object v2, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@3b
    mul-float v3, v4, v0

    #@3d
    sub-float v3, v4, v3

    #@3f
    invoke-virtual {v2, v6, v3, v5, v0}, Landroid/filterfw/core/ShaderProgram;->setTargetRect(FFFF)V

    #@42
    goto :goto_22

    #@43
    .line 262
    :pswitch_43
    cmpl-float v2, v0, v5

    #@45
    if-lez v2, :cond_51

    #@47
    .line 264
    iget-object v2, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@49
    mul-float v3, v4, v0

    #@4b
    sub-float v3, v4, v3

    #@4d
    invoke-virtual {v2, v6, v3, v5, v0}, Landroid/filterfw/core/ShaderProgram;->setTargetRect(FFFF)V

    #@50
    goto :goto_22

    #@51
    .line 268
    :cond_51
    iget-object v2, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@53
    div-float v3, v4, v0

    #@55
    sub-float v3, v4, v3

    #@57
    div-float v4, v5, v0

    #@59
    invoke-virtual {v2, v3, v6, v4, v5}, Landroid/filterfw/core/ShaderProgram;->setTargetRect(FFFF)V

    #@5c
    goto :goto_22

    #@5d
    .line 246
    nop

    #@5e
    :pswitch_data_5e
    .packed-switch 0x0
        :pswitch_23
        :pswitch_29
        :pswitch_43
    .end packed-switch
.end method


# virtual methods
.method public close(Landroid/filterfw/core/FilterContext;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 206
    iget-object v0, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mSurfaceView:Landroid/filterfw/core/FilterSurfaceView;

    #@2
    invoke-virtual {v0}, Landroid/filterfw/core/FilterSurfaceView;->unbind()V

    #@5
    .line 207
    return-void
.end method

.method public fieldPortValueUpdated(Ljava/lang/String;Landroid/filterfw/core/FilterContext;)V
    .registers 3
    .parameter "name"
    .parameter "context"

    #@0
    .prologue
    .line 201
    invoke-direct {p0}, Landroid/filterpacks/ui/SurfaceRenderFilter;->updateTargetRect()V

    #@3
    .line 202
    return-void
.end method

.method public open(Landroid/filterfw/core/FilterContext;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 141
    iget-object v0, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mSurfaceView:Landroid/filterfw/core/FilterSurfaceView;

    #@2
    invoke-virtual {v0}, Landroid/filterfw/core/FilterSurfaceView;->unbind()V

    #@5
    .line 142
    iget-object v0, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mSurfaceView:Landroid/filterfw/core/FilterSurfaceView;

    #@7
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getGLEnvironment()Landroid/filterfw/core/GLEnvironment;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v0, p0, v1}, Landroid/filterfw/core/FilterSurfaceView;->bindToListener(Landroid/view/SurfaceHolder$Callback;Landroid/filterfw/core/GLEnvironment;)V

    #@e
    .line 143
    return-void
.end method

.method public prepare(Landroid/filterfw/core/FilterContext;)V
    .registers 8
    .parameter "context"

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/high16 v4, 0x3f80

    #@3
    const/4 v3, 0x0

    #@4
    .line 120
    invoke-static {p1}, Landroid/filterfw/core/ShaderProgram;->createIdentity(Landroid/filterfw/core/FilterContext;)Landroid/filterfw/core/ShaderProgram;

    #@7
    move-result-object v1

    #@8
    iput-object v1, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@a
    .line 121
    iget-object v1, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@c
    const/high16 v2, -0x4080

    #@e
    invoke-virtual {v1, v3, v4, v4, v2}, Landroid/filterfw/core/ShaderProgram;->setSourceRect(FFFF)V

    #@11
    .line 122
    iget-object v1, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@13
    const/4 v2, 0x1

    #@14
    invoke-virtual {v1, v2}, Landroid/filterfw/core/ShaderProgram;->setClearsOutput(Z)V

    #@17
    .line 123
    iget-object v1, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@19
    invoke-virtual {v1, v3, v3, v3}, Landroid/filterfw/core/ShaderProgram;->setClearColor(FFF)V

    #@1c
    .line 125
    invoke-virtual {p0}, Landroid/filterpacks/ui/SurfaceRenderFilter;->updateRenderMode()V

    #@1f
    .line 128
    iget-object v1, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mSurfaceView:Landroid/filterfw/core/FilterSurfaceView;

    #@21
    invoke-virtual {v1}, Landroid/filterfw/core/FilterSurfaceView;->getWidth()I

    #@24
    move-result v1

    #@25
    iget-object v2, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mSurfaceView:Landroid/filterfw/core/FilterSurfaceView;

    #@27
    invoke-virtual {v2}, Landroid/filterfw/core/FilterSurfaceView;->getHeight()I

    #@2a
    move-result v2

    #@2b
    invoke-static {v1, v2, v5, v5}, Landroid/filterfw/format/ImageFormat;->create(IIII)Landroid/filterfw/core/MutableFrameFormat;

    #@2e
    move-result-object v0

    #@2f
    .line 132
    .local v0, screenFormat:Landroid/filterfw/core/MutableFrameFormat;
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@32
    move-result-object v1

    #@33
    const/16 v2, 0x65

    #@35
    const-wide/16 v3, 0x0

    #@37
    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/filterfw/core/FrameManager;->newBoundFrame(Landroid/filterfw/core/FrameFormat;IJ)Landroid/filterfw/core/Frame;

    #@3a
    move-result-object v1

    #@3b
    check-cast v1, Landroid/filterfw/core/GLFrame;

    #@3d
    iput-object v1, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mScreen:Landroid/filterfw/core/GLFrame;

    #@3f
    .line 135
    return-void
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 12
    .parameter "context"

    #@0
    .prologue
    const/4 v9, 0x3

    #@1
    .line 148
    iget-boolean v6, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mIsBound:Z

    #@3
    if-nez v6, :cond_1e

    #@5
    .line 149
    const-string v6, "SurfaceRenderFilter"

    #@7
    new-instance v7, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v7

    #@10
    const-string v8, ": Ignoring frame as there is no surface to render to!"

    #@12
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v7

    #@16
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v7

    #@1a
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 197
    :cond_1d
    :goto_1d
    return-void

    #@1e
    .line 154
    :cond_1e
    iget-boolean v6, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mLogVerbose:Z

    #@20
    if-eqz v6, :cond_29

    #@22
    const-string v6, "SurfaceRenderFilter"

    #@24
    const-string v7, "Starting frame processing"

    #@26
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 156
    :cond_29
    iget-object v6, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mSurfaceView:Landroid/filterfw/core/FilterSurfaceView;

    #@2b
    invoke-virtual {v6}, Landroid/filterfw/core/FilterSurfaceView;->getGLEnv()Landroid/filterfw/core/GLEnvironment;

    #@2e
    move-result-object v2

    #@2f
    .line 157
    .local v2, glEnv:Landroid/filterfw/core/GLEnvironment;
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getGLEnvironment()Landroid/filterfw/core/GLEnvironment;

    #@32
    move-result-object v6

    #@33
    if-eq v2, v6, :cond_3d

    #@35
    .line 158
    new-instance v6, Ljava/lang/RuntimeException;

    #@37
    const-string v7, "Surface created under different GLEnvironment!"

    #@39
    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@3c
    throw v6

    #@3d
    .line 163
    :cond_3d
    const-string v6, "frame"

    #@3f
    invoke-virtual {p0, v6}, Landroid/filterpacks/ui/SurfaceRenderFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@42
    move-result-object v4

    #@43
    .line 164
    .local v4, input:Landroid/filterfw/core/Frame;
    const/4 v0, 0x0

    #@44
    .line 166
    .local v0, createdFrame:Z
    invoke-virtual {v4}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@47
    move-result-object v6

    #@48
    invoke-virtual {v6}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@4b
    move-result v6

    #@4c
    int-to-float v6, v6

    #@4d
    invoke-virtual {v4}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@50
    move-result-object v7

    #@51
    invoke-virtual {v7}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@54
    move-result v7

    #@55
    int-to-float v7, v7

    #@56
    div-float v1, v6, v7

    #@58
    .line 167
    .local v1, currentAspectRatio:F
    iget v6, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mAspectRatio:F

    #@5a
    cmpl-float v6, v1, v6

    #@5c
    if-eqz v6, :cond_8b

    #@5e
    .line 168
    iget-boolean v6, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mLogVerbose:Z

    #@60
    if-eqz v6, :cond_86

    #@62
    const-string v6, "SurfaceRenderFilter"

    #@64
    new-instance v7, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v8, "New aspect ratio: "

    #@6b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v7

    #@6f
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@72
    move-result-object v7

    #@73
    const-string v8, ", previously: "

    #@75
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v7

    #@79
    iget v8, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mAspectRatio:F

    #@7b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v7

    #@7f
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v7

    #@83
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@86
    .line 169
    :cond_86
    iput v1, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mAspectRatio:F

    #@88
    .line 170
    invoke-direct {p0}, Landroid/filterpacks/ui/SurfaceRenderFilter;->updateTargetRect()V

    #@8b
    .line 174
    :cond_8b
    const/4 v3, 0x0

    #@8c
    .line 175
    .local v3, gpuFrame:Landroid/filterfw/core/Frame;
    iget-boolean v6, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mLogVerbose:Z

    #@8e
    if-eqz v6, :cond_ac

    #@90
    const-string v6, "SurfaceRenderFilter"

    #@92
    new-instance v7, Ljava/lang/StringBuilder;

    #@94
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@97
    const-string v8, "Got input format: "

    #@99
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v7

    #@9d
    invoke-virtual {v4}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@a0
    move-result-object v8

    #@a1
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v7

    #@a5
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a8
    move-result-object v7

    #@a9
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@ac
    .line 176
    :cond_ac
    invoke-virtual {v4}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@af
    move-result-object v6

    #@b0
    invoke-virtual {v6}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@b3
    move-result v5

    #@b4
    .line 177
    .local v5, target:I
    if-eq v5, v9, :cond_d9

    #@b6
    .line 178
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@b9
    move-result-object v6

    #@ba
    invoke-virtual {v6, v4, v9}, Landroid/filterfw/core/FrameManager;->duplicateFrameToTarget(Landroid/filterfw/core/Frame;I)Landroid/filterfw/core/Frame;

    #@bd
    move-result-object v3

    #@be
    .line 180
    const/4 v0, 0x1

    #@bf
    .line 186
    :goto_bf
    iget-object v6, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mSurfaceView:Landroid/filterfw/core/FilterSurfaceView;

    #@c1
    invoke-virtual {v6}, Landroid/filterfw/core/FilterSurfaceView;->getSurfaceId()I

    #@c4
    move-result v6

    #@c5
    invoke-virtual {v2, v6}, Landroid/filterfw/core/GLEnvironment;->activateSurfaceWithId(I)V

    #@c8
    .line 189
    iget-object v6, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mProgram:Landroid/filterfw/core/ShaderProgram;

    #@ca
    iget-object v7, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mScreen:Landroid/filterfw/core/GLFrame;

    #@cc
    invoke-virtual {v6, v3, v7}, Landroid/filterfw/core/ShaderProgram;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@cf
    .line 192
    invoke-virtual {v2}, Landroid/filterfw/core/GLEnvironment;->swapBuffers()V

    #@d2
    .line 194
    if-eqz v0, :cond_1d

    #@d4
    .line 195
    invoke-virtual {v3}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@d7
    goto/16 :goto_1d

    #@d9
    .line 182
    :cond_d9
    move-object v3, v4

    #@da
    goto :goto_bf
.end method

.method public setupPorts()V
    .registers 3

    #@0
    .prologue
    .line 93
    iget-object v0, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mSurfaceView:Landroid/filterfw/core/FilterSurfaceView;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 94
    new-instance v0, Ljava/lang/RuntimeException;

    #@6
    const-string v1, "NULL SurfaceView passed to SurfaceRenderFilter"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 98
    :cond_c
    const-string v0, "frame"

    #@e
    const/4 v1, 0x3

    #@f
    invoke-static {v1}, Landroid/filterfw/format/ImageFormat;->create(I)Landroid/filterfw/core/MutableFrameFormat;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {p0, v0, v1}, Landroid/filterpacks/ui/SurfaceRenderFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@16
    .line 99
    return-void
.end method

.method public declared-synchronized surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .registers 10
    .parameter "holder"
    .parameter "format"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 228
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mScreen:Landroid/filterfw/core/GLFrame;

    #@3
    if-eqz v0, :cond_17

    #@5
    .line 229
    iput p3, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mScreenWidth:I

    #@7
    .line 230
    iput p4, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mScreenHeight:I

    #@9
    .line 231
    iget-object v0, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mScreen:Landroid/filterfw/core/GLFrame;

    #@b
    const/4 v1, 0x0

    #@c
    const/4 v2, 0x0

    #@d
    iget v3, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mScreenWidth:I

    #@f
    iget v4, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mScreenHeight:I

    #@11
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/filterfw/core/GLFrame;->setViewport(IIII)V

    #@14
    .line 232
    invoke-direct {p0}, Landroid/filterpacks/ui/SurfaceRenderFilter;->updateTargetRect()V
    :try_end_17
    .catchall {:try_start_1 .. :try_end_17} :catchall_19

    #@17
    .line 234
    :cond_17
    monitor-exit p0

    #@18
    return-void

    #@19
    .line 228
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit p0

    #@1b
    throw v0
.end method

.method public declared-synchronized surfaceCreated(Landroid/view/SurfaceHolder;)V
    .registers 3
    .parameter "holder"

    #@0
    .prologue
    .line 218
    monitor-enter p0

    #@1
    const/4 v0, 0x1

    #@2
    :try_start_2
    iput-boolean v0, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mIsBound:Z
    :try_end_4
    .catchall {:try_start_2 .. :try_end_4} :catchall_6

    #@4
    .line 219
    monitor-exit p0

    #@5
    return-void

    #@6
    .line 218
    :catchall_6
    move-exception v0

    #@7
    monitor-exit p0

    #@8
    throw v0
.end method

.method public declared-synchronized surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .registers 3
    .parameter "holder"

    #@0
    .prologue
    .line 238
    monitor-enter p0

    #@1
    const/4 v0, 0x0

    #@2
    :try_start_2
    iput-boolean v0, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mIsBound:Z
    :try_end_4
    .catchall {:try_start_2 .. :try_end_4} :catchall_6

    #@4
    .line 239
    monitor-exit p0

    #@5
    return-void

    #@6
    .line 238
    :catchall_6
    move-exception v0

    #@7
    monitor-exit p0

    #@8
    throw v0
.end method

.method public tearDown(Landroid/filterfw/core/FilterContext;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 211
    iget-object v0, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mScreen:Landroid/filterfw/core/GLFrame;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 212
    iget-object v0, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mScreen:Landroid/filterfw/core/GLFrame;

    #@6
    invoke-virtual {v0}, Landroid/filterfw/core/GLFrame;->release()Landroid/filterfw/core/Frame;

    #@9
    .line 214
    :cond_9
    return-void
.end method

.method public updateRenderMode()V
    .registers 4

    #@0
    .prologue
    .line 102
    iget-object v0, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mRenderModeString:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_12

    #@4
    .line 103
    iget-object v0, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mRenderModeString:Ljava/lang/String;

    #@6
    const-string/jumbo v1, "stretch"

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_16

    #@f
    .line 104
    const/4 v0, 0x0

    #@10
    iput v0, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mRenderMode:I

    #@12
    .line 113
    :cond_12
    :goto_12
    invoke-direct {p0}, Landroid/filterpacks/ui/SurfaceRenderFilter;->updateTargetRect()V

    #@15
    .line 114
    return-void

    #@16
    .line 105
    :cond_16
    iget-object v0, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mRenderModeString:Ljava/lang/String;

    #@18
    const-string v1, "fit"

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v0

    #@1e
    if-eqz v0, :cond_24

    #@20
    .line 106
    const/4 v0, 0x1

    #@21
    iput v0, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mRenderMode:I

    #@23
    goto :goto_12

    #@24
    .line 107
    :cond_24
    iget-object v0, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mRenderModeString:Ljava/lang/String;

    #@26
    const-string v1, "fill_crop"

    #@28
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v0

    #@2c
    if-eqz v0, :cond_32

    #@2e
    .line 108
    const/4 v0, 0x2

    #@2f
    iput v0, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mRenderMode:I

    #@31
    goto :goto_12

    #@32
    .line 110
    :cond_32
    new-instance v0, Ljava/lang/RuntimeException;

    #@34
    new-instance v1, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v2, "Unknown render mode \'"

    #@3b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v1

    #@3f
    iget-object v2, p0, Landroid/filterpacks/ui/SurfaceRenderFilter;->mRenderModeString:Ljava/lang/String;

    #@41
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v1

    #@45
    const-string v2, "\'!"

    #@47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v1

    #@4b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v1

    #@4f
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@52
    throw v0
.end method
