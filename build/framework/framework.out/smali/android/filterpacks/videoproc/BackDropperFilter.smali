.class public Landroid/filterpacks/videoproc/BackDropperFilter;
.super Landroid/filterfw/core/Filter;
.source "BackDropperFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/filterpacks/videoproc/BackDropperFilter$LearningDoneListener;
    }
.end annotation


# static fields
.field private static final DEFAULT_ACCEPT_STDDEV:F = 0.85f

.field private static final DEFAULT_ADAPT_RATE_BG:F = 0.0f

.field private static final DEFAULT_ADAPT_RATE_FG:F = 0.0f

.field private static final DEFAULT_AUTO_WB_SCALE:Ljava/lang/String; = "0.25"

.field private static final DEFAULT_BG_FIT_TRANSFORM:[F = null

.field private static final DEFAULT_EXPOSURE_CHANGE:F = 1.0f

.field private static final DEFAULT_HIER_LRG_EXPONENT:I = 0x3

.field private static final DEFAULT_HIER_LRG_SCALE:F = 0.7f

.field private static final DEFAULT_HIER_MID_EXPONENT:I = 0x2

.field private static final DEFAULT_HIER_MID_SCALE:F = 0.6f

.field private static final DEFAULT_HIER_SML_EXPONENT:I = 0x0

.field private static final DEFAULT_HIER_SML_SCALE:F = 0.5f

.field private static final DEFAULT_LEARNING_ADAPT_RATE:F = 0.2f

.field private static final DEFAULT_LEARNING_DONE_THRESHOLD:I = 0x14

.field private static final DEFAULT_LEARNING_DURATION:I = 0x28

.field private static final DEFAULT_LEARNING_VERIFY_DURATION:I = 0xa

.field private static final DEFAULT_MASK_BLEND_BG:F = 0.65f

.field private static final DEFAULT_MASK_BLEND_FG:F = 0.95f

.field private static final DEFAULT_MASK_HEIGHT_EXPONENT:I = 0x8

.field private static final DEFAULT_MASK_VERIFY_RATE:F = 0.25f

.field private static final DEFAULT_MASK_WIDTH_EXPONENT:I = 0x8

.field private static final DEFAULT_UV_SCALE_FACTOR:F = 1.35f

.field private static final DEFAULT_WHITE_BALANCE_BLUE_CHANGE:F = 0.0f

.field private static final DEFAULT_WHITE_BALANCE_RED_CHANGE:F = 0.0f

.field private static final DEFAULT_WHITE_BALANCE_TOGGLE:I = 0x0

.field private static final DEFAULT_Y_SCALE_FACTOR:F = 0.4f

.field private static final DISTANCE_STORAGE_SCALE:Ljava/lang/String; = "0.6"

.field private static final MASK_SMOOTH_EXPONENT:Ljava/lang/String; = "2.0"

.field private static final MIN_VARIANCE:Ljava/lang/String; = "3.0"

.field private static final RGB_TO_YUV_MATRIX:Ljava/lang/String; = "0.299, -0.168736,  0.5,      0.000, 0.587, -0.331264, -0.418688, 0.000, 0.114,  0.5,      -0.081312, 0.000, 0.000,  0.5,       0.5,      1.000 "

.field private static final TAG:Ljava/lang/String; = "BackDropperFilter"

.field private static final VARIANCE_STORAGE_SCALE:Ljava/lang/String; = "5.0"

.field private static final mAutomaticWhiteBalance:Ljava/lang/String; = "uniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform float pyramid_depth;\nuniform bool autowb_toggle;\nvarying vec2 v_texcoord;\nvoid main() {\n   vec4 mean_video = texture2D(tex_sampler_0, v_texcoord, pyramid_depth);\n   vec4 mean_bg = texture2D(tex_sampler_1, v_texcoord, pyramid_depth);\n   float green_normalizer = mean_video.g / mean_bg.g;\n   vec4 adjusted_value = vec4(mean_bg.r / mean_video.r * green_normalizer, 1., \n                         mean_bg.b / mean_video.b * green_normalizer, 1.) * auto_wb_scale; \n   gl_FragColor = autowb_toggle ? adjusted_value : vec4(auto_wb_scale);\n}\n"

.field private static final mBgDistanceShader:Ljava/lang/String; = "uniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform sampler2D tex_sampler_2;\nuniform float subsample_level;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 fg_rgb = texture2D(tex_sampler_0, v_texcoord, subsample_level);\n  vec4 fg = coeff_yuv * vec4(fg_rgb.rgb, 1.);\n  vec4 mean = texture2D(tex_sampler_1, v_texcoord);\n  vec4 variance = inv_var_scale * texture2D(tex_sampler_2, v_texcoord);\n\n  float dist_y = gauss_dist_y(fg.r, mean.r, variance.r);\n  float dist_uv = gauss_dist_uv(fg.gb, mean.gb, variance.gb);\n  gl_FragColor = vec4(0.5*fg.rg, dist_scale*dist_y, dist_scale*dist_uv);\n}\n"

.field private static final mBgMaskShader:Ljava/lang/String; = "uniform sampler2D tex_sampler_0;\nuniform float accept_variance;\nuniform vec2 yuv_weights;\nuniform float scale_lrg;\nuniform float scale_mid;\nuniform float scale_sml;\nuniform float exp_lrg;\nuniform float exp_mid;\nuniform float exp_sml;\nvarying vec2 v_texcoord;\nbool is_fg(vec2 dist_yc, float accept_variance) {\n  return ( dot(yuv_weights, dist_yc) >= accept_variance );\n}\nvoid main() {\n  vec4 dist_lrg_sc = texture2D(tex_sampler_0, v_texcoord, exp_lrg);\n  vec4 dist_mid_sc = texture2D(tex_sampler_0, v_texcoord, exp_mid);\n  vec4 dist_sml_sc = texture2D(tex_sampler_0, v_texcoord, exp_sml);\n  vec2 dist_lrg = inv_dist_scale * dist_lrg_sc.ba;\n  vec2 dist_mid = inv_dist_scale * dist_mid_sc.ba;\n  vec2 dist_sml = inv_dist_scale * dist_sml_sc.ba;\n  vec2 norm_dist = 0.75 * dist_sml / accept_variance;\n  bool is_fg_lrg = is_fg(dist_lrg, accept_variance * scale_lrg);\n  bool is_fg_mid = is_fg_lrg || is_fg(dist_mid, accept_variance * scale_mid);\n  float is_fg_sml =\n      float(is_fg_mid || is_fg(dist_sml, accept_variance * scale_sml));\n  float alpha = 0.5 * is_fg_sml + 0.3 * float(is_fg_mid) + 0.2 * float(is_fg_lrg);\n  gl_FragColor = vec4(alpha, norm_dist, is_fg_sml);\n}\n"

.field private static final mBgSubtractForceShader:Ljava/lang/String; = "  vec4 ghost_rgb = (fg_adjusted * 0.7 + vec4(0.3,0.3,0.4,0.))*0.65 + \n                   0.35*bg_rgb;\n  float glow_start = 0.75 * mask_blend_bg; \n  float glow_max   = mask_blend_bg; \n  gl_FragColor = mask.a < glow_start ? bg_rgb : \n                 mask.a < glow_max ? mix(bg_rgb, vec4(0.9,0.9,1.0,1.0), \n                                     (mask.a - glow_start) / (glow_max - glow_start) ) : \n                 mask.a < mask_blend_fg ? mix(vec4(0.9,0.9,1.0,1.0), ghost_rgb, \n                                    (mask.a - glow_max) / (mask_blend_fg - glow_max) ) : \n                 ghost_rgb;\n}\n"

.field private static final mBgSubtractShader:Ljava/lang/String; = "uniform mat3 bg_fit_transform;\nuniform float mask_blend_bg;\nuniform float mask_blend_fg;\nuniform float exposure_change;\nuniform float whitebalancered_change;\nuniform float whitebalanceblue_change;\nuniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform sampler2D tex_sampler_2;\nuniform sampler2D tex_sampler_3;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec2 bg_texcoord = (bg_fit_transform * vec3(v_texcoord, 1.)).xy;\n  vec4 bg_rgb = texture2D(tex_sampler_1, bg_texcoord);\n  vec4 wb_auto_scale = texture2D(tex_sampler_3, v_texcoord) * exposure_change / auto_wb_scale;\n  vec4 wb_manual_scale = vec4(1. + whitebalancered_change, 1., 1. + whitebalanceblue_change, 1.);\n  vec4 fg_rgb = texture2D(tex_sampler_0, v_texcoord);\n  vec4 fg_adjusted = fg_rgb * wb_manual_scale * wb_auto_scale;\n  vec4 mask = texture2D(tex_sampler_2, v_texcoord, \n                      2.0);\n  float alpha = smoothstep(mask_blend_bg, mask_blend_fg, mask.a);\n  gl_FragColor = mix(bg_rgb, fg_adjusted, alpha);\n"

.field private static final mDebugOutputNames:[Ljava/lang/String; = null

.field private static final mInputNames:[Ljava/lang/String; = null

.field private static final mMaskVerifyShader:Ljava/lang/String; = "uniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform float verify_rate;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 lastmask = texture2D(tex_sampler_0, v_texcoord);\n  vec4 mask = texture2D(tex_sampler_1, v_texcoord);\n  float newmask = mix(lastmask.a, mask.a, verify_rate);\n  gl_FragColor = vec4(0., 0., 0., newmask);\n}\n"

.field private static final mOutputNames:[Ljava/lang/String; = null

.field private static mSharedUtilShader:Ljava/lang/String; = null

.field private static final mUpdateBgModelMeanShader:Ljava/lang/String; = "uniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform sampler2D tex_sampler_2;\nuniform float subsample_level;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 fg_rgb = texture2D(tex_sampler_0, v_texcoord, subsample_level);\n  vec4 fg = coeff_yuv * vec4(fg_rgb.rgb, 1.);\n  vec4 mean = texture2D(tex_sampler_1, v_texcoord);\n  vec4 mask = texture2D(tex_sampler_2, v_texcoord, \n                      2.0);\n\n  float alpha = local_adapt_rate(mask.a);\n  vec4 new_mean = mix(mean, fg, alpha);\n  gl_FragColor = new_mean;\n}\n"

.field private static final mUpdateBgModelVarianceShader:Ljava/lang/String; = "uniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform sampler2D tex_sampler_2;\nuniform sampler2D tex_sampler_3;\nuniform float subsample_level;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 fg_rgb = texture2D(tex_sampler_0, v_texcoord, subsample_level);\n  vec4 fg = coeff_yuv * vec4(fg_rgb.rgb, 1.);\n  vec4 mean = texture2D(tex_sampler_1, v_texcoord);\n  vec4 variance = inv_var_scale * texture2D(tex_sampler_2, v_texcoord);\n  vec4 mask = texture2D(tex_sampler_3, v_texcoord, \n                      2.0);\n\n  float alpha = local_adapt_rate(mask.a);\n  vec4 cur_variance = (fg-mean)*(fg-mean);\n  vec4 new_variance = mix(variance, cur_variance, alpha);\n  new_variance = max(new_variance, vec4(min_variance));\n  gl_FragColor = var_scale * new_variance;\n}\n"


# instance fields
.field private final BACKGROUND_FILL_CROP:I

.field private final BACKGROUND_FIT:I

.field private final BACKGROUND_STRETCH:I

.field private copyShaderProgram:Landroid/filterfw/core/ShaderProgram;

.field private isOpen:Z

.field private mAcceptStddev:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "acceptStddev"
    .end annotation
.end field

.field private mAdaptRateBg:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "adaptRateBg"
    .end annotation
.end field

.field private mAdaptRateFg:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "adaptRateFg"
    .end annotation
.end field

.field private mAdaptRateLearning:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "learningAdaptRate"
    .end annotation
.end field

.field private mAutoWB:Landroid/filterfw/core/GLFrame;

.field private mAutoWBToggle:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "autowbToggle"
    .end annotation
.end field

.field private mAutomaticWhiteBalanceProgram:Landroid/filterfw/core/ShaderProgram;

.field private mAverageFormat:Landroid/filterfw/core/MutableFrameFormat;

.field private mBackgroundFitMode:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "backgroundFitMode"
    .end annotation
.end field

.field private mBackgroundFitModeChanged:Z

.field private mBgDistProgram:Landroid/filterfw/core/ShaderProgram;

.field private mBgInput:Landroid/filterfw/core/GLFrame;

.field private mBgMaskProgram:Landroid/filterfw/core/ShaderProgram;

.field private mBgMean:[Landroid/filterfw/core/GLFrame;

.field private mBgSubtractProgram:Landroid/filterfw/core/ShaderProgram;

.field private mBgUpdateMeanProgram:Landroid/filterfw/core/ShaderProgram;

.field private mBgUpdateVarianceProgram:Landroid/filterfw/core/ShaderProgram;

.field private mBgVariance:[Landroid/filterfw/core/GLFrame;

.field private mChromaScale:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "chromaScale"
    .end annotation
.end field

.field private mCopyOutProgram:Landroid/filterfw/core/ShaderProgram;

.field private mDistance:Landroid/filterfw/core/GLFrame;

.field private mExposureChange:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "exposureChange"
    .end annotation
.end field

.field private mFrameCount:I

.field private mHierarchyLrgExp:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "hierLrgExp"
    .end annotation
.end field

.field private mHierarchyLrgScale:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "hierLrgScale"
    .end annotation
.end field

.field private mHierarchyMidExp:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "hierMidExp"
    .end annotation
.end field

.field private mHierarchyMidScale:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "hierMidScale"
    .end annotation
.end field

.field private mHierarchySmlExp:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "hierSmlExp"
    .end annotation
.end field

.field private mHierarchySmlScale:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "hierSmlScale"
    .end annotation
.end field

.field private mLearningDoneListener:Landroid/filterpacks/videoproc/BackDropperFilter$LearningDoneListener;
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "learningDoneListener"
    .end annotation
.end field

.field private mLearningDuration:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "learningDuration"
    .end annotation
.end field

.field private mLearningVerifyDuration:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "learningVerifyDuration"
    .end annotation
.end field

.field private final mLogVerbose:Z

.field private mLumScale:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "lumScale"
    .end annotation
.end field

.field private mMask:Landroid/filterfw/core/GLFrame;

.field private mMaskAverage:Landroid/filterfw/core/GLFrame;

.field private mMaskBg:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "maskBg"
    .end annotation
.end field

.field private mMaskFg:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "maskFg"
    .end annotation
.end field

.field private mMaskFormat:Landroid/filterfw/core/MutableFrameFormat;

.field private mMaskHeightExp:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "maskHeightExp"
    .end annotation
.end field

.field private mMaskVerify:[Landroid/filterfw/core/GLFrame;

.field private mMaskVerifyProgram:Landroid/filterfw/core/ShaderProgram;

.field private mMaskWidthExp:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "maskWidthExp"
    .end annotation
.end field

.field private mMemoryFormat:Landroid/filterfw/core/MutableFrameFormat;

.field private mMirrorBg:Z
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "mirrorBg"
    .end annotation
.end field

.field private mOrientation:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "orientation"
    .end annotation
.end field

.field private mOutputFormat:Landroid/filterfw/core/FrameFormat;

.field private mPingPong:Z

.field private mProvideDebugOutputs:Z
    .annotation runtime Landroid/filterfw/core/GenerateFinalPort;
        hasDefault = true
        name = "provideDebugOutputs"
    .end annotation
.end field

.field private mPyramidDepth:I

.field private mRelativeAspect:F

.field private mStartLearning:Z

.field private mSubsampleLevel:I

.field private mUseTheForce:Z
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "useTheForce"
    .end annotation
.end field

.field private mVerifyRate:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "maskVerifyRate"
    .end annotation
.end field

.field private mVideoInput:Landroid/filterfw/core/GLFrame;

.field private mWhiteBalanceBlueChange:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "whitebalanceblueChange"
    .end annotation
.end field

.field private mWhiteBalanceRedChange:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "whitebalanceredChange"
    .end annotation
.end field

.field private startTime:J


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 180
    const/16 v0, 0x9

    #@5
    new-array v0, v0, [F

    #@7
    fill-array-data v0, :array_34

    #@a
    sput-object v0, Landroid/filterpacks/videoproc/BackDropperFilter;->DEFAULT_BG_FIT_TRANSFORM:[F

    #@c
    .line 205
    new-array v0, v4, [Ljava/lang/String;

    #@e
    const-string/jumbo v1, "video"

    #@11
    aput-object v1, v0, v2

    #@13
    const-string v1, "background"

    #@15
    aput-object v1, v0, v3

    #@17
    sput-object v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mInputNames:[Ljava/lang/String;

    #@19
    .line 208
    new-array v0, v3, [Ljava/lang/String;

    #@1b
    const-string/jumbo v1, "video"

    #@1e
    aput-object v1, v0, v2

    #@20
    sput-object v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mOutputNames:[Ljava/lang/String;

    #@22
    .line 210
    new-array v0, v4, [Ljava/lang/String;

    #@24
    const-string v1, "debug1"

    #@26
    aput-object v1, v0, v2

    #@28
    const-string v1, "debug2"

    #@2a
    aput-object v1, v0, v3

    #@2c
    sput-object v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mDebugOutputNames:[Ljava/lang/String;

    #@2e
    .line 226
    const-string/jumbo v0, "precision mediump float;\nuniform float fg_adapt_rate;\nuniform float bg_adapt_rate;\nconst mat4 coeff_yuv = mat4(0.299, -0.168736,  0.5,      0.000, 0.587, -0.331264, -0.418688, 0.000, 0.114,  0.5,      -0.081312, 0.000, 0.000,  0.5,       0.5,      1.000 );\nconst float dist_scale = 0.6;\nconst float inv_dist_scale = 1. / dist_scale;\nconst float var_scale=5.0;\nconst float inv_var_scale = 1. / var_scale;\nconst float min_variance = inv_var_scale *3.0/ 256.;\nconst float auto_wb_scale = 0.25;\n\nfloat gauss_dist_y(float y, float mean, float variance) {\n  float dist = (y - mean) * (y - mean) / variance;\n  return dist;\n}\nfloat gauss_dist_uv(vec2 uv, vec2 mean, vec2 variance) {\n  vec2 dist = (uv - mean) * (uv - mean) / variance;\n  return dist.r + dist.g;\n}\nfloat local_adapt_rate(float alpha) {\n  return mix(bg_adapt_rate, fg_adapt_rate, alpha);\n}\n\n"

    #@31
    sput-object v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mSharedUtilShader:Ljava/lang/String;

    #@33
    return-void

    #@34
    .line 180
    :array_34
    .array-data 0x4
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 9
    .parameter "name"

    #@0
    .prologue
    const/16 v6, 0x8

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x0

    #@4
    const/4 v3, 0x0

    #@5
    .line 511
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    #@8
    .line 46
    iput v3, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->BACKGROUND_STRETCH:I

    #@a
    .line 47
    const/4 v2, 0x1

    #@b
    iput v2, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->BACKGROUND_FIT:I

    #@d
    .line 48
    iput v5, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->BACKGROUND_FILL_CROP:I

    #@f
    .line 50
    iput v5, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBackgroundFitMode:I

    #@11
    .line 52
    const/16 v2, 0x28

    #@13
    iput v2, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLearningDuration:I

    #@15
    .line 54
    const/16 v2, 0xa

    #@17
    iput v2, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLearningVerifyDuration:I

    #@19
    .line 56
    const v2, 0x3f59999a

    #@1c
    iput v2, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAcceptStddev:F

    #@1e
    .line 58
    const v2, 0x3f333333

    #@21
    iput v2, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mHierarchyLrgScale:F

    #@23
    .line 60
    const v2, 0x3f19999a

    #@26
    iput v2, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mHierarchyMidScale:F

    #@28
    .line 62
    const/high16 v2, 0x3f00

    #@2a
    iput v2, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mHierarchySmlScale:F

    #@2c
    .line 67
    iput v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskWidthExp:I

    #@2e
    .line 69
    iput v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskHeightExp:I

    #@30
    .line 74
    const/4 v2, 0x3

    #@31
    iput v2, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mHierarchyLrgExp:I

    #@33
    .line 76
    iput v5, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mHierarchyMidExp:I

    #@35
    .line 78
    iput v3, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mHierarchySmlExp:I

    #@37
    .line 81
    const v2, 0x3ecccccd

    #@3a
    iput v2, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLumScale:F

    #@3c
    .line 83
    const v2, 0x3faccccd

    #@3f
    iput v2, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mChromaScale:F

    #@41
    .line 85
    const v2, 0x3f266666

    #@44
    iput v2, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskBg:F

    #@46
    .line 87
    const v2, 0x3f733333

    #@49
    iput v2, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskFg:F

    #@4b
    .line 89
    const/high16 v2, 0x3f80

    #@4d
    iput v2, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mExposureChange:F

    #@4f
    .line 91
    iput v4, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mWhiteBalanceRedChange:F

    #@51
    .line 93
    iput v4, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mWhiteBalanceBlueChange:F

    #@53
    .line 95
    iput v3, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAutoWBToggle:I

    #@55
    .line 99
    const v2, 0x3e4ccccd

    #@58
    iput v2, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAdaptRateLearning:F

    #@5a
    .line 101
    iput v4, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAdaptRateBg:F

    #@5c
    .line 103
    iput v4, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAdaptRateFg:F

    #@5e
    .line 105
    const/high16 v2, 0x3e80

    #@60
    iput v2, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mVerifyRate:F

    #@62
    .line 107
    const/4 v2, 0x0

    #@63
    iput-object v2, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLearningDoneListener:Landroid/filterpacks/videoproc/BackDropperFilter$LearningDoneListener;

    #@65
    .line 110
    iput-boolean v3, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mUseTheForce:Z

    #@67
    .line 113
    iput-boolean v3, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mProvideDebugOutputs:Z

    #@69
    .line 118
    iput-boolean v3, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMirrorBg:Z

    #@6b
    .line 123
    iput v3, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mOrientation:I

    #@6d
    .line 862
    const-wide/16 v2, -0x1

    #@6f
    iput-wide v2, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->startTime:J

    #@71
    .line 513
    const-string v2, "BackDropperFilter"

    #@73
    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@76
    move-result v2

    #@77
    iput-boolean v2, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLogVerbose:Z

    #@79
    .line 515
    const-string/jumbo v2, "ro.media.effect.bgdropper.adj"

    #@7c
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@7f
    move-result-object v0

    #@80
    .line 516
    .local v0, adjStr:Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@83
    move-result v2

    #@84
    if-lez v2, :cond_b7

    #@86
    .line 518
    :try_start_86
    iget v2, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAcceptStddev:F

    #@88
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    #@8b
    move-result v3

    #@8c
    add-float/2addr v2, v3

    #@8d
    iput v2, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAcceptStddev:F

    #@8f
    .line 519
    iget-boolean v2, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLogVerbose:Z

    #@91
    if-eqz v2, :cond_b7

    #@93
    .line 520
    const-string v2, "BackDropperFilter"

    #@95
    new-instance v3, Ljava/lang/StringBuilder;

    #@97
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@9a
    const-string v4, "Adjusting accept threshold by "

    #@9c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v3

    #@a0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v3

    #@a4
    const-string v4, ", now "

    #@a6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v3

    #@aa
    iget v4, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAcceptStddev:F

    #@ac
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@af
    move-result-object v3

    #@b0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b3
    move-result-object v3

    #@b4
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b7
    .catch Ljava/lang/NumberFormatException; {:try_start_86 .. :try_end_b7} :catch_b8

    #@b7
    .line 528
    :cond_b7
    :goto_b7
    return-void

    #@b8
    .line 523
    :catch_b8
    move-exception v1

    #@b9
    .line 524
    .local v1, e:Ljava/lang/NumberFormatException;
    const-string v2, "BackDropperFilter"

    #@bb
    new-instance v3, Ljava/lang/StringBuilder;

    #@bd
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@c0
    const-string v4, "Badly formatted property ro.media.effect.bgdropper.adj: "

    #@c2
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v3

    #@c6
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v3

    #@ca
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cd
    move-result-object v3

    #@ce
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d1
    goto :goto_b7
.end method

.method private allocateFrames(Landroid/filterfw/core/FrameFormat;Landroid/filterfw/core/FilterContext;)V
    .registers 16
    .parameter "inputFormat"
    .parameter "context"

    #@0
    .prologue
    const/4 v12, 0x2

    #@1
    const/4 v11, 0x1

    #@2
    const/4 v10, 0x0

    #@3
    .line 610
    invoke-direct {p0, p1}, Landroid/filterpacks/videoproc/BackDropperFilter;->createMemoryFormat(Landroid/filterfw/core/FrameFormat;)Z

    #@6
    move-result v6

    #@7
    if-nez v6, :cond_a

    #@9
    .line 697
    :goto_9
    return-void

    #@a
    .line 613
    :cond_a
    iget-boolean v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLogVerbose:Z

    #@c
    if-eqz v6, :cond_15

    #@e
    const-string v6, "BackDropperFilter"

    #@10
    const-string v7, "Allocating BackDropperFilter frames"

    #@12
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 616
    :cond_15
    iget-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@17
    invoke-virtual {v6}, Landroid/filterfw/core/MutableFrameFormat;->getSize()I

    #@1a
    move-result v4

    #@1b
    .line 617
    .local v4, numBytes:I
    new-array v1, v4, [B

    #@1d
    .line 618
    .local v1, initialBgMean:[B
    new-array v2, v4, [B

    #@1f
    .line 619
    .local v2, initialBgVariance:[B
    new-array v3, v4, [B

    #@21
    .line 620
    .local v3, initialMaskVerify:[B
    const/4 v0, 0x0

    #@22
    .local v0, i:I
    :goto_22
    if-ge v0, v4, :cond_31

    #@24
    .line 621
    const/16 v6, -0x80

    #@26
    aput-byte v6, v1, v0

    #@28
    .line 622
    const/16 v6, 0xa

    #@2a
    aput-byte v6, v2, v0

    #@2c
    .line 623
    aput-byte v10, v3, v0

    #@2e
    .line 620
    add-int/lit8 v0, v0, 0x1

    #@30
    goto :goto_22

    #@31
    .line 627
    :cond_31
    const/4 v0, 0x0

    #@32
    :goto_32
    if-ge v0, v12, :cond_7c

    #@34
    .line 628
    iget-object v7, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgMean:[Landroid/filterfw/core/GLFrame;

    #@36
    invoke-virtual {p2}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@39
    move-result-object v6

    #@3a
    iget-object v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@3c
    invoke-virtual {v6, v8}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@3f
    move-result-object v6

    #@40
    check-cast v6, Landroid/filterfw/core/GLFrame;

    #@42
    aput-object v6, v7, v0

    #@44
    .line 629
    iget-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgMean:[Landroid/filterfw/core/GLFrame;

    #@46
    aget-object v6, v6, v0

    #@48
    invoke-virtual {v6, v1, v10, v4}, Landroid/filterfw/core/GLFrame;->setData([BII)V

    #@4b
    .line 631
    iget-object v7, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgVariance:[Landroid/filterfw/core/GLFrame;

    #@4d
    invoke-virtual {p2}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@50
    move-result-object v6

    #@51
    iget-object v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@53
    invoke-virtual {v6, v8}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@56
    move-result-object v6

    #@57
    check-cast v6, Landroid/filterfw/core/GLFrame;

    #@59
    aput-object v6, v7, v0

    #@5b
    .line 632
    iget-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgVariance:[Landroid/filterfw/core/GLFrame;

    #@5d
    aget-object v6, v6, v0

    #@5f
    invoke-virtual {v6, v2, v10, v4}, Landroid/filterfw/core/GLFrame;->setData([BII)V

    #@62
    .line 634
    iget-object v7, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskVerify:[Landroid/filterfw/core/GLFrame;

    #@64
    invoke-virtual {p2}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@67
    move-result-object v6

    #@68
    iget-object v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@6a
    invoke-virtual {v6, v8}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@6d
    move-result-object v6

    #@6e
    check-cast v6, Landroid/filterfw/core/GLFrame;

    #@70
    aput-object v6, v7, v0

    #@72
    .line 635
    iget-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskVerify:[Landroid/filterfw/core/GLFrame;

    #@74
    aget-object v6, v6, v0

    #@76
    invoke-virtual {v6, v3, v10, v4}, Landroid/filterfw/core/GLFrame;->setData([BII)V

    #@79
    .line 627
    add-int/lit8 v0, v0, 0x1

    #@7b
    goto :goto_32

    #@7c
    .line 639
    :cond_7c
    iget-boolean v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLogVerbose:Z

    #@7e
    if-eqz v6, :cond_87

    #@80
    const-string v6, "BackDropperFilter"

    #@82
    const-string v7, "Done allocating texture for Mean and Variance objects!"

    #@84
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@87
    .line 641
    :cond_87
    invoke-virtual {p2}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@8a
    move-result-object v6

    #@8b
    iget-object v7, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@8d
    invoke-virtual {v6, v7}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@90
    move-result-object v6

    #@91
    check-cast v6, Landroid/filterfw/core/GLFrame;

    #@93
    iput-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mDistance:Landroid/filterfw/core/GLFrame;

    #@95
    .line 642
    invoke-virtual {p2}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@98
    move-result-object v6

    #@99
    iget-object v7, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@9b
    invoke-virtual {v6, v7}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@9e
    move-result-object v6

    #@9f
    check-cast v6, Landroid/filterfw/core/GLFrame;

    #@a1
    iput-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMask:Landroid/filterfw/core/GLFrame;

    #@a3
    .line 643
    invoke-virtual {p2}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@a6
    move-result-object v6

    #@a7
    iget-object v7, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAverageFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@a9
    invoke-virtual {v6, v7}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@ac
    move-result-object v6

    #@ad
    check-cast v6, Landroid/filterfw/core/GLFrame;

    #@af
    iput-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAutoWB:Landroid/filterfw/core/GLFrame;

    #@b1
    .line 644
    invoke-virtual {p2}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@b4
    move-result-object v6

    #@b5
    iget-object v7, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMemoryFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@b7
    invoke-virtual {v6, v7}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@ba
    move-result-object v6

    #@bb
    check-cast v6, Landroid/filterfw/core/GLFrame;

    #@bd
    iput-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mVideoInput:Landroid/filterfw/core/GLFrame;

    #@bf
    .line 645
    invoke-virtual {p2}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@c2
    move-result-object v6

    #@c3
    iget-object v7, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMemoryFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@c5
    invoke-virtual {v6, v7}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@c8
    move-result-object v6

    #@c9
    check-cast v6, Landroid/filterfw/core/GLFrame;

    #@cb
    iput-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgInput:Landroid/filterfw/core/GLFrame;

    #@cd
    .line 646
    invoke-virtual {p2}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@d0
    move-result-object v6

    #@d1
    iget-object v7, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAverageFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@d3
    invoke-virtual {v6, v7}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@d6
    move-result-object v6

    #@d7
    check-cast v6, Landroid/filterfw/core/GLFrame;

    #@d9
    iput-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskAverage:Landroid/filterfw/core/GLFrame;

    #@db
    .line 649
    new-instance v6, Landroid/filterfw/core/ShaderProgram;

    #@dd
    new-instance v7, Ljava/lang/StringBuilder;

    #@df
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@e2
    sget-object v8, Landroid/filterpacks/videoproc/BackDropperFilter;->mSharedUtilShader:Ljava/lang/String;

    #@e4
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v7

    #@e8
    const-string/jumbo v8, "uniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform sampler2D tex_sampler_2;\nuniform float subsample_level;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 fg_rgb = texture2D(tex_sampler_0, v_texcoord, subsample_level);\n  vec4 fg = coeff_yuv * vec4(fg_rgb.rgb, 1.);\n  vec4 mean = texture2D(tex_sampler_1, v_texcoord);\n  vec4 variance = inv_var_scale * texture2D(tex_sampler_2, v_texcoord);\n\n  float dist_y = gauss_dist_y(fg.r, mean.r, variance.r);\n  float dist_uv = gauss_dist_uv(fg.gb, mean.gb, variance.gb);\n  gl_FragColor = vec4(0.5*fg.rg, dist_scale*dist_y, dist_scale*dist_uv);\n}\n"

    #@eb
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v7

    #@ef
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f2
    move-result-object v7

    #@f3
    invoke-direct {v6, p2, v7}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;)V

    #@f6
    iput-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgDistProgram:Landroid/filterfw/core/ShaderProgram;

    #@f8
    .line 650
    iget-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgDistProgram:Landroid/filterfw/core/ShaderProgram;

    #@fa
    const-string/jumbo v7, "subsample_level"

    #@fd
    iget v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mSubsampleLevel:I

    #@ff
    int-to-float v8, v8

    #@100
    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@103
    move-result-object v8

    #@104
    invoke-virtual {v6, v7, v8}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@107
    .line 652
    new-instance v6, Landroid/filterfw/core/ShaderProgram;

    #@109
    new-instance v7, Ljava/lang/StringBuilder;

    #@10b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@10e
    sget-object v8, Landroid/filterpacks/videoproc/BackDropperFilter;->mSharedUtilShader:Ljava/lang/String;

    #@110
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@113
    move-result-object v7

    #@114
    const-string/jumbo v8, "uniform sampler2D tex_sampler_0;\nuniform float accept_variance;\nuniform vec2 yuv_weights;\nuniform float scale_lrg;\nuniform float scale_mid;\nuniform float scale_sml;\nuniform float exp_lrg;\nuniform float exp_mid;\nuniform float exp_sml;\nvarying vec2 v_texcoord;\nbool is_fg(vec2 dist_yc, float accept_variance) {\n  return ( dot(yuv_weights, dist_yc) >= accept_variance );\n}\nvoid main() {\n  vec4 dist_lrg_sc = texture2D(tex_sampler_0, v_texcoord, exp_lrg);\n  vec4 dist_mid_sc = texture2D(tex_sampler_0, v_texcoord, exp_mid);\n  vec4 dist_sml_sc = texture2D(tex_sampler_0, v_texcoord, exp_sml);\n  vec2 dist_lrg = inv_dist_scale * dist_lrg_sc.ba;\n  vec2 dist_mid = inv_dist_scale * dist_mid_sc.ba;\n  vec2 dist_sml = inv_dist_scale * dist_sml_sc.ba;\n  vec2 norm_dist = 0.75 * dist_sml / accept_variance;\n  bool is_fg_lrg = is_fg(dist_lrg, accept_variance * scale_lrg);\n  bool is_fg_mid = is_fg_lrg || is_fg(dist_mid, accept_variance * scale_mid);\n  float is_fg_sml =\n      float(is_fg_mid || is_fg(dist_sml, accept_variance * scale_sml));\n  float alpha = 0.5 * is_fg_sml + 0.3 * float(is_fg_mid) + 0.2 * float(is_fg_lrg);\n  gl_FragColor = vec4(alpha, norm_dist, is_fg_sml);\n}\n"

    #@117
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11a
    move-result-object v7

    #@11b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11e
    move-result-object v7

    #@11f
    invoke-direct {v6, p2, v7}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;)V

    #@122
    iput-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgMaskProgram:Landroid/filterfw/core/ShaderProgram;

    #@124
    .line 653
    iget-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgMaskProgram:Landroid/filterfw/core/ShaderProgram;

    #@126
    const-string v7, "accept_variance"

    #@128
    iget v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAcceptStddev:F

    #@12a
    iget v9, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAcceptStddev:F

    #@12c
    mul-float/2addr v8, v9

    #@12d
    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@130
    move-result-object v8

    #@131
    invoke-virtual {v6, v7, v8}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@134
    .line 654
    new-array v5, v12, [F

    #@136
    iget v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLumScale:F

    #@138
    aput v6, v5, v10

    #@13a
    iget v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mChromaScale:F

    #@13c
    aput v6, v5, v11

    #@13e
    .line 655
    .local v5, yuvWeights:[F
    iget-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgMaskProgram:Landroid/filterfw/core/ShaderProgram;

    #@140
    const-string/jumbo v7, "yuv_weights"

    #@143
    invoke-virtual {v6, v7, v5}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@146
    .line 656
    iget-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgMaskProgram:Landroid/filterfw/core/ShaderProgram;

    #@148
    const-string/jumbo v7, "scale_lrg"

    #@14b
    iget v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mHierarchyLrgScale:F

    #@14d
    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@150
    move-result-object v8

    #@151
    invoke-virtual {v6, v7, v8}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@154
    .line 657
    iget-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgMaskProgram:Landroid/filterfw/core/ShaderProgram;

    #@156
    const-string/jumbo v7, "scale_mid"

    #@159
    iget v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mHierarchyMidScale:F

    #@15b
    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@15e
    move-result-object v8

    #@15f
    invoke-virtual {v6, v7, v8}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@162
    .line 658
    iget-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgMaskProgram:Landroid/filterfw/core/ShaderProgram;

    #@164
    const-string/jumbo v7, "scale_sml"

    #@167
    iget v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mHierarchySmlScale:F

    #@169
    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@16c
    move-result-object v8

    #@16d
    invoke-virtual {v6, v7, v8}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@170
    .line 659
    iget-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgMaskProgram:Landroid/filterfw/core/ShaderProgram;

    #@172
    const-string v7, "exp_lrg"

    #@174
    iget v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mSubsampleLevel:I

    #@176
    iget v9, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mHierarchyLrgExp:I

    #@178
    add-int/2addr v8, v9

    #@179
    int-to-float v8, v8

    #@17a
    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@17d
    move-result-object v8

    #@17e
    invoke-virtual {v6, v7, v8}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@181
    .line 660
    iget-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgMaskProgram:Landroid/filterfw/core/ShaderProgram;

    #@183
    const-string v7, "exp_mid"

    #@185
    iget v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mSubsampleLevel:I

    #@187
    iget v9, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mHierarchyMidExp:I

    #@189
    add-int/2addr v8, v9

    #@18a
    int-to-float v8, v8

    #@18b
    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@18e
    move-result-object v8

    #@18f
    invoke-virtual {v6, v7, v8}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@192
    .line 661
    iget-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgMaskProgram:Landroid/filterfw/core/ShaderProgram;

    #@194
    const-string v7, "exp_sml"

    #@196
    iget v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mSubsampleLevel:I

    #@198
    iget v9, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mHierarchySmlExp:I

    #@19a
    add-int/2addr v8, v9

    #@19b
    int-to-float v8, v8

    #@19c
    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@19f
    move-result-object v8

    #@1a0
    invoke-virtual {v6, v7, v8}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@1a3
    .line 663
    iget-boolean v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mUseTheForce:Z

    #@1a5
    if-eqz v6, :cond_306

    #@1a7
    .line 664
    new-instance v6, Landroid/filterfw/core/ShaderProgram;

    #@1a9
    new-instance v7, Ljava/lang/StringBuilder;

    #@1ab
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1ae
    sget-object v8, Landroid/filterpacks/videoproc/BackDropperFilter;->mSharedUtilShader:Ljava/lang/String;

    #@1b0
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b3
    move-result-object v7

    #@1b4
    const-string/jumbo v8, "uniform mat3 bg_fit_transform;\nuniform float mask_blend_bg;\nuniform float mask_blend_fg;\nuniform float exposure_change;\nuniform float whitebalancered_change;\nuniform float whitebalanceblue_change;\nuniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform sampler2D tex_sampler_2;\nuniform sampler2D tex_sampler_3;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec2 bg_texcoord = (bg_fit_transform * vec3(v_texcoord, 1.)).xy;\n  vec4 bg_rgb = texture2D(tex_sampler_1, bg_texcoord);\n  vec4 wb_auto_scale = texture2D(tex_sampler_3, v_texcoord) * exposure_change / auto_wb_scale;\n  vec4 wb_manual_scale = vec4(1. + whitebalancered_change, 1., 1. + whitebalanceblue_change, 1.);\n  vec4 fg_rgb = texture2D(tex_sampler_0, v_texcoord);\n  vec4 fg_adjusted = fg_rgb * wb_manual_scale * wb_auto_scale;\n  vec4 mask = texture2D(tex_sampler_2, v_texcoord, \n                      2.0);\n  float alpha = smoothstep(mask_blend_bg, mask_blend_fg, mask.a);\n  gl_FragColor = mix(bg_rgb, fg_adjusted, alpha);\n"

    #@1b7
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ba
    move-result-object v7

    #@1bb
    const-string v8, "  vec4 ghost_rgb = (fg_adjusted * 0.7 + vec4(0.3,0.3,0.4,0.))*0.65 + \n                   0.35*bg_rgb;\n  float glow_start = 0.75 * mask_blend_bg; \n  float glow_max   = mask_blend_bg; \n  gl_FragColor = mask.a < glow_start ? bg_rgb : \n                 mask.a < glow_max ? mix(bg_rgb, vec4(0.9,0.9,1.0,1.0), \n                                     (mask.a - glow_start) / (glow_max - glow_start) ) : \n                 mask.a < mask_blend_fg ? mix(vec4(0.9,0.9,1.0,1.0), ghost_rgb, \n                                    (mask.a - glow_max) / (mask_blend_fg - glow_max) ) : \n                 ghost_rgb;\n}\n"

    #@1bd
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c0
    move-result-object v7

    #@1c1
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c4
    move-result-object v7

    #@1c5
    invoke-direct {v6, p2, v7}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;)V

    #@1c8
    iput-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgSubtractProgram:Landroid/filterfw/core/ShaderProgram;

    #@1ca
    .line 668
    :goto_1ca
    iget-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgSubtractProgram:Landroid/filterfw/core/ShaderProgram;

    #@1cc
    const-string v7, "bg_fit_transform"

    #@1ce
    sget-object v8, Landroid/filterpacks/videoproc/BackDropperFilter;->DEFAULT_BG_FIT_TRANSFORM:[F

    #@1d0
    invoke-virtual {v6, v7, v8}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@1d3
    .line 669
    iget-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgSubtractProgram:Landroid/filterfw/core/ShaderProgram;

    #@1d5
    const-string/jumbo v7, "mask_blend_bg"

    #@1d8
    iget v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskBg:F

    #@1da
    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@1dd
    move-result-object v8

    #@1de
    invoke-virtual {v6, v7, v8}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@1e1
    .line 670
    iget-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgSubtractProgram:Landroid/filterfw/core/ShaderProgram;

    #@1e3
    const-string/jumbo v7, "mask_blend_fg"

    #@1e6
    iget v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskFg:F

    #@1e8
    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@1eb
    move-result-object v8

    #@1ec
    invoke-virtual {v6, v7, v8}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@1ef
    .line 671
    iget-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgSubtractProgram:Landroid/filterfw/core/ShaderProgram;

    #@1f1
    const-string v7, "exposure_change"

    #@1f3
    iget v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mExposureChange:F

    #@1f5
    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@1f8
    move-result-object v8

    #@1f9
    invoke-virtual {v6, v7, v8}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@1fc
    .line 672
    iget-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgSubtractProgram:Landroid/filterfw/core/ShaderProgram;

    #@1fe
    const-string/jumbo v7, "whitebalanceblue_change"

    #@201
    iget v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mWhiteBalanceBlueChange:F

    #@203
    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@206
    move-result-object v8

    #@207
    invoke-virtual {v6, v7, v8}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@20a
    .line 673
    iget-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgSubtractProgram:Landroid/filterfw/core/ShaderProgram;

    #@20c
    const-string/jumbo v7, "whitebalancered_change"

    #@20f
    iget v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mWhiteBalanceRedChange:F

    #@211
    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@214
    move-result-object v8

    #@215
    invoke-virtual {v6, v7, v8}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@218
    .line 676
    new-instance v6, Landroid/filterfw/core/ShaderProgram;

    #@21a
    new-instance v7, Ljava/lang/StringBuilder;

    #@21c
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@21f
    sget-object v8, Landroid/filterpacks/videoproc/BackDropperFilter;->mSharedUtilShader:Ljava/lang/String;

    #@221
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@224
    move-result-object v7

    #@225
    const-string/jumbo v8, "uniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform sampler2D tex_sampler_2;\nuniform float subsample_level;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 fg_rgb = texture2D(tex_sampler_0, v_texcoord, subsample_level);\n  vec4 fg = coeff_yuv * vec4(fg_rgb.rgb, 1.);\n  vec4 mean = texture2D(tex_sampler_1, v_texcoord);\n  vec4 mask = texture2D(tex_sampler_2, v_texcoord, \n                      2.0);\n\n  float alpha = local_adapt_rate(mask.a);\n  vec4 new_mean = mix(mean, fg, alpha);\n  gl_FragColor = new_mean;\n}\n"

    #@228
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22b
    move-result-object v7

    #@22c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22f
    move-result-object v7

    #@230
    invoke-direct {v6, p2, v7}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;)V

    #@233
    iput-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgUpdateMeanProgram:Landroid/filterfw/core/ShaderProgram;

    #@235
    .line 677
    iget-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgUpdateMeanProgram:Landroid/filterfw/core/ShaderProgram;

    #@237
    const-string/jumbo v7, "subsample_level"

    #@23a
    iget v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mSubsampleLevel:I

    #@23c
    int-to-float v8, v8

    #@23d
    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@240
    move-result-object v8

    #@241
    invoke-virtual {v6, v7, v8}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@244
    .line 679
    new-instance v6, Landroid/filterfw/core/ShaderProgram;

    #@246
    new-instance v7, Ljava/lang/StringBuilder;

    #@248
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@24b
    sget-object v8, Landroid/filterpacks/videoproc/BackDropperFilter;->mSharedUtilShader:Ljava/lang/String;

    #@24d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@250
    move-result-object v7

    #@251
    const-string/jumbo v8, "uniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform sampler2D tex_sampler_2;\nuniform sampler2D tex_sampler_3;\nuniform float subsample_level;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 fg_rgb = texture2D(tex_sampler_0, v_texcoord, subsample_level);\n  vec4 fg = coeff_yuv * vec4(fg_rgb.rgb, 1.);\n  vec4 mean = texture2D(tex_sampler_1, v_texcoord);\n  vec4 variance = inv_var_scale * texture2D(tex_sampler_2, v_texcoord);\n  vec4 mask = texture2D(tex_sampler_3, v_texcoord, \n                      2.0);\n\n  float alpha = local_adapt_rate(mask.a);\n  vec4 cur_variance = (fg-mean)*(fg-mean);\n  vec4 new_variance = mix(variance, cur_variance, alpha);\n  new_variance = max(new_variance, vec4(min_variance));\n  gl_FragColor = var_scale * new_variance;\n}\n"

    #@254
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@257
    move-result-object v7

    #@258
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25b
    move-result-object v7

    #@25c
    invoke-direct {v6, p2, v7}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;)V

    #@25f
    iput-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgUpdateVarianceProgram:Landroid/filterfw/core/ShaderProgram;

    #@261
    .line 680
    iget-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgUpdateVarianceProgram:Landroid/filterfw/core/ShaderProgram;

    #@263
    const-string/jumbo v7, "subsample_level"

    #@266
    iget v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mSubsampleLevel:I

    #@268
    int-to-float v8, v8

    #@269
    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@26c
    move-result-object v8

    #@26d
    invoke-virtual {v6, v7, v8}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@270
    .line 682
    invoke-static {p2}, Landroid/filterfw/core/ShaderProgram;->createIdentity(Landroid/filterfw/core/FilterContext;)Landroid/filterfw/core/ShaderProgram;

    #@273
    move-result-object v6

    #@274
    iput-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mCopyOutProgram:Landroid/filterfw/core/ShaderProgram;

    #@276
    .line 684
    new-instance v6, Landroid/filterfw/core/ShaderProgram;

    #@278
    new-instance v7, Ljava/lang/StringBuilder;

    #@27a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@27d
    sget-object v8, Landroid/filterpacks/videoproc/BackDropperFilter;->mSharedUtilShader:Ljava/lang/String;

    #@27f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@282
    move-result-object v7

    #@283
    const-string/jumbo v8, "uniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform float pyramid_depth;\nuniform bool autowb_toggle;\nvarying vec2 v_texcoord;\nvoid main() {\n   vec4 mean_video = texture2D(tex_sampler_0, v_texcoord, pyramid_depth);\n   vec4 mean_bg = texture2D(tex_sampler_1, v_texcoord, pyramid_depth);\n   float green_normalizer = mean_video.g / mean_bg.g;\n   vec4 adjusted_value = vec4(mean_bg.r / mean_video.r * green_normalizer, 1., \n                         mean_bg.b / mean_video.b * green_normalizer, 1.) * auto_wb_scale; \n   gl_FragColor = autowb_toggle ? adjusted_value : vec4(auto_wb_scale);\n}\n"

    #@286
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@289
    move-result-object v7

    #@28a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28d
    move-result-object v7

    #@28e
    invoke-direct {v6, p2, v7}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;)V

    #@291
    iput-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAutomaticWhiteBalanceProgram:Landroid/filterfw/core/ShaderProgram;

    #@293
    .line 685
    iget-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAutomaticWhiteBalanceProgram:Landroid/filterfw/core/ShaderProgram;

    #@295
    const-string/jumbo v7, "pyramid_depth"

    #@298
    iget v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mPyramidDepth:I

    #@29a
    int-to-float v8, v8

    #@29b
    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@29e
    move-result-object v8

    #@29f
    invoke-virtual {v6, v7, v8}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@2a2
    .line 686
    iget-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAutomaticWhiteBalanceProgram:Landroid/filterfw/core/ShaderProgram;

    #@2a4
    const-string v7, "autowb_toggle"

    #@2a6
    iget v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAutoWBToggle:I

    #@2a8
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2ab
    move-result-object v8

    #@2ac
    invoke-virtual {v6, v7, v8}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@2af
    .line 688
    new-instance v6, Landroid/filterfw/core/ShaderProgram;

    #@2b1
    new-instance v7, Ljava/lang/StringBuilder;

    #@2b3
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2b6
    sget-object v8, Landroid/filterpacks/videoproc/BackDropperFilter;->mSharedUtilShader:Ljava/lang/String;

    #@2b8
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2bb
    move-result-object v7

    #@2bc
    const-string/jumbo v8, "uniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform float verify_rate;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 lastmask = texture2D(tex_sampler_0, v_texcoord);\n  vec4 mask = texture2D(tex_sampler_1, v_texcoord);\n  float newmask = mix(lastmask.a, mask.a, verify_rate);\n  gl_FragColor = vec4(0., 0., 0., newmask);\n}\n"

    #@2bf
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c2
    move-result-object v7

    #@2c3
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c6
    move-result-object v7

    #@2c7
    invoke-direct {v6, p2, v7}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;)V

    #@2ca
    iput-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskVerifyProgram:Landroid/filterfw/core/ShaderProgram;

    #@2cc
    .line 689
    iget-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskVerifyProgram:Landroid/filterfw/core/ShaderProgram;

    #@2ce
    const-string/jumbo v7, "verify_rate"

    #@2d1
    iget v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mVerifyRate:F

    #@2d3
    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@2d6
    move-result-object v8

    #@2d7
    invoke-virtual {v6, v7, v8}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@2da
    .line 691
    iget-boolean v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLogVerbose:Z

    #@2dc
    if-eqz v6, :cond_2fc

    #@2de
    const-string v6, "BackDropperFilter"

    #@2e0
    new-instance v7, Ljava/lang/StringBuilder;

    #@2e2
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2e5
    const-string v8, "Shader width set to "

    #@2e7
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ea
    move-result-object v7

    #@2eb
    iget-object v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMemoryFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@2ed
    invoke-virtual {v8}, Landroid/filterfw/core/MutableFrameFormat;->getWidth()I

    #@2f0
    move-result v8

    #@2f1
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f4
    move-result-object v7

    #@2f5
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f8
    move-result-object v7

    #@2f9
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2fc
    .line 693
    :cond_2fc
    const/high16 v6, 0x3f80

    #@2fe
    iput v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mRelativeAspect:F

    #@300
    .line 695
    iput v10, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mFrameCount:I

    #@302
    .line 696
    iput-boolean v11, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mStartLearning:Z

    #@304
    goto/16 :goto_9

    #@306
    .line 666
    :cond_306
    new-instance v6, Landroid/filterfw/core/ShaderProgram;

    #@308
    new-instance v7, Ljava/lang/StringBuilder;

    #@30a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@30d
    sget-object v8, Landroid/filterpacks/videoproc/BackDropperFilter;->mSharedUtilShader:Ljava/lang/String;

    #@30f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@312
    move-result-object v7

    #@313
    const-string/jumbo v8, "uniform mat3 bg_fit_transform;\nuniform float mask_blend_bg;\nuniform float mask_blend_fg;\nuniform float exposure_change;\nuniform float whitebalancered_change;\nuniform float whitebalanceblue_change;\nuniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform sampler2D tex_sampler_2;\nuniform sampler2D tex_sampler_3;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec2 bg_texcoord = (bg_fit_transform * vec3(v_texcoord, 1.)).xy;\n  vec4 bg_rgb = texture2D(tex_sampler_1, bg_texcoord);\n  vec4 wb_auto_scale = texture2D(tex_sampler_3, v_texcoord) * exposure_change / auto_wb_scale;\n  vec4 wb_manual_scale = vec4(1. + whitebalancered_change, 1., 1. + whitebalanceblue_change, 1.);\n  vec4 fg_rgb = texture2D(tex_sampler_0, v_texcoord);\n  vec4 fg_adjusted = fg_rgb * wb_manual_scale * wb_auto_scale;\n  vec4 mask = texture2D(tex_sampler_2, v_texcoord, \n                      2.0);\n  float alpha = smoothstep(mask_blend_bg, mask_blend_fg, mask.a);\n  gl_FragColor = mix(bg_rgb, fg_adjusted, alpha);\n"

    #@316
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@319
    move-result-object v7

    #@31a
    const-string/jumbo v8, "}\n"

    #@31d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@320
    move-result-object v7

    #@321
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@324
    move-result-object v7

    #@325
    invoke-direct {v6, p2, v7}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;)V

    #@328
    iput-object v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgSubtractProgram:Landroid/filterfw/core/ShaderProgram;

    #@32a
    goto/16 :goto_1ca
.end method

.method private createMemoryFormat(Landroid/filterfw/core/FrameFormat;)Z
    .registers 13
    .parameter "inputFormat"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const-wide/high16 v9, 0x4000

    #@3
    .line 565
    iget-object v7, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMemoryFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@5
    if-eqz v7, :cond_9

    #@7
    .line 566
    const/4 v6, 0x0

    #@8
    .line 597
    :goto_8
    return v6

    #@9
    .line 569
    :cond_9
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@c
    move-result v7

    #@d
    if-eqz v7, :cond_15

    #@f
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@12
    move-result v7

    #@13
    if-nez v7, :cond_1d

    #@15
    .line 571
    :cond_15
    new-instance v6, Ljava/lang/RuntimeException;

    #@17
    const-string v7, "Attempting to process input frame with unknown size"

    #@19
    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1c
    throw v6

    #@1d
    .line 574
    :cond_1d
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->mutableCopy()Landroid/filterfw/core/MutableFrameFormat;

    #@20
    move-result-object v7

    #@21
    iput-object v7, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@23
    .line 575
    iget v7, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskWidthExp:I

    #@25
    int-to-double v7, v7

    #@26
    invoke-static {v9, v10, v7, v8}, Ljava/lang/Math;->pow(DD)D

    #@29
    move-result-wide v7

    #@2a
    double-to-int v2, v7

    #@2b
    .line 576
    .local v2, maskWidth:I
    iget v7, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskHeightExp:I

    #@2d
    int-to-double v7, v7

    #@2e
    invoke-static {v9, v10, v7, v8}, Ljava/lang/Math;->pow(DD)D

    #@31
    move-result-wide v7

    #@32
    double-to-int v1, v7

    #@33
    .line 577
    .local v1, maskHeight:I
    iget-object v7, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@35
    invoke-virtual {v7, v2, v1}, Landroid/filterfw/core/MutableFrameFormat;->setDimensions(II)V

    #@38
    .line 579
    iget v7, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskWidthExp:I

    #@3a
    iget v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskHeightExp:I

    #@3c
    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    #@3f
    move-result v7

    #@40
    iput v7, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mPyramidDepth:I

    #@42
    .line 580
    iget-object v7, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@44
    invoke-virtual {v7}, Landroid/filterfw/core/MutableFrameFormat;->mutableCopy()Landroid/filterfw/core/MutableFrameFormat;

    #@47
    move-result-object v7

    #@48
    iput-object v7, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMemoryFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@4a
    .line 581
    iget v7, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskWidthExp:I

    #@4c
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@4f
    move-result v8

    #@50
    invoke-direct {p0, v8}, Landroid/filterpacks/videoproc/BackDropperFilter;->pyramidLevel(I)I

    #@53
    move-result v8

    #@54
    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    #@57
    move-result v5

    #@58
    .line 582
    .local v5, widthExp:I
    iget v7, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskHeightExp:I

    #@5a
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@5d
    move-result v8

    #@5e
    invoke-direct {p0, v8}, Landroid/filterpacks/videoproc/BackDropperFilter;->pyramidLevel(I)I

    #@61
    move-result v8

    #@62
    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    #@65
    move-result v0

    #@66
    .line 583
    .local v0, heightExp:I
    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    #@69
    move-result v7

    #@6a
    iput v7, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mPyramidDepth:I

    #@6c
    .line 584
    int-to-double v7, v5

    #@6d
    invoke-static {v9, v10, v7, v8}, Ljava/lang/Math;->pow(DD)D

    #@70
    move-result-wide v7

    #@71
    double-to-int v7, v7

    #@72
    invoke-static {v2, v7}, Ljava/lang/Math;->max(II)I

    #@75
    move-result v4

    #@76
    .line 585
    .local v4, memWidth:I
    int-to-double v7, v0

    #@77
    invoke-static {v9, v10, v7, v8}, Ljava/lang/Math;->pow(DD)D

    #@7a
    move-result-wide v7

    #@7b
    double-to-int v7, v7

    #@7c
    invoke-static {v1, v7}, Ljava/lang/Math;->max(II)I

    #@7f
    move-result v3

    #@80
    .line 586
    .local v3, memHeight:I
    iget-object v7, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMemoryFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@82
    invoke-virtual {v7, v4, v3}, Landroid/filterfw/core/MutableFrameFormat;->setDimensions(II)V

    #@85
    .line 587
    iget v7, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mPyramidDepth:I

    #@87
    iget v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskWidthExp:I

    #@89
    iget v9, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskHeightExp:I

    #@8b
    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    #@8e
    move-result v8

    #@8f
    sub-int/2addr v7, v8

    #@90
    iput v7, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mSubsampleLevel:I

    #@92
    .line 589
    iget-boolean v7, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLogVerbose:Z

    #@94
    if-eqz v7, :cond_fc

    #@96
    .line 590
    const-string v7, "BackDropperFilter"

    #@98
    new-instance v8, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    const-string v9, "Mask frames size "

    #@9f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v8

    #@a3
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v8

    #@a7
    const-string v9, " x "

    #@a9
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v8

    #@ad
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v8

    #@b1
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b4
    move-result-object v8

    #@b5
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b8
    .line 591
    const-string v7, "BackDropperFilter"

    #@ba
    new-instance v8, Ljava/lang/StringBuilder;

    #@bc
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@bf
    const-string v9, "Pyramid levels "

    #@c1
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v8

    #@c5
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v8

    #@c9
    const-string v9, " x "

    #@cb
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v8

    #@cf
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v8

    #@d3
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d6
    move-result-object v8

    #@d7
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@da
    .line 592
    const-string v7, "BackDropperFilter"

    #@dc
    new-instance v8, Ljava/lang/StringBuilder;

    #@de
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@e1
    const-string v9, "Memory frames size "

    #@e3
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e6
    move-result-object v8

    #@e7
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v8

    #@eb
    const-string v9, " x "

    #@ed
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v8

    #@f1
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v8

    #@f5
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f8
    move-result-object v8

    #@f9
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@fc
    .line 595
    :cond_fc
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->mutableCopy()Landroid/filterfw/core/MutableFrameFormat;

    #@ff
    move-result-object v7

    #@100
    iput-object v7, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAverageFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@102
    .line 596
    iget-object v7, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAverageFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@104
    invoke-virtual {v7, v6, v6}, Landroid/filterfw/core/MutableFrameFormat;->setDimensions(II)V

    #@107
    goto/16 :goto_8
.end method

.method private pyramidLevel(I)I
    .registers 6
    .parameter "size"

    #@0
    .prologue
    .line 997
    int-to-double v0, p1

    #@1
    invoke-static {v0, v1}, Ljava/lang/Math;->log10(D)D

    #@4
    move-result-wide v0

    #@5
    const-wide/high16 v2, 0x4000

    #@7
    invoke-static {v2, v3}, Ljava/lang/Math;->log10(D)D

    #@a
    move-result-wide v2

    #@b
    div-double/2addr v0, v2

    #@c
    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    #@f
    move-result-wide v0

    #@10
    double-to-int v0, v0

    #@11
    add-int/lit8 v0, v0, -0x1

    #@13
    return v0
.end method

.method private updateBgScaling(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;Z)V
    .registers 15
    .parameter "video"
    .parameter "background"
    .parameter "fitModeChanged"

    #@0
    .prologue
    .line 929
    invoke-virtual {p1}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@3
    move-result-object v8

    #@4
    invoke-virtual {v8}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@7
    move-result v8

    #@8
    int-to-float v8, v8

    #@9
    invoke-virtual {p1}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@c
    move-result-object v9

    #@d
    invoke-virtual {v9}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@10
    move-result v9

    #@11
    int-to-float v9, v9

    #@12
    div-float v3, v8, v9

    #@14
    .line 930
    .local v3, foregroundAspect:F
    invoke-virtual {p2}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@17
    move-result-object v8

    #@18
    invoke-virtual {v8}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@1b
    move-result v8

    #@1c
    int-to-float v8, v8

    #@1d
    invoke-virtual {p2}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@20
    move-result-object v9

    #@21
    invoke-virtual {v9}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@24
    move-result v9

    #@25
    int-to-float v9, v9

    #@26
    div-float v0, v8, v9

    #@28
    .line 931
    .local v0, backgroundAspect:F
    div-float v2, v3, v0

    #@2a
    .line 932
    .local v2, currentRelativeAspect:F
    iget v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mRelativeAspect:F

    #@2c
    cmpl-float v8, v2, v8

    #@2e
    if-nez v8, :cond_32

    #@30
    if-eqz p3, :cond_d0

    #@32
    .line 933
    :cond_32
    iput v2, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mRelativeAspect:F

    #@34
    .line 934
    const/4 v4, 0x0

    #@35
    .local v4, xMin:F
    const/high16 v5, 0x3f80

    #@37
    .local v5, xWidth:F
    const/4 v6, 0x0

    #@38
    .local v6, yMin:F
    const/high16 v7, 0x3f80

    #@3a
    .line 935
    .local v7, yWidth:F
    iget v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBackgroundFitMode:I

    #@3c
    packed-switch v8, :pswitch_data_128

    #@3f
    .line 973
    :goto_3f
    :pswitch_3f
    iget-boolean v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMirrorBg:Z

    #@41
    if-eqz v8, :cond_5d

    #@43
    .line 974
    iget-boolean v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLogVerbose:Z

    #@45
    if-eqz v8, :cond_4e

    #@47
    const-string v8, "BackDropperFilter"

    #@49
    const-string v9, "Mirroring the background!"

    #@4b
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 976
    :cond_4e
    iget v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mOrientation:I

    #@50
    if-eqz v8, :cond_58

    #@52
    iget v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mOrientation:I

    #@54
    const/16 v9, 0xb4

    #@56
    if-ne v8, v9, :cond_121

    #@58
    .line 977
    :cond_58
    neg-float v5, v5

    #@59
    .line 978
    const/high16 v8, 0x3f80

    #@5b
    sub-float v4, v8, v4

    #@5d
    .line 985
    :cond_5d
    :goto_5d
    iget-boolean v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLogVerbose:Z

    #@5f
    if-eqz v8, :cond_a3

    #@61
    const-string v8, "BackDropperFilter"

    #@63
    new-instance v9, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string v10, "bgTransform: xMin, yMin, xWidth, yWidth : "

    #@6a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v9

    #@6e
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@71
    move-result-object v9

    #@72
    const-string v10, ", "

    #@74
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v9

    #@78
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v9

    #@7c
    const-string v10, ", "

    #@7e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v9

    #@82
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@85
    move-result-object v9

    #@86
    const-string v10, ", "

    #@88
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v9

    #@8c
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v9

    #@90
    const-string v10, ", mRelAspRatio = "

    #@92
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v9

    #@96
    iget v10, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mRelativeAspect:F

    #@98
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v9

    #@9c
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9f
    move-result-object v9

    #@a0
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@a3
    .line 989
    :cond_a3
    const/16 v8, 0x9

    #@a5
    new-array v1, v8, [F

    #@a7
    const/4 v8, 0x0

    #@a8
    aput v5, v1, v8

    #@aa
    const/4 v8, 0x1

    #@ab
    const/4 v9, 0x0

    #@ac
    aput v9, v1, v8

    #@ae
    const/4 v8, 0x2

    #@af
    const/4 v9, 0x0

    #@b0
    aput v9, v1, v8

    #@b2
    const/4 v8, 0x3

    #@b3
    const/4 v9, 0x0

    #@b4
    aput v9, v1, v8

    #@b6
    const/4 v8, 0x4

    #@b7
    aput v7, v1, v8

    #@b9
    const/4 v8, 0x5

    #@ba
    const/4 v9, 0x0

    #@bb
    aput v9, v1, v8

    #@bd
    const/4 v8, 0x6

    #@be
    aput v4, v1, v8

    #@c0
    const/4 v8, 0x7

    #@c1
    aput v6, v1, v8

    #@c3
    const/16 v8, 0x8

    #@c5
    const/high16 v9, 0x3f80

    #@c7
    aput v9, v1, v8

    #@c9
    .line 992
    .local v1, bgTransform:[F
    iget-object v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgSubtractProgram:Landroid/filterfw/core/ShaderProgram;

    #@cb
    const-string v9, "bg_fit_transform"

    #@cd
    invoke-virtual {v8, v9, v1}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@d0
    .line 994
    .end local v1           #bgTransform:[F
    .end local v4           #xMin:F
    .end local v5           #xWidth:F
    .end local v6           #yMin:F
    .end local v7           #yWidth:F
    :cond_d0
    return-void

    #@d1
    .line 940
    .restart local v4       #xMin:F
    .restart local v5       #xWidth:F
    .restart local v6       #yMin:F
    .restart local v7       #yWidth:F
    :pswitch_d1
    iget v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mRelativeAspect:F

    #@d3
    const/high16 v9, 0x3f80

    #@d5
    cmpl-float v8, v8, v9

    #@d7
    if-lez v8, :cond_ea

    #@d9
    .line 943
    const/high16 v8, 0x3f00

    #@db
    const/high16 v9, 0x3f00

    #@dd
    iget v10, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mRelativeAspect:F

    #@df
    mul-float/2addr v9, v10

    #@e0
    sub-float v4, v8, v9

    #@e2
    .line 944
    const/high16 v8, 0x3f80

    #@e4
    iget v9, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mRelativeAspect:F

    #@e6
    mul-float v5, v8, v9

    #@e8
    goto/16 :goto_3f

    #@ea
    .line 948
    :cond_ea
    const/high16 v8, 0x3f00

    #@ec
    const/high16 v9, 0x3f00

    #@ee
    iget v10, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mRelativeAspect:F

    #@f0
    div-float/2addr v9, v10

    #@f1
    sub-float v6, v8, v9

    #@f3
    .line 949
    const/high16 v8, 0x3f80

    #@f5
    iget v9, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mRelativeAspect:F

    #@f7
    div-float v7, v8, v9

    #@f9
    .line 951
    goto/16 :goto_3f

    #@fb
    .line 953
    :pswitch_fb
    iget v8, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mRelativeAspect:F

    #@fd
    const/high16 v9, 0x3f80

    #@ff
    cmpl-float v8, v8, v9

    #@101
    if-lez v8, :cond_114

    #@103
    .line 956
    const/high16 v8, 0x3f00

    #@105
    const/high16 v9, 0x3f00

    #@107
    iget v10, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mRelativeAspect:F

    #@109
    div-float/2addr v9, v10

    #@10a
    sub-float v6, v8, v9

    #@10c
    .line 957
    const/high16 v8, 0x3f80

    #@10e
    iget v9, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mRelativeAspect:F

    #@110
    div-float v7, v8, v9

    #@112
    goto/16 :goto_3f

    #@114
    .line 961
    :cond_114
    const/high16 v8, 0x3f00

    #@116
    const/high16 v9, 0x3f00

    #@118
    iget v10, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mRelativeAspect:F

    #@11a
    mul-float/2addr v9, v10

    #@11b
    sub-float v4, v8, v9

    #@11d
    .line 962
    iget v5, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mRelativeAspect:F

    #@11f
    goto/16 :goto_3f

    #@121
    .line 981
    :cond_121
    neg-float v7, v7

    #@122
    .line 982
    const/high16 v8, 0x3f80

    #@124
    sub-float v6, v8, v6

    #@126
    goto/16 :goto_5d

    #@128
    .line 935
    :pswitch_data_128
    .packed-switch 0x0
        :pswitch_3f
        :pswitch_d1
        :pswitch_fb
    .end packed-switch
.end method


# virtual methods
.method public close(Landroid/filterfw/core/FilterContext;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 865
    iget-object v1, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMemoryFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 883
    :goto_4
    return-void

    #@5
    .line 869
    :cond_5
    iget-boolean v1, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLogVerbose:Z

    #@7
    if-eqz v1, :cond_10

    #@9
    const-string v1, "BackDropperFilter"

    #@b
    const-string v2, "Filter Closing!"

    #@d
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 870
    :cond_10
    const/4 v0, 0x0

    #@11
    .local v0, i:I
    :goto_11
    const/4 v1, 0x2

    #@12
    if-ge v0, v1, :cond_2c

    #@14
    .line 871
    iget-object v1, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgMean:[Landroid/filterfw/core/GLFrame;

    #@16
    aget-object v1, v1, v0

    #@18
    invoke-virtual {v1}, Landroid/filterfw/core/GLFrame;->release()Landroid/filterfw/core/Frame;

    #@1b
    .line 872
    iget-object v1, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgVariance:[Landroid/filterfw/core/GLFrame;

    #@1d
    aget-object v1, v1, v0

    #@1f
    invoke-virtual {v1}, Landroid/filterfw/core/GLFrame;->release()Landroid/filterfw/core/Frame;

    #@22
    .line 873
    iget-object v1, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskVerify:[Landroid/filterfw/core/GLFrame;

    #@24
    aget-object v1, v1, v0

    #@26
    invoke-virtual {v1}, Landroid/filterfw/core/GLFrame;->release()Landroid/filterfw/core/Frame;

    #@29
    .line 870
    add-int/lit8 v0, v0, 0x1

    #@2b
    goto :goto_11

    #@2c
    .line 875
    :cond_2c
    iget-object v1, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mDistance:Landroid/filterfw/core/GLFrame;

    #@2e
    invoke-virtual {v1}, Landroid/filterfw/core/GLFrame;->release()Landroid/filterfw/core/Frame;

    #@31
    .line 876
    iget-object v1, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMask:Landroid/filterfw/core/GLFrame;

    #@33
    invoke-virtual {v1}, Landroid/filterfw/core/GLFrame;->release()Landroid/filterfw/core/Frame;

    #@36
    .line 877
    iget-object v1, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAutoWB:Landroid/filterfw/core/GLFrame;

    #@38
    invoke-virtual {v1}, Landroid/filterfw/core/GLFrame;->release()Landroid/filterfw/core/Frame;

    #@3b
    .line 878
    iget-object v1, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mVideoInput:Landroid/filterfw/core/GLFrame;

    #@3d
    invoke-virtual {v1}, Landroid/filterfw/core/GLFrame;->release()Landroid/filterfw/core/Frame;

    #@40
    .line 879
    iget-object v1, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgInput:Landroid/filterfw/core/GLFrame;

    #@42
    invoke-virtual {v1}, Landroid/filterfw/core/GLFrame;->release()Landroid/filterfw/core/Frame;

    #@45
    .line 880
    iget-object v1, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskAverage:Landroid/filterfw/core/GLFrame;

    #@47
    invoke-virtual {v1}, Landroid/filterfw/core/GLFrame;->release()Landroid/filterfw/core/Frame;

    #@4a
    .line 882
    const/4 v1, 0x0

    #@4b
    iput-object v1, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMemoryFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@4d
    goto :goto_4
.end method

.method public fieldPortValueUpdated(Ljava/lang/String;Landroid/filterfw/core/FilterContext;)V
    .registers 8
    .parameter "name"
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 894
    const-string v1, "backgroundFitMode"

    #@3
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_c

    #@9
    .line 895
    iput-boolean v3, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBackgroundFitModeChanged:Z

    #@b
    .line 926
    :cond_b
    :goto_b
    return-void

    #@c
    .line 896
    :cond_c
    const-string v1, "acceptStddev"

    #@e
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_25

    #@14
    .line 897
    iget-object v1, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgMaskProgram:Landroid/filterfw/core/ShaderProgram;

    #@16
    const-string v2, "accept_variance"

    #@18
    iget v3, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAcceptStddev:F

    #@1a
    iget v4, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAcceptStddev:F

    #@1c
    mul-float/2addr v3, v4

    #@1d
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v1, v2, v3}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@24
    goto :goto_b

    #@25
    .line 898
    :cond_25
    const-string v1, "hierLrgScale"

    #@27
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a
    move-result v1

    #@2b
    if-eqz v1, :cond_3c

    #@2d
    .line 899
    iget-object v1, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgMaskProgram:Landroid/filterfw/core/ShaderProgram;

    #@2f
    const-string/jumbo v2, "scale_lrg"

    #@32
    iget v3, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mHierarchyLrgScale:F

    #@34
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@37
    move-result-object v3

    #@38
    invoke-virtual {v1, v2, v3}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@3b
    goto :goto_b

    #@3c
    .line 900
    :cond_3c
    const-string v1, "hierMidScale"

    #@3e
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@41
    move-result v1

    #@42
    if-eqz v1, :cond_53

    #@44
    .line 901
    iget-object v1, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgMaskProgram:Landroid/filterfw/core/ShaderProgram;

    #@46
    const-string/jumbo v2, "scale_mid"

    #@49
    iget v3, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mHierarchyMidScale:F

    #@4b
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@4e
    move-result-object v3

    #@4f
    invoke-virtual {v1, v2, v3}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@52
    goto :goto_b

    #@53
    .line 902
    :cond_53
    const-string v1, "hierSmlScale"

    #@55
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@58
    move-result v1

    #@59
    if-eqz v1, :cond_6a

    #@5b
    .line 903
    iget-object v1, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgMaskProgram:Landroid/filterfw/core/ShaderProgram;

    #@5d
    const-string/jumbo v2, "scale_sml"

    #@60
    iget v3, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mHierarchySmlScale:F

    #@62
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@65
    move-result-object v3

    #@66
    invoke-virtual {v1, v2, v3}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@69
    goto :goto_b

    #@6a
    .line 904
    :cond_6a
    const-string v1, "hierLrgExp"

    #@6c
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6f
    move-result v1

    #@70
    if-eqz v1, :cond_84

    #@72
    .line 905
    iget-object v1, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgMaskProgram:Landroid/filterfw/core/ShaderProgram;

    #@74
    const-string v2, "exp_lrg"

    #@76
    iget v3, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mSubsampleLevel:I

    #@78
    iget v4, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mHierarchyLrgExp:I

    #@7a
    add-int/2addr v3, v4

    #@7b
    int-to-float v3, v3

    #@7c
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@7f
    move-result-object v3

    #@80
    invoke-virtual {v1, v2, v3}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@83
    goto :goto_b

    #@84
    .line 906
    :cond_84
    const-string v1, "hierMidExp"

    #@86
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@89
    move-result v1

    #@8a
    if-eqz v1, :cond_9f

    #@8c
    .line 907
    iget-object v1, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgMaskProgram:Landroid/filterfw/core/ShaderProgram;

    #@8e
    const-string v2, "exp_mid"

    #@90
    iget v3, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mSubsampleLevel:I

    #@92
    iget v4, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mHierarchyMidExp:I

    #@94
    add-int/2addr v3, v4

    #@95
    int-to-float v3, v3

    #@96
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@99
    move-result-object v3

    #@9a
    invoke-virtual {v1, v2, v3}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@9d
    goto/16 :goto_b

    #@9f
    .line 908
    :cond_9f
    const-string v1, "hierSmlExp"

    #@a1
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a4
    move-result v1

    #@a5
    if-eqz v1, :cond_ba

    #@a7
    .line 909
    iget-object v1, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgMaskProgram:Landroid/filterfw/core/ShaderProgram;

    #@a9
    const-string v2, "exp_sml"

    #@ab
    iget v3, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mSubsampleLevel:I

    #@ad
    iget v4, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mHierarchySmlExp:I

    #@af
    add-int/2addr v3, v4

    #@b0
    int-to-float v3, v3

    #@b1
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@b4
    move-result-object v3

    #@b5
    invoke-virtual {v1, v2, v3}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@b8
    goto/16 :goto_b

    #@ba
    .line 910
    :cond_ba
    const-string/jumbo v1, "lumScale"

    #@bd
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c0
    move-result v1

    #@c1
    if-nez v1, :cond_cb

    #@c3
    const-string v1, "chromaScale"

    #@c5
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c8
    move-result v1

    #@c9
    if-eqz v1, :cond_e1

    #@cb
    .line 911
    :cond_cb
    const/4 v1, 0x2

    #@cc
    new-array v0, v1, [F

    #@ce
    const/4 v1, 0x0

    #@cf
    iget v2, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLumScale:F

    #@d1
    aput v2, v0, v1

    #@d3
    iget v1, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mChromaScale:F

    #@d5
    aput v1, v0, v3

    #@d7
    .line 912
    .local v0, yuvWeights:[F
    iget-object v1, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgMaskProgram:Landroid/filterfw/core/ShaderProgram;

    #@d9
    const-string/jumbo v2, "yuv_weights"

    #@dc
    invoke-virtual {v1, v2, v0}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@df
    goto/16 :goto_b

    #@e1
    .line 913
    .end local v0           #yuvWeights:[F
    :cond_e1
    const-string/jumbo v1, "maskBg"

    #@e4
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e7
    move-result v1

    #@e8
    if-eqz v1, :cond_fa

    #@ea
    .line 914
    iget-object v1, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgSubtractProgram:Landroid/filterfw/core/ShaderProgram;

    #@ec
    const-string/jumbo v2, "mask_blend_bg"

    #@ef
    iget v3, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskBg:F

    #@f1
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@f4
    move-result-object v3

    #@f5
    invoke-virtual {v1, v2, v3}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@f8
    goto/16 :goto_b

    #@fa
    .line 915
    :cond_fa
    const-string/jumbo v1, "maskFg"

    #@fd
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@100
    move-result v1

    #@101
    if-eqz v1, :cond_113

    #@103
    .line 916
    iget-object v1, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgSubtractProgram:Landroid/filterfw/core/ShaderProgram;

    #@105
    const-string/jumbo v2, "mask_blend_fg"

    #@108
    iget v3, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskFg:F

    #@10a
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@10d
    move-result-object v3

    #@10e
    invoke-virtual {v1, v2, v3}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@111
    goto/16 :goto_b

    #@113
    .line 917
    :cond_113
    const-string v1, "exposureChange"

    #@115
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@118
    move-result v1

    #@119
    if-eqz v1, :cond_12a

    #@11b
    .line 918
    iget-object v1, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgSubtractProgram:Landroid/filterfw/core/ShaderProgram;

    #@11d
    const-string v2, "exposure_change"

    #@11f
    iget v3, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mExposureChange:F

    #@121
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@124
    move-result-object v3

    #@125
    invoke-virtual {v1, v2, v3}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@128
    goto/16 :goto_b

    #@12a
    .line 919
    :cond_12a
    const-string/jumbo v1, "whitebalanceredChange"

    #@12d
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@130
    move-result v1

    #@131
    if-eqz v1, :cond_143

    #@133
    .line 920
    iget-object v1, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgSubtractProgram:Landroid/filterfw/core/ShaderProgram;

    #@135
    const-string/jumbo v2, "whitebalancered_change"

    #@138
    iget v3, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mWhiteBalanceRedChange:F

    #@13a
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@13d
    move-result-object v3

    #@13e
    invoke-virtual {v1, v2, v3}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@141
    goto/16 :goto_b

    #@143
    .line 921
    :cond_143
    const-string/jumbo v1, "whitebalanceblueChange"

    #@146
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@149
    move-result v1

    #@14a
    if-eqz v1, :cond_15c

    #@14c
    .line 922
    iget-object v1, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgSubtractProgram:Landroid/filterfw/core/ShaderProgram;

    #@14e
    const-string/jumbo v2, "whitebalanceblue_change"

    #@151
    iget v3, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mWhiteBalanceBlueChange:F

    #@153
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@156
    move-result-object v3

    #@157
    invoke-virtual {v1, v2, v3}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@15a
    goto/16 :goto_b

    #@15c
    .line 923
    :cond_15c
    const-string v1, "autowbToggle"

    #@15e
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@161
    move-result v1

    #@162
    if-eqz v1, :cond_b

    #@164
    .line 924
    iget-object v1, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAutomaticWhiteBalanceProgram:Landroid/filterfw/core/ShaderProgram;

    #@166
    const-string v2, "autowb_toggle"

    #@168
    iget v3, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAutoWBToggle:I

    #@16a
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@16d
    move-result-object v3

    #@16e
    invoke-virtual {v1, v2, v3}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@171
    goto/16 :goto_b
.end method

.method public getOutputFormat(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .registers 6
    .parameter "portName"
    .parameter "inputFormat"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 555
    invoke-virtual {p2}, Landroid/filterfw/core/FrameFormat;->mutableCopy()Landroid/filterfw/core/MutableFrameFormat;

    #@4
    move-result-object v0

    #@5
    .line 557
    .local v0, format:Landroid/filterfw/core/MutableFrameFormat;
    sget-object v1, Landroid/filterpacks/videoproc/BackDropperFilter;->mOutputNames:[Ljava/lang/String;

    #@7
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    #@a
    move-result-object v1

    #@b
    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@e
    move-result v1

    #@f
    if-nez v1, :cond_14

    #@11
    .line 558
    invoke-virtual {v0, v2, v2}, Landroid/filterfw/core/MutableFrameFormat;->setDimensions(II)V

    #@14
    .line 560
    :cond_14
    return-object v0
.end method

.method public prepare(Landroid/filterfw/core/FilterContext;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    .line 601
    iget-boolean v0, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLogVerbose:Z

    #@3
    if-eqz v0, :cond_c

    #@5
    const-string v0, "BackDropperFilter"

    #@7
    const-string v1, "Preparing BackDropperFilter!"

    #@9
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 603
    :cond_c
    new-array v0, v2, [Landroid/filterfw/core/GLFrame;

    #@e
    iput-object v0, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgMean:[Landroid/filterfw/core/GLFrame;

    #@10
    .line 604
    new-array v0, v2, [Landroid/filterfw/core/GLFrame;

    #@12
    iput-object v0, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgVariance:[Landroid/filterfw/core/GLFrame;

    #@14
    .line 605
    new-array v0, v2, [Landroid/filterfw/core/GLFrame;

    #@16
    iput-object v0, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskVerify:[Landroid/filterfw/core/GLFrame;

    #@18
    .line 606
    invoke-static {p1}, Landroid/filterfw/core/ShaderProgram;->createIdentity(Landroid/filterfw/core/FilterContext;)Landroid/filterfw/core/ShaderProgram;

    #@1b
    move-result-object v0

    #@1c
    iput-object v0, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->copyShaderProgram:Landroid/filterfw/core/ShaderProgram;

    #@1e
    .line 607
    return-void
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .registers 34
    .parameter "context"

    #@0
    .prologue
    .line 701
    const-string/jumbo v21, "video"

    #@3
    move-object/from16 v0, p0

    #@5
    move-object/from16 v1, v21

    #@7
    invoke-virtual {v0, v1}, Landroid/filterpacks/videoproc/BackDropperFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@a
    move-result-object v20

    #@b
    .line 702
    .local v20, video:Landroid/filterfw/core/Frame;
    const-string v21, "background"

    #@d
    move-object/from16 v0, p0

    #@f
    move-object/from16 v1, v21

    #@11
    invoke-virtual {v0, v1}, Landroid/filterpacks/videoproc/BackDropperFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@14
    move-result-object v4

    #@15
    .line 703
    .local v4, background:Landroid/filterfw/core/Frame;
    invoke-virtual/range {v20 .. v20}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@18
    move-result-object v21

    #@19
    move-object/from16 v0, p0

    #@1b
    move-object/from16 v1, v21

    #@1d
    move-object/from16 v2, p1

    #@1f
    invoke-direct {v0, v1, v2}, Landroid/filterpacks/videoproc/BackDropperFilter;->allocateFrames(Landroid/filterfw/core/FrameFormat;Landroid/filterfw/core/FilterContext;)V

    #@22
    .line 706
    move-object/from16 v0, p0

    #@24
    iget-boolean v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mStartLearning:Z

    #@26
    move/from16 v21, v0

    #@28
    if-eqz v21, :cond_95

    #@2a
    .line 707
    move-object/from16 v0, p0

    #@2c
    iget-boolean v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLogVerbose:Z

    #@2e
    move/from16 v21, v0

    #@30
    if-eqz v21, :cond_39

    #@32
    const-string v21, "BackDropperFilter"

    #@34
    const-string v22, "Starting learning"

    #@36
    invoke-static/range {v21 .. v22}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 708
    :cond_39
    move-object/from16 v0, p0

    #@3b
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgUpdateMeanProgram:Landroid/filterfw/core/ShaderProgram;

    #@3d
    move-object/from16 v21, v0

    #@3f
    const-string v22, "bg_adapt_rate"

    #@41
    move-object/from16 v0, p0

    #@43
    iget v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAdaptRateLearning:F

    #@45
    move/from16 v23, v0

    #@47
    invoke-static/range {v23 .. v23}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@4a
    move-result-object v23

    #@4b
    invoke-virtual/range {v21 .. v23}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@4e
    .line 709
    move-object/from16 v0, p0

    #@50
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgUpdateMeanProgram:Landroid/filterfw/core/ShaderProgram;

    #@52
    move-object/from16 v21, v0

    #@54
    const-string v22, "fg_adapt_rate"

    #@56
    move-object/from16 v0, p0

    #@58
    iget v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAdaptRateLearning:F

    #@5a
    move/from16 v23, v0

    #@5c
    invoke-static/range {v23 .. v23}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@5f
    move-result-object v23

    #@60
    invoke-virtual/range {v21 .. v23}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@63
    .line 710
    move-object/from16 v0, p0

    #@65
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgUpdateVarianceProgram:Landroid/filterfw/core/ShaderProgram;

    #@67
    move-object/from16 v21, v0

    #@69
    const-string v22, "bg_adapt_rate"

    #@6b
    move-object/from16 v0, p0

    #@6d
    iget v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAdaptRateLearning:F

    #@6f
    move/from16 v23, v0

    #@71
    invoke-static/range {v23 .. v23}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@74
    move-result-object v23

    #@75
    invoke-virtual/range {v21 .. v23}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@78
    .line 711
    move-object/from16 v0, p0

    #@7a
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgUpdateVarianceProgram:Landroid/filterfw/core/ShaderProgram;

    #@7c
    move-object/from16 v21, v0

    #@7e
    const-string v22, "fg_adapt_rate"

    #@80
    move-object/from16 v0, p0

    #@82
    iget v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAdaptRateLearning:F

    #@84
    move/from16 v23, v0

    #@86
    invoke-static/range {v23 .. v23}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@89
    move-result-object v23

    #@8a
    invoke-virtual/range {v21 .. v23}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@8d
    .line 712
    const/16 v21, 0x0

    #@8f
    move/from16 v0, v21

    #@91
    move-object/from16 v1, p0

    #@93
    iput v0, v1, Landroid/filterpacks/videoproc/BackDropperFilter;->mFrameCount:I

    #@95
    .line 716
    :cond_95
    move-object/from16 v0, p0

    #@97
    iget-boolean v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mPingPong:Z

    #@99
    move/from16 v21, v0

    #@9b
    if-eqz v21, :cond_4a1

    #@9d
    const/4 v11, 0x0

    #@9e
    .line 717
    .local v11, inputIndex:I
    :goto_9e
    move-object/from16 v0, p0

    #@a0
    iget-boolean v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mPingPong:Z

    #@a2
    move/from16 v21, v0

    #@a4
    if-eqz v21, :cond_4a4

    #@a6
    const/16 v17, 0x1

    #@a8
    .line 718
    .local v17, outputIndex:I
    :goto_a8
    move-object/from16 v0, p0

    #@aa
    iget-boolean v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mPingPong:Z

    #@ac
    move/from16 v21, v0

    #@ae
    if-nez v21, :cond_4a8

    #@b0
    const/16 v21, 0x1

    #@b2
    :goto_b2
    move/from16 v0, v21

    #@b4
    move-object/from16 v1, p0

    #@b6
    iput-boolean v0, v1, Landroid/filterpacks/videoproc/BackDropperFilter;->mPingPong:Z

    #@b8
    .line 721
    move-object/from16 v0, p0

    #@ba
    iget-boolean v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBackgroundFitModeChanged:Z

    #@bc
    move/from16 v21, v0

    #@be
    move-object/from16 v0, p0

    #@c0
    move-object/from16 v1, v20

    #@c2
    move/from16 v2, v21

    #@c4
    invoke-direct {v0, v1, v4, v2}, Landroid/filterpacks/videoproc/BackDropperFilter;->updateBgScaling(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;Z)V

    #@c7
    .line 722
    const/16 v21, 0x0

    #@c9
    move/from16 v0, v21

    #@cb
    move-object/from16 v1, p0

    #@cd
    iput-boolean v0, v1, Landroid/filterpacks/videoproc/BackDropperFilter;->mBackgroundFitModeChanged:Z

    #@cf
    .line 726
    move-object/from16 v0, p0

    #@d1
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->copyShaderProgram:Landroid/filterfw/core/ShaderProgram;

    #@d3
    move-object/from16 v21, v0

    #@d5
    move-object/from16 v0, p0

    #@d7
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mVideoInput:Landroid/filterfw/core/GLFrame;

    #@d9
    move-object/from16 v22, v0

    #@db
    move-object/from16 v0, v21

    #@dd
    move-object/from16 v1, v20

    #@df
    move-object/from16 v2, v22

    #@e1
    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/ShaderProgram;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@e4
    .line 727
    move-object/from16 v0, p0

    #@e6
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->copyShaderProgram:Landroid/filterfw/core/ShaderProgram;

    #@e8
    move-object/from16 v21, v0

    #@ea
    move-object/from16 v0, p0

    #@ec
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgInput:Landroid/filterfw/core/GLFrame;

    #@ee
    move-object/from16 v22, v0

    #@f0
    move-object/from16 v0, v21

    #@f2
    move-object/from16 v1, v22

    #@f4
    invoke-virtual {v0, v4, v1}, Landroid/filterfw/core/ShaderProgram;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@f7
    .line 729
    move-object/from16 v0, p0

    #@f9
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mVideoInput:Landroid/filterfw/core/GLFrame;

    #@fb
    move-object/from16 v21, v0

    #@fd
    invoke-virtual/range {v21 .. v21}, Landroid/filterfw/core/GLFrame;->generateMipMap()V

    #@100
    .line 730
    move-object/from16 v0, p0

    #@102
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mVideoInput:Landroid/filterfw/core/GLFrame;

    #@104
    move-object/from16 v21, v0

    #@106
    const/16 v22, 0x2801

    #@108
    const/16 v23, 0x2701

    #@10a
    invoke-virtual/range {v21 .. v23}, Landroid/filterfw/core/GLFrame;->setTextureParameter(II)V

    #@10d
    .line 733
    move-object/from16 v0, p0

    #@10f
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgInput:Landroid/filterfw/core/GLFrame;

    #@111
    move-object/from16 v21, v0

    #@113
    invoke-virtual/range {v21 .. v21}, Landroid/filterfw/core/GLFrame;->generateMipMap()V

    #@116
    .line 734
    move-object/from16 v0, p0

    #@118
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgInput:Landroid/filterfw/core/GLFrame;

    #@11a
    move-object/from16 v21, v0

    #@11c
    const/16 v22, 0x2801

    #@11e
    const/16 v23, 0x2701

    #@120
    invoke-virtual/range {v21 .. v23}, Landroid/filterfw/core/GLFrame;->setTextureParameter(II)V

    #@123
    .line 737
    move-object/from16 v0, p0

    #@125
    iget-boolean v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mStartLearning:Z

    #@127
    move/from16 v21, v0

    #@129
    if-eqz v21, :cond_14a

    #@12b
    .line 738
    move-object/from16 v0, p0

    #@12d
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->copyShaderProgram:Landroid/filterfw/core/ShaderProgram;

    #@12f
    move-object/from16 v21, v0

    #@131
    move-object/from16 v0, p0

    #@133
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mVideoInput:Landroid/filterfw/core/GLFrame;

    #@135
    move-object/from16 v22, v0

    #@137
    move-object/from16 v0, p0

    #@139
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgMean:[Landroid/filterfw/core/GLFrame;

    #@13b
    move-object/from16 v23, v0

    #@13d
    aget-object v23, v23, v11

    #@13f
    invoke-virtual/range {v21 .. v23}, Landroid/filterfw/core/ShaderProgram;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@142
    .line 739
    const/16 v21, 0x0

    #@144
    move/from16 v0, v21

    #@146
    move-object/from16 v1, p0

    #@148
    iput-boolean v0, v1, Landroid/filterpacks/videoproc/BackDropperFilter;->mStartLearning:Z

    #@14a
    .line 743
    :cond_14a
    const/16 v21, 0x3

    #@14c
    move/from16 v0, v21

    #@14e
    new-array v8, v0, [Landroid/filterfw/core/Frame;

    #@150
    const/16 v21, 0x0

    #@152
    move-object/from16 v0, p0

    #@154
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mVideoInput:Landroid/filterfw/core/GLFrame;

    #@156
    move-object/from16 v22, v0

    #@158
    aput-object v22, v8, v21

    #@15a
    const/16 v21, 0x1

    #@15c
    move-object/from16 v0, p0

    #@15e
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgMean:[Landroid/filterfw/core/GLFrame;

    #@160
    move-object/from16 v22, v0

    #@162
    aget-object v22, v22, v11

    #@164
    aput-object v22, v8, v21

    #@166
    const/16 v21, 0x2

    #@168
    move-object/from16 v0, p0

    #@16a
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgVariance:[Landroid/filterfw/core/GLFrame;

    #@16c
    move-object/from16 v22, v0

    #@16e
    aget-object v22, v22, v11

    #@170
    aput-object v22, v8, v21

    #@172
    .line 744
    .local v8, distInputs:[Landroid/filterfw/core/Frame;
    move-object/from16 v0, p0

    #@174
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgDistProgram:Landroid/filterfw/core/ShaderProgram;

    #@176
    move-object/from16 v21, v0

    #@178
    move-object/from16 v0, p0

    #@17a
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mDistance:Landroid/filterfw/core/GLFrame;

    #@17c
    move-object/from16 v22, v0

    #@17e
    move-object/from16 v0, v21

    #@180
    move-object/from16 v1, v22

    #@182
    invoke-virtual {v0, v8, v1}, Landroid/filterfw/core/ShaderProgram;->process([Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@185
    .line 745
    move-object/from16 v0, p0

    #@187
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mDistance:Landroid/filterfw/core/GLFrame;

    #@189
    move-object/from16 v21, v0

    #@18b
    invoke-virtual/range {v21 .. v21}, Landroid/filterfw/core/GLFrame;->generateMipMap()V

    #@18e
    .line 746
    move-object/from16 v0, p0

    #@190
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mDistance:Landroid/filterfw/core/GLFrame;

    #@192
    move-object/from16 v21, v0

    #@194
    const/16 v22, 0x2801

    #@196
    const/16 v23, 0x2701

    #@198
    invoke-virtual/range {v21 .. v23}, Landroid/filterfw/core/GLFrame;->setTextureParameter(II)V

    #@19b
    .line 749
    move-object/from16 v0, p0

    #@19d
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgMaskProgram:Landroid/filterfw/core/ShaderProgram;

    #@19f
    move-object/from16 v21, v0

    #@1a1
    move-object/from16 v0, p0

    #@1a3
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mDistance:Landroid/filterfw/core/GLFrame;

    #@1a5
    move-object/from16 v22, v0

    #@1a7
    move-object/from16 v0, p0

    #@1a9
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMask:Landroid/filterfw/core/GLFrame;

    #@1ab
    move-object/from16 v23, v0

    #@1ad
    invoke-virtual/range {v21 .. v23}, Landroid/filterfw/core/ShaderProgram;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@1b0
    .line 750
    move-object/from16 v0, p0

    #@1b2
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMask:Landroid/filterfw/core/GLFrame;

    #@1b4
    move-object/from16 v21, v0

    #@1b6
    invoke-virtual/range {v21 .. v21}, Landroid/filterfw/core/GLFrame;->generateMipMap()V

    #@1b9
    .line 751
    move-object/from16 v0, p0

    #@1bb
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMask:Landroid/filterfw/core/GLFrame;

    #@1bd
    move-object/from16 v21, v0

    #@1bf
    const/16 v22, 0x2801

    #@1c1
    const/16 v23, 0x2701

    #@1c3
    invoke-virtual/range {v21 .. v23}, Landroid/filterfw/core/GLFrame;->setTextureParameter(II)V

    #@1c6
    .line 754
    const/16 v21, 0x2

    #@1c8
    move/from16 v0, v21

    #@1ca
    new-array v3, v0, [Landroid/filterfw/core/Frame;

    #@1cc
    const/16 v21, 0x0

    #@1ce
    move-object/from16 v0, p0

    #@1d0
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mVideoInput:Landroid/filterfw/core/GLFrame;

    #@1d2
    move-object/from16 v22, v0

    #@1d4
    aput-object v22, v3, v21

    #@1d6
    const/16 v21, 0x1

    #@1d8
    move-object/from16 v0, p0

    #@1da
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgInput:Landroid/filterfw/core/GLFrame;

    #@1dc
    move-object/from16 v22, v0

    #@1de
    aput-object v22, v3, v21

    #@1e0
    .line 755
    .local v3, autoWBInputs:[Landroid/filterfw/core/Frame;
    move-object/from16 v0, p0

    #@1e2
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAutomaticWhiteBalanceProgram:Landroid/filterfw/core/ShaderProgram;

    #@1e4
    move-object/from16 v21, v0

    #@1e6
    move-object/from16 v0, p0

    #@1e8
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAutoWB:Landroid/filterfw/core/GLFrame;

    #@1ea
    move-object/from16 v22, v0

    #@1ec
    move-object/from16 v0, v21

    #@1ee
    move-object/from16 v1, v22

    #@1f0
    invoke-virtual {v0, v3, v1}, Landroid/filterfw/core/ShaderProgram;->process([Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@1f3
    .line 757
    move-object/from16 v0, p0

    #@1f5
    iget v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mFrameCount:I

    #@1f7
    move/from16 v21, v0

    #@1f9
    move-object/from16 v0, p0

    #@1fb
    iget v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLearningDuration:I

    #@1fd
    move/from16 v22, v0

    #@1ff
    move/from16 v0, v21

    #@201
    move/from16 v1, v22

    #@203
    if-gt v0, v1, :cond_539

    #@205
    .line 759
    const-string/jumbo v21, "video"

    #@208
    move-object/from16 v0, p0

    #@20a
    move-object/from16 v1, v21

    #@20c
    move-object/from16 v2, v20

    #@20e
    invoke-virtual {v0, v1, v2}, Landroid/filterpacks/videoproc/BackDropperFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@211
    .line 761
    move-object/from16 v0, p0

    #@213
    iget v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mFrameCount:I

    #@215
    move/from16 v21, v0

    #@217
    move-object/from16 v0, p0

    #@219
    iget v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLearningDuration:I

    #@21b
    move/from16 v22, v0

    #@21d
    move-object/from16 v0, p0

    #@21f
    iget v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLearningVerifyDuration:I

    #@221
    move/from16 v23, v0

    #@223
    sub-int v22, v22, v23

    #@225
    move/from16 v0, v21

    #@227
    move/from16 v1, v22

    #@229
    if-ne v0, v1, :cond_4ac

    #@22b
    .line 762
    move-object/from16 v0, p0

    #@22d
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->copyShaderProgram:Landroid/filterfw/core/ShaderProgram;

    #@22f
    move-object/from16 v21, v0

    #@231
    move-object/from16 v0, p0

    #@233
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMask:Landroid/filterfw/core/GLFrame;

    #@235
    move-object/from16 v22, v0

    #@237
    move-object/from16 v0, p0

    #@239
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskVerify:[Landroid/filterfw/core/GLFrame;

    #@23b
    move-object/from16 v23, v0

    #@23d
    aget-object v23, v23, v17

    #@23f
    invoke-virtual/range {v21 .. v23}, Landroid/filterfw/core/ShaderProgram;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@242
    .line 764
    move-object/from16 v0, p0

    #@244
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgUpdateMeanProgram:Landroid/filterfw/core/ShaderProgram;

    #@246
    move-object/from16 v21, v0

    #@248
    const-string v22, "bg_adapt_rate"

    #@24a
    move-object/from16 v0, p0

    #@24c
    iget v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAdaptRateBg:F

    #@24e
    move/from16 v23, v0

    #@250
    invoke-static/range {v23 .. v23}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@253
    move-result-object v23

    #@254
    invoke-virtual/range {v21 .. v23}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@257
    .line 765
    move-object/from16 v0, p0

    #@259
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgUpdateMeanProgram:Landroid/filterfw/core/ShaderProgram;

    #@25b
    move-object/from16 v21, v0

    #@25d
    const-string v22, "fg_adapt_rate"

    #@25f
    move-object/from16 v0, p0

    #@261
    iget v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAdaptRateFg:F

    #@263
    move/from16 v23, v0

    #@265
    invoke-static/range {v23 .. v23}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@268
    move-result-object v23

    #@269
    invoke-virtual/range {v21 .. v23}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@26c
    .line 766
    move-object/from16 v0, p0

    #@26e
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgUpdateVarianceProgram:Landroid/filterfw/core/ShaderProgram;

    #@270
    move-object/from16 v21, v0

    #@272
    const-string v22, "bg_adapt_rate"

    #@274
    move-object/from16 v0, p0

    #@276
    iget v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAdaptRateBg:F

    #@278
    move/from16 v23, v0

    #@27a
    invoke-static/range {v23 .. v23}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@27d
    move-result-object v23

    #@27e
    invoke-virtual/range {v21 .. v23}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@281
    .line 767
    move-object/from16 v0, p0

    #@283
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgUpdateVarianceProgram:Landroid/filterfw/core/ShaderProgram;

    #@285
    move-object/from16 v21, v0

    #@287
    const-string v22, "fg_adapt_rate"

    #@289
    move-object/from16 v0, p0

    #@28b
    iget v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAdaptRateFg:F

    #@28d
    move/from16 v23, v0

    #@28f
    invoke-static/range {v23 .. v23}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@292
    move-result-object v23

    #@293
    invoke-virtual/range {v21 .. v23}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@296
    .line 780
    :cond_296
    :goto_296
    move-object/from16 v0, p0

    #@298
    iget v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mFrameCount:I

    #@29a
    move/from16 v21, v0

    #@29c
    move-object/from16 v0, p0

    #@29e
    iget v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLearningDuration:I

    #@2a0
    move/from16 v22, v0

    #@2a2
    move/from16 v0, v21

    #@2a4
    move/from16 v1, v22

    #@2a6
    if-ne v0, v1, :cond_310

    #@2a8
    .line 783
    move-object/from16 v0, p0

    #@2aa
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->copyShaderProgram:Landroid/filterfw/core/ShaderProgram;

    #@2ac
    move-object/from16 v21, v0

    #@2ae
    move-object/from16 v0, p0

    #@2b0
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskVerify:[Landroid/filterfw/core/GLFrame;

    #@2b2
    move-object/from16 v22, v0

    #@2b4
    aget-object v22, v22, v17

    #@2b6
    move-object/from16 v0, p0

    #@2b8
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskAverage:Landroid/filterfw/core/GLFrame;

    #@2ba
    move-object/from16 v23, v0

    #@2bc
    invoke-virtual/range {v21 .. v23}, Landroid/filterfw/core/ShaderProgram;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@2bf
    .line 784
    move-object/from16 v0, p0

    #@2c1
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskAverage:Landroid/filterfw/core/GLFrame;

    #@2c3
    move-object/from16 v21, v0

    #@2c5
    invoke-virtual/range {v21 .. v21}, Landroid/filterfw/core/GLFrame;->getData()Ljava/nio/ByteBuffer;

    #@2c8
    move-result-object v12

    #@2c9
    .line 785
    .local v12, mMaskAverageByteBuffer:Ljava/nio/ByteBuffer;
    invoke-virtual {v12}, Ljava/nio/ByteBuffer;->array()[B

    #@2cc
    move-result-object v14

    #@2cd
    .line 786
    .local v14, mask_average:[B
    const/16 v21, 0x3

    #@2cf
    aget-byte v21, v14, v21

    #@2d1
    move/from16 v0, v21

    #@2d3
    and-int/lit16 v5, v0, 0xff

    #@2d5
    .line 788
    .local v5, bi:I
    move-object/from16 v0, p0

    #@2d7
    iget-boolean v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLogVerbose:Z

    #@2d9
    move/from16 v21, v0

    #@2db
    if-eqz v21, :cond_302

    #@2dd
    .line 789
    const-string v21, "BackDropperFilter"

    #@2df
    const-string v22, "Mask_average is %d, threshold is %d"

    #@2e1
    const/16 v23, 0x2

    #@2e3
    move/from16 v0, v23

    #@2e5
    new-array v0, v0, [Ljava/lang/Object;

    #@2e7
    move-object/from16 v23, v0

    #@2e9
    const/16 v24, 0x0

    #@2eb
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2ee
    move-result-object v25

    #@2ef
    aput-object v25, v23, v24

    #@2f1
    const/16 v24, 0x1

    #@2f3
    const/16 v25, 0x14

    #@2f5
    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2f8
    move-result-object v25

    #@2f9
    aput-object v25, v23, v24

    #@2fb
    invoke-static/range {v22 .. v23}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2fe
    move-result-object v22

    #@2ff
    invoke-static/range {v21 .. v22}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@302
    .line 794
    :cond_302
    const/16 v21, 0x14

    #@304
    move/from16 v0, v21

    #@306
    if-lt v5, v0, :cond_513

    #@308
    .line 795
    const/16 v21, 0x1

    #@30a
    move/from16 v0, v21

    #@30c
    move-object/from16 v1, p0

    #@30e
    iput-boolean v0, v1, Landroid/filterpacks/videoproc/BackDropperFilter;->mStartLearning:Z

    #@310
    .line 812
    .end local v5           #bi:I
    .end local v12           #mMaskAverageByteBuffer:Ljava/nio/ByteBuffer;
    .end local v14           #mask_average:[B
    :cond_310
    :goto_310
    move-object/from16 v0, p0

    #@312
    iget v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mFrameCount:I

    #@314
    move/from16 v21, v0

    #@316
    move-object/from16 v0, p0

    #@318
    iget v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLearningDuration:I

    #@31a
    move/from16 v22, v0

    #@31c
    move-object/from16 v0, p0

    #@31e
    iget v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLearningVerifyDuration:I

    #@320
    move/from16 v23, v0

    #@322
    sub-int v22, v22, v23

    #@324
    move/from16 v0, v21

    #@326
    move/from16 v1, v22

    #@328
    if-lt v0, v1, :cond_34c

    #@32a
    move-object/from16 v0, p0

    #@32c
    iget v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAdaptRateBg:F

    #@32e
    move/from16 v21, v0

    #@330
    move/from16 v0, v21

    #@332
    float-to-double v0, v0

    #@333
    move-wide/from16 v21, v0

    #@335
    const-wide/16 v23, 0x0

    #@337
    cmpl-double v21, v21, v23

    #@339
    if-gtz v21, :cond_34c

    #@33b
    move-object/from16 v0, p0

    #@33d
    iget v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAdaptRateFg:F

    #@33f
    move/from16 v21, v0

    #@341
    move/from16 v0, v21

    #@343
    float-to-double v0, v0

    #@344
    move-wide/from16 v21, v0

    #@346
    const-wide/16 v23, 0x0

    #@348
    cmpl-double v21, v21, v23

    #@34a
    if-lez v21, :cond_406

    #@34c
    .line 814
    :cond_34c
    const/16 v21, 0x3

    #@34e
    move/from16 v0, v21

    #@350
    new-array v15, v0, [Landroid/filterfw/core/Frame;

    #@352
    const/16 v21, 0x0

    #@354
    move-object/from16 v0, p0

    #@356
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mVideoInput:Landroid/filterfw/core/GLFrame;

    #@358
    move-object/from16 v22, v0

    #@35a
    aput-object v22, v15, v21

    #@35c
    const/16 v21, 0x1

    #@35e
    move-object/from16 v0, p0

    #@360
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgMean:[Landroid/filterfw/core/GLFrame;

    #@362
    move-object/from16 v22, v0

    #@364
    aget-object v22, v22, v11

    #@366
    aput-object v22, v15, v21

    #@368
    const/16 v21, 0x2

    #@36a
    move-object/from16 v0, p0

    #@36c
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMask:Landroid/filterfw/core/GLFrame;

    #@36e
    move-object/from16 v22, v0

    #@370
    aput-object v22, v15, v21

    #@372
    .line 815
    .local v15, meanUpdateInputs:[Landroid/filterfw/core/Frame;
    move-object/from16 v0, p0

    #@374
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgUpdateMeanProgram:Landroid/filterfw/core/ShaderProgram;

    #@376
    move-object/from16 v21, v0

    #@378
    move-object/from16 v0, p0

    #@37a
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgMean:[Landroid/filterfw/core/GLFrame;

    #@37c
    move-object/from16 v22, v0

    #@37e
    aget-object v22, v22, v17

    #@380
    move-object/from16 v0, v21

    #@382
    move-object/from16 v1, v22

    #@384
    invoke-virtual {v0, v15, v1}, Landroid/filterfw/core/ShaderProgram;->process([Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@387
    .line 816
    move-object/from16 v0, p0

    #@389
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgMean:[Landroid/filterfw/core/GLFrame;

    #@38b
    move-object/from16 v21, v0

    #@38d
    aget-object v21, v21, v17

    #@38f
    invoke-virtual/range {v21 .. v21}, Landroid/filterfw/core/GLFrame;->generateMipMap()V

    #@392
    .line 817
    move-object/from16 v0, p0

    #@394
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgMean:[Landroid/filterfw/core/GLFrame;

    #@396
    move-object/from16 v21, v0

    #@398
    aget-object v21, v21, v17

    #@39a
    const/16 v22, 0x2801

    #@39c
    const/16 v23, 0x2701

    #@39e
    invoke-virtual/range {v21 .. v23}, Landroid/filterfw/core/GLFrame;->setTextureParameter(II)V

    #@3a1
    .line 820
    const/16 v21, 0x4

    #@3a3
    move/from16 v0, v21

    #@3a5
    new-array v0, v0, [Landroid/filterfw/core/Frame;

    #@3a7
    move-object/from16 v19, v0

    #@3a9
    const/16 v21, 0x0

    #@3ab
    move-object/from16 v0, p0

    #@3ad
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mVideoInput:Landroid/filterfw/core/GLFrame;

    #@3af
    move-object/from16 v22, v0

    #@3b1
    aput-object v22, v19, v21

    #@3b3
    const/16 v21, 0x1

    #@3b5
    move-object/from16 v0, p0

    #@3b7
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgMean:[Landroid/filterfw/core/GLFrame;

    #@3b9
    move-object/from16 v22, v0

    #@3bb
    aget-object v22, v22, v11

    #@3bd
    aput-object v22, v19, v21

    #@3bf
    const/16 v21, 0x2

    #@3c1
    move-object/from16 v0, p0

    #@3c3
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgVariance:[Landroid/filterfw/core/GLFrame;

    #@3c5
    move-object/from16 v22, v0

    #@3c7
    aget-object v22, v22, v11

    #@3c9
    aput-object v22, v19, v21

    #@3cb
    const/16 v21, 0x3

    #@3cd
    move-object/from16 v0, p0

    #@3cf
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMask:Landroid/filterfw/core/GLFrame;

    #@3d1
    move-object/from16 v22, v0

    #@3d3
    aput-object v22, v19, v21

    #@3d5
    .line 823
    .local v19, varianceUpdateInputs:[Landroid/filterfw/core/Frame;
    move-object/from16 v0, p0

    #@3d7
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgUpdateVarianceProgram:Landroid/filterfw/core/ShaderProgram;

    #@3d9
    move-object/from16 v21, v0

    #@3db
    move-object/from16 v0, p0

    #@3dd
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgVariance:[Landroid/filterfw/core/GLFrame;

    #@3df
    move-object/from16 v22, v0

    #@3e1
    aget-object v22, v22, v17

    #@3e3
    move-object/from16 v0, v21

    #@3e5
    move-object/from16 v1, v19

    #@3e7
    move-object/from16 v2, v22

    #@3e9
    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/ShaderProgram;->process([Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@3ec
    .line 824
    move-object/from16 v0, p0

    #@3ee
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgVariance:[Landroid/filterfw/core/GLFrame;

    #@3f0
    move-object/from16 v21, v0

    #@3f2
    aget-object v21, v21, v17

    #@3f4
    invoke-virtual/range {v21 .. v21}, Landroid/filterfw/core/GLFrame;->generateMipMap()V

    #@3f7
    .line 825
    move-object/from16 v0, p0

    #@3f9
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgVariance:[Landroid/filterfw/core/GLFrame;

    #@3fb
    move-object/from16 v21, v0

    #@3fd
    aget-object v21, v21, v17

    #@3ff
    const/16 v22, 0x2801

    #@401
    const/16 v23, 0x2701

    #@403
    invoke-virtual/range {v21 .. v23}, Landroid/filterfw/core/GLFrame;->setTextureParameter(II)V

    #@406
    .line 830
    .end local v15           #meanUpdateInputs:[Landroid/filterfw/core/Frame;
    .end local v19           #varianceUpdateInputs:[Landroid/filterfw/core/Frame;
    :cond_406
    move-object/from16 v0, p0

    #@408
    iget-boolean v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mProvideDebugOutputs:Z

    #@40a
    move/from16 v21, v0

    #@40c
    if-eqz v21, :cond_460

    #@40e
    .line 831
    invoke-virtual/range {p1 .. p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@411
    move-result-object v21

    #@412
    invoke-virtual/range {v20 .. v20}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@415
    move-result-object v22

    #@416
    invoke-virtual/range {v21 .. v22}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@419
    move-result-object v6

    #@41a
    .line 832
    .local v6, dbg1:Landroid/filterfw/core/Frame;
    move-object/from16 v0, p0

    #@41c
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mCopyOutProgram:Landroid/filterfw/core/ShaderProgram;

    #@41e
    move-object/from16 v21, v0

    #@420
    move-object/from16 v0, v21

    #@422
    move-object/from16 v1, v20

    #@424
    invoke-virtual {v0, v1, v6}, Landroid/filterfw/core/ShaderProgram;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@427
    .line 833
    const-string v21, "debug1"

    #@429
    move-object/from16 v0, p0

    #@42b
    move-object/from16 v1, v21

    #@42d
    invoke-virtual {v0, v1, v6}, Landroid/filterpacks/videoproc/BackDropperFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@430
    .line 834
    invoke-virtual {v6}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@433
    .line 836
    invoke-virtual/range {p1 .. p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@436
    move-result-object v21

    #@437
    move-object/from16 v0, p0

    #@439
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMemoryFormat:Landroid/filterfw/core/MutableFrameFormat;

    #@43b
    move-object/from16 v22, v0

    #@43d
    invoke-virtual/range {v21 .. v22}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@440
    move-result-object v7

    #@441
    .line 837
    .local v7, dbg2:Landroid/filterfw/core/Frame;
    move-object/from16 v0, p0

    #@443
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mCopyOutProgram:Landroid/filterfw/core/ShaderProgram;

    #@445
    move-object/from16 v21, v0

    #@447
    move-object/from16 v0, p0

    #@449
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMask:Landroid/filterfw/core/GLFrame;

    #@44b
    move-object/from16 v22, v0

    #@44d
    move-object/from16 v0, v21

    #@44f
    move-object/from16 v1, v22

    #@451
    invoke-virtual {v0, v1, v7}, Landroid/filterfw/core/ShaderProgram;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@454
    .line 838
    const-string v21, "debug2"

    #@456
    move-object/from16 v0, p0

    #@458
    move-object/from16 v1, v21

    #@45a
    invoke-virtual {v0, v1, v7}, Landroid/filterpacks/videoproc/BackDropperFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@45d
    .line 839
    invoke-virtual {v7}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@460
    .line 842
    .end local v6           #dbg1:Landroid/filterfw/core/Frame;
    .end local v7           #dbg2:Landroid/filterfw/core/Frame;
    :cond_460
    move-object/from16 v0, p0

    #@462
    iget v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mFrameCount:I

    #@464
    move/from16 v21, v0

    #@466
    add-int/lit8 v21, v21, 0x1

    #@468
    move/from16 v0, v21

    #@46a
    move-object/from16 v1, p0

    #@46c
    iput v0, v1, Landroid/filterpacks/videoproc/BackDropperFilter;->mFrameCount:I

    #@46e
    .line 844
    move-object/from16 v0, p0

    #@470
    iget-boolean v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLogVerbose:Z

    #@472
    move/from16 v21, v0

    #@474
    if-eqz v21, :cond_4a0

    #@476
    .line 845
    move-object/from16 v0, p0

    #@478
    iget v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mFrameCount:I

    #@47a
    move/from16 v21, v0

    #@47c
    rem-int/lit8 v21, v21, 0x1e

    #@47e
    if-nez v21, :cond_4a0

    #@480
    .line 846
    move-object/from16 v0, p0

    #@482
    iget-wide v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->startTime:J

    #@484
    move-wide/from16 v21, v0

    #@486
    const-wide/16 v23, -0x1

    #@488
    cmp-long v21, v21, v23

    #@48a
    if-nez v21, :cond_589

    #@48c
    .line 847
    invoke-virtual/range {p1 .. p1}, Landroid/filterfw/core/FilterContext;->getGLEnvironment()Landroid/filterfw/core/GLEnvironment;

    #@48f
    move-result-object v21

    #@490
    invoke-virtual/range {v21 .. v21}, Landroid/filterfw/core/GLEnvironment;->activate()V

    #@493
    .line 848
    invoke-static {}, Landroid/opengl/GLES20;->glFinish()V

    #@496
    .line 849
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@499
    move-result-wide v21

    #@49a
    move-wide/from16 v0, v21

    #@49c
    move-object/from16 v2, p0

    #@49e
    iput-wide v0, v2, Landroid/filterpacks/videoproc/BackDropperFilter;->startTime:J

    #@4a0
    .line 860
    :cond_4a0
    :goto_4a0
    return-void

    #@4a1
    .line 716
    .end local v3           #autoWBInputs:[Landroid/filterfw/core/Frame;
    .end local v8           #distInputs:[Landroid/filterfw/core/Frame;
    .end local v11           #inputIndex:I
    .end local v17           #outputIndex:I
    :cond_4a1
    const/4 v11, 0x1

    #@4a2
    goto/16 :goto_9e

    #@4a4
    .line 717
    .restart local v11       #inputIndex:I
    :cond_4a4
    const/16 v17, 0x0

    #@4a6
    goto/16 :goto_a8

    #@4a8
    .line 718
    .restart local v17       #outputIndex:I
    :cond_4a8
    const/16 v21, 0x0

    #@4aa
    goto/16 :goto_b2

    #@4ac
    .line 770
    .restart local v3       #autoWBInputs:[Landroid/filterfw/core/Frame;
    .restart local v8       #distInputs:[Landroid/filterfw/core/Frame;
    :cond_4ac
    move-object/from16 v0, p0

    #@4ae
    iget v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mFrameCount:I

    #@4b0
    move/from16 v21, v0

    #@4b2
    move-object/from16 v0, p0

    #@4b4
    iget v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLearningDuration:I

    #@4b6
    move/from16 v22, v0

    #@4b8
    move-object/from16 v0, p0

    #@4ba
    iget v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLearningVerifyDuration:I

    #@4bc
    move/from16 v23, v0

    #@4be
    sub-int v22, v22, v23

    #@4c0
    move/from16 v0, v21

    #@4c2
    move/from16 v1, v22

    #@4c4
    if-le v0, v1, :cond_296

    #@4c6
    .line 773
    const/16 v21, 0x2

    #@4c8
    move/from16 v0, v21

    #@4ca
    new-array v13, v0, [Landroid/filterfw/core/Frame;

    #@4cc
    const/16 v21, 0x0

    #@4ce
    move-object/from16 v0, p0

    #@4d0
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskVerify:[Landroid/filterfw/core/GLFrame;

    #@4d2
    move-object/from16 v22, v0

    #@4d4
    aget-object v22, v22, v11

    #@4d6
    aput-object v22, v13, v21

    #@4d8
    const/16 v21, 0x1

    #@4da
    move-object/from16 v0, p0

    #@4dc
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMask:Landroid/filterfw/core/GLFrame;

    #@4de
    move-object/from16 v22, v0

    #@4e0
    aput-object v22, v13, v21

    #@4e2
    .line 774
    .local v13, maskVerifyInputs:[Landroid/filterfw/core/Frame;
    move-object/from16 v0, p0

    #@4e4
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskVerifyProgram:Landroid/filterfw/core/ShaderProgram;

    #@4e6
    move-object/from16 v21, v0

    #@4e8
    move-object/from16 v0, p0

    #@4ea
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskVerify:[Landroid/filterfw/core/GLFrame;

    #@4ec
    move-object/from16 v22, v0

    #@4ee
    aget-object v22, v22, v17

    #@4f0
    move-object/from16 v0, v21

    #@4f2
    move-object/from16 v1, v22

    #@4f4
    invoke-virtual {v0, v13, v1}, Landroid/filterfw/core/ShaderProgram;->process([Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@4f7
    .line 775
    move-object/from16 v0, p0

    #@4f9
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskVerify:[Landroid/filterfw/core/GLFrame;

    #@4fb
    move-object/from16 v21, v0

    #@4fd
    aget-object v21, v21, v17

    #@4ff
    invoke-virtual/range {v21 .. v21}, Landroid/filterfw/core/GLFrame;->generateMipMap()V

    #@502
    .line 776
    move-object/from16 v0, p0

    #@504
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMaskVerify:[Landroid/filterfw/core/GLFrame;

    #@506
    move-object/from16 v21, v0

    #@508
    aget-object v21, v21, v17

    #@50a
    const/16 v22, 0x2801

    #@50c
    const/16 v23, 0x2701

    #@50e
    invoke-virtual/range {v21 .. v23}, Landroid/filterfw/core/GLFrame;->setTextureParameter(II)V

    #@511
    goto/16 :goto_296

    #@513
    .line 797
    .end local v13           #maskVerifyInputs:[Landroid/filterfw/core/Frame;
    .restart local v5       #bi:I
    .restart local v12       #mMaskAverageByteBuffer:Ljava/nio/ByteBuffer;
    .restart local v14       #mask_average:[B
    :cond_513
    move-object/from16 v0, p0

    #@515
    iget-boolean v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLogVerbose:Z

    #@517
    move/from16 v21, v0

    #@519
    if-eqz v21, :cond_522

    #@51b
    const-string v21, "BackDropperFilter"

    #@51d
    const-string v22, "Learning done"

    #@51f
    invoke-static/range {v21 .. v22}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@522
    .line 798
    :cond_522
    move-object/from16 v0, p0

    #@524
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLearningDoneListener:Landroid/filterpacks/videoproc/BackDropperFilter$LearningDoneListener;

    #@526
    move-object/from16 v21, v0

    #@528
    if-eqz v21, :cond_310

    #@52a
    .line 799
    move-object/from16 v0, p0

    #@52c
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mLearningDoneListener:Landroid/filterpacks/videoproc/BackDropperFilter$LearningDoneListener;

    #@52e
    move-object/from16 v21, v0

    #@530
    move-object/from16 v0, v21

    #@532
    move-object/from16 v1, p0

    #@534
    invoke-interface {v0, v1}, Landroid/filterpacks/videoproc/BackDropperFilter$LearningDoneListener;->onLearningDone(Landroid/filterpacks/videoproc/BackDropperFilter;)V

    #@537
    goto/16 :goto_310

    #@539
    .line 804
    .end local v5           #bi:I
    .end local v12           #mMaskAverageByteBuffer:Ljava/nio/ByteBuffer;
    .end local v14           #mask_average:[B
    :cond_539
    invoke-virtual/range {p1 .. p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@53c
    move-result-object v21

    #@53d
    invoke-virtual/range {v20 .. v20}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@540
    move-result-object v22

    #@541
    invoke-virtual/range {v21 .. v22}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@544
    move-result-object v16

    #@545
    .line 805
    .local v16, output:Landroid/filterfw/core/Frame;
    const/16 v21, 0x4

    #@547
    move/from16 v0, v21

    #@549
    new-array v0, v0, [Landroid/filterfw/core/Frame;

    #@54b
    move-object/from16 v18, v0

    #@54d
    const/16 v21, 0x0

    #@54f
    aput-object v20, v18, v21

    #@551
    const/16 v21, 0x1

    #@553
    aput-object v4, v18, v21

    #@555
    const/16 v21, 0x2

    #@557
    move-object/from16 v0, p0

    #@559
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mMask:Landroid/filterfw/core/GLFrame;

    #@55b
    move-object/from16 v22, v0

    #@55d
    aput-object v22, v18, v21

    #@55f
    const/16 v21, 0x3

    #@561
    move-object/from16 v0, p0

    #@563
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mAutoWB:Landroid/filterfw/core/GLFrame;

    #@565
    move-object/from16 v22, v0

    #@567
    aput-object v22, v18, v21

    #@569
    .line 806
    .local v18, subtractInputs:[Landroid/filterfw/core/Frame;
    move-object/from16 v0, p0

    #@56b
    iget-object v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mBgSubtractProgram:Landroid/filterfw/core/ShaderProgram;

    #@56d
    move-object/from16 v21, v0

    #@56f
    move-object/from16 v0, v21

    #@571
    move-object/from16 v1, v18

    #@573
    move-object/from16 v2, v16

    #@575
    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/ShaderProgram;->process([Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    #@578
    .line 807
    const-string/jumbo v21, "video"

    #@57b
    move-object/from16 v0, p0

    #@57d
    move-object/from16 v1, v21

    #@57f
    move-object/from16 v2, v16

    #@581
    invoke-virtual {v0, v1, v2}, Landroid/filterpacks/videoproc/BackDropperFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@584
    .line 808
    invoke-virtual/range {v16 .. v16}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@587
    goto/16 :goto_310

    #@589
    .line 851
    .end local v16           #output:Landroid/filterfw/core/Frame;
    .end local v18           #subtractInputs:[Landroid/filterfw/core/Frame;
    :cond_589
    invoke-virtual/range {p1 .. p1}, Landroid/filterfw/core/FilterContext;->getGLEnvironment()Landroid/filterfw/core/GLEnvironment;

    #@58c
    move-result-object v21

    #@58d
    invoke-virtual/range {v21 .. v21}, Landroid/filterfw/core/GLEnvironment;->activate()V

    #@590
    .line 852
    invoke-static {}, Landroid/opengl/GLES20;->glFinish()V

    #@593
    .line 853
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@596
    move-result-wide v9

    #@597
    .line 854
    .local v9, endTime:J
    const-string v21, "BackDropperFilter"

    #@599
    new-instance v22, Ljava/lang/StringBuilder;

    #@59b
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@59e
    const-string v23, "Avg. frame duration: "

    #@5a0
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a3
    move-result-object v22

    #@5a4
    const-string v23, "%.2f"

    #@5a6
    const/16 v24, 0x1

    #@5a8
    move/from16 v0, v24

    #@5aa
    new-array v0, v0, [Ljava/lang/Object;

    #@5ac
    move-object/from16 v24, v0

    #@5ae
    const/16 v25, 0x0

    #@5b0
    move-object/from16 v0, p0

    #@5b2
    iget-wide v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->startTime:J

    #@5b4
    move-wide/from16 v26, v0

    #@5b6
    sub-long v26, v9, v26

    #@5b8
    move-wide/from16 v0, v26

    #@5ba
    long-to-double v0, v0

    #@5bb
    move-wide/from16 v26, v0

    #@5bd
    const-wide/high16 v28, 0x403e

    #@5bf
    div-double v26, v26, v28

    #@5c1
    invoke-static/range {v26 .. v27}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    #@5c4
    move-result-object v26

    #@5c5
    aput-object v26, v24, v25

    #@5c7
    invoke-static/range {v23 .. v24}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@5ca
    move-result-object v23

    #@5cb
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5ce
    move-result-object v22

    #@5cf
    const-string v23, " ms. Avg. fps: "

    #@5d1
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d4
    move-result-object v22

    #@5d5
    const-string v23, "%.2f"

    #@5d7
    const/16 v24, 0x1

    #@5d9
    move/from16 v0, v24

    #@5db
    new-array v0, v0, [Ljava/lang/Object;

    #@5dd
    move-object/from16 v24, v0

    #@5df
    const/16 v25, 0x0

    #@5e1
    const-wide v26, 0x408f400000000000L

    #@5e6
    move-object/from16 v0, p0

    #@5e8
    iget-wide v0, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->startTime:J

    #@5ea
    move-wide/from16 v28, v0

    #@5ec
    sub-long v28, v9, v28

    #@5ee
    move-wide/from16 v0, v28

    #@5f0
    long-to-double v0, v0

    #@5f1
    move-wide/from16 v28, v0

    #@5f3
    const-wide/high16 v30, 0x403e

    #@5f5
    div-double v28, v28, v30

    #@5f7
    div-double v26, v26, v28

    #@5f9
    invoke-static/range {v26 .. v27}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    #@5fc
    move-result-object v26

    #@5fd
    aput-object v26, v24, v25

    #@5ff
    invoke-static/range {v23 .. v24}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@602
    move-result-object v23

    #@603
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@606
    move-result-object v22

    #@607
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60a
    move-result-object v22

    #@60b
    invoke-static/range {v21 .. v22}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@60e
    .line 856
    move-object/from16 v0, p0

    #@610
    iput-wide v9, v0, Landroid/filterpacks/videoproc/BackDropperFilter;->startTime:J

    #@612
    goto/16 :goto_4a0
.end method

.method public declared-synchronized relearn()V
    .registers 2

    #@0
    .prologue
    .line 888
    monitor-enter p0

    #@1
    const/4 v0, 0x1

    #@2
    :try_start_2
    iput-boolean v0, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mStartLearning:Z
    :try_end_4
    .catchall {:try_start_2 .. :try_end_4} :catchall_6

    #@4
    .line 889
    monitor-exit p0

    #@5
    return-void

    #@6
    .line 888
    :catchall_6
    move-exception v0

    #@7
    monitor-exit p0

    #@8
    throw v0
.end method

.method public setupPorts()V
    .registers 9

    #@0
    .prologue
    .line 534
    const/4 v6, 0x3

    #@1
    const/4 v7, 0x0

    #@2
    invoke-static {v6, v7}, Landroid/filterfw/format/ImageFormat;->create(II)Landroid/filterfw/core/MutableFrameFormat;

    #@5
    move-result-object v2

    #@6
    .line 536
    .local v2, imageFormat:Landroid/filterfw/core/FrameFormat;
    sget-object v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mInputNames:[Ljava/lang/String;

    #@8
    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    #@9
    .local v4, len$:I
    const/4 v1, 0x0

    #@a
    .local v1, i$:I
    :goto_a
    if-ge v1, v4, :cond_14

    #@c
    aget-object v3, v0, v1

    #@e
    .line 537
    .local v3, inputName:Ljava/lang/String;
    invoke-virtual {p0, v3, v2}, Landroid/filterpacks/videoproc/BackDropperFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@11
    .line 536
    add-int/lit8 v1, v1, 0x1

    #@13
    goto :goto_a

    #@14
    .line 540
    .end local v3           #inputName:Ljava/lang/String;
    :cond_14
    sget-object v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mOutputNames:[Ljava/lang/String;

    #@16
    array-length v4, v0

    #@17
    const/4 v1, 0x0

    #@18
    :goto_18
    if-ge v1, v4, :cond_25

    #@1a
    aget-object v5, v0, v1

    #@1c
    .line 541
    .local v5, outputName:Ljava/lang/String;
    const-string/jumbo v6, "video"

    #@1f
    invoke-virtual {p0, v5, v6}, Landroid/filterpacks/videoproc/BackDropperFilter;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    .line 540
    add-int/lit8 v1, v1, 0x1

    #@24
    goto :goto_18

    #@25
    .line 545
    .end local v5           #outputName:Ljava/lang/String;
    :cond_25
    iget-boolean v6, p0, Landroid/filterpacks/videoproc/BackDropperFilter;->mProvideDebugOutputs:Z

    #@27
    if-eqz v6, :cond_3a

    #@29
    .line 546
    sget-object v0, Landroid/filterpacks/videoproc/BackDropperFilter;->mDebugOutputNames:[Ljava/lang/String;

    #@2b
    array-length v4, v0

    #@2c
    const/4 v1, 0x0

    #@2d
    :goto_2d
    if-ge v1, v4, :cond_3a

    #@2f
    aget-object v5, v0, v1

    #@31
    .line 547
    .restart local v5       #outputName:Ljava/lang/String;
    const-string/jumbo v6, "video"

    #@34
    invoke-virtual {p0, v5, v6}, Landroid/filterpacks/videoproc/BackDropperFilter;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    #@37
    .line 546
    add-int/lit8 v1, v1, 0x1

    #@39
    goto :goto_2d

    #@3a
    .line 550
    .end local v5           #outputName:Ljava/lang/String;
    :cond_3a
    return-void
.end method
