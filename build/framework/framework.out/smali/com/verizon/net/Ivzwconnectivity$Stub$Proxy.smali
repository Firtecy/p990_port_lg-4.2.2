.class Lcom/verizon/net/Ivzwconnectivity$Stub$Proxy;
.super Ljava/lang/Object;
.source "Ivzwconnectivity.java"

# interfaces
.implements Lcom/verizon/net/Ivzwconnectivity;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/verizon/net/Ivzwconnectivity$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 76
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 77
    iput-object p1, p0, Lcom/verizon/net/Ivzwconnectivity$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 78
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 81
    iget-object v0, p0, Lcom/verizon/net/Ivzwconnectivity$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getAPNUsageRxBytes(Ljava/lang/String;I)J
    .registers 10
    .parameter "apnType"
    .parameter "networkType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 89
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 90
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 93
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "com.verizon.net.Ivzwconnectivity"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 94
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 95
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 96
    iget-object v4, p0, Lcom/verizon/net/Ivzwconnectivity$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/4 v5, 0x1

    #@16
    const/4 v6, 0x0

    #@17
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1a
    .line 97
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1d
    .line 98
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J
    :try_end_20
    .catchall {:try_start_8 .. :try_end_20} :catchall_28

    #@20
    move-result-wide v2

    #@21
    .line 101
    .local v2, _result:J
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 102
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 104
    return-wide v2

    #@28
    .line 101
    .end local v2           #_result:J
    :catchall_28
    move-exception v4

    #@29
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 102
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    throw v4
.end method

.method public getAPNUsageTxBytes(Ljava/lang/String;I)J
    .registers 10
    .parameter "apnType"
    .parameter "networkType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 108
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 109
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 112
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "com.verizon.net.Ivzwconnectivity"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 113
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 114
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 115
    iget-object v4, p0, Lcom/verizon/net/Ivzwconnectivity$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/4 v5, 0x2

    #@16
    const/4 v6, 0x0

    #@17
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1a
    .line 116
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1d
    .line 117
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J
    :try_end_20
    .catchall {:try_start_8 .. :try_end_20} :catchall_28

    #@20
    move-result-wide v2

    #@21
    .line 120
    .local v2, _result:J
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 121
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 123
    return-wide v2

    #@28
    .line 120
    .end local v2           #_result:J
    :catchall_28
    move-exception v4

    #@29
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 121
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    throw v4
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 85
    const-string v0, "com.verizon.net.Ivzwconnectivity"

    #@2
    return-object v0
.end method
