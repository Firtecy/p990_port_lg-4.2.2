.class public Lcom/verizon/net/ConnectivityService;
.super Lcom/verizon/net/Ivzwconnectivity$Stub;
.source "ConnectivityService.java"


# static fields
.field private static final DBG:Z = false

.field private static final TAG:Ljava/lang/String; = "VZWConnectivityService"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 23
    invoke-direct {p0}, Lcom/verizon/net/Ivzwconnectivity$Stub;-><init>()V

    #@3
    .line 24
    iput-object p1, p0, Lcom/verizon/net/ConnectivityService;->mContext:Landroid/content/Context;

    #@5
    .line 25
    return-void
.end method

.method private log(Ljava/lang/String;)V
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 62
    const-string v0, "VZWConnectivityService"

    #@2
    invoke-static {v0, p1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 63
    return-void
.end method


# virtual methods
.method public getAPNUsageRxBytes(Ljava/lang/String;I)J
    .registers 13
    .parameter "apnType"
    .parameter "networkType"

    #@0
    .prologue
    const-wide/16 v6, -0x1

    #@2
    .line 28
    const-string/jumbo v8, "network_management"

    #@5
    invoke-static {v8}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@8
    move-result-object v2

    #@9
    .line 29
    .local v2, b:Landroid/os/IBinder;
    invoke-static {v2}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@c
    move-result-object v5

    #@d
    .line 30
    .local v5, service:Landroid/os/INetworkManagementService;
    const-string/jumbo v8, "ril.current.default_iface"

    #@10
    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@13
    move-result-object v3

    #@14
    .line 31
    .local v3, default_rmnet_iface:Ljava/lang/String;
    if-nez v5, :cond_18

    #@16
    move-wide v0, v6

    #@17
    .line 41
    :goto_17
    return-wide v0

    #@18
    .line 33
    :cond_18
    if-nez p2, :cond_45

    #@1a
    .line 34
    :try_start_1a
    invoke-interface {v5, v3}, Landroid/os/INetworkManagementService;->getInterfaceRxCounter(Ljava/lang/String;)J

    #@1d
    move-result-wide v0

    #@1e
    .line 35
    .local v0, InternetUsageRxBytes:J
    new-instance v8, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v9, "VZWINTERNET APN:"

    #@25
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v8

    #@29
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v8

    #@2d
    const-string v9, " getAPNUsageRxBytes = "

    #@2f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v8

    #@33
    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@36
    move-result-object v8

    #@37
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v8

    #@3b
    invoke-direct {p0, v8}, Lcom/verizon/net/ConnectivityService;->log(Ljava/lang/String;)V
    :try_end_3e
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_3e} :catch_3f

    #@3e
    goto :goto_17

    #@3f
    .line 38
    .end local v0           #InternetUsageRxBytes:J
    :catch_3f
    move-exception v4

    #@40
    .line 39
    .local v4, e:Ljava/lang/Exception;
    const-string v8, "getAPNUsageRxBytes FAIL"

    #@42
    invoke-direct {p0, v8}, Lcom/verizon/net/ConnectivityService;->log(Ljava/lang/String;)V

    #@45
    .end local v4           #e:Ljava/lang/Exception;
    :cond_45
    move-wide v0, v6

    #@46
    .line 41
    goto :goto_17
.end method

.method public getAPNUsageTxBytes(Ljava/lang/String;I)J
    .registers 13
    .parameter "apnType"
    .parameter "networkType"

    #@0
    .prologue
    const-wide/16 v6, -0x1

    #@2
    .line 45
    const-string/jumbo v8, "network_management"

    #@5
    invoke-static {v8}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@8
    move-result-object v2

    #@9
    .line 46
    .local v2, b:Landroid/os/IBinder;
    invoke-static {v2}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@c
    move-result-object v5

    #@d
    .line 47
    .local v5, service:Landroid/os/INetworkManagementService;
    const-string/jumbo v8, "ril.current.default_iface"

    #@10
    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@13
    move-result-object v3

    #@14
    .line 48
    .local v3, default_rmnet_iface:Ljava/lang/String;
    if-nez v5, :cond_18

    #@16
    move-wide v0, v6

    #@17
    .line 58
    :goto_17
    return-wide v0

    #@18
    .line 50
    :cond_18
    if-nez p2, :cond_45

    #@1a
    .line 51
    :try_start_1a
    invoke-interface {v5, v3}, Landroid/os/INetworkManagementService;->getInterfaceTxCounter(Ljava/lang/String;)J

    #@1d
    move-result-wide v0

    #@1e
    .line 52
    .local v0, InternetUsageTxBytes:J
    new-instance v8, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v9, "VZWINTERNET APN:"

    #@25
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v8

    #@29
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v8

    #@2d
    const-string v9, " getAPNUsageTxBytes = "

    #@2f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v8

    #@33
    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@36
    move-result-object v8

    #@37
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v8

    #@3b
    invoke-direct {p0, v8}, Lcom/verizon/net/ConnectivityService;->log(Ljava/lang/String;)V
    :try_end_3e
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_3e} :catch_3f

    #@3e
    goto :goto_17

    #@3f
    .line 55
    .end local v0           #InternetUsageTxBytes:J
    :catch_3f
    move-exception v4

    #@40
    .line 56
    .local v4, e:Ljava/lang/Exception;
    const-string v8, "InternetUsageTxBytes FAIL"

    #@42
    invoke-direct {p0, v8}, Lcom/verizon/net/ConnectivityService;->log(Ljava/lang/String;)V

    #@45
    .end local v4           #e:Ljava/lang/Exception;
    :cond_45
    move-wide v0, v6

    #@46
    .line 58
    goto :goto_17
.end method
