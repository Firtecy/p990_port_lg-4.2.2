.class public Lcom/verizon/net/ConnectivityManager;
.super Ljava/lang/Object;
.source "ConnectivityManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "VZWConnectivityManager"


# instance fields
.field private mService:Lcom/verizon/net/Ivzwconnectivity;


# direct methods
.method public constructor <init>(Lcom/verizon/net/Ivzwconnectivity;)V
    .registers 2
    .parameter "service"

    #@0
    .prologue
    .line 15
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 16
    iput-object p1, p0, Lcom/verizon/net/ConnectivityManager;->mService:Lcom/verizon/net/Ivzwconnectivity;

    #@5
    .line 17
    return-void
.end method

.method private log(Ljava/lang/String;)V
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 38
    const-string v0, "VZWConnectivityManager"

    #@2
    invoke-static {v0, p1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 39
    return-void
.end method


# virtual methods
.method public getAPNUsageRxBytes(Ljava/lang/String;I)J
    .registers 6
    .parameter "apnType"
    .parameter "networkType"

    #@0
    .prologue
    .line 20
    const-string v1, "getAPNUsageRxBytes"

    #@2
    invoke-direct {p0, v1}, Lcom/verizon/net/ConnectivityManager;->log(Ljava/lang/String;)V

    #@5
    .line 22
    :try_start_5
    iget-object v1, p0, Lcom/verizon/net/ConnectivityManager;->mService:Lcom/verizon/net/Ivzwconnectivity;

    #@7
    invoke-interface {v1, p1, p2}, Lcom/verizon/net/Ivzwconnectivity;->getAPNUsageRxBytes(Ljava/lang/String;I)J
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_a} :catch_c

    #@a
    move-result-wide v1

    #@b
    .line 24
    :goto_b
    return-wide v1

    #@c
    .line 23
    :catch_c
    move-exception v0

    #@d
    .line 24
    .local v0, e:Landroid/os/RemoteException;
    const-wide/16 v1, -0x1

    #@f
    goto :goto_b
.end method

.method public getAPNUsageTxBytes(Ljava/lang/String;I)J
    .registers 6
    .parameter "apnType"
    .parameter "networkType"

    #@0
    .prologue
    .line 29
    const-string v1, "getAPNUsageTxBytes"

    #@2
    invoke-direct {p0, v1}, Lcom/verizon/net/ConnectivityManager;->log(Ljava/lang/String;)V

    #@5
    .line 31
    :try_start_5
    iget-object v1, p0, Lcom/verizon/net/ConnectivityManager;->mService:Lcom/verizon/net/Ivzwconnectivity;

    #@7
    invoke-interface {v1, p1, p2}, Lcom/verizon/net/Ivzwconnectivity;->getAPNUsageTxBytes(Ljava/lang/String;I)J
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_a} :catch_c

    #@a
    move-result-wide v1

    #@b
    .line 33
    :goto_b
    return-wide v1

    #@c
    .line 32
    :catch_c
    move-exception v0

    #@d
    .line 33
    .local v0, e:Landroid/os/RemoteException;
    const-wide/16 v1, -0x1

    #@f
    goto :goto_b
.end method
