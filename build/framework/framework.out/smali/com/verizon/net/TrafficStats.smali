.class public Lcom/verizon/net/TrafficStats;
.super Landroid/net/TrafficStats;
.source "TrafficStats.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "vzwTrafficStats"


# instance fields
.field private vzwNetworkStats:Landroid/net/NetworkStats;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 40
    invoke-direct {p0}, Landroid/net/TrafficStats;-><init>()V

    #@3
    .line 41
    return-void
.end method

.method private log(Ljava/lang/String;)V
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 176
    const-string/jumbo v0, "vzwTrafficStats"

    #@3
    invoke-static {v0, p1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6
    .line 177
    return-void
.end method


# virtual methods
.method public getInterfaceRxBytes(Ljava/lang/String;)J
    .registers 6
    .parameter "iface"

    #@0
    .prologue
    .line 73
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v3, "[vzw_stats] getInterfaceRxBytes - iface : "

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-direct {p0, v2}, Lcom/verizon/net/TrafficStats;->log(Ljava/lang/String;)V

    #@16
    .line 74
    const-wide/16 v0, 0x0

    #@18
    .line 75
    .local v0, result:J
    if-eqz p1, :cond_34

    #@1a
    .line 76
    invoke-static {p1}, Landroid/net/TrafficStats;->getRxBytes(Ljava/lang/String;)J

    #@1d
    move-result-wide v0

    #@1e
    .line 77
    new-instance v2, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v3, "[vzw_stats] getInterfaceRxBytes : "

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    invoke-direct {p0, v2}, Lcom/verizon/net/TrafficStats;->log(Ljava/lang/String;)V

    #@34
    .line 79
    :cond_34
    return-wide v0
.end method

.method public getInterfaceRxPackets(Ljava/lang/String;)J
    .registers 6
    .parameter "iface"

    #@0
    .prologue
    .line 53
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v3, "[vzw_stats] getInterfaceRxPackets - iface : "

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-direct {p0, v2}, Lcom/verizon/net/TrafficStats;->log(Ljava/lang/String;)V

    #@16
    .line 54
    const-wide/16 v0, 0x0

    #@18
    .line 55
    .local v0, result:J
    if-eqz p1, :cond_34

    #@1a
    .line 56
    invoke-static {p1}, Landroid/net/TrafficStats;->getRxPackets(Ljava/lang/String;)J

    #@1d
    move-result-wide v0

    #@1e
    .line 57
    new-instance v2, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v3, "[vzw_stats] getInterfaceRxPackets : "

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    invoke-direct {p0, v2}, Lcom/verizon/net/TrafficStats;->log(Ljava/lang/String;)V

    #@34
    .line 59
    :cond_34
    return-wide v0
.end method

.method public getInterfaceTxBytes(Ljava/lang/String;)J
    .registers 6
    .parameter "iface"

    #@0
    .prologue
    .line 63
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v3, "[vzw_stats] getInterfaceTxBytes - iface : "

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-direct {p0, v2}, Lcom/verizon/net/TrafficStats;->log(Ljava/lang/String;)V

    #@16
    .line 64
    const-wide/16 v0, 0x0

    #@18
    .line 65
    .local v0, result:J
    if-eqz p1, :cond_34

    #@1a
    .line 66
    invoke-static {p1}, Landroid/net/TrafficStats;->getTxBytes(Ljava/lang/String;)J

    #@1d
    move-result-wide v0

    #@1e
    .line 67
    new-instance v2, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v3, "[vzw_stats] getInterfaceTxBytes : "

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    invoke-direct {p0, v2}, Lcom/verizon/net/TrafficStats;->log(Ljava/lang/String;)V

    #@34
    .line 69
    :cond_34
    return-wide v0
.end method

.method public getInterfaceTxPackets(Ljava/lang/String;)J
    .registers 6
    .parameter "iface"

    #@0
    .prologue
    .line 44
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v3, "[vzw_stats] getInterfaceTxPackets - iface : "

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-direct {p0, v2}, Lcom/verizon/net/TrafficStats;->log(Ljava/lang/String;)V

    #@16
    .line 45
    const-wide/16 v0, 0x0

    #@18
    .line 46
    .local v0, result:J
    if-eqz p1, :cond_34

    #@1a
    .line 47
    invoke-static {p1}, Landroid/net/TrafficStats;->getTxPackets(Ljava/lang/String;)J

    #@1d
    move-result-wide v0

    #@1e
    .line 48
    new-instance v2, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v3, "[vzw_stats] getInterfaceTxPackets : "

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    invoke-direct {p0, v2}, Lcom/verizon/net/TrafficStats;->log(Ljava/lang/String;)V

    #@34
    .line 50
    :cond_34
    return-wide v0
.end method

.method public getUidInterfaceRxBytes(ILjava/lang/String;)J
    .registers 10
    .parameter "uid"
    .parameter "iface"

    #@0
    .prologue
    .line 91
    new-instance v5, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v6, "[vzw_stats] getUidInterfaceRxBytes - uid: "

    #@7
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v5

    #@b
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v5

    #@f
    const-string v6, ", iface : "

    #@11
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v5

    #@15
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v5

    #@19
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v5

    #@1d
    invoke-direct {p0, v5}, Lcom/verizon/net/TrafficStats;->log(Ljava/lang/String;)V

    #@20
    .line 93
    const-wide/16 v2, 0x0

    #@22
    .line 95
    .local v2, result:J
    const-string/jumbo v5, "network_management"

    #@25
    invoke-static {v5}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@28
    move-result-object v0

    #@29
    .line 96
    .local v0, b:Landroid/os/IBinder;
    invoke-static {v0}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@2c
    move-result-object v4

    #@2d
    .line 98
    .local v4, service:Landroid/os/INetworkManagementService;
    if-nez v4, :cond_37

    #@2f
    .line 99
    const-string v5, "[vzw_stats] service is null"

    #@31
    invoke-direct {p0, v5}, Lcom/verizon/net/TrafficStats;->log(Ljava/lang/String;)V

    #@34
    .line 100
    const-wide/16 v5, -0x1

    #@36
    .line 108
    :goto_36
    return-wide v5

    #@37
    .line 103
    :cond_37
    const/4 v5, 0x1

    #@38
    :try_start_38
    invoke-interface {v4, p1, p2, v5}, Landroid/os/INetworkManagementService;->getNetworkStatsUidInterface(ILjava/lang/String;I)J

    #@3b
    move-result-wide v2

    #@3c
    .line 104
    new-instance v5, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v6, "[vzw_stats] getUidInterfaceRxBytes : "

    #@43
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v5

    #@47
    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v5

    #@4b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v5

    #@4f
    invoke-direct {p0, v5}, Lcom/verizon/net/TrafficStats;->log(Ljava/lang/String;)V
    :try_end_52
    .catch Ljava/lang/Exception; {:try_start_38 .. :try_end_52} :catch_54

    #@52
    :goto_52
    move-wide v5, v2

    #@53
    .line 108
    goto :goto_36

    #@54
    .line 105
    :catch_54
    move-exception v1

    #@55
    .line 106
    .local v1, e:Ljava/lang/Exception;
    const-string v5, "[vzw_stats] getUidInterfaceRxBytes FAIL"

    #@57
    invoke-direct {p0, v5}, Lcom/verizon/net/TrafficStats;->log(Ljava/lang/String;)V

    #@5a
    goto :goto_52
.end method

.method public getUidInterfaceRxPackets(ILjava/lang/String;)J
    .registers 10
    .parameter "uid"
    .parameter "iface"

    #@0
    .prologue
    .line 112
    new-instance v5, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v6, "[vzw_stats] getUidInterfaceRxPackets - uid: "

    #@7
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v5

    #@b
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v5

    #@f
    const-string v6, ", iface : "

    #@11
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v5

    #@15
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v5

    #@19
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v5

    #@1d
    invoke-direct {p0, v5}, Lcom/verizon/net/TrafficStats;->log(Ljava/lang/String;)V

    #@20
    .line 114
    const-wide/16 v2, 0x0

    #@22
    .line 116
    .local v2, result:J
    const-string/jumbo v5, "network_management"

    #@25
    invoke-static {v5}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@28
    move-result-object v0

    #@29
    .line 117
    .local v0, b:Landroid/os/IBinder;
    invoke-static {v0}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@2c
    move-result-object v4

    #@2d
    .line 119
    .local v4, service:Landroid/os/INetworkManagementService;
    if-nez v4, :cond_37

    #@2f
    .line 120
    const-string v5, "[vzw_stats] service is null"

    #@31
    invoke-direct {p0, v5}, Lcom/verizon/net/TrafficStats;->log(Ljava/lang/String;)V

    #@34
    .line 121
    const-wide/16 v5, -0x1

    #@36
    .line 129
    :goto_36
    return-wide v5

    #@37
    .line 124
    :cond_37
    const/4 v5, 0x2

    #@38
    :try_start_38
    invoke-interface {v4, p1, p2, v5}, Landroid/os/INetworkManagementService;->getNetworkStatsUidInterface(ILjava/lang/String;I)J

    #@3b
    move-result-wide v2

    #@3c
    .line 125
    new-instance v5, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v6, "[vzw_stats] getUidInterfaceRxPackets : "

    #@43
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v5

    #@47
    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v5

    #@4b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v5

    #@4f
    invoke-direct {p0, v5}, Lcom/verizon/net/TrafficStats;->log(Ljava/lang/String;)V
    :try_end_52
    .catch Ljava/lang/Exception; {:try_start_38 .. :try_end_52} :catch_54

    #@52
    :goto_52
    move-wide v5, v2

    #@53
    .line 129
    goto :goto_36

    #@54
    .line 126
    :catch_54
    move-exception v1

    #@55
    .line 127
    .local v1, e:Ljava/lang/Exception;
    const-string v5, "[vzw_stats] getUidInterfaceRxPackets FAIL"

    #@57
    invoke-direct {p0, v5}, Lcom/verizon/net/TrafficStats;->log(Ljava/lang/String;)V

    #@5a
    goto :goto_52
.end method

.method public getUidInterfaceTxBytes(ILjava/lang/String;)J
    .registers 10
    .parameter "uid"
    .parameter "iface"

    #@0
    .prologue
    .line 133
    new-instance v5, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v6, "[vzw_stats] getUidInterfaceTxBytes - uid: "

    #@7
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v5

    #@b
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v5

    #@f
    const-string v6, ", iface : "

    #@11
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v5

    #@15
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v5

    #@19
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v5

    #@1d
    invoke-direct {p0, v5}, Lcom/verizon/net/TrafficStats;->log(Ljava/lang/String;)V

    #@20
    .line 135
    const-wide/16 v2, 0x0

    #@22
    .line 137
    .local v2, result:J
    const-string/jumbo v5, "network_management"

    #@25
    invoke-static {v5}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@28
    move-result-object v0

    #@29
    .line 138
    .local v0, b:Landroid/os/IBinder;
    invoke-static {v0}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@2c
    move-result-object v4

    #@2d
    .line 140
    .local v4, service:Landroid/os/INetworkManagementService;
    if-nez v4, :cond_37

    #@2f
    .line 141
    const-string v5, "[vzw_stats] service is null"

    #@31
    invoke-direct {p0, v5}, Lcom/verizon/net/TrafficStats;->log(Ljava/lang/String;)V

    #@34
    .line 142
    const-wide/16 v5, -0x1

    #@36
    .line 150
    :goto_36
    return-wide v5

    #@37
    .line 145
    :cond_37
    const/4 v5, 0x3

    #@38
    :try_start_38
    invoke-interface {v4, p1, p2, v5}, Landroid/os/INetworkManagementService;->getNetworkStatsUidInterface(ILjava/lang/String;I)J

    #@3b
    move-result-wide v2

    #@3c
    .line 146
    new-instance v5, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v6, "[vzw_stats] getUidInterfaceTxBytes : "

    #@43
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v5

    #@47
    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v5

    #@4b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v5

    #@4f
    invoke-direct {p0, v5}, Lcom/verizon/net/TrafficStats;->log(Ljava/lang/String;)V
    :try_end_52
    .catch Ljava/lang/Exception; {:try_start_38 .. :try_end_52} :catch_54

    #@52
    :goto_52
    move-wide v5, v2

    #@53
    .line 150
    goto :goto_36

    #@54
    .line 147
    :catch_54
    move-exception v1

    #@55
    .line 148
    .local v1, e:Ljava/lang/Exception;
    const-string v5, "[vzw_stats] getUidInterfaceTxBytes FAIL"

    #@57
    invoke-direct {p0, v5}, Lcom/verizon/net/TrafficStats;->log(Ljava/lang/String;)V

    #@5a
    goto :goto_52
.end method

.method public getUidInterfaceTxPackets(ILjava/lang/String;)J
    .registers 10
    .parameter "uid"
    .parameter "iface"

    #@0
    .prologue
    .line 154
    new-instance v5, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v6, "[vzw_stats] getUidInterfaceTxPackets - uid: "

    #@7
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v5

    #@b
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v5

    #@f
    const-string v6, ", iface : "

    #@11
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v5

    #@15
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v5

    #@19
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v5

    #@1d
    invoke-direct {p0, v5}, Lcom/verizon/net/TrafficStats;->log(Ljava/lang/String;)V

    #@20
    .line 156
    const-wide/16 v2, 0x0

    #@22
    .line 158
    .local v2, result:J
    const-string/jumbo v5, "network_management"

    #@25
    invoke-static {v5}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@28
    move-result-object v0

    #@29
    .line 159
    .local v0, b:Landroid/os/IBinder;
    invoke-static {v0}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@2c
    move-result-object v4

    #@2d
    .line 161
    .local v4, service:Landroid/os/INetworkManagementService;
    if-nez v4, :cond_37

    #@2f
    .line 162
    const-string v5, "[vzw_stats] service is null"

    #@31
    invoke-direct {p0, v5}, Lcom/verizon/net/TrafficStats;->log(Ljava/lang/String;)V

    #@34
    .line 163
    const-wide/16 v5, -0x1

    #@36
    .line 171
    :goto_36
    return-wide v5

    #@37
    .line 166
    :cond_37
    const/4 v5, 0x4

    #@38
    :try_start_38
    invoke-interface {v4, p1, p2, v5}, Landroid/os/INetworkManagementService;->getNetworkStatsUidInterface(ILjava/lang/String;I)J

    #@3b
    move-result-wide v2

    #@3c
    .line 167
    new-instance v5, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v6, "[vzw_stats] getUidInterfaceTxPackets : "

    #@43
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v5

    #@47
    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v5

    #@4b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v5

    #@4f
    invoke-direct {p0, v5}, Lcom/verizon/net/TrafficStats;->log(Ljava/lang/String;)V
    :try_end_52
    .catch Ljava/lang/Exception; {:try_start_38 .. :try_end_52} :catch_54

    #@52
    :goto_52
    move-wide v5, v2

    #@53
    .line 171
    goto :goto_36

    #@54
    .line 168
    :catch_54
    move-exception v1

    #@55
    .line 169
    .local v1, e:Ljava/lang/Exception;
    const-string v5, "[vzw_stats] getUidInterfaceTxPackets FAIL"

    #@57
    invoke-direct {p0, v5}, Lcom/verizon/net/TrafficStats;->log(Ljava/lang/String;)V

    #@5a
    goto :goto_52
.end method
