.class public abstract Lcom/verizon/net/Ivzwconnectivity$Stub;
.super Landroid/os/Binder;
.source "Ivzwconnectivity.java"

# interfaces
.implements Lcom/verizon/net/Ivzwconnectivity;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/verizon/net/Ivzwconnectivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/verizon/net/Ivzwconnectivity$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.verizon.net.Ivzwconnectivity"

.field static final TRANSACTION_getAPNUsageRxBytes:I = 0x1

.field static final TRANSACTION_getAPNUsageTxBytes:I = 0x2


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.verizon.net.Ivzwconnectivity"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/verizon/net/Ivzwconnectivity$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/verizon/net/Ivzwconnectivity;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.verizon.net.Ivzwconnectivity"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/verizon/net/Ivzwconnectivity;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/verizon/net/Ivzwconnectivity;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/verizon/net/Ivzwconnectivity$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/verizon/net/Ivzwconnectivity$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 11
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 38
    sparse-switch p1, :sswitch_data_40

    #@4
    .line 70
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v4

    #@8
    :goto_8
    return v4

    #@9
    .line 42
    :sswitch_9
    const-string v5, "com.verizon.net.Ivzwconnectivity"

    #@b
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 47
    :sswitch_f
    const-string v5, "com.verizon.net.Ivzwconnectivity"

    #@11
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@17
    move-result-object v0

    #@18
    .line 51
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v1

    #@1c
    .line 52
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/verizon/net/Ivzwconnectivity$Stub;->getAPNUsageRxBytes(Ljava/lang/String;I)J

    #@1f
    move-result-wide v2

    #@20
    .line 53
    .local v2, _result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@23
    .line 54
    invoke-virtual {p3, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    #@26
    goto :goto_8

    #@27
    .line 59
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:I
    .end local v2           #_result:J
    :sswitch_27
    const-string v5, "com.verizon.net.Ivzwconnectivity"

    #@29
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2c
    .line 61
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2f
    move-result-object v0

    #@30
    .line 63
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@33
    move-result v1

    #@34
    .line 64
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/verizon/net/Ivzwconnectivity$Stub;->getAPNUsageTxBytes(Ljava/lang/String;I)J

    #@37
    move-result-wide v2

    #@38
    .line 65
    .restart local v2       #_result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3b
    .line 66
    invoke-virtual {p3, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    #@3e
    goto :goto_8

    #@3f
    .line 38
    nop

    #@40
    :sswitch_data_40
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_27
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
