.class public Lcom/kt/wifiapi/OEMExtension;
.super Ljava/lang/Object;
.source "OEMExtension.java"


# static fields
.field public static final DP_MODE_CITY:I = 0x1

.field public static final DP_MODE_GENERAL:I = 0x2

.field public static final DP_MODE_HOME:I = 0x3

.field public static final DP_MODE_OFF:I = 0x0

.field public static final EAP_AKA_NOTIFICATION:Ljava/lang/String; = "EAP_AKA_NOTIFICATION"

.field public static final EAP_NOTIFICATION:Ljava/lang/String; = "EAP_NOTIFICATION"

.field public static final ERROR_NOTIFICATION:Ljava/lang/String; = "ERROR_NOTIFICATION"

.field public static final ERROR_NOTIFICATION_AUTH_FAIL:I = -0x5

.field public static final ERROR_NOTIFICATION_IDPW_MISPATCH:I = -0x4

.field public static final ERROR_NOTIFICATION_INVALID_USIM:I = -0x1

.field public static final ERROR_NOTIFICATION_NO_RESPONSE:I = -0x6

.field public static final ERROR_NOTIFICATION_NO_USIM:I = -0x2

.field public static final ERROR_NOTIFICATION_PW_MISPATCH:I = -0x3

.field public static final FEATURE_KT_WIFIAPI_OEM_DISCONNECTION_PRIORITY:I = 0x2

.field public static final FEATURE_KT_WIFIAPI_OEM_EAP_AKA_NOTIFICATON:I = 0x10

.field public static final FEATURE_KT_WIFIAPI_OEM_EAP_NOTIFICATON:I = 0x8

.field public static final FEATURE_KT_WIFIAPI_OEM_ERROR_NOTIFICATON:I = 0x4

.field public static final FEATURE_KT_WIFIAPI_OEM_MANUAL_CONNECTION:I = 0x1

.field public static final FEATURE_KT_WIFIAPI_OEM_SCAN_RESULT_EXTENSION:I = 0x20

.field public static final MANUAL_CONN_ENABLE_QUERY:Ljava/lang/String; = "com.kt.wifiapi.OEMExtension.MANUAL_CONN_ENABLE_QUERY"

.field public static final NOTIFICATION:Ljava/lang/String; = "com.kt.wifiapi.OEMExtension.NOTIFICATION"

.field private static final TAG:Ljava/lang/String; = "WifiKTAPI"

.field private static mContext:Landroid/content/Context;

.field private static mGWSScanResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/kt/wifiapi/GWSScanResult;",
            ">;"
        }
    .end annotation
.end field

.field private static final mReceiver:Landroid/content/BroadcastReceiver;

.field private static mWifiLgeKTOEMExt:Lcom/kt/wifiapi/OEMExtension;


# instance fields
.field private mDisconnectionPriority:I

.field private mFeatureSupport:I

.field private mManualConnection:Z

.field private mWifiOn:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 23
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/kt/wifiapi/OEMExtension;->mWifiLgeKTOEMExt:Lcom/kt/wifiapi/OEMExtension;

    #@3
    .line 71
    new-instance v0, Lcom/kt/wifiapi/OEMExtension$1;

    #@5
    invoke-direct {v0}, Lcom/kt/wifiapi/OEMExtension$1;-><init>()V

    #@8
    sput-object v0, Lcom/kt/wifiapi/OEMExtension;->mReceiver:Landroid/content/BroadcastReceiver;

    #@a
    return-void
.end method

.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 67
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 25
    iput-boolean v0, p0, Lcom/kt/wifiapi/OEMExtension;->mWifiOn:Z

    #@6
    .line 26
    iput-boolean v0, p0, Lcom/kt/wifiapi/OEMExtension;->mManualConnection:Z

    #@8
    .line 36
    iput v0, p0, Lcom/kt/wifiapi/OEMExtension;->mFeatureSupport:I

    #@a
    .line 47
    const/4 v0, 0x2

    #@b
    iput v0, p0, Lcom/kt/wifiapi/OEMExtension;->mDisconnectionPriority:I

    #@d
    .line 68
    return-void
.end method

.method static synthetic access$002(Ljava/util/List;)Ljava/util/List;
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 20
    sput-object p0, Lcom/kt/wifiapi/OEMExtension;->mGWSScanResults:Ljava/util/List;

    #@2
    return-object p0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/kt/wifiapi/OEMExtension;
    .registers 6
    .parameter "context"

    #@0
    .prologue
    .line 85
    sget-object v2, Lcom/kt/wifiapi/OEMExtension;->mWifiLgeKTOEMExt:Lcom/kt/wifiapi/OEMExtension;

    #@2
    if-nez v2, :cond_12

    #@4
    .line 86
    new-instance v2, Lcom/kt/wifiapi/OEMExtension;

    #@6
    invoke-direct {v2}, Lcom/kt/wifiapi/OEMExtension;-><init>()V

    #@9
    sput-object v2, Lcom/kt/wifiapi/OEMExtension;->mWifiLgeKTOEMExt:Lcom/kt/wifiapi/OEMExtension;

    #@b
    .line 87
    const-string v2, "WifiKTAPI"

    #@d
    const-string v3, "[getInstance] mWifiLgeKTOEMExt"

    #@f
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 90
    :cond_12
    if-eqz p0, :cond_41

    #@14
    sget-object v2, Lcom/kt/wifiapi/OEMExtension;->mContext:Landroid/content/Context;

    #@16
    if-eq p0, v2, :cond_41

    #@18
    .line 91
    sput-object p0, Lcom/kt/wifiapi/OEMExtension;->mContext:Landroid/content/Context;

    #@1a
    .line 93
    new-instance v1, Landroid/content/IntentFilter;

    #@1c
    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    #@1f
    .line 94
    .local v1, intentFilter:Landroid/content/IntentFilter;
    const-string v2, "com.lge.wifi.WIFI_GWS_SCAN_RESULT"

    #@21
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@24
    .line 95
    sget-object v2, Lcom/kt/wifiapi/OEMExtension;->mReceiver:Landroid/content/BroadcastReceiver;

    #@26
    invoke-virtual {p0, v2, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@29
    .line 97
    new-instance v0, Landroid/content/Intent;

    #@2b
    const-string v2, "com.lge.wifi.WIFI_GWS_SCAN_UPDATE"

    #@2d
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@30
    .line 98
    .local v0, intent:Landroid/content/Intent;
    const/high16 v2, 0x800

    #@32
    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@35
    .line 99
    sget-object v2, Lcom/kt/wifiapi/OEMExtension;->mContext:Landroid/content/Context;

    #@37
    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@3a
    .line 100
    const-string v2, "WifiKTAPI"

    #@3c
    const-string v3, "[getInstance] mReceiver registerReceiver"

    #@3e
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 103
    .end local v0           #intent:Landroid/content/Intent;
    .end local v1           #intentFilter:Landroid/content/IntentFilter;
    :cond_41
    const-string v2, "WifiKTAPI"

    #@43
    new-instance v3, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v4, "[getInstance] is called mContext : "

    #@4a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v3

    #@4e
    sget-object v4, Lcom/kt/wifiapi/OEMExtension;->mContext:Landroid/content/Context;

    #@50
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v3

    #@54
    const-string v4, " mWifiLgeKTOEMExt"

    #@56
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v3

    #@5a
    sget-object v4, Lcom/kt/wifiapi/OEMExtension;->mWifiLgeKTOEMExt:Lcom/kt/wifiapi/OEMExtension;

    #@5c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v3

    #@60
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v3

    #@64
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    .line 105
    sget-object v2, Lcom/kt/wifiapi/OEMExtension;->mWifiLgeKTOEMExt:Lcom/kt/wifiapi/OEMExtension;

    #@69
    return-object v2
.end method


# virtual methods
.method public getDisconnectionPriority()I
    .registers 6

    #@0
    .prologue
    .line 174
    const/16 v1, -0x55

    #@2
    .line 175
    .local v1, signalStrength:I
    const/4 v0, 0x0

    #@3
    .line 177
    .local v0, dpMode:I
    sget-object v2, Lcom/kt/wifiapi/OEMExtension;->mContext:Landroid/content/Context;

    #@5
    if-eqz v2, :cond_16

    #@7
    .line 178
    sget-object v2, Lcom/kt/wifiapi/OEMExtension;->mContext:Landroid/content/Context;

    #@9
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c
    move-result-object v2

    #@d
    const-string/jumbo v3, "wifi_rssi_polling_threshold_db"

    #@10
    const/16 v4, -0x55

    #@12
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@15
    move-result v1

    #@16
    .line 180
    :cond_16
    sparse-switch v1, :sswitch_data_20

    #@19
    .line 197
    :goto_19
    return v0

    #@1a
    .line 182
    :sswitch_1a
    const/4 v0, 0x1

    #@1b
    .line 183
    goto :goto_19

    #@1c
    .line 186
    :sswitch_1c
    const/4 v0, 0x2

    #@1d
    .line 187
    goto :goto_19

    #@1e
    .line 190
    :sswitch_1e
    const/4 v0, 0x3

    #@1f
    .line 191
    goto :goto_19

    #@20
    .line 180
    :sswitch_data_20
    .sparse-switch
        -0x5a -> :sswitch_1e
        -0x55 -> :sswitch_1c
        -0x4b -> :sswitch_1a
    .end sparse-switch
.end method

.method public getFeature()I
    .registers 2

    #@0
    .prologue
    .line 111
    const/16 v0, 0x3f

    #@2
    iput v0, p0, Lcom/kt/wifiapi/OEMExtension;->mFeatureSupport:I

    #@4
    .line 118
    iget v0, p0, Lcom/kt/wifiapi/OEMExtension;->mFeatureSupport:I

    #@6
    return v0
.end method

.method public getGWSScanResultsEx()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/kt/wifiapi/GWSScanResult;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 202
    sget-object v0, Lcom/kt/wifiapi/OEMExtension;->mGWSScanResults:Ljava/util/List;

    #@2
    return-object v0
.end method

.method public getManualConnection()Z
    .registers 2

    #@0
    .prologue
    .line 138
    iget-boolean v0, p0, Lcom/kt/wifiapi/OEMExtension;->mManualConnection:Z

    #@2
    return v0
.end method

.method public setDisconnectionPriority(I)Z
    .registers 5
    .parameter "value"

    #@0
    .prologue
    .line 143
    const/16 v0, -0x64

    #@2
    .line 145
    .local v0, signalStrength:I
    if-ltz p1, :cond_7

    #@4
    const/4 v1, 0x3

    #@5
    if-ge v1, p1, :cond_9

    #@7
    .line 146
    :cond_7
    const/4 v1, 0x0

    #@8
    .line 169
    :goto_8
    return v1

    #@9
    .line 149
    :cond_9
    packed-switch p1, :pswitch_data_28

    #@c
    .line 166
    :goto_c
    sget-object v1, Lcom/kt/wifiapi/OEMExtension;->mContext:Landroid/content/Context;

    #@e
    if-eqz v1, :cond_1c

    #@10
    .line 167
    sget-object v1, Lcom/kt/wifiapi/OEMExtension;->mContext:Landroid/content/Context;

    #@12
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@15
    move-result-object v1

    #@16
    const-string/jumbo v2, "wifi_rssi_polling_threshold_db"

    #@19
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@1c
    .line 169
    :cond_1c
    const/4 v1, 0x1

    #@1d
    goto :goto_8

    #@1e
    .line 151
    :pswitch_1e
    const/16 v0, -0x4b

    #@20
    .line 152
    goto :goto_c

    #@21
    .line 155
    :pswitch_21
    const/16 v0, -0x55

    #@23
    .line 156
    goto :goto_c

    #@24
    .line 159
    :pswitch_24
    const/16 v0, -0x5a

    #@26
    .line 160
    goto :goto_c

    #@27
    .line 149
    nop

    #@28
    :pswitch_data_28
    .packed-switch 0x1
        :pswitch_1e
        :pswitch_21
        :pswitch_24
    .end packed-switch
.end method

.method public setManualConnection(Z)Z
    .registers 7
    .parameter "value"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 123
    sget-object v1, Lcom/kt/wifiapi/OEMExtension;->mContext:Landroid/content/Context;

    #@3
    if-eqz v1, :cond_3e

    #@5
    .line 124
    iput-boolean p1, p0, Lcom/kt/wifiapi/OEMExtension;->mManualConnection:Z

    #@7
    .line 125
    const-string v1, "WifiKTAPI"

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "[setManualConnection] mManualConnection : "

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    iget-boolean v3, p0, Lcom/kt/wifiapi/OEMExtension;->mManualConnection:Z

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 127
    new-instance v0, Landroid/content/Intent;

    #@23
    const-string v1, "com.lge.wifi.WIFI_MANUAL_CONNECTION"

    #@25
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@28
    .line 128
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x800

    #@2a
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@2d
    .line 129
    const-string/jumbo v1, "manual_connection_cmd"

    #@30
    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@33
    .line 130
    const-string/jumbo v1, "manual_connection_value"

    #@36
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@39
    .line 131
    sget-object v1, Lcom/kt/wifiapi/OEMExtension;->mContext:Landroid/content/Context;

    #@3b
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@3e
    .line 133
    .end local v0           #intent:Landroid/content/Intent;
    :cond_3e
    return v4
.end method
