.class final Lcom/kt/wifiapi/GWSScanResult$1;
.super Ljava/lang/Object;
.source "GWSScanResult.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kt/wifiapi/GWSScanResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/kt/wifiapi/GWSScanResult;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 125
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/kt/wifiapi/GWSScanResult;
    .registers 11
    .parameter "in"

    #@0
    .prologue
    .line 127
    new-instance v0, Lcom/kt/wifiapi/GWSScanResult;

    #@2
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@11
    move-result v4

    #@12
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@15
    move-result v5

    #@16
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@19
    move-result-object v6

    #@1a
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1d
    move-result-object v7

    #@1e
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@21
    move-result-object v8

    #@22
    invoke-direct/range {v0 .. v8}, Lcom/kt/wifiapi/GWSScanResult;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@25
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 125
    invoke-virtual {p0, p1}, Lcom/kt/wifiapi/GWSScanResult$1;->createFromParcel(Landroid/os/Parcel;)Lcom/kt/wifiapi/GWSScanResult;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Lcom/kt/wifiapi/GWSScanResult;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 140
    new-array v0, p1, [Lcom/kt/wifiapi/GWSScanResult;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 125
    invoke-virtual {p0, p1}, Lcom/kt/wifiapi/GWSScanResult$1;->newArray(I)[Lcom/kt/wifiapi/GWSScanResult;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
