.class public Lcom/kt/wifiapi/GWSScanResult;
.super Ljava/lang/Object;
.source "GWSScanResult.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/kt/wifiapi/GWSScanResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public BSSID:Ljava/lang/String;

.field public BSSLoadElement:Ljava/lang/String;

.field public SSID:Ljava/lang/String;

.field public capabilities:Ljava/lang/String;

.field public frequency:I

.field public level:I

.field public vendorSpecificContents:Ljava/lang/String;

.field public vendorSpecificOUI:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 124
    new-instance v0, Lcom/kt/wifiapi/GWSScanResult$1;

    #@2
    invoke-direct {v0}, Lcom/kt/wifiapi/GWSScanResult$1;-><init>()V

    #@5
    sput-object v0, Lcom/kt/wifiapi/GWSScanResult;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "SSID"
    .parameter "BSSID"
    .parameter "caps"
    .parameter "level"
    .parameter "frequency"
    .parameter "vendorSpecificOUI"
    .parameter "vendorSpecificContents"
    .parameter "BSSLoadElement"

    #@0
    .prologue
    .line 69
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 70
    iput-object p1, p0, Lcom/kt/wifiapi/GWSScanResult;->SSID:Ljava/lang/String;

    #@5
    .line 71
    iput-object p2, p0, Lcom/kt/wifiapi/GWSScanResult;->BSSID:Ljava/lang/String;

    #@7
    .line 72
    iput-object p3, p0, Lcom/kt/wifiapi/GWSScanResult;->capabilities:Ljava/lang/String;

    #@9
    .line 73
    iput p4, p0, Lcom/kt/wifiapi/GWSScanResult;->level:I

    #@b
    .line 74
    iput p5, p0, Lcom/kt/wifiapi/GWSScanResult;->frequency:I

    #@d
    .line 75
    iput-object p6, p0, Lcom/kt/wifiapi/GWSScanResult;->vendorSpecificOUI:Ljava/lang/String;

    #@f
    .line 76
    iput-object p7, p0, Lcom/kt/wifiapi/GWSScanResult;->vendorSpecificContents:Ljava/lang/String;

    #@11
    .line 77
    iput-object p8, p0, Lcom/kt/wifiapi/GWSScanResult;->BSSLoadElement:Ljava/lang/String;

    #@13
    .line 79
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 108
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 83
    new-instance v1, Ljava/lang/StringBuffer;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    #@5
    .line 84
    .local v1, sb:Ljava/lang/StringBuffer;
    const-string v0, "<none>"

    #@7
    .line 86
    .local v0, none:Ljava/lang/String;
    const-string v2, "SSID: "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@c
    move-result-object v3

    #@d
    iget-object v2, p0, Lcom/kt/wifiapi/GWSScanResult;->SSID:Ljava/lang/String;

    #@f
    if-nez v2, :cond_7c

    #@11
    move-object v2, v0

    #@12
    :goto_12
    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@15
    move-result-object v2

    #@16
    const-string v3, ", BSSID: "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1b
    move-result-object v3

    #@1c
    iget-object v2, p0, Lcom/kt/wifiapi/GWSScanResult;->BSSID:Ljava/lang/String;

    #@1e
    if-nez v2, :cond_7f

    #@20
    move-object v2, v0

    #@21
    :goto_21
    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@24
    move-result-object v2

    #@25
    const-string v3, ", capabilities: "

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@2a
    move-result-object v3

    #@2b
    iget-object v2, p0, Lcom/kt/wifiapi/GWSScanResult;->capabilities:Ljava/lang/String;

    #@2d
    if-nez v2, :cond_82

    #@2f
    move-object v2, v0

    #@30
    :goto_30
    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@33
    move-result-object v2

    #@34
    const-string v3, ", level: "

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@39
    move-result-object v2

    #@3a
    iget v3, p0, Lcom/kt/wifiapi/GWSScanResult;->level:I

    #@3c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    #@3f
    move-result-object v2

    #@40
    const-string v3, ", frequency: "

    #@42
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@45
    move-result-object v2

    #@46
    iget v3, p0, Lcom/kt/wifiapi/GWSScanResult;->frequency:I

    #@48
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    #@4b
    move-result-object v2

    #@4c
    const-string v3, ", IEOUI: "

    #@4e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@51
    move-result-object v3

    #@52
    iget-object v2, p0, Lcom/kt/wifiapi/GWSScanResult;->vendorSpecificOUI:Ljava/lang/String;

    #@54
    if-nez v2, :cond_85

    #@56
    move-object v2, v0

    #@57
    :goto_57
    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@5a
    move-result-object v2

    #@5b
    const-string v3, ", IEContent: "

    #@5d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@60
    move-result-object v3

    #@61
    iget-object v2, p0, Lcom/kt/wifiapi/GWSScanResult;->vendorSpecificContents:Ljava/lang/String;

    #@63
    if-nez v2, :cond_88

    #@65
    move-object v2, v0

    #@66
    :goto_66
    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@69
    move-result-object v2

    #@6a
    const-string v3, ", BSSLE: "

    #@6c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@6f
    move-result-object v2

    #@70
    iget-object v3, p0, Lcom/kt/wifiapi/GWSScanResult;->BSSLoadElement:Ljava/lang/String;

    #@72
    if-nez v3, :cond_8b

    #@74
    .end local v0           #none:Ljava/lang/String;
    :goto_74
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@77
    .line 103
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@7a
    move-result-object v2

    #@7b
    return-object v2

    #@7c
    .line 86
    .restart local v0       #none:Ljava/lang/String;
    :cond_7c
    iget-object v2, p0, Lcom/kt/wifiapi/GWSScanResult;->SSID:Ljava/lang/String;

    #@7e
    goto :goto_12

    #@7f
    :cond_7f
    iget-object v2, p0, Lcom/kt/wifiapi/GWSScanResult;->BSSID:Ljava/lang/String;

    #@81
    goto :goto_21

    #@82
    :cond_82
    iget-object v2, p0, Lcom/kt/wifiapi/GWSScanResult;->capabilities:Ljava/lang/String;

    #@84
    goto :goto_30

    #@85
    :cond_85
    iget-object v2, p0, Lcom/kt/wifiapi/GWSScanResult;->vendorSpecificOUI:Ljava/lang/String;

    #@87
    goto :goto_57

    #@88
    :cond_88
    iget-object v2, p0, Lcom/kt/wifiapi/GWSScanResult;->vendorSpecificContents:Ljava/lang/String;

    #@8a
    goto :goto_66

    #@8b
    :cond_8b
    iget-object v0, p0, Lcom/kt/wifiapi/GWSScanResult;->BSSLoadElement:Ljava/lang/String;

    #@8d
    goto :goto_74
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 113
    iget-object v0, p0, Lcom/kt/wifiapi/GWSScanResult;->SSID:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5
    .line 114
    iget-object v0, p0, Lcom/kt/wifiapi/GWSScanResult;->BSSID:Ljava/lang/String;

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@a
    .line 115
    iget-object v0, p0, Lcom/kt/wifiapi/GWSScanResult;->capabilities:Ljava/lang/String;

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 116
    iget v0, p0, Lcom/kt/wifiapi/GWSScanResult;->level:I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 117
    iget v0, p0, Lcom/kt/wifiapi/GWSScanResult;->frequency:I

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 118
    iget-object v0, p0, Lcom/kt/wifiapi/GWSScanResult;->vendorSpecificOUI:Ljava/lang/String;

    #@1b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1e
    .line 119
    iget-object v0, p0, Lcom/kt/wifiapi/GWSScanResult;->vendorSpecificContents:Ljava/lang/String;

    #@20
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@23
    .line 120
    iget-object v0, p0, Lcom/kt/wifiapi/GWSScanResult;->BSSLoadElement:Ljava/lang/String;

    #@25
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@28
    .line 121
    return-void
.end method
