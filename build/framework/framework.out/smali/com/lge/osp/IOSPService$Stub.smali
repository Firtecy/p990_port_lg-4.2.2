.class public abstract Lcom/lge/osp/IOSPService$Stub;
.super Landroid/os/Binder;
.source "IOSPService.java"

# interfaces
.implements Lcom/lge/osp/IOSPService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/osp/IOSPService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/osp/IOSPService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.lge.osp.IOSPService"

.field static final TRANSACTION_disconnect:I = 0x3

.field static final TRANSACTION_getPassword:I = 0x5

.field static final TRANSACTION_getTransport:I = 0x2

.field static final TRANSACTION_isConnected:I = 0x1

.field static final TRANSACTION_notify:I = 0x6

.field static final TRANSACTION_setPassword:I = 0x4


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 19
    const-string v0, "com.lge.osp.IOSPService"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/osp/IOSPService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/osp/IOSPService;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 27
    if-nez p0, :cond_4

    #@2
    .line 28
    const/4 v0, 0x0

    #@3
    .line 34
    :goto_3
    return-object v0

    #@4
    .line 30
    :cond_4
    const-string v1, "com.lge.osp.IOSPService"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 31
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/osp/IOSPService;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 32
    check-cast v0, Lcom/lge/osp/IOSPService;

    #@12
    goto :goto_3

    #@13
    .line 34
    :cond_13
    new-instance v0, Lcom/lge/osp/IOSPService$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/osp/IOSPService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 11
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 42
    sparse-switch p1, :sswitch_data_a8

    #@5
    .line 123
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v4

    #@9
    :goto_9
    return v4

    #@a
    .line 46
    :sswitch_a
    const-string v3, "com.lge.osp.IOSPService"

    #@c
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 51
    :sswitch_10
    const-string v5, "com.lge.osp.IOSPService"

    #@12
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 52
    invoke-virtual {p0}, Lcom/lge/osp/IOSPService$Stub;->isConnected()Z

    #@18
    move-result v2

    #@19
    .line 53
    .local v2, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1c
    .line 54
    if-eqz v2, :cond_1f

    #@1e
    move v3, v4

    #@1f
    :cond_1f
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@22
    goto :goto_9

    #@23
    .line 59
    .end local v2           #_result:Z
    :sswitch_23
    const-string v3, "com.lge.osp.IOSPService"

    #@25
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@28
    .line 60
    invoke-virtual {p0}, Lcom/lge/osp/IOSPService$Stub;->getTransport()I

    #@2b
    move-result v2

    #@2c
    .line 61
    .local v2, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2f
    .line 62
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@32
    goto :goto_9

    #@33
    .line 67
    .end local v2           #_result:I
    :sswitch_33
    const-string v3, "com.lge.osp.IOSPService"

    #@35
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@38
    .line 68
    invoke-virtual {p0}, Lcom/lge/osp/IOSPService$Stub;->disconnect()V

    #@3b
    .line 69
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3e
    goto :goto_9

    #@3f
    .line 74
    :sswitch_3f
    const-string v5, "com.lge.osp.IOSPService"

    #@41
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@44
    .line 76
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@47
    move-result v5

    #@48
    if-eqz v5, :cond_60

    #@4a
    .line 77
    sget-object v5, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@4c
    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@4f
    move-result-object v0

    #@50
    check-cast v0, Ljava/lang/CharSequence;

    #@52
    .line 82
    .local v0, _arg0:Ljava/lang/CharSequence;
    :goto_52
    invoke-virtual {p0, v0}, Lcom/lge/osp/IOSPService$Stub;->setPassword(Ljava/lang/CharSequence;)Z

    #@55
    move-result v2

    #@56
    .line 83
    .local v2, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@59
    .line 84
    if-eqz v2, :cond_5c

    #@5b
    move v3, v4

    #@5c
    :cond_5c
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@5f
    goto :goto_9

    #@60
    .line 80
    .end local v0           #_arg0:Ljava/lang/CharSequence;
    .end local v2           #_result:Z
    :cond_60
    const/4 v0, 0x0

    #@61
    .restart local v0       #_arg0:Ljava/lang/CharSequence;
    goto :goto_52

    #@62
    .line 89
    .end local v0           #_arg0:Ljava/lang/CharSequence;
    :sswitch_62
    const-string v5, "com.lge.osp.IOSPService"

    #@64
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@67
    .line 90
    invoke-virtual {p0}, Lcom/lge/osp/IOSPService$Stub;->getPassword()Ljava/lang/CharSequence;

    #@6a
    move-result-object v2

    #@6b
    .line 91
    .local v2, _result:Ljava/lang/CharSequence;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@6e
    .line 92
    if-eqz v2, :cond_77

    #@70
    .line 93
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@73
    .line 94
    invoke-static {v2, p3, v4}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    #@76
    goto :goto_9

    #@77
    .line 97
    :cond_77
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@7a
    goto :goto_9

    #@7b
    .line 103
    .end local v2           #_result:Ljava/lang/CharSequence;
    :sswitch_7b
    const-string v3, "com.lge.osp.IOSPService"

    #@7d
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@80
    .line 105
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@83
    move-result v3

    #@84
    if-eqz v3, :cond_a4

    #@86
    .line 106
    sget-object v3, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@88
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@8b
    move-result-object v0

    #@8c
    check-cast v0, Ljava/lang/CharSequence;

    #@8e
    .line 112
    .restart local v0       #_arg0:Ljava/lang/CharSequence;
    :goto_8e
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@91
    move-result v3

    #@92
    if-eqz v3, :cond_a6

    #@94
    .line 113
    sget-object v3, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@96
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@99
    move-result-object v1

    #@9a
    check-cast v1, Ljava/lang/CharSequence;

    #@9c
    .line 118
    .local v1, _arg1:Ljava/lang/CharSequence;
    :goto_9c
    invoke-virtual {p0, v0, v1}, Lcom/lge/osp/IOSPService$Stub;->notify(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    #@9f
    .line 119
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a2
    goto/16 :goto_9

    #@a4
    .line 109
    .end local v0           #_arg0:Ljava/lang/CharSequence;
    .end local v1           #_arg1:Ljava/lang/CharSequence;
    :cond_a4
    const/4 v0, 0x0

    #@a5
    .restart local v0       #_arg0:Ljava/lang/CharSequence;
    goto :goto_8e

    #@a6
    .line 116
    :cond_a6
    const/4 v1, 0x0

    #@a7
    .restart local v1       #_arg1:Ljava/lang/CharSequence;
    goto :goto_9c

    #@a8
    .line 42
    :sswitch_data_a8
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_23
        0x3 -> :sswitch_33
        0x4 -> :sswitch_3f
        0x5 -> :sswitch_62
        0x6 -> :sswitch_7b
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
