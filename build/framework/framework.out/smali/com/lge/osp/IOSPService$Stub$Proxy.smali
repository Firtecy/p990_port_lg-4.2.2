.class Lcom/lge/osp/IOSPService$Stub$Proxy;
.super Ljava/lang/Object;
.source "IOSPService.java"

# interfaces
.implements Lcom/lge/osp/IOSPService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/osp/IOSPService$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 129
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 130
    iput-object p1, p0, Lcom/lge/osp/IOSPService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 131
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 134
    iget-object v0, p0, Lcom/lge/osp/IOSPService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public disconnect()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 176
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 177
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 179
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.osp.IOSPService"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 180
    iget-object v2, p0, Lcom/lge/osp/IOSPService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v3, 0x3

    #@10
    const/4 v4, 0x0

    #@11
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 181
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_17
    .catchall {:try_start_8 .. :try_end_17} :catchall_1e

    #@17
    .line 184
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 185
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 187
    return-void

    #@1e
    .line 184
    :catchall_1e
    move-exception v2

    #@1f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 185
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    throw v2
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 138
    const-string v0, "com.lge.osp.IOSPService"

    #@2
    return-object v0
.end method

.method public getPassword()Ljava/lang/CharSequence;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 214
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 215
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 218
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.osp.IOSPService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 219
    iget-object v3, p0, Lcom/lge/osp/IOSPService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x5

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 220
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 221
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1a
    move-result v3

    #@1b
    if-eqz v3, :cond_2c

    #@1d
    .line 222
    sget-object v3, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@1f
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@22
    move-result-object v2

    #@23
    check-cast v2, Ljava/lang/CharSequence;
    :try_end_25
    .catchall {:try_start_8 .. :try_end_25} :catchall_2e

    #@25
    .line 229
    .local v2, _result:Ljava/lang/CharSequence;
    :goto_25
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 230
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 232
    return-object v2

    #@2c
    .line 225
    .end local v2           #_result:Ljava/lang/CharSequence;
    :cond_2c
    const/4 v2, 0x0

    #@2d
    .restart local v2       #_result:Ljava/lang/CharSequence;
    goto :goto_25

    #@2e
    .line 229
    .end local v2           #_result:Ljava/lang/CharSequence;
    :catchall_2e
    move-exception v3

    #@2f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 230
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    throw v3
.end method

.method public getTransport()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 159
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 160
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 163
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.osp.IOSPService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 164
    iget-object v3, p0, Lcom/lge/osp/IOSPService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x2

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 165
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 166
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_22

    #@1a
    move-result v2

    #@1b
    .line 169
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 170
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 172
    return v2

    #@22
    .line 169
    .end local v2           #_result:I
    :catchall_22
    move-exception v3

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 170
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v3
.end method

.method public isConnected()Z
    .registers 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 142
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 143
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 146
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "com.lge.osp.IOSPService"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 147
    iget-object v4, p0, Lcom/lge/osp/IOSPService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@11
    const/4 v5, 0x1

    #@12
    const/4 v6, 0x0

    #@13
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@16
    .line 148
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@19
    .line 149
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1c
    .catchall {:try_start_a .. :try_end_1c} :catchall_28

    #@1c
    move-result v4

    #@1d
    if-eqz v4, :cond_26

    #@1f
    .line 152
    .local v2, _result:Z
    :goto_1f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 153
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 155
    return v2

    #@26
    .end local v2           #_result:Z
    :cond_26
    move v2, v3

    #@27
    .line 149
    goto :goto_1f

    #@28
    .line 152
    :catchall_28
    move-exception v3

    #@29
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 153
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    throw v3
.end method

.method public notify(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .registers 8
    .parameter "packageName"
    .parameter "tickerText"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 236
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 237
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 239
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.osp.IOSPService"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 240
    if-eqz p1, :cond_32

    #@f
    .line 241
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 242
    const/4 v2, 0x0

    #@14
    invoke-static {p1, v0, v2}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    #@17
    .line 247
    :goto_17
    if-eqz p2, :cond_3f

    #@19
    .line 248
    const/4 v2, 0x1

    #@1a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 249
    const/4 v2, 0x0

    #@1e
    invoke-static {p2, v0, v2}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    #@21
    .line 254
    :goto_21
    iget-object v2, p0, Lcom/lge/osp/IOSPService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@23
    const/4 v3, 0x6

    #@24
    const/4 v4, 0x0

    #@25
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@28
    .line 255
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_2b
    .catchall {:try_start_8 .. :try_end_2b} :catchall_37

    #@2b
    .line 258
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 259
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 261
    return-void

    #@32
    .line 245
    :cond_32
    const/4 v2, 0x0

    #@33
    :try_start_33
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_36
    .catchall {:try_start_33 .. :try_end_36} :catchall_37

    #@36
    goto :goto_17

    #@37
    .line 258
    :catchall_37
    move-exception v2

    #@38
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3b
    .line 259
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3e
    throw v2

    #@3f
    .line 252
    :cond_3f
    const/4 v2, 0x0

    #@40
    :try_start_40
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_43
    .catchall {:try_start_40 .. :try_end_43} :catchall_37

    #@43
    goto :goto_21
.end method

.method public setPassword(Ljava/lang/CharSequence;)Z
    .registers 9
    .parameter "newPassword"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 190
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 191
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 194
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "com.lge.osp.IOSPService"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 195
    if-eqz p1, :cond_30

    #@11
    .line 196
    const/4 v4, 0x1

    #@12
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 197
    const/4 v4, 0x0

    #@16
    invoke-static {p1, v0, v4}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    #@19
    .line 202
    :goto_19
    iget-object v4, p0, Lcom/lge/osp/IOSPService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1b
    const/4 v5, 0x4

    #@1c
    const/4 v6, 0x0

    #@1d
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@20
    .line 203
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@23
    .line 204
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_26
    .catchall {:try_start_a .. :try_end_26} :catchall_35

    #@26
    move-result v4

    #@27
    if-eqz v4, :cond_3d

    #@29
    .line 207
    .local v2, _result:Z
    :goto_29
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 208
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 210
    return v2

    #@30
    .line 200
    .end local v2           #_result:Z
    :cond_30
    const/4 v4, 0x0

    #@31
    :try_start_31
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_34
    .catchall {:try_start_31 .. :try_end_34} :catchall_35

    #@34
    goto :goto_19

    #@35
    .line 207
    :catchall_35
    move-exception v3

    #@36
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@39
    .line 208
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3c
    throw v3

    #@3d
    :cond_3d
    move v2, v3

    #@3e
    .line 204
    goto :goto_29
.end method
