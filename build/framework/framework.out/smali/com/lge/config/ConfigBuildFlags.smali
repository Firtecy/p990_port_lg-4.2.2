.class public final Lcom/lge/config/ConfigBuildFlags;
.super Ljava/lang/Object;
.source "ConfigBuildFlags.java"


# static fields
.field public static final CAPP_ALMOND:Z

.field public static final CAPP_ART:Z

.field public static final CAPP_BATTERY_CONDITION:Z

.field public static final CAPP_BUBBLE_POPUP:Z

.field public static final CAPP_CLIPTRAY:Z

.field public static final CAPP_COMPAT:Z

.field public static final CAPP_DRM:Z

.field public static final CAPP_EMOJI:Z

.field public static final CAPP_EMOJI_DCM:Z

.field public static final CAPP_FONTS:Z

.field public static final CAPP_FONTS_JP:Z

.field public static final CAPP_GHOST_FINGER:Z

.field public static final CAPP_INFO_COLLECTOR:Z

.field public static final CAPP_KEYLED_TIMEOUT:Z

.field public static final CAPP_KEY_EXCEPTION:Z

.field public static final CAPP_LOCKSCREEN:Z

.field public static final CAPP_MDM:Z

.field public static final CAPP_MEDIA_AUDIOEFFECT:Z

.field public static final CAPP_MOVE_SDCARD:Z

.field public static final CAPP_NEXTI_LOCKSCREEN:Z

.field public static final CAPP_NFS:Z

.field public static final CAPP_OPTIMUSUI:Z

.field public static final CAPP_OSP:Z

.field public static final CAPP_PRADAUI:Z

.field public static final CAPP_REAL3D:Z

.field public static final CAPP_RESOURCE:Z

.field public static final CAPP_SLIDEASIDE:Z

.field public static final CAPP_SPLITWINDOW:Z

.field public static final CAPP_SPR_CHAMELEON:Z

.field public static final CAPP_THEMEICON:Z

.field public static final CAPP_TOUCH_PALM_COVER:Z

.field public static final CAPP_TOUCH_PALM_SWIPE:Z

.field public static final CAPP_TOUCH_SCROLLER:Z

.field public static final CAPP_TRIMHEAPS:Z

.field public static final CAPP_UPLUSAPI:Z

.field public static final CAPP_VALID_BATTERYID:Z

.field public static final CAPP_WAPSERVICE:Z

.field public static final CAPP_ZDI_I:Z

.field public static final CAPP_ZDI_O:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 32
    const-string/jumbo v0, "ro.lge.capp_wapservice"

    #@4
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@7
    move-result v0

    #@8
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_WAPSERVICE:Z

    #@a
    .line 33
    const-string/jumbo v0, "ro.lge.capp_drm"

    #@d
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@10
    move-result v0

    #@11
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_DRM:Z

    #@13
    .line 34
    const-string/jumbo v0, "ro.lge.capp_key_exception"

    #@16
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@19
    move-result v0

    #@1a
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_KEY_EXCEPTION:Z

    #@1c
    .line 35
    const-string/jumbo v0, "ro.lge.capp_osp"

    #@1f
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@22
    move-result v0

    #@23
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_OSP:Z

    #@25
    .line 36
    const-string/jumbo v0, "ro.lge.capp_lockscreen"

    #@28
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@2b
    move-result v0

    #@2c
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_LOCKSCREEN:Z

    #@2e
    .line 37
    const-string/jumbo v0, "ro.lge.capp_resource"

    #@31
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@34
    move-result v0

    #@35
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_RESOURCE:Z

    #@37
    .line 38
    const-string/jumbo v0, "ro.lge.capp_almond"

    #@3a
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@3d
    move-result v0

    #@3e
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_ALMOND:Z

    #@40
    .line 39
    const-string/jumbo v0, "ro.lge.capp_mdm"

    #@43
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@46
    move-result v0

    #@47
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_MDM:Z

    #@49
    .line 40
    const-string/jumbo v0, "ro.lge.capp_uplus_api"

    #@4c
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@4f
    move-result v0

    #@50
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_UPLUSAPI:Z

    #@52
    .line 41
    const-string/jumbo v0, "ro.lge.capp_keyled_timeout"

    #@55
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@58
    move-result v0

    #@59
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_KEYLED_TIMEOUT:Z

    #@5b
    .line 42
    const-string/jumbo v0, "ro.lge.capp_ghost_finger"

    #@5e
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@61
    move-result v0

    #@62
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_GHOST_FINGER:Z

    #@64
    .line 43
    const-string/jumbo v0, "ro.lge.fonts"

    #@67
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@6a
    move-result v0

    #@6b
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_FONTS:Z

    #@6d
    .line 44
    const-string/jumbo v0, "ro.lge.fonts_jp"

    #@70
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@73
    move-result v0

    #@74
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_FONTS_JP:Z

    #@76
    .line 45
    const-string/jumbo v0, "ro.lge.capp_optimusui"

    #@79
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@7c
    move-result v0

    #@7d
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_OPTIMUSUI:Z

    #@7f
    .line 46
    const-string/jumbo v0, "ro.lge.capp_pradaui"

    #@82
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@85
    move-result v0

    #@86
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_PRADAUI:Z

    #@88
    .line 47
    const-string/jumbo v0, "ro.lge.capp_valid_batteryid"

    #@8b
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@8e
    move-result v0

    #@8f
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_VALID_BATTERYID:Z

    #@91
    .line 48
    const-string/jumbo v0, "ro.lge.capp_touch_scroller"

    #@94
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@97
    move-result v0

    #@98
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_TOUCH_SCROLLER:Z

    #@9a
    .line 49
    const-string/jumbo v0, "ro.lge.capp_real3d"

    #@9d
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@a0
    move-result v0

    #@a1
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_REAL3D:Z

    #@a3
    .line 50
    const-string/jumbo v0, "ro.lge.capp_nfs"

    #@a6
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@a9
    move-result v0

    #@aa
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_NFS:Z

    #@ac
    .line 51
    const-string/jumbo v0, "ro.lge.capp_compat"

    #@af
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@b2
    move-result v0

    #@b3
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_COMPAT:Z

    #@b5
    .line 52
    const-string/jumbo v0, "ro.lge.capp_bubble_popup"

    #@b8
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@bb
    move-result v0

    #@bc
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@be
    .line 53
    const-string/jumbo v0, "ro.lge.capp_trimHeaps"

    #@c1
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@c4
    move-result v0

    #@c5
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_TRIMHEAPS:Z

    #@c7
    .line 54
    const-string/jumbo v0, "ro.lge.capp_art"

    #@ca
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@cd
    move-result v0

    #@ce
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_ART:Z

    #@d0
    .line 55
    const-string/jumbo v0, "ro.lge.capp_themeicon"

    #@d3
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@d6
    move-result v0

    #@d7
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_THEMEICON:Z

    #@d9
    .line 56
    const-string/jumbo v0, "ro.lge.capp_move_sdcard"

    #@dc
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@df
    move-result v0

    #@e0
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_MOVE_SDCARD:Z

    #@e2
    .line 58
    const-string/jumbo v0, "ro.lge.capp_nexti_lockscreen"

    #@e5
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@e8
    move-result v0

    #@e9
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_NEXTI_LOCKSCREEN:Z

    #@eb
    .line 59
    const-string/jumbo v0, "ro.lge.capp_emoji"

    #@ee
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@f1
    move-result v0

    #@f2
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@f4
    .line 60
    const-string/jumbo v0, "ro.lge.capp_emoji_dcm"

    #@f7
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@fa
    move-result v0

    #@fb
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI_DCM:Z

    #@fd
    .line 61
    const-string/jumbo v0, "ro.lge.capp_media_audioeffect"

    #@100
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@103
    move-result v0

    #@104
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_MEDIA_AUDIOEFFECT:Z

    #@106
    .line 63
    const-string/jumbo v0, "ro.lge.capp_cliptray"

    #@109
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@10c
    move-result v0

    #@10d
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_CLIPTRAY:Z

    #@10f
    .line 64
    const-string/jumbo v0, "ro.lge.capp_spr_chameleon"

    #@112
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@115
    move-result v0

    #@116
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPR_CHAMELEON:Z

    #@118
    .line 66
    const-string/jumbo v0, "ro.lge.capp_battery_condition"

    #@11b
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@11e
    move-result v0

    #@11f
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_BATTERY_CONDITION:Z

    #@121
    .line 67
    const-string/jumbo v0, "ro.lge.capp_info_collector"

    #@124
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@127
    move-result v0

    #@128
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_INFO_COLLECTOR:Z

    #@12a
    .line 69
    const-string/jumbo v0, "ro.lge.capp_slideAside"

    #@12d
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@130
    move-result v0

    #@131
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_SLIDEASIDE:Z

    #@133
    .line 71
    const-string/jumbo v0, "ro.lge.capp_touch_palm_swipe"

    #@136
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@139
    move-result v0

    #@13a
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_TOUCH_PALM_SWIPE:Z

    #@13c
    .line 72
    const-string/jumbo v0, "ro.lge.capp_touch_palm_cover"

    #@13f
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@142
    move-result v0

    #@143
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_TOUCH_PALM_COVER:Z

    #@145
    .line 74
    const-string/jumbo v0, "ro.lge.capp_splitwindow"

    #@148
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@14b
    move-result v0

    #@14c
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@14e
    .line 76
    const-string/jumbo v0, "ro.lge.capp_ZDi_O"

    #@151
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@154
    move-result v0

    #@155
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_ZDI_O:Z

    #@157
    .line 78
    const-string/jumbo v0, "ro.lge.capp_ZDi_I"

    #@15a
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@15d
    move-result v0

    #@15e
    sput-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_ZDI_I:Z

    #@160
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 18
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
