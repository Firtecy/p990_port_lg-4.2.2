.class public Lcom/lge/loader/cliptray/CliptrayManagerCreator;
.super Lcom/lge/loader/InstanceCreator;
.source "CliptrayManagerCreator.java"


# static fields
.field private static CLITRAY_MANAGER_CLASS:Ljava/lang/String;

.field private static LGSYSTEM_SERVICE_CORE_LIB:Ljava/lang/String;

.field private static sCliptrayManagerConstructor:Ljava/lang/reflect/Constructor;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    .line 19
    const-string v4, "/system/framework/com.lge.systemservice.core.jar"

    #@2
    sput-object v4, Lcom/lge/loader/cliptray/CliptrayManagerCreator;->LGSYSTEM_SERVICE_CORE_LIB:Ljava/lang/String;

    #@4
    .line 21
    const-string v4, "com.lge.systemservice.core.cliptraymanager.CliptrayManager"

    #@6
    sput-object v4, Lcom/lge/loader/cliptray/CliptrayManagerCreator;->CLITRAY_MANAGER_CLASS:Ljava/lang/String;

    #@8
    .line 25
    new-instance v1, Ldalvik/system/PathClassLoader;

    #@a
    sget-object v4, Lcom/lge/loader/cliptray/CliptrayManagerCreator;->LGSYSTEM_SERVICE_CORE_LIB:Ljava/lang/String;

    #@c
    const-class v5, Lcom/lge/loader/cliptray/CliptrayManagerCreator;

    #@e
    invoke-virtual {v5}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    #@11
    move-result-object v5

    #@12
    invoke-direct {v1, v4, v5}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    #@15
    .line 29
    .local v1, cloader:Ljava/lang/ClassLoader;
    :try_start_15
    sget-object v4, Lcom/lge/loader/cliptray/CliptrayManagerCreator;->CLITRAY_MANAGER_CLASS:Ljava/lang/String;

    #@17
    const/4 v5, 0x1

    #@18
    invoke-static {v4, v5, v1}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    #@1b
    move-result-object v0

    #@1c
    .line 33
    .local v0, cliptrayManagerClass:Ljava/lang/Class;
    const/4 v4, 0x1

    #@1d
    new-array v3, v4, [Ljava/lang/Class;

    #@1f
    const/4 v4, 0x0

    #@20
    const-class v5, Landroid/content/Context;

    #@22
    aput-object v5, v3, v4

    #@24
    .line 34
    .local v3, paramType:[Ljava/lang/Class;
    invoke-virtual {v0, v3}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    #@27
    move-result-object v4

    #@28
    sput-object v4, Lcom/lge/loader/cliptray/CliptrayManagerCreator;->sCliptrayManagerConstructor:Ljava/lang/reflect/Constructor;
    :try_end_2a
    .catch Ljava/lang/ClassNotFoundException; {:try_start_15 .. :try_end_2a} :catch_2b
    .catch Ljava/lang/SecurityException; {:try_start_15 .. :try_end_2a} :catch_4b
    .catch Ljava/lang/NoSuchMethodException; {:try_start_15 .. :try_end_2a} :catch_50

    #@2a
    .line 45
    .end local v0           #cliptrayManagerClass:Ljava/lang/Class;
    .end local v3           #paramType:[Ljava/lang/Class;
    :goto_2a
    return-void

    #@2b
    .line 35
    :catch_2b
    move-exception v2

    #@2c
    .line 37
    .local v2, e:Ljava/lang/ClassNotFoundException;
    const-string v4, "CliptrayManagerLoader"

    #@2e
    new-instance v5, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    sget-object v6, Lcom/lge/loader/cliptray/CliptrayManagerCreator;->CLITRAY_MANAGER_CLASS:Ljava/lang/String;

    #@35
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v5

    #@39
    const-string v6, "don\'t exist in library: "

    #@3b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v5

    #@3f
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v5

    #@43
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v5

    #@47
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    goto :goto_2a

    #@4b
    .line 38
    .end local v2           #e:Ljava/lang/ClassNotFoundException;
    :catch_4b
    move-exception v2

    #@4c
    .line 40
    .local v2, e:Ljava/lang/SecurityException;
    invoke-virtual {v2}, Ljava/lang/SecurityException;->printStackTrace()V

    #@4f
    goto :goto_2a

    #@50
    .line 41
    .end local v2           #e:Ljava/lang/SecurityException;
    :catch_50
    move-exception v2

    #@51
    .line 43
    .local v2, e:Ljava/lang/NoSuchMethodException;
    const-string v4, "CliptrayManagerLoader"

    #@53
    new-instance v5, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    sget-object v6, Lcom/lge/loader/cliptray/CliptrayManagerCreator;->CLITRAY_MANAGER_CLASS:Ljava/lang/String;

    #@5a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v5

    #@5e
    const-string v6, "don\'t exist Constructor in library: "

    #@60
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v5

    #@64
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v5

    #@68
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v5

    #@6c
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6f
    goto :goto_2a
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 47
    invoke-direct {p0}, Lcom/lge/loader/InstanceCreator;-><init>()V

    #@3
    .line 48
    return-void
.end method


# virtual methods
.method public newInstance(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5
    .parameter "args"

    #@0
    .prologue
    .line 52
    const/4 v2, 0x1

    #@1
    new-array v1, v2, [Ljava/lang/Object;

    #@3
    const/4 v2, 0x0

    #@4
    aput-object p1, v1, v2

    #@6
    .line 54
    .local v1, paramType:[Ljava/lang/Object;
    :try_start_6
    sget-object v2, Lcom/lge/loader/cliptray/CliptrayManagerCreator;->sCliptrayManagerConstructor:Ljava/lang/reflect/Constructor;

    #@8
    if-eqz v2, :cond_15

    #@a
    .line 55
    sget-object v2, Lcom/lge/loader/cliptray/CliptrayManagerCreator;->sCliptrayManagerConstructor:Ljava/lang/reflect/Constructor;

    #@c
    invoke-virtual {v2, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_f} :catch_11
    .catch Ljava/lang/InstantiationException; {:try_start_6 .. :try_end_f} :catch_17
    .catch Ljava/lang/IllegalAccessException; {:try_start_6 .. :try_end_f} :catch_1c
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_6 .. :try_end_f} :catch_21

    #@f
    move-result-object v2

    #@10
    .line 66
    :goto_10
    return-object v2

    #@11
    .line 57
    :catch_11
    move-exception v0

    #@12
    .line 58
    .local v0, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@15
    .line 66
    .end local v0           #e:Ljava/lang/IllegalArgumentException;
    :cond_15
    :goto_15
    const/4 v2, 0x0

    #@16
    goto :goto_10

    #@17
    .line 59
    :catch_17
    move-exception v0

    #@18
    .line 60
    .local v0, e:Ljava/lang/InstantiationException;
    invoke-virtual {v0}, Ljava/lang/InstantiationException;->printStackTrace()V

    #@1b
    goto :goto_15

    #@1c
    .line 61
    .end local v0           #e:Ljava/lang/InstantiationException;
    :catch_1c
    move-exception v0

    #@1d
    .line 62
    .local v0, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@20
    goto :goto_15

    #@21
    .line 63
    .end local v0           #e:Ljava/lang/IllegalAccessException;
    :catch_21
    move-exception v0

    #@22
    .line 64
    .local v0, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    #@25
    goto :goto_15
.end method
