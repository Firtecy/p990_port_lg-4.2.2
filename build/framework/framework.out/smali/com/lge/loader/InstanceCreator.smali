.class public abstract Lcom/lge/loader/InstanceCreator;
.super Ljava/lang/Object;
.source "InstanceCreator.java"


# instance fields
.field private mDefault:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public getDefaultInstance()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 19
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Lcom/lge/loader/InstanceCreator;->getDefaultInstance(Ljava/lang/Object;)Ljava/lang/Object;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public getDefaultInstance(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter "args"

    #@0
    .prologue
    .line 29
    iget-object v0, p0, Lcom/lge/loader/InstanceCreator;->mDefault:Ljava/lang/Object;

    #@2
    if-nez v0, :cond_a

    #@4
    .line 30
    invoke-virtual {p0, p1}, Lcom/lge/loader/InstanceCreator;->newInstance(Ljava/lang/Object;)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Lcom/lge/loader/InstanceCreator;->mDefault:Ljava/lang/Object;

    #@a
    .line 32
    :cond_a
    iget-object v0, p0, Lcom/lge/loader/InstanceCreator;->mDefault:Ljava/lang/Object;

    #@c
    return-object v0
.end method

.method public abstract newInstance(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public setDefaultInstance(Ljava/lang/Object;)V
    .registers 2
    .parameter "newInstance"

    #@0
    .prologue
    .line 40
    iput-object p1, p0, Lcom/lge/loader/InstanceCreator;->mDefault:Ljava/lang/Object;

    #@2
    .line 41
    return-void
.end method
