.class public Lcom/lge/loader/interaction/InteractionManagerCreator;
.super Lcom/lge/loader/InstanceCreator;
.source "InteractionManagerCreator.java"


# static fields
.field private static INTERACTION_MANAGER_CLASS:Ljava/lang/String;

.field private static INTERACTION_SERVICE_LIB:Ljava/lang/String;

.field private static cloader:Ljava/lang/ClassLoader;

.field private static mClass:Ljava/lang/Class;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 15
    const-string v0, "/system/framework/com.lge.zdi.interaction.jar"

    #@2
    sput-object v0, Lcom/lge/loader/interaction/InteractionManagerCreator;->INTERACTION_SERVICE_LIB:Ljava/lang/String;

    #@4
    .line 17
    const-string v0, "com.lge.zdi.interaction.InteractionManager"

    #@6
    sput-object v0, Lcom/lge/loader/interaction/InteractionManagerCreator;->INTERACTION_MANAGER_CLASS:Ljava/lang/String;

    #@8
    .line 21
    new-instance v0, Ldalvik/system/PathClassLoader;

    #@a
    sget-object v1, Lcom/lge/loader/interaction/InteractionManagerCreator;->INTERACTION_SERVICE_LIB:Ljava/lang/String;

    #@c
    const-class v2, Lcom/lge/loader/interaction/InteractionManagerCreator;

    #@e
    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    #@11
    move-result-object v2

    #@12
    invoke-direct {v0, v1, v2}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    #@15
    sput-object v0, Lcom/lge/loader/interaction/InteractionManagerCreator;->cloader:Ljava/lang/ClassLoader;

    #@17
    .line 23
    return-void
.end method

.method public constructor <init>()V
    .registers 5

    #@0
    .prologue
    .line 25
    invoke-direct {p0}, Lcom/lge/loader/InstanceCreator;-><init>()V

    #@3
    .line 27
    :try_start_3
    sget-object v1, Lcom/lge/loader/interaction/InteractionManagerCreator;->INTERACTION_MANAGER_CLASS:Ljava/lang/String;

    #@5
    const/4 v2, 0x1

    #@6
    sget-object v3, Lcom/lge/loader/interaction/InteractionManagerCreator;->cloader:Ljava/lang/ClassLoader;

    #@8
    invoke-static {v1, v2, v3}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    #@b
    move-result-object v1

    #@c
    sput-object v1, Lcom/lge/loader/interaction/InteractionManagerCreator;->mClass:Ljava/lang/Class;
    :try_end_e
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_e} :catch_f

    #@e
    .line 31
    :goto_e
    return-void

    #@f
    .line 28
    :catch_f
    move-exception v0

    #@10
    .line 29
    .local v0, e:Ljava/lang/ClassNotFoundException;
    const-string v1, "InteractionManagerCreator"

    #@12
    new-instance v2, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    sget-object v3, Lcom/lge/loader/interaction/InteractionManagerCreator;->INTERACTION_MANAGER_CLASS:Ljava/lang/String;

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    const-string v3, " don\'t exist in library"

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    goto :goto_e
.end method


# virtual methods
.method public newInstance(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter "args"

    #@0
    .prologue
    .line 36
    :try_start_0
    sget-object v1, Lcom/lge/loader/interaction/InteractionManagerCreator;->mClass:Ljava/lang/Class;

    #@2
    if-eqz v1, :cond_f

    #@4
    .line 37
    sget-object v1, Lcom/lge/loader/interaction/InteractionManagerCreator;->mClass:Ljava/lang/Class;

    #@6
    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;
    :try_end_9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_9} :catch_b
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_9} :catch_11
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_9} :catch_16

    #@9
    move-result-object v1

    #@a
    .line 46
    :goto_a
    return-object v1

    #@b
    .line 39
    :catch_b
    move-exception v0

    #@c
    .line 40
    .local v0, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@f
    .line 46
    .end local v0           #e:Ljava/lang/IllegalArgumentException;
    :cond_f
    :goto_f
    const/4 v1, 0x0

    #@10
    goto :goto_a

    #@11
    .line 41
    :catch_11
    move-exception v0

    #@12
    .line 42
    .local v0, e:Ljava/lang/InstantiationException;
    invoke-virtual {v0}, Ljava/lang/InstantiationException;->printStackTrace()V

    #@15
    goto :goto_f

    #@16
    .line 43
    .end local v0           #e:Ljava/lang/InstantiationException;
    :catch_16
    move-exception v0

    #@17
    .line 44
    .local v0, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@1a
    goto :goto_f
.end method
