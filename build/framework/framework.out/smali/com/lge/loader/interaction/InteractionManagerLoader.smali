.class public Lcom/lge/loader/interaction/InteractionManagerLoader;
.super Ljava/lang/Object;
.source "InteractionManagerLoader.java"


# static fields
.field private static MANAGER_INSTANCE:Lcom/lge/loader/interaction/IInteractionManager;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 14
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/lge/loader/interaction/InteractionManagerLoader;->MANAGER_INSTANCE:Lcom/lge/loader/interaction/IInteractionManager;

    #@3
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 13
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static getServiceManager()Lcom/lge/loader/interaction/IInteractionManager;
    .registers 2

    #@0
    .prologue
    .line 33
    sget-object v0, Lcom/lge/loader/interaction/InteractionManagerLoader;->MANAGER_INSTANCE:Lcom/lge/loader/interaction/IInteractionManager;

    #@2
    if-nez v0, :cond_12

    #@4
    .line 35
    :try_start_4
    sget-object v0, Lcom/lge/loader/RuntimeLibraryLoader;->ZDI_INTERACTION:Ljava/lang/String;

    #@6
    invoke-static {v0}, Lcom/lge/loader/RuntimeLibraryLoader;->getCreator(Ljava/lang/String;)Lcom/lge/loader/InstanceCreator;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0}, Lcom/lge/loader/InstanceCreator;->getDefaultInstance()Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Lcom/lge/loader/interaction/IInteractionManager;

    #@10
    sput-object v0, Lcom/lge/loader/interaction/InteractionManagerLoader;->MANAGER_INSTANCE:Lcom/lge/loader/interaction/IInteractionManager;
    :try_end_12
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_12} :catch_22

    #@12
    .line 40
    :cond_12
    :goto_12
    sget-object v0, Lcom/lge/loader/interaction/InteractionManagerLoader;->MANAGER_INSTANCE:Lcom/lge/loader/interaction/IInteractionManager;

    #@14
    if-nez v0, :cond_18

    #@16
    .line 41
    const/4 v0, 0x0

    #@17
    .line 45
    :goto_17
    return-object v0

    #@18
    .line 44
    :cond_18
    sget-object v1, Lcom/lge/loader/interaction/InteractionManagerLoader;->MANAGER_INSTANCE:Lcom/lge/loader/interaction/IInteractionManager;

    #@1a
    monitor-enter v1

    #@1b
    .line 45
    :try_start_1b
    sget-object v0, Lcom/lge/loader/interaction/InteractionManagerLoader;->MANAGER_INSTANCE:Lcom/lge/loader/interaction/IInteractionManager;

    #@1d
    monitor-exit v1

    #@1e
    goto :goto_17

    #@1f
    .line 46
    :catchall_1f
    move-exception v0

    #@20
    monitor-exit v1
    :try_end_21
    .catchall {:try_start_1b .. :try_end_21} :catchall_1f

    #@21
    throw v0

    #@22
    .line 36
    :catch_22
    move-exception v0

    #@23
    goto :goto_12
.end method

.method public static launchService(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 17
    invoke-static {}, Lcom/lge/loader/interaction/InteractionManagerLoader;->getServiceManager()Lcom/lge/loader/interaction/IInteractionManager;

    #@3
    move-result-object v0

    #@4
    .line 18
    .local v0, manager:Lcom/lge/loader/interaction/IInteractionManager;
    if-eqz v0, :cond_9

    #@6
    .line 19
    invoke-interface {v0, p0}, Lcom/lge/loader/interaction/IInteractionManager;->launchService(Landroid/content/Context;)Z

    #@9
    .line 21
    :cond_9
    return-void
.end method

.method public static serviceSystemReady()V
    .registers 2

    #@0
    .prologue
    .line 24
    invoke-static {}, Lcom/lge/loader/interaction/InteractionManagerLoader;->getServiceManager()Lcom/lge/loader/interaction/IInteractionManager;

    #@3
    move-result-object v0

    #@4
    .line 25
    .local v0, manager:Lcom/lge/loader/interaction/IInteractionManager;
    if-eqz v0, :cond_b

    #@6
    .line 26
    monitor-enter v0

    #@7
    .line 27
    :try_start_7
    invoke-interface {v0}, Lcom/lge/loader/interaction/IInteractionManager;->serviceSystemReady()Z

    #@a
    .line 28
    monitor-exit v0

    #@b
    .line 30
    :cond_b
    return-void

    #@c
    .line 28
    :catchall_c
    move-exception v1

    #@d
    monitor-exit v0
    :try_end_e
    .catchall {:try_start_7 .. :try_end_e} :catchall_c

    #@e
    throw v1
.end method
