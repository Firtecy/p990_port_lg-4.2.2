.class public Lcom/lge/loader/volumevibrator/VolumeVibratorManagerCreator;
.super Lcom/lge/loader/InstanceCreator;
.source "VolumeVibratorManagerCreator.java"


# static fields
.field private static LGSYSTEM_SERVICE_CORE_LIB:Ljava/lang/String;

.field private static VOL_VIBRATOR_MANAGER_CLASS:Ljava/lang/String;

.field private static sVolumeVibratorManagerConstructor:Ljava/lang/Class;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    .line 12
    const-string v2, "/system/framework/com.lge.systemservice.core.jar"

    #@2
    sput-object v2, Lcom/lge/loader/volumevibrator/VolumeVibratorManagerCreator;->LGSYSTEM_SERVICE_CORE_LIB:Ljava/lang/String;

    #@4
    .line 14
    const-string v2, "com.lge.systemservice.core.volumevibratormanager.VolumeVibratorManager"

    #@6
    sput-object v2, Lcom/lge/loader/volumevibrator/VolumeVibratorManagerCreator;->VOL_VIBRATOR_MANAGER_CLASS:Ljava/lang/String;

    #@8
    .line 18
    new-instance v0, Ldalvik/system/PathClassLoader;

    #@a
    sget-object v2, Lcom/lge/loader/volumevibrator/VolumeVibratorManagerCreator;->LGSYSTEM_SERVICE_CORE_LIB:Ljava/lang/String;

    #@c
    const-class v3, Lcom/lge/loader/volumevibrator/VolumeVibratorManagerCreator;

    #@e
    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    #@11
    move-result-object v3

    #@12
    invoke-direct {v0, v2, v3}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    #@15
    .line 21
    .local v0, cloader:Ljava/lang/ClassLoader;
    :try_start_15
    sget-object v2, Lcom/lge/loader/volumevibrator/VolumeVibratorManagerCreator;->VOL_VIBRATOR_MANAGER_CLASS:Ljava/lang/String;

    #@17
    const/4 v3, 0x1

    #@18
    invoke-static {v2, v3, v0}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    #@1b
    move-result-object v2

    #@1c
    sput-object v2, Lcom/lge/loader/volumevibrator/VolumeVibratorManagerCreator;->sVolumeVibratorManagerConstructor:Ljava/lang/Class;
    :try_end_1e
    .catch Ljava/lang/ClassNotFoundException; {:try_start_15 .. :try_end_1e} :catch_1f

    #@1e
    .line 27
    :goto_1e
    return-void

    #@1f
    .line 23
    :catch_1f
    move-exception v1

    #@20
    .line 25
    .local v1, e:Ljava/lang/ClassNotFoundException;
    const-string v2, "VolumeVibratorLoader"

    #@22
    new-instance v3, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    sget-object v4, Lcom/lge/loader/volumevibrator/VolumeVibratorManagerCreator;->VOL_VIBRATOR_MANAGER_CLASS:Ljava/lang/String;

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    const-string v4, " don\'t exist in library"

    #@2f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v3

    #@37
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    goto :goto_1e
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 9
    invoke-direct {p0}, Lcom/lge/loader/InstanceCreator;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public newInstance(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter "args"

    #@0
    .prologue
    .line 32
    :try_start_0
    sget-object v1, Lcom/lge/loader/volumevibrator/VolumeVibratorManagerCreator;->sVolumeVibratorManagerConstructor:Ljava/lang/Class;

    #@2
    if-eqz v1, :cond_f

    #@4
    .line 33
    sget-object v1, Lcom/lge/loader/volumevibrator/VolumeVibratorManagerCreator;->sVolumeVibratorManagerConstructor:Ljava/lang/Class;

    #@6
    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;
    :try_end_9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_9} :catch_b
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_9} :catch_11
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_9} :catch_16

    #@9
    move-result-object v1

    #@a
    .line 42
    :goto_a
    return-object v1

    #@b
    .line 35
    :catch_b
    move-exception v0

    #@c
    .line 36
    .local v0, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@f
    .line 42
    .end local v0           #e:Ljava/lang/IllegalArgumentException;
    :cond_f
    :goto_f
    const/4 v1, 0x0

    #@10
    goto :goto_a

    #@11
    .line 37
    :catch_11
    move-exception v0

    #@12
    .line 38
    .local v0, e:Ljava/lang/InstantiationException;
    invoke-virtual {v0}, Ljava/lang/InstantiationException;->printStackTrace()V

    #@15
    goto :goto_f

    #@16
    .line 39
    .end local v0           #e:Ljava/lang/InstantiationException;
    :catch_16
    move-exception v0

    #@17
    .line 40
    .local v0, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@1a
    goto :goto_f
.end method
