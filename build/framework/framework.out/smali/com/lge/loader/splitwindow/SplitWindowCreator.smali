.class public Lcom/lge/loader/splitwindow/SplitWindowCreator;
.super Lcom/lge/loader/InstanceCreator;
.source "SplitWindowCreator.java"


# static fields
.field private static cloader:Ljava/lang/ClassLoader;


# instance fields
.field private mClass:Ljava/lang/Class;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 16
    new-instance v0, Ldalvik/system/PathClassLoader;

    #@2
    const-string v1, "/system/framework/com.lge.zdi.splitwindow.jar"

    #@4
    const-class v2, Lcom/lge/loader/splitwindow/SplitWindowCreator;

    #@6
    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    #@9
    move-result-object v2

    #@a
    invoke-direct {v0, v1, v2}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    #@d
    sput-object v0, Lcom/lge/loader/splitwindow/SplitWindowCreator;->cloader:Ljava/lang/ClassLoader;

    #@f
    .line 18
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 7
    .parameter "windowType"

    #@0
    .prologue
    .line 20
    invoke-direct {p0}, Lcom/lge/loader/InstanceCreator;-><init>()V

    #@3
    .line 21
    const/4 v0, 0x0

    #@4
    .line 22
    .local v0, constructorName:Ljava/lang/String;
    const-string v0, "com.lge.zdi.splitwindow.nativebridge.SplitWindowBridge"

    #@6
    .line 25
    const/4 v2, 0x1

    #@7
    :try_start_7
    sget-object v3, Lcom/lge/loader/splitwindow/SplitWindowCreator;->cloader:Ljava/lang/ClassLoader;

    #@9
    invoke-static {v0, v2, v3}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    #@c
    move-result-object v2

    #@d
    iput-object v2, p0, Lcom/lge/loader/splitwindow/SplitWindowCreator;->mClass:Ljava/lang/Class;
    :try_end_f
    .catch Ljava/lang/ClassNotFoundException; {:try_start_7 .. :try_end_f} :catch_10

    #@f
    .line 30
    :goto_f
    return-void

    #@10
    .line 26
    :catch_10
    move-exception v1

    #@11
    .line 28
    .local v1, e:Ljava/lang/ClassNotFoundException;
    const-string v2, "SplitWindowCreator"

    #@13
    new-instance v3, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    const-string v4, "don\'t exist"

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    goto :goto_f
.end method


# virtual methods
.method public newInstance(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter "args"

    #@0
    .prologue
    .line 35
    :try_start_0
    iget-object v1, p0, Lcom/lge/loader/splitwindow/SplitWindowCreator;->mClass:Ljava/lang/Class;

    #@2
    if-eqz v1, :cond_f

    #@4
    .line 36
    iget-object v1, p0, Lcom/lge/loader/splitwindow/SplitWindowCreator;->mClass:Ljava/lang/Class;

    #@6
    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;
    :try_end_9
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_9} :catch_b
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_9} :catch_11

    #@9
    move-result-object v1

    #@a
    .line 45
    :goto_a
    return-object v1

    #@b
    .line 38
    :catch_b
    move-exception v0

    #@c
    .line 40
    .local v0, e:Ljava/lang/InstantiationException;
    invoke-virtual {v0}, Ljava/lang/InstantiationException;->printStackTrace()V

    #@f
    .line 45
    .end local v0           #e:Ljava/lang/InstantiationException;
    :cond_f
    :goto_f
    const/4 v1, 0x0

    #@10
    goto :goto_a

    #@11
    .line 41
    :catch_11
    move-exception v0

    #@12
    .line 43
    .local v0, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@15
    goto :goto_f
.end method
