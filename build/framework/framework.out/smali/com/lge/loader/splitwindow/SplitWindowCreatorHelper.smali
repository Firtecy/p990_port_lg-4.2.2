.class public Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;
.super Ljava/lang/Object;
.source "SplitWindowCreatorHelper.java"


# static fields
.field private static final SPLITWINDOW_INSTANCE:Lcom/lge/loader/splitwindow/ISplitWindow;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 12
    sget-object v0, Lcom/lge/loader/RuntimeLibraryLoader;->SPLIT_WINDOW:Ljava/lang/String;

    #@2
    invoke-static {v0}, Lcom/lge/loader/RuntimeLibraryLoader;->getCreator(Ljava/lang/String;)Lcom/lge/loader/InstanceCreator;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Lcom/lge/loader/InstanceCreator;->getDefaultInstance()Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Lcom/lge/loader/splitwindow/ISplitWindow;

    #@c
    sput-object v0, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->SPLITWINDOW_INSTANCE:Lcom/lge/loader/splitwindow/ISplitWindow;

    #@e
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 11
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static getPolicyService()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;
    .registers 2

    #@0
    .prologue
    .line 15
    sget-object v0, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->SPLITWINDOW_INSTANCE:Lcom/lge/loader/splitwindow/ISplitWindow;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 16
    const/4 v0, 0x0

    #@5
    .line 19
    :goto_5
    return-object v0

    #@6
    .line 18
    :cond_6
    sget-object v1, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->SPLITWINDOW_INSTANCE:Lcom/lge/loader/splitwindow/ISplitWindow;

    #@8
    monitor-enter v1

    #@9
    .line 19
    :try_start_9
    sget-object v0, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->SPLITWINDOW_INSTANCE:Lcom/lge/loader/splitwindow/ISplitWindow;

    #@b
    invoke-interface {v0}, Lcom/lge/loader/splitwindow/ISplitWindow;->getPolicyService()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@e
    move-result-object v0

    #@f
    monitor-exit v1

    #@10
    goto :goto_5

    #@11
    .line 20
    :catchall_11
    move-exception v0

    #@12
    monitor-exit v1
    :try_end_13
    .catchall {:try_start_9 .. :try_end_13} :catchall_11

    #@13
    throw v0
.end method

.method public static launchService(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 25
    sget-object v0, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->SPLITWINDOW_INSTANCE:Lcom/lge/loader/splitwindow/ISplitWindow;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 26
    sget-object v0, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->SPLITWINDOW_INSTANCE:Lcom/lge/loader/splitwindow/ISplitWindow;

    #@6
    invoke-interface {v0, p0}, Lcom/lge/loader/splitwindow/ISplitWindow;->launchService(Landroid/content/Context;)V

    #@9
    .line 29
    :cond_9
    return-void
.end method
