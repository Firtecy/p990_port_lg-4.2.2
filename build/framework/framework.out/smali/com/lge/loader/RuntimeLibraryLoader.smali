.class public Lcom/lge/loader/RuntimeLibraryLoader;
.super Ljava/lang/Object;
.source "RuntimeLibraryLoader.java"


# static fields
.field public static CLIPTRAY_MANAGER:Ljava/lang/String;

.field private static FACTORY_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Singleton",
            "<",
            "Lcom/lge/loader/InstanceCreator;",
            ">;>;"
        }
    .end annotation
.end field

.field public static MDM:Ljava/lang/String;

.field public static MDM_SPRINT:Ljava/lang/String;

.field public static SPLIT_WINDOW:Ljava/lang/String;

.field public static VOLUME_MANAGER:Ljava/lang/String;

.field public static ZDI_INTERACTION:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 15
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    sput-object v0, Lcom/lge/loader/RuntimeLibraryLoader;->FACTORY_MAP:Ljava/util/HashMap;

    #@7
    .line 21
    const-string/jumbo v0, "volume"

    #@a
    sput-object v0, Lcom/lge/loader/RuntimeLibraryLoader;->VOLUME_MANAGER:Ljava/lang/String;

    #@c
    .line 22
    const-string v0, "cliptray"

    #@e
    sput-object v0, Lcom/lge/loader/RuntimeLibraryLoader;->CLIPTRAY_MANAGER:Ljava/lang/String;

    #@10
    .line 23
    const-string/jumbo v0, "mdm"

    #@13
    sput-object v0, Lcom/lge/loader/RuntimeLibraryLoader;->MDM:Ljava/lang/String;

    #@15
    .line 24
    const-string/jumbo v0, "mdm_sprint"

    #@18
    sput-object v0, Lcom/lge/loader/RuntimeLibraryLoader;->MDM_SPRINT:Ljava/lang/String;

    #@1a
    .line 25
    const-string/jumbo v0, "splitwindow"

    #@1d
    sput-object v0, Lcom/lge/loader/RuntimeLibraryLoader;->SPLIT_WINDOW:Ljava/lang/String;

    #@1f
    .line 26
    const-string/jumbo v0, "zdi_interaction"

    #@22
    sput-object v0, Lcom/lge/loader/RuntimeLibraryLoader;->ZDI_INTERACTION:Ljava/lang/String;

    #@24
    .line 64
    sget-object v0, Lcom/lge/loader/RuntimeLibraryLoader;->CLIPTRAY_MANAGER:Ljava/lang/String;

    #@26
    new-instance v1, Lcom/lge/loader/RuntimeLibraryLoader$1;

    #@28
    invoke-direct {v1}, Lcom/lge/loader/RuntimeLibraryLoader$1;-><init>()V

    #@2b
    invoke-static {v0, v1}, Lcom/lge/loader/RuntimeLibraryLoader;->registerCreator(Ljava/lang/String;Landroid/util/Singleton;)V

    #@2e
    .line 88
    sget-object v0, Lcom/lge/loader/RuntimeLibraryLoader;->VOLUME_MANAGER:Ljava/lang/String;

    #@30
    new-instance v1, Lcom/lge/loader/RuntimeLibraryLoader$2;

    #@32
    invoke-direct {v1}, Lcom/lge/loader/RuntimeLibraryLoader$2;-><init>()V

    #@35
    invoke-static {v0, v1}, Lcom/lge/loader/RuntimeLibraryLoader;->registerCreator(Ljava/lang/String;Landroid/util/Singleton;)V

    #@38
    .line 96
    sget-object v0, Lcom/lge/loader/RuntimeLibraryLoader;->SPLIT_WINDOW:Ljava/lang/String;

    #@3a
    new-instance v1, Lcom/lge/loader/RuntimeLibraryLoader$3;

    #@3c
    invoke-direct {v1}, Lcom/lge/loader/RuntimeLibraryLoader$3;-><init>()V

    #@3f
    invoke-static {v0, v1}, Lcom/lge/loader/RuntimeLibraryLoader;->registerCreator(Ljava/lang/String;Landroid/util/Singleton;)V

    #@42
    .line 104
    sget-object v0, Lcom/lge/loader/RuntimeLibraryLoader;->ZDI_INTERACTION:Ljava/lang/String;

    #@44
    new-instance v1, Lcom/lge/loader/RuntimeLibraryLoader$4;

    #@46
    invoke-direct {v1}, Lcom/lge/loader/RuntimeLibraryLoader$4;-><init>()V

    #@49
    invoke-static {v0, v1}, Lcom/lge/loader/RuntimeLibraryLoader;->registerCreator(Ljava/lang/String;Landroid/util/Singleton;)V

    #@4c
    .line 110
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 28
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 29
    return-void
.end method

.method public static getCreator(Ljava/lang/String;)Lcom/lge/loader/InstanceCreator;
    .registers 2
    .parameter "moduleName"

    #@0
    .prologue
    .line 37
    const/4 v0, 0x0

    #@1
    invoke-static {p0, v0}, Lcom/lge/loader/RuntimeLibraryLoader;->getCreator(Ljava/lang/String;Ljava/lang/Object;)Lcom/lge/loader/InstanceCreator;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static getCreator(Ljava/lang/String;Ljava/lang/Object;)Lcom/lge/loader/InstanceCreator;
    .registers 4
    .parameter "moduleName"
    .parameter "args"

    #@0
    .prologue
    .line 48
    sget-object v1, Lcom/lge/loader/RuntimeLibraryLoader;->FACTORY_MAP:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/util/Singleton;

    #@8
    .line 49
    .local v0, singletonCreator:Landroid/util/Singleton;,"Landroid/util/Singleton<Lcom/lge/loader/InstanceCreator;>;"
    if-nez v0, :cond_c

    #@a
    const/4 v1, 0x0

    #@b
    :goto_b
    return-object v1

    #@c
    :cond_c
    invoke-virtual {v0}, Landroid/util/Singleton;->get()Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Lcom/lge/loader/InstanceCreator;

    #@12
    goto :goto_b
.end method

.method private static registerCreator(Ljava/lang/String;Landroid/util/Singleton;)V
    .registers 3
    .parameter "factoryName"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/util/Singleton",
            "<",
            "Lcom/lge/loader/InstanceCreator;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 54
    .local p1, factory:Landroid/util/Singleton;,"Landroid/util/Singleton<Lcom/lge/loader/InstanceCreator;>;"
    sget-object v0, Lcom/lge/loader/RuntimeLibraryLoader;->FACTORY_MAP:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    .line 55
    return-void
.end method
