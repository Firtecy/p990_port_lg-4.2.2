.class public Lcom/lge/loader/mdm/MdmCreator;
.super Lcom/lge/loader/InstanceCreator;
.source "MdmCreator.java"


# static fields
.field private static cloader:Ljava/lang/ClassLoader;


# instance fields
.field private mClass:Ljava/lang/Class;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 16
    new-instance v0, Ldalvik/system/PathClassLoader;

    #@2
    const-string v1, "/system/framework/com.lge.mdm.jar"

    #@4
    const-class v2, Lcom/lge/loader/mdm/MdmCreator;

    #@6
    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    #@9
    move-result-object v2

    #@a
    invoke-direct {v0, v1, v2}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    #@d
    sput-object v0, Lcom/lge/loader/mdm/MdmCreator;->cloader:Ljava/lang/ClassLoader;

    #@f
    .line 18
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 7
    .parameter "mdmType"

    #@0
    .prologue
    .line 20
    invoke-direct {p0}, Lcom/lge/loader/InstanceCreator;-><init>()V

    #@3
    .line 21
    const/4 v0, 0x0

    #@4
    .line 22
    .local v0, constructorName:Ljava/lang/String;
    if-eqz p1, :cond_1a

    #@6
    const-string v2, "Sprint"

    #@8
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_1a

    #@e
    .line 23
    const-string v0, "com.lge.mdm.MDMInterfaceImplSprint"

    #@10
    .line 29
    :goto_10
    const/4 v2, 0x1

    #@11
    :try_start_11
    sget-object v3, Lcom/lge/loader/mdm/MdmCreator;->cloader:Ljava/lang/ClassLoader;

    #@13
    invoke-static {v0, v2, v3}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    #@16
    move-result-object v2

    #@17
    iput-object v2, p0, Lcom/lge/loader/mdm/MdmCreator;->mClass:Ljava/lang/Class;
    :try_end_19
    .catch Ljava/lang/ClassNotFoundException; {:try_start_11 .. :try_end_19} :catch_1d

    #@19
    .line 34
    :goto_19
    return-void

    #@1a
    .line 25
    :cond_1a
    const-string v0, "com.lge.mdm.MDMInterfaceImpl"

    #@1c
    goto :goto_10

    #@1d
    .line 30
    :catch_1d
    move-exception v1

    #@1e
    .line 32
    .local v1, e:Ljava/lang/ClassNotFoundException;
    const-string v2, "Mdm"

    #@20
    new-instance v3, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    const-string v4, "don\'t exist"

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v3

    #@37
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    goto :goto_19
.end method


# virtual methods
.method public newInstance(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter "args"

    #@0
    .prologue
    .line 39
    :try_start_0
    iget-object v1, p0, Lcom/lge/loader/mdm/MdmCreator;->mClass:Ljava/lang/Class;

    #@2
    if-eqz v1, :cond_f

    #@4
    .line 40
    iget-object v1, p0, Lcom/lge/loader/mdm/MdmCreator;->mClass:Ljava/lang/Class;

    #@6
    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;
    :try_end_9
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_9} :catch_b
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_9} :catch_11

    #@9
    move-result-object v1

    #@a
    .line 49
    :goto_a
    return-object v1

    #@b
    .line 42
    :catch_b
    move-exception v0

    #@c
    .line 44
    .local v0, e:Ljava/lang/InstantiationException;
    invoke-virtual {v0}, Ljava/lang/InstantiationException;->printStackTrace()V

    #@f
    .line 49
    .end local v0           #e:Ljava/lang/InstantiationException;
    :cond_f
    :goto_f
    const/4 v1, 0x0

    #@10
    goto :goto_a

    #@11
    .line 45
    :catch_11
    move-exception v0

    #@12
    .line 47
    .local v0, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@15
    goto :goto_f
.end method
