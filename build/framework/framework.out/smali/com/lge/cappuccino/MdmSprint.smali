.class public Lcom/lge/cappuccino/MdmSprint;
.super Ljava/lang/Object;
.source "MdmSprint.java"


# static fields
.field private static sMdm:Lcom/lge/cappuccino/IMdmSprint;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 10
    sput-object v7, Lcom/lge/cappuccino/MdmSprint;->sMdm:Lcom/lge/cappuccino/IMdmSprint;

    #@3
    .line 13
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_MDM:Z

    #@5
    if-eqz v4, :cond_23

    #@7
    .line 14
    new-instance v1, Ldalvik/system/PathClassLoader;

    #@9
    const-string v4, "/system/framework/com.lge.mdm.jar"

    #@b
    const-class v5, Lcom/lge/cappuccino/Mdm;

    #@d
    invoke-virtual {v5}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    #@10
    move-result-object v5

    #@11
    invoke-direct {v1, v4, v5}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    #@14
    .line 17
    .local v1, cloader:Ljava/lang/ClassLoader;
    :try_start_14
    const-string v4, "com.lge.mdm.MDMInterfaceImplSprint"

    #@16
    const/4 v5, 0x1

    #@17
    invoke-static {v4, v5, v1}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    #@1a
    move-result-object v0

    #@1b
    .line 18
    .local v0, c:Ljava/lang/Class;
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    #@1e
    move-result-object v3

    #@1f
    check-cast v3, Lcom/lge/cappuccino/IMdmSprint;

    #@21
    .line 19
    .local v3, mdm:Lcom/lge/cappuccino/IMdmSprint;
    sput-object v3, Lcom/lge/cappuccino/MdmSprint;->sMdm:Lcom/lge/cappuccino/IMdmSprint;
    :try_end_23
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_23} :catch_24

    #@23
    .line 25
    .end local v0           #c:Ljava/lang/Class;
    .end local v3           #mdm:Lcom/lge/cappuccino/IMdmSprint;
    :cond_23
    :goto_23
    return-void

    #@24
    .line 20
    :catch_24
    move-exception v2

    #@25
    .line 21
    .local v2, e:Ljava/lang/Exception;
    const-string v4, "Mdm"

    #@27
    new-instance v5, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v6, "Library loading failure: "

    #@2e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v5

    #@32
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v5

    #@36
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v5

    #@3a
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 22
    sput-object v7, Lcom/lge/cappuccino/MdmSprint;->sMdm:Lcom/lge/cappuccino/IMdmSprint;

    #@3f
    goto :goto_23
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 9
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static getInstance()Lcom/lge/cappuccino/IMdmSprint;
    .registers 1

    #@0
    .prologue
    .line 28
    sget-object v0, Lcom/lge/cappuccino/MdmSprint;->sMdm:Lcom/lge/cappuccino/IMdmSprint;

    #@2
    return-object v0
.end method
