.class public Lcom/lge/app/atsagent/AtsViewAgent;
.super Ljava/lang/Object;
.source "AtsViewAgent.java"


# static fields
.field private static final ETA2_PACKAGE_NAME:Ljava/lang/String; = "lgerp.android.eta2"

#the value of this static final field might be set in the static constructor
.field public static final IS_ETA2_ACTIVATED:Z = false

.field private static final TAG:Ljava/lang/String; = "AtsViewAgent"

.field private static final VIEWAGENT2_CLASS_NAME:Ljava/lang/String; = "com.lge.atsagent.ViewAgent"

.field private static mClassLoader:Ljava/lang/ClassLoader;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 22
    const-string/jumbo v0, "persist.sys.ats_start"

    #@3
    const/4 v1, 0x0

    #@4
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@7
    move-result v0

    #@8
    sput-boolean v0, Lcom/lge/app/atsagent/AtsViewAgent;->IS_ETA2_ACTIVATED:Z

    #@a
    .line 24
    const/4 v0, 0x0

    #@b
    sput-object v0, Lcom/lge/app/atsagent/AtsViewAgent;->mClassLoader:Ljava/lang/ClassLoader;

    #@d
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 16
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static createViewAgent(Landroid/view/View;Ljava/lang/String;)Lcom/lge/app/atsagent/IViewAgent;
    .registers 8
    .parameter "rootView"
    .parameter "viewHolderType"

    #@0
    .prologue
    .line 33
    sget-boolean v3, Lcom/lge/app/atsagent/AtsViewAgent;->IS_ETA2_ACTIVATED:Z

    #@2
    if-eqz v3, :cond_57

    #@4
    if-eqz p0, :cond_57

    #@6
    .line 35
    :try_start_6
    sget-object v3, Lcom/lge/app/atsagent/AtsViewAgent;->mClassLoader:Ljava/lang/ClassLoader;

    #@8
    if-nez v3, :cond_10

    #@a
    .line 36
    invoke-static {}, Lcom/lge/app/atsagent/AtsViewAgent;->getClassLoader()Ljava/lang/ClassLoader;

    #@d
    move-result-object v3

    #@e
    sput-object v3, Lcom/lge/app/atsagent/AtsViewAgent;->mClassLoader:Ljava/lang/ClassLoader;

    #@10
    .line 39
    :cond_10
    sget-object v3, Lcom/lge/app/atsagent/AtsViewAgent;->mClassLoader:Ljava/lang/ClassLoader;

    #@12
    if-eqz v3, :cond_57

    #@14
    .line 40
    const-string v3, "com.lge.atsagent.ViewAgent"

    #@16
    const/4 v4, 0x1

    #@17
    sget-object v5, Lcom/lge/app/atsagent/AtsViewAgent;->mClassLoader:Ljava/lang/ClassLoader;

    #@19
    invoke-static {v3, v4, v5}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    #@1c
    move-result-object v0

    #@1d
    .line 41
    .local v0, c:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    const/4 v3, 0x2

    #@1e
    new-array v3, v3, [Ljava/lang/Class;

    #@20
    const/4 v4, 0x0

    #@21
    const-class v5, Landroid/view/View;

    #@23
    aput-object v5, v3, v4

    #@25
    const/4 v4, 0x1

    #@26
    const-class v5, Ljava/lang/String;

    #@28
    aput-object v5, v3, v4

    #@2a
    invoke-virtual {v0, v3}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    #@2d
    move-result-object v1

    #@2e
    .line 42
    .local v1, con:Ljava/lang/reflect/Constructor;,"Ljava/lang/reflect/Constructor<*>;"
    const/4 v3, 0x2

    #@2f
    new-array v3, v3, [Ljava/lang/Object;

    #@31
    const/4 v4, 0x0

    #@32
    aput-object p0, v3, v4

    #@34
    const/4 v4, 0x1

    #@35
    aput-object p1, v3, v4

    #@37
    invoke-virtual {v1, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    #@3a
    move-result-object v3

    #@3b
    check-cast v3, Lcom/lge/app/atsagent/IViewAgent;
    :try_end_3d
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_3d} :catch_3e

    #@3d
    .line 48
    .end local v0           #c:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    .end local v1           #con:Ljava/lang/reflect/Constructor;,"Ljava/lang/reflect/Constructor<*>;"
    :goto_3d
    return-object v3

    #@3e
    .line 44
    :catch_3e
    move-exception v2

    #@3f
    .line 45
    .local v2, e:Ljava/lang/Exception;
    const-string v3, "AtsViewAgent"

    #@41
    new-instance v4, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v5, "ViewAgent classes loading failure: "

    #@48
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v4

    #@4c
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v4

    #@50
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v4

    #@54
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    .line 48
    .end local v2           #e:Ljava/lang/Exception;
    :cond_57
    const/4 v3, 0x0

    #@58
    goto :goto_3d
.end method

.method private static getClassLoader()Ljava/lang/ClassLoader;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 52
    const-string/jumbo v2, "package"

    #@4
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@7
    move-result-object v2

    #@8
    invoke-static {v2}, Landroid/content/pm/IPackageManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageManager;

    #@b
    move-result-object v1

    #@c
    .line 54
    .local v1, pm:Landroid/content/pm/IPackageManager;
    if-eqz v1, :cond_33

    #@e
    .line 55
    const-string/jumbo v2, "lgerp.android.eta2"

    #@11
    invoke-interface {v1, v2, v3, v3}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;

    #@14
    move-result-object v0

    #@15
    .line 56
    .local v0, info:Landroid/content/pm/ApplicationInfo;
    if-eqz v0, :cond_2c

    #@17
    .line 57
    const-string v2, "AtsViewAgent"

    #@19
    const-string v3, "Loading ViewAgent classes from Eta2..."

    #@1b
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 58
    new-instance v2, Ldalvik/system/PathClassLoader;

    #@20
    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    #@22
    const-class v4, Lcom/lge/app/atsagent/AtsViewAgent;

    #@24
    invoke-virtual {v4}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    #@27
    move-result-object v4

    #@28
    invoke-direct {v2, v3, v4}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    #@2b
    .line 64
    .end local v0           #info:Landroid/content/pm/ApplicationInfo;
    :goto_2b
    return-object v2

    #@2c
    .line 60
    .restart local v0       #info:Landroid/content/pm/ApplicationInfo;
    :cond_2c
    const-string v2, "AtsViewAgent"

    #@2e
    const-string v3, "No Eta2 is installed"

    #@30
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 64
    .end local v0           #info:Landroid/content/pm/ApplicationInfo;
    :cond_33
    const/4 v2, 0x0

    #@34
    goto :goto_2b
.end method
