.class Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;
.super Ljava/lang/Object;
.source "MatchingDigitsHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/util/MatchingDigitsHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MatchingDigitsMechanism"
.end annotation


# static fields
.field private static final DIGITEL:Ljava/lang/String; = "Digitel"

.field private static final MNC_DIGITEL:Ljava/lang/String; = "02"

.field private static final MNC_MOVILNET:Ljava/lang/String; = "06"

.field private static final MNC_MOVISTAR:Ljava/lang/String; = "04"

.field private static final MOVILNET:Ljava/lang/String; = "Movilnet"

.field private static final MOVISTAR:Ljava/lang/String; = "Movistar"

.field private static final PREFIXES_DIGITEL:Ljava/lang/String; = "412"

.field private static final PREFIXES_MOVILNET:Ljava/lang/String; = "416;426"

.field private static final PREFIXES_MOVISTAR:Ljava/lang/String; = "414;424"

.field private static final VENEZUELA_DEFAULT_MATCHING_NUMBER:I = 0xa

.field public static final VENEZUELA_SECONDARY_MATCH_NUMBER:I = 0x7

.field private static mCurrentMnc:Ljava/lang/String;

.field private static mIsvenezuelaMatching:Z

.field private static mMatchingDigitsNumber:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 137
    const/4 v0, -0x1

    #@1
    sput v0, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;->mMatchingDigitsNumber:I

    #@3
    .line 138
    const/4 v0, 0x0

    #@4
    sput-boolean v0, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;->mIsvenezuelaMatching:Z

    #@6
    .line 139
    const-string v0, ""

    #@8
    sput-object v0, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;->mCurrentMnc:Ljava/lang/String;

    #@a
    return-void
.end method

.method public constructor <init>()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x5

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v4, 0x1

    #@3
    const/4 v5, 0x0

    #@4
    .line 152
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    .line 153
    const-string v3, "gsm.sim.operator.numeric"

    #@9
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@c
    move-result-object v1

    #@d
    .line 154
    .local v1, mccmnc:Ljava/lang/String;
    if-eqz v1, :cond_44

    #@f
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@12
    move-result v3

    #@13
    if-lt v3, v7, :cond_44

    #@15
    .line 155
    invoke-virtual {v1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    .line 156
    .local v0, mcc:Ljava/lang/String;
    invoke-virtual {v1, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    .line 157
    .local v2, mnc:Ljava/lang/String;
    const-string v3, "734"

    #@1f
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v3

    #@23
    if-eqz v3, :cond_45

    #@25
    move v3, v4

    #@26
    :goto_26
    sput-boolean v3, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;->mIsvenezuelaMatching:Z

    #@28
    .line 158
    sget-boolean v3, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;->mIsvenezuelaMatching:Z

    #@2a
    if-eqz v3, :cond_44

    #@2c
    .line 159
    sput-object v2, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;->mCurrentMnc:Ljava/lang/String;

    #@2e
    .line 160
    const/16 v3, 0xa

    #@30
    sput v3, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;->mMatchingDigitsNumber:I

    #@32
    .line 161
    const-string v3, "MatchingDigitsMechanism"

    #@34
    const-string v6, "Mcc:%s | Mnc:%s"

    #@36
    const/4 v7, 0x2

    #@37
    new-array v7, v7, [Ljava/lang/Object;

    #@39
    aput-object v0, v7, v5

    #@3b
    aput-object v2, v7, v4

    #@3d
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@40
    move-result-object v4

    #@41
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 164
    .end local v0           #mcc:Ljava/lang/String;
    .end local v2           #mnc:Ljava/lang/String;
    :cond_44
    return-void

    #@45
    .restart local v0       #mcc:Ljava/lang/String;
    .restart local v2       #mnc:Ljava/lang/String;
    :cond_45
    move v3, v5

    #@46
    .line 157
    goto :goto_26
.end method


# virtual methods
.method getCurrentCarrier()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 175
    sget-object v0, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;->mCurrentMnc:Ljava/lang/String;

    #@2
    const-string v1, "02"

    #@4
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_d

    #@a
    const-string v0, "Digitel"

    #@c
    .line 178
    :goto_c
    return-object v0

    #@d
    .line 176
    :cond_d
    sget-object v0, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;->mCurrentMnc:Ljava/lang/String;

    #@f
    const-string v1, "04"

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_1a

    #@17
    const-string v0, "Movistar"

    #@19
    goto :goto_c

    #@1a
    .line 177
    :cond_1a
    sget-object v0, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;->mCurrentMnc:Ljava/lang/String;

    #@1c
    const-string v1, "06"

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v0

    #@22
    if-eqz v0, :cond_27

    #@24
    const-string v0, "Movilnet"

    #@26
    goto :goto_c

    #@27
    .line 178
    :cond_27
    const-string v0, ""

    #@29
    goto :goto_c
.end method

.method getMatchingDigitsNumber()I
    .registers 2

    #@0
    .prologue
    .line 167
    sget v0, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;->mMatchingDigitsNumber:I

    #@2
    return v0
.end method

.method getOperatorPrefixes()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 182
    sget-object v0, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;->mCurrentMnc:Ljava/lang/String;

    #@2
    const-string v1, "02"

    #@4
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_d

    #@a
    const-string v0, "412"

    #@c
    .line 185
    :goto_c
    return-object v0

    #@d
    .line 183
    :cond_d
    sget-object v0, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;->mCurrentMnc:Ljava/lang/String;

    #@f
    const-string v1, "04"

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_1a

    #@17
    const-string v0, "414;424"

    #@19
    goto :goto_c

    #@1a
    .line 184
    :cond_1a
    sget-object v0, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;->mCurrentMnc:Ljava/lang/String;

    #@1c
    const-string v1, "06"

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v0

    #@22
    if-eqz v0, :cond_27

    #@24
    const-string v0, "416;426"

    #@26
    goto :goto_c

    #@27
    .line 185
    :cond_27
    const-string v0, ""

    #@29
    goto :goto_c
.end method

.method isVenezuela()Z
    .registers 2

    #@0
    .prologue
    .line 171
    sget-boolean v0, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;->mIsvenezuelaMatching:Z

    #@2
    return v0
.end method

.method preparePhoneNumberForMatching(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "phoneNumber"

    #@0
    .prologue
    .line 189
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v2

    #@4
    if-eqz v2, :cond_9

    #@6
    const-string v1, ""

    #@8
    .line 201
    :cond_8
    :goto_8
    return-object v1

    #@9
    .line 191
    :cond_9
    invoke-virtual {p0, p1}, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;->prepareReallyDialableNumber(Ljava/lang/String;)Ljava/lang/String;

    #@c
    move-result-object p1

    #@d
    .line 193
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_16

    #@13
    const-string v1, ""

    #@15
    goto :goto_8

    #@16
    .line 195
    :cond_16
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@19
    move-result v0

    #@1a
    .line 196
    .local v0, numberLength:I
    move-object v1, p1

    #@1b
    .line 198
    .local v1, numberToQuery:Ljava/lang/String;
    sget v2, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;->mMatchingDigitsNumber:I

    #@1d
    if-le v0, v2, :cond_8

    #@1f
    .line 199
    sget v2, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;->mMatchingDigitsNumber:I

    #@21
    sub-int v2, v0, v2

    #@23
    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    goto :goto_8
.end method

.method prepareReallyDialableNumber(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "phoneNumber"

    #@0
    .prologue
    .line 205
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v4

    #@4
    if-eqz v4, :cond_9

    #@6
    const-string v4, ""

    #@8
    .line 215
    :goto_8
    return-object v4

    #@9
    .line 206
    :cond_9
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@c
    move-result v2

    #@d
    .line 207
    .local v2, len:I
    new-instance v3, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    #@12
    .line 209
    .local v3, ret:Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    #@13
    .local v1, i:I
    :goto_13
    if-ge v1, v2, :cond_25

    #@15
    .line 210
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    #@18
    move-result v0

    #@19
    .line 211
    .local v0, c:C
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isReallyDialable(C)Z

    #@1c
    move-result v4

    #@1d
    if-eqz v4, :cond_22

    #@1f
    .line 212
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@22
    .line 209
    :cond_22
    add-int/lit8 v1, v1, 0x1

    #@24
    goto :goto_13

    #@25
    .line 215
    .end local v0           #c:C
    :cond_25
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v4

    #@29
    goto :goto_8
.end method
