.class public final Lcom/lge/util/MatchingDigitsHelper;
.super Ljava/lang/Object;
.source "MatchingDigitsHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;
    }
.end annotation


# static fields
.field private static final TAG_MATCHING:Ljava/lang/String; = "MatchingDigitsMechanism"

.field private static mMatchingMechanism:Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 14
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/lge/util/MatchingDigitsHelper;->mMatchingMechanism:Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;

    #@3
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 11
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 132
    return-void
.end method

.method public static compareForMatching(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 11
    .parameter "num1"
    .parameter "num2"

    #@0
    .prologue
    const/4 v8, 0x2

    #@1
    const/4 v7, 0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 32
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@6
    move-result v4

    #@7
    if-nez v4, :cond_f

    #@9
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@c
    move-result v4

    #@d
    if-eqz v4, :cond_11

    #@f
    :cond_f
    move v0, v3

    #@10
    .line 54
    :goto_10
    return v0

    #@11
    .line 35
    :cond_11
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@18
    move-result v4

    #@19
    if-nez v4, :cond_25

    #@1b
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@22
    move-result v4

    #@23
    if-eqz v4, :cond_27

    #@25
    :cond_25
    move v0, v3

    #@26
    .line 36
    goto :goto_10

    #@27
    .line 38
    :cond_27
    const/4 v0, 0x0

    #@28
    .line 39
    .local v0, isEqual:Z
    invoke-static {}, Lcom/lge/util/MatchingDigitsHelper;->getMatchingMechanism()Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;

    #@2b
    move-result-object v1

    #@2c
    .line 41
    .local v1, matchingMechanism:Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;
    invoke-virtual {v1, p0}, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;->preparePhoneNumberForMatching(Ljava/lang/String;)Ljava/lang/String;

    #@2f
    move-result-object p0

    #@30
    .line 42
    invoke-virtual {v1, p1}, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;->preparePhoneNumberForMatching(Ljava/lang/String;)Ljava/lang/String;

    #@33
    move-result-object p1

    #@34
    .line 43
    const-string v4, "MatchingDigitsMechanism"

    #@36
    const-string v5, "[LGESP] num1 = %s and num2 = %s)"

    #@38
    new-array v6, v8, [Ljava/lang/Object;

    #@3a
    aput-object p0, v6, v3

    #@3c
    aput-object p1, v6, v7

    #@3e
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@41
    move-result-object v5

    #@42
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 45
    invoke-virtual {v1}, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;->getOperatorPrefixes()Ljava/lang/String;

    #@48
    move-result-object v4

    #@49
    const-string v5, ";"

    #@4b
    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@4e
    move-result-object v2

    #@4f
    .line 47
    .local v2, prefixes:[Ljava/lang/String;
    if-eqz v2, :cond_8a

    #@51
    array-length v4, v2

    #@52
    if-lez v4, :cond_8a

    #@54
    .line 48
    invoke-virtual {v1}, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;->getMatchingDigitsNumber()I

    #@57
    move-result v4

    #@58
    invoke-virtual {v1}, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;->getCurrentCarrier()Ljava/lang/String;

    #@5b
    move-result-object v5

    #@5c
    invoke-static {p0, p1, v4, v5, v2}, Lcom/lge/util/MatchingDigitsHelper;->phoneNumbersEqual_VE(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[Ljava/lang/String;)Z

    #@5f
    move-result v0

    #@60
    .line 49
    const-string v4, "MatchingDigitsMechanism"

    #@62
    const-string v5, "[LGESP] phoneNumbersEqual_VE(%s, %s, %s, %s, %s)"

    #@64
    const/4 v6, 0x5

    #@65
    new-array v6, v6, [Ljava/lang/Object;

    #@67
    aput-object p0, v6, v3

    #@69
    aput-object p1, v6, v7

    #@6b
    invoke-virtual {v1}, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;->getMatchingDigitsNumber()I

    #@6e
    move-result v3

    #@6f
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@72
    move-result-object v3

    #@73
    aput-object v3, v6, v8

    #@75
    const/4 v3, 0x3

    #@76
    invoke-virtual {v1}, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;->getCurrentCarrier()Ljava/lang/String;

    #@79
    move-result-object v7

    #@7a
    aput-object v7, v6, v3

    #@7c
    const/4 v3, 0x4

    #@7d
    invoke-virtual {v1}, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;->getOperatorPrefixes()Ljava/lang/String;

    #@80
    move-result-object v7

    #@81
    aput-object v7, v6, v3

    #@83
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@86
    move-result-object v3

    #@87
    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8a
    .line 53
    :cond_8a
    const-string v4, "MatchingDigitsMechanism"

    #@8c
    new-instance v3, Ljava/lang/StringBuilder;

    #@8e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@91
    const-string v5, "[LGESP] Matching digits compareForMatching result: "

    #@93
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v5

    #@97
    if-eqz v0, :cond_a8

    #@99
    const-string v3, "equal"

    #@9b
    :goto_9b
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v3

    #@9f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a2
    move-result-object v3

    #@a3
    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a6
    goto/16 :goto_10

    #@a8
    :cond_a8
    const-string/jumbo v3, "not equal"

    #@ab
    goto :goto_9b
.end method

.method public static getCarrierVenezuela()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 63
    invoke-static {}, Lcom/lge/util/MatchingDigitsHelper;->getMatchingMechanism()Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;

    #@3
    move-result-object v0

    #@4
    .line 64
    .local v0, matchingMechanism:Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;
    invoke-virtual {v0}, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;->getCurrentCarrier()Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    return-object v1
.end method

.method private static getMatchingMechanism()Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;
    .registers 2

    #@0
    .prologue
    .line 17
    sget-object v0, Lcom/lge/util/MatchingDigitsHelper;->mMatchingMechanism:Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;

    #@2
    if-nez v0, :cond_12

    #@4
    .line 18
    const-string v0, "MatchingDigitsMechanism"

    #@6
    const-string v1, "[LGESP] Creating a new instance of MatchingDigitsMechanism"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 19
    new-instance v0, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;

    #@d
    invoke-direct {v0}, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;-><init>()V

    #@10
    sput-object v0, Lcom/lge/util/MatchingDigitsHelper;->mMatchingMechanism:Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;

    #@12
    .line 21
    :cond_12
    sget-object v0, Lcom/lge/util/MatchingDigitsHelper;->mMatchingMechanism:Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;

    #@14
    return-object v0
.end method

.method public static getOperatorPrefixesVenezuela()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 68
    invoke-static {}, Lcom/lge/util/MatchingDigitsHelper;->getMatchingMechanism()Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;

    #@3
    move-result-object v0

    #@4
    .line 69
    .local v0, matchingMechanism:Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;
    invoke-virtual {v0}, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;->getOperatorPrefixes()Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    return-object v1
.end method

.method public static isVenezuela()Z
    .registers 2

    #@0
    .prologue
    .line 58
    invoke-static {}, Lcom/lge/util/MatchingDigitsHelper;->getMatchingMechanism()Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;

    #@3
    move-result-object v0

    #@4
    .line 59
    .local v0, matchingMechanism:Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;
    invoke-virtual {v0}, Lcom/lge/util/MatchingDigitsHelper$MatchingDigitsMechanism;->isVenezuela()Z

    #@7
    move-result v1

    #@8
    return v1
.end method

.method private static phoneNumbersEqual_VE(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[Ljava/lang/String;)Z
    .registers 13
    .parameter "num1"
    .parameter "num2"
    .parameter "minMatch"
    .parameter "carrier"
    .parameter "prefixes"

    #@0
    .prologue
    .line 74
    const/4 v2, 0x0

    #@1
    .line 75
    .local v2, isEqual:Z
    const/4 v4, 0x7

    #@2
    .line 77
    .local v4, minMatch2:I
    const/4 v0, 0x0

    #@3
    .line 78
    .local v0, i:I
    const-string v6, "Movistar"

    #@5
    invoke-virtual {p3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8
    move-result v6

    #@9
    if-eqz v6, :cond_17

    #@b
    .line 79
    invoke-static {p0, p1, p2}, Landroid/telephony/PhoneNumberUtils;->compareLoosely(Ljava/lang/String;Ljava/lang/String;I)Z

    #@e
    move-result v2

    #@f
    .line 80
    const-string v6, "MatchingDigitsMechanism"

    #@11
    const-string v7, "Carrier is Movistar"

    #@13
    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 126
    :cond_16
    :goto_16
    return v2

    #@17
    .line 82
    :cond_17
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@1a
    move-result v6

    #@1b
    if-lt v6, p2, :cond_4d

    #@1d
    .line 84
    invoke-static {p0, p1, p2}, Landroid/telephony/PhoneNumberUtils;->compareLoosely(Ljava/lang/String;Ljava/lang/String;I)Z

    #@20
    move-result v2

    #@21
    .line 86
    if-nez v2, :cond_16

    #@23
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@26
    move-result v6

    #@27
    if-ne v6, v4, :cond_16

    #@29
    .line 87
    const/4 v3, 0x0

    #@2a
    .line 89
    .local v3, isPrefixEqual:Z
    const/4 v0, 0x0

    #@2b
    .line 90
    :goto_2b
    array-length v6, p4

    #@2c
    if-ge v0, v6, :cond_3d

    #@2e
    if-nez v3, :cond_3d

    #@30
    .line 91
    add-int/lit8 v1, v0, 0x1

    #@32
    .end local v0           #i:I
    .local v1, i:I
    aget-object v6, p4, v0

    #@34
    invoke-virtual {p1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@37
    move-result v6

    #@38
    if-eqz v6, :cond_85

    #@3a
    .line 92
    const/4 v3, 0x1

    #@3b
    move v0, v1

    #@3c
    .end local v1           #i:I
    .restart local v0       #i:I
    goto :goto_2b

    #@3d
    .line 95
    :cond_3d
    if-eqz v3, :cond_16

    #@3f
    .line 97
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@42
    move-result v6

    #@43
    sub-int/2addr v6, v4

    #@44
    invoke-virtual {p1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@47
    move-result-object p1

    #@48
    .line 99
    invoke-static {p0, p1, v4}, Landroid/telephony/PhoneNumberUtils;->compareLoosely(Ljava/lang/String;Ljava/lang/String;I)Z

    #@4b
    move-result v2

    #@4c
    goto :goto_16

    #@4d
    .line 103
    .end local v3           #isPrefixEqual:Z
    :cond_4d
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@50
    move-result v6

    #@51
    if-ne v6, v4, :cond_80

    #@53
    .line 105
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@56
    move-result v6

    #@57
    if-ne v6, v4, :cond_5d

    #@59
    .line 106
    invoke-static {p0, p1, v4}, Landroid/telephony/PhoneNumberUtils;->compareLoosely(Ljava/lang/String;Ljava/lang/String;I)Z

    #@5c
    move-result v2

    #@5d
    .line 109
    :cond_5d
    if-nez v2, :cond_16

    #@5f
    .line 110
    const/4 v5, 0x0

    #@60
    .line 111
    .local v5, numTmp:Ljava/lang/String;
    const/4 v0, 0x0

    #@61
    .line 112
    :goto_61
    array-length v6, p4

    #@62
    if-ge v0, v6, :cond_16

    #@64
    if-nez v2, :cond_16

    #@66
    .line 114
    new-instance v6, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    aget-object v7, p4, v0

    #@6d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v6

    #@71
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v6

    #@75
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v5

    #@79
    .line 115
    invoke-static {p0, v5, p2}, Landroid/telephony/PhoneNumberUtils;->compareLoosely(Ljava/lang/String;Ljava/lang/String;I)Z

    #@7c
    move-result v2

    #@7d
    .line 116
    add-int/lit8 v0, v0, 0x1

    #@7f
    goto :goto_61

    #@80
    .line 122
    .end local v5           #numTmp:Ljava/lang/String;
    :cond_80
    invoke-static {p0, p1, p2}, Landroid/telephony/PhoneNumberUtils;->compareLoosely(Ljava/lang/String;Ljava/lang/String;I)Z

    #@83
    move-result v2

    #@84
    goto :goto_16

    #@85
    .end local v0           #i:I
    .restart local v1       #i:I
    .restart local v3       #isPrefixEqual:Z
    :cond_85
    move v0, v1

    #@86
    .end local v1           #i:I
    .restart local v0       #i:I
    goto :goto_2b
.end method
