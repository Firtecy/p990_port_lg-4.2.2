.class public final Lcom/lge/internal/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/internal/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final BT_tethered_notification_title:I = 0x2090152

.field public static final CnapMmi:I = 0x2090119

.field public static final LGECfbMmi:I = 0x2090154

.field public static final LGECfnrMmi:I = 0x2090156

.field public static final LGECfnryMmi:I = 0x2090155

.field public static final LGECfuMmi:I = 0x2090153

.field public static final PUK2_blocked_LGEres:I = 0x209012e

.field public static final PdpRejectedTitle:I = 0x209029d

.field public static final Pin2Mmi:I = 0x209012f

.field public static final Puk2Mmi:I = 0x2090131

.field public static final PukMmi:I = 0x2090130

.field public static final STR_POPUPMESSAGE_SECURITY_SETPINCODEREQON_COMMON_NORMAL:I = 0x209012c

.field public static final STR_TITLETEXT_UICC_INVAILDPIN_COMMON_SHORT:I = 0x2090116

.field public static final STR_TITLETEXT_UICC_INVAILDPUK_COMMON_SHORT:I = 0x2090110

.field public static final STR_TITLETEXT_UICC_MISMATCHPINS_COMMON_SHORT:I = 0x2090113

.field public static final STR_TITLETEXT_UICC_NEWPIN_COMMON_SHORT:I = 0x2090111

.field public static final STR_TITLETEXT_UICC_PERMBLOCK_COMMON_SHORT:I = 0x2090115

.field public static final STR_TITLETEXT_UICC_REMAINING_COMMON_SHORT:I = 0x2090114

.field public static final STR_TITLETEXT_UICC_TYPEPUK_COMMON_SHORT:I = 0x209010f

.field public static final STR_TITLETEXT_UICC_USIMPERSOCHECK:I = 0x20902d6

.field public static final STR_TITLETEXT_UICC_VERIFYPIN_COMMON_SHORT:I = 0x2090112

.field public static final STR_TITLETEXT_USIMPERSO_CODE_CHANGE_EXCEED:I = 0x20902d4

.field public static final STR_TITLETEXT_USIMPERSO_CODE_RETRY_LEFT:I = 0x20902d5

.field public static final STR_res_ABUDHABI_NORMAL:I = 0x209024a

.field public static final STR_res_AFGHANISTAN_NORMAL:I = 0x20901da

.field public static final STR_res_ALBALIA_NORMAL:I = 0x20901db

.field public static final STR_res_ALGERIA_NORMAL:I = 0x2090175

.field public static final STR_res_AMERICANSAMOA_NORMAL:I = 0x20901dc

.field public static final STR_res_ANDORRA_NORMAL:I = 0x20901dd

.field public static final STR_res_ANGOLA_NORMAL:I = 0x2090176

.field public static final STR_res_ANGUILLA_NORMAL:I = 0x20901de

.field public static final STR_res_ANTIGUAANDBARBUDA_NORMAL:I = 0x20901df

.field public static final STR_res_ARGENTINA_NORMAL:I = 0x2090177

.field public static final STR_res_ARMENIA_NORMAL:I = 0x20901e0

.field public static final STR_res_ARUBA_NORMAL:I = 0x20901e1

.field public static final STR_res_AUSTRALIA_NORMAL:I = 0x20901e2

.field public static final STR_res_AUSTRIA_NORMAL:I = 0x20901e3

.field public static final STR_res_AZERBAIJANIREPUBLIC_NORMAL:I = 0x20901e4

.field public static final STR_res_BAHAMAS_NORMAL:I = 0x2090178

.field public static final STR_res_BAHRAIN_NORMAL:I = 0x20901e5

.field public static final STR_res_BANGLADESH_NORMAL:I = 0x2090179

.field public static final STR_res_BARBADOS_NORMAL:I = 0x209017a

.field public static final STR_res_BELARUS_NORMAL:I = 0x209017b

.field public static final STR_res_BELGIUM_NORMAL:I = 0x20901e6

.field public static final STR_res_BELIZE_NORMAL:I = 0x209017c

.field public static final STR_res_BENIN_NORMAL:I = 0x20901e7

.field public static final STR_res_BERMUDA_NORMAL:I = 0x209017d

.field public static final STR_res_BHUTAN_NORMAL:I = 0x20901e8

.field public static final STR_res_BOLIVIA_NORMAL:I = 0x20901e9

.field public static final STR_res_BOSNIAANDHERZEGOVINA_NORMAL:I = 0x20901ea

.field public static final STR_res_BOTSWANA_NORMAL:I = 0x209017e

.field public static final STR_res_BRAZIL_NORMAL:I = 0x209017f

.field public static final STR_res_BRITISHVIRGINISLANDS_NORMAL:I = 0x2090180

.field public static final STR_res_BRUNEIDARUSSALAM_NORMAL:I = 0x20901eb

.field public static final STR_res_BULGARIA_NORMAL:I = 0x20901ec

.field public static final STR_res_BURKINAFASO_NORMAL:I = 0x20901ed

.field public static final STR_res_BURUNDI_NORMAL:I = 0x20901ee

.field public static final STR_res_CAMBODIA_NORMAL:I = 0x20901ef

.field public static final STR_res_CAMEROON_NORMAL:I = 0x2090181

.field public static final STR_res_CANADA_NORMAL:I = 0x2090182

.field public static final STR_res_CAPEVERDE_NORMAL:I = 0x2090183

.field public static final STR_res_CAUSECODE_NORMAL:I = 0x2090315

.field public static final STR_res_CAYMANISLANDS_NORMAL:I = 0x2090184

.field public static final STR_res_CENTRALAFRICANREP_SHORT:I = 0x2090185

.field public static final STR_res_CHAD_NORMAL:I = 0x20901f0

.field public static final STR_res_CHILE_NORMAL:I = 0x2090186

.field public static final STR_res_CHINA_NORMAL:I = 0x2090187

.field public static final STR_res_COLOMBIA_NORMAL:I = 0x2090188

.field public static final STR_res_COMOROS_NORMAL:I = 0x20901f1

.field public static final STR_res_COOKISLAND_NORMAL:I = 0x20901f2

.field public static final STR_res_COSTARICA_NORMAL:I = 0x20901f3

.field public static final STR_res_COTEDIVOIRE_NORMAL:I = 0x2090189

.field public static final STR_res_CROATIA_NORMAL:I = 0x20901f4

.field public static final STR_res_CUBA_NORMAL:I = 0x20901f5

.field public static final STR_res_CYPRUS_NORMAL:I = 0x20901f6

.field public static final STR_res_CZECHREPUBLIC_NORMAL:I = 0x209018a

.field public static final STR_res_DEMOCRATICREPCONGO_SHORT:I = 0x209018b

.field public static final STR_res_DENMARK_NORMAL:I = 0x209018c

.field public static final STR_res_DJIBOUTI_NORMAL:I = 0x209018d

.field public static final STR_res_DOMINICANREP_SHORT:I = 0x209018e

.field public static final STR_res_DOMINICA_NORMAL:I = 0x20901f7

.field public static final STR_res_EASTTIMOR_NORMAL:I = 0x20901f8

.field public static final STR_res_ECUADOR_NORMAL:I = 0x209018f

.field public static final STR_res_EGYPT_NORMAL:I = 0x2090190

.field public static final STR_res_ELSALVADOR_NORMAL:I = 0x2090191

.field public static final STR_res_EQUATORIALGUINEA_NORMAL:I = 0x20901f9

.field public static final STR_res_ERITREA_NORMAL:I = 0x20901fa

.field public static final STR_res_ESTONIA_NORMAL:I = 0x2090192

.field public static final STR_res_ETHIOPIA_NORMAL:I = 0x2090193

.field public static final STR_res_FAROEISLANDS_NORMAL:I = 0x20901fb

.field public static final STR_res_FEDERATEDSMICRONESIA_NORMAL:I = 0x20901fc

.field public static final STR_res_FIJI_NORMAL:I = 0x2090194

.field public static final STR_res_FINLAND_NORMAL:I = 0x20901fd

.field public static final STR_res_FRANCE_NORMAL:I = 0x20901fe

.field public static final STR_res_FRENCHGUIANA_NORMAL:I = 0x20901ff

.field public static final STR_res_FRENCHPOLYNESIA_NORMAL:I = 0x2090200

.field public static final STR_res_GABONESEREPUBLIC_NORMAL:I = 0x2090201

.field public static final STR_res_GAMBIA_NORMAL:I = 0x2090195

.field public static final STR_res_GENERALPROBLEMS_NORMAL:I = 0x2090316

.field public static final STR_res_GEORGIA_NORMAL:I = 0x2090202

.field public static final STR_res_GERMANY_NORMAL:I = 0x2090203

.field public static final STR_res_GHANA_NORMAL:I = 0x2090196

.field public static final STR_res_GIBRALTAR_NORMAL:I = 0x2090204

.field public static final STR_res_GREECE_NORMAL:I = 0x2090205

.field public static final STR_res_GREENLAND_NORMAL:I = 0x2090206

.field public static final STR_res_GRENADA_NORMAL:I = 0x2090207

.field public static final STR_res_GUADELOUPE_NORMAL:I = 0x2090208

.field public static final STR_res_GUAM_NORMAL:I = 0x2090197

.field public static final STR_res_GUATEMALA_NORMAL:I = 0x2090198

.field public static final STR_res_GUINEABISSAU_NORMAL:I = 0x209020a

.field public static final STR_res_GUINEA_NORMAL:I = 0x2090209

.field public static final STR_res_GUYANA_NORMAL:I = 0x209020b

.field public static final STR_res_HAITI_NORMAL:I = 0x2090199

.field public static final STR_res_HONDURAS_NORMAL:I = 0x209019a

.field public static final STR_res_HONGKONG_NORMAL:I = 0x209019b

.field public static final STR_res_HUNGARY_NORMAL:I = 0x209019c

.field public static final STR_res_ICELAND_NORMAL:I = 0x209019d

.field public static final STR_res_INDIA_NORMAL:I = 0x209019e

.field public static final STR_res_INDONESIA_NORMAL:I = 0x209019f

.field public static final STR_res_IRAN_NORMAL:I = 0x209020c

.field public static final STR_res_IRAQ_NORMAL:I = 0x20901a0

.field public static final STR_res_IRELAND_NORMAL:I = 0x20901a1

.field public static final STR_res_ISRAEL_NORMAL:I = 0x20901a2

.field public static final STR_res_ITALY_NORMAL:I = 0x209020d

.field public static final STR_res_JAMAICA_NORMAL:I = 0x20901a3

.field public static final STR_res_JAPAN_NORMAL:I = 0x20901a4

.field public static final STR_res_JORDAN_NORMAL:I = 0x20901a5

.field public static final STR_res_KAZAKHSTAN_NORMAL:I = 0x209020e

.field public static final STR_res_KENYA_NORMAL:I = 0x20901a6

.field public static final STR_res_KIRIBATI_NORMAL:I = 0x209020f

.field public static final STR_res_KOREANORTH_NORMAL:I = 0x20901a7

.field public static final STR_res_KOREASOUTH_NORMAL:I = 0x20901a8

.field public static final STR_res_KUWAIT_NORMAL:I = 0x20901a9

.field public static final STR_res_KYRGYZREPUBLIC_NORMAL:I = 0x2090210

.field public static final STR_res_LAOS_NORMAL:I = 0x20901aa

.field public static final STR_res_LATVIA_NORMAL:I = 0x20901ab

.field public static final STR_res_LEBANON_NORMAL:I = 0x2090211

.field public static final STR_res_LESOTHO_NORMAL:I = 0x2090212

.field public static final STR_res_LIBERIA_NORMAL:I = 0x2090213

.field public static final STR_res_LIBYA_NORMAL:I = 0x2090214

.field public static final STR_res_LIECHTENSTEIN_NORMAL:I = 0x2090215

.field public static final STR_res_LITHUANIA_NORMAL:I = 0x2090216

.field public static final STR_res_LUXEMBOURG_NORMAL:I = 0x2090217

.field public static final STR_res_MACAO_NORMAL:I = 0x2090218

.field public static final STR_res_MADAGASCAR_NORMAL:I = 0x20901ac

.field public static final STR_res_MALAWI_NORMAL:I = 0x2090219

.field public static final STR_res_MALAYSIA_NORMAL:I = 0x20901ad

.field public static final STR_res_MALDIVES_NORMAL:I = 0x209021a

.field public static final STR_res_MALI_NORMAL:I = 0x20901ae

.field public static final STR_res_MALTA_NORMAL:I = 0x209021b

.field public static final STR_res_MARITIME_NORMAL:I = 0x209021c

.field public static final STR_res_MARSHALL_NORMAL:I = 0x209021d

.field public static final STR_res_MARTINIQUE_NORMAL:I = 0x209021e

.field public static final STR_res_MAURITANIA_NORMAL:I = 0x20901af

.field public static final STR_res_MAURITIUS_NORMAL:I = 0x20901b0

.field public static final STR_res_MEXICO_NORMAL:I = 0x20901b1

.field public static final STR_res_MOLDOVA_NORMAL:I = 0x20901b2

.field public static final STR_res_MONACO_NORMAL:I = 0x209021f

.field public static final STR_res_MONGOLIA_NORMAL:I = 0x20901b3

.field public static final STR_res_MONTSERRAT_NORMAL:I = 0x2090220

.field public static final STR_res_MOROCCO_NORMAL:I = 0x20901b4

.field public static final STR_res_MOZAMBIQUE_NORMAL:I = 0x20901b5

.field public static final STR_res_MSGNOTSENT_NORMAL:I = 0x209031b

.field public static final STR_res_MSGTOOLONGFORNEWORK_NORMAL:I = 0x209031a

.field public static final STR_res_MYANMAR_NORMAL:I = 0x20901b6

.field public static final STR_res_NAMIBIA_NORMAL:I = 0x20901b7

.field public static final STR_res_NAURU_NORMAL:I = 0x2090221

.field public static final STR_res_NEPAL_NORMAL:I = 0x2090222

.field public static final STR_res_NETHERLANDASANTILLES_NORMAL:I = 0x20901b8

.field public static final STR_res_NETHERLANDS_NORMAL:I = 0x2090223

.field public static final STR_res_NETWORKPROBLEMS_NORMAL:I = 0x2090317

.field public static final STR_res_NEWCALEDONIA_NORMAL:I = 0x2090224

.field public static final STR_res_NEWCOUNTRYPHONE_NORMAL:I = 0x20901d9

.field public static final STR_res_NEWCOUNTRY_NORMAL:I = 0x2090253

.field public static final STR_res_NEWZEALAND_NORMAL:I = 0x20901b9

.field public static final STR_res_NICARAGUA_NORMAL:I = 0x20901ba

.field public static final STR_res_NIGERIA_NORMAL:I = 0x20901bc

.field public static final STR_res_NIGER_NORMAL:I = 0x20901bb

.field public static final STR_res_NORTHERNMARIANAISLD_NORMAL:I = 0x20901bd

.field public static final STR_res_NORWAY_NORMAL:I = 0x20901be

.field public static final STR_res_OMAN_NORMAL:I = 0x20901bf

.field public static final STR_res_PAKISTAN_NORMAL:I = 0x20901c0

.field public static final STR_res_PALAU_NORMAL:I = 0x2090225

.field public static final STR_res_PANAMA_NORMAL:I = 0x2090226

.field public static final STR_res_PAPUANEWGUINEA_NORMAL:I = 0x2090227

.field public static final STR_res_PARAGUAY_NORMAL:I = 0x2090228

.field public static final STR_res_PERU_NORMAL:I = 0x20901c1

.field public static final STR_res_PHILIPPINESS_NORMAL:I = 0x2090229

.field public static final STR_res_POLAND_NORMAL:I = 0x20901c2

.field public static final STR_res_PORTUGAL_NORMAL:I = 0x20901c3

.field public static final STR_res_PUERTORICO_NORMAL:I = 0x20901c4

.field public static final STR_res_QATAR_NORMAL:I = 0x209022a

.field public static final STR_res_RECONGO_NORMAL:I = 0x209022d

.field public static final STR_res_REMACEDONIA_NORMAL:I = 0x209022c

.field public static final STR_res_REUNION_NORMAL:I = 0x209022b

.field public static final STR_res_ROMANIA_NORMAL:I = 0x20901c5

.field public static final STR_res_RUSSIANFEDERATION_NORMAL:I = 0x20901c6

.field public static final STR_res_RWANDESEREPUBLIC_NORMAL:I = 0x20901c7

.field public static final STR_res_SAINTKITTS_NORMAL:I = 0x209022f

.field public static final STR_res_SAINTLUCIA_NORMAL:I = 0x2090230

.field public static final STR_res_SAINTPIERRE_NORMAL:I = 0x2090231

.field public static final STR_res_SAINTVINCENT_NORMAL:I = 0x2090232

.field public static final STR_res_SAMOA_NORMAL:I = 0x2090233

.field public static final STR_res_SANMARINO_NORMAL:I = 0x2090234

.field public static final STR_res_SAOTOME_NORMAL:I = 0x209022e

.field public static final STR_res_SAUDIARABIA_NORMAL:I = 0x20901c8

.field public static final STR_res_SENEGAL_NORMAL:I = 0x2090235

.field public static final STR_res_SERBIAMONTENEGRO_NORMAL:I = 0x2090236

.field public static final STR_res_SERVICENOTAVAILABLE_NORMAL:I = 0x2090318

.field public static final STR_res_SERVICENOTSUPPORTED_NORMAL:I = 0x2090319

.field public static final STR_res_SEYCHELLES_NORMAL:I = 0x2090237

.field public static final STR_res_SIERRALEONE_NORMAL:I = 0x2090238

.field public static final STR_res_SINGAPORE_NORMAL:I = 0x2090239

.field public static final STR_res_SLOVAKIA_NORMAL:I = 0x209023a

.field public static final STR_res_SLOVENIA_NORMAL:I = 0x209023b

.field public static final STR_res_SOLOMONISLANDS_NORMAL:I = 0x209023c

.field public static final STR_res_SOMALIA_NORMAL:I = 0x209023d

.field public static final STR_res_SOUTHAFRICA_NORMAL:I = 0x20901c9

.field public static final STR_res_SPAIN_NORMAL:I = 0x209023e

.field public static final STR_res_SRILANKA_NORMAL:I = 0x209023f

.field public static final STR_res_SUDAN_NORMAL:I = 0x2090240

.field public static final STR_res_SURINAME_NORMAL:I = 0x20901ca

.field public static final STR_res_SWAZILAND_NORMAL:I = 0x20901cb

.field public static final STR_res_SWEDEN_NORMAL:I = 0x20901cc

.field public static final STR_res_SWITZERLAND_NORMAL:I = 0x2090241

.field public static final STR_res_SYRIA_NORMAL:I = 0x2090242

.field public static final STR_res_TAIWAN_NORMAL:I = 0x2090243

.field public static final STR_res_TAJIKISTAN_NORMAL:I = 0x2090244

.field public static final STR_res_TANZANIA_NORMAL:I = 0x20901cd

.field public static final STR_res_THAILAND_NORMAL:I = 0x20901ce

.field public static final STR_res_TOGOLESSREPUBLIC_NORMAL:I = 0x20901cf

.field public static final STR_res_TONGA_NORMAL:I = 0x20901d0

.field public static final STR_res_TRINIDADANDTOBAGO_NORMAL:I = 0x20901d1

.field public static final STR_res_TUNISIA_NORMAL:I = 0x2090245

.field public static final STR_res_TURKEY_NORMAL:I = 0x2090246

.field public static final STR_res_TURKMENISTAN_NORMAL:I = 0x2090247

.field public static final STR_res_TURKSANDCAICOS_NORMAL:I = 0x2090248

.field public static final STR_res_UGANDA_NORMAL:I = 0x20901d2

.field public static final STR_res_UKRAINE_NORMAL:I = 0x20901d3

.field public static final STR_res_UNITEDARABEMIRATES2_NORMAL:I = 0x2090249

.field public static final STR_res_UNITEDARABEMIRATES_NORMAL:I = 0x209024b

.field public static final STR_res_UNITEDKINGDOM_NORMAL:I = 0x209024c

.field public static final STR_res_URUGUAY_NORMAL:I = 0x209024d

.field public static final STR_res_USA_NORMAL:I = 0x2090174

.field public static final STR_res_USVIRGINISLANDS_NORMAL:I = 0x20901d4

.field public static final STR_res_UZBERKISTAN_NORMAL:I = 0x20901d5

.field public static final STR_res_VANUATU_NORMAL:I = 0x209024e

.field public static final STR_res_VATICANCITYSTATE_NORMAL:I = 0x209024f

.field public static final STR_res_VENEZUELA_NORMAL:I = 0x20901d6

.field public static final STR_res_VIETNAM_NORMAL:I = 0x2090250

.field public static final STR_res_WALLISNADFUTUNA_NORMAL:I = 0x2090251

.field public static final STR_res_YEMEN_NORMAL:I = 0x20901d7

.field public static final STR_res_ZAMBIA_NORMAL:I = 0x20901d8

.field public static final STR_res_ZIMBABWE_NORMAL:I = 0x2090252

.field public static final Text_Link:I = 0x20902e1

.field public static final always_allow_popup:I = 0x2090287

.field public static final always_ask_on_cancel:I = 0x2090289

.field public static final always_not_allow_popup:I = 0x209028a

.field public static final always_not_allow_popup_3gonly:I = 0x209028b

.field public static final always_req_notice:I = 0x2090285

.field public static final ap_lgmdm_desc_control_apn_NORMAL:I = 0x209009b

.field public static final ap_lgmdm_lab_control_apn_NORMAL:I = 0x20900c9

.field public static final app_name:I = 0x20903ae

.field public static final authentication_reject:I = 0x2090139

.field public static final backgroundDataBlock_noti_text:I = 0x2090281

.field public static final backgroundDataBlock_noti_title:I = 0x2090280

.field public static final badPin2_LGEres:I = 0x2090133

.field public static final badPuk2:I = 0x2090132

.field public static final block_inmms:I = 0x2090291

.field public static final body_data_usage_alert:I = 0x2090305

.field public static final cancel:I = 0x2090255

.field public static final cancel_pin_screen:I = 0x2090126

.field public static final cliptray:I = 0x20902a0

.field public static final cliptray_copied_message:I = 0x20902a4

.field public static final cliptray_damaged_file_error:I = 0x20902a1

.field public static final cliptray_limit_reached_error:I = 0x20902a2

.field public static final cliptray_no_content:I = 0x20902a3

.field public static final config_vib_magnitude_index:I = 0x209000b

.field public static final config_wlan_chip_vendor:I = 0x2090006

.field public static final config_wlan_chip_version:I = 0x2090007

.field public static final config_wlan_concurrency:I = 0x2090008

.field public static final config_wlan_passpoint:I = 0x2090009

.field public static final config_wlan_wifidisplay:I = 0x209000a

.field public static final dataBlock_noti_error:I = 0x209029c

.field public static final dataBlock_noti_text:I = 0x2090292

.field public static final dataBlock_noti_ticker:I = 0x2090294

.field public static final dataBlock_noti_title:I = 0x2090293

.field public static final data_allowed_toast:I = 0x209029a

.field public static final data_blocked_dun:I = 0x2090297

.field public static final data_blocked_toast:I = 0x209029b

.field public static final data_call_is_now_rejected:I = 0x2090274

.field public static final data_disable_body:I = 0x209030c

.field public static final data_disable_title:I = 0x209030b

.field public static final data_disable_wifi_to_3g_transition_toast:I = 0x2090299

.field public static final data_enable_wifi_to_3g_transition_toast:I = 0x2090298

.field public static final data_setup_complete_toast:I = 0x2090296

.field public static final data_setup_toast:I = 0x2090295

.field public static final data_usage_disable_mobile_limit_KR:I = 0x209030f

.field public static final data_usage_disable_mobile_limit_LGU:I = 0x2090328

.field public static final data_usage_limit_body_kr:I = 0x209030d

.field public static final data_usage_warning_body_kr:I = 0x209030e

.field public static final day:I = 0x2090170

.field public static final dcm_lockscreen_perm_block_message:I = 0x209026f

.field public static final dcm_lockscreen_perm_block_title:I = 0x209026e

.field public static final dcm_lockscreen_sim_unlock_progress_dialog_message:I = 0x20902b1

.field public static final dcm_low_internal_storage_view_text:I = 0x2090327

.field public static final dcm_low_internal_storage_view_title:I = 0x2090326

.field public static final default_petname:I = 0x2090310

.field public static final dlg_cancel:I = 0x2090393

.field public static final dlg_ok:I = 0x2090254

.field public static final do_not_save:I = 0x2090300

.field public static final dum_popup_disconnect:I = 0x2090278

.field public static final dun_not_connected:I = 0x2090279

.field public static final dun_noti_text:I = 0x2090275

.field public static final dun_noti_ticker:I = 0x2090277

.field public static final dun_noti_title:I = 0x2090276

.field public static final emergency_calls_only_kt:I = 0x2090362

.field public static final emergency_calls_only_skt:I = 0x209034c

.field public static final encryption_policy_popup:I = 0x2090070

.field public static final encryption_policy_title:I = 0x2090071

.field public static final ext_media_nofs_blank_notification_message_sd:I = 0x2090325

.field public static final ext_media_nofs_blank_notification_message_usb:I = 0x2090324

.field public static final ext_media_nofs_blank_notification_title_sd:I = 0x2090323

.field public static final ext_media_nofs_blank_notification_title_usb:I = 0x2090322

.field public static final ext_media_nofs_unsupported_notification_message_sd:I = 0x2090321

.field public static final ext_media_nofs_unsupported_notification_message_usb:I = 0x2090320

.field public static final ext_media_nofs_unsupported_notification_title_sd:I = 0x209031f

.field public static final ext_media_nofs_unsupported_notification_title_usb:I = 0x209031e

.field public static final fro_ena_to_ask_popup:I = 0x2090286

.field public static final global_action_power_off:I = 0x209000d

.field public static final global_action_power_restart:I = 0x209000e

.field public static final gprs_connection_for_kt:I = 0x209027b

.field public static final hour:I = 0x2090171

.field public static final illegal_me:I = 0x209013c

.field public static final illegal_ms:I = 0x209013b

.field public static final imsi_unknown_hlr:I = 0x209013a

.field public static final invalidPin2:I = 0x209012b

.field public static final jp_lockscreen_uim_unlock_progress_dialog_message:I = 0x20902cb

.field public static final keyboard_connected:I = 0x20902e7

.field public static final keyguard_accessibility_icon_nexti:I = 0x2090270

.field public static final keyguard_accessibility_touch_unlock_nexti:I = 0x2090272

.field public static final keyguard_accessibility_unlock_area_nexti:I = 0x2090271

.field public static final keyguard_accessibility_unlock_nexti:I = 0x2090273

.field public static final keyguard_password_enter_pin_code:I = 0x20902ba

.field public static final keyguard_password_wrong_pin_code:I = 0x20902d7

.field public static final keyguard_password_wrong_puk_code:I = 0x20902ce

.field public static final keyguard_pin_accepted:I = 0x2090127

.field public static final kt_block_pukcode_message:I = 0x20902c9

.field public static final kt_managed_roaming_dialog_cancel_button:I = 0x2090365

.field public static final kt_managed_roaming_dialog_content:I = 0x2090363

.field public static final kt_managed_roaming_dialog_ok_button:I = 0x2090364

.field public static final kt_msg_password_wrong_puk_code:I = 0x20902cf

.field public static final kt_msg_pin_code_block:I = 0x20902bf

.field public static final kt_reject_cause_status_illegal_me:I = 0x209035c

.field public static final kt_reject_cause_status_illegal_ms:I = 0x209035b

.field public static final kt_reject_cause_status_imsi_unknown:I = 0x209035a

.field public static final kt_reject_cause_status_lu_fail:I = 0x209035d

.field public static final kt_reject_cause_status_lu_proceed:I = 0x2090361

.field public static final kt_reject_cause_status_not_opened_number:I = 0x209035f

.field public static final kt_reject_cause_status_nw_scanning:I = 0x2090360

.field public static final kt_reject_cause_status_nw_skt_scanned:I = 0x209035e

.field public static final kt_ussd_error_code_0x09:I = 0x2090352

.field public static final kt_ussd_error_code_0x1C:I = 0x2090353

.field public static final kt_ussd_error_code_0x22:I = 0x2090355

.field public static final kt_ussd_error_code_0x23:I = 0x2090356

.field public static final kt_ussd_error_code_0x24:I = 0x2090357

.field public static final kt_ussd_error_code_0x2B:I = 0x2090354

.field public static final kt_ussd_error_code_0x47:I = 0x2090358

.field public static final kt_ussd_error_code_0x48:I = 0x2090359

.field public static final label_ringtone:I = 0x20902f9

.field public static final lgt_msg_password_wrong_pin_code:I = 0x20902bc

.field public static final lgt_msg_password_wrong_puk_code:I = 0x20902c7

.field public static final lgt_msg_pin_code_block:I = 0x20902c0

.field public static final lgt_msg_puk_code_block:I = 0x20902ca

.field public static final lgu_hdr_network_error:I = 0x2090368

.field public static final lgu_lteemmreject:I = 0x209037c

.field public static final lgu_lteemmreject_19_8:I = 0x209037d

.field public static final lgu_reject_cause_02_imsi_unknown_in_hlr:I = 0x2090369

.field public static final lgu_reject_cause_03_illegal_ms:I = 0x209036a

.field public static final lgu_reject_cause_06_illegal_me:I = 0x209036b

.field public static final lgu_reject_cause_07_gprs_services_not_allowed:I = 0x209036c

.field public static final lgu_reject_cause_08_gprs_and_non_gprs_services_not_allowed:I = 0x209036d

.field public static final lgu_reject_cause_11_plmn_not_allowed:I = 0x209036e

.field public static final lgu_reject_cause_12_location_area_not_allowed:I = 0x209036f

.field public static final lgu_reject_cause_13_national_roaming_not_allowed:I = 0x2090370

.field public static final lgu_reject_cause_14_gprs_services_not_allowed_in_this_plmn:I = 0x2090371

.field public static final lgu_reject_cause_15_no_suitable_cells_in_location_area:I = 0x2090372

.field public static final lgu_reject_cause_16_MSC_temporarily_not_reachable:I = 0x2090373

.field public static final lgu_reject_cause_17_network_failure:I = 0x2090374

.field public static final lgu_reject_cause_22_congestion:I = 0x2090375

.field public static final lgu_reject_cause_etc:I = 0x2090376

.field public static final lgu_roaming_fail_notification_content:I = 0x2090378

.field public static final lgu_roaming_fail_notification_title:I = 0x2090377

.field public static final lgu_roaming_searching_notification_content:I = 0x209037b

.field public static final lgu_roaming_searching_notification_title:I = 0x209037a

.field public static final lgu_roaming_success_notification_title:I = 0x2090379

.field public static final lgu_unauthenticated:I = 0x2090367

.field public static final lgu_unregister:I = 0x2090366

.field public static final limited_service:I = 0x2090138

.field public static final lockscreen_failed_attempts_almost_at_lockout:I = 0x2090076

.field public static final lockscreen_failed_attempts_now_lockouting:I = 0x2090077

.field public static final lockscreen_network_locked_message:I = 0x209011b

.field public static final lockscreen_otp_failed_locked_message:I = 0x2090120

.field public static final lockscreen_perso_locked_message:I = 0x209011a

.field public static final lockscreen_service_provider_locked_message:I = 0x209011c

.field public static final lockscreen_sim_corporate_locked_message:I = 0x209011e

.field public static final lockscreen_sim_network_subset_locked_message:I = 0x209011d

.field public static final lockscreen_sim_pin_blocked:I = 0x2090117

.field public static final lockscreen_sim_sim_locked_message:I = 0x209011f

.field public static final lte_connection_for_kt:I = 0x209027c

.field public static final managed_roaming_dialog_cancel_button:I = 0x209002b

.field public static final managed_roaming_dialog_content:I = 0x2090029

.field public static final managed_roaming_dialog_ok_button:I = 0x209002a

.field public static final managed_roaming_title:I = 0x2090028

.field public static final media_checking_PrDnosdcard:I = 0x2090309

.field public static final media_scan_status_message:I = 0x209002c

.field public static final media_shared_PrDnosdcard:I = 0x209030a

.field public static final minute:I = 0x2090172

.field public static final mismatchPin2_LGEres:I = 0x209012a

.field public static final mouse_connected:I = 0x20902e8

.field public static final not_enough_space_on_memory:I = 0x20902e3

.field public static final notification_default_with_actual:I = 0x2090303

.field public static final password_expire_warn:I = 0x2090068

.field public static final password_expired:I = 0x2090066

.field public static final paypopup_cancelled_for_kt:I = 0x209027f

.field public static final paypopup_for_kt:I = 0x209027d

.field public static final paypopup_for_kt_roaming:I = 0x209027e

.field public static final perlab_group_smartcard:I = 0x2090269

.field public static final perlab_group_smartcard_jb_mr1:I = 0x209026b

.field public static final perlab_smartcard:I = 0x209026a

.field public static final perlab_smartcard_description_jb_mr1:I = 0x209026d

.field public static final perlab_smartcard_jb_mr1:I = 0x209026c

.field public static final power_dialog:I = 0x209025e

.field public static final power_off:I = 0x2090137

.field public static final pref_default_text_encoding:I = 0x2090302

.field public static final pref_default_text_encoding_dialogtitle:I = 0x2090301

.field public static final progress_erasing_PrDdefault:I = 0x20902ee

.field public static final progress_erasing_PrDnosdcard:I = 0x20902ef

.field public static final progress_unmounting_PrDdefault:I = 0x20902f0

.field public static final progress_unmounting_PrDnosdcard:I = 0x20902f1

.field public static final ps_alert_context:I = 0x2090284

.field public static final qslide_cannot_return_to_app_incomingcall:I = 0x209029e

.field public static final remove_device_block_NORMAL:I = 0x2090056

.field public static final resetPassword:I = 0x209006e

.field public static final reset_password_back:I = 0x209006d

.field public static final reset_password_description:I = 0x209006b

.field public static final reset_password_next:I = 0x209006c

.field public static final reset_password_title:I = 0x209006a

.field public static final restart_action_press_hold_power:I = 0x209000f

.field public static final restart_count_down_format:I = 0x2090012

.field public static final restart_count_format:I = 0x2090011

.field public static final restart_embedded_format:I = 0x2090013

.field public static final ringtone_title:I = 0x20902fb

.field public static final roamBlock_noti_text:I = 0x209028e

.field public static final roamBlock_noti_ticker:I = 0x2090290

.field public static final roamBlock_noti_title:I = 0x209028f

.field public static final roaming_alert_context_off:I = 0x2090283

.field public static final roaming_alert_context_on:I = 0x2090282

.field public static final roaming_not_allow_popup:I = 0x209028c

.field public static final roaming_not_allow_popup_in_menu:I = 0x209028d

.field public static final roaming_notallow_bt:I = 0x2090288

.field public static final scrap:I = 0x20902e2

.field public static final second:I = 0x2090173

.field public static final service_disabled:I = 0x209032a

.field public static final sim_added_auto_restart_message:I = 0x20902b2

.field public static final sim_removed_message:I = 0x20902b3

.field public static final sim_removed_message_confirm:I = 0x20902b4

.field public static final skt_fa_changed:I = 0x2090350

.field public static final skt_fa_changed_fail:I = 0x2090351

.field public static final skt_lockscreen_sim_unlock_progress_dialog_message:I = 0x20902b7

.field public static final skt_managed_roaming_dialog_cancel_button:I = 0x209034f

.field public static final skt_managed_roaming_dialog_content:I = 0x209034d

.field public static final skt_managed_roaming_dialog_ok_button:I = 0x209034e

.field public static final skt_msg_password_wrong_pin_code:I = 0x20902bb

.field public static final skt_msg_password_wrong_puk_code:I = 0x20902c6

.field public static final skt_msg_password_wrong_usim_perso:I = 0x20902d3

.field public static final skt_msg_pin_code_block:I = 0x20902be

.field public static final skt_msg_puk_code_block:I = 0x20902c8

.field public static final skt_msg_registered_new_pin_code:I = 0x20902c4

.field public static final skt_msg_verify_new_pin_error:I = 0x20902cd

.field public static final skt_password_guide_new_pin_code:I = 0x20902c3

.field public static final skt_password_guide_pin_code:I = 0x20902b9

.field public static final skt_password_guide_puk_code:I = 0x20902c2

.field public static final skt_password_guide_usim_perso:I = 0x20902d1

.field public static final skt_password_guide_verify_new_pin_code:I = 0x20902cc

.field public static final skt_reject_cause_02_imsi_unknown_in_hlr:I = 0x209032d

.field public static final skt_reject_cause_02_inter_imsi_unknown_in_hlr:I = 0x209032e

.field public static final skt_reject_cause_02_no_num_normal:I = 0x2090345

.field public static final skt_reject_cause_03_illegal_ms:I = 0x209032f

.field public static final skt_reject_cause_03_inter_illegal_ms:I = 0x2090330

.field public static final skt_reject_cause_03_no_num_normal:I = 0x2090346

.field public static final skt_reject_cause_06_illegal_me:I = 0x2090331

.field public static final skt_reject_cause_06_inter_illegal_me:I = 0x2090332

.field public static final skt_reject_cause_06_no_num_normal:I = 0x2090347

.field public static final skt_reject_cause_07_gprs_services_not_allowed:I = 0x2090333

.field public static final skt_reject_cause_08_gprs_and_non_gprs_services_not_allowed:I = 0x2090335

.field public static final skt_reject_cause_08_inter_gprs_and_non_gprs_services_not_allowed:I = 0x2090336

.field public static final skt_reject_cause_08_no_num_normal:I = 0x2090348

.field public static final skt_reject_cause_11_plmn_not_allowed:I = 0x2090337

.field public static final skt_reject_cause_12_location_area_not_allowed:I = 0x2090338

.field public static final skt_reject_cause_13_roaming_not_allowed_in_this_location_area:I = 0x2090339

.field public static final skt_reject_cause_14_gprs_services_not_allowed_in_this_plmn:I = 0x2090334

.field public static final skt_reject_cause_15_no_suitable_cells_in_location_area:I = 0x209033a

.field public static final skt_reject_cause_16_MSC_temporarily_not_reachable:I = 0x209033b

.field public static final skt_reject_cause_16_inter_MSC_temporarily_not_reachable:I = 0x209033c

.field public static final skt_reject_cause_16_no_num_normal:I = 0x2090349

.field public static final skt_reject_cause_17_inter_network_failure:I = 0x209033e

.field public static final skt_reject_cause_17_network_failure:I = 0x209033d

.field public static final skt_reject_cause_17_no_num_normal:I = 0x209034a

.field public static final skt_reject_cause_22_congestion:I = 0x209033f

.field public static final skt_reject_cause_22_inter_congestion:I = 0x2090340

.field public static final skt_reject_cause_22_no_num_normal:I = 0x209034b

.field public static final skt_roaming_fail_notificatiation_content:I = 0x2090343

.field public static final skt_roaming_fail_notificatiation_title:I = 0x2090342

.field public static final skt_roaming_network_reselection_guide:I = 0x2090341

.field public static final skt_roaming_success_notificatiation_title:I = 0x2090344

.field public static final skt_title_password_enter_pin_code:I = 0x20902b8

.field public static final skt_title_password_enter_puk_code:I = 0x20902c1

.field public static final skt_title_password_wrong_pin_code:I = 0x20902bd

.field public static final skt_title_password_wrong_puk_code:I = 0x20902c5

.field public static final skt_title_usim_persocheck:I = 0x20902d0

.field public static final skt_title_usim_persocheck_error:I = 0x20902d2

.field public static final sp_3times_simpin_fail_NORMAL:I = 0x2090123

.field public static final sp_Allow_web_noti_Dialog_msg_NORMAL:I = 0x20902de

.field public static final sp_Allow_web_noti_Dialog_msg_notitle_NORMAL:I = 0x20902df

.field public static final sp_Device_Options_NORMAL:I = 0x209000c

.field public static final sp_Encryption_SHORT:I = 0x2090074

.field public static final sp_PIN_NORMAL:I = 0x2090136

.field public static final sp_Web_notifications_NORMAL:I = 0x20902dd

.field public static final sp_activate_noti_NORMAL:I = 0x20903a7

.field public static final sp_addToDictionary_NORMAL:I = 0x2090129

.field public static final sp_airplane_mode_off_NORMAL:I = 0x209001b

.field public static final sp_airplane_mode_on_LGU_NORMAL:I = 0x2090261

.field public static final sp_airplane_mode_on_NORMAL:I = 0x209001a

.field public static final sp_airplane_mode_on_common_JB_NORMAL:I = 0x2090260

.field public static final sp_airplane_mode_on_message_NORMAL:I = 0x209025f

.field public static final sp_audio_earjack_vol_down_automatically_toast_NORMAL_X01:I = 0x20902a6

.field public static final sp_audio_safe_media_volume_warning:I = 0x20902a9

.field public static final sp_audio_vol_down_automatically_toast_NORMAL:I = 0x20902a7

.field public static final sp_audio_vol_tty_mode_warning_toast_NORMAL:I = 0x20902aa

.field public static final sp_audio_vol_up_warning_toast_NORMAL:I = 0x20902a8

.field public static final sp_auto_connect_vzw_ap_MLINE:I = 0x20903a6

.field public static final sp_backup_pin_NORMAL:I = 0x2090134

.field public static final sp_backup_pin_intruction_NORMAL:I = 0x2090135

.field public static final sp_block_bt_tethering_NORMAL:I = 0x2090052

.field public static final sp_block_consummail_NORMAL:I = 0x2090032

.field public static final sp_block_data_roaming_NORMAL:I = 0x2090064

.field public static final sp_block_enable_app_NORMAL:I = 0x2090038

.field public static final sp_block_enable_unsigned_app_NORMAL:I = 0x2090039

.field public static final sp_block_install_app_NORMAL:I = 0x2090036

.field public static final sp_block_microphone_NORMAL:I = 0x2090040

.field public static final sp_block_pop3_imap4_NORMAL:I = 0x209003a

.field public static final sp_block_sending_message_NORMAL:I = 0x2090031

.field public static final sp_block_uninstall_app_NORMAL:I = 0x2090037

.field public static final sp_block_unknownsource_NORMAL:I = 0x2090054

.field public static final sp_block_usb_debugging_NORMAL:I = 0x2090055

.field public static final sp_block_usb_mass_storage_NORMAL:I = 0x209005f

.field public static final sp_block_usb_thering_NORMAL:I = 0x2090050

.field public static final sp_block_wifi_hotspot_NORMAL:I = 0x2090051

.field public static final sp_cannot_copy_to_cliptray_NORMAL:I = 0x20902a5

.field public static final sp_charge_only_title_NORMAL:I = 0x209013f

.field public static final sp_clear_NORMAL:I = 0x20903ac

.field public static final sp_click_NORMAL:I = 0x20903ad

.field public static final sp_connection_ethernet_NORMAL:I = 0x209014a

.field public static final sp_connection_modem_NORMAL:I = 0x209014b

.field public static final sp_copied_to_clipboard_NORMAL:I = 0x2090157

.field public static final sp_data_pay_popup_title_VZW_NORMAL:I = 0x2090306

.field public static final sp_datalist_others_NORMAL:I = 0x209037e

.field public static final sp_dcm_pin_not_recognized_NORMAL:I = 0x20902af

.field public static final sp_dcm_puk_code_NORMAL:I = 0x20902ad

.field public static final sp_dcm_puk_code_block_NORMAL:I = 0x20902ac

.field public static final sp_dcm_puknot_recognized_NORMAL:I = 0x20902b0

.field public static final sp_dcm_wrong_pin_code_NORMAL:I = 0x20902ae

.field public static final sp_dialer_0_NORMAL:I = 0x209015d

.field public static final sp_dialer_0_sprint_NORMAL:I = 0x20902db

.field public static final sp_dialer_211_NORMAL:I = 0x209015e

.field public static final sp_dialer_2_sprint_NORMAL:I = 0x20902d9

.field public static final sp_dialer_311_NORMAL:I = 0x209015f

.field public static final sp_dialer_411_NORMAL:I = 0x2090160

.field public static final sp_dialer_4_sprint_NORMAL:I = 0x20902da

.field public static final sp_dialer_511_NORMAL:I = 0x2090161

.field public static final sp_dialer_611_NORMAL:I = 0x2090162

.field public static final sp_dialer_711_NORMAL:I = 0x2090163

.field public static final sp_dialer_811_NORMAL:I = 0x2090164

.field public static final sp_dlg_Available_networks_NORMAL:I = 0x209025d

.field public static final sp_dlg_detectsim_NORMAL:I = 0x2090259

.field public static final sp_dlg_global_NORMAL:I = 0x209025a

.field public static final sp_dlg_global_check_select:I = 0x209025b

.field public static final sp_dlg_insertsim_NORMAL:I = 0x2090258

.field public static final sp_dlg_networkmode_NORMAL:I = 0x209025c

.field public static final sp_dlg_nosim_NORMAL:I = 0x2090257

.field public static final sp_dlg_note_NORMAL:I = 0x2090256

.field public static final sp_do_not_show_this_again_NORMAL:I = 0x2090395

.field public static final sp_emergency_pop_body1_NORMAL:I = 0x2090265

.field public static final sp_emergency_pop_body2_NORMAL:I = 0x2090266

.field public static final sp_emergency_pop_body_NORMAL:I = 0x2090264

.field public static final sp_emergency_pop_cancel_NORMAL:I = 0x2090267

.field public static final sp_emergency_pop_ok_NORMAL:I = 0x2090268

.field public static final sp_emergency_pop_title_NORMAL:I = 0x2090263

.field public static final sp_encrypt_enabling_NORMAL:I = 0x2090075

.field public static final sp_expand_button_NORMAL:I = 0x20902ff

.field public static final sp_failed_attempt_alert_NORMAL:I = 0x209015c

.field public static final sp_half_failed_attempt_NORMAL:I = 0x209015a

.field public static final sp_home_mode_setting:I = 0x2090010

.field public static final sp_internet_connection_NORMAL:I = 0x209014c

.field public static final sp_invalid_sim_card_NORMAL:I = 0x2090128

.field public static final sp_keyguard_password_enter_pin_code_for_sub_NORMAL:I = 0x2090118

.field public static final sp_lg_software_NORMAL:I = 0x2090147

.field public static final sp_lg_software_title_NORMAL:I = 0x2090143

.field public static final sp_lgmdm_EAS_password_SL_NORMAL:I = 0x2090058

.field public static final sp_lgmdm_EAS_password_SU_NORMAL:I = 0x2090057

.field public static final sp_lgmdm_available_device_un_encryption_NORMAL:I = 0x20900ed

.field public static final sp_lgmdm_block_3gdata_NORMAL:I = 0x2090042

.field public static final sp_lgmdm_block_access_folder_NORMAL:I = 0x2090105

.field public static final sp_lgmdm_block_add_wifi_profile_NORMAL:I = 0x20900fe

.field public static final sp_lgmdm_block_airplane_NORMAL:I = 0x20900d9

.field public static final sp_lgmdm_block_all_devices_NORMAL:I = 0x209010a

.field public static final sp_lgmdm_block_androidbeam_NORMAL:I = 0x20900df

.field public static final sp_lgmdm_block_app_NORMAL:I = 0x2090044

.field public static final sp_lgmdm_block_automatic_restore_NORMAL:I = 0x20900fb

.field public static final sp_lgmdm_block_back_key_NORMAL:I = 0x20900f2

.field public static final sp_lgmdm_block_backup_my_data_NORMAL:I = 0x20900fa

.field public static final sp_lgmdm_block_bluetooth_NORMAL:I = 0x209005a

.field public static final sp_lgmdm_block_camera_NORMAL:I = 0x2090035

.field public static final sp_lgmdm_block_camera_ptp_NORMAL:I = 0x209005e

.field public static final sp_lgmdm_block_clipboard_NORMAL:I = 0x2090102

.field public static final sp_lgmdm_block_common_NORMAL:I = 0x2090107

.field public static final sp_lgmdm_block_data_NORMAL:I = 0x2090065

.field public static final sp_lgmdm_block_deleted_google_account_NORMAL:I = 0x2090103

.field public static final sp_lgmdm_block_erase_memory_NORMAL:I = 0x2090061

.field public static final sp_lgmdm_block_factoryreset_NORMAL:I = 0x2090046

.field public static final sp_lgmdm_block_forget_wifi_network_NORMAL:I = 0x2090100

.field public static final sp_lgmdm_block_gps_NORMAL:I = 0x209002e

.field public static final sp_lgmdm_block_home_key_NORMAL:I = 0x20900f0

.field public static final sp_lgmdm_block_lg_software_NORMAL:I = 0x209005d

.field public static final sp_lgmdm_block_loaction_NORMAL:I = 0x2090059

.field public static final sp_lgmdm_block_media_sync_NORMAL:I = 0x209005c

.field public static final sp_lgmdm_block_menu_key_NORMAL:I = 0x20900f1

.field public static final sp_lgmdm_block_message_NORMAL:I = 0x2090043

.field public static final sp_lgmdm_block_mobile_data_NORMAL:I = 0x20900d5

.field public static final sp_lgmdm_block_mock_location_NORMAL:I = 0x20900dd

.field public static final sp_lgmdm_block_modify_wifi_network_NORMAL:I = 0x2090101

.field public static final sp_lgmdm_block_nfc_NORMAL:I = 0x20900d6

.field public static final sp_lgmdm_block_nfc_cardmode:I = 0x20900d7

.field public static final sp_lgmdm_block_osp_NORMAL:I = 0x2090041

.field public static final sp_lgmdm_block_password_typing_visible_NORMAL:I = 0x20900f7

.field public static final sp_lgmdm_block_playstore_NORMAL:I = 0x20900e2

.field public static final sp_lgmdm_block_readwrite_p2p_NORMAL:I = 0x20900de

.field public static final sp_lgmdm_block_readwrite_p2p_nfc_NORMAL:I = 0x20900d8

.field public static final sp_lgmdm_block_recent_key_NORMAL:I = 0x20900f3

.field public static final sp_lgmdm_block_remove_account_NORMAL:I = 0x20900ef

.field public static final sp_lgmdm_block_restrict_background_data_NORMAL:I = 0x20900f9

.field public static final sp_lgmdm_block_sdcard_NORMAL:I = 0x2090053

.field public static final sp_lgmdm_block_setting_NORMAL:I = 0x20900f5

.field public static final sp_lgmdm_block_setting_wifi_profile_NORMAL:I = 0x20900ff

.field public static final sp_lgmdm_block_share_folder_NORMAL:I = 0x2090104

.field public static final sp_lgmdm_block_some_bluetooth_NORMAL:I = 0x2090108

.field public static final sp_lgmdm_block_some_devices_NORMAL:I = 0x2090109

.field public static final sp_lgmdm_block_statusbar_NORMAL:I = 0x20900fc

.field public static final sp_lgmdm_block_storage_encryption_NORMAL:I = 0x2090073

.field public static final sp_lgmdm_block_task_manager_NORMAL:I = 0x20900f6

.field public static final sp_lgmdm_block_usb_storage_NORMAL:I = 0x209010c

.field public static final sp_lgmdm_block_voice_dialer_NORMAL:I = 0x20900e4

.field public static final sp_lgmdm_block_voice_record_NORMAL:I = 0x209004a

.field public static final sp_lgmdm_block_vpn_NORMAL:I = 0x209003f

.field public static final sp_lgmdm_block_vpn_connection_NORMAL:I = 0x209010b

.field public static final sp_lgmdm_block_wifi_direct_NORMAL:I = 0x209004e

.field public static final sp_lgmdm_block_wifi_scan_NORMAL:I = 0x209010e

.field public static final sp_lgmdm_block_wifi_screen_share:I = 0x209004f

.field public static final sp_lgmdm_block_youtube_NORMAL:I = 0x20900e3

.field public static final sp_lgmdm_blockbrowser_NORMAL:I = 0x209002f

.field public static final sp_lgmdm_blockscreencapture_NORMAL:I = 0x209002d

.field public static final sp_lgmdm_blocksms_NORMAL:I = 0x2090030

.field public static final sp_lgmdm_blockwifi_NORMAL:I = 0x209004d

.field public static final sp_lgmdm_bluetooth_pairing_SHORT:I = 0x20900f4

.field public static final sp_lgmdm_cannot_gps_NORMAL:I = 0x20900e1

.field public static final sp_lgmdm_cannot_restrict_background_data_NORMAL:I = 0x20900f8

.field public static final sp_lgmdm_clear_appcache_NORMAL:I = 0x209003d

.field public static final sp_lgmdm_clear_appdata_NORMAL:I = 0x209003c

.field public static final sp_lgmdm_clear_appdefault_NORMAL:I = 0x209003e

.field public static final sp_lgmdm_control_autosync_NORMAL:I = 0x2090034

.field public static final sp_lgmdm_desc_VPN_config_NORMAL:I = 0x2090082

.field public static final sp_lgmdm_desc_WiFi_config_NORMAL:I = 0x2090083

.field public static final sp_lgmdm_desc_certification_NORMAL:I = 0x2090081

.field public static final sp_lgmdm_desc_dis_WiFi_NORMAL:I = 0x209008e

.field public static final sp_lgmdm_desc_dis_airplane_NORMAL:I = 0x2090096

.field public static final sp_lgmdm_desc_dis_app_NORMAL:I = 0x209007b

.field public static final sp_lgmdm_desc_dis_auto_sync_data_NORMAL:I = 0x209009d

.field public static final sp_lgmdm_desc_dis_automatic_restore_NORMAL:I = 0x20900a4

.field public static final sp_lgmdm_desc_dis_backup_my_data_NORMAL:I = 0x20900a3

.field public static final sp_lgmdm_desc_dis_backup_restoration_NORMAL:I = 0x20900a0

.field public static final sp_lgmdm_desc_dis_backup_restore_NORMAL:I = 0x2090097

.field public static final sp_lgmdm_desc_dis_bluetooth_NORMAL:I = 0x209008b

.field public static final sp_lgmdm_desc_dis_browser_NORMAL:I = 0x209007f

.field public static final sp_lgmdm_desc_dis_clipboard_NORMAL:I = 0x209009f

.field public static final sp_lgmdm_desc_dis_consumer_email_NORMAL:I = 0x2090084

.field public static final sp_lgmdm_desc_dis_data_roaming_NORMAL:I = 0x2090088

.field public static final sp_lgmdm_desc_dis_del_account_NORMAL:I = 0x209009c

.field public static final sp_lgmdm_desc_dis_external_memory_NORMAL:I = 0x209008d

.field public static final sp_lgmdm_desc_dis_gps_NORMAL:I = 0x2090090

.field public static final sp_lgmdm_desc_dis_install_app_NORMAL:I = 0x209007c

.field public static final sp_lgmdm_desc_dis_message_NORMAL:I = 0x2090080

.field public static final sp_lgmdm_desc_dis_microphone_NORMAL:I = 0x2090094

.field public static final sp_lgmdm_desc_dis_mobile_data_NORMAL:I = 0x209008a

.field public static final sp_lgmdm_desc_dis_network_location_NORMAL:I = 0x2090093

.field public static final sp_lgmdm_desc_dis_nfc_NORMAL:I = 0x2090095

.field public static final sp_lgmdm_desc_dis_outgoing_call_NORMAL:I = 0x209009e

.field public static final sp_lgmdm_desc_dis_password_typing_visible_NORMAL:I = 0x20900a2

.field public static final sp_lgmdm_desc_dis_popimap_email_NORMAL:I = 0x2090085

.field public static final sp_lgmdm_desc_dis_screen_capture_NORMAL:I = 0x2090091

.field public static final sp_lgmdm_desc_dis_statusbar_NORMAL:I = 0x20900a1

.field public static final sp_lgmdm_desc_dis_tethering_NORMAL:I = 0x209008c

.field public static final sp_lgmdm_desc_dis_uninstall_app_NORMAL:I = 0x209007d

.field public static final sp_lgmdm_desc_dis_usb_NORMAL:I = 0x209008f

.field public static final sp_lgmdm_desc_enforce_lockout_NORMAL:I = 0x2090089

.field public static final sp_lgmdm_desc_force_app_control_NORMAL:I = 0x2090098

.field public static final sp_lgmdm_desc_get_data_info_NORMAL:I = 0x209009a

.field public static final sp_lgmdm_desc_install_app_NORMAL:I = 0x2090078

.field public static final sp_lgmdm_desc_manual_sync_roaming_NORMAL:I = 0x2090086

.field public static final sp_lgmdm_desc_recovery_password_NORMAL:I = 0x2090092

.field public static final sp_lgmdm_desc_remove_admin_NORMAL:I = 0x209007a

.field public static final sp_lgmdm_desc_set_email_NORMAL:I = 0x2090087

.field public static final sp_lgmdm_desc_turn_on_device_NORMAL:I = 0x2090099

.field public static final sp_lgmdm_desc_turnoff_mobile_NORMAL:I = 0x20900a5

.field public static final sp_lgmdm_desc_uninstall_app_NORMAL:I = 0x2090079

.field public static final sp_lgmdm_desc_wipe_appdata_NORMAL:I = 0x209007e

.field public static final sp_lgmdm_device_encryption_done:I = 0x2090062

.field public static final sp_lgmdm_dis_erase_usb_storage_NORMAL:I = 0x209010d

.field public static final sp_lgmdm_enable_gps_NORMAL:I = 0x20900e0

.field public static final sp_lgmdm_eraging_sdcard_NORMAL:I = 0x2090106

.field public static final sp_lgmdm_erase_sdcard_NORMAL:I = 0x2090060

.field public static final sp_lgmdm_factory_reset_NORMAL:I = 0x2090033

.field public static final sp_lgmdm_force_data_NORMAL:I = 0x20900d4

.field public static final sp_lgmdm_google_map_resetart_NORMAL:I = 0x20900ee

.field public static final sp_lgmdm_keep_data_connection_NORMAL:I = 0x20900da

.field public static final sp_lgmdm_lab_VPN_config_NORMAL:I = 0x20900b0

.field public static final sp_lgmdm_lab_WiFi_config_NORMAL:I = 0x20900b1

.field public static final sp_lgmdm_lab_certification_NORMAL:I = 0x20900af

.field public static final sp_lgmdm_lab_dis_WiFi_NORMAL:I = 0x20900bc

.field public static final sp_lgmdm_lab_dis_airplane_NORMAL:I = 0x20900c4

.field public static final sp_lgmdm_lab_dis_app_NORMAL:I = 0x20900a9

.field public static final sp_lgmdm_lab_dis_auto_sync_data_NORMAL:I = 0x20900cb

.field public static final sp_lgmdm_lab_dis_automatic_restore_NORMAL:I = 0x20900d2

.field public static final sp_lgmdm_lab_dis_backup_my_data_NORMAL:I = 0x20900d1

.field public static final sp_lgmdm_lab_dis_backup_restoration_NORMAL:I = 0x20900ce

.field public static final sp_lgmdm_lab_dis_backup_restore_NORMAL:I = 0x20900c5

.field public static final sp_lgmdm_lab_dis_bluetooth_NORMAL:I = 0x20900b9

.field public static final sp_lgmdm_lab_dis_browser_NORMAL:I = 0x20900ad

.field public static final sp_lgmdm_lab_dis_clipboard_NORMAL:I = 0x20900cd

.field public static final sp_lgmdm_lab_dis_consumer_email_NORMAL:I = 0x20900b2

.field public static final sp_lgmdm_lab_dis_data_roaming_NORMAL:I = 0x20900b6

.field public static final sp_lgmdm_lab_dis_del_account_NORMAL:I = 0x20900ca

.field public static final sp_lgmdm_lab_dis_external_memory_NORMAL:I = 0x20900bb

.field public static final sp_lgmdm_lab_dis_gps_NORMAL:I = 0x20900be

.field public static final sp_lgmdm_lab_dis_install_app_NORMAL:I = 0x20900aa

.field public static final sp_lgmdm_lab_dis_message_NORMAL:I = 0x20900ae

.field public static final sp_lgmdm_lab_dis_microphone_NORMAL:I = 0x20900c2

.field public static final sp_lgmdm_lab_dis_mobile_data_NORMAL:I = 0x20900b8

.field public static final sp_lgmdm_lab_dis_network_location_NORMAL:I = 0x20900c1

.field public static final sp_lgmdm_lab_dis_nfc_NORMAL:I = 0x20900c3

.field public static final sp_lgmdm_lab_dis_outgoing_call_NORMAL:I = 0x20900cc

.field public static final sp_lgmdm_lab_dis_password_typing_visible_NORMAL:I = 0x20900d0

.field public static final sp_lgmdm_lab_dis_popimap_email_NORMAL:I = 0x20900b3

.field public static final sp_lgmdm_lab_dis_screen_capture_NORMAL:I = 0x20900bf

.field public static final sp_lgmdm_lab_dis_statusbar_NORMAL:I = 0x20900cf

.field public static final sp_lgmdm_lab_dis_tethering_NORMAL:I = 0x20900ba

.field public static final sp_lgmdm_lab_dis_uninstall_app_NORMAL:I = 0x20900ab

.field public static final sp_lgmdm_lab_dis_usb_NORMAL:I = 0x20900bd

.field public static final sp_lgmdm_lab_enforce_lockout_NORMAL:I = 0x20900b7

.field public static final sp_lgmdm_lab_force_app_control_NORMAL:I = 0x20900c6

.field public static final sp_lgmdm_lab_get_data_info_NORMAL:I = 0x20900c8

.field public static final sp_lgmdm_lab_install_app_NORMAL:I = 0x20900a6

.field public static final sp_lgmdm_lab_manual_sync_roaming_NORMAL:I = 0x20900b4

.field public static final sp_lgmdm_lab_recovery_password_NORMAL:I = 0x20900c0

.field public static final sp_lgmdm_lab_remove_admin_NORMAL:I = 0x20900a8

.field public static final sp_lgmdm_lab_set_email_NORMAL:I = 0x20900b5

.field public static final sp_lgmdm_lab_turn_on_device_NORMAL:I = 0x20900c7

.field public static final sp_lgmdm_lab_turnoff_mobile_NORMAL:I = 0x20900d3

.field public static final sp_lgmdm_lab_uninstall_app_NORMAL:I = 0x20900a7

.field public static final sp_lgmdm_lab_wipe_appdata_NORMAL:I = 0x20900ac

.field public static final sp_lgmdm_location_whitelist_NORMAL:I = 0x20900e9

.field public static final sp_lgmdm_lockout_NORMAL:I = 0x2090048

.field public static final sp_lgmdm_lockout_dialog_confirm_SHORT:I = 0x2090049

.field public static final sp_lgmdm_lockout_dialog_title_NORMAL:I = 0x2090047

.field public static final sp_lgmdm_make_visible_NORMAL:I = 0x2090067

.field public static final sp_lgmdm_need_update_software_NORMAL:I = 0x20900ea

.field public static final sp_lgmdm_only_audio_bluetooth_NORMAL:I = 0x209005b

.field public static final sp_lgmdm_proceed_encrytion_NORMAL:I = 0x2090072

.field public static final sp_lgmdm_recovery_password_NORMAL:I = 0x2090045

.field public static final sp_lgmdm_start_application_NORMAL:I = 0x20900db

.field public static final sp_lgmdm_stop_application_NORMAL:I = 0x20900dc

.field public static final sp_lgmdm_storage_encryption_done:I = 0x2090063

.field public static final sp_lgmdm_turn_off_data_network_NORMAL:I = 0x20900e8

.field public static final sp_lgmdm_turn_off_gps_NORMAL:I = 0x20900e6

.field public static final sp_lgmdm_turn_on_data_network_NORMAL:I = 0x20900e7

.field public static final sp_lgmdm_turn_on_gps_NORMAL:I = 0x20900e5

.field public static final sp_lgmdm_update_software_NORMAL:I = 0x20900eb

.field public static final sp_lgmdm_wipe_sdcard_NORMAL:I = 0x209003b

.field public static final sp_lgmdm_wireless_storage_disable_NORMAL:I = 0x20900fd

.field public static final sp_lock_pw_repeated_NORMAL:I = 0x209004b

.field public static final sp_lock_pw_sequential_NORMAL:I = 0x209004c

.field public static final sp_lockscreen_emergencycall_press_and_hold_MLINE:I = 0x209013d

.field public static final sp_managed_time_setting_dialog_content:I = 0x20902dc

.field public static final sp_mdm_recovery_password_not_match:I = 0x2090069

.field public static final sp_media_sync_title_NORMAL:I = 0x2090141

.field public static final sp_miracast_guide_intro_desc_NORMAL:I = 0x2090394

.field public static final sp_miracast_guide_tips_batt_desc_NORMAL:I = 0x209039c

.field public static final sp_miracast_guide_title_NORMAL:I = 0x2090392

.field public static final sp_miracast_hdcp_warning_and_definition_NORMAL:I = 0x209039b

.field public static final sp_miracast_noti_enabled_NORMAL:I = 0x2090398

.field public static final sp_miracast_noti_low_batt_NORMAL:I = 0x209039d

.field public static final sp_miracast_noti_turned_off_NORMAL:I = 0x2090399

.field public static final sp_miracast_title_NORMAL:I = 0x2090391

.field public static final sp_miracast_warn_for_block_process_NORMAL:I = 0x209039a

.field public static final sp_miracast_warn_for_using_ext_disp_NORMAL:I = 0x209039e

.field public static final sp_miracast_warn_for_using_mhl_NORMAL:I = 0x2090397

.field public static final sp_movial_wifi_calling_status_airplane_on_NORMAL:I = 0x209016e

.field public static final sp_movial_wifi_calling_status_cell_pref_NORMAL:I = 0x209016b

.field public static final sp_movial_wifi_calling_status_disabled_NORMAL:I = 0x2090167

.field public static final sp_movial_wifi_calling_status_disabling_NORMAL:I = 0x209016f

.field public static final sp_movial_wifi_calling_status_enabling_NORMAL:I = 0x2090168

.field public static final sp_movial_wifi_calling_status_error_code_NORMAL:I = 0x2090169

.field public static final sp_movial_wifi_calling_status_ims_reg_NORMAL:I = 0x209016c

.field public static final sp_movial_wifi_calling_status_off_NORMAL:I = 0x209016d

.field public static final sp_movial_wifi_calling_status_poor_signal_NORMAL:I = 0x209016a

.field public static final sp_ms_originated_sms_denied_SHORT:I = 0x209031d

.field public static final sp_no_NORMAL:I = 0x20902b6

.field public static final sp_not_available_during_a_call_NORMAL:I = 0x2090166

.field public static final sp_ongoing_noti_ap_NORMAL:I = 0x20903b4

.field public static final sp_ongoing_noti_ap_connected_NORMAL:I = 0x20903b5

.field public static final sp_ongoing_noti_no_ap_NORMAL:I = 0x20903b3

.field public static final sp_ongoing_noti_summary_NORMAL:I = 0x20903b6

.field public static final sp_ongoing_noti_wifi_off_NORMAL:I = 0x20903b2

.field public static final sp_pc_software_NORMAL:I = 0x2090148

.field public static final sp_pc_software_title_NORMAL:I = 0x2090142

.field public static final sp_phone_power_off_confirm_NORMAL:I = 0x209001c

.field public static final sp_phone_restart_confirm_NORMAL:I = 0x209001f

.field public static final sp_pin2_is_blocked_NORMAL:I = 0x209012d

.field public static final sp_power_off_confirm_NORMAL:I = 0x209001d

.field public static final sp_recovery_password_NORMAL:I = 0x209006f

.field public static final sp_restart_confirm_NORMAL:I = 0x209001e

.field public static final sp_restart_confirm_new_NORMAL:I = 0x209013e

.field public static final sp_restart_confirm_question_NORMAL:I = 0x2090020

.field public static final sp_retry_title_NORMAL:I = 0x2090311

.field public static final sp_screenshot_failed_enough_space_internal_storage_NORMAL:I = 0x2090262

.field public static final sp_security_check_NORMAL:I = 0x2090159

.field public static final sp_sim_incorrect_puk_NORMAL:I = 0x2090124

.field public static final sp_sim_permlocked_alert_NORMAL:I = 0x2090121

.field public static final sp_sim_puk_alert_NORMAL:I = 0x2090122

.field public static final sp_sim_remaining_count_NORMAL:I = 0x2090125

.field public static final sp_sim_removed_message_dcm_MLINE:I = 0x20902ab

.field public static final sp_sms_vzw_retry_msg_NORMAL:I = 0x2090312

.field public static final sp_sms_vzw_retry_no_NORMAL:I = 0x2090314

.field public static final sp_sms_vzw_retry_yes_NORMAL:I = 0x2090313

.field public static final sp_sound_dialog_volume:I = 0x20902fa

.field public static final sp_sound_noti_NORMAL:I = 0x20902fc

.field public static final sp_tethered_hotspot_notification_title_NORMAL:I = 0x20903af

.field public static final sp_tethered_notification_att_title_NORMAL:I = 0x209037f

.field public static final sp_tethered_notification_title_usb_only_NORMAL:I = 0x209014f

.field public static final sp_tethered_notification_title_wifi_only_NORMAL:I = 0x2090150

.field public static final sp_tethered_notification_title_wifi_usb_NORMAL:I = 0x2090151

.field public static final sp_tethered_usb_hotspot_notification_title_NORMAL:I = 0x20903b0

.field public static final sp_tethered_usb_notification_title_NORMAL:I = 0x20903b1

.field public static final sp_title_sw_update_NORMAL:I = 0x20900ec

.field public static final sp_touchto_config_NORMAL:I = 0x20903ab

.field public static final sp_turn_off_airplane_mode_NORMAL:I = 0x2090019

.field public static final sp_turn_off_flight_mode_NORMAL:I = 0x2090017

.field public static final sp_turn_off_silent_mode_NORMAL:I = 0x2090015

.field public static final sp_turn_on_airplane_mode_NORMAL:I = 0x2090018

.field public static final sp_turn_on_flight_mode_NORMAL:I = 0x2090016

.field public static final sp_turn_on_silent_mode_NORMAL:I = 0x2090014

.field public static final sp_type_keyword_NORMAL:I = 0x2090158

.field public static final sp_unknown_destination_address_SHORT:I = 0x209031c

.field public static final sp_usb_charger_title_NORMAL:I = 0x2090144

.field public static final sp_usb_tethering_NORMAL:I = 0x2090146

.field public static final sp_usb_tethering_title_NORMAL:I = 0x2090140

.field public static final sp_usbtype_mtp_title_NORMAL:I = 0x2090145

.field public static final sp_volume_media_description_NORMAL:I = 0x20902fe

.field public static final sp_volume_popup_feedback_system_NORMAL:I = 0x20902fd

.field public static final sp_vzw_network_avail_NORMAL:I = 0x20903aa

.field public static final sp_warning_not_available_during_call_NORMAL:I = 0x2090165

.field public static final sp_wifi_audio_noti_NORMAL:I = 0x20903a8

.field public static final sp_wifi_hotspot_maxClientReached_message_NORMAL:I = 0x2090381

.field public static final sp_wifi_hotspot_maxClientReached_title_NORMAL:I = 0x2090380

.field public static final sp_wifi_ip_settings_duplicated_ssid_NORMAL:I = 0x209038b

.field public static final sp_wifi_ip_settings_invalid_password_NORMAL:I = 0x209038c

.field public static final sp_wifi_lge_eap_auth_timeout:I = 0x209038f

.field public static final sp_wifi_lge_eap_failure:I = 0x209038e

.field public static final sp_wifi_lge_eap_failure_with_error_code:I = 0x2090390

.field public static final sp_wifi_lge_eap_success:I = 0x209038d

.field public static final sp_wifi_no_response_from_internet:I = 0x20903a3

.field public static final sp_wifi_no_response_from_internet_and_disconnet:I = 0x20903a4

.field public static final sp_wifi_notify_connect_skt_NORMAL:I = 0x2090386

.field public static final sp_wifi_notify_disconnected_NORMAL:I = 0x2090387

.field public static final sp_wifi_notify_open_AP_connect_NORMAL:I = 0x2090388

.field public static final sp_wifi_notify_prohibit_connect_ap_NORMAL:I = 0x209038a

.field public static final sp_wifi_notify_wep_ap_connect_NORMAL:I = 0x2090389

.field public static final sp_wifi_scr_shr_warn_for_wifi_direct_NORMAL:I = 0x2090396

.field public static final sp_wifi_vib_noti_NORMAL:I = 0x20903a9

.field public static final sp_wrong_password_NORMAL:I = 0x209015b

.field public static final sp_yes_NORMAL:I = 0x20902b5

.field public static final spcExcessFail:I = 0x2090308

.field public static final spcExcessFailTitle:I = 0x2090307

.field public static final stkCcSsToDial:I = 0x2090025

.field public static final stkCcSsToSs:I = 0x2090027

.field public static final stkCcSsToUssd:I = 0x2090026

.field public static final stkCcUssdToDial:I = 0x2090022

.field public static final stkCcUssdToSs:I = 0x2090023

.field public static final stkCcUssdToUssd:I = 0x2090024

.field public static final storage_usb_storage:I = 0x20902f2

.field public static final storage_usb_storage2:I = 0x20902f3

.field public static final storage_usb_storage3:I = 0x20902f4

.field public static final storage_usb_storage4:I = 0x20902f5

.field public static final storage_usb_storage5:I = 0x20902f6

.field public static final storage_usb_storage6:I = 0x20902f7

.field public static final tab_to_remove_usb_storage:I = 0x20902e6

.field public static final telephony_dialog_cancel_button:I = 0x209032c

.field public static final telephony_dialog_ok_button:I = 0x209032b

.field public static final tethered_notification_message:I = 0x209029f

.field public static final tethered_notification_summary_tmus:I = 0x2090385

.field public static final tethered_notification_title_2:I = 0x2090383

.field public static final tethered_notification_title_msm8960:I = 0x2090382

.field public static final tethered_notification_title_tmus:I = 0x2090384

.field public static final text_translate:I = 0x20902e0

.field public static final title_data_usage_alert:I = 0x2090304

.field public static final title_data_usage_alert_LGU:I = 0x2090329

.field public static final tmus_msg_pin_code_block:I = 0x20902d8

.field public static final unmount_usb_storage_before_removing_it:I = 0x20902eb

.field public static final unsupportedMmiCode:I = 0x2090021

.field public static final unsupported_device:I = 0x20902ed

.field public static final unsupported_device_msg:I = 0x20902ec

.field public static final usb_device_connected:I = 0x20902e9

.field public static final usb_mtp_notification_title:I = 0x209014d

.field public static final usb_ptp_notification_title:I = 0x209014e

.field public static final usb_ptp_title:I = 0x2090149

.field public static final usb_storage_can_be_removed_safely_now:I = 0x20902f8

.field public static final usb_storage_connected:I = 0x20902e5

.field public static final usb_storage_unexpectedly_removed:I = 0x20902ea

.field public static final wcdma_connection_for_kt:I = 0x209027a

.field public static final widget_default_class_name_nexti:I = 0x2090001

.field public static final widget_default_package_name_nexti:I = 0x2090000

.field public static final widget_lg_music_class_name:I = 0x2090003

.field public static final widget_lg_music_package_name:I = 0x2090002

.field public static final widget_qremote_class_name:I = 0x2090005

.field public static final widget_qremote_package_name:I = 0x2090004

.field public static final wifi_p2p_connected:I = 0x20903a1

.field public static final wifi_p2p_connection_failed_channel_limitation:I = 0x209039f

.field public static final wifi_p2p_groupcreate_failed_channel_limitation:I = 0x20903a0

.field public static final wifi_p2p_prevent_connect_when_BT_WiFi_connected:I = 0x20903a2

.field public static final wifi_settings:I = 0x20903a5

.field public static final xtra_notif_message:I = 0x20902e4


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 2741
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
