.class public final Lcom/lge/internal/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/internal/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final TextView01:I = 0x20d0075

.field public static final accountPreferences:I = 0x20d0031

.field public static final action_menu_divider:I = 0x20d0038

.field public static final action_menu_presenter:I = 0x20d003b

.field public static final addToDictionary:I = 0x20d0030

.field public static final background:I = 0x20d0000

.field public static final btnOk:I = 0x20d0052

.field public static final button1:I = 0x20d001e

.field public static final button2:I = 0x20d001f

.field public static final button3:I = 0x20d0020

.field public static final button_blinkfield:I = 0x20d006f

.field public static final button_tap:I = 0x20d006e

.field public static final candidatesArea:I = 0x20d0022

.field public static final chara:I = 0x20d005c

.field public static final check:I = 0x20d003f

.field public static final checkbox:I = 0x20d0001

.field public static final clip_cell:I = 0x20d0047

.field public static final clip_cell_land:I = 0x20d004d

.field public static final clip_cell_port:I = 0x20d004b

.field public static final clip_delete:I = 0x20d004a

.field public static final clip_img:I = 0x20d0048

.field public static final clip_img_dim:I = 0x20d004c

.field public static final clip_text:I = 0x20d0049

.field public static final closeButton:I = 0x20d002d

.field public static final content:I = 0x20d0002

.field public static final copy:I = 0x20d0027

.field public static final copyUrl:I = 0x20d0029

.field public static final custom:I = 0x20d0033

.field public static final cut:I = 0x20d0026

.field public static final description:I = 0x20d004f

.field public static final doc_bottom:I = 0x20d0061

.field public static final doc_frame2:I = 0x20d0062

.field public static final doc_frame3:I = 0x20d0064

.field public static final doc_imageView_camera:I = 0x20d005e

.field public static final doc_imageView_mascot_voice:I = 0x20d0060

.field public static final edit:I = 0x20d001c

.field public static final emergencyText:I = 0x20d0055

.field public static final empty:I = 0x20d0003

.field public static final enter_confirm_string:I = 0x20d0051

.field public static final enter_password:I = 0x20d006a

.field public static final extractArea:I = 0x20d0021

.field public static final fillInIntent:I = 0x20d0035

.field public static final gridlayout:I = 0x20d0042

.field public static final half_wrong_password_dialog_root:I = 0x20d004e

.field public static final hint:I = 0x20d0004

.field public static final history_atticon2:I = 0x20d0063

.field public static final history_atticon3:I = 0x20d0065

.field public static final home:I = 0x20d0034

.field public static final icon:I = 0x20d0005

.field public static final icon1:I = 0x20d0006

.field public static final icon2:I = 0x20d0007

.field public static final icon_menu_presenter:I = 0x20d0039

.field public static final image:I = 0x20d0050

.field public static final input:I = 0x20d0008

.field public static final inputArea:I = 0x20d0023

.field public static final inputExtractEditText:I = 0x20d0024

.field public static final keyboardView:I = 0x20d002c

.field public static final keyguard_message_area:I = 0x20d0066

.field public static final keyguard_selector_fade_container:I = 0x20d0068

.field public static final keyguard_sim_perso_view:I = 0x20d0053

.field public static final left_icon:I = 0x20d0009

.field public static final line2:I = 0x20d0078

.field public static final line3:I = 0x20d0074

.field public static final list:I = 0x20d000a

.field public static final list_menu_presenter:I = 0x20d003a

.field public static final listview:I = 0x20d006c

.field public static final locale:I = 0x20d0056

.field public static final lock:I = 0x20d005d

.field public static final lock_screen:I = 0x20d001b

.field public static final menu:I = 0x20d000b

.field public static final message:I = 0x20d000c

.field public static final miracast_battery_tip_text:I = 0x20d007e

.field public static final nexti_keyguard_selector_fade_container:I = 0x20d0067

.field public static final nexti_keyguard_selector_view:I = 0x20d005a

.field public static final nexti_keyguard_selector_view_frame:I = 0x20d005b

.field public static final no_content_view_land:I = 0x20d0045

.field public static final no_content_view_port:I = 0x20d0043

.field public static final noti_icon:I = 0x20d0077

.field public static final noti_summary:I = 0x20d007a

.field public static final noti_title:I = 0x20d0079

.field public static final notilayout:I = 0x20d0072

.field public static final notititle:I = 0x20d0073

.field public static final overflow_menu_presenter:I = 0x20d003c

.field public static final padlock_image:I = 0x20d0070

.field public static final password_root:I = 0x20d0069

.field public static final paste:I = 0x20d0028

.field public static final point_touch_unlock_frame:I = 0x20d005f

.field public static final popup_submenu_presenter:I = 0x20d003d

.field public static final primary:I = 0x20d000d

.field public static final progress:I = 0x20d000e

.field public static final retryText:I = 0x20d0054

.field public static final right_icon:I = 0x20d000f

.field public static final rowTypeId:I = 0x20d0036

.field public static final scroll:I = 0x20d0040

.field public static final secondaryProgress:I = 0x20d001a

.field public static final selectAll:I = 0x20d0025

.field public static final selectTextMode:I = 0x20d002a

.field public static final selectedIcon:I = 0x20d0011

.field public static final skip:I = 0x20d0059

.field public static final smallIcon:I = 0x20d0032

.field public static final spacerBottom:I = 0x20d0058

.field public static final startSelectingText:I = 0x20d002e

.field public static final status_bar_aggregation_auto_noti:I = 0x20d0071

.field public static final status_bar_ongoing_noti:I = 0x20d0076

.field public static final stopSelectingText:I = 0x20d002f

.field public static final sub:I = 0x20d0057

.field public static final summary:I = 0x20d0010

.field public static final switchInputMethod:I = 0x20d002b

.field public static final switchWidget:I = 0x20d006b

.field public static final tabcontent:I = 0x20d0012

.field public static final tabhost:I = 0x20d0013

.field public static final tabs:I = 0x20d0014

.field public static final text1:I = 0x20d0015

.field public static final text2:I = 0x20d0016

.field public static final textlink:I = 0x20d003e

.field public static final title:I = 0x20d0017

.field public static final title_container:I = 0x20d0018

.field public static final toggle:I = 0x20d0019

.field public static final tracks:I = 0x20d0041

.field public static final txt_no_content_land:I = 0x20d0046

.field public static final txt_no_content_port:I = 0x20d0044

.field public static final unlock_touch_point:I = 0x20d006d

.field public static final up:I = 0x20d0037

.field public static final widget_frame:I = 0x20d001d

.field public static final wifi_screen_intro_image:I = 0x20d007c

.field public static final wifi_screen_intro_landscape:I = 0x20d007f

.field public static final wifi_screen_intro_portrait:I = 0x20d007b

.field public static final wifi_screen_intro_text:I = 0x20d007d


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 2501
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
