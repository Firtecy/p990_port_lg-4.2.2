.class public final Lcom/lge/lgdrm/DrmRight;
.super Ljava/lang/Object;
.source "DrmRight.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "DrmRight"


# instance fields
.field private accumulated:Ljava/lang/String;

.field private context:Landroid/content/Context;

.field private count:Ljava/lang/String;

.field private dateTime:Ljava/lang/String;

.field private filename:Ljava/lang/String;

.field private index:I

.field private individual:Ljava/lang/String;

.field private interval:Ljava/lang/String;

.field private metering:Z

.field private mid:Ljava/lang/String;

.field private permission:I

.field private previewContent:I

.field private system:Ljava/lang/String;

.field private timedCount:Ljava/lang/String;

.field private unlimited:Z

.field private useLeft:Ljava/lang/String;

.field private validFor:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 192
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 193
    return-void
.end method

.method protected constructor <init>(Lcom/lge/lgdrm/DrmContent;Landroid/content/Context;)V
    .registers 4
    .parameter "content"
    .parameter "context"

    #@0
    .prologue
    .line 202
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 204
    iget-object v0, p1, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@5
    iput-object v0, p0, Lcom/lge/lgdrm/DrmRight;->filename:Ljava/lang/String;

    #@7
    .line 205
    iget v0, p1, Lcom/lge/lgdrm/DrmContent;->index:I

    #@9
    iput v0, p0, Lcom/lge/lgdrm/DrmRight;->index:I

    #@b
    .line 206
    iget v0, p1, Lcom/lge/lgdrm/DrmContent;->previewContent:I

    #@d
    iput v0, p0, Lcom/lge/lgdrm/DrmRight;->previewContent:I

    #@f
    .line 207
    iget v0, p1, Lcom/lge/lgdrm/DrmContent;->permisson:I

    #@11
    iput v0, p0, Lcom/lge/lgdrm/DrmRight;->permission:I

    #@13
    .line 208
    iput-object p2, p0, Lcom/lge/lgdrm/DrmRight;->context:Landroid/content/Context;

    #@15
    .line 209
    return-void
.end method

.method public static getKeyByCID(Ljava/lang/String;[B[B)I
    .registers 5
    .parameter "cid"
    .parameter "encKey"
    .parameter "authSeed"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;,
            Ljava/lang/NullPointerException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v1, 0x10

    #@2
    .line 232
    sget-boolean v0, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@4
    if-nez v0, :cond_8

    #@6
    .line 233
    const/4 v0, -0x1

    #@7
    .line 253
    :goto_7
    return v0

    #@8
    .line 236
    :cond_8
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_16

    #@e
    .line 237
    new-instance v0, Ljava/lang/SecurityException;

    #@10
    const-string v1, "Need proper permission to access drm"

    #@12
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@15
    throw v0

    #@16
    .line 240
    :cond_16
    if-nez p0, :cond_20

    #@18
    .line 241
    new-instance v0, Ljava/lang/NullPointerException;

    #@1a
    const-string v1, "cid is null"

    #@1c
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0

    #@20
    .line 243
    :cond_20
    if-eqz p1, :cond_24

    #@22
    if-nez p2, :cond_2d

    #@24
    .line 244
    :cond_24
    new-instance v0, Ljava/lang/NullPointerException;

    #@26
    const-string/jumbo v1, "one of the key is null"

    #@29
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v0

    #@2d
    .line 246
    :cond_2d
    array-length v0, p1

    #@2e
    if-ge v0, v1, :cond_38

    #@30
    .line 247
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@32
    const-string v1, "encKey length is invalid"

    #@34
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@37
    throw v0

    #@38
    .line 249
    :cond_38
    array-length v0, p2

    #@39
    if-ge v0, v1, :cond_43

    #@3b
    .line 250
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@3d
    const-string v1, "authSeed length is invalid"

    #@3f
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@42
    throw v0

    #@43
    .line 253
    :cond_43
    invoke-static {p0, p1, p2}, Lcom/lge/lgdrm/DrmRight;->nativeGetKeyByCID(Ljava/lang/String;[B[B)I

    #@46
    move-result v0

    #@47
    goto :goto_7
.end method

.method public static getRegistrationCode()Ljava/lang/String;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 285
    sget-boolean v0, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@2
    if-nez v0, :cond_6

    #@4
    .line 286
    const/4 v0, 0x0

    #@5
    .line 293
    :goto_5
    return-object v0

    #@6
    .line 289
    :cond_6
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_14

    #@c
    .line 290
    new-instance v0, Ljava/lang/SecurityException;

    #@e
    const-string v1, "Need proper permission to access drm"

    #@10
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 293
    :cond_14
    invoke-static {}, Lcom/lge/lgdrm/DrmRight;->nativeGetRegistrationCode()Ljava/lang/String;

    #@17
    move-result-object v0

    #@18
    goto :goto_5
.end method

.method private native nativeCheckMetering(Ljava/lang/String;)Z
.end method

.method private static native nativeGetKeyByCID(Ljava/lang/String;[B[B)I
.end method

.method private static native nativeGetRegistrationCode()Ljava/lang/String;
.end method

.method private native nativeGetRightInfo(Ljava/lang/String;III)I
.end method

.method private native nativeManageMetering(Ljava/lang/String;Z)I
.end method

.method private static native nativeSetRandomSample()V
.end method

.method public static setRandomSample()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 264
    sget-boolean v0, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@2
    if-nez v0, :cond_5

    #@4
    .line 273
    :goto_4
    return-void

    #@5
    .line 268
    :cond_5
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_13

    #@b
    .line 269
    new-instance v0, Ljava/lang/SecurityException;

    #@d
    const-string v1, "Need proper permission to access drm"

    #@f
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@12
    throw v0

    #@13
    .line 272
    :cond_13
    invoke-static {}, Lcom/lge/lgdrm/DrmRight;->nativeSetRandomSample()V

    #@16
    goto :goto_4
.end method

.method private setRightInfo(ZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 14
    .parameter "unlimited"
    .parameter "metering"
    .parameter "useLeft"
    .parameter "validFor"
    .parameter "count"
    .parameter "dateTime"
    .parameter "interval"
    .parameter "timedCount"
    .parameter "accumulated"
    .parameter "individual"
    .parameter "system"
    .parameter "mid"

    #@0
    .prologue
    .line 601
    iput-boolean p1, p0, Lcom/lge/lgdrm/DrmRight;->unlimited:Z

    #@2
    .line 602
    invoke-direct {p0, p3}, Lcom/lge/lgdrm/DrmRight;->translateSummaryInfo(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    iput-object v0, p0, Lcom/lge/lgdrm/DrmRight;->useLeft:Ljava/lang/String;

    #@8
    .line 603
    invoke-direct {p0, p4}, Lcom/lge/lgdrm/DrmRight;->translateSummaryInfo(Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    iput-object v0, p0, Lcom/lge/lgdrm/DrmRight;->validFor:Ljava/lang/String;

    #@e
    .line 604
    iput-object p5, p0, Lcom/lge/lgdrm/DrmRight;->count:Ljava/lang/String;

    #@10
    .line 605
    iput-object p6, p0, Lcom/lge/lgdrm/DrmRight;->dateTime:Ljava/lang/String;

    #@12
    .line 606
    iput-object p7, p0, Lcom/lge/lgdrm/DrmRight;->interval:Ljava/lang/String;

    #@14
    .line 607
    iput-object p8, p0, Lcom/lge/lgdrm/DrmRight;->timedCount:Ljava/lang/String;

    #@16
    .line 608
    iput-object p9, p0, Lcom/lge/lgdrm/DrmRight;->accumulated:Ljava/lang/String;

    #@18
    .line 609
    iput-object p10, p0, Lcom/lge/lgdrm/DrmRight;->individual:Ljava/lang/String;

    #@1a
    .line 610
    iput-object p11, p0, Lcom/lge/lgdrm/DrmRight;->system:Ljava/lang/String;

    #@1c
    .line 611
    iput-boolean p2, p0, Lcom/lge/lgdrm/DrmRight;->metering:Z

    #@1e
    .line 612
    iput-object p12, p0, Lcom/lge/lgdrm/DrmRight;->mid:Ljava/lang/String;

    #@20
    .line 613
    return-void
.end method

.method private translateSummaryInfo(Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .parameter "original"

    #@0
    .prologue
    .line 553
    if-nez p1, :cond_4

    #@2
    .line 554
    const/4 p1, 0x0

    #@3
    .line 592
    .end local p1
    :cond_3
    :goto_3
    return-object p1

    #@4
    .line 557
    .restart local p1
    :cond_4
    iget-object v4, p0, Lcom/lge/lgdrm/DrmRight;->context:Landroid/content/Context;

    #@6
    if-eqz v4, :cond_3

    #@8
    .line 562
    const/4 v0, 0x0

    #@9
    .line 563
    .local v0, count:I
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@e
    move-result v4

    #@f
    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    #@12
    .line 564
    .local v1, result:Ljava/lang/StringBuilder;
    new-instance v2, Ljava/util/StringTokenizer;

    #@14
    const-string v4, " )"

    #@16
    const/4 v5, 0x1

    #@17
    invoke-direct {v2, p1, v4, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    #@1a
    .line 566
    .local v2, st:Ljava/util/StringTokenizer;
    :goto_1a
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    #@1d
    move-result v4

    #@1e
    if-eqz v4, :cond_a4

    #@20
    .line 567
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    .line 568
    .local v3, w:Ljava/lang/String;
    const-string v4, " "

    #@26
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@29
    move-result v4

    #@2a
    if-eqz v4, :cond_30

    #@2c
    .line 569
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    goto :goto_1a

    #@30
    .line 571
    :cond_30
    and-int/lit8 v4, v0, 0x1

    #@32
    if-nez v4, :cond_4b

    #@34
    const-string v4, "day"

    #@36
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39
    move-result v4

    #@3a
    if-eqz v4, :cond_4b

    #@3c
    .line 572
    iget-object v4, p0, Lcom/lge/lgdrm/DrmRight;->context:Landroid/content/Context;

    #@3e
    const v5, 0x2090170

    #@41
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@44
    move-result-object v4

    #@45
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    .line 573
    or-int/lit8 v0, v0, 0x1

    #@4a
    .line 574
    goto :goto_1a

    #@4b
    .line 575
    :cond_4b
    and-int/lit8 v4, v0, 0x2

    #@4d
    if-nez v4, :cond_66

    #@4f
    const-string v4, "hour"

    #@51
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@54
    move-result v4

    #@55
    if-eqz v4, :cond_66

    #@57
    .line 576
    iget-object v4, p0, Lcom/lge/lgdrm/DrmRight;->context:Landroid/content/Context;

    #@59
    const v5, 0x2090171

    #@5c
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@5f
    move-result-object v4

    #@60
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    .line 577
    or-int/lit8 v0, v0, 0x2

    #@65
    .line 578
    goto :goto_1a

    #@66
    .line 579
    :cond_66
    and-int/lit8 v4, v0, 0x4

    #@68
    if-nez v4, :cond_82

    #@6a
    const-string/jumbo v4, "min"

    #@6d
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@70
    move-result v4

    #@71
    if-eqz v4, :cond_82

    #@73
    .line 580
    iget-object v4, p0, Lcom/lge/lgdrm/DrmRight;->context:Landroid/content/Context;

    #@75
    const v5, 0x2090172

    #@78
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@7b
    move-result-object v4

    #@7c
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    .line 581
    or-int/lit8 v0, v0, 0x4

    #@81
    .line 582
    goto :goto_1a

    #@82
    .line 583
    :cond_82
    and-int/lit8 v4, v0, 0x8

    #@84
    if-nez v4, :cond_9f

    #@86
    const-string/jumbo v4, "sec"

    #@89
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8c
    move-result v4

    #@8d
    if-eqz v4, :cond_9f

    #@8f
    .line 584
    iget-object v4, p0, Lcom/lge/lgdrm/DrmRight;->context:Landroid/content/Context;

    #@91
    const v5, 0x2090173

    #@94
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@97
    move-result-object v4

    #@98
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    .line 585
    or-int/lit8 v0, v0, 0x8

    #@9d
    .line 586
    goto/16 :goto_1a

    #@9f
    .line 589
    :cond_9f
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    goto/16 :goto_1a

    #@a4
    .line 592
    .end local v3           #w:Ljava/lang/String;
    :cond_a4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a7
    move-result-object p1

    #@a8
    goto/16 :goto_3
.end method


# virtual methods
.method public disableMetering()I
    .registers 3

    #@0
    .prologue
    .line 515
    iget-object v0, p0, Lcom/lge/lgdrm/DrmRight;->mid:Ljava/lang/String;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {p0, v0, v1}, Lcom/lge/lgdrm/DrmRight;->nativeManageMetering(Ljava/lang/String;Z)I

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public enableMetering()I
    .registers 3

    #@0
    .prologue
    .line 505
    iget-object v0, p0, Lcom/lge/lgdrm/DrmRight;->mid:Ljava/lang/String;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-direct {p0, v0, v1}, Lcom/lge/lgdrm/DrmRight;->nativeManageMetering(Ljava/lang/String;Z)I

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public getConstraint(I)Ljava/lang/String;
    .registers 4
    .parameter "type"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 463
    iget-boolean v0, p0, Lcom/lge/lgdrm/DrmRight;->unlimited:Z

    #@2
    if-eqz v0, :cond_6

    #@4
    .line 464
    const/4 v0, 0x0

    #@5
    .line 481
    :goto_5
    return-object v0

    #@6
    .line 467
    :cond_6
    packed-switch p1, :pswitch_data_26

    #@9
    .line 483
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@b
    const-string v1, "Invalid type"

    #@d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0

    #@11
    .line 469
    :pswitch_11
    iget-object v0, p0, Lcom/lge/lgdrm/DrmRight;->count:Ljava/lang/String;

    #@13
    goto :goto_5

    #@14
    .line 471
    :pswitch_14
    iget-object v0, p0, Lcom/lge/lgdrm/DrmRight;->dateTime:Ljava/lang/String;

    #@16
    goto :goto_5

    #@17
    .line 473
    :pswitch_17
    iget-object v0, p0, Lcom/lge/lgdrm/DrmRight;->interval:Ljava/lang/String;

    #@19
    goto :goto_5

    #@1a
    .line 475
    :pswitch_1a
    iget-object v0, p0, Lcom/lge/lgdrm/DrmRight;->timedCount:Ljava/lang/String;

    #@1c
    goto :goto_5

    #@1d
    .line 477
    :pswitch_1d
    iget-object v0, p0, Lcom/lge/lgdrm/DrmRight;->accumulated:Ljava/lang/String;

    #@1f
    goto :goto_5

    #@20
    .line 479
    :pswitch_20
    iget-object v0, p0, Lcom/lge/lgdrm/DrmRight;->individual:Ljava/lang/String;

    #@22
    goto :goto_5

    #@23
    .line 481
    :pswitch_23
    iget-object v0, p0, Lcom/lge/lgdrm/DrmRight;->system:Ljava/lang/String;

    #@25
    goto :goto_5

    #@26
    .line 467
    :pswitch_data_26
    .packed-switch 0x1
        :pswitch_11
        :pswitch_14
        :pswitch_17
        :pswitch_1a
        :pswitch_1d
        :pswitch_20
        :pswitch_23
    .end packed-switch
.end method

.method public getConstraintList()[I
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    .line 386
    const/4 v0, 0x0

    #@3
    .line 390
    .local v0, indx:I
    iget-boolean v4, p0, Lcom/lge/lgdrm/DrmRight;->unlimited:Z

    #@5
    if-eqz v4, :cond_9

    #@7
    move-object v1, v3

    #@8
    .line 439
    :goto_8
    return-object v1

    #@9
    .line 394
    :cond_9
    const/16 v4, 0x8

    #@b
    new-array v2, v4, [I

    #@d
    .line 395
    .local v2, tmp:[I
    if-nez v2, :cond_11

    #@f
    move-object v1, v3

    #@10
    .line 396
    goto :goto_8

    #@11
    .line 399
    :cond_11
    iget-object v4, p0, Lcom/lge/lgdrm/DrmRight;->count:Ljava/lang/String;

    #@13
    if-eqz v4, :cond_1a

    #@15
    .line 400
    const/4 v4, 0x1

    #@16
    aput v4, v2, v0

    #@18
    .line 401
    add-int/lit8 v0, v0, 0x1

    #@1a
    .line 403
    :cond_1a
    iget-object v4, p0, Lcom/lge/lgdrm/DrmRight;->dateTime:Ljava/lang/String;

    #@1c
    if-eqz v4, :cond_23

    #@1e
    .line 404
    const/4 v4, 0x2

    #@1f
    aput v4, v2, v0

    #@21
    .line 405
    add-int/lit8 v0, v0, 0x1

    #@23
    .line 407
    :cond_23
    iget-object v4, p0, Lcom/lge/lgdrm/DrmRight;->interval:Ljava/lang/String;

    #@25
    if-eqz v4, :cond_2c

    #@27
    .line 408
    const/4 v4, 0x3

    #@28
    aput v4, v2, v0

    #@2a
    .line 409
    add-int/lit8 v0, v0, 0x1

    #@2c
    .line 411
    :cond_2c
    iget-object v4, p0, Lcom/lge/lgdrm/DrmRight;->timedCount:Ljava/lang/String;

    #@2e
    if-eqz v4, :cond_35

    #@30
    .line 412
    const/4 v4, 0x4

    #@31
    aput v4, v2, v0

    #@33
    .line 413
    add-int/lit8 v0, v0, 0x1

    #@35
    .line 415
    :cond_35
    iget-object v4, p0, Lcom/lge/lgdrm/DrmRight;->accumulated:Ljava/lang/String;

    #@37
    if-eqz v4, :cond_3e

    #@39
    .line 416
    const/4 v4, 0x5

    #@3a
    aput v4, v2, v0

    #@3c
    .line 417
    add-int/lit8 v0, v0, 0x1

    #@3e
    .line 419
    :cond_3e
    iget-object v4, p0, Lcom/lge/lgdrm/DrmRight;->individual:Ljava/lang/String;

    #@40
    if-eqz v4, :cond_47

    #@42
    .line 420
    const/4 v4, 0x6

    #@43
    aput v4, v2, v0

    #@45
    .line 421
    add-int/lit8 v0, v0, 0x1

    #@47
    .line 423
    :cond_47
    iget-object v4, p0, Lcom/lge/lgdrm/DrmRight;->system:Ljava/lang/String;

    #@49
    if-eqz v4, :cond_50

    #@4b
    .line 424
    const/4 v4, 0x7

    #@4c
    aput v4, v2, v0

    #@4e
    .line 425
    add-int/lit8 v0, v0, 0x1

    #@50
    .line 428
    :cond_50
    if-nez v0, :cond_54

    #@52
    move-object v1, v3

    #@53
    .line 429
    goto :goto_8

    #@54
    .line 432
    :cond_54
    new-array v1, v0, [I

    #@56
    .line 433
    .local v1, list:[I
    if-nez v1, :cond_5a

    #@58
    move-object v1, v3

    #@59
    .line 434
    goto :goto_8

    #@5a
    .line 437
    :cond_5a
    invoke-static {v2, v5, v1, v5, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@5d
    goto :goto_8
.end method

.method public getPermission()I
    .registers 2

    #@0
    .prologue
    .line 345
    iget v0, p0, Lcom/lge/lgdrm/DrmRight;->permission:I

    #@2
    return v0
.end method

.method public getSummaryInfo(I)Ljava/lang/String;
    .registers 4
    .parameter "type"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 361
    const/4 v0, 0x1

    #@1
    if-ne p1, v0, :cond_6

    #@3
    .line 362
    iget-object v0, p0, Lcom/lge/lgdrm/DrmRight;->validFor:Ljava/lang/String;

    #@5
    .line 366
    :goto_5
    return-object v0

    #@6
    .line 365
    :cond_6
    const/4 v0, 0x2

    #@7
    if-ne p1, v0, :cond_c

    #@9
    .line 366
    iget-object v0, p0, Lcom/lge/lgdrm/DrmRight;->useLeft:Ljava/lang/String;

    #@b
    goto :goto_5

    #@c
    .line 369
    :cond_c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@e
    const-string v1, "Invalid type"

    #@10
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0
.end method

.method protected isMatched(Lcom/lge/lgdrm/DrmContent;)Z
    .registers 5
    .parameter "content"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 536
    iget-object v1, p0, Lcom/lge/lgdrm/DrmRight;->filename:Ljava/lang/String;

    #@3
    iget-object v2, p1, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@5
    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_c

    #@b
    .line 542
    :cond_b
    :goto_b
    return v0

    #@c
    .line 539
    :cond_c
    iget v1, p0, Lcom/lge/lgdrm/DrmRight;->index:I

    #@e
    iget v2, p1, Lcom/lge/lgdrm/DrmContent;->index:I

    #@10
    if-ne v1, v2, :cond_b

    #@12
    .line 542
    const/4 v0, 0x1

    #@13
    goto :goto_b
.end method

.method public isMeteringEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 524
    iget-object v0, p0, Lcom/lge/lgdrm/DrmRight;->mid:Ljava/lang/String;

    #@2
    invoke-direct {p0, v0}, Lcom/lge/lgdrm/DrmRight;->nativeCheckMetering(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isMeteringRight()Z
    .registers 2

    #@0
    .prologue
    .line 493
    iget-boolean v0, p0, Lcom/lge/lgdrm/DrmRight;->metering:Z

    #@2
    return v0
.end method

.method public isUnlimited()Z
    .registers 2

    #@0
    .prologue
    .line 329
    iget-boolean v0, p0, Lcom/lge/lgdrm/DrmRight;->unlimited:Z

    #@2
    return v0
.end method

.method public isValidRight()Z
    .registers 8

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v6, 0x0

    #@2
    .line 303
    iget-object v2, p0, Lcom/lge/lgdrm/DrmRight;->filename:Ljava/lang/String;

    #@4
    iget v3, p0, Lcom/lge/lgdrm/DrmRight;->index:I

    #@6
    iget v4, p0, Lcom/lge/lgdrm/DrmRight;->previewContent:I

    #@8
    iget v5, p0, Lcom/lge/lgdrm/DrmRight;->permission:I

    #@a
    invoke-direct {p0, v2, v3, v4, v5}, Lcom/lge/lgdrm/DrmRight;->nativeGetRightInfo(Ljava/lang/String;III)I

    #@d
    move-result v0

    #@e
    .line 305
    .local v0, retVal:I
    if-eqz v0, :cond_25

    #@10
    .line 307
    iput-boolean v1, p0, Lcom/lge/lgdrm/DrmRight;->unlimited:Z

    #@12
    .line 308
    iput-boolean v1, p0, Lcom/lge/lgdrm/DrmRight;->metering:Z

    #@14
    .line 309
    iput-object v6, p0, Lcom/lge/lgdrm/DrmRight;->count:Ljava/lang/String;

    #@16
    .line 310
    iput-object v6, p0, Lcom/lge/lgdrm/DrmRight;->dateTime:Ljava/lang/String;

    #@18
    .line 311
    iput-object v6, p0, Lcom/lge/lgdrm/DrmRight;->interval:Ljava/lang/String;

    #@1a
    .line 312
    iput-object v6, p0, Lcom/lge/lgdrm/DrmRight;->timedCount:Ljava/lang/String;

    #@1c
    .line 313
    iput-object v6, p0, Lcom/lge/lgdrm/DrmRight;->accumulated:Ljava/lang/String;

    #@1e
    .line 314
    iput-object v6, p0, Lcom/lge/lgdrm/DrmRight;->individual:Ljava/lang/String;

    #@20
    .line 315
    iput-object v6, p0, Lcom/lge/lgdrm/DrmRight;->system:Ljava/lang/String;

    #@22
    .line 316
    iput-object v6, p0, Lcom/lge/lgdrm/DrmRight;->mid:Ljava/lang/String;

    #@24
    .line 320
    :goto_24
    return v1

    #@25
    :cond_25
    const/4 v1, 0x1

    #@26
    goto :goto_24
.end method
