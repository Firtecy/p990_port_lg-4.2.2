.class public final Lcom/lge/lgdrm/DrmContentMetaData;
.super Ljava/lang/Object;
.source "DrmContentMetaData.java"


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field public album:Ljava/lang/StringBuffer;

.field public author:Ljava/lang/StringBuffer;

.field public category:Ljava/lang/StringBuffer;

.field public classification:Ljava/lang/StringBuffer;

.field public copyRight:Ljava/lang/StringBuffer;

.field public coverURI:Ljava/lang/StringBuffer;

.field public description:Ljava/lang/StringBuffer;

.field public duration:Ljava/lang/StringBuffer;

.field public lyricsURI:Ljava/lang/StringBuffer;

.field public performer:Ljava/lang/StringBuffer;

.field public recordingYear:Ljava/lang/StringBuffer;

.field public title:Ljava/lang/StringBuffer;

.field public trackNumber:Ljava/lang/StringBuffer;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 39
    const-string v0, "DrmCmtMeta"

    #@2
    sput-object v0, Lcom/lge/lgdrm/DrmContentMetaData;->TAG:Ljava/lang/String;

    #@4
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 71
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 72
    return-void
.end method


# virtual methods
.method public getData(I)Ljava/lang/String;
    .registers 4
    .parameter "type"

    #@0
    .prologue
    .line 187
    const/4 v0, 0x0

    #@1
    .line 188
    .local v0, data:Ljava/lang/StringBuffer;
    packed-switch p1, :pswitch_data_34

    #@4
    .line 230
    :goto_4
    if-nez v0, :cond_2f

    #@6
    .line 231
    const/4 v1, 0x0

    #@7
    .line 234
    :goto_7
    return-object v1

    #@8
    .line 190
    :pswitch_8
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContentMetaData;->title:Ljava/lang/StringBuffer;

    #@a
    .line 191
    goto :goto_4

    #@b
    .line 193
    :pswitch_b
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContentMetaData;->description:Ljava/lang/StringBuffer;

    #@d
    .line 194
    goto :goto_4

    #@e
    .line 196
    :pswitch_e
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContentMetaData;->copyRight:Ljava/lang/StringBuffer;

    #@10
    .line 197
    goto :goto_4

    #@11
    .line 199
    :pswitch_11
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContentMetaData;->performer:Ljava/lang/StringBuffer;

    #@13
    .line 200
    goto :goto_4

    #@14
    .line 202
    :pswitch_14
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContentMetaData;->author:Ljava/lang/StringBuffer;

    #@16
    .line 203
    goto :goto_4

    #@17
    .line 205
    :pswitch_17
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContentMetaData;->category:Ljava/lang/StringBuffer;

    #@19
    .line 206
    goto :goto_4

    #@1a
    .line 208
    :pswitch_1a
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContentMetaData;->classification:Ljava/lang/StringBuffer;

    #@1c
    .line 209
    goto :goto_4

    #@1d
    .line 211
    :pswitch_1d
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContentMetaData;->album:Ljava/lang/StringBuffer;

    #@1f
    .line 212
    goto :goto_4

    #@20
    .line 214
    :pswitch_20
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContentMetaData;->recordingYear:Ljava/lang/StringBuffer;

    #@22
    .line 215
    goto :goto_4

    #@23
    .line 217
    :pswitch_23
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContentMetaData;->coverURI:Ljava/lang/StringBuffer;

    #@25
    .line 218
    goto :goto_4

    #@26
    .line 220
    :pswitch_26
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContentMetaData;->lyricsURI:Ljava/lang/StringBuffer;

    #@28
    .line 221
    goto :goto_4

    #@29
    .line 223
    :pswitch_29
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContentMetaData;->trackNumber:Ljava/lang/StringBuffer;

    #@2b
    .line 224
    goto :goto_4

    #@2c
    .line 226
    :pswitch_2c
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContentMetaData;->duration:Ljava/lang/StringBuffer;

    #@2e
    goto :goto_4

    #@2f
    .line 234
    :cond_2f
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    goto :goto_7

    #@34
    .line 188
    :pswitch_data_34
    .packed-switch 0x1
        :pswitch_8
        :pswitch_b
        :pswitch_e
        :pswitch_11
        :pswitch_14
        :pswitch_17
        :pswitch_1a
        :pswitch_1d
        :pswitch_20
        :pswitch_23
        :pswitch_26
        :pswitch_29
        :pswitch_2c
    .end packed-switch
.end method

.method public getDataList()[I
    .registers 7

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v5, 0x0

    #@2
    .line 94
    const/4 v0, 0x0

    #@3
    .line 95
    .local v0, count:I
    const/16 v4, 0xe

    #@5
    new-array v2, v4, [I

    #@7
    .line 98
    .local v2, tmp:[I
    iget-object v4, p0, Lcom/lge/lgdrm/DrmContentMetaData;->title:Ljava/lang/StringBuffer;

    #@9
    if-eqz v4, :cond_10

    #@b
    .line 99
    const/4 v4, 0x1

    #@c
    aput v4, v2, v0

    #@e
    .line 100
    add-int/lit8 v0, v0, 0x1

    #@10
    .line 102
    :cond_10
    iget-object v4, p0, Lcom/lge/lgdrm/DrmContentMetaData;->description:Ljava/lang/StringBuffer;

    #@12
    if-eqz v4, :cond_19

    #@14
    .line 103
    const/4 v4, 0x2

    #@15
    aput v4, v2, v0

    #@17
    .line 104
    add-int/lit8 v0, v0, 0x1

    #@19
    .line 106
    :cond_19
    iget-object v4, p0, Lcom/lge/lgdrm/DrmContentMetaData;->copyRight:Ljava/lang/StringBuffer;

    #@1b
    if-eqz v4, :cond_22

    #@1d
    .line 107
    const/4 v4, 0x3

    #@1e
    aput v4, v2, v0

    #@20
    .line 108
    add-int/lit8 v0, v0, 0x1

    #@22
    .line 110
    :cond_22
    iget-object v4, p0, Lcom/lge/lgdrm/DrmContentMetaData;->performer:Ljava/lang/StringBuffer;

    #@24
    if-eqz v4, :cond_2b

    #@26
    .line 111
    const/4 v4, 0x4

    #@27
    aput v4, v2, v0

    #@29
    .line 112
    add-int/lit8 v0, v0, 0x1

    #@2b
    .line 114
    :cond_2b
    iget-object v4, p0, Lcom/lge/lgdrm/DrmContentMetaData;->author:Ljava/lang/StringBuffer;

    #@2d
    if-eqz v4, :cond_34

    #@2f
    .line 115
    const/4 v4, 0x5

    #@30
    aput v4, v2, v0

    #@32
    .line 116
    add-int/lit8 v0, v0, 0x1

    #@34
    .line 118
    :cond_34
    iget-object v4, p0, Lcom/lge/lgdrm/DrmContentMetaData;->category:Ljava/lang/StringBuffer;

    #@36
    if-eqz v4, :cond_3d

    #@38
    .line 119
    const/4 v4, 0x6

    #@39
    aput v4, v2, v0

    #@3b
    .line 120
    add-int/lit8 v0, v0, 0x1

    #@3d
    .line 122
    :cond_3d
    iget-object v4, p0, Lcom/lge/lgdrm/DrmContentMetaData;->classification:Ljava/lang/StringBuffer;

    #@3f
    if-eqz v4, :cond_46

    #@41
    .line 123
    const/4 v4, 0x7

    #@42
    aput v4, v2, v0

    #@44
    .line 124
    add-int/lit8 v0, v0, 0x1

    #@46
    .line 126
    :cond_46
    iget-object v4, p0, Lcom/lge/lgdrm/DrmContentMetaData;->album:Ljava/lang/StringBuffer;

    #@48
    if-eqz v4, :cond_50

    #@4a
    .line 127
    const/16 v4, 0x8

    #@4c
    aput v4, v2, v0

    #@4e
    .line 128
    add-int/lit8 v0, v0, 0x1

    #@50
    .line 130
    :cond_50
    iget-object v4, p0, Lcom/lge/lgdrm/DrmContentMetaData;->recordingYear:Ljava/lang/StringBuffer;

    #@52
    if-eqz v4, :cond_5a

    #@54
    .line 131
    const/16 v4, 0x9

    #@56
    aput v4, v2, v0

    #@58
    .line 132
    add-int/lit8 v0, v0, 0x1

    #@5a
    .line 134
    :cond_5a
    iget-object v4, p0, Lcom/lge/lgdrm/DrmContentMetaData;->coverURI:Ljava/lang/StringBuffer;

    #@5c
    if-eqz v4, :cond_64

    #@5e
    .line 135
    const/16 v4, 0xa

    #@60
    aput v4, v2, v0

    #@62
    .line 136
    add-int/lit8 v0, v0, 0x1

    #@64
    .line 138
    :cond_64
    iget-object v4, p0, Lcom/lge/lgdrm/DrmContentMetaData;->lyricsURI:Ljava/lang/StringBuffer;

    #@66
    if-eqz v4, :cond_6e

    #@68
    .line 139
    const/16 v4, 0xb

    #@6a
    aput v4, v2, v0

    #@6c
    .line 140
    add-int/lit8 v0, v0, 0x1

    #@6e
    .line 142
    :cond_6e
    iget-object v4, p0, Lcom/lge/lgdrm/DrmContentMetaData;->trackNumber:Ljava/lang/StringBuffer;

    #@70
    if-eqz v4, :cond_78

    #@72
    .line 143
    const/16 v4, 0xc

    #@74
    aput v4, v2, v0

    #@76
    .line 144
    add-int/lit8 v0, v0, 0x1

    #@78
    .line 146
    :cond_78
    iget-object v4, p0, Lcom/lge/lgdrm/DrmContentMetaData;->duration:Ljava/lang/StringBuffer;

    #@7a
    if-eqz v4, :cond_82

    #@7c
    .line 147
    const/16 v4, 0xd

    #@7e
    aput v4, v2, v0

    #@80
    .line 148
    add-int/lit8 v0, v0, 0x1

    #@82
    .line 151
    :cond_82
    if-nez v0, :cond_86

    #@84
    move-object v1, v3

    #@85
    .line 162
    :goto_85
    return-object v1

    #@86
    .line 155
    :cond_86
    new-array v1, v0, [I

    #@88
    .line 156
    .local v1, list:[I
    if-nez v1, :cond_8c

    #@8a
    move-object v1, v3

    #@8b
    .line 157
    goto :goto_85

    #@8c
    .line 160
    :cond_8c
    invoke-static {v2, v5, v1, v5, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@8f
    goto :goto_85
.end method

.method public setData(ILjava/lang/String;)I
    .registers 6
    .parameter "type"
    .parameter "value"

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 266
    const/4 v1, 0x1

    #@2
    if-lt p1, v1, :cond_8

    #@4
    const/16 v1, 0xe

    #@6
    if-lt p1, v1, :cond_11

    #@8
    .line 267
    :cond_8
    sget-object v1, Lcom/lge/lgdrm/DrmContentMetaData;->TAG:Ljava/lang/String;

    #@a
    const-string/jumbo v2, "setData() : Type is invalid"

    #@d
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 318
    :goto_10
    return v0

    #@11
    .line 270
    :cond_11
    if-nez p2, :cond_1c

    #@13
    .line 271
    sget-object v1, Lcom/lge/lgdrm/DrmContentMetaData;->TAG:Ljava/lang/String;

    #@15
    const-string/jumbo v2, "setData() : Value is null"

    #@18
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    goto :goto_10

    #@1c
    .line 276
    :cond_1c
    packed-switch p1, :pswitch_data_8a

    #@1f
    .line 318
    :goto_1f
    const/4 v0, 0x0

    #@20
    goto :goto_10

    #@21
    .line 278
    :pswitch_21
    new-instance v0, Ljava/lang/StringBuffer;

    #@23
    invoke-direct {v0, p2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    #@26
    iput-object v0, p0, Lcom/lge/lgdrm/DrmContentMetaData;->title:Ljava/lang/StringBuffer;

    #@28
    goto :goto_1f

    #@29
    .line 281
    :pswitch_29
    new-instance v0, Ljava/lang/StringBuffer;

    #@2b
    invoke-direct {v0, p2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    #@2e
    iput-object v0, p0, Lcom/lge/lgdrm/DrmContentMetaData;->description:Ljava/lang/StringBuffer;

    #@30
    goto :goto_1f

    #@31
    .line 284
    :pswitch_31
    new-instance v0, Ljava/lang/StringBuffer;

    #@33
    invoke-direct {v0, p2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    #@36
    iput-object v0, p0, Lcom/lge/lgdrm/DrmContentMetaData;->copyRight:Ljava/lang/StringBuffer;

    #@38
    goto :goto_1f

    #@39
    .line 287
    :pswitch_39
    new-instance v0, Ljava/lang/StringBuffer;

    #@3b
    invoke-direct {v0, p2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    #@3e
    iput-object v0, p0, Lcom/lge/lgdrm/DrmContentMetaData;->performer:Ljava/lang/StringBuffer;

    #@40
    goto :goto_1f

    #@41
    .line 290
    :pswitch_41
    new-instance v0, Ljava/lang/StringBuffer;

    #@43
    invoke-direct {v0, p2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    #@46
    iput-object v0, p0, Lcom/lge/lgdrm/DrmContentMetaData;->author:Ljava/lang/StringBuffer;

    #@48
    goto :goto_1f

    #@49
    .line 293
    :pswitch_49
    new-instance v0, Ljava/lang/StringBuffer;

    #@4b
    invoke-direct {v0, p2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    #@4e
    iput-object v0, p0, Lcom/lge/lgdrm/DrmContentMetaData;->category:Ljava/lang/StringBuffer;

    #@50
    goto :goto_1f

    #@51
    .line 296
    :pswitch_51
    new-instance v0, Ljava/lang/StringBuffer;

    #@53
    invoke-direct {v0, p2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    #@56
    iput-object v0, p0, Lcom/lge/lgdrm/DrmContentMetaData;->classification:Ljava/lang/StringBuffer;

    #@58
    goto :goto_1f

    #@59
    .line 299
    :pswitch_59
    new-instance v0, Ljava/lang/StringBuffer;

    #@5b
    invoke-direct {v0, p2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    #@5e
    iput-object v0, p0, Lcom/lge/lgdrm/DrmContentMetaData;->album:Ljava/lang/StringBuffer;

    #@60
    goto :goto_1f

    #@61
    .line 302
    :pswitch_61
    new-instance v0, Ljava/lang/StringBuffer;

    #@63
    invoke-direct {v0, p2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    #@66
    iput-object v0, p0, Lcom/lge/lgdrm/DrmContentMetaData;->recordingYear:Ljava/lang/StringBuffer;

    #@68
    goto :goto_1f

    #@69
    .line 305
    :pswitch_69
    new-instance v0, Ljava/lang/StringBuffer;

    #@6b
    invoke-direct {v0, p2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    #@6e
    iput-object v0, p0, Lcom/lge/lgdrm/DrmContentMetaData;->coverURI:Ljava/lang/StringBuffer;

    #@70
    goto :goto_1f

    #@71
    .line 308
    :pswitch_71
    new-instance v0, Ljava/lang/StringBuffer;

    #@73
    invoke-direct {v0, p2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    #@76
    iput-object v0, p0, Lcom/lge/lgdrm/DrmContentMetaData;->lyricsURI:Ljava/lang/StringBuffer;

    #@78
    goto :goto_1f

    #@79
    .line 311
    :pswitch_79
    new-instance v0, Ljava/lang/StringBuffer;

    #@7b
    invoke-direct {v0, p2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    #@7e
    iput-object v0, p0, Lcom/lge/lgdrm/DrmContentMetaData;->trackNumber:Ljava/lang/StringBuffer;

    #@80
    goto :goto_1f

    #@81
    .line 314
    :pswitch_81
    new-instance v0, Ljava/lang/StringBuffer;

    #@83
    invoke-direct {v0, p2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    #@86
    iput-object v0, p0, Lcom/lge/lgdrm/DrmContentMetaData;->duration:Ljava/lang/StringBuffer;

    #@88
    goto :goto_1f

    #@89
    .line 276
    nop

    #@8a
    :pswitch_data_8a
    .packed-switch 0x1
        :pswitch_21
        :pswitch_29
        :pswitch_31
        :pswitch_39
        :pswitch_41
        :pswitch_49
        :pswitch_51
        :pswitch_59
        :pswitch_61
        :pswitch_69
        :pswitch_71
        :pswitch_79
        :pswitch_81
    .end packed-switch
.end method
