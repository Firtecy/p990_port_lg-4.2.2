.class public final Lcom/lge/lgdrm/DrmObjectSession;
.super Ljava/lang/Object;
.source "DrmObjectSession.java"


# static fields
.field private static final PINIT_HND:I = 0x0

.field private static final PINIT_MAX:I = 0x2

.field private static final PINIT_SL:I = 0x1

.field private static final TAG:Ljava/lang/String; = "DrmObjSes"


# instance fields
.field bEndError:Z

.field private context:Landroid/content/Context;

.field private downloadAgent:I

.field private errorMsg:Ljava/lang/String;

.field private failReason:I

.field private nativeProcessHandle:I

.field private nativeSLType:I

.field private nativeSession:I

.field nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

.field private redirectURL:Ljava/lang/String;

.field private storedFilename:Ljava/lang/String;

.field private validSession:Z

.field private waitFlag:Z


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 289
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 290
    return-void
.end method

.method protected constructor <init>(ILandroid/content/Context;I)V
    .registers 5
    .parameter "downloadAgent"
    .parameter "context"
    .parameter "session"

    #@0
    .prologue
    .line 301
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 302
    iput p1, p0, Lcom/lge/lgdrm/DrmObjectSession;->downloadAgent:I

    #@5
    .line 303
    iput-object p2, p0, Lcom/lge/lgdrm/DrmObjectSession;->context:Landroid/content/Context;

    #@7
    .line 304
    iput p3, p0, Lcom/lge/lgdrm/DrmObjectSession;->nativeSession:I

    #@9
    .line 306
    const/4 v0, 0x1

    #@a
    iput-boolean v0, p0, Lcom/lge/lgdrm/DrmObjectSession;->validSession:Z

    #@c
    .line 307
    return-void
.end method

.method private native nativeDestroySession(II)V
.end method

.method private native nativeProcessEnd(IILjava/lang/String;)I
.end method

.method private native nativeProcessInit(ILjava/lang/String;Ljava/lang/String;II)[I
.end method

.method private native nativeProcessStatus(I)I
.end method

.method private native nativeProcessUpdate(I[BII)I
.end method

.method private native nativeSetContentSize(IJ)I
.end method

.method private postWaitResult(Ljava/lang/String;IILjava/lang/String;)V
    .registers 19
    .parameter "resultReceiver"
    .parameter "result"
    .parameter "waitType"
    .parameter "filename"

    #@0
    .prologue
    .line 932
    const-string v11, "DrmObjSes"

    #@2
    new-instance v12, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v13, "postWaitResult() : result = "

    #@a
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v12

    #@e
    move/from16 v0, p2

    #@10
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v12

    #@14
    const-string v13, " waitType = "

    #@16
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v12

    #@1a
    move/from16 v0, p3

    #@1c
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v12

    #@20
    const-string v13, " filename = "

    #@22
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v12

    #@26
    move-object/from16 v0, p4

    #@28
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v12

    #@2c
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v12

    #@30
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 934
    if-nez p1, :cond_3e

    #@35
    .line 936
    const-string v11, "DrmObjSes"

    #@37
    const-string/jumbo v12, "postWaitResult() : resultReceiver is null"

    #@3a
    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 1071
    :cond_3d
    :goto_3d
    return-void

    #@3e
    .line 941
    :cond_3e
    const-string v11, ";"

    #@40
    invoke-virtual {p1, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@43
    move-result-object v1

    #@44
    .line 942
    .local v1, component:[Ljava/lang/String;
    if-nez v1, :cond_4f

    #@46
    .line 943
    const-string v11, "DrmObjSes"

    #@48
    const-string/jumbo v12, "postWaitResult() : Fail to devide package;class"

    #@4b
    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    goto :goto_3d

    #@4f
    .line 947
    :cond_4f
    if-nez p4, :cond_5a

    #@51
    .line 949
    const-string v11, "DrmObjSes"

    #@53
    const-string/jumbo v12, "postWaitResult() : Filename is null"

    #@56
    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    goto :goto_3d

    #@5a
    .line 953
    :cond_5a
    const/4 v2, 0x0

    #@5b
    .line 955
    .local v2, content:Lcom/lge/lgdrm/DrmContent;
    :try_start_5b
    iget-object v11, p0, Lcom/lge/lgdrm/DrmObjectSession;->context:Landroid/content/Context;

    #@5d
    move-object/from16 v0, p4

    #@5f
    invoke-static {v0, v11}, Lcom/lge/lgdrm/DrmManager;->createContentSession(Ljava/lang/String;Landroid/content/Context;)Lcom/lge/lgdrm/DrmContentSession;

    #@62
    move-result-object v10

    #@63
    .line 956
    .local v10, session:Lcom/lge/lgdrm/DrmContentSession;
    if-nez v10, :cond_89

    #@65
    .line 957
    const-string v11, "DrmObjSes"

    #@67
    const-string v12, "createContentSession() : Fail"

    #@69
    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6c
    .catch Ljava/lang/Exception; {:try_start_5b .. :try_end_6c} :catch_6d

    #@6c
    goto :goto_3d

    #@6d
    .line 961
    .end local v10           #session:Lcom/lge/lgdrm/DrmContentSession;
    :catch_6d
    move-exception v6

    #@6e
    .line 962
    .local v6, e:Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    #@71
    .line 965
    .end local v6           #e:Ljava/lang/Exception;
    :goto_71
    if-eqz v2, :cond_3d

    #@73
    .line 970
    new-instance v11, Ljava/io/File;

    #@75
    move-object/from16 v0, p4

    #@77
    invoke-direct {v11, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@7a
    invoke-static {v11}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    #@7d
    move-result-object v9

    #@7e
    .line 971
    .local v9, path:Landroid/net/Uri;
    if-nez v9, :cond_8f

    #@80
    .line 972
    const-string v11, "DrmObjSes"

    #@82
    const-string/jumbo v12, "postWaitResult() : Fail to make URI"

    #@85
    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@88
    goto :goto_3d

    #@89
    .line 960
    .end local v9           #path:Landroid/net/Uri;
    .restart local v10       #session:Lcom/lge/lgdrm/DrmContentSession;
    :cond_89
    const/4 v11, 0x1

    #@8a
    :try_start_8a
    invoke-virtual {v10, v11}, Lcom/lge/lgdrm/DrmContentSession;->getSelectedContent(Z)Lcom/lge/lgdrm/DrmContent;
    :try_end_8d
    .catch Ljava/lang/Exception; {:try_start_8a .. :try_end_8d} :catch_6d

    #@8d
    move-result-object v2

    #@8e
    goto :goto_71

    #@8f
    .line 977
    .end local v10           #session:Lcom/lge/lgdrm/DrmContentSession;
    .restart local v9       #path:Landroid/net/Uri;
    :cond_8f
    iget-object v11, p0, Lcom/lge/lgdrm/DrmObjectSession;->context:Landroid/content/Context;

    #@91
    new-instance v12, Landroid/content/Intent;

    #@93
    const-string v13, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    #@95
    invoke-direct {v12, v13, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@98
    invoke-virtual {v11, v12}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@9b
    .line 979
    const/4 v11, 0x0

    #@9c
    aget-object v11, v1, v11

    #@9e
    const-string v12, "com.android.providers.downloads"

    #@a0
    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@a3
    move-result v3

    #@a4
    .line 982
    .local v3, downloadProvider:Z
    const/4 v11, 0x2

    #@a5
    move/from16 v0, p3

    #@a7
    if-eq v0, v11, :cond_ab

    #@a9
    if-eqz v3, :cond_f0

    #@ab
    .line 984
    :cond_ab
    new-instance v7, Landroid/content/Intent;

    #@ad
    const-string v11, "com.lge.lgdrm.action.DRM_PROCESS_RESULT"

    #@af
    invoke-direct {v7, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@b2
    .line 985
    .local v7, intent:Landroid/content/Intent;
    if-nez v7, :cond_bd

    #@b4
    .line 986
    const-string v11, "DrmObjSes"

    #@b6
    const-string/jumbo v12, "postWaitResult() : Fail to new intent"

    #@b9
    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@bc
    goto :goto_3d

    #@bd
    .line 989
    :cond_bd
    const-string v11, "DrmObjSes"

    #@bf
    const-string/jumbo v12, "postWaitResult() : Intent (action wait result)"

    #@c2
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c5
    .line 991
    const-string v11, "com.lge.lgdrm.extra.WAIT_RESULT"

    #@c7
    move/from16 v0, p2

    #@c9
    invoke-virtual {v7, v11, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@cc
    .line 992
    const-string v11, "com.lge.lgdrm.extra.WAIT_TYPE"

    #@ce
    move/from16 v0, p3

    #@d0
    invoke-virtual {v7, v11, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@d3
    .line 993
    const-string v11, "com.lge.lgdrm.extra.FILE_NAME"

    #@d5
    move-object/from16 v0, p4

    #@d7
    invoke-virtual {v7, v11, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@da
    move-object v8, v7

    #@db
    .line 1043
    .end local v7           #intent:Landroid/content/Intent;
    .local v8, intent:Landroid/content/Intent;
    :goto_db
    if-nez v3, :cond_e2

    #@dd
    const/4 v11, 0x2

    #@de
    move/from16 v0, p3

    #@e0
    if-ne v0, v11, :cond_14c

    #@e2
    .line 1044
    :cond_e2
    :try_start_e2
    iget-object v11, p0, Lcom/lge/lgdrm/DrmObjectSession;->context:Landroid/content/Context;

    #@e4
    invoke-virtual {v11, v8}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_e7
    .catch Ljava/lang/Exception; {:try_start_e2 .. :try_end_e7} :catch_e9

    #@e7
    goto/16 :goto_3d

    #@e9
    .line 1067
    :catch_e9
    move-exception v6

    #@ea
    move-object v7, v8

    #@eb
    .line 1069
    .end local v8           #intent:Landroid/content/Intent;
    .restart local v6       #e:Ljava/lang/Exception;
    .restart local v7       #intent:Landroid/content/Intent;
    :goto_eb
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    #@ee
    goto/16 :goto_3d

    #@f0
    .line 996
    .end local v6           #e:Ljava/lang/Exception;
    .end local v7           #intent:Landroid/content/Intent;
    :cond_f0
    new-instance v7, Landroid/content/Intent;

    #@f2
    const-string v11, "android.intent.action.VIEW"

    #@f4
    invoke-direct {v7, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@f7
    .line 997
    .restart local v7       #intent:Landroid/content/Intent;
    if-nez v7, :cond_103

    #@f9
    .line 998
    const-string v11, "DrmObjSes"

    #@fb
    const-string/jumbo v12, "postWaitResult() : Fail to new intent"

    #@fe
    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@101
    goto/16 :goto_3d

    #@103
    .line 1001
    :cond_103
    const-string v11, "DrmObjSes"

    #@105
    const-string/jumbo v12, "postWaitResult() : Intent (action view)"

    #@108
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10b
    .line 1003
    const/4 v4, 0x0

    #@10c
    .line 1006
    .local v4, drmMime:Ljava/lang/String;
    invoke-virtual {v2}, Lcom/lge/lgdrm/DrmContent;->getDrmContentType()I

    #@10f
    move-result v5

    #@110
    .line 1007
    .local v5, drmType:I
    and-int/lit8 v11, v5, 0x10

    #@112
    if-eqz v11, :cond_129

    #@114
    .line 1008
    const-string v4, "application/vnd.oma.drm.message"

    #@116
    .line 1018
    :goto_116
    invoke-virtual {v7, v9, v4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    #@119
    .line 1020
    invoke-virtual {v2}, Lcom/lge/lgdrm/DrmContent;->getContentType()I

    #@11c
    move-result v11

    #@11d
    packed-switch v11, :pswitch_data_18e

    #@120
    goto/16 :goto_3d

    #@122
    .line 1022
    :pswitch_122
    const-string v11, "com.lge.lgdrm.IMAGE_CATEGORY"

    #@124
    invoke-virtual {v7, v11}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    #@127
    move-object v8, v7

    #@128
    .line 1023
    .end local v7           #intent:Landroid/content/Intent;
    .restart local v8       #intent:Landroid/content/Intent;
    goto :goto_db

    #@129
    .line 1009
    .end local v8           #intent:Landroid/content/Intent;
    .restart local v7       #intent:Landroid/content/Intent;
    :cond_129
    and-int/lit16 v11, v5, 0x100

    #@12b
    if-eqz v11, :cond_130

    #@12d
    .line 1010
    const-string v4, "application/vnd.oma.drm.content"

    #@12f
    goto :goto_116

    #@130
    .line 1011
    :cond_130
    and-int/lit16 v11, v5, 0x1000

    #@132
    if-eqz v11, :cond_3d

    #@134
    .line 1012
    const-string v4, "application/vnd.oma.drm.dcf"

    #@136
    goto :goto_116

    #@137
    .line 1025
    :pswitch_137
    const-string v11, "com.lge.lgdrm.AUDIO_CATEGORY"

    #@139
    invoke-virtual {v7, v11}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    #@13c
    move-object v8, v7

    #@13d
    .line 1026
    .end local v7           #intent:Landroid/content/Intent;
    .restart local v8       #intent:Landroid/content/Intent;
    goto :goto_db

    #@13e
    .line 1028
    .end local v8           #intent:Landroid/content/Intent;
    .restart local v7       #intent:Landroid/content/Intent;
    :pswitch_13e
    const-string v11, "com.lge.lgdrm.VIDEO_CATEGORY"

    #@140
    invoke-virtual {v7, v11}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    #@143
    move-object v8, v7

    #@144
    .line 1029
    .end local v7           #intent:Landroid/content/Intent;
    .restart local v8       #intent:Landroid/content/Intent;
    goto :goto_db

    #@145
    .line 1031
    .end local v8           #intent:Landroid/content/Intent;
    .restart local v7       #intent:Landroid/content/Intent;
    :pswitch_145
    const-string v11, "com.lge.lgdrm.GAME_CATEGORY"

    #@147
    invoke-virtual {v7, v11}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    #@14a
    move-object v8, v7

    #@14b
    .line 1032
    .end local v7           #intent:Landroid/content/Intent;
    .restart local v8       #intent:Landroid/content/Intent;
    goto :goto_db

    #@14c
    .line 1049
    .end local v4           #drmMime:Ljava/lang/String;
    .end local v5           #drmType:I
    :cond_14c
    :try_start_14c
    invoke-virtual {v2}, Lcom/lge/lgdrm/DrmContent;->getDrmContentType()I

    #@14f
    move-result v5

    #@150
    .line 1050
    .restart local v5       #drmType:I
    and-int/lit16 v11, v5, 0x100

    #@152
    if-eqz v11, :cond_176

    #@154
    .line 1052
    new-instance v7, Landroid/content/Intent;

    #@156
    const-string v11, "com.lge.lgdrm.action.RIGHTS_RECEIVED"

    #@158
    invoke-direct {v7, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V
    :try_end_15b
    .catch Ljava/lang/Exception; {:try_start_14c .. :try_end_15b} :catch_e9

    #@15b
    .line 1053
    .end local v8           #intent:Landroid/content/Intent;
    .restart local v7       #intent:Landroid/content/Intent;
    :try_start_15b
    const-string v11, "com.lge.lgdrm.extra.FILE_NAME"

    #@15d
    move-object/from16 v0, p4

    #@15f
    invoke-virtual {v7, v11, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@162
    .line 1054
    const-string v11, "com.lge.lgdrm.extra.CID"

    #@164
    const/4 v12, 0x3

    #@165
    invoke-virtual {v2, v12}, Lcom/lge/lgdrm/DrmContent;->getContentInfo(I)Ljava/lang/String;

    #@168
    move-result-object v12

    #@169
    invoke-virtual {v7, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@16c
    .line 1055
    iget-object v11, p0, Lcom/lge/lgdrm/DrmObjectSession;->context:Landroid/content/Context;

    #@16e
    invoke-virtual {v11, v7}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_171
    .catch Ljava/lang/Exception; {:try_start_15b .. :try_end_171} :catch_173

    #@171
    goto/16 :goto_3d

    #@173
    .line 1067
    :catch_173
    move-exception v6

    #@174
    goto/16 :goto_eb

    #@176
    .line 1059
    .end local v7           #intent:Landroid/content/Intent;
    .restart local v8       #intent:Landroid/content/Intent;
    :cond_176
    :try_start_176
    iget v11, p0, Lcom/lge/lgdrm/DrmObjectSession;->downloadAgent:I

    #@178
    const/4 v12, 0x3

    #@179
    if-eq v11, v12, :cond_3d

    #@17b
    iget v11, p0, Lcom/lge/lgdrm/DrmObjectSession;->downloadAgent:I

    #@17d
    const/4 v12, 0x4

    #@17e
    if-eq v11, v12, :cond_3d

    #@180
    .line 1065
    const/high16 v11, 0x1000

    #@182
    invoke-virtual {v8, v11}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@185
    .line 1066
    iget-object v11, p0, Lcom/lge/lgdrm/DrmObjectSession;->context:Landroid/content/Context;

    #@187
    invoke-virtual {v11, v8}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_18a
    .catch Ljava/lang/Exception; {:try_start_176 .. :try_end_18a} :catch_e9

    #@18a
    move-object v7, v8

    #@18b
    .line 1070
    .end local v8           #intent:Landroid/content/Intent;
    .restart local v7       #intent:Landroid/content/Intent;
    goto/16 :goto_3d

    #@18d
    .line 1020
    nop

    #@18e
    :pswitch_data_18e
    .packed-switch 0x1
        :pswitch_122
        :pswitch_137
        :pswitch_13e
        :pswitch_145
    .end packed-switch
.end method

.method private setFailInfo(ILjava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "failReason"
    .parameter "errorMsg"
    .parameter "redirectURL"

    #@0
    .prologue
    .line 826
    iput p1, p0, Lcom/lge/lgdrm/DrmObjectSession;->failReason:I

    #@2
    .line 827
    iput-object p2, p0, Lcom/lge/lgdrm/DrmObjectSession;->errorMsg:Ljava/lang/String;

    #@4
    .line 828
    iput-object p3, p0, Lcom/lge/lgdrm/DrmObjectSession;->redirectURL:Ljava/lang/String;

    #@6
    .line 829
    return-void
.end method

.method private setNextRequest(IIILjava/lang/String;[B)I
    .registers 7
    .parameter "session"
    .parameter "requestType"
    .parameter "httpMethod"
    .parameter "url"
    .parameter "data"

    #@0
    .prologue
    .line 857
    iput p1, p0, Lcom/lge/lgdrm/DrmObjectSession;->nativeSession:I

    #@2
    .line 860
    new-instance v0, Lcom/lge/lgdrm/DrmDldRequest;

    #@4
    invoke-direct {v0}, Lcom/lge/lgdrm/DrmDldRequest;-><init>()V

    #@7
    iput-object v0, p0, Lcom/lge/lgdrm/DrmObjectSession;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

    #@9
    .line 861
    iget-object v0, p0, Lcom/lge/lgdrm/DrmObjectSession;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

    #@b
    if-nez v0, :cond_f

    #@d
    .line 862
    const/4 v0, -0x1

    #@e
    .line 870
    :goto_e
    return v0

    #@f
    .line 865
    :cond_f
    iget-object v0, p0, Lcom/lge/lgdrm/DrmObjectSession;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

    #@11
    iput p2, v0, Lcom/lge/lgdrm/DrmDldRequest;->requestType:I

    #@13
    .line 866
    iget-object v0, p0, Lcom/lge/lgdrm/DrmObjectSession;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

    #@15
    iput p3, v0, Lcom/lge/lgdrm/DrmDldRequest;->httpMethod:I

    #@17
    .line 867
    iget-object v0, p0, Lcom/lge/lgdrm/DrmObjectSession;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

    #@19
    iput-object p4, v0, Lcom/lge/lgdrm/DrmDldRequest;->url:Ljava/lang/String;

    #@1b
    .line 868
    iget-object v0, p0, Lcom/lge/lgdrm/DrmObjectSession;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

    #@1d
    iput-object p5, v0, Lcom/lge/lgdrm/DrmDldRequest;->data:[B

    #@1f
    .line 870
    const/4 v0, 0x0

    #@20
    goto :goto_e
.end method

.method private setStoredFilename(Ljava/lang/String;)V
    .registers 2
    .parameter "filename"

    #@0
    .prologue
    .line 837
    iput-object p1, p0, Lcom/lge/lgdrm/DrmObjectSession;->storedFilename:Ljava/lang/String;

    #@2
    .line 838
    return-void
.end method

.method private startDldClient()I
    .registers 9

    #@0
    .prologue
    const/4 v4, -0x1

    #@1
    const/4 v3, 0x3

    #@2
    const/4 v5, 0x0

    #@3
    .line 884
    iget v6, p0, Lcom/lge/lgdrm/DrmObjectSession;->nativeSLType:I

    #@5
    const/4 v7, 0x2

    #@6
    if-ne v6, v7, :cond_19

    #@8
    move v1, v3

    #@9
    .line 886
    .local v1, downloadAgent:I
    :goto_9
    new-instance v2, Lcom/lge/lgdrm/DrmObjectSession;

    #@b
    iget-object v6, p0, Lcom/lge/lgdrm/DrmObjectSession;->context:Landroid/content/Context;

    #@d
    iget v7, p0, Lcom/lge/lgdrm/DrmObjectSession;->nativeSession:I

    #@f
    invoke-direct {v2, v1, v6, v7}, Lcom/lge/lgdrm/DrmObjectSession;-><init>(ILandroid/content/Context;I)V

    #@12
    .line 887
    .local v2, session:Lcom/lge/lgdrm/DrmObjectSession;
    if-nez v2, :cond_1b

    #@14
    .line 889
    invoke-virtual {p0, v3}, Lcom/lge/lgdrm/DrmObjectSession;->destroySession(I)V

    #@17
    move v3, v4

    #@18
    .line 914
    :goto_18
    return v3

    #@19
    .line 884
    .end local v1           #downloadAgent:I
    .end local v2           #session:Lcom/lge/lgdrm/DrmObjectSession;
    :cond_19
    const/4 v1, 0x4

    #@1a
    goto :goto_9

    #@1b
    .line 897
    .restart local v1       #downloadAgent:I
    .restart local v2       #session:Lcom/lge/lgdrm/DrmObjectSession;
    :cond_1b
    new-instance v0, Lcom/lge/lgdrm/DrmDldClient;

    #@1d
    iget-object v6, p0, Lcom/lge/lgdrm/DrmObjectSession;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

    #@1f
    iget-object v7, p0, Lcom/lge/lgdrm/DrmObjectSession;->context:Landroid/content/Context;

    #@21
    invoke-direct {v0, v2, v6, v7}, Lcom/lge/lgdrm/DrmDldClient;-><init>(Lcom/lge/lgdrm/DrmObjectSession;Lcom/lge/lgdrm/DrmDldRequest;Landroid/content/Context;)V

    #@24
    .line 898
    .local v0, dldClient:Lcom/lge/lgdrm/DrmDldClient;
    if-nez v0, :cond_2b

    #@26
    .line 900
    invoke-virtual {p0, v3}, Lcom/lge/lgdrm/DrmObjectSession;->destroySession(I)V

    #@29
    move v3, v4

    #@2a
    .line 901
    goto :goto_18

    #@2b
    .line 905
    :cond_2b
    iput v5, p0, Lcom/lge/lgdrm/DrmObjectSession;->nativeSession:I

    #@2d
    .line 906
    const/4 v3, 0x0

    #@2e
    iput-object v3, p0, Lcom/lge/lgdrm/DrmObjectSession;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

    #@30
    .line 909
    invoke-virtual {v0}, Lcom/lge/lgdrm/DrmDldClient;->start()V

    #@33
    .line 912
    iput-boolean v5, p0, Lcom/lge/lgdrm/DrmObjectSession;->validSession:Z

    #@35
    move v3, v5

    #@36
    .line 914
    goto :goto_18
.end method


# virtual methods
.method public destroySession(I)V
    .registers 5
    .parameter "reason"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 332
    if-ltz p1, :cond_6

    #@3
    const/4 v0, 0x3

    #@4
    if-le p1, v0, :cond_e

    #@6
    .line 333
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v1, "Invalid reason"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 336
    :cond_e
    if-nez p1, :cond_11

    #@10
    .line 352
    :goto_10
    return-void

    #@11
    .line 341
    :cond_11
    iput-boolean v2, p0, Lcom/lge/lgdrm/DrmObjectSession;->validSession:Z

    #@13
    .line 342
    iget v0, p0, Lcom/lge/lgdrm/DrmObjectSession;->nativeProcessHandle:I

    #@15
    if-eqz v0, :cond_21

    #@17
    .line 344
    const/4 v0, 0x1

    #@18
    const/4 v1, 0x0

    #@19
    invoke-virtual {p0, v0, v1}, Lcom/lge/lgdrm/DrmObjectSession;->processEnd(ILandroid/content/ComponentName;)I

    #@1c
    .line 350
    :cond_1c
    :goto_1c
    iput v2, p0, Lcom/lge/lgdrm/DrmObjectSession;->nativeSession:I

    #@1e
    .line 351
    iput v2, p0, Lcom/lge/lgdrm/DrmObjectSession;->nativeProcessHandle:I

    #@20
    goto :goto_10

    #@21
    .line 345
    :cond_21
    iget v0, p0, Lcom/lge/lgdrm/DrmObjectSession;->nativeSession:I

    #@23
    if-eqz v0, :cond_1c

    #@25
    .line 347
    iget v0, p0, Lcom/lge/lgdrm/DrmObjectSession;->nativeSession:I

    #@27
    invoke-direct {p0, v0, p1}, Lcom/lge/lgdrm/DrmObjectSession;->nativeDestroySession(II)V

    #@2a
    goto :goto_1c
.end method

.method protected finalize()V
    .registers 3

    #@0
    .prologue
    .line 805
    iget v0, p0, Lcom/lge/lgdrm/DrmObjectSession;->nativeProcessHandle:I

    #@2
    if-eqz v0, :cond_11

    #@4
    .line 807
    const-string v0, "DrmObjSes"

    #@6
    const-string v1, "finalize() : There still exists valid processing handle. Check impl"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 808
    const/4 v0, 0x1

    #@c
    const/4 v1, 0x0

    #@d
    invoke-virtual {p0, v0, v1}, Lcom/lge/lgdrm/DrmObjectSession;->processEnd(ILandroid/content/ComponentName;)I

    #@10
    .line 814
    :cond_10
    :goto_10
    return-void

    #@11
    .line 809
    :cond_11
    iget v0, p0, Lcom/lge/lgdrm/DrmObjectSession;->nativeSession:I

    #@13
    if-eqz v0, :cond_10

    #@15
    .line 811
    const-string v0, "DrmObjSes"

    #@17
    const-string v1, "finalize() : There still exists valid session. Check impl"

    #@19
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 812
    const/4 v0, 0x3

    #@1d
    invoke-virtual {p0, v0}, Lcom/lge/lgdrm/DrmObjectSession;->destroySession(I)V

    #@20
    goto :goto_10
.end method

.method public getFailInfo(I)Ljava/lang/String;
    .registers 4
    .parameter "type"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 722
    iget-boolean v0, p0, Lcom/lge/lgdrm/DrmObjectSession;->bEndError:Z

    #@2
    if-nez v0, :cond_c

    #@4
    .line 723
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "No error was occurred"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 726
    :cond_c
    const/4 v0, 0x1

    #@d
    if-ne p1, v0, :cond_12

    #@f
    .line 727
    iget-object v0, p0, Lcom/lge/lgdrm/DrmObjectSession;->errorMsg:Ljava/lang/String;

    #@11
    .line 729
    :goto_11
    return-object v0

    #@12
    .line 728
    :cond_12
    const/4 v0, 0x2

    #@13
    if-ne p1, v0, :cond_18

    #@15
    .line 729
    iget-object v0, p0, Lcom/lge/lgdrm/DrmObjectSession;->redirectURL:Ljava/lang/String;

    #@17
    goto :goto_11

    #@18
    .line 731
    :cond_18
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1a
    const-string v1, "Invalid type"

    #@1c
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0
.end method

.method public getFailReason()I
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 700
    iget-boolean v0, p0, Lcom/lge/lgdrm/DrmObjectSession;->bEndError:Z

    #@2
    if-nez v0, :cond_c

    #@4
    .line 701
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "No error was occurred"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 703
    :cond_c
    iget v0, p0, Lcom/lge/lgdrm/DrmObjectSession;->failReason:I

    #@e
    return v0
.end method

.method public getNextRequest()Lcom/lge/lgdrm/DrmDldRequest;
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 754
    iget-object v0, p0, Lcom/lge/lgdrm/DrmObjectSession;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

    #@2
    .line 756
    .local v0, request:Lcom/lge/lgdrm/DrmDldRequest;
    iget-boolean v1, p0, Lcom/lge/lgdrm/DrmObjectSession;->validSession:Z

    #@4
    if-nez v1, :cond_e

    #@6
    .line 757
    new-instance v1, Ljava/lang/IllegalStateException;

    #@8
    const-string v2, "Session is invalid"

    #@a
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v1

    #@e
    .line 759
    :cond_e
    if-nez v0, :cond_18

    #@10
    .line 760
    new-instance v1, Ljava/lang/IllegalStateException;

    #@12
    const-string v2, "Not exist next message"

    #@14
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@17
    throw v1

    #@18
    .line 764
    :cond_18
    const/4 v1, 0x0

    #@19
    iput-object v1, p0, Lcom/lge/lgdrm/DrmObjectSession;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

    #@1b
    .line 765
    return-object v0
.end method

.method public getStoredFilename()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 741
    iget-object v0, p0, Lcom/lge/lgdrm/DrmObjectSession;->storedFilename:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public isValidSession()Z
    .registers 2

    #@0
    .prologue
    .line 315
    iget-boolean v0, p0, Lcom/lge/lgdrm/DrmObjectSession;->validSession:Z

    #@2
    return v0
.end method

.method public processEnd(ILandroid/content/ComponentName;)I
    .registers 9
    .parameter "userResponse"
    .parameter "resultReceiver"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 620
    const/4 v0, 0x0

    #@3
    .line 622
    .local v0, intentReceiver:Ljava/lang/String;
    iget v2, p0, Lcom/lge/lgdrm/DrmObjectSession;->nativeProcessHandle:I

    #@5
    if-nez v2, :cond_f

    #@7
    .line 623
    new-instance v2, Ljava/lang/IllegalStateException;

    #@9
    const-string v3, "Init was not called successfully"

    #@b
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@e
    throw v2

    #@f
    .line 625
    :cond_f
    if-lt p1, v4, :cond_14

    #@11
    const/4 v2, 0x3

    #@12
    if-le p1, v2, :cond_1c

    #@14
    .line 626
    :cond_14
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@16
    const-string v3, "Invalid userResponse"

    #@18
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v2

    #@1c
    .line 628
    :cond_1c
    iget-boolean v2, p0, Lcom/lge/lgdrm/DrmObjectSession;->validSession:Z

    #@1e
    if-nez v2, :cond_2a

    #@20
    if-eq p1, v4, :cond_2a

    #@22
    .line 630
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@24
    const-string v3, "Invalid userResponse. Only permit PROC_RESP_CANCEL"

    #@26
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@29
    throw v2

    #@2a
    .line 632
    :cond_2a
    if-eqz p2, :cond_4f

    #@2c
    .line 643
    iget-boolean v2, p0, Lcom/lge/lgdrm/DrmObjectSession;->waitFlag:Z

    #@2e
    if-eqz v2, :cond_5f

    #@30
    .line 644
    new-instance v2, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    invoke-virtual {p2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    const-string v3, ";"

    #@3f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    invoke-virtual {p2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@46
    move-result-object v3

    #@47
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v0

    #@4f
    .line 654
    :cond_4f
    :goto_4f
    iget v2, p0, Lcom/lge/lgdrm/DrmObjectSession;->nativeProcessHandle:I

    #@51
    invoke-direct {p0, v2, p1, v0}, Lcom/lge/lgdrm/DrmObjectSession;->nativeProcessEnd(IILjava/lang/String;)I

    #@54
    move-result v1

    #@55
    .line 657
    .local v1, retVal:I
    iput v5, p0, Lcom/lge/lgdrm/DrmObjectSession;->nativeProcessHandle:I

    #@57
    .line 658
    iput-boolean v5, p0, Lcom/lge/lgdrm/DrmObjectSession;->waitFlag:Z

    #@59
    .line 660
    const/4 v2, -0x1

    #@5a
    if-ne v1, v2, :cond_68

    #@5c
    .line 661
    iput-boolean v4, p0, Lcom/lge/lgdrm/DrmObjectSession;->bEndError:Z

    #@5e
    .line 672
    :cond_5e
    :goto_5e
    return v1

    #@5f
    .line 646
    .end local v1           #retVal:I
    :cond_5f
    const-string v2, "DrmObjSes"

    #@61
    const-string/jumbo v3, "processEnd() : Invalid resultReceiver. Setup only based on processStatus()"

    #@64
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    goto :goto_4f

    #@68
    .line 662
    .restart local v1       #retVal:I
    :cond_68
    const/4 v2, 0x4

    #@69
    if-ne v1, v2, :cond_5e

    #@6b
    .line 663
    iget v2, p0, Lcom/lge/lgdrm/DrmObjectSession;->downloadAgent:I

    #@6d
    const/4 v3, 0x6

    #@6e
    if-eq v2, v3, :cond_75

    #@70
    iget v2, p0, Lcom/lge/lgdrm/DrmObjectSession;->downloadAgent:I

    #@72
    const/4 v3, 0x2

    #@73
    if-ne v2, v3, :cond_5e

    #@75
    .line 665
    :cond_75
    const/4 v1, 0x0

    #@76
    .line 666
    invoke-direct {p0}, Lcom/lge/lgdrm/DrmObjectSession;->startDldClient()I

    #@79
    move-result v2

    #@7a
    if-eqz v2, :cond_5e

    #@7c
    .line 667
    const/4 v1, -0x1

    #@7d
    goto :goto_5e
.end method

.method public processInit(Ljava/lang/String;Ljava/lang/String;I)I
    .registers 12
    .parameter "drmMimeType"
    .parameter "filename"
    .parameter "attribute"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 401
    const/4 v6, 0x0

    #@2
    .line 403
    .local v6, nativeHnds:[I
    iget-boolean v0, p0, Lcom/lge/lgdrm/DrmObjectSession;->validSession:Z

    #@4
    if-nez v0, :cond_e

    #@6
    .line 404
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    const-string v1, "Session is invalid"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 407
    :cond_e
    iget v1, p0, Lcom/lge/lgdrm/DrmObjectSession;->nativeSession:I

    #@10
    iget v5, p0, Lcom/lge/lgdrm/DrmObjectSession;->downloadAgent:I

    #@12
    move-object v0, p0

    #@13
    move-object v2, p1

    #@14
    move-object v3, p2

    #@15
    move v4, p3

    #@16
    invoke-direct/range {v0 .. v5}, Lcom/lge/lgdrm/DrmObjectSession;->nativeProcessInit(ILjava/lang/String;Ljava/lang/String;II)[I

    #@19
    move-result-object v6

    #@1a
    .line 408
    if-eqz v6, :cond_2d

    #@1c
    aget v0, v6, v7

    #@1e
    if-eqz v0, :cond_2d

    #@20
    .line 413
    iput v7, p0, Lcom/lge/lgdrm/DrmObjectSession;->nativeSession:I

    #@22
    .line 416
    aget v0, v6, v7

    #@24
    iput v0, p0, Lcom/lge/lgdrm/DrmObjectSession;->nativeProcessHandle:I

    #@26
    .line 417
    const/4 v0, 0x1

    #@27
    aget v0, v6, v0

    #@29
    iput v0, p0, Lcom/lge/lgdrm/DrmObjectSession;->nativeSLType:I

    #@2b
    move v0, v7

    #@2c
    .line 421
    :goto_2c
    return v0

    #@2d
    :cond_2d
    const/4 v0, -0x1

    #@2e
    goto :goto_2c
.end method

.method public processStatus()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 526
    iget-boolean v1, p0, Lcom/lge/lgdrm/DrmObjectSession;->validSession:Z

    #@4
    if-nez v1, :cond_e

    #@6
    .line 527
    new-instance v1, Ljava/lang/IllegalStateException;

    #@8
    const-string v2, "Session is invalid"

    #@a
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v1

    #@e
    .line 529
    :cond_e
    iget v1, p0, Lcom/lge/lgdrm/DrmObjectSession;->nativeProcessHandle:I

    #@10
    if-nez v1, :cond_1a

    #@12
    .line 530
    new-instance v1, Ljava/lang/IllegalStateException;

    #@14
    const-string v2, "Init was not called successfully"

    #@16
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@19
    throw v1

    #@1a
    .line 533
    :cond_1a
    iget v1, p0, Lcom/lge/lgdrm/DrmObjectSession;->nativeProcessHandle:I

    #@1c
    invoke-direct {p0, v1}, Lcom/lge/lgdrm/DrmObjectSession;->nativeProcessStatus(I)I

    #@1f
    move-result v0

    #@20
    .line 534
    .local v0, retVal:I
    const/4 v1, -0x1

    #@21
    if-ne v0, v1, :cond_25

    #@23
    .line 535
    iput-boolean v2, p0, Lcom/lge/lgdrm/DrmObjectSession;->validSession:Z

    #@25
    .line 538
    :cond_25
    if-eq v0, v3, :cond_2a

    #@27
    const/4 v1, 0x3

    #@28
    if-ne v0, v1, :cond_2d

    #@2a
    .line 539
    :cond_2a
    iput-boolean v3, p0, Lcom/lge/lgdrm/DrmObjectSession;->waitFlag:Z

    #@2c
    .line 544
    :goto_2c
    return v0

    #@2d
    .line 541
    :cond_2d
    iput-boolean v2, p0, Lcom/lge/lgdrm/DrmObjectSession;->waitFlag:Z

    #@2f
    goto :goto_2c
.end method

.method public processUpdate([BI)I
    .registers 4
    .parameter "buf"
    .parameter "length"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/NullPointerException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 445
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0, p2}, Lcom/lge/lgdrm/DrmObjectSession;->processUpdate([BII)I

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public processUpdate([BII)I
    .registers 7
    .parameter "buf"
    .parameter "offset"
    .parameter "length"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/NullPointerException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 473
    iget-boolean v1, p0, Lcom/lge/lgdrm/DrmObjectSession;->validSession:Z

    #@2
    if-nez v1, :cond_c

    #@4
    .line 474
    new-instance v1, Ljava/lang/IllegalStateException;

    #@6
    const-string v2, "Session is invalid"

    #@8
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v1

    #@c
    .line 476
    :cond_c
    iget v1, p0, Lcom/lge/lgdrm/DrmObjectSession;->nativeProcessHandle:I

    #@e
    if-nez v1, :cond_18

    #@10
    .line 477
    new-instance v1, Ljava/lang/IllegalStateException;

    #@12
    const-string v2, "Init was not called successfully"

    #@14
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@17
    throw v1

    #@18
    .line 479
    :cond_18
    if-nez p1, :cond_22

    #@1a
    .line 480
    new-instance v1, Ljava/lang/NullPointerException;

    #@1c
    const-string v2, "Parameter buf is null"

    #@1e
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@21
    throw v1

    #@22
    .line 482
    :cond_22
    if-ltz p3, :cond_27

    #@24
    array-length v1, p1

    #@25
    if-le p3, v1, :cond_2f

    #@27
    .line 483
    :cond_27
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@29
    const-string v2, "Invalid length"

    #@2b
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2e
    throw v1

    #@2f
    .line 486
    :cond_2f
    iget v1, p0, Lcom/lge/lgdrm/DrmObjectSession;->nativeProcessHandle:I

    #@31
    invoke-direct {p0, v1, p1, p2, p3}, Lcom/lge/lgdrm/DrmObjectSession;->nativeProcessUpdate(I[BII)I

    #@34
    move-result v0

    #@35
    .line 487
    .local v0, retVal:I
    if-eqz v0, :cond_3a

    #@37
    .line 488
    const/4 v1, 0x0

    #@38
    iput-boolean v1, p0, Lcom/lge/lgdrm/DrmObjectSession;->validSession:Z

    #@3a
    .line 491
    :cond_3a
    return v0
.end method

.method public setContentSize(J)I
    .registers 5
    .parameter "size"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 784
    iget-boolean v0, p0, Lcom/lge/lgdrm/DrmObjectSession;->validSession:Z

    #@2
    if-nez v0, :cond_c

    #@4
    .line 785
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "Session is invalid"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 787
    :cond_c
    iget v0, p0, Lcom/lge/lgdrm/DrmObjectSession;->nativeProcessHandle:I

    #@e
    if-nez v0, :cond_18

    #@10
    .line 788
    new-instance v0, Ljava/lang/IllegalStateException;

    #@12
    const-string v1, "Init was not called successfully"

    #@14
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 790
    :cond_18
    const-wide/16 v0, 0x0

    #@1a
    cmp-long v0, p1, v0

    #@1c
    if-gtz v0, :cond_26

    #@1e
    .line 791
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@20
    const-string v1, "Invalid size"

    #@22
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@25
    throw v0

    #@26
    .line 794
    :cond_26
    iget v0, p0, Lcom/lge/lgdrm/DrmObjectSession;->nativeSLType:I

    #@28
    const/4 v1, 0x3

    #@29
    if-eq v0, v1, :cond_2d

    #@2b
    .line 795
    const/4 v0, 0x0

    #@2c
    .line 799
    :goto_2c
    return v0

    #@2d
    :cond_2d
    iget v0, p0, Lcom/lge/lgdrm/DrmObjectSession;->nativeProcessHandle:I

    #@2f
    invoke-direct {p0, v0, p1, p2}, Lcom/lge/lgdrm/DrmObjectSession;->nativeSetContentSize(IJ)I

    #@32
    move-result v0

    #@33
    goto :goto_2c
.end method
