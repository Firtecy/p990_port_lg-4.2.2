.class public final Lcom/lge/lgdrm/DrmManager;
.super Ljava/lang/Object;
.source "DrmManager.java"


# static fields
.field private static sSLExtMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 102
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    sput-object v0, Lcom/lge/lgdrm/DrmManager;->sSLExtMap:Ljava/util/HashMap;

    #@7
    .line 664
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 107
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 108
    return-void
.end method

.method public static createContentManager(Ljava/lang/String;)Lcom/lge/lgdrm/DrmContentManager;
    .registers 4
    .parameter "filename"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lge/lgdrm/DrmException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    #@0
    .prologue
    .line 207
    sget-boolean v1, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@2
    if-nez v1, :cond_6

    #@4
    .line 208
    const/4 v1, 0x0

    #@5
    .line 220
    :goto_5
    return-object v1

    #@6
    .line 211
    :cond_6
    if-nez p0, :cond_10

    #@8
    .line 212
    new-instance v1, Ljava/lang/NullPointerException;

    #@a
    const-string v2, "Parameter filename is null"

    #@c
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@f
    throw v1

    #@10
    .line 215
    :cond_10
    invoke-static {p0}, Lcom/lge/lgdrm/DrmManager;->nativeIsDRM(Ljava/lang/String;)I

    #@13
    move-result v0

    #@14
    .line 216
    .local v0, contentType:I
    if-nez v0, :cond_1e

    #@16
    .line 217
    new-instance v1, Lcom/lge/lgdrm/DrmException;

    #@18
    const-string v2, "Not DRM protected content"

    #@1a
    invoke-direct {v1, v2}, Lcom/lge/lgdrm/DrmException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v1

    #@1e
    .line 220
    :cond_1e
    new-instance v1, Lcom/lge/lgdrm/DrmContentManager;

    #@20
    invoke-direct {v1, p0, v0}, Lcom/lge/lgdrm/DrmContentManager;-><init>(Ljava/lang/String;I)V

    #@23
    goto :goto_5
.end method

.method public static createContentSession(Ljava/lang/String;Landroid/content/Context;)Lcom/lge/lgdrm/DrmContentSession;
    .registers 6
    .parameter "filename"
    .parameter "context"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lge/lgdrm/DrmException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    .line 171
    sget-boolean v2, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@4
    if-nez v2, :cond_7

    #@6
    .line 188
    :cond_6
    :goto_6
    return-object v1

    #@7
    .line 175
    :cond_7
    if-nez p0, :cond_11

    #@9
    .line 176
    new-instance v1, Ljava/lang/NullPointerException;

    #@b
    const-string v2, "Parameter filename is null"

    #@d
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@10
    throw v1

    #@11
    .line 179
    :cond_11
    new-instance v0, Lcom/lge/lgdrm/DrmContent;

    #@13
    invoke-direct {v0, p0, v3, v3}, Lcom/lge/lgdrm/DrmContent;-><init>(Ljava/lang/String;II)V

    #@16
    .line 180
    .local v0, content:Lcom/lge/lgdrm/DrmContent;
    if-eqz v0, :cond_6

    #@18
    .line 184
    invoke-static {v0, p0, v3}, Lcom/lge/lgdrm/DrmManager;->nativeGetContentBasicInfo(Lcom/lge/lgdrm/DrmContent;Ljava/lang/String;I)I

    #@1b
    move-result v2

    #@1c
    if-nez v2, :cond_6

    #@1e
    .line 188
    new-instance v1, Lcom/lge/lgdrm/DrmContentSession;

    #@20
    invoke-direct {v1, v0, p1}, Lcom/lge/lgdrm/DrmContentSession;-><init>(Lcom/lge/lgdrm/DrmContent;Landroid/content/Context;)V

    #@23
    goto :goto_6
.end method

.method public static createObjectSession(ILandroid/content/Context;)Lcom/lge/lgdrm/DrmObjectSession;
    .registers 4
    .parameter "downloadAgent"
    .parameter "context"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 344
    sget-boolean v0, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@2
    if-nez v0, :cond_6

    #@4
    .line 345
    const/4 v0, 0x0

    #@5
    .line 355
    :goto_5
    return-object v0

    #@6
    .line 348
    :cond_6
    if-nez p1, :cond_10

    #@8
    .line 349
    new-instance v0, Ljava/lang/NullPointerException;

    #@a
    const-string v1, "context is null"

    #@c
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 351
    :cond_10
    const/4 v0, 0x1

    #@11
    if-lt p0, v0, :cond_16

    #@13
    const/4 v0, 0x6

    #@14
    if-le p0, v0, :cond_1e

    #@16
    .line 352
    :cond_16
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@18
    const-string v1, "Invalid appType"

    #@1a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v0

    #@1e
    .line 355
    :cond_1e
    new-instance v0, Lcom/lge/lgdrm/DrmObjectSession;

    #@20
    const/4 v1, 0x0

    #@21
    invoke-direct {v0, p0, p1, v1}, Lcom/lge/lgdrm/DrmObjectSession;-><init>(ILandroid/content/Context;I)V

    #@24
    goto :goto_5
.end method

.method public static getAgentInformation(I)Ljava/lang/String;
    .registers 3
    .parameter "type"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 483
    sget-boolean v0, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@2
    if-nez v0, :cond_6

    #@4
    .line 484
    const/4 v0, 0x0

    #@5
    .line 494
    :goto_5
    return-object v0

    #@6
    .line 487
    :cond_6
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_14

    #@c
    .line 488
    new-instance v0, Ljava/lang/SecurityException;

    #@e
    const-string v1, "Need proper permission to access drm"

    #@10
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 490
    :cond_14
    if-ltz p0, :cond_19

    #@16
    const/4 v0, 0x2

    #@17
    if-le p0, v0, :cond_21

    #@19
    .line 491
    :cond_19
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1b
    const-string v1, "Invalid type"

    #@1d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@20
    throw v0

    #@21
    .line 494
    :cond_21
    invoke-static {p0}, Lcom/lge/lgdrm/DrmManager;->nativeGetAgentInformation(I)Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    goto :goto_5
.end method

.method public static getLastError()J
    .registers 2

    #@0
    .prologue
    .line 635
    const-wide/16 v0, 0x0

    #@2
    return-wide v0
.end method

.method public static getObjectDrmType([B)I
    .registers 3
    .parameter "buf"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    #@0
    .prologue
    .line 246
    sget-boolean v0, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@2
    if-nez v0, :cond_6

    #@4
    .line 247
    const/4 v0, 0x0

    #@5
    .line 254
    :goto_5
    return v0

    #@6
    .line 250
    :cond_6
    if-nez p0, :cond_10

    #@8
    .line 251
    new-instance v0, Ljava/lang/NullPointerException;

    #@a
    const-string v1, "Parameter buf is null"

    #@c
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 254
    :cond_10
    invoke-static {p0}, Lcom/lge/lgdrm/DrmManager;->nativeGetObjectDrmType([B)I

    #@13
    move-result v0

    #@14
    goto :goto_5
.end method

.method public static getObjectInfo(I[B)Ljava/lang/String;
    .registers 4
    .parameter "type"
    .parameter "buf"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lge/lgdrm/DrmException;,
            Ljava/lang/NullPointerException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 307
    sget-boolean v0, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@2
    if-nez v0, :cond_6

    #@4
    .line 308
    const/4 v0, 0x0

    #@5
    .line 318
    :goto_5
    return-object v0

    #@6
    .line 311
    :cond_6
    const/4 v0, 0x1

    #@7
    if-lt p0, v0, :cond_c

    #@9
    const/4 v0, 0x2

    #@a
    if-le p0, v0, :cond_14

    #@c
    .line 312
    :cond_c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@e
    const-string v1, "Invalid type"

    #@10
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 314
    :cond_14
    if-nez p1, :cond_1e

    #@16
    .line 315
    new-instance v0, Ljava/lang/NullPointerException;

    #@18
    const-string v1, "Parameter buf is null"

    #@1a
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v0

    #@1e
    .line 318
    :cond_1e
    invoke-static {p0, p1}, Lcom/lge/lgdrm/DrmManager;->nativeGetObjectInfo(I[B)Ljava/lang/String;

    #@21
    move-result-object v0

    #@22
    goto :goto_5
.end method

.method public static getObjectMediaMimeType([B)Ljava/lang/String;
    .registers 3
    .parameter "buf"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lge/lgdrm/DrmException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    #@0
    .prologue
    .line 274
    sget-boolean v0, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@2
    if-nez v0, :cond_6

    #@4
    .line 275
    const/4 v0, 0x0

    #@5
    .line 282
    :goto_5
    return-object v0

    #@6
    .line 278
    :cond_6
    if-nez p0, :cond_10

    #@8
    .line 279
    new-instance v0, Ljava/lang/NullPointerException;

    #@a
    const-string v1, "Parameter buf is null"

    #@c
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 282
    :cond_10
    invoke-static {p0}, Lcom/lge/lgdrm/DrmManager;->nativeGetObjectMediaMimeType([B)Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    goto :goto_5
.end method

.method public static getSupportedAgent()[I
    .registers 1

    #@0
    .prologue
    .line 416
    sget-boolean v0, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@2
    if-nez v0, :cond_6

    #@4
    .line 417
    const/4 v0, 0x0

    #@5
    .line 420
    :goto_5
    return-object v0

    #@6
    :cond_6
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeGetSupportedAgent()[I

    #@9
    move-result-object v0

    #@a
    goto :goto_5
.end method

.method public static isDRM(Ljava/lang/String;)I
    .registers 3
    .parameter "filename"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    #@0
    .prologue
    .line 142
    sget-boolean v0, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@2
    if-nez v0, :cond_6

    #@4
    .line 143
    const/4 v0, 0x0

    #@5
    .line 150
    :goto_5
    return v0

    #@6
    .line 146
    :cond_6
    if-nez p0, :cond_10

    #@8
    .line 147
    new-instance v0, Ljava/lang/NullPointerException;

    #@a
    const-string v1, "Parameter filename is null"

    #@c
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 150
    :cond_10
    invoke-static {p0}, Lcom/lge/lgdrm/DrmManager;->nativeIsDRM(Ljava/lang/String;)I

    #@13
    move-result v0

    #@14
    goto :goto_5
.end method

.method public static isSupportedAgent(I)Z
    .registers 6
    .parameter "agentType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 449
    if-lt p0, v3, :cond_8

    #@4
    const/16 v4, 0xf

    #@6
    if-lt p0, v4, :cond_10

    #@8
    .line 450
    :cond_8
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@a
    const-string v3, "Invalid agentType"

    #@c
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v2

    #@10
    .line 453
    :cond_10
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->getSupportedAgent()[I

    #@13
    move-result-object v0

    #@14
    .line 454
    .local v0, agents:[I
    if-nez v0, :cond_17

    #@16
    .line 464
    :cond_16
    :goto_16
    return v2

    #@17
    .line 458
    :cond_17
    const/4 v1, 0x0

    #@18
    .local v1, i:I
    :goto_18
    array-length v4, v0

    #@19
    if-ge v1, v4, :cond_16

    #@1b
    .line 459
    aget v4, v0, v1

    #@1d
    if-ne v4, p0, :cond_21

    #@1f
    move v2, v3

    #@20
    .line 460
    goto :goto_16

    #@21
    .line 458
    :cond_21
    add-int/lit8 v1, v1, 0x1

    #@23
    goto :goto_18
.end method

.method public static isSupportedExtension(ILjava/lang/String;Ljava/lang/String;)Z
    .registers 14
    .parameter "agentType"
    .parameter "filename"
    .parameter "extension"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v10, 0x7

    #@1
    const/4 v9, 0x5

    #@2
    const/4 v8, 0x2

    #@3
    const/4 v6, 0x1

    #@4
    const/4 v5, 0x0

    #@5
    .line 522
    sget-boolean v7, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@7
    if-nez v7, :cond_a

    #@9
    .line 593
    :cond_9
    :goto_9
    return v5

    #@a
    .line 526
    :cond_a
    if-nez p1, :cond_17

    #@c
    if-nez p2, :cond_17

    #@e
    .line 527
    new-instance v5, Ljava/lang/NullPointerException;

    #@10
    const-string/jumbo v6, "parameter is null"

    #@13
    invoke-direct {v5, v6}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@16
    throw v5

    #@17
    .line 530
    :cond_17
    const/4 v1, 0x0

    #@18
    .line 531
    .local v1, ext:Ljava/lang/String;
    if-eqz p1, :cond_5a

    #@1a
    .line 532
    const-string v7, "."

    #@1c
    invoke-virtual {p1, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    #@1f
    move-result v2

    #@20
    .line 533
    .local v2, lastDot:I
    if-ltz v2, :cond_9

    #@22
    .line 536
    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@25
    move-result-object v7

    #@26
    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    .line 542
    .end local v2           #lastDot:I
    :goto_2a
    const/4 v4, 0x0

    #@2b
    .line 543
    .local v4, storeAs:I
    if-eq p0, v8, :cond_37

    #@2d
    if-eq p0, v6, :cond_37

    #@2f
    const/16 v7, 0x9

    #@31
    if-eq p0, v7, :cond_37

    #@33
    const/16 v7, 0xa

    #@35
    if-ne p0, v7, :cond_81

    #@37
    .line 545
    :cond_37
    const-string v7, ".dm"

    #@39
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3c
    move-result v7

    #@3d
    if-nez v7, :cond_47

    #@3f
    const-string v7, ".dcf"

    #@41
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@44
    move-result v7

    #@45
    if-eqz v7, :cond_5f

    #@47
    .line 546
    :cond_47
    const/4 v4, 0x1

    #@48
    .line 564
    :cond_48
    :goto_48
    sget-object v7, Lcom/lge/lgdrm/DrmManager;->sSLExtMap:Ljava/util/HashMap;

    #@4a
    invoke-virtual {v7, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@4d
    move-result-object v0

    #@4e
    check-cast v0, Ljava/lang/Integer;

    #@50
    .line 565
    .local v0, agent:Ljava/lang/Integer;
    if-eqz v0, :cond_bf

    #@52
    .line 566
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@55
    move-result v3

    #@56
    .line 567
    .local v3, sAgentType:I
    if-ne v3, p0, :cond_b1

    #@58
    move v5, v6

    #@59
    .line 568
    goto :goto_9

    #@5a
    .line 538
    .end local v0           #agent:Ljava/lang/Integer;
    .end local v3           #sAgentType:I
    .end local v4           #storeAs:I
    :cond_5a
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@5d
    move-result-object v1

    #@5e
    goto :goto_2a

    #@5f
    .line 547
    .restart local v4       #storeAs:I
    :cond_5f
    const-string v7, ".odf"

    #@61
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@64
    move-result v7

    #@65
    if-nez v7, :cond_7f

    #@67
    const-string v7, ".o4a"

    #@69
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6c
    move-result v7

    #@6d
    if-nez v7, :cond_7f

    #@6f
    const-string v7, ".o4v"

    #@71
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@74
    move-result v7

    #@75
    if-nez v7, :cond_7f

    #@77
    const-string v7, ".o4i"

    #@79
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7c
    move-result v7

    #@7d
    if-eqz v7, :cond_9

    #@7f
    .line 549
    :cond_7f
    const/4 v4, 0x2

    #@80
    goto :goto_48

    #@81
    .line 553
    :cond_81
    if-eq p0, v9, :cond_85

    #@83
    if-ne p0, v10, :cond_48

    #@85
    .line 554
    :cond_85
    const-string v7, ".wma"

    #@87
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8a
    move-result v7

    #@8b
    if-nez v7, :cond_95

    #@8d
    const-string v7, ".wmv"

    #@8f
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@92
    move-result v7

    #@93
    if-eqz v7, :cond_97

    #@95
    .line 555
    :cond_95
    const/4 v4, 0x5

    #@96
    goto :goto_48

    #@97
    .line 556
    :cond_97
    const-string v7, ".eny"

    #@99
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9c
    move-result v7

    #@9d
    if-nez v7, :cond_af

    #@9f
    const-string v7, ".pya"

    #@a1
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a4
    move-result v7

    #@a5
    if-nez v7, :cond_af

    #@a7
    const-string v7, ".pyv"

    #@a9
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ac
    move-result v7

    #@ad
    if-eqz v7, :cond_9

    #@af
    .line 557
    :cond_af
    const/4 v4, 0x7

    #@b0
    goto :goto_48

    #@b1
    .line 570
    .restart local v0       #agent:Ljava/lang/Integer;
    .restart local v3       #sAgentType:I
    :cond_b1
    if-ne p0, v8, :cond_b8

    #@b3
    if-ne v3, v6, :cond_b8

    #@b5
    move v5, v6

    #@b6
    .line 572
    goto/16 :goto_9

    #@b8
    .line 574
    :cond_b8
    if-ne p0, v10, :cond_9

    #@ba
    if-ne v3, v9, :cond_9

    #@bc
    move v5, v6

    #@bd
    .line 576
    goto/16 :goto_9

    #@bf
    .line 583
    .end local v3           #sAgentType:I
    :cond_bf
    invoke-static {p0, v1}, Lcom/lge/lgdrm/DrmManager;->nativeIsSupportedExtension(ILjava/lang/String;)Z

    #@c2
    move-result v7

    #@c3
    if-eqz v7, :cond_9

    #@c5
    .line 585
    if-eqz v4, :cond_d3

    #@c7
    .line 586
    sget-object v5, Lcom/lge/lgdrm/DrmManager;->sSLExtMap:Ljava/util/HashMap;

    #@c9
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@cc
    move-result-object v7

    #@cd
    invoke-virtual {v5, v1, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@d0
    :goto_d0
    move v5, v6

    #@d1
    .line 590
    goto/16 :goto_9

    #@d3
    .line 588
    :cond_d3
    sget-object v5, Lcom/lge/lgdrm/DrmManager;->sSLExtMap:Ljava/util/HashMap;

    #@d5
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d8
    move-result-object v7

    #@d9
    invoke-virtual {v5, v1, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@dc
    goto :goto_d0
.end method

.method public static manageDatabase(I)I
    .registers 3
    .parameter "operation"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 383
    sget-boolean v0, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@2
    if-nez v0, :cond_6

    #@4
    .line 384
    const/4 v0, -0x1

    #@5
    .line 394
    :goto_5
    return v0

    #@6
    .line 387
    :cond_6
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_14

    #@c
    .line 388
    new-instance v0, Ljava/lang/SecurityException;

    #@e
    const-string v1, "Need proper permission to access drm"

    #@10
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 391
    :cond_14
    if-ltz p0, :cond_1a

    #@16
    const/16 v0, 0x10

    #@18
    if-lt p0, v0, :cond_22

    #@1a
    .line 392
    :cond_1a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1c
    const-string v1, "Invalid operation"

    #@1e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@21
    throw v0

    #@22
    .line 394
    :cond_22
    invoke-static {p0}, Lcom/lge/lgdrm/DrmManager;->nativeManageDatabase(I)I

    #@25
    move-result v0

    #@26
    goto :goto_5
.end method

.method protected static native nativeAuthCaller()Z
.end method

.method private static native nativeGetAgentInformation(I)Ljava/lang/String;
.end method

.method private static native nativeGetContentBasicInfo(Lcom/lge/lgdrm/DrmContent;Ljava/lang/String;I)I
.end method

.method private static native nativeGetObjectDrmType([B)I
.end method

.method private static native nativeGetObjectInfo(I[B)Ljava/lang/String;
.end method

.method private static native nativeGetObjectMediaMimeType([B)Ljava/lang/String;
.end method

.method private static native nativeGetSupportedAgent()[I
.end method

.method private static native nativeIsDRM(Ljava/lang/String;)I
.end method

.method private static native nativeIsSupportedExtension(ILjava/lang/String;)Z
.end method

.method private static native nativeManageDatabase(I)I
.end method

.method private static native nativeSetDebugOptions(III)V
.end method

.method public static setDebugOptions(III)V
    .registers 5
    .parameter "agentType"
    .parameter "option"
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 618
    sget-boolean v0, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@2
    if-nez v0, :cond_5

    #@4
    .line 627
    :goto_4
    return-void

    #@5
    .line 622
    :cond_5
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_13

    #@b
    .line 623
    new-instance v0, Ljava/lang/SecurityException;

    #@d
    const-string v1, "Need proper permission to access drm"

    #@f
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@12
    throw v0

    #@13
    .line 626
    :cond_13
    invoke-static {p0, p1, p2}, Lcom/lge/lgdrm/DrmManager;->nativeSetDebugOptions(III)V

    #@16
    goto :goto_4
.end method
