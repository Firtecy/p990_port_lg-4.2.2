.class public final Lcom/lge/lgdrm/DrmContent;
.super Ljava/lang/Object;
.source "DrmContent.java"


# static fields
.field private static FLIconStatus:I = 0x0

.field private static final TAG:Ljava/lang/String; = "DrmCnt"


# instance fields
.field protected agentType:I

.field protected autoRightState:Z

.field private cid:Ljava/lang/String;

.field private contentSize:J

.field protected contentType:I

.field private contentURL:Ljava/lang/String;

.field private extension:Ljava/lang/String;

.field protected filename:Ljava/lang/String;

.field protected index:I

.field private mediaType:I

.field private metadata:Lcom/lge/lgdrm/DrmContentMetaData;

.field private mimeType:Ljava/lang/String;

.field protected permisson:I

.field protected previewContent:I

.field private riURL:Ljava/lang/String;

.field protected rightState:I

.field private ttid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 134
    const/4 v0, -0x1

    #@1
    sput v0, Lcom/lge/lgdrm/DrmContent;->FLIconStatus:I

    #@3
    return-void
.end method

.method private constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 136
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 114
    const/4 v0, -0x4

    #@5
    iput v0, p0, Lcom/lge/lgdrm/DrmContent;->rightState:I

    #@7
    .line 116
    iput-boolean v1, p0, Lcom/lge/lgdrm/DrmContent;->autoRightState:Z

    #@9
    .line 118
    iput v1, p0, Lcom/lge/lgdrm/DrmContent;->permisson:I

    #@b
    .line 130
    const-wide/16 v0, -0x1

    #@d
    iput-wide v0, p0, Lcom/lge/lgdrm/DrmContent;->contentSize:J

    #@f
    .line 137
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;II)V
    .registers 6
    .parameter "filename"
    .parameter "index"
    .parameter "contentType"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 139
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 114
    const/4 v0, -0x4

    #@5
    iput v0, p0, Lcom/lge/lgdrm/DrmContent;->rightState:I

    #@7
    .line 116
    iput-boolean v1, p0, Lcom/lge/lgdrm/DrmContent;->autoRightState:Z

    #@9
    .line 118
    iput v1, p0, Lcom/lge/lgdrm/DrmContent;->permisson:I

    #@b
    .line 130
    const-wide/16 v0, -0x1

    #@d
    iput-wide v0, p0, Lcom/lge/lgdrm/DrmContent;->contentSize:J

    #@f
    .line 140
    iput-object p1, p0, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@11
    .line 141
    iput p2, p0, Lcom/lge/lgdrm/DrmContent;->index:I

    #@13
    .line 142
    iput p3, p0, Lcom/lge/lgdrm/DrmContent;->contentType:I

    #@15
    .line 144
    if-nez p3, :cond_18

    #@17
    .line 163
    :cond_17
    :goto_17
    return-void

    #@18
    .line 148
    :cond_18
    const/16 v0, 0x1800

    #@1a
    if-ne p3, v0, :cond_21

    #@1c
    .line 149
    const/16 v0, 0x9

    #@1e
    iput v0, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@20
    goto :goto_17

    #@21
    .line 150
    :cond_21
    const/16 v0, 0x501

    #@23
    if-ne p3, v0, :cond_2a

    #@25
    .line 151
    const/16 v0, 0xa

    #@27
    iput v0, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@29
    goto :goto_17

    #@2a
    .line 152
    :cond_2a
    const/16 v0, 0x10

    #@2c
    if-le p3, v0, :cond_36

    #@2e
    const/16 v0, 0x1000

    #@30
    if-ge p3, v0, :cond_36

    #@32
    .line 153
    const/4 v0, 0x1

    #@33
    iput v0, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@35
    goto :goto_17

    #@36
    .line 154
    :cond_36
    const/16 v0, 0x3000

    #@38
    if-ne p3, v0, :cond_3e

    #@3a
    .line 155
    const/4 v0, 0x2

    #@3b
    iput v0, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@3d
    goto :goto_17

    #@3e
    .line 156
    :cond_3e
    const/high16 v0, 0x1

    #@40
    and-int/2addr v0, p3

    #@41
    if-eqz v0, :cond_47

    #@43
    .line 157
    const/4 v0, 0x5

    #@44
    iput v0, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@46
    goto :goto_17

    #@47
    .line 158
    :cond_47
    const/high16 v0, 0x8

    #@49
    if-ne p3, v0, :cond_4f

    #@4b
    .line 159
    const/4 v0, 0x6

    #@4c
    iput v0, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@4e
    goto :goto_17

    #@4f
    .line 160
    :cond_4f
    const/high16 v0, 0x10

    #@51
    and-int/2addr v0, p3

    #@52
    if-nez v0, :cond_58

    #@54
    const/high16 v0, 0x80

    #@56
    if-ne p3, v0, :cond_17

    #@58
    .line 161
    :cond_58
    const/4 v0, 0x7

    #@59
    iput v0, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@5b
    goto :goto_17
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;I)V
    .registers 9
    .parameter "filename"
    .parameter "index"
    .parameter "contentType"
    .parameter "mimeType"
    .parameter "extension"
    .parameter "mediaType"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 180
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 114
    const/4 v0, -0x4

    #@5
    iput v0, p0, Lcom/lge/lgdrm/DrmContent;->rightState:I

    #@7
    .line 116
    iput-boolean v1, p0, Lcom/lge/lgdrm/DrmContent;->autoRightState:Z

    #@9
    .line 118
    iput v1, p0, Lcom/lge/lgdrm/DrmContent;->permisson:I

    #@b
    .line 130
    const-wide/16 v0, -0x1

    #@d
    iput-wide v0, p0, Lcom/lge/lgdrm/DrmContent;->contentSize:J

    #@f
    .line 181
    iput-object p1, p0, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@11
    .line 182
    iput p3, p0, Lcom/lge/lgdrm/DrmContent;->contentType:I

    #@13
    .line 183
    iput-object p4, p0, Lcom/lge/lgdrm/DrmContent;->mimeType:Ljava/lang/String;

    #@15
    .line 184
    iput-object p5, p0, Lcom/lge/lgdrm/DrmContent;->extension:Ljava/lang/String;

    #@17
    .line 185
    iput p6, p0, Lcom/lge/lgdrm/DrmContent;->mediaType:I

    #@19
    .line 187
    const/16 v0, 0x1800

    #@1b
    if-ne p3, v0, :cond_22

    #@1d
    .line 188
    const/16 v0, 0x9

    #@1f
    iput v0, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@21
    .line 202
    :cond_21
    :goto_21
    return-void

    #@22
    .line 189
    :cond_22
    const/16 v0, 0x501

    #@24
    if-ne p3, v0, :cond_2b

    #@26
    .line 190
    const/16 v0, 0xa

    #@28
    iput v0, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@2a
    goto :goto_21

    #@2b
    .line 191
    :cond_2b
    const/16 v0, 0x10

    #@2d
    if-le p3, v0, :cond_37

    #@2f
    const/16 v0, 0x1000

    #@31
    if-ge p3, v0, :cond_37

    #@33
    .line 192
    const/4 v0, 0x1

    #@34
    iput v0, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@36
    goto :goto_21

    #@37
    .line 193
    :cond_37
    const/16 v0, 0x3000

    #@39
    if-ne p3, v0, :cond_3f

    #@3b
    .line 194
    const/4 v0, 0x2

    #@3c
    iput v0, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@3e
    goto :goto_21

    #@3f
    .line 195
    :cond_3f
    const/high16 v0, 0x1

    #@41
    and-int/2addr v0, p3

    #@42
    if-eqz v0, :cond_48

    #@44
    .line 196
    const/4 v0, 0x5

    #@45
    iput v0, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@47
    goto :goto_21

    #@48
    .line 197
    :cond_48
    const/high16 v0, 0x8

    #@4a
    if-ne p3, v0, :cond_50

    #@4c
    .line 198
    const/4 v0, 0x6

    #@4d
    iput v0, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@4f
    goto :goto_21

    #@50
    .line 199
    :cond_50
    const/high16 v0, 0x10

    #@52
    and-int/2addr v0, p3

    #@53
    if-nez v0, :cond_59

    #@55
    const/high16 v0, 0x80

    #@57
    if-ne p3, v0, :cond_21

    #@59
    .line 200
    :cond_59
    const/4 v0, 0x7

    #@5a
    iput v0, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@5c
    goto :goto_21
.end method

.method public static getContentInfo(Ljava/lang/String;I)Ljava/lang/String;
    .registers 4
    .parameter "filename"
    .parameter "type"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/lge/lgdrm/DrmException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 378
    sget-boolean v0, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@3
    if-nez v0, :cond_7

    #@5
    .line 379
    const/4 v0, 0x0

    #@6
    .line 393
    :goto_6
    return-object v0

    #@7
    .line 382
    :cond_7
    if-nez p0, :cond_11

    #@9
    .line 383
    new-instance v0, Ljava/lang/NullPointerException;

    #@b
    const-string v1, "Parameter filename is null"

    #@d
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0

    #@11
    .line 385
    :cond_11
    const/4 v0, 0x1

    #@12
    if-lt p1, v0, :cond_17

    #@14
    const/4 v0, 0x6

    #@15
    if-le p1, v0, :cond_1f

    #@17
    .line 386
    :cond_17
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@19
    const-string v1, "Invalid type"

    #@1b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v0

    #@1f
    .line 389
    :cond_1f
    invoke-static {p0}, Lcom/lge/lgdrm/DrmContent;->nativeIsDRM(Ljava/lang/String;)I

    #@22
    move-result v0

    #@23
    if-nez v0, :cond_2d

    #@25
    .line 390
    new-instance v0, Lcom/lge/lgdrm/DrmException;

    #@27
    const-string v1, "Not DRM protected content"

    #@29
    invoke-direct {v0, v1}, Lcom/lge/lgdrm/DrmException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v0

    #@2d
    .line 393
    :cond_2d
    invoke-static {p1, p0, v1, v1}, Lcom/lge/lgdrm/DrmContent;->nativeGetContentInfo(ILjava/lang/String;II)Ljava/lang/String;

    #@30
    move-result-object v0

    #@31
    goto :goto_6
.end method

.method public static getContentType(Ljava/lang/String;)I
    .registers 3
    .parameter "filename"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Lcom/lge/lgdrm/DrmException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 262
    sget-boolean v1, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@3
    if-nez v1, :cond_6

    #@5
    .line 274
    :goto_5
    return v0

    #@6
    .line 266
    :cond_6
    if-nez p0, :cond_10

    #@8
    .line 267
    new-instance v0, Ljava/lang/NullPointerException;

    #@a
    const-string v1, "Parameter filename is null"

    #@c
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 270
    :cond_10
    invoke-static {p0}, Lcom/lge/lgdrm/DrmContent;->nativeIsDRM(Ljava/lang/String;)I

    #@13
    move-result v1

    #@14
    if-nez v1, :cond_1e

    #@16
    .line 271
    new-instance v0, Lcom/lge/lgdrm/DrmException;

    #@18
    const-string v1, "Not DRM protected content"

    #@1a
    invoke-direct {v0, v1}, Lcom/lge/lgdrm/DrmException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v0

    #@1e
    .line 274
    :cond_1e
    invoke-static {p0, v0, v0}, Lcom/lge/lgdrm/DrmContent;->nativeGetContentType(Ljava/lang/String;II)I

    #@21
    move-result v0

    #@22
    goto :goto_5
.end method

.method public static isForwardlockIconVisible()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 543
    sget-boolean v2, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@4
    if-nez v2, :cond_7

    #@6
    .line 555
    :goto_6
    return v1

    #@7
    .line 547
    :cond_7
    sget v2, Lcom/lge/lgdrm/DrmContent;->FLIconStatus:I

    #@9
    const/4 v3, -0x1

    #@a
    if-ne v2, v3, :cond_14

    #@c
    .line 548
    invoke-static {}, Lcom/lge/lgdrm/DrmContent;->nativeIsForwardlockIconVisible()Z

    #@f
    move-result v2

    #@10
    if-eqz v2, :cond_1a

    #@12
    .line 549
    sput v0, Lcom/lge/lgdrm/DrmContent;->FLIconStatus:I

    #@14
    .line 555
    :cond_14
    :goto_14
    sget v2, Lcom/lge/lgdrm/DrmContent;->FLIconStatus:I

    #@16
    if-ne v2, v0, :cond_1d

    #@18
    :goto_18
    move v1, v0

    #@19
    goto :goto_6

    #@1a
    .line 551
    :cond_1a
    sput v1, Lcom/lge/lgdrm/DrmContent;->FLIconStatus:I

    #@1c
    goto :goto_14

    #@1d
    :cond_1d
    move v0, v1

    #@1e
    .line 555
    goto :goto_18
.end method

.method private native nativeCheckPreviewContent(Ljava/lang/String;I)Z
.end method

.method private static native nativeGetContentInfo(ILjava/lang/String;II)Ljava/lang/String;
.end method

.method private native nativeGetContentSize(Ljava/lang/String;II)J
.end method

.method private static native nativeGetContentType(Ljava/lang/String;II)I
.end method

.method private native nativeGetMetaData(Lcom/lge/lgdrm/DrmContentMetaData;Ljava/lang/String;II)I
.end method

.method private static native nativeIsDRM(Ljava/lang/String;)I
.end method

.method private static native nativeIsForwardlockIconVisible()Z
.end method

.method private setBasicInfo(IILjava/lang/String;Ljava/lang/String;I)V
    .registers 7
    .parameter "index"
    .parameter "contentType"
    .parameter "mimeType"
    .parameter "extension"
    .parameter "mediaType"

    #@0
    .prologue
    .line 613
    iput p2, p0, Lcom/lge/lgdrm/DrmContent;->contentType:I

    #@2
    .line 614
    iput-object p3, p0, Lcom/lge/lgdrm/DrmContent;->mimeType:Ljava/lang/String;

    #@4
    .line 615
    iput-object p4, p0, Lcom/lge/lgdrm/DrmContent;->extension:Ljava/lang/String;

    #@6
    .line 616
    iput p5, p0, Lcom/lge/lgdrm/DrmContent;->mediaType:I

    #@8
    .line 618
    const/16 v0, 0x1800

    #@a
    if-ne p2, v0, :cond_11

    #@c
    .line 619
    const/16 v0, 0x9

    #@e
    iput v0, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@10
    .line 633
    :cond_10
    :goto_10
    return-void

    #@11
    .line 620
    :cond_11
    const/16 v0, 0x501

    #@13
    if-ne p2, v0, :cond_1a

    #@15
    .line 621
    const/16 v0, 0xa

    #@17
    iput v0, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@19
    goto :goto_10

    #@1a
    .line 622
    :cond_1a
    const/16 v0, 0x10

    #@1c
    if-le p2, v0, :cond_26

    #@1e
    const/16 v0, 0x1000

    #@20
    if-ge p2, v0, :cond_26

    #@22
    .line 623
    const/4 v0, 0x1

    #@23
    iput v0, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@25
    goto :goto_10

    #@26
    .line 624
    :cond_26
    const/16 v0, 0x3000

    #@28
    if-ne p2, v0, :cond_2e

    #@2a
    .line 625
    const/4 v0, 0x2

    #@2b
    iput v0, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@2d
    goto :goto_10

    #@2e
    .line 626
    :cond_2e
    const/high16 v0, 0x1

    #@30
    and-int/2addr v0, p2

    #@31
    if-eqz v0, :cond_37

    #@33
    .line 627
    const/4 v0, 0x5

    #@34
    iput v0, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@36
    goto :goto_10

    #@37
    .line 628
    :cond_37
    const/high16 v0, 0x8

    #@39
    if-ne p2, v0, :cond_3f

    #@3b
    .line 629
    const/4 v0, 0x6

    #@3c
    iput v0, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@3e
    goto :goto_10

    #@3f
    .line 630
    :cond_3f
    const/high16 v0, 0x10

    #@41
    and-int/2addr v0, p2

    #@42
    if-nez v0, :cond_48

    #@44
    const/high16 v0, 0x80

    #@46
    if-ne p2, v0, :cond_10

    #@48
    .line 631
    :cond_48
    const/4 v0, 0x7

    #@49
    iput v0, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@4b
    goto :goto_10
.end method

.method private setBasicInfo(Ljava/lang/String;Ljava/lang/String;I)V
    .registers 4
    .parameter "mimeType"
    .parameter "extension"
    .parameter "mediaType"

    #@0
    .prologue
    .line 645
    iput-object p1, p0, Lcom/lge/lgdrm/DrmContent;->mimeType:Ljava/lang/String;

    #@2
    .line 646
    iput-object p2, p0, Lcom/lge/lgdrm/DrmContent;->extension:Ljava/lang/String;

    #@4
    .line 647
    iput p3, p0, Lcom/lge/lgdrm/DrmContent;->mediaType:I

    #@6
    .line 648
    return-void
.end method


# virtual methods
.method public checkPreviewContent()Z
    .registers 3

    #@0
    .prologue
    .line 417
    iget v0, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@2
    const/4 v1, 0x2

    #@3
    if-ne v0, v1, :cond_e

    #@5
    .line 418
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@7
    iget v1, p0, Lcom/lge/lgdrm/DrmContent;->index:I

    #@9
    invoke-direct {p0, v0, v1}, Lcom/lge/lgdrm/DrmContent;->nativeCheckPreviewContent(Ljava/lang/String;I)Z

    #@c
    move-result v0

    #@d
    .line 421
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public getContentInfo(I)Ljava/lang/String;
    .registers 8
    .parameter "type"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v5, 0x9

    #@2
    const/4 v4, 0x6

    #@3
    const/4 v0, 0x0

    #@4
    const/4 v3, 0x2

    #@5
    const/4 v2, 0x1

    #@6
    .line 294
    if-lt p1, v2, :cond_a

    #@8
    if-le p1, v4, :cond_12

    #@a
    .line 295
    :cond_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@c
    const-string v1, "Invalid type"

    #@e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0

    #@12
    .line 298
    :cond_12
    if-ne p1, v2, :cond_17

    #@14
    .line 299
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContent;->mimeType:Ljava/lang/String;

    #@16
    .line 352
    :cond_16
    :goto_16
    return-object v0

    #@17
    .line 301
    :cond_17
    if-ne p1, v3, :cond_1c

    #@19
    .line 302
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContent;->extension:Ljava/lang/String;

    #@1b
    goto :goto_16

    #@1c
    .line 306
    :cond_1c
    iget v1, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@1e
    if-eq v1, v2, :cond_2e

    #@20
    iget v1, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@22
    if-eq v1, v3, :cond_2e

    #@24
    iget v1, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@26
    if-eq v1, v5, :cond_2e

    #@28
    iget v1, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@2a
    const/16 v2, 0xa

    #@2c
    if-ne v1, v2, :cond_16

    #@2e
    .line 311
    :cond_2e
    const/4 v1, 0x3

    #@2f
    if-ne p1, v1, :cond_47

    #@31
    .line 312
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContent;->cid:Ljava/lang/String;

    #@33
    if-eqz v0, :cond_38

    #@35
    .line 313
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContent;->cid:Ljava/lang/String;

    #@37
    goto :goto_16

    #@38
    .line 316
    :cond_38
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@3a
    iget v1, p0, Lcom/lge/lgdrm/DrmContent;->index:I

    #@3c
    iget v2, p0, Lcom/lge/lgdrm/DrmContent;->previewContent:I

    #@3e
    invoke-static {p1, v0, v1, v2}, Lcom/lge/lgdrm/DrmContent;->nativeGetContentInfo(ILjava/lang/String;II)Ljava/lang/String;

    #@41
    move-result-object v0

    #@42
    iput-object v0, p0, Lcom/lge/lgdrm/DrmContent;->cid:Ljava/lang/String;

    #@44
    .line 317
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContent;->cid:Ljava/lang/String;

    #@46
    goto :goto_16

    #@47
    .line 320
    :cond_47
    const/4 v1, 0x4

    #@48
    if-ne p1, v1, :cond_60

    #@4a
    .line 321
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContent;->riURL:Ljava/lang/String;

    #@4c
    if-eqz v0, :cond_51

    #@4e
    .line 322
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContent;->riURL:Ljava/lang/String;

    #@50
    goto :goto_16

    #@51
    .line 325
    :cond_51
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@53
    iget v1, p0, Lcom/lge/lgdrm/DrmContent;->index:I

    #@55
    iget v2, p0, Lcom/lge/lgdrm/DrmContent;->previewContent:I

    #@57
    invoke-static {p1, v0, v1, v2}, Lcom/lge/lgdrm/DrmContent;->nativeGetContentInfo(ILjava/lang/String;II)Ljava/lang/String;

    #@5a
    move-result-object v0

    #@5b
    iput-object v0, p0, Lcom/lge/lgdrm/DrmContent;->riURL:Ljava/lang/String;

    #@5d
    .line 326
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContent;->riURL:Ljava/lang/String;

    #@5f
    goto :goto_16

    #@60
    .line 330
    :cond_60
    iget v1, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@62
    if-eq v1, v3, :cond_68

    #@64
    iget v1, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@66
    if-ne v1, v5, :cond_16

    #@68
    .line 334
    :cond_68
    const/4 v1, 0x5

    #@69
    if-ne p1, v1, :cond_81

    #@6b
    .line 335
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContent;->contentURL:Ljava/lang/String;

    #@6d
    if-eqz v0, :cond_72

    #@6f
    .line 336
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContent;->contentURL:Ljava/lang/String;

    #@71
    goto :goto_16

    #@72
    .line 339
    :cond_72
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@74
    iget v1, p0, Lcom/lge/lgdrm/DrmContent;->index:I

    #@76
    iget v2, p0, Lcom/lge/lgdrm/DrmContent;->previewContent:I

    #@78
    invoke-static {p1, v0, v1, v2}, Lcom/lge/lgdrm/DrmContent;->nativeGetContentInfo(ILjava/lang/String;II)Ljava/lang/String;

    #@7b
    move-result-object v0

    #@7c
    iput-object v0, p0, Lcom/lge/lgdrm/DrmContent;->contentURL:Ljava/lang/String;

    #@7e
    .line 340
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContent;->contentURL:Ljava/lang/String;

    #@80
    goto :goto_16

    #@81
    .line 343
    :cond_81
    if-ne p1, v4, :cond_16

    #@83
    .line 344
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContent;->ttid:Ljava/lang/String;

    #@85
    if-eqz v0, :cond_8a

    #@87
    .line 345
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContent;->ttid:Ljava/lang/String;

    #@89
    goto :goto_16

    #@8a
    .line 348
    :cond_8a
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@8c
    iget v1, p0, Lcom/lge/lgdrm/DrmContent;->index:I

    #@8e
    iget v2, p0, Lcom/lge/lgdrm/DrmContent;->previewContent:I

    #@90
    invoke-static {p1, v0, v1, v2}, Lcom/lge/lgdrm/DrmContent;->nativeGetContentInfo(ILjava/lang/String;II)Ljava/lang/String;

    #@93
    move-result-object v0

    #@94
    iput-object v0, p0, Lcom/lge/lgdrm/DrmContent;->ttid:Ljava/lang/String;

    #@96
    .line 349
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContent;->ttid:Ljava/lang/String;

    #@98
    goto/16 :goto_16
.end method

.method public getContentSize()J
    .registers 5

    #@0
    .prologue
    .line 403
    iget-wide v0, p0, Lcom/lge/lgdrm/DrmContent;->contentSize:J

    #@2
    const-wide/16 v2, -0x1

    #@4
    cmp-long v0, v0, v2

    #@6
    if-eqz v0, :cond_b

    #@8
    .line 404
    iget-wide v0, p0, Lcom/lge/lgdrm/DrmContent;->contentSize:J

    #@a
    .line 408
    :goto_a
    return-wide v0

    #@b
    .line 407
    :cond_b
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@d
    iget v1, p0, Lcom/lge/lgdrm/DrmContent;->index:I

    #@f
    iget v2, p0, Lcom/lge/lgdrm/DrmContent;->previewContent:I

    #@11
    invoke-direct {p0, v0, v1, v2}, Lcom/lge/lgdrm/DrmContent;->nativeGetContentSize(Ljava/lang/String;II)J

    #@14
    move-result-wide v0

    #@15
    iput-wide v0, p0, Lcom/lge/lgdrm/DrmContent;->contentSize:J

    #@17
    .line 408
    iget-wide v0, p0, Lcom/lge/lgdrm/DrmContent;->contentSize:J

    #@19
    goto :goto_a
.end method

.method public getContentType()I
    .registers 2

    #@0
    .prologue
    .line 240
    iget v0, p0, Lcom/lge/lgdrm/DrmContent;->mediaType:I

    #@2
    return v0
.end method

.method public getDrmContentType()I
    .registers 2

    #@0
    .prologue
    .line 225
    iget v0, p0, Lcom/lge/lgdrm/DrmContent;->contentType:I

    #@2
    return v0
.end method

.method public getMetaData()Lcom/lge/lgdrm/DrmContentMetaData;
    .registers 6

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 467
    iget v2, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@3
    const/4 v3, 0x1

    #@4
    if-eq v2, v3, :cond_19

    #@6
    iget v2, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@8
    const/4 v3, 0x2

    #@9
    if-eq v2, v3, :cond_19

    #@b
    iget v2, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@d
    const/16 v3, 0x9

    #@f
    if-eq v2, v3, :cond_19

    #@11
    iget v2, p0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@13
    const/16 v3, 0xa

    #@15
    if-eq v2, v3, :cond_19

    #@17
    move-object v0, v1

    #@18
    .line 492
    :cond_18
    :goto_18
    return-object v0

    #@19
    .line 473
    :cond_19
    iget-object v2, p0, Lcom/lge/lgdrm/DrmContent;->metadata:Lcom/lge/lgdrm/DrmContentMetaData;

    #@1b
    if-eqz v2, :cond_20

    #@1d
    .line 475
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContent;->metadata:Lcom/lge/lgdrm/DrmContentMetaData;

    #@1f
    goto :goto_18

    #@20
    .line 478
    :cond_20
    new-instance v0, Lcom/lge/lgdrm/DrmContentMetaData;

    #@22
    invoke-direct {v0}, Lcom/lge/lgdrm/DrmContentMetaData;-><init>()V

    #@25
    .line 479
    .local v0, meta:Lcom/lge/lgdrm/DrmContentMetaData;
    if-nez v0, :cond_29

    #@27
    move-object v0, v1

    #@28
    .line 480
    goto :goto_18

    #@29
    .line 483
    :cond_29
    iget-object v2, p0, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@2b
    iget v3, p0, Lcom/lge/lgdrm/DrmContent;->index:I

    #@2d
    iget v4, p0, Lcom/lge/lgdrm/DrmContent;->previewContent:I

    #@2f
    invoke-direct {p0, v0, v2, v3, v4}, Lcom/lge/lgdrm/DrmContent;->nativeGetMetaData(Lcom/lge/lgdrm/DrmContentMetaData;Ljava/lang/String;II)I

    #@32
    move-result v2

    #@33
    if-eqz v2, :cond_37

    #@35
    move-object v0, v1

    #@36
    .line 484
    goto :goto_18

    #@37
    .line 487
    :cond_37
    const/16 v1, 0xf

    #@39
    invoke-virtual {p0, v1}, Lcom/lge/lgdrm/DrmContent;->isActionSupported(I)Z

    #@3c
    move-result v1

    #@3d
    if-nez v1, :cond_18

    #@3f
    .line 489
    iput-object v0, p0, Lcom/lge/lgdrm/DrmContent;->metadata:Lcom/lge/lgdrm/DrmContentMetaData;

    #@41
    goto :goto_18
.end method

.method public isActionSupported(I)Z
    .registers 4
    .parameter "action"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 449
    const/16 v0, 0xf

    #@2
    if-eq p1, v0, :cond_c

    #@4
    .line 450
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@6
    const-string v1, "Invalid action"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 454
    :cond_c
    const/4 v0, 0x0

    #@d
    return v0
.end method

.method protected isIdentical(Lcom/lge/lgdrm/DrmContent;)Z
    .registers 5
    .parameter "content"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 585
    if-nez p1, :cond_4

    #@3
    .line 596
    :cond_3
    :goto_3
    return v0

    #@4
    .line 588
    :cond_4
    iget-object v1, p1, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@6
    iget-object v2, p0, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@b
    move-result v1

    #@c
    if-nez v1, :cond_3

    #@e
    .line 593
    iget v1, p1, Lcom/lge/lgdrm/DrmContent;->index:I

    #@10
    iget v2, p0, Lcom/lge/lgdrm/DrmContent;->index:I

    #@12
    if-ne v1, v2, :cond_3

    #@14
    .line 596
    const/4 v0, 0x1

    #@15
    goto :goto_3
.end method

.method protected isSibling(Lcom/lge/lgdrm/DrmContent;)Z
    .registers 5
    .parameter "content"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 566
    if-nez p1, :cond_4

    #@3
    .line 574
    :cond_3
    :goto_3
    return v0

    #@4
    .line 571
    :cond_4
    iget-object v1, p1, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@6
    iget-object v2, p0, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@b
    move-result v1

    #@c
    if-nez v1, :cond_3

    #@e
    .line 574
    const/4 v0, 0x1

    #@f
    goto :goto_3
.end method

.method public resetMetaData()I
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    .line 534
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    const-string v1, "Meta dat editing is not permitted"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public selectPreviewContent(Z)V
    .registers 3
    .parameter "reset"

    #@0
    .prologue
    .line 430
    if-eqz p1, :cond_6

    #@2
    .line 431
    const/4 v0, 0x0

    #@3
    iput v0, p0, Lcom/lge/lgdrm/DrmContent;->previewContent:I

    #@5
    .line 435
    :goto_5
    return-void

    #@6
    .line 433
    :cond_6
    const/4 v0, 0x1

    #@7
    iput v0, p0, Lcom/lge/lgdrm/DrmContent;->previewContent:I

    #@9
    goto :goto_5
.end method

.method public setMetaData(Lcom/lge/lgdrm/DrmContentMetaData;)I
    .registers 4
    .parameter "metaData"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    #@0
    .prologue
    .line 513
    if-nez p1, :cond_a

    #@2
    .line 514
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string v1, "Parameter metaData is null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 517
    :cond_a
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@c
    const-string v1, "Meta dat editing is not permitted"

    #@e
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0
.end method
