.class public Lcom/lge/lgdrm/DrmFileLockClient;
.super Ljava/lang/Object;
.source "DrmFileLockClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/lgdrm/DrmFileLockClient$OnEventListener;
    }
.end annotation


# static fields
.field public static final FILE_IO_UNIT_128KB:I = 0x20000

.field public static final FILE_IO_UNIT_16KB:I = 0x4000

.field public static final FILE_IO_UNIT_1KB:I = 0x400

.field public static final FILE_IO_UNIT_256KB:I = 0x40000

.field public static final FILE_IO_UNIT_2KB:I = 0x800

.field public static final FILE_IO_UNIT_32KB:I = 0x8000

.field public static final FILE_IO_UNIT_4KB:I = 0x1000

.field public static final FILE_IO_UNIT_64KB:I = 0x10000

.field public static final FILE_IO_UNIT_8KB:I = 0x2000

.field public static final LOCK_FILE_COMPLETE:I = 0xa

.field public static final LOCK_FILE_ERROR:I = -0xa

.field private static final TAG:Ljava/lang/String; = "DrmFileLockClient"

.field public static final UNLOCK_FILE_COMPLETE:I = 0x14

.field public static final UNLOCK_FILE_ERROR:I = -0x14


# instance fields
.field private mDstFilePath:Ljava/lang/String;

.field private mLockSessionID:I

.field private mOnEventListener:Lcom/lge/lgdrm/DrmFileLockClient$OnEventListener;

.field private mSrcFilePath:Ljava/lang/String;

.field private mStartTime:J

.field private mUnlockSessionID:I


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 28
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 95
    iput v0, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mLockSessionID:I

    #@7
    .line 96
    iput v0, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mUnlockSessionID:I

    #@9
    .line 97
    iput-object v1, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mSrcFilePath:Ljava/lang/String;

    #@b
    .line 98
    iput-object v1, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mDstFilePath:Ljava/lang/String;

    #@d
    .line 99
    const-wide/16 v0, 0x0

    #@f
    iput-wide v0, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mStartTime:J

    #@11
    .line 104
    return-void
.end method

.method private cleanSessions()V
    .registers 2

    #@0
    .prologue
    .line 595
    iget v0, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mLockSessionID:I

    #@2
    if-eqz v0, :cond_e

    #@4
    .line 596
    iget v0, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mLockSessionID:I

    #@6
    invoke-virtual {p0, v0}, Lcom/lge/lgdrm/DrmFileLockClient;->abortFileLockSession(I)I

    #@9
    .line 597
    iget v0, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mLockSessionID:I

    #@b
    invoke-virtual {p0, v0}, Lcom/lge/lgdrm/DrmFileLockClient;->closeFileLockSession(I)I

    #@e
    .line 600
    :cond_e
    iget v0, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mUnlockSessionID:I

    #@10
    if-eqz v0, :cond_1c

    #@12
    .line 601
    iget v0, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mUnlockSessionID:I

    #@14
    invoke-virtual {p0, v0}, Lcom/lge/lgdrm/DrmFileLockClient;->abortFileUnlockSession(I)I

    #@17
    .line 602
    iget v0, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mUnlockSessionID:I

    #@19
    invoke-virtual {p0, v0}, Lcom/lge/lgdrm/DrmFileLockClient;->closeFileUnlockSession(I)I

    #@1c
    .line 604
    :cond_1c
    return-void
.end method

.method public static isFileLockAllowed(Ljava/lang/String;Ljava/lang/String;I)Z
    .registers 5
    .parameter "srcFile"
    .parameter "srcMimeType"
    .parameter "srcMediaType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 196
    sget-boolean v1, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@3
    if-nez v1, :cond_6

    #@5
    .line 204
    :cond_5
    :goto_5
    return v0

    #@6
    .line 200
    :cond_6
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_14

    #@c
    .line 201
    new-instance v0, Ljava/lang/SecurityException;

    #@e
    const-string v1, "Need proper permission to access drm"

    #@10
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 204
    :cond_14
    invoke-static {p0, p1, p2}, Lcom/lge/lgdrm/DrmFileLockClient;->nativeIsFileLockAllowed(Ljava/lang/String;Ljava/lang/String;I)I

    #@17
    move-result v1

    #@18
    if-nez v1, :cond_5

    #@1a
    const/4 v0, 0x1

    #@1b
    goto :goto_5
.end method

.method public static isFileUnlockAllowed(Ljava/lang/String;)Z
    .registers 3
    .parameter "srcFile"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 402
    sget-boolean v1, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@3
    if-nez v1, :cond_6

    #@5
    .line 410
    :cond_5
    :goto_5
    return v0

    #@6
    .line 406
    :cond_6
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_14

    #@c
    .line 407
    new-instance v0, Ljava/lang/SecurityException;

    #@e
    const-string v1, "Need proper permission to access drm"

    #@10
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 410
    :cond_14
    invoke-static {p0}, Lcom/lge/lgdrm/DrmFileLockClient;->nativeIsFileUnlockAllowed(Ljava/lang/String;)I

    #@17
    move-result v1

    #@18
    if-nez v1, :cond_5

    #@1a
    const/4 v0, 0x1

    #@1b
    goto :goto_5
.end method

.method private static native nativeAbortFileLockSession(I)I
.end method

.method private static native nativeAbortFileUnlockSession(I)I
.end method

.method private static native nativeCloseFileLockSession(I)I
.end method

.method private static native nativeCloseFileUnlockSession(I)I
.end method

.method private static native nativeFileLock(I)I
.end method

.method private static native nativeFileUnlock(I)I
.end method

.method private static native nativeIsFileLockAllowed(Ljava/lang/String;Ljava/lang/String;I)I
.end method

.method private static native nativeIsFileUnlockAllowed(Ljava/lang/String;)I
.end method

.method private static native nativeOpenFileLockSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)I
.end method

.method private static native nativeOpenFileUnlockSession(Ljava/lang/String;Ljava/lang/String;II)I
.end method


# virtual methods
.method public abortFileLockSession(I)I
    .registers 5
    .parameter "sessionID"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 340
    sget-boolean v1, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@3
    if-nez v1, :cond_6

    #@5
    .line 355
    :goto_5
    return v0

    #@6
    .line 344
    :cond_6
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_14

    #@c
    .line 345
    new-instance v1, Ljava/lang/SecurityException;

    #@e
    const-string v2, "Need proper permission to access drm"

    #@10
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@13
    throw v1

    #@14
    .line 348
    :cond_14
    if-eq p1, v0, :cond_1c

    #@16
    if-eqz p1, :cond_1c

    #@18
    iget v1, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mLockSessionID:I

    #@1a
    if-eq p1, v1, :cond_24

    #@1c
    .line 349
    :cond_1c
    const-string v1, "DrmFileLockClient"

    #@1e
    const-string v2, "abortFileLockSession() : sessionID is invalid."

    #@20
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    goto :goto_5

    #@24
    .line 353
    :cond_24
    invoke-static {p1}, Lcom/lge/lgdrm/DrmFileLockClient;->nativeAbortFileLockSession(I)I

    #@27
    move-result v0

    #@28
    .line 355
    .local v0, nResult:I
    goto :goto_5
.end method

.method public abortFileUnlockSession(I)I
    .registers 5
    .parameter "sessionID"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 536
    sget-boolean v1, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@3
    if-nez v1, :cond_6

    #@5
    .line 551
    :goto_5
    return v0

    #@6
    .line 540
    :cond_6
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_14

    #@c
    .line 541
    new-instance v1, Ljava/lang/SecurityException;

    #@e
    const-string v2, "Need proper permission to access drm"

    #@10
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@13
    throw v1

    #@14
    .line 544
    :cond_14
    if-eq p1, v0, :cond_1c

    #@16
    if-eqz p1, :cond_1c

    #@18
    iget v1, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mUnlockSessionID:I

    #@1a
    if-eq p1, v1, :cond_24

    #@1c
    .line 545
    :cond_1c
    const-string v1, "DrmFileLockClient"

    #@1e
    const-string v2, "abortFileUnlockSession() : sessionID is invalid."

    #@20
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    goto :goto_5

    #@24
    .line 549
    :cond_24
    invoke-static {p1}, Lcom/lge/lgdrm/DrmFileLockClient;->nativeAbortFileUnlockSession(I)I

    #@27
    move-result v0

    #@28
    .line 551
    .local v0, nResult:I
    goto :goto_5
.end method

.method public closeFileLockSession(I)I
    .registers 5
    .parameter "sessionID"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 373
    sget-boolean v1, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@3
    if-nez v1, :cond_6

    #@5
    .line 390
    :goto_5
    return v0

    #@6
    .line 377
    :cond_6
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_14

    #@c
    .line 378
    new-instance v1, Ljava/lang/SecurityException;

    #@e
    const-string v2, "Need proper permission to access drm"

    #@10
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@13
    throw v1

    #@14
    .line 381
    :cond_14
    if-eq p1, v0, :cond_1c

    #@16
    if-eqz p1, :cond_1c

    #@18
    iget v1, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mLockSessionID:I

    #@1a
    if-eq p1, v1, :cond_24

    #@1c
    .line 382
    :cond_1c
    const-string v1, "DrmFileLockClient"

    #@1e
    const-string v2, "closeFileLockSession() : sessionID is invalid."

    #@20
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    goto :goto_5

    #@24
    .line 386
    :cond_24
    invoke-static {p1}, Lcom/lge/lgdrm/DrmFileLockClient;->nativeCloseFileLockSession(I)I

    #@27
    move-result v0

    #@28
    .line 387
    .local v0, nResult:I
    const/4 v1, 0x0

    #@29
    iput v1, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mLockSessionID:I

    #@2b
    .line 388
    const-wide/16 v1, 0x0

    #@2d
    iput-wide v1, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mStartTime:J

    #@2f
    goto :goto_5
.end method

.method public closeFileUnlockSession(I)I
    .registers 5
    .parameter "sessionID"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 569
    sget-boolean v1, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@3
    if-nez v1, :cond_6

    #@5
    .line 586
    :goto_5
    return v0

    #@6
    .line 573
    :cond_6
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_14

    #@c
    .line 574
    new-instance v1, Ljava/lang/SecurityException;

    #@e
    const-string v2, "Need proper permission to access drm"

    #@10
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@13
    throw v1

    #@14
    .line 577
    :cond_14
    if-eq p1, v0, :cond_1c

    #@16
    if-eqz p1, :cond_1c

    #@18
    iget v1, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mUnlockSessionID:I

    #@1a
    if-eq p1, v1, :cond_24

    #@1c
    .line 578
    :cond_1c
    const-string v1, "DrmFileLockClient"

    #@1e
    const-string v2, "closeFileUnlockSession() : sessionID is invalid."

    #@20
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    goto :goto_5

    #@24
    .line 582
    :cond_24
    invoke-static {p1}, Lcom/lge/lgdrm/DrmFileLockClient;->nativeCloseFileUnlockSession(I)I

    #@27
    move-result v0

    #@28
    .line 583
    .local v0, nResult:I
    const/4 v1, 0x0

    #@29
    iput v1, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mUnlockSessionID:I

    #@2b
    .line 584
    const-wide/16 v1, 0x0

    #@2d
    iput-wide v1, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mStartTime:J

    #@2f
    goto :goto_5
.end method

.method public fileLock(I)I
    .registers 6
    .parameter "sessionID"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 302
    sget-boolean v2, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@3
    if-nez v2, :cond_7

    #@5
    move v0, v1

    #@6
    .line 326
    :cond_6
    :goto_6
    return v0

    #@7
    .line 306
    :cond_7
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@a
    move-result v2

    #@b
    if-nez v2, :cond_15

    #@d
    .line 307
    new-instance v1, Ljava/lang/SecurityException;

    #@f
    const-string v2, "Need proper permission to access drm"

    #@11
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@14
    throw v1

    #@15
    .line 310
    :cond_15
    if-eq p1, v1, :cond_1d

    #@17
    if-eqz p1, :cond_1d

    #@19
    iget v2, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mLockSessionID:I

    #@1b
    if-eq p1, v2, :cond_26

    #@1d
    .line 311
    :cond_1d
    const-string v2, "DrmFileLockClient"

    #@1f
    const-string v3, "fileLock() : sessionID is invalid."

    #@21
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    move v0, v1

    #@25
    .line 312
    goto :goto_6

    #@26
    .line 315
    :cond_26
    invoke-static {p1}, Lcom/lge/lgdrm/DrmFileLockClient;->nativeFileLock(I)I

    #@29
    move-result v0

    #@2a
    .line 317
    .local v0, nResult:I
    iget-object v2, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mOnEventListener:Lcom/lge/lgdrm/DrmFileLockClient$OnEventListener;

    #@2c
    if-eqz v2, :cond_6

    #@2e
    .line 318
    if-ne v1, v0, :cond_38

    #@30
    .line 319
    iget-object v1, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mOnEventListener:Lcom/lge/lgdrm/DrmFileLockClient$OnEventListener;

    #@32
    const/16 v2, -0xa

    #@34
    invoke-interface {v1, p0, v2}, Lcom/lge/lgdrm/DrmFileLockClient$OnEventListener;->onEvent(Lcom/lge/lgdrm/DrmFileLockClient;I)V

    #@37
    goto :goto_6

    #@38
    .line 322
    :cond_38
    iget-object v1, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mOnEventListener:Lcom/lge/lgdrm/DrmFileLockClient$OnEventListener;

    #@3a
    const/16 v2, 0xa

    #@3c
    invoke-interface {v1, p0, v2}, Lcom/lge/lgdrm/DrmFileLockClient$OnEventListener;->onEvent(Lcom/lge/lgdrm/DrmFileLockClient;I)V

    #@3f
    goto :goto_6
.end method

.method public fileUnlock(I)I
    .registers 6
    .parameter "sessionID"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 498
    sget-boolean v2, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@3
    if-nez v2, :cond_7

    #@5
    move v0, v1

    #@6
    .line 522
    :cond_6
    :goto_6
    return v0

    #@7
    .line 502
    :cond_7
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@a
    move-result v2

    #@b
    if-nez v2, :cond_15

    #@d
    .line 503
    new-instance v1, Ljava/lang/SecurityException;

    #@f
    const-string v2, "Need proper permission to access drm"

    #@11
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@14
    throw v1

    #@15
    .line 506
    :cond_15
    if-eq p1, v1, :cond_1d

    #@17
    if-eqz p1, :cond_1d

    #@19
    iget v2, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mUnlockSessionID:I

    #@1b
    if-eq p1, v2, :cond_26

    #@1d
    .line 507
    :cond_1d
    const-string v2, "DrmFileLockClient"

    #@1f
    const-string v3, "fileUnlock() : sessionID is invalid."

    #@21
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    move v0, v1

    #@25
    .line 508
    goto :goto_6

    #@26
    .line 511
    :cond_26
    invoke-static {p1}, Lcom/lge/lgdrm/DrmFileLockClient;->nativeFileUnlock(I)I

    #@29
    move-result v0

    #@2a
    .line 513
    .local v0, nResult:I
    iget-object v2, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mOnEventListener:Lcom/lge/lgdrm/DrmFileLockClient$OnEventListener;

    #@2c
    if-eqz v2, :cond_6

    #@2e
    .line 514
    if-ne v1, v0, :cond_38

    #@30
    .line 515
    iget-object v1, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mOnEventListener:Lcom/lge/lgdrm/DrmFileLockClient$OnEventListener;

    #@32
    const/16 v2, -0x14

    #@34
    invoke-interface {v1, p0, v2}, Lcom/lge/lgdrm/DrmFileLockClient$OnEventListener;->onEvent(Lcom/lge/lgdrm/DrmFileLockClient;I)V

    #@37
    goto :goto_6

    #@38
    .line 518
    :cond_38
    iget-object v1, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mOnEventListener:Lcom/lge/lgdrm/DrmFileLockClient$OnEventListener;

    #@3a
    const/16 v2, 0x14

    #@3c
    invoke-interface {v1, p0, v2}, Lcom/lge/lgdrm/DrmFileLockClient$OnEventListener;->onEvent(Lcom/lge/lgdrm/DrmFileLockClient;I)V

    #@3f
    goto :goto_6
.end method

.method protected finalize()V
    .registers 1

    #@0
    .prologue
    .line 591
    invoke-direct {p0}, Lcom/lge/lgdrm/DrmFileLockClient;->cleanSessions()V

    #@3
    .line 592
    return-void
.end method

.method public getDstFilePath()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 141
    iget-object v0, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mDstFilePath:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getRemainingTime(I)J
    .registers 16
    .parameter "sessionID"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    const-wide/16 v12, 0x0

    #@2
    const-wide/16 v6, -0x1

    #@4
    .line 153
    sget-boolean v10, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@6
    if-nez v10, :cond_9

    #@8
    .line 179
    :goto_8
    return-wide v6

    #@9
    .line 157
    :cond_9
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@c
    move-result v10

    #@d
    if-nez v10, :cond_17

    #@f
    .line 158
    new-instance v10, Ljava/lang/SecurityException;

    #@11
    const-string v11, "Need proper permission to access drm"

    #@13
    invoke-direct {v10, v11}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@16
    throw v10

    #@17
    .line 161
    :cond_17
    const/4 v10, -0x1

    #@18
    if-eq p1, v10, :cond_24

    #@1a
    if-eqz p1, :cond_24

    #@1c
    iget v10, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mLockSessionID:I

    #@1e
    if-eq p1, v10, :cond_2c

    #@20
    iget v10, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mUnlockSessionID:I

    #@22
    if-eq p1, v10, :cond_2c

    #@24
    .line 162
    :cond_24
    const-string v10, "DrmFileLockClient"

    #@26
    const-string v11, "getRemainingTime() : sessionID is invalid."

    #@28
    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    goto :goto_8

    #@2c
    .line 166
    :cond_2c
    new-instance v10, Ljava/io/File;

    #@2e
    iget-object v11, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mSrcFilePath:Ljava/lang/String;

    #@30
    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@33
    invoke-virtual {v10}, Ljava/io/File;->length()J

    #@36
    move-result-wide v8

    #@37
    .line 167
    .local v8, totalSize:J
    new-instance v10, Ljava/io/File;

    #@39
    iget-object v11, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mDstFilePath:Ljava/lang/String;

    #@3b
    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@3e
    invoke-virtual {v10}, Ljava/io/File;->length()J

    #@41
    move-result-wide v0

    #@42
    .line 168
    .local v0, currentSize:J
    cmp-long v10, v8, v12

    #@44
    if-eqz v10, :cond_4a

    #@46
    cmp-long v10, v0, v12

    #@48
    if-nez v10, :cond_52

    #@4a
    .line 169
    :cond_4a
    const-string v10, "DrmFileLockClient"

    #@4c
    const-string v11, "getRemainingTime() : file size is wrong"

    #@4e
    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    goto :goto_8

    #@52
    .line 172
    :cond_52
    const-wide/16 v10, 0x800

    #@54
    add-long/2addr v8, v10

    #@55
    .line 175
    new-instance v10, Ljava/util/Date;

    #@57
    invoke-direct {v10}, Ljava/util/Date;-><init>()V

    #@5a
    invoke-virtual {v10}, Ljava/util/Date;->getTime()J

    #@5d
    move-result-wide v4

    #@5e
    .line 176
    .local v4, now:J
    iget-wide v10, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mStartTime:J

    #@60
    sub-long v2, v4, v10

    #@62
    .line 177
    .local v2, elapsedTime:J
    long-to-double v10, v8

    #@63
    long-to-double v12, v0

    #@64
    div-double/2addr v10, v12

    #@65
    long-to-double v12, v2

    #@66
    mul-double/2addr v10, v12

    #@67
    double-to-long v10, v10

    #@68
    sub-long v6, v10, v2

    #@6a
    .line 179
    .local v6, result:J
    goto :goto_8
.end method

.method public getSrcFilePath()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 132
    iget-object v0, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mSrcFilePath:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public openFileLockSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I
    .registers 14
    .parameter "srcFile"
    .parameter "dstFile"
    .parameter "srcMimeType"
    .parameter "srcMediaType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 223
    const/4 v8, 0x0

    #@1
    .line 226
    .local v8, nResult:I
    const/high16 v5, 0x4

    #@3
    const/4 v6, 0x0

    #@4
    move-object v0, p0

    #@5
    move-object v1, p1

    #@6
    move-object v2, p2

    #@7
    move-object v3, p3

    #@8
    move v4, p4

    #@9
    :try_start_9
    invoke-virtual/range {v0 .. v6}, Lcom/lge/lgdrm/DrmFileLockClient;->openFileLockSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)I
    :try_end_c
    .catch Ljava/lang/SecurityException; {:try_start_9 .. :try_end_c} :catch_e

    #@c
    move-result v8

    #@d
    .line 231
    return v8

    #@e
    .line 227
    :catch_e
    move-exception v7

    #@f
    .line 228
    .local v7, e:Ljava/lang/SecurityException;
    throw v7
.end method

.method public openFileLockSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)I
    .registers 9
    .parameter "srcFile"
    .parameter "dstFile"
    .parameter "srcMimeType"
    .parameter "srcMediaType"
    .parameter "ioUnitSize"
    .parameter "sleepUSec"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 261
    sget-boolean v1, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@3
    if-nez v1, :cond_6

    #@5
    .line 288
    :cond_5
    :goto_5
    return v0

    #@6
    .line 265
    :cond_6
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_14

    #@c
    .line 266
    new-instance v0, Ljava/lang/SecurityException;

    #@e
    const-string v1, "Need proper permission to access drm"

    #@10
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 269
    :cond_14
    const/16 v1, 0x400

    #@16
    if-eq p5, v1, :cond_39

    #@18
    const/16 v1, 0x800

    #@1a
    if-eq p5, v1, :cond_39

    #@1c
    const/16 v1, 0x1000

    #@1e
    if-eq p5, v1, :cond_39

    #@20
    const/16 v1, 0x2000

    #@22
    if-eq p5, v1, :cond_39

    #@24
    const/16 v1, 0x4000

    #@26
    if-eq p5, v1, :cond_39

    #@28
    const v1, 0x8000

    #@2b
    if-eq p5, v1, :cond_39

    #@2d
    const/high16 v1, 0x1

    #@2f
    if-eq p5, v1, :cond_39

    #@31
    const/high16 v1, 0x2

    #@33
    if-eq p5, v1, :cond_39

    #@35
    const/high16 v1, 0x4

    #@37
    if-ne p5, v1, :cond_5

    #@39
    .line 277
    :cond_39
    if-ltz p6, :cond_5

    #@3b
    .line 281
    invoke-static/range {p1 .. p6}, Lcom/lge/lgdrm/DrmFileLockClient;->nativeOpenFileLockSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)I

    #@3e
    move-result v0

    #@3f
    iput v0, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mLockSessionID:I

    #@41
    .line 283
    iput-object p1, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mSrcFilePath:Ljava/lang/String;

    #@43
    .line 284
    iput-object p2, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mDstFilePath:Ljava/lang/String;

    #@45
    .line 286
    new-instance v0, Ljava/util/Date;

    #@47
    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    #@4a
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    #@4d
    move-result-wide v0

    #@4e
    iput-wide v0, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mStartTime:J

    #@50
    .line 288
    iget v0, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mLockSessionID:I

    #@52
    goto :goto_5
.end method

.method public openFileUnlockSession(Ljava/lang/String;Ljava/lang/String;)I
    .registers 7
    .parameter "srcFile"
    .parameter "dstFile"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 424
    const/4 v1, 0x0

    #@1
    .line 427
    .local v1, nResult:I
    const/high16 v2, 0x4

    #@3
    const/4 v3, 0x0

    #@4
    :try_start_4
    invoke-virtual {p0, p1, p2, v2, v3}, Lcom/lge/lgdrm/DrmFileLockClient;->openFileUnlockSession(Ljava/lang/String;Ljava/lang/String;II)I
    :try_end_7
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_7} :catch_9

    #@7
    move-result v1

    #@8
    .line 432
    return v1

    #@9
    .line 428
    :catch_9
    move-exception v0

    #@a
    .line 429
    .local v0, e:Ljava/lang/SecurityException;
    throw v0
.end method

.method public openFileUnlockSession(Ljava/lang/String;Ljava/lang/String;II)I
    .registers 7
    .parameter "srcFile"
    .parameter "dstFile"
    .parameter "ioUnitSize"
    .parameter "sleepUSec"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 457
    sget-boolean v1, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@3
    if-nez v1, :cond_6

    #@5
    .line 484
    :cond_5
    :goto_5
    return v0

    #@6
    .line 461
    :cond_6
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_14

    #@c
    .line 462
    new-instance v0, Ljava/lang/SecurityException;

    #@e
    const-string v1, "Need proper permission to access drm"

    #@10
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 465
    :cond_14
    const/16 v1, 0x400

    #@16
    if-eq p3, v1, :cond_39

    #@18
    const/16 v1, 0x800

    #@1a
    if-eq p3, v1, :cond_39

    #@1c
    const/16 v1, 0x1000

    #@1e
    if-eq p3, v1, :cond_39

    #@20
    const/16 v1, 0x2000

    #@22
    if-eq p3, v1, :cond_39

    #@24
    const/16 v1, 0x4000

    #@26
    if-eq p3, v1, :cond_39

    #@28
    const v1, 0x8000

    #@2b
    if-eq p3, v1, :cond_39

    #@2d
    const/high16 v1, 0x1

    #@2f
    if-eq p3, v1, :cond_39

    #@31
    const/high16 v1, 0x2

    #@33
    if-eq p3, v1, :cond_39

    #@35
    const/high16 v1, 0x4

    #@37
    if-ne p3, v1, :cond_5

    #@39
    .line 473
    :cond_39
    if-ltz p4, :cond_5

    #@3b
    .line 477
    invoke-static {p1, p2, p3, p4}, Lcom/lge/lgdrm/DrmFileLockClient;->nativeOpenFileUnlockSession(Ljava/lang/String;Ljava/lang/String;II)I

    #@3e
    move-result v0

    #@3f
    iput v0, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mUnlockSessionID:I

    #@41
    .line 479
    iput-object p1, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mSrcFilePath:Ljava/lang/String;

    #@43
    .line 480
    iput-object p2, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mDstFilePath:Ljava/lang/String;

    #@45
    .line 482
    new-instance v0, Ljava/util/Date;

    #@47
    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    #@4a
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    #@4d
    move-result-wide v0

    #@4e
    iput-wide v0, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mStartTime:J

    #@50
    .line 484
    iget v0, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mUnlockSessionID:I

    #@52
    goto :goto_5
.end method

.method public declared-synchronized setOnEventListener(Lcom/lge/lgdrm/DrmFileLockClient$OnEventListener;)V
    .registers 3
    .parameter "eventListener"

    #@0
    .prologue
    .line 121
    monitor-enter p0

    #@1
    if-eqz p1, :cond_5

    #@3
    .line 122
    :try_start_3
    iput-object p1, p0, Lcom/lge/lgdrm/DrmFileLockClient;->mOnEventListener:Lcom/lge/lgdrm/DrmFileLockClient$OnEventListener;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    #@5
    .line 124
    :cond_5
    monitor-exit p0

    #@6
    return-void

    #@7
    .line 121
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method
