.class public Lcom/lge/lgdrm/DrmFwExt$MediaFile;
.super Ljava/lang/Object;
.source "DrmFwExt.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/lgdrm/DrmFwExt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MediaFile"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/lgdrm/DrmFwExt$MediaFile$AVTypeList;,
        Lcom/lge/lgdrm/DrmFwExt$MediaFile$MediaFileType;
    }
.end annotation


# static fields
.field private static AVTypeCount:I

.field private static final sAVTypeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/lge/lgdrm/DrmFwExt$MediaFile$AVTypeList;",
            ">;"
        }
    .end annotation
.end field

.field private static final sFileTypeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/lge/lgdrm/DrmFwExt$MediaFile$MediaFileType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 261
    const/4 v0, 0x0

    #@1
    sput v0, Lcom/lge/lgdrm/DrmFwExt$MediaFile;->AVTypeCount:I

    #@3
    .line 265
    const-string v0, "init"

    #@5
    invoke-static {v0}, Landroid/media/MediaFile;->getFileTitle(Ljava/lang/String;)Ljava/lang/String;

    #@8
    .line 291
    new-instance v0, Ljava/util/HashMap;

    #@a
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@d
    sput-object v0, Lcom/lge/lgdrm/DrmFwExt$MediaFile;->sAVTypeMap:Ljava/util/HashMap;

    #@f
    .line 295
    new-instance v0, Ljava/util/HashMap;

    #@11
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@14
    sput-object v0, Lcom/lge/lgdrm/DrmFwExt$MediaFile;->sFileTypeMap:Ljava/util/HashMap;

    #@16
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 259
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 278
    return-void
.end method

.method public static addFileType(Ljava/lang/String;ILjava/lang/String;)V
    .registers 6
    .parameter "extension"
    .parameter "fileType"
    .parameter "mimeType"

    #@0
    .prologue
    .line 299
    sget-object v0, Lcom/lge/lgdrm/DrmFwExt$MediaFile;->sFileTypeMap:Ljava/util/HashMap;

    #@2
    new-instance v1, Lcom/lge/lgdrm/DrmFwExt$MediaFile$MediaFileType;

    #@4
    invoke-direct {v1, p1, p2}, Lcom/lge/lgdrm/DrmFwExt$MediaFile$MediaFileType;-><init>(ILjava/lang/String;)V

    #@7
    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    .line 300
    invoke-static {p1}, Landroid/media/MediaFile;->isAudioFileType(I)Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_16

    #@10
    invoke-static {p1}, Landroid/media/MediaFile;->isVideoFileType(I)Z

    #@13
    move-result v0

    #@14
    if-eqz v0, :cond_2a

    #@16
    .line 302
    :cond_16
    sget-object v0, Lcom/lge/lgdrm/DrmFwExt$MediaFile;->sAVTypeMap:Ljava/util/HashMap;

    #@18
    sget v1, Lcom/lge/lgdrm/DrmFwExt$MediaFile;->AVTypeCount:I

    #@1a
    add-int/lit8 v2, v1, 0x1

    #@1c
    sput v2, Lcom/lge/lgdrm/DrmFwExt$MediaFile;->AVTypeCount:I

    #@1e
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@21
    move-result-object v1

    #@22
    new-instance v2, Lcom/lge/lgdrm/DrmFwExt$MediaFile$AVTypeList;

    #@24
    invoke-direct {v2, p1, p2, p0}, Lcom/lge/lgdrm/DrmFwExt$MediaFile$AVTypeList;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    #@27
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2a
    .line 304
    :cond_2a
    return-void
.end method

.method public static getFileTypeFromDrm(Ljava/lang/String;)Lcom/lge/lgdrm/DrmFwExt$MediaFile$MediaFileType;
    .registers 14
    .parameter "path"

    #@0
    .prologue
    const/4 v12, 0x1

    #@1
    const/4 v11, 0x3

    #@2
    const/4 v10, 0x2

    #@3
    const/4 v7, 0x0

    #@4
    .line 307
    sget-boolean v8, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@6
    if-nez v8, :cond_a

    #@8
    move-object v6, v7

    #@9
    .line 382
    :cond_9
    :goto_9
    return-object v6

    #@a
    .line 310
    :cond_a
    invoke-static {v10, p0, v7}, Lcom/lge/lgdrm/DrmManager;->isSupportedExtension(ILjava/lang/String;Ljava/lang/String;)Z

    #@d
    move-result v8

    #@e
    if-nez v8, :cond_12

    #@10
    move-object v6, v7

    #@11
    .line 311
    goto :goto_9

    #@12
    .line 314
    :cond_12
    const-string v8, "."

    #@14
    invoke-virtual {p0, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    #@17
    move-result v4

    #@18
    .line 315
    .local v4, lastDot:I
    if-gez v4, :cond_1c

    #@1a
    move-object v6, v7

    #@1b
    .line 316
    goto :goto_9

    #@1c
    .line 318
    :cond_1c
    sget-object v8, Lcom/lge/lgdrm/DrmFwExt$MediaFile;->sFileTypeMap:Ljava/util/HashMap;

    #@1e
    add-int/lit8 v9, v4, 0x1

    #@20
    invoke-virtual {p0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@23
    move-result-object v9

    #@24
    invoke-virtual {v9}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@27
    move-result-object v9

    #@28
    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2b
    move-result-object v5

    #@2c
    check-cast v5, Lcom/lge/lgdrm/DrmFwExt$MediaFile$MediaFileType;

    #@2e
    .line 319
    .local v5, mediaType:Lcom/lge/lgdrm/DrmFwExt$MediaFile$MediaFileType;
    if-nez v5, :cond_39

    #@30
    .line 320
    const-string v8, "DrmFwExt"

    #@32
    const-string v9, "getFileTypeFromDrm : mediaType is null"

    #@34
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    move-object v6, v7

    #@38
    .line 321
    goto :goto_9

    #@39
    .line 323
    :cond_39
    iget v8, v5, Lcom/lge/lgdrm/DrmFwExt$MediaFile$MediaFileType;->fileType:I

    #@3b
    const/16 v9, 0x34

    #@3d
    if-lt v8, v9, :cond_45

    #@3f
    iget v8, v5, Lcom/lge/lgdrm/DrmFwExt$MediaFile$MediaFileType;->fileType:I

    #@41
    const/16 v9, 0x39

    #@43
    if-le v8, v9, :cond_47

    #@45
    :cond_45
    move-object v6, v7

    #@46
    .line 325
    goto :goto_9

    #@47
    .line 328
    :cond_47
    invoke-static {p0}, Lcom/lge/lgdrm/DrmManager;->isDRM(Ljava/lang/String;)I

    #@4a
    move-result v8

    #@4b
    if-nez v8, :cond_4f

    #@4d
    move-object v6, v7

    #@4e
    .line 329
    goto :goto_9

    #@4f
    .line 334
    :cond_4f
    :try_start_4f
    invoke-static {p0}, Lcom/lge/lgdrm/DrmContent;->getContentType(Ljava/lang/String;)I

    #@52
    move-result v1

    #@53
    .line 336
    .local v1, contentType:I
    const/4 v8, 0x2

    #@54
    invoke-static {p0, v8}, Lcom/lge/lgdrm/DrmContent;->getContentInfo(Ljava/lang/String;I)Ljava/lang/String;

    #@57
    move-result-object v2

    #@58
    .line 338
    .local v2, extension:Ljava/lang/String;
    if-eqz v2, :cond_60

    #@5a
    if-eq v1, v12, :cond_62

    #@5c
    if-eq v1, v10, :cond_62

    #@5e
    if-eq v1, v11, :cond_62

    #@60
    :cond_60
    move-object v6, v7

    #@61
    .line 344
    goto :goto_9

    #@62
    .line 346
    :cond_62
    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@65
    move-result-object v2

    #@66
    .line 348
    sget-object v8, Lcom/lge/lgdrm/DrmFwExt$MediaFile;->sFileTypeMap:Ljava/util/HashMap;

    #@68
    invoke-virtual {v8, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6b
    move-result-object v6

    #@6c
    check-cast v6, Lcom/lge/lgdrm/DrmFwExt$MediaFile$MediaFileType;

    #@6e
    .line 349
    .local v6, orgMediaType:Lcom/lge/lgdrm/DrmFwExt$MediaFile$MediaFileType;
    if-nez v6, :cond_72

    #@70
    move-object v6, v7

    #@71
    .line 350
    goto :goto_9

    #@72
    .line 354
    :cond_72
    if-eq v1, v12, :cond_9

    #@74
    .line 357
    if-ne v1, v10, :cond_7e

    #@76
    iget v8, v6, Lcom/lge/lgdrm/DrmFwExt$MediaFile$MediaFileType;->fileType:I

    #@78
    invoke-static {v8}, Landroid/media/MediaFile;->isAudioFileType(I)Z

    #@7b
    move-result v8

    #@7c
    if-nez v8, :cond_9

    #@7e
    :cond_7e
    if-ne v1, v11, :cond_88

    #@80
    iget v8, v6, Lcom/lge/lgdrm/DrmFwExt$MediaFile$MediaFileType;->fileType:I

    #@82
    invoke-static {v8}, Landroid/media/MediaFile;->isVideoFileType(I)Z

    #@85
    move-result v8

    #@86
    if-nez v8, :cond_9

    #@88
    .line 364
    :cond_88
    sget-object v8, Lcom/lge/lgdrm/DrmFwExt$MediaFile;->sAVTypeMap:Ljava/util/HashMap;

    #@8a
    invoke-virtual {v8}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@8d
    move-result-object v8

    #@8e
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@91
    move-result-object v3

    #@92
    .line 365
    .local v3, iterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/Integer;>;"
    const/4 v0, 0x0

    #@93
    .line 367
    .local v0, avType:Lcom/lge/lgdrm/DrmFwExt$MediaFile$AVTypeList;
    :cond_93
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@96
    move-result v8

    #@97
    if-eqz v8, :cond_c3

    #@99
    .line 368
    sget-object v8, Lcom/lge/lgdrm/DrmFwExt$MediaFile;->sAVTypeMap:Ljava/util/HashMap;

    #@9b
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@9e
    move-result-object v9

    #@9f
    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a2
    move-result-object v0

    #@a3
    .end local v0           #avType:Lcom/lge/lgdrm/DrmFwExt$MediaFile$AVTypeList;
    check-cast v0, Lcom/lge/lgdrm/DrmFwExt$MediaFile$AVTypeList;

    #@a5
    .line 369
    .restart local v0       #avType:Lcom/lge/lgdrm/DrmFwExt$MediaFile$AVTypeList;
    iget-object v8, v0, Lcom/lge/lgdrm/DrmFwExt$MediaFile$AVTypeList;->extension:Ljava/lang/String;

    #@a7
    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@aa
    move-result v8

    #@ab
    if-eqz v8, :cond_93

    #@ad
    .line 370
    if-ne v1, v10, :cond_c6

    #@af
    iget v8, v0, Lcom/lge/lgdrm/DrmFwExt$MediaFile$AVTypeList;->fileType:I

    #@b1
    invoke-static {v8}, Landroid/media/MediaFile;->isAudioFileType(I)Z

    #@b4
    move-result v8

    #@b5
    if-eqz v8, :cond_c6

    #@b7
    .line 371
    new-instance v6, Lcom/lge/lgdrm/DrmFwExt$MediaFile$MediaFileType;

    #@b9
    .end local v6           #orgMediaType:Lcom/lge/lgdrm/DrmFwExt$MediaFile$MediaFileType;
    iget v8, v0, Lcom/lge/lgdrm/DrmFwExt$MediaFile$AVTypeList;->fileType:I

    #@bb
    iget-object v9, v0, Lcom/lge/lgdrm/DrmFwExt$MediaFile$AVTypeList;->mimeType:Ljava/lang/String;

    #@bd
    invoke-direct {v6, v8, v9}, Lcom/lge/lgdrm/DrmFwExt$MediaFile$MediaFileType;-><init>(ILjava/lang/String;)V

    #@c0
    goto/16 :goto_9

    #@c2
    .line 378
    .end local v0           #avType:Lcom/lge/lgdrm/DrmFwExt$MediaFile$AVTypeList;
    .end local v1           #contentType:I
    .end local v2           #extension:Ljava/lang/String;
    .end local v3           #iterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :catch_c2
    move-exception v8

    #@c3
    :cond_c3
    move-object v6, v7

    #@c4
    .line 382
    goto/16 :goto_9

    #@c6
    .line 372
    .restart local v0       #avType:Lcom/lge/lgdrm/DrmFwExt$MediaFile$AVTypeList;
    .restart local v1       #contentType:I
    .restart local v2       #extension:Ljava/lang/String;
    .restart local v3       #iterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .restart local v6       #orgMediaType:Lcom/lge/lgdrm/DrmFwExt$MediaFile$MediaFileType;
    :cond_c6
    if-ne v1, v11, :cond_93

    #@c8
    iget v8, v0, Lcom/lge/lgdrm/DrmFwExt$MediaFile$AVTypeList;->fileType:I

    #@ca
    invoke-static {v8}, Landroid/media/MediaFile;->isVideoFileType(I)Z

    #@cd
    move-result v8

    #@ce
    if-eqz v8, :cond_93

    #@d0
    .line 373
    new-instance v6, Lcom/lge/lgdrm/DrmFwExt$MediaFile$MediaFileType;

    #@d2
    .end local v6           #orgMediaType:Lcom/lge/lgdrm/DrmFwExt$MediaFile$MediaFileType;
    iget v8, v0, Lcom/lge/lgdrm/DrmFwExt$MediaFile$AVTypeList;->fileType:I

    #@d4
    iget-object v9, v0, Lcom/lge/lgdrm/DrmFwExt$MediaFile$AVTypeList;->mimeType:Ljava/lang/String;

    #@d6
    invoke-direct {v6, v8, v9}, Lcom/lge/lgdrm/DrmFwExt$MediaFile$MediaFileType;-><init>(ILjava/lang/String;)V
    :try_end_d9
    .catch Ljava/lang/Exception; {:try_start_4f .. :try_end_d9} :catch_c2

    #@d9
    goto/16 :goto_9
.end method

.method public static getFileTypes()Ljava/util/HashMap;
    .registers 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/lge/lgdrm/DrmFwExt$MediaFile$MediaFileType;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 402
    sget-boolean v0, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@2
    if-nez v0, :cond_6

    #@4
    .line 403
    const/4 v0, 0x0

    #@5
    .line 406
    :goto_5
    return-object v0

    #@6
    :cond_6
    sget-object v0, Lcom/lge/lgdrm/DrmFwExt$MediaFile;->sFileTypeMap:Ljava/util/HashMap;

    #@8
    goto :goto_5
.end method

.method public static isAudioFileType(I)Z
    .registers 2
    .parameter "fileType"

    #@0
    .prologue
    .line 386
    invoke-static {p0}, Landroid/media/MediaFile;->isAudioFileType(I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static isImageFileType(I)Z
    .registers 2
    .parameter "fileType"

    #@0
    .prologue
    .line 394
    invoke-static {p0}, Landroid/media/MediaFile;->isImageFileType(I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static isPlayListFileType(I)Z
    .registers 2
    .parameter "fileType"

    #@0
    .prologue
    .line 398
    invoke-static {p0}, Landroid/media/MediaFile;->isPlayListFileType(I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static isVideoFileType(I)Z
    .registers 2
    .parameter "fileType"

    #@0
    .prologue
    .line 390
    invoke-static {p0}, Landroid/media/MediaFile;->isVideoFileType(I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method
