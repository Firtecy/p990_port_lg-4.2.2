.class public final Lcom/lge/lgdrm/DrmContentSession;
.super Ljava/lang/Object;
.source "DrmContentSession.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "DrmCntSes"


# instance fields
.field private contentList:[Lcom/lge/lgdrm/DrmContent;

.field private context:Landroid/content/Context;

.field private defaultContent:Lcom/lge/lgdrm/DrmContent;

.field private nativeSession:I

.field private nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

.field private selectedContent:Lcom/lge/lgdrm/DrmContent;


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 547
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 548
    return-void
.end method

.method protected constructor <init>(Lcom/lge/lgdrm/DrmContent;Landroid/content/Context;)V
    .registers 3
    .parameter "content"
    .parameter "context"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    #@0
    .prologue
    .line 559
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 561
    iput-object p1, p0, Lcom/lge/lgdrm/DrmContentSession;->defaultContent:Lcom/lge/lgdrm/DrmContent;

    #@5
    .line 562
    iput-object p2, p0, Lcom/lge/lgdrm/DrmContentSession;->context:Landroid/content/Context;

    #@7
    .line 563
    return-void
.end method

.method private activateContent(Ljava/lang/String;Ljava/lang/String;)I
    .registers 12
    .parameter "url"
    .parameter "resultReceiver"

    #@0
    .prologue
    const/high16 v8, 0x1000

    #@2
    const/16 v7, 0x9

    #@4
    const/4 v5, 0x0

    #@5
    const/4 v4, -0x1

    #@6
    .line 1554
    const/4 v3, 0x0

    #@7
    .line 1556
    .local v3, lguDRM:Z
    invoke-virtual {p0, v5}, Lcom/lge/lgdrm/DrmContentSession;->getSelectedContent(Z)Lcom/lge/lgdrm/DrmContent;

    #@a
    move-result-object v1

    #@b
    .line 1557
    .local v1, content:Lcom/lge/lgdrm/DrmContent;
    if-nez v1, :cond_e

    #@d
    .line 1637
    :cond_d
    :goto_d
    return v4

    #@e
    .line 1561
    :cond_e
    if-nez p1, :cond_14

    #@10
    iget v6, v1, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@12
    if-ne v6, v7, :cond_d

    #@14
    .line 1565
    :cond_14
    iget v6, v1, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@16
    if-ne v6, v7, :cond_4a

    #@18
    .line 1566
    new-instance v2, Landroid/content/Intent;

    #@1a
    const-string v6, "com.lge.lgdrm.action.DRM_LGU_ACTIVATION"

    #@1c
    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1f
    .line 1567
    .local v2, intent:Landroid/content/Intent;
    if-eqz v2, :cond_d

    #@21
    .line 1571
    const-string v4, "com.lge.lgdrm.extra.FILE_NAME"

    #@23
    iget-object v6, v1, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@25
    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@28
    .line 1572
    const-string v4, "com.lge.lgdrm.extra.CONTENT_TYPE"

    #@2a
    invoke-virtual {v1}, Lcom/lge/lgdrm/DrmContent;->getContentType()I

    #@2d
    move-result v6

    #@2e
    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@31
    .line 1573
    const-string v4, "com.lge.lgdrm.extra.DRM_TYPE"

    #@33
    invoke-virtual {v1}, Lcom/lge/lgdrm/DrmContent;->getDrmContentType()I

    #@36
    move-result v6

    #@37
    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@3a
    .line 1574
    iget-object v4, p0, Lcom/lge/lgdrm/DrmContentSession;->context:Landroid/content/Context;

    #@3c
    instance-of v4, v4, Landroid/app/Activity;

    #@3e
    if-nez v4, :cond_43

    #@40
    .line 1577
    invoke-virtual {v2, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@43
    .line 1579
    :cond_43
    iget-object v4, p0, Lcom/lge/lgdrm/DrmContentSession;->context:Landroid/content/Context;

    #@45
    invoke-virtual {v4, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@48
    move v4, v5

    #@49
    .line 1581
    goto :goto_d

    #@4a
    .line 1584
    .end local v2           #intent:Landroid/content/Intent;
    :cond_4a
    iget-object v6, p0, Lcom/lge/lgdrm/DrmContentSession;->context:Landroid/content/Context;

    #@4c
    instance-of v6, v6, Landroid/app/Activity;

    #@4e
    if-nez v6, :cond_57

    #@50
    .line 1585
    const-string v6, "DrmCntSes"

    #@52
    const-string v7, "Use activity context instead"

    #@54
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    .line 1588
    :cond_57
    invoke-virtual {v1}, Lcom/lge/lgdrm/DrmContent;->getDrmContentType()I

    #@5a
    move-result v6

    #@5b
    const/16 v7, 0x3000

    #@5d
    if-ne v6, v7, :cond_94

    #@5f
    iget-object v6, p0, Lcom/lge/lgdrm/DrmContentSession;->context:Landroid/content/Context;

    #@61
    instance-of v6, v6, Landroid/app/Activity;

    #@63
    if-eqz v6, :cond_94

    #@65
    .line 1595
    new-instance v2, Landroid/content/Intent;

    #@67
    const-string v6, "com.lge.lgdrm.action.DRM_ACTIVATION"

    #@69
    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@6c
    .line 1596
    .restart local v2       #intent:Landroid/content/Intent;
    if-eqz v2, :cond_d

    #@6e
    .line 1600
    const-string v4, "com.lge.lgdrm.extra.ACTIVATION_URL"

    #@70
    invoke-virtual {v2, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@73
    .line 1601
    const-string v4, "com.lge.lgdrm.extra.FILE_NAME"

    #@75
    iget-object v6, v1, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@77
    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@7a
    .line 1602
    const-string v4, "com.lge.lgdrm.extra.CONTENT_TYPE"

    #@7c
    invoke-virtual {v1}, Lcom/lge/lgdrm/DrmContent;->getContentType()I

    #@7f
    move-result v6

    #@80
    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@83
    .line 1603
    const-string v4, "com.lge.lgdrm.extra.DRM_TYPE"

    #@85
    invoke-virtual {v1}, Lcom/lge/lgdrm/DrmContent;->getDrmContentType()I

    #@88
    move-result v6

    #@89
    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@8c
    .line 1604
    iget-object v4, p0, Lcom/lge/lgdrm/DrmContentSession;->context:Landroid/content/Context;

    #@8e
    invoke-virtual {v4, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@91
    move v4, v5

    #@92
    .line 1606
    goto/16 :goto_d

    #@94
    .line 1610
    .end local v2           #intent:Landroid/content/Intent;
    :cond_94
    new-instance v2, Landroid/content/Intent;

    #@96
    const-string v6, "android.intent.action.VIEW"

    #@98
    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@9b
    .line 1611
    .restart local v2       #intent:Landroid/content/Intent;
    if-eqz v2, :cond_d

    #@9d
    .line 1616
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@a0
    move-result-object v6

    #@a1
    invoke-virtual {v2, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    #@a4
    .line 1617
    const-string v6, "com.android.browser"

    #@a6
    const-string v7, "com.android.browser.BrowserActivity"

    #@a8
    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@ab
    .line 1618
    invoke-virtual {v2, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@ae
    .line 1621
    const-string v6, "com.lge.lgdrm.extra.FILE_NAME"

    #@b0
    iget-object v7, v1, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@b2
    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@b5
    .line 1623
    if-eqz p2, :cond_c4

    #@b7
    .line 1625
    const-string v6, ";"

    #@b9
    invoke-virtual {p2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@bc
    move-result-object v0

    #@bd
    .line 1626
    .local v0, component:[Ljava/lang/String;
    if-eqz v0, :cond_d

    #@bf
    .line 1630
    const-string v4, "com.lge.lgdrm.extra.RECEIVER_NAME"

    #@c1
    invoke-virtual {v2, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    #@c4
    .line 1634
    .end local v0           #component:[Ljava/lang/String;
    :cond_c4
    iget-object v4, p0, Lcom/lge/lgdrm/DrmContentSession;->context:Landroid/content/Context;

    #@c6
    invoke-virtual {v4, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@c9
    move v4, v5

    #@ca
    .line 1637
    goto/16 :goto_d
.end method

.method public static getDrmTimeStatus(I)Z
    .registers 5
    .parameter "agentType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 886
    sget-boolean v3, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@4
    if-nez v3, :cond_7

    #@6
    .line 895
    :goto_6
    return v2

    #@7
    .line 890
    :cond_7
    if-lt p0, v1, :cond_d

    #@9
    const/16 v3, 0xa

    #@b
    if-le p0, v3, :cond_15

    #@d
    .line 891
    :cond_d
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@f
    const-string v2, "Invalid agentType"

    #@11
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@14
    throw v1

    #@15
    .line 894
    :cond_15
    invoke-static {p0}, Lcom/lge/lgdrm/DrmContentSession;->nativeGetDrmTimeStatus(I)I

    #@18
    move-result v0

    #@19
    .line 895
    .local v0, status:I
    if-nez v0, :cond_1d

    #@1b
    :goto_1b
    move v2, v1

    #@1c
    goto :goto_6

    #@1d
    :cond_1d
    move v1, v2

    #@1e
    goto :goto_1b
.end method

.method private native nativeActivateContent(ILjava/lang/String;ILjava/lang/String;)I
.end method

.method private native nativeConsumeRight(Ljava/lang/String;IIIJJ)I
.end method

.method private native nativeDestroySession(II)V
.end method

.method private native nativeDoRoap(ILjava/lang/String;ILjava/lang/String;)I
.end method

.method private static native nativeDrmOpen(Ljava/lang/String;ILjava/io/FileDescriptor;I[B)I
.end method

.method private native nativeGetContentBasicInfo(Lcom/lge/lgdrm/DrmContent;Ljava/lang/String;I)I
.end method

.method private native nativeGetContentList(Ljava/lang/String;)[Lcom/lge/lgdrm/DrmContent;
.end method

.method private native nativeGetDecryptionInfo(ILjava/lang/String;II)[B
.end method

.method private native nativeGetDrmTime()J
.end method

.method private static native nativeGetDrmTimeStatus(I)I
.end method

.method private native nativeGetIndexByCID(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method private native nativeGetRightInfo(Lcom/lge/lgdrm/DrmRight;Ljava/lang/String;III)I
.end method

.method private native nativeIsSettingsAvailable(Ljava/lang/String;II)I
.end method

.method private native nativeJudgeRight(Ljava/lang/String;III)I
.end method

.method private native nativeSetDecryptionInfo(ZILjava/lang/String;II)I
.end method

.method private static native nativeSetDrmTime(IJ)I
.end method

.method private native nativeSyncDrmTime(Ljava/lang/String;ILjava/lang/String;)I
.end method

.method public static openDrmStream(Ljava/io/FileDescriptor;[B)Lcom/lge/lgdrm/DrmStream;
    .registers 6
    .parameter "fd"
    .parameter "decInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 1352
    sget-boolean v2, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@4
    if-nez v2, :cond_7

    #@6
    .line 1368
    :cond_6
    :goto_6
    return-object v1

    #@7
    .line 1356
    :cond_7
    if-nez p0, :cond_11

    #@9
    .line 1357
    new-instance v1, Ljava/lang/NullPointerException;

    #@b
    const-string v2, "Parameter fd is null"

    #@d
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@10
    throw v1

    #@11
    .line 1359
    :cond_11
    if-nez p1, :cond_1b

    #@13
    .line 1360
    new-instance v1, Ljava/lang/NullPointerException;

    #@15
    const-string v2, "Parameter decInfo is null"

    #@17
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v1

    #@1b
    .line 1363
    :cond_1b
    invoke-static {v1, v3, p0, v3, p1}, Lcom/lge/lgdrm/DrmContentSession;->nativeDrmOpen(Ljava/lang/String;ILjava/io/FileDescriptor;I[B)I

    #@1e
    move-result v0

    #@1f
    .line 1364
    .local v0, nativeHandle:I
    if-eqz v0, :cond_6

    #@21
    .line 1368
    new-instance v2, Lcom/lge/lgdrm/DrmStream;

    #@23
    invoke-direct {v2, v1, v0}, Lcom/lge/lgdrm/DrmStream;-><init>(Ljava/lang/String;I)V

    #@26
    move-object v1, v2

    #@27
    goto :goto_6
.end method

.method public static openDrmStream(Ljava/lang/String;[B)Lcom/lge/lgdrm/DrmStream;
    .registers 6
    .parameter "filename"
    .parameter "decInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 1320
    sget-boolean v2, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@4
    if-nez v2, :cond_7

    #@6
    .line 1336
    :cond_6
    :goto_6
    return-object v1

    #@7
    .line 1324
    :cond_7
    if-nez p0, :cond_11

    #@9
    .line 1325
    new-instance v1, Ljava/lang/NullPointerException;

    #@b
    const-string v2, "Parameter filename is null"

    #@d
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@10
    throw v1

    #@11
    .line 1327
    :cond_11
    if-nez p1, :cond_1b

    #@13
    .line 1328
    new-instance v1, Ljava/lang/NullPointerException;

    #@15
    const-string v2, "Parameter decInfo is null"

    #@17
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v1

    #@1b
    .line 1331
    :cond_1b
    invoke-static {p0, v3, v1, v3, p1}, Lcom/lge/lgdrm/DrmContentSession;->nativeDrmOpen(Ljava/lang/String;ILjava/io/FileDescriptor;I[B)I

    #@1e
    move-result v0

    #@1f
    .line 1332
    .local v0, nativeHandle:I
    if-eqz v0, :cond_6

    #@21
    .line 1336
    new-instance v1, Lcom/lge/lgdrm/DrmStream;

    #@23
    invoke-direct {v1, p0, v0}, Lcom/lge/lgdrm/DrmStream;-><init>(Ljava/lang/String;I)V

    #@26
    goto :goto_6
.end method

.method public static setDrmTime(IJ)I
    .registers 5
    .parameter "agentType"
    .parameter "time"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 981
    sget-boolean v0, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@2
    if-nez v0, :cond_6

    #@4
    .line 982
    const/4 v0, -0x1

    #@5
    .line 996
    :goto_5
    return v0

    #@6
    .line 985
    :cond_6
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_14

    #@c
    .line 986
    new-instance v0, Ljava/lang/SecurityException;

    #@e
    const-string v1, "Need proper permission to access drm"

    #@10
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 989
    :cond_14
    const/4 v0, 0x1

    #@15
    if-lt p0, v0, :cond_1b

    #@17
    const/16 v0, 0xa

    #@19
    if-le p0, v0, :cond_23

    #@1b
    .line 990
    :cond_1b
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1d
    const-string v1, "Invalid agentType"

    #@1f
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@22
    throw v0

    #@23
    .line 992
    :cond_23
    const-wide/16 v0, 0x0

    #@25
    cmp-long v0, p1, v0

    #@27
    if-nez v0, :cond_31

    #@29
    .line 993
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@2b
    const-string v1, "Invalid time"

    #@2d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@30
    throw v0

    #@31
    .line 996
    :cond_31
    invoke-static {p0, p1, p2}, Lcom/lge/lgdrm/DrmContentSession;->nativeSetDrmTime(IJ)I

    #@34
    move-result v0

    #@35
    goto :goto_5
.end method

.method private setNextRequest(IIILjava/lang/String;[B)I
    .registers 7
    .parameter "session"
    .parameter "requestType"
    .parameter "httpMethod"
    .parameter "url"
    .parameter "data"

    #@0
    .prologue
    .line 1657
    iput p1, p0, Lcom/lge/lgdrm/DrmContentSession;->nativeSession:I

    #@2
    .line 1660
    new-instance v0, Lcom/lge/lgdrm/DrmDldRequest;

    #@4
    invoke-direct {v0}, Lcom/lge/lgdrm/DrmDldRequest;-><init>()V

    #@7
    iput-object v0, p0, Lcom/lge/lgdrm/DrmContentSession;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

    #@9
    .line 1661
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContentSession;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

    #@b
    if-nez v0, :cond_f

    #@d
    .line 1662
    const/4 v0, -0x1

    #@e
    .line 1669
    :goto_e
    return v0

    #@f
    .line 1664
    :cond_f
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContentSession;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

    #@11
    iput p2, v0, Lcom/lge/lgdrm/DrmDldRequest;->requestType:I

    #@13
    .line 1665
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContentSession;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

    #@15
    iput p3, v0, Lcom/lge/lgdrm/DrmDldRequest;->httpMethod:I

    #@17
    .line 1666
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContentSession;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

    #@19
    iput-object p4, v0, Lcom/lge/lgdrm/DrmDldRequest;->url:Ljava/lang/String;

    #@1b
    .line 1667
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContentSession;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

    #@1d
    iput-object p5, v0, Lcom/lge/lgdrm/DrmDldRequest;->data:[B

    #@1f
    .line 1669
    const/4 v0, 0x0

    #@20
    goto :goto_e
.end method

.method private startDldClient(Lcom/lge/lgdrm/DrmContent;)I
    .registers 11
    .parameter "content"

    #@0
    .prologue
    const/4 v4, -0x1

    #@1
    const/4 v8, 0x0

    #@2
    const/4 v3, 0x3

    #@3
    const/4 v5, 0x0

    #@4
    .line 1684
    iget v6, p1, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@6
    const/4 v7, 0x2

    #@7
    if-ne v6, v7, :cond_20

    #@9
    move v1, v3

    #@a
    .line 1686
    .local v1, downloadAgent:I
    :goto_a
    new-instance v2, Lcom/lge/lgdrm/DrmObjectSession;

    #@c
    iget-object v6, p0, Lcom/lge/lgdrm/DrmContentSession;->context:Landroid/content/Context;

    #@e
    iget v7, p0, Lcom/lge/lgdrm/DrmContentSession;->nativeSession:I

    #@10
    invoke-direct {v2, v1, v6, v7}, Lcom/lge/lgdrm/DrmObjectSession;-><init>(ILandroid/content/Context;I)V

    #@13
    .line 1687
    .local v2, session:Lcom/lge/lgdrm/DrmObjectSession;
    if-nez v2, :cond_22

    #@15
    .line 1688
    iget v6, p0, Lcom/lge/lgdrm/DrmContentSession;->nativeSession:I

    #@17
    invoke-direct {p0, v6, v3}, Lcom/lge/lgdrm/DrmContentSession;->nativeDestroySession(II)V

    #@1a
    .line 1689
    iput v5, p0, Lcom/lge/lgdrm/DrmContentSession;->nativeSession:I

    #@1c
    .line 1690
    iput-object v8, p0, Lcom/lge/lgdrm/DrmContentSession;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

    #@1e
    move v3, v4

    #@1f
    .line 1713
    :goto_1f
    return v3

    #@20
    .line 1684
    .end local v1           #downloadAgent:I
    .end local v2           #session:Lcom/lge/lgdrm/DrmObjectSession;
    :cond_20
    const/4 v1, 0x4

    #@21
    goto :goto_a

    #@22
    .line 1697
    .restart local v1       #downloadAgent:I
    .restart local v2       #session:Lcom/lge/lgdrm/DrmObjectSession;
    :cond_22
    new-instance v0, Lcom/lge/lgdrm/DrmDldClient;

    #@24
    iget-object v6, p0, Lcom/lge/lgdrm/DrmContentSession;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

    #@26
    iget-object v7, p0, Lcom/lge/lgdrm/DrmContentSession;->context:Landroid/content/Context;

    #@28
    invoke-direct {v0, v2, v6, v7}, Lcom/lge/lgdrm/DrmDldClient;-><init>(Lcom/lge/lgdrm/DrmObjectSession;Lcom/lge/lgdrm/DrmDldRequest;Landroid/content/Context;)V

    #@2b
    .line 1698
    .local v0, dldClient:Lcom/lge/lgdrm/DrmDldClient;
    if-nez v0, :cond_38

    #@2d
    .line 1700
    iget v6, p0, Lcom/lge/lgdrm/DrmContentSession;->nativeSession:I

    #@2f
    invoke-direct {p0, v6, v3}, Lcom/lge/lgdrm/DrmContentSession;->nativeDestroySession(II)V

    #@32
    .line 1701
    iput v5, p0, Lcom/lge/lgdrm/DrmContentSession;->nativeSession:I

    #@34
    .line 1702
    iput-object v8, p0, Lcom/lge/lgdrm/DrmContentSession;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

    #@36
    move v3, v4

    #@37
    .line 1703
    goto :goto_1f

    #@38
    .line 1707
    :cond_38
    iput v5, p0, Lcom/lge/lgdrm/DrmContentSession;->nativeSession:I

    #@3a
    .line 1708
    iput-object v8, p0, Lcom/lge/lgdrm/DrmContentSession;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

    #@3c
    .line 1711
    invoke-virtual {v0}, Lcom/lge/lgdrm/DrmDldClient;->start()V

    #@3f
    move v3, v5

    #@40
    .line 1713
    goto :goto_1f
.end method


# virtual methods
.method public consumeRight(JJ)I
    .registers 15
    .parameter "startTime"
    .parameter "usedTime"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const-wide/16 v2, 0x0

    #@3
    .line 1150
    cmp-long v1, p1, v2

    #@5
    if-ltz v1, :cond_b

    #@7
    cmp-long v1, p3, v2

    #@9
    if-gez v1, :cond_13

    #@b
    .line 1151
    :cond_b
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@d
    const-string v1, "Invalid time"

    #@f
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@12
    throw v0

    #@13
    .line 1153
    :cond_13
    cmp-long v1, p1, v2

    #@15
    if-nez v1, :cond_23

    #@17
    cmp-long v1, p3, v2

    #@19
    if-nez v1, :cond_23

    #@1b
    .line 1154
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1d
    const-string v1, "Invalid time"

    #@1f
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@22
    throw v0

    #@23
    .line 1157
    :cond_23
    invoke-virtual {p0, v0}, Lcom/lge/lgdrm/DrmContentSession;->getSelectedContent(Z)Lcom/lge/lgdrm/DrmContent;

    #@26
    move-result-object v9

    #@27
    .line 1158
    .local v9, content:Lcom/lge/lgdrm/DrmContent;
    if-nez v9, :cond_2b

    #@29
    .line 1159
    const/4 v0, -0x1

    #@2a
    .line 1171
    :cond_2a
    :goto_2a
    return v0

    #@2b
    .line 1162
    :cond_2b
    iget v1, v9, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@2d
    const/16 v2, 0x9

    #@2f
    if-eq v1, v2, :cond_2a

    #@31
    iget v1, v9, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@33
    const/16 v2, 0xa

    #@35
    if-eq v1, v2, :cond_2a

    #@37
    .line 1166
    iget v0, v9, Lcom/lge/lgdrm/DrmContent;->rightState:I

    #@39
    const/4 v1, -0x4

    #@3a
    if-ne v0, v1, :cond_48

    #@3c
    iget-boolean v0, v9, Lcom/lge/lgdrm/DrmContent;->autoRightState:Z

    #@3e
    if-nez v0, :cond_48

    #@40
    .line 1168
    new-instance v0, Ljava/lang/IllegalStateException;

    #@42
    const-string v1, "Select Right first"

    #@44
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@47
    throw v0

    #@48
    .line 1171
    :cond_48
    iget-object v1, v9, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@4a
    iget v2, v9, Lcom/lge/lgdrm/DrmContent;->index:I

    #@4c
    iget v3, v9, Lcom/lge/lgdrm/DrmContent;->previewContent:I

    #@4e
    iget v4, v9, Lcom/lge/lgdrm/DrmContent;->permisson:I

    #@50
    move-object v0, p0

    #@51
    move-wide v5, p1

    #@52
    move-wide v7, p3

    #@53
    invoke-direct/range {v0 .. v8}, Lcom/lge/lgdrm/DrmContentSession;->nativeConsumeRight(Ljava/lang/String;IIIJJ)I

    #@56
    move-result v0

    #@57
    goto :goto_2a
.end method

.method public destroySession()V
    .registers 1

    #@0
    .prologue
    .line 582
    return-void
.end method

.method public getContent(Ljava/lang/String;)Lcom/lge/lgdrm/DrmContent;
    .registers 7
    .parameter "cid"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 597
    if-nez p1, :cond_b

    #@3
    .line 598
    new-instance v2, Ljava/lang/NullPointerException;

    #@5
    const-string v3, "Parameter cid is null"

    #@7
    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v2

    #@b
    .line 601
    :cond_b
    iget-object v3, p0, Lcom/lge/lgdrm/DrmContentSession;->defaultContent:Lcom/lge/lgdrm/DrmContent;

    #@d
    if-nez v3, :cond_11

    #@f
    move-object v0, v2

    #@10
    .line 626
    :cond_10
    :goto_10
    return-object v0

    #@11
    .line 604
    :cond_11
    iget-object v3, p0, Lcom/lge/lgdrm/DrmContentSession;->defaultContent:Lcom/lge/lgdrm/DrmContent;

    #@13
    iget v3, v3, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@15
    const/4 v4, 0x2

    #@16
    if-eq v3, v4, :cond_1a

    #@18
    move-object v0, v2

    #@19
    .line 605
    goto :goto_10

    #@1a
    .line 608
    :cond_1a
    iget-object v3, p0, Lcom/lge/lgdrm/DrmContentSession;->defaultContent:Lcom/lge/lgdrm/DrmContent;

    #@1c
    iget-object v3, v3, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@1e
    invoke-direct {p0, v3, p1}, Lcom/lge/lgdrm/DrmContentSession;->nativeGetIndexByCID(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    move-result v1

    #@22
    .line 609
    .local v1, index:I
    const/4 v3, -0x1

    #@23
    if-ne v1, v3, :cond_27

    #@25
    move-object v0, v2

    #@26
    .line 610
    goto :goto_10

    #@27
    .line 617
    :cond_27
    new-instance v0, Lcom/lge/lgdrm/DrmContent;

    #@29
    iget-object v3, p0, Lcom/lge/lgdrm/DrmContentSession;->defaultContent:Lcom/lge/lgdrm/DrmContent;

    #@2b
    iget-object v3, v3, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@2d
    iget-object v4, p0, Lcom/lge/lgdrm/DrmContentSession;->defaultContent:Lcom/lge/lgdrm/DrmContent;

    #@2f
    iget v4, v4, Lcom/lge/lgdrm/DrmContent;->contentType:I

    #@31
    invoke-direct {v0, v3, v1, v4}, Lcom/lge/lgdrm/DrmContent;-><init>(Ljava/lang/String;II)V

    #@34
    .line 618
    .local v0, content:Lcom/lge/lgdrm/DrmContent;
    if-nez v0, :cond_38

    #@36
    move-object v0, v2

    #@37
    .line 619
    goto :goto_10

    #@38
    .line 622
    :cond_38
    iget-object v3, p0, Lcom/lge/lgdrm/DrmContentSession;->defaultContent:Lcom/lge/lgdrm/DrmContent;

    #@3a
    iget-object v3, v3, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@3c
    invoke-direct {p0, v0, v3, v1}, Lcom/lge/lgdrm/DrmContentSession;->nativeGetContentBasicInfo(Lcom/lge/lgdrm/DrmContent;Ljava/lang/String;I)I

    #@3f
    move-result v3

    #@40
    if-eqz v3, :cond_10

    #@42
    move-object v0, v2

    #@43
    .line 623
    goto :goto_10
.end method

.method public getContentList()[Lcom/lge/lgdrm/DrmContent;
    .registers 6

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v4, 0x0

    #@2
    .line 637
    iget-object v2, p0, Lcom/lge/lgdrm/DrmContentSession;->defaultContent:Lcom/lge/lgdrm/DrmContent;

    #@4
    if-nez v2, :cond_8

    #@6
    move-object v0, v1

    #@7
    .line 680
    :goto_7
    return-object v0

    #@8
    .line 642
    :cond_8
    iget-object v2, p0, Lcom/lge/lgdrm/DrmContentSession;->contentList:[Lcom/lge/lgdrm/DrmContent;

    #@a
    if-eqz v2, :cond_1e

    #@c
    .line 643
    iget-object v2, p0, Lcom/lge/lgdrm/DrmContentSession;->contentList:[Lcom/lge/lgdrm/DrmContent;

    #@e
    array-length v2, v2

    #@f
    new-array v0, v2, [Lcom/lge/lgdrm/DrmContent;

    #@11
    .line 644
    .local v0, list:[Lcom/lge/lgdrm/DrmContent;
    if-nez v0, :cond_15

    #@13
    move-object v0, v1

    #@14
    .line 646
    goto :goto_7

    #@15
    .line 650
    :cond_15
    iget-object v1, p0, Lcom/lge/lgdrm/DrmContentSession;->contentList:[Lcom/lge/lgdrm/DrmContent;

    #@17
    iget-object v2, p0, Lcom/lge/lgdrm/DrmContentSession;->contentList:[Lcom/lge/lgdrm/DrmContent;

    #@19
    array-length v2, v2

    #@1a
    invoke-static {v1, v4, v0, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@1d
    goto :goto_7

    #@1e
    .line 654
    .end local v0           #list:[Lcom/lge/lgdrm/DrmContent;
    :cond_1e
    iget-object v2, p0, Lcom/lge/lgdrm/DrmContentSession;->defaultContent:Lcom/lge/lgdrm/DrmContent;

    #@20
    iget v2, v2, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@22
    const/4 v3, 0x2

    #@23
    if-eq v2, v3, :cond_31

    #@25
    .line 655
    const/4 v2, 0x1

    #@26
    new-array v0, v2, [Lcom/lge/lgdrm/DrmContent;

    #@28
    .line 656
    .restart local v0       #list:[Lcom/lge/lgdrm/DrmContent;
    if-nez v0, :cond_2c

    #@2a
    move-object v0, v1

    #@2b
    .line 658
    goto :goto_7

    #@2c
    .line 660
    :cond_2c
    iget-object v1, p0, Lcom/lge/lgdrm/DrmContentSession;->defaultContent:Lcom/lge/lgdrm/DrmContent;

    #@2e
    aput-object v1, v0, v4

    #@30
    goto :goto_7

    #@31
    .line 665
    .end local v0           #list:[Lcom/lge/lgdrm/DrmContent;
    :cond_31
    iget-object v2, p0, Lcom/lge/lgdrm/DrmContentSession;->defaultContent:Lcom/lge/lgdrm/DrmContent;

    #@33
    iget-object v2, v2, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@35
    invoke-direct {p0, v2}, Lcom/lge/lgdrm/DrmContentSession;->nativeGetContentList(Ljava/lang/String;)[Lcom/lge/lgdrm/DrmContent;

    #@38
    move-result-object v0

    #@39
    .line 666
    .restart local v0       #list:[Lcom/lge/lgdrm/DrmContent;
    if-nez v0, :cond_3d

    #@3b
    move-object v0, v1

    #@3c
    .line 667
    goto :goto_7

    #@3d
    .line 670
    :cond_3d
    iget-object v2, p0, Lcom/lge/lgdrm/DrmContentSession;->defaultContent:Lcom/lge/lgdrm/DrmContent;

    #@3f
    aput-object v2, v0, v4

    #@41
    .line 672
    array-length v2, v0

    #@42
    new-array v2, v2, [Lcom/lge/lgdrm/DrmContent;

    #@44
    iput-object v2, p0, Lcom/lge/lgdrm/DrmContentSession;->contentList:[Lcom/lge/lgdrm/DrmContent;

    #@46
    .line 673
    iget-object v2, p0, Lcom/lge/lgdrm/DrmContentSession;->contentList:[Lcom/lge/lgdrm/DrmContent;

    #@48
    if-nez v2, :cond_4c

    #@4a
    move-object v0, v1

    #@4b
    .line 674
    goto :goto_7

    #@4c
    .line 678
    :cond_4c
    iget-object v1, p0, Lcom/lge/lgdrm/DrmContentSession;->contentList:[Lcom/lge/lgdrm/DrmContent;

    #@4e
    array-length v2, v0

    #@4f
    invoke-static {v0, v4, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@52
    goto :goto_7
.end method

.method public getDecryptionInfo()[B
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1385
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_e

    #@6
    .line 1386
    new-instance v1, Ljava/lang/SecurityException;

    #@8
    const-string v2, "Need proper permission to access drm"

    #@a
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@d
    throw v1

    #@e
    .line 1389
    :cond_e
    const/4 v1, 0x0

    #@f
    invoke-virtual {p0, v1}, Lcom/lge/lgdrm/DrmContentSession;->getSelectedContent(Z)Lcom/lge/lgdrm/DrmContent;

    #@12
    move-result-object v0

    #@13
    .line 1390
    .local v0, content:Lcom/lge/lgdrm/DrmContent;
    if-nez v0, :cond_17

    #@15
    .line 1391
    const/4 v1, 0x0

    #@16
    .line 1399
    :goto_16
    return-object v1

    #@17
    .line 1394
    :cond_17
    iget v1, v0, Lcom/lge/lgdrm/DrmContent;->rightState:I

    #@19
    const/4 v2, -0x4

    #@1a
    if-ne v1, v2, :cond_28

    #@1c
    iget-boolean v1, v0, Lcom/lge/lgdrm/DrmContent;->autoRightState:Z

    #@1e
    if-nez v1, :cond_28

    #@20
    .line 1396
    new-instance v1, Ljava/lang/IllegalStateException;

    #@22
    const-string v2, "Select Right first"

    #@24
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@27
    throw v1

    #@28
    .line 1399
    :cond_28
    iget v1, v0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@2a
    iget-object v2, v0, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@2c
    iget v3, v0, Lcom/lge/lgdrm/DrmContent;->index:I

    #@2e
    iget v4, v0, Lcom/lge/lgdrm/DrmContent;->previewContent:I

    #@30
    invoke-direct {p0, v1, v2, v3, v4}, Lcom/lge/lgdrm/DrmContentSession;->nativeGetDecryptionInfo(ILjava/lang/String;II)[B

    #@33
    move-result-object v1

    #@34
    goto :goto_16
.end method

.method public getDrmTime()J
    .registers 3

    #@0
    .prologue
    .line 867
    invoke-direct {p0}, Lcom/lge/lgdrm/DrmContentSession;->nativeGetDrmTime()J

    #@3
    move-result-wide v0

    #@4
    return-wide v0
.end method

.method public getRightList(I)[Lcom/lge/lgdrm/DrmRight;
    .registers 4
    .parameter "permission"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 768
    const/4 v0, 0x1

    #@1
    if-lt p1, v0, :cond_7

    #@3
    const/16 v0, 0x80

    #@5
    if-le p1, v0, :cond_f

    #@7
    .line 769
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@9
    const-string v1, "Invalid permission"

    #@b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 771
    :cond_f
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@11
    const-string v1, "Rights selection is not supported"

    #@13
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@16
    throw v0
.end method

.method public getSelectedContent(Z)Lcom/lge/lgdrm/DrmContent;
    .registers 4
    .parameter "defaults"

    #@0
    .prologue
    .line 729
    const/4 v0, 0x0

    #@1
    .line 731
    .local v0, temp:Lcom/lge/lgdrm/DrmContent;
    if-eqz p1, :cond_6

    #@3
    .line 732
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContentSession;->defaultContent:Lcom/lge/lgdrm/DrmContent;

    #@5
    .line 741
    :goto_5
    return-object v0

    #@6
    .line 734
    :cond_6
    iget-object v1, p0, Lcom/lge/lgdrm/DrmContentSession;->selectedContent:Lcom/lge/lgdrm/DrmContent;

    #@8
    if-nez v1, :cond_d

    #@a
    .line 735
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContentSession;->defaultContent:Lcom/lge/lgdrm/DrmContent;

    #@c
    goto :goto_5

    #@d
    .line 737
    :cond_d
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContentSession;->selectedContent:Lcom/lge/lgdrm/DrmContent;

    #@f
    goto :goto_5
.end method

.method public getSelectedRight(Z)Lcom/lge/lgdrm/DrmRight;
    .registers 10
    .parameter "defaults"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 838
    const/4 v0, 0x0

    #@2
    invoke-virtual {p0, v0}, Lcom/lge/lgdrm/DrmContentSession;->getSelectedContent(Z)Lcom/lge/lgdrm/DrmContent;

    #@5
    move-result-object v6

    #@6
    .line 839
    .local v6, content:Lcom/lge/lgdrm/DrmContent;
    if-nez v6, :cond_a

    #@8
    move-object v1, v7

    #@9
    .line 858
    :cond_9
    :goto_9
    return-object v1

    #@a
    .line 843
    :cond_a
    iget v0, v6, Lcom/lge/lgdrm/DrmContent;->rightState:I

    #@c
    const/4 v2, -0x4

    #@d
    if-ne v0, v2, :cond_1b

    #@f
    iget-boolean v0, v6, Lcom/lge/lgdrm/DrmContent;->autoRightState:Z

    #@11
    if-nez v0, :cond_1b

    #@13
    .line 845
    new-instance v0, Ljava/lang/IllegalStateException;

    #@15
    const-string v2, "Select Right first"

    #@17
    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v0

    #@1b
    .line 848
    :cond_1b
    new-instance v1, Lcom/lge/lgdrm/DrmRight;

    #@1d
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContentSession;->context:Landroid/content/Context;

    #@1f
    invoke-direct {v1, v6, v0}, Lcom/lge/lgdrm/DrmRight;-><init>(Lcom/lge/lgdrm/DrmContent;Landroid/content/Context;)V

    #@22
    .line 849
    .local v1, right:Lcom/lge/lgdrm/DrmRight;
    if-nez v1, :cond_26

    #@24
    move-object v1, v7

    #@25
    .line 850
    goto :goto_9

    #@26
    .line 853
    :cond_26
    iget-object v2, v6, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@28
    iget v3, v6, Lcom/lge/lgdrm/DrmContent;->index:I

    #@2a
    iget v4, v6, Lcom/lge/lgdrm/DrmContent;->previewContent:I

    #@2c
    iget v5, v6, Lcom/lge/lgdrm/DrmContent;->permisson:I

    #@2e
    move-object v0, p0

    #@2f
    invoke-direct/range {v0 .. v5}, Lcom/lge/lgdrm/DrmContentSession;->nativeGetRightInfo(Lcom/lge/lgdrm/DrmRight;Ljava/lang/String;III)I

    #@32
    move-result v0

    #@33
    if-eqz v0, :cond_9

    #@35
    move-object v1, v7

    #@36
    .line 855
    goto :goto_9
.end method

.method public isActionSupported(I)Z
    .registers 10
    .parameter "action"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x7

    #@1
    const/4 v6, 0x2

    #@2
    const/4 v2, 0x0

    #@3
    const/4 v3, 0x1

    #@4
    .line 1193
    const/4 v1, -0x1

    #@5
    .line 1196
    .local v1, retVal:I
    if-lt p1, v3, :cond_b

    #@7
    const/16 v4, 0x8

    #@9
    if-le p1, v4, :cond_13

    #@b
    .line 1197
    :cond_b
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@d
    const-string v3, "Invalid action"

    #@f
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@12
    throw v2

    #@13
    .line 1200
    :cond_13
    invoke-virtual {p0, v2}, Lcom/lge/lgdrm/DrmContentSession;->getSelectedContent(Z)Lcom/lge/lgdrm/DrmContent;

    #@16
    move-result-object v0

    #@17
    .line 1201
    .local v0, content:Lcom/lge/lgdrm/DrmContent;
    if-nez v0, :cond_1a

    #@19
    .line 1268
    :cond_19
    :goto_19
    :pswitch_19
    return v2

    #@1a
    .line 1205
    :cond_1a
    packed-switch p1, :pswitch_data_98

    #@1d
    .line 1264
    :cond_1d
    :goto_1d
    const/4 v4, -0x1

    #@1e
    if-eq v1, v4, :cond_19

    #@20
    move v2, v3

    #@21
    .line 1268
    goto :goto_19

    #@22
    .line 1207
    :pswitch_22
    iget v4, v0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@24
    const/4 v5, 0x5

    #@25
    if-eq v4, v5, :cond_2b

    #@27
    iget v4, v0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@29
    if-ne v4, v7, :cond_2d

    #@2b
    .line 1209
    :cond_2b
    const/4 v1, -0x1

    #@2c
    goto :goto_1d

    #@2d
    .line 1210
    :cond_2d
    iget v4, v0, Lcom/lge/lgdrm/DrmContent;->contentType:I

    #@2f
    and-int/lit8 v4, v4, 0x1

    #@31
    if-nez v4, :cond_1d

    #@33
    iget v4, v0, Lcom/lge/lgdrm/DrmContent;->contentType:I

    #@35
    and-int/lit8 v4, v4, 0x2

    #@37
    if-nez v4, :cond_1d

    #@39
    .line 1212
    const/4 v1, 0x0

    #@3a
    goto :goto_1d

    #@3b
    .line 1219
    :pswitch_3b
    iget-object v4, v0, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@3d
    iget v5, v0, Lcom/lge/lgdrm/DrmContent;->index:I

    #@3f
    invoke-direct {p0, v4, v5, p1}, Lcom/lge/lgdrm/DrmContentSession;->nativeIsSettingsAvailable(Ljava/lang/String;II)I

    #@42
    move-result v1

    #@43
    .line 1220
    if-nez v1, :cond_1d

    #@45
    .line 1221
    if-eq p1, v6, :cond_4a

    #@47
    const/4 v4, 0x4

    #@48
    if-ne p1, v4, :cond_4f

    #@4a
    .line 1222
    :cond_4a
    iput v3, v0, Lcom/lge/lgdrm/DrmContent;->permisson:I

    #@4c
    .line 1226
    :cond_4c
    :goto_4c
    iput-boolean v3, v0, Lcom/lge/lgdrm/DrmContent;->autoRightState:Z

    #@4e
    goto :goto_1d

    #@4f
    .line 1223
    :cond_4f
    const/4 v4, 0x3

    #@50
    if-ne p1, v4, :cond_4c

    #@52
    .line 1224
    iput v6, v0, Lcom/lge/lgdrm/DrmContent;->permisson:I

    #@54
    goto :goto_4c

    #@55
    .line 1231
    :pswitch_55
    iget v4, v0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@57
    if-eq v4, v3, :cond_61

    #@59
    iget v4, v0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@5b
    if-eq v4, v6, :cond_61

    #@5d
    iget v4, v0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@5f
    if-ne v4, v7, :cond_75

    #@61
    .line 1234
    :cond_61
    iget v4, v0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@63
    if-ne v4, v3, :cond_73

    #@65
    .line 1235
    iget v4, v0, Lcom/lge/lgdrm/DrmContent;->contentType:I

    #@67
    const/16 v5, 0x300

    #@69
    if-eq v4, v5, :cond_71

    #@6b
    iget v4, v0, Lcom/lge/lgdrm/DrmContent;->contentType:I

    #@6d
    const/16 v5, 0x301

    #@6f
    if-ne v4, v5, :cond_1d

    #@71
    .line 1237
    :cond_71
    const/4 v1, 0x0

    #@72
    goto :goto_1d

    #@73
    .line 1240
    :cond_73
    const/4 v1, 0x0

    #@74
    goto :goto_1d

    #@75
    .line 1242
    :cond_75
    iget v4, v0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@77
    const/16 v5, 0x9

    #@79
    if-ne v4, v5, :cond_7d

    #@7b
    .line 1243
    const/4 v1, 0x0

    #@7c
    goto :goto_1d

    #@7d
    .line 1244
    :cond_7d
    iget v4, v0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@7f
    const/16 v5, 0xa

    #@81
    if-ne v4, v5, :cond_1d

    #@83
    .line 1245
    const/4 v1, -0x1

    #@84
    goto :goto_1d

    #@85
    .line 1250
    :pswitch_85
    iget v4, v0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@87
    if-ne v4, v3, :cond_1d

    #@89
    .line 1251
    iget v4, v0, Lcom/lge/lgdrm/DrmContent;->contentType:I

    #@8b
    const/16 v5, 0x51

    #@8d
    if-eq v4, v5, :cond_95

    #@8f
    iget v4, v0, Lcom/lge/lgdrm/DrmContent;->contentType:I

    #@91
    and-int/lit16 v4, v4, 0x100

    #@93
    if-eqz v4, :cond_1d

    #@95
    .line 1253
    :cond_95
    const/4 v1, 0x0

    #@96
    goto :goto_1d

    #@97
    .line 1205
    nop

    #@98
    :pswitch_data_98
    .packed-switch 0x1
        :pswitch_22
        :pswitch_3b
        :pswitch_3b
        :pswitch_3b
        :pswitch_85
        :pswitch_19
        :pswitch_55
        :pswitch_19
    .end packed-switch
.end method

.method public isValidSession()Z
    .registers 2

    #@0
    .prologue
    .line 571
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContentSession;->defaultContent:Lcom/lge/lgdrm/DrmContent;

    #@2
    if-nez v0, :cond_a

    #@4
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContentSession;->selectedContent:Lcom/lge/lgdrm/DrmContent;

    #@6
    if-nez v0, :cond_a

    #@8
    .line 573
    const/4 v0, 0x0

    #@9
    .line 575
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x1

    #@b
    goto :goto_9
.end method

.method public judgeRight(IZ)I
    .registers 8
    .parameter "permission"
    .parameter "checkPreviewURL"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 1089
    const/4 v1, 0x0

    #@2
    .line 1092
    .local v1, preview:I
    if-lt p1, v2, :cond_8

    #@4
    const/16 v3, 0x80

    #@6
    if-le p1, v3, :cond_10

    #@8
    .line 1093
    :cond_8
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@a
    const-string v4, "Invalid permission"

    #@c
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v3

    #@10
    .line 1096
    :cond_10
    const/4 v3, 0x0

    #@11
    invoke-virtual {p0, v3}, Lcom/lge/lgdrm/DrmContentSession;->getSelectedContent(Z)Lcom/lge/lgdrm/DrmContent;

    #@14
    move-result-object v0

    #@15
    .line 1097
    .local v0, content:Lcom/lge/lgdrm/DrmContent;
    if-nez v0, :cond_19

    #@17
    .line 1098
    const/4 v2, -0x1

    #@18
    .line 1114
    :cond_18
    :goto_18
    return v2

    #@19
    .line 1101
    :cond_19
    if-eqz p2, :cond_20

    #@1b
    iget v3, v0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@1d
    const/4 v4, 0x2

    #@1e
    if-ne v3, v4, :cond_18

    #@20
    .line 1106
    :cond_20
    if-nez p2, :cond_26

    #@22
    iget v3, v0, Lcom/lge/lgdrm/DrmContent;->previewContent:I

    #@24
    if-ne v3, v2, :cond_27

    #@26
    .line 1107
    :cond_26
    const/4 v1, 0x1

    #@27
    .line 1110
    :cond_27
    iput p1, v0, Lcom/lge/lgdrm/DrmContent;->permisson:I

    #@29
    .line 1111
    iget-object v3, v0, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@2b
    iget v4, v0, Lcom/lge/lgdrm/DrmContent;->index:I

    #@2d
    invoke-direct {p0, v3, v4, v1, p1}, Lcom/lge/lgdrm/DrmContentSession;->nativeJudgeRight(Ljava/lang/String;III)I

    #@30
    move-result v2

    #@31
    .line 1112
    .local v2, retVal:I
    iput v2, v0, Lcom/lge/lgdrm/DrmContent;->rightState:I

    #@33
    goto :goto_18
.end method

.method public obtainNewRight(ILandroid/content/ComponentName;)I
    .registers 10
    .parameter "type"
    .parameter "resultReceiver"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 1459
    const/4 v2, 0x0

    #@3
    .line 1463
    .local v2, retVal:I
    const/4 v5, 0x7

    #@4
    invoke-virtual {p0, v5}, Lcom/lge/lgdrm/DrmContentSession;->isActionSupported(I)Z

    #@7
    move-result v5

    #@8
    if-nez v5, :cond_12

    #@a
    .line 1464
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    #@c
    const-string v4, "Rights renewal is not supported"

    #@e
    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@11
    throw v3

    #@12
    .line 1466
    :cond_12
    if-nez p2, :cond_1c

    #@14
    .line 1467
    new-instance v3, Ljava/lang/NullPointerException;

    #@16
    const-string v4, "Parameter resultReceiver is null"

    #@18
    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v3

    #@1c
    .line 1469
    :cond_1c
    iget-object v5, p0, Lcom/lge/lgdrm/DrmContentSession;->context:Landroid/content/Context;

    #@1e
    if-nez v5, :cond_28

    #@20
    .line 1470
    new-instance v3, Ljava/lang/NullPointerException;

    #@22
    const-string v4, "context is null"

    #@24
    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@27
    throw v3

    #@28
    .line 1473
    :cond_28
    invoke-virtual {p0, v4}, Lcom/lge/lgdrm/DrmContentSession;->getSelectedContent(Z)Lcom/lge/lgdrm/DrmContent;

    #@2b
    move-result-object v0

    #@2c
    .line 1474
    .local v0, content:Lcom/lge/lgdrm/DrmContent;
    if-nez v0, :cond_30

    #@2e
    move v3, v4

    #@2f
    .line 1504
    :goto_2f
    return v3

    #@30
    .line 1478
    :cond_30
    new-instance v5, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    invoke-virtual {p2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@38
    move-result-object v6

    #@39
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v5

    #@3d
    const-string v6, ";"

    #@3f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v5

    #@43
    invoke-virtual {p2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@46
    move-result-object v6

    #@47
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v5

    #@4b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v1

    #@4f
    .line 1479
    .local v1, intentReceiver:Ljava/lang/String;
    if-nez v1, :cond_53

    #@51
    move v3, v4

    #@52
    .line 1480
    goto :goto_2f

    #@53
    .line 1483
    :cond_53
    if-ne p1, v3, :cond_72

    #@55
    .line 1484
    iget v5, v0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@57
    const/16 v6, 0x9

    #@59
    if-ne v5, v6, :cond_67

    #@5b
    .line 1485
    const/4 v5, 0x0

    #@5c
    invoke-direct {p0, v5, v1}, Lcom/lge/lgdrm/DrmContentSession;->activateContent(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    move-result v2

    #@60
    .line 1486
    if-nez v2, :cond_63

    #@62
    move v2, v3

    #@63
    .line 1500
    :cond_63
    :goto_63
    if-gtz v2, :cond_8a

    #@65
    move v3, v4

    #@66
    .line 1501
    goto :goto_2f

    #@67
    .line 1488
    :cond_67
    iget v3, v0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@69
    iget-object v5, v0, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@6b
    iget v6, v0, Lcom/lge/lgdrm/DrmContent;->index:I

    #@6d
    invoke-direct {p0, v3, v5, v6, v1}, Lcom/lge/lgdrm/DrmContentSession;->nativeActivateContent(ILjava/lang/String;ILjava/lang/String;)I

    #@70
    move-result v2

    #@71
    goto :goto_63

    #@72
    .line 1490
    :cond_72
    iget v3, v0, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@74
    const/4 v5, 0x2

    #@75
    if-ne v3, v5, :cond_63

    #@77
    .line 1491
    iget-object v3, v0, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@79
    iget v5, v0, Lcom/lge/lgdrm/DrmContent;->index:I

    #@7b
    invoke-direct {p0, p1, v3, v5, v1}, Lcom/lge/lgdrm/DrmContentSession;->nativeDoRoap(ILjava/lang/String;ILjava/lang/String;)I

    #@7e
    move-result v2

    #@7f
    .line 1492
    if-lez v2, :cond_63

    #@81
    .line 1494
    const/4 v3, -0x1

    #@82
    invoke-direct {p0, v0}, Lcom/lge/lgdrm/DrmContentSession;->startDldClient(Lcom/lge/lgdrm/DrmContent;)I

    #@85
    move-result v5

    #@86
    if-ne v3, v5, :cond_63

    #@88
    .line 1495
    const/4 v2, -0x1

    #@89
    goto :goto_63

    #@8a
    :cond_8a
    move v3, v2

    #@8b
    .line 1504
    goto :goto_2f
.end method

.method public openDrmStream()Lcom/lge/lgdrm/DrmStream;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1285
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@4
    move-result v3

    #@5
    if-nez v3, :cond_f

    #@7
    .line 1286
    new-instance v2, Ljava/lang/SecurityException;

    #@9
    const-string v3, "Need proper permission to access drm"

    #@b
    invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@e
    throw v2

    #@f
    .line 1289
    :cond_f
    const/4 v3, 0x0

    #@10
    invoke-virtual {p0, v3}, Lcom/lge/lgdrm/DrmContentSession;->getSelectedContent(Z)Lcom/lge/lgdrm/DrmContent;

    #@13
    move-result-object v0

    #@14
    .line 1290
    .local v0, content:Lcom/lge/lgdrm/DrmContent;
    if-nez v0, :cond_17

    #@16
    .line 1304
    :cond_16
    :goto_16
    return-object v2

    #@17
    .line 1294
    :cond_17
    iget v3, v0, Lcom/lge/lgdrm/DrmContent;->rightState:I

    #@19
    const/4 v4, -0x4

    #@1a
    if-ne v3, v4, :cond_28

    #@1c
    iget-boolean v3, v0, Lcom/lge/lgdrm/DrmContent;->autoRightState:Z

    #@1e
    if-nez v3, :cond_28

    #@20
    .line 1296
    new-instance v2, Ljava/lang/IllegalStateException;

    #@22
    const-string v3, "Select Right first"

    #@24
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@27
    throw v2

    #@28
    .line 1299
    :cond_28
    iget-object v3, v0, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@2a
    iget v4, v0, Lcom/lge/lgdrm/DrmContent;->index:I

    #@2c
    iget v5, v0, Lcom/lge/lgdrm/DrmContent;->previewContent:I

    #@2e
    invoke-static {v3, v4, v2, v5, v2}, Lcom/lge/lgdrm/DrmContentSession;->nativeDrmOpen(Ljava/lang/String;ILjava/io/FileDescriptor;I[B)I

    #@31
    move-result v1

    #@32
    .line 1300
    .local v1, nativeHandle:I
    if-eqz v1, :cond_16

    #@34
    .line 1304
    new-instance v2, Lcom/lge/lgdrm/DrmStream;

    #@36
    iget-object v3, v0, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@38
    invoke-direct {v2, v3, v1}, Lcom/lge/lgdrm/DrmStream;-><init>(Ljava/lang/String;I)V

    #@3b
    goto :goto_16
.end method

.method public selectContent(Lcom/lge/lgdrm/DrmContent;)I
    .registers 4
    .parameter "content"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 702
    if-nez p1, :cond_a

    #@2
    .line 703
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string v1, "Parameter content is null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 706
    :cond_a
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContentSession;->defaultContent:Lcom/lge/lgdrm/DrmContent;

    #@c
    if-nez v0, :cond_10

    #@e
    .line 707
    const/4 v0, -0x1

    #@f
    .line 716
    :goto_f
    return v0

    #@10
    .line 710
    :cond_10
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContentSession;->defaultContent:Lcom/lge/lgdrm/DrmContent;

    #@12
    invoke-virtual {v0, p1}, Lcom/lge/lgdrm/DrmContent;->isSibling(Lcom/lge/lgdrm/DrmContent;)Z

    #@15
    move-result v0

    #@16
    if-nez v0, :cond_20

    #@18
    .line 711
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1a
    const-string v1, "Invalid content to this session"

    #@1c
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0

    #@20
    .line 714
    :cond_20
    iput-object p1, p0, Lcom/lge/lgdrm/DrmContentSession;->selectedContent:Lcom/lge/lgdrm/DrmContent;

    #@22
    .line 716
    const/4 v0, 0x0

    #@23
    goto :goto_f
.end method

.method public selectRight(Lcom/lge/lgdrm/DrmRight;)I
    .registers 5
    .parameter "right"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;,
            Ljava/lang/NullPointerException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 797
    if-nez p1, :cond_a

    #@2
    .line 798
    new-instance v1, Ljava/lang/NullPointerException;

    #@4
    const-string v2, "Parameter right is null"

    #@6
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@9
    throw v1

    #@a
    .line 801
    :cond_a
    const/4 v1, 0x0

    #@b
    invoke-virtual {p0, v1}, Lcom/lge/lgdrm/DrmContentSession;->getSelectedContent(Z)Lcom/lge/lgdrm/DrmContent;

    #@e
    move-result-object v0

    #@f
    .line 802
    .local v0, content:Lcom/lge/lgdrm/DrmContent;
    if-nez v0, :cond_13

    #@11
    .line 803
    const/4 v1, -0x1

    #@12
    return v1

    #@13
    .line 806
    :cond_13
    invoke-virtual {p1, v0}, Lcom/lge/lgdrm/DrmRight;->isMatched(Lcom/lge/lgdrm/DrmContent;)Z

    #@16
    move-result v1

    #@17
    if-eqz v1, :cond_21

    #@19
    .line 807
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@1b
    const-string v2, "Invalid right to this session"

    #@1d
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@20
    throw v1

    #@21
    .line 810
    :cond_21
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    #@23
    const-string v2, "Rights selection is not supported"

    #@25
    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@28
    throw v1
.end method

.method public setDecryptionInfo(Z)I
    .registers 9
    .parameter "reset"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1419
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Lcom/lge/lgdrm/DrmContentSession;->getSelectedContent(Z)Lcom/lge/lgdrm/DrmContent;

    #@4
    move-result-object v6

    #@5
    .line 1420
    .local v6, content:Lcom/lge/lgdrm/DrmContent;
    if-nez v6, :cond_9

    #@7
    .line 1421
    const/4 v0, -0x1

    #@8
    .line 1435
    :goto_8
    return v0

    #@9
    .line 1424
    :cond_9
    iget-boolean v0, v6, Lcom/lge/lgdrm/DrmContent;->autoRightState:Z

    #@b
    if-nez v0, :cond_1b

    #@d
    .line 1425
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@10
    move-result v0

    #@11
    if-nez v0, :cond_1b

    #@13
    .line 1426
    new-instance v0, Ljava/lang/SecurityException;

    #@15
    const-string v1, "Need proper permission to access drm"

    #@17
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v0

    #@1b
    .line 1430
    :cond_1b
    if-nez p1, :cond_2e

    #@1d
    iget v0, v6, Lcom/lge/lgdrm/DrmContent;->rightState:I

    #@1f
    const/4 v1, -0x4

    #@20
    if-ne v0, v1, :cond_2e

    #@22
    iget-boolean v0, v6, Lcom/lge/lgdrm/DrmContent;->autoRightState:Z

    #@24
    if-nez v0, :cond_2e

    #@26
    .line 1432
    new-instance v0, Ljava/lang/IllegalStateException;

    #@28
    const-string v1, "Select Right first"

    #@2a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@2d
    throw v0

    #@2e
    .line 1435
    :cond_2e
    iget v2, v6, Lcom/lge/lgdrm/DrmContent;->agentType:I

    #@30
    iget-object v3, v6, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@32
    iget v4, v6, Lcom/lge/lgdrm/DrmContent;->index:I

    #@34
    iget v5, v6, Lcom/lge/lgdrm/DrmContent;->previewContent:I

    #@36
    move-object v0, p0

    #@37
    move v1, p1

    #@38
    invoke-direct/range {v0 .. v5}, Lcom/lge/lgdrm/DrmContentSession;->nativeSetDecryptionInfo(ZILjava/lang/String;II)I

    #@3b
    move-result v0

    #@3c
    goto :goto_8
.end method

.method public syncDrmTime(Landroid/content/ComponentName;)I
    .registers 7
    .parameter "resultReceiver"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;,
            Ljava/lang/UnsupportedOperationException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 920
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@4
    move-result v3

    #@5
    if-nez v3, :cond_f

    #@7
    .line 921
    new-instance v2, Ljava/lang/SecurityException;

    #@9
    const-string v3, "Need proper permission to access drm"

    #@b
    invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@e
    throw v2

    #@f
    .line 924
    :cond_f
    const/4 v3, 0x5

    #@10
    invoke-virtual {p0, v3}, Lcom/lge/lgdrm/DrmContentSession;->isActionSupported(I)Z

    #@13
    move-result v3

    #@14
    if-nez v3, :cond_1e

    #@16
    .line 925
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    #@18
    const-string v3, "Time sync is not supported"

    #@1a
    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v2

    #@1e
    .line 927
    :cond_1e
    if-nez p1, :cond_28

    #@20
    .line 928
    new-instance v2, Ljava/lang/NullPointerException;

    #@22
    const-string v3, "Parameter resultReceiver is null"

    #@24
    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@27
    throw v2

    #@28
    .line 930
    :cond_28
    iget-object v3, p0, Lcom/lge/lgdrm/DrmContentSession;->context:Landroid/content/Context;

    #@2a
    if-nez v3, :cond_34

    #@2c
    .line 931
    new-instance v2, Ljava/lang/NullPointerException;

    #@2e
    const-string v3, "context is null"

    #@30
    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@33
    throw v2

    #@34
    .line 934
    :cond_34
    iget-object v3, p0, Lcom/lge/lgdrm/DrmContentSession;->context:Landroid/content/Context;

    #@36
    instance-of v3, v3, Landroid/app/Activity;

    #@38
    if-nez v3, :cond_42

    #@3a
    .line 935
    const-string v3, "DrmCntSes"

    #@3c
    const-string v4, "Use activity context instead"

    #@3e
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 955
    :cond_41
    :goto_41
    return v2

    #@42
    .line 940
    :cond_42
    new-instance v1, Landroid/content/Intent;

    #@44
    const-string v3, "com.lge.lgdrm.action.DRM_SYNC_TIME"

    #@46
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@49
    .line 941
    .local v1, intent:Landroid/content/Intent;
    if-eqz v1, :cond_41

    #@4b
    .line 945
    invoke-virtual {p0, v2}, Lcom/lge/lgdrm/DrmContentSession;->getSelectedContent(Z)Lcom/lge/lgdrm/DrmContent;

    #@4e
    move-result-object v0

    #@4f
    .line 946
    .local v0, content:Lcom/lge/lgdrm/DrmContent;
    if-eqz v0, :cond_41

    #@51
    .line 950
    const-string v2, "com.lge.lgdrm.extra.FILE_NAME"

    #@53
    iget-object v3, v0, Lcom/lge/lgdrm/DrmContent;->filename:Ljava/lang/String;

    #@55
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@58
    .line 951
    const-string v2, "com.lge.lgdrm.extra.CONTENT_TYPE"

    #@5a
    invoke-virtual {v0}, Lcom/lge/lgdrm/DrmContent;->getContentType()I

    #@5d
    move-result v3

    #@5e
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@61
    .line 952
    const-string v2, "com.lge.lgdrm.extra.DRM_TYPE"

    #@63
    invoke-virtual {v0}, Lcom/lge/lgdrm/DrmContent;->getDrmContentType()I

    #@66
    move-result v3

    #@67
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@6a
    .line 953
    iget-object v2, p0, Lcom/lge/lgdrm/DrmContentSession;->context:Landroid/content/Context;

    #@6c
    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@6f
    .line 955
    const/4 v2, 0x1

    #@70
    goto :goto_41
.end method

.method public uploadRight(Landroid/content/ComponentName;)I
    .registers 5
    .parameter "resultReceiver"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1525
    if-nez p1, :cond_b

    #@3
    .line 1526
    new-instance v1, Ljava/lang/NullPointerException;

    #@5
    const-string v2, "Parameter resultReceiver is null"

    #@7
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v1

    #@b
    .line 1528
    :cond_b
    iget-object v1, p0, Lcom/lge/lgdrm/DrmContentSession;->context:Landroid/content/Context;

    #@d
    if-nez v1, :cond_17

    #@f
    .line 1529
    new-instance v1, Ljava/lang/NullPointerException;

    #@11
    const-string v2, "context is null"

    #@13
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@16
    throw v1

    #@17
    .line 1532
    :cond_17
    invoke-virtual {p0, v2}, Lcom/lge/lgdrm/DrmContentSession;->getSelectedContent(Z)Lcom/lge/lgdrm/DrmContent;

    #@1a
    move-result-object v0

    #@1b
    .line 1533
    .local v0, content:Lcom/lge/lgdrm/DrmContent;
    if-nez v0, :cond_1e

    #@1d
    .line 1534
    return v2

    #@1e
    .line 1537
    :cond_1e
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    #@20
    const-string v2, "Rights upload is not supported"

    #@22
    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@25
    throw v1
.end method
