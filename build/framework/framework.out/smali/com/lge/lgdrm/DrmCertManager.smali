.class public final Lcom/lge/lgdrm/DrmCertManager;
.super Ljava/lang/Object;
.source "DrmCertManager.java"


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 27
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 28
    return-void
.end method

.method public static getIndex()I
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 127
    sget-boolean v0, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@2
    if-nez v0, :cond_6

    #@4
    .line 128
    const/4 v0, -0x1

    #@5
    .line 135
    :goto_5
    return v0

    #@6
    .line 131
    :cond_6
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_14

    #@c
    .line 132
    new-instance v0, Ljava/lang/SecurityException;

    #@e
    const-string v1, "Need proper permission to access drm"

    #@10
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 135
    :cond_14
    const/4 v0, 0x7

    #@15
    const/4 v1, 0x0

    #@16
    const/4 v2, 0x0

    #@17
    invoke-static {v0, v1, v2}, Lcom/lge/lgdrm/DrmCertManager;->nativeManageCertificate(IILjava/lang/String;)I

    #@1a
    move-result v0

    #@1b
    goto :goto_5
.end method

.method public static getInformation(I)Ljava/lang/String;
    .registers 3
    .parameter "type"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 175
    sget-boolean v0, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@2
    if-nez v0, :cond_6

    #@4
    .line 176
    const/4 v0, 0x0

    #@5
    .line 186
    :goto_5
    return-object v0

    #@6
    .line 179
    :cond_6
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_14

    #@c
    .line 180
    new-instance v0, Ljava/lang/SecurityException;

    #@e
    const-string v1, "Need proper permission to access drm"

    #@10
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 183
    :cond_14
    const/4 v0, 0x1

    #@15
    if-lt p0, v0, :cond_1a

    #@17
    const/4 v0, 0x2

    #@18
    if-le p0, v0, :cond_22

    #@1a
    .line 184
    :cond_1a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1c
    const-string v1, "Invalid type"

    #@1e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@21
    throw v0

    #@22
    .line 186
    :cond_22
    invoke-static {p0}, Lcom/lge/lgdrm/DrmCertManager;->nativeGetCertificateInformation(I)Ljava/lang/String;

    #@25
    move-result-object v0

    #@26
    goto :goto_5
.end method

.method public static isTestSet()Z
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 198
    sget-boolean v1, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@3
    if-nez v1, :cond_6

    #@5
    .line 210
    :cond_5
    :goto_5
    return v0

    #@6
    .line 202
    :cond_6
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_14

    #@c
    .line 203
    new-instance v0, Ljava/lang/SecurityException;

    #@e
    const-string v1, "Need proper permission to access drm"

    #@10
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 206
    :cond_14
    const/4 v1, 0x2

    #@15
    const/4 v2, 0x0

    #@16
    invoke-static {v1, v0, v2}, Lcom/lge/lgdrm/DrmCertManager;->nativeManageCertificate(IILjava/lang/String;)I

    #@19
    move-result v1

    #@1a
    if-nez v1, :cond_5

    #@1c
    .line 207
    const/4 v0, 0x1

    #@1d
    goto :goto_5
.end method

.method public static load(ZLjava/lang/String;)I
    .registers 5
    .parameter "loadOnDemand"
    .parameter "path"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 227
    sget-boolean v0, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@3
    if-nez v0, :cond_7

    #@5
    .line 228
    const/4 v0, -0x1

    #@6
    .line 238
    :goto_6
    return v0

    #@7
    .line 231
    :cond_7
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@a
    move-result v0

    #@b
    if-nez v0, :cond_15

    #@d
    .line 232
    new-instance v0, Ljava/lang/SecurityException;

    #@f
    const-string v1, "Need proper permission to access drm"

    #@11
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@14
    throw v0

    #@15
    .line 235
    :cond_15
    if-eqz p0, :cond_1d

    #@17
    .line 236
    const/4 v0, 0x4

    #@18
    invoke-static {v0, v2, p1}, Lcom/lge/lgdrm/DrmCertManager;->nativeManageCertificate(IILjava/lang/String;)I

    #@1b
    move-result v0

    #@1c
    goto :goto_6

    #@1d
    .line 238
    :cond_1d
    const/4 v0, 0x3

    #@1e
    const/4 v1, 0x0

    #@1f
    invoke-static {v0, v2, v1}, Lcom/lge/lgdrm/DrmCertManager;->nativeManageCertificate(IILjava/lang/String;)I

    #@22
    move-result v0

    #@23
    goto :goto_6
.end method

.method private static native nativeGetCertificateInformation(I)Ljava/lang/String;
.end method

.method private static native nativeManageCertificate(IILjava/lang/String;)I
.end method

.method private static native nativeWriteCertificate(I[B)I
.end method

.method public static reset()I
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 148
    sget-boolean v0, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@2
    if-nez v0, :cond_6

    #@4
    .line 149
    const/4 v0, -0x1

    #@5
    .line 156
    :goto_5
    return v0

    #@6
    .line 152
    :cond_6
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_14

    #@c
    .line 153
    new-instance v0, Ljava/lang/SecurityException;

    #@e
    const-string v1, "Need proper permission to access drm"

    #@10
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 156
    :cond_14
    const/4 v0, 0x5

    #@15
    const/4 v1, 0x0

    #@16
    const/4 v2, 0x0

    #@17
    invoke-static {v0, v1, v2}, Lcom/lge/lgdrm/DrmCertManager;->nativeManageCertificate(IILjava/lang/String;)I

    #@1a
    move-result v0

    #@1b
    goto :goto_5
.end method

.method public static setIndex(I)I
    .registers 3
    .parameter "index"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 106
    sget-boolean v0, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@2
    if-nez v0, :cond_6

    #@4
    .line 107
    const/4 v0, -0x1

    #@5
    .line 114
    :goto_5
    return v0

    #@6
    .line 110
    :cond_6
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_14

    #@c
    .line 111
    new-instance v0, Ljava/lang/SecurityException;

    #@e
    const-string v1, "Need proper permission to access drm"

    #@10
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 114
    :cond_14
    const/4 v0, 0x6

    #@15
    const/4 v1, 0x0

    #@16
    invoke-static {v0, p0, v1}, Lcom/lge/lgdrm/DrmCertManager;->nativeManageCertificate(IILjava/lang/String;)I

    #@19
    move-result v0

    #@1a
    goto :goto_5
.end method

.method public static verify()I
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 83
    sget-boolean v0, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@2
    if-nez v0, :cond_6

    #@4
    .line 84
    const/4 v0, -0x1

    #@5
    .line 91
    :goto_5
    return v0

    #@6
    .line 87
    :cond_6
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_14

    #@c
    .line 88
    new-instance v0, Ljava/lang/SecurityException;

    #@e
    const-string v1, "Need proper permission to access drm"

    #@10
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 91
    :cond_14
    const/4 v0, 0x1

    #@15
    const/4 v1, 0x0

    #@16
    const/4 v2, 0x0

    #@17
    invoke-static {v0, v1, v2}, Lcom/lge/lgdrm/DrmCertManager;->nativeManageCertificate(IILjava/lang/String;)I

    #@1a
    move-result v0

    #@1b
    goto :goto_5
.end method

.method public static write(I[B)I
    .registers 4
    .parameter "type"
    .parameter "buf"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    #@0
    .prologue
    .line 52
    sget-boolean v0, Lcom/lge/lgdrm/Drm;->LGDRM:Z

    #@2
    if-nez v0, :cond_6

    #@4
    .line 53
    const/4 v0, -0x1

    #@5
    .line 68
    :goto_5
    return v0

    #@6
    .line 56
    :cond_6
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_14

    #@c
    .line 57
    new-instance v0, Ljava/lang/SecurityException;

    #@e
    const-string v1, "Need proper permission to access drm"

    #@10
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 60
    :cond_14
    const/4 v0, 0x1

    #@15
    if-lt p0, v0, :cond_1a

    #@17
    const/4 v0, 0x4

    #@18
    if-le p0, v0, :cond_22

    #@1a
    .line 61
    :cond_1a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1c
    const-string v1, "Invalid type"

    #@1e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@21
    throw v0

    #@22
    .line 64
    :cond_22
    if-nez p1, :cond_2c

    #@24
    .line 65
    new-instance v0, Ljava/lang/NullPointerException;

    #@26
    const-string v1, "Parameter buf is null"

    #@28
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@2b
    throw v0

    #@2c
    .line 68
    :cond_2c
    invoke-static {p0, p1}, Lcom/lge/lgdrm/DrmCertManager;->nativeWriteCertificate(I[B)I

    #@2f
    move-result v0

    #@30
    goto :goto_5
.end method
