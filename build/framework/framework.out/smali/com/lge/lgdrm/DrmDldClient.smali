.class public final Lcom/lge/lgdrm/DrmDldClient;
.super Ljava/lang/Thread;
.source "DrmDldClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/lgdrm/DrmDldClient$FailInfo;
    }
.end annotation


# static fields
.field public static final ERROR_CONNECT:I = 0x3

.field public static final ERROR_HTTP_404:I = 0x2

.field public static final ERROR_INTERNAL:I = 0x1

.field public static final ERROR_INTERRUPTED:I = 0x7

.field public static final ERROR_MIME_MISMATCHED:I = 0x6

.field public static final ERROR_NONE:I = 0x0

.field public static final ERROR_OUT_OF_STORAGE:I = 0x5

.field public static final ERROR_RO_CORRUPTED:I = 0x4

.field private static final PREVIOUS_VERSION:Ljava/lang/String; = "3.1"

.field private static final READ_UNIT:I = 0x1000

.field public static final STATUS_ERROR:I = 0x3

.field public static final STATUS_GET_CONFIRM:I = 0x1

.field public static final STATUS_PROGRESS:I = 0x2

.field public static final STATUS_SUCCESS:I = 0x0

.field private static final TAG:Ljava/lang/String; = "DrmDldClient"

.field private static cachedUA:Ljava/lang/String;

.field private static sLocale:Ljava/util/Locale;

.field private static sLockForLocaleSettings:Ljava/lang/Object;


# instance fields
.field private final HTTP_TIMEOUT:I

.field private client:Landroid/net/http/AndroidHttpClient;

.field private context:Landroid/content/Context;

.field private errorCode:I

.field private failInfo:Lcom/lge/lgdrm/DrmDldClient$FailInfo;

.field private filename:Ljava/lang/String;

.field private handler:Landroid/os/Handler;

.field private interrupted:Z

.field private mimeType:Ljava/lang/String;

.field private nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

.field private objSession:Lcom/lge/lgdrm/DrmObjectSession;

.field private userAgent:Ljava/lang/String;

.field private userConfirm:I


# direct methods
.method private constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 106
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@4
    .line 52
    iput-boolean v1, p0, Lcom/lge/lgdrm/DrmDldClient;->interrupted:Z

    #@6
    .line 64
    const/16 v0, 0x1e

    #@8
    iput v0, p0, Lcom/lge/lgdrm/DrmDldClient;->HTTP_TIMEOUT:I

    #@a
    .line 78
    iput v1, p0, Lcom/lge/lgdrm/DrmDldClient;->errorCode:I

    #@c
    .line 107
    return-void
.end method

.method public constructor <init>(Lcom/lge/lgdrm/DrmDldRequest;Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;Landroid/content/Context;)V
    .registers 8
    .parameter "request"
    .parameter "filename"
    .parameter "mimeType"
    .parameter "handler"
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 155
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@4
    .line 52
    iput-boolean v1, p0, Lcom/lge/lgdrm/DrmDldClient;->interrupted:Z

    #@6
    .line 64
    const/16 v0, 0x1e

    #@8
    iput v0, p0, Lcom/lge/lgdrm/DrmDldClient;->HTTP_TIMEOUT:I

    #@a
    .line 78
    iput v1, p0, Lcom/lge/lgdrm/DrmDldClient;->errorCode:I

    #@c
    .line 156
    iput-object p1, p0, Lcom/lge/lgdrm/DrmDldClient;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

    #@e
    .line 157
    iput-object p2, p0, Lcom/lge/lgdrm/DrmDldClient;->filename:Ljava/lang/String;

    #@10
    .line 158
    iput-object p3, p0, Lcom/lge/lgdrm/DrmDldClient;->mimeType:Ljava/lang/String;

    #@12
    .line 159
    iput-object p4, p0, Lcom/lge/lgdrm/DrmDldClient;->handler:Landroid/os/Handler;

    #@14
    .line 160
    iput-object p5, p0, Lcom/lge/lgdrm/DrmDldClient;->context:Landroid/content/Context;

    #@16
    .line 161
    return-void
.end method

.method protected constructor <init>(Lcom/lge/lgdrm/DrmObjectSession;Lcom/lge/lgdrm/DrmDldRequest;Landroid/content/Context;)V
    .registers 6
    .parameter "objSession"
    .parameter "request"
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 118
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@4
    .line 52
    iput-boolean v1, p0, Lcom/lge/lgdrm/DrmDldClient;->interrupted:Z

    #@6
    .line 64
    const/16 v0, 0x1e

    #@8
    iput v0, p0, Lcom/lge/lgdrm/DrmDldClient;->HTTP_TIMEOUT:I

    #@a
    .line 78
    iput v1, p0, Lcom/lge/lgdrm/DrmDldClient;->errorCode:I

    #@c
    .line 119
    iput-object p1, p0, Lcom/lge/lgdrm/DrmDldClient;->objSession:Lcom/lge/lgdrm/DrmObjectSession;

    #@e
    .line 120
    iput-object p2, p0, Lcom/lge/lgdrm/DrmDldClient;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

    #@10
    .line 121
    iput-object p3, p0, Lcom/lge/lgdrm/DrmDldClient;->context:Landroid/content/Context;

    #@12
    .line 122
    return-void
.end method

.method public constructor <init>(Lcom/lge/lgdrm/DrmObjectSession;Lcom/lge/lgdrm/DrmDldRequest;Landroid/os/Handler;Landroid/content/Context;)V
    .registers 7
    .parameter "objSession"
    .parameter "request"
    .parameter "handler"
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 135
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@4
    .line 52
    iput-boolean v1, p0, Lcom/lge/lgdrm/DrmDldClient;->interrupted:Z

    #@6
    .line 64
    const/16 v0, 0x1e

    #@8
    iput v0, p0, Lcom/lge/lgdrm/DrmDldClient;->HTTP_TIMEOUT:I

    #@a
    .line 78
    iput v1, p0, Lcom/lge/lgdrm/DrmDldClient;->errorCode:I

    #@c
    .line 136
    iput-object p1, p0, Lcom/lge/lgdrm/DrmDldClient;->objSession:Lcom/lge/lgdrm/DrmObjectSession;

    #@e
    .line 137
    iput-object p2, p0, Lcom/lge/lgdrm/DrmDldClient;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

    #@10
    .line 138
    iput-object p3, p0, Lcom/lge/lgdrm/DrmDldClient;->handler:Landroid/os/Handler;

    #@12
    .line 139
    iput-object p4, p0, Lcom/lge/lgdrm/DrmDldClient;->context:Landroid/content/Context;

    #@14
    .line 140
    return-void
.end method

.method private checkMimeType()I
    .registers 14

    #@0
    .prologue
    const/4 v12, 0x7

    #@1
    const/4 v11, 0x3

    #@2
    const/4 v10, 0x1

    #@3
    const/4 v5, -0x1

    #@4
    const/4 v9, 0x0

    #@5
    .line 244
    const/4 v3, 0x0

    #@6
    .line 247
    .local v3, response:Lorg/apache/http/HttpResponse;
    invoke-direct {p0}, Lcom/lge/lgdrm/DrmDldClient;->isInterrupt()Z

    #@9
    move-result v6

    #@a
    if-eqz v6, :cond_10

    #@c
    .line 248
    invoke-direct {p0, v12, v9, v9}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@f
    .line 306
    :goto_f
    return v5

    #@10
    .line 252
    :cond_10
    iget-object v6, p0, Lcom/lge/lgdrm/DrmDldClient;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

    #@12
    if-eqz v6, :cond_1a

    #@14
    iget-object v6, p0, Lcom/lge/lgdrm/DrmDldClient;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

    #@16
    iget-object v6, v6, Lcom/lge/lgdrm/DrmDldRequest;->url:Ljava/lang/String;

    #@18
    if-nez v6, :cond_1e

    #@1a
    .line 253
    :cond_1a
    invoke-direct {p0, v10, v9, v9}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@1d
    goto :goto_f

    #@1e
    .line 259
    :cond_1e
    :try_start_1e
    iget-object v6, p0, Lcom/lge/lgdrm/DrmDldClient;->client:Landroid/net/http/AndroidHttpClient;

    #@20
    new-instance v7, Lorg/apache/http/client/methods/HttpHead;

    #@22
    iget-object v8, p0, Lcom/lge/lgdrm/DrmDldClient;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

    #@24
    iget-object v8, v8, Lcom/lge/lgdrm/DrmDldRequest;->url:Ljava/lang/String;

    #@26
    invoke-direct {v7, v8}, Lorg/apache/http/client/methods/HttpHead;-><init>(Ljava/lang/String;)V

    #@29
    invoke-virtual {v6, v7}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_2c
    .catch Ljava/nio/channels/ClosedByInterruptException; {:try_start_1e .. :try_end_2c} :catch_3c
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_2c} :catch_43
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_2c} :catch_4b

    #@2c
    move-result-object v3

    #@2d
    .line 274
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    #@30
    move-result-object v6

    #@31
    invoke-interface {v6}, Lorg/apache/http/StatusLine;->getStatusCode()I

    #@34
    move-result v4

    #@35
    .line 276
    .local v4, status:I
    sparse-switch v4, :sswitch_data_7c

    #@38
    .line 290
    invoke-direct {p0, v11, v9, v9}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@3b
    goto :goto_f

    #@3c
    .line 260
    .end local v4           #status:I
    :catch_3c
    move-exception v0

    #@3d
    .line 261
    .local v0, e:Ljava/nio/channels/ClosedByInterruptException;
    iput-boolean v10, p0, Lcom/lge/lgdrm/DrmDldClient;->interrupted:Z

    #@3f
    .line 262
    invoke-direct {p0, v12, v9, v9}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@42
    goto :goto_f

    #@43
    .line 264
    .end local v0           #e:Ljava/nio/channels/ClosedByInterruptException;
    :catch_43
    move-exception v0

    #@44
    .line 265
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    #@47
    .line 266
    invoke-direct {p0, v11, v9, v9}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@4a
    goto :goto_f

    #@4b
    .line 268
    .end local v0           #e:Ljava/io/IOException;
    :catch_4b
    move-exception v0

    #@4c
    .line 269
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@4f
    .line 270
    invoke-direct {p0, v10, v9, v9}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@52
    goto :goto_f

    #@53
    .line 285
    .end local v0           #e:Ljava/lang/Exception;
    .restart local v4       #status:I
    :sswitch_53
    const/4 v6, 0x2

    #@54
    invoke-direct {p0, v6, v9, v9}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@57
    goto :goto_f

    #@58
    .line 295
    :sswitch_58
    const-string v6, "Content-Type"

    #@5a
    invoke-interface {v3, v6}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    #@5d
    move-result-object v1

    #@5e
    .line 296
    .local v1, httpHeaders:[Lorg/apache/http/Header;
    if-eqz v1, :cond_77

    #@60
    .line 297
    const/4 v2, 0x0

    #@61
    .local v2, i:I
    :goto_61
    array-length v6, v1

    #@62
    if-ge v2, v6, :cond_77

    #@64
    .line 299
    aget-object v6, v1, v2

    #@66
    invoke-interface {v6}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    #@69
    move-result-object v6

    #@6a
    iget-object v7, p0, Lcom/lge/lgdrm/DrmDldClient;->mimeType:Ljava/lang/String;

    #@6c
    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@6f
    move-result v6

    #@70
    if-eqz v6, :cond_74

    #@72
    .line 300
    const/4 v5, 0x0

    #@73
    goto :goto_f

    #@74
    .line 297
    :cond_74
    add-int/lit8 v2, v2, 0x1

    #@76
    goto :goto_61

    #@77
    .line 305
    .end local v2           #i:I
    :cond_77
    const/4 v6, 0x6

    #@78
    invoke-direct {p0, v6, v9, v9}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@7b
    goto :goto_f

    #@7c
    .line 276
    :sswitch_data_7c
    .sparse-switch
        0xc8 -> :sswitch_58
        0x194 -> :sswitch_53
        0x1f6 -> :sswitch_58
    .end sparse-switch
.end method

.method private static convertObsoleteLanguageCodeToNew(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "langCode"

    #@0
    .prologue
    .line 941
    if-nez p0, :cond_4

    #@2
    .line 942
    const/4 p0, 0x0

    #@3
    .line 954
    .end local p0
    :cond_3
    :goto_3
    return-object p0

    #@4
    .line 944
    .restart local p0
    :cond_4
    const-string/jumbo v0, "iw"

    #@7
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_10

    #@d
    .line 946
    const-string p0, "he"

    #@f
    goto :goto_3

    #@10
    .line 947
    :cond_10
    const-string v0, "in"

    #@12
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_1b

    #@18
    .line 949
    const-string p0, "id"

    #@1a
    goto :goto_3

    #@1b
    .line 950
    :cond_1b
    const-string/jumbo v0, "ji"

    #@1e
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v0

    #@22
    if-eqz v0, :cond_3

    #@24
    .line 952
    const-string/jumbo p0, "yi"

    #@27
    goto :goto_3
.end method

.method private declared-synchronized getCurrentUserAgent()Ljava/lang/String;
    .registers 12

    #@0
    .prologue
    .line 959
    monitor-enter p0

    #@1
    :try_start_1
    sget-object v5, Lcom/lge/lgdrm/DrmDldClient;->sLocale:Ljava/util/Locale;

    #@3
    .line 961
    .local v5, locale:Ljava/util/Locale;
    new-instance v1, Ljava/lang/StringBuffer;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    #@8
    .line 963
    .local v1, buffer:Ljava/lang/StringBuffer;
    sget-object v8, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    #@a
    .line 964
    .local v8, version:Ljava/lang/String;
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@d
    move-result v9

    #@e
    if-lez v9, :cond_ac

    #@10
    .line 965
    const/4 v9, 0x0

    #@11
    invoke-virtual {v8, v9}, Ljava/lang/String;->charAt(I)C

    #@14
    move-result v9

    #@15
    invoke-static {v9}, Ljava/lang/Character;->isDigit(C)Z

    #@18
    move-result v9

    #@19
    if-eqz v9, :cond_a2

    #@1b
    .line 967
    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1e
    .line 977
    :goto_1e
    const-string v9, "; "

    #@20
    invoke-virtual {v1, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@23
    .line 978
    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    #@26
    move-result-object v4

    #@27
    .line 979
    .local v4, language:Ljava/lang/String;
    if-eqz v4, :cond_b3

    #@29
    .line 980
    invoke-static {v4}, Lcom/lge/lgdrm/DrmDldClient;->convertObsoleteLanguageCodeToNew(Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object v9

    #@2d
    invoke-virtual {v1, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@30
    .line 981
    invoke-virtual {v5}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    #@33
    move-result-object v2

    #@34
    .line 982
    .local v2, country:Ljava/lang/String;
    if-eqz v2, :cond_42

    #@36
    .line 983
    const-string v9, "-"

    #@38
    invoke-virtual {v1, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@3b
    .line 984
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@3e
    move-result-object v9

    #@3f
    invoke-virtual {v1, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@42
    .line 990
    .end local v2           #country:Ljava/lang/String;
    :cond_42
    :goto_42
    const-string v9, ";"

    #@44
    invoke-virtual {v1, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@47
    .line 992
    const-string v9, "REL"

    #@49
    sget-object v10, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    #@4b
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4e
    move-result v9

    #@4f
    if-eqz v9, :cond_61

    #@51
    .line 993
    sget-object v7, Landroid/os/Build;->MODEL:Ljava/lang/String;

    #@53
    .line 994
    .local v7, model:Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@56
    move-result v9

    #@57
    if-lez v9, :cond_61

    #@59
    .line 995
    const-string v9, " "

    #@5b
    invoke-virtual {v1, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@5e
    .line 996
    invoke-virtual {v1, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@61
    .line 999
    .end local v7           #model:Ljava/lang/String;
    :cond_61
    sget-object v3, Landroid/os/Build;->ID:Ljava/lang/String;

    #@63
    .line 1000
    .local v3, id:Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@66
    move-result v9

    #@67
    if-lez v9, :cond_71

    #@69
    .line 1001
    const-string v9, " Build/"

    #@6b
    invoke-virtual {v1, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@6e
    .line 1002
    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@71
    .line 1004
    :cond_71
    iget-object v9, p0, Lcom/lge/lgdrm/DrmDldClient;->context:Landroid/content/Context;

    #@73
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@76
    move-result-object v9

    #@77
    const v10, 0x1040368

    #@7a
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@7d
    move-result-object v9

    #@7e
    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@81
    move-result-object v6

    #@82
    .line 1006
    .local v6, mobile:Ljava/lang/String;
    iget-object v9, p0, Lcom/lge/lgdrm/DrmDldClient;->context:Landroid/content/Context;

    #@84
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@87
    move-result-object v9

    #@88
    const v10, 0x1040367

    #@8b
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@8e
    move-result-object v9

    #@8f
    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@92
    move-result-object v0

    #@93
    .line 1008
    .local v0, base:Ljava/lang/String;
    const/4 v9, 0x2

    #@94
    new-array v9, v9, [Ljava/lang/Object;

    #@96
    const/4 v10, 0x0

    #@97
    aput-object v1, v9, v10

    #@99
    const/4 v10, 0x1

    #@9a
    aput-object v6, v9, v10

    #@9c
    invoke-static {v0, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_9f
    .catchall {:try_start_1 .. :try_end_9f} :catchall_a9

    #@9f
    move-result-object v9

    #@a0
    monitor-exit p0

    #@a1
    return-object v9

    #@a2
    .line 971
    .end local v0           #base:Ljava/lang/String;
    .end local v3           #id:Ljava/lang/String;
    .end local v4           #language:Ljava/lang/String;
    .end local v6           #mobile:Ljava/lang/String;
    :cond_a2
    :try_start_a2
    const-string v9, "3.1"

    #@a4
    invoke-virtual {v1, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_a7
    .catchall {:try_start_a2 .. :try_end_a7} :catchall_a9

    #@a7
    goto/16 :goto_1e

    #@a9
    .line 959
    .end local v1           #buffer:Ljava/lang/StringBuffer;
    .end local v5           #locale:Ljava/util/Locale;
    .end local v8           #version:Ljava/lang/String;
    :catchall_a9
    move-exception v9

    #@aa
    monitor-exit p0

    #@ab
    throw v9

    #@ac
    .line 975
    .restart local v1       #buffer:Ljava/lang/StringBuffer;
    .restart local v5       #locale:Ljava/util/Locale;
    .restart local v8       #version:Ljava/lang/String;
    :cond_ac
    :try_start_ac
    const-string v9, "1.0"

    #@ae
    invoke-virtual {v1, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@b1
    goto/16 :goto_1e

    #@b3
    .line 988
    .restart local v4       #language:Ljava/lang/String;
    :cond_b3
    const-string v9, "en"

    #@b5
    invoke-virtual {v1, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_b8
    .catchall {:try_start_ac .. :try_end_b8} :catchall_a9

    #@b8
    goto :goto_42
.end method

.method private getUserAgentString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 922
    sget-object v1, Lcom/lge/lgdrm/DrmDldClient;->sLockForLocaleSettings:Ljava/lang/Object;

    #@2
    if-nez v1, :cond_11

    #@4
    .line 923
    new-instance v1, Ljava/lang/Object;

    #@6
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@9
    sput-object v1, Lcom/lge/lgdrm/DrmDldClient;->sLockForLocaleSettings:Ljava/lang/Object;

    #@b
    .line 924
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@e
    move-result-object v1

    #@f
    sput-object v1, Lcom/lge/lgdrm/DrmDldClient;->sLocale:Ljava/util/Locale;

    #@11
    .line 927
    :cond_11
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@14
    move-result-object v0

    #@15
    .line 928
    .local v0, currentLocale:Ljava/util/Locale;
    sget-object v1, Lcom/lge/lgdrm/DrmDldClient;->sLocale:Ljava/util/Locale;

    #@17
    invoke-virtual {v1, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    #@1a
    move-result v1

    #@1b
    if-eqz v1, :cond_21

    #@1d
    sget-object v1, Lcom/lge/lgdrm/DrmDldClient;->cachedUA:Ljava/lang/String;

    #@1f
    if-nez v1, :cond_29

    #@21
    .line 929
    :cond_21
    sput-object v0, Lcom/lge/lgdrm/DrmDldClient;->sLocale:Ljava/util/Locale;

    #@23
    .line 930
    invoke-direct {p0}, Lcom/lge/lgdrm/DrmDldClient;->getCurrentUserAgent()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    sput-object v1, Lcom/lge/lgdrm/DrmDldClient;->cachedUA:Ljava/lang/String;

    #@29
    .line 933
    :cond_29
    sget-object v1, Lcom/lge/lgdrm/DrmDldClient;->cachedUA:Ljava/lang/String;

    #@2b
    return-object v1
.end method

.method private httpTransaction(Lcom/lge/lgdrm/DrmDldRequest;)I
    .registers 23
    .parameter "request"

    #@0
    .prologue
    .line 318
    const/4 v5, 0x0

    #@1
    .line 319
    .local v5, content:Ljava/io/InputStream;
    const/4 v10, 0x0

    #@2
    .line 322
    .local v10, httpRequest:Lorg/apache/http/client/methods/HttpRequestBase;
    invoke-direct/range {p0 .. p0}, Lcom/lge/lgdrm/DrmDldClient;->isInterrupt()Z

    #@5
    move-result v18

    #@6
    if-eqz v18, :cond_1b

    #@8
    .line 323
    const/16 v18, 0x7

    #@a
    const/16 v19, 0x0

    #@c
    const/16 v20, 0x0

    #@e
    move-object/from16 v0, p0

    #@10
    move/from16 v1, v18

    #@12
    move-object/from16 v2, v19

    #@14
    move-object/from16 v3, v20

    #@16
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@19
    .line 324
    const/4 v15, -0x1

    #@1a
    .line 553
    :goto_1a
    return v15

    #@1b
    .line 328
    :cond_1b
    :try_start_1b
    move-object/from16 v0, p1

    #@1d
    iget v0, v0, Lcom/lge/lgdrm/DrmDldRequest;->httpMethod:I

    #@1f
    move/from16 v18, v0

    #@21
    const/16 v19, 0x10

    #@23
    move/from16 v0, v18

    #@25
    move/from16 v1, v19

    #@27
    if-ne v0, v1, :cond_54

    #@29
    .line 330
    new-instance v11, Lorg/apache/http/client/methods/HttpGet;

    #@2b
    move-object/from16 v0, p1

    #@2d
    iget-object v0, v0, Lcom/lge/lgdrm/DrmDldRequest;->url:Ljava/lang/String;

    #@2f
    move-object/from16 v18, v0

    #@31
    move-object/from16 v0, v18

    #@33
    invoke-direct {v11, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V
    :try_end_36
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1b .. :try_end_36} :catch_63
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_36} :catch_81

    #@36
    .end local v10           #httpRequest:Lorg/apache/http/client/methods/HttpRequestBase;
    .local v11, httpRequest:Lorg/apache/http/client/methods/HttpRequestBase;
    move-object v10, v11

    #@37
    .line 348
    .end local v11           #httpRequest:Lorg/apache/http/client/methods/HttpRequestBase;
    .restart local v10       #httpRequest:Lorg/apache/http/client/methods/HttpRequestBase;
    :goto_37
    move-object/from16 v0, p0

    #@39
    move-object/from16 v1, p1

    #@3b
    invoke-direct {v0, v10, v1}, Lcom/lge/lgdrm/DrmDldClient;->setHttpHeader(Lorg/apache/http/client/methods/HttpRequestBase;Lcom/lge/lgdrm/DrmDldRequest;)I

    #@3e
    move-result v18

    #@3f
    if-eqz v18, :cond_98

    #@41
    .line 350
    const/16 v18, 0x1

    #@43
    const/16 v19, 0x0

    #@45
    const/16 v20, 0x0

    #@47
    move-object/from16 v0, p0

    #@49
    move/from16 v1, v18

    #@4b
    move-object/from16 v2, v19

    #@4d
    move-object/from16 v3, v20

    #@4f
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@52
    .line 351
    const/4 v15, -0x1

    #@53
    goto :goto_1a

    #@54
    .line 333
    :cond_54
    :try_start_54
    new-instance v11, Lorg/apache/http/client/methods/HttpPost;

    #@56
    move-object/from16 v0, p1

    #@58
    iget-object v0, v0, Lcom/lge/lgdrm/DrmDldRequest;->url:Ljava/lang/String;

    #@5a
    move-object/from16 v18, v0

    #@5c
    move-object/from16 v0, v18

    #@5e
    invoke-direct {v11, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V
    :try_end_61
    .catch Ljava/lang/IllegalArgumentException; {:try_start_54 .. :try_end_61} :catch_63
    .catch Ljava/lang/Exception; {:try_start_54 .. :try_end_61} :catch_81

    #@61
    .end local v10           #httpRequest:Lorg/apache/http/client/methods/HttpRequestBase;
    .restart local v11       #httpRequest:Lorg/apache/http/client/methods/HttpRequestBase;
    move-object v10, v11

    #@62
    .end local v11           #httpRequest:Lorg/apache/http/client/methods/HttpRequestBase;
    .restart local v10       #httpRequest:Lorg/apache/http/client/methods/HttpRequestBase;
    goto :goto_37

    #@63
    .line 335
    :catch_63
    move-exception v7

    #@64
    .line 336
    .local v7, e:Ljava/lang/IllegalArgumentException;
    const-string v18, "DrmDldClient"

    #@66
    const-string v19, "httpTransaction() : Invalid URL"

    #@68
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6b
    .line 337
    invoke-virtual {v7}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@6e
    .line 340
    const/16 v18, 0x1

    #@70
    const/16 v19, 0x0

    #@72
    const/16 v20, 0x0

    #@74
    move-object/from16 v0, p0

    #@76
    move/from16 v1, v18

    #@78
    move-object/from16 v2, v19

    #@7a
    move-object/from16 v3, v20

    #@7c
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@7f
    .line 341
    const/4 v15, -0x1

    #@80
    goto :goto_1a

    #@81
    .line 342
    .end local v7           #e:Ljava/lang/IllegalArgumentException;
    :catch_81
    move-exception v7

    #@82
    .line 343
    .local v7, e:Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    #@85
    .line 344
    const/16 v18, 0x1

    #@87
    const/16 v19, 0x0

    #@89
    const/16 v20, 0x0

    #@8b
    move-object/from16 v0, p0

    #@8d
    move/from16 v1, v18

    #@8f
    move-object/from16 v2, v19

    #@91
    move-object/from16 v3, v20

    #@93
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@96
    .line 345
    const/4 v15, -0x1

    #@97
    goto :goto_1a

    #@98
    .line 355
    .end local v7           #e:Ljava/lang/Exception;
    :cond_98
    invoke-direct/range {p0 .. p0}, Lcom/lge/lgdrm/DrmDldClient;->isInterrupt()Z

    #@9b
    move-result v18

    #@9c
    if-eqz v18, :cond_b2

    #@9e
    .line 356
    const/16 v18, 0x7

    #@a0
    const/16 v19, 0x0

    #@a2
    const/16 v20, 0x0

    #@a4
    move-object/from16 v0, p0

    #@a6
    move/from16 v1, v18

    #@a8
    move-object/from16 v2, v19

    #@aa
    move-object/from16 v3, v20

    #@ac
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@af
    .line 357
    const/4 v15, -0x1

    #@b0
    goto/16 :goto_1a

    #@b2
    .line 361
    :cond_b2
    const/4 v14, 0x0

    #@b3
    .line 363
    .local v14, response:Lorg/apache/http/HttpResponse;
    :try_start_b3
    move-object/from16 v0, p0

    #@b5
    iget-object v0, v0, Lcom/lge/lgdrm/DrmDldClient;->client:Landroid/net/http/AndroidHttpClient;

    #@b7
    move-object/from16 v18, v0

    #@b9
    move-object/from16 v0, v18

    #@bb
    invoke-virtual {v0, v10}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_be
    .catch Ljava/nio/channels/ClosedByInterruptException; {:try_start_b3 .. :try_end_be} :catch_de
    .catch Ljava/io/IOException; {:try_start_b3 .. :try_end_be} :catch_fb
    .catch Ljava/lang/Exception; {:try_start_b3 .. :try_end_be} :catch_113

    #@be
    move-result-object v14

    #@bf
    .line 378
    invoke-interface {v14}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    #@c2
    move-result-object v18

    #@c3
    invoke-interface/range {v18 .. v18}, Lorg/apache/http/StatusLine;->getStatusCode()I

    #@c6
    move-result v16

    #@c7
    .line 380
    .local v16, status:I
    sparse-switch v16, :sswitch_data_3a0

    #@ca
    .line 406
    const/16 v18, 0x3

    #@cc
    const/16 v19, 0x0

    #@ce
    const/16 v20, 0x0

    #@d0
    move-object/from16 v0, p0

    #@d2
    move/from16 v1, v18

    #@d4
    move-object/from16 v2, v19

    #@d6
    move-object/from16 v3, v20

    #@d8
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@db
    .line 407
    const/4 v15, -0x1

    #@dc
    goto/16 :goto_1a

    #@de
    .line 364
    .end local v16           #status:I
    :catch_de
    move-exception v7

    #@df
    .line 365
    .local v7, e:Ljava/nio/channels/ClosedByInterruptException;
    const/16 v18, 0x1

    #@e1
    move/from16 v0, v18

    #@e3
    move-object/from16 v1, p0

    #@e5
    iput-boolean v0, v1, Lcom/lge/lgdrm/DrmDldClient;->interrupted:Z

    #@e7
    .line 366
    const/16 v18, 0x7

    #@e9
    const/16 v19, 0x0

    #@eb
    const/16 v20, 0x0

    #@ed
    move-object/from16 v0, p0

    #@ef
    move/from16 v1, v18

    #@f1
    move-object/from16 v2, v19

    #@f3
    move-object/from16 v3, v20

    #@f5
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@f8
    .line 367
    const/4 v15, -0x1

    #@f9
    goto/16 :goto_1a

    #@fb
    .line 368
    .end local v7           #e:Ljava/nio/channels/ClosedByInterruptException;
    :catch_fb
    move-exception v7

    #@fc
    .line 369
    .local v7, e:Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    #@ff
    .line 370
    const/16 v18, 0x3

    #@101
    const/16 v19, 0x0

    #@103
    const/16 v20, 0x0

    #@105
    move-object/from16 v0, p0

    #@107
    move/from16 v1, v18

    #@109
    move-object/from16 v2, v19

    #@10b
    move-object/from16 v3, v20

    #@10d
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@110
    .line 371
    const/4 v15, -0x1

    #@111
    goto/16 :goto_1a

    #@113
    .line 372
    .end local v7           #e:Ljava/io/IOException;
    :catch_113
    move-exception v7

    #@114
    .line 373
    .local v7, e:Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    #@117
    .line 374
    const/16 v18, 0x1

    #@119
    const/16 v19, 0x0

    #@11b
    const/16 v20, 0x0

    #@11d
    move-object/from16 v0, p0

    #@11f
    move/from16 v1, v18

    #@121
    move-object/from16 v2, v19

    #@123
    move-object/from16 v3, v20

    #@125
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@128
    .line 375
    const/4 v15, -0x1

    #@129
    goto/16 :goto_1a

    #@12b
    .line 389
    .end local v7           #e:Ljava/lang/Exception;
    .restart local v16       #status:I
    :sswitch_12b
    move-object/from16 v0, p1

    #@12d
    iget v0, v0, Lcom/lge/lgdrm/DrmDldRequest;->requestType:I

    #@12f
    move/from16 v18, v0

    #@131
    const/16 v19, 0x1

    #@133
    move/from16 v0, v18

    #@135
    move/from16 v1, v19

    #@137
    if-eq v0, v1, :cond_156

    #@139
    .line 411
    :sswitch_139
    invoke-interface {v14}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    #@13c
    move-result-object v8

    #@13d
    .line 412
    .local v8, entity:Lorg/apache/http/HttpEntity;
    if-nez v8, :cond_17e

    #@13f
    .line 413
    invoke-virtual {v10}, Lorg/apache/http/client/methods/HttpRequestBase;->abort()V

    #@142
    .line 414
    const/16 v18, 0x1

    #@144
    const/16 v19, 0x0

    #@146
    const/16 v20, 0x0

    #@148
    move-object/from16 v0, p0

    #@14a
    move/from16 v1, v18

    #@14c
    move-object/from16 v2, v19

    #@14e
    move-object/from16 v3, v20

    #@150
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@153
    .line 415
    const/4 v15, -0x1

    #@154
    goto/16 :goto_1a

    #@156
    .line 396
    .end local v8           #entity:Lorg/apache/http/HttpEntity;
    :cond_156
    const/16 v18, 0x3

    #@158
    const/16 v19, 0x0

    #@15a
    const/16 v20, 0x0

    #@15c
    move-object/from16 v0, p0

    #@15e
    move/from16 v1, v18

    #@160
    move-object/from16 v2, v19

    #@162
    move-object/from16 v3, v20

    #@164
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@167
    .line 397
    const/4 v15, -0x1

    #@168
    goto/16 :goto_1a

    #@16a
    .line 401
    :sswitch_16a
    const/16 v18, 0x2

    #@16c
    const/16 v19, 0x0

    #@16e
    const/16 v20, 0x0

    #@170
    move-object/from16 v0, p0

    #@172
    move/from16 v1, v18

    #@174
    move-object/from16 v2, v19

    #@176
    move-object/from16 v3, v20

    #@178
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@17b
    .line 402
    const/4 v15, -0x1

    #@17c
    goto/16 :goto_1a

    #@17e
    .line 419
    .restart local v8       #entity:Lorg/apache/http/HttpEntity;
    :cond_17e
    invoke-interface {v8}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    #@181
    move-result-object v9

    #@182
    .line 420
    .local v9, hdr:Lorg/apache/http/Header;
    if-nez v9, :cond_19b

    #@184
    .line 421
    invoke-virtual {v10}, Lorg/apache/http/client/methods/HttpRequestBase;->abort()V

    #@187
    .line 422
    const/16 v18, 0x1

    #@189
    const/16 v19, 0x0

    #@18b
    const/16 v20, 0x0

    #@18d
    move-object/from16 v0, p0

    #@18f
    move/from16 v1, v18

    #@191
    move-object/from16 v2, v19

    #@193
    move-object/from16 v3, v20

    #@195
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@198
    .line 423
    const/4 v15, -0x1

    #@199
    goto/16 :goto_1a

    #@19b
    .line 427
    :cond_19b
    invoke-interface {v9}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    #@19e
    move-result-object v6

    #@19f
    .line 428
    .local v6, contentType:Ljava/lang/String;
    if-nez v6, :cond_1b8

    #@1a1
    .line 429
    invoke-virtual {v10}, Lorg/apache/http/client/methods/HttpRequestBase;->abort()V

    #@1a4
    .line 430
    const/16 v18, 0x1

    #@1a6
    const/16 v19, 0x0

    #@1a8
    const/16 v20, 0x0

    #@1aa
    move-object/from16 v0, p0

    #@1ac
    move/from16 v1, v18

    #@1ae
    move-object/from16 v2, v19

    #@1b0
    move-object/from16 v3, v20

    #@1b2
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@1b5
    .line 431
    const/4 v15, -0x1

    #@1b6
    goto/16 :goto_1a

    #@1b8
    .line 435
    :cond_1b8
    const-string v18, "[;]"

    #@1ba
    move-object/from16 v0, v18

    #@1bc
    invoke-virtual {v6, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@1bf
    move-result-object v17

    #@1c0
    .line 436
    .local v17, subs:[Ljava/lang/String;
    if-nez v17, :cond_1d9

    #@1c2
    .line 437
    invoke-virtual {v10}, Lorg/apache/http/client/methods/HttpRequestBase;->abort()V

    #@1c5
    .line 438
    const/16 v18, 0x1

    #@1c7
    const/16 v19, 0x0

    #@1c9
    const/16 v20, 0x0

    #@1cb
    move-object/from16 v0, p0

    #@1cd
    move/from16 v1, v18

    #@1cf
    move-object/from16 v2, v19

    #@1d1
    move-object/from16 v3, v20

    #@1d3
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@1d6
    .line 439
    const/4 v15, -0x1

    #@1d7
    goto/16 :goto_1a

    #@1d9
    .line 441
    :cond_1d9
    const/16 v18, 0x0

    #@1db
    aget-object v18, v17, v18

    #@1dd
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@1e0
    move-result-object v6

    #@1e1
    .line 444
    invoke-direct/range {p0 .. p0}, Lcom/lge/lgdrm/DrmDldClient;->isInterrupt()Z

    #@1e4
    move-result v18

    #@1e5
    if-eqz v18, :cond_1fb

    #@1e7
    .line 445
    const/16 v18, 0x7

    #@1e9
    const/16 v19, 0x0

    #@1eb
    const/16 v20, 0x0

    #@1ed
    move-object/from16 v0, p0

    #@1ef
    move/from16 v1, v18

    #@1f1
    move-object/from16 v2, v19

    #@1f3
    move-object/from16 v3, v20

    #@1f5
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@1f8
    .line 446
    const/4 v15, -0x1

    #@1f9
    goto/16 :goto_1a

    #@1fb
    .line 450
    :cond_1fb
    :try_start_1fb
    move-object/from16 v0, p0

    #@1fd
    iget-object v0, v0, Lcom/lge/lgdrm/DrmDldClient;->objSession:Lcom/lge/lgdrm/DrmObjectSession;

    #@1ff
    move-object/from16 v18, v0

    #@201
    if-nez v18, :cond_23b

    #@203
    .line 452
    const/16 v18, 0x3

    #@205
    move-object/from16 v0, p0

    #@207
    iget-object v0, v0, Lcom/lge/lgdrm/DrmDldClient;->context:Landroid/content/Context;

    #@209
    move-object/from16 v19, v0

    #@20b
    invoke-static/range {v18 .. v19}, Lcom/lge/lgdrm/DrmManager;->createObjectSession(ILandroid/content/Context;)Lcom/lge/lgdrm/DrmObjectSession;

    #@20e
    move-result-object v18

    #@20f
    move-object/from16 v0, v18

    #@211
    move-object/from16 v1, p0

    #@213
    iput-object v0, v1, Lcom/lge/lgdrm/DrmDldClient;->objSession:Lcom/lge/lgdrm/DrmObjectSession;

    #@215
    .line 453
    move-object/from16 v0, p0

    #@217
    iget-object v0, v0, Lcom/lge/lgdrm/DrmDldClient;->objSession:Lcom/lge/lgdrm/DrmObjectSession;

    #@219
    move-object/from16 v18, v0

    #@21b
    if-nez v18, :cond_23b

    #@21d
    .line 454
    const-string v18, "DrmDldClient"

    #@21f
    const-string v19, "Failed to create DRM object session"

    #@221
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@224
    .line 455
    invoke-virtual {v10}, Lorg/apache/http/client/methods/HttpRequestBase;->abort()V

    #@227
    .line 456
    const/16 v18, 0x1

    #@229
    const/16 v19, 0x0

    #@22b
    const/16 v20, 0x0

    #@22d
    move-object/from16 v0, p0

    #@22f
    move/from16 v1, v18

    #@231
    move-object/from16 v2, v19

    #@233
    move-object/from16 v3, v20

    #@235
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@238
    .line 457
    const/4 v15, -0x1

    #@239
    goto/16 :goto_1a

    #@23b
    .line 462
    :cond_23b
    invoke-interface {v8}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;
    :try_end_23e
    .catch Ljava/nio/channels/ClosedByInterruptException; {:try_start_1fb .. :try_end_23e} :catch_27d
    .catch Ljava/io/IOException; {:try_start_1fb .. :try_end_23e} :catch_29a
    .catch Ljava/lang/IllegalStateException; {:try_start_1fb .. :try_end_23e} :catch_2b2
    .catch Ljava/lang/Exception; {:try_start_1fb .. :try_end_23e} :catch_2cd

    #@23e
    move-result-object v5

    #@23f
    .line 493
    const/4 v4, 0x0

    #@240
    .line 494
    .local v4, attribute:I
    :try_start_240
    move-object/from16 v0, p1

    #@242
    iget v0, v0, Lcom/lge/lgdrm/DrmDldRequest;->requestType:I

    #@244
    move/from16 v18, v0

    #@246
    const/16 v19, 0x1

    #@248
    move/from16 v0, v18

    #@24a
    move/from16 v1, v19

    #@24c
    if-eq v0, v1, :cond_250

    #@24e
    .line 495
    const/16 v4, 0x8

    #@250
    .line 498
    :cond_250
    move-object/from16 v0, p0

    #@252
    iget-object v0, v0, Lcom/lge/lgdrm/DrmDldClient;->objSession:Lcom/lge/lgdrm/DrmObjectSession;

    #@254
    move-object/from16 v18, v0

    #@256
    move-object/from16 v0, p0

    #@258
    iget-object v0, v0, Lcom/lge/lgdrm/DrmDldClient;->filename:Ljava/lang/String;

    #@25a
    move-object/from16 v19, v0

    #@25c
    move-object/from16 v0, v18

    #@25e
    move-object/from16 v1, v19

    #@260
    invoke-virtual {v0, v6, v1, v4}, Lcom/lge/lgdrm/DrmObjectSession;->processInit(Ljava/lang/String;Ljava/lang/String;I)I

    #@263
    move-result v18

    #@264
    if-eqz v18, :cond_31e

    #@266
    .line 499
    invoke-virtual {v10}, Lorg/apache/http/client/methods/HttpRequestBase;->abort()V

    #@269
    .line 500
    const/16 v18, 0x1

    #@26b
    const/16 v19, 0x0

    #@26d
    const/16 v20, 0x0

    #@26f
    move-object/from16 v0, p0

    #@271
    move/from16 v1, v18

    #@273
    move-object/from16 v2, v19

    #@275
    move-object/from16 v3, v20

    #@277
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_27a
    .catch Ljava/lang/IllegalStateException; {:try_start_240 .. :try_end_27a} :catch_2e8
    .catch Ljava/lang/Exception; {:try_start_240 .. :try_end_27a} :catch_303

    #@27a
    .line 501
    const/4 v15, -0x1

    #@27b
    goto/16 :goto_1a

    #@27d
    .line 463
    .end local v4           #attribute:I
    :catch_27d
    move-exception v7

    #@27e
    .line 464
    .local v7, e:Ljava/nio/channels/ClosedByInterruptException;
    const/16 v18, 0x1

    #@280
    move/from16 v0, v18

    #@282
    move-object/from16 v1, p0

    #@284
    iput-boolean v0, v1, Lcom/lge/lgdrm/DrmDldClient;->interrupted:Z

    #@286
    .line 465
    const/16 v18, 0x7

    #@288
    const/16 v19, 0x0

    #@28a
    const/16 v20, 0x0

    #@28c
    move-object/from16 v0, p0

    #@28e
    move/from16 v1, v18

    #@290
    move-object/from16 v2, v19

    #@292
    move-object/from16 v3, v20

    #@294
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@297
    .line 466
    const/4 v15, -0x1

    #@298
    goto/16 :goto_1a

    #@29a
    .line 467
    .end local v7           #e:Ljava/nio/channels/ClosedByInterruptException;
    :catch_29a
    move-exception v7

    #@29b
    .line 468
    .local v7, e:Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    #@29e
    .line 473
    const/16 v18, 0x3

    #@2a0
    const/16 v19, 0x0

    #@2a2
    const/16 v20, 0x0

    #@2a4
    move-object/from16 v0, p0

    #@2a6
    move/from16 v1, v18

    #@2a8
    move-object/from16 v2, v19

    #@2aa
    move-object/from16 v3, v20

    #@2ac
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@2af
    .line 474
    const/4 v15, -0x1

    #@2b0
    goto/16 :goto_1a

    #@2b2
    .line 475
    .end local v7           #e:Ljava/io/IOException;
    :catch_2b2
    move-exception v7

    #@2b3
    .line 476
    .local v7, e:Ljava/lang/IllegalStateException;
    invoke-virtual {v7}, Ljava/lang/IllegalStateException;->printStackTrace()V

    #@2b6
    .line 477
    invoke-virtual {v10}, Lorg/apache/http/client/methods/HttpRequestBase;->abort()V

    #@2b9
    .line 478
    const/16 v18, 0x1

    #@2bb
    const/16 v19, 0x0

    #@2bd
    const/16 v20, 0x0

    #@2bf
    move-object/from16 v0, p0

    #@2c1
    move/from16 v1, v18

    #@2c3
    move-object/from16 v2, v19

    #@2c5
    move-object/from16 v3, v20

    #@2c7
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@2ca
    .line 479
    const/4 v15, -0x1

    #@2cb
    goto/16 :goto_1a

    #@2cd
    .line 480
    .end local v7           #e:Ljava/lang/IllegalStateException;
    :catch_2cd
    move-exception v7

    #@2ce
    .line 481
    .local v7, e:Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    #@2d1
    .line 487
    invoke-virtual {v10}, Lorg/apache/http/client/methods/HttpRequestBase;->abort()V

    #@2d4
    .line 488
    const/16 v18, 0x1

    #@2d6
    const/16 v19, 0x0

    #@2d8
    const/16 v20, 0x0

    #@2da
    move-object/from16 v0, p0

    #@2dc
    move/from16 v1, v18

    #@2de
    move-object/from16 v2, v19

    #@2e0
    move-object/from16 v3, v20

    #@2e2
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@2e5
    .line 489
    const/4 v15, -0x1

    #@2e6
    goto/16 :goto_1a

    #@2e8
    .line 503
    .end local v7           #e:Ljava/lang/Exception;
    .restart local v4       #attribute:I
    :catch_2e8
    move-exception v7

    #@2e9
    .line 504
    .local v7, e:Ljava/lang/IllegalStateException;
    invoke-virtual {v7}, Ljava/lang/IllegalStateException;->printStackTrace()V

    #@2ec
    .line 505
    invoke-virtual {v10}, Lorg/apache/http/client/methods/HttpRequestBase;->abort()V

    #@2ef
    .line 506
    const/16 v18, 0x1

    #@2f1
    const/16 v19, 0x0

    #@2f3
    const/16 v20, 0x0

    #@2f5
    move-object/from16 v0, p0

    #@2f7
    move/from16 v1, v18

    #@2f9
    move-object/from16 v2, v19

    #@2fb
    move-object/from16 v3, v20

    #@2fd
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@300
    .line 507
    const/4 v15, -0x1

    #@301
    goto/16 :goto_1a

    #@303
    .line 508
    .end local v7           #e:Ljava/lang/IllegalStateException;
    :catch_303
    move-exception v7

    #@304
    .line 509
    .local v7, e:Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    #@307
    .line 510
    invoke-virtual {v10}, Lorg/apache/http/client/methods/HttpRequestBase;->abort()V

    #@30a
    .line 511
    const/16 v18, 0x1

    #@30c
    const/16 v19, 0x0

    #@30e
    const/16 v20, 0x0

    #@310
    move-object/from16 v0, p0

    #@312
    move/from16 v1, v18

    #@314
    move-object/from16 v2, v19

    #@316
    move-object/from16 v3, v20

    #@318
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@31b
    .line 512
    const/4 v15, -0x1

    #@31c
    goto/16 :goto_1a

    #@31e
    .line 515
    .end local v7           #e:Ljava/lang/Exception;
    :cond_31e
    const/4 v15, 0x0

    #@31f
    .line 516
    .local v15, retVal:I
    invoke-interface {v8}, Lorg/apache/http/HttpEntity;->isChunked()Z

    #@322
    move-result v18

    #@323
    if-eqz v18, :cond_354

    #@325
    .line 518
    const-wide/16 v18, 0x0

    #@327
    move-object/from16 v0, p0

    #@329
    move-wide/from16 v1, v18

    #@32b
    invoke-direct {v0, v5, v1, v2}, Lcom/lge/lgdrm/DrmDldClient;->processData(Ljava/io/InputStream;J)I

    #@32e
    move-result v15

    #@32f
    .line 536
    :goto_32f
    :try_start_32f
    invoke-interface {v8}, Lorg/apache/http/HttpEntity;->consumeContent()V

    #@332
    .line 538
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_335
    .catch Ljava/nio/channels/ClosedByInterruptException; {:try_start_32f .. :try_end_335} :catch_337
    .catch Ljava/io/IOException; {:try_start_32f .. :try_end_335} :catch_370
    .catch Ljava/lang/Exception; {:try_start_32f .. :try_end_335} :catch_388

    #@335
    goto/16 :goto_1a

    #@337
    .line 539
    :catch_337
    move-exception v7

    #@338
    .line 540
    .local v7, e:Ljava/nio/channels/ClosedByInterruptException;
    const/16 v18, 0x1

    #@33a
    move/from16 v0, v18

    #@33c
    move-object/from16 v1, p0

    #@33e
    iput-boolean v0, v1, Lcom/lge/lgdrm/DrmDldClient;->interrupted:Z

    #@340
    .line 541
    const/16 v18, 0x7

    #@342
    const/16 v19, 0x0

    #@344
    const/16 v20, 0x0

    #@346
    move-object/from16 v0, p0

    #@348
    move/from16 v1, v18

    #@34a
    move-object/from16 v2, v19

    #@34c
    move-object/from16 v3, v20

    #@34e
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@351
    .line 542
    const/4 v15, -0x1

    #@352
    goto/16 :goto_1a

    #@354
    .line 520
    .end local v7           #e:Ljava/nio/channels/ClosedByInterruptException;
    :cond_354
    invoke-interface {v8}, Lorg/apache/http/HttpEntity;->getContentLength()J

    #@357
    move-result-wide v12

    #@358
    .line 522
    .local v12, length:J
    const-wide/16 v18, 0x0

    #@35a
    cmp-long v18, v12, v18

    #@35c
    if-gtz v18, :cond_369

    #@35e
    .line 527
    const-wide/16 v18, 0x0

    #@360
    move-object/from16 v0, p0

    #@362
    move-wide/from16 v1, v18

    #@364
    invoke-direct {v0, v5, v1, v2}, Lcom/lge/lgdrm/DrmDldClient;->processData(Ljava/io/InputStream;J)I

    #@367
    move-result v15

    #@368
    goto :goto_32f

    #@369
    .line 530
    :cond_369
    move-object/from16 v0, p0

    #@36b
    invoke-direct {v0, v5, v12, v13}, Lcom/lge/lgdrm/DrmDldClient;->processData(Ljava/io/InputStream;J)I

    #@36e
    move-result v15

    #@36f
    goto :goto_32f

    #@370
    .line 543
    .end local v12           #length:J
    :catch_370
    move-exception v7

    #@371
    .line 544
    .local v7, e:Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    #@374
    .line 545
    const/16 v18, 0x3

    #@376
    const/16 v19, 0x0

    #@378
    const/16 v20, 0x0

    #@37a
    move-object/from16 v0, p0

    #@37c
    move/from16 v1, v18

    #@37e
    move-object/from16 v2, v19

    #@380
    move-object/from16 v3, v20

    #@382
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@385
    .line 546
    const/4 v15, -0x1

    #@386
    goto/16 :goto_1a

    #@388
    .line 547
    .end local v7           #e:Ljava/io/IOException;
    :catch_388
    move-exception v7

    #@389
    .line 548
    .local v7, e:Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    #@38c
    .line 549
    const/16 v18, 0x1

    #@38e
    const/16 v19, 0x0

    #@390
    const/16 v20, 0x0

    #@392
    move-object/from16 v0, p0

    #@394
    move/from16 v1, v18

    #@396
    move-object/from16 v2, v19

    #@398
    move-object/from16 v3, v20

    #@39a
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@39d
    .line 550
    const/4 v15, -0x1

    #@39e
    goto/16 :goto_1a

    #@3a0
    .line 380
    :sswitch_data_3a0
    .sparse-switch
        0xc8 -> :sswitch_139
        0x194 -> :sswitch_16a
        0x1f4 -> :sswitch_12b
        0x1f6 -> :sswitch_139
    .end sparse-switch
.end method

.method private isInterrupt()Z
    .registers 3

    #@0
    .prologue
    .line 824
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_e

    #@a
    iget-boolean v0, p0, Lcom/lge/lgdrm/DrmDldClient;->interrupted:Z

    #@c
    if-eqz v0, :cond_17

    #@e
    .line 825
    :cond_e
    const-string v0, "DrmDldClient"

    #@10
    const-string v1, "Thread was interrupted"

    #@12
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 826
    const/4 v0, 0x1

    #@16
    .line 828
    :goto_16
    return v0

    #@17
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_16
.end method

.method private processData(Ljava/io/InputStream;J)I
    .registers 14
    .parameter "in"
    .parameter "length"

    #@0
    .prologue
    .line 634
    const/16 v6, 0x1000

    #@2
    new-array v0, v6, [B

    #@4
    .line 637
    .local v0, data:[B
    const-wide/16 v6, 0x0

    #@6
    cmp-long v6, p2, v6

    #@8
    if-nez v6, :cond_3d

    #@a
    .line 641
    :cond_a
    :try_start_a
    invoke-direct {p0}, Lcom/lge/lgdrm/DrmDldClient;->isInterrupt()Z

    #@d
    move-result v6

    #@e
    if-eqz v6, :cond_18

    #@10
    .line 642
    const/4 v6, 0x7

    #@11
    const/4 v7, 0x0

    #@12
    const/4 v8, 0x0

    #@13
    invoke-direct {p0, v6, v7, v8}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@16
    .line 643
    const/4 v6, -0x1

    #@17
    .line 774
    :goto_17
    return v6

    #@18
    .line 646
    :cond_18
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I

    #@1b
    move-result v3

    #@1c
    .line 647
    .local v3, readLen:I
    const/4 v6, -0x1

    #@1d
    if-ne v3, v6, :cond_2d

    #@1f
    .line 684
    .end local v3           #readLen:I
    :cond_1f
    invoke-direct {p0}, Lcom/lge/lgdrm/DrmDldClient;->isInterrupt()Z

    #@22
    move-result v6

    #@23
    if-eqz v6, :cond_72

    #@25
    .line 685
    const/4 v6, 0x7

    #@26
    const/4 v7, 0x0

    #@27
    const/4 v8, 0x0

    #@28
    invoke-direct {p0, v6, v7, v8}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@2b
    .line 686
    const/4 v6, -0x1

    #@2c
    goto :goto_17

    #@2d
    .line 652
    .restart local v3       #readLen:I
    :cond_2d
    iget-object v6, p0, Lcom/lge/lgdrm/DrmDldClient;->objSession:Lcom/lge/lgdrm/DrmObjectSession;

    #@2f
    invoke-virtual {v6, v0, v3}, Lcom/lge/lgdrm/DrmObjectSession;->processUpdate([BI)I

    #@32
    move-result v6

    #@33
    if-eqz v6, :cond_a

    #@35
    .line 654
    const/4 v6, 0x1

    #@36
    const/4 v7, 0x0

    #@37
    const/4 v8, 0x0

    #@38
    invoke-direct {p0, v6, v7, v8}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@3b
    .line 655
    const/4 v6, -0x1

    #@3c
    goto :goto_17

    #@3d
    .line 659
    .end local v3           #readLen:I
    :cond_3d
    const/4 v2, 0x0

    #@3e
    .line 660
    .local v2, index:I
    :goto_3e
    int-to-long v6, v2

    #@3f
    cmp-long v6, v6, p2

    #@41
    if-gez v6, :cond_1f

    #@43
    .line 662
    invoke-direct {p0}, Lcom/lge/lgdrm/DrmDldClient;->isInterrupt()Z

    #@46
    move-result v6

    #@47
    if-eqz v6, :cond_51

    #@49
    .line 663
    const/4 v6, 0x7

    #@4a
    const/4 v7, 0x0

    #@4b
    const/4 v8, 0x0

    #@4c
    invoke-direct {p0, v6, v7, v8}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@4f
    .line 664
    const/4 v6, -0x1

    #@50
    goto :goto_17

    #@51
    .line 667
    :cond_51
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I

    #@54
    move-result v3

    #@55
    .line 668
    .restart local v3       #readLen:I
    const/4 v6, -0x1

    #@56
    if-ne v3, v6, :cond_60

    #@58
    .line 670
    const/4 v6, 0x1

    #@59
    const/4 v7, 0x0

    #@5a
    const/4 v8, 0x0

    #@5b
    invoke-direct {p0, v6, v7, v8}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@5e
    .line 671
    const/4 v6, -0x1

    #@5f
    goto :goto_17

    #@60
    .line 674
    :cond_60
    iget-object v6, p0, Lcom/lge/lgdrm/DrmDldClient;->objSession:Lcom/lge/lgdrm/DrmObjectSession;

    #@62
    invoke-virtual {v6, v0, v3}, Lcom/lge/lgdrm/DrmObjectSession;->processUpdate([BI)I

    #@65
    move-result v6

    #@66
    if-eqz v6, :cond_70

    #@68
    .line 676
    const/4 v6, 0x1

    #@69
    const/4 v7, 0x0

    #@6a
    const/4 v8, 0x0

    #@6b
    invoke-direct {p0, v6, v7, v8}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@6e
    .line 677
    const/4 v6, -0x1

    #@6f
    goto :goto_17

    #@70
    .line 679
    :cond_70
    add-int/2addr v2, v3

    #@71
    goto :goto_3e

    #@72
    .line 689
    .end local v2           #index:I
    .end local v3           #readLen:I
    :cond_72
    iget-object v6, p0, Lcom/lge/lgdrm/DrmDldClient;->objSession:Lcom/lge/lgdrm/DrmObjectSession;

    #@74
    invoke-virtual {v6}, Lcom/lge/lgdrm/DrmObjectSession;->processStatus()I

    #@77
    move-result v4

    #@78
    .line 690
    .local v4, retVal:I
    const/4 v5, 0x2

    #@79
    .line 691
    .local v5, userResponse:I
    packed-switch v4, :pswitch_data_140

    #@7c
    .line 722
    :goto_7c
    :pswitch_7c
    invoke-direct {p0}, Lcom/lge/lgdrm/DrmDldClient;->isInterrupt()Z

    #@7f
    move-result v6

    #@80
    if-eqz v6, :cond_d7

    #@82
    .line 723
    const/4 v6, 0x7

    #@83
    const/4 v7, 0x0

    #@84
    const/4 v8, 0x0

    #@85
    invoke-direct {p0, v6, v7, v8}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@88
    .line 724
    const/4 v6, -0x1

    #@89
    goto :goto_17

    #@8a
    .line 693
    :pswitch_8a
    const/4 v6, 0x1

    #@8b
    const/4 v7, 0x0

    #@8c
    const/4 v8, 0x0

    #@8d
    invoke-direct {p0, v6, v7, v8}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@90
    .line 694
    const/4 v6, -0x1

    #@91
    goto :goto_17

    #@92
    .line 698
    :pswitch_92
    iget-object v6, p0, Lcom/lge/lgdrm/DrmDldClient;->handler:Landroid/os/Handler;

    #@94
    if-eqz v6, :cond_d5

    #@96
    .line 700
    const/4 v6, 0x1

    #@97
    invoke-direct {p0, v6}, Lcom/lge/lgdrm/DrmDldClient;->sendStatus(I)V

    #@9a
    .line 702
    :goto_9a
    iget v6, p0, Lcom/lge/lgdrm/DrmDldClient;->userConfirm:I

    #@9c
    if-eqz v6, :cond_b0

    #@9e
    .line 707
    iget v6, p0, Lcom/lge/lgdrm/DrmDldClient;->userConfirm:I

    #@a0
    if-eqz v6, :cond_a7

    #@a2
    iget v6, p0, Lcom/lge/lgdrm/DrmDldClient;->userConfirm:I

    #@a4
    const/4 v7, 0x2

    #@a5
    if-ne v6, v7, :cond_c3

    #@a7
    .line 709
    :cond_a7
    const/4 v6, 0x7

    #@a8
    const/4 v7, 0x0

    #@a9
    const/4 v8, 0x0

    #@aa
    invoke-direct {p0, v6, v7, v8}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@ad
    .line 710
    const/4 v6, -0x1

    #@ae
    goto/16 :goto_17

    #@b0
    .line 705
    :cond_b0
    const-wide/16 v6, 0x1f4

    #@b2
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_b5
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_b5} :catch_b6
    .catch Ljava/nio/channels/ClosedByInterruptException; {:try_start_a .. :try_end_b5} :catch_c8
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_b5} :catch_fa
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_b5} :catch_11a

    #@b5
    goto :goto_9a

    #@b6
    .line 755
    .end local v4           #retVal:I
    .end local v5           #userResponse:I
    :catch_b6
    move-exception v1

    #@b7
    .line 756
    .local v1, e:Ljava/lang/InterruptedException;
    const/4 v6, 0x1

    #@b8
    iput-boolean v6, p0, Lcom/lge/lgdrm/DrmDldClient;->interrupted:Z

    #@ba
    .line 757
    const/4 v6, 0x7

    #@bb
    const/4 v7, 0x0

    #@bc
    const/4 v8, 0x0

    #@bd
    invoke-direct {p0, v6, v7, v8}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@c0
    .line 758
    const/4 v6, -0x1

    #@c1
    goto/16 :goto_17

    #@c3
    .line 713
    .end local v1           #e:Ljava/lang/InterruptedException;
    .restart local v4       #retVal:I
    .restart local v5       #userResponse:I
    :cond_c3
    const/4 v6, 0x2

    #@c4
    :try_start_c4
    invoke-direct {p0, v6}, Lcom/lge/lgdrm/DrmDldClient;->sendStatus(I)V
    :try_end_c7
    .catch Ljava/lang/InterruptedException; {:try_start_c4 .. :try_end_c7} :catch_b6
    .catch Ljava/nio/channels/ClosedByInterruptException; {:try_start_c4 .. :try_end_c7} :catch_c8
    .catch Ljava/io/IOException; {:try_start_c4 .. :try_end_c7} :catch_fa
    .catch Ljava/lang/Exception; {:try_start_c4 .. :try_end_c7} :catch_11a

    #@c7
    goto :goto_7c

    #@c8
    .line 759
    .end local v4           #retVal:I
    .end local v5           #userResponse:I
    :catch_c8
    move-exception v1

    #@c9
    .line 760
    .local v1, e:Ljava/nio/channels/ClosedByInterruptException;
    const/4 v6, 0x1

    #@ca
    iput-boolean v6, p0, Lcom/lge/lgdrm/DrmDldClient;->interrupted:Z

    #@cc
    .line 761
    const/4 v6, 0x7

    #@cd
    const/4 v7, 0x0

    #@ce
    const/4 v8, 0x0

    #@cf
    invoke-direct {p0, v6, v7, v8}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@d2
    .line 762
    const/4 v6, -0x1

    #@d3
    goto/16 :goto_17

    #@d5
    .line 716
    .end local v1           #e:Ljava/nio/channels/ClosedByInterruptException;
    .restart local v4       #retVal:I
    .restart local v5       #userResponse:I
    :cond_d5
    const/4 v5, 0x1

    #@d6
    goto :goto_7c

    #@d7
    .line 727
    :cond_d7
    :try_start_d7
    iget-object v6, p0, Lcom/lge/lgdrm/DrmDldClient;->objSession:Lcom/lge/lgdrm/DrmObjectSession;

    #@d9
    const/4 v7, 0x0

    #@da
    invoke-virtual {v6, v5, v7}, Lcom/lge/lgdrm/DrmObjectSession;->processEnd(ILandroid/content/ComponentName;)I

    #@dd
    move-result v4

    #@de
    .line 728
    const/4 v6, -0x1

    #@df
    if-ne v4, v6, :cond_132

    #@e1
    .line 729
    iget-object v6, p0, Lcom/lge/lgdrm/DrmDldClient;->objSession:Lcom/lge/lgdrm/DrmObjectSession;

    #@e3
    invoke-virtual {v6}, Lcom/lge/lgdrm/DrmObjectSession;->getFailReason()I

    #@e6
    move-result v6

    #@e7
    packed-switch v6, :pswitch_data_14e

    #@ea
    .line 747
    const/4 v6, 0x1

    #@eb
    const/4 v7, 0x0

    #@ec
    const/4 v8, 0x0

    #@ed
    invoke-direct {p0, v6, v7, v8}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@f0
    .line 750
    :goto_f0
    const/4 v6, -0x1

    #@f1
    goto/16 :goto_17

    #@f3
    .line 731
    :pswitch_f3
    const/4 v6, 0x1

    #@f4
    const/4 v7, 0x0

    #@f5
    const/4 v8, 0x0

    #@f6
    invoke-direct {p0, v6, v7, v8}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_f9
    .catch Ljava/lang/InterruptedException; {:try_start_d7 .. :try_end_f9} :catch_b6
    .catch Ljava/nio/channels/ClosedByInterruptException; {:try_start_d7 .. :try_end_f9} :catch_c8
    .catch Ljava/io/IOException; {:try_start_d7 .. :try_end_f9} :catch_fa
    .catch Ljava/lang/Exception; {:try_start_d7 .. :try_end_f9} :catch_11a

    #@f9
    goto :goto_f0

    #@fa
    .line 763
    .end local v4           #retVal:I
    .end local v5           #userResponse:I
    :catch_fa
    move-exception v1

    #@fb
    .line 764
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    #@fe
    .line 766
    const/4 v6, 0x3

    #@ff
    const/4 v7, 0x0

    #@100
    const/4 v8, 0x0

    #@101
    invoke-direct {p0, v6, v7, v8}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@104
    .line 767
    const/4 v6, -0x1

    #@105
    goto/16 :goto_17

    #@107
    .line 736
    .end local v1           #e:Ljava/io/IOException;
    .restart local v4       #retVal:I
    .restart local v5       #userResponse:I
    :pswitch_107
    const/4 v6, 0x1

    #@108
    :try_start_108
    iget-object v7, p0, Lcom/lge/lgdrm/DrmDldClient;->objSession:Lcom/lge/lgdrm/DrmObjectSession;

    #@10a
    const/4 v8, 0x1

    #@10b
    invoke-virtual {v7, v8}, Lcom/lge/lgdrm/DrmObjectSession;->getFailInfo(I)Ljava/lang/String;

    #@10e
    move-result-object v7

    #@10f
    iget-object v8, p0, Lcom/lge/lgdrm/DrmDldClient;->objSession:Lcom/lge/lgdrm/DrmObjectSession;

    #@111
    const/4 v9, 0x2

    #@112
    invoke-virtual {v8, v9}, Lcom/lge/lgdrm/DrmObjectSession;->getFailInfo(I)Ljava/lang/String;

    #@115
    move-result-object v8

    #@116
    invoke-direct {p0, v6, v7, v8}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_119
    .catch Ljava/lang/InterruptedException; {:try_start_108 .. :try_end_119} :catch_b6
    .catch Ljava/nio/channels/ClosedByInterruptException; {:try_start_108 .. :try_end_119} :catch_c8
    .catch Ljava/io/IOException; {:try_start_108 .. :try_end_119} :catch_fa
    .catch Ljava/lang/Exception; {:try_start_108 .. :try_end_119} :catch_11a

    #@119
    goto :goto_f0

    #@11a
    .line 768
    .end local v4           #retVal:I
    .end local v5           #userResponse:I
    :catch_11a
    move-exception v1

    #@11b
    .line 770
    .local v1, e:Ljava/lang/Exception;
    const/4 v6, 0x1

    #@11c
    const/4 v7, 0x0

    #@11d
    const/4 v8, 0x0

    #@11e
    invoke-direct {p0, v6, v7, v8}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@121
    .line 771
    const/4 v6, -0x1

    #@122
    goto/16 :goto_17

    #@124
    .line 741
    .end local v1           #e:Ljava/lang/Exception;
    .restart local v4       #retVal:I
    .restart local v5       #userResponse:I
    :pswitch_124
    const/4 v6, 0x4

    #@125
    const/4 v7, 0x0

    #@126
    const/4 v8, 0x0

    #@127
    :try_start_127
    invoke-direct {p0, v6, v7, v8}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@12a
    goto :goto_f0

    #@12b
    .line 744
    :pswitch_12b
    const/4 v6, 0x5

    #@12c
    const/4 v7, 0x0

    #@12d
    const/4 v8, 0x0

    #@12e
    invoke-direct {p0, v6, v7, v8}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@131
    goto :goto_f0

    #@132
    .line 751
    :cond_132
    const/4 v6, 0x4

    #@133
    if-ne v4, v6, :cond_13d

    #@135
    .line 753
    iget-object v6, p0, Lcom/lge/lgdrm/DrmDldClient;->objSession:Lcom/lge/lgdrm/DrmObjectSession;

    #@137
    invoke-virtual {v6}, Lcom/lge/lgdrm/DrmObjectSession;->getNextRequest()Lcom/lge/lgdrm/DrmDldRequest;

    #@13a
    move-result-object v6

    #@13b
    iput-object v6, p0, Lcom/lge/lgdrm/DrmDldClient;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;
    :try_end_13d
    .catch Ljava/lang/InterruptedException; {:try_start_127 .. :try_end_13d} :catch_b6
    .catch Ljava/nio/channels/ClosedByInterruptException; {:try_start_127 .. :try_end_13d} :catch_c8
    .catch Ljava/io/IOException; {:try_start_127 .. :try_end_13d} :catch_fa
    .catch Ljava/lang/Exception; {:try_start_127 .. :try_end_13d} :catch_11a

    #@13d
    .line 774
    :cond_13d
    const/4 v6, 0x0

    #@13e
    goto/16 :goto_17

    #@140
    .line 691
    :pswitch_data_140
    .packed-switch -0x1
        :pswitch_8a
        :pswitch_7c
        :pswitch_7c
        :pswitch_92
        :pswitch_92
    .end packed-switch

    #@14e
    .line 729
    :pswitch_data_14e
    .packed-switch 0x0
        :pswitch_f3
        :pswitch_107
        :pswitch_107
        :pswitch_124
        :pswitch_107
        :pswitch_12b
    .end packed-switch
.end method

.method private processRequest()I
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    const/4 v1, -0x1

    #@3
    .line 211
    const/4 v3, 0x2

    #@4
    invoke-direct {p0, v3}, Lcom/lge/lgdrm/DrmDldClient;->sendStatus(I)V

    #@7
    .line 214
    iget-object v3, p0, Lcom/lge/lgdrm/DrmDldClient;->mimeType:Ljava/lang/String;

    #@9
    if-eqz v3, :cond_12

    #@b
    .line 215
    invoke-direct {p0}, Lcom/lge/lgdrm/DrmDldClient;->checkMimeType()I

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_12

    #@11
    .line 234
    :goto_11
    return v1

    #@12
    .line 220
    :cond_12
    const/4 v0, 0x0

    #@13
    .line 221
    .local v0, currentRequest:Lcom/lge/lgdrm/DrmDldRequest;
    :cond_13
    iget-object v3, p0, Lcom/lge/lgdrm/DrmDldClient;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

    #@15
    if-eqz v3, :cond_22

    #@17
    .line 222
    iget-object v0, p0, Lcom/lge/lgdrm/DrmDldClient;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

    #@19
    .line 223
    iput-object v4, p0, Lcom/lge/lgdrm/DrmDldClient;->nextRequest:Lcom/lge/lgdrm/DrmDldRequest;

    #@1b
    .line 225
    invoke-direct {p0, v0}, Lcom/lge/lgdrm/DrmDldClient;->httpTransaction(Lcom/lge/lgdrm/DrmDldRequest;)I

    #@1e
    move-result v3

    #@1f
    if-ne v1, v3, :cond_13

    #@21
    goto :goto_11

    #@22
    .line 230
    :cond_22
    iget-object v1, p0, Lcom/lge/lgdrm/DrmDldClient;->objSession:Lcom/lge/lgdrm/DrmObjectSession;

    #@24
    if-eqz v1, :cond_2d

    #@26
    .line 231
    iget-object v1, p0, Lcom/lge/lgdrm/DrmDldClient;->objSession:Lcom/lge/lgdrm/DrmObjectSession;

    #@28
    invoke-virtual {v1, v2}, Lcom/lge/lgdrm/DrmObjectSession;->destroySession(I)V

    #@2b
    .line 232
    iput-object v4, p0, Lcom/lge/lgdrm/DrmDldClient;->objSession:Lcom/lge/lgdrm/DrmObjectSession;

    #@2d
    :cond_2d
    move v1, v2

    #@2e
    .line 234
    goto :goto_11
.end method

.method private sendStatus(I)V
    .registers 8
    .parameter "processStatus"

    #@0
    .prologue
    const/4 v3, 0x2

    #@1
    const/4 v2, 0x1

    #@2
    const/4 v5, 0x3

    #@3
    const/4 v4, 0x0

    #@4
    .line 841
    iget-object v1, p0, Lcom/lge/lgdrm/DrmDldClient;->handler:Landroid/os/Handler;

    #@6
    if-nez v1, :cond_9

    #@8
    .line 872
    :cond_8
    :goto_8
    return-void

    #@9
    .line 846
    :cond_9
    invoke-direct {p0}, Lcom/lge/lgdrm/DrmDldClient;->isInterrupt()Z

    #@c
    move-result v1

    #@d
    if-nez v1, :cond_8

    #@f
    .line 850
    if-nez p1, :cond_17

    #@11
    .line 851
    iget-object v1, p0, Lcom/lge/lgdrm/DrmDldClient;->handler:Landroid/os/Handler;

    #@13
    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@16
    goto :goto_8

    #@17
    .line 852
    :cond_17
    if-ne p1, v3, :cond_1f

    #@19
    .line 853
    iget-object v1, p0, Lcom/lge/lgdrm/DrmDldClient;->handler:Landroid/os/Handler;

    #@1b
    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@1e
    goto :goto_8

    #@1f
    .line 854
    :cond_1f
    if-ne p1, v2, :cond_27

    #@21
    .line 855
    iget-object v1, p0, Lcom/lge/lgdrm/DrmDldClient;->handler:Landroid/os/Handler;

    #@23
    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@26
    goto :goto_8

    #@27
    .line 856
    :cond_27
    if-ne p1, v5, :cond_8

    #@29
    .line 857
    iget v1, p0, Lcom/lge/lgdrm/DrmDldClient;->errorCode:I

    #@2b
    if-eqz v1, :cond_8

    #@2d
    .line 861
    const/4 v0, 0x0

    #@2e
    .line 862
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/lge/lgdrm/DrmDldClient;->failInfo:Lcom/lge/lgdrm/DrmDldClient$FailInfo;

    #@30
    if-eqz v1, :cond_42

    #@32
    .line 864
    iget-object v1, p0, Lcom/lge/lgdrm/DrmDldClient;->handler:Landroid/os/Handler;

    #@34
    iget v2, p0, Lcom/lge/lgdrm/DrmDldClient;->errorCode:I

    #@36
    iget-object v3, p0, Lcom/lge/lgdrm/DrmDldClient;->failInfo:Lcom/lge/lgdrm/DrmDldClient$FailInfo;

    #@38
    invoke-virtual {v1, v5, v2, v4, v3}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@3b
    move-result-object v0

    #@3c
    .line 870
    :goto_3c
    iget-object v1, p0, Lcom/lge/lgdrm/DrmDldClient;->handler:Landroid/os/Handler;

    #@3e
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@41
    goto :goto_8

    #@42
    .line 866
    :cond_42
    iget-object v1, p0, Lcom/lge/lgdrm/DrmDldClient;->handler:Landroid/os/Handler;

    #@44
    iget v2, p0, Lcom/lge/lgdrm/DrmDldClient;->errorCode:I

    #@46
    invoke-virtual {v1, v5, v2, v4}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@49
    move-result-object v0

    #@4a
    goto :goto_3c
.end method

.method private setError(ILjava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "errorCode"
    .parameter "errorMsg"
    .parameter "redirectURL"

    #@0
    .prologue
    const/4 v4, 0x3

    #@1
    const/4 v3, 0x2

    #@2
    .line 795
    iget-object v0, p0, Lcom/lge/lgdrm/DrmDldClient;->objSession:Lcom/lge/lgdrm/DrmObjectSession;

    #@4
    if-eqz v0, :cond_2a

    #@6
    .line 796
    const-string v0, "DrmDldClient"

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string/jumbo v2, "setError() : Destroy session errCode = "

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 797
    if-ne p1, v3, :cond_2f

    #@21
    .line 798
    iget-object v0, p0, Lcom/lge/lgdrm/DrmDldClient;->objSession:Lcom/lge/lgdrm/DrmObjectSession;

    #@23
    const/4 v1, 0x1

    #@24
    invoke-virtual {v0, v1}, Lcom/lge/lgdrm/DrmObjectSession;->destroySession(I)V

    #@27
    .line 804
    :goto_27
    const/4 v0, 0x0

    #@28
    iput-object v0, p0, Lcom/lge/lgdrm/DrmDldClient;->objSession:Lcom/lge/lgdrm/DrmObjectSession;

    #@2a
    .line 807
    :cond_2a
    iget v0, p0, Lcom/lge/lgdrm/DrmDldClient;->errorCode:I

    #@2c
    if-eqz v0, :cond_3d

    #@2e
    .line 816
    :cond_2e
    :goto_2e
    return-void

    #@2f
    .line 799
    :cond_2f
    if-ne p1, v4, :cond_37

    #@31
    .line 800
    iget-object v0, p0, Lcom/lge/lgdrm/DrmDldClient;->objSession:Lcom/lge/lgdrm/DrmObjectSession;

    #@33
    invoke-virtual {v0, v3}, Lcom/lge/lgdrm/DrmObjectSession;->destroySession(I)V

    #@36
    goto :goto_27

    #@37
    .line 802
    :cond_37
    iget-object v0, p0, Lcom/lge/lgdrm/DrmDldClient;->objSession:Lcom/lge/lgdrm/DrmObjectSession;

    #@39
    invoke-virtual {v0, v4}, Lcom/lge/lgdrm/DrmObjectSession;->destroySession(I)V

    #@3c
    goto :goto_27

    #@3d
    .line 812
    :cond_3d
    iput p1, p0, Lcom/lge/lgdrm/DrmDldClient;->errorCode:I

    #@3f
    .line 813
    if-eqz p2, :cond_2e

    #@41
    .line 814
    new-instance v0, Lcom/lge/lgdrm/DrmDldClient$FailInfo;

    #@43
    invoke-direct {v0, p0, p1, p2, p3}, Lcom/lge/lgdrm/DrmDldClient$FailInfo;-><init>(Lcom/lge/lgdrm/DrmDldClient;ILjava/lang/String;Ljava/lang/String;)V

    #@46
    iput-object v0, p0, Lcom/lge/lgdrm/DrmDldClient;->failInfo:Lcom/lge/lgdrm/DrmDldClient$FailInfo;

    #@48
    goto :goto_2e
.end method

.method private setHttpHeader(Lorg/apache/http/client/methods/HttpRequestBase;Lcom/lge/lgdrm/DrmDldRequest;)I
    .registers 8
    .parameter "httpRequest"
    .parameter "request"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 567
    iget v1, p2, Lcom/lge/lgdrm/DrmDldRequest;->httpMethod:I

    #@3
    const/16 v2, 0x10

    #@5
    if-ne v1, v2, :cond_3e

    #@7
    .line 569
    iget v1, p2, Lcom/lge/lgdrm/DrmDldRequest;->requestType:I

    #@9
    if-ne v1, v4, :cond_36

    #@b
    .line 570
    const-string v1, "Accept"

    #@d
    const-string v2, "application/vnd.oma.drm.roap-trigger+xml, application/vnd.oma.drm.roap-pdu+xml, multipart/related"

    #@f
    invoke-virtual {p1, v1, v2}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    #@12
    .line 611
    :goto_12
    const-string v1, "Connection"

    #@14
    const-string v2, "Keep Alive, Keep-Alive"

    #@16
    invoke-virtual {p1, v1, v2}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    #@19
    .line 612
    const-string v1, "Cache-Control"

    #@1b
    const-string/jumbo v2, "no-cache"

    #@1e
    invoke-virtual {p1, v1, v2}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    #@21
    .line 613
    const-string v1, "Pragma"

    #@23
    const-string/jumbo v2, "no-cache"

    #@26
    invoke-virtual {p1, v1, v2}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    #@29
    .line 615
    iget v1, p2, Lcom/lge/lgdrm/DrmDldRequest;->requestType:I

    #@2b
    if-ne v1, v4, :cond_34

    #@2d
    .line 616
    const-string v1, "DRM-Version"

    #@2f
    const-string v2, "2.1"

    #@31
    invoke-virtual {p1, v1, v2}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    #@34
    .line 619
    :cond_34
    const/4 v1, 0x0

    #@35
    :goto_35
    return v1

    #@36
    .line 572
    :cond_36
    const-string v1, "Accept"

    #@38
    const-string v2, "*/*"

    #@3a
    invoke-virtual {p1, v1, v2}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    #@3d
    goto :goto_12

    #@3e
    .line 576
    :cond_3e
    iget v1, p2, Lcom/lge/lgdrm/DrmDldRequest;->requestType:I

    #@40
    if-ne v1, v4, :cond_65

    #@42
    .line 577
    const-string v1, "Accept"

    #@44
    const-string v2, "application/vnd.oma.drm.roap-trigger+xml, multipart/related"

    #@46
    invoke-virtual {p1, v1, v2}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    #@49
    .line 594
    :cond_49
    :goto_49
    new-instance v0, Lorg/apache/http/entity/InputStreamEntity;

    #@4b
    new-instance v1, Ljava/io/ByteArrayInputStream;

    #@4d
    iget-object v2, p2, Lcom/lge/lgdrm/DrmDldRequest;->data:[B

    #@4f
    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    #@52
    iget-object v2, p2, Lcom/lge/lgdrm/DrmDldRequest;->data:[B

    #@54
    array-length v2, v2

    #@55
    int-to-long v2, v2

    #@56
    invoke-direct {v0, v1, v2, v3}, Lorg/apache/http/entity/InputStreamEntity;-><init>(Ljava/io/InputStream;J)V

    #@59
    .line 595
    .local v0, postEntity:Lorg/apache/http/entity/InputStreamEntity;
    if-nez v0, :cond_b5

    #@5b
    .line 596
    const-string v1, "DrmDldClient"

    #@5d
    const-string/jumbo v2, "setHttpHeader() : Fail to create postEntity"

    #@60
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@63
    .line 597
    const/4 v1, -0x1

    #@64
    goto :goto_35

    #@65
    .line 578
    .end local v0           #postEntity:Lorg/apache/http/entity/InputStreamEntity;
    :cond_65
    iget v1, p2, Lcom/lge/lgdrm/DrmDldRequest;->requestType:I

    #@67
    const/4 v2, 0x4

    #@68
    if-ne v1, v2, :cond_72

    #@6a
    .line 579
    const-string v1, "SOAPAction"

    #@6c
    const-string v2, "http://schemas.microsoft.com/DRM/2007/03/protocols/AcquireLicense"

    #@6e
    invoke-virtual {p1, v1, v2}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    #@71
    goto :goto_49

    #@72
    .line 580
    :cond_72
    iget v1, p2, Lcom/lge/lgdrm/DrmDldRequest;->requestType:I

    #@74
    const/4 v2, 0x5

    #@75
    if-ne v1, v2, :cond_7f

    #@77
    .line 581
    const-string v1, "SOAPAction"

    #@79
    const-string v2, "http://schemas.microsoft.com/DRM/2007/03/protocols/AcknowledgeLicense"

    #@7b
    invoke-virtual {p1, v1, v2}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    #@7e
    goto :goto_49

    #@7f
    .line 582
    :cond_7f
    iget v1, p2, Lcom/lge/lgdrm/DrmDldRequest;->requestType:I

    #@81
    const/4 v2, 0x6

    #@82
    if-ne v1, v2, :cond_8c

    #@84
    .line 583
    const-string v1, "SOAPAction"

    #@86
    const-string v2, "http://schemas.microsoft.com/DRM/2007/03/protocols/JoinDomain"

    #@88
    invoke-virtual {p1, v1, v2}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    #@8b
    goto :goto_49

    #@8c
    .line 584
    :cond_8c
    iget v1, p2, Lcom/lge/lgdrm/DrmDldRequest;->requestType:I

    #@8e
    const/4 v2, 0x7

    #@8f
    if-ne v1, v2, :cond_99

    #@91
    .line 585
    const-string v1, "SOAPAction"

    #@93
    const-string v2, "http://schemas.microsoft.com/DRM/2007/03/protocols/LeaveDomain"

    #@95
    invoke-virtual {p1, v1, v2}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    #@98
    goto :goto_49

    #@99
    .line 586
    :cond_99
    iget v1, p2, Lcom/lge/lgdrm/DrmDldRequest;->requestType:I

    #@9b
    const/16 v2, 0x8

    #@9d
    if-ne v1, v2, :cond_a7

    #@9f
    .line 587
    const-string v1, "SOAPAction"

    #@a1
    const-string v2, "http://schemas.microsoft.com/DRM/2007/03/protocols/ProcessMeteringData"

    #@a3
    invoke-virtual {p1, v1, v2}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    #@a6
    goto :goto_49

    #@a7
    .line 588
    :cond_a7
    iget v1, p2, Lcom/lge/lgdrm/DrmDldRequest;->requestType:I

    #@a9
    const/16 v2, 0x9

    #@ab
    if-ne v1, v2, :cond_49

    #@ad
    .line 589
    const-string v1, "SOAPAction"

    #@af
    const-string v2, "http://schemas.microsoft.com/DRM/2007/03/protocols/GetMeteringCertificate"

    #@b1
    invoke-virtual {p1, v1, v2}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    #@b4
    goto :goto_49

    #@b5
    .line 601
    .restart local v0       #postEntity:Lorg/apache/http/entity/InputStreamEntity;
    :cond_b5
    iget v1, p2, Lcom/lge/lgdrm/DrmDldRequest;->requestType:I

    #@b7
    if-ne v1, v4, :cond_c6

    #@b9
    .line 602
    const-string v1, "application/vnd.oma.drm.roap-pdu+xml"

    #@bb
    invoke-virtual {v0, v1}, Lorg/apache/http/entity/InputStreamEntity;->setContentType(Ljava/lang/String;)V

    #@be
    :goto_be
    move-object v1, p1

    #@bf
    .line 607
    check-cast v1, Lorg/apache/http/client/methods/HttpPost;

    #@c1
    invoke-virtual {v1, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    #@c4
    goto/16 :goto_12

    #@c6
    .line 604
    :cond_c6
    const-string/jumbo v1, "text/xml; charset=utf-8"

    #@c9
    invoke-virtual {v0, v1}, Lorg/apache/http/entity/InputStreamEntity;->setContentType(Ljava/lang/String;)V

    #@cc
    goto :goto_be
.end method


# virtual methods
.method public run()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 166
    iget-object v0, p0, Lcom/lge/lgdrm/DrmDldClient;->handler:Landroid/os/Handler;

    #@5
    if-nez v0, :cond_c

    #@7
    .line 167
    const/16 v0, 0xa

    #@9
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    #@c
    .line 171
    :cond_c
    invoke-direct {p0}, Lcom/lge/lgdrm/DrmDldClient;->getUserAgentString()Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    iput-object v0, p0, Lcom/lge/lgdrm/DrmDldClient;->userAgent:Ljava/lang/String;

    #@12
    .line 172
    iget-object v0, p0, Lcom/lge/lgdrm/DrmDldClient;->userAgent:Ljava/lang/String;

    #@14
    if-nez v0, :cond_25

    #@16
    .line 173
    const-string v0, "DrmDldClient"

    #@18
    const-string/jumbo v1, "run() : Fail to get UAString"

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 174
    invoke-direct {p0, v4, v3, v3}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@21
    .line 175
    invoke-direct {p0, v5}, Lcom/lge/lgdrm/DrmDldClient;->sendStatus(I)V

    #@24
    .line 201
    :goto_24
    return-void

    #@25
    .line 180
    :cond_25
    iget-object v0, p0, Lcom/lge/lgdrm/DrmDldClient;->userAgent:Ljava/lang/String;

    #@27
    invoke-static {v0}, Landroid/net/http/AndroidHttpClient;->newInstance(Ljava/lang/String;)Landroid/net/http/AndroidHttpClient;

    #@2a
    move-result-object v0

    #@2b
    iput-object v0, p0, Lcom/lge/lgdrm/DrmDldClient;->client:Landroid/net/http/AndroidHttpClient;

    #@2d
    .line 181
    iget-object v0, p0, Lcom/lge/lgdrm/DrmDldClient;->client:Landroid/net/http/AndroidHttpClient;

    #@2f
    if-nez v0, :cond_40

    #@31
    .line 182
    const-string v0, "DrmDldClient"

    #@33
    const-string/jumbo v1, "run() : Fail to create http client"

    #@36
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 183
    invoke-direct {p0, v4, v3, v3}, Lcom/lge/lgdrm/DrmDldClient;->setError(ILjava/lang/String;Ljava/lang/String;)V

    #@3c
    .line 184
    invoke-direct {p0, v5}, Lcom/lge/lgdrm/DrmDldClient;->sendStatus(I)V

    #@3f
    goto :goto_24

    #@40
    .line 188
    :cond_40
    iget-object v0, p0, Lcom/lge/lgdrm/DrmDldClient;->client:Landroid/net/http/AndroidHttpClient;

    #@42
    invoke-virtual {v0}, Landroid/net/http/AndroidHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    #@45
    move-result-object v0

    #@46
    const-string v1, "http.socket.timeout"

    #@48
    const/16 v2, 0x7530

    #@4a
    invoke-interface {v0, v1, v2}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    #@4d
    .line 190
    iget-object v0, p0, Lcom/lge/lgdrm/DrmDldClient;->client:Landroid/net/http/AndroidHttpClient;

    #@4f
    invoke-virtual {v0}, Landroid/net/http/AndroidHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    #@52
    move-result-object v0

    #@53
    const-string v1, "http.protocol.handle-redirects"

    #@55
    invoke-interface {v0, v1, v4}, Lorg/apache/http/params/HttpParams;->setBooleanParameter(Ljava/lang/String;Z)Lorg/apache/http/params/HttpParams;

    #@58
    .line 192
    invoke-direct {p0}, Lcom/lge/lgdrm/DrmDldClient;->processRequest()I

    #@5b
    move-result v0

    #@5c
    if-nez v0, :cond_71

    #@5e
    .line 193
    const/4 v0, 0x0

    #@5f
    invoke-direct {p0, v0}, Lcom/lge/lgdrm/DrmDldClient;->sendStatus(I)V

    #@62
    .line 198
    :goto_62
    const-string v0, "DrmDldClient"

    #@64
    const-string v1, "Close HTTP Client"

    #@66
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@69
    .line 199
    iget-object v0, p0, Lcom/lge/lgdrm/DrmDldClient;->client:Landroid/net/http/AndroidHttpClient;

    #@6b
    invoke-virtual {v0}, Landroid/net/http/AndroidHttpClient;->close()V

    #@6e
    .line 200
    iput-object v3, p0, Lcom/lge/lgdrm/DrmDldClient;->client:Landroid/net/http/AndroidHttpClient;

    #@70
    goto :goto_24

    #@71
    .line 195
    :cond_71
    invoke-direct {p0, v5}, Lcom/lge/lgdrm/DrmDldClient;->sendStatus(I)V

    #@74
    goto :goto_62
.end method

.method public setUserConfirm(Z)V
    .registers 3
    .parameter "agreed"

    #@0
    .prologue
    .line 880
    if-eqz p1, :cond_6

    #@2
    .line 881
    const/4 v0, 0x1

    #@3
    iput v0, p0, Lcom/lge/lgdrm/DrmDldClient;->userConfirm:I

    #@5
    .line 885
    :goto_5
    return-void

    #@6
    .line 883
    :cond_6
    const/4 v0, 0x2

    #@7
    iput v0, p0, Lcom/lge/lgdrm/DrmDldClient;->userConfirm:I

    #@9
    goto :goto_5
.end method
