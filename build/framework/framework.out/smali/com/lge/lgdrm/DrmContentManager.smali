.class public final Lcom/lge/lgdrm/DrmContentManager;
.super Ljava/lang/Object;
.source "DrmContentManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "DrmCntMngr"


# instance fields
.field private contentType:I

.field private filename:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 70
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 71
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter "filename"
    .parameter "contentType"

    #@0
    .prologue
    .line 80
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 81
    iput-object p1, p0, Lcom/lge/lgdrm/DrmContentManager;->filename:Ljava/lang/String;

    #@5
    .line 82
    iput p2, p0, Lcom/lge/lgdrm/DrmContentManager;->contentType:I

    #@7
    .line 83
    return-void
.end method

.method private native nativeDeleteRights(Ljava/lang/String;)I
.end method

.method private native nativePostprocessDistributedContent(Ljava/lang/String;)I
.end method

.method private native nativePreprocessDistributeContent(Ljava/lang/String;)I
.end method


# virtual methods
.method public backupContent(Ljava/lang/String;)I
    .registers 4
    .parameter "dstFilename"

    #@0
    .prologue
    .line 167
    const-string v0, "DrmCntMngr"

    #@2
    const-string v1, "backupContent() : Not supported"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 168
    const/4 v0, -0x1

    #@8
    return v0
.end method

.method public copyContent(Ljava/lang/String;)I
    .registers 3
    .parameter "dstFilename"

    #@0
    .prologue
    .line 128
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public deleteContent()I
    .registers 2

    #@0
    .prologue
    .line 115
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public deleteRights()I
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 95
    invoke-static {}, Lcom/lge/lgdrm/DrmManager;->nativeAuthCaller()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_e

    #@6
    .line 96
    new-instance v0, Ljava/lang/SecurityException;

    #@8
    const-string v1, "Need proper permission to access drm"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 99
    :cond_e
    iget v0, p0, Lcom/lge/lgdrm/DrmContentManager;->contentType:I

    #@10
    const/16 v1, 0x51

    #@12
    if-lt v0, v1, :cond_21

    #@14
    iget v0, p0, Lcom/lge/lgdrm/DrmContentManager;->contentType:I

    #@16
    const/16 v1, 0x3000

    #@18
    if-gt v0, v1, :cond_21

    #@1a
    .line 101
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContentManager;->filename:Ljava/lang/String;

    #@1c
    invoke-direct {p0, v0}, Lcom/lge/lgdrm/DrmContentManager;->nativeDeleteRights(Ljava/lang/String;)I

    #@1f
    move-result v0

    #@20
    .line 104
    :goto_20
    return v0

    #@21
    :cond_21
    const/4 v0, 0x0

    #@22
    goto :goto_20
.end method

.method public moveContent(Ljava/lang/String;)I
    .registers 3
    .parameter "dstFilename"

    #@0
    .prologue
    .line 141
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public postprocessDistributedContent()I
    .registers 3

    #@0
    .prologue
    .line 209
    iget v0, p0, Lcom/lge/lgdrm/DrmContentManager;->contentType:I

    #@2
    const/16 v1, 0x3000

    #@4
    if-eq v0, v1, :cond_8

    #@6
    .line 210
    const/4 v0, 0x0

    #@7
    .line 212
    :goto_7
    return v0

    #@8
    :cond_8
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContentManager;->filename:Ljava/lang/String;

    #@a
    invoke-direct {p0, v0}, Lcom/lge/lgdrm/DrmContentManager;->nativePostprocessDistributedContent(Ljava/lang/String;)I

    #@d
    move-result v0

    #@e
    goto :goto_7
.end method

.method public preprocessDistributeContent()I
    .registers 3

    #@0
    .prologue
    .line 193
    iget v0, p0, Lcom/lge/lgdrm/DrmContentManager;->contentType:I

    #@2
    const/16 v1, 0x3000

    #@4
    if-eq v0, v1, :cond_13

    #@6
    iget v0, p0, Lcom/lge/lgdrm/DrmContentManager;->contentType:I

    #@8
    const/high16 v1, 0x10

    #@a
    and-int/2addr v0, v1

    #@b
    if-nez v0, :cond_13

    #@d
    iget v0, p0, Lcom/lge/lgdrm/DrmContentManager;->contentType:I

    #@f
    const/high16 v1, 0x80

    #@11
    if-ne v0, v1, :cond_1a

    #@13
    .line 196
    :cond_13
    iget-object v0, p0, Lcom/lge/lgdrm/DrmContentManager;->filename:Ljava/lang/String;

    #@15
    invoke-direct {p0, v0}, Lcom/lge/lgdrm/DrmContentManager;->nativePreprocessDistributeContent(Ljava/lang/String;)I

    #@18
    move-result v0

    #@19
    .line 198
    :goto_19
    return v0

    #@1a
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_19
.end method

.method public renameContent(Ljava/lang/String;)I
    .registers 3
    .parameter "dstFilename"

    #@0
    .prologue
    .line 154
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public restoreContent(Ljava/lang/String;)I
    .registers 4
    .parameter "dstFilename"

    #@0
    .prologue
    .line 181
    const-string v0, "DrmCntMngr"

    #@2
    const-string/jumbo v1, "restoreContent() : Not supported"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 182
    const/4 v0, -0x1

    #@9
    return v0
.end method
