.class public final Lcom/lge/lgdrm/DrmFwExt;
.super Ljava/lang/Object;
.source "DrmFwExt.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/lgdrm/DrmFwExt$MediaFile;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "DrmFwExt"


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 50
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 51
    return-void
.end method

.method public static checkDRMMediaFile(Landroid/content/Context;Ljava/lang/String;Z)I
    .registers 9
    .parameter "context"
    .parameter "filename"
    .parameter "checkIsImage"

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v2, 0x0

    #@2
    const/4 v3, 0x1

    #@3
    .line 206
    if-eqz p1, :cond_7

    #@5
    if-nez p0, :cond_8

    #@7
    .line 243
    :cond_7
    :goto_7
    return v2

    #@8
    .line 210
    :cond_8
    const/4 v5, 0x0

    #@9
    invoke-static {v4, p1, v5}, Lcom/lge/lgdrm/DrmManager;->isSupportedExtension(ILjava/lang/String;Ljava/lang/String;)Z

    #@c
    move-result v5

    #@d
    if-eqz v5, :cond_7

    #@f
    .line 214
    invoke-static {p1}, Lcom/lge/lgdrm/DrmManager;->isDRM(Ljava/lang/String;)I

    #@12
    move-result v0

    #@13
    .line 215
    .local v0, drmType:I
    const/16 v5, 0x10

    #@15
    if-lt v0, v5, :cond_7

    #@17
    const/16 v5, 0x3000

    #@19
    if-gt v0, v5, :cond_7

    #@1b
    .line 219
    const-string v2, "android.permission.ACCESS_LGDRM"

    #@1d
    invoke-virtual {p0, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@20
    move-result v2

    #@21
    if-eqz v2, :cond_25

    #@23
    move v2, v3

    #@24
    .line 220
    goto :goto_7

    #@25
    .line 224
    :cond_25
    if-eqz p2, :cond_2f

    #@27
    .line 225
    :try_start_27
    invoke-static {p1}, Lcom/lge/lgdrm/DrmContent;->getContentType(Ljava/lang/String;)I

    #@2a
    move-result v2

    #@2b
    if-ne v3, v2, :cond_2f

    #@2d
    move v2, v3

    #@2e
    .line 226
    goto :goto_7

    #@2f
    .line 230
    :cond_2f
    invoke-static {p1, p0}, Lcom/lge/lgdrm/DrmManager;->createContentSession(Ljava/lang/String;Landroid/content/Context;)Lcom/lge/lgdrm/DrmContentSession;

    #@32
    move-result-object v1

    #@33
    .line 231
    .local v1, session:Lcom/lge/lgdrm/DrmContentSession;
    if-nez v1, :cond_37

    #@35
    move v2, v3

    #@36
    .line 232
    goto :goto_7

    #@37
    .line 235
    :cond_37
    const/4 v2, 0x1

    #@38
    const/4 v5, 0x0

    #@39
    invoke-virtual {v1, v2, v5}, Lcom/lge/lgdrm/DrmContentSession;->judgeRight(IZ)I

    #@3c
    move-result v2

    #@3d
    if-nez v2, :cond_49

    #@3f
    .line 236
    const/4 v2, 0x0

    #@40
    invoke-virtual {v1, v2}, Lcom/lge/lgdrm/DrmContentSession;->setDecryptionInfo(Z)I
    :try_end_43
    .catch Ljava/lang/Exception; {:try_start_27 .. :try_end_43} :catch_48

    #@43
    move-result v2

    #@44
    if-nez v2, :cond_49

    #@46
    move v2, v4

    #@47
    .line 237
    goto :goto_7

    #@48
    .line 240
    .end local v1           #session:Lcom/lge/lgdrm/DrmContentSession;
    :catch_48
    move-exception v2

    #@49
    :cond_49
    move v2, v3

    #@4a
    .line 243
    goto :goto_7
.end method

.method public static checkDRMRingtone(Landroid/content/Context;Ljava/lang/String;ZZZ)I
    .registers 14
    .parameter "context"
    .parameter "filename"
    .parameter "setKey"
    .parameter "resetKey"
    .parameter "consume"

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v2, 0x0

    #@2
    const/4 v3, 0x1

    #@3
    .line 60
    if-nez p1, :cond_6

    #@5
    .line 103
    :cond_5
    :goto_5
    return v2

    #@6
    .line 64
    :cond_6
    const/4 v5, 0x0

    #@7
    invoke-static {v4, p1, v5}, Lcom/lge/lgdrm/DrmManager;->isSupportedExtension(ILjava/lang/String;Ljava/lang/String;)Z

    #@a
    move-result v5

    #@b
    if-eqz v5, :cond_5

    #@d
    .line 68
    invoke-static {p1}, Lcom/lge/lgdrm/DrmManager;->isDRM(Ljava/lang/String;)I

    #@10
    move-result v0

    #@11
    .line 69
    .local v0, drmType:I
    const/16 v5, 0x10

    #@13
    if-lt v0, v5, :cond_5

    #@15
    const/16 v5, 0x3000

    #@17
    if-gt v0, v5, :cond_5

    #@19
    .line 74
    :try_start_19
    invoke-static {p1, p0}, Lcom/lge/lgdrm/DrmManager;->createContentSession(Ljava/lang/String;Landroid/content/Context;)Lcom/lge/lgdrm/DrmContentSession;

    #@1c
    move-result-object v1

    #@1d
    .line 75
    .local v1, session:Lcom/lge/lgdrm/DrmContentSession;
    if-nez v1, :cond_21

    #@1f
    move v2, v3

    #@20
    .line 76
    goto :goto_5

    #@21
    .line 79
    :cond_21
    const/4 v2, 0x2

    #@22
    invoke-virtual {v1, v2}, Lcom/lge/lgdrm/DrmContentSession;->isActionSupported(I)Z

    #@25
    move-result v2

    #@26
    if-nez v2, :cond_2a

    #@28
    move v2, v3

    #@29
    .line 80
    goto :goto_5

    #@2a
    .line 83
    :cond_2a
    if-eqz p2, :cond_35

    #@2c
    .line 84
    const/4 v2, 0x0

    #@2d
    invoke-virtual {v1, v2}, Lcom/lge/lgdrm/DrmContentSession;->setDecryptionInfo(Z)I

    #@30
    move-result v2

    #@31
    if-eqz v2, :cond_35

    #@33
    move v2, v3

    #@34
    .line 85
    goto :goto_5

    #@35
    .line 88
    :cond_35
    if-eqz p3, :cond_40

    #@37
    .line 89
    const/4 v2, 0x1

    #@38
    invoke-virtual {v1, v2}, Lcom/lge/lgdrm/DrmContentSession;->setDecryptionInfo(Z)I

    #@3b
    move-result v2

    #@3c
    if-eqz v2, :cond_40

    #@3e
    move v2, v3

    #@3f
    .line 90
    goto :goto_5

    #@40
    .line 94
    :cond_40
    if-eqz p4, :cond_4b

    #@42
    .line 96
    invoke-virtual {v1}, Lcom/lge/lgdrm/DrmContentSession;->getDrmTime()J

    #@45
    move-result-wide v5

    #@46
    const-wide/16 v7, 0x0

    #@48
    invoke-virtual {v1, v5, v6, v7, v8}, Lcom/lge/lgdrm/DrmContentSession;->consumeRight(JJ)I
    :try_end_4b
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_4b} :catch_4d

    #@4b
    :cond_4b
    move v2, v4

    #@4c
    .line 99
    goto :goto_5

    #@4d
    .line 100
    .end local v1           #session:Lcom/lge/lgdrm/DrmContentSession;
    :catch_4d
    move-exception v2

    #@4e
    move v2, v3

    #@4f
    .line 103
    goto :goto_5
.end method

.method public static getActualRingtoneUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .registers 15
    .parameter "context"
    .parameter "ringtoneUri"

    #@0
    .prologue
    const/4 v12, 0x1

    #@1
    const/4 v11, 0x0

    #@2
    const/4 v3, 0x0

    #@3
    .line 114
    if-eqz p0, :cond_7

    #@5
    if-nez p1, :cond_8

    #@7
    .line 162
    :cond_7
    :goto_7
    return-object v3

    #@8
    .line 117
    :cond_8
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@b
    move-result-object v9

    #@c
    .line 118
    .local v9, scheme:Ljava/lang/String;
    if-eqz v9, :cond_16

    #@e
    const-string v0, "file"

    #@10
    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v0

    #@14
    if-eqz v0, :cond_1b

    #@16
    .line 119
    :cond_16
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    goto :goto_7

    #@1b
    .line 121
    :cond_1b
    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    #@1e
    move-result-object v6

    #@1f
    .line 122
    .local v6, authority:Ljava/lang/String;
    const-string/jumbo v0, "settings"

    #@22
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v0

    #@26
    if-eqz v0, :cond_46

    #@28
    .line 123
    invoke-static {p1}, Landroid/media/RingtoneManager;->getDefaultType(Landroid/net/Uri;)I

    #@2b
    move-result v0

    #@2c
    invoke-static {p0, v0}, Landroid/media/RingtoneManager;->getActualDefaultRingtoneUri(Landroid/content/Context;I)Landroid/net/Uri;

    #@2f
    move-result-object v10

    #@30
    .line 125
    .local v10, uri:Landroid/net/Uri;
    if-eqz v10, :cond_7

    #@32
    .line 128
    invoke-virtual {v10}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    #@35
    move-result-object v6

    #@36
    .line 129
    if-eqz v6, :cond_41

    #@38
    const-string/jumbo v0, "settings"

    #@3b
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3e
    move-result v0

    #@3f
    if-nez v0, :cond_7

    #@41
    .line 133
    :cond_41
    invoke-static {p0, v10}, Lcom/lge/lgdrm/DrmFwExt;->getActualRingtoneUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    #@44
    move-result-object v3

    #@45
    goto :goto_7

    #@46
    .line 134
    .end local v10           #uri:Landroid/net/Uri;
    :cond_46
    const-string/jumbo v0, "media"

    #@49
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4c
    move-result v0

    #@4d
    if-eqz v0, :cond_78

    #@4f
    .line 135
    const/4 v7, 0x0

    #@50
    .line 136
    .local v7, cursor:Landroid/database/Cursor;
    const/4 v8, 0x0

    #@51
    .line 137
    .local v8, data:Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@54
    move-result-object v0

    #@55
    new-array v2, v12, [Ljava/lang/String;

    #@57
    const-string v1, "_data"

    #@59
    aput-object v1, v2, v11

    #@5b
    move-object v1, p1

    #@5c
    move-object v4, v3

    #@5d
    move-object v5, v3

    #@5e
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@61
    move-result-object v7

    #@62
    .line 139
    if-eqz v7, :cond_71

    #@64
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    #@67
    move-result v0

    #@68
    if-ne v0, v12, :cond_71

    #@6a
    .line 140
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    #@6d
    .line 141
    invoke-interface {v7, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@70
    move-result-object v8

    #@71
    .line 143
    :cond_71
    if-eqz v7, :cond_76

    #@73
    .line 144
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@76
    :cond_76
    move-object v3, v8

    #@77
    .line 146
    goto :goto_7

    #@78
    .line 147
    .end local v7           #cursor:Landroid/database/Cursor;
    .end local v8           #data:Ljava/lang/String;
    :cond_78
    const-string v0, "drm"

    #@7a
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7d
    move-result v0

    #@7e
    if-eqz v0, :cond_7

    #@80
    .line 148
    const/4 v7, 0x0

    #@81
    .line 149
    .restart local v7       #cursor:Landroid/database/Cursor;
    const/4 v8, 0x0

    #@82
    .line 151
    .restart local v8       #data:Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@85
    move-result-object v0

    #@86
    new-array v2, v12, [Ljava/lang/String;

    #@88
    const-string v1, "_data"

    #@8a
    aput-object v1, v2, v11

    #@8c
    move-object v1, p1

    #@8d
    move-object v4, v3

    #@8e
    move-object v5, v3

    #@8f
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@92
    move-result-object v7

    #@93
    .line 153
    if-eqz v7, :cond_a2

    #@95
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    #@98
    move-result v0

    #@99
    if-ne v0, v12, :cond_a2

    #@9b
    .line 154
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    #@9e
    .line 155
    invoke-interface {v7, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@a1
    move-result-object v8

    #@a2
    .line 157
    :cond_a2
    if-eqz v7, :cond_a7

    #@a4
    .line 158
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@a7
    :cond_a7
    move-object v3, v8

    #@a8
    .line 160
    goto/16 :goto_7
.end method

.method public static getDRMAlbumArt(Landroid/content/Context;Ljava/lang/String;)[B
    .registers 5
    .parameter "context"
    .parameter "path"

    #@0
    .prologue
    .line 247
    const/4 v0, 0x0

    #@1
    .line 249
    .local v0, albumArt:[B
    :try_start_1
    new-instance v1, Landroid/media/MediaMetadataRetriever;

    #@3
    invoke-direct {v1}, Landroid/media/MediaMetadataRetriever;-><init>()V

    #@6
    .line 250
    .local v1, retriever:Landroid/media/MediaMetadataRetriever;
    invoke-virtual {v1, p1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    #@9
    .line 251
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->getEmbeddedPicture()[B

    #@c
    move-result-object v0

    #@d
    .line 252
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_10} :catch_11

    #@10
    .line 255
    .end local v1           #retriever:Landroid/media/MediaMetadataRetriever;
    :goto_10
    return-object v0

    #@11
    .line 253
    :catch_11
    move-exception v2

    #@12
    goto :goto_10
.end method

.method public static setDRMMediaQuery(Landroid/content/Context;Landroid/database/sqlite/SQLiteQueryBuilder;Z)V
    .registers 4
    .parameter "context"
    .parameter "qb"
    .parameter "appendAND"

    #@0
    .prologue
    .line 180
    const-string v0, "android.permission.ACCESS_LGDRM"

    #@2
    invoke-virtual {p0, v0}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_9

    #@8
    .line 195
    :cond_8
    :goto_8
    return-void

    #@9
    .line 184
    :cond_9
    const-string v0, "android.permission.ACCESS_LGDRM_CNTLIST"

    #@b
    invoke-virtual {p0, v0}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_8

    #@11
    .line 190
    if-eqz p2, :cond_19

    #@13
    .line 191
    const-string v0, " AND (mime_type NOT IN(\'application/vnd.oma.drm.message\',\'application/vnd.oma.drm.content\',\'application/vnd.oma.drm.dcf\'))"

    #@15
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    #@18
    goto :goto_8

    #@19
    .line 193
    :cond_19
    const-string v0, " mime_type NOT IN(\'application/vnd.oma.drm.message\',\'application/vnd.oma.drm.content\',\'application/vnd.oma.drm.dcf\')"

    #@1b
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    #@1e
    goto :goto_8
.end method

.method public static setDataSourceFromResource(Landroid/content/Context;Landroid/media/MediaPlayer;I)V
    .registers 10
    .parameter "context"
    .parameter "player"
    .parameter "res"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 167
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;

    #@7
    move-result-object v6

    #@8
    .line 168
    .local v6, afd:Landroid/content/res/AssetFileDescriptor;
    if-eqz v6, :cond_1d

    #@a
    .line 169
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    #@11
    move-result-wide v2

    #@12
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    #@15
    move-result-wide v4

    #@16
    move-object v0, p1

    #@17
    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V

    #@1a
    .line 171
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V

    #@1d
    .line 173
    :cond_1d
    return-void
.end method
