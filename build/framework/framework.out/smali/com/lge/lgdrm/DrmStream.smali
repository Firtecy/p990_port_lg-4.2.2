.class public final Lcom/lge/lgdrm/DrmStream;
.super Ljava/io/InputStream;
.source "DrmStream.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "DrmStream"


# instance fields
.field private b:[B

.field protected filename:Ljava/lang/String;

.field private nativeHandle:I


# direct methods
.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 37
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    #@3
    .line 30
    const/4 v0, 0x1

    #@4
    new-array v0, v0, [B

    #@6
    iput-object v0, p0, Lcom/lge/lgdrm/DrmStream;->b:[B

    #@8
    .line 38
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;I)V
    .registers 5
    .parameter "filename"
    .parameter "nativeHandle"

    #@0
    .prologue
    .line 43
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    #@3
    .line 30
    const/4 v0, 0x1

    #@4
    new-array v0, v0, [B

    #@6
    iput-object v0, p0, Lcom/lge/lgdrm/DrmStream;->b:[B

    #@8
    .line 44
    if-nez p2, :cond_12

    #@a
    .line 45
    const-string v0, "DrmStream"

    #@c
    const-string v1, "DrmStream() : Invalid native handle"

    #@e
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 51
    :goto_11
    return-void

    #@12
    .line 49
    :cond_12
    iput-object p1, p0, Lcom/lge/lgdrm/DrmStream;->filename:Ljava/lang/String;

    #@14
    .line 50
    iput p2, p0, Lcom/lge/lgdrm/DrmStream;->nativeHandle:I

    #@16
    goto :goto_11
.end method

.method private native nativeAvailable(I)I
.end method

.method private native nativeClose(I)V
.end method

.method private native nativeRead(I[BII)I
.end method

.method private native nativeSkip(IJ)J
.end method


# virtual methods
.method public available()I
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 56
    iget v0, p0, Lcom/lge/lgdrm/DrmStream;->nativeHandle:I

    #@2
    if-nez v0, :cond_c

    #@4
    .line 57
    new-instance v0, Ljava/io/IOException;

    #@6
    const-string v1, "File was not opened properly"

    #@8
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 59
    :cond_c
    iget v0, p0, Lcom/lge/lgdrm/DrmStream;->nativeHandle:I

    #@e
    invoke-direct {p0, v0}, Lcom/lge/lgdrm/DrmStream;->nativeAvailable(I)I

    #@11
    move-result v0

    #@12
    return v0
.end method

.method public close()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 65
    iget v0, p0, Lcom/lge/lgdrm/DrmStream;->nativeHandle:I

    #@2
    if-nez v0, :cond_c

    #@4
    .line 66
    new-instance v0, Ljava/io/IOException;

    #@6
    const-string v1, "File was not opened properly"

    #@8
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 69
    :cond_c
    iget v0, p0, Lcom/lge/lgdrm/DrmStream;->nativeHandle:I

    #@e
    invoke-direct {p0, v0}, Lcom/lge/lgdrm/DrmStream;->nativeClose(I)V

    #@11
    .line 70
    const/4 v0, 0x0

    #@12
    iput v0, p0, Lcom/lge/lgdrm/DrmStream;->nativeHandle:I

    #@14
    .line 71
    return-void
.end method

.method public getPath()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 174
    iget-object v0, p0, Lcom/lge/lgdrm/DrmStream;->filename:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public declared-synchronized mark(I)V
    .registers 2
    .parameter "readlimit"

    #@0
    .prologue
    .line 75
    monitor-enter p0

    #@1
    monitor-exit p0

    #@2
    return-void
.end method

.method public markSupported()Z
    .registers 2

    #@0
    .prologue
    .line 79
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public read()I
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v1, -0x1

    #@2
    .line 87
    iget v2, p0, Lcom/lge/lgdrm/DrmStream;->nativeHandle:I

    #@4
    if-nez v2, :cond_e

    #@6
    .line 88
    new-instance v1, Ljava/io/IOException;

    #@8
    const-string v2, "File was not opened properly"

    #@a
    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@d
    throw v1

    #@e
    .line 91
    :cond_e
    iget-object v2, p0, Lcom/lge/lgdrm/DrmStream;->b:[B

    #@10
    const/4 v3, 0x1

    #@11
    invoke-virtual {p0, v2, v4, v3}, Lcom/lge/lgdrm/DrmStream;->read([BII)I

    #@14
    move-result v0

    #@15
    .line 92
    .local v0, nRead:I
    if-ne v0, v1, :cond_18

    #@17
    .line 96
    :goto_17
    return v1

    #@18
    :cond_18
    iget-object v1, p0, Lcom/lge/lgdrm/DrmStream;->b:[B

    #@1a
    aget-byte v1, v1, v4

    #@1c
    and-int/lit16 v1, v1, 0xff

    #@1e
    goto :goto_17
.end method

.method public read([B)I
    .registers 4
    .parameter "b"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 102
    iget v0, p0, Lcom/lge/lgdrm/DrmStream;->nativeHandle:I

    #@2
    if-nez v0, :cond_c

    #@4
    .line 103
    new-instance v0, Ljava/io/IOException;

    #@6
    const-string v1, "File was not opened properly"

    #@8
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 106
    :cond_c
    const/4 v0, 0x0

    #@d
    array-length v1, p1

    #@e
    invoke-virtual {p0, p1, v0, v1}, Lcom/lge/lgdrm/DrmStream;->read([BII)I

    #@11
    move-result v0

    #@12
    return v0
.end method

.method public read([BII)I
    .registers 8
    .parameter "b"
    .parameter "off"
    .parameter "len"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 112
    const/4 v1, 0x0

    #@1
    .line 113
    .local v1, nRead:I
    const/4 v0, 0x0

    #@2
    .line 115
    .local v0, i:I
    iget v2, p0, Lcom/lge/lgdrm/DrmStream;->nativeHandle:I

    #@4
    if-nez v2, :cond_e

    #@6
    .line 116
    new-instance v2, Ljava/io/IOException;

    #@8
    const-string v3, "File was not opened properly"

    #@a
    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@d
    throw v2

    #@e
    .line 119
    :cond_e
    if-nez p1, :cond_19

    #@10
    .line 120
    new-instance v2, Ljava/lang/NullPointerException;

    #@12
    const-string/jumbo v3, "parameter b is NULL"

    #@15
    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@18
    throw v2

    #@19
    .line 122
    :cond_19
    or-int v2, p2, p3

    #@1b
    if-ltz v2, :cond_21

    #@1d
    array-length v2, p1

    #@1e
    sub-int/2addr v2, p2

    #@1f
    if-le p3, v2, :cond_27

    #@21
    .line 123
    :cond_21
    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@23
    invoke-direct {v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@26
    throw v2

    #@27
    .line 126
    :cond_27
    iget v2, p0, Lcom/lge/lgdrm/DrmStream;->nativeHandle:I

    #@29
    invoke-direct {p0, v2, p1, p2, p3}, Lcom/lge/lgdrm/DrmStream;->nativeRead(I[BII)I

    #@2c
    move-result v1

    #@2d
    .line 127
    if-nez v1, :cond_31

    #@2f
    .line 129
    const/4 v1, -0x1

    #@30
    .line 136
    .end local v1           #nRead:I
    :cond_30
    return v1

    #@31
    .line 130
    .restart local v1       #nRead:I
    :cond_31
    if-gez v1, :cond_30

    #@33
    .line 131
    iget v2, p0, Lcom/lge/lgdrm/DrmStream;->nativeHandle:I

    #@35
    invoke-direct {p0, v2}, Lcom/lge/lgdrm/DrmStream;->nativeClose(I)V

    #@38
    .line 132
    const/4 v2, 0x0

    #@39
    iput v2, p0, Lcom/lge/lgdrm/DrmStream;->nativeHandle:I

    #@3b
    .line 133
    new-instance v2, Ljava/io/IOException;

    #@3d
    invoke-direct {v2}, Ljava/io/IOException;-><init>()V

    #@40
    throw v2
.end method

.method public declared-synchronized reset()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 142
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Lcom/lge/lgdrm/DrmStream;->nativeHandle:I

    #@3
    if-eqz v0, :cond_d

    #@5
    .line 143
    iget v0, p0, Lcom/lge/lgdrm/DrmStream;->nativeHandle:I

    #@7
    invoke-direct {p0, v0}, Lcom/lge/lgdrm/DrmStream;->nativeClose(I)V

    #@a
    .line 144
    const/4 v0, 0x0

    #@b
    iput v0, p0, Lcom/lge/lgdrm/DrmStream;->nativeHandle:I

    #@d
    .line 147
    :cond_d
    new-instance v0, Ljava/io/IOException;

    #@f
    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    #@12
    throw v0
    :try_end_13
    .catchall {:try_start_1 .. :try_end_13} :catchall_13

    #@13
    .line 142
    :catchall_13
    move-exception v0

    #@14
    monitor-exit p0

    #@15
    throw v0
.end method

.method public skip(J)J
    .registers 6
    .parameter "n"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const-wide/16 v0, 0x0

    #@2
    .line 153
    iget v2, p0, Lcom/lge/lgdrm/DrmStream;->nativeHandle:I

    #@4
    if-nez v2, :cond_e

    #@6
    .line 154
    new-instance v0, Ljava/io/IOException;

    #@8
    const-string v1, "File was not opened properly"

    #@a
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 157
    :cond_e
    cmp-long v2, p1, v0

    #@10
    if-gtz v2, :cond_13

    #@12
    .line 165
    :goto_12
    return-wide v0

    #@13
    :cond_13
    iget v0, p0, Lcom/lge/lgdrm/DrmStream;->nativeHandle:I

    #@15
    invoke-direct {p0, v0, p1, p2}, Lcom/lge/lgdrm/DrmStream;->nativeSkip(IJ)J

    #@18
    move-result-wide v0

    #@19
    goto :goto_12
.end method
