.class Lcom/lge/view/TouchEventFilter$PenPalmFilter;
.super Ljava/lang/Object;
.source "TouchEventFilter.java"

# interfaces
.implements Lcom/lge/view/TouchEventFilter$IEventFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/view/TouchEventFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PenPalmFilter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/view/TouchEventFilter$PenPalmFilter$PalmRegion;,
        Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;
    }
.end annotation


# static fields
.field private static final EDGE_AREA:I = 0x1e

.field private static final MIN_PEN_COUNT:I = 0x1

.field private static final MIN_TIME_GAP:I = 0x64


# instance fields
.field private act:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

.field private edge:Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;

.field private mPalmRegion:Lcom/lge/view/TouchEventFilter$PenPalmFilter$PalmRegion;

.field private mPenIDFinder:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;",
            ">;"
        }
    .end annotation
.end field

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field private mView:Landroid/view/View;

.field private reportIdBits:I

.field private reportMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

.field private savedTopID:I

.field final synthetic this$0:Lcom/lge/view/TouchEventFilter;

.field private upTime:J

.field private usePen:Z

.field private validXLeft:I

.field private validXRight:I


# direct methods
.method public constructor <init>(Lcom/lge/view/TouchEventFilter;Landroid/view/View;)V
    .registers 6
    .parameter
    .parameter "view"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 918
    iput-object p1, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->this$0:Lcom/lge/view/TouchEventFilter;

    #@3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 790
    iput v1, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->validXLeft:I

    #@8
    .line 791
    iput v1, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->validXRight:I

    #@a
    .line 793
    const/4 v1, -0x1

    #@b
    iput v1, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->savedTopID:I

    #@d
    .line 795
    const-wide/16 v1, 0x0

    #@f
    iput-wide v1, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->upTime:J

    #@11
    .line 911
    new-instance v1, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@13
    invoke-direct {v1}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;-><init>()V

    #@16
    iput-object v1, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->reportMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@18
    .line 913
    new-instance v1, Ljava/util/ArrayList;

    #@1a
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@1d
    iput-object v1, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->mPenIDFinder:Ljava/util/ArrayList;

    #@1f
    .line 914
    new-instance v1, Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;

    #@21
    const/16 v2, 0x1e

    #@23
    invoke-direct {v1, v2}, Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;-><init>(I)V

    #@26
    iput-object v1, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->edge:Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;

    #@28
    .line 915
    new-instance v1, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PalmRegion;

    #@2a
    invoke-direct {v1, p0}, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PalmRegion;-><init>(Lcom/lge/view/TouchEventFilter$PenPalmFilter;)V

    #@2d
    iput-object v1, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->mPalmRegion:Lcom/lge/view/TouchEventFilter$PenPalmFilter$PalmRegion;

    #@2f
    .line 919
    const/4 v0, 0x0

    #@30
    .local v0, i:I
    :goto_30
    const/16 v1, 0xa

    #@32
    if-ge v0, v1, :cond_41

    #@34
    .line 920
    iget-object v1, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->mPenIDFinder:Ljava/util/ArrayList;

    #@36
    new-instance v2, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;

    #@38
    invoke-direct {v2, p0}, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;-><init>(Lcom/lge/view/TouchEventFilter$PenPalmFilter;)V

    #@3b
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3e
    .line 919
    add-int/lit8 v0, v0, 0x1

    #@40
    goto :goto_30

    #@41
    .line 923
    :cond_41
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    #@44
    move-result-object v1

    #@45
    iput-object v1, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@47
    .line 924
    iput-object p2, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->mView:Landroid/view/View;

    #@49
    .line 925
    invoke-virtual {p0}, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->init()V

    #@4c
    .line 926
    return-void
.end method

.method private checkShouldReset(IJ)Z
    .registers 9
    .parameter "act"
    .parameter "time"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1071
    if-ne p1, v2, :cond_7

    #@4
    .line 1072
    iput-wide p2, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->upTime:J

    #@6
    .line 1082
    :cond_6
    :goto_6
    return v1

    #@7
    .line 1074
    :cond_7
    if-nez p1, :cond_6

    #@9
    .line 1075
    iget-wide v3, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->upTime:J

    #@b
    sub-long/2addr v3, p2

    #@c
    long-to-int v3, v3

    #@d
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    #@10
    move-result v0

    #@11
    .line 1076
    .local v0, dt:I
    if-lez v0, :cond_17

    #@13
    const/16 v3, 0x64

    #@15
    if-lt v0, v3, :cond_6

    #@17
    :cond_17
    move v1, v2

    #@18
    .line 1079
    goto :goto_6
.end method


# virtual methods
.method public filtering(Landroid/view/MotionEvent;I)Z
    .registers 35
    .parameter "event"
    .parameter "tmpMask"

    #@0
    .prologue
    .line 943
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@3
    move-result v12

    #@4
    .line 944
    .local v12, NI:I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@7
    move-result v13

    #@8
    .line 945
    .local v13, action:I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@b
    move-result v14

    #@c
    .line 946
    .local v14, actionIndex:I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    #@f
    move-result-wide v24

    #@10
    .line 948
    .local v24, time:J
    move-object/from16 v0, p0

    #@12
    move-wide/from16 v1, v24

    #@14
    invoke-direct {v0, v13, v1, v2}, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->checkShouldReset(IJ)Z

    #@17
    move-result v29

    #@18
    if-eqz v29, :cond_2b

    #@1a
    .line 949
    const/16 v29, 0x0

    #@1c
    move/from16 v0, v29

    #@1e
    move-object/from16 v1, p0

    #@20
    iput-boolean v0, v1, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->usePen:Z

    #@22
    .line 950
    move-object/from16 v0, p0

    #@24
    iget-object v0, v0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->mPalmRegion:Lcom/lge/view/TouchEventFilter$PenPalmFilter$PalmRegion;

    #@26
    move-object/from16 v29, v0

    #@28
    invoke-virtual/range {v29 .. v29}, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PalmRegion;->init()V

    #@2b
    .line 953
    :cond_2b
    move-object/from16 v0, p0

    #@2d
    iget-object v0, v0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->reportMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@2f
    move-object/from16 v29, v0

    #@31
    invoke-virtual/range {v29 .. v29}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->clearAllMask()I

    #@34
    .line 954
    sget-object v29, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->NONE:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@36
    move-object/from16 v0, v29

    #@38
    move-object/from16 v1, p0

    #@3a
    iput-object v0, v1, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->act:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@3c
    .line 956
    move-object/from16 v0, p0

    #@3e
    iget-object v0, v0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->edge:Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;

    #@40
    move-object/from16 v29, v0

    #@42
    invoke-virtual/range {v29 .. v29}, Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;->getLeftEdgeRegion()I

    #@45
    move-result v29

    #@46
    move/from16 v0, v29

    #@48
    move-object/from16 v1, p0

    #@4a
    iput v0, v1, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->validXLeft:I

    #@4c
    .line 957
    move-object/from16 v0, p0

    #@4e
    iget-object v0, v0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->edge:Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;

    #@50
    move-object/from16 v29, v0

    #@52
    invoke-virtual/range {v29 .. v29}, Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;->getRightEdgeRegion()I

    #@55
    move-result v29

    #@56
    move/from16 v0, v29

    #@58
    move-object/from16 v1, p0

    #@5a
    iput v0, v1, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->validXRight:I

    #@5c
    .line 959
    move-object/from16 v0, p0

    #@5e
    iget-object v0, v0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@60
    move-object/from16 v29, v0

    #@62
    move-object/from16 v0, v29

    #@64
    move-object/from16 v1, p1

    #@66
    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    #@69
    .line 960
    move-object/from16 v0, p0

    #@6b
    iget-object v0, v0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@6d
    move-object/from16 v29, v0

    #@6f
    const/16 v30, 0x3e8

    #@71
    invoke-virtual/range {v29 .. v30}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    #@74
    .line 962
    const/4 v15, 0x0

    #@75
    .line 964
    .local v15, hasPen:Z
    const/16 v16, 0x0

    #@77
    .local v16, i:I
    :goto_77
    move/from16 v0, v16

    #@79
    if-ge v0, v12, :cond_190

    #@7b
    .line 965
    move-object/from16 v0, p1

    #@7d
    move/from16 v1, v16

    #@7f
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@82
    move-result v17

    #@83
    .line 966
    .local v17, id:I
    move-object/from16 v0, p1

    #@85
    move/from16 v1, v16

    #@87
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    #@8a
    move-result v29

    #@8b
    move/from16 v0, v29

    #@8d
    float-to-int v0, v0

    #@8e
    move/from16 v28, v0

    #@90
    .line 967
    .local v28, x:I
    move-object/from16 v0, p1

    #@92
    move/from16 v1, v16

    #@94
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    #@97
    move-result v8

    #@98
    .line 968
    .local v8, y:F
    move-object/from16 v0, p1

    #@9a
    move/from16 v1, v16

    #@9c
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPressure(I)F

    #@9f
    move-result v29

    #@a0
    const/high16 v30, 0x437f

    #@a2
    mul-float v9, v29, v30

    #@a4
    .line 969
    .local v9, z:F
    move-object/from16 v0, p1

    #@a6
    move/from16 v1, v16

    #@a8
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolMinor(I)F

    #@ab
    move-result v10

    #@ac
    .line 970
    .local v10, w:F
    move-object/from16 v0, p1

    #@ae
    move/from16 v1, v16

    #@b0
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    #@b3
    move-result v26

    #@b4
    .line 971
    .local v26, toolType:I
    move-object/from16 v0, p0

    #@b6
    iget-object v0, v0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@b8
    move-object/from16 v29, v0

    #@ba
    move-object/from16 v0, v29

    #@bc
    move/from16 v1, v17

    #@be
    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    #@c1
    move-result v29

    #@c2
    move/from16 v0, v29

    #@c4
    float-to-int v0, v0

    #@c5
    move/from16 v29, v0

    #@c7
    invoke-static/range {v29 .. v29}, Ljava/lang/Math;->abs(I)I

    #@ca
    move-result v6

    #@cb
    .line 972
    .local v6, velX:I
    move-object/from16 v0, p0

    #@cd
    iget-object v0, v0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@cf
    move-object/from16 v29, v0

    #@d1
    move-object/from16 v0, v29

    #@d3
    move/from16 v1, v17

    #@d5
    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    #@d8
    move-result v29

    #@d9
    move/from16 v0, v29

    #@db
    float-to-int v0, v0

    #@dc
    move/from16 v29, v0

    #@de
    invoke-static/range {v29 .. v29}, Ljava/lang/Math;->abs(I)I

    #@e1
    move-result v7

    #@e2
    .line 973
    .local v7, velY:I
    move-object/from16 v0, p0

    #@e4
    iget v0, v0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->validXLeft:I

    #@e6
    move/from16 v29, v0

    #@e8
    move/from16 v0, v28

    #@ea
    move/from16 v1, v29

    #@ec
    if-lt v0, v1, :cond_fa

    #@ee
    move-object/from16 v0, p0

    #@f0
    iget v0, v0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->validXRight:I

    #@f2
    move/from16 v29, v0

    #@f4
    move/from16 v0, v28

    #@f6
    move/from16 v1, v29

    #@f8
    if-le v0, v1, :cond_184

    #@fa
    :cond_fa
    const/4 v11, 0x1

    #@fb
    .line 974
    .local v11, isEdge:Z
    :goto_fb
    move-object/from16 v0, p0

    #@fd
    iget-object v0, v0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->mPenIDFinder:Ljava/util/ArrayList;

    #@ff
    move-object/from16 v29, v0

    #@101
    move-object/from16 v0, v29

    #@103
    move/from16 v1, v17

    #@105
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@108
    move-result-object v5

    #@109
    check-cast v5, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;

    #@10b
    .line 975
    .local v5, penFinder:Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;
    if-eqz v13, :cond_113

    #@10d
    const/16 v29, 0x5

    #@10f
    move/from16 v0, v29

    #@111
    if-ne v13, v0, :cond_187

    #@113
    :cond_113
    move-object/from16 v0, p0

    #@115
    iget-object v0, v0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->mPalmRegion:Lcom/lge/view/TouchEventFilter$PenPalmFilter$PalmRegion;

    #@117
    move-object/from16 v29, v0

    #@119
    float-to-int v0, v8

    #@11a
    move/from16 v30, v0

    #@11c
    move-object/from16 v0, v29

    #@11e
    move/from16 v1, v28

    #@120
    move/from16 v2, v30

    #@122
    invoke-virtual {v0, v1, v2}, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PalmRegion;->checkPalmRegion(II)Z

    #@125
    move-result v29

    #@126
    if-eqz v29, :cond_187

    #@128
    move/from16 v0, v16

    #@12a
    if-ne v14, v0, :cond_187

    #@12c
    const/16 v20, 0x1

    #@12e
    .line 977
    .local v20, isInPalmRegion:Z
    :goto_12e
    if-eqz v13, :cond_136

    #@130
    const/16 v29, 0x5

    #@132
    move/from16 v0, v29

    #@134
    if-ne v13, v0, :cond_18a

    #@136
    :cond_136
    if-eqz v11, :cond_18a

    #@138
    move/from16 v0, v16

    #@13a
    if-ne v14, v0, :cond_18a

    #@13c
    const/16 v19, 0x1

    #@13e
    .line 980
    .local v19, isInEdgeRegion:Z
    :goto_13e
    if-eqz v20, :cond_147

    #@140
    .line 981
    const/16 v29, 0x1

    #@142
    move/from16 v0, v29

    #@144
    invoke-virtual {v5, v0}, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->setPalmRegion(Z)V

    #@147
    .line 982
    :cond_147
    if-eqz v19, :cond_150

    #@149
    .line 983
    const/16 v29, 0x1

    #@14b
    move/from16 v0, v29

    #@14d
    invoke-virtual {v5, v0}, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->setEdgeRegion(Z)V

    #@150
    .line 985
    :cond_150
    invoke-virtual/range {v5 .. v11}, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->addInfo(IIFFFZ)V

    #@153
    .line 987
    invoke-virtual {v5}, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->isPen()Z

    #@156
    move-result v29

    #@157
    if-eqz v29, :cond_15a

    #@159
    .line 988
    const/4 v15, 0x1

    #@15a
    .line 989
    :cond_15a
    invoke-virtual {v5}, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->isFinger()Z

    #@15d
    move-result v29

    #@15e
    if-eqz v29, :cond_180

    #@160
    .line 990
    move-object/from16 v0, p0

    #@162
    iget-object v0, v0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->mPalmRegion:Lcom/lge/view/TouchEventFilter$PenPalmFilter$PalmRegion;

    #@164
    move-object/from16 v30, v0

    #@166
    float-to-int v0, v8

    #@167
    move/from16 v31, v0

    #@169
    const/16 v29, 0x5

    #@16b
    move/from16 v0, v26

    #@16d
    move/from16 v1, v29

    #@16f
    if-ne v0, v1, :cond_18d

    #@171
    const/16 v29, 0x1

    #@173
    :goto_173
    move-object/from16 v0, v30

    #@175
    move/from16 v1, v17

    #@177
    move/from16 v2, v28

    #@179
    move/from16 v3, v31

    #@17b
    move/from16 v4, v29

    #@17d
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PalmRegion;->assignPalm(IIIZ)V

    #@180
    .line 964
    :cond_180
    add-int/lit8 v16, v16, 0x1

    #@182
    goto/16 :goto_77

    #@184
    .line 973
    .end local v5           #penFinder:Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;
    .end local v11           #isEdge:Z
    .end local v19           #isInEdgeRegion:Z
    .end local v20           #isInPalmRegion:Z
    :cond_184
    const/4 v11, 0x0

    #@185
    goto/16 :goto_fb

    #@187
    .line 975
    .restart local v5       #penFinder:Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;
    .restart local v11       #isEdge:Z
    :cond_187
    const/16 v20, 0x0

    #@189
    goto :goto_12e

    #@18a
    .line 977
    .restart local v20       #isInPalmRegion:Z
    :cond_18a
    const/16 v19, 0x0

    #@18c
    goto :goto_13e

    #@18d
    .line 990
    .restart local v19       #isInEdgeRegion:Z
    :cond_18d
    const/16 v29, 0x0

    #@18f
    goto :goto_173

    #@190
    .line 993
    .end local v5           #penFinder:Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;
    .end local v6           #velX:I
    .end local v7           #velY:I
    .end local v8           #y:F
    .end local v9           #z:F
    .end local v10           #w:F
    .end local v11           #isEdge:Z
    .end local v17           #id:I
    .end local v19           #isInEdgeRegion:Z
    .end local v20           #isInPalmRegion:Z
    .end local v26           #toolType:I
    .end local v28           #x:I
    :cond_190
    const/high16 v22, -0x4080

    #@192
    .line 994
    .local v22, result:F
    const/16 v27, -0x1

    #@194
    .line 995
    .local v27, topID:I
    if-eqz v15, :cond_2a1

    #@196
    .line 996
    const/16 v16, 0x0

    #@198
    :goto_198
    move/from16 v0, v16

    #@19a
    if-ge v0, v12, :cond_1db

    #@19c
    .line 997
    move-object/from16 v0, p1

    #@19e
    move/from16 v1, v16

    #@1a0
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@1a3
    move-result v17

    #@1a4
    .line 998
    .restart local v17       #id:I
    move-object/from16 v0, p1

    #@1a6
    move/from16 v1, v16

    #@1a8
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    #@1ab
    move-result v29

    #@1ac
    move/from16 v0, v29

    #@1ae
    float-to-int v0, v0

    #@1af
    move/from16 v28, v0

    #@1b1
    .line 999
    .restart local v28       #x:I
    move-object/from16 v0, p1

    #@1b3
    move/from16 v1, v16

    #@1b5
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    #@1b8
    move-result v29

    #@1b9
    move/from16 v0, v29

    #@1bb
    float-to-int v8, v0

    #@1bc
    .line 1000
    .local v8, y:I
    move-object/from16 v0, p0

    #@1be
    iget-object v0, v0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->mPenIDFinder:Ljava/util/ArrayList;

    #@1c0
    move-object/from16 v29, v0

    #@1c2
    move-object/from16 v0, v29

    #@1c4
    move/from16 v1, v17

    #@1c6
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1c9
    move-result-object v5

    #@1ca
    check-cast v5, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;

    #@1cc
    .line 1001
    .restart local v5       #penFinder:Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;
    invoke-virtual {v5}, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->getResult()F

    #@1cf
    move-result v21

    #@1d0
    .line 1003
    .local v21, newResult:F
    cmpl-float v29, v21, v22

    #@1d2
    if-lez v29, :cond_1d8

    #@1d4
    .line 1004
    move/from16 v22, v21

    #@1d6
    .line 1005
    move/from16 v27, v17

    #@1d8
    .line 996
    :cond_1d8
    add-int/lit8 v16, v16, 0x1

    #@1da
    goto :goto_198

    #@1db
    .line 1009
    .end local v5           #penFinder:Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;
    .end local v8           #y:I
    .end local v17           #id:I
    .end local v21           #newResult:F
    .end local v28           #x:I
    :cond_1db
    const/16 v29, -0x1

    #@1dd
    move/from16 v0, v27

    #@1df
    move/from16 v1, v29

    #@1e1
    if-eq v0, v1, :cond_224

    #@1e3
    .line 1010
    move-object/from16 v0, p0

    #@1e5
    iget-object v0, v0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->reportMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@1e7
    move-object/from16 v29, v0

    #@1e9
    move-object/from16 v0, v29

    #@1eb
    move/from16 v1, v27

    #@1ed
    invoke-virtual {v0, v1}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->setMask(I)I

    #@1f0
    .line 1011
    move-object/from16 v0, p0

    #@1f2
    iget v0, v0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->savedTopID:I

    #@1f4
    move/from16 v29, v0

    #@1f6
    const/16 v30, -0x1

    #@1f8
    move/from16 v0, v29

    #@1fa
    move/from16 v1, v30

    #@1fc
    if-ne v0, v1, :cond_289

    #@1fe
    const/16 v23, 0x0

    #@200
    .line 1012
    .local v23, savedTopIDIsPen:Z
    :goto_200
    move-object/from16 v0, p0

    #@202
    iget v0, v0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->savedTopID:I

    #@204
    move/from16 v29, v0

    #@206
    const/16 v30, -0x1

    #@208
    move/from16 v0, v29

    #@20a
    move/from16 v1, v30

    #@20c
    if-eq v0, v1, :cond_224

    #@20e
    move-object/from16 v0, p0

    #@210
    iget v0, v0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->savedTopID:I

    #@212
    move/from16 v29, v0

    #@214
    move/from16 v0, v27

    #@216
    move/from16 v1, v29

    #@218
    if-eq v0, v1, :cond_224

    #@21a
    if-nez v23, :cond_224

    #@21c
    .line 1013
    sget-object v29, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->CANCEL_AND_REPEAT:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@21e
    move-object/from16 v0, v29

    #@220
    move-object/from16 v1, p0

    #@222
    iput-object v0, v1, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->act:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@224
    .line 1032
    .end local v23           #savedTopIDIsPen:Z
    :cond_224
    :goto_224
    move/from16 v0, v27

    #@226
    move-object/from16 v1, p0

    #@228
    iput v0, v1, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->savedTopID:I

    #@22a
    .line 1034
    const/16 v29, 0x6

    #@22c
    move/from16 v0, v29

    #@22e
    if-eq v13, v0, :cond_236

    #@230
    const/16 v29, 0x1

    #@232
    move/from16 v0, v29

    #@234
    if-ne v13, v0, :cond_276

    #@236
    .line 1035
    :cond_236
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@239
    move-result v18

    #@23a
    .line 1036
    .local v18, index:I
    move-object/from16 v0, p1

    #@23c
    move/from16 v1, v18

    #@23e
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@241
    move-result v17

    #@242
    .line 1037
    .restart local v17       #id:I
    move-object/from16 v0, p0

    #@244
    iget-object v0, v0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->mPenIDFinder:Ljava/util/ArrayList;

    #@246
    move-object/from16 v29, v0

    #@248
    move-object/from16 v0, v29

    #@24a
    move/from16 v1, v17

    #@24c
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@24f
    move-result-object v5

    #@250
    check-cast v5, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;

    #@252
    .line 1039
    .restart local v5       #penFinder:Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;
    const/16 v29, 0x1

    #@254
    move/from16 v0, v29

    #@256
    if-eq v13, v0, :cond_266

    #@258
    invoke-virtual {v5}, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->isPen()Z

    #@25b
    move-result v29

    #@25c
    if-eqz v29, :cond_266

    #@25e
    .line 1040
    const/16 v29, 0x1

    #@260
    move/from16 v0, v29

    #@262
    move-object/from16 v1, p0

    #@264
    iput-boolean v0, v1, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->usePen:Z

    #@266
    .line 1042
    :cond_266
    move-object/from16 v0, p0

    #@268
    iget-object v0, v0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->reportMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@26a
    move-object/from16 v29, v0

    #@26c
    move-object/from16 v0, v29

    #@26e
    move/from16 v1, v17

    #@270
    invoke-virtual {v0, v1}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->clearMask(I)I

    #@273
    .line 1043
    invoke-virtual {v5}, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->init()V

    #@276
    .line 1046
    .end local v5           #penFinder:Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;
    .end local v17           #id:I
    .end local v18           #index:I
    :cond_276
    move-object/from16 v0, p0

    #@278
    iget-object v0, v0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->reportMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@27a
    move-object/from16 v29, v0

    #@27c
    invoke-virtual/range {v29 .. v29}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->getMask()I

    #@27f
    move-result v29

    #@280
    move/from16 v0, v29

    #@282
    move-object/from16 v1, p0

    #@284
    iput v0, v1, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->reportIdBits:I

    #@286
    .line 1047
    const/16 v29, 0x1

    #@288
    return v29

    #@289
    .line 1011
    :cond_289
    move-object/from16 v0, p0

    #@28b
    iget-object v0, v0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->mPenIDFinder:Ljava/util/ArrayList;

    #@28d
    move-object/from16 v29, v0

    #@28f
    move-object/from16 v0, p0

    #@291
    iget v0, v0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->savedTopID:I

    #@293
    move/from16 v30, v0

    #@295
    invoke-virtual/range {v29 .. v30}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@298
    move-result-object v29

    #@299
    check-cast v29, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;

    #@29b
    invoke-virtual/range {v29 .. v29}, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->isPen()Z

    #@29e
    move-result v23

    #@29f
    goto/16 :goto_200

    #@2a1
    .line 1017
    :cond_2a1
    const/16 v16, 0x0

    #@2a3
    :goto_2a3
    move/from16 v0, v16

    #@2a5
    if-ge v0, v12, :cond_2d6

    #@2a7
    .line 1018
    move-object/from16 v0, p1

    #@2a9
    move/from16 v1, v16

    #@2ab
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@2ae
    move-result v17

    #@2af
    .line 1019
    .restart local v17       #id:I
    move-object/from16 v0, p0

    #@2b1
    iget-object v0, v0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->mPenIDFinder:Ljava/util/ArrayList;

    #@2b3
    move-object/from16 v29, v0

    #@2b5
    move-object/from16 v0, v29

    #@2b7
    move/from16 v1, v17

    #@2b9
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2bc
    move-result-object v5

    #@2bd
    check-cast v5, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;

    #@2bf
    .line 1020
    .restart local v5       #penFinder:Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;
    invoke-virtual {v5}, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->getCount()I

    #@2c2
    move-result v21

    #@2c3
    .line 1022
    .local v21, newResult:I
    move/from16 v0, v21

    #@2c5
    int-to-float v0, v0

    #@2c6
    move/from16 v29, v0

    #@2c8
    cmpl-float v29, v29, v22

    #@2ca
    if-lez v29, :cond_2d3

    #@2cc
    .line 1023
    move/from16 v0, v21

    #@2ce
    int-to-float v0, v0

    #@2cf
    move/from16 v22, v0

    #@2d1
    .line 1024
    move/from16 v27, v17

    #@2d3
    .line 1017
    :cond_2d3
    add-int/lit8 v16, v16, 0x1

    #@2d5
    goto :goto_2a3

    #@2d6
    .line 1028
    .end local v5           #penFinder:Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;
    .end local v17           #id:I
    .end local v21           #newResult:I
    :cond_2d6
    move-object/from16 v0, p0

    #@2d8
    iget-boolean v0, v0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->usePen:Z

    #@2da
    move/from16 v29, v0

    #@2dc
    if-nez v29, :cond_224

    #@2de
    const/16 v29, -0x1

    #@2e0
    move/from16 v0, v27

    #@2e2
    move/from16 v1, v29

    #@2e4
    if-eq v0, v1, :cond_224

    #@2e6
    .line 1029
    move-object/from16 v0, p0

    #@2e8
    iget-object v0, v0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->reportMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@2ea
    move-object/from16 v29, v0

    #@2ec
    move-object/from16 v0, v29

    #@2ee
    move/from16 v1, v27

    #@2f0
    invoke-virtual {v0, v1}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->setMask(I)I

    #@2f3
    goto/16 :goto_224
.end method

.method public getAct()Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;
    .registers 2

    #@0
    .prologue
    .line 1051
    iget-object v0, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->act:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@2
    return-object v0
.end method

.method public getReportMask()I
    .registers 2

    #@0
    .prologue
    .line 1055
    iget v0, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->reportIdBits:I

    #@2
    return v0
.end method

.method public init()V
    .registers 4

    #@0
    .prologue
    .line 929
    sget-object v2, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->NONE:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@2
    iput-object v2, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->act:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@4
    .line 930
    const/4 v2, 0x0

    #@5
    iput v2, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->reportIdBits:I

    #@7
    .line 931
    const/4 v2, -0x1

    #@8
    iput v2, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->savedTopID:I

    #@a
    .line 933
    iget-object v2, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->reportMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@c
    invoke-virtual {v2}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->clearAllMask()I

    #@f
    .line 934
    iget-object v2, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@11
    invoke-virtual {v2}, Landroid/view/VelocityTracker;->clear()V

    #@14
    .line 936
    const/4 v0, 0x0

    #@15
    .local v0, i:I
    :goto_15
    const/16 v2, 0xa

    #@17
    if-ge v0, v2, :cond_27

    #@19
    .line 937
    iget-object v2, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->mPenIDFinder:Ljava/util/ArrayList;

    #@1b
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1e
    move-result-object v1

    #@1f
    check-cast v1, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;

    #@21
    .line 938
    .local v1, penFinder:Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;
    invoke-virtual {v1}, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->init()V

    #@24
    .line 936
    add-int/lit8 v0, v0, 0x1

    #@26
    goto :goto_15

    #@27
    .line 940
    .end local v1           #penFinder:Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;
    :cond_27
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 1059
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 1060
    .local v1, msg:Ljava/lang/StringBuilder;
    new-instance v3, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v4, "PenPalmFilter usePen["

    #@c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v3

    #@10
    iget-boolean v4, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->usePen:Z

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    const-string v4, "] valid"

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    iget-object v4, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->edge:Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    .line 1061
    const/4 v0, 0x0

    #@2a
    .local v0, i:I
    :goto_2a
    const/16 v3, 0xa

    #@2c
    if-ge v0, v3, :cond_5f

    #@2e
    .line 1062
    iget-object v3, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->mPenIDFinder:Ljava/util/ArrayList;

    #@30
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@33
    move-result-object v2

    #@34
    check-cast v2, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;

    #@36
    .line 1063
    .local v2, penFinder:Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;
    invoke-virtual {v2}, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->getCount()I

    #@39
    move-result v3

    #@3a
    if-lez v3, :cond_5c

    #@3c
    .line 1064
    new-instance v3, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v4, " ["

    #@43
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v3

    #@47
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v3

    #@4b
    const-string v4, "] "

    #@4d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v3

    #@51
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v3

    #@55
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v3

    #@59
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    .line 1061
    :cond_5c
    add-int/lit8 v0, v0, 0x1

    #@5e
    goto :goto_2a

    #@5f
    .line 1066
    .end local v2           #penFinder:Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;
    :cond_5f
    new-instance v3, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v4, "  Rect - "

    #@66
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v3

    #@6a
    iget-object v4, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter;->mPalmRegion:Lcom/lge/view/TouchEventFilter$PenPalmFilter$PalmRegion;

    #@6c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v3

    #@70
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v3

    #@74
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    .line 1067
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v3

    #@7b
    return-object v3
.end method
