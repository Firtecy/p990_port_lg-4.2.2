.class Lcom/lge/view/TouchEventFilter$PenPalmFilter$PalmRegion;
.super Ljava/lang/Object;
.source "TouchEventFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/view/TouchEventFilter$PenPalmFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PalmRegion"
.end annotation


# static fields
.field private static final LARGE_PALM_AREA_SIZE:I = 0x190

.field private static final PALM_AREA_SIZE:I = 0xc8


# instance fields
.field private rectList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$1:Lcom/lge/view/TouchEventFilter$PenPalmFilter;


# direct methods
.method public constructor <init>(Lcom/lge/view/TouchEventFilter$PenPalmFilter;)V
    .registers 5
    .parameter

    #@0
    .prologue
    .line 868
    iput-object p1, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PalmRegion;->this$1:Lcom/lge/view/TouchEventFilter$PenPalmFilter;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 866
    new-instance v1, Ljava/util/ArrayList;

    #@7
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@a
    iput-object v1, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PalmRegion;->rectList:Ljava/util/ArrayList;

    #@c
    .line 869
    const/4 v0, 0x0

    #@d
    .local v0, i:I
    :goto_d
    const/16 v1, 0xa

    #@f
    if-ge v0, v1, :cond_1e

    #@11
    .line 870
    iget-object v1, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PalmRegion;->rectList:Ljava/util/ArrayList;

    #@13
    new-instance v2, Landroid/graphics/Rect;

    #@15
    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    #@18
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1b
    .line 869
    add-int/lit8 v0, v0, 0x1

    #@1d
    goto :goto_d

    #@1e
    .line 872
    :cond_1e
    invoke-virtual {p0}, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PalmRegion;->init()V

    #@21
    .line 873
    return-void
.end method


# virtual methods
.method public assignPalm(IIIZ)V
    .registers 11
    .parameter "id"
    .parameter "x"
    .parameter "y"
    .parameter "largePalm"

    #@0
    .prologue
    .line 883
    if-eqz p4, :cond_18

    #@2
    const/16 v1, 0x190

    #@4
    .line 884
    .local v1, rectSize:I
    :goto_4
    iget-object v2, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PalmRegion;->rectList:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/graphics/Rect;

    #@c
    .line 885
    .local v0, mRect:Landroid/graphics/Rect;
    sub-int v2, p2, v1

    #@e
    sub-int v3, p3, v1

    #@10
    add-int v4, p2, v1

    #@12
    add-int v5, p3, v1

    #@14
    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    #@17
    .line 886
    return-void

    #@18
    .line 883
    .end local v0           #mRect:Landroid/graphics/Rect;
    .end local v1           #rectSize:I
    :cond_18
    const/16 v1, 0xc8

    #@1a
    goto :goto_4
.end method

.method public checkPalmRegion(II)Z
    .registers 6
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 889
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v2, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PalmRegion;->rectList:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v2

    #@7
    if-ge v0, v2, :cond_1c

    #@9
    .line 890
    iget-object v2, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PalmRegion;->rectList:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Landroid/graphics/Rect;

    #@11
    .line 891
    .local v1, mRect:Landroid/graphics/Rect;
    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_19

    #@17
    .line 892
    const/4 v2, 0x1

    #@18
    .line 895
    .end local v1           #mRect:Landroid/graphics/Rect;
    :goto_18
    return v2

    #@19
    .line 889
    .restart local v1       #mRect:Landroid/graphics/Rect;
    :cond_19
    add-int/lit8 v0, v0, 0x1

    #@1b
    goto :goto_1

    #@1c
    .line 895
    .end local v1           #mRect:Landroid/graphics/Rect;
    :cond_1c
    const/4 v2, 0x0

    #@1d
    goto :goto_18
.end method

.method public init()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 876
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :goto_2
    iget-object v2, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PalmRegion;->rectList:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v2

    #@8
    if-ge v0, v2, :cond_18

    #@a
    .line 877
    iget-object v2, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PalmRegion;->rectList:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Landroid/graphics/Rect;

    #@12
    .line 878
    .local v1, mRect:Landroid/graphics/Rect;
    invoke-virtual {v1, v3, v3, v3, v3}, Landroid/graphics/Rect;->set(IIII)V

    #@15
    .line 876
    add-int/lit8 v0, v0, 0x1

    #@17
    goto :goto_2

    #@18
    .line 880
    .end local v1           #mRect:Landroid/graphics/Rect;
    :cond_18
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 899
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 901
    .local v2, msg:Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    #@6
    .local v0, i:I
    :goto_6
    iget-object v3, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PalmRegion;->rectList:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@b
    move-result v3

    #@c
    if-ge v0, v3, :cond_39

    #@e
    .line 902
    iget-object v3, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PalmRegion;->rectList:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Landroid/graphics/Rect;

    #@16
    .line 903
    .local v1, mRect:Landroid/graphics/Rect;
    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    #@19
    move-result v3

    #@1a
    if-nez v3, :cond_36

    #@1c
    .line 904
    new-instance v3, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    invoke-virtual {v1}, Landroid/graphics/Rect;->toShortString()Ljava/lang/String;

    #@24
    move-result-object v4

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    const-string v4, " "

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    .line 901
    :cond_36
    add-int/lit8 v0, v0, 0x1

    #@38
    goto :goto_6

    #@39
    .line 907
    .end local v1           #mRect:Landroid/graphics/Rect;
    :cond_39
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v3

    #@3d
    return-object v3
.end method
