.class public final enum Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;
.super Ljava/lang/Enum;
.source "TouchEventFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/view/TouchEventFilter$IEventFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ReturnAct"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

.field public static final enum CANCEL:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

.field public static final enum CANCEL_AND_REPEAT:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

.field public static final enum IGNORE:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

.field public static final enum NONE:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .registers 6

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/4 v4, 0x2

    #@2
    const/4 v3, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    .line 433
    new-instance v0, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@6
    const-string v1, "NONE"

    #@8
    invoke-direct {v0, v1, v2, v2}, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;-><init>(Ljava/lang/String;II)V

    #@b
    sput-object v0, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->NONE:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@d
    new-instance v0, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@f
    const-string v1, "IGNORE"

    #@11
    invoke-direct {v0, v1, v3, v3}, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;-><init>(Ljava/lang/String;II)V

    #@14
    sput-object v0, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->IGNORE:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@16
    new-instance v0, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@18
    const-string v1, "CANCEL"

    #@1a
    invoke-direct {v0, v1, v4, v4}, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;-><init>(Ljava/lang/String;II)V

    #@1d
    sput-object v0, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->CANCEL:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@1f
    new-instance v0, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@21
    const-string v1, "CANCEL_AND_REPEAT"

    #@23
    invoke-direct {v0, v1, v5, v5}, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;-><init>(Ljava/lang/String;II)V

    #@26
    sput-object v0, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->CANCEL_AND_REPEAT:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@28
    .line 432
    const/4 v0, 0x4

    #@29
    new-array v0, v0, [Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@2b
    sget-object v1, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->NONE:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@2d
    aput-object v1, v0, v2

    #@2f
    sget-object v1, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->IGNORE:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@31
    aput-object v1, v0, v3

    #@33
    sget-object v1, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->CANCEL:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@35
    aput-object v1, v0, v4

    #@37
    sget-object v1, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->CANCEL_AND_REPEAT:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@39
    aput-object v1, v0, v5

    #@3b
    sput-object v0, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->$VALUES:[Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@3d
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter "val"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 436
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    iput p3, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->value:I

    #@5
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 432
    const-class v0, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;
    .registers 1

    #@0
    .prologue
    .line 432
    sget-object v0, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->$VALUES:[Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@2
    invoke-virtual {v0}, [Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@8
    return-object v0
.end method


# virtual methods
.method public getVal()I
    .registers 2

    #@0
    .prologue
    .line 437
    iget v0, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->value:I

    #@2
    return v0
.end method
