.class public Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;
.super Ljava/lang/Object;
.source "TouchEventFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/view/TouchEventFilter$IEventFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FindEdgeRegion"
.end annotation


# instance fields
.field private edgeSize:I

.field private validX:I

.field private validY:I


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 455
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 456
    const/4 v0, 0x0

    #@4
    invoke-virtual {p0, v0}, Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;->setEdgeSize(I)V

    #@7
    .line 457
    return-void
.end method

.method public constructor <init>(I)V
    .registers 2
    .parameter "size"

    #@0
    .prologue
    .line 459
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 460
    invoke-virtual {p0, p1}, Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;->setEdgeSize(I)V

    #@6
    .line 461
    return-void
.end method


# virtual methods
.method public getLeftEdgeRegion()I
    .registers 2

    #@0
    .prologue
    .line 470
    iget v0, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;->edgeSize:I

    #@2
    return v0
.end method

.method public getRightEdgeRegion()I
    .registers 2

    #@0
    .prologue
    .line 474
    invoke-static {}, Lcom/lge/view/TouchEventFilter;->access$000()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_9

    #@6
    .line 475
    iget v0, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;->validX:I

    #@8
    .line 477
    :goto_8
    return v0

    #@9
    :cond_9
    iget v0, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;->validY:I

    #@b
    goto :goto_8
.end method

.method public setEdgeSize(I)V
    .registers 3
    .parameter "edge"

    #@0
    .prologue
    .line 464
    add-int/lit8 v0, p1, 0x1

    #@2
    iput v0, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;->edgeSize:I

    #@4
    .line 465
    iget v0, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;->edgeSize:I

    #@6
    rsub-int v0, v0, 0x438

    #@8
    iput v0, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;->validX:I

    #@a
    .line 466
    iget v0, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;->edgeSize:I

    #@c
    rsub-int v0, v0, 0x780

    #@e
    iput v0, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;->validY:I

    #@10
    .line 467
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 481
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "["

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p0}, Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;->getLeftEdgeRegion()I

    #@e
    move-result v1

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    const-string v1, ":"

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {p0}, Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;->getRightEdgeRegion()I

    #@1c
    move-result v1

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    const-string v1, "]"

    #@23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v0

    #@27
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v0

    #@2b
    return-object v0
.end method
