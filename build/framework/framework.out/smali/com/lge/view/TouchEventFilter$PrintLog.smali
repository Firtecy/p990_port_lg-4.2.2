.class Lcom/lge/view/TouchEventFilter$PrintLog;
.super Ljava/lang/Object;
.source "TouchEventFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/view/TouchEventFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PrintLog"
.end annotation


# static fields
.field private static final DEBUG_HIGH:I = 0x2

.field private static final DEBUG_LOG:Ljava/lang/String; = "TouchEventFilter"

.field private static final DEBUG_LOW:I = 0x1

.field private static final DEBUG_NONE:I = 0x0

.field private static final DEBUG_TIME:I = -0x1


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 1162
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method private static actionLog(I)Ljava/lang/String;
    .registers 4
    .parameter "action"

    #@0
    .prologue
    .line 1202
    packed-switch p0, :pswitch_data_54

    #@3
    .line 1212
    const v1, 0xff00

    #@6
    and-int/2addr v1, p0

    #@7
    shr-int/lit8 v0, v1, 0x8

    #@9
    .line 1213
    .local v0, index:I
    and-int/lit16 v1, p0, 0xff

    #@b
    packed-switch v1, :pswitch_data_60

    #@e
    .line 1219
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@11
    move-result-object v1

    #@12
    .end local v0           #index:I
    :goto_12
    return-object v1

    #@13
    .line 1204
    :pswitch_13
    const-string v1, "ACTION_DOWN"

    #@15
    goto :goto_12

    #@16
    .line 1206
    :pswitch_16
    const-string v1, "ACTION_UP"

    #@18
    goto :goto_12

    #@19
    .line 1208
    :pswitch_19
    const-string v1, "ACTION_CANCEL"

    #@1b
    goto :goto_12

    #@1c
    .line 1210
    :pswitch_1c
    const-string v1, "ACTION_MOVE"

    #@1e
    goto :goto_12

    #@1f
    .line 1215
    .restart local v0       #index:I
    :pswitch_1f
    new-instance v1, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v2, "ACTION_POINTER_DOWN("

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    const-string v2, ")"

    #@30
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v1

    #@38
    goto :goto_12

    #@39
    .line 1217
    :pswitch_39
    new-instance v1, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v2, "ACTION_POINTER_UP("

    #@40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@47
    move-result-object v1

    #@48
    const-string v2, ")"

    #@4a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v1

    #@4e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v1

    #@52
    goto :goto_12

    #@53
    .line 1202
    nop

    #@54
    :pswitch_data_54
    .packed-switch 0x0
        :pswitch_13
        :pswitch_16
        :pswitch_1c
        :pswitch_19
    .end packed-switch

    #@60
    .line 1213
    :pswitch_data_60
    .packed-switch 0x5
        :pswitch_1f
        :pswitch_39
    .end packed-switch
.end method

.method public static checkTime(IJJ)V
    .registers 9
    .parameter "DEBUG_LEVEL"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 1197
    const/4 v0, -0x1

    #@1
    if-ne p0, v0, :cond_37

    #@3
    .line 1198
    const-string v0, "TouchEventFilter"

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "TotalTime["

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    sub-long v2, p3, p1

    #@12
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    const-string v2, "] start["

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    const-string v2, "] end["

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    const-string v2, "]"

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 1199
    :cond_37
    return-void
.end method

.method public static event(ILjava/lang/String;Landroid/view/MotionEvent;Z)V
    .registers 7
    .parameter "DEBUG_LEVEL"
    .parameter "prefix"
    .parameter "event"
    .parameter "ignore"

    #@0
    .prologue
    .line 1165
    const/4 v0, 0x1

    #@1
    if-lt p0, v0, :cond_1f

    #@3
    .line 1166
    if-nez p3, :cond_7

    #@5
    if-nez p2, :cond_20

    #@7
    .line 1167
    :cond_7
    const-string v0, "TouchEventFilter"

    #@9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    const-string v2, "IGNORE - event will not be sent to APP"

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 1171
    :cond_1f
    :goto_1f
    return-void

    #@20
    .line 1169
    :cond_20
    const-string v0, "TouchEventFilter"

    #@22
    new-instance v1, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-static {p2}, Lcom/lge/view/TouchEventFilter$PrintLog;->eventLog(Landroid/view/MotionEvent;)Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v1

    #@37
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    goto :goto_1f
.end method

.method private static eventLog(Landroid/view/MotionEvent;)Ljava/lang/String;
    .registers 7
    .parameter "event"

    #@0
    .prologue
    .line 1224
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 1225
    .local v1, msg:Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getPointerCount()I

    #@8
    move-result v2

    #@9
    .line 1227
    .local v2, pointerCount:I
    const-string v3, "action["

    #@b
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v3

    #@f
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getAction()I

    #@12
    move-result v4

    #@13
    invoke-static {v4}, Lcom/lge/view/TouchEventFilter$PrintLog;->actionLog(I)Ljava/lang/String;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    const-string v4, "]"

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    .line 1228
    const-string v3, " count["

    #@22
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v3

    #@2a
    const-string v4, "]"

    #@2c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    .line 1229
    const-string v3, " time["

    #@31
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getEventTime()J

    #@38
    move-result-wide v4

    #@39
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    const-string v4, "]"

    #@3f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    .line 1231
    const/4 v0, 0x0

    #@43
    .local v0, i:I
    :goto_43
    if-ge v0, v2, :cond_106

    #@45
    .line 1232
    const-string v3, " ("

    #@47
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v3

    #@4b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v3

    #@4f
    const-string v4, ")"

    #@51
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    .line 1233
    const-string v3, " id["

    #@56
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v3

    #@5a
    invoke-virtual {p0, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@5d
    move-result v4

    #@5e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@61
    move-result-object v3

    #@62
    const-string v4, "]"

    #@64
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    .line 1234
    const-string v3, " raw_x["

    #@69
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v3

    #@6d
    invoke-virtual {p0, v0}, Landroid/view/MotionEvent;->getRawX(I)F

    #@70
    move-result v4

    #@71
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@74
    move-result-object v3

    #@75
    const-string v4, "]"

    #@77
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    .line 1235
    const-string v3, " raw_y["

    #@7c
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v3

    #@80
    invoke-virtual {p0, v0}, Landroid/view/MotionEvent;->getRawY(I)F

    #@83
    move-result v4

    #@84
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@87
    move-result-object v3

    #@88
    const-string v4, "]"

    #@8a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    .line 1236
    const-string v3, " x["

    #@8f
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v3

    #@93
    invoke-virtual {p0, v0}, Landroid/view/MotionEvent;->getX(I)F

    #@96
    move-result v4

    #@97
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v3

    #@9b
    const-string v4, "]"

    #@9d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    .line 1237
    const-string v3, " y["

    #@a2
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v3

    #@a6
    invoke-virtual {p0, v0}, Landroid/view/MotionEvent;->getY(I)F

    #@a9
    move-result v4

    #@aa
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v3

    #@ae
    const-string v4, "]"

    #@b0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    .line 1238
    const-string v3, " z["

    #@b5
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v3

    #@b9
    invoke-virtual {p0, v0}, Landroid/view/MotionEvent;->getPressure(I)F

    #@bc
    move-result v4

    #@bd
    const/high16 v5, 0x437f

    #@bf
    mul-float/2addr v4, v5

    #@c0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v3

    #@c4
    const-string v4, "]"

    #@c6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    .line 1239
    const-string v3, " wM["

    #@cb
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v3

    #@cf
    invoke-virtual {p0, v0}, Landroid/view/MotionEvent;->getToolMajor(I)F

    #@d2
    move-result v4

    #@d3
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v3

    #@d7
    const-string v4, "]"

    #@d9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc
    .line 1240
    const-string v3, " wm["

    #@de
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v3

    #@e2
    invoke-virtual {p0, v0}, Landroid/view/MotionEvent;->getToolMinor(I)F

    #@e5
    move-result v4

    #@e6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v3

    #@ea
    const-string v4, "]"

    #@ec
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ef
    .line 1241
    const-string v3, " type["

    #@f1
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v3

    #@f5
    invoke-virtual {p0, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    #@f8
    move-result v4

    #@f9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v3

    #@fd
    const-string v4, "]"

    #@ff
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@102
    .line 1231
    add-int/lit8 v0, v0, 0x1

    #@104
    goto/16 :goto_43

    #@106
    .line 1243
    :cond_106
    const-string v3, " source["

    #@108
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10b
    move-result-object v3

    #@10c
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getSource()I

    #@10f
    move-result v4

    #@110
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@113
    move-result-object v3

    #@114
    const-string v4, "]"

    #@116
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@119
    .line 1246
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11c
    move-result-object v3

    #@11d
    return-object v3
.end method

.method private static filterLog(I)Ljava/lang/String;
    .registers 3
    .parameter "useFilter"

    #@0
    .prologue
    .line 1250
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 1252
    .local v0, msg:Ljava/lang/StringBuilder;
    and-int/lit8 v1, p0, 0x1

    #@7
    if-eqz v1, :cond_e

    #@9
    .line 1253
    const-string v1, " PALM_REJECTION_FILTER &"

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    .line 1254
    :cond_e
    and-int/lit8 v1, p0, 0x2

    #@10
    if-eqz v1, :cond_17

    #@12
    .line 1255
    const-string v1, " GRIP_SUPPRESSION_FILTER &"

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    .line 1256
    :cond_17
    and-int/lit8 v1, p0, 0x4

    #@19
    if-eqz v1, :cond_20

    #@1b
    .line 1257
    const-string v1, " SENSITIVITY_FILTER &"

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    .line 1258
    :cond_20
    and-int/lit8 v1, p0, 0x8

    #@22
    if-eqz v1, :cond_29

    #@24
    .line 1259
    const-string v1, " PEN_PALM_FILTER"

    #@26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    .line 1260
    :cond_29
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    return-object v1
.end method

.method public static filterStatus(ILandroid/view/View;I)V
    .registers 6
    .parameter "DEBUG_LEVEL"
    .parameter "view"
    .parameter "useFilter"

    #@0
    .prologue
    .line 1192
    const/4 v0, 0x1

    #@1
    if-lt p0, v0, :cond_37

    #@3
    .line 1193
    const-string v0, "TouchEventFilter"

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "USE_FILTER["

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    const-string v2, ": "

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-static {p2}, Lcom/lge/view/TouchEventFilter$PrintLog;->filterLog(I)Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    const-string v2, "]"

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 1194
    :cond_37
    return-void
.end method

.method public static filtering(ILcom/lge/view/TouchEventFilter$IEventFilter;IILcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;)V
    .registers 8
    .parameter "DEBUG_LEVEL"
    .parameter "eventFilter"
    .parameter "newMask"
    .parameter "originalMask"
    .parameter "act"

    #@0
    .prologue
    .line 1187
    const/4 v0, 0x2

    #@1
    if-lt p0, v0, :cond_3f

    #@3
    .line 1188
    const-string v0, "TouchEventFilter"

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "<"

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    const-string v2, "> - reportMask["

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    const-string v2, "/"

    #@20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    const-string v2, "] actMask["

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    const-string v2, "]"

    #@34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v1

    #@3c
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 1189
    :cond_3f
    return-void
.end method

.method public static properties(IZLcom/lge/view/TouchEventFilter$DoAction;IZZI)V
    .registers 10
    .parameter "DEBUG_LEVEL"
    .parameter "repeat"
    .parameter "result"
    .parameter "loopCount"
    .parameter "isPortrait"
    .parameter "isPointerIDChanged"
    .parameter "savedPointerChangedID"

    #@0
    .prologue
    .line 1180
    const/4 v0, 0x2

    #@1
    if-lt p0, v0, :cond_5f

    #@3
    .line 1181
    const-string v0, "TouchEventFilter"

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "<properties> needToSendAddEvent["

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    const-string v2, "]"

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    const-string v2, " filteringResult["

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    const-string v2, "] loopCount["

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    const-string v2, "] isPortrait["

    #@30
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    const-string v2, "]"

    #@3a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v1

    #@3e
    const-string v2, "] isPointerIDChanged["

    #@40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@47
    move-result-object v1

    #@48
    const-string v2, "] savedPointerChangedID["

    #@4a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v1

    #@4e
    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@51
    move-result-object v1

    #@52
    const-string v2, "]"

    #@54
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v1

    #@58
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v1

    #@5c
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    .line 1184
    :cond_5f
    return-void
.end method

.method public static reportValue(ILcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;I)V
    .registers 7
    .parameter "DEBUG_LEVEL"
    .parameter "maskA"
    .parameter "maskB"
    .parameter "action"

    #@0
    .prologue
    .line 1174
    const/4 v0, 0x2

    #@1
    if-lt p0, v0, :cond_33

    #@3
    .line 1175
    const-string v0, "TouchEventFilter"

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "<reportValue> reportIDBits"

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    const-string v2, " reportAction["

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-static {p3}, Lcom/lge/view/TouchEventFilter$PrintLog;->actionLog(I)Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    const-string v2, "]"

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 1177
    :cond_33
    return-void
.end method
