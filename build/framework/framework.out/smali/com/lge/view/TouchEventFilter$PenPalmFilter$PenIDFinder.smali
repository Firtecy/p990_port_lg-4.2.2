.class Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;
.super Ljava/lang/Object;
.source "TouchEventFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/view/TouchEventFilter$PenPalmFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PenIDFinder"
.end annotation


# static fields
.field private static final MAX_PEN_PRESSURE:I = 0x23

.field private static final MAX_SAMPLE_COUNT:I = 0x14

.field private static final MIN_WIDTH_MINOR:I = 0x4

.field private static final Z_AVR_MAX_VALUE:I = 0x14

.field private static final Z_AVR_MIN_VALUE:I = 0xf


# instance fields
.field private cnt:I

.field private dAvr:F

.field private inEdgeRegion:Z

.field private inPalmRegion:Z

.field private pen:Z

.field private result:F

.field final synthetic this$1:Lcom/lge/view/TouchEventFilter$PenPalmFilter;

.field private zAvr:F


# direct methods
.method public constructor <init>(Lcom/lge/view/TouchEventFilter$PenPalmFilter;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 812
    iput-object p1, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->this$1:Lcom/lge/view/TouchEventFilter$PenPalmFilter;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 813
    invoke-virtual {p0}, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->init()V

    #@8
    .line 814
    return-void
.end method


# virtual methods
.method public addInfo(IIFFFZ)V
    .registers 17
    .parameter "velX"
    .parameter "velY"
    .parameter "y"
    .parameter "z"
    .parameter "w"
    .parameter "edge"

    #@0
    .prologue
    .line 827
    iget v8, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->cnt:I

    #@2
    add-int/lit8 v8, v8, 0x1

    #@4
    iput v8, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->cnt:I

    #@6
    .line 828
    iget v8, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->cnt:I

    #@8
    const/16 v9, 0x14

    #@a
    if-lt v8, v9, :cond_7b

    #@c
    const/16 v0, 0x14

    #@e
    .line 830
    .local v0, count:I
    :goto_e
    int-to-float v8, p1

    #@f
    int-to-float v9, p2

    #@10
    mul-float v3, v8, v9

    #@12
    .line 831
    .local v3, mulVel:F
    div-float v4, v3, p3

    #@14
    .line 833
    .local v4, newDAvr:F
    if-lez v0, :cond_32

    #@16
    .line 834
    iget v8, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->zAvr:F

    #@18
    add-int/lit8 v9, v0, -0x1

    #@1a
    int-to-float v9, v9

    #@1b
    mul-float/2addr v8, v9

    #@1c
    int-to-float v9, v0

    #@1d
    div-float/2addr v8, v9

    #@1e
    int-to-float v9, v0

    #@1f
    div-float v9, p4, v9

    #@21
    add-float/2addr v8, v9

    #@22
    iput v8, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->zAvr:F

    #@24
    .line 835
    iget v8, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->dAvr:F

    #@26
    add-int/lit8 v9, v0, -0x1

    #@28
    int-to-float v9, v9

    #@29
    mul-float/2addr v8, v9

    #@2a
    int-to-float v9, v0

    #@2b
    div-float/2addr v8, v9

    #@2c
    int-to-float v9, v0

    #@2d
    div-float v9, v4, v9

    #@2f
    add-float/2addr v8, v9

    #@30
    iput v8, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->dAvr:F

    #@32
    .line 838
    :cond_32
    iget v8, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->zAvr:F

    #@34
    const/high16 v9, 0x420c

    #@36
    cmpl-float v8, v8, v9

    #@38
    if-gtz v8, :cond_40

    #@3a
    const/high16 v8, 0x4080

    #@3c
    cmpl-float v8, p5, v8

    #@3e
    if-lez v8, :cond_43

    #@40
    .line 839
    :cond_40
    const/4 v8, 0x0

    #@41
    iput-boolean v8, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->pen:Z

    #@43
    .line 841
    :cond_43
    if-nez p6, :cond_48

    #@45
    .line 842
    const/4 v8, 0x0

    #@46
    iput-boolean v8, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->inEdgeRegion:Z

    #@48
    .line 844
    :cond_48
    iget v8, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->zAvr:F

    #@4a
    const/high16 v9, 0x41a0

    #@4c
    cmpl-float v8, v8, v9

    #@4e
    if-lez v8, :cond_7e

    #@50
    iget v8, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->zAvr:F

    #@52
    const/high16 v9, 0x41a0

    #@54
    sub-float v1, v8, v9

    #@56
    .line 845
    .local v1, deltaZ:F
    :goto_56
    mul-float v8, v1, v1

    #@58
    const/high16 v9, 0x3f80

    #@5a
    add-float v7, v8, v9

    #@5c
    .line 846
    .local v7, zVal:F
    iget-boolean v8, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->pen:Z

    #@5e
    if-eqz v8, :cond_8f

    #@60
    const/high16 v6, 0x3f80

    #@62
    .line 847
    .local v6, penVal:F
    :goto_62
    iget-boolean v8, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->inPalmRegion:Z

    #@64
    if-eqz v8, :cond_91

    #@66
    const/4 v5, 0x0

    #@67
    .line 848
    .local v5, palmVal:F
    :goto_67
    iget-boolean v8, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->inEdgeRegion:Z

    #@69
    if-eqz v8, :cond_94

    #@6b
    const/4 v2, 0x0

    #@6c
    .line 849
    .local v2, edgeVal:F
    :goto_6c
    iget v8, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->dAvr:F

    #@6e
    mul-float/2addr v8, v6

    #@6f
    mul-float/2addr v8, v5

    #@70
    mul-float/2addr v8, v2

    #@71
    int-to-float v9, v0

    #@72
    mul-float/2addr v8, v9

    #@73
    div-float/2addr v8, v7

    #@74
    mul-float v9, v6, v5

    #@76
    mul-float/2addr v9, v2

    #@77
    add-float/2addr v8, v9

    #@78
    iput v8, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->result:F

    #@7a
    .line 850
    return-void

    #@7b
    .line 828
    .end local v0           #count:I
    .end local v1           #deltaZ:F
    .end local v2           #edgeVal:F
    .end local v3           #mulVel:F
    .end local v4           #newDAvr:F
    .end local v5           #palmVal:F
    .end local v6           #penVal:F
    .end local v7           #zVal:F
    :cond_7b
    iget v0, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->cnt:I

    #@7d
    goto :goto_e

    #@7e
    .line 844
    .restart local v0       #count:I
    .restart local v3       #mulVel:F
    .restart local v4       #newDAvr:F
    :cond_7e
    iget v8, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->zAvr:F

    #@80
    const/high16 v9, 0x4170

    #@82
    cmpg-float v8, v8, v9

    #@84
    if-gez v8, :cond_8d

    #@86
    iget v8, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->zAvr:F

    #@88
    const/high16 v9, 0x4170

    #@8a
    sub-float v1, v8, v9

    #@8c
    goto :goto_56

    #@8d
    :cond_8d
    const/4 v1, 0x0

    #@8e
    goto :goto_56

    #@8f
    .line 846
    .restart local v1       #deltaZ:F
    .restart local v7       #zVal:F
    :cond_8f
    const/4 v6, 0x0

    #@90
    goto :goto_62

    #@91
    .line 847
    .restart local v6       #penVal:F
    :cond_91
    const/high16 v5, 0x3f80

    #@93
    goto :goto_67

    #@94
    .line 848
    .restart local v5       #palmVal:F
    :cond_94
    const/high16 v2, 0x3f80

    #@96
    goto :goto_6c
.end method

.method public getCount()I
    .registers 2

    #@0
    .prologue
    .line 853
    iget v0, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->cnt:I

    #@2
    return v0
.end method

.method public getResult()F
    .registers 2

    #@0
    .prologue
    .line 852
    iget v0, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->result:F

    #@2
    return v0
.end method

.method public init()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 817
    iput v1, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->cnt:I

    #@4
    .line 818
    iput v0, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->zAvr:F

    #@6
    .line 819
    iput v0, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->dAvr:F

    #@8
    .line 820
    iput v0, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->result:F

    #@a
    .line 821
    const/4 v0, 0x1

    #@b
    iput-boolean v0, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->pen:Z

    #@d
    .line 822
    iput-boolean v1, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->inPalmRegion:Z

    #@f
    .line 823
    iput-boolean v1, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->inEdgeRegion:Z

    #@11
    .line 824
    return-void
.end method

.method public isFinger()Z
    .registers 2

    #@0
    .prologue
    .line 855
    iget-boolean v0, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->pen:Z

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public isPen()Z
    .registers 3

    #@0
    .prologue
    .line 854
    iget v0, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->cnt:I

    #@2
    const/4 v1, 0x1

    #@3
    if-lt v0, v1, :cond_d

    #@5
    iget-boolean v0, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->inEdgeRegion:Z

    #@7
    if-nez v0, :cond_d

    #@9
    iget-boolean v0, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->inPalmRegion:Z

    #@b
    if-eqz v0, :cond_f

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    :goto_e
    return v0

    #@f
    :cond_f
    iget-boolean v0, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->pen:Z

    #@11
    goto :goto_e
.end method

.method public setEdgeRegion(Z)V
    .registers 2
    .parameter "isInEdgeRegion"

    #@0
    .prologue
    .line 857
    iput-boolean p1, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->inEdgeRegion:Z

    #@2
    return-void
.end method

.method public setPalmRegion(Z)V
    .registers 2
    .parameter "isInPalmRegion"

    #@0
    .prologue
    .line 856
    iput-boolean p1, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->inPalmRegion:Z

    #@2
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 859
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "count["

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->cnt:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, "] zAvr["

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->zAvr:F

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, "] dAvr["

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget v1, p0, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->dAvr:F

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, "] isPen["

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    invoke-virtual {p0}, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->isPen()Z

    #@32
    move-result v1

    #@33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@36
    move-result-object v0

    #@37
    const-string v1, "] result["

    #@39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v0

    #@3d
    invoke-virtual {p0}, Lcom/lge/view/TouchEventFilter$PenPalmFilter$PenIDFinder;->getResult()F

    #@40
    move-result v1

    #@41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@44
    move-result-object v0

    #@45
    const-string v1, "]"

    #@47
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v0

    #@4b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v0

    #@4f
    return-object v0
.end method
