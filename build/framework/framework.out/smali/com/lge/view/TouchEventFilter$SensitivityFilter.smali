.class Lcom/lge/view/TouchEventFilter$SensitivityFilter;
.super Ljava/lang/Object;
.source "TouchEventFilter.java"

# interfaces
.implements Lcom/lge/view/TouchEventFilter$IEventFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/view/TouchEventFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SensitivityFilter"
.end annotation


# instance fields
.field private final PRESSURE_LIMIT:I

.field private act:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

.field private downMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

.field private reportIdBits:I

.field private reportMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

.field final synthetic this$0:Lcom/lge/view/TouchEventFilter;


# direct methods
.method public constructor <init>(Lcom/lge/view/TouchEventFilter;Landroid/view/View;)V
    .registers 4
    .parameter
    .parameter "view"

    #@0
    .prologue
    .line 723
    iput-object p1, p0, Lcom/lge/view/TouchEventFilter$SensitivityFilter;->this$0:Lcom/lge/view/TouchEventFilter;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 718
    const/16 v0, 0x28

    #@7
    iput v0, p0, Lcom/lge/view/TouchEventFilter$SensitivityFilter;->PRESSURE_LIMIT:I

    #@9
    .line 720
    new-instance v0, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@b
    invoke-direct {v0}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;-><init>()V

    #@e
    iput-object v0, p0, Lcom/lge/view/TouchEventFilter$SensitivityFilter;->downMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@10
    .line 721
    new-instance v0, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@12
    invoke-direct {v0}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;-><init>()V

    #@15
    iput-object v0, p0, Lcom/lge/view/TouchEventFilter$SensitivityFilter;->reportMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@17
    .line 724
    invoke-virtual {p0}, Lcom/lge/view/TouchEventFilter$SensitivityFilter;->init()V

    #@1a
    .line 725
    return-void
.end method


# virtual methods
.method public filtering(Landroid/view/MotionEvent;I)Z
    .registers 13
    .parameter "event"
    .parameter "tmpMask"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 735
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@4
    move-result v0

    #@5
    .line 736
    .local v0, NI:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@8
    move-result v1

    #@9
    .line 738
    .local v1, action:I
    iget-object v8, p0, Lcom/lge/view/TouchEventFilter$SensitivityFilter;->reportMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@b
    invoke-virtual {v8}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->clearAllMask()I

    #@e
    .line 740
    const/4 v2, 0x0

    #@f
    .local v2, i:I
    :goto_f
    if-ge v2, v0, :cond_43

    #@11
    .line 741
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@14
    move-result v3

    #@15
    .line 742
    .local v3, id:I
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPressure(I)F

    #@18
    move-result v8

    #@19
    const/high16 v9, 0x437f

    #@1b
    mul-float/2addr v8, v9

    #@1c
    float-to-int v5, v8

    #@1d
    .line 743
    .local v5, pressure:I
    const/16 v8, 0x28

    #@1f
    if-le v5, v8, :cond_41

    #@21
    move v6, v7

    #@22
    .line 745
    .local v6, validPressure:Z
    :goto_22
    iget-object v8, p0, Lcom/lge/view/TouchEventFilter$SensitivityFilter;->downMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@24
    invoke-virtual {v8, v3}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->checkMask(I)Z

    #@27
    move-result v8

    #@28
    if-nez v8, :cond_31

    #@2a
    .line 746
    if-eqz v6, :cond_31

    #@2c
    .line 747
    iget-object v8, p0, Lcom/lge/view/TouchEventFilter$SensitivityFilter;->downMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@2e
    invoke-virtual {v8, v3}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->setMask(I)I

    #@31
    .line 750
    :cond_31
    iget-object v8, p0, Lcom/lge/view/TouchEventFilter$SensitivityFilter;->downMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@33
    invoke-virtual {v8, v3}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->checkMask(I)Z

    #@36
    move-result v8

    #@37
    if-eqz v8, :cond_3e

    #@39
    .line 751
    iget-object v8, p0, Lcom/lge/view/TouchEventFilter$SensitivityFilter;->reportMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@3b
    invoke-virtual {v8, v3}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->setMask(I)I

    #@3e
    .line 740
    :cond_3e
    add-int/lit8 v2, v2, 0x1

    #@40
    goto :goto_f

    #@41
    .line 743
    .end local v6           #validPressure:Z
    :cond_41
    const/4 v6, 0x0

    #@42
    goto :goto_22

    #@43
    .line 754
    .end local v3           #id:I
    .end local v5           #pressure:I
    :cond_43
    const/4 v8, 0x6

    #@44
    if-eq v1, v8, :cond_48

    #@46
    if-ne v1, v7, :cond_5a

    #@48
    .line 755
    :cond_48
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@4b
    move-result v4

    #@4c
    .line 756
    .local v4, index:I
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@4f
    move-result v3

    #@50
    .line 757
    .restart local v3       #id:I
    iget-object v8, p0, Lcom/lge/view/TouchEventFilter$SensitivityFilter;->downMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@52
    invoke-virtual {v8, v3}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->clearMask(I)I

    #@55
    .line 758
    iget-object v8, p0, Lcom/lge/view/TouchEventFilter$SensitivityFilter;->reportMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@57
    invoke-virtual {v8, v3}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->clearMask(I)I

    #@5a
    .line 761
    .end local v3           #id:I
    .end local v4           #index:I
    :cond_5a
    iget-object v8, p0, Lcom/lge/view/TouchEventFilter$SensitivityFilter;->reportMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@5c
    invoke-virtual {v8}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->getMask()I

    #@5f
    move-result v8

    #@60
    iput v8, p0, Lcom/lge/view/TouchEventFilter$SensitivityFilter;->reportIdBits:I

    #@62
    .line 763
    return v7
.end method

.method public getAct()Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;
    .registers 2

    #@0
    .prologue
    .line 767
    iget-object v0, p0, Lcom/lge/view/TouchEventFilter$SensitivityFilter;->act:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@2
    return-object v0
.end method

.method public getReportMask()I
    .registers 2

    #@0
    .prologue
    .line 771
    iget v0, p0, Lcom/lge/view/TouchEventFilter$SensitivityFilter;->reportIdBits:I

    #@2
    return v0
.end method

.method public init()V
    .registers 2

    #@0
    .prologue
    .line 728
    sget-object v0, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->NONE:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@2
    iput-object v0, p0, Lcom/lge/view/TouchEventFilter$SensitivityFilter;->act:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@4
    .line 729
    const/4 v0, 0x0

    #@5
    iput v0, p0, Lcom/lge/view/TouchEventFilter$SensitivityFilter;->reportIdBits:I

    #@7
    .line 730
    iget-object v0, p0, Lcom/lge/view/TouchEventFilter$SensitivityFilter;->downMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@9
    invoke-virtual {v0}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->clearAllMask()I

    #@c
    .line 731
    iget-object v0, p0, Lcom/lge/view/TouchEventFilter$SensitivityFilter;->reportMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@e
    invoke-virtual {v0}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->clearAllMask()I

    #@11
    .line 732
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 775
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "SensitivityFilter: downMask"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Lcom/lge/view/TouchEventFilter$SensitivityFilter;->downMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    return-object v0
.end method
