.class final enum Lcom/lge/view/TouchEventFilter$DoAction;
.super Ljava/lang/Enum;
.source "TouchEventFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/view/TouchEventFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "DoAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/lge/view/TouchEventFilter$DoAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/lge/view/TouchEventFilter$DoAction;

.field public static final enum DO_IGNORE:Lcom/lge/view/TouchEventFilter$DoAction;

.field public static final enum DO_NOTHING:Lcom/lge/view/TouchEventFilter$DoAction;

.field public static final enum DO_SPLIT:Lcom/lge/view/TouchEventFilter$DoAction;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 53
    new-instance v0, Lcom/lge/view/TouchEventFilter$DoAction;

    #@5
    const-string v1, "DO_NOTHING"

    #@7
    invoke-direct {v0, v1, v2}, Lcom/lge/view/TouchEventFilter$DoAction;-><init>(Ljava/lang/String;I)V

    #@a
    sput-object v0, Lcom/lge/view/TouchEventFilter$DoAction;->DO_NOTHING:Lcom/lge/view/TouchEventFilter$DoAction;

    #@c
    new-instance v0, Lcom/lge/view/TouchEventFilter$DoAction;

    #@e
    const-string v1, "DO_IGNORE"

    #@10
    invoke-direct {v0, v1, v3}, Lcom/lge/view/TouchEventFilter$DoAction;-><init>(Ljava/lang/String;I)V

    #@13
    sput-object v0, Lcom/lge/view/TouchEventFilter$DoAction;->DO_IGNORE:Lcom/lge/view/TouchEventFilter$DoAction;

    #@15
    new-instance v0, Lcom/lge/view/TouchEventFilter$DoAction;

    #@17
    const-string v1, "DO_SPLIT"

    #@19
    invoke-direct {v0, v1, v4}, Lcom/lge/view/TouchEventFilter$DoAction;-><init>(Ljava/lang/String;I)V

    #@1c
    sput-object v0, Lcom/lge/view/TouchEventFilter$DoAction;->DO_SPLIT:Lcom/lge/view/TouchEventFilter$DoAction;

    #@1e
    .line 52
    const/4 v0, 0x3

    #@1f
    new-array v0, v0, [Lcom/lge/view/TouchEventFilter$DoAction;

    #@21
    sget-object v1, Lcom/lge/view/TouchEventFilter$DoAction;->DO_NOTHING:Lcom/lge/view/TouchEventFilter$DoAction;

    #@23
    aput-object v1, v0, v2

    #@25
    sget-object v1, Lcom/lge/view/TouchEventFilter$DoAction;->DO_IGNORE:Lcom/lge/view/TouchEventFilter$DoAction;

    #@27
    aput-object v1, v0, v3

    #@29
    sget-object v1, Lcom/lge/view/TouchEventFilter$DoAction;->DO_SPLIT:Lcom/lge/view/TouchEventFilter$DoAction;

    #@2b
    aput-object v1, v0, v4

    #@2d
    sput-object v0, Lcom/lge/view/TouchEventFilter$DoAction;->$VALUES:[Lcom/lge/view/TouchEventFilter$DoAction;

    #@2f
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/lge/view/TouchEventFilter$DoAction;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 52
    const-class v0, Lcom/lge/view/TouchEventFilter$DoAction;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/lge/view/TouchEventFilter$DoAction;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/lge/view/TouchEventFilter$DoAction;
    .registers 1

    #@0
    .prologue
    .line 52
    sget-object v0, Lcom/lge/view/TouchEventFilter$DoAction;->$VALUES:[Lcom/lge/view/TouchEventFilter$DoAction;

    #@2
    invoke-virtual {v0}, [Lcom/lge/view/TouchEventFilter$DoAction;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/lge/view/TouchEventFilter$DoAction;

    #@8
    return-object v0
.end method
