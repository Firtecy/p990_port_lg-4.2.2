.class Lcom/lge/view/TouchEventFilter$EssentialFilter;
.super Ljava/lang/Object;
.source "TouchEventFilter.java"

# interfaces
.implements Lcom/lge/view/TouchEventFilter$IEventFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/view/TouchEventFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EssentialFilter"
.end annotation


# instance fields
.field private act:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

.field private isCancel:Z

.field private reportIdBits:I

.field private reportMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

.field final synthetic this$0:Lcom/lge/view/TouchEventFilter;


# direct methods
.method public constructor <init>(Lcom/lge/view/TouchEventFilter;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 1094
    iput-object p1, p0, Lcom/lge/view/TouchEventFilter$EssentialFilter;->this$0:Lcom/lge/view/TouchEventFilter;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 1092
    new-instance v0, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@7
    invoke-direct {v0}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;-><init>()V

    #@a
    iput-object v0, p0, Lcom/lge/view/TouchEventFilter$EssentialFilter;->reportMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@c
    .line 1095
    invoke-virtual {p0}, Lcom/lge/view/TouchEventFilter$EssentialFilter;->init()V

    #@f
    .line 1096
    return-void
.end method


# virtual methods
.method public filtering(Landroid/view/MotionEvent;I)Z
    .registers 9
    .parameter "event"
    .parameter "tmpMask"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 1106
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@4
    move-result v0

    #@5
    .line 1107
    .local v0, action:I
    iget-object v3, p0, Lcom/lge/view/TouchEventFilter$EssentialFilter;->reportMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@7
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerIdBits()I

    #@a
    move-result v4

    #@b
    invoke-virtual {v3, v4}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->setAllMask(I)I

    #@e
    .line 1109
    const/4 v3, 0x6

    #@f
    if-eq v0, v3, :cond_13

    #@11
    if-ne v0, v5, :cond_20

    #@13
    .line 1110
    :cond_13
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@16
    move-result v2

    #@17
    .line 1111
    .local v2, index:I
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@1a
    move-result v1

    #@1b
    .line 1112
    .local v1, id:I
    iget-object v3, p0, Lcom/lge/view/TouchEventFilter$EssentialFilter;->reportMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@1d
    invoke-virtual {v3, v1}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->clearMask(I)I

    #@20
    .line 1115
    .end local v1           #id:I
    .end local v2           #index:I
    :cond_20
    iget-object v3, p0, Lcom/lge/view/TouchEventFilter$EssentialFilter;->reportMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@22
    invoke-virtual {v3}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->getMask()I

    #@25
    move-result v3

    #@26
    iput v3, p0, Lcom/lge/view/TouchEventFilter$EssentialFilter;->reportIdBits:I

    #@28
    .line 1117
    return v5
.end method

.method public getAct()Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;
    .registers 2

    #@0
    .prologue
    .line 1121
    iget-object v0, p0, Lcom/lge/view/TouchEventFilter$EssentialFilter;->act:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@2
    return-object v0
.end method

.method public getReportMask()I
    .registers 2

    #@0
    .prologue
    .line 1125
    iget v0, p0, Lcom/lge/view/TouchEventFilter$EssentialFilter;->reportIdBits:I

    #@2
    return v0
.end method

.method public init()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1099
    sget-object v0, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->NONE:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@3
    iput-object v0, p0, Lcom/lge/view/TouchEventFilter$EssentialFilter;->act:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@5
    .line 1100
    iput v1, p0, Lcom/lge/view/TouchEventFilter$EssentialFilter;->reportIdBits:I

    #@7
    .line 1101
    iget-object v0, p0, Lcom/lge/view/TouchEventFilter$EssentialFilter;->reportMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@9
    invoke-virtual {v0}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->clearAllMask()I

    #@c
    .line 1102
    iput-boolean v1, p0, Lcom/lge/view/TouchEventFilter$EssentialFilter;->isCancel:Z

    #@e
    .line 1103
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1129
    const-string v0, "EssentialFilter"

    #@2
    return-object v0
.end method
