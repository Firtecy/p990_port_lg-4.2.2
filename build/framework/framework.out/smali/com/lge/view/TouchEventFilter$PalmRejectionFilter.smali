.class Lcom/lge/view/TouchEventFilter$PalmRejectionFilter;
.super Ljava/lang/Object;
.source "TouchEventFilter.java"

# interfaces
.implements Lcom/lge/view/TouchEventFilter$IEventFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/view/TouchEventFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PalmRejectionFilter"
.end annotation


# instance fields
.field private act:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

.field private ignore:Z

.field private reportIdBits:I

.field final synthetic this$0:Lcom/lge/view/TouchEventFilter;


# direct methods
.method public constructor <init>(Lcom/lge/view/TouchEventFilter;Landroid/view/View;)V
    .registers 3
    .parameter
    .parameter "view"

    #@0
    .prologue
    .line 564
    iput-object p1, p0, Lcom/lge/view/TouchEventFilter$PalmRejectionFilter;->this$0:Lcom/lge/view/TouchEventFilter;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 565
    invoke-virtual {p0}, Lcom/lge/view/TouchEventFilter$PalmRejectionFilter;->init()V

    #@8
    .line 566
    return-void
.end method


# virtual methods
.method public filtering(Landroid/view/MotionEvent;I)Z
    .registers 10
    .parameter "event"
    .parameter "tmpMask"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 575
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@4
    move-result v0

    #@5
    .line 576
    .local v0, NI:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@8
    move-result v1

    #@9
    .line 578
    .local v1, action:I
    const/4 v3, 0x0

    #@a
    .line 581
    .local v3, isPalm:Z
    iget-boolean v5, p0, Lcom/lge/view/TouchEventFilter$PalmRejectionFilter;->ignore:Z

    #@c
    if-eqz v5, :cond_16

    #@e
    .line 582
    sget-object v5, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->IGNORE:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@10
    iput-object v5, p0, Lcom/lge/view/TouchEventFilter$PalmRejectionFilter;->act:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@12
    .line 583
    const/4 v5, 0x0

    #@13
    iput v5, p0, Lcom/lge/view/TouchEventFilter$PalmRejectionFilter;->reportIdBits:I

    #@15
    .line 598
    :goto_15
    return v6

    #@16
    .line 586
    :cond_16
    const/4 v2, 0x0

    #@17
    .local v2, i:I
    :goto_17
    if-ge v2, v0, :cond_27

    #@19
    .line 587
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getToolType(I)I

    #@1c
    move-result v4

    #@1d
    .line 588
    .local v4, toolType:I
    const/4 v5, 0x5

    #@1e
    if-eq v4, v5, :cond_23

    #@20
    const/4 v5, 0x6

    #@21
    if-ne v4, v5, :cond_24

    #@23
    .line 589
    :cond_23
    const/4 v3, 0x1

    #@24
    .line 586
    :cond_24
    add-int/lit8 v2, v2, 0x1

    #@26
    goto :goto_17

    #@27
    .line 592
    .end local v4           #toolType:I
    :cond_27
    if-eqz v3, :cond_2f

    #@29
    .line 593
    iput-boolean v6, p0, Lcom/lge/view/TouchEventFilter$PalmRejectionFilter;->ignore:Z

    #@2b
    .line 594
    sget-object v5, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->CANCEL:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@2d
    iput-object v5, p0, Lcom/lge/view/TouchEventFilter$PalmRejectionFilter;->act:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@2f
    .line 596
    :cond_2f
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerIdBits()I

    #@32
    move-result v5

    #@33
    iput v5, p0, Lcom/lge/view/TouchEventFilter$PalmRejectionFilter;->reportIdBits:I

    #@35
    goto :goto_15
.end method

.method public getAct()Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;
    .registers 2

    #@0
    .prologue
    .line 602
    iget-object v0, p0, Lcom/lge/view/TouchEventFilter$PalmRejectionFilter;->act:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@2
    return-object v0
.end method

.method public getReportMask()I
    .registers 2

    #@0
    .prologue
    .line 606
    iget v0, p0, Lcom/lge/view/TouchEventFilter$PalmRejectionFilter;->reportIdBits:I

    #@2
    return v0
.end method

.method public init()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 569
    sget-object v0, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->NONE:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@3
    iput-object v0, p0, Lcom/lge/view/TouchEventFilter$PalmRejectionFilter;->act:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@5
    .line 570
    iput v1, p0, Lcom/lge/view/TouchEventFilter$PalmRejectionFilter;->reportIdBits:I

    #@7
    .line 571
    iput-boolean v1, p0, Lcom/lge/view/TouchEventFilter$PalmRejectionFilter;->ignore:Z

    #@9
    .line 572
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 610
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "PalmRejectionFilter: ignore["

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-boolean v1, p0, Lcom/lge/view/TouchEventFilter$PalmRejectionFilter;->ignore:Z

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, "]"

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    return-object v0
.end method
