.class public Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;
.super Ljava/lang/Object;
.source "TouchEventFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/view/TouchEventFilter$IEventFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TouchCheckMask"
.end annotation


# instance fields
.field private count:I

.field private mask:I


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 489
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 490
    invoke-virtual {p0}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->clearAllMask()I

    #@6
    .line 491
    return-void
.end method


# virtual methods
.method public calCount()V
    .registers 4

    #@0
    .prologue
    .line 532
    const/4 v1, 0x1

    #@1
    .line 533
    .local v1, index:I
    const/4 v2, 0x0

    #@2
    iput v2, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->count:I

    #@4
    .line 534
    const/4 v0, 0x0

    #@5
    .local v0, i:I
    :goto_5
    const/16 v2, 0xa

    #@7
    if-ge v0, v2, :cond_19

    #@9
    .line 535
    iget v2, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->mask:I

    #@b
    and-int/2addr v2, v1

    #@c
    if-eqz v2, :cond_14

    #@e
    .line 536
    iget v2, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->count:I

    #@10
    add-int/lit8 v2, v2, 0x1

    #@12
    iput v2, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->count:I

    #@14
    .line 537
    :cond_14
    shl-int/lit8 v1, v1, 0x1

    #@16
    .line 534
    add-int/lit8 v0, v0, 0x1

    #@18
    goto :goto_5

    #@19
    .line 539
    :cond_19
    return-void
.end method

.method public checkMask(I)Z
    .registers 5
    .parameter "i"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 520
    iget v1, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->mask:I

    #@3
    shl-int v2, v0, p1

    #@5
    and-int/2addr v1, v2

    #@6
    if-eqz v1, :cond_9

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public clearAllMask()I
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 514
    iput v0, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->mask:I

    #@3
    .line 515
    invoke-virtual {p0, v0}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->setCount(I)V

    #@6
    .line 516
    iget v0, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->mask:I

    #@8
    return v0
.end method

.method public clearMask(I)I
    .registers 4
    .parameter "i"

    #@0
    .prologue
    .line 507
    invoke-virtual {p0, p1}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->checkMask(I)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_9

    #@6
    .line 508
    invoke-virtual {p0}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->decCount()V

    #@9
    .line 509
    :cond_9
    iget v0, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->mask:I

    #@b
    const/4 v1, 0x1

    #@c
    shl-int/2addr v1, p1

    #@d
    xor-int/lit8 v1, v1, -0x1

    #@f
    and-int/2addr v0, v1

    #@10
    iput v0, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->mask:I

    #@12
    .line 510
    iget v0, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->mask:I

    #@14
    return v0
.end method

.method public decCount()V
    .registers 2

    #@0
    .prologue
    .line 529
    iget v0, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->count:I

    #@2
    add-int/lit8 v0, v0, -0x1

    #@4
    iput v0, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->count:I

    #@6
    return-void
.end method

.method public getCount()I
    .registers 2

    #@0
    .prologue
    .line 527
    iget v0, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->count:I

    #@2
    return v0
.end method

.method public getLastBitToID()I
    .registers 3

    #@0
    .prologue
    .line 542
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    const/16 v1, 0xa

    #@3
    if-ge v0, v1, :cond_f

    #@5
    .line 543
    invoke-virtual {p0, v0}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->checkMask(I)Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_c

    #@b
    .line 546
    .end local v0           #i:I
    :goto_b
    return v0

    #@c
    .line 542
    .restart local v0       #i:I
    :cond_c
    add-int/lit8 v0, v0, 0x1

    #@e
    goto :goto_1

    #@f
    .line 546
    :cond_f
    const/4 v0, -0x1

    #@10
    goto :goto_b
.end method

.method public getMask()I
    .registers 2

    #@0
    .prologue
    .line 524
    iget v0, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->mask:I

    #@2
    return v0
.end method

.method public incCount()V
    .registers 2

    #@0
    .prologue
    .line 528
    iget v0, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->count:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->count:I

    #@6
    return-void
.end method

.method public setAllMask(I)I
    .registers 3
    .parameter "newMask"

    #@0
    .prologue
    .line 501
    iput p1, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->mask:I

    #@2
    .line 502
    invoke-virtual {p0}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->calCount()V

    #@5
    .line 503
    iget v0, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->mask:I

    #@7
    return v0
.end method

.method public setCount(I)V
    .registers 2
    .parameter "newCount"

    #@0
    .prologue
    .line 530
    iput p1, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->count:I

    #@2
    return-void
.end method

.method public setMask(I)I
    .registers 4
    .parameter "i"

    #@0
    .prologue
    .line 494
    invoke-virtual {p0, p1}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->checkMask(I)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_9

    #@6
    .line 495
    invoke-virtual {p0}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->incCount()V

    #@9
    .line 496
    :cond_9
    iget v0, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->mask:I

    #@b
    const/4 v1, 0x1

    #@c
    shl-int/2addr v1, p1

    #@d
    or-int/2addr v0, v1

    #@e
    iput v0, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->mask:I

    #@10
    .line 497
    iget v0, p0, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->mask:I

    #@12
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 550
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 551
    .local v0, msg:Ljava/lang/StringBuilder;
    const-string v1, "["

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {p0}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->getCount()I

    #@e
    move-result v2

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, ":"

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {p0}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->getMask()I

    #@1c
    move-result v2

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    const-string v2, "]"

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    .line 552
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    return-object v1
.end method
