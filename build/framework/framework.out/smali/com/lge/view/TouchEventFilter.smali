.class public Lcom/lge/view/TouchEventFilter;
.super Ljava/lang/Object;
.source "TouchEventFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/view/TouchEventFilter$PrintLog;,
        Lcom/lge/view/TouchEventFilter$EmptyFilter;,
        Lcom/lge/view/TouchEventFilter$EssentialFilter;,
        Lcom/lge/view/TouchEventFilter$PenPalmFilter;,
        Lcom/lge/view/TouchEventFilter$SensitivityFilter;,
        Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;,
        Lcom/lge/view/TouchEventFilter$PalmRejectionFilter;,
        Lcom/lge/view/TouchEventFilter$IEventFilter;,
        Lcom/lge/view/TouchEventFilter$DoAction;,
        Lcom/lge/view/TouchEventFilter$DoDispatchMotionEvent;
    }
.end annotation


# static fields
.field public static final FILTER_NUM:I = 0x4

.field public static final GRIP_SUPPRESSION_FILTER:I = 0x1

.field public static final GRIP_SUPPRESSION_FILTER_BIT:I = 0x2

.field private static final MAX_LOOP_COUNT:I = 0x5

.field public static final PALM_REJECTION_FILTER:I = 0x0

.field public static final PALM_REJECTION_FILTER_BIT:I = 0x1

.field public static final PEN_PALM_FILTER:I = 0x3

.field public static final PEN_PALM_FILTER_BIT:I = 0x8

.field public static final SENSITIVITY_FILTER:I = 0x2

.field public static final SENSITIVITY_FILTER_BIT:I = 0x4

.field private static isPortrait:Z


# instance fields
.field private final DEBUG_LEVEL:I

.field private filteringResult:Lcom/lge/view/TouchEventFilter$DoAction;

.field private isPointerIDChanged:Z

.field private loopCount:I

.field private mDoDispatchMotionEvent:Lcom/lge/view/TouchEventFilter$DoDispatchMotionEvent;

.field private mEventFilter:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/lge/view/TouchEventFilter$IEventFilter;",
            ">;"
        }
    .end annotation
.end field

.field private mView:Landroid/view/View;

.field private needToSendAdditionalEvent:Z

.field private onlyNewMaskBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

.field private onlyOldMaskBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

.field private reportAction:I

.field private reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

.field private reportIDBitsWithoutUpEvent:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

.field private savedPointerChangedID:I

.field private useFilter:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 84
    const/4 v0, 0x0

    #@1
    sput-boolean v0, Lcom/lge/view/TouchEventFilter;->isPortrait:Z

    #@3
    return-void
.end method

.method public constructor <init>(Lcom/lge/view/TouchEventFilter$DoDispatchMotionEvent;)V
    .registers 5
    .parameter "dispatch"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 92
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 32
    const-string v1, "debug.view.toucheventfilter"

    #@6
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@9
    move-result v1

    #@a
    iput v1, p0, Lcom/lge/view/TouchEventFilter;->DEBUG_LEVEL:I

    #@c
    .line 69
    new-instance v1, Ljava/util/ArrayList;

    #@e
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@11
    iput-object v1, p0, Lcom/lge/view/TouchEventFilter;->mEventFilter:Ljava/util/ArrayList;

    #@13
    .line 71
    const/4 v1, 0x0

    #@14
    iput-object v1, p0, Lcom/lge/view/TouchEventFilter;->mView:Landroid/view/View;

    #@16
    .line 74
    new-instance v1, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@18
    invoke-direct {v1}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;-><init>()V

    #@1b
    iput-object v1, p0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@1d
    .line 75
    new-instance v1, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@1f
    invoke-direct {v1}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;-><init>()V

    #@22
    iput-object v1, p0, Lcom/lge/view/TouchEventFilter;->reportIDBitsWithoutUpEvent:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@24
    .line 76
    iput v2, p0, Lcom/lge/view/TouchEventFilter;->reportAction:I

    #@26
    .line 79
    iput-boolean v2, p0, Lcom/lge/view/TouchEventFilter;->needToSendAdditionalEvent:Z

    #@28
    .line 80
    sget-object v1, Lcom/lge/view/TouchEventFilter$DoAction;->DO_NOTHING:Lcom/lge/view/TouchEventFilter$DoAction;

    #@2a
    iput-object v1, p0, Lcom/lge/view/TouchEventFilter;->filteringResult:Lcom/lge/view/TouchEventFilter$DoAction;

    #@2c
    .line 81
    iput v2, p0, Lcom/lge/view/TouchEventFilter;->loopCount:I

    #@2e
    .line 82
    iput-boolean v2, p0, Lcom/lge/view/TouchEventFilter;->isPointerIDChanged:Z

    #@30
    .line 83
    const/4 v1, -0x1

    #@31
    iput v1, p0, Lcom/lge/view/TouchEventFilter;->savedPointerChangedID:I

    #@33
    .line 87
    iput v2, p0, Lcom/lge/view/TouchEventFilter;->useFilter:I

    #@35
    .line 296
    new-instance v1, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@37
    invoke-direct {v1}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;-><init>()V

    #@3a
    iput-object v1, p0, Lcom/lge/view/TouchEventFilter;->onlyNewMaskBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@3c
    .line 297
    new-instance v1, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@3e
    invoke-direct {v1}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;-><init>()V

    #@41
    iput-object v1, p0, Lcom/lge/view/TouchEventFilter;->onlyOldMaskBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@43
    .line 93
    invoke-direct {p0, p1}, Lcom/lge/view/TouchEventFilter;->init(Lcom/lge/view/TouchEventFilter$DoDispatchMotionEvent;)V

    #@46
    .line 94
    const/4 v0, 0x0

    #@47
    .local v0, i:I
    :goto_47
    const/4 v1, 0x4

    #@48
    if-ge v0, v1, :cond_57

    #@4a
    .line 95
    iget-object v1, p0, Lcom/lge/view/TouchEventFilter;->mEventFilter:Ljava/util/ArrayList;

    #@4c
    new-instance v2, Lcom/lge/view/TouchEventFilter$EmptyFilter;

    #@4e
    invoke-direct {v2, p0}, Lcom/lge/view/TouchEventFilter$EmptyFilter;-><init>(Lcom/lge/view/TouchEventFilter;)V

    #@51
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@54
    .line 94
    add-int/lit8 v0, v0, 0x1

    #@56
    goto :goto_47

    #@57
    .line 97
    :cond_57
    iget-object v1, p0, Lcom/lge/view/TouchEventFilter;->mEventFilter:Ljava/util/ArrayList;

    #@59
    new-instance v2, Lcom/lge/view/TouchEventFilter$EssentialFilter;

    #@5b
    invoke-direct {v2, p0}, Lcom/lge/view/TouchEventFilter$EssentialFilter;-><init>(Lcom/lge/view/TouchEventFilter;)V

    #@5e
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@61
    .line 98
    return-void
.end method

.method static synthetic access$000()Z
    .registers 1

    #@0
    .prologue
    .line 31
    sget-boolean v0, Lcom/lge/view/TouchEventFilter;->isPortrait:Z

    #@2
    return v0
.end method

.method private checkPointerID(Landroid/view/MotionEvent;)V
    .registers 12
    .parameter "event"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 382
    iget-object v8, p0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@4
    invoke-virtual {v8}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->getMask()I

    #@7
    move-result v8

    #@8
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerIdBits()I

    #@b
    move-result v9

    #@c
    if-eq v8, v9, :cond_4e

    #@e
    move v5, v6

    #@f
    .line 383
    .local v5, isFiltered:Z
    :goto_f
    iget v8, p0, Lcom/lge/view/TouchEventFilter;->reportAction:I

    #@11
    if-nez v8, :cond_50

    #@13
    iget-object v8, p0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@15
    invoke-virtual {v8}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->getCount()I

    #@18
    move-result v8

    #@19
    if-ne v8, v6, :cond_50

    #@1b
    iget-object v8, p0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@1d
    invoke-virtual {v8, v7}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->checkMask(I)Z

    #@20
    move-result v8

    #@21
    if-nez v8, :cond_50

    #@23
    move v4, v6

    #@24
    .line 384
    .local v4, isActionDownButIdNotZero:Z
    :goto_24
    if-eqz v5, :cond_32

    #@26
    if-eqz v4, :cond_32

    #@28
    .line 385
    iput-boolean v6, p0, Lcom/lge/view/TouchEventFilter;->isPointerIDChanged:Z

    #@2a
    .line 386
    iget-object v6, p0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@2c
    invoke-virtual {v6}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->getLastBitToID()I

    #@2f
    move-result v6

    #@30
    iput v6, p0, Lcom/lge/view/TouchEventFilter;->savedPointerChangedID:I

    #@32
    .line 389
    :cond_32
    iget-boolean v6, p0, Lcom/lge/view/TouchEventFilter;->isPointerIDChanged:Z

    #@34
    if-eqz v6, :cond_4d

    #@36
    .line 390
    iget-object v6, p0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@38
    iget v8, p0, Lcom/lge/view/TouchEventFilter;->savedPointerChangedID:I

    #@3a
    invoke-virtual {v6, v8}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->checkMask(I)Z

    #@3d
    move-result v6

    #@3e
    if-nez v6, :cond_52

    #@40
    iget-object v6, p0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@42
    invoke-virtual {v6, v7}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->checkMask(I)Z

    #@45
    move-result v6

    #@46
    if-nez v6, :cond_52

    #@48
    .line 391
    iput-boolean v7, p0, Lcom/lge/view/TouchEventFilter;->isPointerIDChanged:Z

    #@4a
    .line 392
    const/4 v6, -0x1

    #@4b
    iput v6, p0, Lcom/lge/view/TouchEventFilter;->savedPointerChangedID:I

    #@4d
    .line 416
    :cond_4d
    :goto_4d
    return-void

    #@4e
    .end local v4           #isActionDownButIdNotZero:Z
    .end local v5           #isFiltered:Z
    :cond_4e
    move v5, v7

    #@4f
    .line 382
    goto :goto_f

    #@50
    .restart local v5       #isFiltered:Z
    :cond_50
    move v4, v7

    #@51
    .line 383
    goto :goto_24

    #@52
    .line 395
    .restart local v4       #isActionDownButIdNotZero:Z
    :cond_52
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    #@55
    move-result v2

    #@56
    .line 396
    .local v2, index1:I
    iget v6, p0, Lcom/lge/view/TouchEventFilter;->savedPointerChangedID:I

    #@58
    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    #@5b
    move-result v3

    #@5c
    .line 397
    .local v3, index2:I
    iget-object v6, p0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@5e
    invoke-virtual {v6, v7}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->checkMask(I)Z

    #@61
    move-result v1

    #@62
    .line 398
    .local v1, idZeroShouldReport:Z
    iget-object v6, p0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@64
    iget v8, p0, Lcom/lge/view/TouchEventFilter;->savedPointerChangedID:I

    #@66
    invoke-virtual {v6, v8}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->checkMask(I)Z

    #@69
    move-result v0

    #@6a
    .line 400
    .local v0, idSavedShouldReport:Z
    if-ltz v2, :cond_7a

    #@6c
    .line 401
    iget v6, p0, Lcom/lge/view/TouchEventFilter;->savedPointerChangedID:I

    #@6e
    invoke-virtual {p1, v2, v6}, Landroid/view/MotionEvent;->setPointerId(II)V

    #@71
    .line 402
    if-eqz v1, :cond_87

    #@73
    .line 403
    iget-object v6, p0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@75
    iget v8, p0, Lcom/lge/view/TouchEventFilter;->savedPointerChangedID:I

    #@77
    invoke-virtual {v6, v8}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->setMask(I)I

    #@7a
    .line 407
    :cond_7a
    :goto_7a
    if-ltz v3, :cond_4d

    #@7c
    .line 408
    invoke-virtual {p1, v3, v7}, Landroid/view/MotionEvent;->setPointerId(II)V

    #@7f
    .line 409
    if-eqz v0, :cond_8f

    #@81
    .line 410
    iget-object v6, p0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@83
    invoke-virtual {v6, v7}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->setMask(I)I

    #@86
    goto :goto_4d

    #@87
    .line 405
    :cond_87
    iget-object v6, p0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@89
    iget v8, p0, Lcom/lge/view/TouchEventFilter;->savedPointerChangedID:I

    #@8b
    invoke-virtual {v6, v8}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->clearMask(I)I

    #@8e
    goto :goto_7a

    #@8f
    .line 412
    :cond_8f
    iget-object v6, p0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@91
    invoke-virtual {v6, v7}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->clearMask(I)I

    #@94
    goto :goto_4d
.end method

.method private errorHandlingOfFiltering(II)Lcom/lge/view/TouchEventFilter$DoAction;
    .registers 4
    .parameter "mask"
    .parameter "action"

    #@0
    .prologue
    .line 244
    invoke-direct {p0, p1, p2}, Lcom/lge/view/TouchEventFilter;->setReportValue(II)V

    #@3
    .line 245
    invoke-direct {p0}, Lcom/lge/view/TouchEventFilter;->initProperties()V

    #@6
    .line 246
    sget-object v0, Lcom/lge/view/TouchEventFilter$DoAction;->DO_NOTHING:Lcom/lge/view/TouchEventFilter$DoAction;

    #@8
    return-object v0
.end method

.method private filtering(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    .registers 22
    .parameter "event"

    #@0
    .prologue
    .line 178
    move-object/from16 v0, p0

    #@2
    iget v4, v0, Lcom/lge/view/TouchEventFilter;->DEBUG_LEVEL:I

    #@4
    const-string v5, "<OLD> "

    #@6
    const/4 v6, 0x0

    #@7
    move-object/from16 v0, p1

    #@9
    invoke-static {v4, v5, v0, v6}, Lcom/lge/view/TouchEventFilter$PrintLog;->event(ILjava/lang/String;Landroid/view/MotionEvent;Z)V

    #@c
    .line 180
    invoke-direct/range {p0 .. p1}, Lcom/lge/view/TouchEventFilter;->isFilterableEvent(Landroid/view/MotionEvent;)Z

    #@f
    move-result v4

    #@10
    if-nez v4, :cond_22

    #@12
    .line 181
    invoke-direct/range {p0 .. p0}, Lcom/lge/view/TouchEventFilter;->initAllFilter()V

    #@15
    .line 182
    move-object/from16 v0, p0

    #@17
    iget v4, v0, Lcom/lge/view/TouchEventFilter;->DEBUG_LEVEL:I

    #@19
    const-string v5, "<NOT> "

    #@1b
    const/4 v6, 0x0

    #@1c
    move-object/from16 v0, p1

    #@1e
    invoke-static {v4, v5, v0, v6}, Lcom/lge/view/TouchEventFilter$PrintLog;->event(ILjava/lang/String;Landroid/view/MotionEvent;Z)V

    #@21
    .line 231
    .end local p1
    :goto_21
    return-object p1

    #@22
    .line 186
    .restart local p1
    :cond_22
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerIdBits()I

    #@25
    move-result v18

    #@26
    .line 187
    .local v18, pointerIdBits:I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    #@29
    move-result v15

    #@2a
    .line 188
    .local v15, oldAction:I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@2d
    move-result v16

    #@2e
    .line 190
    .local v16, oldActionMasked:I
    move/from16 v19, v18

    #@30
    .line 191
    .local v19, reportMask:I
    sget-object v11, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->NONE:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@32
    .line 192
    .local v11, act:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;
    sget-object v4, Lcom/lge/view/TouchEventFilter$DoAction;->DO_NOTHING:Lcom/lge/view/TouchEventFilter$DoAction;

    #@34
    move-object/from16 v0, p0

    #@36
    iput-object v4, v0, Lcom/lge/view/TouchEventFilter;->filteringResult:Lcom/lge/view/TouchEventFilter$DoAction;

    #@38
    .line 194
    if-nez v15, :cond_43

    #@3a
    move-object/from16 v0, p0

    #@3c
    iget v4, v0, Lcom/lge/view/TouchEventFilter;->loopCount:I

    #@3e
    if-nez v4, :cond_43

    #@40
    .line 195
    invoke-direct/range {p0 .. p0}, Lcom/lge/view/TouchEventFilter;->initAllFilter()V

    #@43
    .line 197
    :cond_43
    const/4 v13, 0x0

    #@44
    .local v13, i:I
    :goto_44
    move-object/from16 v0, p0

    #@46
    iget-object v4, v0, Lcom/lge/view/TouchEventFilter;->mEventFilter:Ljava/util/ArrayList;

    #@48
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@4b
    move-result v4

    #@4c
    if-ge v13, v4, :cond_81

    #@4e
    .line 198
    move-object/from16 v0, p0

    #@50
    iget-object v4, v0, Lcom/lge/view/TouchEventFilter;->mEventFilter:Ljava/util/ArrayList;

    #@52
    invoke-virtual {v4, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@55
    move-result-object v12

    #@56
    check-cast v12, Lcom/lge/view/TouchEventFilter$IEventFilter;

    #@58
    .line 199
    .local v12, eventFilter:Lcom/lge/view/TouchEventFilter$IEventFilter;
    move-object/from16 v0, p1

    #@5a
    move/from16 v1, v19

    #@5c
    invoke-interface {v12, v0, v1}, Lcom/lge/view/TouchEventFilter$IEventFilter;->filtering(Landroid/view/MotionEvent;I)Z

    #@5f
    move-result v4

    #@60
    if-nez v4, :cond_65

    #@62
    .line 197
    :goto_62
    add-int/lit8 v13, v13, 0x1

    #@64
    goto :goto_44

    #@65
    .line 202
    :cond_65
    invoke-interface {v12}, Lcom/lge/view/TouchEventFilter$IEventFilter;->getReportMask()I

    #@68
    move-result v4

    #@69
    and-int v19, v19, v4

    #@6b
    .line 203
    invoke-interface {v12}, Lcom/lge/view/TouchEventFilter$IEventFilter;->getAct()Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@6e
    move-result-object v4

    #@6f
    move-object/from16 v0, p0

    #@71
    invoke-direct {v0, v11, v4}, Lcom/lge/view/TouchEventFilter;->getAct(Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;)Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@74
    move-result-object v11

    #@75
    .line 205
    move-object/from16 v0, p0

    #@77
    iget v4, v0, Lcom/lge/view/TouchEventFilter;->DEBUG_LEVEL:I

    #@79
    move/from16 v0, v19

    #@7b
    move/from16 v1, v18

    #@7d
    invoke-static {v4, v12, v0, v1, v11}, Lcom/lge/view/TouchEventFilter$PrintLog;->filtering(ILcom/lge/view/TouchEventFilter$IEventFilter;IILcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;)V

    #@80
    goto :goto_62

    #@81
    .line 208
    .end local v12           #eventFilter:Lcom/lge/view/TouchEventFilter$IEventFilter;
    :cond_81
    move-object/from16 v0, p0

    #@83
    iget-object v4, v0, Lcom/lge/view/TouchEventFilter;->reportIDBitsWithoutUpEvent:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@85
    invoke-virtual {v4}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->getMask()I

    #@88
    move-result v17

    #@89
    .line 209
    .local v17, oldReportMask:I
    move-object/from16 v0, p0

    #@8b
    move-object/from16 v1, p1

    #@8d
    move/from16 v2, v19

    #@8f
    move/from16 v3, v17

    #@91
    invoke-direct {v0, v1, v2, v3, v11}, Lcom/lge/view/TouchEventFilter;->setReportValue(Landroid/view/MotionEvent;IILcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;)Lcom/lge/view/TouchEventFilter$DoAction;

    #@94
    move-result-object v4

    #@95
    move-object/from16 v0, p0

    #@97
    iput-object v4, v0, Lcom/lge/view/TouchEventFilter;->filteringResult:Lcom/lge/view/TouchEventFilter$DoAction;

    #@99
    .line 210
    invoke-direct/range {p0 .. p1}, Lcom/lge/view/TouchEventFilter;->checkPointerID(Landroid/view/MotionEvent;)V

    #@9c
    .line 212
    move-object/from16 v0, p0

    #@9e
    iget v4, v0, Lcom/lge/view/TouchEventFilter;->reportAction:I

    #@a0
    move-object/from16 v0, p1

    #@a2
    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->setAction(I)V

    #@a5
    .line 215
    move-object/from16 v14, p1

    #@a7
    .line 216
    .local v14, newEvent:Landroid/view/MotionEvent;
    move-object/from16 v0, p0

    #@a9
    iget-object v4, v0, Lcom/lge/view/TouchEventFilter;->filteringResult:Lcom/lge/view/TouchEventFilter$DoAction;

    #@ab
    sget-object v5, Lcom/lge/view/TouchEventFilter$DoAction;->DO_SPLIT:Lcom/lge/view/TouchEventFilter$DoAction;

    #@ad
    if-ne v4, v5, :cond_bd

    #@af
    .line 217
    move-object/from16 v0, p0

    #@b1
    iget-object v4, v0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@b3
    invoke-virtual {v4}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->getMask()I

    #@b6
    move-result v4

    #@b7
    move-object/from16 v0, p1

    #@b9
    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->split(I)Landroid/view/MotionEvent;

    #@bc
    move-result-object v14

    #@bd
    .line 218
    :cond_bd
    move-object/from16 v0, p0

    #@bf
    iget-boolean v4, v0, Lcom/lge/view/TouchEventFilter;->needToSendAdditionalEvent:Z

    #@c1
    if-eqz v4, :cond_d0

    #@c3
    .line 219
    move-object/from16 v0, p1

    #@c5
    if-ne v14, v0, :cond_cb

    #@c7
    .line 220
    invoke-static/range {p1 .. p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@ca
    move-result-object v14

    #@cb
    .line 221
    :cond_cb
    move-object/from16 v0, p1

    #@cd
    invoke-virtual {v0, v15}, Landroid/view/MotionEvent;->setAction(I)V

    #@d0
    .line 224
    :cond_d0
    move-object/from16 v0, p0

    #@d2
    iget v4, v0, Lcom/lge/view/TouchEventFilter;->DEBUG_LEVEL:I

    #@d4
    move-object/from16 v0, p0

    #@d6
    iget-object v5, v0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@d8
    move-object/from16 v0, p0

    #@da
    iget-object v6, v0, Lcom/lge/view/TouchEventFilter;->reportIDBitsWithoutUpEvent:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@dc
    move-object/from16 v0, p0

    #@de
    iget v7, v0, Lcom/lge/view/TouchEventFilter;->reportAction:I

    #@e0
    invoke-static {v4, v5, v6, v7}, Lcom/lge/view/TouchEventFilter$PrintLog;->reportValue(ILcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;I)V

    #@e3
    .line 225
    move-object/from16 v0, p0

    #@e5
    iget v4, v0, Lcom/lge/view/TouchEventFilter;->DEBUG_LEVEL:I

    #@e7
    move-object/from16 v0, p0

    #@e9
    iget-boolean v5, v0, Lcom/lge/view/TouchEventFilter;->needToSendAdditionalEvent:Z

    #@eb
    move-object/from16 v0, p0

    #@ed
    iget-object v6, v0, Lcom/lge/view/TouchEventFilter;->filteringResult:Lcom/lge/view/TouchEventFilter$DoAction;

    #@ef
    move-object/from16 v0, p0

    #@f1
    iget v7, v0, Lcom/lge/view/TouchEventFilter;->loopCount:I

    #@f3
    sget-boolean v8, Lcom/lge/view/TouchEventFilter;->isPortrait:Z

    #@f5
    move-object/from16 v0, p0

    #@f7
    iget-boolean v9, v0, Lcom/lge/view/TouchEventFilter;->isPointerIDChanged:Z

    #@f9
    move-object/from16 v0, p0

    #@fb
    iget v10, v0, Lcom/lge/view/TouchEventFilter;->savedPointerChangedID:I

    #@fd
    invoke-static/range {v4 .. v10}, Lcom/lge/view/TouchEventFilter$PrintLog;->properties(IZLcom/lge/view/TouchEventFilter$DoAction;IZZI)V

    #@100
    .line 226
    move-object/from16 v0, p0

    #@102
    iget v5, v0, Lcom/lge/view/TouchEventFilter;->DEBUG_LEVEL:I

    #@104
    const-string v6, "<NEW> "

    #@106
    move-object/from16 v0, p0

    #@108
    iget-object v4, v0, Lcom/lge/view/TouchEventFilter;->filteringResult:Lcom/lge/view/TouchEventFilter$DoAction;

    #@10a
    sget-object v7, Lcom/lge/view/TouchEventFilter$DoAction;->DO_IGNORE:Lcom/lge/view/TouchEventFilter$DoAction;

    #@10c
    if-ne v4, v7, :cond_128

    #@10e
    const/4 v4, 0x1

    #@10f
    :goto_10f
    invoke-static {v5, v6, v14, v4}, Lcom/lge/view/TouchEventFilter$PrintLog;->event(ILjava/lang/String;Landroid/view/MotionEvent;Z)V

    #@112
    .line 228
    const/4 v4, 0x1

    #@113
    move/from16 v0, v16

    #@115
    if-ne v0, v4, :cond_124

    #@117
    move-object/from16 v0, p0

    #@119
    iget-object v4, v0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@11b
    invoke-virtual {v4}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->getCount()I

    #@11e
    move-result v4

    #@11f
    if-nez v4, :cond_124

    #@121
    .line 229
    invoke-direct/range {p0 .. p0}, Lcom/lge/view/TouchEventFilter;->initAllFilter()V

    #@124
    :cond_124
    move-object/from16 p1, v14

    #@126
    .line 231
    goto/16 :goto_21

    #@128
    .line 226
    :cond_128
    const/4 v4, 0x0

    #@129
    goto :goto_10f
.end method

.method private getAct(Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;)Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;
    .registers 5
    .parameter "oldAct"
    .parameter "newAct"

    #@0
    .prologue
    .line 293
    invoke-virtual {p2}, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->getVal()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p1}, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->getVal()I

    #@7
    move-result v1

    #@8
    if-le v0, v1, :cond_b

    #@a
    .end local p2
    :goto_a
    return-object p2

    #@b
    .restart local p2
    :cond_b
    move-object p2, p1

    #@c
    goto :goto_a
.end method

.method private init(Lcom/lge/view/TouchEventFilter$DoDispatchMotionEvent;)V
    .registers 3
    .parameter "dispatch"

    #@0
    .prologue
    .line 145
    iget-object v0, p0, Lcom/lge/view/TouchEventFilter;->mEventFilter:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@5
    .line 146
    invoke-direct {p0}, Lcom/lge/view/TouchEventFilter;->initReportValue()V

    #@8
    .line 147
    invoke-direct {p0}, Lcom/lge/view/TouchEventFilter;->initProperties()V

    #@b
    .line 149
    iput-object p1, p0, Lcom/lge/view/TouchEventFilter;->mDoDispatchMotionEvent:Lcom/lge/view/TouchEventFilter$DoDispatchMotionEvent;

    #@d
    .line 150
    const/4 v0, 0x0

    #@e
    iput v0, p0, Lcom/lge/view/TouchEventFilter;->useFilter:I

    #@10
    .line 151
    return-void
.end method

.method private initAllFilter()V
    .registers 4

    #@0
    .prologue
    .line 235
    const/4 v1, 0x0

    #@1
    .local v1, i:I
    :goto_1
    iget-object v2, p0, Lcom/lge/view/TouchEventFilter;->mEventFilter:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v2

    #@7
    if-ge v1, v2, :cond_17

    #@9
    .line 236
    iget-object v2, p0, Lcom/lge/view/TouchEventFilter;->mEventFilter:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Lcom/lge/view/TouchEventFilter$IEventFilter;

    #@11
    .line 237
    .local v0, eventFilter:Lcom/lge/view/TouchEventFilter$IEventFilter;
    invoke-interface {v0}, Lcom/lge/view/TouchEventFilter$IEventFilter;->init()V

    #@14
    .line 235
    add-int/lit8 v1, v1, 0x1

    #@16
    goto :goto_1

    #@17
    .line 239
    .end local v0           #eventFilter:Lcom/lge/view/TouchEventFilter$IEventFilter;
    :cond_17
    invoke-direct {p0}, Lcom/lge/view/TouchEventFilter;->initReportValue()V

    #@1a
    .line 240
    invoke-direct {p0}, Lcom/lge/view/TouchEventFilter;->initProperties()V

    #@1d
    .line 241
    return-void
.end method

.method private initProperties()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 164
    sget-object v0, Lcom/lge/view/TouchEventFilter$DoAction;->DO_NOTHING:Lcom/lge/view/TouchEventFilter$DoAction;

    #@3
    invoke-direct {p0, v1, v0, v1}, Lcom/lge/view/TouchEventFilter;->setProperties(ZLcom/lge/view/TouchEventFilter$DoAction;I)V

    #@6
    .line 165
    return-void
.end method

.method private initReportValue()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 154
    invoke-direct {p0, v0, v0}, Lcom/lge/view/TouchEventFilter;->setReportValue(II)V

    #@4
    .line 155
    return-void
.end method

.method private isFilterableEvent(Landroid/view/MotionEvent;)Z
    .registers 15
    .parameter "event"

    #@0
    .prologue
    .line 250
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    #@3
    move-result v7

    #@4
    .line 251
    .local v7, source:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@7
    move-result v1

    #@8
    .line 252
    .local v1, action:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@b
    move-result v0

    #@c
    .line 254
    .local v0, NI:I
    and-int/lit8 v12, v7, 0x2

    #@e
    if-nez v12, :cond_31

    #@10
    const/4 v5, 0x1

    #@11
    .line 255
    .local v5, invalidSource:Z
    :goto_11
    const/4 v6, 0x0

    #@12
    .line 256
    .local v6, invalidToolType:Z
    const/4 v3, 0x0

    #@13
    .line 257
    .local v3, invalidAction:Z
    const/4 v4, 0x0

    #@14
    .line 259
    .local v4, invalidEvent:Z
    packed-switch v1, :pswitch_data_42

    #@17
    .line 269
    :pswitch_17
    const/4 v3, 0x1

    #@18
    .line 273
    :goto_18
    const/4 v2, 0x0

    #@19
    .local v2, i:I
    :goto_19
    if-ge v2, v0, :cond_35

    #@1b
    .line 274
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getToolType(I)I

    #@1e
    move-result v8

    #@1f
    .line 275
    .local v8, toolType:I
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPressure(I)F

    #@22
    move-result v11

    #@23
    .line 276
    .local v11, z:F
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getToolMajor(I)F

    #@26
    move-result v9

    #@27
    .line 277
    .local v9, wM:F
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getToolMinor(I)F

    #@2a
    move-result v10

    #@2b
    .line 279
    .local v10, wm:F
    if-nez v8, :cond_2e

    #@2d
    .line 280
    const/4 v6, 0x1

    #@2e
    .line 273
    :cond_2e
    add-int/lit8 v2, v2, 0x1

    #@30
    goto :goto_19

    #@31
    .line 254
    .end local v2           #i:I
    .end local v3           #invalidAction:Z
    .end local v4           #invalidEvent:Z
    .end local v5           #invalidSource:Z
    .end local v6           #invalidToolType:Z
    .end local v8           #toolType:I
    .end local v9           #wM:F
    .end local v10           #wm:F
    .end local v11           #z:F
    :cond_31
    const/4 v5, 0x0

    #@32
    goto :goto_11

    #@33
    .line 266
    .restart local v3       #invalidAction:Z
    .restart local v4       #invalidEvent:Z
    .restart local v5       #invalidSource:Z
    .restart local v6       #invalidToolType:Z
    :pswitch_33
    const/4 v3, 0x0

    #@34
    .line 267
    goto :goto_18

    #@35
    .line 286
    .restart local v2       #i:I
    :cond_35
    if-nez v5, :cond_3d

    #@37
    if-nez v6, :cond_3d

    #@39
    if-nez v3, :cond_3d

    #@3b
    if-eqz v4, :cond_3f

    #@3d
    .line 287
    :cond_3d
    const/4 v12, 0x0

    #@3e
    .line 289
    :goto_3e
    return v12

    #@3f
    :cond_3f
    const/4 v12, 0x1

    #@40
    goto :goto_3e

    #@41
    .line 259
    nop

    #@42
    :pswitch_data_42
    .packed-switch 0x0
        :pswitch_33
        :pswitch_33
        :pswitch_33
        :pswitch_33
        :pswitch_17
        :pswitch_33
        :pswitch_33
    .end packed-switch
.end method

.method private setProperties(ZLcom/lge/view/TouchEventFilter$DoAction;I)V
    .registers 7
    .parameter "repeat"
    .parameter "result"
    .parameter "cnt"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 168
    iput-boolean p1, p0, Lcom/lge/view/TouchEventFilter;->needToSendAdditionalEvent:Z

    #@3
    .line 169
    iput-object p2, p0, Lcom/lge/view/TouchEventFilter;->filteringResult:Lcom/lge/view/TouchEventFilter$DoAction;

    #@5
    .line 170
    iput p3, p0, Lcom/lge/view/TouchEventFilter;->loopCount:I

    #@7
    .line 171
    iput-boolean v0, p0, Lcom/lge/view/TouchEventFilter;->isPointerIDChanged:Z

    #@9
    .line 172
    const/4 v1, -0x1

    #@a
    iput v1, p0, Lcom/lge/view/TouchEventFilter;->savedPointerChangedID:I

    #@c
    .line 173
    iget-object v1, p0, Lcom/lge/view/TouchEventFilter;->mView:Landroid/view/View;

    #@e
    if-eqz v1, :cond_21

    #@10
    .line 174
    iget-object v1, p0, Lcom/lge/view/TouchEventFilter;->mView:Landroid/view/View;

    #@12
    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@19
    move-result-object v1

    #@1a
    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    #@1c
    const/4 v2, 0x2

    #@1d
    if-ne v1, v2, :cond_22

    #@1f
    :goto_1f
    sput-boolean v0, Lcom/lge/view/TouchEventFilter;->isPortrait:Z

    #@21
    .line 175
    :cond_21
    return-void

    #@22
    .line 174
    :cond_22
    const/4 v0, 0x1

    #@23
    goto :goto_1f
.end method

.method private setReportValue(Landroid/view/MotionEvent;IILcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;)Lcom/lge/view/TouchEventFilter$DoAction;
    .registers 15
    .parameter "event"
    .parameter "newReportMask"
    .parameter "oldReportMask"
    .parameter "act"

    #@0
    .prologue
    const/4 v9, 0x5

    #@1
    const/4 v7, 0x0

    #@2
    const/4 v6, 0x1

    #@3
    .line 300
    and-int v0, p2, p3

    #@5
    .line 301
    .local v0, andMask:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerIdBits()I

    #@8
    move-result v4

    #@9
    .line 303
    .local v4, pointerIdBits:I
    iget-object v5, p0, Lcom/lge/view/TouchEventFilter;->onlyNewMaskBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@b
    xor-int/lit8 v8, v0, -0x1

    #@d
    and-int/2addr v8, p2

    #@e
    invoke-virtual {v5, v8}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->setAllMask(I)I

    #@11
    .line 304
    iget-object v5, p0, Lcom/lge/view/TouchEventFilter;->onlyOldMaskBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@13
    xor-int/lit8 v8, v0, -0x1

    #@15
    and-int/2addr v8, p3

    #@16
    invoke-virtual {v5, v8}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->setAllMask(I)I

    #@19
    .line 306
    sget-object v3, Lcom/lge/view/TouchEventFilter$DoAction;->DO_NOTHING:Lcom/lge/view/TouchEventFilter$DoAction;

    #@1b
    .line 311
    .local v3, mDoAction:Lcom/lge/view/TouchEventFilter$DoAction;
    sget-object v5, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->CANCEL:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@1d
    if-eq p4, v5, :cond_23

    #@1f
    sget-object v5, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->CANCEL_AND_REPEAT:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@21
    if-ne p4, v5, :cond_72

    #@23
    .line 312
    :cond_23
    iget-object v5, p0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@25
    invoke-virtual {v5, p3}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->setAllMask(I)I

    #@28
    .line 313
    iget-object v5, p0, Lcom/lge/view/TouchEventFilter;->reportIDBitsWithoutUpEvent:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@2a
    iget-object v8, p0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@2c
    invoke-virtual {v8}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->getMask()I

    #@2f
    move-result v8

    #@30
    invoke-virtual {v5, v8}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->setAllMask(I)I

    #@33
    .line 314
    const/4 v5, 0x3

    #@34
    iput v5, p0, Lcom/lge/view/TouchEventFilter;->reportAction:I

    #@36
    .line 349
    :goto_36
    iget-object v5, p0, Lcom/lge/view/TouchEventFilter;->onlyOldMaskBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@38
    invoke-virtual {v5}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->getCount()I

    #@3b
    move-result v5

    #@3c
    iget-object v8, p0, Lcom/lge/view/TouchEventFilter;->onlyNewMaskBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@3e
    invoke-virtual {v8}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->getCount()I

    #@41
    move-result v8

    #@42
    add-int/2addr v5, v8

    #@43
    if-gt v5, v6, :cond_49

    #@45
    sget-object v5, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->CANCEL_AND_REPEAT:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@47
    if-ne p4, v5, :cond_11c

    #@49
    :cond_49
    iget v5, p0, Lcom/lge/view/TouchEventFilter;->loopCount:I

    #@4b
    if-ge v5, v9, :cond_11c

    #@4d
    .line 350
    iput-boolean v6, p0, Lcom/lge/view/TouchEventFilter;->needToSendAdditionalEvent:Z

    #@4f
    .line 351
    iget v5, p0, Lcom/lge/view/TouchEventFilter;->loopCount:I

    #@51
    add-int/lit8 v5, v5, 0x1

    #@53
    iput v5, p0, Lcom/lge/view/TouchEventFilter;->loopCount:I

    #@55
    .line 358
    :goto_55
    sget-object v5, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->IGNORE:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@57
    if-eq p4, v5, :cond_61

    #@59
    iget-object v5, p0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@5b
    invoke-virtual {v5}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->getMask()I

    #@5e
    move-result v5

    #@5f
    if-nez v5, :cond_122

    #@61
    .line 359
    :cond_61
    sget-object v3, Lcom/lge/view/TouchEventFilter$DoAction;->DO_IGNORE:Lcom/lge/view/TouchEventFilter$DoAction;

    #@63
    .line 365
    :goto_63
    iget v5, p0, Lcom/lge/view/TouchEventFilter;->loopCount:I

    #@65
    if-lt v5, v9, :cond_132

    #@67
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@6a
    move-result v5

    #@6b
    if-ne v5, v6, :cond_132

    #@6d
    .line 366
    invoke-direct {p0, v4, v6}, Lcom/lge/view/TouchEventFilter;->errorHandlingOfFiltering(II)Lcom/lge/view/TouchEventFilter$DoAction;

    #@70
    move-result-object v5

    #@71
    .line 368
    :goto_71
    return-object v5

    #@72
    .line 316
    :cond_72
    sget-object v5, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->IGNORE:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@74
    if-ne p4, v5, :cond_84

    #@76
    .line 317
    iget-object v5, p0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@78
    invoke-virtual {v5}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->clearAllMask()I

    #@7b
    .line 318
    iget-object v5, p0, Lcom/lge/view/TouchEventFilter;->reportIDBitsWithoutUpEvent:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@7d
    invoke-virtual {v5}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->clearAllMask()I

    #@80
    .line 319
    const/4 v5, -0x1

    #@81
    iput v5, p0, Lcom/lge/view/TouchEventFilter;->reportAction:I

    #@83
    goto :goto_36

    #@84
    .line 321
    :cond_84
    iget-object v5, p0, Lcom/lge/view/TouchEventFilter;->onlyOldMaskBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@86
    invoke-virtual {v5}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->getCount()I

    #@89
    move-result v5

    #@8a
    if-eqz v5, :cond_c4

    #@8c
    .line 322
    iget-object v5, p0, Lcom/lge/view/TouchEventFilter;->onlyOldMaskBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@8e
    invoke-virtual {v5}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->getLastBitToID()I

    #@91
    move-result v1

    #@92
    .line 323
    .local v1, id:I
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    #@95
    move-result v2

    #@96
    .line 324
    .local v2, index:I
    if-gez v2, :cond_9d

    #@98
    .line 325
    invoke-direct {p0, v4, v6}, Lcom/lge/view/TouchEventFilter;->errorHandlingOfFiltering(II)Lcom/lge/view/TouchEventFilter$DoAction;

    #@9b
    move-result-object v5

    #@9c
    goto :goto_71

    #@9d
    .line 326
    :cond_9d
    iget-object v5, p0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@9f
    invoke-virtual {v5, p3}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->setAllMask(I)I

    #@a2
    .line 327
    iget-object v5, p0, Lcom/lge/view/TouchEventFilter;->reportIDBitsWithoutUpEvent:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@a4
    iget-object v8, p0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@a6
    invoke-virtual {v8}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->getMask()I

    #@a9
    move-result v8

    #@aa
    invoke-virtual {v5, v8}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->setAllMask(I)I

    #@ad
    .line 328
    iget-object v5, p0, Lcom/lge/view/TouchEventFilter;->reportIDBitsWithoutUpEvent:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@af
    invoke-virtual {v5, v1}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->clearMask(I)I

    #@b2
    .line 329
    iget-object v5, p0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@b4
    invoke-virtual {v5}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->getCount()I

    #@b7
    move-result v5

    #@b8
    if-ne v5, v6, :cond_bf

    #@ba
    move v5, v6

    #@bb
    :goto_bb
    iput v5, p0, Lcom/lge/view/TouchEventFilter;->reportAction:I

    #@bd
    goto/16 :goto_36

    #@bf
    :cond_bf
    shl-int/lit8 v5, v2, 0x8

    #@c1
    or-int/lit8 v5, v5, 0x6

    #@c3
    goto :goto_bb

    #@c4
    .line 332
    .end local v1           #id:I
    .end local v2           #index:I
    :cond_c4
    iget-object v5, p0, Lcom/lge/view/TouchEventFilter;->onlyNewMaskBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@c6
    invoke-virtual {v5}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->getCount()I

    #@c9
    move-result v5

    #@ca
    if-eqz v5, :cond_104

    #@cc
    .line 333
    iget-object v5, p0, Lcom/lge/view/TouchEventFilter;->onlyNewMaskBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@ce
    invoke-virtual {v5}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->getLastBitToID()I

    #@d1
    move-result v1

    #@d2
    .line 334
    .restart local v1       #id:I
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    #@d5
    move-result v2

    #@d6
    .line 335
    .restart local v2       #index:I
    if-gez v2, :cond_dd

    #@d8
    .line 336
    invoke-direct {p0, v4, v6}, Lcom/lge/view/TouchEventFilter;->errorHandlingOfFiltering(II)Lcom/lge/view/TouchEventFilter$DoAction;

    #@db
    move-result-object v5

    #@dc
    goto :goto_71

    #@dd
    .line 337
    :cond_dd
    iget-object v5, p0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@df
    invoke-virtual {v5, v0}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->setAllMask(I)I

    #@e2
    .line 338
    iget-object v5, p0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@e4
    invoke-virtual {v5, v1}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->setMask(I)I

    #@e7
    .line 339
    iget-object v5, p0, Lcom/lge/view/TouchEventFilter;->reportIDBitsWithoutUpEvent:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@e9
    iget-object v8, p0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@eb
    invoke-virtual {v8}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->getMask()I

    #@ee
    move-result v8

    #@ef
    invoke-virtual {v5, v8}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->setAllMask(I)I

    #@f2
    .line 340
    iget-object v5, p0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@f4
    invoke-virtual {v5}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->getCount()I

    #@f7
    move-result v5

    #@f8
    if-ne v5, v6, :cond_ff

    #@fa
    move v5, v7

    #@fb
    :goto_fb
    iput v5, p0, Lcom/lge/view/TouchEventFilter;->reportAction:I

    #@fd
    goto/16 :goto_36

    #@ff
    :cond_ff
    shl-int/lit8 v5, v2, 0x8

    #@101
    or-int/lit8 v5, v5, 0x5

    #@103
    goto :goto_fb

    #@104
    .line 344
    .end local v1           #id:I
    .end local v2           #index:I
    :cond_104
    iget-object v5, p0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@106
    invoke-virtual {v5, p2}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->setAllMask(I)I

    #@109
    .line 345
    iget-object v5, p0, Lcom/lge/view/TouchEventFilter;->reportIDBitsWithoutUpEvent:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@10b
    iget-object v8, p0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@10d
    invoke-virtual {v8}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->getMask()I

    #@110
    move-result v8

    #@111
    invoke-virtual {v5, v8}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->setAllMask(I)I

    #@114
    .line 346
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@117
    move-result v5

    #@118
    iput v5, p0, Lcom/lge/view/TouchEventFilter;->reportAction:I

    #@11a
    goto/16 :goto_36

    #@11c
    .line 354
    :cond_11c
    iput-boolean v7, p0, Lcom/lge/view/TouchEventFilter;->needToSendAdditionalEvent:Z

    #@11e
    .line 355
    iput v7, p0, Lcom/lge/view/TouchEventFilter;->loopCount:I

    #@120
    goto/16 :goto_55

    #@122
    .line 360
    :cond_122
    iget-object v5, p0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@124
    invoke-virtual {v5}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->getMask()I

    #@127
    move-result v5

    #@128
    if-eq v5, v4, :cond_12e

    #@12a
    .line 361
    sget-object v3, Lcom/lge/view/TouchEventFilter$DoAction;->DO_SPLIT:Lcom/lge/view/TouchEventFilter$DoAction;

    #@12c
    goto/16 :goto_63

    #@12e
    .line 363
    :cond_12e
    sget-object v3, Lcom/lge/view/TouchEventFilter$DoAction;->DO_NOTHING:Lcom/lge/view/TouchEventFilter$DoAction;

    #@130
    goto/16 :goto_63

    #@132
    :cond_132
    move-object v5, v3

    #@133
    .line 368
    goto/16 :goto_71
.end method

.method private setReportValue(II)V
    .registers 4
    .parameter "mask"
    .parameter "action"

    #@0
    .prologue
    .line 158
    iget-object v0, p0, Lcom/lge/view/TouchEventFilter;->reportIDBits:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@2
    invoke-virtual {v0, p1}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->setAllMask(I)I

    #@5
    .line 159
    iget-object v0, p0, Lcom/lge/view/TouchEventFilter;->reportIDBitsWithoutUpEvent:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@7
    invoke-virtual {v0, p1}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->setAllMask(I)I

    #@a
    .line 160
    iput p2, p0, Lcom/lge/view/TouchEventFilter;->reportAction:I

    #@c
    .line 161
    return-void
.end method


# virtual methods
.method public addTouchEventFilter(Landroid/view/View;I)V
    .registers 6
    .parameter "view"
    .parameter "option"

    #@0
    .prologue
    .line 104
    and-int/lit8 v0, p2, 0x1

    #@2
    if-eqz v0, :cond_f

    #@4
    .line 105
    iget-object v0, p0, Lcom/lge/view/TouchEventFilter;->mEventFilter:Ljava/util/ArrayList;

    #@6
    const/4 v1, 0x0

    #@7
    new-instance v2, Lcom/lge/view/TouchEventFilter$PalmRejectionFilter;

    #@9
    invoke-direct {v2, p0, p1}, Lcom/lge/view/TouchEventFilter$PalmRejectionFilter;-><init>(Lcom/lge/view/TouchEventFilter;Landroid/view/View;)V

    #@c
    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@f
    .line 106
    :cond_f
    and-int/lit8 v0, p2, 0x2

    #@11
    if-eqz v0, :cond_1e

    #@13
    .line 107
    iget-object v0, p0, Lcom/lge/view/TouchEventFilter;->mEventFilter:Ljava/util/ArrayList;

    #@15
    const/4 v1, 0x1

    #@16
    new-instance v2, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;

    #@18
    invoke-direct {v2, p0, p1}, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;-><init>(Lcom/lge/view/TouchEventFilter;Landroid/view/View;)V

    #@1b
    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@1e
    .line 108
    :cond_1e
    and-int/lit8 v0, p2, 0x4

    #@20
    if-eqz v0, :cond_2d

    #@22
    .line 109
    iget-object v0, p0, Lcom/lge/view/TouchEventFilter;->mEventFilter:Ljava/util/ArrayList;

    #@24
    const/4 v1, 0x2

    #@25
    new-instance v2, Lcom/lge/view/TouchEventFilter$SensitivityFilter;

    #@27
    invoke-direct {v2, p0, p1}, Lcom/lge/view/TouchEventFilter$SensitivityFilter;-><init>(Lcom/lge/view/TouchEventFilter;Landroid/view/View;)V

    #@2a
    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@2d
    .line 110
    :cond_2d
    and-int/lit8 v0, p2, 0x8

    #@2f
    if-eqz v0, :cond_3c

    #@31
    .line 111
    iget-object v0, p0, Lcom/lge/view/TouchEventFilter;->mEventFilter:Ljava/util/ArrayList;

    #@33
    const/4 v1, 0x3

    #@34
    new-instance v2, Lcom/lge/view/TouchEventFilter$PenPalmFilter;

    #@36
    invoke-direct {v2, p0, p1}, Lcom/lge/view/TouchEventFilter$PenPalmFilter;-><init>(Lcom/lge/view/TouchEventFilter;Landroid/view/View;)V

    #@39
    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@3c
    .line 113
    :cond_3c
    iget v0, p0, Lcom/lge/view/TouchEventFilter;->useFilter:I

    #@3e
    or-int/2addr v0, p2

    #@3f
    iput v0, p0, Lcom/lge/view/TouchEventFilter;->useFilter:I

    #@41
    .line 114
    iput-object p1, p0, Lcom/lge/view/TouchEventFilter;->mView:Landroid/view/View;

    #@43
    .line 116
    iget v0, p0, Lcom/lge/view/TouchEventFilter;->DEBUG_LEVEL:I

    #@45
    iget v1, p0, Lcom/lge/view/TouchEventFilter;->useFilter:I

    #@47
    invoke-static {v0, p1, v1}, Lcom/lge/view/TouchEventFilter$PrintLog;->filterStatus(ILandroid/view/View;I)V

    #@4a
    .line 117
    return-void
.end method

.method public dispatchFilteredTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 10
    .parameter "event"

    #@0
    .prologue
    .line 124
    const/4 v2, 0x1

    #@1
    .line 126
    .local v2, handled:Z
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    #@4
    move-result-wide v4

    #@5
    .line 128
    .local v4, start:J
    :cond_5
    const/4 v6, 0x0

    #@6
    iput-boolean v6, p0, Lcom/lge/view/TouchEventFilter;->needToSendAdditionalEvent:Z

    #@8
    .line 129
    invoke-direct {p0, p1}, Lcom/lge/view/TouchEventFilter;->filtering(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@b
    move-result-object v3

    #@c
    .line 131
    .local v3, newEvent:Landroid/view/MotionEvent;
    iget-object v6, p0, Lcom/lge/view/TouchEventFilter;->filteringResult:Lcom/lge/view/TouchEventFilter$DoAction;

    #@e
    sget-object v7, Lcom/lge/view/TouchEventFilter$DoAction;->DO_IGNORE:Lcom/lge/view/TouchEventFilter$DoAction;

    #@10
    if-eq v6, v7, :cond_20

    #@12
    if-eqz v3, :cond_20

    #@14
    .line 132
    iget-object v6, p0, Lcom/lge/view/TouchEventFilter;->mDoDispatchMotionEvent:Lcom/lge/view/TouchEventFilter$DoDispatchMotionEvent;

    #@16
    invoke-interface {v6, v3}, Lcom/lge/view/TouchEventFilter$DoDispatchMotionEvent;->doDispatch(Landroid/view/MotionEvent;)Z

    #@19
    move-result v6

    #@1a
    and-int/2addr v2, v6

    #@1b
    .line 133
    if-eq v3, p1, :cond_20

    #@1d
    .line 134
    invoke-virtual {v3}, Landroid/view/MotionEvent;->recycle()V

    #@20
    .line 136
    :cond_20
    iget-boolean v6, p0, Lcom/lge/view/TouchEventFilter;->needToSendAdditionalEvent:Z

    #@22
    if-nez v6, :cond_5

    #@24
    .line 138
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    #@27
    move-result-wide v0

    #@28
    .line 139
    .local v0, end:J
    iget v6, p0, Lcom/lge/view/TouchEventFilter;->DEBUG_LEVEL:I

    #@2a
    invoke-static {v6, v4, v5, v0, v1}, Lcom/lge/view/TouchEventFilter$PrintLog;->checkTime(IJJ)V

    #@2d
    .line 141
    return v2
.end method
