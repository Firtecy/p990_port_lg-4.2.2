.class Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;
.super Ljava/lang/Object;
.source "TouchEventFilter.java"

# interfaces
.implements Lcom/lge/view/TouchEventFilter$IEventFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/view/TouchEventFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GripSuppressionFilter"
.end annotation


# instance fields
.field private final MAX_VELOCITY:I

.field private final REJECT_PIXEL:I

.field private final USE_POSITION:Z

.field private final USE_VELOCITY:Z

.field private final USE_WIDTH_RATIO:Z

.field private final WIDTH_RATIO:F

.field private act:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

.field private edge:Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;

.field private gripMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field private reportIdBits:I

.field private reportMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

.field final synthetic this$0:Lcom/lge/view/TouchEventFilter;

.field private validXLeft:I

.field private validXRight:I


# direct methods
.method public constructor <init>(Lcom/lge/view/TouchEventFilter;Landroid/view/View;)V
    .registers 6
    .parameter
    .parameter "view"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 638
    iput-object p1, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->this$0:Lcom/lge/view/TouchEventFilter;

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    .line 619
    iput v1, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->REJECT_PIXEL:I

    #@9
    .line 621
    const/high16 v0, 0x4000

    #@b
    iput v0, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->WIDTH_RATIO:F

    #@d
    .line 622
    const/16 v0, 0x1f4

    #@f
    iput v0, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->MAX_VELOCITY:I

    #@11
    .line 624
    iput-boolean v1, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->USE_WIDTH_RATIO:Z

    #@13
    .line 625
    iput-boolean v1, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->USE_POSITION:Z

    #@15
    .line 626
    iput-boolean v1, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->USE_VELOCITY:Z

    #@17
    .line 628
    iput v2, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->validXLeft:I

    #@19
    .line 629
    iput v2, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->validXRight:I

    #@1b
    .line 631
    new-instance v0, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@1d
    invoke-direct {v0}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;-><init>()V

    #@20
    iput-object v0, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->gripMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@22
    .line 632
    new-instance v0, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@24
    invoke-direct {v0}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;-><init>()V

    #@27
    iput-object v0, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->reportMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@29
    .line 634
    new-instance v0, Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;

    #@2b
    invoke-direct {v0, v1}, Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;-><init>(I)V

    #@2e
    iput-object v0, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->edge:Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;

    #@30
    .line 639
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    #@33
    move-result-object v0

    #@34
    iput-object v0, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@36
    .line 640
    invoke-virtual {p0}, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->init()V

    #@39
    .line 641
    return-void
.end method


# virtual methods
.method public filtering(Landroid/view/MotionEvent;I)Z
    .registers 16
    .parameter "event"
    .parameter "tmpMask"

    #@0
    .prologue
    .line 652
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@3
    move-result v0

    #@4
    .line 653
    .local v0, NI:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@7
    move-result v1

    #@8
    .line 655
    .local v1, action:I
    iget-object v10, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->edge:Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;

    #@a
    invoke-virtual {v10}, Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;->getLeftEdgeRegion()I

    #@d
    move-result v10

    #@e
    iput v10, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->validXLeft:I

    #@10
    .line 656
    iget-object v10, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->edge:Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;

    #@12
    invoke-virtual {v10}, Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;->getRightEdgeRegion()I

    #@15
    move-result v10

    #@16
    iput v10, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->validXRight:I

    #@18
    .line 658
    iget-object v10, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->reportMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@1a
    invoke-virtual {v10}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->clearAllMask()I

    #@1d
    .line 660
    iget-object v10, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@1f
    invoke-virtual {v10, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    #@22
    .line 661
    iget-object v6, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@24
    .line 662
    .local v6, vt:Landroid/view/VelocityTracker;
    const/16 v10, 0x3e8

    #@26
    invoke-virtual {v6, v10}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    #@29
    .line 664
    const/4 v10, 0x5

    #@2a
    if-eq v1, v10, :cond_2e

    #@2c
    if-nez v1, :cond_48

    #@2e
    .line 665
    :cond_2e
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@31
    move-result v4

    #@32
    .line 666
    .local v4, index:I
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@35
    move-result v3

    #@36
    .line 667
    .local v3, id:I
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getRawX(I)F

    #@39
    move-result v10

    #@3a
    float-to-int v9, v10

    #@3b
    .line 669
    .local v9, x:I
    iget v10, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->validXLeft:I

    #@3d
    if-lt v9, v10, :cond_43

    #@3f
    iget v10, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->validXRight:I

    #@41
    if-le v9, v10, :cond_48

    #@43
    .line 670
    :cond_43
    iget-object v10, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->gripMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@45
    invoke-virtual {v10, v3}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->setMask(I)I

    #@48
    .line 673
    .end local v3           #id:I
    .end local v4           #index:I
    .end local v9           #x:I
    :cond_48
    const/4 v2, 0x0

    #@49
    .local v2, i:I
    :goto_49
    if-ge v2, v0, :cond_99

    #@4b
    .line 674
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@4e
    move-result v3

    #@4f
    .line 675
    .restart local v3       #id:I
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getRawX(I)F

    #@52
    move-result v10

    #@53
    float-to-int v9, v10

    #@54
    .line 676
    .restart local v9       #x:I
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getToolMajor(I)F

    #@57
    move-result v10

    #@58
    float-to-int v7, v10

    #@59
    .line 677
    .local v7, widthMajor:I
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getToolMinor(I)F

    #@5c
    move-result v10

    #@5d
    float-to-int v8, v10

    #@5e
    .line 678
    .local v8, widthMinor:I
    invoke-virtual {v6, v3}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    #@61
    move-result v10

    #@62
    float-to-int v10, v10

    #@63
    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    #@66
    move-result v5

    #@67
    .line 680
    .local v5, velX:I
    iget-object v10, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->gripMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@69
    invoke-virtual {v10, v3}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->checkMask(I)Z

    #@6c
    move-result v10

    #@6d
    if-eqz v10, :cond_89

    #@6f
    .line 681
    int-to-float v10, v7

    #@70
    int-to-float v11, v8

    #@71
    const/high16 v12, 0x4000

    #@73
    mul-float/2addr v11, v12

    #@74
    cmpl-float v10, v10, v11

    #@76
    if-gez v10, :cond_80

    #@78
    iget v10, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->validXLeft:I

    #@7a
    if-lt v9, v10, :cond_80

    #@7c
    iget v10, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->validXRight:I

    #@7e
    if-le v9, v10, :cond_84

    #@80
    :cond_80
    const/16 v10, 0x1f4

    #@82
    if-lt v5, v10, :cond_89

    #@84
    .line 682
    :cond_84
    iget-object v10, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->gripMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@86
    invoke-virtual {v10, v3}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->clearMask(I)I

    #@89
    .line 685
    :cond_89
    iget-object v10, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->gripMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@8b
    invoke-virtual {v10, v3}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->checkMask(I)Z

    #@8e
    move-result v10

    #@8f
    if-nez v10, :cond_96

    #@91
    .line 686
    iget-object v10, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->reportMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@93
    invoke-virtual {v10, v3}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->setMask(I)I

    #@96
    .line 673
    :cond_96
    add-int/lit8 v2, v2, 0x1

    #@98
    goto :goto_49

    #@99
    .line 689
    .end local v3           #id:I
    .end local v5           #velX:I
    .end local v7           #widthMajor:I
    .end local v8           #widthMinor:I
    .end local v9           #x:I
    :cond_99
    const/4 v10, 0x6

    #@9a
    if-eq v1, v10, :cond_9f

    #@9c
    const/4 v10, 0x1

    #@9d
    if-ne v1, v10, :cond_b1

    #@9f
    .line 690
    :cond_9f
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@a2
    move-result v4

    #@a3
    .line 691
    .restart local v4       #index:I
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@a6
    move-result v3

    #@a7
    .line 692
    .restart local v3       #id:I
    iget-object v10, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->gripMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@a9
    invoke-virtual {v10, v3}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->clearMask(I)I

    #@ac
    .line 693
    iget-object v10, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->reportMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@ae
    invoke-virtual {v10, v3}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->clearMask(I)I

    #@b1
    .line 696
    .end local v3           #id:I
    .end local v4           #index:I
    :cond_b1
    iget-object v10, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->reportMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@b3
    invoke-virtual {v10}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->getMask()I

    #@b6
    move-result v10

    #@b7
    iput v10, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->reportIdBits:I

    #@b9
    .line 697
    const/4 v10, 0x1

    #@ba
    return v10
.end method

.method public getAct()Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;
    .registers 2

    #@0
    .prologue
    .line 701
    iget-object v0, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->act:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@2
    return-object v0
.end method

.method public getReportMask()I
    .registers 2

    #@0
    .prologue
    .line 705
    iget v0, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->reportIdBits:I

    #@2
    return v0
.end method

.method public init()V
    .registers 2

    #@0
    .prologue
    .line 644
    sget-object v0, Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;->NONE:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@2
    iput-object v0, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->act:Lcom/lge/view/TouchEventFilter$IEventFilter$ReturnAct;

    #@4
    .line 645
    const/4 v0, 0x0

    #@5
    iput v0, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->reportIdBits:I

    #@7
    .line 646
    iget-object v0, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->gripMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@9
    invoke-virtual {v0}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->clearAllMask()I

    #@c
    .line 647
    iget-object v0, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->reportMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@e
    invoke-virtual {v0}, Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;->clearAllMask()I

    #@11
    .line 648
    iget-object v0, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@13
    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    #@16
    .line 649
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 709
    new-instance v0, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v1, "GripSuppressionFilter: valid"

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v0

    #@c
    iget-object v1, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->edge:Lcom/lge/view/TouchEventFilter$IEventFilter$FindEdgeRegion;

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v0

    #@12
    const-string v1, " grip"

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    iget-object v1, p0, Lcom/lge/view/TouchEventFilter$GripSuppressionFilter;->gripMask:Lcom/lge/view/TouchEventFilter$IEventFilter$TouchCheckMask;

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    const-string v1, " USE_WIDTH["

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v0

    #@24
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@27
    move-result-object v0

    #@28
    const-string v1, "] USE_POSITION["

    #@2a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v0

    #@2e
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@31
    move-result-object v0

    #@32
    const-string v1, "] USE_VELOCITY["

    #@34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v0

    #@38
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v0

    #@3c
    const-string v1, "]"

    #@3e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v0

    #@42
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v0

    #@46
    return-object v0
.end method
