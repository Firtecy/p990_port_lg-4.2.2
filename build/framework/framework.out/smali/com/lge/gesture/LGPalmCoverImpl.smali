.class Lcom/lge/gesture/LGPalmCoverImpl;
.super Lcom/lge/gesture/LGPalmCover;
.source "LGPalmCoverImpl.java"


# static fields
.field private static final INTENT_TOUCH_PALM_COVER:Ljava/lang/String; = "com.lge.android.intent.action.TOUCH_PALM_COVER"


# instance fields
.field private final TIMELIMIT4GESTURE:I

.field mContext:Landroid/content/Context;

.field private mFirstPressTime:J

.field private mPalmCoverPressed:Z

.field private mPalmCovered:Z


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 27
    invoke-direct {p0}, Lcom/lge/gesture/LGPalmCover;-><init>()V

    #@4
    .line 16
    const/4 v0, 0x0

    #@5
    iput-object v0, p0, Lcom/lge/gesture/LGPalmCoverImpl;->mContext:Landroid/content/Context;

    #@7
    .line 20
    const/16 v0, 0x1f3

    #@9
    iput v0, p0, Lcom/lge/gesture/LGPalmCoverImpl;->TIMELIMIT4GESTURE:I

    #@b
    .line 22
    iput-boolean v1, p0, Lcom/lge/gesture/LGPalmCoverImpl;->mPalmCoverPressed:Z

    #@d
    .line 23
    iput-boolean v1, p0, Lcom/lge/gesture/LGPalmCoverImpl;->mPalmCovered:Z

    #@f
    .line 24
    const-wide/16 v0, 0x0

    #@11
    iput-wide v0, p0, Lcom/lge/gesture/LGPalmCoverImpl;->mFirstPressTime:J

    #@13
    .line 28
    iput-object p1, p0, Lcom/lge/gesture/LGPalmCoverImpl;->mContext:Landroid/content/Context;

    #@15
    .line 29
    return-void
.end method


# virtual methods
.method public PlayWithInputEvent(Landroid/view/InputEvent;)V
    .registers 14
    .parameter "event"

    #@0
    .prologue
    const-wide/16 v10, 0x0

    #@2
    const/4 v7, 0x0

    #@3
    const/4 v9, 0x1

    #@4
    .line 33
    move-object v4, p1

    #@5
    check-cast v4, Landroid/view/MotionEvent;

    #@7
    .line 34
    .local v4, motionEvent:Landroid/view/MotionEvent;
    invoke-virtual {v4}, Landroid/view/MotionEvent;->getAction()I

    #@a
    move-result v0

    #@b
    .line 36
    .local v0, action:I
    const/4 v5, 0x2

    #@c
    if-ne v0, v5, :cond_32

    #@e
    iget-boolean v5, p0, Lcom/lge/gesture/LGPalmCoverImpl;->mPalmCoverPressed:Z

    #@10
    if-nez v5, :cond_32

    #@12
    .line 37
    const/4 v3, 0x0

    #@13
    .local v3, i:I
    :goto_13
    invoke-virtual {v4}, Landroid/view/MotionEvent;->getPointerCount()I

    #@16
    move-result v5

    #@17
    if-ge v3, v5, :cond_32

    #@19
    .line 38
    invoke-virtual {v4, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    #@1c
    move-result v5

    #@1d
    const/4 v6, 0x6

    #@1e
    if-ne v5, v6, :cond_2f

    #@20
    .line 39
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@23
    move-result-wide v5

    #@24
    iput-wide v5, p0, Lcom/lge/gesture/LGPalmCoverImpl;->mFirstPressTime:J

    #@26
    .line 40
    iput-boolean v9, p0, Lcom/lge/gesture/LGPalmCoverImpl;->mPalmCoverPressed:Z

    #@28
    .line 41
    const-string v5, "PALMCOVER"

    #@2a
    const-string v6, "***** Recognize palm-cover press *****"

    #@2c
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 37
    :cond_2f
    add-int/lit8 v3, v3, 0x1

    #@31
    goto :goto_13

    #@32
    .line 46
    .end local v3           #i:I
    :cond_32
    if-eq v0, v9, :cond_37

    #@34
    const/4 v5, 0x3

    #@35
    if-ne v0, v5, :cond_3d

    #@37
    .line 47
    :cond_37
    iput-boolean v7, p0, Lcom/lge/gesture/LGPalmCoverImpl;->mPalmCoverPressed:Z

    #@39
    .line 48
    iput-boolean v7, p0, Lcom/lge/gesture/LGPalmCoverImpl;->mPalmCovered:Z

    #@3b
    .line 49
    iput-wide v10, p0, Lcom/lge/gesture/LGPalmCoverImpl;->mFirstPressTime:J

    #@3d
    .line 52
    :cond_3d
    iget-boolean v5, p0, Lcom/lge/gesture/LGPalmCoverImpl;->mPalmCovered:Z

    #@3f
    if-eqz v5, :cond_49

    #@41
    .line 53
    const-string v5, "PALMCOVER"

    #@43
    const-string v6, "***** Palm Cover is running *****"

    #@45
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 71
    :cond_48
    :goto_48
    return-void

    #@49
    .line 57
    :cond_49
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@4c
    move-result-wide v1

    #@4d
    .line 59
    .local v1, currTime:J
    iget-boolean v5, p0, Lcom/lge/gesture/LGPalmCoverImpl;->mPalmCoverPressed:Z

    #@4f
    if-eqz v5, :cond_70

    #@51
    iget-boolean v5, p0, Lcom/lge/gesture/LGPalmCoverImpl;->mPalmCovered:Z

    #@53
    if-nez v5, :cond_70

    #@55
    .line 60
    iget-wide v5, p0, Lcom/lge/gesture/LGPalmCoverImpl;->mFirstPressTime:J

    #@57
    sub-long v5, v1, v5

    #@59
    cmp-long v5, v5, v10

    #@5b
    if-ltz v5, :cond_70

    #@5d
    iget-wide v5, p0, Lcom/lge/gesture/LGPalmCoverImpl;->mFirstPressTime:J

    #@5f
    sub-long v5, v1, v5

    #@61
    const-wide/16 v7, 0x1f3

    #@63
    cmp-long v5, v5, v7

    #@65
    if-ltz v5, :cond_70

    #@67
    .line 62
    iput-boolean v9, p0, Lcom/lge/gesture/LGPalmCoverImpl;->mPalmCovered:Z

    #@69
    .line 63
    const-string v5, "PALMCOVER"

    #@6b
    const-string v6, "***** Palm Cover Recognized"

    #@6d
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@70
    .line 67
    :cond_70
    iget-boolean v5, p0, Lcom/lge/gesture/LGPalmCoverImpl;->mPalmCovered:Z

    #@72
    if-eqz v5, :cond_48

    #@74
    .line 68
    iget-object v5, p0, Lcom/lge/gesture/LGPalmCoverImpl;->mContext:Landroid/content/Context;

    #@76
    const-string v6, "com.lge.android.intent.action.TOUCH_PALM_COVER"

    #@78
    invoke-static {v5, v6}, Lcom/lge/gesture/LgeGestureBasicUtil;->broadcastIntent(Landroid/content/Context;Ljava/lang/String;)V

    #@7b
    goto :goto_48
.end method
