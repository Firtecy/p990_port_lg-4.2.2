.class public final enum Lcom/lge/gesture/LgeGestureFactory$GestureType;
.super Ljava/lang/Enum;
.source "LgeGestureFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/gesture/LgeGestureFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "GestureType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/lge/gesture/LgeGestureFactory$GestureType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/lge/gesture/LgeGestureFactory$GestureType;

.field public static final enum PALMCOVER:Lcom/lge/gesture/LgeGestureFactory$GestureType;

.field public static final enum PALMSWIPE:Lcom/lge/gesture/LgeGestureFactory$GestureType;

.field public static final enum SLIDEASIDE:Lcom/lge/gesture/LgeGestureFactory$GestureType;

.field public static final enum TILT:Lcom/lge/gesture/LgeGestureFactory$GestureType;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/4 v4, 0x2

    #@2
    const/4 v3, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    .line 15
    new-instance v0, Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@6
    const-string v1, "SLIDEASIDE"

    #@8
    invoke-direct {v0, v1, v2}, Lcom/lge/gesture/LgeGestureFactory$GestureType;-><init>(Ljava/lang/String;I)V

    #@b
    sput-object v0, Lcom/lge/gesture/LgeGestureFactory$GestureType;->SLIDEASIDE:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@d
    new-instance v0, Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@f
    const-string v1, "PALMSWIPE"

    #@11
    invoke-direct {v0, v1, v3}, Lcom/lge/gesture/LgeGestureFactory$GestureType;-><init>(Ljava/lang/String;I)V

    #@14
    sput-object v0, Lcom/lge/gesture/LgeGestureFactory$GestureType;->PALMSWIPE:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@16
    new-instance v0, Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@18
    const-string v1, "PALMCOVER"

    #@1a
    invoke-direct {v0, v1, v4}, Lcom/lge/gesture/LgeGestureFactory$GestureType;-><init>(Ljava/lang/String;I)V

    #@1d
    sput-object v0, Lcom/lge/gesture/LgeGestureFactory$GestureType;->PALMCOVER:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@1f
    new-instance v0, Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@21
    const-string v1, "TILT"

    #@23
    invoke-direct {v0, v1, v5}, Lcom/lge/gesture/LgeGestureFactory$GestureType;-><init>(Ljava/lang/String;I)V

    #@26
    sput-object v0, Lcom/lge/gesture/LgeGestureFactory$GestureType;->TILT:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@28
    .line 14
    const/4 v0, 0x4

    #@29
    new-array v0, v0, [Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@2b
    sget-object v1, Lcom/lge/gesture/LgeGestureFactory$GestureType;->SLIDEASIDE:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@2d
    aput-object v1, v0, v2

    #@2f
    sget-object v1, Lcom/lge/gesture/LgeGestureFactory$GestureType;->PALMSWIPE:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@31
    aput-object v1, v0, v3

    #@33
    sget-object v1, Lcom/lge/gesture/LgeGestureFactory$GestureType;->PALMCOVER:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@35
    aput-object v1, v0, v4

    #@37
    sget-object v1, Lcom/lge/gesture/LgeGestureFactory$GestureType;->TILT:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@39
    aput-object v1, v0, v5

    #@3b
    sput-object v0, Lcom/lge/gesture/LgeGestureFactory$GestureType;->$VALUES:[Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@3d
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/lge/gesture/LgeGestureFactory$GestureType;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 14
    const-class v0, Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/lge/gesture/LgeGestureFactory$GestureType;
    .registers 1

    #@0
    .prologue
    .line 14
    sget-object v0, Lcom/lge/gesture/LgeGestureFactory$GestureType;->$VALUES:[Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@2
    invoke-virtual {v0}, [Lcom/lge/gesture/LgeGestureFactory$GestureType;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@8
    return-object v0
.end method
