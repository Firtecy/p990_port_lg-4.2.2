.class public Lcom/lge/gesture/LgeGestureFactory;
.super Ljava/lang/Object;
.source "LgeGestureFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/gesture/LgeGestureFactory$GestureType;
    }
.end annotation


# static fields
.field public static final PALMCOVER:Lcom/lge/gesture/LgeGestureFactory$GestureType;

.field public static final PALMSWIPE:Lcom/lge/gesture/LgeGestureFactory$GestureType;

.field public static final SLIDEASIDE:Lcom/lge/gesture/LgeGestureFactory$GestureType;

.field public static final TILT:Lcom/lge/gesture/LgeGestureFactory$GestureType;

.field private static mPalmCover:Lcom/lge/gesture/LgeGesture;

.field private static mPalmSwipe:Lcom/lge/gesture/LgeGesture;

.field private static mSlideAside:Lcom/lge/gesture/LgeGesture;

.field private static mTilt:Lcom/lge/gesture/LgeGesture;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 7
    sput-object v0, Lcom/lge/gesture/LgeGestureFactory;->mSlideAside:Lcom/lge/gesture/LgeGesture;

    #@3
    .line 8
    sput-object v0, Lcom/lge/gesture/LgeGestureFactory;->mPalmSwipe:Lcom/lge/gesture/LgeGesture;

    #@5
    .line 9
    sput-object v0, Lcom/lge/gesture/LgeGestureFactory;->mPalmCover:Lcom/lge/gesture/LgeGesture;

    #@7
    .line 10
    sput-object v0, Lcom/lge/gesture/LgeGestureFactory;->mTilt:Lcom/lge/gesture/LgeGesture;

    #@9
    .line 18
    sget-object v0, Lcom/lge/gesture/LgeGestureFactory$GestureType;->SLIDEASIDE:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@b
    sput-object v0, Lcom/lge/gesture/LgeGestureFactory;->SLIDEASIDE:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@d
    .line 19
    sget-object v0, Lcom/lge/gesture/LgeGestureFactory$GestureType;->PALMSWIPE:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@f
    sput-object v0, Lcom/lge/gesture/LgeGestureFactory;->PALMSWIPE:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@11
    .line 20
    sget-object v0, Lcom/lge/gesture/LgeGestureFactory$GestureType;->PALMCOVER:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@13
    sput-object v0, Lcom/lge/gesture/LgeGestureFactory;->PALMCOVER:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@15
    .line 21
    sget-object v0, Lcom/lge/gesture/LgeGestureFactory$GestureType;->TILT:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@17
    sput-object v0, Lcom/lge/gesture/LgeGestureFactory;->TILT:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@19
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 12
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static getInstance(Landroid/content/Context;Lcom/lge/gesture/LgeGestureFactory$GestureType;)Lcom/lge/gesture/LgeGesture;
    .registers 3
    .parameter "context"
    .parameter "type"

    #@0
    .prologue
    .line 24
    sget-object v0, Lcom/lge/gesture/LgeGestureFactory$GestureType;->SLIDEASIDE:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@2
    if-ne p1, v0, :cond_15

    #@4
    .line 25
    sget-object v0, Lcom/lge/gesture/LgeGestureFactory;->mSlideAside:Lcom/lge/gesture/LgeGesture;

    #@6
    if-eqz v0, :cond_b

    #@8
    .line 26
    sget-object v0, Lcom/lge/gesture/LgeGestureFactory;->mSlideAside:Lcom/lge/gesture/LgeGesture;

    #@a
    .line 49
    :goto_a
    return-object v0

    #@b
    .line 28
    :cond_b
    new-instance v0, Lcom/lge/gesture/LGSlideAsideImpl;

    #@d
    invoke-direct {v0, p0}, Lcom/lge/gesture/LGSlideAsideImpl;-><init>(Landroid/content/Context;)V

    #@10
    sput-object v0, Lcom/lge/gesture/LgeGestureFactory;->mSlideAside:Lcom/lge/gesture/LgeGesture;

    #@12
    .line 29
    sget-object v0, Lcom/lge/gesture/LgeGestureFactory;->mSlideAside:Lcom/lge/gesture/LgeGesture;

    #@14
    goto :goto_a

    #@15
    .line 30
    :cond_15
    sget-object v0, Lcom/lge/gesture/LgeGestureFactory$GestureType;->PALMSWIPE:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@17
    if-ne p1, v0, :cond_2a

    #@19
    .line 31
    sget-object v0, Lcom/lge/gesture/LgeGestureFactory;->mPalmSwipe:Lcom/lge/gesture/LgeGesture;

    #@1b
    if-eqz v0, :cond_20

    #@1d
    .line 32
    sget-object v0, Lcom/lge/gesture/LgeGestureFactory;->mPalmSwipe:Lcom/lge/gesture/LgeGesture;

    #@1f
    goto :goto_a

    #@20
    .line 34
    :cond_20
    new-instance v0, Lcom/lge/gesture/LGPalmSwipeImpl;

    #@22
    invoke-direct {v0, p0}, Lcom/lge/gesture/LGPalmSwipeImpl;-><init>(Landroid/content/Context;)V

    #@25
    sput-object v0, Lcom/lge/gesture/LgeGestureFactory;->mPalmSwipe:Lcom/lge/gesture/LgeGesture;

    #@27
    .line 35
    sget-object v0, Lcom/lge/gesture/LgeGestureFactory;->mPalmSwipe:Lcom/lge/gesture/LgeGesture;

    #@29
    goto :goto_a

    #@2a
    .line 36
    :cond_2a
    sget-object v0, Lcom/lge/gesture/LgeGestureFactory$GestureType;->PALMCOVER:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@2c
    if-ne p1, v0, :cond_3f

    #@2e
    .line 37
    sget-object v0, Lcom/lge/gesture/LgeGestureFactory;->mPalmCover:Lcom/lge/gesture/LgeGesture;

    #@30
    if-eqz v0, :cond_35

    #@32
    .line 38
    sget-object v0, Lcom/lge/gesture/LgeGestureFactory;->mPalmCover:Lcom/lge/gesture/LgeGesture;

    #@34
    goto :goto_a

    #@35
    .line 40
    :cond_35
    new-instance v0, Lcom/lge/gesture/LGPalmCoverImpl;

    #@37
    invoke-direct {v0, p0}, Lcom/lge/gesture/LGPalmCoverImpl;-><init>(Landroid/content/Context;)V

    #@3a
    sput-object v0, Lcom/lge/gesture/LgeGestureFactory;->mPalmCover:Lcom/lge/gesture/LgeGesture;

    #@3c
    .line 41
    sget-object v0, Lcom/lge/gesture/LgeGestureFactory;->mPalmCover:Lcom/lge/gesture/LgeGesture;

    #@3e
    goto :goto_a

    #@3f
    .line 42
    :cond_3f
    sget-object v0, Lcom/lge/gesture/LgeGestureFactory$GestureType;->TILT:Lcom/lge/gesture/LgeGestureFactory$GestureType;

    #@41
    if-ne p1, v0, :cond_54

    #@43
    .line 43
    sget-object v0, Lcom/lge/gesture/LgeGestureFactory;->mTilt:Lcom/lge/gesture/LgeGesture;

    #@45
    if-eqz v0, :cond_4a

    #@47
    .line 44
    sget-object v0, Lcom/lge/gesture/LgeGestureFactory;->mTilt:Lcom/lge/gesture/LgeGesture;

    #@49
    goto :goto_a

    #@4a
    .line 46
    :cond_4a
    new-instance v0, Lcom/lge/gesture/LGTiltImpl;

    #@4c
    invoke-direct {v0, p0}, Lcom/lge/gesture/LGTiltImpl;-><init>(Landroid/content/Context;)V

    #@4f
    sput-object v0, Lcom/lge/gesture/LgeGestureFactory;->mTilt:Lcom/lge/gesture/LgeGesture;

    #@51
    .line 47
    sget-object v0, Lcom/lge/gesture/LgeGestureFactory;->mTilt:Lcom/lge/gesture/LgeGesture;

    #@53
    goto :goto_a

    #@54
    .line 49
    :cond_54
    const/4 v0, 0x0

    #@55
    goto :goto_a
.end method
