.class Lcom/lge/gesture/LGSlideAsideImpl;
.super Lcom/lge/gesture/LGSlideaside;
.source "LGSlideAsideImpl.java"


# static fields
.field private static final ACTION_SLIDE_ASIDE_IN:Ljava/lang/String; = "com.lge.slideaside.pin.in"

.field private static final ACTION_SLIDE_ASIDE_OUT:Ljava/lang/String; = "com.lge.slideaside.pin.out"

.field private static final INVALID_POINTER:I = -0x1


# instance fields
.field private final FINGERCOUNT:I

.field private final LEFTSIDE:I

.field private final RIGHTSIDE:I

.field private SLIDEASIDE_DRAGSLOP:F

.field private SLIDEASIE_VERTICAL_DRAGSLOP:F

.field private final WAITFORPRESS:I

.field private final WAITSIDE:I

.field final configuration:Landroid/view/ViewConfiguration;

.field private mContext:Landroid/content/Context;

.field private final mCurrentCoodrinateX:[F

.field private final mCurrentCoodrinateY:[F

.field private mDirection:I

.field private mDirectionChange:Z

.field private final mFirstPointerX:[F

.field private final mFirstPointerY:[F

.field private mFirstTime:J

.field private mMaximumVelocity:I

.field private mMinimumVelocity:I

.field private mReadyToSubmitEvent:Z

.field private mSlideAside:Z

.field private mSlideasideMove:Z

.field private mTouchSlop:I

.field private mVelocityScale:F

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field private final mXdpi:F

.field private final mYdpi:F

.field overTouchSlop:Z


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "Context"

    #@0
    .prologue
    const/high16 v3, 0x4120

    #@2
    const/4 v2, 0x3

    #@3
    const/4 v1, 0x0

    #@4
    .line 55
    invoke-direct {p0}, Lcom/lge/gesture/LGSlideaside;-><init>()V

    #@7
    .line 18
    const/4 v0, -0x1

    #@8
    iput v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->RIGHTSIDE:I

    #@a
    .line 19
    const/4 v0, 0x1

    #@b
    iput v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->LEFTSIDE:I

    #@d
    .line 20
    iput v1, p0, Lcom/lge/gesture/LGSlideAsideImpl;->WAITSIDE:I

    #@f
    .line 21
    iput v2, p0, Lcom/lge/gesture/LGSlideAsideImpl;->FINGERCOUNT:I

    #@11
    .line 22
    const/16 v0, 0x1ea

    #@13
    iput v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->WAITFORPRESS:I

    #@15
    .line 24
    iput-boolean v1, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mReadyToSubmitEvent:Z

    #@17
    .line 25
    iput-boolean v1, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mSlideAside:Z

    #@19
    .line 26
    iput-boolean v1, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mDirectionChange:Z

    #@1b
    .line 27
    iput-boolean v1, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mSlideasideMove:Z

    #@1d
    .line 28
    iput-boolean v1, p0, Lcom/lge/gesture/LGSlideAsideImpl;->overTouchSlop:Z

    #@1f
    .line 30
    new-array v0, v2, [F

    #@21
    iput-object v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mFirstPointerX:[F

    #@23
    .line 31
    new-array v0, v2, [F

    #@25
    iput-object v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mFirstPointerY:[F

    #@27
    .line 32
    new-array v0, v2, [F

    #@29
    iput-object v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mCurrentCoodrinateX:[F

    #@2b
    .line 33
    new-array v0, v2, [F

    #@2d
    iput-object v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mCurrentCoodrinateY:[F

    #@2f
    .line 37
    iput v1, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mDirection:I

    #@31
    .line 52
    const/high16 v0, 0x3f80

    #@33
    iput v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mVelocityScale:F

    #@35
    .line 56
    iput-object p1, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mContext:Landroid/content/Context;

    #@37
    .line 57
    iget-object v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mContext:Landroid/content/Context;

    #@39
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3c
    move-result-object v0

    #@3d
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@40
    move-result-object v0

    #@41
    iget v0, v0, Landroid/util/DisplayMetrics;->xdpi:F

    #@43
    iput v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mXdpi:F

    #@45
    .line 58
    iget-object v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mContext:Landroid/content/Context;

    #@47
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@4a
    move-result-object v0

    #@4b
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@4e
    move-result-object v0

    #@4f
    iget v0, v0, Landroid/util/DisplayMetrics;->ydpi:F

    #@51
    iput v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mYdpi:F

    #@53
    .line 59
    iget v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mXdpi:F

    #@55
    invoke-static {v0}, Lcom/lge/gesture/LgeGestureBasicUtil;->getPixelPerMm(F)F

    #@58
    move-result v0

    #@59
    mul-float/2addr v0, v3

    #@5a
    iput v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->SLIDEASIDE_DRAGSLOP:F

    #@5c
    .line 60
    iget v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mYdpi:F

    #@5e
    invoke-static {v0}, Lcom/lge/gesture/LgeGestureBasicUtil;->getPixelPerMm(F)F

    #@61
    move-result v0

    #@62
    mul-float/2addr v0, v3

    #@63
    iput v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->SLIDEASIE_VERTICAL_DRAGSLOP:F

    #@65
    .line 61
    iget-object v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mContext:Landroid/content/Context;

    #@67
    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@6a
    move-result-object v0

    #@6b
    iput-object v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->configuration:Landroid/view/ViewConfiguration;

    #@6d
    .line 62
    iget-object v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->configuration:Landroid/view/ViewConfiguration;

    #@6f
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    #@72
    move-result v0

    #@73
    iput v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mTouchSlop:I

    #@75
    .line 63
    iget-object v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->configuration:Landroid/view/ViewConfiguration;

    #@77
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    #@7a
    move-result v0

    #@7b
    iput v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mMinimumVelocity:I

    #@7d
    .line 64
    iget-object v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->configuration:Landroid/view/ViewConfiguration;

    #@7f
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    #@82
    move-result v0

    #@83
    iput v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mMaximumVelocity:I

    #@85
    .line 65
    return-void
.end method


# virtual methods
.method public PlayWithInputEvent(Landroid/view/InputEvent;)V
    .registers 13
    .parameter "event"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v10, 0x1

    #@2
    const/4 v9, 0x3

    #@3
    .line 207
    move-object v4, p1

    #@4
    check-cast v4, Landroid/view/MotionEvent;

    #@6
    .line 208
    .local v4, motionEvent:Landroid/view/MotionEvent;
    invoke-virtual {v4}, Landroid/view/MotionEvent;->getAction()I

    #@9
    move-result v0

    #@a
    .line 210
    .local v0, action:I
    invoke-virtual {p0}, Lcom/lge/gesture/LGSlideAsideImpl;->initVelocityTrackerIfNotExists()V

    #@d
    .line 211
    iget-object v5, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@f
    invoke-virtual {v5, v4}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    #@12
    .line 213
    invoke-virtual {v4}, Landroid/view/MotionEvent;->getPointerCount()I

    #@15
    move-result v5

    #@16
    if-le v5, v9, :cond_1a

    #@18
    .line 214
    iput-boolean v10, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mDirectionChange:Z

    #@1a
    .line 219
    :cond_1a
    if-nez v0, :cond_22

    #@1c
    .line 220
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@1f
    move-result-wide v5

    #@20
    iput-wide v5, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mFirstTime:J

    #@22
    .line 223
    :cond_22
    if-eq v0, v10, :cond_26

    #@24
    if-ne v0, v9, :cond_47

    #@26
    .line 224
    :cond_26
    iget-boolean v5, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mDirectionChange:Z

    #@28
    if-nez v5, :cond_3f

    #@2a
    iget-boolean v5, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mSlideAside:Z

    #@2c
    if-nez v5, :cond_3f

    #@2e
    iget-boolean v5, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mSlideasideMove:Z

    #@30
    if-eqz v5, :cond_3f

    #@32
    .line 225
    invoke-virtual {p0, v4}, Lcom/lge/gesture/LGSlideAsideImpl;->onFlick(Landroid/view/MotionEvent;)Z

    #@35
    move-result v5

    #@36
    iput-boolean v5, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mSlideAside:Z

    #@38
    .line 226
    iget-boolean v5, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mSlideAside:Z

    #@3a
    if-eqz v5, :cond_3f

    #@3c
    .line 227
    invoke-virtual {p0}, Lcom/lge/gesture/LGSlideAsideImpl;->runSlideAside()V

    #@3f
    .line 231
    :cond_3f
    iput-boolean v7, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mSlideAside:Z

    #@41
    .line 232
    iput-boolean v7, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mDirectionChange:Z

    #@43
    .line 233
    invoke-virtual {p0}, Lcom/lge/gesture/LGSlideAsideImpl;->initialize()V

    #@46
    .line 294
    :cond_46
    :goto_46
    return-void

    #@47
    .line 238
    :cond_47
    iget-boolean v5, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mSlideAside:Z

    #@49
    if-nez v5, :cond_46

    #@4b
    .line 248
    invoke-virtual {v4}, Landroid/view/MotionEvent;->getPointerCount()I

    #@4e
    move-result v5

    #@4f
    if-ne v5, v9, :cond_93

    #@51
    iget-boolean v5, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mReadyToSubmitEvent:Z

    #@53
    if-nez v5, :cond_93

    #@55
    iget-boolean v5, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mSlideAside:Z

    #@57
    if-nez v5, :cond_93

    #@59
    iget-boolean v5, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mDirectionChange:Z

    #@5b
    if-nez v5, :cond_93

    #@5d
    .line 249
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@60
    move-result-wide v1

    #@61
    .line 250
    .local v1, currTime:J
    iget-wide v5, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mFirstTime:J

    #@63
    sub-long v5, v1, v5

    #@65
    const-wide/16 v7, 0x0

    #@67
    cmp-long v5, v5, v7

    #@69
    if-ltz v5, :cond_93

    #@6b
    iget-wide v5, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mFirstTime:J

    #@6d
    sub-long v5, v1, v5

    #@6f
    const-wide/16 v7, 0x1ea

    #@71
    cmp-long v5, v5, v7

    #@73
    if-gtz v5, :cond_93

    #@75
    .line 251
    invoke-virtual {v4}, Landroid/view/MotionEvent;->getPointerCount()I

    #@78
    move-result v5

    #@79
    if-ne v5, v9, :cond_93

    #@7b
    .line 252
    iput-boolean v10, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mReadyToSubmitEvent:Z

    #@7d
    .line 253
    const/4 v3, 0x0

    #@7e
    .local v3, i:I
    :goto_7e
    if-ge v3, v9, :cond_46

    #@80
    .line 254
    iget-object v5, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mFirstPointerX:[F

    #@82
    invoke-virtual {v4, v3}, Landroid/view/MotionEvent;->getX(I)F

    #@85
    move-result v6

    #@86
    aput v6, v5, v3

    #@88
    .line 255
    iget-object v5, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mFirstPointerY:[F

    #@8a
    invoke-virtual {v4, v3}, Landroid/view/MotionEvent;->getY(I)F

    #@8d
    move-result v6

    #@8e
    aput v6, v5, v3

    #@90
    .line 253
    add-int/lit8 v3, v3, 0x1

    #@92
    goto :goto_7e

    #@93
    .line 268
    .end local v1           #currTime:J
    .end local v3           #i:I
    :cond_93
    iget-boolean v5, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mReadyToSubmitEvent:Z

    #@95
    if-eqz v5, :cond_ab

    #@97
    iget-boolean v5, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mSlideAside:Z

    #@99
    if-nez v5, :cond_ab

    #@9b
    iget-boolean v5, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mDirectionChange:Z

    #@9d
    if-nez v5, :cond_ab

    #@9f
    .line 269
    invoke-virtual {v4}, Landroid/view/MotionEvent;->getPointerCount()I

    #@a2
    move-result v5

    #@a3
    if-ne v5, v9, :cond_b3

    #@a5
    .line 270
    invoke-virtual {p0, v4}, Lcom/lge/gesture/LGSlideAsideImpl;->checkTouchSlopDirection(Landroid/view/MotionEvent;)Z

    #@a8
    move-result v5

    #@a9
    iput-boolean v5, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mSlideAside:Z

    #@ab
    .line 291
    :cond_ab
    :goto_ab
    iget-boolean v5, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mSlideAside:Z

    #@ad
    if-eqz v5, :cond_46

    #@af
    .line 292
    invoke-virtual {p0}, Lcom/lge/gesture/LGSlideAsideImpl;->runSlideAside()V

    #@b2
    goto :goto_46

    #@b3
    .line 271
    :cond_b3
    invoke-virtual {v4}, Landroid/view/MotionEvent;->getPointerCount()I

    #@b6
    move-result v5

    #@b7
    if-ge v5, v9, :cond_ab

    #@b9
    .line 273
    iget-boolean v5, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mSlideasideMove:Z

    #@bb
    if-eqz v5, :cond_cc

    #@bd
    .line 274
    invoke-virtual {p0, v4}, Lcom/lge/gesture/LGSlideAsideImpl;->onFlick(Landroid/view/MotionEvent;)Z

    #@c0
    move-result v5

    #@c1
    iput-boolean v5, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mSlideAside:Z

    #@c3
    .line 275
    iget-boolean v5, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mSlideAside:Z

    #@c5
    if-eqz v5, :cond_cc

    #@c7
    .line 276
    invoke-virtual {p0}, Lcom/lge/gesture/LGSlideAsideImpl;->runSlideAside()V

    #@ca
    goto/16 :goto_46

    #@cc
    .line 281
    :cond_cc
    iput-boolean v10, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mDirectionChange:Z

    #@ce
    .line 282
    invoke-virtual {p0}, Lcom/lge/gesture/LGSlideAsideImpl;->initialize()V

    #@d1
    goto :goto_ab
.end method

.method checkTouchSlopDirection(Landroid/view/MotionEvent;)Z
    .registers 14
    .parameter "motionEvent"

    #@0
    .prologue
    .line 100
    const/4 v6, 0x0

    #@1
    .line 101
    .local v6, mSumOfDeltaX:F
    const/4 v7, 0x0

    #@2
    .line 102
    .local v7, mSumOfDeltaY:F
    const/4 v0, 0x0

    #@3
    .line 103
    .local v0, deltaX:I
    const/4 v1, 0x0

    #@4
    .line 104
    .local v1, deltaY:I
    const/4 v3, 0x0

    #@5
    .line 105
    .local v3, mSameSignal:Z
    const/4 v10, 0x3

    #@6
    new-array v8, v10, [I

    #@8
    .line 106
    .local v8, mslopX:[I
    const/4 v10, 0x3

    #@9
    new-array v9, v10, [I

    #@b
    .line 107
    .local v9, mslopY:[I
    const/4 v10, 0x3

    #@c
    new-array v4, v10, [I

    #@e
    .line 108
    .local v4, mSignalX:[I
    const/4 v10, 0x3

    #@f
    new-array v5, v10, [I

    #@11
    .line 112
    .local v5, mSignalY:[I
    const/4 v2, 0x0

    #@12
    .local v2, i:I
    :goto_12
    const/4 v10, 0x3

    #@13
    if-ge v2, v10, :cond_60

    #@15
    .line 114
    iget-object v10, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mCurrentCoodrinateX:[F

    #@17
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    #@1a
    move-result v11

    #@1b
    aput v11, v10, v2

    #@1d
    .line 115
    iget-object v10, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mCurrentCoodrinateY:[F

    #@1f
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    #@22
    move-result v11

    #@23
    aput v11, v10, v2

    #@25
    .line 117
    iget-object v10, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mFirstPointerX:[F

    #@27
    aget v10, v10, v2

    #@29
    iget-object v11, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mCurrentCoodrinateX:[F

    #@2b
    aget v11, v11, v2

    #@2d
    sub-float/2addr v10, v11

    #@2e
    float-to-int v0, v10

    #@2f
    .line 118
    iget-object v10, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mFirstPointerY:[F

    #@31
    aget v10, v10, v2

    #@33
    iget-object v11, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mCurrentCoodrinateY:[F

    #@35
    aget v11, v11, v2

    #@37
    sub-float/2addr v10, v11

    #@38
    float-to-int v1, v10

    #@39
    .line 120
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    #@3c
    move-result v10

    #@3d
    aput v10, v8, v2

    #@3f
    .line 121
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    #@42
    move-result v10

    #@43
    aput v10, v9, v2

    #@45
    .line 123
    int-to-float v10, v0

    #@46
    invoke-static {v10}, Ljava/lang/Math;->signum(F)F

    #@49
    move-result v10

    #@4a
    float-to-int v10, v10

    #@4b
    aput v10, v4, v2

    #@4d
    .line 124
    int-to-float v10, v1

    #@4e
    invoke-static {v10}, Ljava/lang/Math;->signum(F)F

    #@51
    move-result v10

    #@52
    float-to-int v10, v10

    #@53
    aput v10, v5, v2

    #@55
    .line 126
    aget v10, v8, v2

    #@57
    int-to-float v10, v10

    #@58
    add-float/2addr v6, v10

    #@59
    .line 127
    aget v10, v9, v2

    #@5b
    int-to-float v10, v10

    #@5c
    add-float/2addr v7, v10

    #@5d
    .line 112
    add-int/lit8 v2, v2, 0x1

    #@5f
    goto :goto_12

    #@60
    .line 131
    :cond_60
    const/4 v10, 0x0

    #@61
    aget v10, v8, v10

    #@63
    iget v11, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mTouchSlop:I

    #@65
    if-le v10, v11, :cond_ba

    #@67
    const/4 v10, 0x1

    #@68
    aget v10, v8, v10

    #@6a
    iget v11, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mTouchSlop:I

    #@6c
    if-le v10, v11, :cond_ba

    #@6e
    const/4 v10, 0x2

    #@6f
    aget v10, v8, v10

    #@71
    iget v11, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mTouchSlop:I

    #@73
    if-le v10, v11, :cond_ba

    #@75
    .line 132
    const/4 v10, 0x1

    #@76
    iput-boolean v10, p0, Lcom/lge/gesture/LGSlideAsideImpl;->overTouchSlop:Z

    #@78
    .line 138
    iget-boolean v10, p0, Lcom/lge/gesture/LGSlideAsideImpl;->overTouchSlop:Z

    #@7a
    if-eqz v10, :cond_91

    #@7c
    .line 139
    const/4 v10, 0x0

    #@7d
    aget v10, v4, v10

    #@7f
    if-lez v10, :cond_bc

    #@81
    const/4 v10, 0x1

    #@82
    aget v10, v4, v10

    #@84
    if-lez v10, :cond_bc

    #@86
    const/4 v10, 0x2

    #@87
    aget v10, v4, v10

    #@89
    if-lez v10, :cond_bc

    #@8b
    .line 141
    const/4 v10, 0x1

    #@8c
    iput v10, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mDirection:I

    #@8e
    .line 142
    const/4 v10, 0x1

    #@8f
    iput-boolean v10, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mSlideasideMove:Z

    #@91
    .line 154
    :cond_91
    :goto_91
    const/4 v10, 0x0

    #@92
    aget v10, v5, v10

    #@94
    const/4 v11, 0x1

    #@95
    aget v11, v5, v11

    #@97
    if-ne v10, v11, :cond_aa

    #@99
    const/4 v10, 0x1

    #@9a
    aget v10, v5, v10

    #@9c
    const/4 v11, 0x2

    #@9d
    aget v11, v5, v11

    #@9f
    if-ne v10, v11, :cond_aa

    #@a1
    const/4 v10, 0x2

    #@a2
    aget v10, v5, v10

    #@a4
    const/4 v11, 0x0

    #@a5
    aget v11, v5, v11

    #@a7
    if-ne v10, v11, :cond_aa

    #@a9
    .line 155
    const/4 v3, 0x1

    #@aa
    .line 158
    :cond_aa
    iget-boolean v10, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mSlideasideMove:Z

    #@ac
    if-eqz v10, :cond_ea

    #@ae
    .line 159
    const/high16 v10, 0x4040

    #@b0
    div-float v10, v6, v10

    #@b2
    iget v11, p0, Lcom/lge/gesture/LGSlideAsideImpl;->SLIDEASIDE_DRAGSLOP:F

    #@b4
    cmpl-float v10, v10, v11

    #@b6
    if-ltz v10, :cond_d9

    #@b8
    .line 160
    const/4 v10, 0x1

    #@b9
    .line 168
    :goto_b9
    return v10

    #@ba
    .line 134
    :cond_ba
    const/4 v10, 0x0

    #@bb
    goto :goto_b9

    #@bc
    .line 143
    :cond_bc
    const/4 v10, 0x0

    #@bd
    aget v10, v4, v10

    #@bf
    if-gez v10, :cond_d2

    #@c1
    const/4 v10, 0x1

    #@c2
    aget v10, v4, v10

    #@c4
    if-gez v10, :cond_d2

    #@c6
    const/4 v10, 0x2

    #@c7
    aget v10, v4, v10

    #@c9
    if-gez v10, :cond_d2

    #@cb
    .line 145
    const/4 v10, -0x1

    #@cc
    iput v10, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mDirection:I

    #@ce
    .line 146
    const/4 v10, 0x1

    #@cf
    iput-boolean v10, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mSlideasideMove:Z

    #@d1
    goto :goto_91

    #@d2
    .line 148
    :cond_d2
    const/4 v10, 0x0

    #@d3
    iput v10, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mDirection:I

    #@d5
    .line 149
    const/4 v10, 0x1

    #@d6
    iput-boolean v10, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mDirectionChange:Z

    #@d8
    goto :goto_91

    #@d9
    .line 162
    :cond_d9
    if-eqz v3, :cond_e8

    #@db
    const/high16 v10, 0x4040

    #@dd
    div-float v10, v7, v10

    #@df
    iget v11, p0, Lcom/lge/gesture/LGSlideAsideImpl;->SLIDEASIE_VERTICAL_DRAGSLOP:F

    #@e1
    cmpl-float v10, v10, v11

    #@e3
    if-ltz v10, :cond_e8

    #@e5
    .line 163
    const/4 v10, 0x1

    #@e6
    iput-boolean v10, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mDirectionChange:Z

    #@e8
    .line 165
    :cond_e8
    const/4 v10, 0x0

    #@e9
    goto :goto_b9

    #@ea
    .line 168
    :cond_ea
    const/4 v10, 0x0

    #@eb
    goto :goto_b9
.end method

.method initVelocityTrackerIfNotExists()V
    .registers 2

    #@0
    .prologue
    .line 68
    iget-object v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@2
    if-nez v0, :cond_a

    #@4
    .line 69
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@a
    .line 71
    :cond_a
    return-void
.end method

.method initialize()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 74
    iput-boolean v1, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mReadyToSubmitEvent:Z

    #@4
    .line 75
    iput-boolean v1, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mSlideasideMove:Z

    #@6
    .line 76
    iput v1, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mDirection:I

    #@8
    .line 77
    const/4 v0, 0x0

    #@9
    .local v0, j:I
    :goto_9
    const/4 v1, 0x3

    #@a
    if-ge v0, v1, :cond_1f

    #@c
    .line 78
    iget-object v1, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mFirstPointerY:[F

    #@e
    iget-object v2, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mFirstPointerX:[F

    #@10
    aput v3, v2, v0

    #@12
    aput v3, v1, v0

    #@14
    .line 79
    iget-object v1, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mCurrentCoodrinateY:[F

    #@16
    iget-object v2, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mCurrentCoodrinateX:[F

    #@18
    aput v3, v2, v0

    #@1a
    aput v3, v1, v0

    #@1c
    .line 77
    add-int/lit8 v0, v0, 0x1

    #@1e
    goto :goto_9

    #@1f
    .line 81
    :cond_1f
    return-void
.end method

.method onFlick(Landroid/view/MotionEvent;)Z
    .registers 12
    .parameter "motionEvent"

    #@0
    .prologue
    .line 179
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@3
    move-result v2

    #@4
    .line 180
    .local v2, mCount:I
    const/4 v1, -0x1

    #@5
    .line 182
    .local v1, mActivePointerId:I
    const/4 v3, 0x0

    #@6
    .line 183
    .local v3, mSumVelocityX:I
    const/4 v4, 0x0

    #@7
    .line 187
    .local v4, mSumVelocityY:I
    iget-object v5, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@9
    .line 188
    .local v5, velocityTracker:Landroid/view/VelocityTracker;
    const/16 v8, 0x3e8

    #@b
    iget v9, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mMaximumVelocity:I

    #@d
    int-to-float v9, v9

    #@e
    invoke-virtual {v5, v8, v9}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    #@11
    .line 190
    const/4 v0, 0x0

    #@12
    .local v0, i:I
    :goto_12
    if-ge v0, v2, :cond_2d

    #@14
    .line 191
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@17
    move-result v1

    #@18
    .line 192
    invoke-virtual {v5, v1}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    #@1b
    move-result v8

    #@1c
    iget v9, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mVelocityScale:F

    #@1e
    mul-float/2addr v8, v9

    #@1f
    float-to-int v8, v8

    #@20
    add-int/2addr v4, v8

    #@21
    .line 193
    invoke-virtual {v5, v1}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    #@24
    move-result v8

    #@25
    iget v9, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mVelocityScale:F

    #@27
    mul-float/2addr v8, v9

    #@28
    float-to-int v8, v8

    #@29
    add-int/2addr v3, v8

    #@2a
    .line 190
    add-int/lit8 v0, v0, 0x1

    #@2c
    goto :goto_12

    #@2d
    .line 196
    :cond_2d
    div-int v6, v3, v2

    #@2f
    .line 197
    .local v6, xVelocity:I
    div-int v7, v4, v2

    #@31
    .line 199
    .local v7, yVelocity:I
    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    #@34
    move-result v8

    #@35
    iget v9, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mMinimumVelocity:I

    #@37
    if-le v8, v9, :cond_45

    #@39
    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    #@3c
    move-result v8

    #@3d
    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    #@40
    move-result v9

    #@41
    if-le v8, v9, :cond_45

    #@43
    .line 200
    const/4 v8, 0x1

    #@44
    .line 202
    :goto_44
    return v8

    #@45
    :cond_45
    const/4 v8, 0x0

    #@46
    goto :goto_44
.end method

.method runSlideAside()V
    .registers 3

    #@0
    .prologue
    .line 84
    iget v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mDirection:I

    #@2
    const/4 v1, -0x1

    #@3
    if-ne v0, v1, :cond_d

    #@5
    .line 85
    iget-object v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mContext:Landroid/content/Context;

    #@7
    const-string v1, "com.lge.slideaside.pin.out"

    #@9
    invoke-static {v0, v1}, Lcom/lge/gesture/LgeGestureBasicUtil;->broadcastIntent(Landroid/content/Context;Ljava/lang/String;)V

    #@c
    .line 89
    :cond_c
    :goto_c
    return-void

    #@d
    .line 86
    :cond_d
    iget v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mDirection:I

    #@f
    const/4 v1, 0x1

    #@10
    if-ne v0, v1, :cond_c

    #@12
    .line 87
    iget-object v0, p0, Lcom/lge/gesture/LGSlideAsideImpl;->mContext:Landroid/content/Context;

    #@14
    const-string v1, "com.lge.slideaside.pin.in"

    #@16
    invoke-static {v0, v1}, Lcom/lge/gesture/LgeGestureBasicUtil;->broadcastIntent(Landroid/content/Context;Ljava/lang/String;)V

    #@19
    goto :goto_c
.end method
