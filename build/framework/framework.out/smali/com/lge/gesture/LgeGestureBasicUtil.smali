.class Lcom/lge/gesture/LgeGestureBasicUtil;
.super Ljava/lang/Object;
.source "LgeGestureBasicUtil.java"


# static fields
.field private static intent:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 9
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/lge/gesture/LgeGestureBasicUtil;->intent:Landroid/content/Intent;

    #@3
    return-void
.end method

.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 11
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 13
    return-void
.end method

.method static broadcastIntent(Landroid/content/Context;Ljava/lang/String;)V
    .registers 3
    .parameter "context"
    .parameter "action"

    #@0
    .prologue
    .line 32
    new-instance v0, Landroid/content/Intent;

    #@2
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@5
    sput-object v0, Lcom/lge/gesture/LgeGestureBasicUtil;->intent:Landroid/content/Intent;

    #@7
    .line 33
    sget-object v0, Lcom/lge/gesture/LgeGestureBasicUtil;->intent:Landroid/content/Intent;

    #@9
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@c
    .line 34
    sget-object v0, Lcom/lge/gesture/LgeGestureBasicUtil;->intent:Landroid/content/Intent;

    #@e
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@11
    .line 35
    return-void
.end method

.method static calculateSlope(FFFF)F
    .registers 6
    .parameter "x1"
    .parameter "y1"
    .parameter "x2"
    .parameter "y2"

    #@0
    .prologue
    .line 16
    sub-float v0, p3, p1

    #@2
    sub-float v1, p2, p0

    #@4
    div-float/2addr v0, v1

    #@5
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    #@8
    move-result v0

    #@9
    return v0
.end method

.method static getDistance(FFFF)F
    .registers 8
    .parameter "x1"
    .parameter "x2"
    .parameter "y1"
    .parameter "y2"

    #@0
    .prologue
    .line 20
    sub-float v0, p0, p1

    #@2
    .line 21
    .local v0, x:F
    sub-float v1, p2, p3

    #@4
    .line 22
    .local v1, y:F
    mul-float v2, v0, v0

    #@6
    mul-float v3, v1, v1

    #@8
    add-float/2addr v2, v3

    #@9
    invoke-static {v2}, Landroid/util/FloatMath;->sqrt(F)F

    #@c
    move-result v2

    #@d
    return v2
.end method

.method static getPixelPerMm(F)F
    .registers 6
    .parameter "dpi"

    #@0
    .prologue
    .line 27
    float-to-double v1, p0

    #@1
    const-wide v3, 0x4039666666666666L

    #@6
    div-double/2addr v1, v3

    #@7
    double-to-float v0, v1

    #@8
    .line 28
    .local v0, mPixelPerMM:F
    return v0
.end method
