.class Lcom/lge/gesture/LGPalmSwipeImpl;
.super Lcom/lge/gesture/LGPalmSwipe;
.source "LGPalmSwipeImpl.java"


# static fields
.field private static final INTENT_TOUCH_PALM_SWIPE_DOWN:Ljava/lang/String; = "com.lge.android.intent.action.TOUCH_PALM_SWIPE_DOWN"

.field private static final INTENT_TOUCH_PALM_SWIPE_UP:Ljava/lang/String; = "com.lge.android.intent.action.TOUCH_PALM_SWIPE_UP"


# instance fields
.field private final ALLOWANCE_OF_SLOPE:F

.field private final AXIS_DIRECTION:I

.field private final MAX_DRAGGING_ANGLE_COS:F

.field private final NEUTRAL:I

.field private final PALMSWIPE_DIRECTION:I

.field private final PALMSWIPE_MAX_DETECTION_RIGHTANGLE_DRAG_PIXELS:I

.field private final PALMSWIPE_MIN_DETECTION_DRAG_PIXELS:I

.field private final PALMSWIPE_MIN_DETECTION_REAL_PALM_DRAG_PIXELS:I

.field private final PIXELS_PER_MM:I

.field private final REVERSE_AXIS_DIRECTION:I

.field private final TIMELIMIT4GESTURE:I

.field private final TUNING_ON_RUNTIME:Z

.field private final XDPI:F

.field private final YDPI:F

.field private isInTime:Z

.field private mAxisDirectionCount:I

.field mContext:Landroid/content/Context;

.field private mCurrentPointerX:I

.field private mCurrentPointerY:I

.field private mDirection:I

.field private mDirectionChange:Z

.field private mDirectionOfSwipe:I

.field private mFirstPalmPointerIndex:I

.field private mFirstPalmPointerX:I

.field private mFirstPalmPointerY:I

.field private mFirstPointerX:I

.field private mFirstPointerY:I

.field private mFirstPressTime:J

.field private mPalmPressed:Z

.field private mPalmSwipe:Z

.field private mPrevDirection:I

.field private mPrevPointerX:I

.field private mPrevPointerY:I

.field private mPrevRightAngleDirection:I

.field private mReadyToSubmitPalmEvent:Z

.field private mReverseAxisDirectionCount:I


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v3, -0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 64
    invoke-direct {p0}, Lcom/lge/gesture/LGPalmSwipe;-><init>()V

    #@6
    .line 16
    const/4 v0, 0x0

    #@7
    iput-object v0, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mContext:Landroid/content/Context;

    #@9
    .line 18
    iput-boolean v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->TUNING_ON_RUNTIME:Z

    #@b
    .line 24
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->NEUTRAL:I

    #@d
    .line 25
    iput v1, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->REVERSE_AXIS_DIRECTION:I

    #@f
    .line 26
    iput v3, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->AXIS_DIRECTION:I

    #@11
    .line 30
    const v0, 0x3f067b80

    #@14
    iput v0, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->MAX_DRAGGING_ANGLE_COS:F

    #@16
    .line 31
    const/high16 v0, 0x3f80

    #@18
    iput v0, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->ALLOWANCE_OF_SLOPE:F

    #@1a
    .line 41
    iput-boolean v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mPalmPressed:Z

    #@1c
    .line 42
    iput-boolean v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mPalmSwipe:Z

    #@1e
    .line 43
    iput-boolean v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mReadyToSubmitPalmEvent:Z

    #@20
    .line 44
    iput-boolean v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mDirectionChange:Z

    #@22
    .line 45
    iput-boolean v1, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->isInTime:Z

    #@24
    .line 46
    const-wide/16 v0, 0x0

    #@26
    iput-wide v0, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPressTime:J

    #@28
    .line 47
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPointerX:I

    #@2a
    .line 48
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPointerY:I

    #@2c
    .line 49
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPalmPointerX:I

    #@2e
    .line 50
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPalmPointerY:I

    #@30
    .line 51
    iput v3, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPalmPointerIndex:I

    #@32
    .line 52
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mCurrentPointerX:I

    #@34
    .line 53
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mCurrentPointerY:I

    #@36
    .line 54
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mPrevPointerX:I

    #@38
    .line 55
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mPrevPointerY:I

    #@3a
    .line 56
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mDirectionOfSwipe:I

    #@3c
    .line 57
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mDirection:I

    #@3e
    .line 58
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mPrevDirection:I

    #@40
    .line 59
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mPrevRightAngleDirection:I

    #@42
    .line 60
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mAxisDirectionCount:I

    #@44
    .line 61
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mReverseAxisDirectionCount:I

    #@46
    .line 65
    iput-object p1, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mContext:Landroid/content/Context;

    #@48
    .line 66
    iget-object v0, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mContext:Landroid/content/Context;

    #@4a
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@4d
    move-result-object v0

    #@4e
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@51
    move-result-object v0

    #@52
    iget v0, v0, Landroid/util/DisplayMetrics;->xdpi:F

    #@54
    iput v0, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->XDPI:F

    #@56
    .line 67
    iget-object v0, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mContext:Landroid/content/Context;

    #@58
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5b
    move-result-object v0

    #@5c
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@5f
    move-result-object v0

    #@60
    iget v0, v0, Landroid/util/DisplayMetrics;->ydpi:F

    #@62
    iput v0, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->YDPI:F

    #@64
    .line 70
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->PALMSWIPE_DIRECTION:I

    #@66
    .line 71
    iget v0, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->YDPI:F

    #@68
    invoke-static {v0}, Lcom/lge/gesture/LgeGestureBasicUtil;->getPixelPerMm(F)F

    #@6b
    move-result v0

    #@6c
    float-to-int v0, v0

    #@6d
    iput v0, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->PIXELS_PER_MM:I

    #@6f
    .line 72
    iget v0, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->PIXELS_PER_MM:I

    #@71
    mul-int/lit8 v0, v0, 0x1e

    #@73
    iput v0, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->PALMSWIPE_MIN_DETECTION_DRAG_PIXELS:I

    #@75
    .line 73
    iget v0, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->YDPI:F

    #@77
    invoke-static {v0}, Lcom/lge/gesture/LgeGestureBasicUtil;->getPixelPerMm(F)F

    #@7a
    move-result v0

    #@7b
    float-to-int v0, v0

    #@7c
    mul-int/lit8 v0, v0, 0xa

    #@7e
    iput v0, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->PALMSWIPE_MIN_DETECTION_REAL_PALM_DRAG_PIXELS:I

    #@80
    .line 74
    iget v0, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->XDPI:F

    #@82
    invoke-static {v0}, Lcom/lge/gesture/LgeGestureBasicUtil;->getPixelPerMm(F)F

    #@85
    move-result v0

    #@86
    float-to-int v0, v0

    #@87
    mul-int/lit8 v0, v0, 0x1e

    #@89
    iput v0, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->PALMSWIPE_MAX_DETECTION_RIGHTANGLE_DRAG_PIXELS:I

    #@8b
    .line 75
    const/16 v0, 0xbb8

    #@8d
    iput v0, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->TIMELIMIT4GESTURE:I

    #@8f
    .line 83
    return-void
.end method

.method private initialize()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 86
    const-wide/16 v0, 0x0

    #@3
    iput-wide v0, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPressTime:J

    #@5
    .line 87
    iput-boolean v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mReadyToSubmitPalmEvent:Z

    #@7
    .line 88
    iput-boolean v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mDirectionChange:Z

    #@9
    .line 89
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mDirection:I

    #@b
    .line 91
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPointerX:I

    #@d
    .line 92
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPointerY:I

    #@f
    .line 93
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mCurrentPointerX:I

    #@11
    .line 94
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mCurrentPointerY:I

    #@13
    .line 95
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPalmPointerX:I

    #@15
    .line 96
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPalmPointerY:I

    #@17
    .line 97
    const/4 v0, -0x1

    #@18
    iput v0, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPalmPointerIndex:I

    #@1a
    .line 98
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mPrevPointerX:I

    #@1c
    .line 99
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mPrevPointerY:I

    #@1e
    .line 100
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mDirectionOfSwipe:I

    #@20
    .line 101
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mPrevDirection:I

    #@22
    .line 102
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mPrevRightAngleDirection:I

    #@24
    .line 103
    iput-boolean v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mPalmPressed:Z

    #@26
    .line 104
    iput-boolean v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mPalmSwipe:Z

    #@28
    .line 105
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mAxisDirectionCount:I

    #@2a
    .line 106
    iput v2, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->mReverseAxisDirectionCount:I

    #@2c
    .line 107
    const/4 v0, 0x1

    #@2d
    iput-boolean v0, p0, Lcom/lge/gesture/LGPalmSwipeImpl;->isInTime:Z

    #@2f
    .line 108
    return-void
.end method


# virtual methods
.method public PlayWithInputEvent(Landroid/view/InputEvent;)V
    .registers 23
    .parameter "event"

    #@0
    .prologue
    .line 112
    move-object/from16 v15, p1

    #@2
    check-cast v15, Landroid/view/MotionEvent;

    #@4
    .line 113
    .local v15, motionEvent:Landroid/view/MotionEvent;
    invoke-virtual {v15}, Landroid/view/MotionEvent;->getAction()I

    #@7
    move-result v3

    #@8
    .line 116
    .local v3, action:I
    if-nez v3, :cond_5e

    #@a
    .line 117
    const/16 v17, 0x0

    #@c
    move/from16 v0, v17

    #@e
    invoke-virtual {v15, v0}, Landroid/view/MotionEvent;->getX(I)F

    #@11
    move-result v17

    #@12
    move/from16 v0, v17

    #@14
    float-to-int v0, v0

    #@15
    move/from16 v17, v0

    #@17
    move/from16 v0, v17

    #@19
    move-object/from16 v1, p0

    #@1b
    iput v0, v1, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPointerX:I

    #@1d
    .line 118
    const/16 v17, 0x0

    #@1f
    move/from16 v0, v17

    #@21
    invoke-virtual {v15, v0}, Landroid/view/MotionEvent;->getY(I)F

    #@24
    move-result v17

    #@25
    move/from16 v0, v17

    #@27
    float-to-int v0, v0

    #@28
    move/from16 v17, v0

    #@2a
    move/from16 v0, v17

    #@2c
    move-object/from16 v1, p0

    #@2e
    iput v0, v1, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPointerY:I

    #@30
    .line 119
    const-string v17, "PALM"

    #@32
    new-instance v18, Ljava/lang/StringBuilder;

    #@34
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v19, "***** mFirstPointerX: "

    #@39
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v18

    #@3d
    move-object/from16 v0, p0

    #@3f
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPointerX:I

    #@41
    move/from16 v19, v0

    #@43
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v18

    #@47
    const-string v19, ", mFirstPointerY: "

    #@49
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v18

    #@4d
    move-object/from16 v0, p0

    #@4f
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPointerY:I

    #@51
    move/from16 v19, v0

    #@53
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@56
    move-result-object v18

    #@57
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v18

    #@5b
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 123
    :cond_5e
    const/16 v17, 0x2

    #@60
    move/from16 v0, v17

    #@62
    if-ne v3, v0, :cond_a1

    #@64
    move-object/from16 v0, p0

    #@66
    iget-boolean v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mPalmPressed:Z

    #@68
    move/from16 v17, v0

    #@6a
    if-nez v17, :cond_a1

    #@6c
    .line 124
    const/4 v8, 0x0

    #@6d
    .local v8, i:I
    :goto_6d
    invoke-virtual {v15}, Landroid/view/MotionEvent;->getPointerCount()I

    #@70
    move-result v17

    #@71
    move/from16 v0, v17

    #@73
    if-ge v8, v0, :cond_a1

    #@75
    .line 125
    invoke-virtual {v15, v8}, Landroid/view/MotionEvent;->getToolType(I)I

    #@78
    move-result v17

    #@79
    const/16 v18, 0x5

    #@7b
    move/from16 v0, v17

    #@7d
    move/from16 v1, v18

    #@7f
    if-ne v0, v1, :cond_9e

    #@81
    .line 126
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@84
    move-result-wide v17

    #@85
    move-wide/from16 v0, v17

    #@87
    move-object/from16 v2, p0

    #@89
    iput-wide v0, v2, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPressTime:J

    #@8b
    .line 127
    const/16 v17, 0x1

    #@8d
    move/from16 v0, v17

    #@8f
    move-object/from16 v1, p0

    #@91
    iput-boolean v0, v1, Lcom/lge/gesture/LGPalmSwipeImpl;->mPalmPressed:Z

    #@93
    .line 128
    move-object/from16 v0, p0

    #@95
    iput v8, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPalmPointerIndex:I

    #@97
    .line 129
    const-string v17, "PALM"

    #@99
    const-string v18, "Palm press recognized!!!"

    #@9b
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@9e
    .line 124
    :cond_9e
    add-int/lit8 v8, v8, 0x1

    #@a0
    goto :goto_6d

    #@a1
    .line 134
    .end local v8           #i:I
    :cond_a1
    move-object/from16 v0, p0

    #@a3
    iget-boolean v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->isInTime:Z

    #@a5
    move/from16 v17, v0

    #@a7
    if-eqz v17, :cond_102

    #@a9
    move-object/from16 v0, p0

    #@ab
    iget-boolean v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mPalmPressed:Z

    #@ad
    move/from16 v17, v0

    #@af
    if-eqz v17, :cond_102

    #@b1
    .line 135
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@b4
    move-result-wide v4

    #@b5
    .line 137
    .local v4, currTime:J
    move-object/from16 v0, p0

    #@b7
    iget-wide v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPressTime:J

    #@b9
    move-wide/from16 v17, v0

    #@bb
    sub-long v17, v4, v17

    #@bd
    const-wide/16 v19, 0x0

    #@bf
    cmp-long v17, v17, v19

    #@c1
    if-ltz v17, :cond_da

    #@c3
    move-object/from16 v0, p0

    #@c5
    iget-wide v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPressTime:J

    #@c7
    move-wide/from16 v17, v0

    #@c9
    sub-long v17, v4, v17

    #@cb
    move-object/from16 v0, p0

    #@cd
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->TIMELIMIT4GESTURE:I

    #@cf
    move/from16 v19, v0

    #@d1
    move/from16 v0, v19

    #@d3
    int-to-long v0, v0

    #@d4
    move-wide/from16 v19, v0

    #@d6
    cmp-long v17, v17, v19

    #@d8
    if-lez v17, :cond_102

    #@da
    .line 138
    :cond_da
    const-string v17, "PALM"

    #@dc
    new-instance v18, Ljava/lang/StringBuilder;

    #@de
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@e1
    const-string v19, "action: "

    #@e3
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e6
    move-result-object v18

    #@e7
    move-object/from16 v0, v18

    #@e9
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ec
    move-result-object v18

    #@ed
    const-string v19, ", OUT OF TIME !!!"

    #@ef
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v18

    #@f3
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f6
    move-result-object v18

    #@f7
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@fa
    .line 139
    const/16 v17, 0x0

    #@fc
    move/from16 v0, v17

    #@fe
    move-object/from16 v1, p0

    #@100
    iput-boolean v0, v1, Lcom/lge/gesture/LGPalmSwipeImpl;->isInTime:Z

    #@102
    .line 143
    .end local v4           #currTime:J
    :cond_102
    const/16 v17, 0x1

    #@104
    move/from16 v0, v17

    #@106
    if-eq v3, v0, :cond_10e

    #@108
    const/16 v17, 0x3

    #@10a
    move/from16 v0, v17

    #@10c
    if-ne v3, v0, :cond_141

    #@10e
    .line 145
    :cond_10e
    move-object/from16 v0, p0

    #@110
    iget-boolean v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mPalmSwipe:Z

    #@112
    move/from16 v17, v0

    #@114
    if-eqz v17, :cond_13e

    #@116
    move-object/from16 v0, p0

    #@118
    iget-boolean v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->isInTime:Z

    #@11a
    move/from16 v17, v0

    #@11c
    if-eqz v17, :cond_13e

    #@11e
    .line 146
    move-object/from16 v0, p0

    #@120
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mDirection:I

    #@122
    move/from16 v17, v0

    #@124
    const/16 v18, -0x1

    #@126
    move/from16 v0, v17

    #@128
    move/from16 v1, v18

    #@12a
    if-ne v0, v1, :cond_159

    #@12c
    .line 147
    const-string v17, "PALM"

    #@12e
    const-string v18, "PALM SWIPE DOWN|RIGHT!!"

    #@130
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@133
    .line 148
    move-object/from16 v0, p0

    #@135
    iget-object v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mContext:Landroid/content/Context;

    #@137
    move-object/from16 v17, v0

    #@139
    const-string v18, "com.lge.android.intent.action.TOUCH_PALM_SWIPE_DOWN"

    #@13b
    invoke-static/range {v17 .. v18}, Lcom/lge/gesture/LgeGestureBasicUtil;->broadcastIntent(Landroid/content/Context;Ljava/lang/String;)V

    #@13e
    .line 157
    :cond_13e
    :goto_13e
    invoke-direct/range {p0 .. p0}, Lcom/lge/gesture/LGPalmSwipeImpl;->initialize()V

    #@141
    .line 160
    :cond_141
    move-object/from16 v0, p0

    #@143
    iget-boolean v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mPalmSwipe:Z

    #@145
    move/from16 v17, v0

    #@147
    if-nez v17, :cond_151

    #@149
    move-object/from16 v0, p0

    #@14b
    iget-boolean v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->isInTime:Z

    #@14d
    move/from16 v17, v0

    #@14f
    if-nez v17, :cond_182

    #@151
    .line 161
    :cond_151
    const-string v17, "PALM"

    #@153
    const-string v18, "Palm Swipe is RUNning OR gesture is not detected due to OUT OF TIME !!"

    #@155
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@158
    .line 256
    :cond_158
    :goto_158
    return-void

    #@159
    .line 149
    :cond_159
    move-object/from16 v0, p0

    #@15b
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mDirection:I

    #@15d
    move/from16 v17, v0

    #@15f
    const/16 v18, 0x1

    #@161
    move/from16 v0, v17

    #@163
    move/from16 v1, v18

    #@165
    if-ne v0, v1, :cond_17a

    #@167
    .line 150
    const-string v17, "PALM"

    #@169
    const-string v18, "PALM SWIPE UP|LEFT!!"

    #@16b
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@16e
    .line 151
    move-object/from16 v0, p0

    #@170
    iget-object v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mContext:Landroid/content/Context;

    #@172
    move-object/from16 v17, v0

    #@174
    const-string v18, "com.lge.android.intent.action.TOUCH_PALM_SWIPE_UP"

    #@176
    invoke-static/range {v17 .. v18}, Lcom/lge/gesture/LgeGestureBasicUtil;->broadcastIntent(Landroid/content/Context;Ljava/lang/String;)V

    #@179
    goto :goto_13e

    #@17a
    .line 153
    :cond_17a
    const-string v17, "PALM"

    #@17c
    const-string v18, "PALM SWIPE No direction!!"

    #@17e
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@181
    goto :goto_13e

    #@182
    .line 166
    :cond_182
    move-object/from16 v0, p0

    #@184
    iget-boolean v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mPalmPressed:Z

    #@186
    move/from16 v17, v0

    #@188
    if-eqz v17, :cond_214

    #@18a
    move-object/from16 v0, p0

    #@18c
    iget-boolean v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mReadyToSubmitPalmEvent:Z

    #@18e
    move/from16 v17, v0

    #@190
    if-nez v17, :cond_214

    #@192
    move-object/from16 v0, p0

    #@194
    iget-boolean v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mPalmSwipe:Z

    #@196
    move/from16 v17, v0

    #@198
    if-nez v17, :cond_214

    #@19a
    move-object/from16 v0, p0

    #@19c
    iget-boolean v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mDirectionChange:Z

    #@19e
    move/from16 v17, v0

    #@1a0
    if-nez v17, :cond_214

    #@1a2
    .line 167
    const/16 v17, 0x1

    #@1a4
    move/from16 v0, v17

    #@1a6
    move-object/from16 v1, p0

    #@1a8
    iput-boolean v0, v1, Lcom/lge/gesture/LGPalmSwipeImpl;->mReadyToSubmitPalmEvent:Z

    #@1aa
    .line 169
    move-object/from16 v0, p0

    #@1ac
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPalmPointerIndex:I

    #@1ae
    move/from16 v17, v0

    #@1b0
    move/from16 v0, v17

    #@1b2
    invoke-virtual {v15, v0}, Landroid/view/MotionEvent;->getX(I)F

    #@1b5
    move-result v17

    #@1b6
    move/from16 v0, v17

    #@1b8
    float-to-int v0, v0

    #@1b9
    move/from16 v17, v0

    #@1bb
    move/from16 v0, v17

    #@1bd
    move-object/from16 v1, p0

    #@1bf
    iput v0, v1, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPalmPointerX:I

    #@1c1
    move/from16 v0, v17

    #@1c3
    move-object/from16 v1, p0

    #@1c5
    iput v0, v1, Lcom/lge/gesture/LGPalmSwipeImpl;->mPrevPointerX:I

    #@1c7
    .line 170
    move-object/from16 v0, p0

    #@1c9
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPalmPointerIndex:I

    #@1cb
    move/from16 v17, v0

    #@1cd
    move/from16 v0, v17

    #@1cf
    invoke-virtual {v15, v0}, Landroid/view/MotionEvent;->getY(I)F

    #@1d2
    move-result v17

    #@1d3
    move/from16 v0, v17

    #@1d5
    float-to-int v0, v0

    #@1d6
    move/from16 v17, v0

    #@1d8
    move/from16 v0, v17

    #@1da
    move-object/from16 v1, p0

    #@1dc
    iput v0, v1, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPalmPointerY:I

    #@1de
    move/from16 v0, v17

    #@1e0
    move-object/from16 v1, p0

    #@1e2
    iput v0, v1, Lcom/lge/gesture/LGPalmSwipeImpl;->mPrevPointerY:I

    #@1e4
    .line 171
    const-string v17, "PALM"

    #@1e6
    new-instance v18, Ljava/lang/StringBuilder;

    #@1e8
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@1eb
    const-string v19, "***** mFirstPalmPointerX: "

    #@1ed
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f0
    move-result-object v18

    #@1f1
    move-object/from16 v0, p0

    #@1f3
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPalmPointerX:I

    #@1f5
    move/from16 v19, v0

    #@1f7
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1fa
    move-result-object v18

    #@1fb
    const-string v19, ", mFirstPalmPointerY: "

    #@1fd
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@200
    move-result-object v18

    #@201
    move-object/from16 v0, p0

    #@203
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPalmPointerY:I

    #@205
    move/from16 v19, v0

    #@207
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20a
    move-result-object v18

    #@20b
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20e
    move-result-object v18

    #@20f
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@212
    goto/16 :goto_158

    #@214
    .line 176
    :cond_214
    move-object/from16 v0, p0

    #@216
    iget-boolean v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mReadyToSubmitPalmEvent:Z

    #@218
    move/from16 v17, v0

    #@21a
    if-eqz v17, :cond_158

    #@21c
    move-object/from16 v0, p0

    #@21e
    iget-boolean v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mPalmSwipe:Z

    #@220
    move/from16 v17, v0

    #@222
    if-nez v17, :cond_158

    #@224
    move-object/from16 v0, p0

    #@226
    iget-boolean v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mDirectionChange:Z

    #@228
    move/from16 v17, v0

    #@22a
    if-nez v17, :cond_158

    #@22c
    .line 178
    const/16 v17, 0x0

    #@22e
    move/from16 v0, v17

    #@230
    invoke-virtual {v15, v0}, Landroid/view/MotionEvent;->getX(I)F

    #@233
    move-result v17

    #@234
    move/from16 v0, v17

    #@236
    float-to-int v0, v0

    #@237
    move/from16 v17, v0

    #@239
    move/from16 v0, v17

    #@23b
    move-object/from16 v1, p0

    #@23d
    iput v0, v1, Lcom/lge/gesture/LGPalmSwipeImpl;->mCurrentPointerX:I

    #@23f
    .line 179
    const/16 v17, 0x0

    #@241
    move/from16 v0, v17

    #@243
    invoke-virtual {v15, v0}, Landroid/view/MotionEvent;->getY(I)F

    #@246
    move-result v17

    #@247
    move/from16 v0, v17

    #@249
    float-to-int v0, v0

    #@24a
    move/from16 v17, v0

    #@24c
    move/from16 v0, v17

    #@24e
    move-object/from16 v1, p0

    #@250
    iput v0, v1, Lcom/lge/gesture/LGPalmSwipeImpl;->mCurrentPointerY:I

    #@252
    .line 181
    move-object/from16 v0, p0

    #@254
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mPrevPointerX:I

    #@256
    move/from16 v17, v0

    #@258
    move-object/from16 v0, p0

    #@25a
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mCurrentPointerX:I

    #@25c
    move/from16 v18, v0

    #@25e
    sub-int v6, v17, v18

    #@260
    .line 182
    .local v6, deltaX:I
    move-object/from16 v0, p0

    #@262
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mPrevPointerY:I

    #@264
    move/from16 v17, v0

    #@266
    move-object/from16 v0, p0

    #@268
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mCurrentPointerY:I

    #@26a
    move/from16 v18, v0

    #@26c
    sub-int v7, v17, v18

    #@26e
    .line 185
    .local v7, deltaY:I
    move-object/from16 v0, p0

    #@270
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->PALMSWIPE_DIRECTION:I

    #@272
    move/from16 v17, v0

    #@274
    if-nez v17, :cond_46e

    #@276
    .line 186
    int-to-float v0, v7

    #@277
    move/from16 v17, v0

    #@279
    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->signum(F)F

    #@27c
    move-result v17

    #@27d
    move/from16 v0, v17

    #@27f
    float-to-int v0, v0

    #@280
    move/from16 v16, v0

    #@282
    .line 190
    .local v16, signal:I
    :goto_282
    move-object/from16 v0, p0

    #@284
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mPrevPointerX:I

    #@286
    move/from16 v17, v0

    #@288
    move/from16 v0, v17

    #@28a
    int-to-float v0, v0

    #@28b
    move/from16 v17, v0

    #@28d
    move-object/from16 v0, p0

    #@28f
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mCurrentPointerX:I

    #@291
    move/from16 v18, v0

    #@293
    move/from16 v0, v18

    #@295
    int-to-float v0, v0

    #@296
    move/from16 v18, v0

    #@298
    move-object/from16 v0, p0

    #@29a
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mPrevPointerY:I

    #@29c
    move/from16 v19, v0

    #@29e
    move/from16 v0, v19

    #@2a0
    int-to-float v0, v0

    #@2a1
    move/from16 v19, v0

    #@2a3
    move-object/from16 v0, p0

    #@2a5
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mCurrentPointerY:I

    #@2a7
    move/from16 v20, v0

    #@2a9
    move/from16 v0, v20

    #@2ab
    int-to-float v0, v0

    #@2ac
    move/from16 v20, v0

    #@2ae
    invoke-static/range {v17 .. v20}, Lcom/lge/gesture/LgeGestureBasicUtil;->getDistance(FFFF)F

    #@2b1
    move-result v17

    #@2b2
    move/from16 v0, v17

    #@2b4
    float-to-int v9, v0

    #@2b5
    .line 192
    .local v9, mDistance:I
    move-object/from16 v0, p0

    #@2b7
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->PIXELS_PER_MM:I

    #@2b9
    move/from16 v17, v0

    #@2bb
    move/from16 v0, v17

    #@2bd
    if-gt v9, v0, :cond_49a

    #@2bf
    .line 193
    move-object/from16 v0, p0

    #@2c1
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mPrevDirection:I

    #@2c3
    move/from16 v17, v0

    #@2c5
    move/from16 v0, v17

    #@2c7
    move-object/from16 v1, p0

    #@2c9
    iput v0, v1, Lcom/lge/gesture/LGPalmSwipeImpl;->mDirectionOfSwipe:I

    #@2cb
    .line 195
    move-object/from16 v0, p0

    #@2cd
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mPrevDirection:I

    #@2cf
    move/from16 v17, v0

    #@2d1
    const/16 v18, -0x1

    #@2d3
    move/from16 v0, v17

    #@2d5
    move/from16 v1, v18

    #@2d7
    if-ne v0, v1, :cond_47c

    #@2d9
    .line 196
    move-object/from16 v0, p0

    #@2db
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mAxisDirectionCount:I

    #@2dd
    move/from16 v17, v0

    #@2df
    add-int/lit8 v17, v17, 0x1

    #@2e1
    move/from16 v0, v17

    #@2e3
    move-object/from16 v1, p0

    #@2e5
    iput v0, v1, Lcom/lge/gesture/LGPalmSwipeImpl;->mAxisDirectionCount:I

    #@2e7
    .line 217
    :cond_2e7
    :goto_2e7
    move-object/from16 v0, p0

    #@2e9
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mCurrentPointerX:I

    #@2eb
    move/from16 v17, v0

    #@2ed
    move/from16 v0, v17

    #@2ef
    move-object/from16 v1, p0

    #@2f1
    iput v0, v1, Lcom/lge/gesture/LGPalmSwipeImpl;->mPrevPointerX:I

    #@2f3
    .line 218
    move-object/from16 v0, p0

    #@2f5
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mCurrentPointerY:I

    #@2f7
    move/from16 v17, v0

    #@2f9
    move/from16 v0, v17

    #@2fb
    move-object/from16 v1, p0

    #@2fd
    iput v0, v1, Lcom/lge/gesture/LGPalmSwipeImpl;->mPrevPointerY:I

    #@2ff
    .line 221
    move-object/from16 v0, p0

    #@301
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mDirectionOfSwipe:I

    #@303
    move/from16 v17, v0

    #@305
    move/from16 v0, v17

    #@307
    move-object/from16 v1, p0

    #@309
    iput v0, v1, Lcom/lge/gesture/LGPalmSwipeImpl;->mDirection:I

    #@30b
    .line 224
    move-object/from16 v0, p0

    #@30d
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPointerX:I

    #@30f
    move/from16 v17, v0

    #@311
    move-object/from16 v0, p0

    #@313
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mCurrentPointerX:I

    #@315
    move/from16 v18, v0

    #@317
    sub-int v17, v17, v18

    #@319
    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->abs(I)I

    #@31c
    move-result v12

    #@31d
    .line 225
    .local v12, mDistanceFrom1stPointerX:I
    move-object/from16 v0, p0

    #@31f
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPointerY:I

    #@321
    move/from16 v17, v0

    #@323
    move-object/from16 v0, p0

    #@325
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mCurrentPointerY:I

    #@327
    move/from16 v18, v0

    #@329
    sub-int v17, v17, v18

    #@32b
    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->abs(I)I

    #@32e
    move-result v13

    #@32f
    .line 226
    .local v13, mDistanceFrom1stPointerY:I
    move-object/from16 v0, p0

    #@331
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPalmPointerY:I

    #@333
    move/from16 v17, v0

    #@335
    move-object/from16 v0, p0

    #@337
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mCurrentPointerY:I

    #@339
    move/from16 v18, v0

    #@33b
    sub-int v17, v17, v18

    #@33d
    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->abs(I)I

    #@340
    move-result v11

    #@341
    .line 227
    .local v11, mDistanceFrom1stPalmPointerY:I
    move-object/from16 v0, p0

    #@343
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPalmPointerX:I

    #@345
    move/from16 v17, v0

    #@347
    move-object/from16 v0, p0

    #@349
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mCurrentPointerX:I

    #@34b
    move/from16 v18, v0

    #@34d
    sub-int v17, v17, v18

    #@34f
    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->abs(I)I

    #@352
    move-result v10

    #@353
    .line 228
    .local v10, mDistanceFrom1stPalmPointerX:I
    move-object/from16 v0, p0

    #@355
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPointerX:I

    #@357
    move/from16 v17, v0

    #@359
    move/from16 v0, v17

    #@35b
    int-to-float v0, v0

    #@35c
    move/from16 v17, v0

    #@35e
    move-object/from16 v0, p0

    #@360
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mFirstPointerY:I

    #@362
    move/from16 v18, v0

    #@364
    move/from16 v0, v18

    #@366
    int-to-float v0, v0

    #@367
    move/from16 v18, v0

    #@369
    move-object/from16 v0, p0

    #@36b
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mCurrentPointerX:I

    #@36d
    move/from16 v19, v0

    #@36f
    move/from16 v0, v19

    #@371
    int-to-float v0, v0

    #@372
    move/from16 v19, v0

    #@374
    move-object/from16 v0, p0

    #@376
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mCurrentPointerY:I

    #@378
    move/from16 v20, v0

    #@37a
    move/from16 v0, v20

    #@37c
    int-to-float v0, v0

    #@37d
    move/from16 v20, v0

    #@37f
    invoke-static/range {v17 .. v20}, Lcom/lge/gesture/LgeGestureBasicUtil;->calculateSlope(FFFF)F

    #@382
    move-result v14

    #@383
    .line 230
    .local v14, mSlopeOf2Points:F
    const-string v17, "PALM"

    #@385
    new-instance v18, Ljava/lang/StringBuilder;

    #@387
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@38a
    const-string/jumbo v19, "mSlopeOf2Points: "

    #@38d
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@390
    move-result-object v18

    #@391
    move-object/from16 v0, v18

    #@393
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@396
    move-result-object v18

    #@397
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39a
    move-result-object v18

    #@39b
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@39e
    .line 231
    const-string v17, "PALM"

    #@3a0
    new-instance v18, Ljava/lang/StringBuilder;

    #@3a2
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@3a5
    const-string/jumbo v19, "mDistanceFrom1stPointerX: "

    #@3a8
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3ab
    move-result-object v18

    #@3ac
    move-object/from16 v0, v18

    #@3ae
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3b1
    move-result-object v18

    #@3b2
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b5
    move-result-object v18

    #@3b6
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3b9
    .line 232
    const-string v17, "PALM"

    #@3bb
    new-instance v18, Ljava/lang/StringBuilder;

    #@3bd
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@3c0
    const-string/jumbo v19, "mDistanceFrom1stPointerY: "

    #@3c3
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c6
    move-result-object v18

    #@3c7
    move-object/from16 v0, v18

    #@3c9
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3cc
    move-result-object v18

    #@3cd
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d0
    move-result-object v18

    #@3d1
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3d4
    .line 233
    const-string v17, "PALM"

    #@3d6
    new-instance v18, Ljava/lang/StringBuilder;

    #@3d8
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@3db
    const-string/jumbo v19, "mDistanceFrom1stPalmPointerY: "

    #@3de
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e1
    move-result-object v18

    #@3e2
    move-object/from16 v0, v18

    #@3e4
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e7
    move-result-object v18

    #@3e8
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3eb
    move-result-object v18

    #@3ec
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3ef
    .line 234
    const-string v17, "PALM"

    #@3f1
    new-instance v18, Ljava/lang/StringBuilder;

    #@3f3
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@3f6
    const-string/jumbo v19, "mDistanceFrom1stPalmPointerX: "

    #@3f9
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3fc
    move-result-object v18

    #@3fd
    move-object/from16 v0, v18

    #@3ff
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@402
    move-result-object v18

    #@403
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@406
    move-result-object v18

    #@407
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@40a
    .line 236
    move-object/from16 v0, p0

    #@40c
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->PALMSWIPE_DIRECTION:I

    #@40e
    move/from16 v17, v0

    #@410
    if-nez v17, :cond_4fa

    #@412
    move/from16 v17, v13

    #@414
    :goto_414
    move-object/from16 v0, p0

    #@416
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->PALMSWIPE_MIN_DETECTION_DRAG_PIXELS:I

    #@418
    move/from16 v18, v0

    #@41a
    move/from16 v0, v17

    #@41c
    move/from16 v1, v18

    #@41e
    if-lt v0, v1, :cond_525

    #@420
    move-object/from16 v0, p0

    #@422
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->PALMSWIPE_DIRECTION:I

    #@424
    move/from16 v17, v0

    #@426
    if-nez v17, :cond_4fe

    #@428
    .end local v11           #mDistanceFrom1stPalmPointerY:I
    :goto_428
    move-object/from16 v0, p0

    #@42a
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->PALMSWIPE_MIN_DETECTION_REAL_PALM_DRAG_PIXELS:I

    #@42c
    move/from16 v17, v0

    #@42e
    move/from16 v0, v17

    #@430
    if-lt v11, v0, :cond_525

    #@432
    move-object/from16 v0, p0

    #@434
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->PALMSWIPE_DIRECTION:I

    #@436
    move/from16 v17, v0

    #@438
    if-nez v17, :cond_501

    #@43a
    move/from16 v17, v12

    #@43c
    :goto_43c
    move-object/from16 v0, p0

    #@43e
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->PALMSWIPE_MAX_DETECTION_RIGHTANGLE_DRAG_PIXELS:I

    #@440
    move/from16 v18, v0

    #@442
    move/from16 v0, v17

    #@444
    move/from16 v1, v18

    #@446
    if-ge v0, v1, :cond_525

    #@448
    .line 240
    move-object/from16 v0, p0

    #@44a
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mDirection:I

    #@44c
    move/from16 v17, v0

    #@44e
    const/16 v18, -0x1

    #@450
    move/from16 v0, v17

    #@452
    move/from16 v1, v18

    #@454
    if-eq v0, v1, :cond_464

    #@456
    move-object/from16 v0, p0

    #@458
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mDirection:I

    #@45a
    move/from16 v17, v0

    #@45c
    const/16 v18, 0x1

    #@45e
    move/from16 v0, v17

    #@460
    move/from16 v1, v18

    #@462
    if-ne v0, v1, :cond_505

    #@464
    .line 241
    :cond_464
    const/16 v17, 0x1

    #@466
    move/from16 v0, v17

    #@468
    move-object/from16 v1, p0

    #@46a
    iput-boolean v0, v1, Lcom/lge/gesture/LGPalmSwipeImpl;->mPalmSwipe:Z

    #@46c
    goto/16 :goto_158

    #@46e
    .line 188
    .end local v9           #mDistance:I
    .end local v10           #mDistanceFrom1stPalmPointerX:I
    .end local v12           #mDistanceFrom1stPointerX:I
    .end local v13           #mDistanceFrom1stPointerY:I
    .end local v14           #mSlopeOf2Points:F
    .end local v16           #signal:I
    :cond_46e
    int-to-float v0, v6

    #@46f
    move/from16 v17, v0

    #@471
    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->signum(F)F

    #@474
    move-result v17

    #@475
    move/from16 v0, v17

    #@477
    float-to-int v0, v0

    #@478
    move/from16 v16, v0

    #@47a
    .restart local v16       #signal:I
    goto/16 :goto_282

    #@47c
    .line 197
    .restart local v9       #mDistance:I
    :cond_47c
    move-object/from16 v0, p0

    #@47e
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mPrevDirection:I

    #@480
    move/from16 v17, v0

    #@482
    const/16 v18, -0x1

    #@484
    move/from16 v0, v17

    #@486
    move/from16 v1, v18

    #@488
    if-ne v0, v1, :cond_2e7

    #@48a
    .line 198
    move-object/from16 v0, p0

    #@48c
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mAxisDirectionCount:I

    #@48e
    move/from16 v17, v0

    #@490
    add-int/lit8 v17, v17, 0x1

    #@492
    move/from16 v0, v17

    #@494
    move-object/from16 v1, p0

    #@496
    iput v0, v1, Lcom/lge/gesture/LGPalmSwipeImpl;->mAxisDirectionCount:I

    #@498
    goto/16 :goto_2e7

    #@49a
    .line 201
    :cond_49a
    const/16 v17, -0x1

    #@49c
    move/from16 v0, v16

    #@49e
    move/from16 v1, v17

    #@4a0
    if-ne v0, v1, :cond_4c2

    #@4a2
    .line 203
    const/16 v17, -0x1

    #@4a4
    move/from16 v0, v17

    #@4a6
    move-object/from16 v1, p0

    #@4a8
    iput v0, v1, Lcom/lge/gesture/LGPalmSwipeImpl;->mDirectionOfSwipe:I

    #@4aa
    .line 204
    const/16 v17, -0x1

    #@4ac
    move/from16 v0, v17

    #@4ae
    move-object/from16 v1, p0

    #@4b0
    iput v0, v1, Lcom/lge/gesture/LGPalmSwipeImpl;->mPrevDirection:I

    #@4b2
    .line 205
    move-object/from16 v0, p0

    #@4b4
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mAxisDirectionCount:I

    #@4b6
    move/from16 v17, v0

    #@4b8
    add-int/lit8 v17, v17, 0x1

    #@4ba
    move/from16 v0, v17

    #@4bc
    move-object/from16 v1, p0

    #@4be
    iput v0, v1, Lcom/lge/gesture/LGPalmSwipeImpl;->mAxisDirectionCount:I

    #@4c0
    goto/16 :goto_2e7

    #@4c2
    .line 206
    :cond_4c2
    const/16 v17, 0x1

    #@4c4
    move/from16 v0, v16

    #@4c6
    move/from16 v1, v17

    #@4c8
    if-ne v0, v1, :cond_4ea

    #@4ca
    .line 208
    const/16 v17, 0x1

    #@4cc
    move/from16 v0, v17

    #@4ce
    move-object/from16 v1, p0

    #@4d0
    iput v0, v1, Lcom/lge/gesture/LGPalmSwipeImpl;->mDirectionOfSwipe:I

    #@4d2
    .line 209
    const/16 v17, 0x1

    #@4d4
    move/from16 v0, v17

    #@4d6
    move-object/from16 v1, p0

    #@4d8
    iput v0, v1, Lcom/lge/gesture/LGPalmSwipeImpl;->mPrevDirection:I

    #@4da
    .line 210
    move-object/from16 v0, p0

    #@4dc
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mReverseAxisDirectionCount:I

    #@4de
    move/from16 v17, v0

    #@4e0
    add-int/lit8 v17, v17, 0x1

    #@4e2
    move/from16 v0, v17

    #@4e4
    move-object/from16 v1, p0

    #@4e6
    iput v0, v1, Lcom/lge/gesture/LGPalmSwipeImpl;->mReverseAxisDirectionCount:I

    #@4e8
    goto/16 :goto_2e7

    #@4ea
    .line 211
    :cond_4ea
    if-nez v16, :cond_2e7

    #@4ec
    .line 213
    move-object/from16 v0, p0

    #@4ee
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mPrevDirection:I

    #@4f0
    move/from16 v17, v0

    #@4f2
    move/from16 v0, v17

    #@4f4
    move-object/from16 v1, p0

    #@4f6
    iput v0, v1, Lcom/lge/gesture/LGPalmSwipeImpl;->mDirectionOfSwipe:I

    #@4f8
    goto/16 :goto_2e7

    #@4fa
    .restart local v10       #mDistanceFrom1stPalmPointerX:I
    .restart local v11       #mDistanceFrom1stPalmPointerY:I
    .restart local v12       #mDistanceFrom1stPointerX:I
    .restart local v13       #mDistanceFrom1stPointerY:I
    .restart local v14       #mSlopeOf2Points:F
    :cond_4fa
    move/from16 v17, v12

    #@4fc
    .line 236
    goto/16 :goto_414

    #@4fe
    :cond_4fe
    move v11, v10

    #@4ff
    goto/16 :goto_428

    #@501
    .end local v11           #mDistanceFrom1stPalmPointerY:I
    :cond_501
    move/from16 v17, v13

    #@503
    goto/16 :goto_43c

    #@505
    .line 243
    :cond_505
    const-string v17, "PALM"

    #@507
    new-instance v18, Ljava/lang/StringBuilder;

    #@509
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@50c
    const-string v19, "Wrong direction!!! mDirection: "

    #@50e
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@511
    move-result-object v18

    #@512
    move-object/from16 v0, p0

    #@514
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->mDirection:I

    #@516
    move/from16 v19, v0

    #@518
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@51b
    move-result-object v18

    #@51c
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51f
    move-result-object v18

    #@520
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@523
    goto/16 :goto_158

    #@525
    .line 246
    :cond_525
    move-object/from16 v0, p0

    #@527
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->PALMSWIPE_DIRECTION:I

    #@529
    move/from16 v17, v0

    #@52b
    if-nez v17, :cond_55d

    #@52d
    .end local v12           #mDistanceFrom1stPointerX:I
    :goto_52d
    move-object/from16 v0, p0

    #@52f
    iget v0, v0, Lcom/lge/gesture/LGPalmSwipeImpl;->PALMSWIPE_MAX_DETECTION_RIGHTANGLE_DRAG_PIXELS:I

    #@531
    move/from16 v17, v0

    #@533
    move/from16 v0, v17

    #@535
    if-lt v12, v0, :cond_554

    #@537
    .line 247
    const-string v17, "PALM"

    #@539
    const-string v18, "inner of touchslop"

    #@53b
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@53e
    .line 248
    const/high16 v17, 0x3f80

    #@540
    cmpg-float v17, v14, v17

    #@542
    if-gez v17, :cond_554

    #@544
    .line 249
    const/16 v17, 0x1

    #@546
    move/from16 v0, v17

    #@548
    move-object/from16 v1, p0

    #@54a
    iput-boolean v0, v1, Lcom/lge/gesture/LGPalmSwipeImpl;->mDirectionChange:Z

    #@54c
    .line 250
    const-string v17, "PALM"

    #@54e
    const-string/jumbo v18, "less than 45 degree from x-coordinate , so palm swipe is false"

    #@551
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@554
    .line 253
    :cond_554
    const-string v17, "PALM"

    #@556
    const-string v18, "Not Yet!!!"

    #@558
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@55b
    goto/16 :goto_158

    #@55d
    .restart local v12       #mDistanceFrom1stPointerX:I
    :cond_55d
    move v12, v13

    #@55e
    .line 246
    goto :goto_52d
.end method
