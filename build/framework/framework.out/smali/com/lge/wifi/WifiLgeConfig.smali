.class public Lcom/lge/wifi/WifiLgeConfig;
.super Ljava/lang/Object;
.source "WifiLgeConfig.java"


# static fields
#the value of this static final field might be set in the static constructor
.field public static final CONFIG_LGE_WLAN_BRCM_PATCH:Z = false

#the value of this static final field might be set in the static constructor
.field public static final CONFIG_LGE_WLAN_PATCH:Z = false

#the value of this static final field might be set in the static constructor
.field public static final CONFIG_LGE_WLAN_QCOM_PATCH:Z = false

#the value of this static final field might be set in the static constructor
.field public static final CONFIG_LGE_WLAN_TEST:Z = false

#the value of this static final field might be set in the static constructor
.field public static final CONFIG_LGE_WLAN_TEST_PROFILE:Z = false

#the value of this static final field might be set in the static constructor
.field private static final KSC5601SSID:Z = false

.field private static final LOCAL_LOGD:Z = true

.field private static final TAG:Ljava/lang/String; = "WifiLgeConfig"

.field private static mContext:Landroid/content/Context; = null

.field private static mInitKsc5601Ssid:Z = false

.field private static mIsAvailableKtUsim:Z = false

.field private static final mLgeEap:Z = true

.field private static final mLgeKtCm:Z

.field private static final mMobileHotspot:Z

.field private static final mTargetCountry:Ljava/lang/String;

.field private static final mTargetOperator:Ljava/lang/String;

.field private static final mWiFiOffloading:Z

.field private static final mWifiIfaceName:Ljava/lang/String;

.field private static mWifiLgeConfig:Lcom/lge/wifi/WifiLgeConfig;

.field private static final mWifiOffdelayAfter15alarm:Z

.field private static final mWlanChipVersion:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 20
    sput-object v0, Lcom/lge/wifi/WifiLgeConfig;->mWifiLgeConfig:Lcom/lge/wifi/WifiLgeConfig;

    #@4
    .line 21
    sput-object v0, Lcom/lge/wifi/WifiLgeConfig;->mContext:Landroid/content/Context;

    #@6
    .line 27
    const-string/jumbo v0, "wifi.lge.patch"

    #@9
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@c
    move-result v0

    #@d
    sput-boolean v0, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@f
    .line 29
    const-string/jumbo v0, "wlan.chip.vendor"

    #@12
    const-string v2, "brcm"

    #@14
    invoke-static {v0, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@17
    move-result-object v0

    #@18
    const-string/jumbo v2, "qcom"

    #@1b
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e
    move-result v0

    #@1f
    sput-boolean v0, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_QCOM_PATCH:Z

    #@21
    .line 31
    const-string/jumbo v0, "wlan.chip.vendor"

    #@24
    const-string/jumbo v2, "qcom"

    #@27
    invoke-static {v0, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2a
    move-result-object v0

    #@2b
    const-string v2, "brcm"

    #@2d
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v0

    #@31
    sput-boolean v0, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_BRCM_PATCH:Z

    #@33
    .line 37
    const-string/jumbo v0, "wifi.lge.test"

    #@36
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@39
    move-result v0

    #@3a
    sput-boolean v0, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_TEST:Z

    #@3c
    .line 39
    const-string/jumbo v0, "wifi.lge.test_profile"

    #@3f
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@42
    move-result v0

    #@43
    sput-boolean v0, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_TEST_PROFILE:Z

    #@45
    .line 46
    const-string/jumbo v0, "wlan.chip.version"

    #@48
    const-string v2, "bcm4329"

    #@4a
    invoke-static {v0, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@4d
    move-result-object v0

    #@4e
    sput-object v0, Lcom/lge/wifi/WifiLgeConfig;->mWlanChipVersion:Ljava/lang/String;

    #@50
    .line 49
    const-string/jumbo v0, "wifi.interface"

    #@53
    const-string/jumbo v2, "wlan0"

    #@56
    invoke-static {v0, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@59
    move-result-object v0

    #@5a
    sput-object v0, Lcom/lge/wifi/WifiLgeConfig;->mWifiIfaceName:Ljava/lang/String;

    #@5c
    .line 54
    const-string/jumbo v0, "ro.build.target_operator"

    #@5f
    const-string v2, "OPEN"

    #@61
    invoke-static {v0, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@64
    move-result-object v0

    #@65
    sput-object v0, Lcom/lge/wifi/WifiLgeConfig;->mTargetOperator:Ljava/lang/String;

    #@67
    .line 56
    const-string/jumbo v0, "ro.build.target_country"

    #@6a
    const-string v2, "COM"

    #@6c
    invoke-static {v0, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@6f
    move-result-object v0

    #@70
    sput-object v0, Lcom/lge/wifi/WifiLgeConfig;->mTargetCountry:Ljava/lang/String;

    #@72
    .line 62
    const-string/jumbo v0, "wifi.lge.kt.cm"

    #@75
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@78
    move-result v0

    #@79
    if-eqz v0, :cond_b1

    #@7b
    sget-object v0, Lcom/lge/wifi/WifiLgeConfig;->mTargetOperator:Ljava/lang/String;

    #@7d
    const-string v2, "KT"

    #@7f
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@82
    move-result v0

    #@83
    if-eqz v0, :cond_b1

    #@85
    const/4 v0, 0x1

    #@86
    :goto_86
    sput-boolean v0, Lcom/lge/wifi/WifiLgeConfig;->mLgeKtCm:Z

    #@88
    .line 69
    const-string/jumbo v0, "wifi.lge.hanglessid"

    #@8b
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@8e
    move-result v0

    #@8f
    sput-boolean v0, Lcom/lge/wifi/WifiLgeConfig;->KSC5601SSID:Z

    #@91
    .line 77
    const-string/jumbo v0, "wifi.lge.offdelay"

    #@94
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@97
    move-result v0

    #@98
    sput-boolean v0, Lcom/lge/wifi/WifiLgeConfig;->mWifiOffdelayAfter15alarm:Z

    #@9a
    .line 82
    sput-boolean v1, Lcom/lge/wifi/WifiLgeConfig;->mInitKsc5601Ssid:Z

    #@9c
    .line 88
    const-string/jumbo v0, "wifi.lge.offloading"

    #@9f
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@a2
    move-result v0

    #@a3
    sput-boolean v0, Lcom/lge/wifi/WifiLgeConfig;->mWiFiOffloading:Z

    #@a5
    .line 95
    const-string/jumbo v0, "wifi.lge.mhp"

    #@a8
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@ab
    move-result v0

    #@ac
    sput-boolean v0, Lcom/lge/wifi/WifiLgeConfig;->mMobileHotspot:Z

    #@ae
    .line 107
    sput-boolean v1, Lcom/lge/wifi/WifiLgeConfig;->mIsAvailableKtUsim:Z

    #@b0
    return-void

    #@b1
    :cond_b1
    move v0, v1

    #@b2
    .line 62
    goto :goto_86
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 112
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 114
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 117
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 118
    sput-object p1, Lcom/lge/wifi/WifiLgeConfig;->mContext:Landroid/content/Context;

    #@5
    .line 120
    return-void
.end method

.method public static checkLgeKtCmProfileAccess()Z
    .registers 1

    #@0
    .prologue
    .line 454
    sget-boolean v0, Lcom/lge/wifi/WifiLgeConfig;->mIsAvailableKtUsim:Z

    #@2
    if-eqz v0, :cond_6

    #@4
    .line 455
    const/4 v0, 0x1

    #@5
    .line 457
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public static checkLgeKtUsimInfo()Z
    .registers 8

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 416
    const/4 v0, 0x0

    #@3
    .line 418
    .local v0, checkedImsi:Ljava/lang/String;
    const-string v5, "WifiLgeConfig"

    #@5
    const-string v6, "checkLgeKtUsimInfo"

    #@7
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 420
    sget-object v5, Lcom/lge/wifi/WifiLgeConfig;->mContext:Landroid/content/Context;

    #@c
    const-string/jumbo v6, "phone"

    #@f
    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@12
    move-result-object v2

    #@13
    check-cast v2, Landroid/telephony/TelephonyManager;

    #@15
    .line 422
    .local v2, telephonyManager:Landroid/telephony/TelephonyManager;
    if-eqz v2, :cond_75

    #@17
    .line 423
    const-string v5, "WifiLgeConfig"

    #@19
    const-string/jumbo v6, "telephonyManager != null"

    #@1c
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 424
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    .line 427
    :try_start_23
    const-string v5, "WifiLgeConfig"

    #@25
    new-instance v6, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v7, "checkedImsi = "

    #@2c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v6

    #@30
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v6

    #@34
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v6

    #@38
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 428
    if-eqz v0, :cond_65

    #@3d
    const-string v5, "45008"

    #@3f
    const/4 v6, 0x0

    #@40
    const/4 v7, 0x5

    #@41
    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@44
    move-result-object v6

    #@45
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@48
    move-result v5

    #@49
    if-nez v5, :cond_59

    #@4b
    const-string v5, "45002"

    #@4d
    const/4 v6, 0x0

    #@4e
    const/4 v7, 0x5

    #@4f
    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@52
    move-result-object v6

    #@53
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@56
    move-result v5

    #@57
    if-eqz v5, :cond_65

    #@59
    .line 431
    :cond_59
    const-string v5, "WifiLgeConfig"

    #@5b
    const-string/jumbo v6, "mIsAvailableKtUsim = true"

    #@5e
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@61
    .line 432
    const/4 v5, 0x1

    #@62
    sput-boolean v5, Lcom/lge/wifi/WifiLgeConfig;->mIsAvailableKtUsim:Z

    #@64
    .line 444
    :goto_64
    return v3

    #@65
    .line 434
    :cond_65
    const-string v5, "WifiLgeConfig"

    #@67
    const-string/jumbo v6, "mIsAvailableKtUsim = false"

    #@6a
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    .line 435
    const/4 v5, 0x0

    #@6e
    sput-boolean v5, Lcom/lge/wifi/WifiLgeConfig;->mIsAvailableKtUsim:Z
    :try_end_70
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_70} :catch_71

    #@70
    goto :goto_64

    #@71
    .line 437
    :catch_71
    move-exception v1

    #@72
    .line 438
    .local v1, e:Ljava/lang/Exception;
    sput-boolean v4, Lcom/lge/wifi/WifiLgeConfig;->mIsAvailableKtUsim:Z

    #@74
    goto :goto_64

    #@75
    .line 442
    .end local v1           #e:Ljava/lang/Exception;
    :cond_75
    const-string v3, "WifiLgeConfig"

    #@77
    const-string/jumbo v5, "telephonyManager == null"

    #@7a
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7d
    move v3, v4

    #@7e
    .line 444
    goto :goto_64
.end method

.method public static doesSupportHotspotList()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 339
    const-string v1, "TMO"

    #@3
    sget-object v2, Lcom/lge/wifi/WifiLgeConfig;->mTargetOperator:Ljava/lang/String;

    #@5
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8
    move-result v1

    #@9
    if-nez v1, :cond_25

    #@b
    const-string v1, "ATT"

    #@d
    sget-object v2, Lcom/lge/wifi/WifiLgeConfig;->mTargetOperator:Ljava/lang/String;

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v1

    #@13
    if-nez v1, :cond_25

    #@15
    const-string v1, "MPCS"

    #@17
    sget-object v2, Lcom/lge/wifi/WifiLgeConfig;->mTargetOperator:Ljava/lang/String;

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v1

    #@1d
    if-nez v1, :cond_25

    #@1f
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->isWifiChameleonFeaturedCarrier()Z

    #@22
    move-result v1

    #@23
    if-eqz v1, :cond_30

    #@25
    :cond_25
    const-string v1, "US"

    #@27
    sget-object v2, Lcom/lge/wifi/WifiLgeConfig;->mTargetCountry:Ljava/lang/String;

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c
    move-result v1

    #@2d
    if-eqz v1, :cond_30

    #@2f
    .line 350
    :cond_2f
    :goto_2f
    return v0

    #@30
    .line 342
    :cond_30
    const-string v1, "TLS"

    #@32
    sget-object v2, Lcom/lge/wifi/WifiLgeConfig;->mTargetOperator:Ljava/lang/String;

    #@34
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@37
    move-result v1

    #@38
    if-nez v1, :cond_4e

    #@3a
    const-string v1, "BELL"

    #@3c
    sget-object v2, Lcom/lge/wifi/WifiLgeConfig;->mTargetOperator:Ljava/lang/String;

    #@3e
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@41
    move-result v1

    #@42
    if-nez v1, :cond_4e

    #@44
    const-string v1, "RGS"

    #@46
    sget-object v2, Lcom/lge/wifi/WifiLgeConfig;->mTargetOperator:Ljava/lang/String;

    #@48
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4b
    move-result v1

    #@4c
    if-eqz v1, :cond_58

    #@4e
    :cond_4e
    const-string v1, "CA"

    #@50
    sget-object v2, Lcom/lge/wifi/WifiLgeConfig;->mTargetCountry:Ljava/lang/String;

    #@52
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@55
    move-result v1

    #@56
    if-nez v1, :cond_2f

    #@58
    .line 346
    :cond_58
    const-string v1, "VZW"

    #@5a
    sget-object v2, Lcom/lge/wifi/WifiLgeConfig;->mTargetOperator:Ljava/lang/String;

    #@5c
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5f
    move-result v1

    #@60
    if-eqz v1, :cond_6c

    #@62
    const-string v1, "US"

    #@64
    sget-object v2, Lcom/lge/wifi/WifiLgeConfig;->mTargetCountry:Ljava/lang/String;

    #@66
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@69
    move-result v1

    #@6a
    if-nez v1, :cond_2f

    #@6c
    .line 350
    :cond_6c
    const/4 v0, 0x0

    #@6d
    goto :goto_2f
.end method

.method public static getCountry()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 152
    sget-object v0, Lcom/lge/wifi/WifiLgeConfig;->mTargetCountry:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public static getInstance()Lcom/lge/wifi/WifiLgeConfig;
    .registers 1

    #@0
    .prologue
    .line 124
    sget-object v0, Lcom/lge/wifi/WifiLgeConfig;->mWifiLgeConfig:Lcom/lge/wifi/WifiLgeConfig;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 125
    new-instance v0, Lcom/lge/wifi/WifiLgeConfig;

    #@6
    invoke-direct {v0}, Lcom/lge/wifi/WifiLgeConfig;-><init>()V

    #@9
    sput-object v0, Lcom/lge/wifi/WifiLgeConfig;->mWifiLgeConfig:Lcom/lge/wifi/WifiLgeConfig;

    #@b
    .line 128
    :cond_b
    sget-object v0, Lcom/lge/wifi/WifiLgeConfig;->mWifiLgeConfig:Lcom/lge/wifi/WifiLgeConfig;

    #@d
    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/lge/wifi/WifiLgeConfig;
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 132
    sget-object v0, Lcom/lge/wifi/WifiLgeConfig;->mWifiLgeConfig:Lcom/lge/wifi/WifiLgeConfig;

    #@2
    if-nez v0, :cond_e

    #@4
    .line 133
    new-instance v0, Lcom/lge/wifi/WifiLgeConfig;

    #@6
    invoke-direct {v0, p0}, Lcom/lge/wifi/WifiLgeConfig;-><init>(Landroid/content/Context;)V

    #@9
    sput-object v0, Lcom/lge/wifi/WifiLgeConfig;->mWifiLgeConfig:Lcom/lge/wifi/WifiLgeConfig;

    #@b
    .line 138
    :cond_b
    :goto_b
    sget-object v0, Lcom/lge/wifi/WifiLgeConfig;->mWifiLgeConfig:Lcom/lge/wifi/WifiLgeConfig;

    #@d
    return-object v0

    #@e
    .line 134
    :cond_e
    sget-object v0, Lcom/lge/wifi/WifiLgeConfig;->mContext:Landroid/content/Context;

    #@10
    if-nez v0, :cond_b

    #@12
    .line 135
    sput-object p0, Lcom/lge/wifi/WifiLgeConfig;->mContext:Landroid/content/Context;

    #@14
    goto :goto_b
.end method

.method private getMccMncInfo()[Ljava/lang/String;
    .registers 8

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 272
    const/4 v2, 0x2

    #@2
    new-array v1, v2, [Ljava/lang/String;

    #@4
    .line 274
    .local v1, simMccMnc:[Ljava/lang/String;
    sget-object v2, Lcom/lge/wifi/WifiLgeConfig;->mContext:Landroid/content/Context;

    #@6
    if-nez v2, :cond_a

    #@8
    move-object v1, v3

    #@9
    .line 293
    .end local v1           #simMccMnc:[Ljava/lang/String;
    :goto_9
    return-object v1

    #@a
    .line 280
    .restart local v1       #simMccMnc:[Ljava/lang/String;
    :cond_a
    const/4 v4, 0x0

    #@b
    :try_start_b
    sget-object v2, Lcom/lge/wifi/WifiLgeConfig;->mContext:Landroid/content/Context;

    #@d
    const-string/jumbo v5, "phone"

    #@10
    invoke-virtual {v2, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@13
    move-result-object v2

    #@14
    check-cast v2, Landroid/telephony/TelephonyManager;

    #@16
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    const/4 v5, 0x0

    #@1b
    const/4 v6, 0x3

    #@1c
    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    aput-object v2, v1, v4

    #@22
    .line 284
    const/4 v4, 0x1

    #@23
    sget-object v2, Lcom/lge/wifi/WifiLgeConfig;->mContext:Landroid/content/Context;

    #@25
    const-string/jumbo v5, "phone"

    #@28
    invoke-virtual {v2, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2b
    move-result-object v2

    #@2c
    check-cast v2, Landroid/telephony/TelephonyManager;

    #@2e
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    #@31
    move-result-object v2

    #@32
    const/4 v5, 0x3

    #@33
    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    aput-object v2, v1, v4

    #@39
    .line 287
    const-string v2, "WifiLgeConfig"

    #@3b
    new-instance v4, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v5, "getMccMncInfo: MCC ["

    #@42
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v4

    #@46
    const/4 v5, 0x0

    #@47
    aget-object v5, v1, v5

    #@49
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v4

    #@4d
    const-string v5, "] MNC ["

    #@4f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v4

    #@53
    const/4 v5, 0x1

    #@54
    aget-object v5, v1, v5

    #@56
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v4

    #@5a
    const-string v5, "]"

    #@5c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v4

    #@60
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v4

    #@64
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_67
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_b .. :try_end_67} :catch_68

    #@67
    goto :goto_9

    #@68
    .line 288
    :catch_68
    move-exception v0

    #@69
    .line 289
    .local v0, e:Ljava/lang/StringIndexOutOfBoundsException;
    const-string v2, "WifiLgeConfig"

    #@6b
    new-instance v4, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v5, "getMccMncInfo : "

    #@72
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v4

    #@76
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v4

    #@7a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v4

    #@7e
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    move-object v1, v3

    #@82
    .line 290
    goto :goto_9
.end method

.method public static getOperator()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 145
    sget-object v0, Lcom/lge/wifi/WifiLgeConfig;->mTargetOperator:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public static isWifiChameleonFeaturedCarrier()Z
    .registers 2

    #@0
    .prologue
    .line 359
    const-string v0, "US"

    #@2
    sget-object v1, Lcom/lge/wifi/WifiLgeConfig;->mTargetCountry:Ljava/lang/String;

    #@4
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_24

    #@a
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    const-string v1, "SPR"

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v0

    #@14
    if-nez v0, :cond_22

    #@16
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@19
    move-result-object v0

    #@1a
    const-string v1, "BM"

    #@1c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v0

    #@20
    if-eqz v0, :cond_24

    #@22
    .line 362
    :cond_22
    const/4 v0, 0x1

    #@23
    .line 364
    :goto_23
    return v0

    #@24
    :cond_24
    const/4 v0, 0x0

    #@25
    goto :goto_23
.end method

.method public static setDefaultMobileHotspotUS()Z
    .registers 2

    #@0
    .prologue
    .line 467
    const-string v0, "MPCS"

    #@2
    sget-object v1, Lcom/lge/wifi/WifiLgeConfig;->mTargetOperator:Ljava/lang/String;

    #@4
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_14

    #@a
    const-string v0, "USC"

    #@c
    sget-object v1, Lcom/lge/wifi/WifiLgeConfig;->mTargetOperator:Ljava/lang/String;

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_1e

    #@14
    :cond_14
    const-string v0, "US"

    #@16
    sget-object v1, Lcom/lge/wifi/WifiLgeConfig;->mTargetCountry:Ljava/lang/String;

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v0

    #@1c
    if-nez v0, :cond_24

    #@1e
    :cond_1e
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->isWifiChameleonFeaturedCarrier()Z

    #@21
    move-result v0

    #@22
    if-eqz v0, :cond_26

    #@24
    .line 469
    :cond_24
    const/4 v0, 0x1

    #@25
    .line 471
    :goto_25
    return v0

    #@26
    :cond_26
    const/4 v0, 0x0

    #@27
    goto :goto_25
.end method

.method public static setLgeKtUsimRemoved()Z
    .registers 2

    #@0
    .prologue
    .line 448
    const-string v0, "WifiLgeConfig"

    #@2
    const-string/jumbo v1, "setLgeKtUsimRemoved"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 449
    const/4 v0, 0x0

    #@9
    sput-boolean v0, Lcom/lge/wifi/WifiLgeConfig;->mIsAvailableKtUsim:Z

    #@b
    .line 450
    const/4 v0, 0x1

    #@c
    return v0
.end method

.method public static setWiFiAutoChannel(Ljava/lang/String;)V
    .registers 2
    .parameter "value"

    #@0
    .prologue
    .line 401
    const-string/jumbo v0, "wifi.lge.autochannel"

    #@3
    invoke-static {v0, p0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@6
    .line 402
    return-void
.end method

.method public static useChangeSsid()Z
    .registers 1

    #@0
    .prologue
    .line 222
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public static useDefaultWifiOn()Z
    .registers 2

    #@0
    .prologue
    .line 183
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    const-string v1, "VZW"

    #@6
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_18

    #@c
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    const-string v1, "ATT"

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_1a

    #@18
    .line 185
    :cond_18
    const/4 v0, 0x1

    #@19
    .line 188
    :goto_19
    return v0

    #@1a
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_19
.end method

.method public static useDefaultWifiSleepPolicy()I
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 195
    const-string/jumbo v2, "wifi.lge.sleeppolicy"

    #@4
    invoke-static {v2, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@7
    move-result v0

    #@8
    .line 197
    .local v0, wifiSleepPolicy:I
    packed-switch v0, :pswitch_data_2c

    #@b
    .line 203
    const-string v2, "WifiLgeConfig"

    #@d
    new-instance v3, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string/jumbo v4, "unknown wifiSleepPolicy : ["

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    const-string v4, "]"

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v3

    #@27
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    move v0, v1

    #@2b
    .line 206
    .end local v0           #wifiSleepPolicy:I
    :pswitch_2b
    return v0

    #@2c
    .line 197
    :pswitch_data_2c
    .packed-switch 0x0
        :pswitch_2b
        :pswitch_2b
        :pswitch_2b
    .end packed-switch
.end method

.method public static useHotspotHidden()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 368
    const-string v1, "US"

    #@3
    sget-object v2, Lcom/lge/wifi/WifiLgeConfig;->mTargetCountry:Ljava/lang/String;

    #@5
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_16

    #@b
    const-string v1, "ATT"

    #@d
    sget-object v2, Lcom/lge/wifi/WifiLgeConfig;->mTargetOperator:Ljava/lang/String;

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_16

    #@15
    .line 378
    :cond_15
    :goto_15
    return v0

    #@16
    .line 372
    :cond_16
    const-string v1, "CA"

    #@18
    sget-object v2, Lcom/lge/wifi/WifiLgeConfig;->mTargetCountry:Ljava/lang/String;

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v1

    #@1e
    if-eqz v1, :cond_3e

    #@20
    .line 373
    const-string v1, "TLS"

    #@22
    sget-object v2, Lcom/lge/wifi/WifiLgeConfig;->mTargetOperator:Ljava/lang/String;

    #@24
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@27
    move-result v1

    #@28
    if-nez v1, :cond_15

    #@2a
    const-string v1, "BELL"

    #@2c
    sget-object v2, Lcom/lge/wifi/WifiLgeConfig;->mTargetOperator:Ljava/lang/String;

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@31
    move-result v1

    #@32
    if-nez v1, :cond_15

    #@34
    const-string v1, "RGS"

    #@36
    sget-object v2, Lcom/lge/wifi/WifiLgeConfig;->mTargetOperator:Ljava/lang/String;

    #@38
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b
    move-result v1

    #@3c
    if-nez v1, :cond_15

    #@3e
    .line 378
    :cond_3e
    const/4 v0, 0x0

    #@3f
    goto :goto_15
.end method

.method public static useKoreanSsid()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 241
    sget-boolean v1, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@3
    if-eqz v1, :cond_13

    #@5
    sget-boolean v1, Lcom/lge/wifi/WifiLgeConfig;->KSC5601SSID:Z

    #@7
    if-eqz v1, :cond_13

    #@9
    .line 242
    const-string v1, "CN"

    #@b
    sget-object v2, Lcom/lge/wifi/WifiLgeConfig;->mTargetCountry:Ljava/lang/String;

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v1

    #@11
    if-eqz v1, :cond_14

    #@13
    .line 251
    :cond_13
    :goto_13
    return v0

    #@14
    .line 244
    :cond_14
    const-string v1, "HK"

    #@16
    sget-object v2, Lcom/lge/wifi/WifiLgeConfig;->mTargetCountry:Ljava/lang/String;

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v1

    #@1c
    if-nez v1, :cond_13

    #@1e
    .line 248
    const/4 v0, 0x1

    #@1f
    goto :goto_13
.end method

.method public static useKoreanSsid(Ljava/lang/String;)Z
    .registers 4
    .parameter "SSID"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 255
    sget-boolean v1, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@3
    if-eqz v1, :cond_1c

    #@5
    sget-boolean v1, Lcom/lge/wifi/WifiLgeConfig;->KSC5601SSID:Z

    #@7
    if-eqz v1, :cond_1c

    #@9
    .line 257
    const-string/jumbo v1, "\u200b\""

    #@c
    invoke-virtual {p0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@f
    move-result v1

    #@10
    if-nez v1, :cond_1c

    #@12
    .line 258
    const-string v1, "CN"

    #@14
    sget-object v2, Lcom/lge/wifi/WifiLgeConfig;->mTargetCountry:Ljava/lang/String;

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v1

    #@1a
    if-eqz v1, :cond_1d

    #@1c
    .line 268
    :cond_1c
    :goto_1c
    return v0

    #@1d
    .line 260
    :cond_1d
    const-string v1, "HK"

    #@1f
    sget-object v2, Lcom/lge/wifi/WifiLgeConfig;->mTargetCountry:Ljava/lang/String;

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v1

    #@25
    if-nez v1, :cond_1c

    #@27
    .line 264
    const/4 v0, 0x1

    #@28
    goto :goto_1c
.end method

.method public static useLgeEap()Z
    .registers 1

    #@0
    .prologue
    .line 408
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public static useLgeKtCm()Z
    .registers 1

    #@0
    .prologue
    .line 159
    sget-boolean v0, Lcom/lge/wifi/WifiLgeConfig;->mLgeKtCm:Z

    #@2
    return v0
.end method

.method public static useMobileHotspot()Z
    .registers 1

    #@0
    .prologue
    .line 323
    sget-boolean v0, Lcom/lge/wifi/WifiLgeConfig;->mMobileHotspot:Z

    #@2
    return v0
.end method

.method public static useOpProfile()Z
    .registers 1

    #@0
    .prologue
    .line 226
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public static useWiFiAggregation()Z
    .registers 2

    #@0
    .prologue
    .line 386
    const-string/jumbo v0, "wifi.lge.aggregation"

    #@3
    const/4 v1, 0x0

    #@4
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static useWiFiAutoChannel()I
    .registers 3

    #@0
    .prologue
    .line 393
    const-string/jumbo v1, "wifi.lge.autochannel"

    #@3
    const/4 v2, 0x0

    #@4
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@7
    move-result v0

    #@8
    .line 394
    .local v0, value:I
    return v0
.end method

.method public static useWiFiOffloading()Z
    .registers 2

    #@0
    .prologue
    .line 313
    const-string/jumbo v0, "wifi.lge.offloading"

    #@3
    const/4 v1, 0x0

    #@4
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static useWifiActivationWhileCharging()Z
    .registers 2

    #@0
    .prologue
    .line 214
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    const-string v1, "VZW"

    #@6
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    .line 215
    const/4 v0, 0x1

    #@d
    .line 218
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public static useWifiDLNA()Z
    .registers 2

    #@0
    .prologue
    .line 331
    const-string v0, "dhcp.dlna.using"

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public static useWifiOffDelayAfter15alarm()Z
    .registers 1

    #@0
    .prologue
    .line 301
    sget-boolean v0, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@2
    if-eqz v0, :cond_a

    #@4
    sget-boolean v0, Lcom/lge/wifi/WifiLgeConfig;->mWifiOffdelayAfter15alarm:Z

    #@6
    if-eqz v0, :cond_a

    #@8
    .line 302
    const/4 v0, 0x1

    #@9
    .line 304
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method


# virtual methods
.method public checkMccMnc(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 7
    .parameter "mcc"
    .parameter "mnc"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 168
    invoke-direct {p0}, Lcom/lge/wifi/WifiLgeConfig;->getMccMncInfo()[Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 170
    .local v0, myMccMnc:[Ljava/lang/String;
    if-eqz v0, :cond_19

    #@8
    .line 171
    aget-object v3, v0, v2

    #@a
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v3

    #@e
    if-eqz v3, :cond_19

    #@10
    aget-object v3, v0, v1

    #@12
    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v3

    #@16
    if-eqz v3, :cond_19

    #@18
    .line 176
    :goto_18
    return v1

    #@19
    :cond_19
    move v1, v2

    #@1a
    goto :goto_18
.end method
