.class public Lcom/lge/wifi/WifiLgeSharedValues;
.super Ljava/lang/Object;
.source "WifiLgeSharedValues.java"


# static fields
.field private static final LGE_EAP_AUTH_INFO:Ljava/lang/String; = "/data/misc/wifi/lge_eap_info"

.field private static final TAG:Ljava/lang/String; = "WifiLgeSharedValues"

.field private static mIsDlnaReady2:Z

.field private static mUnicodeSSID:Z

.field private static mWifiLgeSharedValues:Lcom/lge/wifi/WifiLgeSharedValues;


# instance fields
.field private mP2pState:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 16
    const/4 v0, 0x0

    #@2
    sput-object v0, Lcom/lge/wifi/WifiLgeSharedValues;->mWifiLgeSharedValues:Lcom/lge/wifi/WifiLgeSharedValues;

    #@4
    .line 21
    sput-boolean v1, Lcom/lge/wifi/WifiLgeSharedValues;->mIsDlnaReady2:Z

    #@6
    .line 27
    sput-boolean v1, Lcom/lge/wifi/WifiLgeSharedValues;->mUnicodeSSID:Z

    #@8
    return-void
.end method

.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 44
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 45
    const/4 v0, 0x1

    #@4
    iput v0, p0, Lcom/lge/wifi/WifiLgeSharedValues;->mP2pState:I

    #@6
    .line 46
    return-void
.end method

.method public static getInstance()Lcom/lge/wifi/WifiLgeSharedValues;
    .registers 1

    #@0
    .prologue
    .line 50
    sget-object v0, Lcom/lge/wifi/WifiLgeSharedValues;->mWifiLgeSharedValues:Lcom/lge/wifi/WifiLgeSharedValues;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 51
    new-instance v0, Lcom/lge/wifi/WifiLgeSharedValues;

    #@6
    invoke-direct {v0}, Lcom/lge/wifi/WifiLgeSharedValues;-><init>()V

    #@9
    sput-object v0, Lcom/lge/wifi/WifiLgeSharedValues;->mWifiLgeSharedValues:Lcom/lge/wifi/WifiLgeSharedValues;

    #@b
    .line 54
    :cond_b
    sget-object v0, Lcom/lge/wifi/WifiLgeSharedValues;->mWifiLgeSharedValues:Lcom/lge/wifi/WifiLgeSharedValues;

    #@d
    return-object v0
.end method

.method public static getLgeEapAuthInfo()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    .line 100
    const/4 v3, 0x0

    #@1
    .line 101
    .local v3, temp:Ljava/lang/String;
    const/4 v2, 0x0

    #@2
    .line 104
    .local v2, line:Ljava/lang/String;
    :try_start_2
    new-instance v0, Ljava/io/BufferedReader;

    #@4
    new-instance v4, Ljava/io/FileReader;

    #@6
    const-string v5, "/data/misc/wifi/lge_eap_info"

    #@8
    invoke-direct {v4, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    #@b
    invoke-direct {v0, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    #@e
    .line 105
    .local v0, br:Ljava/io/BufferedReader;
    const-string v3, ""

    #@10
    .line 106
    :goto_10
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    if-eqz v2, :cond_28

    #@16
    .line 107
    new-instance v4, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v3

    #@27
    goto :goto_10

    #@28
    .line 109
    :cond_28
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_2b
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2b} :catch_2c

    #@2b
    .line 114
    .end local v0           #br:Ljava/io/BufferedReader;
    :goto_2b
    return-object v3

    #@2c
    .line 110
    :catch_2c
    move-exception v1

    #@2d
    .line 111
    .local v1, e:Ljava/lang/Exception;
    const-string v4, "WifiLgeSharedValues"

    #@2f
    new-instance v5, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string/jumbo v6, "setSmomo - error"

    #@37
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v5

    #@3b
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v5

    #@3f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v5

    #@43
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    goto :goto_2b
.end method

.method public static setLgeEapAuthInfo(Ljava/lang/String;)V
    .registers 6
    .parameter "str"

    #@0
    .prologue
    .line 118
    if-eqz p0, :cond_17

    #@2
    .line 120
    :try_start_2
    new-instance v0, Ljava/io/BufferedWriter;

    #@4
    new-instance v2, Ljava/io/FileWriter;

    #@6
    const-string v3, "/data/misc/wifi/lge_eap_info"

    #@8
    invoke-direct {v2, v3}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    #@b
    invoke-direct {v0, v2}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    #@e
    .line 121
    .local v0, bw:Ljava/io/BufferedWriter;
    invoke-virtual {v0, p0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@11
    .line 122
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->newLine()V

    #@14
    .line 123
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_17} :catch_18

    #@17
    .line 128
    .end local v0           #bw:Ljava/io/BufferedWriter;
    :cond_17
    :goto_17
    return-void

    #@18
    .line 124
    :catch_18
    move-exception v1

    #@19
    .line 125
    .local v1, e:Ljava/lang/Exception;
    const-string v2, "WifiLgeSharedValues"

    #@1b
    new-instance v3, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string/jumbo v4, "setSmomo - error"

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    goto :goto_17
.end method


# virtual methods
.method public getDlnaEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 69
    sget-boolean v0, Lcom/lge/wifi/WifiLgeSharedValues;->mIsDlnaReady2:Z

    #@2
    return v0
.end method

.method public getP2pWifiState()I
    .registers 2

    #@0
    .prologue
    .line 94
    iget v0, p0, Lcom/lge/wifi/WifiLgeSharedValues;->mP2pState:I

    #@2
    return v0
.end method

.method public getUnicodeSSID()Z
    .registers 2

    #@0
    .prologue
    .line 84
    sget-boolean v0, Lcom/lge/wifi/WifiLgeSharedValues;->mUnicodeSSID:Z

    #@2
    return v0
.end method

.method public setDlnaEnabled(Z)V
    .registers 5
    .parameter "input"

    #@0
    .prologue
    .line 63
    const-string v0, "WifiLgeSharedValues"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v2, "setDLNAEnabled() - IsDlnaReady2 is set :"

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 64
    sput-boolean p1, Lcom/lge/wifi/WifiLgeSharedValues;->mIsDlnaReady2:Z

    #@1b
    .line 65
    return-void
.end method

.method public setP2pWifiState(I)V
    .registers 2
    .parameter "input"

    #@0
    .prologue
    .line 89
    iput p1, p0, Lcom/lge/wifi/WifiLgeSharedValues;->mP2pState:I

    #@2
    .line 90
    return-void
.end method

.method public setUnicodeSSID(Z)V
    .registers 2
    .parameter "input"

    #@0
    .prologue
    .line 79
    sput-boolean p1, Lcom/lge/wifi/WifiLgeSharedValues;->mUnicodeSSID:Z

    #@2
    .line 80
    return-void
.end method
