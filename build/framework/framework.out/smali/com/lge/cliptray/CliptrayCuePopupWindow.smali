.class public Lcom/lge/cliptray/CliptrayCuePopupWindow;
.super Ljava/lang/Object;
.source "CliptrayCuePopupWindow.java"


# static fields
.field private static final CUE_HEIGHT_DP:I = 0x31

.field private static final CUE_WIDTH_DP:I = 0x2c


# instance fields
.field private mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

.field private mContext:Landroid/content/Context;

.field private mCueBtn:Landroid/widget/ImageView;

.field private mCuePopupWindow:Landroid/widget/PopupWindow;

.field private mIsEditableText:Z

.field private mRes:Landroid/content/res/Resources;

.field private mShowSoftInputOnFocus:Z

.field private mTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/lge/loader/cliptray/ICliptrayManagerLoader;)V
    .registers 7
    .parameter "context"
    .parameter "clipmanager"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 30
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 23
    iput-object v3, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mCueBtn:Landroid/widget/ImageView;

    #@7
    .line 24
    iput-object v3, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mTextView:Landroid/widget/TextView;

    #@9
    .line 27
    iput-boolean v2, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mShowSoftInputOnFocus:Z

    #@b
    .line 28
    iput-boolean v2, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mIsEditableText:Z

    #@d
    .line 31
    iput-object p1, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mContext:Landroid/content/Context;

    #@f
    .line 32
    iget-object v2, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mContext:Landroid/content/Context;

    #@11
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@14
    move-result-object v2

    #@15
    iput-object v2, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mRes:Landroid/content/res/Resources;

    #@17
    .line 33
    iput-object p2, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@19
    .line 35
    iget-object v2, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mContext:Landroid/content/Context;

    #@1b
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1e
    move-result-object v2

    #@1f
    const v3, 0x20c0048

    #@22
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@25
    move-result v1

    #@26
    .line 36
    .local v1, cueWidth:I
    iget-object v2, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mContext:Landroid/content/Context;

    #@28
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@2b
    move-result-object v2

    #@2c
    const v3, 0x20c004a

    #@2f
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@32
    move-result v0

    #@33
    .line 38
    .local v0, cueHeight:I
    new-instance v2, Landroid/widget/ImageView;

    #@35
    iget-object v3, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mContext:Landroid/content/Context;

    #@37
    invoke-direct {v2, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    #@3a
    iput-object v2, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mCueBtn:Landroid/widget/ImageView;

    #@3c
    .line 39
    new-instance v2, Landroid/widget/PopupWindow;

    #@3e
    iget-object v3, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mCueBtn:Landroid/widget/ImageView;

    #@40
    invoke-direct {v2, v3, v1, v0}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    #@43
    iput-object v2, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mCuePopupWindow:Landroid/widget/PopupWindow;

    #@45
    .line 40
    iget-object v2, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mCueBtn:Landroid/widget/ImageView;

    #@47
    const v3, 0x20201a0

    #@4a
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    #@4d
    .line 41
    iget-object v2, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mCueBtn:Landroid/widget/ImageView;

    #@4f
    new-instance v3, Lcom/lge/cliptray/CliptrayCuePopupWindow$1;

    #@51
    invoke-direct {v3, p0}, Lcom/lge/cliptray/CliptrayCuePopupWindow$1;-><init>(Lcom/lge/cliptray/CliptrayCuePopupWindow;)V

    #@54
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@57
    .line 59
    return-void
.end method

.method static synthetic access$000(Lcom/lge/cliptray/CliptrayCuePopupWindow;)Lcom/lge/loader/cliptray/ICliptrayManagerLoader;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 17
    iget-object v0, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/lge/cliptray/CliptrayCuePopupWindow;)Landroid/widget/TextView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 17
    iget-object v0, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mTextView:Landroid/widget/TextView;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/lge/cliptray/CliptrayCuePopupWindow;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mShowSoftInputOnFocus:Z

    #@2
    return v0
.end method

.method static synthetic access$300(Lcom/lge/cliptray/CliptrayCuePopupWindow;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mIsEditableText:Z

    #@2
    return v0
.end method

.method static synthetic access$400(Lcom/lge/cliptray/CliptrayCuePopupWindow;)Landroid/widget/PopupWindow;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 17
    iget-object v0, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mCuePopupWindow:Landroid/widget/PopupWindow;

    #@2
    return-object v0
.end method


# virtual methods
.method public hideCliptrayCuePopupWindow()V
    .registers 2

    #@0
    .prologue
    .line 106
    iget-object v0, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mCuePopupWindow:Landroid/widget/PopupWindow;

    #@2
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    #@5
    .line 107
    return-void
.end method

.method public isOpenCueShowing()Z
    .registers 2

    #@0
    .prologue
    .line 110
    iget-object v0, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mCuePopupWindow:Landroid/widget/PopupWindow;

    #@2
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public showCliptrayCuePopupWindow(Landroid/view/View;)V
    .registers 8
    .parameter "parent"

    #@0
    .prologue
    .line 63
    iget-object v3, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@2
    if-eqz v3, :cond_3d

    #@4
    iget-object v3, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@6
    invoke-interface {v3}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->getVisibility()I

    #@9
    move-result v3

    #@a
    if-eqz v3, :cond_3d

    #@c
    .line 64
    iget-object v3, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mContext:Landroid/content/Context;

    #@e
    const-string/jumbo v4, "window"

    #@11
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@14
    move-result-object v2

    #@15
    check-cast v2, Landroid/view/WindowManager;

    #@17
    .line 65
    .local v2, wm:Landroid/view/WindowManager;
    new-instance v1, Landroid/util/DisplayMetrics;

    #@19
    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    #@1c
    .line 66
    .local v1, metrics:Landroid/util/DisplayMetrics;
    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    #@23
    .line 68
    const/4 v0, 0x0

    #@24
    .line 69
    .local v0, cliptrayHeight:I
    iget v3, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    #@26
    iget v4, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    #@28
    if-ge v3, v4, :cond_3e

    #@2a
    .line 70
    iget-object v3, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mRes:Landroid/content/res/Resources;

    #@2c
    const v4, 0x20c004b

    #@2f
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@32
    move-result v0

    #@33
    .line 75
    :goto_33
    if-lez v0, :cond_3d

    #@35
    .line 76
    iget-object v3, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mCuePopupWindow:Landroid/widget/PopupWindow;

    #@37
    const/16 v4, 0x55

    #@39
    const/4 v5, 0x0

    #@3a
    invoke-virtual {v3, p1, v4, v5, v0}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    #@3d
    .line 79
    .end local v0           #cliptrayHeight:I
    .end local v1           #metrics:Landroid/util/DisplayMetrics;
    .end local v2           #wm:Landroid/view/WindowManager;
    :cond_3d
    return-void

    #@3e
    .line 72
    .restart local v0       #cliptrayHeight:I
    .restart local v1       #metrics:Landroid/util/DisplayMetrics;
    .restart local v2       #wm:Landroid/view/WindowManager;
    :cond_3e
    iget-object v3, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mRes:Landroid/content/res/Resources;

    #@40
    const v4, 0x20c004c

    #@43
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@46
    move-result v0

    #@47
    goto :goto_33
.end method

.method public showCliptrayCuePopupWindow(Landroid/widget/TextView;ZZ)V
    .registers 11
    .parameter "txt"
    .parameter "showSoftInputOnFocus"
    .parameter "editableText"

    #@0
    .prologue
    .line 82
    iput-object p1, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mTextView:Landroid/widget/TextView;

    #@2
    .line 83
    iput-boolean p2, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mShowSoftInputOnFocus:Z

    #@4
    .line 84
    iput-boolean p3, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mIsEditableText:Z

    #@6
    .line 87
    iget-object v3, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@8
    if-eqz v3, :cond_45

    #@a
    iget-object v3, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mClipManager:Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@c
    invoke-interface {v3}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->getVisibility()I

    #@f
    move-result v3

    #@10
    if-eqz v3, :cond_45

    #@12
    .line 88
    iget-object v3, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mContext:Landroid/content/Context;

    #@14
    const-string/jumbo v4, "window"

    #@17
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1a
    move-result-object v2

    #@1b
    check-cast v2, Landroid/view/WindowManager;

    #@1d
    .line 89
    .local v2, wm:Landroid/view/WindowManager;
    new-instance v1, Landroid/util/DisplayMetrics;

    #@1f
    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    #@22
    .line 90
    .local v1, metrics:Landroid/util/DisplayMetrics;
    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    #@29
    .line 92
    const/4 v0, 0x0

    #@2a
    .line 93
    .local v0, cliptrayHeight:I
    iget v3, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    #@2c
    iget v4, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    #@2e
    if-ge v3, v4, :cond_46

    #@30
    .line 94
    iget-object v3, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mRes:Landroid/content/res/Resources;

    #@32
    const v4, 0x20c004b

    #@35
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@38
    move-result v0

    #@39
    .line 99
    :goto_39
    if-lez v0, :cond_45

    #@3b
    .line 100
    iget-object v3, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mCuePopupWindow:Landroid/widget/PopupWindow;

    #@3d
    iget-object v4, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mTextView:Landroid/widget/TextView;

    #@3f
    const/16 v5, 0x55

    #@41
    const/4 v6, 0x0

    #@42
    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    #@45
    .line 103
    .end local v0           #cliptrayHeight:I
    .end local v1           #metrics:Landroid/util/DisplayMetrics;
    .end local v2           #wm:Landroid/view/WindowManager;
    :cond_45
    return-void

    #@46
    .line 96
    .restart local v0       #cliptrayHeight:I
    .restart local v1       #metrics:Landroid/util/DisplayMetrics;
    .restart local v2       #wm:Landroid/view/WindowManager;
    :cond_46
    iget-object v3, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow;->mRes:Landroid/content/res/Resources;

    #@48
    const v4, 0x20c004c

    #@4b
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@4e
    move-result v0

    #@4f
    goto :goto_39
.end method
