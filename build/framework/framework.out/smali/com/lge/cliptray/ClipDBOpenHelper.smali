.class public Lcom/lge/cliptray/ClipDBOpenHelper;
.super Ljava/lang/Object;
.source "ClipDBOpenHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/cliptray/ClipDBOpenHelper$CreateDB;,
        Lcom/lge/cliptray/ClipDBOpenHelper$DatabaseHelper;
    }
.end annotation


# static fields
.field private static final DATABASE_NAME:Ljava/lang/String; = "clipdata.db"

.field private static final DATABASE_VERSION:I = 0x2


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDBHelper:Lcom/lge/cliptray/ClipDBOpenHelper$DatabaseHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 8
    .parameter "context"

    #@0
    .prologue
    .line 52
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 53
    iput-object p1, p0, Lcom/lge/cliptray/ClipDBOpenHelper;->mContext:Landroid/content/Context;

    #@5
    .line 54
    new-instance v0, Lcom/lge/cliptray/ClipDBOpenHelper$DatabaseHelper;

    #@7
    iget-object v2, p0, Lcom/lge/cliptray/ClipDBOpenHelper;->mContext:Landroid/content/Context;

    #@9
    const-string v3, "clipdata.db"

    #@b
    const/4 v4, 0x0

    #@c
    const/4 v5, 0x2

    #@d
    move-object v1, p0

    #@e
    invoke-direct/range {v0 .. v5}, Lcom/lge/cliptray/ClipDBOpenHelper$DatabaseHelper;-><init>(Lcom/lge/cliptray/ClipDBOpenHelper;Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    #@11
    iput-object v0, p0, Lcom/lge/cliptray/ClipDBOpenHelper;->mDBHelper:Lcom/lge/cliptray/ClipDBOpenHelper$DatabaseHelper;

    #@13
    .line 55
    return-void
.end method


# virtual methods
.method public deleteAll()Z
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 192
    const/4 v1, 0x0

    #@2
    .line 193
    .local v1, doneDelete:I
    iget-object v2, p0, Lcom/lge/cliptray/ClipDBOpenHelper;->mDBHelper:Lcom/lge/cliptray/ClipDBOpenHelper$DatabaseHelper;

    #@4
    invoke-virtual {v2}, Lcom/lge/cliptray/ClipDBOpenHelper$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@7
    move-result-object v0

    #@8
    .line 194
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v2, "clipdb"

    #@a
    invoke-virtual {v0, v2, v3, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@d
    move-result v1

    #@e
    .line 195
    if-lez v1, :cond_12

    #@10
    const/4 v2, 0x1

    #@11
    :goto_11
    return v2

    #@12
    :cond_12
    const/4 v2, 0x0

    #@13
    goto :goto_11
.end method

.method public deleteGlobalPosition(I)V
    .registers 9
    .parameter "delete_pos"

    #@0
    .prologue
    .line 73
    iget-object v2, p0, Lcom/lge/cliptray/ClipDBOpenHelper;->mDBHelper:Lcom/lge/cliptray/ClipDBOpenHelper$DatabaseHelper;

    #@2
    invoke-virtual {v2}, Lcom/lge/cliptray/ClipDBOpenHelper$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@5
    move-result-object v0

    #@6
    .line 74
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    #@9
    .line 76
    :try_start_9
    const-string v2, "clipdb"

    #@b
    const-string v3, "global_position=?"

    #@d
    const/4 v4, 0x1

    #@e
    new-array v4, v4, [Ljava/lang/String;

    #@10
    const/4 v5, 0x0

    #@11
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@14
    move-result-object v6

    #@15
    aput-object v6, v4, v5

    #@17
    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@1a
    .line 77
    new-instance v2, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v3, "UPDATE clipdb SET global_position = global_position-1 WHERE global_position > "

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    #@30
    .line 79
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_33
    .catchall {:try_start_9 .. :try_end_33} :catchall_43
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_33} :catch_37

    #@33
    .line 83
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@36
    .line 85
    :goto_36
    return-void

    #@37
    .line 80
    :catch_37
    move-exception v1

    #@38
    .line 81
    .local v1, e:Landroid/database/sqlite/SQLiteException;
    :try_start_38
    const-string v2, "ClipTray"

    #@3a
    const-string v3, "ClipDBOpenHelper: deleteGlobalPosition failed!"

    #@3c
    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3f
    .catchall {:try_start_38 .. :try_end_3f} :catchall_43

    #@3f
    .line 83
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@42
    goto :goto_36

    #@43
    .end local v1           #e:Landroid/database/sqlite/SQLiteException;
    :catchall_43
    move-exception v2

    #@44
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@47
    throw v2
.end method

.method public getClipdata()Landroid/content/ClipData;
    .registers 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    #@0
    .prologue
    .line 88
    const/4 v11, 0x0

    #@1
    .line 89
    .local v11, clip:Landroid/content/ClipData;
    move-object/from16 v0, p0

    #@3
    iget-object v2, v0, Lcom/lge/cliptray/ClipDBOpenHelper;->mDBHelper:Lcom/lge/cliptray/ClipDBOpenHelper$DatabaseHelper;

    #@5
    invoke-virtual {v2}, Lcom/lge/cliptray/ClipDBOpenHelper$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@8
    move-result-object v1

    #@9
    .line 90
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    const/4 v14, 0x0

    #@a
    .line 92
    .local v14, mCursor:Landroid/database/Cursor;
    const/4 v2, 0x1

    #@b
    :try_start_b
    const-string v3, "clipdb"

    #@d
    const/4 v4, 0x4

    #@e
    new-array v4, v4, [Ljava/lang/String;

    #@10
    const/4 v5, 0x0

    #@11
    const-string v6, "global_position"

    #@13
    aput-object v6, v4, v5

    #@15
    const/4 v5, 0x1

    #@16
    const-string/jumbo v6, "local_position"

    #@19
    aput-object v6, v4, v5

    #@1b
    const/4 v5, 0x2

    #@1c
    const-string v6, "data_type"

    #@1e
    aput-object v6, v4, v5

    #@20
    const/4 v5, 0x3

    #@21
    const-string v6, "data"

    #@23
    aput-object v6, v4, v5

    #@25
    const/4 v5, 0x0

    #@26
    const/4 v6, 0x0

    #@27
    const/4 v7, 0x0

    #@28
    const/4 v8, 0x0

    #@29
    const-string v9, "global_position DESC, local_position ASC"

    #@2b
    const/4 v10, 0x0

    #@2c
    invoke-virtual/range {v1 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2f
    .catchall {:try_start_b .. :try_end_2f} :catchall_d7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_b .. :try_end_2f} :catch_8f

    #@2f
    move-result-object v14

    #@30
    .line 96
    if-nez v14, :cond_3f

    #@32
    .line 124
    if-eqz v14, :cond_3d

    #@34
    invoke-interface {v14}, Landroid/database/Cursor;->isClosed()Z

    #@37
    move-result v2

    #@38
    if-nez v2, :cond_3d

    #@3a
    .line 125
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    #@3d
    :cond_3d
    move-object v12, v11

    #@3e
    .line 128
    .end local v11           #clip:Landroid/content/ClipData;
    .local v12, clip:Landroid/content/ClipData;
    :goto_3e
    return-object v12

    #@3f
    .line 100
    .end local v12           #clip:Landroid/content/ClipData;
    .restart local v11       #clip:Landroid/content/ClipData;
    :cond_3f
    :try_start_3f
    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    #@42
    move-result v2

    #@43
    if-eqz v2, :cond_75

    #@45
    .line 101
    const-string v2, "data_type"

    #@47
    invoke-interface {v14, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@4a
    move-result v16

    #@4b
    .line 102
    .local v16, mIdxDataType:I
    const-string v2, "data"

    #@4d
    invoke-interface {v14, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@50
    move-result v15

    #@51
    .line 104
    .local v15, mIdxData:I
    :cond_51
    move/from16 v0, v16

    #@53
    invoke-interface {v14, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@56
    move-result-object v17

    #@57
    .line 105
    .local v17, type:Ljava/lang/String;
    const-string/jumbo v2, "text"

    #@5a
    move-object/from16 v0, v17

    #@5c
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5f
    move-result v2

    #@60
    if-eqz v2, :cond_a3

    #@62
    .line 106
    if-nez v11, :cond_82

    #@64
    .line 107
    const-string/jumbo v2, "text"

    #@67
    invoke-interface {v14, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@6a
    move-result-object v3

    #@6b
    invoke-static {v2, v3}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    #@6e
    move-result-object v11

    #@6f
    .line 119
    :cond_6f
    :goto_6f
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_72
    .catchall {:try_start_3f .. :try_end_72} :catchall_d7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3f .. :try_end_72} :catch_8f

    #@72
    move-result v2

    #@73
    if-nez v2, :cond_51

    #@75
    .line 124
    .end local v15           #mIdxData:I
    .end local v16           #mIdxDataType:I
    .end local v17           #type:Ljava/lang/String;
    :cond_75
    if-eqz v14, :cond_80

    #@77
    invoke-interface {v14}, Landroid/database/Cursor;->isClosed()Z

    #@7a
    move-result v2

    #@7b
    if-nez v2, :cond_80

    #@7d
    .line 125
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    #@80
    :cond_80
    :goto_80
    move-object v12, v11

    #@81
    .line 128
    .end local v11           #clip:Landroid/content/ClipData;
    .restart local v12       #clip:Landroid/content/ClipData;
    goto :goto_3e

    #@82
    .line 109
    .end local v12           #clip:Landroid/content/ClipData;
    .restart local v11       #clip:Landroid/content/ClipData;
    .restart local v15       #mIdxData:I
    .restart local v16       #mIdxDataType:I
    .restart local v17       #type:Ljava/lang/String;
    :cond_82
    :try_start_82
    new-instance v2, Landroid/content/ClipData$Item;

    #@84
    invoke-interface {v14, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@87
    move-result-object v3

    #@88
    invoke-direct {v2, v3}, Landroid/content/ClipData$Item;-><init>(Ljava/lang/CharSequence;)V

    #@8b
    invoke-virtual {v11, v2}, Landroid/content/ClipData;->addItem(Landroid/content/ClipData$Item;)V
    :try_end_8e
    .catchall {:try_start_82 .. :try_end_8e} :catchall_d7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_82 .. :try_end_8e} :catch_8f

    #@8e
    goto :goto_6f

    #@8f
    .line 121
    .end local v15           #mIdxData:I
    .end local v16           #mIdxDataType:I
    .end local v17           #type:Ljava/lang/String;
    :catch_8f
    move-exception v13

    #@90
    .line 122
    .local v13, e:Landroid/database/sqlite/SQLiteException;
    :try_start_90
    const-string v2, "ClipTray"

    #@92
    const-string v3, "ClipDBOpenHelper: getClipdata  failed!"

    #@94
    invoke-static {v2, v3, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_97
    .catchall {:try_start_90 .. :try_end_97} :catchall_d7

    #@97
    .line 124
    if-eqz v14, :cond_80

    #@99
    invoke-interface {v14}, Landroid/database/Cursor;->isClosed()Z

    #@9c
    move-result v2

    #@9d
    if-nez v2, :cond_80

    #@9f
    .line 125
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    #@a2
    goto :goto_80

    #@a3
    .line 111
    .end local v13           #e:Landroid/database/sqlite/SQLiteException;
    .restart local v15       #mIdxData:I
    .restart local v16       #mIdxDataType:I
    .restart local v17       #type:Ljava/lang/String;
    :cond_a3
    :try_start_a3
    const-string v2, "img"

    #@a5
    move-object/from16 v0, v17

    #@a7
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@aa
    move-result v2

    #@ab
    if-eqz v2, :cond_6f

    #@ad
    .line 112
    if-nez v11, :cond_c6

    #@af
    .line 113
    move-object/from16 v0, p0

    #@b1
    iget-object v2, v0, Lcom/lge/cliptray/ClipDBOpenHelper;->mContext:Landroid/content/Context;

    #@b3
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b6
    move-result-object v2

    #@b7
    const-string v3, "img"

    #@b9
    invoke-interface {v14, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@bc
    move-result-object v4

    #@bd
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@c0
    move-result-object v4

    #@c1
    invoke-static {v2, v3, v4}, Landroid/content/ClipData;->newUri(Landroid/content/ContentResolver;Ljava/lang/CharSequence;Landroid/net/Uri;)Landroid/content/ClipData;

    #@c4
    move-result-object v11

    #@c5
    goto :goto_6f

    #@c6
    .line 116
    :cond_c6
    new-instance v2, Landroid/content/ClipData$Item;

    #@c8
    invoke-interface {v14, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@cb
    move-result-object v3

    #@cc
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@cf
    move-result-object v3

    #@d0
    invoke-direct {v2, v3}, Landroid/content/ClipData$Item;-><init>(Landroid/net/Uri;)V

    #@d3
    invoke-virtual {v11, v2}, Landroid/content/ClipData;->addItem(Landroid/content/ClipData$Item;)V
    :try_end_d6
    .catchall {:try_start_a3 .. :try_end_d6} :catchall_d7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_a3 .. :try_end_d6} :catch_8f

    #@d6
    goto :goto_6f

    #@d7
    .line 124
    .end local v15           #mIdxData:I
    .end local v16           #mIdxDataType:I
    .end local v17           #type:Ljava/lang/String;
    :catchall_d7
    move-exception v2

    #@d8
    if-eqz v14, :cond_e3

    #@da
    invoke-interface {v14}, Landroid/database/Cursor;->isClosed()Z

    #@dd
    move-result v3

    #@de
    if-nez v3, :cond_e3

    #@e0
    .line 125
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    #@e3
    :cond_e3
    throw v2
.end method

.method public getClipdataList()Ljava/util/ArrayList;
    .registers 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ClipData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    #@0
    .prologue
    .line 138
    new-instance v13, Ljava/util/ArrayList;

    #@2
    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 139
    .local v13, clipList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ClipData;>;"
    const/4 v12, 0x0

    #@6
    .line 140
    .local v12, clip:Landroid/content/ClipData;
    move-object/from16 v0, p0

    #@8
    iget-object v3, v0, Lcom/lge/cliptray/ClipDBOpenHelper;->mDBHelper:Lcom/lge/cliptray/ClipDBOpenHelper$DatabaseHelper;

    #@a
    invoke-virtual {v3}, Lcom/lge/cliptray/ClipDBOpenHelper$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@d
    move-result-object v2

    #@e
    .line 141
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    const/16 v17, 0x0

    #@10
    .line 143
    .local v17, mCursor:Landroid/database/Cursor;
    const/4 v3, 0x1

    #@11
    :try_start_11
    const-string v4, "clipdb"

    #@13
    const/4 v5, 0x5

    #@14
    new-array v5, v5, [Ljava/lang/String;

    #@16
    const/4 v6, 0x0

    #@17
    const-string v7, "global_position"

    #@19
    aput-object v7, v5, v6

    #@1b
    const/4 v6, 0x1

    #@1c
    const-string/jumbo v7, "local_position"

    #@1f
    aput-object v7, v5, v6

    #@21
    const/4 v6, 0x2

    #@22
    const-string v7, "data_num"

    #@24
    aput-object v7, v5, v6

    #@26
    const/4 v6, 0x3

    #@27
    const-string v7, "data_type"

    #@29
    aput-object v7, v5, v6

    #@2b
    const/4 v6, 0x4

    #@2c
    const-string v7, "data"

    #@2e
    aput-object v7, v5, v6

    #@30
    const/4 v6, 0x0

    #@31
    const/4 v7, 0x0

    #@32
    const/4 v8, 0x0

    #@33
    const/4 v9, 0x0

    #@34
    const-string v10, "global_position , local_position"

    #@36
    const/4 v11, 0x0

    #@37
    invoke-virtual/range {v2 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_3a
    .catchall {:try_start_11 .. :try_end_3a} :catchall_112
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_11 .. :try_end_3a} :catch_c9

    #@3a
    move-result-object v17

    #@3b
    .line 147
    if-nez v17, :cond_49

    #@3d
    .line 184
    if-eqz v17, :cond_48

    #@3f
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->isClosed()Z

    #@42
    move-result v3

    #@43
    if-nez v3, :cond_48

    #@45
    .line 185
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    #@48
    .line 188
    :cond_48
    :goto_48
    return-object v13

    #@49
    .line 150
    :cond_49
    :try_start_49
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToFirst()Z

    #@4c
    move-result v3

    #@4d
    if-eqz v3, :cond_b0

    #@4f
    .line 151
    const-string v3, "data_num"

    #@51
    move-object/from16 v0, v17

    #@53
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@56
    move-result v20

    #@57
    .line 152
    .local v20, mIdxItemNum:I
    const-string/jumbo v3, "local_position"

    #@5a
    move-object/from16 v0, v17

    #@5c
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@5f
    move-result v21

    #@60
    .line 153
    .local v21, mIdxLocalPos:I
    const-string v3, "data_type"

    #@62
    move-object/from16 v0, v17

    #@64
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@67
    move-result v19

    #@68
    .line 154
    .local v19, mIdxDataType:I
    const-string v3, "data"

    #@6a
    move-object/from16 v0, v17

    #@6c
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@6f
    move-result v18

    #@70
    .line 156
    .local v18, mIdxData:I
    :cond_70
    move-object/from16 v0, v17

    #@72
    move/from16 v1, v20

    #@74
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    #@77
    move-result v15

    #@78
    .line 157
    .local v15, itemNum:I
    move-object/from16 v0, v17

    #@7a
    move/from16 v1, v21

    #@7c
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    #@7f
    move-result v16

    #@80
    .line 158
    .local v16, localPos:I
    move-object/from16 v0, v17

    #@82
    move/from16 v1, v19

    #@84
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@87
    move-result-object v22

    #@88
    .line 160
    .local v22, type:Ljava/lang/String;
    const-string/jumbo v3, "text"

    #@8b
    move-object/from16 v0, v22

    #@8d
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@90
    move-result v3

    #@91
    if-eqz v3, :cond_de

    #@93
    .line 161
    if-nez v12, :cond_bc

    #@95
    .line 162
    const-string/jumbo v3, "text"

    #@98
    invoke-interface/range {v17 .. v18}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@9b
    move-result-object v4

    #@9c
    invoke-static {v3, v4}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    #@9f
    move-result-object v12

    #@a0
    .line 175
    :cond_a0
    :goto_a0
    add-int/lit8 v3, v15, -0x1

    #@a2
    move/from16 v0, v16

    #@a4
    if-ne v0, v3, :cond_aa

    #@a6
    .line 176
    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a9
    .line 177
    const/4 v12, 0x0

    #@aa
    .line 179
    :cond_aa
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_ad
    .catchall {:try_start_49 .. :try_end_ad} :catchall_112
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_49 .. :try_end_ad} :catch_c9

    #@ad
    move-result v3

    #@ae
    if-nez v3, :cond_70

    #@b0
    .line 184
    .end local v15           #itemNum:I
    .end local v16           #localPos:I
    .end local v18           #mIdxData:I
    .end local v19           #mIdxDataType:I
    .end local v20           #mIdxItemNum:I
    .end local v21           #mIdxLocalPos:I
    .end local v22           #type:Ljava/lang/String;
    :cond_b0
    if-eqz v17, :cond_48

    #@b2
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->isClosed()Z

    #@b5
    move-result v3

    #@b6
    if-nez v3, :cond_48

    #@b8
    .line 185
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    #@bb
    goto :goto_48

    #@bc
    .line 164
    .restart local v15       #itemNum:I
    .restart local v16       #localPos:I
    .restart local v18       #mIdxData:I
    .restart local v19       #mIdxDataType:I
    .restart local v20       #mIdxItemNum:I
    .restart local v21       #mIdxLocalPos:I
    .restart local v22       #type:Ljava/lang/String;
    :cond_bc
    :try_start_bc
    new-instance v3, Landroid/content/ClipData$Item;

    #@be
    invoke-interface/range {v17 .. v18}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@c1
    move-result-object v4

    #@c2
    invoke-direct {v3, v4}, Landroid/content/ClipData$Item;-><init>(Ljava/lang/CharSequence;)V

    #@c5
    invoke-virtual {v12, v3}, Landroid/content/ClipData;->addItem(Landroid/content/ClipData$Item;)V
    :try_end_c8
    .catchall {:try_start_bc .. :try_end_c8} :catchall_112
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_bc .. :try_end_c8} :catch_c9

    #@c8
    goto :goto_a0

    #@c9
    .line 181
    .end local v15           #itemNum:I
    .end local v16           #localPos:I
    .end local v18           #mIdxData:I
    .end local v19           #mIdxDataType:I
    .end local v20           #mIdxItemNum:I
    .end local v21           #mIdxLocalPos:I
    .end local v22           #type:Ljava/lang/String;
    :catch_c9
    move-exception v14

    #@ca
    .line 182
    .local v14, e:Landroid/database/sqlite/SQLiteException;
    :try_start_ca
    const-string v3, "ClipTray"

    #@cc
    const-string v4, "ClipDBOpenHelper: getClipdataList failed!"

    #@ce
    invoke-static {v3, v4, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_d1
    .catchall {:try_start_ca .. :try_end_d1} :catchall_112

    #@d1
    .line 184
    if-eqz v17, :cond_48

    #@d3
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->isClosed()Z

    #@d6
    move-result v3

    #@d7
    if-nez v3, :cond_48

    #@d9
    .line 185
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    #@dc
    goto/16 :goto_48

    #@de
    .line 166
    .end local v14           #e:Landroid/database/sqlite/SQLiteException;
    .restart local v15       #itemNum:I
    .restart local v16       #localPos:I
    .restart local v18       #mIdxData:I
    .restart local v19       #mIdxDataType:I
    .restart local v20       #mIdxItemNum:I
    .restart local v21       #mIdxLocalPos:I
    .restart local v22       #type:Ljava/lang/String;
    :cond_de
    :try_start_de
    const-string v3, "img"

    #@e0
    move-object/from16 v0, v22

    #@e2
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e5
    move-result v3

    #@e6
    if-eqz v3, :cond_a0

    #@e8
    .line 167
    if-nez v12, :cond_101

    #@ea
    .line 168
    move-object/from16 v0, p0

    #@ec
    iget-object v3, v0, Lcom/lge/cliptray/ClipDBOpenHelper;->mContext:Landroid/content/Context;

    #@ee
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@f1
    move-result-object v3

    #@f2
    const-string v4, "img"

    #@f4
    invoke-interface/range {v17 .. v18}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@f7
    move-result-object v5

    #@f8
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@fb
    move-result-object v5

    #@fc
    invoke-static {v3, v4, v5}, Landroid/content/ClipData;->newUri(Landroid/content/ContentResolver;Ljava/lang/CharSequence;Landroid/net/Uri;)Landroid/content/ClipData;

    #@ff
    move-result-object v12

    #@100
    goto :goto_a0

    #@101
    .line 171
    :cond_101
    new-instance v3, Landroid/content/ClipData$Item;

    #@103
    invoke-interface/range {v17 .. v18}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@106
    move-result-object v4

    #@107
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@10a
    move-result-object v4

    #@10b
    invoke-direct {v3, v4}, Landroid/content/ClipData$Item;-><init>(Landroid/net/Uri;)V

    #@10e
    invoke-virtual {v12, v3}, Landroid/content/ClipData;->addItem(Landroid/content/ClipData$Item;)V
    :try_end_111
    .catchall {:try_start_de .. :try_end_111} :catchall_112
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_de .. :try_end_111} :catch_c9

    #@111
    goto :goto_a0

    #@112
    .line 184
    .end local v15           #itemNum:I
    .end local v16           #localPos:I
    .end local v18           #mIdxData:I
    .end local v19           #mIdxDataType:I
    .end local v20           #mIdxItemNum:I
    .end local v21           #mIdxLocalPos:I
    .end local v22           #type:Ljava/lang/String;
    :catchall_112
    move-exception v3

    #@113
    if-eqz v17, :cond_11e

    #@115
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->isClosed()Z

    #@118
    move-result v4

    #@119
    if-nez v4, :cond_11e

    #@11b
    .line 185
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    #@11e
    :cond_11e
    throw v3
.end method

.method public hasClipData()Z
    .registers 6

    #@0
    .prologue
    .line 132
    iget-object v3, p0, Lcom/lge/cliptray/ClipDBOpenHelper;->mDBHelper:Lcom/lge/cliptray/ClipDBOpenHelper$DatabaseHelper;

    #@2
    invoke-virtual {v3}, Lcom/lge/cliptray/ClipDBOpenHelper$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@5
    move-result-object v2

    #@6
    .line 133
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v3, "Select exists(select 1 from clipdb);"

    #@8
    const/4 v4, 0x0

    #@9
    invoke-static {v2, v3, v4}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    #@c
    move-result-wide v0

    #@d
    .line 134
    .local v0, count:J
    const-wide/16 v3, 0x1

    #@f
    cmp-long v3, v0, v3

    #@11
    if-nez v3, :cond_15

    #@13
    const/4 v3, 0x1

    #@14
    :goto_14
    return v3

    #@15
    :cond_15
    const/4 v3, 0x0

    #@16
    goto :goto_14
.end method

.method public insert(IIILjava/lang/String;Ljava/lang/String;)Z
    .registers 12
    .parameter "globalpos"
    .parameter "localpos"
    .parameter "itemnum"
    .parameter "datatype"
    .parameter "data"

    #@0
    .prologue
    .line 58
    new-instance v0, Landroid/content/ContentValues;

    #@2
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    #@5
    .line 60
    .local v0, cv:Landroid/content/ContentValues;
    const-string v4, "global_position"

    #@7
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a
    move-result-object v5

    #@b
    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@e
    .line 61
    const-string/jumbo v4, "local_position"

    #@11
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@14
    move-result-object v5

    #@15
    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@18
    .line 62
    const-string v4, "data_num"

    #@1a
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1d
    move-result-object v5

    #@1e
    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@21
    .line 63
    const-string v4, "data_type"

    #@23
    invoke-virtual {v0, v4, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@26
    .line 64
    const-string v4, "data"

    #@28
    invoke-virtual {v0, v4, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2b
    .line 66
    iget-object v4, p0, Lcom/lge/cliptray/ClipDBOpenHelper;->mDBHelper:Lcom/lge/cliptray/ClipDBOpenHelper$DatabaseHelper;

    #@2d
    invoke-virtual {v4}, Lcom/lge/cliptray/ClipDBOpenHelper$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@30
    move-result-object v1

    #@31
    .line 67
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v4, "clipdb"

    #@33
    const/4 v5, 0x0

    #@34
    invoke-virtual {v1, v4, v5, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    #@37
    move-result-wide v2

    #@38
    .line 69
    .local v2, id:J
    const-wide/16 v4, 0x0

    #@3a
    cmp-long v4, v2, v4

    #@3c
    if-lez v4, :cond_40

    #@3e
    const/4 v4, 0x1

    #@3f
    :goto_3f
    return v4

    #@40
    :cond_40
    const/4 v4, 0x0

    #@41
    goto :goto_3f
.end method
