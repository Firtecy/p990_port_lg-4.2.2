.class Lcom/lge/cliptray/CliptrayCuePopupWindow$1;
.super Ljava/lang/Object;
.source "CliptrayCuePopupWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lge/cliptray/CliptrayCuePopupWindow;-><init>(Landroid/content/Context;Lcom/lge/loader/cliptray/ICliptrayManagerLoader;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/cliptray/CliptrayCuePopupWindow;


# direct methods
.method constructor <init>(Lcom/lge/cliptray/CliptrayCuePopupWindow;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 41
    iput-object p1, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow$1;->this$0:Lcom/lge/cliptray/CliptrayCuePopupWindow;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 6
    .parameter "view"

    #@0
    .prologue
    .line 45
    iget-object v1, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow$1;->this$0:Lcom/lge/cliptray/CliptrayCuePopupWindow;

    #@2
    invoke-static {v1}, Lcom/lge/cliptray/CliptrayCuePopupWindow;->access$000(Lcom/lge/cliptray/CliptrayCuePopupWindow;)Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@5
    move-result-object v1

    #@6
    if-eqz v1, :cond_4f

    #@8
    .line 47
    iget-object v1, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow$1;->this$0:Lcom/lge/cliptray/CliptrayCuePopupWindow;

    #@a
    invoke-static {v1}, Lcom/lge/cliptray/CliptrayCuePopupWindow;->access$100(Lcom/lge/cliptray/CliptrayCuePopupWindow;)Landroid/widget/TextView;

    #@d
    move-result-object v1

    #@e
    if-eqz v1, :cond_3d

    #@10
    iget-object v1, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow$1;->this$0:Lcom/lge/cliptray/CliptrayCuePopupWindow;

    #@12
    invoke-static {v1}, Lcom/lge/cliptray/CliptrayCuePopupWindow;->access$100(Lcom/lge/cliptray/CliptrayCuePopupWindow;)Landroid/widget/TextView;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Landroid/widget/TextView;->isTextSelectable()Z

    #@19
    move-result v1

    #@1a
    if-nez v1, :cond_3d

    #@1c
    iget-object v1, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow$1;->this$0:Lcom/lge/cliptray/CliptrayCuePopupWindow;

    #@1e
    invoke-static {v1}, Lcom/lge/cliptray/CliptrayCuePopupWindow;->access$200(Lcom/lge/cliptray/CliptrayCuePopupWindow;)Z

    #@21
    move-result v1

    #@22
    if-eqz v1, :cond_3d

    #@24
    iget-object v1, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow$1;->this$0:Lcom/lge/cliptray/CliptrayCuePopupWindow;

    #@26
    invoke-static {v1}, Lcom/lge/cliptray/CliptrayCuePopupWindow;->access$300(Lcom/lge/cliptray/CliptrayCuePopupWindow;)Z

    #@29
    move-result v1

    #@2a
    if-eqz v1, :cond_3d

    #@2c
    .line 48
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@2f
    move-result-object v0

    #@30
    .line 49
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_3d

    #@32
    .line 50
    iget-object v1, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow$1;->this$0:Lcom/lge/cliptray/CliptrayCuePopupWindow;

    #@34
    invoke-static {v1}, Lcom/lge/cliptray/CliptrayCuePopupWindow;->access$100(Lcom/lge/cliptray/CliptrayCuePopupWindow;)Landroid/widget/TextView;

    #@37
    move-result-object v1

    #@38
    const/4 v2, 0x0

    #@39
    const/4 v3, 0x0

    #@3a
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;ILandroid/os/ResultReceiver;)Z

    #@3d
    .line 54
    .end local v0           #imm:Landroid/view/inputmethod/InputMethodManager;
    :cond_3d
    iget-object v1, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow$1;->this$0:Lcom/lge/cliptray/CliptrayCuePopupWindow;

    #@3f
    invoke-static {v1}, Lcom/lge/cliptray/CliptrayCuePopupWindow;->access$400(Lcom/lge/cliptray/CliptrayCuePopupWindow;)Landroid/widget/PopupWindow;

    #@42
    move-result-object v1

    #@43
    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V

    #@46
    .line 55
    iget-object v1, p0, Lcom/lge/cliptray/CliptrayCuePopupWindow$1;->this$0:Lcom/lge/cliptray/CliptrayCuePopupWindow;

    #@48
    invoke-static {v1}, Lcom/lge/cliptray/CliptrayCuePopupWindow;->access$000(Lcom/lge/cliptray/CliptrayCuePopupWindow;)Lcom/lge/loader/cliptray/ICliptrayManagerLoader;

    #@4b
    move-result-object v1

    #@4c
    invoke-interface {v1}, Lcom/lge/loader/cliptray/ICliptrayManagerLoader;->showCliptraycueClose()V

    #@4f
    .line 57
    :cond_4f
    return-void
.end method
