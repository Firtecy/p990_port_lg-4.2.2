.class public Lcom/lge/wifi_iface/DefaultWifiSapIface;
.super Ljava/lang/Object;
.source "DefaultWifiSapIface.java"

# interfaces
.implements Lcom/lge/wifi_iface/WifiSapIfaceIface;


# static fields
.field private static final TAG:Ljava/lang/String; = "DefaultWifiSapIface"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 24
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 25
    return-void
.end method


# virtual methods
.method public addMacFilterAllowList(Ljava/lang/String;I)Z
    .registers 5
    .parameter "mac"
    .parameter "addORdel"

    #@0
    .prologue
    .line 67
    const-string v0, "DefaultWifiSapIface"

    #@2
    const-string v1, "addMacFilterAllowList"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 68
    const/4 v0, 0x1

    #@8
    return v0
.end method

.method public getAllAssocMacListATT()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 46
    const-string v0, "DefaultWifiSapIface"

    #@2
    const-string v1, "getAllAssocMacListATT"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 47
    const/4 v0, 0x0

    #@8
    return-object v0
.end method

.method public removeAlltheList()I
    .registers 3

    #@0
    .prologue
    .line 77
    const-string v0, "DefaultWifiSapIface"

    #@2
    const-string/jumbo v1, "removeAlltheList"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 78
    const/4 v0, 0x0

    #@9
    return v0
.end method

.method public setMacaddracl(I)Z
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 72
    const-string v0, "DefaultWifiSapIface"

    #@2
    const-string/jumbo v1, "setMacaddracl"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 73
    const/4 v0, 0x1

    #@9
    return v0
.end method

.method public setNstartMonitoring(ZLandroid/net/wifi/WifiConfiguration;II)Z
    .registers 7
    .parameter "bEnable"
    .parameter "wifiConfig"
    .parameter "channel"
    .parameter "maxScb"

    #@0
    .prologue
    .line 36
    const-string v0, "DefaultWifiSapIface"

    #@2
    const-string/jumbo v1, "setNstartMonitoring"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 37
    const/4 v0, 0x1

    #@9
    return v0
.end method

.method public setSoftApWifiCfg(Landroid/net/wifi/WifiConfiguration;II)Z
    .registers 6
    .parameter "wifiConfig"
    .parameter "channel"
    .parameter "maxScb"

    #@0
    .prologue
    .line 41
    const-string v0, "DefaultWifiSapIface"

    #@2
    const-string/jumbo v1, "setSoftApWifiCfg"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 42
    const/4 v0, 0x1

    #@9
    return v0
.end method

.method public setVZWNstartMonitoring(ZLjava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIILjava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;)Z
    .registers 21
    .parameter "bEnable"
    .parameter "ssid"
    .parameter "security_type"
    .parameter "wepkeyindex"
    .parameter "wepkey1"
    .parameter "wepkey2"
    .parameter "wepkey3"
    .parameter "wepkey4"
    .parameter "presharedkey"
    .parameter "hiddenssid"
    .parameter "channel"
    .parameter "maxScb"
    .parameter "hw_mode"
    .parameter "countrycode"
    .parameter "ap_isolate"
    .parameter "ieee_mode"
    .parameter "macaddr_acl"
    .parameter "accept_mac_file"
    .parameter "deny_mac_file"

    #@0
    .prologue
    .line 55
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public setVZWSoftApWifiCfg(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIILjava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Z)Z
    .registers 21
    .parameter "ssid"
    .parameter "security_type"
    .parameter "wepkeyindex"
    .parameter "wepkey1"
    .parameter "wepkey2"
    .parameter "wepkey3"
    .parameter "wepkey4"
    .parameter "presharedkey"
    .parameter "hiddenssid"
    .parameter "channel"
    .parameter "maxScb"
    .parameter "hw_mode"
    .parameter "countrycode"
    .parameter "ap_isolate"
    .parameter "ieee_mode"
    .parameter "macaddr_acl"
    .parameter "accept_mac_file"
    .parameter "deny_mac_file"
    .parameter "bWoNmService"

    #@0
    .prologue
    .line 62
    const/4 v0, 0x1

    #@1
    return v0
.end method
