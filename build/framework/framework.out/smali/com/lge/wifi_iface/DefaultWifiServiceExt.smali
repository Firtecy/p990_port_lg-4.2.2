.class public Lcom/lge/wifi_iface/DefaultWifiServiceExt;
.super Ljava/lang/Object;
.source "DefaultWifiServiceExt.java"

# interfaces
.implements Lcom/lge/wifi_iface/WifiServiceExtIface;


# static fields
.field private static final TAG:Ljava/lang/String; = "DefaultWifiServiceExt"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 34
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 35
    return-void
.end method


# virtual methods
.method public IsWEPSecurity()Z
    .registers 2

    #@0
    .prologue
    .line 334
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public SetMaxNumOfStation(Ljava/lang/String;)V
    .registers 2
    .parameter "AuthType"

    #@0
    .prologue
    .line 243
    return-void
.end method

.method public addOrUpdateNetwork(Landroid/net/wifi/WifiConfiguration;)Z
    .registers 4
    .parameter "config"

    #@0
    .prologue
    .line 72
    const-string v0, "DefaultWifiServiceExt"

    #@2
    const-string v1, "addOrUpdateNetwork"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 73
    const/4 v0, 0x1

    #@8
    return v0
.end method

.method public addWifiConnectionList(II)V
    .registers 3
    .parameter "netId"
    .parameter "success"

    #@0
    .prologue
    .line 281
    return-void
.end method

.method public beShouldUpdatedWifiApConfig(Landroid/net/wifi/WifiConfiguration;)Z
    .registers 3
    .parameter "config"

    #@0
    .prologue
    .line 173
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public broadcastAutoIPIntent(Landroid/net/DhcpInfoInternal;)Landroid/net/DhcpInfoInternal;
    .registers 4
    .parameter "dhcpInfoInternal"

    #@0
    .prologue
    .line 237
    const-string v0, "DefaultWifiServiceExt"

    #@2
    const-string v1, "broadcastAutoIPIntent"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 238
    const/4 v0, 0x0

    #@8
    return-object v0
.end method

.method public changeSsidString(Ljava/lang/String;Z)Ljava/lang/String;
    .registers 6
    .parameter "value"
    .parameter "bQuoted"

    #@0
    .prologue
    .line 82
    move-object v0, p1

    #@1
    .line 84
    .local v0, retString:Ljava/lang/String;
    const-string v1, "DefaultWifiServiceExt"

    #@3
    const-string v2, "changeSsidString: Nothing to do"

    #@5
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 86
    return-object v0
.end method

.method public checkAPSecurity(Ljava/util/List;I)V
    .registers 3
    .parameter
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/WifiConfiguration;",
            ">;I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 106
    .local p1, configList:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    return-void
.end method

.method public checkAutoConnectProfile()V
    .registers 1

    #@0
    .prologue
    .line 309
    return-void
.end method

.method public checkP2pPostfix(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "postfix"

    #@0
    .prologue
    .line 340
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public checkRemovableNetwork(I)Z
    .registers 4
    .parameter "netId"

    #@0
    .prologue
    .line 77
    const-string v0, "DefaultWifiServiceExt"

    #@2
    const-string v1, "checkRemovableNetwork"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 78
    const/4 v0, 0x1

    #@8
    return v0
.end method

.method public checkRssiAndDisconnect(I)Z
    .registers 3
    .parameter "rssi"

    #@0
    .prologue
    .line 264
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public checkVTRunning()Z
    .registers 2

    #@0
    .prologue
    .line 109
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public checkWifiEnableCondition()Z
    .registers 2

    #@0
    .prologue
    .line 148
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public checkWifiStartPossible(Ljava/lang/String;)V
    .registers 2
    .parameter "value"

    #@0
    .prologue
    .line 100
    return-void
.end method

.method public controlManualConnection(IZ)Z
    .registers 4
    .parameter "cmd"
    .parameter "enable"

    #@0
    .prologue
    .line 305
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public fetchSSID()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 212
    const-string v0, "DefaultWifiServiceExt"

    #@2
    const-string v1, "fetchSSID: Nothing to do"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 213
    const/4 v0, 0x0

    #@8
    return-object v0
.end method

.method public getBtWifiState()Z
    .registers 2

    #@0
    .prologue
    .line 328
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getDefaultWifiApConfiguration(Landroid/net/wifi/WifiConfiguration;)Landroid/net/wifi/WifiConfiguration;
    .registers 3
    .parameter "config"

    #@0
    .prologue
    .line 161
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getIsP2pConnected()Z
    .registers 2

    #@0
    .prologue
    .line 300
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getLgeEvent(Ljava/lang/String;)I
    .registers 3
    .parameter "eventName"

    #@0
    .prologue
    .line 270
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getNetworkVariableCommand(ILjava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "netId"
    .parameter "ssidVarName"

    #@0
    .prologue
    .line 201
    move-object v0, p2

    #@1
    .line 202
    .local v0, retString:Ljava/lang/String;
    const-string v1, "DefaultWifiServiceExt"

    #@3
    const-string v2, "getNetworkVariableCommand: Nothing to do"

    #@5
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 203
    return-object v0
.end method

.method public getSecurityType()I
    .registers 2

    #@0
    .prologue
    .line 118
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getShowKTPayPopup()Z
    .registers 2

    #@0
    .prologue
    .line 177
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public getSoftApMaxScb(I)I
    .registers 3
    .parameter "defaultMaxScb"

    #@0
    .prologue
    .line 154
    const/4 v0, -0x1

    #@1
    return v0
.end method

.method public getWiFiPowerSaveModeEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 286
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getWifiActivationWhileCharging()Lcom/lge/wifi_iface/WifiActivationWhileChargingIface;
    .registers 3

    #@0
    .prologue
    .line 67
    const-string v0, "DefaultWifiServiceExt"

    #@2
    const-string v1, "getWifiActivationWhileCharging"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 68
    const/4 v0, 0x0

    #@8
    return-object v0
.end method

.method public getWifiAdhocDisable()Z
    .registers 2

    #@0
    .prologue
    .line 223
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public getWifiApDisableIfNotUsed(Ljava/lang/String;)Lcom/lge/wifi_iface/WifiApDisableIfNotUsedIface;
    .registers 4
    .parameter "softApIface"

    #@0
    .prologue
    .line 62
    const-string v0, "DefaultWifiServiceExt"

    #@2
    const-string v1, "getWifiApDisableIfNotUsed"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 63
    const/4 v0, 0x0

    #@8
    return-object v0
.end method

.method public getWifiDualbandAPConnection()I
    .registers 2

    #@0
    .prologue
    .line 188
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getWifiOffDelayIfNotUsed()Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;
    .registers 3

    #@0
    .prologue
    .line 48
    const-string v0, "DefaultWifiServiceExt"

    #@2
    const-string v1, "getWifiOffDelayIfNotUsed"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 49
    const/4 v0, 0x0

    #@8
    return-object v0
.end method

.method public getWifiP2pOffDelayIfNotUsed()Lcom/lge/wifi_iface/WifiP2pOffDelayIfNotUsedIface;
    .registers 3

    #@0
    .prologue
    .line 53
    const-string v0, "DefaultWifiServiceExt"

    #@2
    const-string v1, "getWifiP2pOffDelayIfNotUsed"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 54
    const/4 v0, 0x0

    #@8
    return-object v0
.end method

.method public getWifiState()I
    .registers 2

    #@0
    .prologue
    .line 291
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public handleLgeEvent(ILjava/lang/String;)V
    .registers 3
    .parameter "event"
    .parameter "remainder"

    #@0
    .prologue
    .line 273
    return-void
.end method

.method public handleN3Event(ILjava/lang/String;)V
    .registers 3
    .parameter "event"
    .parameter "remainder"

    #@0
    .prologue
    .line 124
    return-void
.end method

.method public initWifiSerivceExt(Landroid/content/Context;Landroid/net/wifi/WifiStateMachine;Landroid/net/wifi/WifiNative;Landroid/net/wifi/WifiConfigStoreProxy;Ljava/lang/String;)V
    .registers 8
    .parameter "context"
    .parameter "wifiStateMachine"
    .parameter "wifiNative"
    .parameter "wifiConfigStoreProxy"
    .parameter "wlanInterface"

    #@0
    .prologue
    .line 43
    const-string v0, "DefaultWifiServiceExt"

    #@2
    const-string v1, "initWifiSerivceExt"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 44
    return-void
.end method

.method public isHotspotSSIDKSC5601(Ljava/lang/StringBuilder;)Z
    .registers 3
    .parameter "cmd"

    #@0
    .prologue
    .line 97
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public isProvisioned()Z
    .registers 2

    #@0
    .prologue
    .line 249
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isTethered()Z
    .registers 2

    #@0
    .prologue
    .line 259
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isUPulsMsCHAPv2AP(Ljava/lang/String;)Z
    .registers 3
    .parameter "ssid"

    #@0
    .prologue
    .line 115
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public mappingN3Event(Ljava/lang/String;)I
    .registers 3
    .parameter "eventName"

    #@0
    .prologue
    .line 121
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public processMessageAtDriverStartedState(Landroid/os/Message;)Z
    .registers 3
    .parameter "message"

    #@0
    .prologue
    .line 322
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public sendP2pStateChangedBroadcast(I)V
    .registers 4
    .parameter "newState"

    #@0
    .prologue
    .line 127
    const-string v0, "DefaultWifiServiceExt"

    #@2
    const-string/jumbo v1, "sendP2pStateChangedBroadcast"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 128
    return-void
.end method

.method public setDlnaEnabled(Z)Z
    .registers 4
    .parameter "bEnable"

    #@0
    .prologue
    .line 232
    const-string v0, "DefaultWifiServiceExt"

    #@2
    const-string/jumbo v1, "setDLNAEnabled: Nothing to do"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 233
    const/4 v0, 0x1

    #@9
    return v0
.end method

.method public setKeepAlivePacket(Landroid/net/wifi/WifiInfo;)V
    .registers 2
    .parameter "mWifiInfo"

    #@0
    .prologue
    .line 103
    return-void
.end method

.method public setNetworkVariableCommand(ILjava/lang/String;Ljava/lang/String;)Z
    .registers 6
    .parameter "netId"
    .parameter "ssidVarName"
    .parameter "SSID"

    #@0
    .prologue
    .line 196
    const-string v0, "DefaultWifiServiceExt"

    #@2
    const-string/jumbo v1, "setNetworkVariableCommand: Nothing to do"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 197
    const/4 v0, 0x1

    #@9
    return v0
.end method

.method public setProhibitConnectAP()Z
    .registers 2

    #@0
    .prologue
    .line 112
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public setProvisioned(Z)V
    .registers 2
    .parameter "value"

    #@0
    .prologue
    .line 253
    return-void
.end method

.method public setScanResultsCommand()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 207
    const-string v0, "DefaultWifiServiceExt"

    #@2
    const-string/jumbo v1, "setScanResultsCommand: Nothing to do"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 208
    const/4 v0, 0x0

    #@9
    return-object v0
.end method

.method public setShowKTPayPopup(Z)V
    .registers 2
    .parameter "set"

    #@0
    .prologue
    .line 181
    return-void
.end method

.method public setSoftApDefaultKey(Landroid/net/wifi/WifiConfiguration;)Ljava/lang/String;
    .registers 3
    .parameter "config"

    #@0
    .prologue
    .line 167
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public setSoftApRename(Landroid/content/Context;Landroid/net/wifi/WifiConfiguration;)Ljava/lang/String;
    .registers 4
    .parameter "context"
    .parameter "config"

    #@0
    .prologue
    .line 136
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public setWifiDualbandAPConnection(I)V
    .registers 2
    .parameter "band"

    #@0
    .prologue
    .line 184
    return-void
.end method

.method public setWifiP2pNotificationIcon(Z)V
    .registers 4
    .parameter "visible"

    #@0
    .prologue
    .line 58
    const-string v0, "DefaultWifiServiceExt"

    #@2
    const-string/jumbo v1, "setWifiP2pNotificationIcon"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 59
    return-void
.end method

.method public startWifiDelay()Z
    .registers 2

    #@0
    .prologue
    .line 140
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public startWifiDelaySendMsg()Z
    .registers 2

    #@0
    .prologue
    .line 144
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public syncGetGWSScanResultsList()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/kt/wifiapi/GWSScanResult;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 316
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public unregBrdcastReceiverDelay()V
    .registers 1

    #@0
    .prologue
    .line 132
    return-void
.end method

.method public updateAutoConnectProfile(ILandroid/net/wifi/SupplicantState;)V
    .registers 3
    .parameter "networkId"
    .parameter "state"

    #@0
    .prologue
    .line 312
    return-void
.end method

.method public updateConnectionInfo(Landroid/net/wifi/WifiInfo;)V
    .registers 2
    .parameter "mWifiInfo"

    #@0
    .prologue
    .line 89
    return-void
.end method

.method public updatePriorityNetwork(ILjava/util/List;)V
    .registers 3
    .parameter "netId"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/WifiConfiguration;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 295
    .local p2, configs:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    return-void
.end method

.method public updateRssiAndLinkSpeedNative(IIII)V
    .registers 5
    .parameter "newRssi"
    .parameter "newLinkSpeed"
    .parameter "minRssi"
    .parameter "maxRssi"

    #@0
    .prologue
    .line 92
    move p1, p3

    #@1
    .line 93
    const/4 p2, -0x1

    #@2
    .line 94
    return-void
.end method

.method public waitForEvent()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 217
    const-string v0, "DefaultWifiServiceExt"

    #@2
    const-string/jumbo v1, "waitForEvent: Nothing to do"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 218
    const/4 v0, 0x0

    #@9
    return-object v0
.end method
