.class public Lcom/lge/wifi_iface/DefaultWiFiOffloadingIface;
.super Ljava/lang/Object;
.source "DefaultWiFiOffloadingIface.java"

# interfaces
.implements Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;


# static fields
.field private static final TAG:Ljava/lang/String; = "DefaultWiFiOffloadingIface"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 20
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 21
    return-void
.end method


# virtual methods
.method public disableBackgroundOffloading()V
    .registers 1

    #@0
    .prologue
    .line 34
    return-void
.end method

.method public disableWifioffloadTimerReminder()V
    .registers 1

    #@0
    .prologue
    .line 50
    return-void
.end method

.method public getWifiOffloadingStart()I
    .registers 2

    #@0
    .prologue
    .line 73
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isOffloadingAvailable(Landroid/content/Context;Landroid/content/Intent;)Z
    .registers 4
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 38
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isOffloadingReminder_on()I
    .registers 2

    #@0
    .prologue
    .line 46
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isOffloadingTimer_on()Z
    .registers 2

    #@0
    .prologue
    .line 42
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isWifiOffloadingEnabled()I
    .registers 2

    #@0
    .prologue
    .line 66
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public processingOffloading()V
    .registers 1

    #@0
    .prologue
    .line 30
    return-void
.end method

.method public resetWifioffloadTimerReminder(I)Z
    .registers 3
    .parameter "check"

    #@0
    .prologue
    .line 58
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public setWifiOffloadOngoing(Z)V
    .registers 2
    .parameter "OffloadOngoing"

    #@0
    .prologue
    .line 62
    return-void
.end method

.method public setWifiOffloadingStart(I)V
    .registers 2
    .parameter "WiFiOffloadingStart"

    #@0
    .prologue
    .line 70
    return-void
.end method

.method public startService(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 25
    const-string v0, "DefaultWiFiOffloadingIface"

    #@2
    const-string/jumbo v1, "startService"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 26
    return-void
.end method

.method public stopWifioffloadTimer()V
    .registers 1

    #@0
    .prologue
    .line 54
    return-void
.end method
