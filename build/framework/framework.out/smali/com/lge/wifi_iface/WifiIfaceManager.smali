.class public Lcom/lge/wifi_iface/WifiIfaceManager;
.super Ljava/lang/Object;
.source "WifiIfaceManager.java"


# static fields
.field private static sWiFiAggregationIfaceIface:Lcom/lge/wifi_iface/WiFiAggregationIfaceIface;

.field private static sWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

.field private static sWifiExtServiceIface:Lcom/lge/wifi_iface/WifiExtSvcIfaceIface;

.field private static sWifiMHPIfaceIface:Lcom/lge/wifi_iface/WifiMHPIfaceIface;

.field private static sWifiSapIfaceIface:Lcom/lge/wifi_iface/WifiSapIfaceIface;

.field private static sWifiServiceExtIface:Lcom/lge/wifi_iface/WifiServiceExtIface;


# direct methods
.method static constructor <clinit>()V
    .registers 10

    #@0
    .prologue
    .line 10
    new-instance v7, Lcom/lge/wifi_iface/DefaultWifiExtServiceIface;

    #@2
    invoke-direct {v7}, Lcom/lge/wifi_iface/DefaultWifiExtServiceIface;-><init>()V

    #@5
    sput-object v7, Lcom/lge/wifi_iface/WifiIfaceManager;->sWifiExtServiceIface:Lcom/lge/wifi_iface/WifiExtSvcIfaceIface;

    #@7
    .line 11
    new-instance v7, Lcom/lge/wifi_iface/DefaultWifiServiceExt;

    #@9
    invoke-direct {v7}, Lcom/lge/wifi_iface/DefaultWifiServiceExt;-><init>()V

    #@c
    sput-object v7, Lcom/lge/wifi_iface/WifiIfaceManager;->sWifiServiceExtIface:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@e
    .line 12
    new-instance v7, Lcom/lge/wifi_iface/DefaultWifiSapIface;

    #@10
    invoke-direct {v7}, Lcom/lge/wifi_iface/DefaultWifiSapIface;-><init>()V

    #@13
    sput-object v7, Lcom/lge/wifi_iface/WifiIfaceManager;->sWifiSapIfaceIface:Lcom/lge/wifi_iface/WifiSapIfaceIface;

    #@15
    .line 13
    new-instance v7, Lcom/lge/wifi_iface/DefaultWifiMHPIface;

    #@17
    invoke-direct {v7}, Lcom/lge/wifi_iface/DefaultWifiMHPIface;-><init>()V

    #@1a
    sput-object v7, Lcom/lge/wifi_iface/WifiIfaceManager;->sWifiMHPIfaceIface:Lcom/lge/wifi_iface/WifiMHPIfaceIface;

    #@1c
    .line 15
    new-instance v7, Lcom/lge/wifi_iface/DefaultWiFiAggregationIface;

    #@1e
    invoke-direct {v7}, Lcom/lge/wifi_iface/DefaultWiFiAggregationIface;-><init>()V

    #@21
    sput-object v7, Lcom/lge/wifi_iface/WifiIfaceManager;->sWiFiAggregationIfaceIface:Lcom/lge/wifi_iface/WiFiAggregationIfaceIface;

    #@23
    .line 17
    new-instance v7, Lcom/lge/wifi_iface/DefaultWiFiOffloadingIface;

    #@25
    invoke-direct {v7}, Lcom/lge/wifi_iface/DefaultWiFiOffloadingIface;-><init>()V

    #@28
    sput-object v7, Lcom/lge/wifi_iface/WifiIfaceManager;->sWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@2a
    .line 19
    new-instance v0, Ldalvik/system/PathClassLoader;

    #@2c
    const-string v7, "/system/framework/com.lge.wifiext.jar"

    #@2e
    const-class v8, Lcom/lge/wifi_iface/WifiIfaceManager;

    #@30
    invoke-virtual {v8}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    #@33
    move-result-object v8

    #@34
    invoke-direct {v0, v7, v8}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    #@37
    .line 22
    .local v0, cloader:Ljava/lang/ClassLoader;
    :try_start_37
    const-string v7, "com.lge.wifiext.WifiExtServiceIface"

    #@39
    const/4 v8, 0x1

    #@3a
    invoke-static {v7, v8, v0}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    #@3d
    move-result-object v6

    #@3e
    .line 23
    .local v6, wifiExtServiceClass:Ljava/lang/Class;
    invoke-virtual {v6}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    #@41
    move-result-object v7

    #@42
    check-cast v7, Lcom/lge/wifi_iface/WifiExtSvcIfaceIface;

    #@44
    sput-object v7, Lcom/lge/wifi_iface/WifiIfaceManager;->sWifiExtServiceIface:Lcom/lge/wifi_iface/WifiExtSvcIfaceIface;

    #@46
    .line 25
    const-string v7, "com.lge.wifiext.WifiServiceExt"

    #@48
    const/4 v8, 0x1

    #@49
    invoke-static {v7, v8, v0}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    #@4c
    move-result-object v2

    #@4d
    .line 32
    .local v2, extClass:Ljava/lang/Class;
    const-string v7, "com.lge.wifiext.wifiSap.WifiSapIface"

    #@4f
    const/4 v8, 0x1

    #@50
    invoke-static {v7, v8, v0}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    #@53
    move-result-object v5

    #@54
    .line 39
    .local v5, sapIfaceClass:Ljava/lang/Class;
    const-string v7, "com.lge.wifiext.mobilehotspot.WifiMHPIface"

    #@56
    const/4 v8, 0x1

    #@57
    invoke-static {v7, v8, v0}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    #@5a
    move-result-object v3

    #@5b
    .line 46
    .local v3, mhpIfaceClass:Ljava/lang/Class;
    const-string v7, "com.lge.wifiext.offloading.WiFiOffloadingIface"

    #@5d
    const/4 v8, 0x1

    #@5e
    invoke-static {v7, v8, v0}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    #@61
    move-result-object v4

    #@62
    .line 54
    .local v4, offloadingIfaceClass:Ljava/lang/Class;
    const-string v7, "com.lge.wifiext.aggregation.WiFiAggregationIface"

    #@64
    const/4 v8, 0x1

    #@65
    invoke-static {v7, v8, v0}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;
    :try_end_68
    .catch Ljava/lang/Exception; {:try_start_37 .. :try_end_68} :catch_69

    #@68
    .line 64
    .end local v2           #extClass:Ljava/lang/Class;
    .end local v3           #mhpIfaceClass:Ljava/lang/Class;
    .end local v4           #offloadingIfaceClass:Ljava/lang/Class;
    .end local v5           #sapIfaceClass:Ljava/lang/Class;
    .end local v6           #wifiExtServiceClass:Ljava/lang/Class;
    :goto_68
    return-void

    #@69
    .line 61
    :catch_69
    move-exception v1

    #@6a
    .line 62
    .local v1, e:Ljava/lang/Exception;
    const-string v7, "WifiIfaceManager"

    #@6c
    new-instance v8, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    const-string v9, "Class not found: "

    #@73
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v8

    #@77
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v8

    #@7b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v8

    #@7f
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@82
    goto :goto_68
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 66
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 67
    return-void
.end method

.method public static getWiFiAggregationIfaceIface()Lcom/lge/wifi_iface/WiFiAggregationIfaceIface;
    .registers 1

    #@0
    .prologue
    .line 131
    sget-object v0, Lcom/lge/wifi_iface/WifiIfaceManager;->sWiFiAggregationIfaceIface:Lcom/lge/wifi_iface/WiFiAggregationIfaceIface;

    #@2
    return-object v0
.end method

.method public static getWiFiOffloadingIfaceIface()Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;
    .registers 1

    #@0
    .prologue
    .line 117
    sget-object v0, Lcom/lge/wifi_iface/WifiIfaceManager;->sWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@2
    return-object v0
.end method

.method public static getWifiExtServiceIface()Lcom/lge/wifi_iface/WifiExtSvcIfaceIface;
    .registers 1

    #@0
    .prologue
    .line 74
    sget-object v0, Lcom/lge/wifi_iface/WifiIfaceManager;->sWifiExtServiceIface:Lcom/lge/wifi_iface/WifiExtSvcIfaceIface;

    #@2
    return-object v0
.end method

.method public static getWifiMHPIfaceIface()Lcom/lge/wifi_iface/WifiMHPIfaceIface;
    .registers 1

    #@0
    .prologue
    .line 105
    sget-object v0, Lcom/lge/wifi_iface/WifiIfaceManager;->sWifiMHPIfaceIface:Lcom/lge/wifi_iface/WifiMHPIfaceIface;

    #@2
    return-object v0
.end method

.method public static getWifiSapIfaceIface()Lcom/lge/wifi_iface/WifiSapIfaceIface;
    .registers 1

    #@0
    .prologue
    .line 93
    sget-object v0, Lcom/lge/wifi_iface/WifiIfaceManager;->sWifiSapIfaceIface:Lcom/lge/wifi_iface/WifiSapIfaceIface;

    #@2
    return-object v0
.end method

.method public static getWifiServiceExtIface()Lcom/lge/wifi_iface/WifiServiceExtIface;
    .registers 1

    #@0
    .prologue
    .line 81
    sget-object v0, Lcom/lge/wifi_iface/WifiIfaceManager;->sWifiServiceExtIface:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@2
    return-object v0
.end method

.method public static setWiFiAggregationIfaceIface(Lcom/lge/wifi_iface/WiFiAggregationIfaceIface;)V
    .registers 1
    .parameter "iface"

    #@0
    .prologue
    .line 135
    sput-object p0, Lcom/lge/wifi_iface/WifiIfaceManager;->sWiFiAggregationIfaceIface:Lcom/lge/wifi_iface/WiFiAggregationIfaceIface;

    #@2
    .line 136
    return-void
.end method

.method public static setWiFiOffloadingIfaceIface(Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;)V
    .registers 1
    .parameter "iface"

    #@0
    .prologue
    .line 121
    sput-object p0, Lcom/lge/wifi_iface/WifiIfaceManager;->sWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@2
    .line 122
    return-void
.end method

.method public static setWifiMHPIfaceIface(Lcom/lge/wifi_iface/WifiMHPIfaceIface;)V
    .registers 1
    .parameter "iface"

    #@0
    .prologue
    .line 109
    sput-object p0, Lcom/lge/wifi_iface/WifiIfaceManager;->sWifiMHPIfaceIface:Lcom/lge/wifi_iface/WifiMHPIfaceIface;

    #@2
    .line 110
    return-void
.end method

.method public static setWifiSapIfaceIface(Lcom/lge/wifi_iface/WifiSapIfaceIface;)V
    .registers 1
    .parameter "iface"

    #@0
    .prologue
    .line 97
    sput-object p0, Lcom/lge/wifi_iface/WifiIfaceManager;->sWifiSapIfaceIface:Lcom/lge/wifi_iface/WifiSapIfaceIface;

    #@2
    .line 98
    return-void
.end method

.method public static setWifiServiceExtIface(Lcom/lge/wifi_iface/WifiServiceExtIface;)V
    .registers 1
    .parameter "iface"

    #@0
    .prologue
    .line 85
    sput-object p0, Lcom/lge/wifi_iface/WifiIfaceManager;->sWifiServiceExtIface:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@2
    .line 86
    return-void
.end method
