.class public Lcom/qualcomm/sensor/QCSensor;
.super Ljava/lang/Object;
.source "QCSensor.java"

# interfaces
.implements Lcom/qualcomm/snapdragon/util/QCCapabilitiesInterface;


# static fields
.field public static BASIC_GESTURE_PULL_V01:I

.field public static BASIC_GESTURE_PUSH_V01:I

.field public static BASIC_GESTURE_SHAKE_AXIS_BOTTOM_V01:I

.field public static BASIC_GESTURE_SHAKE_AXIS_LEFT_V01:I

.field public static BASIC_GESTURE_SHAKE_AXIS_RIGHT_V01:I

.field public static BASIC_GESTURE_SHAKE_AXIS_TOP_V01:I

.field public static FACING_DOWN_V01:I

.field public static FACING_UP_V01:I

.field public static GYRO_TAP_BOTTOM_V01:I

.field public static GYRO_TAP_LEFT_V01:I

.field public static GYRO_TAP_RIGHT_V01:I

.field public static GYRO_TAP_TOP_V01:I

.field private static KEY_EVENT_TYPES:Ljava/lang/String;

.field private static KEY_SENSOR_TYPES:Ljava/lang/String;

.field private static QC_SENSOR_TYPE_BASE:I

.field public static SENSOR_TYPE_BASIC_GESTURES:I

.field public static SENSOR_TYPE_FACING:I

.field public static SENSOR_TYPE_TAP:I

.field public static SENSOR_TYPE_TILT:I


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x4

    #@1
    const/4 v3, 0x3

    #@2
    const/4 v2, 0x2

    #@3
    const/4 v1, 0x1

    #@4
    .line 36
    const-string/jumbo v0, "key_sensor_types"

    #@7
    sput-object v0, Lcom/qualcomm/sensor/QCSensor;->KEY_SENSOR_TYPES:Ljava/lang/String;

    #@9
    .line 37
    const-string/jumbo v0, "key_event_types"

    #@c
    sput-object v0, Lcom/qualcomm/sensor/QCSensor;->KEY_EVENT_TYPES:Ljava/lang/String;

    #@e
    .line 39
    const v0, 0x1fa2638

    #@11
    sput v0, Lcom/qualcomm/sensor/QCSensor;->QC_SENSOR_TYPE_BASE:I

    #@13
    .line 42
    sget v0, Lcom/qualcomm/sensor/QCSensor;->QC_SENSOR_TYPE_BASE:I

    #@15
    sput v0, Lcom/qualcomm/sensor/QCSensor;->SENSOR_TYPE_BASIC_GESTURES:I

    #@17
    .line 43
    sget v0, Lcom/qualcomm/sensor/QCSensor;->QC_SENSOR_TYPE_BASE:I

    #@19
    add-int/lit8 v0, v0, 0x1

    #@1b
    sput v0, Lcom/qualcomm/sensor/QCSensor;->SENSOR_TYPE_TAP:I

    #@1d
    .line 44
    sget v0, Lcom/qualcomm/sensor/QCSensor;->QC_SENSOR_TYPE_BASE:I

    #@1f
    add-int/lit8 v0, v0, 0x2

    #@21
    sput v0, Lcom/qualcomm/sensor/QCSensor;->SENSOR_TYPE_FACING:I

    #@23
    .line 45
    sget v0, Lcom/qualcomm/sensor/QCSensor;->QC_SENSOR_TYPE_BASE:I

    #@25
    add-int/lit8 v0, v0, 0x3

    #@27
    sput v0, Lcom/qualcomm/sensor/QCSensor;->SENSOR_TYPE_TILT:I

    #@29
    .line 50
    sput v1, Lcom/qualcomm/sensor/QCSensor;->BASIC_GESTURE_PUSH_V01:I

    #@2b
    .line 51
    sput v2, Lcom/qualcomm/sensor/QCSensor;->BASIC_GESTURE_PULL_V01:I

    #@2d
    .line 52
    sput v3, Lcom/qualcomm/sensor/QCSensor;->BASIC_GESTURE_SHAKE_AXIS_LEFT_V01:I

    #@2f
    .line 53
    sput v4, Lcom/qualcomm/sensor/QCSensor;->BASIC_GESTURE_SHAKE_AXIS_RIGHT_V01:I

    #@31
    .line 54
    const/4 v0, 0x5

    #@32
    sput v0, Lcom/qualcomm/sensor/QCSensor;->BASIC_GESTURE_SHAKE_AXIS_TOP_V01:I

    #@34
    .line 55
    const/4 v0, 0x6

    #@35
    sput v0, Lcom/qualcomm/sensor/QCSensor;->BASIC_GESTURE_SHAKE_AXIS_BOTTOM_V01:I

    #@37
    .line 58
    sput v1, Lcom/qualcomm/sensor/QCSensor;->GYRO_TAP_LEFT_V01:I

    #@39
    .line 59
    sput v2, Lcom/qualcomm/sensor/QCSensor;->GYRO_TAP_RIGHT_V01:I

    #@3b
    .line 60
    sput v3, Lcom/qualcomm/sensor/QCSensor;->GYRO_TAP_TOP_V01:I

    #@3d
    .line 61
    sput v4, Lcom/qualcomm/sensor/QCSensor;->GYRO_TAP_BOTTOM_V01:I

    #@3f
    .line 64
    sput v1, Lcom/qualcomm/sensor/QCSensor;->FACING_UP_V01:I

    #@41
    .line 65
    sput v2, Lcom/qualcomm/sensor/QCSensor;->FACING_DOWN_V01:I

    #@43
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 34
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public getCapabilities()Landroid/os/Bundle;
    .registers 6

    #@0
    .prologue
    .line 90
    new-instance v1, Landroid/os/Bundle;

    #@2
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    #@5
    .line 91
    .local v1, constantFieldBundle:Landroid/os/Bundle;
    new-instance v3, Ljava/util/ArrayList;

    #@7
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@a
    .line 92
    .local v3, sensorTypesList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v4, "SENSOR_TYPE_BASIC_GESTURES"

    #@c
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@f
    .line 93
    const-string v4, "SENSOR_TYPE_TAP"

    #@11
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@14
    .line 94
    const-string v4, "SENSOR_TYPE_FACING"

    #@16
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@19
    .line 95
    const-string v4, "SENSOR_TYPE_TILT"

    #@1b
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1e
    .line 97
    sget-object v4, Lcom/qualcomm/sensor/QCSensor;->KEY_SENSOR_TYPES:Ljava/lang/String;

    #@20
    invoke-virtual {v1, v4, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    #@23
    .line 99
    new-instance v2, Ljava/util/ArrayList;

    #@25
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@28
    .line 100
    .local v2, eventTypesList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v4, "BASIC_GESTURE_PUSH_V01"

    #@2a
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2d
    .line 101
    const-string v4, "BASIC_GESTURE_PULL_V01"

    #@2f
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@32
    .line 102
    const-string v4, "BASIC_GESTURE_SHAKE_AXIS_LEFT_V01"

    #@34
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@37
    .line 103
    const-string v4, "BASIC_GESTURE_SHAKE_AXIS_RIGHT_V01"

    #@39
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3c
    .line 104
    const-string v4, "BASIC_GESTURE_SHAKE_AXIS_TOP_V01"

    #@3e
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@41
    .line 105
    const-string v4, "BASIC_GESTURE_SHAKE_AXIS_BOTTOM_V01"

    #@43
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@46
    .line 106
    const-string v4, "GYRO_TAP_LEFT_V01"

    #@48
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4b
    .line 107
    const-string v4, "GYRO_TAP_RIGHT_V01"

    #@4d
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@50
    .line 108
    const-string v4, "GYRO_TAP_TOP_V01"

    #@52
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@55
    .line 109
    const-string v4, "GYRO_TAP_BOTTOM_V01"

    #@57
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5a
    .line 110
    const-string v4, "FACING_UP_V01"

    #@5c
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5f
    .line 111
    const-string v4, "FACING_DOWN_V01"

    #@61
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@64
    .line 113
    sget-object v4, Lcom/qualcomm/sensor/QCSensor;->KEY_EVENT_TYPES:Ljava/lang/String;

    #@66
    invoke-virtual {v1, v4, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    #@69
    .line 115
    new-instance v0, Landroid/os/Bundle;

    #@6b
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@6e
    .line 116
    .local v0, capabilitiesBundle:Landroid/os/Bundle;
    const-string/jumbo v4, "key_constant_field_values"

    #@71
    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    #@74
    .line 118
    return-object v0
.end method
