.class public Lcom/qualcomm/camera/QCFace;
.super Landroid/hardware/Camera$Face;
.source "QCFace.java"

# interfaces
.implements Lcom/qualcomm/snapdragon/util/QCCapabilitiesInterface;


# static fields
.field private static final BUNDLE_KEY_BLINK_DETECTED:Ljava/lang/String; = "blinkDetected"

.field private static final BUNDLE_KEY_FACE_PITCH_DEGREE:Ljava/lang/String; = "facePitchDegree"

.field private static final BUNDLE_KEY_FACE_RECOGNIZED:Ljava/lang/String; = "faceRecognized"

.field private static final BUNDLE_KEY_FACE_ROLL_DEGREE:Ljava/lang/String; = "faceRollDegree"

.field private static final BUNDLE_KEY_FACE_YAW_DEGREE:Ljava/lang/String; = "faceYawDegree"

.field private static final BUNDLE_KEY_GAZE_LEFT_RIGHT_DEGREE:Ljava/lang/String; = "gazeLeftRightDegree"

.field private static final BUNDLE_KEY_GAZE_UP_DOWN_DEGREE:Ljava/lang/String; = "gazeUpDownDegree"

.field private static final BUNDLE_KEY_LEFT_EYE_CLOSED_VALUE:Ljava/lang/String; = "leftEyeClosedValue"

.field private static final BUNDLE_KEY_RIGHT_EYE_CLOSED_VALUE:Ljava/lang/String; = "rightEyeClosedValue"

.field private static final BUNDLE_KEY_SMILE_SCORE:Ljava/lang/String; = "smileScore"

.field private static final BUNDLE_KEY_SMILE_VALUE:Ljava/lang/String; = "smileValue"

.field private static final STR_FACIAL_PROCESSING:Ljava/lang/String; = "ro.qc.sdk.camera.facialproc"

.field private static final STR_FALSE:Ljava/lang/String; = "false"

.field private static final STR_TRUE:Ljava/lang/String; = "true"


# instance fields
.field private blinkDetected:I

.field private faceRecognized:I

.field private gazeAngle:I

.field private leftrightDir:I

.field private leftrightGaze:I

.field private leyeBlink:I

.field private reyeBlink:I

.field private rollDir:I

.field private smileDegree:I

.field private smileScore:I

.field private topbottomGaze:I

.field private updownDir:I


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 58
    invoke-direct {p0}, Landroid/hardware/Camera$Face;-><init>()V

    #@4
    .line 61
    iput v0, p0, Lcom/qualcomm/camera/QCFace;->smileDegree:I

    #@6
    .line 62
    iput v0, p0, Lcom/qualcomm/camera/QCFace;->smileScore:I

    #@8
    .line 63
    iput v0, p0, Lcom/qualcomm/camera/QCFace;->blinkDetected:I

    #@a
    .line 64
    iput v0, p0, Lcom/qualcomm/camera/QCFace;->faceRecognized:I

    #@c
    .line 65
    iput v0, p0, Lcom/qualcomm/camera/QCFace;->gazeAngle:I

    #@e
    .line 66
    iput v0, p0, Lcom/qualcomm/camera/QCFace;->updownDir:I

    #@10
    .line 67
    iput v0, p0, Lcom/qualcomm/camera/QCFace;->leftrightDir:I

    #@12
    .line 68
    iput v0, p0, Lcom/qualcomm/camera/QCFace;->rollDir:I

    #@14
    .line 69
    iput v0, p0, Lcom/qualcomm/camera/QCFace;->leyeBlink:I

    #@16
    .line 70
    iput v0, p0, Lcom/qualcomm/camera/QCFace;->reyeBlink:I

    #@18
    .line 71
    iput v0, p0, Lcom/qualcomm/camera/QCFace;->leftrightGaze:I

    #@1a
    .line 72
    iput v0, p0, Lcom/qualcomm/camera/QCFace;->topbottomGaze:I

    #@1c
    .line 59
    return-void
.end method


# virtual methods
.method public getBlinkDetected()I
    .registers 2

    #@0
    .prologue
    .line 102
    iget v0, p0, Lcom/qualcomm/camera/QCFace;->blinkDetected:I

    #@2
    return v0
.end method

.method public getCapabilities()Landroid/os/Bundle;
    .registers 5

    #@0
    .prologue
    .line 220
    const-string/jumbo v3, "ro.qc.sdk.camera.facialproc"

    #@3
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v2

    #@7
    .line 222
    .local v2, propertyVal:Ljava/lang/String;
    const/4 v1, 0x0

    #@8
    .line 223
    .local v1, methodList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string/jumbo v3, "true"

    #@b
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_57

    #@11
    .line 225
    new-instance v1, Ljava/util/ArrayList;

    #@13
    .end local v1           #methodList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@16
    .line 226
    .restart local v1       #methodList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v3, "getSmileDegree"

    #@18
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1b
    .line 227
    const-string v3, "getSmileScore"

    #@1d
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@20
    .line 228
    const-string v3, "getBlinkDetected"

    #@22
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@25
    .line 229
    const-string v3, "getFaceRecognized"

    #@27
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2a
    .line 230
    const-string v3, "getGazeAngle"

    #@2c
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2f
    .line 231
    const-string v3, "getUpDownDirection"

    #@31
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@34
    .line 232
    const-string v3, "getLeftRightDirection"

    #@36
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@39
    .line 233
    const-string v3, "getRollDirection"

    #@3b
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3e
    .line 234
    const-string v3, "getLeftRightGazeDegree"

    #@40
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@43
    .line 235
    const-string v3, "getTopBottomGazeDegree"

    #@45
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@48
    .line 236
    const-string v3, "getLeftEyeBlinkDegree"

    #@4a
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4d
    .line 237
    const-string v3, "getRightEyeBlinkDegree"

    #@4f
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@52
    .line 238
    const-string v3, "getQCFaceInfo"

    #@54
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@57
    .line 241
    :cond_57
    new-instance v0, Landroid/os/Bundle;

    #@59
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@5c
    .line 242
    .local v0, capabilitiesBundle:Landroid/os/Bundle;
    const-string/jumbo v3, "key_active_method_names"

    #@5f
    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    #@62
    .line 245
    return-object v0
.end method

.method public getFaceRecognized()I
    .registers 2

    #@0
    .prologue
    .line 111
    iget v0, p0, Lcom/qualcomm/camera/QCFace;->faceRecognized:I

    #@2
    return v0
.end method

.method public getGazeAngle()I
    .registers 2

    #@0
    .prologue
    .line 120
    iget v0, p0, Lcom/qualcomm/camera/QCFace;->gazeAngle:I

    #@2
    return v0
.end method

.method public getLeftEyeBlinkDegree()I
    .registers 2

    #@0
    .prologue
    .line 156
    iget v0, p0, Lcom/qualcomm/camera/QCFace;->leyeBlink:I

    #@2
    return v0
.end method

.method public getLeftRightDirection()I
    .registers 2

    #@0
    .prologue
    .line 138
    iget v0, p0, Lcom/qualcomm/camera/QCFace;->leftrightDir:I

    #@2
    return v0
.end method

.method public getLeftRightGazeDegree()I
    .registers 2

    #@0
    .prologue
    .line 174
    iget v0, p0, Lcom/qualcomm/camera/QCFace;->leftrightGaze:I

    #@2
    return v0
.end method

.method public getQCFaceInfo()Landroid/os/Bundle;
    .registers 4

    #@0
    .prologue
    .line 199
    new-instance v0, Landroid/os/Bundle;

    #@2
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@5
    .line 200
    .local v0, faceInfo:Landroid/os/Bundle;
    const-string/jumbo v1, "smileValue"

    #@8
    iget v2, p0, Lcom/qualcomm/camera/QCFace;->smileDegree:I

    #@a
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@d
    .line 202
    const-string/jumbo v1, "leftEyeClosedValue"

    #@10
    iget v2, p0, Lcom/qualcomm/camera/QCFace;->leyeBlink:I

    #@12
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@15
    .line 203
    const-string/jumbo v1, "rightEyeClosedValue"

    #@18
    iget v2, p0, Lcom/qualcomm/camera/QCFace;->reyeBlink:I

    #@1a
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@1d
    .line 205
    const-string v1, "facePitchDegree"

    #@1f
    iget v2, p0, Lcom/qualcomm/camera/QCFace;->updownDir:I

    #@21
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@24
    .line 206
    const-string v1, "faceYawDegree"

    #@26
    iget v2, p0, Lcom/qualcomm/camera/QCFace;->leftrightDir:I

    #@28
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@2b
    .line 207
    const-string v1, "faceRollDegree"

    #@2d
    iget v2, p0, Lcom/qualcomm/camera/QCFace;->rollDir:I

    #@2f
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@32
    .line 208
    const-string v1, "gazeUpDownDegree"

    #@34
    iget v2, p0, Lcom/qualcomm/camera/QCFace;->topbottomGaze:I

    #@36
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@39
    .line 209
    const-string v1, "gazeLeftRightDegree"

    #@3b
    iget v2, p0, Lcom/qualcomm/camera/QCFace;->leftrightGaze:I

    #@3d
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@40
    .line 211
    const-string v1, "blinkDetected"

    #@42
    iget v2, p0, Lcom/qualcomm/camera/QCFace;->blinkDetected:I

    #@44
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@47
    .line 212
    const-string/jumbo v1, "smileScore"

    #@4a
    iget v2, p0, Lcom/qualcomm/camera/QCFace;->smileScore:I

    #@4c
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@4f
    .line 213
    const-string v1, "faceRecognized"

    #@51
    iget v2, p0, Lcom/qualcomm/camera/QCFace;->faceRecognized:I

    #@53
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@56
    .line 215
    return-object v0
.end method

.method public getRightEyeBlinkDegree()I
    .registers 2

    #@0
    .prologue
    .line 165
    iget v0, p0, Lcom/qualcomm/camera/QCFace;->reyeBlink:I

    #@2
    return v0
.end method

.method public getRollDirection()I
    .registers 2

    #@0
    .prologue
    .line 147
    iget v0, p0, Lcom/qualcomm/camera/QCFace;->rollDir:I

    #@2
    return v0
.end method

.method public getSmileDegree()I
    .registers 2

    #@0
    .prologue
    .line 84
    iget v0, p0, Lcom/qualcomm/camera/QCFace;->smileDegree:I

    #@2
    return v0
.end method

.method public getSmileScore()I
    .registers 2

    #@0
    .prologue
    .line 93
    iget v0, p0, Lcom/qualcomm/camera/QCFace;->smileScore:I

    #@2
    return v0
.end method

.method public getTopBottomGazeDegree()I
    .registers 2

    #@0
    .prologue
    .line 183
    iget v0, p0, Lcom/qualcomm/camera/QCFace;->topbottomGaze:I

    #@2
    return v0
.end method

.method public getUpDownDirection()I
    .registers 2

    #@0
    .prologue
    .line 129
    iget v0, p0, Lcom/qualcomm/camera/QCFace;->updownDir:I

    #@2
    return v0
.end method
