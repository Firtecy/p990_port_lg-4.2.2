.class public Lcom/qualcomm/audio/QCAudioInfo;
.super Ljava/lang/Object;
.source "QCAudioInfo.java"

# interfaces
.implements Lcom/qualcomm/snapdragon/util/QCCapabilitiesInterface;


# static fields
.field public static final AUDIO_CODEC_EVRC:I = 0x6

.field public static final AUDIO_CODEC_LPCM:I = 0x8

.field public static final AUDIO_CODEC_QCELP:I = 0x7

.field public static final FILE_FORMAT_QCP:I = 0x9

.field public static final FILE_FORMAT_WAVE:I = 0xb

.field private static final KEY_AUDIO_CODECS:Ljava/lang/String; = "key_audio_codecs"

.field private static final KEY_FILE_FORMATS:Ljava/lang/String; = "key_file_formats"

.field private static final KEY_VOCODERS:Ljava/lang/String; = "key_vocoders"

.field public static final QC_AUDIO_CODEC_BASE:I = 0x6

.field public static final QC_FILE_FORMAT_BASE:I = 0x9

.field public static final QC_VOCODER_BASE:I = 0x64

.field public static final VOCODER_AMR_NB:I = 0x64

.field public static final VOCODER_AMR_WB:I = 0x65

.field public static final VOCODER_EVRC:I = 0x66

.field public static final VOCODER_EVRCB:I = 0x67

.field public static final VOCODER_EVRCWB:I = 0x68


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 28
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public getCapabilities()Landroid/os/Bundle;
    .registers 7

    #@0
    .prologue
    .line 80
    new-instance v2, Landroid/os/Bundle;

    #@2
    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    #@5
    .line 81
    .local v2, constantFieldBundle:Landroid/os/Bundle;
    new-instance v4, Ljava/util/ArrayList;

    #@7
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@a
    .line 82
    .local v4, vocodersList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v5, "VOCODER_AMR_NB"

    #@c
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@f
    .line 83
    const-string v5, "VOCODER_AMR_WB"

    #@11
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@14
    .line 84
    const-string v5, "VOCODER_EVRC"

    #@16
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@19
    .line 85
    const-string v5, "VOCODER_EVRCB"

    #@1b
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1e
    .line 86
    const-string v5, "VOCODER_EVRCWB"

    #@20
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@23
    .line 88
    const-string/jumbo v5, "key_vocoders"

    #@26
    invoke-virtual {v2, v5, v4}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    #@29
    .line 90
    new-instance v0, Ljava/util/ArrayList;

    #@2b
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@2e
    .line 91
    .local v0, audioCodecsList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v5, "AUDIO_CODEC_EVRC"

    #@30
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@33
    .line 92
    const-string v5, "AUDIO_CODEC_QCELP"

    #@35
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@38
    .line 93
    const-string v5, "AUDIO_CODEC_LPCM"

    #@3a
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3d
    .line 95
    const-string/jumbo v5, "key_audio_codecs"

    #@40
    invoke-virtual {v2, v5, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    #@43
    .line 97
    new-instance v3, Ljava/util/ArrayList;

    #@45
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@48
    .line 98
    .local v3, fileFormatList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v5, "FILE_FORMAT_QCP"

    #@4a
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4d
    .line 99
    const-string v5, "FILE_FORMAT_WAVE"

    #@4f
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@52
    .line 101
    const-string/jumbo v5, "key_file_formats"

    #@55
    invoke-virtual {v2, v5, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    #@58
    .line 103
    new-instance v1, Landroid/os/Bundle;

    #@5a
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    #@5d
    .line 104
    .local v1, capabilitiesBundle:Landroid/os/Bundle;
    const-string/jumbo v5, "key_constant_field_values"

    #@60
    invoke-virtual {v1, v5, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    #@63
    .line 106
    return-object v1
.end method
