.class public Lcom/sprint/net/ConnectionManager;
.super Ljava/lang/Object;
.source "ConnectionManager.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static sConnectionManager:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 36
    const-class v0, Lcom/sprint/net/ConnectionManager;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/sprint/net/ConnectionManager;->TAG:Ljava/lang/String;

    #@8
    .line 37
    const/4 v0, 0x0

    #@9
    sput-object v0, Lcom/sprint/net/ConnectionManager;->sConnectionManager:Ljava/lang/Object;

    #@b
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 35
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static getInstance(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Object;
    .registers 8
    .parameter "ctx"
    .parameter "service"

    #@0
    .prologue
    .line 42
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v4, "com.sprint.net."

    #@7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v3

    #@b
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v3

    #@f
    const-string v4, ".ConnectionManager"

    #@11
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    #@1c
    move-result-object v0

    #@1d
    .line 43
    .local v0, clazz:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    const/4 v3, 0x1

    #@1e
    new-array v3, v3, [Ljava/lang/Class;

    #@20
    const/4 v4, 0x0

    #@21
    const-class v5, Landroid/content/Context;

    #@23
    aput-object v5, v3, v4

    #@25
    invoke-virtual {v0, v3}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    #@28
    move-result-object v2

    #@29
    .line 44
    .local v2, mConstructor:Ljava/lang/reflect/Constructor;,"Ljava/lang/reflect/Constructor<*>;"
    sget-object v3, Lcom/sprint/net/ConnectionManager;->TAG:Ljava/lang/String;

    #@2b
    new-instance v4, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string/jumbo v5, "sprint :: "

    #@33
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v4

    #@37
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    const-string v5, " ConnectionManager"

    #@3d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v4

    #@45
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 45
    const/4 v3, 0x1

    #@49
    new-array v3, v3, [Ljava/lang/Object;

    #@4b
    const/4 v4, 0x0

    #@4c
    aput-object p0, v3, v4

    #@4e
    invoke-virtual {v2, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    #@51
    move-result-object v3

    #@52
    sput-object v3, Lcom/sprint/net/ConnectionManager;->sConnectionManager:Ljava/lang/Object;
    :try_end_54
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_54} :catch_57

    #@54
    .line 50
    .end local v0           #clazz:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    .end local v2           #mConstructor:Ljava/lang/reflect/Constructor;,"Ljava/lang/reflect/Constructor<*>;"
    :goto_54
    sget-object v3, Lcom/sprint/net/ConnectionManager;->sConnectionManager:Ljava/lang/Object;

    #@56
    return-object v3

    #@57
    .line 46
    :catch_57
    move-exception v1

    #@58
    .line 47
    .local v1, e:Ljava/lang/Exception;
    sget-object v3, Lcom/sprint/net/ConnectionManager;->TAG:Ljava/lang/String;

    #@5a
    new-instance v4, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v5, "Can not initiated by "

    #@61
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v4

    #@65
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v4

    #@69
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v4

    #@6d
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@70
    .line 48
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    #@73
    goto :goto_54
.end method
