.class public Lcom/google/android/gles_jni/EGLDisplayImpl;
.super Ljavax/microedition/khronos/egl/EGLDisplay;
.source "EGLDisplayImpl.java"


# instance fields
.field mEGLDisplay:I


# direct methods
.method public constructor <init>(I)V
    .registers 2
    .parameter "dpy"

    #@0
    .prologue
    .line 24
    invoke-direct {p0}, Ljavax/microedition/khronos/egl/EGLDisplay;-><init>()V

    #@3
    .line 25
    iput p1, p0, Lcom/google/android/gles_jni/EGLDisplayImpl;->mEGLDisplay:I

    #@5
    .line 26
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 30
    if-ne p0, p1, :cond_5

    #@4
    .line 35
    :cond_4
    :goto_4
    return v1

    #@5
    .line 31
    :cond_5
    if-eqz p1, :cond_11

    #@7
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@a
    move-result-object v3

    #@b
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@e
    move-result-object v4

    #@f
    if-eq v3, v4, :cond_13

    #@11
    :cond_11
    move v1, v2

    #@12
    goto :goto_4

    #@13
    :cond_13
    move-object v0, p1

    #@14
    .line 33
    check-cast v0, Lcom/google/android/gles_jni/EGLDisplayImpl;

    #@16
    .line 35
    .local v0, that:Lcom/google/android/gles_jni/EGLDisplayImpl;
    iget v3, p0, Lcom/google/android/gles_jni/EGLDisplayImpl;->mEGLDisplay:I

    #@18
    iget v4, v0, Lcom/google/android/gles_jni/EGLDisplayImpl;->mEGLDisplay:I

    #@1a
    if-eq v3, v4, :cond_4

    #@1c
    move v1, v2

    #@1d
    goto :goto_4
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 41
    iget v0, p0, Lcom/google/android/gles_jni/EGLDisplayImpl;->mEGLDisplay:I

    #@2
    return v0
.end method
