.class public Lcom/google/android/gles_jni/EGLSurfaceImpl;
.super Ljavax/microedition/khronos/egl/EGLSurface;
.source "EGLSurfaceImpl.java"


# instance fields
.field mEGLSurface:I

.field private mNativePixelRef:I


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 24
    invoke-direct {p0}, Ljavax/microedition/khronos/egl/EGLSurface;-><init>()V

    #@4
    .line 25
    iput v0, p0, Lcom/google/android/gles_jni/EGLSurfaceImpl;->mEGLSurface:I

    #@6
    .line 26
    iput v0, p0, Lcom/google/android/gles_jni/EGLSurfaceImpl;->mNativePixelRef:I

    #@8
    .line 27
    return-void
.end method

.method public constructor <init>(I)V
    .registers 3
    .parameter "surface"

    #@0
    .prologue
    .line 28
    invoke-direct {p0}, Ljavax/microedition/khronos/egl/EGLSurface;-><init>()V

    #@3
    .line 29
    iput p1, p0, Lcom/google/android/gles_jni/EGLSurfaceImpl;->mEGLSurface:I

    #@5
    .line 30
    const/4 v0, 0x0

    #@6
    iput v0, p0, Lcom/google/android/gles_jni/EGLSurfaceImpl;->mNativePixelRef:I

    #@8
    .line 31
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 35
    if-ne p0, p1, :cond_5

    #@4
    .line 40
    :cond_4
    :goto_4
    return v1

    #@5
    .line 36
    :cond_5
    if-eqz p1, :cond_11

    #@7
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@a
    move-result-object v3

    #@b
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@e
    move-result-object v4

    #@f
    if-eq v3, v4, :cond_13

    #@11
    :cond_11
    move v1, v2

    #@12
    goto :goto_4

    #@13
    :cond_13
    move-object v0, p1

    #@14
    .line 38
    check-cast v0, Lcom/google/android/gles_jni/EGLSurfaceImpl;

    #@16
    .line 40
    .local v0, that:Lcom/google/android/gles_jni/EGLSurfaceImpl;
    iget v3, p0, Lcom/google/android/gles_jni/EGLSurfaceImpl;->mEGLSurface:I

    #@18
    iget v4, v0, Lcom/google/android/gles_jni/EGLSurfaceImpl;->mEGLSurface:I

    #@1a
    if-eq v3, v4, :cond_4

    #@1c
    move v1, v2

    #@1d
    goto :goto_4
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 46
    iget v0, p0, Lcom/google/android/gles_jni/EGLSurfaceImpl;->mEGLSurface:I

    #@2
    return v0
.end method
