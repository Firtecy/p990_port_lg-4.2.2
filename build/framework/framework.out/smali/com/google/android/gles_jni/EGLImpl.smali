.class public Lcom/google/android/gles_jni/EGLImpl;
.super Ljava/lang/Object;
.source "EGLImpl.java"

# interfaces
.implements Ljavax/microedition/khronos/egl/EGL10;


# instance fields
.field private mContext:Lcom/google/android/gles_jni/EGLContextImpl;

.field private mDisplay:Lcom/google/android/gles_jni/EGLDisplayImpl;

.field private mSurface:Lcom/google/android/gles_jni/EGLSurfaceImpl;


# direct methods
.method static constructor <clinit>()V
    .registers 0

    #@0
    .prologue
    .line 159
    invoke-static {}, Lcom/google/android/gles_jni/EGLImpl;->_nativeClassInit()V

    #@3
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 26
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 27
    new-instance v0, Lcom/google/android/gles_jni/EGLContextImpl;

    #@6
    invoke-direct {v0, v1}, Lcom/google/android/gles_jni/EGLContextImpl;-><init>(I)V

    #@9
    iput-object v0, p0, Lcom/google/android/gles_jni/EGLImpl;->mContext:Lcom/google/android/gles_jni/EGLContextImpl;

    #@b
    .line 28
    new-instance v0, Lcom/google/android/gles_jni/EGLDisplayImpl;

    #@d
    invoke-direct {v0, v1}, Lcom/google/android/gles_jni/EGLDisplayImpl;-><init>(I)V

    #@10
    iput-object v0, p0, Lcom/google/android/gles_jni/EGLImpl;->mDisplay:Lcom/google/android/gles_jni/EGLDisplayImpl;

    #@12
    .line 29
    new-instance v0, Lcom/google/android/gles_jni/EGLSurfaceImpl;

    #@14
    invoke-direct {v0, v1}, Lcom/google/android/gles_jni/EGLSurfaceImpl;-><init>(I)V

    #@17
    iput-object v0, p0, Lcom/google/android/gles_jni/EGLImpl;->mSurface:Lcom/google/android/gles_jni/EGLSurfaceImpl;

    #@19
    return-void
.end method

.method private native _eglCreateContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljavax/microedition/khronos/egl/EGLContext;[I)I
.end method

.method private native _eglCreatePbufferSurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;[I)I
.end method

.method private native _eglCreatePixmapSurface(Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljava/lang/Object;[I)V
.end method

.method private native _eglCreateWindowSurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljava/lang/Object;[I)I
.end method

.method private native _eglCreateWindowSurfaceTexture(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljava/lang/Object;[I)I
.end method

.method private native _eglGetCurrentContext()I
.end method

.method private native _eglGetCurrentDisplay()I
.end method

.method private native _eglGetCurrentSurface(I)I
.end method

.method private native _eglGetDisplay(Ljava/lang/Object;)I
.end method

.method private static native _nativeClassInit()V
.end method

.method public static native getInitCount(Ljavax/microedition/khronos/egl/EGLDisplay;)I
.end method


# virtual methods
.method public native eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z
.end method

.method public native eglCopyBuffers(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljava/lang/Object;)Z
.end method

.method public eglCreateContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljavax/microedition/khronos/egl/EGLContext;[I)Ljavax/microedition/khronos/egl/EGLContext;
    .registers 7
    .parameter "display"
    .parameter "config"
    .parameter "share_context"
    .parameter "attrib_list"

    #@0
    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gles_jni/EGLImpl;->_eglCreateContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljavax/microedition/khronos/egl/EGLContext;[I)I

    #@3
    move-result v0

    #@4
    .line 55
    .local v0, eglContextId:I
    if-nez v0, :cond_9

    #@6
    .line 56
    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    #@8
    .line 58
    :goto_8
    return-object v1

    #@9
    :cond_9
    new-instance v1, Lcom/google/android/gles_jni/EGLContextImpl;

    #@b
    invoke-direct {v1, v0}, Lcom/google/android/gles_jni/EGLContextImpl;-><init>(I)V

    #@e
    goto :goto_8
.end method

.method public eglCreatePbufferSurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;[I)Ljavax/microedition/khronos/egl/EGLSurface;
    .registers 6
    .parameter "display"
    .parameter "config"
    .parameter "attrib_list"

    #@0
    .prologue
    .line 62
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gles_jni/EGLImpl;->_eglCreatePbufferSurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;[I)I

    #@3
    move-result v0

    #@4
    .line 63
    .local v0, eglSurfaceId:I
    if-nez v0, :cond_9

    #@6
    .line 64
    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    #@8
    .line 66
    :goto_8
    return-object v1

    #@9
    :cond_9
    new-instance v1, Lcom/google/android/gles_jni/EGLSurfaceImpl;

    #@b
    invoke-direct {v1, v0}, Lcom/google/android/gles_jni/EGLSurfaceImpl;-><init>(I)V

    #@e
    goto :goto_8
.end method

.method public eglCreatePixmapSurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljava/lang/Object;[I)Ljavax/microedition/khronos/egl/EGLSurface;
    .registers 11
    .parameter "display"
    .parameter "config"
    .parameter "native_pixmap"
    .parameter "attrib_list"

    #@0
    .prologue
    .line 70
    new-instance v1, Lcom/google/android/gles_jni/EGLSurfaceImpl;

    #@2
    invoke-direct {v1}, Lcom/google/android/gles_jni/EGLSurfaceImpl;-><init>()V

    #@5
    .local v1, sur:Lcom/google/android/gles_jni/EGLSurfaceImpl;
    move-object v0, p0

    #@6
    move-object v2, p1

    #@7
    move-object v3, p2

    #@8
    move-object v4, p3

    #@9
    move-object v5, p4

    #@a
    .line 71
    invoke-direct/range {v0 .. v5}, Lcom/google/android/gles_jni/EGLImpl;->_eglCreatePixmapSurface(Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljava/lang/Object;[I)V

    #@d
    .line 72
    iget v0, v1, Lcom/google/android/gles_jni/EGLSurfaceImpl;->mEGLSurface:I

    #@f
    if-nez v0, :cond_13

    #@11
    .line 73
    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    #@13
    .line 75
    .end local v1           #sur:Lcom/google/android/gles_jni/EGLSurfaceImpl;
    :cond_13
    return-object v1
.end method

.method public eglCreateWindowSurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljava/lang/Object;[I)Ljavax/microedition/khronos/egl/EGLSurface;
    .registers 11
    .parameter "display"
    .parameter "config"
    .parameter "native_window"
    .parameter "attrib_list"

    #@0
    .prologue
    .line 79
    const/4 v2, 0x0

    #@1
    .line 80
    .local v2, sur:Landroid/view/Surface;
    instance-of v4, p3, Landroid/view/SurfaceView;

    #@3
    if-eqz v4, :cond_1b

    #@5
    move-object v3, p3

    #@6
    .line 81
    check-cast v3, Landroid/view/SurfaceView;

    #@8
    .line 82
    .local v3, surfaceView:Landroid/view/SurfaceView;
    invoke-virtual {v3}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    #@b
    move-result-object v4

    #@c
    invoke-interface {v4}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    #@f
    move-result-object v2

    #@10
    .line 91
    .end local v3           #surfaceView:Landroid/view/SurfaceView;
    :cond_10
    :goto_10
    if-eqz v2, :cond_2f

    #@12
    .line 92
    invoke-direct {p0, p1, p2, v2, p4}, Lcom/google/android/gles_jni/EGLImpl;->_eglCreateWindowSurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljava/lang/Object;[I)I

    #@15
    move-result v0

    #@16
    .line 102
    .local v0, eglSurfaceId:I
    :goto_16
    if-nez v0, :cond_40

    #@18
    .line 103
    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    #@1a
    .line 105
    :goto_1a
    return-object v4

    #@1b
    .line 83
    .end local v0           #eglSurfaceId:I
    :cond_1b
    instance-of v4, p3, Landroid/view/SurfaceHolder;

    #@1d
    if-eqz v4, :cond_27

    #@1f
    move-object v1, p3

    #@20
    .line 84
    check-cast v1, Landroid/view/SurfaceHolder;

    #@22
    .line 85
    .local v1, holder:Landroid/view/SurfaceHolder;
    invoke-interface {v1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    #@25
    move-result-object v2

    #@26
    .line 86
    goto :goto_10

    #@27
    .end local v1           #holder:Landroid/view/SurfaceHolder;
    :cond_27
    instance-of v4, p3, Landroid/view/Surface;

    #@29
    if-eqz v4, :cond_10

    #@2b
    move-object v2, p3

    #@2c
    .line 87
    check-cast v2, Landroid/view/Surface;

    #@2e
    goto :goto_10

    #@2f
    .line 93
    :cond_2f
    instance-of v4, p3, Landroid/graphics/SurfaceTexture;

    #@31
    if-eqz v4, :cond_38

    #@33
    .line 94
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gles_jni/EGLImpl;->_eglCreateWindowSurfaceTexture(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljava/lang/Object;[I)I

    #@36
    move-result v0

    #@37
    .restart local v0       #eglSurfaceId:I
    goto :goto_16

    #@38
    .line 97
    .end local v0           #eglSurfaceId:I
    :cond_38
    new-instance v4, Ljava/lang/UnsupportedOperationException;

    #@3a
    const-string v5, "eglCreateWindowSurface() can only be called with an instance of Surface, SurfaceView, SurfaceHolder or SurfaceTexture at the moment."

    #@3c
    invoke-direct {v4, v5}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@3f
    throw v4

    #@40
    .line 105
    .restart local v0       #eglSurfaceId:I
    :cond_40
    new-instance v4, Lcom/google/android/gles_jni/EGLSurfaceImpl;

    #@42
    invoke-direct {v4, v0}, Lcom/google/android/gles_jni/EGLSurfaceImpl;-><init>(I)V

    #@45
    goto :goto_1a
.end method

.method public native eglDestroyContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;)Z
.end method

.method public native eglDestroySurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z
.end method

.method public native eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z
.end method

.method public native eglGetConfigs(Ljavax/microedition/khronos/egl/EGLDisplay;[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z
.end method

.method public declared-synchronized eglGetCurrentContext()Ljavax/microedition/khronos/egl/EGLContext;
    .registers 3

    #@0
    .prologue
    .line 119
    monitor-enter p0

    #@1
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/gles_jni/EGLImpl;->_eglGetCurrentContext()I

    #@4
    move-result v0

    #@5
    .line 120
    .local v0, value:I
    if-nez v0, :cond_b

    #@7
    .line 121
    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_1b

    #@9
    .line 125
    :goto_9
    monitor-exit p0

    #@a
    return-object v1

    #@b
    .line 123
    :cond_b
    :try_start_b
    iget-object v1, p0, Lcom/google/android/gles_jni/EGLImpl;->mContext:Lcom/google/android/gles_jni/EGLContextImpl;

    #@d
    iget v1, v1, Lcom/google/android/gles_jni/EGLContextImpl;->mEGLContext:I

    #@f
    if-eq v1, v0, :cond_18

    #@11
    .line 124
    new-instance v1, Lcom/google/android/gles_jni/EGLContextImpl;

    #@13
    invoke-direct {v1, v0}, Lcom/google/android/gles_jni/EGLContextImpl;-><init>(I)V

    #@16
    iput-object v1, p0, Lcom/google/android/gles_jni/EGLImpl;->mContext:Lcom/google/android/gles_jni/EGLContextImpl;

    #@18
    .line 125
    :cond_18
    iget-object v1, p0, Lcom/google/android/gles_jni/EGLImpl;->mContext:Lcom/google/android/gles_jni/EGLContextImpl;
    :try_end_1a
    .catchall {:try_start_b .. :try_end_1a} :catchall_1b

    #@1a
    goto :goto_9

    #@1b
    .line 119
    .end local v0           #value:I
    :catchall_1b
    move-exception v1

    #@1c
    monitor-exit p0

    #@1d
    throw v1
.end method

.method public declared-synchronized eglGetCurrentDisplay()Ljavax/microedition/khronos/egl/EGLDisplay;
    .registers 3

    #@0
    .prologue
    .line 129
    monitor-enter p0

    #@1
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/gles_jni/EGLImpl;->_eglGetCurrentDisplay()I

    #@4
    move-result v0

    #@5
    .line 130
    .local v0, value:I
    if-nez v0, :cond_b

    #@7
    .line 131
    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_1b

    #@9
    .line 135
    :goto_9
    monitor-exit p0

    #@a
    return-object v1

    #@b
    .line 133
    :cond_b
    :try_start_b
    iget-object v1, p0, Lcom/google/android/gles_jni/EGLImpl;->mDisplay:Lcom/google/android/gles_jni/EGLDisplayImpl;

    #@d
    iget v1, v1, Lcom/google/android/gles_jni/EGLDisplayImpl;->mEGLDisplay:I

    #@f
    if-eq v1, v0, :cond_18

    #@11
    .line 134
    new-instance v1, Lcom/google/android/gles_jni/EGLDisplayImpl;

    #@13
    invoke-direct {v1, v0}, Lcom/google/android/gles_jni/EGLDisplayImpl;-><init>(I)V

    #@16
    iput-object v1, p0, Lcom/google/android/gles_jni/EGLImpl;->mDisplay:Lcom/google/android/gles_jni/EGLDisplayImpl;

    #@18
    .line 135
    :cond_18
    iget-object v1, p0, Lcom/google/android/gles_jni/EGLImpl;->mDisplay:Lcom/google/android/gles_jni/EGLDisplayImpl;
    :try_end_1a
    .catchall {:try_start_b .. :try_end_1a} :catchall_1b

    #@1a
    goto :goto_9

    #@1b
    .line 129
    .end local v0           #value:I
    :catchall_1b
    move-exception v1

    #@1c
    monitor-exit p0

    #@1d
    throw v1
.end method

.method public declared-synchronized eglGetCurrentSurface(I)Ljavax/microedition/khronos/egl/EGLSurface;
    .registers 4
    .parameter "readdraw"

    #@0
    .prologue
    .line 139
    monitor-enter p0

    #@1
    :try_start_1
    invoke-direct {p0, p1}, Lcom/google/android/gles_jni/EGLImpl;->_eglGetCurrentSurface(I)I

    #@4
    move-result v0

    #@5
    .line 140
    .local v0, value:I
    if-nez v0, :cond_b

    #@7
    .line 141
    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_1b

    #@9
    .line 145
    :goto_9
    monitor-exit p0

    #@a
    return-object v1

    #@b
    .line 143
    :cond_b
    :try_start_b
    iget-object v1, p0, Lcom/google/android/gles_jni/EGLImpl;->mSurface:Lcom/google/android/gles_jni/EGLSurfaceImpl;

    #@d
    iget v1, v1, Lcom/google/android/gles_jni/EGLSurfaceImpl;->mEGLSurface:I

    #@f
    if-eq v1, v0, :cond_18

    #@11
    .line 144
    new-instance v1, Lcom/google/android/gles_jni/EGLSurfaceImpl;

    #@13
    invoke-direct {v1, v0}, Lcom/google/android/gles_jni/EGLSurfaceImpl;-><init>(I)V

    #@16
    iput-object v1, p0, Lcom/google/android/gles_jni/EGLImpl;->mSurface:Lcom/google/android/gles_jni/EGLSurfaceImpl;

    #@18
    .line 145
    :cond_18
    iget-object v1, p0, Lcom/google/android/gles_jni/EGLImpl;->mSurface:Lcom/google/android/gles_jni/EGLSurfaceImpl;
    :try_end_1a
    .catchall {:try_start_b .. :try_end_1a} :catchall_1b

    #@1a
    goto :goto_9

    #@1b
    .line 139
    .end local v0           #value:I
    :catchall_1b
    move-exception v1

    #@1c
    monitor-exit p0

    #@1d
    throw v1
.end method

.method public declared-synchronized eglGetDisplay(Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLDisplay;
    .registers 4
    .parameter "native_display"

    #@0
    .prologue
    .line 109
    monitor-enter p0

    #@1
    :try_start_1
    invoke-direct {p0, p1}, Lcom/google/android/gles_jni/EGLImpl;->_eglGetDisplay(Ljava/lang/Object;)I

    #@4
    move-result v0

    #@5
    .line 110
    .local v0, value:I
    if-nez v0, :cond_b

    #@7
    .line 111
    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_1b

    #@9
    .line 115
    :goto_9
    monitor-exit p0

    #@a
    return-object v1

    #@b
    .line 113
    :cond_b
    :try_start_b
    iget-object v1, p0, Lcom/google/android/gles_jni/EGLImpl;->mDisplay:Lcom/google/android/gles_jni/EGLDisplayImpl;

    #@d
    iget v1, v1, Lcom/google/android/gles_jni/EGLDisplayImpl;->mEGLDisplay:I

    #@f
    if-eq v1, v0, :cond_18

    #@11
    .line 114
    new-instance v1, Lcom/google/android/gles_jni/EGLDisplayImpl;

    #@13
    invoke-direct {v1, v0}, Lcom/google/android/gles_jni/EGLDisplayImpl;-><init>(I)V

    #@16
    iput-object v1, p0, Lcom/google/android/gles_jni/EGLImpl;->mDisplay:Lcom/google/android/gles_jni/EGLDisplayImpl;

    #@18
    .line 115
    :cond_18
    iget-object v1, p0, Lcom/google/android/gles_jni/EGLImpl;->mDisplay:Lcom/google/android/gles_jni/EGLDisplayImpl;
    :try_end_1a
    .catchall {:try_start_b .. :try_end_1a} :catchall_1b

    #@1a
    goto :goto_9

    #@1b
    .line 109
    .end local v0           #value:I
    :catchall_1b
    move-exception v1

    #@1c
    monitor-exit p0

    #@1d
    throw v1
.end method

.method public native eglGetError()I
.end method

.method public native eglInitialize(Ljavax/microedition/khronos/egl/EGLDisplay;[I)Z
.end method

.method public native eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z
.end method

.method public native eglQueryContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;I[I)Z
.end method

.method public native eglQueryString(Ljavax/microedition/khronos/egl/EGLDisplay;I)Ljava/lang/String;
.end method

.method public native eglQuerySurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;I[I)Z
.end method

.method public native eglReleaseThread()Z
.end method

.method public native eglSwapBuffers(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z
.end method

.method public native eglTerminate(Ljavax/microedition/khronos/egl/EGLDisplay;)Z
.end method

.method public native eglWaitGL()Z
.end method

.method public native eglWaitNative(ILjava/lang/Object;)Z
.end method
