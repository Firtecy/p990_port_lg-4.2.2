.class public Lcom/google/android/gles_jni/EGLContextImpl;
.super Ljavax/microedition/khronos/egl/EGLContext;
.source "EGLContextImpl.java"


# instance fields
.field mEGLContext:I

.field private mGLContext:Lcom/google/android/gles_jni/GLImpl;


# direct methods
.method public constructor <init>(I)V
    .registers 3
    .parameter "ctx"

    #@0
    .prologue
    .line 26
    invoke-direct {p0}, Ljavax/microedition/khronos/egl/EGLContext;-><init>()V

    #@3
    .line 27
    iput p1, p0, Lcom/google/android/gles_jni/EGLContextImpl;->mEGLContext:I

    #@5
    .line 28
    new-instance v0, Lcom/google/android/gles_jni/GLImpl;

    #@7
    invoke-direct {v0}, Lcom/google/android/gles_jni/GLImpl;-><init>()V

    #@a
    iput-object v0, p0, Lcom/google/android/gles_jni/EGLContextImpl;->mGLContext:Lcom/google/android/gles_jni/GLImpl;

    #@c
    .line 29
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 38
    if-ne p0, p1, :cond_5

    #@4
    .line 43
    :cond_4
    :goto_4
    return v1

    #@5
    .line 39
    :cond_5
    if-eqz p1, :cond_11

    #@7
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@a
    move-result-object v3

    #@b
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@e
    move-result-object v4

    #@f
    if-eq v3, v4, :cond_13

    #@11
    :cond_11
    move v1, v2

    #@12
    goto :goto_4

    #@13
    :cond_13
    move-object v0, p1

    #@14
    .line 41
    check-cast v0, Lcom/google/android/gles_jni/EGLContextImpl;

    #@16
    .line 43
    .local v0, that:Lcom/google/android/gles_jni/EGLContextImpl;
    iget v3, p0, Lcom/google/android/gles_jni/EGLContextImpl;->mEGLContext:I

    #@18
    iget v4, v0, Lcom/google/android/gles_jni/EGLContextImpl;->mEGLContext:I

    #@1a
    if-eq v3, v4, :cond_4

    #@1c
    move v1, v2

    #@1d
    goto :goto_4
.end method

.method public getGL()Ljavax/microedition/khronos/opengles/GL;
    .registers 2

    #@0
    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gles_jni/EGLContextImpl;->mGLContext:Lcom/google/android/gles_jni/GLImpl;

    #@2
    return-object v0
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 48
    iget v0, p0, Lcom/google/android/gles_jni/EGLContextImpl;->mEGLContext:I

    #@2
    return v0
.end method
