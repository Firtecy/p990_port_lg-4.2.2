.class public Lcom/google/android/util/SmileyParser;
.super Lcom/google/android/util/AbstractMessageParser;
.source "SmileyParser.java"


# instance fields
.field private mRes:Lcom/google/android/util/SmileyResources;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/util/SmileyResources;)V
    .registers 11
    .parameter "text"
    .parameter "res"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 34
    const/4 v2, 0x1

    #@2
    move-object v0, p0

    #@3
    move-object v1, p1

    #@4
    move v4, v3

    #@5
    move v5, v3

    #@6
    move v6, v3

    #@7
    move v7, v3

    #@8
    invoke-direct/range {v0 .. v7}, Lcom/google/android/util/AbstractMessageParser;-><init>(Ljava/lang/String;ZZZZZZ)V

    #@b
    .line 42
    iput-object p2, p0, Lcom/google/android/util/SmileyParser;->mRes:Lcom/google/android/util/SmileyResources;

    #@d
    .line 43
    return-void
.end method


# virtual methods
.method protected getResources()Lcom/google/android/util/AbstractMessageParser$Resources;
    .registers 2

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/util/SmileyParser;->mRes:Lcom/google/android/util/SmileyResources;

    #@2
    return-object v0
.end method

.method public getSpannableString(Landroid/content/Context;)Ljava/lang/CharSequence;
    .registers 13
    .parameter "context"

    #@0
    .prologue
    .line 56
    new-instance v0, Landroid/text/SpannableStringBuilder;

    #@2
    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    #@5
    .line 58
    .local v0, builder:Landroid/text/SpannableStringBuilder;
    invoke-virtual {p0}, Lcom/google/android/util/SmileyParser;->getPartCount()I

    #@8
    move-result v8

    #@9
    if-nez v8, :cond_e

    #@b
    .line 59
    const-string v0, ""

    #@d
    .line 80
    .end local v0           #builder:Landroid/text/SpannableStringBuilder;
    :cond_d
    return-object v0

    #@e
    .line 63
    .restart local v0       #builder:Landroid/text/SpannableStringBuilder;
    :cond_e
    const/4 v8, 0x0

    #@f
    invoke-virtual {p0, v8}, Lcom/google/android/util/SmileyParser;->getPart(I)Lcom/google/android/util/AbstractMessageParser$Part;

    #@12
    move-result-object v3

    #@13
    .line 64
    .local v3, part:Lcom/google/android/util/AbstractMessageParser$Part;
    invoke-virtual {v3}, Lcom/google/android/util/AbstractMessageParser$Part;->getTokens()Ljava/util/ArrayList;

    #@16
    move-result-object v7

    #@17
    .line 65
    .local v7, tokens:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/util/AbstractMessageParser$Token;>;"
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@1a
    move-result v2

    #@1b
    .line 66
    .local v2, len:I
    const/4 v1, 0x0

    #@1c
    .local v1, i:I
    :goto_1c
    if-ge v1, v2, :cond_d

    #@1e
    .line 67
    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@21
    move-result-object v6

    #@22
    check-cast v6, Lcom/google/android/util/AbstractMessageParser$Token;

    #@24
    .line 68
    .local v6, token:Lcom/google/android/util/AbstractMessageParser$Token;
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    #@27
    move-result v5

    #@28
    .line 69
    .local v5, start:I
    invoke-virtual {v6}, Lcom/google/android/util/AbstractMessageParser$Token;->getRawText()Ljava/lang/String;

    #@2b
    move-result-object v8

    #@2c
    invoke-virtual {v0, v8}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@2f
    .line 70
    invoke-virtual {v6}, Lcom/google/android/util/AbstractMessageParser$Token;->getType()Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@32
    move-result-object v8

    #@33
    sget-object v9, Lcom/google/android/util/AbstractMessageParser$Token$Type;->SMILEY:Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@35
    if-ne v8, v9, :cond_52

    #@37
    .line 71
    iget-object v8, p0, Lcom/google/android/util/SmileyParser;->mRes:Lcom/google/android/util/SmileyResources;

    #@39
    invoke-virtual {v6}, Lcom/google/android/util/AbstractMessageParser$Token;->getRawText()Ljava/lang/String;

    #@3c
    move-result-object v9

    #@3d
    invoke-virtual {v8, v9}, Lcom/google/android/util/SmileyResources;->getSmileyRes(Ljava/lang/String;)I

    #@40
    move-result v4

    #@41
    .line 72
    .local v4, resid:I
    const/4 v8, -0x1

    #@42
    if-eq v4, v8, :cond_52

    #@44
    .line 73
    new-instance v8, Landroid/text/style/ImageSpan;

    #@46
    invoke-direct {v8, p1, v4}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;I)V

    #@49
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    #@4c
    move-result v9

    #@4d
    const/16 v10, 0x21

    #@4f
    invoke-virtual {v0, v8, v5, v9, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    #@52
    .line 66
    .end local v4           #resid:I
    :cond_52
    add-int/lit8 v1, v1, 0x1

    #@54
    goto :goto_1c
.end method
