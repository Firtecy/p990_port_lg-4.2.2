.class public Lcom/google/android/util/AbstractMessageParser$Format;
.super Lcom/google/android/util/AbstractMessageParser$Token;
.source "AbstractMessageParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/util/AbstractMessageParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Format"
.end annotation


# instance fields
.field private ch:C

.field private matched:Z

.field private start:Z


# direct methods
.method public constructor <init>(CZ)V
    .registers 5
    .parameter "ch"
    .parameter "start"

    #@0
    .prologue
    .line 1183
    sget-object v0, Lcom/google/android/util/AbstractMessageParser$Token$Type;->FORMAT:Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@2
    invoke-static {p1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-direct {p0, v0, v1}, Lcom/google/android/util/AbstractMessageParser$Token;-><init>(Lcom/google/android/util/AbstractMessageParser$Token$Type;Ljava/lang/String;)V

    #@9
    .line 1184
    iput-char p1, p0, Lcom/google/android/util/AbstractMessageParser$Format;->ch:C

    #@b
    .line 1185
    iput-boolean p2, p0, Lcom/google/android/util/AbstractMessageParser$Format;->start:Z

    #@d
    .line 1186
    return-void
.end method

.method private getFormatEnd(C)Ljava/lang/String;
    .registers 5
    .parameter "ch"

    #@0
    .prologue
    .line 1224
    sparse-switch p1, :sswitch_data_30

    #@3
    .line 1229
    new-instance v0, Ljava/lang/AssertionError;

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string/jumbo v2, "unknown format \'"

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, "\'"

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    #@22
    throw v0

    #@23
    .line 1225
    :sswitch_23
    const-string v0, "</b>"

    #@25
    .line 1228
    :goto_25
    return-object v0

    #@26
    .line 1226
    :sswitch_26
    const-string v0, "</i>"

    #@28
    goto :goto_25

    #@29
    .line 1227
    :sswitch_29
    const-string v0, "</font></b>"

    #@2b
    goto :goto_25

    #@2c
    .line 1228
    :sswitch_2c
    const-string/jumbo v0, "\u201d</font>"

    #@2f
    goto :goto_25

    #@30
    .line 1224
    :sswitch_data_30
    .sparse-switch
        0x22 -> :sswitch_2c
        0x2a -> :sswitch_23
        0x5e -> :sswitch_29
        0x5f -> :sswitch_26
    .end sparse-switch
.end method

.method private getFormatStart(C)Ljava/lang/String;
    .registers 5
    .parameter "ch"

    #@0
    .prologue
    .line 1214
    sparse-switch p1, :sswitch_data_30

    #@3
    .line 1219
    new-instance v0, Ljava/lang/AssertionError;

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string/jumbo v2, "unknown format \'"

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, "\'"

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    #@22
    throw v0

    #@23
    .line 1215
    :sswitch_23
    const-string v0, "<b>"

    #@25
    .line 1218
    :goto_25
    return-object v0

    #@26
    .line 1216
    :sswitch_26
    const-string v0, "<i>"

    #@28
    goto :goto_25

    #@29
    .line 1217
    :sswitch_29
    const-string v0, "<b><font color=\"#005FFF\">"

    #@2b
    goto :goto_25

    #@2c
    .line 1218
    :sswitch_2c
    const-string v0, "<font color=\"#999999\">\u201c"

    #@2e
    goto :goto_25

    #@2f
    .line 1214
    nop

    #@30
    :sswitch_data_30
    .sparse-switch
        0x22 -> :sswitch_2c
        0x2a -> :sswitch_23
        0x5e -> :sswitch_29
        0x5f -> :sswitch_26
    .end sparse-switch
.end method


# virtual methods
.method public controlCaps()Z
    .registers 3

    #@0
    .prologue
    .line 1210
    iget-char v0, p0, Lcom/google/android/util/AbstractMessageParser$Format;->ch:C

    #@2
    const/16 v1, 0x5e

    #@4
    if-ne v0, v1, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public getInfo()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1207
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@5
    throw v0
.end method

.method public isHtml()Z
    .registers 2

    #@0
    .prologue
    .line 1190
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public setCaps()Z
    .registers 2

    #@0
    .prologue
    .line 1211
    iget-boolean v0, p0, Lcom/google/android/util/AbstractMessageParser$Format;->start:Z

    #@2
    return v0
.end method

.method public setMatched(Z)V
    .registers 2
    .parameter "matched"

    #@0
    .prologue
    .line 1188
    iput-boolean p1, p0, Lcom/google/android/util/AbstractMessageParser$Format;->matched:Z

    #@2
    return-void
.end method

.method public toHtml(Z)Ljava/lang/String;
    .registers 4
    .parameter "caps"

    #@0
    .prologue
    .line 1195
    iget-boolean v0, p0, Lcom/google/android/util/AbstractMessageParser$Format;->matched:Z

    #@2
    if-eqz v0, :cond_16

    #@4
    .line 1196
    iget-boolean v0, p0, Lcom/google/android/util/AbstractMessageParser$Format;->start:Z

    #@6
    if-eqz v0, :cond_f

    #@8
    iget-char v0, p0, Lcom/google/android/util/AbstractMessageParser$Format;->ch:C

    #@a
    invoke-direct {p0, v0}, Lcom/google/android/util/AbstractMessageParser$Format;->getFormatStart(C)Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    .line 1199
    :goto_e
    return-object v0

    #@f
    .line 1196
    :cond_f
    iget-char v0, p0, Lcom/google/android/util/AbstractMessageParser$Format;->ch:C

    #@11
    invoke-direct {p0, v0}, Lcom/google/android/util/AbstractMessageParser$Format;->getFormatEnd(C)Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    goto :goto_e

    #@16
    .line 1199
    :cond_16
    iget-char v0, p0, Lcom/google/android/util/AbstractMessageParser$Format;->ch:C

    #@18
    const/16 v1, 0x22

    #@1a
    if-ne v0, v1, :cond_1f

    #@1c
    const-string v0, "&quot;"

    #@1e
    goto :goto_e

    #@1f
    :cond_1f
    iget-char v0, p0, Lcom/google/android/util/AbstractMessageParser$Format;->ch:C

    #@21
    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    goto :goto_e
.end method
