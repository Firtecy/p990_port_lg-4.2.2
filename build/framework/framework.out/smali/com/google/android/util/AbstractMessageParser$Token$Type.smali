.class public final enum Lcom/google/android/util/AbstractMessageParser$Token$Type;
.super Ljava/lang/Enum;
.source "AbstractMessageParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/util/AbstractMessageParser$Token;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/util/AbstractMessageParser$Token$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/util/AbstractMessageParser$Token$Type;

.field public static final enum ACRONYM:Lcom/google/android/util/AbstractMessageParser$Token$Type;

.field public static final enum FLICKR:Lcom/google/android/util/AbstractMessageParser$Token$Type;

.field public static final enum FORMAT:Lcom/google/android/util/AbstractMessageParser$Token$Type;

.field public static final enum GOOGLE_VIDEO:Lcom/google/android/util/AbstractMessageParser$Token$Type;

.field public static final enum HTML:Lcom/google/android/util/AbstractMessageParser$Token$Type;

.field public static final enum LINK:Lcom/google/android/util/AbstractMessageParser$Token$Type;

.field public static final enum MUSIC:Lcom/google/android/util/AbstractMessageParser$Token$Type;

.field public static final enum PHOTO:Lcom/google/android/util/AbstractMessageParser$Token$Type;

.field public static final enum SMILEY:Lcom/google/android/util/AbstractMessageParser$Token$Type;

.field public static final enum YOUTUBE_VIDEO:Lcom/google/android/util/AbstractMessageParser$Token$Type;


# instance fields
.field private stringRep:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 9

    #@0
    .prologue
    const/4 v8, 0x4

    #@1
    const/4 v7, 0x3

    #@2
    const/4 v6, 0x2

    #@3
    const/4 v5, 0x1

    #@4
    const/4 v4, 0x0

    #@5
    .line 657
    new-instance v0, Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@7
    const-string v1, "HTML"

    #@9
    const-string v2, "html"

    #@b
    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/util/AbstractMessageParser$Token$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    #@e
    sput-object v0, Lcom/google/android/util/AbstractMessageParser$Token$Type;->HTML:Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@10
    .line 658
    new-instance v0, Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@12
    const-string v1, "FORMAT"

    #@14
    const-string v2, "format"

    #@16
    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/util/AbstractMessageParser$Token$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    #@19
    sput-object v0, Lcom/google/android/util/AbstractMessageParser$Token$Type;->FORMAT:Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@1b
    .line 659
    new-instance v0, Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@1d
    const-string v1, "LINK"

    #@1f
    const-string/jumbo v2, "l"

    #@22
    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/util/AbstractMessageParser$Token$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    #@25
    sput-object v0, Lcom/google/android/util/AbstractMessageParser$Token$Type;->LINK:Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@27
    .line 660
    new-instance v0, Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@29
    const-string v1, "SMILEY"

    #@2b
    const-string v2, "e"

    #@2d
    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/util/AbstractMessageParser$Token$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    #@30
    sput-object v0, Lcom/google/android/util/AbstractMessageParser$Token$Type;->SMILEY:Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@32
    .line 661
    new-instance v0, Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@34
    const-string v1, "ACRONYM"

    #@36
    const-string v2, "a"

    #@38
    invoke-direct {v0, v1, v8, v2}, Lcom/google/android/util/AbstractMessageParser$Token$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    #@3b
    sput-object v0, Lcom/google/android/util/AbstractMessageParser$Token$Type;->ACRONYM:Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@3d
    .line 662
    new-instance v0, Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@3f
    const-string v1, "MUSIC"

    #@41
    const/4 v2, 0x5

    #@42
    const-string/jumbo v3, "m"

    #@45
    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/util/AbstractMessageParser$Token$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    #@48
    sput-object v0, Lcom/google/android/util/AbstractMessageParser$Token$Type;->MUSIC:Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@4a
    .line 663
    new-instance v0, Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@4c
    const-string v1, "GOOGLE_VIDEO"

    #@4e
    const/4 v2, 0x6

    #@4f
    const-string/jumbo v3, "v"

    #@52
    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/util/AbstractMessageParser$Token$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    #@55
    sput-object v0, Lcom/google/android/util/AbstractMessageParser$Token$Type;->GOOGLE_VIDEO:Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@57
    .line 664
    new-instance v0, Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@59
    const-string v1, "YOUTUBE_VIDEO"

    #@5b
    const/4 v2, 0x7

    #@5c
    const-string/jumbo v3, "yt"

    #@5f
    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/util/AbstractMessageParser$Token$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    #@62
    sput-object v0, Lcom/google/android/util/AbstractMessageParser$Token$Type;->YOUTUBE_VIDEO:Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@64
    .line 665
    new-instance v0, Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@66
    const-string v1, "PHOTO"

    #@68
    const/16 v2, 0x8

    #@6a
    const-string/jumbo v3, "p"

    #@6d
    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/util/AbstractMessageParser$Token$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    #@70
    sput-object v0, Lcom/google/android/util/AbstractMessageParser$Token$Type;->PHOTO:Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@72
    .line 666
    new-instance v0, Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@74
    const-string v1, "FLICKR"

    #@76
    const/16 v2, 0x9

    #@78
    const-string v3, "f"

    #@7a
    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/util/AbstractMessageParser$Token$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    #@7d
    sput-object v0, Lcom/google/android/util/AbstractMessageParser$Token$Type;->FLICKR:Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@7f
    .line 655
    const/16 v0, 0xa

    #@81
    new-array v0, v0, [Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@83
    sget-object v1, Lcom/google/android/util/AbstractMessageParser$Token$Type;->HTML:Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@85
    aput-object v1, v0, v4

    #@87
    sget-object v1, Lcom/google/android/util/AbstractMessageParser$Token$Type;->FORMAT:Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@89
    aput-object v1, v0, v5

    #@8b
    sget-object v1, Lcom/google/android/util/AbstractMessageParser$Token$Type;->LINK:Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@8d
    aput-object v1, v0, v6

    #@8f
    sget-object v1, Lcom/google/android/util/AbstractMessageParser$Token$Type;->SMILEY:Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@91
    aput-object v1, v0, v7

    #@93
    sget-object v1, Lcom/google/android/util/AbstractMessageParser$Token$Type;->ACRONYM:Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@95
    aput-object v1, v0, v8

    #@97
    const/4 v1, 0x5

    #@98
    sget-object v2, Lcom/google/android/util/AbstractMessageParser$Token$Type;->MUSIC:Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@9a
    aput-object v2, v0, v1

    #@9c
    const/4 v1, 0x6

    #@9d
    sget-object v2, Lcom/google/android/util/AbstractMessageParser$Token$Type;->GOOGLE_VIDEO:Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@9f
    aput-object v2, v0, v1

    #@a1
    const/4 v1, 0x7

    #@a2
    sget-object v2, Lcom/google/android/util/AbstractMessageParser$Token$Type;->YOUTUBE_VIDEO:Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@a4
    aput-object v2, v0, v1

    #@a6
    const/16 v1, 0x8

    #@a8
    sget-object v2, Lcom/google/android/util/AbstractMessageParser$Token$Type;->PHOTO:Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@aa
    aput-object v2, v0, v1

    #@ac
    const/16 v1, 0x9

    #@ae
    sget-object v2, Lcom/google/android/util/AbstractMessageParser$Token$Type;->FLICKR:Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@b0
    aput-object v2, v0, v1

    #@b2
    sput-object v0, Lcom/google/android/util/AbstractMessageParser$Token$Type;->$VALUES:[Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@b4
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter "stringRep"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 673
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 674
    iput-object p3, p0, Lcom/google/android/util/AbstractMessageParser$Token$Type;->stringRep:Ljava/lang/String;

    #@5
    .line 675
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/util/AbstractMessageParser$Token$Type;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 655
    const-class v0, Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/google/android/util/AbstractMessageParser$Token$Type;
    .registers 1

    #@0
    .prologue
    .line 655
    sget-object v0, Lcom/google/android/util/AbstractMessageParser$Token$Type;->$VALUES:[Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@2
    invoke-virtual {v0}, [Lcom/google/android/util/AbstractMessageParser$Token$Type;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@8
    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 679
    iget-object v0, p0, Lcom/google/android/util/AbstractMessageParser$Token$Type;->stringRep:Ljava/lang/String;

    #@2
    return-object v0
.end method
