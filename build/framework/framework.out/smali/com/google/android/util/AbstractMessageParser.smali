.class public abstract Lcom/google/android/util/AbstractMessageParser;
.super Ljava/lang/Object;
.source "AbstractMessageParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/util/AbstractMessageParser$1;,
        Lcom/google/android/util/AbstractMessageParser$Part;,
        Lcom/google/android/util/AbstractMessageParser$TrieNode;,
        Lcom/google/android/util/AbstractMessageParser$Format;,
        Lcom/google/android/util/AbstractMessageParser$Acronym;,
        Lcom/google/android/util/AbstractMessageParser$Smiley;,
        Lcom/google/android/util/AbstractMessageParser$FlickrPhoto;,
        Lcom/google/android/util/AbstractMessageParser$Photo;,
        Lcom/google/android/util/AbstractMessageParser$YouTubeVideo;,
        Lcom/google/android/util/AbstractMessageParser$Video;,
        Lcom/google/android/util/AbstractMessageParser$Link;,
        Lcom/google/android/util/AbstractMessageParser$MusicTrack;,
        Lcom/google/android/util/AbstractMessageParser$Html;,
        Lcom/google/android/util/AbstractMessageParser$Token;,
        Lcom/google/android/util/AbstractMessageParser$Resources;
    }
.end annotation


# static fields
.field public static final musicNote:Ljava/lang/String; = "\u266b "


# instance fields
.field private formatStart:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Character;",
            "Lcom/google/android/util/AbstractMessageParser$Format;",
            ">;"
        }
    .end annotation
.end field

.field private nextChar:I

.field private nextClass:I

.field private parseAcronyms:Z

.field private parseFormatting:Z

.field private parseMeText:Z

.field private parseMusic:Z

.field private parseSmilies:Z

.field private parseUrls:Z

.field private parts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/util/AbstractMessageParser$Part;",
            ">;"
        }
    .end annotation
.end field

.field private text:Ljava/lang/String;

.field private tokens:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/util/AbstractMessageParser$Token;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 10
    .parameter "text"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 89
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move v3, v2

    #@4
    move v4, v2

    #@5
    move v5, v2

    #@6
    move v6, v2

    #@7
    move v7, v2

    #@8
    invoke-direct/range {v0 .. v7}, Lcom/google/android/util/AbstractMessageParser;-><init>(Ljava/lang/String;ZZZZZZ)V

    #@b
    .line 90
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZZZZZZ)V
    .registers 9
    .parameter "text"
    .parameter "parseSmilies"
    .parameter "parseAcronyms"
    .parameter "parseFormatting"
    .parameter "parseUrls"
    .parameter "parseMusic"
    .parameter "parseMeText"

    #@0
    .prologue
    .line 100
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 101
    iput-object p1, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@5
    .line 102
    const/4 v0, 0x0

    #@6
    iput v0, p0, Lcom/google/android/util/AbstractMessageParser;->nextChar:I

    #@8
    .line 103
    const/16 v0, 0xa

    #@a
    iput v0, p0, Lcom/google/android/util/AbstractMessageParser;->nextClass:I

    #@c
    .line 104
    new-instance v0, Ljava/util/ArrayList;

    #@e
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@11
    iput-object v0, p0, Lcom/google/android/util/AbstractMessageParser;->parts:Ljava/util/ArrayList;

    #@13
    .line 105
    new-instance v0, Ljava/util/ArrayList;

    #@15
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@18
    iput-object v0, p0, Lcom/google/android/util/AbstractMessageParser;->tokens:Ljava/util/ArrayList;

    #@1a
    .line 106
    new-instance v0, Ljava/util/HashMap;

    #@1c
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@1f
    iput-object v0, p0, Lcom/google/android/util/AbstractMessageParser;->formatStart:Ljava/util/HashMap;

    #@21
    .line 107
    iput-boolean p2, p0, Lcom/google/android/util/AbstractMessageParser;->parseSmilies:Z

    #@23
    .line 108
    iput-boolean p3, p0, Lcom/google/android/util/AbstractMessageParser;->parseAcronyms:Z

    #@25
    .line 109
    iput-boolean p4, p0, Lcom/google/android/util/AbstractMessageParser;->parseFormatting:Z

    #@27
    .line 110
    iput-boolean p5, p0, Lcom/google/android/util/AbstractMessageParser;->parseUrls:Z

    #@29
    .line 111
    iput-boolean p6, p0, Lcom/google/android/util/AbstractMessageParser;->parseMusic:Z

    #@2b
    .line 112
    iput-boolean p7, p0, Lcom/google/android/util/AbstractMessageParser;->parseMeText:Z

    #@2d
    .line 113
    return-void
.end method

.method private addToken(Lcom/google/android/util/AbstractMessageParser$Token;)V
    .registers 3
    .parameter "token"

    #@0
    .prologue
    .line 1236
    iget-object v0, p0, Lcom/google/android/util/AbstractMessageParser;->tokens:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5
    .line 1237
    return-void
.end method

.method private addURLToken(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "url"
    .parameter "text"

    #@0
    .prologue
    .line 465
    invoke-static {p1, p2}, Lcom/google/android/util/AbstractMessageParser;->tokenForUrl(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/util/AbstractMessageParser$Token;

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0}, Lcom/google/android/util/AbstractMessageParser;->addToken(Lcom/google/android/util/AbstractMessageParser$Token;)V

    #@7
    .line 466
    return-void
.end method

.method private buildParts(Ljava/lang/String;)V
    .registers 6
    .parameter "meText"

    #@0
    .prologue
    .line 234
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v2, p0, Lcom/google/android/util/AbstractMessageParser;->tokens:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v2

    #@7
    if-ge v0, v2, :cond_3d

    #@9
    .line 235
    iget-object v2, p0, Lcom/google/android/util/AbstractMessageParser;->tokens:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Lcom/google/android/util/AbstractMessageParser$Token;

    #@11
    .line 236
    .local v1, token:Lcom/google/android/util/AbstractMessageParser$Token;
    invoke-virtual {v1}, Lcom/google/android/util/AbstractMessageParser$Token;->isMedia()Z

    #@14
    move-result v2

    #@15
    if-nez v2, :cond_29

    #@17
    iget-object v2, p0, Lcom/google/android/util/AbstractMessageParser;->parts:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@1c
    move-result v2

    #@1d
    if-eqz v2, :cond_29

    #@1f
    invoke-direct {p0}, Lcom/google/android/util/AbstractMessageParser;->lastPart()Lcom/google/android/util/AbstractMessageParser$Part;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Lcom/google/android/util/AbstractMessageParser$Part;->isMedia()Z

    #@26
    move-result v2

    #@27
    if-eqz v2, :cond_33

    #@29
    .line 237
    :cond_29
    iget-object v2, p0, Lcom/google/android/util/AbstractMessageParser;->parts:Ljava/util/ArrayList;

    #@2b
    new-instance v3, Lcom/google/android/util/AbstractMessageParser$Part;

    #@2d
    invoke-direct {v3}, Lcom/google/android/util/AbstractMessageParser$Part;-><init>()V

    #@30
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@33
    .line 239
    :cond_33
    invoke-direct {p0}, Lcom/google/android/util/AbstractMessageParser;->lastPart()Lcom/google/android/util/AbstractMessageParser$Part;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2, v1}, Lcom/google/android/util/AbstractMessageParser$Part;->add(Lcom/google/android/util/AbstractMessageParser$Token;)V

    #@3a
    .line 234
    add-int/lit8 v0, v0, 0x1

    #@3c
    goto :goto_1

    #@3d
    .line 243
    .end local v1           #token:Lcom/google/android/util/AbstractMessageParser$Token;
    :cond_3d
    iget-object v2, p0, Lcom/google/android/util/AbstractMessageParser;->parts:Ljava/util/ArrayList;

    #@3f
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@42
    move-result v2

    #@43
    if-lez v2, :cond_51

    #@45
    .line 244
    iget-object v2, p0, Lcom/google/android/util/AbstractMessageParser;->parts:Ljava/util/ArrayList;

    #@47
    const/4 v3, 0x0

    #@48
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@4b
    move-result-object v2

    #@4c
    check-cast v2, Lcom/google/android/util/AbstractMessageParser$Part;

    #@4e
    invoke-virtual {v2, p1}, Lcom/google/android/util/AbstractMessageParser$Part;->setMeText(Ljava/lang/String;)V

    #@51
    .line 246
    :cond_51
    return-void
.end method

.method private getCharClass(I)I
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 576
    if-ltz p1, :cond_a

    #@2
    iget-object v1, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@4
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@7
    move-result v1

    #@8
    if-gt v1, p1, :cond_c

    #@a
    .line 577
    :cond_a
    const/4 v1, 0x0

    #@b
    .line 593
    :goto_b
    return v1

    #@c
    .line 580
    :cond_c
    iget-object v1, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@e
    invoke-virtual {v1, p1}, Ljava/lang/String;->charAt(I)C

    #@11
    move-result v0

    #@12
    .line 581
    .local v0, ch:C
    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(C)Z

    #@15
    move-result v1

    #@16
    if-eqz v1, :cond_1a

    #@18
    .line 582
    const/4 v1, 0x1

    #@19
    goto :goto_b

    #@1a
    .line 583
    :cond_1a
    invoke-static {v0}, Ljava/lang/Character;->isLetter(C)Z

    #@1d
    move-result v1

    #@1e
    if-eqz v1, :cond_22

    #@20
    .line 584
    const/4 v1, 0x2

    #@21
    goto :goto_b

    #@22
    .line 585
    :cond_22
    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    #@25
    move-result v1

    #@26
    if-eqz v1, :cond_2a

    #@28
    .line 586
    const/4 v1, 0x3

    #@29
    goto :goto_b

    #@2a
    .line 587
    :cond_2a
    invoke-static {v0}, Lcom/google/android/util/AbstractMessageParser;->isPunctuation(C)Z

    #@2d
    move-result v1

    #@2e
    if-eqz v1, :cond_37

    #@30
    .line 591
    iget v1, p0, Lcom/google/android/util/AbstractMessageParser;->nextClass:I

    #@32
    add-int/lit8 v1, v1, 0x1

    #@34
    iput v1, p0, Lcom/google/android/util/AbstractMessageParser;->nextClass:I

    #@36
    goto :goto_b

    #@37
    .line 593
    :cond_37
    const/4 v1, 0x4

    #@38
    goto :goto_b
.end method

.method private isDomainChar(C)Z
    .registers 3
    .parameter "c"

    #@0
    .prologue
    .line 328
    const/16 v0, 0x2d

    #@2
    if-eq p1, v0, :cond_10

    #@4
    invoke-static {p1}, Ljava/lang/Character;->isLetter(C)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_10

    #@a
    invoke-static {p1}, Ljava/lang/Character;->isDigit(C)Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_12

    #@10
    :cond_10
    const/4 v0, 0x1

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method private static isFormatChar(C)Z
    .registers 2
    .parameter "ch"

    #@0
    .prologue
    .line 644
    sparse-switch p0, :sswitch_data_8

    #@3
    .line 649
    const/4 v0, 0x0

    #@4
    :goto_4
    return v0

    #@5
    .line 646
    :sswitch_5
    const/4 v0, 0x1

    #@6
    goto :goto_4

    #@7
    .line 644
    nop

    #@8
    :sswitch_data_8
    .sparse-switch
        0x2a -> :sswitch_5
        0x5e -> :sswitch_5
        0x5f -> :sswitch_5
    .end sparse-switch
.end method

.method private static isPunctuation(C)Z
    .registers 2
    .parameter "ch"

    #@0
    .prologue
    .line 629
    sparse-switch p0, :sswitch_data_8

    #@3
    .line 635
    const/4 v0, 0x0

    #@4
    :goto_4
    return v0

    #@5
    .line 632
    :sswitch_5
    const/4 v0, 0x1

    #@6
    goto :goto_4

    #@7
    .line 629
    nop

    #@8
    :sswitch_data_8
    .sparse-switch
        0x21 -> :sswitch_5
        0x22 -> :sswitch_5
        0x28 -> :sswitch_5
        0x29 -> :sswitch_5
        0x2c -> :sswitch_5
        0x2e -> :sswitch_5
        0x3a -> :sswitch_5
        0x3b -> :sswitch_5
        0x3f -> :sswitch_5
    .end sparse-switch
.end method

.method private static isSmileyBreak(CC)Z
    .registers 3
    .parameter "c1"
    .parameter "c2"

    #@0
    .prologue
    .line 604
    sparse-switch p0, :sswitch_data_c

    #@3
    .line 624
    :goto_3
    const/4 v0, 0x0

    #@4
    :goto_4
    return v0

    #@5
    .line 612
    :sswitch_5
    sparse-switch p1, :sswitch_data_52

    #@8
    goto :goto_3

    #@9
    .line 620
    :sswitch_9
    const/4 v0, 0x1

    #@a
    goto :goto_4

    #@b
    .line 604
    nop

    #@c
    :sswitch_data_c
    .sparse-switch
        0x24 -> :sswitch_5
        0x26 -> :sswitch_5
        0x2a -> :sswitch_5
        0x2b -> :sswitch_5
        0x2d -> :sswitch_5
        0x2f -> :sswitch_5
        0x3c -> :sswitch_5
        0x3d -> :sswitch_5
        0x3e -> :sswitch_5
        0x40 -> :sswitch_5
        0x5b -> :sswitch_5
        0x5c -> :sswitch_5
        0x5d -> :sswitch_5
        0x5e -> :sswitch_5
        0x7c -> :sswitch_5
        0x7d -> :sswitch_5
        0x7e -> :sswitch_5
    .end sparse-switch

    #@52
    .line 612
    :sswitch_data_52
    .sparse-switch
        0x23 -> :sswitch_9
        0x24 -> :sswitch_9
        0x25 -> :sswitch_9
        0x2a -> :sswitch_9
        0x2f -> :sswitch_9
        0x3c -> :sswitch_9
        0x3d -> :sswitch_9
        0x3e -> :sswitch_9
        0x40 -> :sswitch_9
        0x5b -> :sswitch_9
        0x5c -> :sswitch_9
        0x5e -> :sswitch_9
        0x7e -> :sswitch_9
    .end sparse-switch
.end method

.method private isSmileyBreak(I)Z
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 547
    if-lez p1, :cond_20

    #@2
    iget-object v0, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@4
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@7
    move-result v0

    #@8
    if-ge p1, v0, :cond_20

    #@a
    .line 548
    iget-object v0, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@c
    add-int/lit8 v1, p1, -0x1

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    #@11
    move-result v0

    #@12
    iget-object v1, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@14
    invoke-virtual {v1, p1}, Ljava/lang/String;->charAt(I)C

    #@17
    move-result v1

    #@18
    invoke-static {v0, v1}, Lcom/google/android/util/AbstractMessageParser;->isSmileyBreak(CC)Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_20

    #@1e
    .line 549
    const/4 v0, 0x1

    #@1f
    .line 553
    :goto_1f
    return v0

    #@20
    :cond_20
    const/4 v0, 0x0

    #@21
    goto :goto_1f
.end method

.method private isURLBreak(I)Z
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 561
    add-int/lit8 v0, p1, -0x1

    #@2
    invoke-direct {p0, v0}, Lcom/google/android/util/AbstractMessageParser;->getCharClass(I)I

    #@5
    move-result v0

    #@6
    packed-switch v0, :pswitch_data_e

    #@9
    .line 570
    const/4 v0, 0x1

    #@a
    :goto_a
    return v0

    #@b
    .line 565
    :pswitch_b
    const/4 v0, 0x0

    #@c
    goto :goto_a

    #@d
    .line 561
    nop

    #@e
    :pswitch_data_e
    .packed-switch 0x2
        :pswitch_b
        :pswitch_b
        :pswitch_b
    .end packed-switch
.end method

.method private isValidDomain(Ljava/lang/String;)Z
    .registers 4
    .parameter "domain"

    #@0
    .prologue
    .line 334
    invoke-virtual {p0}, Lcom/google/android/util/AbstractMessageParser;->getResources()Lcom/google/android/util/AbstractMessageParser$Resources;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0}, Lcom/google/android/util/AbstractMessageParser$Resources;->getDomainSuffixes()Lcom/google/android/util/AbstractMessageParser$TrieNode;

    #@7
    move-result-object v0

    #@8
    invoke-static {p1}, Lcom/google/android/util/AbstractMessageParser;->reverse(Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    invoke-static {v0, v1}, Lcom/google/android/util/AbstractMessageParser;->matches(Lcom/google/android/util/AbstractMessageParser$TrieNode;Ljava/lang/String;)Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_14

    #@12
    .line 335
    const/4 v0, 0x1

    #@13
    .line 337
    :goto_13
    return v0

    #@14
    :cond_14
    const/4 v0, 0x0

    #@15
    goto :goto_13
.end method

.method private isWordBreak(I)Z
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 542
    add-int/lit8 v0, p1, -0x1

    #@2
    invoke-direct {p0, v0}, Lcom/google/android/util/AbstractMessageParser;->getCharClass(I)I

    #@5
    move-result v0

    #@6
    invoke-direct {p0, p1}, Lcom/google/android/util/AbstractMessageParser;->getCharClass(I)I

    #@9
    move-result v1

    #@a
    if-eq v0, v1, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method private lastPart()Lcom/google/android/util/AbstractMessageParser$Part;
    .registers 3

    #@0
    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/util/AbstractMessageParser;->parts:Ljava/util/ArrayList;

    #@2
    iget-object v1, p0, Lcom/google/android/util/AbstractMessageParser;->parts:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v1

    #@8
    add-int/lit8 v1, v1, -0x1

    #@a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Lcom/google/android/util/AbstractMessageParser$Part;

    #@10
    return-object v0
.end method

.method private static longestMatch(Lcom/google/android/util/AbstractMessageParser$TrieNode;Lcom/google/android/util/AbstractMessageParser;I)Lcom/google/android/util/AbstractMessageParser$TrieNode;
    .registers 4
    .parameter "root"
    .parameter "p"
    .parameter "start"

    #@0
    .prologue
    .line 1400
    const/4 v0, 0x0

    #@1
    invoke-static {p0, p1, p2, v0}, Lcom/google/android/util/AbstractMessageParser;->longestMatch(Lcom/google/android/util/AbstractMessageParser$TrieNode;Lcom/google/android/util/AbstractMessageParser;IZ)Lcom/google/android/util/AbstractMessageParser$TrieNode;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method private static longestMatch(Lcom/google/android/util/AbstractMessageParser$TrieNode;Lcom/google/android/util/AbstractMessageParser;IZ)Lcom/google/android/util/AbstractMessageParser$TrieNode;
    .registers 8
    .parameter "root"
    .parameter "p"
    .parameter "start"
    .parameter "smiley"

    #@0
    .prologue
    .line 1410
    move v1, p2

    #@1
    .line 1411
    .local v1, index:I
    const/4 v0, 0x0

    #@2
    .line 1412
    .local v0, bestMatch:Lcom/google/android/util/AbstractMessageParser$TrieNode;
    :goto_2
    invoke-virtual {p1}, Lcom/google/android/util/AbstractMessageParser;->getRawText()Ljava/lang/String;

    #@5
    move-result-object v3

    #@6
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@9
    move-result v3

    #@a
    if-ge v1, v3, :cond_1d

    #@c
    .line 1413
    invoke-virtual {p1}, Lcom/google/android/util/AbstractMessageParser;->getRawText()Ljava/lang/String;

    #@f
    move-result-object v3

    #@10
    add-int/lit8 v2, v1, 0x1

    #@12
    .end local v1           #index:I
    .local v2, index:I
    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    #@15
    move-result v3

    #@16
    invoke-virtual {p0, v3}, Lcom/google/android/util/AbstractMessageParser$TrieNode;->getChild(C)Lcom/google/android/util/AbstractMessageParser$TrieNode;

    #@19
    move-result-object p0

    #@1a
    .line 1414
    if-nez p0, :cond_1e

    #@1c
    move v1, v2

    #@1d
    .line 1424
    .end local v2           #index:I
    .restart local v1       #index:I
    :cond_1d
    return-object v0

    #@1e
    .line 1416
    .end local v1           #index:I
    .restart local v2       #index:I
    :cond_1e
    invoke-virtual {p0}, Lcom/google/android/util/AbstractMessageParser$TrieNode;->exists()Z

    #@21
    move-result v3

    #@22
    if-eqz v3, :cond_38

    #@24
    .line 1417
    invoke-direct {p1, v2}, Lcom/google/android/util/AbstractMessageParser;->isWordBreak(I)Z

    #@27
    move-result v3

    #@28
    if-eqz v3, :cond_2d

    #@2a
    .line 1418
    move-object v0, p0

    #@2b
    move v1, v2

    #@2c
    .end local v2           #index:I
    .restart local v1       #index:I
    goto :goto_2

    #@2d
    .line 1419
    .end local v1           #index:I
    .restart local v2       #index:I
    :cond_2d
    if-eqz p3, :cond_38

    #@2f
    invoke-direct {p1, v2}, Lcom/google/android/util/AbstractMessageParser;->isSmileyBreak(I)Z

    #@32
    move-result v3

    #@33
    if-eqz v3, :cond_38

    #@35
    .line 1420
    move-object v0, p0

    #@36
    move v1, v2

    #@37
    .end local v2           #index:I
    .restart local v1       #index:I
    goto :goto_2

    #@38
    .end local v1           #index:I
    .restart local v2       #index:I
    :cond_38
    move v1, v2

    #@39
    .end local v2           #index:I
    .restart local v1       #index:I
    goto :goto_2
.end method

.method private static matches(Lcom/google/android/util/AbstractMessageParser$TrieNode;Ljava/lang/String;)Z
    .registers 5
    .parameter "root"
    .parameter "str"

    #@0
    .prologue
    .line 1382
    const/4 v0, 0x0

    #@1
    .line 1383
    .local v0, index:I
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@4
    move-result v2

    #@5
    if-ge v0, v2, :cond_14

    #@7
    .line 1384
    add-int/lit8 v1, v0, 0x1

    #@9
    .end local v0           #index:I
    .local v1, index:I
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    #@c
    move-result v2

    #@d
    invoke-virtual {p0, v2}, Lcom/google/android/util/AbstractMessageParser$TrieNode;->getChild(C)Lcom/google/android/util/AbstractMessageParser$TrieNode;

    #@10
    move-result-object p0

    #@11
    .line 1385
    if-nez p0, :cond_16

    #@13
    move v0, v1

    #@14
    .line 1391
    .end local v1           #index:I
    .restart local v0       #index:I
    :cond_14
    const/4 v2, 0x0

    #@15
    :goto_15
    return v2

    #@16
    .line 1387
    .end local v0           #index:I
    .restart local v1       #index:I
    :cond_16
    invoke-virtual {p0}, Lcom/google/android/util/AbstractMessageParser$TrieNode;->exists()Z

    #@19
    move-result v2

    #@1a
    if-eqz v2, :cond_1f

    #@1c
    .line 1388
    const/4 v2, 0x1

    #@1d
    move v0, v1

    #@1e
    .end local v1           #index:I
    .restart local v0       #index:I
    goto :goto_15

    #@1f
    .end local v0           #index:I
    .restart local v1       #index:I
    :cond_1f
    move v0, v1

    #@20
    .end local v1           #index:I
    .restart local v0       #index:I
    goto :goto_1
.end method

.method private parseAcronym()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 313
    iget-boolean v2, p0, Lcom/google/android/util/AbstractMessageParser;->parseAcronyms:Z

    #@3
    if-nez v2, :cond_6

    #@5
    .line 322
    :cond_5
    :goto_5
    return v1

    #@6
    .line 316
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/util/AbstractMessageParser;->getResources()Lcom/google/android/util/AbstractMessageParser$Resources;

    #@9
    move-result-object v2

    #@a
    invoke-interface {v2}, Lcom/google/android/util/AbstractMessageParser$Resources;->getAcronyms()Lcom/google/android/util/AbstractMessageParser$TrieNode;

    #@d
    move-result-object v2

    #@e
    iget v3, p0, Lcom/google/android/util/AbstractMessageParser;->nextChar:I

    #@10
    invoke-static {v2, p0, v3}, Lcom/google/android/util/AbstractMessageParser;->longestMatch(Lcom/google/android/util/AbstractMessageParser$TrieNode;Lcom/google/android/util/AbstractMessageParser;I)Lcom/google/android/util/AbstractMessageParser$TrieNode;

    #@13
    move-result-object v0

    #@14
    .line 317
    .local v0, match:Lcom/google/android/util/AbstractMessageParser$TrieNode;
    if-eqz v0, :cond_5

    #@16
    .line 320
    new-instance v1, Lcom/google/android/util/AbstractMessageParser$Acronym;

    #@18
    invoke-virtual {v0}, Lcom/google/android/util/AbstractMessageParser$TrieNode;->getText()Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v0}, Lcom/google/android/util/AbstractMessageParser$TrieNode;->getValue()Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    invoke-direct {v1, v2, v3}, Lcom/google/android/util/AbstractMessageParser$Acronym;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@23
    invoke-direct {p0, v1}, Lcom/google/android/util/AbstractMessageParser;->addToken(Lcom/google/android/util/AbstractMessageParser$Token;)V

    #@26
    .line 321
    iget v1, p0, Lcom/google/android/util/AbstractMessageParser;->nextChar:I

    #@28
    invoke-virtual {v0}, Lcom/google/android/util/AbstractMessageParser$TrieNode;->getText()Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@2f
    move-result v2

    #@30
    add-int/2addr v1, v2

    #@31
    iput v1, p0, Lcom/google/android/util/AbstractMessageParser;->nextChar:I

    #@33
    .line 322
    const/4 v1, 0x1

    #@34
    goto :goto_5
.end method

.method private parseFormatting()Z
    .registers 13

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 485
    iget-boolean v10, p0, Lcom/google/android/util/AbstractMessageParser;->parseFormatting:Z

    #@4
    if-nez v10, :cond_7

    #@6
    .line 537
    :cond_6
    :goto_6
    return v8

    #@7
    .line 488
    :cond_7
    iget v2, p0, Lcom/google/android/util/AbstractMessageParser;->nextChar:I

    #@9
    .line 489
    .local v2, endChar:I
    :goto_9
    iget-object v10, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@b
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    #@e
    move-result v10

    #@f
    if-ge v2, v10, :cond_20

    #@11
    iget-object v10, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@13
    invoke-virtual {v10, v2}, Ljava/lang/String;->charAt(I)C

    #@16
    move-result v10

    #@17
    invoke-static {v10}, Lcom/google/android/util/AbstractMessageParser;->isFormatChar(C)Z

    #@1a
    move-result v10

    #@1b
    if-eqz v10, :cond_20

    #@1d
    .line 490
    add-int/lit8 v2, v2, 0x1

    #@1f
    goto :goto_9

    #@20
    .line 493
    :cond_20
    iget v10, p0, Lcom/google/android/util/AbstractMessageParser;->nextChar:I

    #@22
    if-eq v2, v10, :cond_6

    #@24
    invoke-direct {p0, v2}, Lcom/google/android/util/AbstractMessageParser;->isWordBreak(I)Z

    #@27
    move-result v10

    #@28
    if-eqz v10, :cond_6

    #@2a
    .line 500
    new-instance v6, Ljava/util/LinkedHashMap;

    #@2c
    invoke-direct {v6}, Ljava/util/LinkedHashMap;-><init>()V

    #@2f
    .line 503
    .local v6, seenCharacters:Ljava/util/LinkedHashMap;,"Ljava/util/LinkedHashMap<Ljava/lang/Character;Ljava/lang/Boolean;>;"
    iget v4, p0, Lcom/google/android/util/AbstractMessageParser;->nextChar:I

    #@31
    .local v4, index:I
    :goto_31
    if-ge v4, v2, :cond_79

    #@33
    .line 504
    iget-object v10, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@35
    invoke-virtual {v10, v4}, Ljava/lang/String;->charAt(I)C

    #@38
    move-result v0

    #@39
    .line 505
    .local v0, ch:C
    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@3c
    move-result-object v5

    #@3d
    .line 506
    .local v5, key:Ljava/lang/Character;
    invoke-virtual {v6, v5}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    #@40
    move-result v10

    #@41
    if-eqz v10, :cond_4e

    #@43
    .line 509
    new-instance v10, Lcom/google/android/util/AbstractMessageParser$Format;

    #@45
    invoke-direct {v10, v0, v8}, Lcom/google/android/util/AbstractMessageParser$Format;-><init>(CZ)V

    #@48
    invoke-direct {p0, v10}, Lcom/google/android/util/AbstractMessageParser;->addToken(Lcom/google/android/util/AbstractMessageParser$Token;)V

    #@4b
    .line 503
    :goto_4b
    add-int/lit8 v4, v4, 0x1

    #@4d
    goto :goto_31

    #@4e
    .line 511
    :cond_4e
    iget-object v10, p0, Lcom/google/android/util/AbstractMessageParser;->formatStart:Ljava/util/HashMap;

    #@50
    invoke-virtual {v10, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@53
    move-result-object v7

    #@54
    check-cast v7, Lcom/google/android/util/AbstractMessageParser$Format;

    #@56
    .line 512
    .local v7, start:Lcom/google/android/util/AbstractMessageParser$Format;
    if-eqz v7, :cond_66

    #@58
    .line 514
    invoke-virtual {v7, v9}, Lcom/google/android/util/AbstractMessageParser$Format;->setMatched(Z)V

    #@5b
    .line 515
    iget-object v10, p0, Lcom/google/android/util/AbstractMessageParser;->formatStart:Ljava/util/HashMap;

    #@5d
    invoke-virtual {v10, v5}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@60
    .line 516
    sget-object v10, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    #@62
    invoke-virtual {v6, v5, v10}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@65
    goto :goto_4b

    #@66
    .line 519
    :cond_66
    new-instance v7, Lcom/google/android/util/AbstractMessageParser$Format;

    #@68
    .end local v7           #start:Lcom/google/android/util/AbstractMessageParser$Format;
    invoke-direct {v7, v0, v9}, Lcom/google/android/util/AbstractMessageParser$Format;-><init>(CZ)V

    #@6b
    .line 520
    .restart local v7       #start:Lcom/google/android/util/AbstractMessageParser$Format;
    iget-object v10, p0, Lcom/google/android/util/AbstractMessageParser;->formatStart:Ljava/util/HashMap;

    #@6d
    invoke-virtual {v10, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@70
    .line 521
    invoke-direct {p0, v7}, Lcom/google/android/util/AbstractMessageParser;->addToken(Lcom/google/android/util/AbstractMessageParser$Token;)V

    #@73
    .line 522
    sget-object v10, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    #@75
    invoke-virtual {v6, v5, v10}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@78
    goto :goto_4b

    #@79
    .line 528
    .end local v0           #ch:C
    .end local v5           #key:Ljava/lang/Character;
    .end local v7           #start:Lcom/google/android/util/AbstractMessageParser$Format;
    :cond_79
    invoke-virtual {v6}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    #@7c
    move-result-object v10

    #@7d
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@80
    move-result-object v3

    #@81
    .local v3, i$:Ljava/util/Iterator;
    :cond_81
    :goto_81
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@84
    move-result v10

    #@85
    if-eqz v10, :cond_a5

    #@87
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@8a
    move-result-object v5

    #@8b
    check-cast v5, Ljava/lang/Character;

    #@8d
    .line 529
    .restart local v5       #key:Ljava/lang/Character;
    invoke-virtual {v6, v5}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@90
    move-result-object v10

    #@91
    sget-object v11, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    #@93
    if-ne v10, v11, :cond_81

    #@95
    .line 530
    new-instance v1, Lcom/google/android/util/AbstractMessageParser$Format;

    #@97
    invoke-virtual {v5}, Ljava/lang/Character;->charValue()C

    #@9a
    move-result v10

    #@9b
    invoke-direct {v1, v10, v8}, Lcom/google/android/util/AbstractMessageParser$Format;-><init>(CZ)V

    #@9e
    .line 531
    .local v1, end:Lcom/google/android/util/AbstractMessageParser$Format;
    invoke-virtual {v1, v9}, Lcom/google/android/util/AbstractMessageParser$Format;->setMatched(Z)V

    #@a1
    .line 532
    invoke-direct {p0, v1}, Lcom/google/android/util/AbstractMessageParser;->addToken(Lcom/google/android/util/AbstractMessageParser$Token;)V

    #@a4
    goto :goto_81

    #@a5
    .line 536
    .end local v1           #end:Lcom/google/android/util/AbstractMessageParser$Format;
    .end local v5           #key:Ljava/lang/Character;
    :cond_a5
    iput v2, p0, Lcom/google/android/util/AbstractMessageParser;->nextChar:I

    #@a7
    move v8, v9

    #@a8
    .line 537
    goto/16 :goto_6
.end method

.method private parseMusicTrack()Z
    .registers 4

    #@0
    .prologue
    .line 257
    iget-boolean v0, p0, Lcom/google/android/util/AbstractMessageParser;->parseMusic:Z

    #@2
    if-eqz v0, :cond_2e

    #@4
    iget-object v0, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@6
    const-string/jumbo v1, "\u266b "

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_2e

    #@f
    .line 258
    new-instance v0, Lcom/google/android/util/AbstractMessageParser$MusicTrack;

    #@11
    iget-object v1, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@13
    const-string/jumbo v2, "\u266b "

    #@16
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@19
    move-result v2

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-direct {v0, v1}, Lcom/google/android/util/AbstractMessageParser$MusicTrack;-><init>(Ljava/lang/String;)V

    #@21
    invoke-direct {p0, v0}, Lcom/google/android/util/AbstractMessageParser;->addToken(Lcom/google/android/util/AbstractMessageParser$Token;)V

    #@24
    .line 259
    iget-object v0, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@26
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@29
    move-result v0

    #@2a
    iput v0, p0, Lcom/google/android/util/AbstractMessageParser;->nextChar:I

    #@2c
    .line 260
    const/4 v0, 0x1

    #@2d
    .line 262
    :goto_2d
    return v0

    #@2e
    :cond_2e
    const/4 v0, 0x0

    #@2f
    goto :goto_2d
.end method

.method private parseSmiley()Z
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x3

    #@1
    const/4 v7, 0x2

    #@2
    const/4 v4, 0x1

    #@3
    const/4 v3, 0x0

    #@4
    .line 290
    iget-boolean v5, p0, Lcom/google/android/util/AbstractMessageParser;->parseSmilies:Z

    #@6
    if-nez v5, :cond_9

    #@8
    .line 306
    :cond_8
    :goto_8
    return v3

    #@9
    .line 293
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/util/AbstractMessageParser;->getResources()Lcom/google/android/util/AbstractMessageParser$Resources;

    #@c
    move-result-object v5

    #@d
    invoke-interface {v5}, Lcom/google/android/util/AbstractMessageParser$Resources;->getSmileys()Lcom/google/android/util/AbstractMessageParser$TrieNode;

    #@10
    move-result-object v5

    #@11
    iget v6, p0, Lcom/google/android/util/AbstractMessageParser;->nextChar:I

    #@13
    invoke-static {v5, p0, v6, v4}, Lcom/google/android/util/AbstractMessageParser;->longestMatch(Lcom/google/android/util/AbstractMessageParser$TrieNode;Lcom/google/android/util/AbstractMessageParser;IZ)Lcom/google/android/util/AbstractMessageParser$TrieNode;

    #@16
    move-result-object v0

    #@17
    .line 295
    .local v0, match:Lcom/google/android/util/AbstractMessageParser$TrieNode;
    if-eqz v0, :cond_8

    #@19
    .line 298
    iget v5, p0, Lcom/google/android/util/AbstractMessageParser;->nextChar:I

    #@1b
    add-int/lit8 v5, v5, -0x1

    #@1d
    invoke-direct {p0, v5}, Lcom/google/android/util/AbstractMessageParser;->getCharClass(I)I

    #@20
    move-result v2

    #@21
    .line 299
    .local v2, previousCharClass:I
    iget v5, p0, Lcom/google/android/util/AbstractMessageParser;->nextChar:I

    #@23
    invoke-virtual {v0}, Lcom/google/android/util/AbstractMessageParser$TrieNode;->getText()Ljava/lang/String;

    #@26
    move-result-object v6

    #@27
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@2a
    move-result v6

    #@2b
    add-int/2addr v5, v6

    #@2c
    invoke-direct {p0, v5}, Lcom/google/android/util/AbstractMessageParser;->getCharClass(I)I

    #@2f
    move-result v1

    #@30
    .line 300
    .local v1, nextCharClass:I
    if-eq v2, v7, :cond_34

    #@32
    if-ne v2, v8, :cond_38

    #@34
    :cond_34
    if-eq v1, v7, :cond_8

    #@36
    if-eq v1, v8, :cond_8

    #@38
    .line 304
    :cond_38
    new-instance v3, Lcom/google/android/util/AbstractMessageParser$Smiley;

    #@3a
    invoke-virtual {v0}, Lcom/google/android/util/AbstractMessageParser$TrieNode;->getText()Ljava/lang/String;

    #@3d
    move-result-object v5

    #@3e
    invoke-direct {v3, v5}, Lcom/google/android/util/AbstractMessageParser$Smiley;-><init>(Ljava/lang/String;)V

    #@41
    invoke-direct {p0, v3}, Lcom/google/android/util/AbstractMessageParser;->addToken(Lcom/google/android/util/AbstractMessageParser$Token;)V

    #@44
    .line 305
    iget v3, p0, Lcom/google/android/util/AbstractMessageParser;->nextChar:I

    #@46
    invoke-virtual {v0}, Lcom/google/android/util/AbstractMessageParser$TrieNode;->getText()Ljava/lang/String;

    #@49
    move-result-object v5

    #@4a
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@4d
    move-result v5

    #@4e
    add-int/2addr v3, v5

    #@4f
    iput v3, p0, Lcom/google/android/util/AbstractMessageParser;->nextChar:I

    #@51
    move v3, v4

    #@52
    .line 306
    goto :goto_8
.end method

.method private parseText()V
    .registers 7

    #@0
    .prologue
    .line 267
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 268
    .local v0, buf:Ljava/lang/StringBuilder;
    iget v2, p0, Lcom/google/android/util/AbstractMessageParser;->nextChar:I

    #@7
    .line 270
    .local v2, start:I
    :cond_7
    iget-object v3, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@9
    iget v4, p0, Lcom/google/android/util/AbstractMessageParser;->nextChar:I

    #@b
    add-int/lit8 v5, v4, 0x1

    #@d
    iput v5, p0, Lcom/google/android/util/AbstractMessageParser;->nextChar:I

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    #@12
    move-result v1

    #@13
    .line 271
    .local v1, ch:C
    sparse-switch v1, :sswitch_data_5a

    #@16
    .line 278
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@19
    .line 280
    :goto_19
    iget v3, p0, Lcom/google/android/util/AbstractMessageParser;->nextChar:I

    #@1b
    invoke-direct {p0, v3}, Lcom/google/android/util/AbstractMessageParser;->isWordBreak(I)Z

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_7

    #@21
    .line 282
    new-instance v3, Lcom/google/android/util/AbstractMessageParser$Html;

    #@23
    iget-object v4, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@25
    iget v5, p0, Lcom/google/android/util/AbstractMessageParser;->nextChar:I

    #@27
    invoke-virtual {v4, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2a
    move-result-object v4

    #@2b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v5

    #@2f
    invoke-direct {v3, v4, v5}, Lcom/google/android/util/AbstractMessageParser$Html;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@32
    invoke-direct {p0, v3}, Lcom/google/android/util/AbstractMessageParser;->addToken(Lcom/google/android/util/AbstractMessageParser$Token;)V

    #@35
    .line 283
    return-void

    #@36
    .line 272
    :sswitch_36
    const-string v3, "&lt;"

    #@38
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    goto :goto_19

    #@3c
    .line 273
    :sswitch_3c
    const-string v3, "&gt;"

    #@3e
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    goto :goto_19

    #@42
    .line 274
    :sswitch_42
    const-string v3, "&amp;"

    #@44
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    goto :goto_19

    #@48
    .line 275
    :sswitch_48
    const-string v3, "&quot;"

    #@4a
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    goto :goto_19

    #@4e
    .line 276
    :sswitch_4e
    const-string v3, "&apos;"

    #@50
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    goto :goto_19

    #@54
    .line 277
    :sswitch_54
    const-string v3, "<br>"

    #@56
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    goto :goto_19

    #@5a
    .line 271
    :sswitch_data_5a
    .sparse-switch
        0xa -> :sswitch_54
        0x22 -> :sswitch_48
        0x26 -> :sswitch_42
        0x27 -> :sswitch_4e
        0x3c -> :sswitch_36
        0x3e -> :sswitch_3c
    .end sparse-switch
.end method

.method private parseURL()Z
    .registers 14

    #@0
    .prologue
    const/16 v12, 0x3a

    #@2
    const/16 v11, 0x2e

    #@4
    const/4 v9, 0x0

    #@5
    .line 346
    iget-boolean v10, p0, Lcom/google/android/util/AbstractMessageParser;->parseUrls:Z

    #@7
    if-eqz v10, :cond_11

    #@9
    iget v10, p0, Lcom/google/android/util/AbstractMessageParser;->nextChar:I

    #@b
    invoke-direct {p0, v10}, Lcom/google/android/util/AbstractMessageParser;->isURLBreak(I)Z

    #@e
    move-result v10

    #@f
    if-nez v10, :cond_12

    #@11
    .line 457
    :cond_11
    :goto_11
    return v9

    #@12
    .line 350
    :cond_12
    iget v6, p0, Lcom/google/android/util/AbstractMessageParser;->nextChar:I

    #@14
    .line 353
    .local v6, start:I
    move v4, v6

    #@15
    .line 354
    .local v4, index:I
    :goto_15
    iget-object v10, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@17
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    #@1a
    move-result v10

    #@1b
    if-ge v4, v10, :cond_2c

    #@1d
    iget-object v10, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@1f
    invoke-virtual {v10, v4}, Ljava/lang/String;->charAt(I)C

    #@22
    move-result v10

    #@23
    invoke-direct {p0, v10}, Lcom/google/android/util/AbstractMessageParser;->isDomainChar(C)Z

    #@26
    move-result v10

    #@27
    if-eqz v10, :cond_2c

    #@29
    .line 355
    add-int/lit8 v4, v4, 0x1

    #@2b
    goto :goto_15

    #@2c
    .line 358
    :cond_2c
    const-string v7, ""

    #@2e
    .line 359
    .local v7, url:Ljava/lang/String;
    const/4 v3, 0x0

    #@2f
    .line 361
    .local v3, done:Z
    iget-object v10, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@31
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    #@34
    move-result v10

    #@35
    if-eq v4, v10, :cond_11

    #@37
    .line 363
    iget-object v10, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@39
    invoke-virtual {v10, v4}, Ljava/lang/String;->charAt(I)C

    #@3c
    move-result v10

    #@3d
    if-ne v10, v12, :cond_6e

    #@3f
    .line 365
    iget-object v10, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@41
    iget v11, p0, Lcom/google/android/util/AbstractMessageParser;->nextChar:I

    #@43
    invoke-virtual {v10, v11, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@46
    move-result-object v5

    #@47
    .line 366
    .local v5, scheme:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/util/AbstractMessageParser;->getResources()Lcom/google/android/util/AbstractMessageParser$Resources;

    #@4a
    move-result-object v10

    #@4b
    invoke-interface {v10}, Lcom/google/android/util/AbstractMessageParser$Resources;->getSchemes()Ljava/util/Set;

    #@4e
    move-result-object v10

    #@4f
    invoke-interface {v10, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    #@52
    move-result v10

    #@53
    if-eqz v10, :cond_11

    #@55
    .line 443
    .end local v5           #scheme:Ljava/lang/String;
    :goto_55
    if-nez v3, :cond_128

    #@57
    .line 444
    :goto_57
    iget-object v9, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@59
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    #@5c
    move-result v9

    #@5d
    if-ge v4, v9, :cond_128

    #@5f
    iget-object v9, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@61
    invoke-virtual {v9, v4}, Ljava/lang/String;->charAt(I)C

    #@64
    move-result v9

    #@65
    invoke-static {v9}, Ljava/lang/Character;->isWhitespace(C)Z

    #@68
    move-result v9

    #@69
    if-nez v9, :cond_128

    #@6b
    .line 446
    add-int/lit8 v4, v4, 0x1

    #@6d
    goto :goto_57

    #@6e
    .line 369
    :cond_6e
    iget-object v10, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@70
    invoke-virtual {v10, v4}, Ljava/lang/String;->charAt(I)C

    #@73
    move-result v10

    #@74
    if-ne v10, v11, :cond_11

    #@76
    .line 371
    :goto_76
    iget-object v10, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@78
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    #@7b
    move-result v10

    #@7c
    if-ge v4, v10, :cond_8c

    #@7e
    .line 372
    iget-object v10, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@80
    invoke-virtual {v10, v4}, Ljava/lang/String;->charAt(I)C

    #@83
    move-result v0

    #@84
    .line 373
    .local v0, ch:C
    if-eq v0, v11, :cond_d3

    #@86
    invoke-direct {p0, v0}, Lcom/google/android/util/AbstractMessageParser;->isDomainChar(C)Z

    #@89
    move-result v10

    #@8a
    if-nez v10, :cond_d3

    #@8c
    .line 382
    .end local v0           #ch:C
    :cond_8c
    iget-object v10, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@8e
    iget v11, p0, Lcom/google/android/util/AbstractMessageParser;->nextChar:I

    #@90
    invoke-virtual {v10, v11, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@93
    move-result-object v2

    #@94
    .line 383
    .local v2, domain:Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/google/android/util/AbstractMessageParser;->isValidDomain(Ljava/lang/String;)Z

    #@97
    move-result v10

    #@98
    if-eqz v10, :cond_11

    #@9a
    .line 389
    add-int/lit8 v10, v4, 0x1

    #@9c
    iget-object v11, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@9e
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    #@a1
    move-result v11

    #@a2
    if-ge v10, v11, :cond_d6

    #@a4
    iget-object v10, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@a6
    invoke-virtual {v10, v4}, Ljava/lang/String;->charAt(I)C

    #@a9
    move-result v10

    #@aa
    if-ne v10, v12, :cond_d6

    #@ac
    .line 390
    iget-object v10, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@ae
    add-int/lit8 v11, v4, 0x1

    #@b0
    invoke-virtual {v10, v11}, Ljava/lang/String;->charAt(I)C

    #@b3
    move-result v0

    #@b4
    .line 391
    .restart local v0       #ch:C
    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    #@b7
    move-result v10

    #@b8
    if-eqz v10, :cond_d6

    #@ba
    .line 392
    add-int/lit8 v4, v4, 0x1

    #@bc
    .line 393
    :goto_bc
    iget-object v10, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@be
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    #@c1
    move-result v10

    #@c2
    if-ge v4, v10, :cond_d6

    #@c4
    iget-object v10, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@c6
    invoke-virtual {v10, v4}, Ljava/lang/String;->charAt(I)C

    #@c9
    move-result v10

    #@ca
    invoke-static {v10}, Ljava/lang/Character;->isDigit(C)Z

    #@cd
    move-result v10

    #@ce
    if-eqz v10, :cond_d6

    #@d0
    .line 395
    add-int/lit8 v4, v4, 0x1

    #@d2
    goto :goto_bc

    #@d3
    .line 376
    .end local v2           #domain:Ljava/lang/String;
    :cond_d3
    add-int/lit8 v4, v4, 0x1

    #@d5
    .line 378
    goto :goto_76

    #@d6
    .line 406
    .end local v0           #ch:C
    .restart local v2       #domain:Ljava/lang/String;
    :cond_d6
    iget-object v10, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@d8
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    #@db
    move-result v10

    #@dc
    if-ne v4, v10, :cond_e3

    #@de
    .line 407
    const/4 v3, 0x1

    #@df
    .line 436
    :cond_df
    :goto_df
    const-string v7, "http://"

    #@e1
    .line 437
    goto/16 :goto_55

    #@e3
    .line 409
    :cond_e3
    iget-object v10, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@e5
    invoke-virtual {v10, v4}, Ljava/lang/String;->charAt(I)C

    #@e8
    move-result v0

    #@e9
    .line 410
    .restart local v0       #ch:C
    const/16 v10, 0x3f

    #@eb
    if-ne v0, v10, :cond_10f

    #@ed
    .line 413
    add-int/lit8 v9, v4, 0x1

    #@ef
    iget-object v10, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@f1
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    #@f4
    move-result v10

    #@f5
    if-ne v9, v10, :cond_f9

    #@f7
    .line 414
    const/4 v3, 0x1

    #@f8
    goto :goto_df

    #@f9
    .line 416
    :cond_f9
    iget-object v9, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@fb
    add-int/lit8 v10, v4, 0x1

    #@fd
    invoke-virtual {v9, v10}, Ljava/lang/String;->charAt(I)C

    #@100
    move-result v1

    #@101
    .line 417
    .local v1, ch2:C
    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    #@104
    move-result v9

    #@105
    if-nez v9, :cond_10d

    #@107
    invoke-static {v1}, Lcom/google/android/util/AbstractMessageParser;->isPunctuation(C)Z

    #@10a
    move-result v9

    #@10b
    if-eqz v9, :cond_df

    #@10d
    .line 418
    :cond_10d
    const/4 v3, 0x1

    #@10e
    goto :goto_df

    #@10f
    .line 421
    .end local v1           #ch2:C
    :cond_10f
    invoke-static {v0}, Lcom/google/android/util/AbstractMessageParser;->isPunctuation(C)Z

    #@112
    move-result v10

    #@113
    if-eqz v10, :cond_117

    #@115
    .line 422
    const/4 v3, 0x1

    #@116
    goto :goto_df

    #@117
    .line 423
    :cond_117
    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(C)Z

    #@11a
    move-result v10

    #@11b
    if-eqz v10, :cond_11f

    #@11d
    .line 424
    const/4 v3, 0x1

    #@11e
    goto :goto_df

    #@11f
    .line 425
    :cond_11f
    const/16 v10, 0x2f

    #@121
    if-eq v0, v10, :cond_df

    #@123
    const/16 v10, 0x23

    #@125
    if-ne v0, v10, :cond_11

    #@127
    goto :goto_df

    #@128
    .line 450
    .end local v0           #ch:C
    .end local v2           #domain:Ljava/lang/String;
    :cond_128
    iget-object v9, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@12a
    invoke-virtual {v9, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@12d
    move-result-object v8

    #@12e
    .line 451
    .local v8, urlText:Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    #@130
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@133
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@136
    move-result-object v9

    #@137
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13a
    move-result-object v9

    #@13b
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13e
    move-result-object v7

    #@13f
    .line 454
    invoke-direct {p0, v7, v8}, Lcom/google/android/util/AbstractMessageParser;->addURLToken(Ljava/lang/String;Ljava/lang/String;)V

    #@142
    .line 456
    iput v4, p0, Lcom/google/android/util/AbstractMessageParser;->nextChar:I

    #@144
    .line 457
    const/4 v9, 0x1

    #@145
    goto/16 :goto_11
.end method

.method protected static reverse(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "str"

    #@0
    .prologue
    .line 1331
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 1332
    .local v0, buf:Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@8
    move-result v2

    #@9
    add-int/lit8 v1, v2, -0x1

    #@b
    .local v1, i:I
    :goto_b
    if-ltz v1, :cond_17

    #@d
    .line 1333
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@10
    move-result v2

    #@11
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@14
    .line 1332
    add-int/lit8 v1, v1, -0x1

    #@16
    goto :goto_b

    #@17
    .line 1335
    :cond_17
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    return-object v2
.end method

.method public static tokenForUrl(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/util/AbstractMessageParser$Token;
    .registers 6
    .parameter "url"
    .parameter "text"

    #@0
    .prologue
    .line 196
    if-nez p0, :cond_4

    #@2
    .line 197
    const/4 v2, 0x0

    #@3
    .line 225
    :cond_3
    :goto_3
    return-object v2

    #@4
    .line 201
    :cond_4
    invoke-static {p0, p1}, Lcom/google/android/util/AbstractMessageParser$Video;->matchURL(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/util/AbstractMessageParser$Video;

    #@7
    move-result-object v2

    #@8
    .line 202
    .local v2, video:Lcom/google/android/util/AbstractMessageParser$Video;
    if-nez v2, :cond_3

    #@a
    .line 207
    invoke-static {p0, p1}, Lcom/google/android/util/AbstractMessageParser$YouTubeVideo;->matchURL(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/util/AbstractMessageParser$YouTubeVideo;

    #@d
    move-result-object v3

    #@e
    .line 208
    .local v3, ytVideo:Lcom/google/android/util/AbstractMessageParser$YouTubeVideo;
    if-eqz v3, :cond_12

    #@10
    move-object v2, v3

    #@11
    .line 209
    goto :goto_3

    #@12
    .line 213
    :cond_12
    invoke-static {p0, p1}, Lcom/google/android/util/AbstractMessageParser$Photo;->matchURL(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/util/AbstractMessageParser$Photo;

    #@15
    move-result-object v1

    #@16
    .line 214
    .local v1, photo:Lcom/google/android/util/AbstractMessageParser$Photo;
    if-eqz v1, :cond_1a

    #@18
    move-object v2, v1

    #@19
    .line 215
    goto :goto_3

    #@1a
    .line 219
    :cond_1a
    invoke-static {p0, p1}, Lcom/google/android/util/AbstractMessageParser$FlickrPhoto;->matchURL(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/util/AbstractMessageParser$FlickrPhoto;

    #@1d
    move-result-object v0

    #@1e
    .line 220
    .local v0, flickrPhoto:Lcom/google/android/util/AbstractMessageParser$FlickrPhoto;
    if-eqz v0, :cond_22

    #@20
    move-object v2, v0

    #@21
    .line 221
    goto :goto_3

    #@22
    .line 225
    :cond_22
    new-instance v2, Lcom/google/android/util/AbstractMessageParser$Link;

    #@24
    .end local v2           #video:Lcom/google/android/util/AbstractMessageParser$Video;
    invoke-direct {v2, p0, p1}, Lcom/google/android/util/AbstractMessageParser$Link;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@27
    goto :goto_3
.end method


# virtual methods
.method public final getPart(I)Lcom/google/android/util/AbstractMessageParser$Part;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/util/AbstractMessageParser;->parts:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/google/android/util/AbstractMessageParser$Part;

    #@8
    return-object v0
.end method

.method public final getPartCount()I
    .registers 2

    #@0
    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/util/AbstractMessageParser;->parts:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final getParts()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/util/AbstractMessageParser$Part;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/util/AbstractMessageParser;->parts:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method public final getRawText()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method protected abstract getResources()Lcom/google/android/util/AbstractMessageParser$Resources;
.end method

.method public parse()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x4

    #@1
    const/4 v5, 0x3

    #@2
    .line 131
    invoke-direct {p0}, Lcom/google/android/util/AbstractMessageParser;->parseMusicTrack()Z

    #@5
    move-result v3

    #@6
    if-eqz v3, :cond_d

    #@8
    .line 132
    const/4 v3, 0x0

    #@9
    invoke-direct {p0, v3}, Lcom/google/android/util/AbstractMessageParser;->buildParts(Ljava/lang/String;)V

    #@c
    .line 186
    :goto_c
    return-void

    #@d
    .line 137
    :cond_d
    const/4 v1, 0x0

    #@e
    .line 138
    .local v1, meText:Ljava/lang/String;
    iget-boolean v3, p0, Lcom/google/android/util/AbstractMessageParser;->parseMeText:Z

    #@10
    if-eqz v3, :cond_3f

    #@12
    iget-object v3, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@14
    const-string v4, "/me"

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@19
    move-result v3

    #@1a
    if-eqz v3, :cond_3f

    #@1c
    iget-object v3, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@1e
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@21
    move-result v3

    #@22
    if-le v3, v5, :cond_3f

    #@24
    iget-object v3, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@26
    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    #@29
    move-result v3

    #@2a
    invoke-static {v3}, Ljava/lang/Character;->isWhitespace(C)Z

    #@2d
    move-result v3

    #@2e
    if-eqz v3, :cond_3f

    #@30
    .line 140
    iget-object v3, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@32
    const/4 v4, 0x0

    #@33
    invoke-virtual {v3, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@36
    move-result-object v1

    #@37
    .line 141
    iget-object v3, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@39
    invoke-virtual {v3, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@3c
    move-result-object v3

    #@3d
    iput-object v3, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@3f
    .line 145
    :cond_3f
    const/4 v2, 0x0

    #@40
    .line 146
    .local v2, wasSmiley:Z
    :cond_40
    :goto_40
    iget v3, p0, Lcom/google/android/util/AbstractMessageParser;->nextChar:I

    #@42
    iget-object v4, p0, Lcom/google/android/util/AbstractMessageParser;->text:Ljava/lang/String;

    #@44
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@47
    move-result v4

    #@48
    if-ge v3, v4, :cond_84

    #@4a
    .line 147
    iget v3, p0, Lcom/google/android/util/AbstractMessageParser;->nextChar:I

    #@4c
    invoke-direct {p0, v3}, Lcom/google/android/util/AbstractMessageParser;->isWordBreak(I)Z

    #@4f
    move-result v3

    #@50
    if-nez v3, :cond_65

    #@52
    .line 148
    if-eqz v2, :cond_5c

    #@54
    iget v3, p0, Lcom/google/android/util/AbstractMessageParser;->nextChar:I

    #@56
    invoke-direct {p0, v3}, Lcom/google/android/util/AbstractMessageParser;->isSmileyBreak(I)Z

    #@59
    move-result v3

    #@5a
    if-nez v3, :cond_65

    #@5c
    .line 149
    :cond_5c
    new-instance v3, Ljava/lang/AssertionError;

    #@5e
    const-string/jumbo v4, "last chunk did not end at word break"

    #@61
    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    #@64
    throw v3

    #@65
    .line 153
    :cond_65
    invoke-direct {p0}, Lcom/google/android/util/AbstractMessageParser;->parseSmiley()Z

    #@68
    move-result v3

    #@69
    if-eqz v3, :cond_6d

    #@6b
    .line 154
    const/4 v2, 0x1

    #@6c
    goto :goto_40

    #@6d
    .line 156
    :cond_6d
    const/4 v2, 0x0

    #@6e
    .line 158
    invoke-direct {p0}, Lcom/google/android/util/AbstractMessageParser;->parseAcronym()Z

    #@71
    move-result v3

    #@72
    if-nez v3, :cond_40

    #@74
    invoke-direct {p0}, Lcom/google/android/util/AbstractMessageParser;->parseURL()Z

    #@77
    move-result v3

    #@78
    if-nez v3, :cond_40

    #@7a
    invoke-direct {p0}, Lcom/google/android/util/AbstractMessageParser;->parseFormatting()Z

    #@7d
    move-result v3

    #@7e
    if-nez v3, :cond_40

    #@80
    .line 159
    invoke-direct {p0}, Lcom/google/android/util/AbstractMessageParser;->parseText()V

    #@83
    goto :goto_40

    #@84
    .line 165
    :cond_84
    const/4 v0, 0x0

    #@85
    .local v0, i:I
    :goto_85
    iget-object v3, p0, Lcom/google/android/util/AbstractMessageParser;->tokens:Ljava/util/ArrayList;

    #@87
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@8a
    move-result v3

    #@8b
    if-ge v0, v3, :cond_dc

    #@8d
    .line 166
    iget-object v3, p0, Lcom/google/android/util/AbstractMessageParser;->tokens:Ljava/util/ArrayList;

    #@8f
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@92
    move-result-object v3

    #@93
    check-cast v3, Lcom/google/android/util/AbstractMessageParser$Token;

    #@95
    invoke-virtual {v3}, Lcom/google/android/util/AbstractMessageParser$Token;->isMedia()Z

    #@98
    move-result v3

    #@99
    if-eqz v3, :cond_d9

    #@9b
    .line 167
    if-lez v0, :cond_b6

    #@9d
    iget-object v3, p0, Lcom/google/android/util/AbstractMessageParser;->tokens:Ljava/util/ArrayList;

    #@9f
    add-int/lit8 v4, v0, -0x1

    #@a1
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@a4
    move-result-object v3

    #@a5
    instance-of v3, v3, Lcom/google/android/util/AbstractMessageParser$Html;

    #@a7
    if-eqz v3, :cond_b6

    #@a9
    .line 168
    iget-object v3, p0, Lcom/google/android/util/AbstractMessageParser;->tokens:Ljava/util/ArrayList;

    #@ab
    add-int/lit8 v4, v0, -0x1

    #@ad
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@b0
    move-result-object v3

    #@b1
    check-cast v3, Lcom/google/android/util/AbstractMessageParser$Html;

    #@b3
    invoke-virtual {v3}, Lcom/google/android/util/AbstractMessageParser$Html;->trimLeadingWhitespace()V

    #@b6
    .line 170
    :cond_b6
    add-int/lit8 v3, v0, 0x1

    #@b8
    iget-object v4, p0, Lcom/google/android/util/AbstractMessageParser;->tokens:Ljava/util/ArrayList;

    #@ba
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@bd
    move-result v4

    #@be
    if-ge v3, v4, :cond_d9

    #@c0
    iget-object v3, p0, Lcom/google/android/util/AbstractMessageParser;->tokens:Ljava/util/ArrayList;

    #@c2
    add-int/lit8 v4, v0, 0x1

    #@c4
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c7
    move-result-object v3

    #@c8
    instance-of v3, v3, Lcom/google/android/util/AbstractMessageParser$Html;

    #@ca
    if-eqz v3, :cond_d9

    #@cc
    .line 171
    iget-object v3, p0, Lcom/google/android/util/AbstractMessageParser;->tokens:Ljava/util/ArrayList;

    #@ce
    add-int/lit8 v4, v0, 0x1

    #@d0
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@d3
    move-result-object v3

    #@d4
    check-cast v3, Lcom/google/android/util/AbstractMessageParser$Html;

    #@d6
    invoke-virtual {v3}, Lcom/google/android/util/AbstractMessageParser$Html;->trimTrailingWhitespace()V

    #@d9
    .line 165
    :cond_d9
    add-int/lit8 v0, v0, 0x1

    #@db
    goto :goto_85

    #@dc
    .line 177
    :cond_dc
    const/4 v0, 0x0

    #@dd
    :goto_dd
    iget-object v3, p0, Lcom/google/android/util/AbstractMessageParser;->tokens:Ljava/util/ArrayList;

    #@df
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@e2
    move-result v3

    #@e3
    if-ge v0, v3, :cond_110

    #@e5
    .line 178
    iget-object v3, p0, Lcom/google/android/util/AbstractMessageParser;->tokens:Ljava/util/ArrayList;

    #@e7
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@ea
    move-result-object v3

    #@eb
    check-cast v3, Lcom/google/android/util/AbstractMessageParser$Token;

    #@ed
    invoke-virtual {v3}, Lcom/google/android/util/AbstractMessageParser$Token;->isHtml()Z

    #@f0
    move-result v3

    #@f1
    if-eqz v3, :cond_10d

    #@f3
    iget-object v3, p0, Lcom/google/android/util/AbstractMessageParser;->tokens:Ljava/util/ArrayList;

    #@f5
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f8
    move-result-object v3

    #@f9
    check-cast v3, Lcom/google/android/util/AbstractMessageParser$Token;

    #@fb
    const/4 v4, 0x1

    #@fc
    invoke-virtual {v3, v4}, Lcom/google/android/util/AbstractMessageParser$Token;->toHtml(Z)Ljava/lang/String;

    #@ff
    move-result-object v3

    #@100
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@103
    move-result v3

    #@104
    if-nez v3, :cond_10d

    #@106
    .line 180
    iget-object v3, p0, Lcom/google/android/util/AbstractMessageParser;->tokens:Ljava/util/ArrayList;

    #@108
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@10b
    .line 181
    add-int/lit8 v0, v0, -0x1

    #@10d
    .line 177
    :cond_10d
    add-int/lit8 v0, v0, 0x1

    #@10f
    goto :goto_dd

    #@110
    .line 185
    :cond_110
    invoke-direct {p0, v1}, Lcom/google/android/util/AbstractMessageParser;->buildParts(Ljava/lang/String;)V

    #@113
    goto/16 :goto_c
.end method

.method public toHtml()Ljava/lang/String;
    .registers 11

    #@0
    .prologue
    .line 1241
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 1243
    .local v1, html:Ljava/lang/StringBuilder;
    iget-object v7, p0, Lcom/google/android/util/AbstractMessageParser;->parts:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v2

    #@b
    :goto_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v7

    #@f
    if-eqz v7, :cond_15d

    #@11
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v5

    #@15
    check-cast v5, Lcom/google/android/util/AbstractMessageParser$Part;

    #@17
    .line 1244
    .local v5, part:Lcom/google/android/util/AbstractMessageParser$Part;
    const/4 v0, 0x0

    #@18
    .line 1246
    .local v0, caps:Z
    const-string v7, "<p>"

    #@1a
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    .line 1247
    invoke-virtual {v5}, Lcom/google/android/util/AbstractMessageParser$Part;->getTokens()Ljava/util/ArrayList;

    #@20
    move-result-object v7

    #@21
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@24
    move-result-object v3

    #@25
    .local v3, i$:Ljava/util/Iterator;
    :cond_25
    :goto_25
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@28
    move-result v7

    #@29
    if-eqz v7, :cond_156

    #@2b
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2e
    move-result-object v6

    #@2f
    check-cast v6, Lcom/google/android/util/AbstractMessageParser$Token;

    #@31
    .line 1248
    .local v6, token:Lcom/google/android/util/AbstractMessageParser$Token;
    invoke-virtual {v6}, Lcom/google/android/util/AbstractMessageParser$Token;->isHtml()Z

    #@34
    move-result v7

    #@35
    if-eqz v7, :cond_49

    #@37
    .line 1249
    invoke-virtual {v6, v0}, Lcom/google/android/util/AbstractMessageParser$Token;->toHtml(Z)Ljava/lang/String;

    #@3a
    move-result-object v7

    #@3b
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    .line 1319
    :goto_3e
    invoke-virtual {v6}, Lcom/google/android/util/AbstractMessageParser$Token;->controlCaps()Z

    #@41
    move-result v7

    #@42
    if-eqz v7, :cond_25

    #@44
    .line 1320
    invoke-virtual {v6}, Lcom/google/android/util/AbstractMessageParser$Token;->setCaps()Z

    #@47
    move-result v0

    #@48
    goto :goto_25

    #@49
    .line 1251
    :cond_49
    sget-object v7, Lcom/google/android/util/AbstractMessageParser$1;->$SwitchMap$com$google$android$util$AbstractMessageParser$Token$Type:[I

    #@4b
    invoke-virtual {v6}, Lcom/google/android/util/AbstractMessageParser$Token;->getType()Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@4e
    move-result-object v8

    #@4f
    invoke-virtual {v8}, Lcom/google/android/util/AbstractMessageParser$Token$Type;->ordinal()I

    #@52
    move-result v8

    #@53
    aget v7, v7, v8

    #@55
    packed-switch v7, :pswitch_data_162

    #@58
    .line 1315
    new-instance v7, Ljava/lang/AssertionError;

    #@5a
    new-instance v8, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string/jumbo v9, "unknown token type: "

    #@62
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v8

    #@66
    invoke-virtual {v6}, Lcom/google/android/util/AbstractMessageParser$Token;->getType()Lcom/google/android/util/AbstractMessageParser$Token$Type;

    #@69
    move-result-object v9

    #@6a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v8

    #@6e
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v8

    #@72
    invoke-direct {v7, v8}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    #@75
    throw v7

    #@76
    .line 1253
    :pswitch_76
    const-string v7, "<a href=\""

    #@78
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-object v7, v6

    #@7c
    .line 1254
    check-cast v7, Lcom/google/android/util/AbstractMessageParser$Link;

    #@7e
    invoke-virtual {v7}, Lcom/google/android/util/AbstractMessageParser$Link;->getURL()Ljava/lang/String;

    #@81
    move-result-object v7

    #@82
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    .line 1255
    const-string v7, "\">"

    #@87
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    .line 1256
    invoke-virtual {v6}, Lcom/google/android/util/AbstractMessageParser$Token;->getRawText()Ljava/lang/String;

    #@8d
    move-result-object v7

    #@8e
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    .line 1257
    const-string v7, "</a>"

    #@93
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    goto :goto_3e

    #@97
    .line 1262
    :pswitch_97
    invoke-virtual {v6}, Lcom/google/android/util/AbstractMessageParser$Token;->getRawText()Ljava/lang/String;

    #@9a
    move-result-object v7

    #@9b
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    goto :goto_3e

    #@9f
    .line 1266
    :pswitch_9f
    invoke-virtual {v6}, Lcom/google/android/util/AbstractMessageParser$Token;->getRawText()Ljava/lang/String;

    #@a2
    move-result-object v7

    #@a3
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    goto :goto_3e

    #@a7
    :pswitch_a7
    move-object v7, v6

    #@a8
    .line 1271
    check-cast v7, Lcom/google/android/util/AbstractMessageParser$MusicTrack;

    #@aa
    invoke-virtual {v7}, Lcom/google/android/util/AbstractMessageParser$MusicTrack;->getTrack()Ljava/lang/String;

    #@ad
    move-result-object v7

    #@ae
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    goto :goto_3e

    #@b2
    .line 1276
    :pswitch_b2
    const-string v7, "<a href=\""

    #@b4
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-object v7, v6

    #@b8
    .line 1277
    check-cast v7, Lcom/google/android/util/AbstractMessageParser$Video;

    #@ba
    move-object v7, v6

    #@bb
    check-cast v7, Lcom/google/android/util/AbstractMessageParser$Video;

    #@bd
    invoke-virtual {v7}, Lcom/google/android/util/AbstractMessageParser$Video;->getDocID()Ljava/lang/String;

    #@c0
    move-result-object v7

    #@c1
    invoke-static {v7}, Lcom/google/android/util/AbstractMessageParser$Video;->getURL(Ljava/lang/String;)Ljava/lang/String;

    #@c4
    move-result-object v7

    #@c5
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    .line 1278
    const-string v7, "\">"

    #@ca
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    .line 1279
    invoke-virtual {v6}, Lcom/google/android/util/AbstractMessageParser$Token;->getRawText()Ljava/lang/String;

    #@d0
    move-result-object v7

    #@d1
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    .line 1280
    const-string v7, "</a>"

    #@d6
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    goto/16 :goto_3e

    #@db
    .line 1285
    :pswitch_db
    const-string v7, "<a href=\""

    #@dd
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e0
    move-object v7, v6

    #@e1
    .line 1286
    check-cast v7, Lcom/google/android/util/AbstractMessageParser$YouTubeVideo;

    #@e3
    move-object v7, v6

    #@e4
    check-cast v7, Lcom/google/android/util/AbstractMessageParser$YouTubeVideo;

    #@e6
    invoke-virtual {v7}, Lcom/google/android/util/AbstractMessageParser$YouTubeVideo;->getDocID()Ljava/lang/String;

    #@e9
    move-result-object v7

    #@ea
    invoke-static {v7}, Lcom/google/android/util/AbstractMessageParser$YouTubeVideo;->getURL(Ljava/lang/String;)Ljava/lang/String;

    #@ed
    move-result-object v7

    #@ee
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f1
    .line 1288
    const-string v7, "\">"

    #@f3
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f6
    .line 1289
    invoke-virtual {v6}, Lcom/google/android/util/AbstractMessageParser$Token;->getRawText()Ljava/lang/String;

    #@f9
    move-result-object v7

    #@fa
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fd
    .line 1290
    const-string v7, "</a>"

    #@ff
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@102
    goto/16 :goto_3e

    #@104
    .line 1295
    :pswitch_104
    const-string v7, "<a href=\""

    #@106
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@109
    move-object v7, v6

    #@10a
    .line 1296
    check-cast v7, Lcom/google/android/util/AbstractMessageParser$Photo;

    #@10c
    invoke-virtual {v7}, Lcom/google/android/util/AbstractMessageParser$Photo;->getUser()Ljava/lang/String;

    #@10f
    move-result-object v8

    #@110
    move-object v7, v6

    #@111
    check-cast v7, Lcom/google/android/util/AbstractMessageParser$Photo;

    #@113
    invoke-virtual {v7}, Lcom/google/android/util/AbstractMessageParser$Photo;->getAlbum()Ljava/lang/String;

    #@116
    move-result-object v7

    #@117
    invoke-static {v8, v7}, Lcom/google/android/util/AbstractMessageParser$Photo;->getAlbumURL(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@11a
    move-result-object v7

    #@11b
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11e
    .line 1298
    const-string v7, "\">"

    #@120
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@123
    .line 1299
    invoke-virtual {v6}, Lcom/google/android/util/AbstractMessageParser$Token;->getRawText()Ljava/lang/String;

    #@126
    move-result-object v7

    #@127
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12a
    .line 1300
    const-string v7, "</a>"

    #@12c
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12f
    goto/16 :goto_3e

    #@131
    :pswitch_131
    move-object v4, v6

    #@132
    .line 1306
    check-cast v4, Lcom/google/android/util/AbstractMessageParser$Photo;

    #@134
    .line 1307
    .local v4, p:Lcom/google/android/util/AbstractMessageParser$Photo;
    const-string v7, "<a href=\""

    #@136
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@139
    move-object v7, v6

    #@13a
    .line 1308
    check-cast v7, Lcom/google/android/util/AbstractMessageParser$FlickrPhoto;

    #@13c
    invoke-virtual {v7}, Lcom/google/android/util/AbstractMessageParser$FlickrPhoto;->getUrl()Ljava/lang/String;

    #@13f
    move-result-object v7

    #@140
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@143
    .line 1309
    const-string v7, "\">"

    #@145
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@148
    .line 1310
    invoke-virtual {v6}, Lcom/google/android/util/AbstractMessageParser$Token;->getRawText()Ljava/lang/String;

    #@14b
    move-result-object v7

    #@14c
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14f
    .line 1311
    const-string v7, "</a>"

    #@151
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@154
    goto/16 :goto_3e

    #@156
    .line 1323
    .end local v4           #p:Lcom/google/android/util/AbstractMessageParser$Photo;
    .end local v6           #token:Lcom/google/android/util/AbstractMessageParser$Token;
    :cond_156
    const-string v7, "</p>\n"

    #@158
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15b
    goto/16 :goto_b

    #@15d
    .line 1326
    .end local v0           #caps:Z
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v5           #part:Lcom/google/android/util/AbstractMessageParser$Part;
    :cond_15d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@160
    move-result-object v7

    #@161
    return-object v7

    #@162
    .line 1251
    :pswitch_data_162
    .packed-switch 0x1
        :pswitch_76
        :pswitch_97
        :pswitch_9f
        :pswitch_a7
        :pswitch_b2
        :pswitch_db
        :pswitch_104
        :pswitch_131
    .end packed-switch
.end method
