.class public final Lcom/android/server/sip/SipService;
.super Landroid/net/sip/ISipService$Stub;
.source "SipService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/sip/SipService$1;,
        Lcom/android/server/sip/SipService$MyExecutor;,
        Lcom/android/server/sip/SipService$ConnectivityReceiver;,
        Lcom/android/server/sip/SipService$AutoRegistrationProcess;,
        Lcom/android/server/sip/SipService$IntervalMeasurementProcess;,
        Lcom/android/server/sip/SipService$SipSessionGroupExt;
    }
.end annotation


# static fields
.field static final DEBUG:Z = false

.field private static final DEFAULT_KEEPALIVE_INTERVAL:I = 0xa

.field private static final DEFAULT_MAX_KEEPALIVE_INTERVAL:I = 0x78

.field private static final EXPIRY_TIME:I = 0xe10

.field private static final MIN_EXPIRY_TIME:I = 0x3c

.field private static final SHORT_EXPIRY_TIME:I = 0xa

.field static final TAG:Ljava/lang/String; = "SipService"


# instance fields
.field private mConnectivityReceiver:Lcom/android/server/sip/SipService$ConnectivityReceiver;

.field private mContext:Landroid/content/Context;

.field private mExecutor:Lcom/android/server/sip/SipService$MyExecutor;

.field private mIntervalMeasurementProcess:Lcom/android/server/sip/SipService$IntervalMeasurementProcess;

.field private mKeepAliveInterval:I

.field private mLastGoodKeepAliveInterval:I

.field private mLocalIp:Ljava/lang/String;

.field private mMyWakeLock:Lcom/android/server/sip/SipWakeLock;

.field private mNetworkType:I

.field private mPendingSessions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/net/sip/ISipSession;",
            ">;"
        }
    .end annotation
.end field

.field private mSipGroups:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/sip/SipService$SipSessionGroupExt;",
            ">;"
        }
    .end annotation
.end field

.field private mSipOnWifiOnly:Z

.field private mTimer:Lcom/android/server/sip/SipWakeupTimer;

.field private mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 114
    invoke-direct {p0}, Landroid/net/sip/ISipService$Stub;-><init>()V

    #@3
    .line 80
    const/4 v0, -0x1

    #@4
    iput v0, p0, Lcom/android/server/sip/SipService;->mNetworkType:I

    #@6
    .line 87
    new-instance v0, Lcom/android/server/sip/SipService$MyExecutor;

    #@8
    invoke-direct {v0, p0}, Lcom/android/server/sip/SipService$MyExecutor;-><init>(Lcom/android/server/sip/SipService;)V

    #@b
    iput-object v0, p0, Lcom/android/server/sip/SipService;->mExecutor:Lcom/android/server/sip/SipService$MyExecutor;

    #@d
    .line 90
    new-instance v0, Ljava/util/HashMap;

    #@f
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@12
    iput-object v0, p0, Lcom/android/server/sip/SipService;->mSipGroups:Ljava/util/Map;

    #@14
    .line 94
    new-instance v0, Ljava/util/HashMap;

    #@16
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@19
    iput-object v0, p0, Lcom/android/server/sip/SipService;->mPendingSessions:Ljava/util/Map;

    #@1b
    .line 100
    const/16 v0, 0xa

    #@1d
    iput v0, p0, Lcom/android/server/sip/SipService;->mLastGoodKeepAliveInterval:I

    #@1f
    .line 116
    iput-object p1, p0, Lcom/android/server/sip/SipService;->mContext:Landroid/content/Context;

    #@21
    .line 117
    new-instance v0, Lcom/android/server/sip/SipService$ConnectivityReceiver;

    #@23
    const/4 v1, 0x0

    #@24
    invoke-direct {v0, p0, v1}, Lcom/android/server/sip/SipService$ConnectivityReceiver;-><init>(Lcom/android/server/sip/SipService;Lcom/android/server/sip/SipService$1;)V

    #@27
    iput-object v0, p0, Lcom/android/server/sip/SipService;->mConnectivityReceiver:Lcom/android/server/sip/SipService$ConnectivityReceiver;

    #@29
    .line 119
    const-string/jumbo v0, "wifi"

    #@2c
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2f
    move-result-object v0

    #@30
    check-cast v0, Landroid/net/wifi/WifiManager;

    #@32
    const/4 v1, 0x1

    #@33
    const-string v2, "SipService"

    #@35
    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    #@38
    move-result-object v0

    #@39
    iput-object v0, p0, Lcom/android/server/sip/SipService;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    #@3b
    .line 122
    iget-object v0, p0, Lcom/android/server/sip/SipService;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    #@3d
    const/4 v1, 0x0

    #@3e
    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager$WifiLock;->setReferenceCounted(Z)V

    #@41
    .line 123
    invoke-static {p1}, Landroid/net/sip/SipManager;->isSipWifiOnly(Landroid/content/Context;)Z

    #@44
    move-result v0

    #@45
    iput-boolean v0, p0, Lcom/android/server/sip/SipService;->mSipOnWifiOnly:Z

    #@47
    .line 125
    new-instance v1, Lcom/android/server/sip/SipWakeLock;

    #@49
    const-string/jumbo v0, "power"

    #@4c
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@4f
    move-result-object v0

    #@50
    check-cast v0, Landroid/os/PowerManager;

    #@52
    invoke-direct {v1, v0}, Lcom/android/server/sip/SipWakeLock;-><init>(Landroid/os/PowerManager;)V

    #@55
    iput-object v1, p0, Lcom/android/server/sip/SipService;->mMyWakeLock:Lcom/android/server/sip/SipWakeLock;

    #@57
    .line 128
    new-instance v0, Lcom/android/server/sip/SipWakeupTimer;

    #@59
    iget-object v1, p0, Lcom/android/server/sip/SipService;->mExecutor:Lcom/android/server/sip/SipService$MyExecutor;

    #@5b
    invoke-direct {v0, p1, v1}, Lcom/android/server/sip/SipWakeupTimer;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;)V

    #@5e
    iput-object v0, p0, Lcom/android/server/sip/SipService;->mTimer:Lcom/android/server/sip/SipWakeupTimer;

    #@60
    .line 129
    return-void
.end method

.method static synthetic access$1002(Lcom/android/server/sip/SipService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 69
    iput p1, p0, Lcom/android/server/sip/SipService;->mLastGoodKeepAliveInterval:I

    #@2
    return p1
.end method

.method static synthetic access$1100(Lcom/android/server/sip/SipService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 69
    invoke-direct {p0}, Lcom/android/server/sip/SipService;->onKeepAliveIntervalChanged()V

    #@3
    return-void
.end method

.method static synthetic access$1200(Lcom/android/server/sip/SipService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    invoke-direct {p0}, Lcom/android/server/sip/SipService;->getKeepAliveInterval()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1300(Lcom/android/server/sip/SipService;Landroid/net/sip/SipProfile;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Lcom/android/server/sip/SipService;->restartPortMappingLifetimeMeasurement(Landroid/net/sip/SipProfile;I)V

    #@3
    return-void
.end method

.method static synthetic access$1400(Lcom/android/server/sip/SipService;Landroid/net/sip/SipProfile;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipService;->startPortMappingLifetimeMeasurement(Landroid/net/sip/SipProfile;)V

    #@3
    return-void
.end method

.method static synthetic access$1500(Lcom/android/server/sip/SipService;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/server/sip/SipService;->mLocalIp:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/server/sip/SipService;Ljava/lang/String;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipService;->isBehindNAT(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1700(Lcom/android/server/sip/SipService;Landroid/net/NetworkInfo;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipService;->onConnectivityChanged(Landroid/net/NetworkInfo;)V

    #@3
    return-void
.end method

.method static synthetic access$1800()Landroid/os/Looper;
    .registers 1

    #@0
    .prologue
    .line 69
    invoke-static {}, Lcom/android/server/sip/SipService;->createLooper()Landroid/os/Looper;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/server/sip/SipService;)Lcom/android/server/sip/SipWakeupTimer;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/server/sip/SipService;->mTimer:Lcom/android/server/sip/SipWakeupTimer;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/server/sip/SipService;)Lcom/android/server/sip/SipWakeLock;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/server/sip/SipService;->mMyWakeLock:Lcom/android/server/sip/SipWakeLock;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/server/sip/SipService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget v0, p0, Lcom/android/server/sip/SipService;->mNetworkType:I

    #@2
    return v0
.end method

.method static synthetic access$500(Lcom/android/server/sip/SipService;Lcom/android/server/sip/SipService$SipSessionGroupExt;Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Lcom/android/server/sip/SipService;->callingSelf(Lcom/android/server/sip/SipService$SipSessionGroupExt;Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$600(Lcom/android/server/sip/SipService;Landroid/net/sip/ISipSession;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipService;->addPendingSession(Landroid/net/sip/ISipSession;)V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/android/server/sip/SipService;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/server/sip/SipService;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Lcom/android/server/sip/SipService;)Lcom/android/server/sip/SipService$MyExecutor;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/server/sip/SipService;->mExecutor:Lcom/android/server/sip/SipService$MyExecutor;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Lcom/android/server/sip/SipService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget v0, p0, Lcom/android/server/sip/SipService;->mKeepAliveInterval:I

    #@2
    return v0
.end method

.method static synthetic access$902(Lcom/android/server/sip/SipService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 69
    iput p1, p0, Lcom/android/server/sip/SipService;->mKeepAliveInterval:I

    #@2
    return p1
.end method

.method private declared-synchronized addPendingSession(Landroid/net/sip/ISipSession;)V
    .registers 5
    .parameter "session"

    #@0
    .prologue
    .line 383
    monitor-enter p0

    #@1
    :try_start_1
    invoke-direct {p0}, Lcom/android/server/sip/SipService;->cleanUpPendingSessions()V

    #@4
    .line 384
    iget-object v1, p0, Lcom/android/server/sip/SipService;->mPendingSessions:Ljava/util/Map;

    #@6
    invoke-interface {p1}, Landroid/net/sip/ISipSession;->getCallId()Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_18
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_d} :catch_f

    #@d
    .line 390
    :goto_d
    monitor-exit p0

    #@e
    return-void

    #@f
    .line 386
    :catch_f
    move-exception v0

    #@10
    .line 388
    .local v0, e:Landroid/os/RemoteException;
    :try_start_10
    const-string v1, "SipService"

    #@12
    const-string v2, "addPendingSession()"

    #@14
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_17
    .catchall {:try_start_10 .. :try_end_17} :catchall_18

    #@17
    goto :goto_d

    #@18
    .line 383
    .end local v0           #e:Landroid/os/RemoteException;
    :catchall_18
    move-exception v1

    #@19
    monitor-exit p0

    #@1a
    throw v1
.end method

.method private declared-synchronized callingSelf(Lcom/android/server/sip/SipService$SipSessionGroupExt;Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;)Z
    .registers 7
    .parameter "ringingGroup"
    .parameter "ringingSession"

    #@0
    .prologue
    .line 405
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p2}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->getCallId()Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 406
    .local v0, callId:Ljava/lang/String;
    iget-object v3, p0, Lcom/android/server/sip/SipService;->mSipGroups:Ljava/util/Map;

    #@7
    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    #@a
    move-result-object v3

    #@b
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@e
    move-result-object v2

    #@f
    .local v2, i$:Ljava/util/Iterator;
    :cond_f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@12
    move-result v3

    #@13
    if-eqz v3, :cond_26

    #@15
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@18
    move-result-object v1

    #@19
    check-cast v1, Lcom/android/server/sip/SipService$SipSessionGroupExt;

    #@1b
    .line 407
    .local v1, group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    if-eq v1, p1, :cond_f

    #@1d
    invoke-virtual {v1, v0}, Lcom/android/server/sip/SipService$SipSessionGroupExt;->containsSession(Ljava/lang/String;)Z
    :try_end_20
    .catchall {:try_start_1 .. :try_end_20} :catchall_28

    #@20
    move-result v3

    #@21
    if-eqz v3, :cond_f

    #@23
    .line 411
    const/4 v3, 0x1

    #@24
    .line 414
    .end local v1           #group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    :goto_24
    monitor-exit p0

    #@25
    return v3

    #@26
    :cond_26
    const/4 v3, 0x0

    #@27
    goto :goto_24

    #@28
    .line 405
    .end local v0           #callId:Ljava/lang/String;
    .end local v2           #i$:Ljava/util/Iterator;
    :catchall_28
    move-exception v3

    #@29
    monitor-exit p0

    #@2a
    throw v3
.end method

.method private cleanUpPendingSessions()V
    .registers 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 393
    iget-object v5, p0, Lcom/android/server/sip/SipService;->mPendingSessions:Ljava/util/Map;

    #@2
    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@5
    move-result-object v5

    #@6
    iget-object v6, p0, Lcom/android/server/sip/SipService;->mPendingSessions:Ljava/util/Map;

    #@8
    invoke-interface {v6}, Ljava/util/Map;->size()I

    #@b
    move-result v6

    #@c
    new-array v6, v6, [Ljava/util/Map$Entry;

    #@e
    invoke-interface {v5, v6}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    check-cast v1, [Ljava/util/Map$Entry;

    #@14
    .line 396
    .local v1, entries:[Ljava/util/Map$Entry;,"[Ljava/util/Map$Entry<Ljava/lang/String;Landroid/net/sip/ISipSession;>;"
    move-object v0, v1

    #@15
    .local v0, arr$:[Ljava/util/Map$Entry;
    array-length v4, v0

    #@16
    .local v4, len$:I
    const/4 v3, 0x0

    #@17
    .local v3, i$:I
    :goto_17
    if-ge v3, v4, :cond_34

    #@19
    aget-object v2, v0, v3

    #@1b
    .line 397
    .local v2, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Landroid/net/sip/ISipSession;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@1e
    move-result-object v5

    #@1f
    check-cast v5, Landroid/net/sip/ISipSession;

    #@21
    invoke-interface {v5}, Landroid/net/sip/ISipSession;->getState()I

    #@24
    move-result v5

    #@25
    const/4 v6, 0x3

    #@26
    if-eq v5, v6, :cond_31

    #@28
    .line 398
    iget-object v5, p0, Lcom/android/server/sip/SipService;->mPendingSessions:Ljava/util/Map;

    #@2a
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@2d
    move-result-object v6

    #@2e
    invoke-interface {v5, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@31
    .line 396
    :cond_31
    add-int/lit8 v3, v3, 0x1

    #@33
    goto :goto_17

    #@34
    .line 401
    .end local v2           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Landroid/net/sip/ISipSession;>;"
    :cond_34
    return-void
.end method

.method private createGroup(Landroid/net/sip/SipProfile;)Lcom/android/server/sip/SipService$SipSessionGroupExt;
    .registers 6
    .parameter "localProfile"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 287
    invoke-virtual {p1}, Landroid/net/sip/SipProfile;->getUriString()Ljava/lang/String;

    #@4
    move-result-object v1

    #@5
    .line 288
    .local v1, key:Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/sip/SipService;->mSipGroups:Ljava/util/Map;

    #@7
    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    move-result-object v0

    #@b
    check-cast v0, Lcom/android/server/sip/SipService$SipSessionGroupExt;

    #@d
    .line 289
    .local v0, group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    if-nez v0, :cond_1d

    #@f
    .line 290
    new-instance v0, Lcom/android/server/sip/SipService$SipSessionGroupExt;

    #@11
    .end local v0           #group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    invoke-direct {v0, p0, p1, v3, v3}, Lcom/android/server/sip/SipService$SipSessionGroupExt;-><init>(Lcom/android/server/sip/SipService;Landroid/net/sip/SipProfile;Landroid/app/PendingIntent;Landroid/net/sip/ISipSessionListener;)V

    #@14
    .line 291
    .restart local v0       #group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    iget-object v2, p0, Lcom/android/server/sip/SipService;->mSipGroups:Ljava/util/Map;

    #@16
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@19
    .line 292
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipService;->notifyProfileAdded(Landroid/net/sip/SipProfile;)V

    #@1c
    .line 296
    :cond_1c
    return-object v0

    #@1d
    .line 293
    :cond_1d
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipService;->isCallerCreator(Lcom/android/server/sip/SipService$SipSessionGroupExt;)Z

    #@20
    move-result v2

    #@21
    if-nez v2, :cond_1c

    #@23
    .line 294
    new-instance v2, Ljavax/sip/SipException;

    #@25
    const-string/jumbo v3, "only creator can access the profile"

    #@28
    invoke-direct {v2, v3}, Ljavax/sip/SipException;-><init>(Ljava/lang/String;)V

    #@2b
    throw v2
.end method

.method private createGroup(Landroid/net/sip/SipProfile;Landroid/app/PendingIntent;Landroid/net/sip/ISipSessionListener;)Lcom/android/server/sip/SipService$SipSessionGroupExt;
    .registers 8
    .parameter "localProfile"
    .parameter "incomingCallPendingIntent"
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 302
    invoke-virtual {p1}, Landroid/net/sip/SipProfile;->getUriString()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 303
    .local v1, key:Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/sip/SipService;->mSipGroups:Ljava/util/Map;

    #@6
    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Lcom/android/server/sip/SipService$SipSessionGroupExt;

    #@c
    .line 304
    .local v0, group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    if-eqz v0, :cond_24

    #@e
    .line 305
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipService;->isCallerCreator(Lcom/android/server/sip/SipService$SipSessionGroupExt;)Z

    #@11
    move-result v2

    #@12
    if-nez v2, :cond_1d

    #@14
    .line 306
    new-instance v2, Ljavax/sip/SipException;

    #@16
    const-string/jumbo v3, "only creator can access the profile"

    #@19
    invoke-direct {v2, v3}, Ljavax/sip/SipException;-><init>(Ljava/lang/String;)V

    #@1c
    throw v2

    #@1d
    .line 308
    :cond_1d
    invoke-virtual {v0, p2}, Lcom/android/server/sip/SipService$SipSessionGroupExt;->setIncomingCallPendingIntent(Landroid/app/PendingIntent;)V

    #@20
    .line 309
    invoke-virtual {v0, p3}, Lcom/android/server/sip/SipService$SipSessionGroupExt;->setListener(Landroid/net/sip/ISipSessionListener;)V

    #@23
    .line 316
    :goto_23
    return-object v0

    #@24
    .line 311
    :cond_24
    new-instance v0, Lcom/android/server/sip/SipService$SipSessionGroupExt;

    #@26
    .end local v0           #group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/server/sip/SipService$SipSessionGroupExt;-><init>(Lcom/android/server/sip/SipService;Landroid/net/sip/SipProfile;Landroid/app/PendingIntent;Landroid/net/sip/ISipSessionListener;)V

    #@29
    .line 313
    .restart local v0       #group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    iget-object v2, p0, Lcom/android/server/sip/SipService;->mSipGroups:Ljava/util/Map;

    #@2b
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2e
    .line 314
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipService;->notifyProfileAdded(Landroid/net/sip/SipProfile;)V

    #@31
    goto :goto_23
.end method

.method private static createLooper()Landroid/os/Looper;
    .registers 2

    #@0
    .prologue
    .line 1166
    new-instance v0, Landroid/os/HandlerThread;

    #@2
    const-string v1, "SipService.Executor"

    #@4
    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@7
    .line 1167
    .local v0, thread:Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    #@a
    .line 1168
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@d
    move-result-object v1

    #@e
    return-object v1
.end method

.method private determineLocalIp()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 275
    :try_start_0
    new-instance v1, Ljava/net/DatagramSocket;

    #@2
    invoke-direct {v1}, Ljava/net/DatagramSocket;-><init>()V

    #@5
    .line 276
    .local v1, s:Ljava/net/DatagramSocket;
    const-string v2, "192.168.1.1"

    #@7
    invoke-static {v2}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    #@a
    move-result-object v2

    #@b
    const/16 v3, 0x50

    #@d
    invoke-virtual {v1, v2, v3}, Ljava/net/DatagramSocket;->connect(Ljava/net/InetAddress;I)V

    #@10
    .line 277
    invoke-virtual {v1}, Ljava/net/DatagramSocket;->getLocalAddress()Ljava/net/InetAddress;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_17} :catch_19

    #@17
    move-result-object v2

    #@18
    .line 281
    .end local v1           #s:Ljava/net/DatagramSocket;
    :goto_18
    return-object v2

    #@19
    .line 278
    :catch_19
    move-exception v0

    #@1a
    .line 281
    .local v0, e:Ljava/io/IOException;
    const/4 v2, 0x0

    #@1b
    goto :goto_18
.end method

.method private getKeepAliveInterval()I
    .registers 2

    #@0
    .prologue
    .line 424
    iget v0, p0, Lcom/android/server/sip/SipService;->mKeepAliveInterval:I

    #@2
    if-gez v0, :cond_7

    #@4
    iget v0, p0, Lcom/android/server/sip/SipService;->mLastGoodKeepAliveInterval:I

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    iget v0, p0, Lcom/android/server/sip/SipService;->mKeepAliveInterval:I

    #@9
    goto :goto_6
.end method

.method private isBehindNAT(Ljava/lang/String;)Z
    .registers 8
    .parameter "address"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 431
    :try_start_2
    invoke-static {p1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    #@5
    move-result-object v4

    #@6
    invoke-virtual {v4}, Ljava/net/InetAddress;->getAddress()[B

    #@9
    move-result-object v0

    #@a
    .line 432
    .local v0, d:[B
    const/4 v4, 0x0

    #@b
    aget-byte v4, v0, v4

    #@d
    const/16 v5, 0xa

    #@f
    if-eq v4, v5, :cond_35

    #@11
    const/4 v4, 0x0

    #@12
    aget-byte v4, v0, v4

    #@14
    and-int/lit16 v4, v4, 0xff

    #@16
    const/16 v5, 0xac

    #@18
    if-ne v4, v5, :cond_23

    #@1a
    const/4 v4, 0x1

    #@1b
    aget-byte v4, v0, v4

    #@1d
    and-int/lit16 v4, v4, 0xf0

    #@1f
    const/16 v5, 0x10

    #@21
    if-eq v4, v5, :cond_35

    #@23
    :cond_23
    const/4 v4, 0x0

    #@24
    aget-byte v4, v0, v4

    #@26
    and-int/lit16 v4, v4, 0xff

    #@28
    const/16 v5, 0xc0

    #@2a
    if-ne v4, v5, :cond_4f

    #@2c
    const/4 v4, 0x1

    #@2d
    aget-byte v4, v0, v4
    :try_end_2f
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2f} :catch_36

    #@2f
    and-int/lit16 v4, v4, 0xff

    #@31
    const/16 v5, 0xa8

    #@33
    if-ne v4, v5, :cond_4f

    #@35
    .line 442
    .end local v0           #d:[B
    :cond_35
    :goto_35
    return v2

    #@36
    .line 439
    :catch_36
    move-exception v1

    #@37
    .line 440
    .local v1, e:Ljava/net/UnknownHostException;
    const-string v2, "SipService"

    #@39
    new-instance v4, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v5, "isBehindAT()"

    #@40
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v4

    #@44
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v4

    #@48
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v4

    #@4c
    invoke-static {v2, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4f
    .end local v1           #e:Ljava/net/UnknownHostException;
    :cond_4f
    move v2, v3

    #@50
    .line 442
    goto :goto_35
.end method

.method private isCallerCreator(Lcom/android/server/sip/SipService$SipSessionGroupExt;)Z
    .registers 5
    .parameter "group"

    #@0
    .prologue
    .line 183
    invoke-virtual {p1}, Lcom/android/server/sip/SipService$SipSessionGroupExt;->getLocalProfile()Landroid/net/sip/SipProfile;

    #@3
    move-result-object v0

    #@4
    .line 184
    .local v0, profile:Landroid/net/sip/SipProfile;
    invoke-virtual {v0}, Landroid/net/sip/SipProfile;->getCallingUid()I

    #@7
    move-result v1

    #@8
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@b
    move-result v2

    #@c
    if-ne v1, v2, :cond_10

    #@e
    const/4 v1, 0x1

    #@f
    :goto_f
    return v1

    #@10
    :cond_10
    const/4 v1, 0x0

    #@11
    goto :goto_f
.end method

.method private isCallerCreatorOrRadio(Lcom/android/server/sip/SipService$SipSessionGroupExt;)Z
    .registers 3
    .parameter "group"

    #@0
    .prologue
    .line 188
    invoke-direct {p0}, Lcom/android/server/sip/SipService;->isCallerRadio()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_c

    #@6
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipService;->isCallerCreator(Lcom/android/server/sip/SipService$SipSessionGroupExt;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method private isCallerRadio()Z
    .registers 3

    #@0
    .prologue
    .line 192
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v0

    #@4
    const/16 v1, 0x3e9

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method private notifyProfileAdded(Landroid/net/sip/SipProfile;)V
    .registers 5
    .parameter "localProfile"

    #@0
    .prologue
    .line 321
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "com.android.phone.SIP_ADD_PHONE"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 322
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "android:localSipUri"

    #@9
    invoke-virtual {p1}, Landroid/net/sip/SipProfile;->getUriString()Ljava/lang/String;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@10
    .line 323
    iget-object v1, p0, Lcom/android/server/sip/SipService;->mContext:Landroid/content/Context;

    #@12
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@15
    .line 324
    iget-object v1, p0, Lcom/android/server/sip/SipService;->mSipGroups:Ljava/util/Map;

    #@17
    invoke-interface {v1}, Ljava/util/Map;->size()I

    #@1a
    move-result v1

    #@1b
    const/4 v2, 0x1

    #@1c
    if-ne v1, v2, :cond_21

    #@1e
    .line 325
    invoke-direct {p0}, Lcom/android/server/sip/SipService;->registerReceivers()V

    #@21
    .line 327
    :cond_21
    return-void
.end method

.method private notifyProfileRemoved(Landroid/net/sip/SipProfile;)V
    .registers 5
    .parameter "localProfile"

    #@0
    .prologue
    .line 331
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "com.android.phone.SIP_REMOVE_PHONE"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 332
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "android:localSipUri"

    #@9
    invoke-virtual {p1}, Landroid/net/sip/SipProfile;->getUriString()Ljava/lang/String;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@10
    .line 333
    iget-object v1, p0, Lcom/android/server/sip/SipService;->mContext:Landroid/content/Context;

    #@12
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@15
    .line 334
    iget-object v1, p0, Lcom/android/server/sip/SipService;->mSipGroups:Ljava/util/Map;

    #@17
    invoke-interface {v1}, Ljava/util/Map;->size()I

    #@1a
    move-result v1

    #@1b
    if-nez v1, :cond_20

    #@1d
    .line 335
    invoke-direct {p0}, Lcom/android/server/sip/SipService;->unregisterReceivers()V

    #@20
    .line 337
    :cond_20
    return-void
.end method

.method private declared-synchronized onConnectivityChanged(Landroid/net/NetworkInfo;)V
    .registers 11
    .parameter "info"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v5, -0x1

    #@2
    .line 1119
    monitor-enter p0

    #@3
    if-eqz p1, :cond_13

    #@5
    :try_start_5
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->isConnected()Z

    #@8
    move-result v6

    #@9
    if-nez v6, :cond_13

    #@b
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getType()I

    #@e
    move-result v6

    #@f
    iget v7, p0, Lcom/android/server/sip/SipService;->mNetworkType:I

    #@11
    if-eq v6, v7, :cond_21

    #@13
    .line 1120
    :cond_13
    iget-object v6, p0, Lcom/android/server/sip/SipService;->mContext:Landroid/content/Context;

    #@15
    const-string v7, "connectivity"

    #@17
    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1a
    move-result-object v0

    #@1b
    check-cast v0, Landroid/net/ConnectivityManager;

    #@1d
    .line 1122
    .local v0, cm:Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    #@20
    move-result-object p1

    #@21
    .line 1127
    .end local v0           #cm:Landroid/net/ConnectivityManager;
    :cond_21
    if-eqz p1, :cond_3a

    #@23
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->isConnected()Z

    #@26
    move-result v6

    #@27
    if-eqz v6, :cond_3a

    #@29
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getType()I

    #@2c
    move-result v4

    #@2d
    .line 1128
    .local v4, networkType:I
    :goto_2d
    iget-boolean v6, p0, Lcom/android/server/sip/SipService;->mSipOnWifiOnly:Z

    #@2f
    if-eqz v6, :cond_34

    #@31
    if-eq v4, v8, :cond_34

    #@33
    .line 1129
    const/4 v4, -0x1

    #@34
    .line 1133
    :cond_34
    iget v6, p0, Lcom/android/server/sip/SipService;->mNetworkType:I
    :try_end_36
    .catchall {:try_start_5 .. :try_end_36} :catchall_6b

    #@36
    if-ne v6, v4, :cond_3c

    #@38
    .line 1163
    :goto_38
    monitor-exit p0

    #@39
    return-void

    #@3a
    .end local v4           #networkType:I
    :cond_3a
    move v4, v5

    #@3b
    .line 1127
    goto :goto_2d

    #@3c
    .line 1142
    .restart local v4       #networkType:I
    :cond_3c
    :try_start_3c
    iget v6, p0, Lcom/android/server/sip/SipService;->mNetworkType:I

    #@3e
    if-eq v6, v5, :cond_6e

    #@40
    .line 1143
    const/4 v6, 0x0

    #@41
    iput-object v6, p0, Lcom/android/server/sip/SipService;->mLocalIp:Ljava/lang/String;

    #@43
    .line 1144
    invoke-direct {p0}, Lcom/android/server/sip/SipService;->stopPortMappingMeasurement()V

    #@46
    .line 1145
    iget-object v6, p0, Lcom/android/server/sip/SipService;->mSipGroups:Ljava/util/Map;

    #@48
    invoke-interface {v6}, Ljava/util/Map;->values()Ljava/util/Collection;

    #@4b
    move-result-object v6

    #@4c
    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@4f
    move-result-object v3

    #@50
    .local v3, i$:Ljava/util/Iterator;
    :goto_50
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@53
    move-result v6

    #@54
    if-eqz v6, :cond_6e

    #@56
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@59
    move-result-object v2

    #@5a
    check-cast v2, Lcom/android/server/sip/SipService$SipSessionGroupExt;

    #@5c
    .line 1146
    .local v2, group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    const/4 v6, 0x0

    #@5d
    invoke-virtual {v2, v6}, Lcom/android/server/sip/SipService$SipSessionGroupExt;->onConnectivityChanged(Z)V
    :try_end_60
    .catchall {:try_start_3c .. :try_end_60} :catchall_6b
    .catch Ljavax/sip/SipException; {:try_start_3c .. :try_end_60} :catch_61

    #@60
    goto :goto_50

    #@61
    .line 1160
    .end local v2           #group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    .end local v3           #i$:Ljava/util/Iterator;
    :catch_61
    move-exception v1

    #@62
    .line 1161
    .local v1, e:Ljavax/sip/SipException;
    :try_start_62
    const-string v5, "SipService"

    #@64
    const-string/jumbo v6, "onConnectivityChanged()"

    #@67
    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6a
    .catchall {:try_start_62 .. :try_end_6a} :catchall_6b

    #@6a
    goto :goto_38

    #@6b
    .line 1119
    .end local v1           #e:Ljavax/sip/SipException;
    .end local v4           #networkType:I
    :catchall_6b
    move-exception v5

    #@6c
    monitor-exit p0

    #@6d
    throw v5

    #@6e
    .line 1149
    .restart local v4       #networkType:I
    :cond_6e
    :try_start_6e
    iput v4, p0, Lcom/android/server/sip/SipService;->mNetworkType:I

    #@70
    .line 1151
    iget v6, p0, Lcom/android/server/sip/SipService;->mNetworkType:I

    #@72
    if-eq v6, v5, :cond_9c

    #@74
    .line 1152
    invoke-direct {p0}, Lcom/android/server/sip/SipService;->determineLocalIp()Ljava/lang/String;

    #@77
    move-result-object v5

    #@78
    iput-object v5, p0, Lcom/android/server/sip/SipService;->mLocalIp:Ljava/lang/String;

    #@7a
    .line 1153
    const/4 v5, -0x1

    #@7b
    iput v5, p0, Lcom/android/server/sip/SipService;->mKeepAliveInterval:I

    #@7d
    .line 1154
    const/16 v5, 0xa

    #@7f
    iput v5, p0, Lcom/android/server/sip/SipService;->mLastGoodKeepAliveInterval:I

    #@81
    .line 1155
    iget-object v5, p0, Lcom/android/server/sip/SipService;->mSipGroups:Ljava/util/Map;

    #@83
    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    #@86
    move-result-object v5

    #@87
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@8a
    move-result-object v3

    #@8b
    .restart local v3       #i$:Ljava/util/Iterator;
    :goto_8b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@8e
    move-result v5

    #@8f
    if-eqz v5, :cond_9c

    #@91
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@94
    move-result-object v2

    #@95
    check-cast v2, Lcom/android/server/sip/SipService$SipSessionGroupExt;

    #@97
    .line 1156
    .restart local v2       #group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    const/4 v5, 0x1

    #@98
    invoke-virtual {v2, v5}, Lcom/android/server/sip/SipService$SipSessionGroupExt;->onConnectivityChanged(Z)V

    #@9b
    goto :goto_8b

    #@9c
    .line 1159
    .end local v2           #group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    .end local v3           #i$:Ljava/util/Iterator;
    :cond_9c
    invoke-direct {p0}, Lcom/android/server/sip/SipService;->updateWakeLocks()V
    :try_end_9f
    .catchall {:try_start_6e .. :try_end_9f} :catchall_6b
    .catch Ljavax/sip/SipException; {:try_start_6e .. :try_end_9f} :catch_61

    #@9f
    goto :goto_38
.end method

.method private declared-synchronized onKeepAliveIntervalChanged()V
    .registers 4

    #@0
    .prologue
    .line 418
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v2, p0, Lcom/android/server/sip/SipService;->mSipGroups:Ljava/util/Map;

    #@3
    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    #@6
    move-result-object v2

    #@7
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v1

    #@b
    .local v1, i$:Ljava/util/Iterator;
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v2

    #@f
    if-eqz v2, :cond_1e

    #@11
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Lcom/android/server/sip/SipService$SipSessionGroupExt;

    #@17
    .line 419
    .local v0, group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    invoke-virtual {v0}, Lcom/android/server/sip/SipService$SipSessionGroupExt;->onKeepAliveIntervalChanged()V
    :try_end_1a
    .catchall {:try_start_1 .. :try_end_1a} :catchall_1b

    #@1a
    goto :goto_b

    #@1b
    .line 418
    .end local v0           #group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    .end local v1           #i$:Ljava/util/Iterator;
    :catchall_1b
    move-exception v2

    #@1c
    monitor-exit p0

    #@1d
    throw v2

    #@1e
    .line 421
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_1e
    monitor-exit p0

    #@1f
    return-void
.end method

.method private registerReceivers()V
    .registers 5

    #@0
    .prologue
    .line 1080
    iget-object v0, p0, Lcom/android/server/sip/SipService;->mContext:Landroid/content/Context;

    #@2
    iget-object v1, p0, Lcom/android/server/sip/SipService;->mConnectivityReceiver:Lcom/android/server/sip/SipService$ConnectivityReceiver;

    #@4
    new-instance v2, Landroid/content/IntentFilter;

    #@6
    const-string v3, "android.net.conn.CONNECTIVITY_CHANGE"

    #@8
    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@b
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@e
    .line 1083
    return-void
.end method

.method private restartPortMappingLifetimeMeasurement(Landroid/net/sip/SipProfile;I)V
    .registers 4
    .parameter "localProfile"
    .parameter "maxInterval"

    #@0
    .prologue
    .line 376
    invoke-direct {p0}, Lcom/android/server/sip/SipService;->stopPortMappingMeasurement()V

    #@3
    .line 377
    const/4 v0, -0x1

    #@4
    iput v0, p0, Lcom/android/server/sip/SipService;->mKeepAliveInterval:I

    #@6
    .line 378
    invoke-direct {p0, p1, p2}, Lcom/android/server/sip/SipService;->startPortMappingLifetimeMeasurement(Landroid/net/sip/SipProfile;I)V

    #@9
    .line 379
    return-void
.end method

.method public static start(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 107
    invoke-static {p0}, Landroid/net/sip/SipManager;->isApiSupported(Landroid/content/Context;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_1b

    #@6
    .line 108
    const-string/jumbo v0, "sip"

    #@9
    new-instance v1, Lcom/android/server/sip/SipService;

    #@b
    invoke-direct {v1, p0}, Lcom/android/server/sip/SipService;-><init>(Landroid/content/Context;)V

    #@e
    invoke-static {v0, v1}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    #@11
    .line 109
    new-instance v0, Landroid/content/Intent;

    #@13
    const-string v1, "android.net.sip.SIP_SERVICE_UP"

    #@15
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@18
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@1b
    .line 112
    :cond_1b
    return-void
.end method

.method private startPortMappingLifetimeMeasurement(Landroid/net/sip/SipProfile;)V
    .registers 3
    .parameter "localProfile"

    #@0
    .prologue
    .line 348
    const/16 v0, 0x78

    #@2
    invoke-direct {p0, p1, v0}, Lcom/android/server/sip/SipService;->startPortMappingLifetimeMeasurement(Landroid/net/sip/SipProfile;I)V

    #@5
    .line 350
    return-void
.end method

.method private startPortMappingLifetimeMeasurement(Landroid/net/sip/SipProfile;I)V
    .registers 7
    .parameter "localProfile"
    .parameter "maxInterval"

    #@0
    .prologue
    .line 354
    iget-object v1, p0, Lcom/android/server/sip/SipService;->mIntervalMeasurementProcess:Lcom/android/server/sip/SipService$IntervalMeasurementProcess;

    #@2
    if-nez v1, :cond_5a

    #@4
    iget v1, p0, Lcom/android/server/sip/SipService;->mKeepAliveInterval:I

    #@6
    const/4 v2, -0x1

    #@7
    if-ne v1, v2, :cond_5a

    #@9
    iget-object v1, p0, Lcom/android/server/sip/SipService;->mLocalIp:Ljava/lang/String;

    #@b
    invoke-direct {p0, v1}, Lcom/android/server/sip/SipService;->isBehindNAT(Ljava/lang/String;)Z

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_5a

    #@11
    .line 357
    const-string v1, "SipService"

    #@13
    new-instance v2, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string/jumbo v3, "start NAT port mapping timeout measurement on "

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {p1}, Landroid/net/sip/SipProfile;->getUriString()Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 360
    iget v0, p0, Lcom/android/server/sip/SipService;->mLastGoodKeepAliveInterval:I

    #@30
    .line 361
    .local v0, minInterval:I
    if-lt v0, p2, :cond_4e

    #@32
    .line 364
    const/16 v0, 0xa

    #@34
    iput v0, p0, Lcom/android/server/sip/SipService;->mLastGoodKeepAliveInterval:I

    #@36
    .line 366
    const-string v1, "SipService"

    #@38
    new-instance v2, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v3, "  reset min interval to "

    #@3f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v2

    #@4b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 368
    :cond_4e
    new-instance v1, Lcom/android/server/sip/SipService$IntervalMeasurementProcess;

    #@50
    invoke-direct {v1, p0, p1, v0, p2}, Lcom/android/server/sip/SipService$IntervalMeasurementProcess;-><init>(Lcom/android/server/sip/SipService;Landroid/net/sip/SipProfile;II)V

    #@53
    iput-object v1, p0, Lcom/android/server/sip/SipService;->mIntervalMeasurementProcess:Lcom/android/server/sip/SipService$IntervalMeasurementProcess;

    #@55
    .line 370
    iget-object v1, p0, Lcom/android/server/sip/SipService;->mIntervalMeasurementProcess:Lcom/android/server/sip/SipService$IntervalMeasurementProcess;

    #@57
    invoke-virtual {v1}, Lcom/android/server/sip/SipService$IntervalMeasurementProcess;->start()V

    #@5a
    .line 372
    .end local v0           #minInterval:I
    :cond_5a
    return-void
.end method

.method private stopPortMappingMeasurement()V
    .registers 2

    #@0
    .prologue
    .line 340
    iget-object v0, p0, Lcom/android/server/sip/SipService;->mIntervalMeasurementProcess:Lcom/android/server/sip/SipService$IntervalMeasurementProcess;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 341
    iget-object v0, p0, Lcom/android/server/sip/SipService;->mIntervalMeasurementProcess:Lcom/android/server/sip/SipService$IntervalMeasurementProcess;

    #@6
    invoke-virtual {v0}, Lcom/android/server/sip/SipService$IntervalMeasurementProcess;->stop()V

    #@9
    .line 342
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Lcom/android/server/sip/SipService;->mIntervalMeasurementProcess:Lcom/android/server/sip/SipService$IntervalMeasurementProcess;

    #@c
    .line 344
    :cond_c
    return-void
.end method

.method private unregisterReceivers()V
    .registers 3

    #@0
    .prologue
    .line 1086
    iget-object v0, p0, Lcom/android/server/sip/SipService;->mContext:Landroid/content/Context;

    #@2
    iget-object v1, p0, Lcom/android/server/sip/SipService;->mConnectivityReceiver:Lcom/android/server/sip/SipService$ConnectivityReceiver;

    #@4
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@7
    .line 1090
    iget-object v0, p0, Lcom/android/server/sip/SipService;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    #@9
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    #@c
    .line 1091
    const/4 v0, -0x1

    #@d
    iput v0, p0, Lcom/android/server/sip/SipService;->mNetworkType:I

    #@f
    .line 1092
    return-void
.end method

.method private updateWakeLocks()V
    .registers 5

    #@0
    .prologue
    .line 1095
    iget-object v2, p0, Lcom/android/server/sip/SipService;->mSipGroups:Ljava/util/Map;

    #@2
    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    #@5
    move-result-object v2

    #@6
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v1

    #@a
    .local v1, i$:Ljava/util/Iterator;
    :cond_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_32

    #@10
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Lcom/android/server/sip/SipService$SipSessionGroupExt;

    #@16
    .line 1096
    .local v0, group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    invoke-virtual {v0}, Lcom/android/server/sip/SipService$SipSessionGroupExt;->isOpenedToReceiveCalls()Z

    #@19
    move-result v2

    #@1a
    if-eqz v2, :cond_a

    #@1c
    .line 1100
    iget v2, p0, Lcom/android/server/sip/SipService;->mNetworkType:I

    #@1e
    const/4 v3, 0x1

    #@1f
    if-eq v2, v3, :cond_26

    #@21
    iget v2, p0, Lcom/android/server/sip/SipService;->mNetworkType:I

    #@23
    const/4 v3, -0x1

    #@24
    if-ne v2, v3, :cond_2c

    #@26
    .line 1101
    :cond_26
    iget-object v2, p0, Lcom/android/server/sip/SipService;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    #@28
    invoke-virtual {v2}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    #@2b
    .line 1110
    .end local v0           #group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    :goto_2b
    return-void

    #@2c
    .line 1103
    .restart local v0       #group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    :cond_2c
    iget-object v2, p0, Lcom/android/server/sip/SipService;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    #@2e
    invoke-virtual {v2}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    #@31
    goto :goto_2b

    #@32
    .line 1108
    .end local v0           #group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    :cond_32
    iget-object v2, p0, Lcom/android/server/sip/SipService;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    #@34
    invoke-virtual {v2}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    #@37
    .line 1109
    iget-object v2, p0, Lcom/android/server/sip/SipService;->mMyWakeLock:Lcom/android/server/sip/SipWakeLock;

    #@39
    invoke-virtual {v2}, Lcom/android/server/sip/SipWakeLock;->reset()V

    #@3c
    goto :goto_2b
.end method


# virtual methods
.method public declared-synchronized close(Ljava/lang/String;)V
    .registers 6
    .parameter "localProfileUri"

    #@0
    .prologue
    .line 196
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v1, p0, Lcom/android/server/sip/SipService;->mContext:Landroid/content/Context;

    #@3
    const-string v2, "android.permission.USE_SIP"

    #@5
    const/4 v3, 0x0

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 198
    iget-object v1, p0, Lcom/android/server/sip/SipService;->mSipGroups:Ljava/util/Map;

    #@b
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Lcom/android/server/sip/SipService$SipSessionGroupExt;
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_24

    #@11
    .line 199
    .local v0, group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    if-nez v0, :cond_15

    #@13
    .line 210
    :goto_13
    monitor-exit p0

    #@14
    return-void

    #@15
    .line 200
    :cond_15
    :try_start_15
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipService;->isCallerCreatorOrRadio(Lcom/android/server/sip/SipService$SipSessionGroupExt;)Z

    #@18
    move-result v1

    #@19
    if-nez v1, :cond_27

    #@1b
    .line 201
    const-string v1, "SipService"

    #@1d
    const-string/jumbo v2, "only creator or radio can close this profile"

    #@20
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_23
    .catchall {:try_start_15 .. :try_end_23} :catchall_24

    #@23
    goto :goto_13

    #@24
    .line 196
    .end local v0           #group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    :catchall_24
    move-exception v1

    #@25
    monitor-exit p0

    #@26
    throw v1

    #@27
    .line 205
    .restart local v0       #group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    :cond_27
    :try_start_27
    iget-object v1, p0, Lcom/android/server/sip/SipService;->mSipGroups:Ljava/util/Map;

    #@29
    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@2c
    move-result-object v0

    #@2d
    .end local v0           #group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    check-cast v0, Lcom/android/server/sip/SipService$SipSessionGroupExt;

    #@2f
    .line 206
    .restart local v0       #group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    invoke-virtual {v0}, Lcom/android/server/sip/SipService$SipSessionGroupExt;->getLocalProfile()Landroid/net/sip/SipProfile;

    #@32
    move-result-object v1

    #@33
    invoke-direct {p0, v1}, Lcom/android/server/sip/SipService;->notifyProfileRemoved(Landroid/net/sip/SipProfile;)V

    #@36
    .line 207
    invoke-virtual {v0}, Lcom/android/server/sip/SipService$SipSessionGroupExt;->close()V

    #@39
    .line 209
    invoke-direct {p0}, Lcom/android/server/sip/SipService;->updateWakeLocks()V
    :try_end_3c
    .catchall {:try_start_27 .. :try_end_3c} :catchall_24

    #@3c
    goto :goto_13
.end method

.method public declared-synchronized createSession(Landroid/net/sip/SipProfile;Landroid/net/sip/ISipSessionListener;)Landroid/net/sip/ISipSession;
    .registers 9
    .parameter "localProfile"
    .parameter "listener"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 253
    monitor-enter p0

    #@2
    :try_start_2
    iget-object v3, p0, Lcom/android/server/sip/SipService;->mContext:Landroid/content/Context;

    #@4
    const-string v4, "android.permission.USE_SIP"

    #@6
    const/4 v5, 0x0

    #@7
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 255
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@d
    move-result v3

    #@e
    invoke-virtual {p1, v3}, Landroid/net/sip/SipProfile;->setCallingUid(I)V

    #@11
    .line 256
    iget v3, p0, Lcom/android/server/sip/SipService;->mNetworkType:I
    :try_end_13
    .catchall {:try_start_2 .. :try_end_13} :catchall_23

    #@13
    const/4 v4, -0x1

    #@14
    if-ne v3, v4, :cond_18

    #@16
    .line 262
    :goto_16
    monitor-exit p0

    #@17
    return-object v2

    #@18
    .line 258
    :cond_18
    :try_start_18
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipService;->createGroup(Landroid/net/sip/SipProfile;)Lcom/android/server/sip/SipService$SipSessionGroupExt;

    #@1b
    move-result-object v1

    #@1c
    .line 259
    .local v1, group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    invoke-virtual {v1, p2}, Lcom/android/server/sip/SipService$SipSessionGroupExt;->createSession(Landroid/net/sip/ISipSessionListener;)Landroid/net/sip/ISipSession;
    :try_end_1f
    .catchall {:try_start_18 .. :try_end_1f} :catchall_23
    .catch Ljavax/sip/SipException; {:try_start_18 .. :try_end_1f} :catch_21

    #@1f
    move-result-object v2

    #@20
    goto :goto_16

    #@21
    .line 260
    .end local v1           #group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    :catch_21
    move-exception v0

    #@22
    .line 262
    .local v0, e:Ljavax/sip/SipException;
    goto :goto_16

    #@23
    .line 253
    .end local v0           #e:Ljavax/sip/SipException;
    :catchall_23
    move-exception v2

    #@24
    monitor-exit p0

    #@25
    throw v2
.end method

.method public declared-synchronized getListOfProfiles()[Landroid/net/sip/SipProfile;
    .registers 8

    #@0
    .prologue
    .line 132
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v4, p0, Lcom/android/server/sip/SipService;->mContext:Landroid/content/Context;

    #@3
    const-string v5, "android.permission.USE_SIP"

    #@5
    const/4 v6, 0x0

    #@6
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 134
    invoke-direct {p0}, Lcom/android/server/sip/SipService;->isCallerRadio()Z

    #@c
    move-result v2

    #@d
    .line 135
    .local v2, isCallerRadio:Z
    new-instance v3, Ljava/util/ArrayList;

    #@f
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@12
    .line 136
    .local v3, profiles:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/sip/SipProfile;>;"
    iget-object v4, p0, Lcom/android/server/sip/SipService;->mSipGroups:Ljava/util/Map;

    #@14
    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    #@17
    move-result-object v4

    #@18
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@1b
    move-result-object v1

    #@1c
    .local v1, i$:Ljava/util/Iterator;
    :cond_1c
    :goto_1c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@1f
    move-result v4

    #@20
    if-eqz v4, :cond_3b

    #@22
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@25
    move-result-object v0

    #@26
    check-cast v0, Lcom/android/server/sip/SipService$SipSessionGroupExt;

    #@28
    .line 137
    .local v0, group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    if-nez v2, :cond_30

    #@2a
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipService;->isCallerCreator(Lcom/android/server/sip/SipService$SipSessionGroupExt;)Z

    #@2d
    move-result v4

    #@2e
    if-eqz v4, :cond_1c

    #@30
    .line 138
    :cond_30
    invoke-virtual {v0}, Lcom/android/server/sip/SipService$SipSessionGroupExt;->getLocalProfile()Landroid/net/sip/SipProfile;

    #@33
    move-result-object v4

    #@34
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_37
    .catchall {:try_start_1 .. :try_end_37} :catchall_38

    #@37
    goto :goto_1c

    #@38
    .line 132
    .end local v0           #group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #isCallerRadio:Z
    .end local v3           #profiles:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/sip/SipProfile;>;"
    :catchall_38
    move-exception v4

    #@39
    monitor-exit p0

    #@3a
    throw v4

    #@3b
    .line 141
    .restart local v1       #i$:Ljava/util/Iterator;
    .restart local v2       #isCallerRadio:Z
    .restart local v3       #profiles:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/sip/SipProfile;>;"
    :cond_3b
    :try_start_3b
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@3e
    move-result v4

    #@3f
    new-array v4, v4, [Landroid/net/sip/SipProfile;

    #@41
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@44
    move-result-object v4

    #@45
    check-cast v4, [Landroid/net/sip/SipProfile;
    :try_end_47
    .catchall {:try_start_3b .. :try_end_47} :catchall_38

    #@47
    monitor-exit p0

    #@48
    return-object v4
.end method

.method public declared-synchronized getPendingSession(Ljava/lang/String;)Landroid/net/sip/ISipSession;
    .registers 6
    .parameter "callId"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 267
    monitor-enter p0

    #@2
    :try_start_2
    iget-object v1, p0, Lcom/android/server/sip/SipService;->mContext:Landroid/content/Context;

    #@4
    const-string v2, "android.permission.USE_SIP"

    #@6
    const/4 v3, 0x0

    #@7
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_2 .. :try_end_a} :catchall_17

    #@a
    .line 269
    if-nez p1, :cond_e

    #@c
    .line 270
    :goto_c
    monitor-exit p0

    #@d
    return-object v0

    #@e
    :cond_e
    :try_start_e
    iget-object v0, p0, Lcom/android/server/sip/SipService;->mPendingSessions:Ljava/util/Map;

    #@10
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Landroid/net/sip/ISipSession;
    :try_end_16
    .catchall {:try_start_e .. :try_end_16} :catchall_17

    #@16
    goto :goto_c

    #@17
    .line 267
    :catchall_17
    move-exception v0

    #@18
    monitor-exit p0

    #@19
    throw v0
.end method

.method public declared-synchronized isOpened(Ljava/lang/String;)Z
    .registers 7
    .parameter "localProfileUri"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 213
    monitor-enter p0

    #@2
    :try_start_2
    iget-object v2, p0, Lcom/android/server/sip/SipService;->mContext:Landroid/content/Context;

    #@4
    const-string v3, "android.permission.USE_SIP"

    #@6
    const/4 v4, 0x0

    #@7
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 215
    iget-object v2, p0, Lcom/android/server/sip/SipService;->mSipGroups:Ljava/util/Map;

    #@c
    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/android/server/sip/SipService$SipSessionGroupExt;
    :try_end_12
    .catchall {:try_start_2 .. :try_end_12} :catchall_27

    #@12
    .line 216
    .local v0, group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    if-nez v0, :cond_16

    #@14
    .line 221
    :goto_14
    monitor-exit p0

    #@15
    return v1

    #@16
    .line 217
    :cond_16
    :try_start_16
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipService;->isCallerCreatorOrRadio(Lcom/android/server/sip/SipService$SipSessionGroupExt;)Z

    #@19
    move-result v2

    #@1a
    if-eqz v2, :cond_1e

    #@1c
    .line 218
    const/4 v1, 0x1

    #@1d
    goto :goto_14

    #@1e
    .line 220
    :cond_1e
    const-string v2, "SipService"

    #@20
    const-string/jumbo v3, "only creator or radio can query on the profile"

    #@23
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_26
    .catchall {:try_start_16 .. :try_end_26} :catchall_27

    #@26
    goto :goto_14

    #@27
    .line 213
    .end local v0           #group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    :catchall_27
    move-exception v1

    #@28
    monitor-exit p0

    #@29
    throw v1
.end method

.method public declared-synchronized isRegistered(Ljava/lang/String;)Z
    .registers 7
    .parameter "localProfileUri"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 226
    monitor-enter p0

    #@2
    :try_start_2
    iget-object v2, p0, Lcom/android/server/sip/SipService;->mContext:Landroid/content/Context;

    #@4
    const-string v3, "android.permission.USE_SIP"

    #@6
    const/4 v4, 0x0

    #@7
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 228
    iget-object v2, p0, Lcom/android/server/sip/SipService;->mSipGroups:Ljava/util/Map;

    #@c
    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/android/server/sip/SipService$SipSessionGroupExt;
    :try_end_12
    .catchall {:try_start_2 .. :try_end_12} :catchall_2a

    #@12
    .line 229
    .local v0, group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    if-nez v0, :cond_16

    #@14
    .line 234
    :goto_14
    monitor-exit p0

    #@15
    return v1

    #@16
    .line 230
    :cond_16
    :try_start_16
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipService;->isCallerCreatorOrRadio(Lcom/android/server/sip/SipService$SipSessionGroupExt;)Z

    #@19
    move-result v2

    #@1a
    if-eqz v2, :cond_21

    #@1c
    .line 231
    invoke-virtual {v0}, Lcom/android/server/sip/SipService$SipSessionGroupExt;->isRegistered()Z

    #@1f
    move-result v1

    #@20
    goto :goto_14

    #@21
    .line 233
    :cond_21
    const-string v2, "SipService"

    #@23
    const-string/jumbo v3, "only creator or radio can query on the profile"

    #@26
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_29
    .catchall {:try_start_16 .. :try_end_29} :catchall_2a

    #@29
    goto :goto_14

    #@2a
    .line 226
    .end local v0           #group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    :catchall_2a
    move-exception v1

    #@2b
    monitor-exit p0

    #@2c
    throw v1
.end method

.method public declared-synchronized open(Landroid/net/sip/SipProfile;)V
    .registers 6
    .parameter "localProfile"

    #@0
    .prologue
    .line 145
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v1, p0, Lcom/android/server/sip/SipService;->mContext:Landroid/content/Context;

    #@3
    const-string v2, "android.permission.USE_SIP"

    #@5
    const/4 v3, 0x0

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 147
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@c
    move-result v1

    #@d
    invoke-virtual {p1, v1}, Landroid/net/sip/SipProfile;->setCallingUid(I)V
    :try_end_10
    .catchall {:try_start_1 .. :try_end_10} :catchall_1f

    #@10
    .line 149
    :try_start_10
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipService;->createGroup(Landroid/net/sip/SipProfile;)Lcom/android/server/sip/SipService$SipSessionGroupExt;
    :try_end_13
    .catchall {:try_start_10 .. :try_end_13} :catchall_1f
    .catch Ljavax/sip/SipException; {:try_start_10 .. :try_end_13} :catch_15

    #@13
    .line 154
    :goto_13
    monitor-exit p0

    #@14
    return-void

    #@15
    .line 150
    :catch_15
    move-exception v0

    #@16
    .line 151
    .local v0, e:Ljavax/sip/SipException;
    :try_start_16
    const-string v1, "SipService"

    #@18
    const-string/jumbo v2, "openToMakeCalls()"

    #@1b
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1e
    .catchall {:try_start_16 .. :try_end_1e} :catchall_1f

    #@1e
    goto :goto_13

    #@1f
    .line 145
    .end local v0           #e:Ljavax/sip/SipException;
    :catchall_1f
    move-exception v1

    #@20
    monitor-exit p0

    #@21
    throw v1
.end method

.method public declared-synchronized open3(Landroid/net/sip/SipProfile;Landroid/app/PendingIntent;Landroid/net/sip/ISipSessionListener;)V
    .registers 9
    .parameter "localProfile"
    .parameter "incomingCallPendingIntent"
    .parameter "listener"

    #@0
    .prologue
    .line 159
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v2, p0, Lcom/android/server/sip/SipService;->mContext:Landroid/content/Context;

    #@3
    const-string v3, "android.permission.USE_SIP"

    #@5
    const/4 v4, 0x0

    #@6
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 161
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@c
    move-result v2

    #@d
    invoke-virtual {p1, v2}, Landroid/net/sip/SipProfile;->setCallingUid(I)V

    #@10
    .line 162
    if-nez p2, :cond_1b

    #@12
    .line 163
    const-string v2, "SipService"

    #@14
    const-string v3, "incomingCallPendingIntent cannot be null; the profile is not opened"

    #@16
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_19
    .catchall {:try_start_1 .. :try_end_19} :catchall_36

    #@19
    .line 180
    :cond_19
    :goto_19
    monitor-exit p0

    #@1a
    return-void

    #@1b
    .line 170
    :cond_1b
    :try_start_1b
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/sip/SipService;->createGroup(Landroid/net/sip/SipProfile;Landroid/app/PendingIntent;Landroid/net/sip/ISipSessionListener;)Lcom/android/server/sip/SipService$SipSessionGroupExt;

    #@1e
    move-result-object v1

    #@1f
    .line 172
    .local v1, group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    invoke-virtual {p1}, Landroid/net/sip/SipProfile;->getAutoRegistration()Z

    #@22
    move-result v2

    #@23
    if-eqz v2, :cond_19

    #@25
    .line 173
    invoke-virtual {v1}, Lcom/android/server/sip/SipService$SipSessionGroupExt;->openToReceiveCalls()V

    #@28
    .line 174
    invoke-direct {p0}, Lcom/android/server/sip/SipService;->updateWakeLocks()V
    :try_end_2b
    .catchall {:try_start_1b .. :try_end_2b} :catchall_36
    .catch Ljavax/sip/SipException; {:try_start_1b .. :try_end_2b} :catch_2c

    #@2b
    goto :goto_19

    #@2c
    .line 176
    .end local v1           #group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    :catch_2c
    move-exception v0

    #@2d
    .line 177
    .local v0, e:Ljavax/sip/SipException;
    :try_start_2d
    const-string v2, "SipService"

    #@2f
    const-string/jumbo v3, "openToReceiveCalls()"

    #@32
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_35
    .catchall {:try_start_2d .. :try_end_35} :catchall_36

    #@35
    goto :goto_19

    #@36
    .line 159
    .end local v0           #e:Ljavax/sip/SipException;
    :catchall_36
    move-exception v2

    #@37
    monitor-exit p0

    #@38
    throw v2
.end method

.method public declared-synchronized setRegistrationListener(Ljava/lang/String;Landroid/net/sip/ISipSessionListener;)V
    .registers 7
    .parameter "localProfileUri"
    .parameter "listener"

    #@0
    .prologue
    .line 240
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v1, p0, Lcom/android/server/sip/SipService;->mContext:Landroid/content/Context;

    #@3
    const-string v2, "android.permission.USE_SIP"

    #@5
    const/4 v3, 0x0

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 242
    iget-object v1, p0, Lcom/android/server/sip/SipService;->mSipGroups:Ljava/util/Map;

    #@b
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Lcom/android/server/sip/SipService$SipSessionGroupExt;
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_1f

    #@11
    .line 243
    .local v0, group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    if-nez v0, :cond_15

    #@13
    .line 249
    :goto_13
    monitor-exit p0

    #@14
    return-void

    #@15
    .line 244
    :cond_15
    :try_start_15
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipService;->isCallerCreator(Lcom/android/server/sip/SipService$SipSessionGroupExt;)Z

    #@18
    move-result v1

    #@19
    if-eqz v1, :cond_22

    #@1b
    .line 245
    invoke-virtual {v0, p2}, Lcom/android/server/sip/SipService$SipSessionGroupExt;->setListener(Landroid/net/sip/ISipSessionListener;)V
    :try_end_1e
    .catchall {:try_start_15 .. :try_end_1e} :catchall_1f

    #@1e
    goto :goto_13

    #@1f
    .line 240
    .end local v0           #group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    :catchall_1f
    move-exception v1

    #@20
    monitor-exit p0

    #@21
    throw v1

    #@22
    .line 247
    .restart local v0       #group:Lcom/android/server/sip/SipService$SipSessionGroupExt;
    :cond_22
    :try_start_22
    const-string v1, "SipService"

    #@24
    const-string/jumbo v2, "only creator can set listener on the profile"

    #@27
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2a
    .catchall {:try_start_22 .. :try_end_2a} :catchall_1f

    #@2a
    goto :goto_13
.end method
