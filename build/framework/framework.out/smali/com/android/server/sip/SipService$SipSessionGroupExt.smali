.class Lcom/android/server/sip/SipService$SipSessionGroupExt;
.super Landroid/net/sip/SipSessionAdapter;
.source "SipService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/sip/SipService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SipSessionGroupExt"
.end annotation


# instance fields
.field private mAutoRegistration:Lcom/android/server/sip/SipService$AutoRegistrationProcess;

.field private mIncomingCallPendingIntent:Landroid/app/PendingIntent;

.field private mOpenedToReceiveCalls:Z

.field private mSipGroup:Lcom/android/server/sip/SipSessionGroup;

.field final synthetic this$0:Lcom/android/server/sip/SipService;


# direct methods
.method public constructor <init>(Lcom/android/server/sip/SipService;Landroid/net/sip/SipProfile;Landroid/app/PendingIntent;Landroid/net/sip/ISipSessionListener;)V
    .registers 10
    .parameter
    .parameter "localProfile"
    .parameter "incomingCallPendingIntent"
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 455
    iput-object p1, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->this$0:Lcom/android/server/sip/SipService;

    #@2
    invoke-direct {p0}, Landroid/net/sip/SipSessionAdapter;-><init>()V

    #@5
    .line 450
    new-instance v0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;

    #@7
    iget-object v1, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->this$0:Lcom/android/server/sip/SipService;

    #@9
    const/4 v2, 0x0

    #@a
    invoke-direct {v0, v1, v2}, Lcom/android/server/sip/SipService$AutoRegistrationProcess;-><init>(Lcom/android/server/sip/SipService;Lcom/android/server/sip/SipService$1;)V

    #@d
    iput-object v0, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->mAutoRegistration:Lcom/android/server/sip/SipService$AutoRegistrationProcess;

    #@f
    .line 456
    new-instance v0, Lcom/android/server/sip/SipSessionGroup;

    #@11
    invoke-direct {p0, p2}, Lcom/android/server/sip/SipService$SipSessionGroupExt;->duplicate(Landroid/net/sip/SipProfile;)Landroid/net/sip/SipProfile;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {p2}, Landroid/net/sip/SipProfile;->getPassword()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-static {p1}, Lcom/android/server/sip/SipService;->access$200(Lcom/android/server/sip/SipService;)Lcom/android/server/sip/SipWakeupTimer;

    #@1c
    move-result-object v3

    #@1d
    invoke-static {p1}, Lcom/android/server/sip/SipService;->access$300(Lcom/android/server/sip/SipService;)Lcom/android/server/sip/SipWakeLock;

    #@20
    move-result-object v4

    #@21
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/server/sip/SipSessionGroup;-><init>(Landroid/net/sip/SipProfile;Ljava/lang/String;Lcom/android/server/sip/SipWakeupTimer;Lcom/android/server/sip/SipWakeLock;)V

    #@24
    iput-object v0, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->mSipGroup:Lcom/android/server/sip/SipSessionGroup;

    #@26
    .line 458
    iput-object p3, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->mIncomingCallPendingIntent:Landroid/app/PendingIntent;

    #@28
    .line 459
    iget-object v0, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->mAutoRegistration:Lcom/android/server/sip/SipService$AutoRegistrationProcess;

    #@2a
    invoke-virtual {v0, p4}, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->setListener(Landroid/net/sip/ISipSessionListener;)V

    #@2d
    .line 460
    return-void
.end method

.method private duplicate(Landroid/net/sip/SipProfile;)Landroid/net/sip/SipProfile;
    .registers 5
    .parameter "p"

    #@0
    .prologue
    .line 482
    :try_start_0
    new-instance v1, Landroid/net/sip/SipProfile$Builder;

    #@2
    invoke-direct {v1, p1}, Landroid/net/sip/SipProfile$Builder;-><init>(Landroid/net/sip/SipProfile;)V

    #@5
    const-string v2, "*"

    #@7
    invoke-virtual {v1, v2}, Landroid/net/sip/SipProfile$Builder;->setPassword(Ljava/lang/String;)Landroid/net/sip/SipProfile$Builder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1}, Landroid/net/sip/SipProfile$Builder;->build()Landroid/net/sip/SipProfile;
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_e} :catch_10

    #@e
    move-result-object v1

    #@f
    return-object v1

    #@10
    .line 483
    :catch_10
    move-exception v0

    #@11
    .line 484
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "SipService"

    #@13
    const-string v2, "duplicate()"

    #@15
    invoke-static {v1, v2, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@18
    .line 485
    new-instance v1, Ljava/lang/RuntimeException;

    #@1a
    const-string v2, "duplicate profile"

    #@1c
    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@1f
    throw v1
.end method

.method private getUri()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 579
    iget-object v0, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->mSipGroup:Lcom/android/server/sip/SipSessionGroup;

    #@2
    invoke-virtual {v0}, Lcom/android/server/sip/SipSessionGroup;->getLocalProfileUri()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method


# virtual methods
.method public close()V
    .registers 2

    #@0
    .prologue
    .line 523
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->mOpenedToReceiveCalls:Z

    #@3
    .line 524
    iget-object v0, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->mSipGroup:Lcom/android/server/sip/SipSessionGroup;

    #@5
    invoke-virtual {v0}, Lcom/android/server/sip/SipSessionGroup;->close()V

    #@8
    .line 525
    iget-object v0, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->mAutoRegistration:Lcom/android/server/sip/SipService$AutoRegistrationProcess;

    #@a
    invoke-virtual {v0}, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->stop()V

    #@d
    .line 528
    return-void
.end method

.method public containsSession(Ljava/lang/String;)Z
    .registers 3
    .parameter "callId"

    #@0
    .prologue
    .line 467
    iget-object v0, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->mSipGroup:Lcom/android/server/sip/SipSessionGroup;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/server/sip/SipSessionGroup;->containsSession(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public createSession(Landroid/net/sip/ISipSessionListener;)Landroid/net/sip/ISipSession;
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 531
    iget-object v0, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->mSipGroup:Lcom/android/server/sip/SipSessionGroup;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/server/sip/SipSessionGroup;->createSession(Landroid/net/sip/ISipSessionListener;)Landroid/net/sip/ISipSession;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getLocalProfile()Landroid/net/sip/SipProfile;
    .registers 2

    #@0
    .prologue
    .line 463
    iget-object v0, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->mSipGroup:Lcom/android/server/sip/SipSessionGroup;

    #@2
    invoke-virtual {v0}, Lcom/android/server/sip/SipSessionGroup;->getLocalProfile()Landroid/net/sip/SipProfile;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public isOpenedToReceiveCalls()Z
    .registers 2

    #@0
    .prologue
    .line 571
    iget-boolean v0, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->mOpenedToReceiveCalls:Z

    #@2
    return v0
.end method

.method public isRegistered()Z
    .registers 2

    #@0
    .prologue
    .line 575
    iget-object v0, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->mAutoRegistration:Lcom/android/server/sip/SipService$AutoRegistrationProcess;

    #@2
    invoke-virtual {v0}, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->isRegistered()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public onConnectivityChanged(Z)V
    .registers 3
    .parameter "connected"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 509
    iget-object v0, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->mSipGroup:Lcom/android/server/sip/SipSessionGroup;

    #@2
    invoke-virtual {v0}, Lcom/android/server/sip/SipSessionGroup;->onConnectivityChanged()V

    #@5
    .line 510
    if-eqz p1, :cond_14

    #@7
    .line 511
    iget-object v0, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->mSipGroup:Lcom/android/server/sip/SipSessionGroup;

    #@9
    invoke-virtual {v0}, Lcom/android/server/sip/SipSessionGroup;->reset()V

    #@c
    .line 512
    iget-boolean v0, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->mOpenedToReceiveCalls:Z

    #@e
    if-eqz v0, :cond_13

    #@10
    invoke-virtual {p0}, Lcom/android/server/sip/SipService$SipSessionGroupExt;->openToReceiveCalls()V

    #@13
    .line 520
    :cond_13
    :goto_13
    return-void

    #@14
    .line 517
    :cond_14
    iget-object v0, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->mSipGroup:Lcom/android/server/sip/SipSessionGroup;

    #@16
    invoke-virtual {v0}, Lcom/android/server/sip/SipSessionGroup;->close()V

    #@19
    .line 518
    iget-object v0, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->mAutoRegistration:Lcom/android/server/sip/SipService$AutoRegistrationProcess;

    #@1b
    invoke-virtual {v0}, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->stop()V

    #@1e
    goto :goto_13
.end method

.method public onError(Landroid/net/sip/ISipSession;ILjava/lang/String;)V
    .registers 4
    .parameter "session"
    .parameter "errorCode"
    .parameter "message"

    #@0
    .prologue
    .line 568
    return-void
.end method

.method public onKeepAliveIntervalChanged()V
    .registers 2

    #@0
    .prologue
    .line 471
    iget-object v0, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->mAutoRegistration:Lcom/android/server/sip/SipService$AutoRegistrationProcess;

    #@2
    invoke-virtual {v0}, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->onKeepAliveIntervalChanged()V

    #@5
    .line 472
    return-void
.end method

.method public onRinging(Landroid/net/sip/ISipSession;Landroid/net/sip/SipProfile;Ljava/lang/String;)V
    .registers 11
    .parameter "s"
    .parameter "caller"
    .parameter "sessionDescription"

    #@0
    .prologue
    .line 538
    move-object v2, p1

    #@1
    check-cast v2, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@3
    .line 540
    .local v2, session:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
    iget-object v4, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->this$0:Lcom/android/server/sip/SipService;

    #@5
    monitor-enter v4

    #@6
    .line 542
    :try_start_6
    invoke-virtual {p0}, Lcom/android/server/sip/SipService$SipSessionGroupExt;->isRegistered()Z

    #@9
    move-result v3

    #@a
    if-eqz v3, :cond_14

    #@c
    iget-object v3, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->this$0:Lcom/android/server/sip/SipService;

    #@e
    invoke-static {v3, p0, v2}, Lcom/android/server/sip/SipService;->access$500(Lcom/android/server/sip/SipService;Lcom/android/server/sip/SipService$SipSessionGroupExt;Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;)Z

    #@11
    move-result v3

    #@12
    if-eqz v3, :cond_19

    #@14
    .line 543
    :cond_14
    invoke-virtual {v2}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->endCall()V
    :try_end_17
    .catchall {:try_start_6 .. :try_end_17} :catchall_35
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_6 .. :try_end_17} :catch_38

    #@17
    .line 544
    :try_start_17
    monitor-exit v4
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_35

    #@18
    .line 561
    :goto_18
    return-void

    #@19
    .line 548
    :cond_19
    :try_start_19
    iget-object v3, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->this$0:Lcom/android/server/sip/SipService;

    #@1b
    invoke-static {v3, v2}, Lcom/android/server/sip/SipService;->access$600(Lcom/android/server/sip/SipService;Landroid/net/sip/ISipSession;)V

    #@1e
    .line 549
    invoke-virtual {v2}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->getCallId()Ljava/lang/String;

    #@21
    move-result-object v3

    #@22
    invoke-static {v3, p3}, Landroid/net/sip/SipManager;->createIncomingCallBroadcast(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@25
    move-result-object v1

    #@26
    .line 554
    .local v1, intent:Landroid/content/Intent;
    iget-object v3, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->mIncomingCallPendingIntent:Landroid/app/PendingIntent;

    #@28
    iget-object v5, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->this$0:Lcom/android/server/sip/SipService;

    #@2a
    invoke-static {v5}, Lcom/android/server/sip/SipService;->access$700(Lcom/android/server/sip/SipService;)Landroid/content/Context;

    #@2d
    move-result-object v5

    #@2e
    const/16 v6, 0x65

    #@30
    invoke-virtual {v3, v5, v6, v1}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_33
    .catchall {:try_start_19 .. :try_end_33} :catchall_35
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_19 .. :try_end_33} :catch_38

    #@33
    .line 560
    .end local v1           #intent:Landroid/content/Intent;
    :goto_33
    :try_start_33
    monitor-exit v4

    #@34
    goto :goto_18

    #@35
    :catchall_35
    move-exception v3

    #@36
    monitor-exit v4
    :try_end_37
    .catchall {:try_start_33 .. :try_end_37} :catchall_35

    #@37
    throw v3

    #@38
    .line 556
    :catch_38
    move-exception v0

    #@39
    .line 557
    .local v0, e:Landroid/app/PendingIntent$CanceledException;
    :try_start_39
    const-string v3, "SipService"

    #@3b
    const-string/jumbo v5, "pendingIntent is canceled, drop incoming call"

    #@3e
    invoke-static {v3, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 558
    invoke-virtual {v2}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->endCall()V
    :try_end_44
    .catchall {:try_start_39 .. :try_end_44} :catchall_35

    #@44
    goto :goto_33
.end method

.method public openToReceiveCalls()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 498
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->mOpenedToReceiveCalls:Z

    #@3
    .line 499
    iget-object v0, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->this$0:Lcom/android/server/sip/SipService;

    #@5
    invoke-static {v0}, Lcom/android/server/sip/SipService;->access$400(Lcom/android/server/sip/SipService;)I

    #@8
    move-result v0

    #@9
    const/4 v1, -0x1

    #@a
    if-eq v0, v1, :cond_18

    #@c
    .line 500
    iget-object v0, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->mSipGroup:Lcom/android/server/sip/SipSessionGroup;

    #@e
    invoke-virtual {v0, p0}, Lcom/android/server/sip/SipSessionGroup;->openToReceiveCalls(Landroid/net/sip/ISipSessionListener;)V

    #@11
    .line 501
    iget-object v0, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->mAutoRegistration:Lcom/android/server/sip/SipService$AutoRegistrationProcess;

    #@13
    iget-object v1, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->mSipGroup:Lcom/android/server/sip/SipSessionGroup;

    #@15
    invoke-virtual {v0, v1}, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->start(Lcom/android/server/sip/SipSessionGroup;)V

    #@18
    .line 505
    :cond_18
    return-void
.end method

.method public setIncomingCallPendingIntent(Landroid/app/PendingIntent;)V
    .registers 2
    .parameter "pIntent"

    #@0
    .prologue
    .line 494
    iput-object p1, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->mIncomingCallPendingIntent:Landroid/app/PendingIntent;

    #@2
    .line 495
    return-void
.end method

.method public setListener(Landroid/net/sip/ISipSessionListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 490
    iget-object v0, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->mAutoRegistration:Lcom/android/server/sip/SipService$AutoRegistrationProcess;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->setListener(Landroid/net/sip/ISipSessionListener;)V

    #@5
    .line 491
    return-void
.end method

.method setWakeupTimer(Lcom/android/server/sip/SipWakeupTimer;)V
    .registers 3
    .parameter "timer"

    #@0
    .prologue
    .line 477
    iget-object v0, p0, Lcom/android/server/sip/SipService$SipSessionGroupExt;->mSipGroup:Lcom/android/server/sip/SipSessionGroup;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/server/sip/SipSessionGroup;->setWakeupTimer(Lcom/android/server/sip/SipWakeupTimer;)V

    #@5
    .line 478
    return-void
.end method
