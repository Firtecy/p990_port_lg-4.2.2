.class Lcom/android/server/sip/SipSessionGroup;
.super Ljava/lang/Object;
.source "SipSessionGroup.java"

# interfaces
.implements Ljavax/sip/SipListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/sip/SipSessionGroup$KeepAliveProcessCallbackProxy;,
        Lcom/android/server/sip/SipSessionGroup$MakeCallCommand;,
        Lcom/android/server/sip/SipSessionGroup$RegisterCommand;,
        Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;,
        Lcom/android/server/sip/SipSessionGroup$KeepAliveProcessCallback;,
        Lcom/android/server/sip/SipSessionGroup$SipSessionCallReceiverImpl;
    }
.end annotation


# static fields
.field private static final ANONYMOUS:Ljava/lang/String; = "anonymous"

.field private static final CANCEL_CALL_TIMER:I = 0x3

.field private static final CONTINUE_CALL:Ljava/util/EventObject; = null

.field private static final DEBUG:Z = false

.field private static final DEBUG_PING:Z = false

.field private static final DEREGISTER:Ljava/util/EventObject; = null

.field private static final END_CALL:Ljava/util/EventObject; = null

.field private static final END_CALL_TIMER:I = 0x3

.field private static final EXPIRY_TIME:I = 0xe10

.field private static final HOLD_CALL:Ljava/util/EventObject; = null

.field private static final INCALL_KEEPALIVE_INTERVAL:I = 0xa

.field private static final KEEPALIVE_TIMEOUT:I = 0x5

.field private static final TAG:Ljava/lang/String; = "SipSession"

.field private static final THREAD_POOL_SIZE:Ljava/lang/String; = "1"

.field private static final WAKE_LOCK_HOLDING_TIME:J = 0x1f4L


# instance fields
.field private mCallReceiverSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

.field private mExternalIp:Ljava/lang/String;

.field private mExternalPort:I

.field private mLocalIp:Ljava/lang/String;

.field private final mLocalProfile:Landroid/net/sip/SipProfile;

.field private final mPassword:Ljava/lang/String;

.field private mSessionMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;",
            ">;"
        }
    .end annotation
.end field

.field private mSipHelper:Lcom/android/server/sip/SipHelper;

.field private mSipStack:Ljavax/sip/SipStack;

.field private mWakeLock:Lcom/android/server/sip/SipWakeLock;

.field private mWakeupTimer:Lcom/android/server/sip/SipWakeupTimer;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 106
    new-instance v0, Ljava/util/EventObject;

    #@2
    const-string v1, "Deregister"

    #@4
    invoke-direct {v0, v1}, Ljava/util/EventObject;-><init>(Ljava/lang/Object;)V

    #@7
    sput-object v0, Lcom/android/server/sip/SipSessionGroup;->DEREGISTER:Ljava/util/EventObject;

    #@9
    .line 107
    new-instance v0, Ljava/util/EventObject;

    #@b
    const-string v1, "End call"

    #@d
    invoke-direct {v0, v1}, Ljava/util/EventObject;-><init>(Ljava/lang/Object;)V

    #@10
    sput-object v0, Lcom/android/server/sip/SipSessionGroup;->END_CALL:Ljava/util/EventObject;

    #@12
    .line 108
    new-instance v0, Ljava/util/EventObject;

    #@14
    const-string v1, "Hold call"

    #@16
    invoke-direct {v0, v1}, Ljava/util/EventObject;-><init>(Ljava/lang/Object;)V

    #@19
    sput-object v0, Lcom/android/server/sip/SipSessionGroup;->HOLD_CALL:Ljava/util/EventObject;

    #@1b
    .line 109
    new-instance v0, Ljava/util/EventObject;

    #@1d
    const-string v1, "Continue call"

    #@1f
    invoke-direct {v0, v1}, Ljava/util/EventObject;-><init>(Ljava/lang/Object;)V

    #@22
    sput-object v0, Lcom/android/server/sip/SipSessionGroup;->CONTINUE_CALL:Ljava/util/EventObject;

    #@24
    return-void
.end method

.method public constructor <init>(Landroid/net/sip/SipProfile;Ljava/lang/String;Lcom/android/server/sip/SipWakeupTimer;Lcom/android/server/sip/SipWakeLock;)V
    .registers 6
    .parameter "profile"
    .parameter "password"
    .parameter "timer"
    .parameter "wakeLock"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 139
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 126
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/sip/SipSessionGroup;->mSessionMap:Ljava/util/Map;

    #@a
    .line 140
    iput-object p1, p0, Lcom/android/server/sip/SipSessionGroup;->mLocalProfile:Landroid/net/sip/SipProfile;

    #@c
    .line 141
    iput-object p2, p0, Lcom/android/server/sip/SipSessionGroup;->mPassword:Ljava/lang/String;

    #@e
    .line 142
    iput-object p3, p0, Lcom/android/server/sip/SipSessionGroup;->mWakeupTimer:Lcom/android/server/sip/SipWakeupTimer;

    #@10
    .line 143
    iput-object p4, p0, Lcom/android/server/sip/SipSessionGroup;->mWakeLock:Lcom/android/server/sip/SipWakeLock;

    #@12
    .line 144
    invoke-virtual {p0}, Lcom/android/server/sip/SipSessionGroup;->reset()V

    #@15
    .line 145
    return-void
.end method

.method static synthetic access$1200(Lcom/android/server/sip/SipSessionGroup;Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup;->removeSipSession(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;)V

    #@3
    return-void
.end method

.method static synthetic access$1300(Lcom/android/server/sip/SipSessionGroup;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 89
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup;->mLocalIp:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/server/sip/SipSessionGroup;)Landroid/net/sip/SipProfile;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 89
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup;->mLocalProfile:Landroid/net/sip/SipProfile;

    #@2
    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/server/sip/SipSessionGroup;Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup;->getRootCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1700()Ljava/util/EventObject;
    .registers 1

    #@0
    .prologue
    .line 89
    sget-object v0, Lcom/android/server/sip/SipSessionGroup;->END_CALL:Ljava/util/EventObject;

    #@2
    return-object v0
.end method

.method static synthetic access$1800()Ljava/util/EventObject;
    .registers 1

    #@0
    .prologue
    .line 89
    sget-object v0, Lcom/android/server/sip/SipSessionGroup;->DEREGISTER:Ljava/util/EventObject;

    #@2
    return-object v0
.end method

.method static synthetic access$1900(Ljava/util/EventObject;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 89
    invoke-static {p0}, Lcom/android/server/sip/SipSessionGroup;->isLoggable(Ljava/util/EventObject;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$200(Lcom/android/server/sip/SipSessionGroup;)Ljava/util/Map;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 89
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup;->mSessionMap:Ljava/util/Map;

    #@2
    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/server/sip/SipSessionGroup;Ljavax/sip/ResponseEvent;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup;->extractExternalAddress(Ljavax/sip/ResponseEvent;)V

    #@3
    return-void
.end method

.method static synthetic access$2100(Ljava/lang/String;Ljava/util/EventObject;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 89
    invoke-static {p0, p1}, Lcom/android/server/sip/SipSessionGroup;->expectResponse(Ljava/lang/String;Ljava/util/EventObject;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2200(Lcom/android/server/sip/SipSessionGroup;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 89
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup;->mPassword:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$2300(Lcom/android/server/sip/SipSessionGroup;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 89
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup;->mExternalIp:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$2400(Lcom/android/server/sip/SipSessionGroup;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 89
    iget v0, p0, Lcom/android/server/sip/SipSessionGroup;->mExternalPort:I

    #@2
    return v0
.end method

.method static synthetic access$2500(Lcom/android/server/sip/SipSessionGroup;Ljavax/sip/message/Message;)Ljava/lang/String;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup;->extractContent(Ljavax/sip/message/Message;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$2600(Ljavax/sip/header/HeaderAddress;)Landroid/net/sip/SipProfile;
    .registers 2
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 89
    invoke-static {p0}, Lcom/android/server/sip/SipSessionGroup;->createPeerProfile(Ljavax/sip/header/HeaderAddress;)Landroid/net/sip/SipProfile;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$2700(Lcom/android/server/sip/SipSessionGroup;)Lcom/android/server/sip/SipWakeupTimer;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 89
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup;->mWakeupTimer:Lcom/android/server/sip/SipWakeupTimer;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/server/sip/SipSessionGroup;)Lcom/android/server/sip/SipHelper;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 89
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup;->mSipHelper:Lcom/android/server/sip/SipHelper;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/server/sip/SipSessionGroup;Ljavax/sip/RequestEvent;Landroid/net/sip/ISipSessionListener;Ljavax/sip/ServerTransaction;I)Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 89
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/sip/SipSessionGroup;->createNewSession(Ljavax/sip/RequestEvent;Landroid/net/sip/ISipSessionListener;Ljavax/sip/ServerTransaction;I)Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/server/sip/SipSessionGroup;Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup;->addSipSession(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;)V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;Ljava/util/EventObject;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 89
    invoke-static {p0, p1}, Lcom/android/server/sip/SipSessionGroup;->isLoggable(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;Ljava/util/EventObject;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$700(Ljava/util/EventObject;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 89
    invoke-static {p0}, Lcom/android/server/sip/SipSessionGroup;->log(Ljava/util/EventObject;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$800(Ljava/lang/String;Ljava/util/EventObject;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 89
    invoke-static {p0, p1}, Lcom/android/server/sip/SipSessionGroup;->isRequestEvent(Ljava/lang/String;Ljava/util/EventObject;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private declared-synchronized addSipSession(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;)V
    .registers 8
    .parameter "newSession"

    #@0
    .prologue
    .line 298
    monitor-enter p0

    #@1
    :try_start_1
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup;->removeSipSession(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;)V

    #@4
    .line 299
    invoke-virtual {p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->getCallId()Ljava/lang/String;

    #@7
    move-result-object v2

    #@8
    .line 300
    .local v2, key:Ljava/lang/String;
    iget-object v3, p0, Lcom/android/server/sip/SipSessionGroup;->mSessionMap:Ljava/util/Map;

    #@a
    invoke-interface {v3, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@d
    .line 301
    invoke-static {p1}, Lcom/android/server/sip/SipSessionGroup;->isLoggable(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;)Z

    #@10
    move-result v3

    #@11
    if-eqz v3, :cond_73

    #@13
    .line 302
    const-string v3, "SipSession"

    #@15
    new-instance v4, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v5, "+++  add a session with key:  \'"

    #@1c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v4

    #@20
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v4

    #@24
    const-string v5, "\'"

    #@26
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v4

    #@2a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v4

    #@2e
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 303
    iget-object v3, p0, Lcom/android/server/sip/SipSessionGroup;->mSessionMap:Ljava/util/Map;

    #@33
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    #@36
    move-result-object v3

    #@37
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@3a
    move-result-object v0

    #@3b
    .local v0, i$:Ljava/util/Iterator;
    :goto_3b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@3e
    move-result v3

    #@3f
    if-eqz v3, :cond_73

    #@41
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@44
    move-result-object v1

    #@45
    check-cast v1, Ljava/lang/String;

    #@47
    .line 304
    .local v1, k:Ljava/lang/String;
    const-string v3, "SipSession"

    #@49
    new-instance v4, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string v5, "  "

    #@50
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v4

    #@54
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v4

    #@58
    const-string v5, ": "

    #@5a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v4

    #@5e
    iget-object v5, p0, Lcom/android/server/sip/SipSessionGroup;->mSessionMap:Ljava/util/Map;

    #@60
    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@63
    move-result-object v5

    #@64
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v4

    #@68
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v4

    #@6c
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6f
    .catchall {:try_start_1 .. :try_end_6f} :catchall_70

    #@6f
    goto :goto_3b

    #@70
    .line 298
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #k:Ljava/lang/String;
    .end local v2           #key:Ljava/lang/String;
    :catchall_70
    move-exception v3

    #@71
    monitor-exit p0

    #@72
    throw v3

    #@73
    .line 307
    .restart local v2       #key:Ljava/lang/String;
    :cond_73
    monitor-exit p0

    #@74
    return-void
.end method

.method private createNewSession(Ljavax/sip/RequestEvent;Landroid/net/sip/ISipSessionListener;Ljavax/sip/ServerTransaction;I)Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
    .registers 8
    .parameter "event"
    .parameter "listener"
    .parameter "transaction"
    .parameter "newState"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 426
    new-instance v0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@2
    invoke-direct {v0, p0, p2}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;-><init>(Lcom/android/server/sip/SipSessionGroup;Landroid/net/sip/ISipSessionListener;)V

    #@5
    .line 427
    .local v0, newSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
    iput-object p3, v0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mServerTransaction:Ljavax/sip/ServerTransaction;

    #@7
    .line 428
    iput p4, v0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mState:I

    #@9
    .line 429
    iget-object v1, v0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mServerTransaction:Ljavax/sip/ServerTransaction;

    #@b
    invoke-interface {v1}, Ljavax/sip/ServerTransaction;->getDialog()Ljavax/sip/Dialog;

    #@e
    move-result-object v1

    #@f
    iput-object v1, v0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mDialog:Ljavax/sip/Dialog;

    #@11
    .line 430
    iput-object p1, v0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mInviteReceived:Ljavax/sip/RequestEvent;

    #@13
    .line 431
    invoke-virtual {p1}, Ljavax/sip/RequestEvent;->getRequest()Ljavax/sip/message/Request;

    #@16
    move-result-object v1

    #@17
    const-string v2, "From"

    #@19
    invoke-interface {v1, v2}, Ljavax/sip/message/Request;->getHeader(Ljava/lang/String;)Ljavax/sip/header/Header;

    #@1c
    move-result-object v1

    #@1d
    check-cast v1, Ljavax/sip/header/HeaderAddress;

    #@1f
    invoke-static {v1}, Lcom/android/server/sip/SipSessionGroup;->createPeerProfile(Ljavax/sip/header/HeaderAddress;)Landroid/net/sip/SipProfile;

    #@22
    move-result-object v1

    #@23
    iput-object v1, v0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mPeerProfile:Landroid/net/sip/SipProfile;

    #@25
    .line 433
    invoke-virtual {p1}, Ljavax/sip/RequestEvent;->getRequest()Ljavax/sip/message/Request;

    #@28
    move-result-object v1

    #@29
    invoke-direct {p0, v1}, Lcom/android/server/sip/SipSessionGroup;->extractContent(Ljavax/sip/message/Message;)Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    iput-object v1, v0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mPeerSessionDescription:Ljava/lang/String;

    #@2f
    .line 435
    return-object v0
.end method

.method private static createPeerProfile(Ljavax/sip/header/HeaderAddress;)Landroid/net/sip/SipProfile;
    .registers 9
    .parameter "header"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 1692
    :try_start_0
    invoke-interface {p0}, Ljavax/sip/header/HeaderAddress;->getAddress()Ljavax/sip/address/Address;

    #@3
    move-result-object v0

    #@4
    .line 1693
    .local v0, address:Ljavax/sip/address/Address;
    invoke-interface {v0}, Ljavax/sip/address/Address;->getURI()Ljavax/sip/address/URI;

    #@7
    move-result-object v4

    #@8
    check-cast v4, Ljavax/sip/address/SipURI;

    #@a
    .line 1694
    .local v4, uri:Ljavax/sip/address/SipURI;
    invoke-interface {v4}, Ljavax/sip/address/SipURI;->getUser()Ljava/lang/String;

    #@d
    move-result-object v5

    #@e
    .line 1695
    .local v5, username:Ljava/lang/String;
    if-nez v5, :cond_12

    #@10
    const-string v5, "anonymous"

    #@12
    .line 1696
    :cond_12
    invoke-interface {v4}, Ljavax/sip/address/SipURI;->getPort()I

    #@15
    move-result v3

    #@16
    .line 1697
    .local v3, port:I
    new-instance v6, Landroid/net/sip/SipProfile$Builder;

    #@18
    invoke-interface {v4}, Ljavax/sip/address/SipURI;->getHost()Ljava/lang/String;

    #@1b
    move-result-object v7

    #@1c
    invoke-direct {v6, v5, v7}, Landroid/net/sip/SipProfile$Builder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@1f
    invoke-interface {v0}, Ljavax/sip/address/Address;->getDisplayName()Ljava/lang/String;

    #@22
    move-result-object v7

    #@23
    invoke-virtual {v6, v7}, Landroid/net/sip/SipProfile$Builder;->setDisplayName(Ljava/lang/String;)Landroid/net/sip/SipProfile$Builder;

    #@26
    move-result-object v1

    #@27
    .line 1700
    .local v1, builder:Landroid/net/sip/SipProfile$Builder;
    if-lez v3, :cond_2c

    #@29
    invoke-virtual {v1, v3}, Landroid/net/sip/SipProfile$Builder;->setPort(I)Landroid/net/sip/SipProfile$Builder;

    #@2c
    .line 1701
    :cond_2c
    invoke-virtual {v1}, Landroid/net/sip/SipProfile$Builder;->build()Landroid/net/sip/SipProfile;
    :try_end_2f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_2f} :catch_31
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_2f} :catch_3a

    #@2f
    move-result-object v6

    #@30
    return-object v6

    #@31
    .line 1702
    .end local v0           #address:Ljavax/sip/address/Address;
    .end local v1           #builder:Landroid/net/sip/SipProfile$Builder;
    .end local v3           #port:I
    .end local v4           #uri:Ljavax/sip/address/SipURI;
    .end local v5           #username:Ljava/lang/String;
    :catch_31
    move-exception v2

    #@32
    .line 1703
    .local v2, e:Ljava/lang/IllegalArgumentException;
    new-instance v6, Ljavax/sip/SipException;

    #@34
    const-string v7, "createPeerProfile()"

    #@36
    invoke-direct {v6, v7, v2}, Ljavax/sip/SipException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@39
    throw v6

    #@3a
    .line 1704
    .end local v2           #e:Ljava/lang/IllegalArgumentException;
    :catch_3a
    move-exception v2

    #@3b
    .line 1705
    .local v2, e:Ljava/text/ParseException;
    new-instance v6, Ljavax/sip/SipException;

    #@3d
    const-string v7, "createPeerProfile()"

    #@3f
    invoke-direct {v6, v7, v2}, Ljavax/sip/SipException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@42
    throw v6
.end method

.method private static expectResponse(ILjava/lang/String;Ljava/util/EventObject;)Z
    .registers 6
    .parameter "responseCode"
    .parameter "expectedMethod"
    .parameter "evt"

    #@0
    .prologue
    .line 1679
    instance-of v2, p2, Ljavax/sip/ResponseEvent;

    #@2
    if-eqz v2, :cond_1a

    #@4
    move-object v0, p2

    #@5
    .line 1680
    check-cast v0, Ljavax/sip/ResponseEvent;

    #@7
    .line 1681
    .local v0, event:Ljavax/sip/ResponseEvent;
    invoke-virtual {v0}, Ljavax/sip/ResponseEvent;->getResponse()Ljavax/sip/message/Response;

    #@a
    move-result-object v1

    #@b
    .line 1682
    .local v1, response:Ljavax/sip/message/Response;
    invoke-interface {v1}, Ljavax/sip/message/Response;->getStatusCode()I

    #@e
    move-result v2

    #@f
    if-ne v2, p0, :cond_1a

    #@11
    .line 1683
    invoke-static {v1}, Lcom/android/server/sip/SipSessionGroup;->getCseqMethod(Ljavax/sip/message/Message;)Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@18
    move-result v2

    #@19
    .line 1686
    .end local v0           #event:Ljavax/sip/ResponseEvent;
    .end local v1           #response:Ljavax/sip/message/Response;
    :goto_19
    return v2

    #@1a
    :cond_1a
    const/4 v2, 0x0

    #@1b
    goto :goto_19
.end method

.method private static expectResponse(Ljava/lang/String;Ljava/util/EventObject;)Z
    .registers 5
    .parameter "expectedMethod"
    .parameter "evt"

    #@0
    .prologue
    .line 1665
    instance-of v2, p1, Ljavax/sip/ResponseEvent;

    #@2
    if-eqz v2, :cond_14

    #@4
    move-object v0, p1

    #@5
    .line 1666
    check-cast v0, Ljavax/sip/ResponseEvent;

    #@7
    .line 1667
    .local v0, event:Ljavax/sip/ResponseEvent;
    invoke-virtual {v0}, Ljavax/sip/ResponseEvent;->getResponse()Ljavax/sip/message/Response;

    #@a
    move-result-object v1

    #@b
    .line 1668
    .local v1, response:Ljavax/sip/message/Response;
    invoke-static {v1}, Lcom/android/server/sip/SipSessionGroup;->getCseqMethod(Ljavax/sip/message/Message;)Ljava/lang/String;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@12
    move-result v2

    #@13
    .line 1670
    .end local v0           #event:Ljavax/sip/ResponseEvent;
    .end local v1           #response:Ljavax/sip/message/Response;
    :goto_13
    return v2

    #@14
    :cond_14
    const/4 v2, 0x0

    #@15
    goto :goto_13
.end method

.method private extractContent(Ljavax/sip/message/Message;)Ljava/lang/String;
    .registers 5
    .parameter "message"

    #@0
    .prologue
    .line 383
    invoke-interface {p1}, Ljavax/sip/message/Message;->getRawContent()[B

    #@3
    move-result-object v0

    #@4
    .line 384
    .local v0, bytes:[B
    if-eqz v0, :cond_1a

    #@6
    .line 386
    :try_start_6
    instance-of v1, p1, Lgov/nist/javax/sip/message/SIPMessage;

    #@8
    if-eqz v1, :cond_11

    #@a
    .line 387
    check-cast p1, Lgov/nist/javax/sip/message/SIPMessage;

    #@c
    .end local p1
    invoke-virtual {p1}, Lgov/nist/javax/sip/message/SIPMessage;->getMessageContent()Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    .line 394
    :goto_10
    return-object v1

    #@11
    .line 389
    .restart local p1
    :cond_11
    new-instance v1, Ljava/lang/String;

    #@13
    const-string v2, "UTF-8"

    #@15
    invoke-direct {v1, v0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_18
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_6 .. :try_end_18} :catch_19

    #@18
    goto :goto_10

    #@19
    .line 391
    .end local p1
    :catch_19
    move-exception v1

    #@1a
    .line 394
    :cond_1a
    const/4 v1, 0x0

    #@1b
    goto :goto_10
.end method

.method private extractExternalAddress(Ljavax/sip/ResponseEvent;)V
    .registers 7
    .parameter "evt"

    #@0
    .prologue
    .line 398
    invoke-virtual {p1}, Ljavax/sip/ResponseEvent;->getResponse()Ljavax/sip/message/Response;

    #@3
    move-result-object v1

    #@4
    .line 399
    .local v1, response:Ljavax/sip/message/Response;
    const-string v4, "Via"

    #@6
    invoke-interface {v1, v4}, Ljavax/sip/message/Response;->getHeader(Ljava/lang/String;)Ljavax/sip/header/Header;

    #@9
    move-result-object v4

    #@a
    check-cast v4, Ljavax/sip/header/ViaHeader;

    #@c
    move-object v3, v4

    #@d
    check-cast v3, Ljavax/sip/header/ViaHeader;

    #@f
    .line 401
    .local v3, viaHeader:Ljavax/sip/header/ViaHeader;
    if-nez v3, :cond_12

    #@11
    .line 412
    :cond_11
    :goto_11
    return-void

    #@12
    .line 402
    :cond_12
    invoke-interface {v3}, Ljavax/sip/header/ViaHeader;->getRPort()I

    #@15
    move-result v2

    #@16
    .line 403
    .local v2, rport:I
    invoke-interface {v3}, Ljavax/sip/header/ViaHeader;->getReceived()Ljava/lang/String;

    #@19
    move-result-object v0

    #@1a
    .line 404
    .local v0, externalIp:Ljava/lang/String;
    if-lez v2, :cond_11

    #@1c
    if-eqz v0, :cond_11

    #@1e
    .line 405
    iput-object v0, p0, Lcom/android/server/sip/SipSessionGroup;->mExternalIp:Ljava/lang/String;

    #@20
    .line 406
    iput v2, p0, Lcom/android/server/sip/SipSessionGroup;->mExternalPort:I

    #@22
    goto :goto_11
.end method

.method private static getCseqMethod(Ljavax/sip/message/Message;)Ljava/lang/String;
    .registers 2
    .parameter "message"

    #@0
    .prologue
    .line 1656
    const-string v0, "CSeq"

    #@2
    invoke-interface {p0, v0}, Ljavax/sip/message/Message;->getHeader(Ljava/lang/String;)Ljavax/sip/header/Header;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljavax/sip/header/CSeqHeader;

    #@8
    invoke-interface {v0}, Ljavax/sip/header/CSeqHeader;->getMethod()Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method private getRootCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .registers 3
    .parameter "exception"

    #@0
    .prologue
    .line 415
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    #@3
    move-result-object v0

    #@4
    .line 416
    .local v0, cause:Ljava/lang/Throwable;
    :goto_4
    if-eqz v0, :cond_c

    #@6
    .line 417
    move-object p1, v0

    #@7
    .line 418
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    #@a
    move-result-object v0

    #@b
    goto :goto_4

    #@c
    .line 420
    :cond_c
    return-object p1
.end method

.method private declared-synchronized getSipSession(Ljava/util/EventObject;)Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
    .registers 9
    .parameter "event"

    #@0
    .prologue
    .line 285
    monitor-enter p0

    #@1
    :try_start_1
    invoke-static {p1}, Lcom/android/server/sip/SipHelper;->getCallId(Ljava/util/EventObject;)Ljava/lang/String;

    #@4
    move-result-object v2

    #@5
    .line 286
    .local v2, key:Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/sip/SipSessionGroup;->mSessionMap:Ljava/util/Map;

    #@7
    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    move-result-object v3

    #@b
    check-cast v3, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@d
    .line 287
    .local v3, session:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
    if-eqz v3, :cond_77

    #@f
    invoke-static {v3}, Lcom/android/server/sip/SipSessionGroup;->isLoggable(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;)Z

    #@12
    move-result v4

    #@13
    if-eqz v4, :cond_77

    #@15
    .line 288
    const-string v4, "SipSession"

    #@17
    new-instance v5, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string/jumbo v6, "session key from event: "

    #@1f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v5

    #@23
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v5

    #@27
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v5

    #@2b
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 289
    const-string v4, "SipSession"

    #@30
    const-string v5, "active sessions:"

    #@32
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 290
    iget-object v4, p0, Lcom/android/server/sip/SipSessionGroup;->mSessionMap:Ljava/util/Map;

    #@37
    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    #@3a
    move-result-object v4

    #@3b
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@3e
    move-result-object v0

    #@3f
    .local v0, i$:Ljava/util/Iterator;
    :goto_3f
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@42
    move-result v4

    #@43
    if-eqz v4, :cond_77

    #@45
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@48
    move-result-object v1

    #@49
    check-cast v1, Ljava/lang/String;

    #@4b
    .line 291
    .local v1, k:Ljava/lang/String;
    const-string v4, "SipSession"

    #@4d
    new-instance v5, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v6, " ..."

    #@54
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v5

    #@58
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v5

    #@5c
    const-string v6, ": "

    #@5e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v5

    #@62
    iget-object v6, p0, Lcom/android/server/sip/SipSessionGroup;->mSessionMap:Ljava/util/Map;

    #@64
    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@67
    move-result-object v6

    #@68
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v5

    #@6c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v5

    #@70
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_73
    .catchall {:try_start_1 .. :try_end_73} :catchall_74

    #@73
    goto :goto_3f

    #@74
    .line 285
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #k:Ljava/lang/String;
    .end local v2           #key:Ljava/lang/String;
    .end local v3           #session:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
    :catchall_74
    move-exception v4

    #@75
    monitor-exit p0

    #@76
    throw v4

    #@77
    .line 294
    .restart local v2       #key:Ljava/lang/String;
    .restart local v3       #session:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
    :cond_77
    if-eqz v3, :cond_7b

    #@79
    .end local v3           #session:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
    :goto_79
    monitor-exit p0

    #@7a
    return-object v3

    #@7b
    .restart local v3       #session:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
    :cond_7b
    :try_start_7b
    iget-object v3, p0, Lcom/android/server/sip/SipSessionGroup;->mCallReceiverSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
    :try_end_7d
    .catchall {:try_start_7b .. :try_end_7d} :catchall_74

    #@7d
    goto :goto_79
.end method

.method private getStackName()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 243
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string/jumbo v1, "stack"

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v0

    #@c
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@f
    move-result-wide v1

    #@10
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@13
    move-result-object v0

    #@14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v0

    #@18
    return-object v0
.end method

.method private static isLoggable(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;)Z
    .registers 3
    .parameter "s"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1710
    if-eqz p0, :cond_8

    #@3
    .line 1711
    iget v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mState:I

    #@5
    packed-switch v0, :pswitch_data_a

    #@8
    .line 1716
    :cond_8
    :pswitch_8
    return v1

    #@9
    .line 1711
    nop

    #@a
    :pswitch_data_a
    .packed-switch 0x9
        :pswitch_8
    .end packed-switch
.end method

.method private static isLoggable(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;Ljava/util/EventObject;)Z
    .registers 6
    .parameter "s"
    .parameter "evt"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1724
    invoke-static {p0}, Lcom/android/server/sip/SipSessionGroup;->isLoggable(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;)Z

    #@4
    move-result v1

    #@5
    if-nez v1, :cond_8

    #@7
    .line 1739
    .end local p1
    :cond_7
    :goto_7
    return v3

    #@8
    .line 1725
    .restart local p1
    :cond_8
    if-eqz p1, :cond_7

    #@a
    .line 1727
    instance-of v1, p1, Ljavax/sip/ResponseEvent;

    #@c
    if-eqz v1, :cond_23

    #@e
    .line 1728
    check-cast p1, Ljavax/sip/ResponseEvent;

    #@10
    .end local p1
    invoke-virtual {p1}, Ljavax/sip/ResponseEvent;->getResponse()Ljavax/sip/message/Response;

    #@13
    move-result-object v0

    #@14
    .line 1729
    .local v0, response:Ljavax/sip/message/Response;
    const-string v1, "OPTIONS"

    #@16
    const-string v2, "CSeq"

    #@18
    invoke-interface {v0, v2}, Ljavax/sip/message/Response;->getHeader(Ljava/lang/String;)Ljavax/sip/header/Header;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v1

    #@20
    if-eqz v1, :cond_7

    #@22
    goto :goto_7

    #@23
    .line 1733
    .end local v0           #response:Ljavax/sip/message/Response;
    .restart local p1
    :cond_23
    instance-of v1, p1, Ljavax/sip/RequestEvent;

    #@25
    if-eqz v1, :cond_7

    #@27
    .line 1734
    const-string v1, "OPTIONS"

    #@29
    invoke-static {v1, p1}, Lcom/android/server/sip/SipSessionGroup;->isRequestEvent(Ljava/lang/String;Ljava/util/EventObject;)Z

    #@2c
    move-result v1

    #@2d
    if-eqz v1, :cond_7

    #@2f
    goto :goto_7
.end method

.method private static isLoggable(Ljava/util/EventObject;)Z
    .registers 2
    .parameter "evt"

    #@0
    .prologue
    .line 1720
    const/4 v0, 0x0

    #@1
    invoke-static {v0, p0}, Lcom/android/server/sip/SipSessionGroup;->isLoggable(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;Ljava/util/EventObject;)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method private static isRequestEvent(Ljava/lang/String;Ljava/util/EventObject;)Z
    .registers 5
    .parameter "method"
    .parameter "event"

    #@0
    .prologue
    .line 1646
    :try_start_0
    instance-of v2, p1, Ljavax/sip/RequestEvent;

    #@2
    if-eqz v2, :cond_16

    #@4
    .line 1647
    move-object v0, p1

    #@5
    check-cast v0, Ljavax/sip/RequestEvent;

    #@7
    move-object v1, v0

    #@8
    .line 1648
    .local v1, requestEvent:Ljavax/sip/RequestEvent;
    invoke-virtual {v1}, Ljavax/sip/RequestEvent;->getRequest()Ljavax/sip/message/Request;

    #@b
    move-result-object v2

    #@c
    invoke-interface {v2}, Ljavax/sip/message/Request;->getMethod()Ljava/lang/String;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_13
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_13} :catch_15

    #@13
    move-result v2

    #@14
    .line 1652
    .end local v1           #requestEvent:Ljavax/sip/RequestEvent;
    :goto_14
    return v2

    #@15
    .line 1650
    :catch_15
    move-exception v2

    #@16
    .line 1652
    :cond_16
    const/4 v2, 0x0

    #@17
    goto :goto_14
.end method

.method private static log(Ljava/util/EventObject;)Ljava/lang/String;
    .registers 2
    .parameter "evt"

    #@0
    .prologue
    .line 1743
    instance-of v0, p0, Ljavax/sip/RequestEvent;

    #@2
    if-eqz v0, :cond_f

    #@4
    .line 1744
    check-cast p0, Ljavax/sip/RequestEvent;

    #@6
    .end local p0
    invoke-virtual {p0}, Ljavax/sip/RequestEvent;->getRequest()Ljavax/sip/message/Request;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    .line 1748
    .restart local p0
    :goto_e
    return-object v0

    #@f
    .line 1745
    :cond_f
    instance-of v0, p0, Ljavax/sip/ResponseEvent;

    #@11
    if-eqz v0, :cond_1e

    #@13
    .line 1746
    check-cast p0, Ljavax/sip/ResponseEvent;

    #@15
    .end local p0
    invoke-virtual {p0}, Ljavax/sip/ResponseEvent;->getResponse()Ljavax/sip/message/Response;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    goto :goto_e

    #@1e
    .line 1748
    .restart local p0
    :cond_1e
    invoke-virtual {p0}, Ljava/util/EventObject;->toString()Ljava/lang/String;

    #@21
    move-result-object v0

    #@22
    goto :goto_e
.end method

.method private declared-synchronized process(Ljava/util/EventObject;)V
    .registers 9
    .parameter "event"

    #@0
    .prologue
    .line 367
    monitor-enter p0

    #@1
    :try_start_1
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup;->getSipSession(Ljava/util/EventObject;)Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_5a

    #@4
    move-result-object v3

    #@5
    .line 369
    .local v3, session:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
    :try_start_5
    invoke-static {v3, p1}, Lcom/android/server/sip/SipSessionGroup;->isLoggable(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;Ljava/util/EventObject;)Z

    #@8
    move-result v1

    #@9
    .line 370
    .local v1, isLoggable:Z
    if-eqz v3, :cond_37

    #@b
    invoke-virtual {v3, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->process(Ljava/util/EventObject;)Z

    #@e
    move-result v4

    #@f
    if-eqz v4, :cond_37

    #@11
    const/4 v2, 0x1

    #@12
    .line 371
    .local v2, processed:Z
    :goto_12
    if-eqz v1, :cond_35

    #@14
    if-eqz v2, :cond_35

    #@16
    .line 372
    const-string v4, "SipSession"

    #@18
    new-instance v5, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string/jumbo v6, "new state after: "

    #@20
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v5

    #@24
    iget v6, v3, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mState:I

    #@26
    invoke-static {v6}, Landroid/net/sip/SipSession$State;->toString(I)Ljava/lang/String;

    #@29
    move-result-object v6

    #@2a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v5

    #@2e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v5

    #@32
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_35
    .catchall {:try_start_5 .. :try_end_35} :catchall_5a
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_35} :catch_39

    #@35
    .line 379
    .end local v1           #isLoggable:Z
    .end local v2           #processed:Z
    :cond_35
    :goto_35
    monitor-exit p0

    #@36
    return-void

    #@37
    .line 370
    .restart local v1       #isLoggable:Z
    :cond_37
    const/4 v2, 0x0

    #@38
    goto :goto_12

    #@39
    .line 375
    .end local v1           #isLoggable:Z
    :catch_39
    move-exception v0

    #@3a
    .line 376
    .local v0, e:Ljava/lang/Throwable;
    :try_start_3a
    const-string v4, "SipSession"

    #@3c
    new-instance v5, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v6, "event process error: "

    #@43
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v5

    #@47
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v5

    #@4b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v5

    #@4f
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionGroup;->getRootCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    #@52
    move-result-object v6

    #@53
    invoke-static {v4, v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@56
    .line 377
    invoke-static {v3, v0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->access$100(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;Ljava/lang/Throwable;)V
    :try_end_59
    .catchall {:try_start_3a .. :try_end_59} :catchall_5a

    #@59
    goto :goto_35

    #@5a
    .line 367
    .end local v0           #e:Ljava/lang/Throwable;
    .end local v3           #session:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
    :catchall_5a
    move-exception v4

    #@5b
    monitor-exit p0

    #@5c
    throw v4
.end method

.method private declared-synchronized removeSipSession(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;)V
    .registers 10
    .parameter "session"

    #@0
    .prologue
    .line 310
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v5, p0, Lcom/android/server/sip/SipSessionGroup;->mCallReceiverSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_6d

    #@3
    if-ne p1, v5, :cond_7

    #@5
    .line 333
    :cond_5
    monitor-exit p0

    #@6
    return-void

    #@7
    .line 311
    :cond_7
    :try_start_7
    invoke-virtual {p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->getCallId()Ljava/lang/String;

    #@a
    move-result-object v3

    #@b
    .line 312
    .local v3, key:Ljava/lang/String;
    iget-object v5, p0, Lcom/android/server/sip/SipSessionGroup;->mSessionMap:Ljava/util/Map;

    #@d
    invoke-interface {v5, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@10
    move-result-object v4

    #@11
    check-cast v4, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@13
    .line 314
    .local v4, s:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
    if-eqz v4, :cond_70

    #@15
    if-eq v4, p1, :cond_70

    #@17
    .line 315
    const-string v5, "SipSession"

    #@19
    new-instance v6, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string/jumbo v7, "session "

    #@21
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v6

    #@25
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v6

    #@29
    const-string v7, " is not associated with key \'"

    #@2b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v6

    #@2f
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v6

    #@33
    const-string v7, "\'"

    #@35
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v6

    #@39
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v6

    #@3d
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 317
    iget-object v5, p0, Lcom/android/server/sip/SipSessionGroup;->mSessionMap:Ljava/util/Map;

    #@42
    invoke-interface {v5, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@45
    .line 319
    iget-object v5, p0, Lcom/android/server/sip/SipSessionGroup;->mSessionMap:Ljava/util/Map;

    #@47
    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@4a
    move-result-object v5

    #@4b
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@4e
    move-result-object v1

    #@4f
    .local v1, i$:Ljava/util/Iterator;
    :cond_4f
    :goto_4f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@52
    move-result v5

    #@53
    if-eqz v5, :cond_70

    #@55
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@58
    move-result-object v0

    #@59
    check-cast v0, Ljava/util/Map$Entry;

    #@5b
    .line 320
    .local v0, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@5e
    move-result-object v5

    #@5f
    if-ne v5, v4, :cond_4f

    #@61
    .line 321
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@64
    move-result-object v3

    #@65
    .end local v3           #key:Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    #@67
    .line 322
    .restart local v3       #key:Ljava/lang/String;
    iget-object v5, p0, Lcom/android/server/sip/SipSessionGroup;->mSessionMap:Ljava/util/Map;

    #@69
    invoke-interface {v5, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_6c
    .catchall {:try_start_7 .. :try_end_6c} :catchall_6d

    #@6c
    goto :goto_4f

    #@6d
    .line 310
    .end local v0           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;>;"
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v3           #key:Ljava/lang/String;
    .end local v4           #s:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
    :catchall_6d
    move-exception v5

    #@6e
    monitor-exit p0

    #@6f
    throw v5

    #@70
    .line 327
    .restart local v3       #key:Ljava/lang/String;
    .restart local v4       #s:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
    :cond_70
    if-eqz v4, :cond_5

    #@72
    :try_start_72
    invoke-static {v4}, Lcom/android/server/sip/SipSessionGroup;->isLoggable(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;)Z

    #@75
    move-result v5

    #@76
    if-eqz v5, :cond_5

    #@78
    .line 328
    const-string v5, "SipSession"

    #@7a
    new-instance v6, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    const-string/jumbo v7, "remove session "

    #@82
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v6

    #@86
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v6

    #@8a
    const-string v7, " @key \'"

    #@8c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v6

    #@90
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v6

    #@94
    const-string v7, "\'"

    #@96
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v6

    #@9a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9d
    move-result-object v6

    #@9e
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a1
    .line 329
    iget-object v5, p0, Lcom/android/server/sip/SipSessionGroup;->mSessionMap:Ljava/util/Map;

    #@a3
    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    #@a6
    move-result-object v5

    #@a7
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@aa
    move-result-object v1

    #@ab
    .restart local v1       #i$:Ljava/util/Iterator;
    :goto_ab
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@ae
    move-result v5

    #@af
    if-eqz v5, :cond_5

    #@b1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@b4
    move-result-object v2

    #@b5
    check-cast v2, Ljava/lang/String;

    #@b7
    .line 330
    .local v2, k:Ljava/lang/String;
    const-string v5, "SipSession"

    #@b9
    new-instance v6, Ljava/lang/StringBuilder;

    #@bb
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@be
    const-string v7, "  "

    #@c0
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v6

    #@c4
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v6

    #@c8
    const-string v7, ": "

    #@ca
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v6

    #@ce
    iget-object v7, p0, Lcom/android/server/sip/SipSessionGroup;->mSessionMap:Ljava/util/Map;

    #@d0
    invoke-interface {v7, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@d3
    move-result-object v7

    #@d4
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v6

    #@d8
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@db
    move-result-object v6

    #@dc
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_df
    .catchall {:try_start_72 .. :try_end_df} :catchall_6d

    #@df
    goto :goto_ab
.end method


# virtual methods
.method public declared-synchronized close()V
    .registers 4

    #@0
    .prologue
    .line 247
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "SipSession"

    #@3
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, " close stack for "

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    iget-object v2, p0, Lcom/android/server/sip/SipSessionGroup;->mLocalProfile:Landroid/net/sip/SipProfile;

    #@10
    invoke-virtual {v2}, Landroid/net/sip/SipProfile;->getUriString()Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 248
    invoke-virtual {p0}, Lcom/android/server/sip/SipSessionGroup;->onConnectivityChanged()V

    #@22
    .line 249
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup;->mSessionMap:Ljava/util/Map;

    #@24
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    #@27
    .line 250
    invoke-virtual {p0}, Lcom/android/server/sip/SipSessionGroup;->closeToNotReceiveCalls()V

    #@2a
    .line 251
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup;->mSipStack:Ljavax/sip/SipStack;

    #@2c
    if-eqz v0, :cond_39

    #@2e
    .line 252
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup;->mSipStack:Ljavax/sip/SipStack;

    #@30
    invoke-interface {v0}, Ljavax/sip/SipStack;->stop()V

    #@33
    .line 253
    const/4 v0, 0x0

    #@34
    iput-object v0, p0, Lcom/android/server/sip/SipSessionGroup;->mSipStack:Ljavax/sip/SipStack;

    #@36
    .line 254
    const/4 v0, 0x0

    #@37
    iput-object v0, p0, Lcom/android/server/sip/SipSessionGroup;->mSipHelper:Lcom/android/server/sip/SipHelper;

    #@39
    .line 256
    :cond_39
    invoke-virtual {p0}, Lcom/android/server/sip/SipSessionGroup;->resetExternalAddress()V
    :try_end_3c
    .catchall {:try_start_1 .. :try_end_3c} :catchall_3e

    #@3c
    .line 257
    monitor-exit p0

    #@3d
    return-void

    #@3e
    .line 247
    :catchall_3e
    move-exception v0

    #@3f
    monitor-exit p0

    #@40
    throw v0
.end method

.method public declared-synchronized closeToNotReceiveCalls()V
    .registers 2

    #@0
    .prologue
    .line 273
    monitor-enter p0

    #@1
    const/4 v0, 0x0

    #@2
    :try_start_2
    iput-object v0, p0, Lcom/android/server/sip/SipSessionGroup;->mCallReceiverSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
    :try_end_4
    .catchall {:try_start_2 .. :try_end_4} :catchall_6

    #@4
    .line 274
    monitor-exit p0

    #@5
    return-void

    #@6
    .line 273
    :catchall_6
    move-exception v0

    #@7
    monitor-exit p0

    #@8
    throw v0
.end method

.method declared-synchronized containsSession(Ljava/lang/String;)Z
    .registers 3
    .parameter "callId"

    #@0
    .prologue
    .line 281
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup;->mSessionMap:Ljava/util/Map;

    #@3
    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_9

    #@6
    move-result v0

    #@7
    monitor-exit p0

    #@8
    return v0

    #@9
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method public createSession(Landroid/net/sip/ISipSessionListener;)Landroid/net/sip/ISipSession;
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 277
    invoke-virtual {p0}, Lcom/android/server/sip/SipSessionGroup;->isClosed()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x0

    #@7
    :goto_7
    return-object v0

    #@8
    :cond_8
    new-instance v0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@a
    invoke-direct {v0, p0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;-><init>(Lcom/android/server/sip/SipSessionGroup;Landroid/net/sip/ISipSessionListener;)V

    #@d
    goto :goto_7
.end method

.method public getLocalProfile()Landroid/net/sip/SipProfile;
    .registers 2

    #@0
    .prologue
    .line 235
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup;->mLocalProfile:Landroid/net/sip/SipProfile;

    #@2
    return-object v0
.end method

.method public getLocalProfileUri()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 239
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup;->mLocalProfile:Landroid/net/sip/SipProfile;

    #@2
    invoke-virtual {v0}, Landroid/net/sip/SipProfile;->getUriString()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public declared-synchronized isClosed()Z
    .registers 2

    #@0
    .prologue
    .line 260
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup;->mSipStack:Ljavax/sip/SipStack;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_a

    #@3
    if-nez v0, :cond_8

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    monitor-exit p0

    #@7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_6

    #@a
    :catchall_a
    move-exception v0

    #@b
    monitor-exit p0

    #@c
    throw v0
.end method

.method declared-synchronized onConnectivityChanged()V
    .registers 8

    #@0
    .prologue
    .line 214
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v5, p0, Lcom/android/server/sip/SipSessionGroup;->mSessionMap:Ljava/util/Map;

    #@3
    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    #@6
    move-result-object v5

    #@7
    iget-object v6, p0, Lcom/android/server/sip/SipSessionGroup;->mSessionMap:Ljava/util/Map;

    #@9
    invoke-interface {v6}, Ljava/util/Map;->size()I

    #@c
    move-result v6

    #@d
    new-array v6, v6, [Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@f
    invoke-interface {v5, v6}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@12
    move-result-object v4

    #@13
    check-cast v4, [Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@15
    .line 220
    .local v4, ss:[Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
    move-object v0, v4

    #@16
    .local v0, arr$:[Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
    array-length v2, v0

    #@17
    .local v2, len$:I
    const/4 v1, 0x0

    #@18
    .local v1, i$:I
    :goto_18
    if-ge v1, v2, :cond_26

    #@1a
    aget-object v3, v0, v1

    #@1c
    .line 221
    .local v3, s:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
    const/16 v5, -0xa

    #@1e
    const-string v6, "data connection lost"

    #@20
    invoke-static {v3, v5, v6}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->access$000(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;ILjava/lang/String;)V
    :try_end_23
    .catchall {:try_start_1 .. :try_end_23} :catchall_28

    #@23
    .line 220
    add-int/lit8 v1, v1, 0x1

    #@25
    goto :goto_18

    #@26
    .line 224
    .end local v3           #s:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
    :cond_26
    monitor-exit p0

    #@27
    return-void

    #@28
    .line 214
    .end local v0           #arr$:[Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
    .end local v1           #i$:I
    .end local v2           #len$:I
    .end local v4           #ss:[Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
    :catchall_28
    move-exception v5

    #@29
    monitor-exit p0

    #@2a
    throw v5
.end method

.method public declared-synchronized openToReceiveCalls(Landroid/net/sip/ISipSessionListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 265
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup;->mCallReceiverSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@3
    if-nez v0, :cond_e

    #@5
    .line 266
    new-instance v0, Lcom/android/server/sip/SipSessionGroup$SipSessionCallReceiverImpl;

    #@7
    invoke-direct {v0, p0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionCallReceiverImpl;-><init>(Lcom/android/server/sip/SipSessionGroup;Landroid/net/sip/ISipSessionListener;)V

    #@a
    iput-object v0, p0, Lcom/android/server/sip/SipSessionGroup;->mCallReceiverSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_14

    #@c
    .line 270
    :goto_c
    monitor-exit p0

    #@d
    return-void

    #@e
    .line 268
    :cond_e
    :try_start_e
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup;->mCallReceiverSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@10
    invoke-virtual {v0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->setListener(Landroid/net/sip/ISipSessionListener;)V
    :try_end_13
    .catchall {:try_start_e .. :try_end_13} :catchall_14

    #@13
    goto :goto_c

    #@14
    .line 265
    :catchall_14
    move-exception v0

    #@15
    monitor-exit p0

    #@16
    throw v0
.end method

.method public processDialogTerminated(Ljavax/sip/DialogTerminatedEvent;)V
    .registers 2
    .parameter "event"

    #@0
    .prologue
    .line 363
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup;->process(Ljava/util/EventObject;)V

    #@3
    .line 364
    return-void
.end method

.method public processIOException(Ljavax/sip/IOExceptionEvent;)V
    .registers 2
    .parameter "event"

    #@0
    .prologue
    .line 351
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup;->process(Ljava/util/EventObject;)V

    #@3
    .line 352
    return-void
.end method

.method public processRequest(Ljavax/sip/RequestEvent;)V
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 336
    const-string v0, "INVITE"

    #@2
    invoke-static {v0, p1}, Lcom/android/server/sip/SipSessionGroup;->isRequestEvent(Ljava/lang/String;Ljava/util/EventObject;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_f

    #@8
    .line 341
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup;->mWakeLock:Lcom/android/server/sip/SipWakeLock;

    #@a
    const-wide/16 v1, 0x1f4

    #@c
    invoke-virtual {v0, v1, v2}, Lcom/android/server/sip/SipWakeLock;->acquire(J)V

    #@f
    .line 343
    :cond_f
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup;->process(Ljava/util/EventObject;)V

    #@12
    .line 344
    return-void
.end method

.method public processResponse(Ljavax/sip/ResponseEvent;)V
    .registers 2
    .parameter "event"

    #@0
    .prologue
    .line 347
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup;->process(Ljava/util/EventObject;)V

    #@3
    .line 348
    return-void
.end method

.method public processTimeout(Ljavax/sip/TimeoutEvent;)V
    .registers 2
    .parameter "event"

    #@0
    .prologue
    .line 355
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup;->process(Ljava/util/EventObject;)V

    #@3
    .line 356
    return-void
.end method

.method public processTransactionTerminated(Ljavax/sip/TransactionTerminatedEvent;)V
    .registers 2
    .parameter "event"

    #@0
    .prologue
    .line 359
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup;->process(Ljava/util/EventObject;)V

    #@3
    .line 360
    return-void
.end method

.method declared-synchronized reset()V
    .registers 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 154
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v6, Ljava/util/Properties;

    #@3
    invoke-direct {v6}, Ljava/util/Properties;-><init>()V

    #@6
    .line 156
    .local v6, properties:Ljava/util/Properties;
    iget-object v12, p0, Lcom/android/server/sip/SipSessionGroup;->mLocalProfile:Landroid/net/sip/SipProfile;

    #@8
    invoke-virtual {v12}, Landroid/net/sip/SipProfile;->getProtocol()Ljava/lang/String;

    #@b
    move-result-object v7

    #@c
    .line 157
    .local v7, protocol:Ljava/lang/String;
    iget-object v12, p0, Lcom/android/server/sip/SipSessionGroup;->mLocalProfile:Landroid/net/sip/SipProfile;

    #@e
    invoke-virtual {v12}, Landroid/net/sip/SipProfile;->getPort()I

    #@11
    move-result v5

    #@12
    .line 158
    .local v5, port:I
    iget-object v12, p0, Lcom/android/server/sip/SipSessionGroup;->mLocalProfile:Landroid/net/sip/SipProfile;

    #@14
    invoke-virtual {v12}, Landroid/net/sip/SipProfile;->getProxyAddress()Ljava/lang/String;

    #@17
    move-result-object v10

    #@18
    .line 160
    .local v10, server:Ljava/lang/String;
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1b
    move-result v12

    #@1c
    if-nez v12, :cond_8c

    #@1e
    .line 161
    const-string/jumbo v12, "javax.sip.OUTBOUND_PROXY"

    #@21
    new-instance v13, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v13

    #@2a
    const/16 v14, 0x3a

    #@2c
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v13

    #@30
    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    move-result-object v13

    #@34
    const/16 v14, 0x2f

    #@36
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@39
    move-result-object v13

    #@3a
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v13

    #@3e
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v13

    #@42
    invoke-virtual {v6, v12, v13}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    #@45
    .line 166
    :goto_45
    const-string v12, "["

    #@47
    invoke-virtual {v10, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@4a
    move-result v12

    #@4b
    if-eqz v12, :cond_60

    #@4d
    const-string v12, "]"

    #@4f
    invoke-virtual {v10, v12}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@52
    move-result v12

    #@53
    if-eqz v12, :cond_60

    #@55
    .line 167
    const/4 v12, 0x1

    #@56
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    #@59
    move-result v13

    #@5a
    add-int/lit8 v13, v13, -0x1

    #@5c
    invoke-virtual {v10, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_5f
    .catchall {:try_start_1 .. :try_end_5f} :catchall_f5

    #@5f
    move-result-object v10

    #@60
    .line 170
    :cond_60
    const/4 v4, 0x0

    #@61
    .line 172
    .local v4, local:Ljava/lang/String;
    :try_start_61
    invoke-static {v10}, Ljava/net/InetAddress;->getAllByName(Ljava/lang/String;)[Ljava/net/InetAddress;

    #@64
    move-result-object v0

    #@65
    .local v0, arr$:[Ljava/net/InetAddress;
    array-length v3, v0

    #@66
    .local v3, len$:I
    const/4 v2, 0x0

    #@67
    .local v2, i$:I
    :goto_67
    if-ge v2, v3, :cond_88

    #@69
    aget-object v9, v0, v2

    #@6b
    .line 173
    .local v9, remote:Ljava/net/InetAddress;
    new-instance v11, Ljava/net/DatagramSocket;

    #@6d
    invoke-direct {v11}, Ljava/net/DatagramSocket;-><init>()V

    #@70
    .line 174
    .local v11, socket:Ljava/net/DatagramSocket;
    invoke-virtual {v11, v9, v5}, Ljava/net/DatagramSocket;->connect(Ljava/net/InetAddress;I)V

    #@73
    .line 175
    invoke-virtual {v11}, Ljava/net/DatagramSocket;->isConnected()Z

    #@76
    move-result v12

    #@77
    if-eqz v12, :cond_93

    #@79
    .line 176
    invoke-virtual {v11}, Ljava/net/DatagramSocket;->getLocalAddress()Ljava/net/InetAddress;

    #@7c
    move-result-object v12

    #@7d
    invoke-virtual {v12}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@80
    move-result-object v4

    #@81
    .line 177
    invoke-virtual {v11}, Ljava/net/DatagramSocket;->getLocalPort()I

    #@84
    move-result v5

    #@85
    .line 178
    invoke-virtual {v11}, Ljava/net/DatagramSocket;->close()V
    :try_end_88
    .catchall {:try_start_61 .. :try_end_88} :catchall_f5
    .catch Ljava/lang/Exception; {:try_start_61 .. :try_end_88} :catch_103

    #@88
    .line 186
    .end local v0           #arr$:[Ljava/net/InetAddress;
    .end local v2           #i$:I
    .end local v3           #len$:I
    .end local v9           #remote:Ljava/net/InetAddress;
    .end local v11           #socket:Ljava/net/DatagramSocket;
    :cond_88
    :goto_88
    if-nez v4, :cond_99

    #@8a
    .line 211
    :goto_8a
    monitor-exit p0

    #@8b
    return-void

    #@8c
    .line 164
    .end local v4           #local:Ljava/lang/String;
    :cond_8c
    :try_start_8c
    iget-object v12, p0, Lcom/android/server/sip/SipSessionGroup;->mLocalProfile:Landroid/net/sip/SipProfile;

    #@8e
    invoke-virtual {v12}, Landroid/net/sip/SipProfile;->getSipDomain()Ljava/lang/String;
    :try_end_91
    .catchall {:try_start_8c .. :try_end_91} :catchall_f5

    #@91
    move-result-object v10

    #@92
    goto :goto_45

    #@93
    .line 181
    .restart local v0       #arr$:[Ljava/net/InetAddress;
    .restart local v2       #i$:I
    .restart local v3       #len$:I
    .restart local v4       #local:Ljava/lang/String;
    .restart local v9       #remote:Ljava/net/InetAddress;
    .restart local v11       #socket:Ljava/net/DatagramSocket;
    :cond_93
    :try_start_93
    invoke-virtual {v11}, Ljava/net/DatagramSocket;->close()V
    :try_end_96
    .catchall {:try_start_93 .. :try_end_96} :catchall_f5
    .catch Ljava/lang/Exception; {:try_start_93 .. :try_end_96} :catch_103

    #@96
    .line 172
    add-int/lit8 v2, v2, 0x1

    #@98
    goto :goto_67

    #@99
    .line 191
    .end local v0           #arr$:[Ljava/net/InetAddress;
    .end local v2           #i$:I
    .end local v3           #len$:I
    .end local v9           #remote:Ljava/net/InetAddress;
    .end local v11           #socket:Ljava/net/DatagramSocket;
    :cond_99
    :try_start_99
    invoke-virtual {p0}, Lcom/android/server/sip/SipSessionGroup;->close()V

    #@9c
    .line 192
    iput-object v4, p0, Lcom/android/server/sip/SipSessionGroup;->mLocalIp:Ljava/lang/String;

    #@9e
    .line 194
    const-string/jumbo v12, "javax.sip.STACK_NAME"

    #@a1
    invoke-direct {p0}, Lcom/android/server/sip/SipSessionGroup;->getStackName()Ljava/lang/String;

    #@a4
    move-result-object v13

    #@a5
    invoke-virtual {v6, v12, v13}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    #@a8
    .line 195
    const-string v12, "gov.nist.javax.sip.THREAD_POOL_SIZE"

    #@aa
    const-string v13, "1"

    #@ac
    invoke-virtual {v6, v12, v13}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    #@af
    .line 197
    invoke-static {}, Ljavax/sip/SipFactory;->getInstance()Ljavax/sip/SipFactory;

    #@b2
    move-result-object v12

    #@b3
    invoke-virtual {v12, v6}, Ljavax/sip/SipFactory;->createSipStack(Ljava/util/Properties;)Ljavax/sip/SipStack;

    #@b6
    move-result-object v12

    #@b7
    iput-object v12, p0, Lcom/android/server/sip/SipSessionGroup;->mSipStack:Ljavax/sip/SipStack;
    :try_end_b9
    .catchall {:try_start_99 .. :try_end_b9} :catchall_f5

    #@b9
    .line 199
    :try_start_b9
    iget-object v12, p0, Lcom/android/server/sip/SipSessionGroup;->mSipStack:Ljavax/sip/SipStack;

    #@bb
    iget-object v13, p0, Lcom/android/server/sip/SipSessionGroup;->mSipStack:Ljavax/sip/SipStack;

    #@bd
    invoke-interface {v13, v4, v5, v7}, Ljavax/sip/SipStack;->createListeningPoint(Ljava/lang/String;ILjava/lang/String;)Ljavax/sip/ListeningPoint;

    #@c0
    move-result-object v13

    #@c1
    invoke-interface {v12, v13}, Ljavax/sip/SipStack;->createSipProvider(Ljavax/sip/ListeningPoint;)Ljavax/sip/SipProvider;

    #@c4
    move-result-object v8

    #@c5
    .line 201
    .local v8, provider:Ljavax/sip/SipProvider;
    invoke-interface {v8, p0}, Ljavax/sip/SipProvider;->addSipListener(Ljavax/sip/SipListener;)V

    #@c8
    .line 202
    new-instance v12, Lcom/android/server/sip/SipHelper;

    #@ca
    iget-object v13, p0, Lcom/android/server/sip/SipSessionGroup;->mSipStack:Ljavax/sip/SipStack;

    #@cc
    invoke-direct {v12, v13, v8}, Lcom/android/server/sip/SipHelper;-><init>(Ljavax/sip/SipStack;Ljavax/sip/SipProvider;)V

    #@cf
    iput-object v12, p0, Lcom/android/server/sip/SipSessionGroup;->mSipHelper:Lcom/android/server/sip/SipHelper;
    :try_end_d1
    .catchall {:try_start_b9 .. :try_end_d1} :catchall_f5
    .catch Ljavax/sip/SipException; {:try_start_b9 .. :try_end_d1} :catch_f8
    .catch Ljava/lang/Exception; {:try_start_b9 .. :try_end_d1} :catch_fa

    #@d1
    .line 209
    :try_start_d1
    const-string v12, "SipSession"

    #@d3
    new-instance v13, Ljava/lang/StringBuilder;

    #@d5
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@d8
    const-string v14, " start stack for "

    #@da
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v13

    #@de
    iget-object v14, p0, Lcom/android/server/sip/SipSessionGroup;->mLocalProfile:Landroid/net/sip/SipProfile;

    #@e0
    invoke-virtual {v14}, Landroid/net/sip/SipProfile;->getUriString()Ljava/lang/String;

    #@e3
    move-result-object v14

    #@e4
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v13

    #@e8
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@eb
    move-result-object v13

    #@ec
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ef
    .line 210
    iget-object v12, p0, Lcom/android/server/sip/SipSessionGroup;->mSipStack:Ljavax/sip/SipStack;

    #@f1
    invoke-interface {v12}, Ljavax/sip/SipStack;->start()V
    :try_end_f4
    .catchall {:try_start_d1 .. :try_end_f4} :catchall_f5

    #@f4
    goto :goto_8a

    #@f5
    .line 154
    .end local v4           #local:Ljava/lang/String;
    .end local v5           #port:I
    .end local v6           #properties:Ljava/util/Properties;
    .end local v7           #protocol:Ljava/lang/String;
    .end local v8           #provider:Ljavax/sip/SipProvider;
    .end local v10           #server:Ljava/lang/String;
    :catchall_f5
    move-exception v12

    #@f6
    monitor-exit p0

    #@f7
    throw v12

    #@f8
    .line 203
    .restart local v4       #local:Ljava/lang/String;
    .restart local v5       #port:I
    .restart local v6       #properties:Ljava/util/Properties;
    .restart local v7       #protocol:Ljava/lang/String;
    .restart local v10       #server:Ljava/lang/String;
    :catch_f8
    move-exception v1

    #@f9
    .line 204
    .local v1, e:Ljavax/sip/SipException;
    :try_start_f9
    throw v1

    #@fa
    .line 205
    .end local v1           #e:Ljavax/sip/SipException;
    :catch_fa
    move-exception v1

    #@fb
    .line 206
    .local v1, e:Ljava/lang/Exception;
    new-instance v12, Ljavax/sip/SipException;

    #@fd
    const-string v13, "failed to initialize SIP stack"

    #@ff
    invoke-direct {v12, v13, v1}, Ljavax/sip/SipException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@102
    throw v12
    :try_end_103
    .catchall {:try_start_f9 .. :try_end_103} :catchall_f5

    #@103
    .line 183
    .end local v1           #e:Ljava/lang/Exception;
    :catch_103
    move-exception v12

    #@104
    goto :goto_88
.end method

.method declared-synchronized resetExternalAddress()V
    .registers 2

    #@0
    .prologue
    .line 230
    monitor-enter p0

    #@1
    const/4 v0, 0x0

    #@2
    :try_start_2
    iput-object v0, p0, Lcom/android/server/sip/SipSessionGroup;->mExternalIp:Ljava/lang/String;

    #@4
    .line 231
    const/4 v0, 0x0

    #@5
    iput v0, p0, Lcom/android/server/sip/SipSessionGroup;->mExternalPort:I
    :try_end_7
    .catchall {:try_start_2 .. :try_end_7} :catchall_9

    #@7
    .line 232
    monitor-exit p0

    #@8
    return-void

    #@9
    .line 230
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method setWakeupTimer(Lcom/android/server/sip/SipWakeupTimer;)V
    .registers 2
    .parameter "timer"

    #@0
    .prologue
    .line 150
    iput-object p1, p0, Lcom/android/server/sip/SipSessionGroup;->mWakeupTimer:Lcom/android/server/sip/SipWakeupTimer;

    #@2
    .line 151
    return-void
.end method
