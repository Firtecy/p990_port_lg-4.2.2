.class Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$SessionTimer;
.super Ljava/lang/Object;
.source "SipSessionGroup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SessionTimer"
.end annotation


# instance fields
.field private mRunning:Z

.field final synthetic this$1:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;


# direct methods
.method constructor <init>(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 549
    iput-object p1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$SessionTimer;->this$1:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 550
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$SessionTimer;->mRunning:Z

    #@8
    return-void
.end method

.method static synthetic access$1000(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$SessionTimer;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 549
    iget-boolean v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$SessionTimer;->mRunning:Z

    #@2
    return v0
.end method

.method static synthetic access$1100(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$SessionTimer;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 549
    invoke-direct {p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$SessionTimer;->timeout()V

    #@3
    return-void
.end method

.method static synthetic access$900(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$SessionTimer;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 549
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$SessionTimer;->sleep(I)V

    #@3
    return-void
.end method

.method private declared-synchronized sleep(I)V
    .registers 5
    .parameter "timeout"

    #@0
    .prologue
    .line 574
    monitor-enter p0

    #@1
    mul-int/lit16 v1, p1, 0x3e8

    #@3
    int-to-long v1, v1

    #@4
    :try_start_4
    invoke-virtual {p0, v1, v2}, Ljava/lang/Object;->wait(J)V
    :try_end_7
    .catchall {:try_start_4 .. :try_end_7} :catchall_13
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_7} :catch_9

    #@7
    .line 578
    :goto_7
    monitor-exit p0

    #@8
    return-void

    #@9
    .line 575
    :catch_9
    move-exception v0

    #@a
    .line 576
    .local v0, e:Ljava/lang/InterruptedException;
    :try_start_a
    const-string v1, "SipSession"

    #@c
    const-string/jumbo v2, "session timer interrupted!"

    #@f
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_12
    .catchall {:try_start_a .. :try_end_12} :catchall_13

    #@12
    goto :goto_7

    #@13
    .line 574
    .end local v0           #e:Ljava/lang/InterruptedException;
    :catchall_13
    move-exception v1

    #@14
    monitor-exit p0

    #@15
    throw v1
.end method

.method private timeout()V
    .registers 5

    #@0
    .prologue
    .line 567
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$SessionTimer;->this$1:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@2
    iget-object v1, v0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@4
    monitor-enter v1

    #@5
    .line 568
    :try_start_5
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$SessionTimer;->this$1:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@7
    const/4 v2, -0x5

    #@8
    const-string v3, "Session timed out!"

    #@a
    invoke-static {v0, v2, v3}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->access$000(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;ILjava/lang/String;)V

    #@d
    .line 569
    monitor-exit v1

    #@e
    .line 570
    return-void

    #@f
    .line 569
    :catchall_f
    move-exception v0

    #@10
    monitor-exit v1
    :try_end_11
    .catchall {:try_start_5 .. :try_end_11} :catchall_f

    #@11
    throw v0
.end method


# virtual methods
.method declared-synchronized cancel()V
    .registers 2

    #@0
    .prologue
    .line 562
    monitor-enter p0

    #@1
    const/4 v0, 0x0

    #@2
    :try_start_2
    iput-boolean v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$SessionTimer;->mRunning:Z

    #@4
    .line 563
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_7
    .catchall {:try_start_2 .. :try_end_7} :catchall_9

    #@7
    .line 564
    monitor-exit p0

    #@8
    return-void

    #@9
    .line 562
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method start(I)V
    .registers 5
    .parameter "timeout"

    #@0
    .prologue
    .line 553
    new-instance v0, Ljava/lang/Thread;

    #@2
    new-instance v1, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$SessionTimer$1;

    #@4
    invoke-direct {v1, p0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$SessionTimer$1;-><init>(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$SessionTimer;I)V

    #@7
    const-string v2, "SipSessionTimerThread"

    #@9
    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    #@c
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@f
    .line 559
    return-void
.end method
