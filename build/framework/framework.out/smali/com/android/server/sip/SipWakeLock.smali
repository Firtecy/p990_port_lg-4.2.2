.class Lcom/android/server/sip/SipWakeLock;
.super Ljava/lang/Object;
.source "SipWakeLock.java"


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "SipWakeLock"


# instance fields
.field private mHolders:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mPowerManager:Landroid/os/PowerManager;

.field private mTimerWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method constructor <init>(Landroid/os/PowerManager;)V
    .registers 3
    .parameter "powerManager"

    #@0
    .prologue
    .line 32
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 30
    new-instance v0, Ljava/util/HashSet;

    #@5
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/sip/SipWakeLock;->mHolders:Ljava/util/HashSet;

    #@a
    .line 33
    iput-object p1, p0, Lcom/android/server/sip/SipWakeLock;->mPowerManager:Landroid/os/PowerManager;

    #@c
    .line 34
    return-void
.end method


# virtual methods
.method declared-synchronized acquire(J)V
    .registers 6
    .parameter "timeout"

    #@0
    .prologue
    .line 43
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/server/sip/SipWakeLock;->mTimerWakeLock:Landroid/os/PowerManager$WakeLock;

    #@3
    if-nez v0, :cond_16

    #@5
    .line 44
    iget-object v0, p0, Lcom/android/server/sip/SipWakeLock;->mPowerManager:Landroid/os/PowerManager;

    #@7
    const/4 v1, 0x1

    #@8
    const-string v2, "SipWakeLock.timer"

    #@a
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@d
    move-result-object v0

    #@e
    iput-object v0, p0, Lcom/android/server/sip/SipWakeLock;->mTimerWakeLock:Landroid/os/PowerManager$WakeLock;

    #@10
    .line 46
    iget-object v0, p0, Lcom/android/server/sip/SipWakeLock;->mTimerWakeLock:Landroid/os/PowerManager$WakeLock;

    #@12
    const/4 v1, 0x1

    #@13
    invoke-virtual {v0, v1}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    #@16
    .line 48
    :cond_16
    iget-object v0, p0, Lcom/android/server/sip/SipWakeLock;->mTimerWakeLock:Landroid/os/PowerManager$WakeLock;

    #@18
    invoke-virtual {v0, p1, p2}, Landroid/os/PowerManager$WakeLock;->acquire(J)V
    :try_end_1b
    .catchall {:try_start_1 .. :try_end_1b} :catchall_1d

    #@1b
    .line 49
    monitor-exit p0

    #@1c
    return-void

    #@1d
    .line 43
    :catchall_1d
    move-exception v0

    #@1e
    monitor-exit p0

    #@1f
    throw v0
.end method

.method declared-synchronized acquire(Ljava/lang/Object;)V
    .registers 5
    .parameter "holder"

    #@0
    .prologue
    .line 52
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/server/sip/SipWakeLock;->mHolders:Ljava/util/HashSet;

    #@3
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@6
    .line 53
    iget-object v0, p0, Lcom/android/server/sip/SipWakeLock;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@8
    if-nez v0, :cond_15

    #@a
    .line 54
    iget-object v0, p0, Lcom/android/server/sip/SipWakeLock;->mPowerManager:Landroid/os/PowerManager;

    #@c
    const/4 v1, 0x1

    #@d
    const-string v2, "SipWakeLock"

    #@f
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@12
    move-result-object v0

    #@13
    iput-object v0, p0, Lcom/android/server/sip/SipWakeLock;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@15
    .line 57
    :cond_15
    iget-object v0, p0, Lcom/android/server/sip/SipWakeLock;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@17
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@1a
    move-result v0

    #@1b
    if-nez v0, :cond_22

    #@1d
    iget-object v0, p0, Lcom/android/server/sip/SipWakeLock;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@1f
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V
    :try_end_22
    .catchall {:try_start_1 .. :try_end_22} :catchall_24

    #@22
    .line 59
    :cond_22
    monitor-exit p0

    #@23
    return-void

    #@24
    .line 52
    :catchall_24
    move-exception v0

    #@25
    monitor-exit p0

    #@26
    throw v0
.end method

.method declared-synchronized release(Ljava/lang/Object;)V
    .registers 3
    .parameter "holder"

    #@0
    .prologue
    .line 62
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/server/sip/SipWakeLock;->mHolders:Ljava/util/HashSet;

    #@3
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@6
    .line 63
    iget-object v0, p0, Lcom/android/server/sip/SipWakeLock;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@8
    if-eqz v0, :cond_1f

    #@a
    iget-object v0, p0, Lcom/android/server/sip/SipWakeLock;->mHolders:Ljava/util/HashSet;

    #@c
    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_1f

    #@12
    iget-object v0, p0, Lcom/android/server/sip/SipWakeLock;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@14
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_1f

    #@1a
    .line 65
    iget-object v0, p0, Lcom/android/server/sip/SipWakeLock;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@1c
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_1f
    .catchall {:try_start_1 .. :try_end_1f} :catchall_21

    #@1f
    .line 68
    :cond_1f
    monitor-exit p0

    #@20
    return-void

    #@21
    .line 62
    :catchall_21
    move-exception v0

    #@22
    monitor-exit p0

    #@23
    throw v0
.end method

.method declared-synchronized reset()V
    .registers 2

    #@0
    .prologue
    .line 38
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/server/sip/SipWakeLock;->mHolders:Ljava/util/HashSet;

    #@3
    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    #@6
    .line 39
    const/4 v0, 0x0

    #@7
    invoke-virtual {p0, v0}, Lcom/android/server/sip/SipWakeLock;->release(Ljava/lang/Object;)V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 40
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 38
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method
