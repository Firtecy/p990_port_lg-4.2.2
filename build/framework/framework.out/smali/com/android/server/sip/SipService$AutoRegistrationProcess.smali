.class Lcom/android/server/sip/SipService$AutoRegistrationProcess;
.super Landroid/net/sip/SipSessionAdapter;
.source "SipService.java"

# interfaces
.implements Ljava/lang/Runnable;
.implements Lcom/android/server/sip/SipSessionGroup$KeepAliveProcessCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/sip/SipService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AutoRegistrationProcess"
.end annotation


# static fields
.field private static final MIN_KEEPALIVE_SUCCESS_COUNT:I = 0xa


# instance fields
.field private TAG:Ljava/lang/String;

.field private mBackoff:I

.field private mErrorCode:I

.field private mErrorMessage:Ljava/lang/String;

.field private mExpiryTime:J

.field private mKeepAliveSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

.field private mKeepAliveSuccessCount:I

.field private mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

.field private mRegistered:Z

.field private mRunning:Z

.field private mSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

.field final synthetic this$0:Lcom/android/server/sip/SipService;


# direct methods
.method private constructor <init>(Lcom/android/server/sip/SipService;)V
    .registers 4
    .parameter

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 740
    iput-object p1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->this$0:Lcom/android/server/sip/SipService;

    #@3
    invoke-direct {p0}, Landroid/net/sip/SipSessionAdapter;-><init>()V

    #@6
    .line 743
    const-string v0, "SipAutoReg"

    #@8
    iput-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->TAG:Ljava/lang/String;

    #@a
    .line 747
    new-instance v0, Lcom/android/server/sip/SipSessionListenerProxy;

    #@c
    invoke-direct {v0}, Lcom/android/server/sip/SipSessionListenerProxy;-><init>()V

    #@f
    iput-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@11
    .line 748
    const/4 v0, 0x1

    #@12
    iput v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mBackoff:I

    #@14
    .line 753
    iput-boolean v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mRunning:Z

    #@16
    .line 755
    iput v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mKeepAliveSuccessCount:I

    #@18
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/sip/SipService;Lcom/android/server/sip/SipService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 740
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipService$AutoRegistrationProcess;-><init>(Lcom/android/server/sip/SipService;)V

    #@3
    return-void
.end method

.method private backoffDuration()I
    .registers 3

    #@0
    .prologue
    .line 951
    iget v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mBackoff:I

    #@2
    mul-int/lit8 v0, v1, 0xa

    #@4
    .line 952
    .local v0, duration:I
    const/16 v1, 0xe10

    #@6
    if-le v0, v1, :cond_b

    #@8
    .line 953
    const/16 v0, 0xe10

    #@a
    .line 957
    :goto_a
    return v0

    #@b
    .line 955
    :cond_b
    iget v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mBackoff:I

    #@d
    mul-int/lit8 v1, v1, 0x2

    #@f
    iput v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mBackoff:I

    #@11
    goto :goto_a
.end method

.method private getAction()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 758
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private notCurrentSession(Landroid/net/sip/ISipSession;)Z
    .registers 5
    .parameter "session"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 972
    iget-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@3
    if-eq p1, v0, :cond_16

    #@5
    move-object v0, p1

    #@6
    .line 973
    check-cast v0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@8
    const/4 v2, 0x0

    #@9
    invoke-virtual {v0, v2}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->setListener(Landroid/net/sip/ISipSessionListener;)V

    #@c
    .line 974
    iget-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->this$0:Lcom/android/server/sip/SipService;

    #@e
    invoke-static {v0}, Lcom/android/server/sip/SipService;->access$300(Lcom/android/server/sip/SipService;)Lcom/android/server/sip/SipWakeLock;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0, p1}, Lcom/android/server/sip/SipWakeLock;->release(Ljava/lang/Object;)V

    #@15
    .line 977
    :goto_15
    return v1

    #@16
    :cond_16
    iget-boolean v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mRunning:Z

    #@18
    if-nez v0, :cond_1d

    #@1a
    move v0, v1

    #@1b
    :goto_1b
    move v1, v0

    #@1c
    goto :goto_15

    #@1d
    :cond_1d
    const/4 v0, 0x0

    #@1e
    goto :goto_1b
.end method

.method private restart(I)V
    .registers 5
    .parameter "duration"

    #@0
    .prologue
    .line 945
    iget-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->TAG:Ljava/lang/String;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Refresh registration "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string/jumbo v2, "s later."

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 946
    iget-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->this$0:Lcom/android/server/sip/SipService;

    #@21
    invoke-static {v0}, Lcom/android/server/sip/SipService;->access$200(Lcom/android/server/sip/SipService;)Lcom/android/server/sip/SipWakeupTimer;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {v0, p0}, Lcom/android/server/sip/SipWakeupTimer;->cancel(Ljava/lang/Runnable;)V

    #@28
    .line 947
    iget-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->this$0:Lcom/android/server/sip/SipService;

    #@2a
    invoke-static {v0}, Lcom/android/server/sip/SipService;->access$200(Lcom/android/server/sip/SipService;)Lcom/android/server/sip/SipWakeupTimer;

    #@2d
    move-result-object v0

    #@2e
    mul-int/lit16 v1, p1, 0x3e8

    #@30
    invoke-virtual {v0, v1, p0}, Lcom/android/server/sip/SipWakeupTimer;->set(ILjava/lang/Runnable;)V

    #@33
    .line 948
    return-void
.end method

.method private restartLater()V
    .registers 2

    #@0
    .prologue
    .line 1056
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mRegistered:Z

    #@3
    .line 1057
    invoke-direct {p0}, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->backoffDuration()I

    #@6
    move-result v0

    #@7
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->restart(I)V

    #@a
    .line 1058
    return-void
.end method

.method private startKeepAliveProcess(I)V
    .registers 6
    .parameter "interval"

    #@0
    .prologue
    .line 781
    iget-object v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mKeepAliveSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@2
    if-nez v1, :cond_12

    #@4
    .line 782
    iget-object v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@6
    invoke-virtual {v1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->duplicate()Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@9
    move-result-object v1

    #@a
    iput-object v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mKeepAliveSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@c
    .line 787
    :goto_c
    :try_start_c
    iget-object v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mKeepAliveSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@e
    invoke-virtual {v1, p1, p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->startKeepAliveProcess(ILcom/android/server/sip/SipSessionGroup$KeepAliveProcessCallback;)V
    :try_end_11
    .catch Ljavax/sip/SipException; {:try_start_c .. :try_end_11} :catch_18

    #@11
    .line 792
    :goto_11
    return-void

    #@12
    .line 784
    :cond_12
    iget-object v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mKeepAliveSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@14
    invoke-virtual {v1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->stopKeepAliveProcess()V

    #@17
    goto :goto_c

    #@18
    .line 788
    :catch_18
    move-exception v0

    #@19
    .line 789
    .local v0, e:Ljavax/sip/SipException;
    iget-object v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->TAG:Ljava/lang/String;

    #@1b
    new-instance v2, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v3, "failed to start keepalive w interval="

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@31
    goto :goto_11
.end method

.method private stopKeepAliveProcess()V
    .registers 2

    #@0
    .prologue
    .line 795
    iget-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mKeepAliveSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 796
    iget-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mKeepAliveSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@6
    invoke-virtual {v0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->stopKeepAliveProcess()V

    #@9
    .line 797
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mKeepAliveSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@c
    .line 799
    :cond_c
    const/4 v0, 0x0

    #@d
    iput v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mKeepAliveSuccessCount:I

    #@f
    .line 800
    return-void
.end method


# virtual methods
.method public isRegistered()Z
    .registers 2

    #@0
    .prologue
    .line 925
    iget-boolean v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mRegistered:Z

    #@2
    return v0
.end method

.method public onError(ILjava/lang/String;)V
    .registers 4
    .parameter "errorCode"
    .parameter "description"

    #@0
    .prologue
    .line 853
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, v0}, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->onResponse(Z)V

    #@4
    .line 854
    return-void
.end method

.method public onKeepAliveIntervalChanged()V
    .registers 3

    #@0
    .prologue
    .line 873
    iget-object v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mKeepAliveSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@2
    if-eqz v1, :cond_10

    #@4
    .line 874
    iget-object v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->this$0:Lcom/android/server/sip/SipService;

    #@6
    invoke-static {v1}, Lcom/android/server/sip/SipService;->access$1200(Lcom/android/server/sip/SipService;)I

    #@9
    move-result v0

    #@a
    .line 878
    .local v0, newInterval:I
    const/4 v1, 0x0

    #@b
    iput v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mKeepAliveSuccessCount:I

    #@d
    .line 879
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->startKeepAliveProcess(I)V

    #@10
    .line 881
    .end local v0           #newInterval:I
    :cond_10
    return-void
.end method

.method public onRegistering(Landroid/net/sip/ISipSession;)V
    .registers 4
    .parameter "session"

    #@0
    .prologue
    .line 963
    iget-object v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->this$0:Lcom/android/server/sip/SipService;

    #@2
    monitor-enter v1

    #@3
    .line 964
    :try_start_3
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->notCurrentSession(Landroid/net/sip/ISipSession;)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_b

    #@9
    monitor-exit v1

    #@a
    .line 969
    :goto_a
    return-void

    #@b
    .line 966
    :cond_b
    const/4 v0, 0x0

    #@c
    iput-boolean v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mRegistered:Z

    #@e
    .line 967
    iget-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@10
    invoke-virtual {v0, p1}, Lcom/android/server/sip/SipSessionListenerProxy;->onRegistering(Landroid/net/sip/ISipSession;)V

    #@13
    .line 968
    monitor-exit v1

    #@14
    goto :goto_a

    #@15
    :catchall_15
    move-exception v0

    #@16
    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    #@17
    throw v0
.end method

.method public onRegistrationDone(Landroid/net/sip/ISipSession;I)V
    .registers 10
    .parameter "session"
    .parameter "duration"

    #@0
    .prologue
    .line 983
    iget-object v2, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->this$0:Lcom/android/server/sip/SipService;

    #@2
    monitor-enter v2

    #@3
    .line 984
    :try_start_3
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->notCurrentSession(Landroid/net/sip/ISipSession;)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_b

    #@9
    monitor-exit v2

    #@a
    .line 1015
    :goto_a
    return-void

    #@b
    .line 986
    :cond_b
    iget-object v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@d
    invoke-virtual {v1, p1, p2}, Lcom/android/server/sip/SipSessionListenerProxy;->onRegistrationDone(Landroid/net/sip/ISipSession;I)V

    #@10
    .line 988
    if-lez p2, :cond_63

    #@12
    .line 989
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@15
    move-result-wide v3

    #@16
    mul-int/lit16 v1, p2, 0x3e8

    #@18
    int-to-long v5, v1

    #@19
    add-long/2addr v3, v5

    #@1a
    iput-wide v3, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mExpiryTime:J

    #@1c
    .line 992
    iget-boolean v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mRegistered:Z

    #@1e
    if-nez v1, :cond_55

    #@20
    .line 993
    const/4 v1, 0x1

    #@21
    iput-boolean v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mRegistered:Z

    #@23
    .line 995
    add-int/lit8 p2, p2, -0x3c

    #@25
    .line 996
    const/16 v1, 0x3c

    #@27
    if-ge p2, v1, :cond_2b

    #@29
    .line 997
    const/16 p2, 0x3c

    #@2b
    .line 999
    :cond_2b
    invoke-direct {p0, p2}, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->restart(I)V

    #@2e
    .line 1001
    iget-object v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@30
    invoke-virtual {v1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->getLocalProfile()Landroid/net/sip/SipProfile;

    #@33
    move-result-object v0

    #@34
    .line 1002
    .local v0, localProfile:Landroid/net/sip/SipProfile;
    iget-object v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mKeepAliveSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@36
    if-nez v1, :cond_55

    #@38
    iget-object v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->this$0:Lcom/android/server/sip/SipService;

    #@3a
    iget-object v3, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->this$0:Lcom/android/server/sip/SipService;

    #@3c
    invoke-static {v3}, Lcom/android/server/sip/SipService;->access$1500(Lcom/android/server/sip/SipService;)Ljava/lang/String;

    #@3f
    move-result-object v3

    #@40
    invoke-static {v1, v3}, Lcom/android/server/sip/SipService;->access$1600(Lcom/android/server/sip/SipService;Ljava/lang/String;)Z

    #@43
    move-result v1

    #@44
    if-nez v1, :cond_4c

    #@46
    invoke-virtual {v0}, Landroid/net/sip/SipProfile;->getSendKeepAlive()Z

    #@49
    move-result v1

    #@4a
    if-eqz v1, :cond_55

    #@4c
    .line 1004
    :cond_4c
    iget-object v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->this$0:Lcom/android/server/sip/SipService;

    #@4e
    invoke-static {v1}, Lcom/android/server/sip/SipService;->access$1200(Lcom/android/server/sip/SipService;)I

    #@51
    move-result v1

    #@52
    invoke-direct {p0, v1}, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->startKeepAliveProcess(I)V

    #@55
    .line 1007
    .end local v0           #localProfile:Landroid/net/sip/SipProfile;
    :cond_55
    iget-object v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->this$0:Lcom/android/server/sip/SipService;

    #@57
    invoke-static {v1}, Lcom/android/server/sip/SipService;->access$300(Lcom/android/server/sip/SipService;)Lcom/android/server/sip/SipWakeLock;

    #@5a
    move-result-object v1

    #@5b
    invoke-virtual {v1, p1}, Lcom/android/server/sip/SipWakeLock;->release(Ljava/lang/Object;)V

    #@5e
    .line 1014
    :goto_5e
    monitor-exit v2

    #@5f
    goto :goto_a

    #@60
    :catchall_60
    move-exception v1

    #@61
    monitor-exit v2
    :try_end_62
    .catchall {:try_start_3 .. :try_end_62} :catchall_60

    #@62
    throw v1

    #@63
    .line 1009
    :cond_63
    const/4 v1, 0x0

    #@64
    :try_start_64
    iput-boolean v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mRegistered:Z

    #@66
    .line 1010
    const-wide/16 v3, -0x1

    #@68
    iput-wide v3, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mExpiryTime:J

    #@6a
    .line 1012
    invoke-virtual {p0}, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->run()V
    :try_end_6d
    .catchall {:try_start_64 .. :try_end_6d} :catchall_60

    #@6d
    goto :goto_5e
.end method

.method public onRegistrationFailed(Landroid/net/sip/ISipSession;ILjava/lang/String;)V
    .registers 6
    .parameter "session"
    .parameter "errorCode"
    .parameter "message"

    #@0
    .prologue
    .line 1022
    iget-object v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->this$0:Lcom/android/server/sip/SipService;

    #@2
    monitor-enter v1

    #@3
    .line 1023
    :try_start_3
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->notCurrentSession(Landroid/net/sip/ISipSession;)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_b

    #@9
    monitor-exit v1

    #@a
    .line 1040
    :goto_a
    return-void

    #@b
    .line 1025
    :cond_b
    sparse-switch p2, :sswitch_data_2c

    #@e
    .line 1032
    invoke-direct {p0}, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->restartLater()V

    #@11
    .line 1035
    :goto_11
    iput p2, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mErrorCode:I

    #@13
    .line 1036
    iput-object p3, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mErrorMessage:Ljava/lang/String;

    #@15
    .line 1037
    iget-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@17
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/sip/SipSessionListenerProxy;->onRegistrationFailed(Landroid/net/sip/ISipSession;ILjava/lang/String;)V

    #@1a
    .line 1038
    iget-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->this$0:Lcom/android/server/sip/SipService;

    #@1c
    invoke-static {v0}, Lcom/android/server/sip/SipService;->access$300(Lcom/android/server/sip/SipService;)Lcom/android/server/sip/SipWakeLock;

    #@1f
    move-result-object v0

    #@20
    invoke-virtual {v0, p1}, Lcom/android/server/sip/SipWakeLock;->release(Ljava/lang/Object;)V

    #@23
    .line 1039
    monitor-exit v1

    #@24
    goto :goto_a

    #@25
    :catchall_25
    move-exception v0

    #@26
    monitor-exit v1
    :try_end_27
    .catchall {:try_start_3 .. :try_end_27} :catchall_25

    #@27
    throw v0

    #@28
    .line 1029
    :sswitch_28
    :try_start_28
    invoke-virtual {p0}, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->stop()V
    :try_end_2b
    .catchall {:try_start_28 .. :try_end_2b} :catchall_25

    #@2b
    goto :goto_11

    #@2c
    .line 1025
    :sswitch_data_2c
    .sparse-switch
        -0xc -> :sswitch_28
        -0x8 -> :sswitch_28
    .end sparse-switch
.end method

.method public onRegistrationTimeout(Landroid/net/sip/ISipSession;)V
    .registers 4
    .parameter "session"

    #@0
    .prologue
    .line 1045
    iget-object v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->this$0:Lcom/android/server/sip/SipService;

    #@2
    monitor-enter v1

    #@3
    .line 1046
    :try_start_3
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->notCurrentSession(Landroid/net/sip/ISipSession;)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_b

    #@9
    monitor-exit v1

    #@a
    .line 1053
    :goto_a
    return-void

    #@b
    .line 1048
    :cond_b
    const/4 v0, -0x5

    #@c
    iput v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mErrorCode:I

    #@e
    .line 1049
    iget-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@10
    invoke-virtual {v0, p1}, Lcom/android/server/sip/SipSessionListenerProxy;->onRegistrationTimeout(Landroid/net/sip/ISipSession;)V

    #@13
    .line 1050
    invoke-direct {p0}, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->restartLater()V

    #@16
    .line 1051
    iget-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->this$0:Lcom/android/server/sip/SipService;

    #@18
    invoke-static {v0}, Lcom/android/server/sip/SipService;->access$300(Lcom/android/server/sip/SipService;)Lcom/android/server/sip/SipWakeLock;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {v0, p1}, Lcom/android/server/sip/SipWakeLock;->release(Ljava/lang/Object;)V

    #@1f
    .line 1052
    monitor-exit v1

    #@20
    goto :goto_a

    #@21
    :catchall_21
    move-exception v0

    #@22
    monitor-exit v1
    :try_end_23
    .catchall {:try_start_3 .. :try_end_23} :catchall_21

    #@23
    throw v0
.end method

.method public onResponse(Z)V
    .registers 8
    .parameter "portChanged"

    #@0
    .prologue
    const/16 v5, 0xa

    #@2
    .line 805
    iget-object v2, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->this$0:Lcom/android/server/sip/SipService;

    #@4
    monitor-enter v2

    #@5
    .line 806
    if-eqz p1, :cond_58

    #@7
    .line 807
    :try_start_7
    iget-object v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->this$0:Lcom/android/server/sip/SipService;

    #@9
    invoke-static {v1}, Lcom/android/server/sip/SipService;->access$1200(Lcom/android/server/sip/SipService;)I

    #@c
    move-result v0

    #@d
    .line 808
    .local v0, interval:I
    iget v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mKeepAliveSuccessCount:I

    #@f
    if-ge v1, v5, :cond_4e

    #@11
    .line 809
    iget-object v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->TAG:Ljava/lang/String;

    #@13
    new-instance v3, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string/jumbo v4, "keepalive doesn\'t work with interval "

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    const-string v4, ", past success count="

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    iget v4, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mKeepAliveSuccessCount:I

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v3

    #@33
    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 812
    if-le v0, v5, :cond_46

    #@38
    .line 813
    iget-object v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->this$0:Lcom/android/server/sip/SipService;

    #@3a
    iget-object v3, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@3c
    invoke-virtual {v3}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->getLocalProfile()Landroid/net/sip/SipProfile;

    #@3f
    move-result-object v3

    #@40
    invoke-static {v1, v3, v0}, Lcom/android/server/sip/SipService;->access$1300(Lcom/android/server/sip/SipService;Landroid/net/sip/SipProfile;I)V

    #@43
    .line 815
    const/4 v1, 0x0

    #@44
    iput v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mKeepAliveSuccessCount:I

    #@46
    .line 833
    .end local v0           #interval:I
    :cond_46
    :goto_46
    iget-boolean v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mRunning:Z

    #@48
    if-eqz v1, :cond_4c

    #@4a
    if-nez p1, :cond_6a

    #@4c
    :cond_4c
    monitor-exit v2

    #@4d
    .line 845
    :goto_4d
    return-void

    #@4e
    .line 823
    .restart local v0       #interval:I
    :cond_4e
    iget v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mKeepAliveSuccessCount:I

    #@50
    div-int/lit8 v1, v1, 0x2

    #@52
    iput v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mKeepAliveSuccessCount:I

    #@54
    goto :goto_46

    #@55
    .line 844
    .end local v0           #interval:I
    :catchall_55
    move-exception v1

    #@56
    monitor-exit v2
    :try_end_57
    .catchall {:try_start_7 .. :try_end_57} :catchall_55

    #@57
    throw v1

    #@58
    .line 828
    :cond_58
    :try_start_58
    iget-object v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->this$0:Lcom/android/server/sip/SipService;

    #@5a
    iget-object v3, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@5c
    invoke-virtual {v3}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->getLocalProfile()Landroid/net/sip/SipProfile;

    #@5f
    move-result-object v3

    #@60
    invoke-static {v1, v3}, Lcom/android/server/sip/SipService;->access$1400(Lcom/android/server/sip/SipService;Landroid/net/sip/SipProfile;)V

    #@63
    .line 830
    iget v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mKeepAliveSuccessCount:I

    #@65
    add-int/lit8 v1, v1, 0x1

    #@67
    iput v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mKeepAliveSuccessCount:I

    #@69
    goto :goto_46

    #@6a
    .line 838
    :cond_6a
    const/4 v1, 0x0

    #@6b
    iput-object v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mKeepAliveSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@6d
    .line 842
    iget-object v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->this$0:Lcom/android/server/sip/SipService;

    #@6f
    invoke-static {v1}, Lcom/android/server/sip/SipService;->access$300(Lcom/android/server/sip/SipService;)Lcom/android/server/sip/SipWakeLock;

    #@72
    move-result-object v1

    #@73
    iget-object v3, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@75
    invoke-virtual {v1, v3}, Lcom/android/server/sip/SipWakeLock;->acquire(Ljava/lang/Object;)V

    #@78
    .line 843
    iget-object v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@7a
    const/16 v3, 0xe10

    #@7c
    invoke-virtual {v1, v3}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->register(I)V

    #@7f
    .line 844
    monitor-exit v2
    :try_end_80
    .catchall {:try_start_58 .. :try_end_80} :catchall_55

    #@80
    goto :goto_4d
.end method

.method public run()V
    .registers 4

    #@0
    .prologue
    .line 931
    iget-object v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->this$0:Lcom/android/server/sip/SipService;

    #@2
    monitor-enter v1

    #@3
    .line 932
    :try_start_3
    iget-boolean v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mRunning:Z

    #@5
    if-nez v0, :cond_9

    #@7
    monitor-exit v1

    #@8
    .line 942
    :goto_8
    return-void

    #@9
    .line 934
    :cond_9
    const/4 v0, 0x0

    #@a
    iput v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mErrorCode:I

    #@c
    .line 935
    const/4 v0, 0x0

    #@d
    iput-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mErrorMessage:Ljava/lang/String;

    #@f
    .line 937
    iget-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->this$0:Lcom/android/server/sip/SipService;

    #@11
    invoke-static {v0}, Lcom/android/server/sip/SipService;->access$400(Lcom/android/server/sip/SipService;)I

    #@14
    move-result v0

    #@15
    const/4 v2, -0x1

    #@16
    if-eq v0, v2, :cond_2a

    #@18
    .line 938
    iget-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->this$0:Lcom/android/server/sip/SipService;

    #@1a
    invoke-static {v0}, Lcom/android/server/sip/SipService;->access$300(Lcom/android/server/sip/SipService;)Lcom/android/server/sip/SipWakeLock;

    #@1d
    move-result-object v0

    #@1e
    iget-object v2, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@20
    invoke-virtual {v0, v2}, Lcom/android/server/sip/SipWakeLock;->acquire(Ljava/lang/Object;)V

    #@23
    .line 939
    iget-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@25
    const/16 v2, 0xe10

    #@27
    invoke-virtual {v0, v2}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->register(I)V

    #@2a
    .line 941
    :cond_2a
    monitor-exit v1

    #@2b
    goto :goto_8

    #@2c
    :catchall_2c
    move-exception v0

    #@2d
    monitor-exit v1
    :try_end_2e
    .catchall {:try_start_3 .. :try_end_2e} :catchall_2c

    #@2e
    throw v0
.end method

.method public setListener(Landroid/net/sip/ISipSessionListener;)V
    .registers 11
    .parameter "listener"

    #@0
    .prologue
    .line 884
    iget-object v4, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->this$0:Lcom/android/server/sip/SipService;

    #@2
    monitor-enter v4

    #@3
    .line 885
    :try_start_3
    iget-object v3, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@5
    invoke-virtual {v3, p1}, Lcom/android/server/sip/SipSessionListenerProxy;->setListener(Landroid/net/sip/ISipSessionListener;)V
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_52

    #@8
    .line 888
    :try_start_8
    iget-object v3, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@a
    if-nez v3, :cond_1c

    #@c
    const/4 v1, 0x0

    #@d
    .line 891
    .local v1, state:I
    :goto_d
    const/4 v3, 0x1

    #@e
    if-eq v1, v3, :cond_13

    #@10
    const/4 v3, 0x2

    #@11
    if-ne v1, v3, :cond_23

    #@13
    .line 893
    :cond_13
    iget-object v3, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@15
    iget-object v5, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@17
    invoke-virtual {v3, v5}, Lcom/android/server/sip/SipSessionListenerProxy;->onRegistering(Landroid/net/sip/ISipSession;)V
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_52
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_1a} :catch_37

    #@1a
    .line 921
    .end local v1           #state:I
    :goto_1a
    :try_start_1a
    monitor-exit v4
    :try_end_1b
    .catchall {:try_start_1a .. :try_end_1b} :catchall_52

    #@1b
    .line 922
    return-void

    #@1c
    .line 888
    :cond_1c
    :try_start_1c
    iget-object v3, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@1e
    invoke-virtual {v3}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->getState()I

    #@21
    move-result v1

    #@22
    goto :goto_d

    #@23
    .line 894
    .restart local v1       #state:I
    :cond_23
    iget-boolean v3, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mRegistered:Z

    #@25
    if-eqz v3, :cond_55

    #@27
    .line 895
    iget-wide v5, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mExpiryTime:J

    #@29
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@2c
    move-result-wide v7

    #@2d
    sub-long/2addr v5, v7

    #@2e
    long-to-int v0, v5

    #@2f
    .line 897
    .local v0, duration:I
    iget-object v3, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@31
    iget-object v5, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@33
    invoke-virtual {v3, v5, v0}, Lcom/android/server/sip/SipSessionListenerProxy;->onRegistrationDone(Landroid/net/sip/ISipSession;I)V
    :try_end_36
    .catchall {:try_start_1c .. :try_end_36} :catchall_52
    .catch Ljava/lang/Throwable; {:try_start_1c .. :try_end_36} :catch_37

    #@36
    goto :goto_1a

    #@37
    .line 918
    .end local v0           #duration:I
    .end local v1           #state:I
    :catch_37
    move-exception v2

    #@38
    .line 919
    .local v2, t:Ljava/lang/Throwable;
    :try_start_38
    iget-object v3, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->TAG:Ljava/lang/String;

    #@3a
    new-instance v5, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string/jumbo v6, "setListener(): "

    #@42
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v5

    #@46
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v5

    #@4a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v5

    #@4e
    invoke-static {v3, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    goto :goto_1a

    #@52
    .line 921
    .end local v2           #t:Ljava/lang/Throwable;
    :catchall_52
    move-exception v3

    #@53
    monitor-exit v4
    :try_end_54
    .catchall {:try_start_38 .. :try_end_54} :catchall_52

    #@54
    throw v3

    #@55
    .line 898
    .restart local v1       #state:I
    :cond_55
    :try_start_55
    iget v3, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mErrorCode:I

    #@57
    if-eqz v3, :cond_72

    #@59
    .line 899
    iget v3, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mErrorCode:I

    #@5b
    const/4 v5, -0x5

    #@5c
    if-ne v3, v5, :cond_66

    #@5e
    .line 900
    iget-object v3, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@60
    iget-object v5, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@62
    invoke-virtual {v3, v5}, Lcom/android/server/sip/SipSessionListenerProxy;->onRegistrationTimeout(Landroid/net/sip/ISipSession;)V

    #@65
    goto :goto_1a

    #@66
    .line 902
    :cond_66
    iget-object v3, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@68
    iget-object v5, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@6a
    iget v6, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mErrorCode:I

    #@6c
    iget-object v7, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mErrorMessage:Ljava/lang/String;

    #@6e
    invoke-virtual {v3, v5, v6, v7}, Lcom/android/server/sip/SipSessionListenerProxy;->onRegistrationFailed(Landroid/net/sip/ISipSession;ILjava/lang/String;)V

    #@71
    goto :goto_1a

    #@72
    .line 905
    :cond_72
    iget-object v3, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->this$0:Lcom/android/server/sip/SipService;

    #@74
    invoke-static {v3}, Lcom/android/server/sip/SipService;->access$400(Lcom/android/server/sip/SipService;)I

    #@77
    move-result v3

    #@78
    const/4 v5, -0x1

    #@79
    if-ne v3, v5, :cond_88

    #@7b
    .line 906
    iget-object v3, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@7d
    iget-object v5, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@7f
    const/16 v6, -0xa

    #@81
    const-string/jumbo v7, "no data connection"

    #@84
    invoke-virtual {v3, v5, v6, v7}, Lcom/android/server/sip/SipSessionListenerProxy;->onRegistrationFailed(Landroid/net/sip/ISipSession;ILjava/lang/String;)V

    #@87
    goto :goto_1a

    #@88
    .line 909
    :cond_88
    iget-boolean v3, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mRunning:Z

    #@8a
    if-nez v3, :cond_98

    #@8c
    .line 910
    iget-object v3, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@8e
    iget-object v5, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@90
    const/4 v6, -0x4

    #@91
    const-string/jumbo v7, "registration not running"

    #@94
    invoke-virtual {v3, v5, v6, v7}, Lcom/android/server/sip/SipSessionListenerProxy;->onRegistrationFailed(Landroid/net/sip/ISipSession;ILjava/lang/String;)V

    #@97
    goto :goto_1a

    #@98
    .line 914
    :cond_98
    iget-object v3, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@9a
    iget-object v5, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@9c
    const/16 v6, -0x9

    #@9e
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@a1
    move-result-object v7

    #@a2
    invoke-virtual {v3, v5, v6, v7}, Lcom/android/server/sip/SipSessionListenerProxy;->onRegistrationFailed(Landroid/net/sip/ISipSession;ILjava/lang/String;)V
    :try_end_a5
    .catchall {:try_start_55 .. :try_end_a5} :catchall_52
    .catch Ljava/lang/Throwable; {:try_start_55 .. :try_end_a5} :catch_37

    #@a5
    goto/16 :goto_1a
.end method

.method public start(Lcom/android/server/sip/SipSessionGroup;)V
    .registers 4
    .parameter "group"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 762
    iget-boolean v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mRunning:Z

    #@3
    if-nez v0, :cond_15

    #@5
    .line 763
    iput-boolean v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mRunning:Z

    #@7
    .line 764
    iput v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mBackoff:I

    #@9
    .line 765
    invoke-virtual {p1, p0}, Lcom/android/server/sip/SipSessionGroup;->createSession(Landroid/net/sip/ISipSessionListener;)Landroid/net/sip/ISipSession;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@f
    iput-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@11
    .line 768
    iget-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@13
    if-nez v0, :cond_16

    #@15
    .line 777
    :cond_15
    :goto_15
    return-void

    #@16
    .line 773
    :cond_16
    iget-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->this$0:Lcom/android/server/sip/SipService;

    #@18
    invoke-static {v0}, Lcom/android/server/sip/SipService;->access$300(Lcom/android/server/sip/SipService;)Lcom/android/server/sip/SipWakeLock;

    #@1b
    move-result-object v0

    #@1c
    iget-object v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@1e
    invoke-virtual {v0, v1}, Lcom/android/server/sip/SipWakeLock;->acquire(Ljava/lang/Object;)V

    #@21
    .line 774
    iget-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@23
    invoke-virtual {v0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->unregister()V

    #@26
    .line 775
    new-instance v0, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v1, "SipAutoReg:"

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v0

    #@31
    iget-object v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@33
    invoke-virtual {v1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->getLocalProfile()Landroid/net/sip/SipProfile;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {v1}, Landroid/net/sip/SipProfile;->getUriString()Ljava/lang/String;

    #@3a
    move-result-object v1

    #@3b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v0

    #@3f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v0

    #@43
    iput-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->TAG:Ljava/lang/String;

    #@45
    goto :goto_15
.end method

.method public stop()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 857
    iget-boolean v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mRunning:Z

    #@3
    if-nez v0, :cond_6

    #@5
    .line 870
    :goto_5
    return-void

    #@6
    .line 858
    :cond_6
    iput-boolean v2, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mRunning:Z

    #@8
    .line 859
    iget-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->this$0:Lcom/android/server/sip/SipService;

    #@a
    invoke-static {v0}, Lcom/android/server/sip/SipService;->access$300(Lcom/android/server/sip/SipService;)Lcom/android/server/sip/SipWakeLock;

    #@d
    move-result-object v0

    #@e
    iget-object v1, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@10
    invoke-virtual {v0, v1}, Lcom/android/server/sip/SipWakeLock;->release(Ljava/lang/Object;)V

    #@13
    .line 860
    iget-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@15
    if-eqz v0, :cond_2f

    #@17
    .line 861
    iget-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@19
    const/4 v1, 0x0

    #@1a
    invoke-virtual {v0, v1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->setListener(Landroid/net/sip/ISipSessionListener;)V

    #@1d
    .line 862
    iget-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->this$0:Lcom/android/server/sip/SipService;

    #@1f
    invoke-static {v0}, Lcom/android/server/sip/SipService;->access$400(Lcom/android/server/sip/SipService;)I

    #@22
    move-result v0

    #@23
    const/4 v1, -0x1

    #@24
    if-eq v0, v1, :cond_2f

    #@26
    iget-boolean v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mRegistered:Z

    #@28
    if-eqz v0, :cond_2f

    #@2a
    iget-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@2c
    invoke-virtual {v0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->unregister()V

    #@2f
    .line 865
    :cond_2f
    iget-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->this$0:Lcom/android/server/sip/SipService;

    #@31
    invoke-static {v0}, Lcom/android/server/sip/SipService;->access$200(Lcom/android/server/sip/SipService;)Lcom/android/server/sip/SipWakeupTimer;

    #@34
    move-result-object v0

    #@35
    invoke-virtual {v0, p0}, Lcom/android/server/sip/SipWakeupTimer;->cancel(Ljava/lang/Runnable;)V

    #@38
    .line 866
    invoke-direct {p0}, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->stopKeepAliveProcess()V

    #@3b
    .line 868
    iput-boolean v2, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mRegistered:Z

    #@3d
    .line 869
    iget-object v0, p0, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@3f
    invoke-virtual {v0}, Lcom/android/server/sip/SipSessionListenerProxy;->getListener()Landroid/net/sip/ISipSessionListener;

    #@42
    move-result-object v0

    #@43
    invoke-virtual {p0, v0}, Lcom/android/server/sip/SipService$AutoRegistrationProcess;->setListener(Landroid/net/sip/ISipSessionListener;)V

    #@46
    goto :goto_5
.end method
