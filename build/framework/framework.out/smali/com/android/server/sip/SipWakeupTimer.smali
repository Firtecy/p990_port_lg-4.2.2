.class Lcom/android/server/sip/SipWakeupTimer;
.super Landroid/content/BroadcastReceiver;
.source "SipWakeupTimer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/sip/SipWakeupTimer$1;,
        Lcom/android/server/sip/SipWakeupTimer$MyEventComparator;,
        Lcom/android/server/sip/SipWakeupTimer$MyEvent;
    }
.end annotation


# static fields
.field private static final DEBUG_TIMER:Z = false

.field private static final TAG:Ljava/lang/String; = "_SIP.WkTimer_"

.field private static final TRIGGER_TIME:Ljava/lang/String; = "TriggerTime"


# instance fields
.field private mAlarmManager:Landroid/app/AlarmManager;

.field private mContext:Landroid/content/Context;

.field private mEventQueue:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Lcom/android/server/sip/SipWakeupTimer$MyEvent;",
            ">;"
        }
    .end annotation
.end field

.field private mExecutor:Ljava/util/concurrent/Executor;

.field private mPendingIntent:Landroid/app/PendingIntent;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;)V
    .registers 7
    .parameter "context"
    .parameter "executor"

    #@0
    .prologue
    .line 63
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@3
    .line 56
    new-instance v1, Ljava/util/TreeSet;

    #@5
    new-instance v2, Lcom/android/server/sip/SipWakeupTimer$MyEventComparator;

    #@7
    const/4 v3, 0x0

    #@8
    invoke-direct {v2, v3}, Lcom/android/server/sip/SipWakeupTimer$MyEventComparator;-><init>(Lcom/android/server/sip/SipWakeupTimer$1;)V

    #@b
    invoke-direct {v1, v2}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    #@e
    iput-object v1, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;

    #@10
    .line 64
    iput-object p1, p0, Lcom/android/server/sip/SipWakeupTimer;->mContext:Landroid/content/Context;

    #@12
    .line 65
    const-string v1, "alarm"

    #@14
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@17
    move-result-object v1

    #@18
    check-cast v1, Landroid/app/AlarmManager;

    #@1a
    iput-object v1, p0, Lcom/android/server/sip/SipWakeupTimer;->mAlarmManager:Landroid/app/AlarmManager;

    #@1c
    .line 68
    new-instance v0, Landroid/content/IntentFilter;

    #@1e
    invoke-direct {p0}, Lcom/android/server/sip/SipWakeupTimer;->getAction()Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@25
    .line 69
    .local v0, filter:Landroid/content/IntentFilter;
    invoke-virtual {p1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@28
    .line 70
    iput-object p2, p0, Lcom/android/server/sip/SipWakeupTimer;->mExecutor:Ljava/util/concurrent/Executor;

    #@2a
    .line 71
    return-void
.end method

.method private cancelAlarm()V
    .registers 3

    #@0
    .prologue
    .line 96
    iget-object v0, p0, Lcom/android/server/sip/SipWakeupTimer;->mAlarmManager:Landroid/app/AlarmManager;

    #@2
    iget-object v1, p0, Lcom/android/server/sip/SipWakeupTimer;->mPendingIntent:Landroid/app/PendingIntent;

    #@4
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@7
    .line 97
    const/4 v0, 0x0

    #@8
    iput-object v0, p0, Lcom/android/server/sip/SipWakeupTimer;->mPendingIntent:Landroid/app/PendingIntent;

    #@a
    .line 98
    return-void
.end method

.method private execute(J)V
    .registers 9
    .parameter "triggerTime"

    #@0
    .prologue
    .line 267
    invoke-direct {p0}, Lcom/android/server/sip/SipWakeupTimer;->stopped()Z

    #@3
    move-result v2

    #@4
    if-nez v2, :cond_e

    #@6
    iget-object v2, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;

    #@8
    invoke-virtual {v2}, Ljava/util/TreeSet;->isEmpty()Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_f

    #@e
    .line 284
    :cond_e
    :goto_e
    return-void

    #@f
    .line 269
    :cond_f
    iget-object v2, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;

    #@11
    invoke-virtual {v2}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    #@14
    move-result-object v1

    #@15
    .local v1, i$:Ljava/util/Iterator;
    :cond_15
    :goto_15
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@18
    move-result v2

    #@19
    if-eqz v2, :cond_39

    #@1b
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1e
    move-result-object v0

    #@1f
    check-cast v0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;

    #@21
    .line 270
    .local v0, event:Lcom/android/server/sip/SipWakeupTimer$MyEvent;
    iget-wide v2, v0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mTriggerTime:J

    #@23
    cmp-long v2, v2, p1

    #@25
    if-nez v2, :cond_15

    #@27
    .line 273
    iput-wide p1, v0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mLastTriggerTime:J

    #@29
    .line 274
    iget-wide v2, v0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mTriggerTime:J

    #@2b
    iget v4, v0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mPeriod:I

    #@2d
    int-to-long v4, v4

    #@2e
    add-long/2addr v2, v4

    #@2f
    iput-wide v2, v0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mTriggerTime:J

    #@31
    .line 277
    iget-object v2, p0, Lcom/android/server/sip/SipWakeupTimer;->mExecutor:Ljava/util/concurrent/Executor;

    #@33
    iget-object v3, v0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mCallback:Ljava/lang/Runnable;

    #@35
    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    #@38
    goto :goto_15

    #@39
    .line 283
    .end local v0           #event:Lcom/android/server/sip/SipWakeupTimer$MyEvent;
    :cond_39
    invoke-direct {p0}, Lcom/android/server/sip/SipWakeupTimer;->scheduleNext()V

    #@3c
    goto :goto_e
.end method

.method private getAction()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 287
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private insertEvent(Lcom/android/server/sip/SipWakeupTimer$MyEvent;)V
    .registers 13
    .parameter "event"

    #@0
    .prologue
    .line 127
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@3
    move-result-wide v3

    #@4
    .line 128
    .local v3, now:J
    iget-object v7, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;

    #@6
    invoke-virtual {v7}, Ljava/util/TreeSet;->isEmpty()Z

    #@9
    move-result v7

    #@a
    if-eqz v7, :cond_18

    #@c
    .line 129
    iget v7, p1, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mPeriod:I

    #@e
    int-to-long v7, v7

    #@f
    add-long/2addr v7, v3

    #@10
    iput-wide v7, p1, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mTriggerTime:J

    #@12
    .line 130
    iget-object v7, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;

    #@14
    invoke-virtual {v7, p1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    #@17
    .line 153
    :goto_17
    return-void

    #@18
    .line 133
    :cond_18
    iget-object v7, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;

    #@1a
    invoke-virtual {v7}, Ljava/util/TreeSet;->first()Ljava/lang/Object;

    #@1d
    move-result-object v0

    #@1e
    check-cast v0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;

    #@20
    .line 134
    .local v0, firstEvent:Lcom/android/server/sip/SipWakeupTimer$MyEvent;
    iget v2, v0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mPeriod:I

    #@22
    .line 135
    .local v2, minPeriod:I
    iget v7, p1, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mMaxPeriod:I

    #@24
    if-gt v2, v7, :cond_43

    #@26
    .line 136
    iget v7, p1, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mMaxPeriod:I

    #@28
    div-int/2addr v7, v2

    #@29
    mul-int/2addr v7, v2

    #@2a
    iput v7, p1, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mPeriod:I

    #@2c
    .line 137
    iget v1, p1, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mMaxPeriod:I

    #@2e
    .line 138
    .local v1, interval:I
    iget-wide v7, v0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mTriggerTime:J

    #@30
    sub-long/2addr v7, v3

    #@31
    long-to-int v7, v7

    #@32
    sub-int/2addr v1, v7

    #@33
    .line 139
    div-int v7, v1, v2

    #@35
    mul-int v1, v7, v2

    #@37
    .line 140
    iget-wide v7, v0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mTriggerTime:J

    #@39
    int-to-long v9, v1

    #@3a
    add-long/2addr v7, v9

    #@3b
    iput-wide v7, p1, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mTriggerTime:J

    #@3d
    .line 141
    iget-object v7, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;

    #@3f
    invoke-virtual {v7, p1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    #@42
    goto :goto_17

    #@43
    .line 143
    .end local v1           #interval:I
    :cond_43
    iget v7, p1, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mPeriod:I

    #@45
    int-to-long v7, v7

    #@46
    add-long v5, v3, v7

    #@48
    .line 144
    .local v5, triggerTime:J
    iget-wide v7, v0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mTriggerTime:J

    #@4a
    cmp-long v7, v7, v5

    #@4c
    if-gez v7, :cond_63

    #@4e
    .line 145
    iget-wide v7, v0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mTriggerTime:J

    #@50
    iput-wide v7, p1, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mTriggerTime:J

    #@52
    .line 146
    iget-wide v7, p1, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mLastTriggerTime:J

    #@54
    iget v9, p1, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mPeriod:I

    #@56
    int-to-long v9, v9

    #@57
    sub-long/2addr v7, v9

    #@58
    iput-wide v7, p1, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mLastTriggerTime:J

    #@5a
    .line 150
    :goto_5a
    iget-object v7, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;

    #@5c
    invoke-virtual {v7, p1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    #@5f
    .line 151
    invoke-direct {p0}, Lcom/android/server/sip/SipWakeupTimer;->recalculatePeriods()V

    #@62
    goto :goto_17

    #@63
    .line 148
    :cond_63
    iput-wide v5, p1, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mTriggerTime:J

    #@65
    goto :goto_5a
.end method

.method private printQueue()V
    .registers 8

    #@0
    .prologue
    .line 250
    const/4 v0, 0x0

    #@1
    .line 251
    .local v0, count:I
    iget-object v3, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;

    #@3
    invoke-virtual {v3}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    #@6
    move-result-object v2

    #@7
    .local v2, i$:Ljava/util/Iterator;
    :cond_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@a
    move-result v3

    #@b
    if-eqz v3, :cond_50

    #@d
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@10
    move-result-object v1

    #@11
    check-cast v1, Lcom/android/server/sip/SipWakeupTimer$MyEvent;

    #@13
    .line 252
    .local v1, event:Lcom/android/server/sip/SipWakeupTimer$MyEvent;
    const-string v3, "_SIP.WkTimer_"

    #@15
    new-instance v4, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v5, "     "

    #@1c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v4

    #@20
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v4

    #@24
    const-string v5, ": scheduled at "

    #@26
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v4

    #@2a
    iget-wide v5, v1, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mTriggerTime:J

    #@2c
    invoke-direct {p0, v5, v6}, Lcom/android/server/sip/SipWakeupTimer;->showTime(J)Ljava/lang/String;

    #@2f
    move-result-object v5

    #@30
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v4

    #@34
    const-string v5, ": last at "

    #@36
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v4

    #@3a
    iget-wide v5, v1, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mLastTriggerTime:J

    #@3c
    invoke-direct {p0, v5, v6}, Lcom/android/server/sip/SipWakeupTimer;->showTime(J)Ljava/lang/String;

    #@3f
    move-result-object v5

    #@40
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v4

    #@44
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v4

    #@48
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 255
    add-int/lit8 v0, v0, 0x1

    #@4d
    const/4 v3, 0x5

    #@4e
    if-lt v0, v3, :cond_7

    #@50
    .line 257
    .end local v1           #event:Lcom/android/server/sip/SipWakeupTimer$MyEvent;
    :cond_50
    iget-object v3, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;

    #@52
    invoke-virtual {v3}, Ljava/util/TreeSet;->size()I

    #@55
    move-result v3

    #@56
    if-le v3, v0, :cond_60

    #@58
    .line 258
    const-string v3, "_SIP.WkTimer_"

    #@5a
    const-string v4, "     ....."

    #@5c
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    .line 262
    :cond_5f
    :goto_5f
    return-void

    #@60
    .line 259
    :cond_60
    if-nez v0, :cond_5f

    #@62
    .line 260
    const-string v3, "_SIP.WkTimer_"

    #@64
    const-string v4, "     <empty>"

    #@66
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@69
    goto :goto_5f
.end method

.method private recalculatePeriods()V
    .registers 13

    #@0
    .prologue
    .line 101
    iget-object v8, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;

    #@2
    invoke-virtual {v8}, Ljava/util/TreeSet;->isEmpty()Z

    #@5
    move-result v8

    #@6
    if-eqz v8, :cond_9

    #@8
    .line 122
    :goto_8
    return-void

    #@9
    .line 103
    :cond_9
    iget-object v8, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;

    #@b
    invoke-virtual {v8}, Ljava/util/TreeSet;->first()Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Lcom/android/server/sip/SipWakeupTimer$MyEvent;

    #@11
    .line 104
    .local v1, firstEvent:Lcom/android/server/sip/SipWakeupTimer$MyEvent;
    iget v4, v1, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mMaxPeriod:I

    #@13
    .line 105
    .local v4, minPeriod:I
    iget-wide v5, v1, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mTriggerTime:J

    #@15
    .line 106
    .local v5, minTriggerTime:J
    iget-object v8, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;

    #@17
    invoke-virtual {v8}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    #@1a
    move-result-object v2

    #@1b
    .local v2, i$:Ljava/util/Iterator;
    :goto_1b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@1e
    move-result v8

    #@1f
    if-eqz v8, :cond_3e

    #@21
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@24
    move-result-object v0

    #@25
    check-cast v0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;

    #@27
    .line 107
    .local v0, e:Lcom/android/server/sip/SipWakeupTimer$MyEvent;
    iget v8, v0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mMaxPeriod:I

    #@29
    div-int/2addr v8, v4

    #@2a
    mul-int/2addr v8, v4

    #@2b
    iput v8, v0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mPeriod:I

    #@2d
    .line 108
    iget-wide v8, v0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mLastTriggerTime:J

    #@2f
    iget v10, v0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mMaxPeriod:I

    #@31
    int-to-long v10, v10

    #@32
    add-long/2addr v8, v10

    #@33
    sub-long/2addr v8, v5

    #@34
    long-to-int v3, v8

    #@35
    .line 110
    .local v3, interval:I
    div-int v8, v3, v4

    #@37
    mul-int v3, v8, v4

    #@39
    .line 111
    int-to-long v8, v3

    #@3a
    add-long/2addr v8, v5

    #@3b
    iput-wide v8, v0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mTriggerTime:J

    #@3d
    goto :goto_1b

    #@3e
    .line 113
    .end local v0           #e:Lcom/android/server/sip/SipWakeupTimer$MyEvent;
    .end local v3           #interval:I
    :cond_3e
    new-instance v7, Ljava/util/TreeSet;

    #@40
    iget-object v8, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;

    #@42
    invoke-virtual {v8}, Ljava/util/TreeSet;->comparator()Ljava/util/Comparator;

    #@45
    move-result-object v8

    #@46
    invoke-direct {v7, v8}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    #@49
    .line 115
    .local v7, newQueue:Ljava/util/TreeSet;,"Ljava/util/TreeSet<Lcom/android/server/sip/SipWakeupTimer$MyEvent;>;"
    iget-object v8, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;

    #@4b
    invoke-virtual {v7, v8}, Ljava/util/TreeSet;->addAll(Ljava/util/Collection;)Z

    #@4e
    .line 116
    iget-object v8, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;

    #@50
    invoke-virtual {v8}, Ljava/util/TreeSet;->clear()V

    #@53
    .line 117
    iput-object v7, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;

    #@55
    goto :goto_8
.end method

.method private scheduleNext()V
    .registers 8

    #@0
    .prologue
    .line 219
    invoke-direct {p0}, Lcom/android/server/sip/SipWakeupTimer;->stopped()Z

    #@3
    move-result v3

    #@4
    if-nez v3, :cond_e

    #@6
    iget-object v3, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;

    #@8
    invoke-virtual {v3}, Ljava/util/TreeSet;->isEmpty()Z

    #@b
    move-result v3

    #@c
    if-eqz v3, :cond_f

    #@e
    .line 233
    :cond_e
    :goto_e
    return-void

    #@f
    .line 221
    :cond_f
    iget-object v3, p0, Lcom/android/server/sip/SipWakeupTimer;->mPendingIntent:Landroid/app/PendingIntent;

    #@11
    if-eqz v3, :cond_1c

    #@13
    .line 222
    new-instance v3, Ljava/lang/RuntimeException;

    #@15
    const-string/jumbo v4, "pendingIntent is not null!"

    #@18
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v3

    #@1c
    .line 225
    :cond_1c
    iget-object v3, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;

    #@1e
    invoke-virtual {v3}, Ljava/util/TreeSet;->first()Ljava/lang/Object;

    #@21
    move-result-object v0

    #@22
    check-cast v0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;

    #@24
    .line 226
    .local v0, event:Lcom/android/server/sip/SipWakeupTimer$MyEvent;
    new-instance v1, Landroid/content/Intent;

    #@26
    invoke-direct {p0}, Lcom/android/server/sip/SipWakeupTimer;->getAction()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@2d
    .line 227
    .local v1, intent:Landroid/content/Intent;
    const-string v3, "TriggerTime"

    #@2f
    iget-wide v4, v0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mTriggerTime:J

    #@31
    invoke-virtual {v1, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    #@34
    .line 228
    iget-object v3, p0, Lcom/android/server/sip/SipWakeupTimer;->mContext:Landroid/content/Context;

    #@36
    const/4 v4, 0x0

    #@37
    const/high16 v5, 0x800

    #@39
    invoke-static {v3, v4, v1, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@3c
    move-result-object v2

    #@3d
    iput-object v2, p0, Lcom/android/server/sip/SipWakeupTimer;->mPendingIntent:Landroid/app/PendingIntent;

    #@3f
    .line 231
    .local v2, pendingIntent:Landroid/app/PendingIntent;
    iget-object v3, p0, Lcom/android/server/sip/SipWakeupTimer;->mAlarmManager:Landroid/app/AlarmManager;

    #@41
    const/4 v4, 0x2

    #@42
    iget-wide v5, v0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mTriggerTime:J

    #@44
    invoke-virtual {v3, v4, v5, v6, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@47
    goto :goto_e
.end method

.method private showTime(J)Ljava/lang/String;
    .registers 10
    .parameter "time"

    #@0
    .prologue
    const-wide/16 v5, 0x3e8

    #@2
    .line 291
    rem-long v3, p1, v5

    #@4
    long-to-int v1, v3

    #@5
    .line 292
    .local v1, ms:I
    div-long v3, p1, v5

    #@7
    long-to-int v2, v3

    #@8
    .line 293
    .local v2, s:I
    div-int/lit8 v0, v2, 0x3c

    #@a
    .line 294
    .local v0, m:I
    rem-int/lit8 v2, v2, 0x3c

    #@c
    .line 295
    const-string v3, "%d.%d.%d"

    #@e
    const/4 v4, 0x3

    #@f
    new-array v4, v4, [Ljava/lang/Object;

    #@11
    const/4 v5, 0x0

    #@12
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@15
    move-result-object v6

    #@16
    aput-object v6, v4, v5

    #@18
    const/4 v5, 0x1

    #@19
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1c
    move-result-object v6

    #@1d
    aput-object v6, v4, v5

    #@1f
    const/4 v5, 0x2

    #@20
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@23
    move-result-object v6

    #@24
    aput-object v6, v4, v5

    #@26
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    return-object v3
.end method

.method private stopped()Z
    .registers 3

    #@0
    .prologue
    .line 87
    iget-object v0, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;

    #@2
    if-nez v0, :cond_d

    #@4
    .line 88
    const-string v0, "_SIP.WkTimer_"

    #@6
    const-string v1, "Timer stopped"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 89
    const/4 v0, 0x1

    #@c
    .line 91
    :goto_c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method


# virtual methods
.method public declared-synchronized cancel(Ljava/lang/Runnable;)V
    .registers 9
    .parameter "callback"

    #@0
    .prologue
    .line 189
    monitor-enter p0

    #@1
    :try_start_1
    invoke-direct {p0}, Lcom/android/server/sip/SipWakeupTimer;->stopped()Z

    #@4
    move-result v3

    #@5
    if-nez v3, :cond_f

    #@7
    iget-object v3, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;

    #@9
    invoke-virtual {v3}, Ljava/util/TreeSet;->isEmpty()Z
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_33

    #@c
    move-result v3

    #@d
    if-eqz v3, :cond_11

    #@f
    .line 216
    :cond_f
    :goto_f
    monitor-exit p0

    #@10
    return-void

    #@11
    .line 192
    :cond_11
    :try_start_11
    iget-object v3, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;

    #@13
    invoke-virtual {v3}, Ljava/util/TreeSet;->first()Ljava/lang/Object;

    #@16
    move-result-object v1

    #@17
    check-cast v1, Lcom/android/server/sip/SipWakeupTimer$MyEvent;

    #@19
    .line 193
    .local v1, firstEvent:Lcom/android/server/sip/SipWakeupTimer$MyEvent;
    iget-object v3, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;

    #@1b
    invoke-virtual {v3}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    #@1e
    move-result-object v2

    #@1f
    .line 194
    .local v2, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/sip/SipWakeupTimer$MyEvent;>;"
    :cond_1f
    :goto_1f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@22
    move-result v3

    #@23
    if-eqz v3, :cond_36

    #@25
    .line 195
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@28
    move-result-object v0

    #@29
    check-cast v0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;

    #@2b
    .line 196
    .local v0, event:Lcom/android/server/sip/SipWakeupTimer$MyEvent;
    iget-object v3, v0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mCallback:Ljava/lang/Runnable;

    #@2d
    if-ne v3, p1, :cond_1f

    #@2f
    .line 197
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V
    :try_end_32
    .catchall {:try_start_11 .. :try_end_32} :catchall_33

    #@32
    goto :goto_1f

    #@33
    .line 189
    .end local v0           #event:Lcom/android/server/sip/SipWakeupTimer$MyEvent;
    .end local v1           #firstEvent:Lcom/android/server/sip/SipWakeupTimer$MyEvent;
    .end local v2           #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/sip/SipWakeupTimer$MyEvent;>;"
    :catchall_33
    move-exception v3

    #@34
    monitor-exit p0

    #@35
    throw v3

    #@36
    .line 201
    .restart local v1       #firstEvent:Lcom/android/server/sip/SipWakeupTimer$MyEvent;
    .restart local v2       #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/sip/SipWakeupTimer$MyEvent;>;"
    :cond_36
    :try_start_36
    iget-object v3, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;

    #@38
    invoke-virtual {v3}, Ljava/util/TreeSet;->isEmpty()Z

    #@3b
    move-result v3

    #@3c
    if-eqz v3, :cond_42

    #@3e
    .line 202
    invoke-direct {p0}, Lcom/android/server/sip/SipWakeupTimer;->cancelAlarm()V

    #@41
    goto :goto_f

    #@42
    .line 203
    :cond_42
    iget-object v3, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;

    #@44
    invoke-virtual {v3}, Ljava/util/TreeSet;->first()Ljava/lang/Object;

    #@47
    move-result-object v3

    #@48
    if-eq v3, v1, :cond_f

    #@4a
    .line 204
    invoke-direct {p0}, Lcom/android/server/sip/SipWakeupTimer;->cancelAlarm()V

    #@4d
    .line 205
    iget-object v3, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;

    #@4f
    invoke-virtual {v3}, Ljava/util/TreeSet;->first()Ljava/lang/Object;

    #@52
    move-result-object v1

    #@53
    .end local v1           #firstEvent:Lcom/android/server/sip/SipWakeupTimer$MyEvent;
    check-cast v1, Lcom/android/server/sip/SipWakeupTimer$MyEvent;

    #@55
    .line 206
    .restart local v1       #firstEvent:Lcom/android/server/sip/SipWakeupTimer$MyEvent;
    iget v3, v1, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mMaxPeriod:I

    #@57
    iput v3, v1, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mPeriod:I

    #@59
    .line 207
    iget-wide v3, v1, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mLastTriggerTime:J

    #@5b
    iget v5, v1, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mPeriod:I

    #@5d
    int-to-long v5, v5

    #@5e
    add-long/2addr v3, v5

    #@5f
    iput-wide v3, v1, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mTriggerTime:J

    #@61
    .line 209
    invoke-direct {p0}, Lcom/android/server/sip/SipWakeupTimer;->recalculatePeriods()V

    #@64
    .line 210
    invoke-direct {p0}, Lcom/android/server/sip/SipWakeupTimer;->scheduleNext()V
    :try_end_67
    .catchall {:try_start_36 .. :try_end_67} :catchall_33

    #@67
    goto :goto_f
.end method

.method public declared-synchronized onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 9
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 238
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 239
    .local v0, action:Ljava/lang/String;
    invoke-direct {p0}, Lcom/android/server/sip/SipWakeupTimer;->getAction()Ljava/lang/String;

    #@8
    move-result-object v3

    #@9
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v3

    #@d
    if-eqz v3, :cond_2b

    #@f
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    #@12
    move-result-object v3

    #@13
    const-string v4, "TriggerTime"

    #@15
    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    #@18
    move-result v3

    #@19
    if-eqz v3, :cond_2b

    #@1b
    .line 241
    const/4 v3, 0x0

    #@1c
    iput-object v3, p0, Lcom/android/server/sip/SipWakeupTimer;->mPendingIntent:Landroid/app/PendingIntent;

    #@1e
    .line 242
    const-string v3, "TriggerTime"

    #@20
    const-wide/16 v4, -0x1

    #@22
    invoke-virtual {p2, v3, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    #@25
    move-result-wide v1

    #@26
    .line 243
    .local v1, triggerTime:J
    invoke-direct {p0, v1, v2}, Lcom/android/server/sip/SipWakeupTimer;->execute(J)V
    :try_end_29
    .catchall {:try_start_1 .. :try_end_29} :catchall_45

    #@29
    .line 247
    .end local v1           #triggerTime:J
    :goto_29
    monitor-exit p0

    #@2a
    return-void

    #@2b
    .line 245
    :cond_2b
    :try_start_2b
    const-string v3, "_SIP.WkTimer_"

    #@2d
    new-instance v4, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string/jumbo v5, "unrecognized intent: "

    #@35
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v4

    #@41
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_44
    .catchall {:try_start_2b .. :try_end_44} :catchall_45

    #@44
    goto :goto_29

    #@45
    .line 238
    .end local v0           #action:Ljava/lang/String;
    :catchall_45
    move-exception v3

    #@46
    monitor-exit p0

    #@47
    throw v3
.end method

.method public declared-synchronized set(ILjava/lang/Runnable;)V
    .registers 10
    .parameter "period"
    .parameter "callback"

    #@0
    .prologue
    .line 163
    monitor-enter p0

    #@1
    :try_start_1
    invoke-direct {p0}, Lcom/android/server/sip/SipWakeupTimer;->stopped()Z
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_2f

    #@4
    move-result v5

    #@5
    if-eqz v5, :cond_9

    #@7
    .line 181
    :goto_7
    monitor-exit p0

    #@8
    return-void

    #@9
    .line 165
    :cond_9
    :try_start_9
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@c
    move-result-wide v1

    #@d
    .line 166
    .local v1, now:J
    new-instance v0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;

    #@f
    invoke-direct {v0, p1, p2, v1, v2}, Lcom/android/server/sip/SipWakeupTimer$MyEvent;-><init>(ILjava/lang/Runnable;J)V

    #@12
    .line 167
    .local v0, event:Lcom/android/server/sip/SipWakeupTimer$MyEvent;
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipWakeupTimer;->insertEvent(Lcom/android/server/sip/SipWakeupTimer$MyEvent;)V

    #@15
    .line 169
    iget-object v5, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;

    #@17
    invoke-virtual {v5}, Ljava/util/TreeSet;->first()Ljava/lang/Object;

    #@1a
    move-result-object v5

    #@1b
    if-ne v5, v0, :cond_2c

    #@1d
    .line 170
    iget-object v5, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;

    #@1f
    invoke-virtual {v5}, Ljava/util/TreeSet;->size()I

    #@22
    move-result v5

    #@23
    const/4 v6, 0x1

    #@24
    if-le v5, v6, :cond_29

    #@26
    invoke-direct {p0}, Lcom/android/server/sip/SipWakeupTimer;->cancelAlarm()V

    #@29
    .line 171
    :cond_29
    invoke-direct {p0}, Lcom/android/server/sip/SipWakeupTimer;->scheduleNext()V

    #@2c
    .line 174
    :cond_2c
    iget-wide v3, v0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mTriggerTime:J
    :try_end_2e
    .catchall {:try_start_9 .. :try_end_2e} :catchall_2f

    #@2e
    .line 181
    .local v3, triggerTime:J
    goto :goto_7

    #@2f
    .line 163
    .end local v0           #event:Lcom/android/server/sip/SipWakeupTimer$MyEvent;
    .end local v1           #now:J
    .end local v3           #triggerTime:J
    :catchall_2f
    move-exception v5

    #@30
    monitor-exit p0

    #@31
    throw v5
.end method

.method public declared-synchronized stop()V
    .registers 3

    #@0
    .prologue
    .line 77
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/server/sip/SipWakeupTimer;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@6
    .line 78
    iget-object v0, p0, Lcom/android/server/sip/SipWakeupTimer;->mPendingIntent:Landroid/app/PendingIntent;

    #@8
    if-eqz v0, :cond_14

    #@a
    .line 79
    iget-object v0, p0, Lcom/android/server/sip/SipWakeupTimer;->mAlarmManager:Landroid/app/AlarmManager;

    #@c
    iget-object v1, p0, Lcom/android/server/sip/SipWakeupTimer;->mPendingIntent:Landroid/app/PendingIntent;

    #@e
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@11
    .line 80
    const/4 v0, 0x0

    #@12
    iput-object v0, p0, Lcom/android/server/sip/SipWakeupTimer;->mPendingIntent:Landroid/app/PendingIntent;

    #@14
    .line 82
    :cond_14
    iget-object v0, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;

    #@16
    invoke-virtual {v0}, Ljava/util/TreeSet;->clear()V

    #@19
    .line 83
    const/4 v0, 0x0

    #@1a
    iput-object v0, p0, Lcom/android/server/sip/SipWakeupTimer;->mEventQueue:Ljava/util/TreeSet;
    :try_end_1c
    .catchall {:try_start_1 .. :try_end_1c} :catchall_1e

    #@1c
    .line 84
    monitor-exit p0

    #@1d
    return-void

    #@1e
    .line 77
    :catchall_1e
    move-exception v0

    #@1f
    monitor-exit p0

    #@20
    throw v0
.end method
