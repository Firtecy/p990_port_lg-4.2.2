.class Lcom/android/server/sip/SipSessionListenerProxy;
.super Landroid/net/sip/ISipSessionListener$Stub;
.source "SipSessionListenerProxy.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SipSession"


# instance fields
.field private mListener:Landroid/net/sip/ISipSessionListener;


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 26
    invoke-direct {p0}, Landroid/net/sip/ISipSessionListener$Stub;-><init>()V

    #@3
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/sip/SipSessionListenerProxy;)Landroid/net/sip/ISipSessionListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 26
    iget-object v0, p0, Lcom/android/server/sip/SipSessionListenerProxy;->mListener:Landroid/net/sip/ISipSessionListener;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/sip/SipSessionListenerProxy;Ljava/lang/Throwable;Ljava/lang/String;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/android/server/sip/SipSessionListenerProxy;->handle(Ljava/lang/Throwable;Ljava/lang/String;)V

    #@3
    return-void
.end method

.method private handle(Ljava/lang/Throwable;Ljava/lang/String;)V
    .registers 4
    .parameter "t"
    .parameter "message"

    #@0
    .prologue
    .line 223
    instance-of v0, p1, Landroid/os/DeadObjectException;

    #@2
    if-eqz v0, :cond_8

    #@4
    .line 224
    const/4 v0, 0x0

    #@5
    iput-object v0, p0, Lcom/android/server/sip/SipSessionListenerProxy;->mListener:Landroid/net/sip/ISipSessionListener;

    #@7
    .line 230
    :cond_7
    :goto_7
    return-void

    #@8
    .line 227
    :cond_8
    iget-object v0, p0, Lcom/android/server/sip/SipSessionListenerProxy;->mListener:Landroid/net/sip/ISipSessionListener;

    #@a
    if-eqz v0, :cond_7

    #@c
    .line 228
    const-string v0, "SipSession"

    #@e
    invoke-static {v0, p2, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@11
    goto :goto_7
.end method

.method private proxy(Ljava/lang/Runnable;)V
    .registers 4
    .parameter "runnable"

    #@0
    .prologue
    .line 43
    new-instance v0, Ljava/lang/Thread;

    #@2
    const-string v1, "SipSessionCallbackThread"

    #@4
    invoke-direct {v0, p1, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    #@7
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@a
    .line 44
    return-void
.end method


# virtual methods
.method public getListener()Landroid/net/sip/ISipSessionListener;
    .registers 2

    #@0
    .prologue
    .line 36
    iget-object v0, p0, Lcom/android/server/sip/SipSessionListenerProxy;->mListener:Landroid/net/sip/ISipSessionListener;

    #@2
    return-object v0
.end method

.method public onCallBusy(Landroid/net/sip/ISipSession;)V
    .registers 3
    .parameter "session"

    #@0
    .prologue
    .line 128
    iget-object v0, p0, Lcom/android/server/sip/SipSessionListenerProxy;->mListener:Landroid/net/sip/ISipSessionListener;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 138
    :goto_4
    return-void

    #@5
    .line 129
    :cond_5
    new-instance v0, Lcom/android/server/sip/SipSessionListenerProxy$7;

    #@7
    invoke-direct {v0, p0, p1}, Lcom/android/server/sip/SipSessionListenerProxy$7;-><init>(Lcom/android/server/sip/SipSessionListenerProxy;Landroid/net/sip/ISipSession;)V

    #@a
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionListenerProxy;->proxy(Ljava/lang/Runnable;)V

    #@d
    goto :goto_4
.end method

.method public onCallChangeFailed(Landroid/net/sip/ISipSession;ILjava/lang/String;)V
    .registers 5
    .parameter "session"
    .parameter "errorCode"
    .parameter "message"

    #@0
    .prologue
    .line 142
    iget-object v0, p0, Lcom/android/server/sip/SipSessionListenerProxy;->mListener:Landroid/net/sip/ISipSessionListener;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 152
    :goto_4
    return-void

    #@5
    .line 143
    :cond_5
    new-instance v0, Lcom/android/server/sip/SipSessionListenerProxy$8;

    #@7
    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/server/sip/SipSessionListenerProxy$8;-><init>(Lcom/android/server/sip/SipSessionListenerProxy;Landroid/net/sip/ISipSession;ILjava/lang/String;)V

    #@a
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionListenerProxy;->proxy(Ljava/lang/Runnable;)V

    #@d
    goto :goto_4
.end method

.method public onCallEnded(Landroid/net/sip/ISipSession;)V
    .registers 3
    .parameter "session"

    #@0
    .prologue
    .line 101
    iget-object v0, p0, Lcom/android/server/sip/SipSessionListenerProxy;->mListener:Landroid/net/sip/ISipSessionListener;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 111
    :goto_4
    return-void

    #@5
    .line 102
    :cond_5
    new-instance v0, Lcom/android/server/sip/SipSessionListenerProxy$5;

    #@7
    invoke-direct {v0, p0, p1}, Lcom/android/server/sip/SipSessionListenerProxy$5;-><init>(Lcom/android/server/sip/SipSessionListenerProxy;Landroid/net/sip/ISipSession;)V

    #@a
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionListenerProxy;->proxy(Ljava/lang/Runnable;)V

    #@d
    goto :goto_4
.end method

.method public onCallEstablished(Landroid/net/sip/ISipSession;Ljava/lang/String;)V
    .registers 4
    .parameter "session"
    .parameter "sessionDescription"

    #@0
    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/server/sip/SipSessionListenerProxy;->mListener:Landroid/net/sip/ISipSessionListener;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 98
    :goto_4
    return-void

    #@5
    .line 89
    :cond_5
    new-instance v0, Lcom/android/server/sip/SipSessionListenerProxy$4;

    #@7
    invoke-direct {v0, p0, p1, p2}, Lcom/android/server/sip/SipSessionListenerProxy$4;-><init>(Lcom/android/server/sip/SipSessionListenerProxy;Landroid/net/sip/ISipSession;Ljava/lang/String;)V

    #@a
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionListenerProxy;->proxy(Ljava/lang/Runnable;)V

    #@d
    goto :goto_4
.end method

.method public onCallTransferring(Landroid/net/sip/ISipSession;Ljava/lang/String;)V
    .registers 4
    .parameter "newSession"
    .parameter "sessionDescription"

    #@0
    .prologue
    .line 115
    iget-object v0, p0, Lcom/android/server/sip/SipSessionListenerProxy;->mListener:Landroid/net/sip/ISipSessionListener;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 125
    :goto_4
    return-void

    #@5
    .line 116
    :cond_5
    new-instance v0, Lcom/android/server/sip/SipSessionListenerProxy$6;

    #@7
    invoke-direct {v0, p0, p1, p2}, Lcom/android/server/sip/SipSessionListenerProxy$6;-><init>(Lcom/android/server/sip/SipSessionListenerProxy;Landroid/net/sip/ISipSession;Ljava/lang/String;)V

    #@a
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionListenerProxy;->proxy(Ljava/lang/Runnable;)V

    #@d
    goto :goto_4
.end method

.method public onCalling(Landroid/net/sip/ISipSession;)V
    .registers 3
    .parameter "session"

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Lcom/android/server/sip/SipSessionListenerProxy;->mListener:Landroid/net/sip/ISipSessionListener;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 57
    :goto_4
    return-void

    #@5
    .line 48
    :cond_5
    new-instance v0, Lcom/android/server/sip/SipSessionListenerProxy$1;

    #@7
    invoke-direct {v0, p0, p1}, Lcom/android/server/sip/SipSessionListenerProxy$1;-><init>(Lcom/android/server/sip/SipSessionListenerProxy;Landroid/net/sip/ISipSession;)V

    #@a
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionListenerProxy;->proxy(Ljava/lang/Runnable;)V

    #@d
    goto :goto_4
.end method

.method public onError(Landroid/net/sip/ISipSession;ILjava/lang/String;)V
    .registers 5
    .parameter "session"
    .parameter "errorCode"
    .parameter "message"

    #@0
    .prologue
    .line 156
    iget-object v0, p0, Lcom/android/server/sip/SipSessionListenerProxy;->mListener:Landroid/net/sip/ISipSessionListener;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 166
    :goto_4
    return-void

    #@5
    .line 157
    :cond_5
    new-instance v0, Lcom/android/server/sip/SipSessionListenerProxy$9;

    #@7
    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/server/sip/SipSessionListenerProxy$9;-><init>(Lcom/android/server/sip/SipSessionListenerProxy;Landroid/net/sip/ISipSession;ILjava/lang/String;)V

    #@a
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionListenerProxy;->proxy(Ljava/lang/Runnable;)V

    #@d
    goto :goto_4
.end method

.method public onRegistering(Landroid/net/sip/ISipSession;)V
    .registers 3
    .parameter "session"

    #@0
    .prologue
    .line 169
    iget-object v0, p0, Lcom/android/server/sip/SipSessionListenerProxy;->mListener:Landroid/net/sip/ISipSessionListener;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 179
    :goto_4
    return-void

    #@5
    .line 170
    :cond_5
    new-instance v0, Lcom/android/server/sip/SipSessionListenerProxy$10;

    #@7
    invoke-direct {v0, p0, p1}, Lcom/android/server/sip/SipSessionListenerProxy$10;-><init>(Lcom/android/server/sip/SipSessionListenerProxy;Landroid/net/sip/ISipSession;)V

    #@a
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionListenerProxy;->proxy(Ljava/lang/Runnable;)V

    #@d
    goto :goto_4
.end method

.method public onRegistrationDone(Landroid/net/sip/ISipSession;I)V
    .registers 4
    .parameter "session"
    .parameter "duration"

    #@0
    .prologue
    .line 183
    iget-object v0, p0, Lcom/android/server/sip/SipSessionListenerProxy;->mListener:Landroid/net/sip/ISipSessionListener;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 193
    :goto_4
    return-void

    #@5
    .line 184
    :cond_5
    new-instance v0, Lcom/android/server/sip/SipSessionListenerProxy$11;

    #@7
    invoke-direct {v0, p0, p1, p2}, Lcom/android/server/sip/SipSessionListenerProxy$11;-><init>(Lcom/android/server/sip/SipSessionListenerProxy;Landroid/net/sip/ISipSession;I)V

    #@a
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionListenerProxy;->proxy(Ljava/lang/Runnable;)V

    #@d
    goto :goto_4
.end method

.method public onRegistrationFailed(Landroid/net/sip/ISipSession;ILjava/lang/String;)V
    .registers 5
    .parameter "session"
    .parameter "errorCode"
    .parameter "message"

    #@0
    .prologue
    .line 197
    iget-object v0, p0, Lcom/android/server/sip/SipSessionListenerProxy;->mListener:Landroid/net/sip/ISipSessionListener;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 207
    :goto_4
    return-void

    #@5
    .line 198
    :cond_5
    new-instance v0, Lcom/android/server/sip/SipSessionListenerProxy$12;

    #@7
    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/server/sip/SipSessionListenerProxy$12;-><init>(Lcom/android/server/sip/SipSessionListenerProxy;Landroid/net/sip/ISipSession;ILjava/lang/String;)V

    #@a
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionListenerProxy;->proxy(Ljava/lang/Runnable;)V

    #@d
    goto :goto_4
.end method

.method public onRegistrationTimeout(Landroid/net/sip/ISipSession;)V
    .registers 3
    .parameter "session"

    #@0
    .prologue
    .line 210
    iget-object v0, p0, Lcom/android/server/sip/SipSessionListenerProxy;->mListener:Landroid/net/sip/ISipSessionListener;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 220
    :goto_4
    return-void

    #@5
    .line 211
    :cond_5
    new-instance v0, Lcom/android/server/sip/SipSessionListenerProxy$13;

    #@7
    invoke-direct {v0, p0, p1}, Lcom/android/server/sip/SipSessionListenerProxy$13;-><init>(Lcom/android/server/sip/SipSessionListenerProxy;Landroid/net/sip/ISipSession;)V

    #@a
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionListenerProxy;->proxy(Ljava/lang/Runnable;)V

    #@d
    goto :goto_4
.end method

.method public onRinging(Landroid/net/sip/ISipSession;Landroid/net/sip/SipProfile;Ljava/lang/String;)V
    .registers 5
    .parameter "session"
    .parameter "caller"
    .parameter "sessionDescription"

    #@0
    .prologue
    .line 61
    iget-object v0, p0, Lcom/android/server/sip/SipSessionListenerProxy;->mListener:Landroid/net/sip/ISipSessionListener;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 71
    :goto_4
    return-void

    #@5
    .line 62
    :cond_5
    new-instance v0, Lcom/android/server/sip/SipSessionListenerProxy$2;

    #@7
    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/server/sip/SipSessionListenerProxy$2;-><init>(Lcom/android/server/sip/SipSessionListenerProxy;Landroid/net/sip/ISipSession;Landroid/net/sip/SipProfile;Ljava/lang/String;)V

    #@a
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionListenerProxy;->proxy(Ljava/lang/Runnable;)V

    #@d
    goto :goto_4
.end method

.method public onRingingBack(Landroid/net/sip/ISipSession;)V
    .registers 3
    .parameter "session"

    #@0
    .prologue
    .line 74
    iget-object v0, p0, Lcom/android/server/sip/SipSessionListenerProxy;->mListener:Landroid/net/sip/ISipSessionListener;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 84
    :goto_4
    return-void

    #@5
    .line 75
    :cond_5
    new-instance v0, Lcom/android/server/sip/SipSessionListenerProxy$3;

    #@7
    invoke-direct {v0, p0, p1}, Lcom/android/server/sip/SipSessionListenerProxy$3;-><init>(Lcom/android/server/sip/SipSessionListenerProxy;Landroid/net/sip/ISipSession;)V

    #@a
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionListenerProxy;->proxy(Ljava/lang/Runnable;)V

    #@d
    goto :goto_4
.end method

.method public setListener(Landroid/net/sip/ISipSessionListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 32
    iput-object p1, p0, Lcom/android/server/sip/SipSessionListenerProxy;->mListener:Landroid/net/sip/ISipSessionListener;

    #@2
    .line 33
    return-void
.end method
