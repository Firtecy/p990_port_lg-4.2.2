.class Lcom/android/server/sip/SipHelper;
.super Ljava/lang/Object;
.source "SipHelper.java"


# static fields
.field private static final DEBUG:Z

.field private static final DEBUG_PING:Z

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAddressFactory:Ljavax/sip/address/AddressFactory;

.field private mHeaderFactory:Ljavax/sip/header/HeaderFactory;

.field private mMessageFactory:Ljavax/sip/message/MessageFactory;

.field private mSipProvider:Ljavax/sip/SipProvider;

.field private mSipStack:Ljavax/sip/SipStack;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 75
    const-class v0, Lcom/android/server/sip/SipHelper;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/android/server/sip/SipHelper;->TAG:Ljava/lang/String;

    #@8
    return-void
.end method

.method public constructor <init>(Ljavax/sip/SipStack;Ljavax/sip/SipProvider;)V
    .registers 5
    .parameter "sipStack"
    .parameter "sipProvider"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/PeerUnavailableException;
        }
    .end annotation

    #@0
    .prologue
    .line 86
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 87
    iput-object p1, p0, Lcom/android/server/sip/SipHelper;->mSipStack:Ljavax/sip/SipStack;

    #@5
    .line 88
    iput-object p2, p0, Lcom/android/server/sip/SipHelper;->mSipProvider:Ljavax/sip/SipProvider;

    #@7
    .line 90
    invoke-static {}, Ljavax/sip/SipFactory;->getInstance()Ljavax/sip/SipFactory;

    #@a
    move-result-object v0

    #@b
    .line 91
    .local v0, sipFactory:Ljavax/sip/SipFactory;
    invoke-virtual {v0}, Ljavax/sip/SipFactory;->createAddressFactory()Ljavax/sip/address/AddressFactory;

    #@e
    move-result-object v1

    #@f
    iput-object v1, p0, Lcom/android/server/sip/SipHelper;->mAddressFactory:Ljavax/sip/address/AddressFactory;

    #@11
    .line 92
    invoke-virtual {v0}, Ljavax/sip/SipFactory;->createHeaderFactory()Ljavax/sip/header/HeaderFactory;

    #@14
    move-result-object v1

    #@15
    iput-object v1, p0, Lcom/android/server/sip/SipHelper;->mHeaderFactory:Ljavax/sip/header/HeaderFactory;

    #@17
    .line 93
    invoke-virtual {v0}, Ljavax/sip/SipFactory;->createMessageFactory()Ljavax/sip/message/MessageFactory;

    #@1a
    move-result-object v1

    #@1b
    iput-object v1, p0, Lcom/android/server/sip/SipHelper;->mMessageFactory:Ljavax/sip/message/MessageFactory;

    #@1d
    .line 94
    return-void
.end method

.method private createCSeqHeader(Ljava/lang/String;)Ljavax/sip/header/CSeqHeader;
    .registers 8
    .parameter "method"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;,
            Ljavax/sip/InvalidArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 116
    invoke-static {}, Ljava/lang/Math;->random()D

    #@3
    move-result-wide v2

    #@4
    const-wide v4, 0x40c3880000000000L

    #@9
    mul-double/2addr v2, v4

    #@a
    double-to-long v0, v2

    #@b
    .line 117
    .local v0, sequence:J
    iget-object v2, p0, Lcom/android/server/sip/SipHelper;->mHeaderFactory:Ljavax/sip/header/HeaderFactory;

    #@d
    invoke-interface {v2, v0, v1, p1}, Ljavax/sip/header/HeaderFactory;->createCSeqHeader(JLjava/lang/String;)Ljavax/sip/header/CSeqHeader;

    #@10
    move-result-object v2

    #@11
    return-object v2
.end method

.method private createCallIdHeader()Ljavax/sip/header/CallIdHeader;
    .registers 2

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/server/sip/SipHelper;->mSipProvider:Ljavax/sip/SipProvider;

    #@2
    invoke-interface {v0}, Ljavax/sip/SipProvider;->getNewCallId()Ljavax/sip/header/CallIdHeader;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method private createContactHeader(Landroid/net/sip/SipProfile;)Ljavax/sip/header/ContactHeader;
    .registers 4
    .parameter "profile"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;,
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 156
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    invoke-direct {p0, p1, v0, v1}, Lcom/android/server/sip/SipHelper;->createContactHeader(Landroid/net/sip/SipProfile;Ljava/lang/String;I)Ljavax/sip/header/ContactHeader;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method private createContactHeader(Landroid/net/sip/SipProfile;Ljava/lang/String;I)Ljavax/sip/header/ContactHeader;
    .registers 9
    .parameter "profile"
    .parameter "ip"
    .parameter "port"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;,
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 162
    if-nez p2, :cond_26

    #@2
    invoke-virtual {p1}, Landroid/net/sip/SipProfile;->getUserName()Ljava/lang/String;

    #@5
    move-result-object v2

    #@6
    invoke-virtual {p1}, Landroid/net/sip/SipProfile;->getProtocol()Ljava/lang/String;

    #@9
    move-result-object v3

    #@a
    invoke-direct {p0}, Lcom/android/server/sip/SipHelper;->getListeningPoint()Ljavax/sip/ListeningPoint;

    #@d
    move-result-object v4

    #@e
    invoke-direct {p0, v2, v3, v4}, Lcom/android/server/sip/SipHelper;->createSipUri(Ljava/lang/String;Ljava/lang/String;Ljavax/sip/ListeningPoint;)Ljavax/sip/address/SipURI;

    #@11
    move-result-object v1

    #@12
    .line 168
    .local v1, contactURI:Ljavax/sip/address/SipURI;
    :goto_12
    iget-object v2, p0, Lcom/android/server/sip/SipHelper;->mAddressFactory:Ljavax/sip/address/AddressFactory;

    #@14
    invoke-interface {v2, v1}, Ljavax/sip/address/AddressFactory;->createAddress(Ljavax/sip/address/URI;)Ljavax/sip/address/Address;

    #@17
    move-result-object v0

    #@18
    .line 169
    .local v0, contactAddress:Ljavax/sip/address/Address;
    invoke-virtual {p1}, Landroid/net/sip/SipProfile;->getDisplayName()Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    invoke-interface {v0, v2}, Ljavax/sip/address/Address;->setDisplayName(Ljava/lang/String;)V

    #@1f
    .line 171
    iget-object v2, p0, Lcom/android/server/sip/SipHelper;->mHeaderFactory:Ljavax/sip/header/HeaderFactory;

    #@21
    invoke-interface {v2, v0}, Ljavax/sip/header/HeaderFactory;->createContactHeader(Ljavax/sip/address/Address;)Ljavax/sip/header/ContactHeader;

    #@24
    move-result-object v2

    #@25
    return-object v2

    #@26
    .line 162
    .end local v0           #contactAddress:Ljavax/sip/address/Address;
    .end local v1           #contactURI:Ljavax/sip/address/SipURI;
    :cond_26
    invoke-virtual {p1}, Landroid/net/sip/SipProfile;->getUserName()Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    invoke-virtual {p1}, Landroid/net/sip/SipProfile;->getProtocol()Ljava/lang/String;

    #@2d
    move-result-object v3

    #@2e
    invoke-direct {p0, v2, v3, p2, p3}, Lcom/android/server/sip/SipHelper;->createSipUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljavax/sip/address/SipURI;

    #@31
    move-result-object v1

    #@32
    goto :goto_12
.end method

.method private createFromHeader(Landroid/net/sip/SipProfile;Ljava/lang/String;)Ljavax/sip/header/FromHeader;
    .registers 5
    .parameter "profile"
    .parameter "tag"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    #@0
    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/server/sip/SipHelper;->mHeaderFactory:Ljavax/sip/header/HeaderFactory;

    #@2
    invoke-virtual {p1}, Landroid/net/sip/SipProfile;->getSipAddress()Ljavax/sip/address/Address;

    #@5
    move-result-object v1

    #@6
    invoke-interface {v0, v1, p2}, Ljavax/sip/header/HeaderFactory;->createFromHeader(Ljavax/sip/address/Address;Ljava/lang/String;)Ljavax/sip/header/FromHeader;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method private createMaxForwardsHeader()Ljavax/sip/header/MaxForwardsHeader;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/InvalidArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 122
    iget-object v0, p0, Lcom/android/server/sip/SipHelper;->mHeaderFactory:Ljavax/sip/header/HeaderFactory;

    #@2
    const/16 v1, 0x46

    #@4
    invoke-interface {v0, v1}, Ljavax/sip/header/HeaderFactory;->createMaxForwardsHeader(I)Ljavax/sip/header/MaxForwardsHeader;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method private createMaxForwardsHeader(I)Ljavax/sip/header/MaxForwardsHeader;
    .registers 3
    .parameter "max"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/InvalidArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 127
    iget-object v0, p0, Lcom/android/server/sip/SipHelper;->mHeaderFactory:Ljavax/sip/header/HeaderFactory;

    #@2
    invoke-interface {v0, p1}, Ljavax/sip/header/HeaderFactory;->createMaxForwardsHeader(I)Ljavax/sip/header/MaxForwardsHeader;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method private createRequest(Ljava/lang/String;Landroid/net/sip/SipProfile;Landroid/net/sip/SipProfile;Ljava/lang/String;)Ljavax/sip/message/Request;
    .registers 15
    .parameter "requestType"
    .parameter "caller"
    .parameter "callee"
    .parameter "tag"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;,
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 273
    invoke-direct {p0, p2, p4}, Lcom/android/server/sip/SipHelper;->createFromHeader(Landroid/net/sip/SipProfile;Ljava/lang/String;)Ljavax/sip/header/FromHeader;

    #@3
    move-result-object v5

    #@4
    .line 274
    .local v5, fromHeader:Ljavax/sip/header/FromHeader;
    invoke-direct {p0, p3}, Lcom/android/server/sip/SipHelper;->createToHeader(Landroid/net/sip/SipProfile;)Ljavax/sip/header/ToHeader;

    #@7
    move-result-object v6

    #@8
    .line 275
    .local v6, toHeader:Ljavax/sip/header/ToHeader;
    invoke-virtual {p3}, Landroid/net/sip/SipProfile;->getUri()Ljavax/sip/address/SipURI;

    #@b
    move-result-object v1

    #@c
    .line 276
    .local v1, requestURI:Ljavax/sip/address/SipURI;
    invoke-direct {p0}, Lcom/android/server/sip/SipHelper;->createViaHeaders()Ljava/util/List;

    #@f
    move-result-object v7

    #@10
    .line 277
    .local v7, viaHeaders:Ljava/util/List;,"Ljava/util/List<Ljavax/sip/header/ViaHeader;>;"
    invoke-direct {p0}, Lcom/android/server/sip/SipHelper;->createCallIdHeader()Ljavax/sip/header/CallIdHeader;

    #@13
    move-result-object v3

    #@14
    .line 278
    .local v3, callIdHeader:Ljavax/sip/header/CallIdHeader;
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipHelper;->createCSeqHeader(Ljava/lang/String;)Ljavax/sip/header/CSeqHeader;

    #@17
    move-result-object v4

    #@18
    .line 279
    .local v4, cSeqHeader:Ljavax/sip/header/CSeqHeader;
    invoke-direct {p0}, Lcom/android/server/sip/SipHelper;->createMaxForwardsHeader()Ljavax/sip/header/MaxForwardsHeader;

    #@1b
    move-result-object v8

    #@1c
    .line 281
    .local v8, maxForwards:Ljavax/sip/header/MaxForwardsHeader;
    iget-object v0, p0, Lcom/android/server/sip/SipHelper;->mMessageFactory:Ljavax/sip/message/MessageFactory;

    #@1e
    move-object v2, p1

    #@1f
    invoke-interface/range {v0 .. v8}, Ljavax/sip/message/MessageFactory;->createRequest(Ljavax/sip/address/URI;Ljava/lang/String;Ljavax/sip/header/CallIdHeader;Ljavax/sip/header/CSeqHeader;Ljavax/sip/header/FromHeader;Ljavax/sip/header/ToHeader;Ljava/util/List;Ljavax/sip/header/MaxForwardsHeader;)Ljavax/sip/message/Request;

    #@22
    move-result-object v9

    #@23
    .line 285
    .local v9, request:Ljavax/sip/message/Request;
    invoke-direct {p0, p2}, Lcom/android/server/sip/SipHelper;->createContactHeader(Landroid/net/sip/SipProfile;)Ljavax/sip/header/ContactHeader;

    #@26
    move-result-object v0

    #@27
    invoke-interface {v9, v0}, Ljavax/sip/message/Request;->addHeader(Ljavax/sip/header/Header;)V

    #@2a
    .line 286
    return-object v9
.end method

.method private createRequest(Ljava/lang/String;Landroid/net/sip/SipProfile;Ljava/lang/String;)Ljavax/sip/message/Request;
    .registers 19
    .parameter "requestType"
    .parameter "userProfile"
    .parameter "tag"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;,
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 237
    move-object/from16 v0, p2

    #@2
    move-object/from16 v1, p3

    #@4
    invoke-direct {p0, v0, v1}, Lcom/android/server/sip/SipHelper;->createFromHeader(Landroid/net/sip/SipProfile;Ljava/lang/String;)Ljavax/sip/header/FromHeader;

    #@7
    move-result-object v7

    #@8
    .line 238
    .local v7, fromHeader:Ljavax/sip/header/FromHeader;
    move-object/from16 v0, p2

    #@a
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipHelper;->createToHeader(Landroid/net/sip/SipProfile;)Ljavax/sip/header/ToHeader;

    #@d
    move-result-object v8

    #@e
    .line 240
    .local v8, toHeader:Ljavax/sip/header/ToHeader;
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    invoke-virtual/range {p2 .. p2}, Landroid/net/sip/SipProfile;->getUserName()Ljava/lang/String;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    const-string v4, "@"

    #@1d
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-static {v2}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    #@28
    move-result-object v11

    #@29
    .line 241
    .local v11, replaceStr:Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/sip/SipHelper;->mAddressFactory:Ljavax/sip/address/AddressFactory;

    #@2b
    invoke-virtual/range {p2 .. p2}, Landroid/net/sip/SipProfile;->getUriString()Ljava/lang/String;

    #@2e
    move-result-object v4

    #@2f
    const-string v14, ""

    #@31
    invoke-virtual {v4, v11, v14}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@34
    move-result-object v4

    #@35
    invoke-interface {v2, v4}, Ljavax/sip/address/AddressFactory;->createSipURI(Ljava/lang/String;)Ljavax/sip/address/SipURI;

    #@38
    move-result-object v3

    #@39
    .line 244
    .local v3, requestURI:Ljavax/sip/address/SipURI;
    invoke-direct {p0}, Lcom/android/server/sip/SipHelper;->createViaHeaders()Ljava/util/List;

    #@3c
    move-result-object v9

    #@3d
    .line 245
    .local v9, viaHeaders:Ljava/util/List;,"Ljava/util/List<Ljavax/sip/header/ViaHeader;>;"
    invoke-direct {p0}, Lcom/android/server/sip/SipHelper;->createCallIdHeader()Ljavax/sip/header/CallIdHeader;

    #@40
    move-result-object v5

    #@41
    .line 246
    .local v5, callIdHeader:Ljavax/sip/header/CallIdHeader;
    invoke-direct/range {p0 .. p1}, Lcom/android/server/sip/SipHelper;->createCSeqHeader(Ljava/lang/String;)Ljavax/sip/header/CSeqHeader;

    #@44
    move-result-object v6

    #@45
    .line 247
    .local v6, cSeqHeader:Ljavax/sip/header/CSeqHeader;
    invoke-direct {p0}, Lcom/android/server/sip/SipHelper;->createMaxForwardsHeader()Ljavax/sip/header/MaxForwardsHeader;

    #@48
    move-result-object v10

    #@49
    .line 248
    .local v10, maxForwards:Ljavax/sip/header/MaxForwardsHeader;
    iget-object v2, p0, Lcom/android/server/sip/SipHelper;->mMessageFactory:Ljavax/sip/message/MessageFactory;

    #@4b
    move-object/from16 v4, p1

    #@4d
    invoke-interface/range {v2 .. v10}, Ljavax/sip/message/MessageFactory;->createRequest(Ljavax/sip/address/URI;Ljava/lang/String;Ljavax/sip/header/CallIdHeader;Ljavax/sip/header/CSeqHeader;Ljavax/sip/header/FromHeader;Ljavax/sip/header/ToHeader;Ljava/util/List;Ljavax/sip/header/MaxForwardsHeader;)Ljavax/sip/message/Request;

    #@50
    move-result-object v12

    #@51
    .line 251
    .local v12, request:Ljavax/sip/message/Request;
    iget-object v2, p0, Lcom/android/server/sip/SipHelper;->mHeaderFactory:Ljavax/sip/header/HeaderFactory;

    #@53
    const-string v4, "User-Agent"

    #@55
    const-string v14, "SIPAUA/0.1.001"

    #@57
    invoke-interface {v2, v4, v14}, Ljavax/sip/header/HeaderFactory;->createHeader(Ljava/lang/String;Ljava/lang/String;)Ljavax/sip/header/Header;

    #@5a
    move-result-object v13

    #@5b
    .line 253
    .local v13, userAgentHeader:Ljavax/sip/header/Header;
    invoke-interface {v12, v13}, Ljavax/sip/message/Request;->addHeader(Ljavax/sip/header/Header;)V

    #@5e
    .line 254
    return-object v12
.end method

.method private createSipUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljavax/sip/address/SipURI;
    .registers 8
    .parameter "username"
    .parameter "transport"
    .parameter "ip"
    .parameter "port"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    #@0
    .prologue
    .line 187
    iget-object v2, p0, Lcom/android/server/sip/SipHelper;->mAddressFactory:Ljavax/sip/address/AddressFactory;

    #@2
    invoke-interface {v2, p1, p3}, Ljavax/sip/address/AddressFactory;->createSipURI(Ljava/lang/String;Ljava/lang/String;)Ljavax/sip/address/SipURI;

    #@5
    move-result-object v1

    #@6
    .line 189
    .local v1, uri:Ljavax/sip/address/SipURI;
    :try_start_6
    invoke-interface {v1, p4}, Ljavax/sip/address/SipURI;->setPort(I)V

    #@9
    .line 190
    invoke-interface {v1, p2}, Ljavax/sip/address/SipURI;->setTransportParam(Ljava/lang/String;)V
    :try_end_c
    .catch Ljavax/sip/InvalidArgumentException; {:try_start_6 .. :try_end_c} :catch_d

    #@c
    .line 194
    return-object v1

    #@d
    .line 191
    :catch_d
    move-exception v0

    #@e
    .line 192
    .local v0, e:Ljavax/sip/InvalidArgumentException;
    new-instance v2, Ljava/lang/RuntimeException;

    #@10
    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@13
    throw v2
.end method

.method private createSipUri(Ljava/lang/String;Ljava/lang/String;Ljavax/sip/ListeningPoint;)Ljavax/sip/address/SipURI;
    .registers 6
    .parameter "username"
    .parameter "transport"
    .parameter "lp"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    #@0
    .prologue
    .line 182
    invoke-interface {p3}, Ljavax/sip/ListeningPoint;->getIPAddress()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-interface {p3}, Ljavax/sip/ListeningPoint;->getPort()I

    #@7
    move-result v1

    #@8
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/server/sip/SipHelper;->createSipUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljavax/sip/address/SipURI;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method private createToHeader(Landroid/net/sip/SipProfile;)Ljavax/sip/header/ToHeader;
    .registers 3
    .parameter "profile"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    #@0
    .prologue
    .line 102
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/server/sip/SipHelper;->createToHeader(Landroid/net/sip/SipProfile;Ljava/lang/String;)Ljavax/sip/header/ToHeader;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method private createToHeader(Landroid/net/sip/SipProfile;Ljava/lang/String;)Ljavax/sip/header/ToHeader;
    .registers 5
    .parameter "profile"
    .parameter "tag"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    #@0
    .prologue
    .line 107
    iget-object v0, p0, Lcom/android/server/sip/SipHelper;->mHeaderFactory:Ljavax/sip/header/HeaderFactory;

    #@2
    invoke-virtual {p1}, Landroid/net/sip/SipProfile;->getSipAddress()Ljavax/sip/address/Address;

    #@5
    move-result-object v1

    #@6
    invoke-interface {v0, v1, p2}, Ljavax/sip/header/HeaderFactory;->createToHeader(Ljavax/sip/address/Address;Ljava/lang/String;)Ljavax/sip/header/ToHeader;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method private createViaHeaders()Ljava/util/List;
    .registers 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljavax/sip/header/ViaHeader;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;,
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 145
    new-instance v2, Ljava/util/ArrayList;

    #@2
    const/4 v3, 0x1

    #@3
    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    #@6
    .line 146
    .local v2, viaHeaders:Ljava/util/List;,"Ljava/util/List<Ljavax/sip/header/ViaHeader;>;"
    invoke-direct {p0}, Lcom/android/server/sip/SipHelper;->getListeningPoint()Ljavax/sip/ListeningPoint;

    #@9
    move-result-object v0

    #@a
    .line 147
    .local v0, lp:Ljavax/sip/ListeningPoint;
    iget-object v3, p0, Lcom/android/server/sip/SipHelper;->mHeaderFactory:Ljavax/sip/header/HeaderFactory;

    #@c
    invoke-interface {v0}, Ljavax/sip/ListeningPoint;->getIPAddress()Ljava/lang/String;

    #@f
    move-result-object v4

    #@10
    invoke-interface {v0}, Ljavax/sip/ListeningPoint;->getPort()I

    #@13
    move-result v5

    #@14
    invoke-interface {v0}, Ljavax/sip/ListeningPoint;->getTransport()Ljava/lang/String;

    #@17
    move-result-object v6

    #@18
    const/4 v7, 0x0

    #@19
    invoke-interface {v3, v4, v5, v6, v7}, Ljavax/sip/header/HeaderFactory;->createViaHeader(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Ljavax/sip/header/ViaHeader;

    #@1c
    move-result-object v1

    #@1d
    .line 149
    .local v1, viaHeader:Ljavax/sip/header/ViaHeader;
    invoke-interface {v1}, Ljavax/sip/header/ViaHeader;->setRPort()V

    #@20
    .line 150
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@23
    .line 151
    return-object v2
.end method

.method private createWildcardContactHeader()Ljavax/sip/header/ContactHeader;
    .registers 3

    #@0
    .prologue
    .line 175
    iget-object v1, p0, Lcom/android/server/sip/SipHelper;->mHeaderFactory:Ljavax/sip/header/HeaderFactory;

    #@2
    invoke-interface {v1}, Ljavax/sip/header/HeaderFactory;->createContactHeader()Ljavax/sip/header/ContactHeader;

    #@5
    move-result-object v0

    #@6
    .line 176
    .local v0, contactHeader:Ljavax/sip/header/ContactHeader;
    invoke-interface {v0}, Ljavax/sip/header/ContactHeader;->setWildCard()V

    #@9
    .line 177
    return-object v0
.end method

.method public static getCallId(Ljava/util/EventObject;)Ljava/lang/String;
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 497
    if-nez p0, :cond_4

    #@2
    const/4 v3, 0x0

    #@3
    .line 518
    :goto_3
    return-object v3

    #@4
    .line 498
    :cond_4
    instance-of v3, p0, Ljavax/sip/RequestEvent;

    #@6
    if-eqz v3, :cond_13

    #@8
    .line 499
    check-cast p0, Ljavax/sip/RequestEvent;

    #@a
    .end local p0
    invoke-virtual {p0}, Ljavax/sip/RequestEvent;->getRequest()Ljavax/sip/message/Request;

    #@d
    move-result-object v3

    #@e
    invoke-static {v3}, Lcom/android/server/sip/SipHelper;->getCallId(Ljavax/sip/message/Message;)Ljava/lang/String;

    #@11
    move-result-object v3

    #@12
    goto :goto_3

    #@13
    .line 500
    .restart local p0
    :cond_13
    instance-of v3, p0, Ljavax/sip/ResponseEvent;

    #@15
    if-eqz v3, :cond_22

    #@17
    .line 501
    check-cast p0, Ljavax/sip/ResponseEvent;

    #@19
    .end local p0
    invoke-virtual {p0}, Ljavax/sip/ResponseEvent;->getResponse()Ljavax/sip/message/Response;

    #@1c
    move-result-object v3

    #@1d
    invoke-static {v3}, Lcom/android/server/sip/SipHelper;->getCallId(Ljavax/sip/message/Message;)Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    goto :goto_3

    #@22
    .line 502
    .restart local p0
    :cond_22
    instance-of v3, p0, Ljavax/sip/DialogTerminatedEvent;

    #@24
    if-eqz v3, :cond_38

    #@26
    move-object v3, p0

    #@27
    .line 503
    check-cast v3, Ljavax/sip/DialogTerminatedEvent;

    #@29
    invoke-virtual {v3}, Ljavax/sip/DialogTerminatedEvent;->getDialog()Ljavax/sip/Dialog;

    #@2c
    move-result-object v0

    #@2d
    .line 504
    .local v0, dialog:Ljavax/sip/Dialog;
    check-cast p0, Ljavax/sip/DialogTerminatedEvent;

    #@2f
    .end local p0
    invoke-virtual {p0}, Ljavax/sip/DialogTerminatedEvent;->getDialog()Ljavax/sip/Dialog;

    #@32
    move-result-object v3

    #@33
    invoke-static {v3}, Lcom/android/server/sip/SipHelper;->getCallId(Ljavax/sip/Dialog;)Ljava/lang/String;

    #@36
    move-result-object v3

    #@37
    goto :goto_3

    #@38
    .line 505
    .end local v0           #dialog:Ljavax/sip/Dialog;
    .restart local p0
    :cond_38
    instance-of v3, p0, Ljavax/sip/TransactionTerminatedEvent;

    #@3a
    if-eqz v3, :cond_53

    #@3c
    move-object v1, p0

    #@3d
    .line 506
    check-cast v1, Ljavax/sip/TransactionTerminatedEvent;

    #@3f
    .line 507
    .local v1, e:Ljavax/sip/TransactionTerminatedEvent;
    invoke-virtual {v1}, Ljavax/sip/TransactionTerminatedEvent;->isServerTransaction()Z

    #@42
    move-result v3

    #@43
    if-eqz v3, :cond_4e

    #@45
    invoke-virtual {v1}, Ljavax/sip/TransactionTerminatedEvent;->getServerTransaction()Ljavax/sip/ServerTransaction;

    #@48
    move-result-object v3

    #@49
    :goto_49
    invoke-static {v3}, Lcom/android/server/sip/SipHelper;->getCallId(Ljavax/sip/Transaction;)Ljava/lang/String;

    #@4c
    move-result-object v3

    #@4d
    goto :goto_3

    #@4e
    :cond_4e
    invoke-virtual {v1}, Ljavax/sip/TransactionTerminatedEvent;->getClientTransaction()Ljavax/sip/ClientTransaction;

    #@51
    move-result-object v3

    #@52
    goto :goto_49

    #@53
    .line 511
    .end local v1           #e:Ljavax/sip/TransactionTerminatedEvent;
    :cond_53
    invoke-virtual {p0}, Ljava/util/EventObject;->getSource()Ljava/lang/Object;

    #@56
    move-result-object v2

    #@57
    .line 512
    .local v2, source:Ljava/lang/Object;
    instance-of v3, v2, Ljavax/sip/Transaction;

    #@59
    if-eqz v3, :cond_62

    #@5b
    .line 513
    check-cast v2, Ljavax/sip/Transaction;

    #@5d
    .end local v2           #source:Ljava/lang/Object;
    invoke-static {v2}, Lcom/android/server/sip/SipHelper;->getCallId(Ljavax/sip/Transaction;)Ljava/lang/String;

    #@60
    move-result-object v3

    #@61
    goto :goto_3

    #@62
    .line 514
    .restart local v2       #source:Ljava/lang/Object;
    :cond_62
    instance-of v3, v2, Ljavax/sip/Dialog;

    #@64
    if-eqz v3, :cond_6d

    #@66
    .line 515
    check-cast v2, Ljavax/sip/Dialog;

    #@68
    .end local v2           #source:Ljava/lang/Object;
    invoke-static {v2}, Lcom/android/server/sip/SipHelper;->getCallId(Ljavax/sip/Dialog;)Ljava/lang/String;

    #@6b
    move-result-object v3

    #@6c
    goto :goto_3

    #@6d
    .line 518
    .restart local v2       #source:Ljava/lang/Object;
    :cond_6d
    const-string v3, ""

    #@6f
    goto :goto_3
.end method

.method private static getCallId(Ljavax/sip/Dialog;)Ljava/lang/String;
    .registers 2
    .parameter "dialog"

    #@0
    .prologue
    .line 533
    invoke-interface {p0}, Ljavax/sip/Dialog;->getCallId()Ljavax/sip/header/CallIdHeader;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0}, Ljavax/sip/header/CallIdHeader;->getCallId()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public static getCallId(Ljavax/sip/Transaction;)Ljava/lang/String;
    .registers 2
    .parameter "transaction"

    #@0
    .prologue
    .line 522
    if-eqz p0, :cond_b

    #@2
    invoke-interface {p0}, Ljavax/sip/Transaction;->getRequest()Ljavax/sip/message/Request;

    #@5
    move-result-object v0

    #@6
    invoke-static {v0}, Lcom/android/server/sip/SipHelper;->getCallId(Ljavax/sip/message/Message;)Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    :goto_a
    return-object v0

    #@b
    :cond_b
    const-string v0, ""

    #@d
    goto :goto_a
.end method

.method private static getCallId(Ljavax/sip/message/Message;)Ljava/lang/String;
    .registers 3
    .parameter "message"

    #@0
    .prologue
    .line 527
    const-string v1, "Call-ID"

    #@2
    invoke-interface {p0, v1}, Ljavax/sip/message/Message;->getHeader(Ljava/lang/String;)Ljavax/sip/header/Header;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljavax/sip/header/CallIdHeader;

    #@8
    .line 529
    .local v0, callIdHeader:Ljavax/sip/header/CallIdHeader;
    invoke-interface {v0}, Ljavax/sip/header/CallIdHeader;->getCallId()Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    return-object v1
.end method

.method private getListeningPoint()Ljavax/sip/ListeningPoint;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 131
    iget-object v2, p0, Lcom/android/server/sip/SipHelper;->mSipProvider:Ljavax/sip/SipProvider;

    #@2
    const-string v3, "UDP"

    #@4
    invoke-interface {v2, v3}, Ljavax/sip/SipProvider;->getListeningPoint(Ljava/lang/String;)Ljavax/sip/ListeningPoint;

    #@7
    move-result-object v0

    #@8
    .line 132
    .local v0, lp:Ljavax/sip/ListeningPoint;
    if-nez v0, :cond_12

    #@a
    iget-object v2, p0, Lcom/android/server/sip/SipHelper;->mSipProvider:Ljavax/sip/SipProvider;

    #@c
    const-string v3, "TCP"

    #@e
    invoke-interface {v2, v3}, Ljavax/sip/SipProvider;->getListeningPoint(Ljava/lang/String;)Ljavax/sip/ListeningPoint;

    #@11
    move-result-object v0

    #@12
    .line 133
    :cond_12
    if-nez v0, :cond_22

    #@14
    .line 134
    iget-object v2, p0, Lcom/android/server/sip/SipHelper;->mSipProvider:Ljavax/sip/SipProvider;

    #@16
    invoke-interface {v2}, Ljavax/sip/SipProvider;->getListeningPoints()[Ljavax/sip/ListeningPoint;

    #@19
    move-result-object v1

    #@1a
    .line 135
    .local v1, lps:[Ljavax/sip/ListeningPoint;
    if-eqz v1, :cond_22

    #@1c
    array-length v2, v1

    #@1d
    if-lez v2, :cond_22

    #@1f
    const/4 v2, 0x0

    #@20
    aget-object v0, v1, v2

    #@22
    .line 137
    .end local v1           #lps:[Ljavax/sip/ListeningPoint;
    :cond_22
    if-nez v0, :cond_2d

    #@24
    .line 138
    new-instance v2, Ljavax/sip/SipException;

    #@26
    const-string/jumbo v3, "no listening point is available"

    #@29
    invoke-direct {v2, v3}, Ljavax/sip/SipException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v2

    #@2d
    .line 140
    :cond_2d
    return-object v0
.end method


# virtual methods
.method public getServerTransaction(Ljavax/sip/RequestEvent;)Ljavax/sip/ServerTransaction;
    .registers 5
    .parameter "event"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 339
    invoke-virtual {p1}, Ljavax/sip/RequestEvent;->getServerTransaction()Ljavax/sip/ServerTransaction;

    #@3
    move-result-object v1

    #@4
    .line 340
    .local v1, transaction:Ljavax/sip/ServerTransaction;
    if-nez v1, :cond_10

    #@6
    .line 341
    invoke-virtual {p1}, Ljavax/sip/RequestEvent;->getRequest()Ljavax/sip/message/Request;

    #@9
    move-result-object v0

    #@a
    .line 342
    .local v0, request:Ljavax/sip/message/Request;
    iget-object v2, p0, Lcom/android/server/sip/SipHelper;->mSipProvider:Ljavax/sip/SipProvider;

    #@c
    invoke-interface {v2, v0}, Ljavax/sip/SipProvider;->getNewServerTransaction(Ljavax/sip/message/Request;)Ljavax/sip/ServerTransaction;

    #@f
    move-result-object v1

    #@10
    .line 344
    .end local v0           #request:Ljavax/sip/message/Request;
    .end local v1           #transaction:Ljavax/sip/ServerTransaction;
    :cond_10
    return-object v1
.end method

.method public handleChallenge(Ljavax/sip/ResponseEvent;Lgov/nist/javax/sip/clientauthutils/AccountManager;)Ljavax/sip/ClientTransaction;
    .registers 9
    .parameter "responseEvent"
    .parameter "accountManager"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 259
    iget-object v3, p0, Lcom/android/server/sip/SipHelper;->mSipStack:Ljavax/sip/SipStack;

    #@2
    check-cast v3, Lgov/nist/javax/sip/SipStackExt;

    #@4
    iget-object v4, p0, Lcom/android/server/sip/SipHelper;->mHeaderFactory:Ljavax/sip/header/HeaderFactory;

    #@6
    invoke-interface {v3, p2, v4}, Lgov/nist/javax/sip/SipStackExt;->getAuthenticationHelper(Lgov/nist/javax/sip/clientauthutils/AccountManager;Ljavax/sip/header/HeaderFactory;)Lgov/nist/javax/sip/clientauthutils/AuthenticationHelper;

    #@9
    move-result-object v0

    #@a
    .line 262
    .local v0, authenticationHelper:Lgov/nist/javax/sip/clientauthutils/AuthenticationHelper;
    invoke-virtual {p1}, Ljavax/sip/ResponseEvent;->getClientTransaction()Ljavax/sip/ClientTransaction;

    #@d
    move-result-object v2

    #@e
    .line 263
    .local v2, tid:Ljavax/sip/ClientTransaction;
    invoke-virtual {p1}, Ljavax/sip/ResponseEvent;->getResponse()Ljavax/sip/message/Response;

    #@11
    move-result-object v3

    #@12
    iget-object v4, p0, Lcom/android/server/sip/SipHelper;->mSipProvider:Ljavax/sip/SipProvider;

    #@14
    const/4 v5, 0x5

    #@15
    invoke-interface {v0, v3, v2, v4, v5}, Lgov/nist/javax/sip/clientauthutils/AuthenticationHelper;->handleChallenge(Ljavax/sip/message/Response;Ljavax/sip/ClientTransaction;Ljavax/sip/SipProvider;I)Ljavax/sip/ClientTransaction;

    #@18
    move-result-object v1

    #@19
    .line 267
    .local v1, ct:Ljavax/sip/ClientTransaction;
    invoke-interface {v1}, Ljavax/sip/ClientTransaction;->sendRequest()V

    #@1c
    .line 268
    return-object v1
.end method

.method public sendBye(Ljavax/sip/Dialog;)V
    .registers 4
    .parameter "dialog"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 437
    const-string v1, "BYE"

    #@2
    invoke-interface {p1, v1}, Ljavax/sip/Dialog;->createRequest(Ljava/lang/String;)Ljavax/sip/message/Request;

    #@5
    move-result-object v0

    #@6
    .line 439
    .local v0, byeRequest:Ljavax/sip/message/Request;
    iget-object v1, p0, Lcom/android/server/sip/SipHelper;->mSipProvider:Ljavax/sip/SipProvider;

    #@8
    invoke-interface {v1, v0}, Ljavax/sip/SipProvider;->getNewClientTransaction(Ljavax/sip/message/Request;)Ljavax/sip/ClientTransaction;

    #@b
    move-result-object v1

    #@c
    invoke-interface {p1, v1}, Ljavax/sip/Dialog;->sendRequest(Ljavax/sip/ClientTransaction;)V

    #@f
    .line 440
    return-void
.end method

.method public sendCancel(Ljavax/sip/ClientTransaction;)V
    .registers 4
    .parameter "inviteTransaction"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 444
    invoke-interface {p1}, Ljavax/sip/ClientTransaction;->createCancel()Ljavax/sip/message/Request;

    #@3
    move-result-object v0

    #@4
    .line 446
    .local v0, cancelRequest:Ljavax/sip/message/Request;
    iget-object v1, p0, Lcom/android/server/sip/SipHelper;->mSipProvider:Ljavax/sip/SipProvider;

    #@6
    invoke-interface {v1, v0}, Ljavax/sip/SipProvider;->getNewClientTransaction(Ljavax/sip/message/Request;)Ljavax/sip/ClientTransaction;

    #@9
    move-result-object v1

    #@a
    invoke-interface {v1}, Ljavax/sip/ClientTransaction;->sendRequest()V

    #@d
    .line 447
    return-void
.end method

.method public sendInvite(Landroid/net/sip/SipProfile;Landroid/net/sip/SipProfile;Ljava/lang/String;Ljava/lang/String;Lgov/nist/javax/sip/header/extensions/ReferredByHeader;Ljava/lang/String;)Ljavax/sip/ClientTransaction;
    .registers 13
    .parameter "caller"
    .parameter "callee"
    .parameter "sessionDescription"
    .parameter "tag"
    .parameter "referredBy"
    .parameter "replaces"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 293
    :try_start_0
    const-string v3, "INVITE"

    #@2
    invoke-direct {p0, v3, p1, p2, p4}, Lcom/android/server/sip/SipHelper;->createRequest(Ljava/lang/String;Landroid/net/sip/SipProfile;Landroid/net/sip/SipProfile;Ljava/lang/String;)Ljavax/sip/message/Request;

    #@5
    move-result-object v2

    #@6
    .line 294
    .local v2, request:Ljavax/sip/message/Request;
    if-eqz p5, :cond_b

    #@8
    invoke-interface {v2, p5}, Ljavax/sip/message/Request;->addHeader(Ljavax/sip/header/Header;)V

    #@b
    .line 295
    :cond_b
    if-eqz p6, :cond_18

    #@d
    .line 296
    iget-object v3, p0, Lcom/android/server/sip/SipHelper;->mHeaderFactory:Ljavax/sip/header/HeaderFactory;

    #@f
    const-string v4, "Replaces"

    #@11
    invoke-interface {v3, v4, p6}, Ljavax/sip/header/HeaderFactory;->createHeader(Ljava/lang/String;Ljava/lang/String;)Ljavax/sip/header/Header;

    #@14
    move-result-object v3

    #@15
    invoke-interface {v2, v3}, Ljavax/sip/message/Request;->addHeader(Ljavax/sip/header/Header;)V

    #@18
    .line 299
    :cond_18
    iget-object v3, p0, Lcom/android/server/sip/SipHelper;->mHeaderFactory:Ljavax/sip/header/HeaderFactory;

    #@1a
    const-string v4, "application"

    #@1c
    const-string/jumbo v5, "sdp"

    #@1f
    invoke-interface {v3, v4, v5}, Ljavax/sip/header/HeaderFactory;->createContentTypeHeader(Ljava/lang/String;Ljava/lang/String;)Ljavax/sip/header/ContentTypeHeader;

    #@22
    move-result-object v3

    #@23
    invoke-interface {v2, p3, v3}, Ljavax/sip/message/Request;->setContent(Ljava/lang/Object;Ljavax/sip/header/ContentTypeHeader;)V

    #@26
    .line 302
    iget-object v3, p0, Lcom/android/server/sip/SipHelper;->mSipProvider:Ljavax/sip/SipProvider;

    #@28
    invoke-interface {v3, v2}, Ljavax/sip/SipProvider;->getNewClientTransaction(Ljavax/sip/message/Request;)Ljavax/sip/ClientTransaction;

    #@2b
    move-result-object v0

    #@2c
    .line 305
    .local v0, clientTransaction:Ljavax/sip/ClientTransaction;
    invoke-interface {v0}, Ljavax/sip/ClientTransaction;->sendRequest()V
    :try_end_2f
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_2f} :catch_30

    #@2f
    .line 306
    return-object v0

    #@30
    .line 307
    .end local v0           #clientTransaction:Ljavax/sip/ClientTransaction;
    .end local v2           #request:Ljavax/sip/message/Request;
    :catch_30
    move-exception v1

    #@31
    .line 308
    .local v1, e:Ljava/text/ParseException;
    new-instance v3, Ljavax/sip/SipException;

    #@33
    const-string/jumbo v4, "sendInvite()"

    #@36
    invoke-direct {v3, v4, v1}, Ljavax/sip/SipException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@39
    throw v3
.end method

.method public sendInviteAck(Ljavax/sip/ResponseEvent;Ljavax/sip/Dialog;)V
    .registers 8
    .parameter "event"
    .parameter "dialog"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 428
    invoke-virtual {p1}, Ljavax/sip/ResponseEvent;->getResponse()Ljavax/sip/message/Response;

    #@3
    move-result-object v3

    #@4
    .line 429
    .local v3, response:Ljavax/sip/message/Response;
    const-string v4, "CSeq"

    #@6
    invoke-interface {v3, v4}, Ljavax/sip/message/Response;->getHeader(Ljava/lang/String;)Ljavax/sip/header/Header;

    #@9
    move-result-object v4

    #@a
    check-cast v4, Ljavax/sip/header/CSeqHeader;

    #@c
    invoke-interface {v4}, Ljavax/sip/header/CSeqHeader;->getSeqNumber()J

    #@f
    move-result-wide v1

    #@10
    .line 431
    .local v1, cseq:J
    invoke-interface {p2, v1, v2}, Ljavax/sip/Dialog;->createAck(J)Ljavax/sip/message/Request;

    #@13
    move-result-object v0

    #@14
    .line 433
    .local v0, ack:Ljavax/sip/message/Request;
    invoke-interface {p2, v0}, Ljavax/sip/Dialog;->sendAck(Ljavax/sip/message/Request;)V

    #@17
    .line 434
    return-void
.end method

.method public sendInviteBusyHere(Ljavax/sip/RequestEvent;Ljavax/sip/ServerTransaction;)V
    .registers 8
    .parameter "event"
    .parameter "inviteTransaction"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 406
    :try_start_0
    invoke-virtual {p1}, Ljavax/sip/RequestEvent;->getRequest()Ljavax/sip/message/Request;

    #@3
    move-result-object v1

    #@4
    .line 407
    .local v1, request:Ljavax/sip/message/Request;
    iget-object v3, p0, Lcom/android/server/sip/SipHelper;->mMessageFactory:Ljavax/sip/message/MessageFactory;

    #@6
    const/16 v4, 0x1e6

    #@8
    invoke-interface {v3, v4, v1}, Ljavax/sip/message/MessageFactory;->createResponse(ILjavax/sip/message/Request;)Ljavax/sip/message/Response;

    #@b
    move-result-object v2

    #@c
    .line 410
    .local v2, response:Ljavax/sip/message/Response;
    if-nez p2, :cond_12

    #@e
    .line 411
    invoke-virtual {p0, p1}, Lcom/android/server/sip/SipHelper;->getServerTransaction(Ljavax/sip/RequestEvent;)Ljavax/sip/ServerTransaction;

    #@11
    move-result-object p2

    #@12
    .line 414
    :cond_12
    invoke-interface {p2}, Ljavax/sip/ServerTransaction;->getState()Ljavax/sip/TransactionState;

    #@15
    move-result-object v3

    #@16
    sget-object v4, Ljavax/sip/TransactionState;->COMPLETED:Ljavax/sip/TransactionState;

    #@18
    if-eq v3, v4, :cond_1d

    #@1a
    .line 416
    invoke-interface {p2, v2}, Ljavax/sip/ServerTransaction;->sendResponse(Ljavax/sip/message/Response;)V
    :try_end_1d
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_1d} :catch_1e

    #@1d
    .line 421
    :cond_1d
    return-void

    #@1e
    .line 418
    .end local v1           #request:Ljavax/sip/message/Request;
    .end local v2           #response:Ljavax/sip/message/Response;
    :catch_1e
    move-exception v0

    #@1f
    .line 419
    .local v0, e:Ljava/text/ParseException;
    new-instance v3, Ljavax/sip/SipException;

    #@21
    const-string/jumbo v4, "sendInviteBusyHere()"

    #@24
    invoke-direct {v3, v4, v0}, Ljavax/sip/SipException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@27
    throw v3
.end method

.method public sendInviteOk(Ljavax/sip/RequestEvent;Landroid/net/sip/SipProfile;Ljava/lang/String;Ljavax/sip/ServerTransaction;Ljava/lang/String;I)Ljavax/sip/ServerTransaction;
    .registers 13
    .parameter "event"
    .parameter "localProfile"
    .parameter "sessionDescription"
    .parameter "inviteTransaction"
    .parameter "externalIp"
    .parameter "externalPort"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 379
    :try_start_0
    invoke-virtual {p1}, Ljavax/sip/RequestEvent;->getRequest()Ljavax/sip/message/Request;

    #@3
    move-result-object v1

    #@4
    .line 380
    .local v1, request:Ljavax/sip/message/Request;
    iget-object v3, p0, Lcom/android/server/sip/SipHelper;->mMessageFactory:Ljavax/sip/message/MessageFactory;

    #@6
    const/16 v4, 0xc8

    #@8
    invoke-interface {v3, v4, v1}, Ljavax/sip/message/MessageFactory;->createResponse(ILjavax/sip/message/Request;)Ljavax/sip/message/Response;

    #@b
    move-result-object v2

    #@c
    .line 382
    .local v2, response:Ljavax/sip/message/Response;
    invoke-direct {p0, p2, p5, p6}, Lcom/android/server/sip/SipHelper;->createContactHeader(Landroid/net/sip/SipProfile;Ljava/lang/String;I)Ljavax/sip/header/ContactHeader;

    #@f
    move-result-object v3

    #@10
    invoke-interface {v2, v3}, Ljavax/sip/message/Response;->addHeader(Ljavax/sip/header/Header;)V

    #@13
    .line 384
    iget-object v3, p0, Lcom/android/server/sip/SipHelper;->mHeaderFactory:Ljavax/sip/header/HeaderFactory;

    #@15
    const-string v4, "application"

    #@17
    const-string/jumbo v5, "sdp"

    #@1a
    invoke-interface {v3, v4, v5}, Ljavax/sip/header/HeaderFactory;->createContentTypeHeader(Ljava/lang/String;Ljava/lang/String;)Ljavax/sip/header/ContentTypeHeader;

    #@1d
    move-result-object v3

    #@1e
    invoke-interface {v2, p3, v3}, Ljavax/sip/message/Response;->setContent(Ljava/lang/Object;Ljavax/sip/header/ContentTypeHeader;)V

    #@21
    .line 388
    if-nez p4, :cond_27

    #@23
    .line 389
    invoke-virtual {p0, p1}, Lcom/android/server/sip/SipHelper;->getServerTransaction(Ljavax/sip/RequestEvent;)Ljavax/sip/ServerTransaction;

    #@26
    move-result-object p4

    #@27
    .line 392
    :cond_27
    invoke-interface {p4}, Ljavax/sip/ServerTransaction;->getState()Ljavax/sip/TransactionState;

    #@2a
    move-result-object v3

    #@2b
    sget-object v4, Ljavax/sip/TransactionState;->COMPLETED:Ljavax/sip/TransactionState;

    #@2d
    if-eq v3, v4, :cond_32

    #@2f
    .line 394
    invoke-interface {p4, v2}, Ljavax/sip/ServerTransaction;->sendResponse(Ljavax/sip/message/Response;)V
    :try_end_32
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_32} :catch_33

    #@32
    .line 397
    :cond_32
    return-object p4

    #@33
    .line 398
    .end local v1           #request:Ljavax/sip/message/Request;
    .end local v2           #response:Ljavax/sip/message/Response;
    :catch_33
    move-exception v0

    #@34
    .line 399
    .local v0, e:Ljava/text/ParseException;
    new-instance v3, Ljavax/sip/SipException;

    #@36
    const-string/jumbo v4, "sendInviteOk()"

    #@39
    invoke-direct {v3, v4, v0}, Ljavax/sip/SipException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@3c
    throw v3
.end method

.method public sendInviteRequestTerminated(Ljavax/sip/message/Request;Ljavax/sip/ServerTransaction;)V
    .registers 7
    .parameter "inviteRequest"
    .parameter "inviteTransaction"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 487
    :try_start_0
    iget-object v2, p0, Lcom/android/server/sip/SipHelper;->mMessageFactory:Ljavax/sip/message/MessageFactory;

    #@2
    const/16 v3, 0x1e7

    #@4
    invoke-interface {v2, v3, p1}, Ljavax/sip/message/MessageFactory;->createResponse(ILjavax/sip/message/Request;)Ljavax/sip/message/Response;

    #@7
    move-result-object v1

    #@8
    .line 490
    .local v1, response:Ljavax/sip/message/Response;
    invoke-interface {p2, v1}, Ljavax/sip/ServerTransaction;->sendResponse(Ljavax/sip/message/Response;)V
    :try_end_b
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_b} :catch_c

    #@b
    .line 494
    return-void

    #@c
    .line 491
    .end local v1           #response:Ljavax/sip/message/Response;
    :catch_c
    move-exception v0

    #@d
    .line 492
    .local v0, e:Ljava/text/ParseException;
    new-instance v2, Ljavax/sip/SipException;

    #@f
    const-string/jumbo v3, "sendInviteRequestTerminated()"

    #@12
    invoke-direct {v2, v3, v0}, Ljavax/sip/SipException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@15
    throw v2
.end method

.method public sendOptions(Landroid/net/sip/SipProfile;Landroid/net/sip/SipProfile;Ljava/lang/String;)Ljavax/sip/ClientTransaction;
    .registers 9
    .parameter "caller"
    .parameter "callee"
    .parameter "tag"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 200
    if-ne p1, p2, :cond_12

    #@2
    :try_start_2
    const-string v3, "OPTIONS"

    #@4
    invoke-direct {p0, v3, p1, p3}, Lcom/android/server/sip/SipHelper;->createRequest(Ljava/lang/String;Landroid/net/sip/SipProfile;Ljava/lang/String;)Ljavax/sip/message/Request;

    #@7
    move-result-object v2

    #@8
    .line 204
    .local v2, request:Ljavax/sip/message/Request;
    :goto_8
    iget-object v3, p0, Lcom/android/server/sip/SipHelper;->mSipProvider:Ljavax/sip/SipProvider;

    #@a
    invoke-interface {v3, v2}, Ljavax/sip/SipProvider;->getNewClientTransaction(Ljavax/sip/message/Request;)Ljavax/sip/ClientTransaction;

    #@d
    move-result-object v0

    #@e
    .line 206
    .local v0, clientTransaction:Ljavax/sip/ClientTransaction;
    invoke-interface {v0}, Ljavax/sip/ClientTransaction;->sendRequest()V

    #@11
    .line 207
    return-object v0

    #@12
    .line 200
    .end local v0           #clientTransaction:Ljavax/sip/ClientTransaction;
    .end local v2           #request:Ljavax/sip/message/Request;
    :cond_12
    const-string v3, "OPTIONS"

    #@14
    invoke-direct {p0, v3, p1, p2, p3}, Lcom/android/server/sip/SipHelper;->createRequest(Ljava/lang/String;Landroid/net/sip/SipProfile;Landroid/net/sip/SipProfile;Ljava/lang/String;)Ljavax/sip/message/Request;
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_17} :catch_19

    #@17
    move-result-object v2

    #@18
    goto :goto_8

    #@19
    .line 208
    :catch_19
    move-exception v1

    #@1a
    .line 209
    .local v1, e:Ljava/lang/Exception;
    new-instance v3, Ljavax/sip/SipException;

    #@1c
    const-string/jumbo v4, "sendOptions()"

    #@1f
    invoke-direct {v3, v4, v1}, Ljavax/sip/SipException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@22
    throw v3
.end method

.method public sendReferNotify(Ljavax/sip/Dialog;Ljava/lang/String;)V
    .registers 8
    .parameter "dialog"
    .parameter "content"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 468
    :try_start_0
    const-string v2, "NOTIFY"

    #@2
    invoke-interface {p1, v2}, Ljavax/sip/Dialog;->createRequest(Ljava/lang/String;)Ljavax/sip/message/Request;

    #@5
    move-result-object v1

    #@6
    .line 469
    .local v1, request:Ljavax/sip/message/Request;
    iget-object v2, p0, Lcom/android/server/sip/SipHelper;->mHeaderFactory:Ljavax/sip/header/HeaderFactory;

    #@8
    const-string v3, "active;expires=60"

    #@a
    invoke-interface {v2, v3}, Ljavax/sip/header/HeaderFactory;->createSubscriptionStateHeader(Ljava/lang/String;)Ljavax/sip/header/SubscriptionStateHeader;

    #@d
    move-result-object v2

    #@e
    invoke-interface {v1, v2}, Ljavax/sip/message/Request;->addHeader(Ljavax/sip/header/Header;)V

    #@11
    .line 472
    iget-object v2, p0, Lcom/android/server/sip/SipHelper;->mHeaderFactory:Ljavax/sip/header/HeaderFactory;

    #@13
    const-string/jumbo v3, "message"

    #@16
    const-string/jumbo v4, "sipfrag"

    #@19
    invoke-interface {v2, v3, v4}, Ljavax/sip/header/HeaderFactory;->createContentTypeHeader(Ljava/lang/String;Ljava/lang/String;)Ljavax/sip/header/ContentTypeHeader;

    #@1c
    move-result-object v2

    #@1d
    invoke-interface {v1, p2, v2}, Ljavax/sip/message/Request;->setContent(Ljava/lang/Object;Ljavax/sip/header/ContentTypeHeader;)V

    #@20
    .line 475
    iget-object v2, p0, Lcom/android/server/sip/SipHelper;->mHeaderFactory:Ljavax/sip/header/HeaderFactory;

    #@22
    const-string/jumbo v3, "refer"

    #@25
    invoke-interface {v2, v3}, Ljavax/sip/header/HeaderFactory;->createEventHeader(Ljava/lang/String;)Ljavax/sip/header/EventHeader;

    #@28
    move-result-object v2

    #@29
    invoke-interface {v1, v2}, Ljavax/sip/message/Request;->addHeader(Ljavax/sip/header/Header;)V

    #@2c
    .line 478
    iget-object v2, p0, Lcom/android/server/sip/SipHelper;->mSipProvider:Ljavax/sip/SipProvider;

    #@2e
    invoke-interface {v2, v1}, Ljavax/sip/SipProvider;->getNewClientTransaction(Ljavax/sip/message/Request;)Ljavax/sip/ClientTransaction;

    #@31
    move-result-object v2

    #@32
    invoke-interface {p1, v2}, Ljavax/sip/Dialog;->sendRequest(Ljavax/sip/ClientTransaction;)V
    :try_end_35
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_35} :catch_36

    #@35
    .line 482
    return-void

    #@36
    .line 479
    .end local v1           #request:Ljavax/sip/message/Request;
    :catch_36
    move-exception v0

    #@37
    .line 480
    .local v0, e:Ljava/text/ParseException;
    new-instance v2, Ljavax/sip/SipException;

    #@39
    const-string/jumbo v3, "sendReferNotify()"

    #@3c
    invoke-direct {v2, v3, v0}, Ljavax/sip/SipException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@3f
    throw v2
.end method

.method public sendRegister(Landroid/net/sip/SipProfile;Ljava/lang/String;I)Ljavax/sip/ClientTransaction;
    .registers 9
    .parameter "userProfile"
    .parameter "tag"
    .parameter "expiry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 216
    :try_start_0
    const-string v3, "REGISTER"

    #@2
    invoke-direct {p0, v3, p1, p2}, Lcom/android/server/sip/SipHelper;->createRequest(Ljava/lang/String;Landroid/net/sip/SipProfile;Ljava/lang/String;)Ljavax/sip/message/Request;

    #@5
    move-result-object v2

    #@6
    .line 217
    .local v2, request:Ljavax/sip/message/Request;
    if-nez p3, :cond_22

    #@8
    .line 220
    invoke-direct {p0}, Lcom/android/server/sip/SipHelper;->createWildcardContactHeader()Ljavax/sip/header/ContactHeader;

    #@b
    move-result-object v3

    #@c
    invoke-interface {v2, v3}, Ljavax/sip/message/Request;->addHeader(Ljavax/sip/header/Header;)V

    #@f
    .line 224
    :goto_f
    iget-object v3, p0, Lcom/android/server/sip/SipHelper;->mHeaderFactory:Ljavax/sip/header/HeaderFactory;

    #@11
    invoke-interface {v3, p3}, Ljavax/sip/header/HeaderFactory;->createExpiresHeader(I)Ljavax/sip/header/ExpiresHeader;

    #@14
    move-result-object v3

    #@15
    invoke-interface {v2, v3}, Ljavax/sip/message/Request;->addHeader(Ljavax/sip/header/Header;)V

    #@18
    .line 226
    iget-object v3, p0, Lcom/android/server/sip/SipHelper;->mSipProvider:Ljavax/sip/SipProvider;

    #@1a
    invoke-interface {v3, v2}, Ljavax/sip/SipProvider;->getNewClientTransaction(Ljavax/sip/message/Request;)Ljavax/sip/ClientTransaction;

    #@1d
    move-result-object v0

    #@1e
    .line 228
    .local v0, clientTransaction:Ljavax/sip/ClientTransaction;
    invoke-interface {v0}, Ljavax/sip/ClientTransaction;->sendRequest()V

    #@21
    .line 229
    return-object v0

    #@22
    .line 222
    .end local v0           #clientTransaction:Ljavax/sip/ClientTransaction;
    :cond_22
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipHelper;->createContactHeader(Landroid/net/sip/SipProfile;)Ljavax/sip/header/ContactHeader;

    #@25
    move-result-object v3

    #@26
    invoke-interface {v2, v3}, Ljavax/sip/message/Request;->addHeader(Ljavax/sip/header/Header;)V
    :try_end_29
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_29} :catch_2a

    #@29
    goto :goto_f

    #@2a
    .line 230
    .end local v2           #request:Ljavax/sip/message/Request;
    :catch_2a
    move-exception v1

    #@2b
    .line 231
    .local v1, e:Ljava/text/ParseException;
    new-instance v3, Ljavax/sip/SipException;

    #@2d
    const-string/jumbo v4, "sendRegister()"

    #@30
    invoke-direct {v3, v4, v1}, Ljavax/sip/SipException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@33
    throw v3
.end method

.method public sendReinvite(Ljavax/sip/Dialog;Ljava/lang/String;)Ljavax/sip/ClientTransaction;
    .registers 10
    .parameter "dialog"
    .parameter "sessionDescription"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 315
    :try_start_0
    const-string v4, "INVITE"

    #@2
    invoke-interface {p1, v4}, Ljavax/sip/Dialog;->createRequest(Ljava/lang/String;)Ljavax/sip/message/Request;

    #@5
    move-result-object v2

    #@6
    .line 316
    .local v2, request:Ljavax/sip/message/Request;
    iget-object v4, p0, Lcom/android/server/sip/SipHelper;->mHeaderFactory:Ljavax/sip/header/HeaderFactory;

    #@8
    const-string v5, "application"

    #@a
    const-string/jumbo v6, "sdp"

    #@d
    invoke-interface {v4, v5, v6}, Ljavax/sip/header/HeaderFactory;->createContentTypeHeader(Ljava/lang/String;Ljava/lang/String;)Ljavax/sip/header/ContentTypeHeader;

    #@10
    move-result-object v4

    #@11
    invoke-interface {v2, p2, v4}, Ljavax/sip/message/Request;->setContent(Ljava/lang/Object;Ljavax/sip/header/ContentTypeHeader;)V

    #@14
    .line 324
    const-string v4, "Via"

    #@16
    invoke-interface {v2, v4}, Ljavax/sip/message/Request;->getHeader(Ljava/lang/String;)Ljavax/sip/header/Header;

    #@19
    move-result-object v3

    #@1a
    check-cast v3, Ljavax/sip/header/ViaHeader;

    #@1c
    .line 325
    .local v3, viaHeader:Ljavax/sip/header/ViaHeader;
    if-eqz v3, :cond_21

    #@1e
    invoke-interface {v3}, Ljavax/sip/header/ViaHeader;->setRPort()V

    #@21
    .line 327
    :cond_21
    iget-object v4, p0, Lcom/android/server/sip/SipHelper;->mSipProvider:Ljavax/sip/SipProvider;

    #@23
    invoke-interface {v4, v2}, Ljavax/sip/SipProvider;->getNewClientTransaction(Ljavax/sip/message/Request;)Ljavax/sip/ClientTransaction;

    #@26
    move-result-object v0

    #@27
    .line 330
    .local v0, clientTransaction:Ljavax/sip/ClientTransaction;
    invoke-interface {p1, v0}, Ljavax/sip/Dialog;->sendRequest(Ljavax/sip/ClientTransaction;)V
    :try_end_2a
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_2a} :catch_2b

    #@2a
    .line 331
    return-object v0

    #@2b
    .line 332
    .end local v0           #clientTransaction:Ljavax/sip/ClientTransaction;
    .end local v2           #request:Ljavax/sip/message/Request;
    .end local v3           #viaHeader:Ljavax/sip/header/ViaHeader;
    :catch_2b
    move-exception v1

    #@2c
    .line 333
    .local v1, e:Ljava/text/ParseException;
    new-instance v4, Ljavax/sip/SipException;

    #@2e
    const-string/jumbo v5, "sendReinvite()"

    #@31
    invoke-direct {v4, v5, v1}, Ljavax/sip/SipException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@34
    throw v4
.end method

.method public sendResponse(Ljavax/sip/RequestEvent;I)V
    .registers 8
    .parameter "event"
    .parameter "responseCode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 452
    :try_start_0
    invoke-virtual {p1}, Ljavax/sip/RequestEvent;->getRequest()Ljavax/sip/message/Request;

    #@3
    move-result-object v1

    #@4
    .line 453
    .local v1, request:Ljavax/sip/message/Request;
    iget-object v3, p0, Lcom/android/server/sip/SipHelper;->mMessageFactory:Ljavax/sip/message/MessageFactory;

    #@6
    invoke-interface {v3, p2, v1}, Ljavax/sip/message/MessageFactory;->createResponse(ILjavax/sip/message/Request;)Ljavax/sip/message/Response;

    #@9
    move-result-object v2

    #@a
    .line 459
    .local v2, response:Ljavax/sip/message/Response;
    invoke-virtual {p0, p1}, Lcom/android/server/sip/SipHelper;->getServerTransaction(Ljavax/sip/RequestEvent;)Ljavax/sip/ServerTransaction;

    #@d
    move-result-object v3

    #@e
    invoke-interface {v3, v2}, Ljavax/sip/ServerTransaction;->sendResponse(Ljavax/sip/message/Response;)V
    :try_end_11
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_11} :catch_12

    #@11
    .line 463
    return-void

    #@12
    .line 460
    .end local v1           #request:Ljavax/sip/message/Request;
    .end local v2           #response:Ljavax/sip/message/Response;
    :catch_12
    move-exception v0

    #@13
    .line 461
    .local v0, e:Ljava/text/ParseException;
    new-instance v3, Ljavax/sip/SipException;

    #@15
    const-string/jumbo v4, "sendResponse()"

    #@18
    invoke-direct {v3, v4, v0}, Ljavax/sip/SipException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@1b
    throw v3
.end method

.method public sendRinging(Ljavax/sip/RequestEvent;Ljava/lang/String;)Ljavax/sip/ServerTransaction;
    .registers 10
    .parameter "event"
    .parameter "tag"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 354
    :try_start_0
    invoke-virtual {p1}, Ljavax/sip/RequestEvent;->getRequest()Ljavax/sip/message/Request;

    #@3
    move-result-object v1

    #@4
    .line 355
    .local v1, request:Ljavax/sip/message/Request;
    invoke-virtual {p0, p1}, Lcom/android/server/sip/SipHelper;->getServerTransaction(Ljavax/sip/RequestEvent;)Ljavax/sip/ServerTransaction;

    #@7
    move-result-object v4

    #@8
    .line 357
    .local v4, transaction:Ljavax/sip/ServerTransaction;
    iget-object v5, p0, Lcom/android/server/sip/SipHelper;->mMessageFactory:Ljavax/sip/message/MessageFactory;

    #@a
    const/16 v6, 0xb4

    #@c
    invoke-interface {v5, v6, v1}, Ljavax/sip/message/MessageFactory;->createResponse(ILjavax/sip/message/Request;)Ljavax/sip/message/Response;

    #@f
    move-result-object v2

    #@10
    .line 360
    .local v2, response:Ljavax/sip/message/Response;
    const-string v5, "To"

    #@12
    invoke-interface {v2, v5}, Ljavax/sip/message/Response;->getHeader(Ljava/lang/String;)Ljavax/sip/header/Header;

    #@15
    move-result-object v3

    #@16
    check-cast v3, Ljavax/sip/header/ToHeader;

    #@18
    .line 361
    .local v3, toHeader:Ljavax/sip/header/ToHeader;
    invoke-interface {v3, p2}, Ljavax/sip/header/ToHeader;->setTag(Ljava/lang/String;)V

    #@1b
    .line 362
    invoke-interface {v2, v3}, Ljavax/sip/message/Response;->addHeader(Ljavax/sip/header/Header;)V

    #@1e
    .line 364
    invoke-interface {v4, v2}, Ljavax/sip/ServerTransaction;->sendResponse(Ljavax/sip/message/Response;)V
    :try_end_21
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_21} :catch_22

    #@21
    .line 365
    return-object v4

    #@22
    .line 366
    .end local v1           #request:Ljavax/sip/message/Request;
    .end local v2           #response:Ljavax/sip/message/Response;
    .end local v3           #toHeader:Ljavax/sip/header/ToHeader;
    .end local v4           #transaction:Ljavax/sip/ServerTransaction;
    :catch_22
    move-exception v0

    #@23
    .line 367
    .local v0, e:Ljava/text/ParseException;
    new-instance v5, Ljavax/sip/SipException;

    #@25
    const-string/jumbo v6, "sendRinging()"

    #@28
    invoke-direct {v5, v6, v0}, Ljavax/sip/SipException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@2b
    throw v5
.end method
