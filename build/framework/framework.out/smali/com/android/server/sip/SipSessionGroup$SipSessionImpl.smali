.class Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
.super Landroid/net/sip/ISipSession$Stub;
.source "SipSessionGroup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/sip/SipSessionGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SipSessionImpl"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$KeepAliveProcess;,
        Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$SessionTimer;
    }
.end annotation


# instance fields
.field mAuthenticationRetryCount:I

.field mClientTransaction:Ljavax/sip/ClientTransaction;

.field mDialog:Ljavax/sip/Dialog;

.field mInCall:Z

.field mInviteReceived:Ljavax/sip/RequestEvent;

.field private mKeepAliveProcess:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$KeepAliveProcess;

.field private mKeepAliveSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

.field mPeerProfile:Landroid/net/sip/SipProfile;

.field mPeerSessionDescription:Ljava/lang/String;

.field mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

.field mReferSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

.field mReferredBy:Lgov/nist/javax/sip/header/extensions/ReferredByHeader;

.field mReplaces:Ljava/lang/String;

.field mServerTransaction:Ljavax/sip/ServerTransaction;

.field mSessionTimer:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$SessionTimer;

.field mState:I

.field final synthetic this$0:Lcom/android/server/sip/SipSessionGroup;


# direct methods
.method public constructor <init>(Lcom/android/server/sip/SipSessionGroup;Landroid/net/sip/ISipSessionListener;)V
    .registers 4
    .parameter
    .parameter "listener"

    #@0
    .prologue
    .line 581
    iput-object p1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@2
    invoke-direct {p0}, Landroid/net/sip/ISipSession$Stub;-><init>()V

    #@5
    .line 528
    new-instance v0, Lcom/android/server/sip/SipSessionListenerProxy;

    #@7
    invoke-direct {v0}, Lcom/android/server/sip/SipSessionListenerProxy;-><init>()V

    #@a
    iput-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@c
    .line 529
    const/4 v0, 0x0

    #@d
    iput v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mState:I

    #@f
    .line 582
    invoke-virtual {p0, p2}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->setListener(Landroid/net/sip/ISipSessionListener;)V

    #@12
    .line 583
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;ILjava/lang/String;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 526
    invoke-direct {p0, p1, p2}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->onError(ILjava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;Ljava/lang/Throwable;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 526
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->onError(Ljava/lang/Throwable;)V

    #@3
    return-void
.end method

.method static synthetic access$1500(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;Ljava/util/EventObject;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 526
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->processCommand(Ljava/util/EventObject;)V

    #@3
    return-void
.end method

.method static synthetic access$2800(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 526
    invoke-direct {p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->cancelSessionTimer()V

    #@3
    return-void
.end method

.method static synthetic access$2900(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 526
    invoke-direct {p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->reset()V

    #@3
    return-void
.end method

.method static synthetic access$3000(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 526
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->startSessionTimer(I)V

    #@3
    return-void
.end method

.method private cancelSessionTimer()V
    .registers 2

    #@0
    .prologue
    .line 1333
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mSessionTimer:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$SessionTimer;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 1334
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mSessionTimer:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$SessionTimer;

    #@6
    invoke-virtual {v0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$SessionTimer;->cancel()V

    #@9
    .line 1335
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mSessionTimer:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$SessionTimer;

    #@c
    .line 1337
    :cond_c
    return-void
.end method

.method private createErrorMessage(Ljavax/sip/message/Response;)Ljava/lang/String;
    .registers 6
    .parameter "response"

    #@0
    .prologue
    .line 1340
    const-string v0, "%s (%d)"

    #@2
    const/4 v1, 0x2

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-interface {p1}, Ljavax/sip/message/Response;->getReasonPhrase()Ljava/lang/String;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    invoke-interface {p1}, Ljavax/sip/message/Response;->getStatusCode()I

    #@10
    move-result v3

    #@11
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@14
    move-result-object v3

    #@15
    aput-object v3, v1, v2

    #@17
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    return-object v0
.end method

.method private crossDomainAuthenticationRequired(Ljavax/sip/message/Response;)Z
    .registers 5
    .parameter "response"

    #@0
    .prologue
    .line 980
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->getRealmFromResponse(Ljavax/sip/message/Response;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 981
    .local v0, realm:Ljava/lang/String;
    if-nez v0, :cond_8

    #@6
    const-string v0, ""

    #@8
    .line 982
    :cond_8
    iget-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@a
    invoke-static {v1}, Lcom/android/server/sip/SipSessionGroup;->access$1400(Lcom/android/server/sip/SipSessionGroup;)Landroid/net/sip/SipProfile;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1}, Landroid/net/sip/SipProfile;->getSipDomain()Ljava/lang/String;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v1

    #@1e
    if-nez v1, :cond_22

    #@20
    const/4 v1, 0x1

    #@21
    :goto_21
    return v1

    #@22
    :cond_22
    const/4 v1, 0x0

    #@23
    goto :goto_21
.end method

.method private doCommandAsync(Ljava/util/EventObject;)V
    .registers 5
    .parameter "command"

    #@0
    .prologue
    .line 664
    new-instance v0, Ljava/lang/Thread;

    #@2
    new-instance v1, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$1;

    #@4
    invoke-direct {v1, p0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$1;-><init>(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;Ljava/util/EventObject;)V

    #@7
    const-string v2, "SipSessionAsyncCmdThread"

    #@9
    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    #@c
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@f
    .line 676
    return-void
.end method

.method private enableKeepAlive()V
    .registers 6

    #@0
    .prologue
    .line 1345
    iget-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mKeepAliveSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@2
    if-eqz v1, :cond_14

    #@4
    .line 1346
    iget-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mKeepAliveSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@6
    invoke-virtual {v1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->stopKeepAliveProcess()V

    #@9
    .line 1351
    :goto_9
    :try_start_9
    iget-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mKeepAliveSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@b
    const/16 v2, 0xa

    #@d
    iget-object v3, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mPeerProfile:Landroid/net/sip/SipProfile;

    #@f
    const/4 v4, 0x0

    #@10
    invoke-virtual {v1, v2, v3, v4}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->startKeepAliveProcess(ILandroid/net/sip/SipProfile;Lcom/android/server/sip/SipSessionGroup$KeepAliveProcessCallback;)V
    :try_end_13
    .catch Ljavax/sip/SipException; {:try_start_9 .. :try_end_13} :catch_1b

    #@13
    .line 1357
    :goto_13
    return-void

    #@14
    .line 1348
    :cond_14
    invoke-virtual {p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->duplicate()Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@17
    move-result-object v1

    #@18
    iput-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mKeepAliveSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@1a
    goto :goto_9

    #@1b
    .line 1353
    :catch_1b
    move-exception v0

    #@1c
    .line 1354
    .local v0, e:Ljavax/sip/SipException;
    const-string v1, "SipSession"

    #@1e
    const-string/jumbo v2, "keepalive cannot be enabled; ignored"

    #@21
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@24
    .line 1355
    iget-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mKeepAliveSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@26
    invoke-virtual {v1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->stopKeepAliveProcess()V

    #@29
    goto :goto_13
.end method

.method private endCallNormally()V
    .registers 2

    #@0
    .prologue
    .line 1368
    invoke-direct {p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->reset()V

    #@3
    .line 1369
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@5
    invoke-virtual {v0, p0}, Lcom/android/server/sip/SipSessionListenerProxy;->onCallEnded(Landroid/net/sip/ISipSession;)V

    #@8
    .line 1370
    return-void
.end method

.method private endCallOnBusy()V
    .registers 2

    #@0
    .prologue
    .line 1378
    invoke-direct {p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->reset()V

    #@3
    .line 1379
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@5
    invoke-virtual {v0, p0}, Lcom/android/server/sip/SipSessionListenerProxy;->onCallBusy(Landroid/net/sip/ISipSession;)V

    #@8
    .line 1380
    return-void
.end method

.method private endCallOnError(ILjava/lang/String;)V
    .registers 4
    .parameter "errorCode"
    .parameter "message"

    #@0
    .prologue
    .line 1373
    invoke-direct {p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->reset()V

    #@3
    .line 1374
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@5
    invoke-virtual {v0, p0, p1, p2}, Lcom/android/server/sip/SipSessionListenerProxy;->onError(Landroid/net/sip/ISipSession;ILjava/lang/String;)V

    #@8
    .line 1375
    return-void
.end method

.method private endingCall(Ljava/util/EventObject;)Z
    .registers 7
    .parameter "evt"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1303
    const-string v4, "BYE"

    #@3
    invoke-static {v4, p1}, Lcom/android/server/sip/SipSessionGroup;->access$2100(Ljava/lang/String;Ljava/util/EventObject;)Z

    #@6
    move-result v4

    #@7
    if-eqz v4, :cond_25

    #@9
    move-object v0, p1

    #@a
    .line 1304
    check-cast v0, Ljavax/sip/ResponseEvent;

    #@c
    .line 1305
    .local v0, event:Ljavax/sip/ResponseEvent;
    invoke-virtual {v0}, Ljavax/sip/ResponseEvent;->getResponse()Ljavax/sip/message/Response;

    #@f
    move-result-object v1

    #@10
    .line 1307
    .local v1, response:Ljavax/sip/message/Response;
    invoke-interface {v1}, Ljavax/sip/message/Response;->getStatusCode()I

    #@13
    move-result v2

    #@14
    .line 1308
    .local v2, statusCode:I
    sparse-switch v2, :sswitch_data_28

    #@17
    .line 1317
    :cond_17
    invoke-direct {p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->cancelSessionTimer()V

    #@1a
    .line 1318
    invoke-direct {p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->reset()V

    #@1d
    .line 1321
    .end local v0           #event:Ljavax/sip/ResponseEvent;
    .end local v1           #response:Ljavax/sip/message/Response;
    .end local v2           #statusCode:I
    :goto_1d
    return v3

    #@1e
    .line 1311
    .restart local v0       #event:Ljavax/sip/ResponseEvent;
    .restart local v1       #response:Ljavax/sip/message/Response;
    .restart local v2       #statusCode:I
    :sswitch_1e
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->handleAuthentication(Ljavax/sip/ResponseEvent;)Z

    #@21
    move-result v4

    #@22
    if-eqz v4, :cond_17

    #@24
    goto :goto_1d

    #@25
    .line 1321
    .end local v0           #event:Ljavax/sip/ResponseEvent;
    .end local v1           #response:Ljavax/sip/message/Response;
    .end local v2           #statusCode:I
    :cond_25
    const/4 v3, 0x0

    #@26
    goto :goto_1d

    #@27
    .line 1308
    nop

    #@28
    :sswitch_data_28
    .sparse-switch
        0x191 -> :sswitch_1e
        0x197 -> :sswitch_1e
    .end sparse-switch
.end method

.method private establishCall(Z)V
    .registers 4
    .parameter "enableKeepAlive"

    #@0
    .prologue
    .line 1360
    const/16 v0, 0x8

    #@2
    iput v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mState:I

    #@4
    .line 1361
    invoke-direct {p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->cancelSessionTimer()V

    #@7
    .line 1362
    iget-boolean v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mInCall:Z

    #@9
    if-nez v0, :cond_10

    #@b
    if-eqz p1, :cond_10

    #@d
    invoke-direct {p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->enableKeepAlive()V

    #@10
    .line 1363
    :cond_10
    const/4 v0, 0x1

    #@11
    iput-boolean v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mInCall:Z

    #@13
    .line 1364
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@15
    iget-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mPeerSessionDescription:Ljava/lang/String;

    #@17
    invoke-virtual {v0, p0, v1}, Lcom/android/server/sip/SipSessionListenerProxy;->onCallEstablished(Landroid/net/sip/ISipSession;Ljava/lang/String;)V

    #@1a
    .line 1365
    return-void
.end method

.method private getAccountManager()Lgov/nist/javax/sip/clientauthutils/AccountManager;
    .registers 2

    #@0
    .prologue
    .line 986
    new-instance v0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$2;

    #@2
    invoke-direct {v0, p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$2;-><init>(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;)V

    #@5
    return-object v0
.end method

.method private getErrorCode(I)I
    .registers 3
    .parameter "responseStatusCode"

    #@0
    .prologue
    .line 1410
    sparse-switch p1, :sswitch_data_12

    #@3
    .line 1428
    const/16 v0, 0x1f4

    #@5
    if-ge p1, v0, :cond_f

    #@7
    .line 1429
    const/4 v0, -0x4

    #@8
    .line 1431
    :goto_8
    return v0

    #@9
    .line 1417
    :sswitch_9
    const/4 v0, -0x7

    #@a
    goto :goto_8

    #@b
    .line 1422
    :sswitch_b
    const/4 v0, -0x6

    #@c
    goto :goto_8

    #@d
    .line 1425
    :sswitch_d
    const/4 v0, -0x5

    #@e
    goto :goto_8

    #@f
    .line 1431
    :cond_f
    const/4 v0, -0x2

    #@10
    goto :goto_8

    #@11
    .line 1410
    nop

    #@12
    :sswitch_data_12
    .sparse-switch
        0x193 -> :sswitch_9
        0x194 -> :sswitch_9
        0x196 -> :sswitch_9
        0x198 -> :sswitch_d
        0x19a -> :sswitch_9
        0x19e -> :sswitch_b
        0x1e0 -> :sswitch_9
        0x1e4 -> :sswitch_b
        0x1e5 -> :sswitch_b
        0x1e8 -> :sswitch_9
    .end sparse-switch
.end method

.method private getErrorCode(Ljava/lang/Throwable;)I
    .registers 4
    .parameter "exception"

    #@0
    .prologue
    .line 1437
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 1438
    .local v0, message:Ljava/lang/String;
    instance-of v1, p1, Ljava/net/UnknownHostException;

    #@6
    if-eqz v1, :cond_b

    #@8
    .line 1439
    const/16 v1, -0xc

    #@a
    .line 1443
    :goto_a
    return v1

    #@b
    .line 1440
    :cond_b
    instance-of v1, p1, Ljava/io/IOException;

    #@d
    if-eqz v1, :cond_11

    #@f
    .line 1441
    const/4 v1, -0x1

    #@10
    goto :goto_a

    #@11
    .line 1443
    :cond_11
    const/4 v1, -0x4

    #@12
    goto :goto_a
.end method

.method private getExpiryTime(Ljavax/sip/message/Response;)I
    .registers 6
    .parameter "response"

    #@0
    .prologue
    .line 899
    const/4 v2, -0x1

    #@1
    .line 900
    .local v2, time:I
    const-string v3, "Contact"

    #@3
    invoke-interface {p1, v3}, Ljavax/sip/message/Response;->getHeader(Ljava/lang/String;)Ljavax/sip/header/Header;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Ljavax/sip/header/ContactHeader;

    #@9
    .line 901
    .local v0, contact:Ljavax/sip/header/ContactHeader;
    if-eqz v0, :cond_f

    #@b
    .line 902
    invoke-interface {v0}, Ljavax/sip/header/ContactHeader;->getExpires()I

    #@e
    move-result v2

    #@f
    .line 904
    :cond_f
    const-string v3, "Expires"

    #@11
    invoke-interface {p1, v3}, Ljavax/sip/message/Response;->getHeader(Ljava/lang/String;)Ljavax/sip/header/Header;

    #@14
    move-result-object v1

    #@15
    check-cast v1, Ljavax/sip/header/ExpiresHeader;

    #@17
    .line 905
    .local v1, expires:Ljavax/sip/header/ExpiresHeader;
    if-eqz v1, :cond_25

    #@19
    if-ltz v2, :cond_21

    #@1b
    invoke-interface {v1}, Ljavax/sip/header/ExpiresHeader;->getExpires()I

    #@1e
    move-result v3

    #@1f
    if-le v2, v3, :cond_25

    #@21
    .line 906
    :cond_21
    invoke-interface {v1}, Ljavax/sip/header/ExpiresHeader;->getExpires()I

    #@24
    move-result v2

    #@25
    .line 908
    :cond_25
    if-gtz v2, :cond_29

    #@27
    .line 909
    const/16 v2, 0xe10

    #@29
    .line 911
    :cond_29
    const-string v3, "Min-Expires"

    #@2b
    invoke-interface {p1, v3}, Ljavax/sip/message/Response;->getHeader(Ljava/lang/String;)Ljavax/sip/header/Header;

    #@2e
    move-result-object v1

    #@2f
    .end local v1           #expires:Ljavax/sip/header/ExpiresHeader;
    check-cast v1, Ljavax/sip/header/ExpiresHeader;

    #@31
    .line 912
    .restart local v1       #expires:Ljavax/sip/header/ExpiresHeader;
    if-eqz v1, :cond_3d

    #@33
    invoke-interface {v1}, Ljavax/sip/header/ExpiresHeader;->getExpires()I

    #@36
    move-result v3

    #@37
    if-ge v2, v3, :cond_3d

    #@39
    .line 913
    invoke-interface {v1}, Ljavax/sip/header/ExpiresHeader;->getExpires()I

    #@3c
    move-result v2

    #@3d
    .line 918
    :cond_3d
    return v2
.end method

.method private getNonceFromResponse(Ljavax/sip/message/Response;)Ljava/lang/String;
    .registers 5
    .parameter "response"

    #@0
    .prologue
    .line 1018
    const-string v2, "WWW-Authenticate"

    #@2
    invoke-interface {p1, v2}, Ljavax/sip/message/Response;->getHeader(Ljava/lang/String;)Ljavax/sip/header/Header;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Lgov/nist/javax/sip/header/WWWAuthenticate;

    #@8
    .line 1020
    .local v1, wwwAuth:Lgov/nist/javax/sip/header/WWWAuthenticate;
    if-eqz v1, :cond_f

    #@a
    invoke-virtual {v1}, Lgov/nist/javax/sip/header/WWWAuthenticate;->getNonce()Ljava/lang/String;

    #@d
    move-result-object v2

    #@e
    .line 1023
    :goto_e
    return-object v2

    #@f
    .line 1021
    :cond_f
    const-string v2, "Proxy-Authenticate"

    #@11
    invoke-interface {p1, v2}, Ljavax/sip/message/Response;->getHeader(Ljava/lang/String;)Ljavax/sip/header/Header;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Lgov/nist/javax/sip/header/ProxyAuthenticate;

    #@17
    .line 1023
    .local v0, proxyAuth:Lgov/nist/javax/sip/header/ProxyAuthenticate;
    if-nez v0, :cond_1b

    #@19
    const/4 v2, 0x0

    #@1a
    goto :goto_e

    #@1b
    :cond_1b
    invoke-virtual {v0}, Lgov/nist/javax/sip/header/ProxyAuthenticate;->getNonce()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    goto :goto_e
.end method

.method private getRealmFromResponse(Ljavax/sip/message/Response;)Ljava/lang/String;
    .registers 5
    .parameter "response"

    #@0
    .prologue
    .line 1009
    const-string v2, "WWW-Authenticate"

    #@2
    invoke-interface {p1, v2}, Ljavax/sip/message/Response;->getHeader(Ljava/lang/String;)Ljavax/sip/header/Header;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Lgov/nist/javax/sip/header/WWWAuthenticate;

    #@8
    .line 1011
    .local v1, wwwAuth:Lgov/nist/javax/sip/header/WWWAuthenticate;
    if-eqz v1, :cond_f

    #@a
    invoke-virtual {v1}, Lgov/nist/javax/sip/header/WWWAuthenticate;->getRealm()Ljava/lang/String;

    #@d
    move-result-object v2

    #@e
    .line 1014
    :goto_e
    return-object v2

    #@f
    .line 1012
    :cond_f
    const-string v2, "Proxy-Authenticate"

    #@11
    invoke-interface {p1, v2}, Ljavax/sip/message/Response;->getHeader(Ljava/lang/String;)Ljavax/sip/header/Header;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Lgov/nist/javax/sip/header/ProxyAuthenticate;

    #@17
    .line 1014
    .local v0, proxyAuth:Lgov/nist/javax/sip/header/ProxyAuthenticate;
    if-nez v0, :cond_1b

    #@19
    const/4 v2, 0x0

    #@1a
    goto :goto_e

    #@1b
    :cond_1b
    invoke-virtual {v0}, Lgov/nist/javax/sip/header/ProxyAuthenticate;->getRealm()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    goto :goto_e
.end method

.method private getResponseString(I)Ljava/lang/String;
    .registers 4
    .parameter "statusCode"

    #@0
    .prologue
    .line 1027
    new-instance v0, Lgov/nist/javax/sip/header/StatusLine;

    #@2
    invoke-direct {v0}, Lgov/nist/javax/sip/header/StatusLine;-><init>()V

    #@5
    .line 1028
    .local v0, statusLine:Lgov/nist/javax/sip/header/StatusLine;
    invoke-virtual {v0, p1}, Lgov/nist/javax/sip/header/StatusLine;->setStatusCode(I)V

    #@8
    .line 1029
    invoke-static {p1}, Lgov/nist/javax/sip/message/SIPResponse;->getReasonPhrase(I)Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v0, v1}, Lgov/nist/javax/sip/header/StatusLine;->setReasonPhrase(Ljava/lang/String;)V

    #@f
    .line 1030
    invoke-virtual {v0}, Lgov/nist/javax/sip/header/StatusLine;->encode()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    return-object v1
.end method

.method private getTransaction()Ljavax/sip/Transaction;
    .registers 2

    #@0
    .prologue
    .line 647
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mClientTransaction:Ljavax/sip/ClientTransaction;

    #@2
    if-eqz v0, :cond_7

    #@4
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mClientTransaction:Ljavax/sip/ClientTransaction;

    #@6
    .line 649
    :goto_6
    return-object v0

    #@7
    .line 648
    :cond_7
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mServerTransaction:Ljavax/sip/ServerTransaction;

    #@9
    if-eqz v0, :cond_e

    #@b
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mServerTransaction:Ljavax/sip/ServerTransaction;

    #@d
    goto :goto_6

    #@e
    .line 649
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_6
.end method

.method private handleAuthentication(Ljavax/sip/ResponseEvent;)Z
    .registers 7
    .parameter "event"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 951
    invoke-virtual {p1}, Ljavax/sip/ResponseEvent;->getResponse()Ljavax/sip/message/Response;

    #@4
    move-result-object v1

    #@5
    .line 952
    .local v1, response:Ljavax/sip/message/Response;
    invoke-direct {p0, v1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->getNonceFromResponse(Ljavax/sip/message/Response;)Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    .line 953
    .local v0, nonce:Ljava/lang/String;
    if-nez v0, :cond_13

    #@b
    .line 954
    const/4 v3, -0x2

    #@c
    const-string/jumbo v4, "server does not provide challenge"

    #@f
    invoke-direct {p0, v3, v4}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->onError(ILjava/lang/String;)V

    #@12
    .line 975
    :goto_12
    return v2

    #@13
    .line 957
    :cond_13
    iget v3, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mAuthenticationRetryCount:I

    #@15
    const/4 v4, 0x2

    #@16
    if-ge v3, v4, :cond_58

    #@18
    .line 958
    iget-object v2, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@1a
    invoke-static {v2}, Lcom/android/server/sip/SipSessionGroup;->access$300(Lcom/android/server/sip/SipSessionGroup;)Lcom/android/server/sip/SipHelper;

    #@1d
    move-result-object v2

    #@1e
    invoke-direct {p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->getAccountManager()Lgov/nist/javax/sip/clientauthutils/AccountManager;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v2, p1, v3}, Lcom/android/server/sip/SipHelper;->handleChallenge(Ljavax/sip/ResponseEvent;Lgov/nist/javax/sip/clientauthutils/AccountManager;)Ljavax/sip/ClientTransaction;

    #@25
    move-result-object v2

    #@26
    iput-object v2, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mClientTransaction:Ljavax/sip/ClientTransaction;

    #@28
    .line 960
    iget-object v2, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mClientTransaction:Ljavax/sip/ClientTransaction;

    #@2a
    invoke-interface {v2}, Ljavax/sip/ClientTransaction;->getDialog()Ljavax/sip/Dialog;

    #@2d
    move-result-object v2

    #@2e
    iput-object v2, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mDialog:Ljavax/sip/Dialog;

    #@30
    .line 961
    iget v2, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mAuthenticationRetryCount:I

    #@32
    add-int/lit8 v2, v2, 0x1

    #@34
    iput v2, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mAuthenticationRetryCount:I

    #@36
    .line 962
    invoke-static {p0, p1}, Lcom/android/server/sip/SipSessionGroup;->access$600(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;Ljava/util/EventObject;)Z

    #@39
    move-result v2

    #@3a
    if-eqz v2, :cond_56

    #@3c
    .line 963
    const-string v2, "SipSession"

    #@3e
    new-instance v3, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v4, "   authentication retry count="

    #@45
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v3

    #@49
    iget v4, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mAuthenticationRetryCount:I

    #@4b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v3

    #@4f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v3

    #@53
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 966
    :cond_56
    const/4 v2, 0x1

    #@57
    goto :goto_12

    #@58
    .line 968
    :cond_58
    invoke-direct {p0, v1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->crossDomainAuthenticationRequired(Ljavax/sip/message/Response;)Z

    #@5b
    move-result v3

    #@5c
    if-eqz v3, :cond_68

    #@5e
    .line 969
    const/16 v3, -0xb

    #@60
    invoke-direct {p0, v1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->getRealmFromResponse(Ljavax/sip/message/Response;)Ljava/lang/String;

    #@63
    move-result-object v4

    #@64
    invoke-direct {p0, v3, v4}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->onError(ILjava/lang/String;)V

    #@67
    goto :goto_12

    #@68
    .line 972
    :cond_68
    const/4 v3, -0x8

    #@69
    const-string v4, "incorrect username or password"

    #@6b
    invoke-direct {p0, v3, v4}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->onError(ILjava/lang/String;)V

    #@6e
    goto :goto_12
.end method

.method private inCall(Ljava/util/EventObject;)Z
    .registers 7
    .parameter "evt"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x3

    #@1
    const/4 v2, 0x1

    #@2
    .line 1268
    invoke-static {}, Lcom/android/server/sip/SipSessionGroup;->access$1700()Ljava/util/EventObject;

    #@5
    move-result-object v1

    #@6
    if-ne v1, p1, :cond_21

    #@8
    .line 1270
    const/16 v1, 0xa

    #@a
    iput v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mState:I

    #@c
    .line 1271
    iget-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@e
    invoke-static {v1}, Lcom/android/server/sip/SipSessionGroup;->access$300(Lcom/android/server/sip/SipSessionGroup;)Lcom/android/server/sip/SipHelper;

    #@11
    move-result-object v1

    #@12
    iget-object v3, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mDialog:Ljavax/sip/Dialog;

    #@14
    invoke-virtual {v1, v3}, Lcom/android/server/sip/SipHelper;->sendBye(Ljavax/sip/Dialog;)V

    #@17
    .line 1272
    iget-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@19
    invoke-virtual {v1, p0}, Lcom/android/server/sip/SipSessionListenerProxy;->onCallEnded(Landroid/net/sip/ISipSession;)V

    #@1c
    .line 1273
    invoke-direct {p0, v4}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->startSessionTimer(I)V

    #@1f
    move v1, v2

    #@20
    .line 1299
    .end local p1
    :goto_20
    return v1

    #@21
    .line 1275
    .restart local p1
    :cond_21
    const-string v1, "INVITE"

    #@23
    invoke-static {v1, p1}, Lcom/android/server/sip/SipSessionGroup;->access$800(Ljava/lang/String;Ljava/util/EventObject;)Z

    #@26
    move-result v1

    #@27
    if-eqz v1, :cond_4a

    #@29
    .line 1277
    iput v4, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mState:I

    #@2b
    move-object v0, p1

    #@2c
    .line 1278
    check-cast v0, Ljavax/sip/RequestEvent;

    #@2e
    iput-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mInviteReceived:Ljavax/sip/RequestEvent;

    #@30
    .line 1279
    .local v0, event:Ljavax/sip/RequestEvent;
    iget-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@32
    invoke-virtual {v0}, Ljavax/sip/RequestEvent;->getRequest()Ljavax/sip/message/Request;

    #@35
    move-result-object v3

    #@36
    invoke-static {v1, v3}, Lcom/android/server/sip/SipSessionGroup;->access$2500(Lcom/android/server/sip/SipSessionGroup;Ljavax/sip/message/Message;)Ljava/lang/String;

    #@39
    move-result-object v1

    #@3a
    iput-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mPeerSessionDescription:Ljava/lang/String;

    #@3c
    .line 1280
    const/4 v1, 0x0

    #@3d
    iput-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mServerTransaction:Ljavax/sip/ServerTransaction;

    #@3f
    .line 1281
    iget-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@41
    iget-object v3, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mPeerProfile:Landroid/net/sip/SipProfile;

    #@43
    iget-object v4, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mPeerSessionDescription:Ljava/lang/String;

    #@45
    invoke-virtual {v1, p0, v3, v4}, Lcom/android/server/sip/SipSessionListenerProxy;->onRinging(Landroid/net/sip/ISipSession;Landroid/net/sip/SipProfile;Ljava/lang/String;)V

    #@48
    move v1, v2

    #@49
    .line 1282
    goto :goto_20

    #@4a
    .line 1283
    .end local v0           #event:Ljavax/sip/RequestEvent;
    :cond_4a
    const-string v1, "BYE"

    #@4c
    invoke-static {v1, p1}, Lcom/android/server/sip/SipSessionGroup;->access$800(Ljava/lang/String;Ljava/util/EventObject;)Z

    #@4f
    move-result v1

    #@50
    if-eqz v1, :cond_64

    #@52
    .line 1284
    iget-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@54
    invoke-static {v1}, Lcom/android/server/sip/SipSessionGroup;->access$300(Lcom/android/server/sip/SipSessionGroup;)Lcom/android/server/sip/SipHelper;

    #@57
    move-result-object v1

    #@58
    check-cast p1, Ljavax/sip/RequestEvent;

    #@5a
    .end local p1
    const/16 v3, 0xc8

    #@5c
    invoke-virtual {v1, p1, v3}, Lcom/android/server/sip/SipHelper;->sendResponse(Ljavax/sip/RequestEvent;I)V

    #@5f
    .line 1285
    invoke-direct {p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->endCallNormally()V

    #@62
    move v1, v2

    #@63
    .line 1286
    goto :goto_20

    #@64
    .line 1287
    .restart local p1
    :cond_64
    const-string v1, "REFER"

    #@66
    invoke-static {v1, p1}, Lcom/android/server/sip/SipSessionGroup;->access$800(Ljava/lang/String;Ljava/util/EventObject;)Z

    #@69
    move-result v1

    #@6a
    if-eqz v1, :cond_73

    #@6c
    .line 1288
    check-cast p1, Ljavax/sip/RequestEvent;

    #@6e
    .end local p1
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->processReferRequest(Ljavax/sip/RequestEvent;)Z

    #@71
    move-result v1

    #@72
    goto :goto_20

    #@73
    .line 1289
    .restart local p1
    :cond_73
    instance-of v1, p1, Lcom/android/server/sip/SipSessionGroup$MakeCallCommand;

    #@75
    if-eqz v1, :cond_9a

    #@77
    .line 1291
    const/4 v1, 0x5

    #@78
    iput v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mState:I

    #@7a
    .line 1292
    iget-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@7c
    invoke-static {v1}, Lcom/android/server/sip/SipSessionGroup;->access$300(Lcom/android/server/sip/SipSessionGroup;)Lcom/android/server/sip/SipHelper;

    #@7f
    move-result-object v3

    #@80
    iget-object v4, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mDialog:Ljavax/sip/Dialog;

    #@82
    move-object v1, p1

    #@83
    check-cast v1, Lcom/android/server/sip/SipSessionGroup$MakeCallCommand;

    #@85
    invoke-virtual {v1}, Lcom/android/server/sip/SipSessionGroup$MakeCallCommand;->getSessionDescription()Ljava/lang/String;

    #@88
    move-result-object v1

    #@89
    invoke-virtual {v3, v4, v1}, Lcom/android/server/sip/SipHelper;->sendReinvite(Ljavax/sip/Dialog;Ljava/lang/String;)Ljavax/sip/ClientTransaction;

    #@8c
    move-result-object v1

    #@8d
    iput-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mClientTransaction:Ljavax/sip/ClientTransaction;

    #@8f
    .line 1294
    check-cast p1, Lcom/android/server/sip/SipSessionGroup$MakeCallCommand;

    #@91
    .end local p1
    invoke-virtual {p1}, Lcom/android/server/sip/SipSessionGroup$MakeCallCommand;->getTimeout()I

    #@94
    move-result v1

    #@95
    invoke-direct {p0, v1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->startSessionTimer(I)V

    #@98
    move v1, v2

    #@99
    .line 1295
    goto :goto_20

    #@9a
    .line 1296
    .restart local p1
    :cond_9a
    instance-of v1, p1, Ljavax/sip/ResponseEvent;

    #@9c
    if-eqz v1, :cond_a9

    #@9e
    .line 1297
    const-string v1, "NOTIFY"

    #@a0
    invoke-static {v1, p1}, Lcom/android/server/sip/SipSessionGroup;->access$2100(Ljava/lang/String;Ljava/util/EventObject;)Z

    #@a3
    move-result v1

    #@a4
    if-eqz v1, :cond_a9

    #@a6
    move v1, v2

    #@a7
    goto/16 :goto_20

    #@a9
    .line 1299
    :cond_a9
    const/4 v1, 0x0

    #@aa
    goto/16 :goto_20
.end method

.method private incomingCall(Ljava/util/EventObject;)Z
    .registers 11
    .parameter "evt"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    .line 1074
    instance-of v0, p1, Lcom/android/server/sip/SipSessionGroup$MakeCallCommand;

    #@3
    if-eqz v0, :cond_3c

    #@5
    .line 1076
    const/4 v0, 0x4

    #@6
    iput v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mState:I

    #@8
    .line 1077
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@a
    invoke-static {v0}, Lcom/android/server/sip/SipSessionGroup;->access$300(Lcom/android/server/sip/SipSessionGroup;)Lcom/android/server/sip/SipHelper;

    #@d
    move-result-object v0

    #@e
    iget-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mInviteReceived:Ljavax/sip/RequestEvent;

    #@10
    iget-object v2, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@12
    invoke-static {v2}, Lcom/android/server/sip/SipSessionGroup;->access$1400(Lcom/android/server/sip/SipSessionGroup;)Landroid/net/sip/SipProfile;

    #@15
    move-result-object v2

    #@16
    move-object v3, p1

    #@17
    check-cast v3, Lcom/android/server/sip/SipSessionGroup$MakeCallCommand;

    #@19
    invoke-virtual {v3}, Lcom/android/server/sip/SipSessionGroup$MakeCallCommand;->getSessionDescription()Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    iget-object v4, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mServerTransaction:Ljavax/sip/ServerTransaction;

    #@1f
    iget-object v5, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@21
    invoke-static {v5}, Lcom/android/server/sip/SipSessionGroup;->access$2300(Lcom/android/server/sip/SipSessionGroup;)Ljava/lang/String;

    #@24
    move-result-object v5

    #@25
    iget-object v6, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@27
    invoke-static {v6}, Lcom/android/server/sip/SipSessionGroup;->access$2400(Lcom/android/server/sip/SipSessionGroup;)I

    #@2a
    move-result v6

    #@2b
    invoke-virtual/range {v0 .. v6}, Lcom/android/server/sip/SipHelper;->sendInviteOk(Ljavax/sip/RequestEvent;Landroid/net/sip/SipProfile;Ljava/lang/String;Ljavax/sip/ServerTransaction;Ljava/lang/String;I)Ljavax/sip/ServerTransaction;

    #@2e
    move-result-object v0

    #@2f
    iput-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mServerTransaction:Ljavax/sip/ServerTransaction;

    #@31
    .line 1082
    check-cast p1, Lcom/android/server/sip/SipSessionGroup$MakeCallCommand;

    #@33
    .end local p1
    invoke-virtual {p1}, Lcom/android/server/sip/SipSessionGroup$MakeCallCommand;->getTimeout()I

    #@36
    move-result v0

    #@37
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->startSessionTimer(I)V

    #@3a
    move v0, v8

    #@3b
    .line 1097
    :goto_3b
    return v0

    #@3c
    .line 1084
    .restart local p1
    :cond_3c
    invoke-static {}, Lcom/android/server/sip/SipSessionGroup;->access$1700()Ljava/util/EventObject;

    #@3f
    move-result-object v0

    #@40
    if-ne v0, p1, :cond_54

    #@42
    .line 1085
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@44
    invoke-static {v0}, Lcom/android/server/sip/SipSessionGroup;->access$300(Lcom/android/server/sip/SipSessionGroup;)Lcom/android/server/sip/SipHelper;

    #@47
    move-result-object v0

    #@48
    iget-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mInviteReceived:Ljavax/sip/RequestEvent;

    #@4a
    iget-object v2, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mServerTransaction:Ljavax/sip/ServerTransaction;

    #@4c
    invoke-virtual {v0, v1, v2}, Lcom/android/server/sip/SipHelper;->sendInviteBusyHere(Ljavax/sip/RequestEvent;Ljavax/sip/ServerTransaction;)V

    #@4f
    .line 1087
    invoke-direct {p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->endCallNormally()V

    #@52
    move v0, v8

    #@53
    .line 1088
    goto :goto_3b

    #@54
    .line 1089
    :cond_54
    const-string v0, "CANCEL"

    #@56
    invoke-static {v0, p1}, Lcom/android/server/sip/SipSessionGroup;->access$800(Ljava/lang/String;Ljava/util/EventObject;)Z

    #@59
    move-result v0

    #@5a
    if-eqz v0, :cond_80

    #@5c
    move-object v7, p1

    #@5d
    .line 1090
    check-cast v7, Ljavax/sip/RequestEvent;

    #@5f
    .line 1091
    .local v7, event:Ljavax/sip/RequestEvent;
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@61
    invoke-static {v0}, Lcom/android/server/sip/SipSessionGroup;->access$300(Lcom/android/server/sip/SipSessionGroup;)Lcom/android/server/sip/SipHelper;

    #@64
    move-result-object v0

    #@65
    const/16 v1, 0xc8

    #@67
    invoke-virtual {v0, v7, v1}, Lcom/android/server/sip/SipHelper;->sendResponse(Ljavax/sip/RequestEvent;I)V

    #@6a
    .line 1092
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@6c
    invoke-static {v0}, Lcom/android/server/sip/SipSessionGroup;->access$300(Lcom/android/server/sip/SipSessionGroup;)Lcom/android/server/sip/SipHelper;

    #@6f
    move-result-object v0

    #@70
    iget-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mInviteReceived:Ljavax/sip/RequestEvent;

    #@72
    invoke-virtual {v1}, Ljavax/sip/RequestEvent;->getRequest()Ljavax/sip/message/Request;

    #@75
    move-result-object v1

    #@76
    iget-object v2, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mServerTransaction:Ljavax/sip/ServerTransaction;

    #@78
    invoke-virtual {v0, v1, v2}, Lcom/android/server/sip/SipHelper;->sendInviteRequestTerminated(Ljavax/sip/message/Request;Ljavax/sip/ServerTransaction;)V

    #@7b
    .line 1094
    invoke-direct {p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->endCallNormally()V

    #@7e
    move v0, v8

    #@7f
    .line 1095
    goto :goto_3b

    #@80
    .line 1097
    .end local v7           #event:Ljavax/sip/RequestEvent;
    :cond_80
    const/4 v0, 0x0

    #@81
    goto :goto_3b
.end method

.method private incomingCallToInCall(Ljava/util/EventObject;)Z
    .registers 7
    .parameter "evt"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1103
    const-string v3, "ACK"

    #@4
    invoke-static {v3, p1}, Lcom/android/server/sip/SipSessionGroup;->access$800(Ljava/lang/String;Ljava/util/EventObject;)Z

    #@7
    move-result v3

    #@8
    if-eqz v3, :cond_2a

    #@a
    .line 1104
    iget-object v3, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@c
    check-cast p1, Ljavax/sip/RequestEvent;

    #@e
    .end local p1
    invoke-virtual {p1}, Ljavax/sip/RequestEvent;->getRequest()Ljavax/sip/message/Request;

    #@11
    move-result-object v4

    #@12
    invoke-static {v3, v4}, Lcom/android/server/sip/SipSessionGroup;->access$2500(Lcom/android/server/sip/SipSessionGroup;Ljavax/sip/message/Message;)Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    .line 1105
    .local v0, sdp:Ljava/lang/String;
    if-eqz v0, :cond_1a

    #@18
    iput-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mPeerSessionDescription:Ljava/lang/String;

    #@1a
    .line 1106
    :cond_1a
    iget-object v3, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mPeerSessionDescription:Ljava/lang/String;

    #@1c
    if-nez v3, :cond_26

    #@1e
    .line 1107
    const/4 v2, -0x4

    #@1f
    const-string/jumbo v3, "peer sdp is empty"

    #@22
    invoke-direct {p0, v2, v3}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->onError(ILjava/lang/String;)V

    #@25
    .line 1117
    .end local v0           #sdp:Ljava/lang/String;
    :cond_25
    :goto_25
    return v1

    #@26
    .line 1109
    .restart local v0       #sdp:Ljava/lang/String;
    :cond_26
    invoke-direct {p0, v2}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->establishCall(Z)V

    #@29
    goto :goto_25

    #@2a
    .line 1112
    .end local v0           #sdp:Ljava/lang/String;
    .restart local p1
    :cond_2a
    const-string v3, "CANCEL"

    #@2c
    invoke-static {v3, p1}, Lcom/android/server/sip/SipSessionGroup;->access$800(Ljava/lang/String;Ljava/util/EventObject;)Z

    #@2f
    move-result v3

    #@30
    if-nez v3, :cond_25

    #@32
    move v1, v2

    #@33
    .line 1117
    goto :goto_25
.end method

.method private isCurrentTransaction(Ljavax/sip/TransactionTerminatedEvent;)Z
    .registers 8
    .parameter "event"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 833
    invoke-virtual {p1}, Ljavax/sip/TransactionTerminatedEvent;->isServerTransaction()Z

    #@4
    move-result v3

    #@5
    if-eqz v3, :cond_48

    #@7
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mServerTransaction:Ljavax/sip/ServerTransaction;

    #@9
    .line 836
    .local v0, current:Ljavax/sip/Transaction;
    :goto_9
    invoke-virtual {p1}, Ljavax/sip/TransactionTerminatedEvent;->isServerTransaction()Z

    #@c
    move-result v3

    #@d
    if-eqz v3, :cond_4b

    #@f
    invoke-virtual {p1}, Ljavax/sip/TransactionTerminatedEvent;->getServerTransaction()Ljavax/sip/ServerTransaction;

    #@12
    move-result-object v1

    #@13
    .line 840
    .local v1, target:Ljavax/sip/Transaction;
    :goto_13
    if-eq v0, v1, :cond_50

    #@15
    iget v3, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mState:I

    #@17
    const/16 v4, 0x9

    #@19
    if-eq v3, v4, :cond_50

    #@1b
    .line 841
    const-string v2, "SipSession"

    #@1d
    new-instance v3, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string/jumbo v4, "not the current transaction; current="

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->toString(Ljavax/sip/Transaction;)Ljava/lang/String;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    const-string v4, ", target="

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-direct {p0, v1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->toString(Ljavax/sip/Transaction;)Ljava/lang/String;

    #@3a
    move-result-object v4

    #@3b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v3

    #@43
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 843
    const/4 v2, 0x0

    #@47
    .line 849
    :cond_47
    :goto_47
    return v2

    #@48
    .line 833
    .end local v0           #current:Ljavax/sip/Transaction;
    .end local v1           #target:Ljavax/sip/Transaction;
    :cond_48
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mClientTransaction:Ljavax/sip/ClientTransaction;

    #@4a
    goto :goto_9

    #@4b
    .line 836
    .restart local v0       #current:Ljavax/sip/Transaction;
    :cond_4b
    invoke-virtual {p1}, Ljavax/sip/TransactionTerminatedEvent;->getClientTransaction()Ljavax/sip/ClientTransaction;

    #@4e
    move-result-object v1

    #@4f
    goto :goto_13

    #@50
    .line 844
    .restart local v1       #target:Ljavax/sip/Transaction;
    :cond_50
    if-eqz v0, :cond_47

    #@52
    .line 845
    const-string v3, "SipSession"

    #@54
    new-instance v4, Ljava/lang/StringBuilder;

    #@56
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@59
    const-string/jumbo v5, "transaction terminated: "

    #@5c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v4

    #@60
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->toString(Ljavax/sip/Transaction;)Ljava/lang/String;

    #@63
    move-result-object v5

    #@64
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v4

    #@68
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v4

    #@6c
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6f
    goto :goto_47
.end method

.method private onError(ILjava/lang/String;)V
    .registers 4
    .parameter "errorCode"
    .parameter "message"

    #@0
    .prologue
    .line 1383
    invoke-direct {p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->cancelSessionTimer()V

    #@3
    .line 1384
    iget v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mState:I

    #@5
    packed-switch v0, :pswitch_data_10

    #@8
    .line 1390
    invoke-direct {p0, p1, p2}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->endCallOnError(ILjava/lang/String;)V

    #@b
    .line 1392
    :goto_b
    return-void

    #@c
    .line 1387
    :pswitch_c
    invoke-direct {p0, p1, p2}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->onRegistrationFailed(ILjava/lang/String;)V

    #@f
    goto :goto_b

    #@10
    .line 1384
    :pswitch_data_10
    .packed-switch 0x1
        :pswitch_c
        :pswitch_c
    .end packed-switch
.end method

.method private onError(Ljava/lang/Throwable;)V
    .registers 4
    .parameter "exception"

    #@0
    .prologue
    .line 1396
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@2
    invoke-static {v0, p1}, Lcom/android/server/sip/SipSessionGroup;->access$1600(Lcom/android/server/sip/SipSessionGroup;Ljava/lang/Throwable;)Ljava/lang/Throwable;

    #@5
    move-result-object p1

    #@6
    .line 1397
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->getErrorCode(Ljava/lang/Throwable;)I

    #@9
    move-result v0

    #@a
    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    invoke-direct {p0, v0, v1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->onError(ILjava/lang/String;)V

    #@11
    .line 1398
    return-void
.end method

.method private onError(Ljavax/sip/message/Response;)V
    .registers 5
    .parameter "response"

    #@0
    .prologue
    .line 1401
    invoke-interface {p1}, Ljavax/sip/message/Response;->getStatusCode()I

    #@3
    move-result v0

    #@4
    .line 1402
    .local v0, statusCode:I
    iget-boolean v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mInCall:Z

    #@6
    if-nez v1, :cond_10

    #@8
    const/16 v1, 0x1e6

    #@a
    if-ne v0, v1, :cond_10

    #@c
    .line 1403
    invoke-direct {p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->endCallOnBusy()V

    #@f
    .line 1407
    :goto_f
    return-void

    #@10
    .line 1405
    :cond_10
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->getErrorCode(I)I

    #@13
    move-result v1

    #@14
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->createErrorMessage(Ljavax/sip/message/Response;)Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    invoke-direct {p0, v1, v2}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->onError(ILjava/lang/String;)V

    #@1b
    goto :goto_f
.end method

.method private onRegistrationDone(I)V
    .registers 3
    .parameter "duration"

    #@0
    .prologue
    .line 1448
    invoke-direct {p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->reset()V

    #@3
    .line 1449
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@5
    invoke-virtual {v0, p0, p1}, Lcom/android/server/sip/SipSessionListenerProxy;->onRegistrationDone(Landroid/net/sip/ISipSession;I)V

    #@8
    .line 1450
    return-void
.end method

.method private onRegistrationFailed(ILjava/lang/String;)V
    .registers 4
    .parameter "errorCode"
    .parameter "message"

    #@0
    .prologue
    .line 1453
    invoke-direct {p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->reset()V

    #@3
    .line 1454
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@5
    invoke-virtual {v0, p0, p1, p2}, Lcom/android/server/sip/SipSessionListenerProxy;->onRegistrationFailed(Landroid/net/sip/ISipSession;ILjava/lang/String;)V

    #@8
    .line 1455
    return-void
.end method

.method private onRegistrationFailed(Ljava/lang/Throwable;)V
    .registers 4
    .parameter "exception"

    #@0
    .prologue
    .line 1458
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@2
    invoke-static {v0, p1}, Lcom/android/server/sip/SipSessionGroup;->access$1600(Lcom/android/server/sip/SipSessionGroup;Ljava/lang/Throwable;)Ljava/lang/Throwable;

    #@5
    move-result-object p1

    #@6
    .line 1459
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->getErrorCode(Ljava/lang/Throwable;)I

    #@9
    move-result v0

    #@a
    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    invoke-direct {p0, v0, v1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->onRegistrationFailed(ILjava/lang/String;)V

    #@11
    .line 1461
    return-void
.end method

.method private onRegistrationFailed(Ljavax/sip/message/Response;)V
    .registers 5
    .parameter "response"

    #@0
    .prologue
    .line 1464
    invoke-interface {p1}, Ljavax/sip/message/Response;->getStatusCode()I

    #@3
    move-result v0

    #@4
    .line 1465
    .local v0, statusCode:I
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->getErrorCode(I)I

    #@7
    move-result v1

    #@8
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->createErrorMessage(Ljavax/sip/message/Response;)Ljava/lang/String;

    #@b
    move-result-object v2

    #@c
    invoke-direct {p0, v1, v2}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->onRegistrationFailed(ILjava/lang/String;)V

    #@f
    .line 1467
    return-void
.end method

.method private outgoingCall(Ljava/util/EventObject;)Z
    .registers 10
    .parameter "evt"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 1121
    const-string v5, "INVITE"

    #@4
    invoke-static {v5, p1}, Lcom/android/server/sip/SipSessionGroup;->access$2100(Ljava/lang/String;Ljava/util/EventObject;)Z

    #@7
    move-result v5

    #@8
    if-eqz v5, :cond_8b

    #@a
    move-object v0, p1

    #@b
    .line 1122
    check-cast v0, Ljavax/sip/ResponseEvent;

    #@d
    .line 1123
    .local v0, event:Ljavax/sip/ResponseEvent;
    invoke-virtual {v0}, Ljavax/sip/ResponseEvent;->getResponse()Ljavax/sip/message/Response;

    #@10
    move-result-object v1

    #@11
    .line 1125
    .local v1, response:Ljavax/sip/message/Response;
    invoke-interface {v1}, Ljavax/sip/message/Response;->getStatusCode()I

    #@14
    move-result v2

    #@15
    .line 1126
    .local v2, statusCode:I
    sparse-switch v2, :sswitch_data_c2

    #@18
    .line 1161
    iget-object v5, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mReferSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@1a
    if-eqz v5, :cond_2f

    #@1c
    .line 1162
    iget-object v5, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@1e
    invoke-static {v5}, Lcom/android/server/sip/SipSessionGroup;->access$300(Lcom/android/server/sip/SipSessionGroup;)Lcom/android/server/sip/SipHelper;

    #@21
    move-result-object v5

    #@22
    iget-object v6, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mReferSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@24
    iget-object v6, v6, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mDialog:Ljavax/sip/Dialog;

    #@26
    const/16 v7, 0x1f7

    #@28
    invoke-direct {p0, v7}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->getResponseString(I)Ljava/lang/String;

    #@2b
    move-result-object v7

    #@2c
    invoke-virtual {v5, v6, v7}, Lcom/android/server/sip/SipHelper;->sendReferNotify(Ljavax/sip/Dialog;Ljava/lang/String;)V

    #@2f
    .line 1165
    :cond_2f
    const/16 v5, 0x190

    #@31
    if-lt v2, v5, :cond_85

    #@33
    .line 1167
    invoke-direct {p0, v1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->onError(Ljavax/sip/message/Response;)V

    #@36
    .line 1192
    .end local v0           #event:Ljavax/sip/ResponseEvent;
    .end local v1           #response:Ljavax/sip/message/Response;
    .end local v2           #statusCode:I
    :cond_36
    :goto_36
    :sswitch_36
    return v3

    #@37
    .line 1133
    .restart local v0       #event:Ljavax/sip/ResponseEvent;
    .restart local v1       #response:Ljavax/sip/message/Response;
    .restart local v2       #statusCode:I
    :sswitch_37
    iget v4, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mState:I

    #@39
    const/4 v5, 0x5

    #@3a
    if-ne v4, v5, :cond_36

    #@3c
    .line 1134
    const/4 v4, 0x6

    #@3d
    iput v4, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mState:I

    #@3f
    .line 1135
    invoke-direct {p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->cancelSessionTimer()V

    #@42
    .line 1136
    iget-object v4, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@44
    invoke-virtual {v4, p0}, Lcom/android/server/sip/SipSessionListenerProxy;->onRingingBack(Landroid/net/sip/ISipSession;)V

    #@47
    goto :goto_36

    #@48
    .line 1140
    :sswitch_48
    iget-object v4, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mReferSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@4a
    if-eqz v4, :cond_62

    #@4c
    .line 1141
    iget-object v4, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@4e
    invoke-static {v4}, Lcom/android/server/sip/SipSessionGroup;->access$300(Lcom/android/server/sip/SipSessionGroup;)Lcom/android/server/sip/SipHelper;

    #@51
    move-result-object v4

    #@52
    iget-object v5, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mReferSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@54
    iget-object v5, v5, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mDialog:Ljavax/sip/Dialog;

    #@56
    const/16 v6, 0xc8

    #@58
    invoke-direct {p0, v6}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->getResponseString(I)Ljava/lang/String;

    #@5b
    move-result-object v6

    #@5c
    invoke-virtual {v4, v5, v6}, Lcom/android/server/sip/SipHelper;->sendReferNotify(Ljavax/sip/Dialog;Ljava/lang/String;)V

    #@5f
    .line 1144
    const/4 v4, 0x0

    #@60
    iput-object v4, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mReferSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@62
    .line 1146
    :cond_62
    iget-object v4, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@64
    invoke-static {v4}, Lcom/android/server/sip/SipSessionGroup;->access$300(Lcom/android/server/sip/SipSessionGroup;)Lcom/android/server/sip/SipHelper;

    #@67
    move-result-object v4

    #@68
    iget-object v5, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mDialog:Ljavax/sip/Dialog;

    #@6a
    invoke-virtual {v4, v0, v5}, Lcom/android/server/sip/SipHelper;->sendInviteAck(Ljavax/sip/ResponseEvent;Ljavax/sip/Dialog;)V

    #@6d
    .line 1147
    iget-object v4, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@6f
    invoke-static {v4, v1}, Lcom/android/server/sip/SipSessionGroup;->access$2500(Lcom/android/server/sip/SipSessionGroup;Ljavax/sip/message/Message;)Ljava/lang/String;

    #@72
    move-result-object v4

    #@73
    iput-object v4, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mPeerSessionDescription:Ljava/lang/String;

    #@75
    .line 1148
    invoke-direct {p0, v3}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->establishCall(Z)V

    #@78
    goto :goto_36

    #@79
    .line 1152
    :sswitch_79
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->handleAuthentication(Ljavax/sip/ResponseEvent;)Z

    #@7c
    move-result v4

    #@7d
    if-eqz v4, :cond_36

    #@7f
    .line 1153
    iget-object v4, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@81
    invoke-static {v4, p0}, Lcom/android/server/sip/SipSessionGroup;->access$500(Lcom/android/server/sip/SipSessionGroup;Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;)V

    #@84
    goto :goto_36

    #@85
    .line 1169
    :cond_85
    const/16 v5, 0x12c

    #@87
    if-lt v2, v5, :cond_36

    #@89
    move v3, v4

    #@8a
    .line 1175
    goto :goto_36

    #@8b
    .line 1176
    .end local v0           #event:Ljavax/sip/ResponseEvent;
    .end local v1           #response:Ljavax/sip/message/Response;
    .end local v2           #statusCode:I
    :cond_8b
    invoke-static {}, Lcom/android/server/sip/SipSessionGroup;->access$1700()Ljava/util/EventObject;

    #@8e
    move-result-object v5

    #@8f
    if-ne v5, p1, :cond_a4

    #@91
    .line 1180
    const/4 v4, 0x7

    #@92
    iput v4, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mState:I

    #@94
    .line 1181
    iget-object v4, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@96
    invoke-static {v4}, Lcom/android/server/sip/SipSessionGroup;->access$300(Lcom/android/server/sip/SipSessionGroup;)Lcom/android/server/sip/SipHelper;

    #@99
    move-result-object v4

    #@9a
    iget-object v5, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mClientTransaction:Ljavax/sip/ClientTransaction;

    #@9c
    invoke-virtual {v4, v5}, Lcom/android/server/sip/SipHelper;->sendCancel(Ljavax/sip/ClientTransaction;)V

    #@9f
    .line 1182
    const/4 v4, 0x3

    #@a0
    invoke-direct {p0, v4}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->startSessionTimer(I)V

    #@a3
    goto :goto_36

    #@a4
    .line 1184
    :cond_a4
    const-string v5, "INVITE"

    #@a6
    invoke-static {v5, p1}, Lcom/android/server/sip/SipSessionGroup;->access$800(Ljava/lang/String;Ljava/util/EventObject;)Z

    #@a9
    move-result v5

    #@aa
    if-eqz v5, :cond_be

    #@ac
    move-object v0, p1

    #@ad
    .line 1187
    check-cast v0, Ljavax/sip/RequestEvent;

    #@af
    .line 1188
    .local v0, event:Ljavax/sip/RequestEvent;
    iget-object v4, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@b1
    invoke-static {v4}, Lcom/android/server/sip/SipSessionGroup;->access$300(Lcom/android/server/sip/SipSessionGroup;)Lcom/android/server/sip/SipHelper;

    #@b4
    move-result-object v4

    #@b5
    invoke-virtual {v0}, Ljavax/sip/RequestEvent;->getServerTransaction()Ljavax/sip/ServerTransaction;

    #@b8
    move-result-object v5

    #@b9
    invoke-virtual {v4, v0, v5}, Lcom/android/server/sip/SipHelper;->sendInviteBusyHere(Ljavax/sip/RequestEvent;Ljavax/sip/ServerTransaction;)V

    #@bc
    goto/16 :goto_36

    #@be
    .end local v0           #event:Ljavax/sip/RequestEvent;
    :cond_be
    move v3, v4

    #@bf
    .line 1192
    goto/16 :goto_36

    #@c1
    .line 1126
    nop

    #@c2
    :sswitch_data_c2
    .sparse-switch
        0xb4 -> :sswitch_37
        0xb5 -> :sswitch_37
        0xb6 -> :sswitch_37
        0xb7 -> :sswitch_37
        0xc8 -> :sswitch_48
        0x191 -> :sswitch_79
        0x197 -> :sswitch_79
        0x1eb -> :sswitch_36
    .end sparse-switch
.end method

.method private outgoingCallToReady(Ljava/util/EventObject;)Z
    .registers 8
    .parameter "evt"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 1197
    instance-of v5, p1, Ljavax/sip/ResponseEvent;

    #@4
    if-eqz v5, :cond_3b

    #@6
    move-object v0, p1

    #@7
    .line 1198
    check-cast v0, Ljavax/sip/ResponseEvent;

    #@9
    .line 1199
    .local v0, event:Ljavax/sip/ResponseEvent;
    invoke-virtual {v0}, Ljavax/sip/ResponseEvent;->getResponse()Ljavax/sip/message/Response;

    #@c
    move-result-object v1

    #@d
    .line 1200
    .local v1, response:Ljavax/sip/message/Response;
    invoke-interface {v1}, Ljavax/sip/message/Response;->getStatusCode()I

    #@10
    move-result v2

    #@11
    .line 1201
    .local v2, statusCode:I
    const-string v5, "CANCEL"

    #@13
    invoke-static {v5, p1}, Lcom/android/server/sip/SipSessionGroup;->access$2100(Ljava/lang/String;Ljava/util/EventObject;)Z

    #@16
    move-result v5

    #@17
    if-eqz v5, :cond_1e

    #@19
    .line 1202
    const/16 v5, 0xc8

    #@1b
    if-ne v2, v5, :cond_29

    #@1d
    .line 1230
    .end local v0           #event:Ljavax/sip/ResponseEvent;
    .end local v1           #response:Ljavax/sip/message/Response;
    .end local v2           #statusCode:I
    :goto_1d
    return v3

    #@1e
    .line 1206
    .restart local v0       #event:Ljavax/sip/ResponseEvent;
    .restart local v1       #response:Ljavax/sip/message/Response;
    .restart local v2       #statusCode:I
    :cond_1e
    const-string v5, "INVITE"

    #@20
    invoke-static {v5, p1}, Lcom/android/server/sip/SipSessionGroup;->access$2100(Ljava/lang/String;Ljava/util/EventObject;)Z

    #@23
    move-result v5

    #@24
    if-eqz v5, :cond_39

    #@26
    .line 1207
    sparse-switch v2, :sswitch_data_4c

    #@29
    .line 1219
    :cond_29
    const/16 v5, 0x190

    #@2b
    if-lt v2, v5, :cond_4a

    #@2d
    .line 1220
    invoke-direct {p0, v1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->onError(Ljavax/sip/message/Response;)V

    #@30
    goto :goto_1d

    #@31
    .line 1209
    :sswitch_31
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->outgoingCall(Ljava/util/EventObject;)Z

    #@34
    goto :goto_1d

    #@35
    .line 1212
    :sswitch_35
    invoke-direct {p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->endCallNormally()V

    #@38
    goto :goto_1d

    #@39
    :cond_39
    move v3, v4

    #@3a
    .line 1216
    goto :goto_1d

    #@3b
    .line 1223
    .end local v0           #event:Ljavax/sip/ResponseEvent;
    .end local v1           #response:Ljavax/sip/message/Response;
    .end local v2           #statusCode:I
    :cond_3b
    instance-of v3, p1, Ljavax/sip/TransactionTerminatedEvent;

    #@3d
    if-eqz v3, :cond_4a

    #@3f
    .line 1228
    new-instance v3, Ljavax/sip/SipException;

    #@41
    const-string/jumbo v5, "timed out"

    #@44
    invoke-direct {v3, v5}, Ljavax/sip/SipException;-><init>(Ljava/lang/String;)V

    #@47
    invoke-direct {p0, v3}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->onError(Ljava/lang/Throwable;)V

    #@4a
    :cond_4a
    move v3, v4

    #@4b
    .line 1230
    goto :goto_1d

    #@4c
    .line 1207
    :sswitch_data_4c
    .sparse-switch
        0xc8 -> :sswitch_31
        0x1e7 -> :sswitch_35
    .end sparse-switch
.end method

.method private processCommand(Ljava/util/EventObject;)V
    .registers 5
    .parameter "command"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 713
    invoke-static {p1}, Lcom/android/server/sip/SipSessionGroup;->access$1900(Ljava/util/EventObject;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_1f

    #@6
    const-string v0, "SipSession"

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string/jumbo v2, "process cmd: "

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 714
    :cond_1f
    invoke-virtual {p0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->process(Ljava/util/EventObject;)Z

    #@22
    move-result v0

    #@23
    if-nez v0, :cond_3d

    #@25
    .line 715
    const/16 v0, -0x9

    #@27
    new-instance v1, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v2, "cannot initiate a new transaction to execute: "

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v1

    #@36
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v1

    #@3a
    invoke-direct {p0, v0, v1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->onError(ILjava/lang/String;)V

    #@3d
    .line 719
    :cond_3d
    return-void
.end method

.method private processDialogTerminated(Ljavax/sip/DialogTerminatedEvent;)V
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 824
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mDialog:Ljavax/sip/Dialog;

    #@2
    invoke-virtual {p1}, Ljavax/sip/DialogTerminatedEvent;->getDialog()Ljavax/sip/Dialog;

    #@5
    move-result-object v1

    #@6
    if-ne v0, v1, :cond_13

    #@8
    .line 825
    new-instance v0, Ljavax/sip/SipException;

    #@a
    const-string v1, "dialog terminated"

    #@c
    invoke-direct {v0, v1}, Ljavax/sip/SipException;-><init>(Ljava/lang/String;)V

    #@f
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->onError(Ljava/lang/Throwable;)V

    #@12
    .line 830
    :goto_12
    return-void

    #@13
    .line 827
    :cond_13
    const-string v0, "SipSession"

    #@15
    new-instance v1, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string/jumbo v2, "not the current dialog; current="

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    iget-object v2, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mDialog:Ljavax/sip/Dialog;

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    const-string v2, ", terminated="

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {p1}, Ljavax/sip/DialogTerminatedEvent;->getDialog()Ljavax/sip/Dialog;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v1

    #@39
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    goto :goto_12
.end method

.method private processExceptions(Ljava/util/EventObject;)Z
    .registers 5
    .parameter "evt"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v2, 0xc8

    #@2
    const/4 v1, 0x1

    #@3
    .line 794
    const-string v0, "BYE"

    #@5
    invoke-static {v0, p1}, Lcom/android/server/sip/SipSessionGroup;->access$800(Ljava/lang/String;Ljava/util/EventObject;)Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_1b

    #@b
    .line 796
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@d
    invoke-static {v0}, Lcom/android/server/sip/SipSessionGroup;->access$300(Lcom/android/server/sip/SipSessionGroup;)Lcom/android/server/sip/SipHelper;

    #@10
    move-result-object v0

    #@11
    check-cast p1, Ljavax/sip/RequestEvent;

    #@13
    .end local p1
    invoke-virtual {v0, p1, v2}, Lcom/android/server/sip/SipHelper;->sendResponse(Ljavax/sip/RequestEvent;I)V

    #@16
    .line 797
    invoke-direct {p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->endCallNormally()V

    #@19
    move v0, v1

    #@1a
    .line 820
    :goto_1a
    return v0

    #@1b
    .line 799
    .restart local p1
    :cond_1b
    const-string v0, "CANCEL"

    #@1d
    invoke-static {v0, p1}, Lcom/android/server/sip/SipSessionGroup;->access$800(Ljava/lang/String;Ljava/util/EventObject;)Z

    #@20
    move-result v0

    #@21
    if-eqz v0, :cond_32

    #@23
    .line 800
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@25
    invoke-static {v0}, Lcom/android/server/sip/SipSessionGroup;->access$300(Lcom/android/server/sip/SipSessionGroup;)Lcom/android/server/sip/SipHelper;

    #@28
    move-result-object v0

    #@29
    check-cast p1, Ljavax/sip/RequestEvent;

    #@2b
    .end local p1
    const/16 v2, 0x1e1

    #@2d
    invoke-virtual {v0, p1, v2}, Lcom/android/server/sip/SipHelper;->sendResponse(Ljavax/sip/RequestEvent;I)V

    #@30
    move v0, v1

    #@31
    .line 802
    goto :goto_1a

    #@32
    .line 803
    .restart local p1
    :cond_32
    instance-of v0, p1, Ljavax/sip/TransactionTerminatedEvent;

    #@34
    if-eqz v0, :cond_50

    #@36
    move-object v0, p1

    #@37
    .line 804
    check-cast v0, Ljavax/sip/TransactionTerminatedEvent;

    #@39
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->isCurrentTransaction(Ljavax/sip/TransactionTerminatedEvent;)Z

    #@3c
    move-result v0

    #@3d
    if-eqz v0, :cond_70

    #@3f
    .line 805
    instance-of v0, p1, Ljavax/sip/TimeoutEvent;

    #@41
    if-eqz v0, :cond_4a

    #@43
    .line 806
    check-cast p1, Ljavax/sip/TimeoutEvent;

    #@45
    .end local p1
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->processTimeout(Ljavax/sip/TimeoutEvent;)V

    #@48
    :goto_48
    move v0, v1

    #@49
    .line 811
    goto :goto_1a

    #@4a
    .line 808
    .restart local p1
    :cond_4a
    check-cast p1, Ljavax/sip/TransactionTerminatedEvent;

    #@4c
    .end local p1
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->processTransactionTerminated(Ljavax/sip/TransactionTerminatedEvent;)V

    #@4f
    goto :goto_48

    #@50
    .line 813
    .restart local p1
    :cond_50
    const-string v0, "OPTIONS"

    #@52
    invoke-static {v0, p1}, Lcom/android/server/sip/SipSessionGroup;->access$800(Ljava/lang/String;Ljava/util/EventObject;)Z

    #@55
    move-result v0

    #@56
    if-eqz v0, :cond_65

    #@58
    .line 814
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@5a
    invoke-static {v0}, Lcom/android/server/sip/SipSessionGroup;->access$300(Lcom/android/server/sip/SipSessionGroup;)Lcom/android/server/sip/SipHelper;

    #@5d
    move-result-object v0

    #@5e
    check-cast p1, Ljavax/sip/RequestEvent;

    #@60
    .end local p1
    invoke-virtual {v0, p1, v2}, Lcom/android/server/sip/SipHelper;->sendResponse(Ljavax/sip/RequestEvent;I)V

    #@63
    move v0, v1

    #@64
    .line 815
    goto :goto_1a

    #@65
    .line 816
    .restart local p1
    :cond_65
    instance-of v0, p1, Ljavax/sip/DialogTerminatedEvent;

    #@67
    if-eqz v0, :cond_70

    #@69
    .line 817
    check-cast p1, Ljavax/sip/DialogTerminatedEvent;

    #@6b
    .end local p1
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->processDialogTerminated(Ljavax/sip/DialogTerminatedEvent;)V

    #@6e
    move v0, v1

    #@6f
    .line 818
    goto :goto_1a

    #@70
    .line 820
    .restart local p1
    :cond_70
    const/4 v0, 0x0

    #@71
    goto :goto_1a
.end method

.method private processReferRequest(Ljavax/sip/RequestEvent;)Z
    .registers 13
    .parameter "event"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 1236
    :try_start_1
    invoke-virtual {p1}, Ljavax/sip/RequestEvent;->getRequest()Ljavax/sip/message/Request;

    #@4
    move-result-object v8

    #@5
    const-string v9, "Refer-To"

    #@7
    invoke-interface {v8, v9}, Ljavax/sip/message/Request;->getHeader(Ljava/lang/String;)Ljavax/sip/header/Header;

    #@a
    move-result-object v3

    #@b
    check-cast v3, Ljavax/sip/header/ReferToHeader;

    #@d
    .line 1238
    .local v3, referto:Ljavax/sip/header/ReferToHeader;
    invoke-interface {v3}, Ljavax/sip/header/ReferToHeader;->getAddress()Ljavax/sip/address/Address;

    #@10
    move-result-object v0

    #@11
    .line 1239
    .local v0, address:Ljavax/sip/address/Address;
    invoke-interface {v0}, Ljavax/sip/address/Address;->getURI()Ljavax/sip/address/URI;

    #@14
    move-result-object v5

    #@15
    check-cast v5, Ljavax/sip/address/SipURI;

    #@17
    .line 1240
    .local v5, uri:Ljavax/sip/address/SipURI;
    const-string v8, "Replaces"

    #@19
    invoke-interface {v5, v8}, Ljavax/sip/address/SipURI;->getHeader(Ljava/lang/String;)Ljava/lang/String;

    #@1c
    move-result-object v4

    #@1d
    .line 1241
    .local v4, replacesHeader:Ljava/lang/String;
    invoke-interface {v5}, Ljavax/sip/address/SipURI;->getUser()Ljava/lang/String;

    #@20
    move-result-object v6

    #@21
    .line 1242
    .local v6, username:Ljava/lang/String;
    if-nez v6, :cond_2f

    #@23
    .line 1243
    iget-object v8, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@25
    invoke-static {v8}, Lcom/android/server/sip/SipSessionGroup;->access$300(Lcom/android/server/sip/SipSessionGroup;)Lcom/android/server/sip/SipHelper;

    #@28
    move-result-object v8

    #@29
    const/16 v9, 0x190

    #@2b
    invoke-virtual {v8, p1, v9}, Lcom/android/server/sip/SipHelper;->sendResponse(Ljavax/sip/RequestEvent;I)V

    #@2e
    .line 1259
    :goto_2e
    return v7

    #@2f
    .line 1247
    :cond_2f
    iget-object v7, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@31
    invoke-static {v7}, Lcom/android/server/sip/SipSessionGroup;->access$300(Lcom/android/server/sip/SipSessionGroup;)Lcom/android/server/sip/SipHelper;

    #@34
    move-result-object v7

    #@35
    const/16 v8, 0xca

    #@37
    invoke-virtual {v7, p1, v8}, Lcom/android/server/sip/SipHelper;->sendResponse(Ljavax/sip/RequestEvent;I)V

    #@3a
    .line 1248
    iget-object v7, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@3c
    iget-object v8, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@3e
    invoke-virtual {v8}, Lcom/android/server/sip/SipSessionListenerProxy;->getListener()Landroid/net/sip/ISipSessionListener;

    #@41
    move-result-object v8

    #@42
    iget-object v9, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@44
    invoke-static {v9}, Lcom/android/server/sip/SipSessionGroup;->access$300(Lcom/android/server/sip/SipSessionGroup;)Lcom/android/server/sip/SipHelper;

    #@47
    move-result-object v9

    #@48
    invoke-virtual {v9, p1}, Lcom/android/server/sip/SipHelper;->getServerTransaction(Ljavax/sip/RequestEvent;)Ljavax/sip/ServerTransaction;

    #@4b
    move-result-object v9

    #@4c
    const/4 v10, 0x0

    #@4d
    invoke-static {v7, p1, v8, v9, v10}, Lcom/android/server/sip/SipSessionGroup;->access$400(Lcom/android/server/sip/SipSessionGroup;Ljavax/sip/RequestEvent;Landroid/net/sip/ISipSessionListener;Ljavax/sip/ServerTransaction;I)Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@50
    move-result-object v2

    #@51
    .line 1252
    .local v2, newSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
    iput-object p0, v2, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mReferSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@53
    .line 1253
    invoke-virtual {p1}, Ljavax/sip/RequestEvent;->getRequest()Ljavax/sip/message/Request;

    #@56
    move-result-object v7

    #@57
    const-string v8, "Referred-By"

    #@59
    invoke-interface {v7, v8}, Ljavax/sip/message/Request;->getHeader(Ljava/lang/String;)Ljavax/sip/header/Header;

    #@5c
    move-result-object v7

    #@5d
    check-cast v7, Lgov/nist/javax/sip/header/extensions/ReferredByHeader;

    #@5f
    iput-object v7, v2, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mReferredBy:Lgov/nist/javax/sip/header/extensions/ReferredByHeader;

    #@61
    .line 1255
    iput-object v4, v2, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mReplaces:Ljava/lang/String;

    #@63
    .line 1256
    invoke-static {v3}, Lcom/android/server/sip/SipSessionGroup;->access$2600(Ljavax/sip/header/HeaderAddress;)Landroid/net/sip/SipProfile;

    #@66
    move-result-object v7

    #@67
    iput-object v7, v2, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mPeerProfile:Landroid/net/sip/SipProfile;

    #@69
    .line 1257
    iget-object v7, v2, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@6b
    const/4 v8, 0x0

    #@6c
    invoke-virtual {v7, v2, v8}, Lcom/android/server/sip/SipSessionListenerProxy;->onCallTransferring(Landroid/net/sip/ISipSession;Ljava/lang/String;)V
    :try_end_6f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_6f} :catch_71

    #@6f
    .line 1259
    const/4 v7, 0x1

    #@70
    goto :goto_2e

    #@71
    .line 1260
    .end local v0           #address:Ljavax/sip/address/Address;
    .end local v2           #newSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
    .end local v3           #referto:Ljavax/sip/header/ReferToHeader;
    .end local v4           #replacesHeader:Ljava/lang/String;
    .end local v5           #uri:Ljavax/sip/address/SipURI;
    .end local v6           #username:Ljava/lang/String;
    :catch_71
    move-exception v1

    #@72
    .line 1261
    .local v1, e:Ljava/lang/IllegalArgumentException;
    new-instance v7, Ljavax/sip/SipException;

    #@74
    const-string v8, "createPeerProfile()"

    #@76
    invoke-direct {v7, v8, v1}, Ljavax/sip/SipException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@79
    throw v7
.end method

.method private processTimeout(Ljavax/sip/TimeoutEvent;)V
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 878
    const-string v0, "SipSession"

    #@2
    const-string/jumbo v1, "processing Timeout..."

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 879
    iget v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mState:I

    #@a
    packed-switch v0, :pswitch_data_28

    #@d
    .line 893
    :pswitch_d
    const-string v0, "SipSession"

    #@f
    const-string v1, "   do nothing"

    #@11
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 896
    :goto_14
    return-void

    #@15
    .line 882
    :pswitch_15
    invoke-direct {p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->reset()V

    #@18
    .line 883
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@1a
    invoke-virtual {v0, p0}, Lcom/android/server/sip/SipSessionListenerProxy;->onRegistrationTimeout(Landroid/net/sip/ISipSession;)V

    #@1d
    goto :goto_14

    #@1e
    .line 889
    :pswitch_1e
    const/4 v0, -0x5

    #@1f
    invoke-virtual {p1}, Ljavax/sip/TimeoutEvent;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-direct {p0, v0, v1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->onError(ILjava/lang/String;)V

    #@26
    goto :goto_14

    #@27
    .line 879
    nop

    #@28
    :pswitch_data_28
    .packed-switch 0x1
        :pswitch_15
        :pswitch_15
        :pswitch_1e
        :pswitch_1e
        :pswitch_1e
        :pswitch_d
        :pswitch_1e
    .end packed-switch
.end method

.method private processTransactionTerminated(Ljavax/sip/TransactionTerminatedEvent;)V
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 865
    iget v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mState:I

    #@2
    sparse-switch v0, :sswitch_data_2e

    #@5
    .line 871
    const-string v0, "SipSession"

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "Transaction terminated early: "

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 872
    const/4 v0, -0x3

    #@1e
    const-string/jumbo v1, "transaction terminated"

    #@21
    invoke-direct {p0, v0, v1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->onError(ILjava/lang/String;)V

    #@24
    .line 875
    :goto_24
    return-void

    #@25
    .line 868
    :sswitch_25
    const-string v0, "SipSession"

    #@27
    const-string v1, "Transaction terminated; do nothing"

    #@29
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    goto :goto_24

    #@2d
    .line 865
    nop

    #@2e
    :sswitch_data_2e
    .sparse-switch
        0x0 -> :sswitch_25
        0x8 -> :sswitch_25
    .end sparse-switch
.end method

.method private readyForCall(Ljava/util/EventObject;)Z
    .registers 12
    .parameter "evt"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v9, 0x1

    #@2
    .line 1035
    instance-of v1, p1, Lcom/android/server/sip/SipSessionGroup$MakeCallCommand;

    #@4
    if-eqz v1, :cond_64

    #@6
    .line 1036
    const/4 v0, 0x5

    #@7
    iput v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mState:I

    #@9
    move-object v7, p1

    #@a
    .line 1037
    check-cast v7, Lcom/android/server/sip/SipSessionGroup$MakeCallCommand;

    #@c
    .line 1038
    .local v7, cmd:Lcom/android/server/sip/SipSessionGroup$MakeCallCommand;
    invoke-virtual {v7}, Lcom/android/server/sip/SipSessionGroup$MakeCallCommand;->getPeerProfile()Landroid/net/sip/SipProfile;

    #@f
    move-result-object v0

    #@10
    iput-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mPeerProfile:Landroid/net/sip/SipProfile;

    #@12
    .line 1039
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mReferSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@14
    if-eqz v0, :cond_29

    #@16
    .line 1040
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@18
    invoke-static {v0}, Lcom/android/server/sip/SipSessionGroup;->access$300(Lcom/android/server/sip/SipSessionGroup;)Lcom/android/server/sip/SipHelper;

    #@1b
    move-result-object v0

    #@1c
    iget-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mReferSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@1e
    iget-object v1, v1, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mDialog:Ljavax/sip/Dialog;

    #@20
    const/16 v2, 0x64

    #@22
    invoke-direct {p0, v2}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->getResponseString(I)Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v0, v1, v2}, Lcom/android/server/sip/SipHelper;->sendReferNotify(Ljavax/sip/Dialog;Ljava/lang/String;)V

    #@29
    .line 1043
    :cond_29
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@2b
    invoke-static {v0}, Lcom/android/server/sip/SipSessionGroup;->access$300(Lcom/android/server/sip/SipSessionGroup;)Lcom/android/server/sip/SipHelper;

    #@2e
    move-result-object v0

    #@2f
    iget-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@31
    invoke-static {v1}, Lcom/android/server/sip/SipSessionGroup;->access$1400(Lcom/android/server/sip/SipSessionGroup;)Landroid/net/sip/SipProfile;

    #@34
    move-result-object v1

    #@35
    iget-object v2, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mPeerProfile:Landroid/net/sip/SipProfile;

    #@37
    invoke-virtual {v7}, Lcom/android/server/sip/SipSessionGroup$MakeCallCommand;->getSessionDescription()Ljava/lang/String;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->generateTag()Ljava/lang/String;

    #@3e
    move-result-object v4

    #@3f
    iget-object v5, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mReferredBy:Lgov/nist/javax/sip/header/extensions/ReferredByHeader;

    #@41
    iget-object v6, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mReplaces:Ljava/lang/String;

    #@43
    invoke-virtual/range {v0 .. v6}, Lcom/android/server/sip/SipHelper;->sendInvite(Landroid/net/sip/SipProfile;Landroid/net/sip/SipProfile;Ljava/lang/String;Ljava/lang/String;Lgov/nist/javax/sip/header/extensions/ReferredByHeader;Ljava/lang/String;)Ljavax/sip/ClientTransaction;

    #@46
    move-result-object v0

    #@47
    iput-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mClientTransaction:Ljavax/sip/ClientTransaction;

    #@49
    .line 1046
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mClientTransaction:Ljavax/sip/ClientTransaction;

    #@4b
    invoke-interface {v0}, Ljavax/sip/ClientTransaction;->getDialog()Ljavax/sip/Dialog;

    #@4e
    move-result-object v0

    #@4f
    iput-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mDialog:Ljavax/sip/Dialog;

    #@51
    .line 1047
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@53
    invoke-static {v0, p0}, Lcom/android/server/sip/SipSessionGroup;->access$500(Lcom/android/server/sip/SipSessionGroup;Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;)V

    #@56
    .line 1048
    invoke-virtual {v7}, Lcom/android/server/sip/SipSessionGroup$MakeCallCommand;->getTimeout()I

    #@59
    move-result v0

    #@5a
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->startSessionTimer(I)V

    #@5d
    .line 1049
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@5f
    invoke-virtual {v0, p0}, Lcom/android/server/sip/SipSessionListenerProxy;->onCalling(Landroid/net/sip/ISipSession;)V

    #@62
    move v0, v9

    #@63
    .line 1069
    .end local v7           #cmd:Lcom/android/server/sip/SipSessionGroup$MakeCallCommand;
    .end local p1
    :cond_63
    :goto_63
    return v0

    #@64
    .line 1051
    .restart local p1
    :cond_64
    instance-of v1, p1, Lcom/android/server/sip/SipSessionGroup$RegisterCommand;

    #@66
    if-eqz v1, :cond_9a

    #@68
    .line 1052
    iput v9, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mState:I

    #@6a
    .line 1053
    check-cast p1, Lcom/android/server/sip/SipSessionGroup$RegisterCommand;

    #@6c
    .end local p1
    invoke-virtual {p1}, Lcom/android/server/sip/SipSessionGroup$RegisterCommand;->getDuration()I

    #@6f
    move-result v8

    #@70
    .line 1054
    .local v8, duration:I
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@72
    invoke-static {v0}, Lcom/android/server/sip/SipSessionGroup;->access$300(Lcom/android/server/sip/SipSessionGroup;)Lcom/android/server/sip/SipHelper;

    #@75
    move-result-object v0

    #@76
    iget-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@78
    invoke-static {v1}, Lcom/android/server/sip/SipSessionGroup;->access$1400(Lcom/android/server/sip/SipSessionGroup;)Landroid/net/sip/SipProfile;

    #@7b
    move-result-object v1

    #@7c
    invoke-virtual {p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->generateTag()Ljava/lang/String;

    #@7f
    move-result-object v2

    #@80
    invoke-virtual {v0, v1, v2, v8}, Lcom/android/server/sip/SipHelper;->sendRegister(Landroid/net/sip/SipProfile;Ljava/lang/String;I)Ljavax/sip/ClientTransaction;

    #@83
    move-result-object v0

    #@84
    iput-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mClientTransaction:Ljavax/sip/ClientTransaction;

    #@86
    .line 1056
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mClientTransaction:Ljavax/sip/ClientTransaction;

    #@88
    invoke-interface {v0}, Ljavax/sip/ClientTransaction;->getDialog()Ljavax/sip/Dialog;

    #@8b
    move-result-object v0

    #@8c
    iput-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mDialog:Ljavax/sip/Dialog;

    #@8e
    .line 1057
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@90
    invoke-static {v0, p0}, Lcom/android/server/sip/SipSessionGroup;->access$500(Lcom/android/server/sip/SipSessionGroup;Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;)V

    #@93
    .line 1058
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@95
    invoke-virtual {v0, p0}, Lcom/android/server/sip/SipSessionListenerProxy;->onRegistering(Landroid/net/sip/ISipSession;)V

    #@98
    move v0, v9

    #@99
    .line 1059
    goto :goto_63

    #@9a
    .line 1060
    .end local v8           #duration:I
    .restart local p1
    :cond_9a
    invoke-static {}, Lcom/android/server/sip/SipSessionGroup;->access$1800()Ljava/util/EventObject;

    #@9d
    move-result-object v1

    #@9e
    if-ne v1, p1, :cond_63

    #@a0
    .line 1061
    const/4 v1, 0x2

    #@a1
    iput v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mState:I

    #@a3
    .line 1062
    iget-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@a5
    invoke-static {v1}, Lcom/android/server/sip/SipSessionGroup;->access$300(Lcom/android/server/sip/SipSessionGroup;)Lcom/android/server/sip/SipHelper;

    #@a8
    move-result-object v1

    #@a9
    iget-object v2, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@ab
    invoke-static {v2}, Lcom/android/server/sip/SipSessionGroup;->access$1400(Lcom/android/server/sip/SipSessionGroup;)Landroid/net/sip/SipProfile;

    #@ae
    move-result-object v2

    #@af
    invoke-virtual {p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->generateTag()Ljava/lang/String;

    #@b2
    move-result-object v3

    #@b3
    invoke-virtual {v1, v2, v3, v0}, Lcom/android/server/sip/SipHelper;->sendRegister(Landroid/net/sip/SipProfile;Ljava/lang/String;I)Ljavax/sip/ClientTransaction;

    #@b6
    move-result-object v0

    #@b7
    iput-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mClientTransaction:Ljavax/sip/ClientTransaction;

    #@b9
    .line 1064
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mClientTransaction:Ljavax/sip/ClientTransaction;

    #@bb
    invoke-interface {v0}, Ljavax/sip/ClientTransaction;->getDialog()Ljavax/sip/Dialog;

    #@be
    move-result-object v0

    #@bf
    iput-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mDialog:Ljavax/sip/Dialog;

    #@c1
    .line 1065
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@c3
    invoke-static {v0, p0}, Lcom/android/server/sip/SipSessionGroup;->access$500(Lcom/android/server/sip/SipSessionGroup;Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;)V

    #@c6
    .line 1066
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@c8
    invoke-virtual {v0, p0}, Lcom/android/server/sip/SipSessionListenerProxy;->onRegistering(Landroid/net/sip/ISipSession;)V

    #@cb
    move v0, v9

    #@cc
    .line 1067
    goto :goto_63
.end method

.method private registeringToReady(Ljava/util/EventObject;)Z
    .registers 8
    .parameter "evt"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 923
    const-string v4, "REGISTER"

    #@3
    invoke-static {v4, p1}, Lcom/android/server/sip/SipSessionGroup;->access$2100(Ljava/lang/String;Ljava/util/EventObject;)Z

    #@6
    move-result v4

    #@7
    if-eqz v4, :cond_3a

    #@9
    move-object v0, p1

    #@a
    .line 924
    check-cast v0, Ljavax/sip/ResponseEvent;

    #@c
    .line 925
    .local v0, event:Ljavax/sip/ResponseEvent;
    invoke-virtual {v0}, Ljavax/sip/ResponseEvent;->getResponse()Ljavax/sip/message/Response;

    #@f
    move-result-object v1

    #@10
    .line 927
    .local v1, response:Ljavax/sip/message/Response;
    invoke-interface {v1}, Ljavax/sip/message/Response;->getStatusCode()I

    #@13
    move-result v3

    #@14
    .line 928
    .local v3, statusCode:I
    sparse-switch v3, :sswitch_data_3c

    #@17
    .line 940
    const/16 v4, 0x1f4

    #@19
    if-lt v3, v4, :cond_3a

    #@1b
    .line 941
    invoke-direct {p0, v1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->onRegistrationFailed(Ljavax/sip/message/Response;)V

    #@1e
    move v4, v5

    #@1f
    .line 946
    .end local v0           #event:Ljavax/sip/ResponseEvent;
    .end local v1           #response:Ljavax/sip/message/Response;
    .end local v3           #statusCode:I
    .end local p1
    :goto_1f
    return v4

    #@20
    .line 930
    .restart local v0       #event:Ljavax/sip/ResponseEvent;
    .restart local v1       #response:Ljavax/sip/message/Response;
    .restart local v3       #statusCode:I
    .restart local p1
    :sswitch_20
    iget v2, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mState:I

    #@22
    .line 931
    .local v2, state:I
    if-ne v2, v5, :cond_33

    #@24
    check-cast p1, Ljavax/sip/ResponseEvent;

    #@26
    .end local p1
    invoke-virtual {p1}, Ljavax/sip/ResponseEvent;->getResponse()Ljavax/sip/message/Response;

    #@29
    move-result-object v4

    #@2a
    invoke-direct {p0, v4}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->getExpiryTime(Ljavax/sip/message/Response;)I

    #@2d
    move-result v4

    #@2e
    :goto_2e
    invoke-direct {p0, v4}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->onRegistrationDone(I)V

    #@31
    move v4, v5

    #@32
    .line 934
    goto :goto_1f

    #@33
    .line 931
    .restart local p1
    :cond_33
    const/4 v4, -0x1

    #@34
    goto :goto_2e

    #@35
    .line 937
    .end local v2           #state:I
    :sswitch_35
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->handleAuthentication(Ljavax/sip/ResponseEvent;)Z

    #@38
    move v4, v5

    #@39
    .line 938
    goto :goto_1f

    #@3a
    .line 946
    .end local v0           #event:Ljavax/sip/ResponseEvent;
    .end local v1           #response:Ljavax/sip/message/Response;
    .end local v3           #statusCode:I
    :cond_3a
    const/4 v4, 0x0

    #@3b
    goto :goto_1f

    #@3c
    .line 928
    :sswitch_data_3c
    .sparse-switch
        0xc8 -> :sswitch_20
        0x191 -> :sswitch_35
        0x197 -> :sswitch_35
    .end sparse-switch
.end method

.method private reset()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 590
    iput-boolean v2, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mInCall:Z

    #@4
    .line 591
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@6
    invoke-static {v0, p0}, Lcom/android/server/sip/SipSessionGroup;->access$1200(Lcom/android/server/sip/SipSessionGroup;Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;)V

    #@9
    .line 592
    iput-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mPeerProfile:Landroid/net/sip/SipProfile;

    #@b
    .line 593
    iput v2, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mState:I

    #@d
    .line 594
    iput-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mInviteReceived:Ljavax/sip/RequestEvent;

    #@f
    .line 595
    iput-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mPeerSessionDescription:Ljava/lang/String;

    #@11
    .line 596
    iput v2, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mAuthenticationRetryCount:I

    #@13
    .line 597
    iput-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mReferSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@15
    .line 598
    iput-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mReferredBy:Lgov/nist/javax/sip/header/extensions/ReferredByHeader;

    #@17
    .line 599
    iput-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mReplaces:Ljava/lang/String;

    #@19
    .line 601
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mDialog:Ljavax/sip/Dialog;

    #@1b
    if-eqz v0, :cond_22

    #@1d
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mDialog:Ljavax/sip/Dialog;

    #@1f
    invoke-interface {v0}, Ljavax/sip/Dialog;->delete()V

    #@22
    .line 602
    :cond_22
    iput-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mDialog:Ljavax/sip/Dialog;

    #@24
    .line 605
    :try_start_24
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mServerTransaction:Ljavax/sip/ServerTransaction;

    #@26
    if-eqz v0, :cond_2d

    #@28
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mServerTransaction:Ljavax/sip/ServerTransaction;

    #@2a
    invoke-interface {v0}, Ljavax/sip/ServerTransaction;->terminate()V
    :try_end_2d
    .catch Ljavax/sip/ObjectInUseException; {:try_start_24 .. :try_end_2d} :catch_4b

    #@2d
    .line 609
    :cond_2d
    :goto_2d
    iput-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mServerTransaction:Ljavax/sip/ServerTransaction;

    #@2f
    .line 612
    :try_start_2f
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mClientTransaction:Ljavax/sip/ClientTransaction;

    #@31
    if-eqz v0, :cond_38

    #@33
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mClientTransaction:Ljavax/sip/ClientTransaction;

    #@35
    invoke-interface {v0}, Ljavax/sip/ClientTransaction;->terminate()V
    :try_end_38
    .catch Ljavax/sip/ObjectInUseException; {:try_start_2f .. :try_end_38} :catch_49

    #@38
    .line 616
    :cond_38
    :goto_38
    iput-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mClientTransaction:Ljavax/sip/ClientTransaction;

    #@3a
    .line 618
    invoke-direct {p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->cancelSessionTimer()V

    #@3d
    .line 620
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mKeepAliveSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@3f
    if-eqz v0, :cond_48

    #@41
    .line 621
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mKeepAliveSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@43
    invoke-virtual {v0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->stopKeepAliveProcess()V

    #@46
    .line 622
    iput-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mKeepAliveSession:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@48
    .line 624
    :cond_48
    return-void

    #@49
    .line 613
    :catch_49
    move-exception v0

    #@4a
    goto :goto_38

    #@4b
    .line 606
    :catch_4b
    move-exception v0

    #@4c
    goto :goto_2d
.end method

.method private startSessionTimer(I)V
    .registers 3
    .parameter "timeout"

    #@0
    .prologue
    .line 1326
    if-lez p1, :cond_e

    #@2
    .line 1327
    new-instance v0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$SessionTimer;

    #@4
    invoke-direct {v0, p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$SessionTimer;-><init>(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;)V

    #@7
    iput-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mSessionTimer:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$SessionTimer;

    #@9
    .line 1328
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mSessionTimer:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$SessionTimer;

    #@b
    invoke-virtual {v0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$SessionTimer;->start(I)V

    #@e
    .line 1330
    :cond_e
    return-void
.end method

.method private toString(Ljavax/sip/Transaction;)Ljava/lang/String;
    .registers 10
    .parameter "transaction"

    #@0
    .prologue
    .line 854
    if-nez p1, :cond_6

    #@2
    const-string/jumbo v3, "null"

    #@5
    .line 858
    :goto_5
    return-object v3

    #@6
    .line 855
    :cond_6
    invoke-interface {p1}, Ljavax/sip/Transaction;->getRequest()Ljavax/sip/message/Request;

    #@9
    move-result-object v2

    #@a
    .line 856
    .local v2, request:Ljavax/sip/message/Request;
    invoke-interface {p1}, Ljavax/sip/Transaction;->getDialog()Ljavax/sip/Dialog;

    #@d
    move-result-object v1

    #@e
    .line 857
    .local v1, dialog:Ljavax/sip/Dialog;
    const-string v3, "CSeq"

    #@10
    invoke-interface {v2, v3}, Ljavax/sip/message/Request;->getHeader(Ljava/lang/String;)Ljavax/sip/header/Header;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Ljavax/sip/header/CSeqHeader;

    #@16
    .line 858
    .local v0, cseq:Ljavax/sip/header/CSeqHeader;
    const-string/jumbo v4, "req=%s,%s,s=%s,ds=%s,"

    #@19
    const/4 v3, 0x4

    #@1a
    new-array v5, v3, [Ljava/lang/Object;

    #@1c
    const/4 v3, 0x0

    #@1d
    invoke-interface {v2}, Ljavax/sip/message/Request;->getMethod()Ljava/lang/String;

    #@20
    move-result-object v6

    #@21
    aput-object v6, v5, v3

    #@23
    const/4 v3, 0x1

    #@24
    invoke-interface {v0}, Ljavax/sip/header/CSeqHeader;->getSeqNumber()J

    #@27
    move-result-wide v6

    #@28
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@2b
    move-result-object v6

    #@2c
    aput-object v6, v5, v3

    #@2e
    const/4 v3, 0x2

    #@2f
    invoke-interface {p1}, Ljavax/sip/Transaction;->getState()Ljavax/sip/TransactionState;

    #@32
    move-result-object v6

    #@33
    aput-object v6, v5, v3

    #@35
    const/4 v6, 0x3

    #@36
    if-nez v1, :cond_41

    #@38
    const-string v3, "-"

    #@3a
    :goto_3a
    aput-object v3, v5, v6

    #@3c
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@3f
    move-result-object v3

    #@40
    goto :goto_5

    #@41
    :cond_41
    invoke-interface {v1}, Ljavax/sip/Dialog;->getState()Ljavax/sip/DialogState;

    #@44
    move-result-object v3

    #@45
    goto :goto_3a
.end method


# virtual methods
.method public answerCall(Ljava/lang/String;I)V
    .registers 7
    .parameter "sessionDescription"
    .parameter "timeout"

    #@0
    .prologue
    .line 685
    iget-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@2
    monitor-enter v1

    #@3
    .line 686
    :try_start_3
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mPeerProfile:Landroid/net/sip/SipProfile;

    #@5
    if-nez v0, :cond_9

    #@7
    monitor-exit v1

    #@8
    .line 690
    :goto_8
    return-void

    #@9
    .line 687
    :cond_9
    new-instance v0, Lcom/android/server/sip/SipSessionGroup$MakeCallCommand;

    #@b
    iget-object v2, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@d
    iget-object v3, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mPeerProfile:Landroid/net/sip/SipProfile;

    #@f
    invoke-direct {v0, v2, v3, p1, p2}, Lcom/android/server/sip/SipSessionGroup$MakeCallCommand;-><init>(Lcom/android/server/sip/SipSessionGroup;Landroid/net/sip/SipProfile;Ljava/lang/String;I)V

    #@12
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->doCommandAsync(Ljava/util/EventObject;)V

    #@15
    .line 689
    monitor-exit v1

    #@16
    goto :goto_8

    #@17
    :catchall_17
    move-exception v0

    #@18
    monitor-exit v1
    :try_end_19
    .catchall {:try_start_3 .. :try_end_19} :catchall_17

    #@19
    throw v0
.end method

.method public changeCall(Ljava/lang/String;I)V
    .registers 7
    .parameter "sessionDescription"
    .parameter "timeout"

    #@0
    .prologue
    .line 697
    iget-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@2
    monitor-enter v1

    #@3
    .line 698
    :try_start_3
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mPeerProfile:Landroid/net/sip/SipProfile;

    #@5
    if-nez v0, :cond_9

    #@7
    monitor-exit v1

    #@8
    .line 702
    :goto_8
    return-void

    #@9
    .line 699
    :cond_9
    new-instance v0, Lcom/android/server/sip/SipSessionGroup$MakeCallCommand;

    #@b
    iget-object v2, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@d
    iget-object v3, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mPeerProfile:Landroid/net/sip/SipProfile;

    #@f
    invoke-direct {v0, v2, v3, p1, p2}, Lcom/android/server/sip/SipSessionGroup$MakeCallCommand;-><init>(Lcom/android/server/sip/SipSessionGroup;Landroid/net/sip/SipProfile;Ljava/lang/String;I)V

    #@12
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->doCommandAsync(Ljava/util/EventObject;)V

    #@15
    .line 701
    monitor-exit v1

    #@16
    goto :goto_8

    #@17
    :catchall_17
    move-exception v0

    #@18
    monitor-exit v1
    :try_end_19
    .catchall {:try_start_3 .. :try_end_19} :catchall_17

    #@19
    throw v0
.end method

.method duplicate()Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;
    .registers 4

    #@0
    .prologue
    .line 586
    new-instance v0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;

    #@2
    iget-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@4
    iget-object v2, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@6
    invoke-virtual {v2}, Lcom/android/server/sip/SipSessionListenerProxy;->getListener()Landroid/net/sip/ISipSessionListener;

    #@9
    move-result-object v2

    #@a
    invoke-direct {v0, v1, v2}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;-><init>(Lcom/android/server/sip/SipSessionGroup;Landroid/net/sip/ISipSessionListener;)V

    #@d
    return-object v0
.end method

.method public endCall()V
    .registers 2

    #@0
    .prologue
    .line 693
    invoke-static {}, Lcom/android/server/sip/SipSessionGroup;->access$1700()Ljava/util/EventObject;

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->doCommandAsync(Ljava/util/EventObject;)V

    #@7
    .line 694
    return-void
.end method

.method protected generateTag()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 723
    invoke-static {}, Ljava/lang/Math;->random()D

    #@3
    move-result-wide v0

    #@4
    const-wide/high16 v2, 0x41f0

    #@6
    mul-double/2addr v0, v2

    #@7
    double-to-long v0, v0

    #@8
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method public getCallId()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 643
    invoke-direct {p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->getTransaction()Ljavax/sip/Transaction;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Lcom/android/server/sip/SipHelper;->getCallId(Ljavax/sip/Transaction;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getLocalIp()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 631
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@2
    invoke-static {v0}, Lcom/android/server/sip/SipSessionGroup;->access$1300(Lcom/android/server/sip/SipSessionGroup;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getLocalProfile()Landroid/net/sip/SipProfile;
    .registers 2

    #@0
    .prologue
    .line 635
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@2
    invoke-static {v0}, Lcom/android/server/sip/SipSessionGroup;->access$1400(Lcom/android/server/sip/SipSessionGroup;)Landroid/net/sip/SipProfile;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getPeerProfile()Landroid/net/sip/SipProfile;
    .registers 2

    #@0
    .prologue
    .line 639
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mPeerProfile:Landroid/net/sip/SipProfile;

    #@2
    return-object v0
.end method

.method public getState()I
    .registers 2

    #@0
    .prologue
    .line 653
    iget v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mState:I

    #@2
    return v0
.end method

.method public isInCall()Z
    .registers 2

    #@0
    .prologue
    .line 627
    iget-boolean v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mInCall:Z

    #@2
    return v0
.end method

.method public makeCall(Landroid/net/sip/SipProfile;Ljava/lang/String;I)V
    .registers 6
    .parameter "peerProfile"
    .parameter "sessionDescription"
    .parameter "timeout"

    #@0
    .prologue
    .line 680
    new-instance v0, Lcom/android/server/sip/SipSessionGroup$MakeCallCommand;

    #@2
    iget-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@4
    invoke-direct {v0, v1, p1, p2, p3}, Lcom/android/server/sip/SipSessionGroup$MakeCallCommand;-><init>(Lcom/android/server/sip/SipSessionGroup;Landroid/net/sip/SipProfile;Ljava/lang/String;I)V

    #@7
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->doCommandAsync(Ljava/util/EventObject;)V

    #@a
    .line 682
    return-void
.end method

.method public process(Ljava/util/EventObject;)Z
    .registers 10
    .parameter "evt"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 737
    invoke-static {p0, p1}, Lcom/android/server/sip/SipSessionGroup;->access$600(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;Ljava/util/EventObject;)Z

    #@5
    move-result v3

    #@6
    if-eqz v3, :cond_3e

    #@8
    const-string v3, "SipSession"

    #@a
    new-instance v6, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v7, " ~~~~~   "

    #@11
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v6

    #@15
    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v6

    #@19
    const-string v7, ": "

    #@1b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v6

    #@1f
    iget v7, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mState:I

    #@21
    invoke-static {v7}, Landroid/net/sip/SipSession$State;->toString(I)Ljava/lang/String;

    #@24
    move-result-object v7

    #@25
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v6

    #@29
    const-string v7, ": processing "

    #@2b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v6

    #@2f
    invoke-static {p1}, Lcom/android/server/sip/SipSessionGroup;->access$700(Ljava/util/EventObject;)Ljava/lang/String;

    #@32
    move-result-object v7

    #@33
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v6

    #@37
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v6

    #@3b
    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 740
    :cond_3e
    iget-object v6, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@40
    monitor-enter v6

    #@41
    .line 741
    :try_start_41
    iget-object v3, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@43
    invoke-virtual {v3}, Lcom/android/server/sip/SipSessionGroup;->isClosed()Z

    #@46
    move-result v3

    #@47
    if-eqz v3, :cond_4c

    #@49
    monitor-exit v6

    #@4a
    move v3, v4

    #@4b
    .line 789
    :goto_4b
    return v3

    #@4c
    .line 743
    :cond_4c
    iget-object v3, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mKeepAliveProcess:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$KeepAliveProcess;

    #@4e
    if-eqz v3, :cond_5b

    #@50
    .line 745
    iget-object v3, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mKeepAliveProcess:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$KeepAliveProcess;

    #@52
    invoke-virtual {v3, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$KeepAliveProcess;->process(Ljava/util/EventObject;)Z

    #@55
    move-result v3

    #@56
    if-eqz v3, :cond_5b

    #@58
    monitor-exit v6

    #@59
    move v3, v5

    #@5a
    goto :goto_4b

    #@5b
    .line 748
    :cond_5b
    const/4 v1, 0x0

    #@5c
    .line 749
    .local v1, dialog:Ljavax/sip/Dialog;
    instance-of v3, p1, Ljavax/sip/RequestEvent;

    #@5e
    if-eqz v3, :cond_80

    #@60
    .line 750
    move-object v0, p1

    #@61
    check-cast v0, Ljavax/sip/RequestEvent;

    #@63
    move-object v3, v0

    #@64
    invoke-virtual {v3}, Ljavax/sip/RequestEvent;->getDialog()Ljavax/sip/Dialog;

    #@67
    move-result-object v1

    #@68
    .line 755
    :cond_68
    :goto_68
    if-eqz v1, :cond_6c

    #@6a
    iput-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mDialog:Ljavax/sip/Dialog;

    #@6c
    .line 759
    :cond_6c
    iget v3, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mState:I

    #@6e
    packed-switch v3, :pswitch_data_c0

    #@71
    .line 787
    :pswitch_71
    const/4 v2, 0x0

    #@72
    .line 789
    .local v2, processed:Z
    :goto_72
    if-nez v2, :cond_7a

    #@74
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->processExceptions(Ljava/util/EventObject;)Z

    #@77
    move-result v3

    #@78
    if-eqz v3, :cond_be

    #@7a
    :cond_7a
    move v3, v5

    #@7b
    :goto_7b
    monitor-exit v6

    #@7c
    goto :goto_4b

    #@7d
    .line 790
    .end local v1           #dialog:Ljavax/sip/Dialog;
    .end local v2           #processed:Z
    :catchall_7d
    move-exception v3

    #@7e
    monitor-exit v6
    :try_end_7f
    .catchall {:try_start_41 .. :try_end_7f} :catchall_7d

    #@7f
    throw v3

    #@80
    .line 751
    .restart local v1       #dialog:Ljavax/sip/Dialog;
    :cond_80
    :try_start_80
    instance-of v3, p1, Ljavax/sip/ResponseEvent;

    #@82
    if-eqz v3, :cond_68

    #@84
    .line 752
    move-object v0, p1

    #@85
    check-cast v0, Ljavax/sip/ResponseEvent;

    #@87
    move-object v3, v0

    #@88
    invoke-virtual {v3}, Ljavax/sip/ResponseEvent;->getDialog()Ljavax/sip/Dialog;

    #@8b
    move-result-object v1

    #@8c
    .line 753
    iget-object v7, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@8e
    move-object v0, p1

    #@8f
    check-cast v0, Ljavax/sip/ResponseEvent;

    #@91
    move-object v3, v0

    #@92
    invoke-static {v7, v3}, Lcom/android/server/sip/SipSessionGroup;->access$2000(Lcom/android/server/sip/SipSessionGroup;Ljavax/sip/ResponseEvent;)V

    #@95
    goto :goto_68

    #@96
    .line 762
    :pswitch_96
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->registeringToReady(Ljava/util/EventObject;)Z

    #@99
    move-result v2

    #@9a
    .line 763
    .restart local v2       #processed:Z
    goto :goto_72

    #@9b
    .line 765
    .end local v2           #processed:Z
    :pswitch_9b
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->readyForCall(Ljava/util/EventObject;)Z

    #@9e
    move-result v2

    #@9f
    .line 766
    .restart local v2       #processed:Z
    goto :goto_72

    #@a0
    .line 768
    .end local v2           #processed:Z
    :pswitch_a0
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->incomingCall(Ljava/util/EventObject;)Z

    #@a3
    move-result v2

    #@a4
    .line 769
    .restart local v2       #processed:Z
    goto :goto_72

    #@a5
    .line 771
    .end local v2           #processed:Z
    :pswitch_a5
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->incomingCallToInCall(Ljava/util/EventObject;)Z

    #@a8
    move-result v2

    #@a9
    .line 772
    .restart local v2       #processed:Z
    goto :goto_72

    #@aa
    .line 775
    .end local v2           #processed:Z
    :pswitch_aa
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->outgoingCall(Ljava/util/EventObject;)Z

    #@ad
    move-result v2

    #@ae
    .line 776
    .restart local v2       #processed:Z
    goto :goto_72

    #@af
    .line 778
    .end local v2           #processed:Z
    :pswitch_af
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->outgoingCallToReady(Ljava/util/EventObject;)Z

    #@b2
    move-result v2

    #@b3
    .line 779
    .restart local v2       #processed:Z
    goto :goto_72

    #@b4
    .line 781
    .end local v2           #processed:Z
    :pswitch_b4
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->inCall(Ljava/util/EventObject;)Z

    #@b7
    move-result v2

    #@b8
    .line 782
    .restart local v2       #processed:Z
    goto :goto_72

    #@b9
    .line 784
    .end local v2           #processed:Z
    :pswitch_b9
    invoke-direct {p0, p1}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->endingCall(Ljava/util/EventObject;)Z
    :try_end_bc
    .catchall {:try_start_80 .. :try_end_bc} :catchall_7d

    #@bc
    move-result v2

    #@bd
    .line 785
    .restart local v2       #processed:Z
    goto :goto_72

    #@be
    :cond_be
    move v3, v4

    #@bf
    .line 789
    goto :goto_7b

    #@c0
    .line 759
    :pswitch_data_c0
    .packed-switch 0x0
        :pswitch_9b
        :pswitch_96
        :pswitch_96
        :pswitch_a0
        :pswitch_a5
        :pswitch_aa
        :pswitch_aa
        :pswitch_af
        :pswitch_b4
        :pswitch_71
        :pswitch_b9
    .end packed-switch
.end method

.method public register(I)V
    .registers 4
    .parameter "duration"

    #@0
    .prologue
    .line 705
    new-instance v0, Lcom/android/server/sip/SipSessionGroup$RegisterCommand;

    #@2
    iget-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@4
    invoke-direct {v0, v1, p1}, Lcom/android/server/sip/SipSessionGroup$RegisterCommand;-><init>(Lcom/android/server/sip/SipSessionGroup;I)V

    #@7
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->doCommandAsync(Ljava/util/EventObject;)V

    #@a
    .line 706
    return-void
.end method

.method public setListener(Landroid/net/sip/ISipSessionListener;)V
    .registers 4
    .parameter "listener"

    #@0
    .prologue
    .line 657
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@2
    instance-of v1, p1, Lcom/android/server/sip/SipSessionListenerProxy;

    #@4
    if-eqz v1, :cond_c

    #@6
    check-cast p1, Lcom/android/server/sip/SipSessionListenerProxy;

    #@8
    .end local p1
    invoke-virtual {p1}, Lcom/android/server/sip/SipSessionListenerProxy;->getListener()Landroid/net/sip/ISipSessionListener;

    #@b
    move-result-object p1

    #@c
    :cond_c
    invoke-virtual {v0, p1}, Lcom/android/server/sip/SipSessionListenerProxy;->setListener(Landroid/net/sip/ISipSessionListener;)V

    #@f
    .line 660
    return-void
.end method

.method public startKeepAliveProcess(ILandroid/net/sip/SipProfile;Lcom/android/server/sip/SipSessionGroup$KeepAliveProcessCallback;)V
    .registers 7
    .parameter "interval"
    .parameter "peerProfile"
    .parameter "callback"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 1482
    iget-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@2
    monitor-enter v1

    #@3
    .line 1483
    :try_start_3
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mKeepAliveProcess:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$KeepAliveProcess;

    #@5
    if-eqz v0, :cond_12

    #@7
    .line 1484
    new-instance v0, Ljavax/sip/SipException;

    #@9
    const-string v2, "Cannot create more than one keepalive process in a SipSession"

    #@b
    invoke-direct {v0, v2}, Ljavax/sip/SipException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 1491
    :catchall_f
    move-exception v0

    #@10
    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    #@11
    throw v0

    #@12
    .line 1487
    :cond_12
    :try_start_12
    iput-object p2, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mPeerProfile:Landroid/net/sip/SipProfile;

    #@14
    .line 1488
    new-instance v0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$KeepAliveProcess;

    #@16
    invoke-direct {v0, p0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$KeepAliveProcess;-><init>(Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;)V

    #@19
    iput-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mKeepAliveProcess:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$KeepAliveProcess;

    #@1b
    .line 1489
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mProxy:Lcom/android/server/sip/SipSessionListenerProxy;

    #@1d
    iget-object v2, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mKeepAliveProcess:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$KeepAliveProcess;

    #@1f
    invoke-virtual {v0, v2}, Lcom/android/server/sip/SipSessionListenerProxy;->setListener(Landroid/net/sip/ISipSessionListener;)V

    #@22
    .line 1490
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mKeepAliveProcess:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$KeepAliveProcess;

    #@24
    invoke-virtual {v0, p1, p3}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$KeepAliveProcess;->start(ILcom/android/server/sip/SipSessionGroup$KeepAliveProcessCallback;)V

    #@27
    .line 1491
    monitor-exit v1
    :try_end_28
    .catchall {:try_start_12 .. :try_end_28} :catchall_f

    #@28
    .line 1492
    return-void
.end method

.method public startKeepAliveProcess(ILcom/android/server/sip/SipSessionGroup$KeepAliveProcessCallback;)V
    .registers 5
    .parameter "interval"
    .parameter "callback"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/sip/SipException;
        }
    .end annotation

    #@0
    .prologue
    .line 1473
    iget-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@2
    monitor-enter v1

    #@3
    .line 1474
    :try_start_3
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@5
    invoke-static {v0}, Lcom/android/server/sip/SipSessionGroup;->access$1400(Lcom/android/server/sip/SipSessionGroup;)Landroid/net/sip/SipProfile;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p0, p1, v0, p2}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->startKeepAliveProcess(ILandroid/net/sip/SipProfile;Lcom/android/server/sip/SipSessionGroup$KeepAliveProcessCallback;)V

    #@c
    .line 1475
    monitor-exit v1

    #@d
    .line 1476
    return-void

    #@e
    .line 1475
    :catchall_e
    move-exception v0

    #@f
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    #@10
    throw v0
.end method

.method public stopKeepAliveProcess()V
    .registers 3

    #@0
    .prologue
    .line 1495
    iget-object v1, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->this$0:Lcom/android/server/sip/SipSessionGroup;

    #@2
    monitor-enter v1

    #@3
    .line 1496
    :try_start_3
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mKeepAliveProcess:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$KeepAliveProcess;

    #@5
    if-eqz v0, :cond_f

    #@7
    .line 1497
    iget-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mKeepAliveProcess:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$KeepAliveProcess;

    #@9
    invoke-virtual {v0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$KeepAliveProcess;->stop()V

    #@c
    .line 1498
    const/4 v0, 0x0

    #@d
    iput-object v0, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mKeepAliveProcess:Lcom/android/server/sip/SipSessionGroup$SipSessionImpl$KeepAliveProcess;

    #@f
    .line 1500
    :cond_f
    monitor-exit v1

    #@10
    .line 1501
    return-void

    #@11
    .line 1500
    :catchall_11
    move-exception v0

    #@12
    monitor-exit v1
    :try_end_13
    .catchall {:try_start_3 .. :try_end_13} :catchall_11

    #@13
    throw v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 728
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 729
    .local v1, s:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v3, "@"

    #@b
    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@e
    move-result v3

    #@f
    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    const-string v3, ":"

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    iget v3, p0, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->mState:I

    #@1f
    invoke-static {v3}, Landroid/net/sip/SipSession$State;->toString(I)Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_2a
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_2a} :catch_2c

    #@2a
    move-result-object v2

    #@2b
    .line 732
    .end local v1           #s:Ljava/lang/String;
    :goto_2b
    return-object v2

    #@2c
    .line 731
    :catch_2c
    move-exception v0

    #@2d
    .line 732
    .local v0, e:Ljava/lang/Throwable;
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    goto :goto_2b
.end method

.method public unregister()V
    .registers 2

    #@0
    .prologue
    .line 709
    invoke-static {}, Lcom/android/server/sip/SipSessionGroup;->access$1800()Ljava/util/EventObject;

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0}, Lcom/android/server/sip/SipSessionGroup$SipSessionImpl;->doCommandAsync(Ljava/util/EventObject;)V

    #@7
    .line 710
    return-void
.end method
