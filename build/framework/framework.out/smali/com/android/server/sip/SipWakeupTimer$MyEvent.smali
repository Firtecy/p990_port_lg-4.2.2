.class Lcom/android/server/sip/SipWakeupTimer$MyEvent;
.super Ljava/lang/Object;
.source "SipWakeupTimer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/sip/SipWakeupTimer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MyEvent"
.end annotation


# instance fields
.field mCallback:Ljava/lang/Runnable;

.field mLastTriggerTime:J

.field mMaxPeriod:I

.field mPeriod:I

.field mTriggerTime:J


# direct methods
.method constructor <init>(ILjava/lang/Runnable;J)V
    .registers 5
    .parameter "period"
    .parameter "callback"
    .parameter "now"

    #@0
    .prologue
    .line 305
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 306
    iput p1, p0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mMaxPeriod:I

    #@5
    iput p1, p0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mPeriod:I

    #@7
    .line 307
    iput-object p2, p0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mCallback:Ljava/lang/Runnable;

    #@9
    .line 308
    iput-wide p3, p0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mLastTriggerTime:J

    #@b
    .line 309
    return-void
.end method

.method private toString(Ljava/lang/Object;)Ljava/lang/String;
    .registers 5
    .parameter "o"

    #@0
    .prologue
    .line 320
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 321
    .local v1, s:Ljava/lang/String;
    const-string v2, "$"

    #@6
    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@9
    move-result v0

    #@a
    .line 322
    .local v0, index:I
    if-lez v0, :cond_12

    #@c
    add-int/lit8 v2, v0, 0x1

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@11
    move-result-object v1

    #@12
    .line 323
    :cond_12
    return-object v1
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 313
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 314
    .local v0, s:Ljava/lang/String;
    const-string v1, "@"

    #@6
    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@9
    move-result v1

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    .line 315
    new-instance v1, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, ":"

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    iget v2, p0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mPeriod:I

    #@1f
    div-int/lit16 v2, v2, 0x3e8

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    const-string v2, ":"

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    iget v2, p0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mMaxPeriod:I

    #@2d
    div-int/lit16 v2, v2, 0x3e8

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    const-string v2, ":"

    #@35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v1

    #@39
    iget-object v2, p0, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->mCallback:Ljava/lang/Runnable;

    #@3b
    invoke-direct {p0, v2}, Lcom/android/server/sip/SipWakeupTimer$MyEvent;->toString(Ljava/lang/Object;)Ljava/lang/String;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v1

    #@47
    return-object v1
.end method
