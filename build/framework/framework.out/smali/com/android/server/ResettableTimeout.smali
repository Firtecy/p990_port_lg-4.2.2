.class abstract Lcom/android/server/ResettableTimeout;
.super Ljava/lang/Object;
.source "ResettableTimeout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/ResettableTimeout$1;,
        Lcom/android/server/ResettableTimeout$T;
    }
.end annotation


# instance fields
.field private mLock:Landroid/os/ConditionVariable;

.field private volatile mOffAt:J

.field private volatile mOffCalled:Z

.field private mThread:Ljava/lang/Thread;


# direct methods
.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 29
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 123
    new-instance v0, Landroid/os/ConditionVariable;

    #@5
    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/ResettableTimeout;->mLock:Landroid/os/ConditionVariable;

    #@a
    return-void
.end method

.method static synthetic access$100(Lcom/android/server/ResettableTimeout;)Landroid/os/ConditionVariable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 29
    iget-object v0, p0, Lcom/android/server/ResettableTimeout;->mLock:Landroid/os/ConditionVariable;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/server/ResettableTimeout;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 29
    iget-wide v0, p0, Lcom/android/server/ResettableTimeout;->mOffAt:J

    #@2
    return-wide v0
.end method

.method static synthetic access$302(Lcom/android/server/ResettableTimeout;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/android/server/ResettableTimeout;->mOffCalled:Z

    #@2
    return p1
.end method

.method static synthetic access$402(Lcom/android/server/ResettableTimeout;Ljava/lang/Thread;)Ljava/lang/Thread;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 29
    iput-object p1, p0, Lcom/android/server/ResettableTimeout;->mThread:Ljava/lang/Thread;

    #@2
    return-object p1
.end method


# virtual methods
.method public cancel()V
    .registers 3

    #@0
    .prologue
    .line 85
    monitor-enter p0

    #@1
    .line 86
    const-wide/16 v0, 0x0

    #@3
    :try_start_3
    iput-wide v0, p0, Lcom/android/server/ResettableTimeout;->mOffAt:J

    #@5
    .line 87
    iget-object v0, p0, Lcom/android/server/ResettableTimeout;->mThread:Ljava/lang/Thread;

    #@7
    if-eqz v0, :cond_11

    #@9
    .line 88
    iget-object v0, p0, Lcom/android/server/ResettableTimeout;->mThread:Ljava/lang/Thread;

    #@b
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    #@e
    .line 89
    const/4 v0, 0x0

    #@f
    iput-object v0, p0, Lcom/android/server/ResettableTimeout;->mThread:Ljava/lang/Thread;

    #@11
    .line 91
    :cond_11
    iget-boolean v0, p0, Lcom/android/server/ResettableTimeout;->mOffCalled:Z

    #@13
    if-nez v0, :cond_1b

    #@15
    .line 92
    const/4 v0, 0x1

    #@16
    iput-boolean v0, p0, Lcom/android/server/ResettableTimeout;->mOffCalled:Z

    #@18
    .line 93
    invoke-virtual {p0}, Lcom/android/server/ResettableTimeout;->off()V

    #@1b
    .line 95
    :cond_1b
    monitor-exit p0

    #@1c
    .line 96
    return-void

    #@1d
    .line 95
    :catchall_1d
    move-exception v0

    #@1e
    monitor-exit p0
    :try_end_1f
    .catchall {:try_start_3 .. :try_end_1f} :catchall_1d

    #@1f
    throw v0
.end method

.method public go(J)V
    .registers 6
    .parameter "milliseconds"

    #@0
    .prologue
    .line 54
    monitor-enter p0

    #@1
    .line 55
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@4
    move-result-wide v1

    #@5
    add-long/2addr v1, p1

    #@6
    iput-wide v1, p0, Lcom/android/server/ResettableTimeout;->mOffAt:J

    #@8
    .line 64
    iget-object v1, p0, Lcom/android/server/ResettableTimeout;->mThread:Ljava/lang/Thread;

    #@a
    if-nez v1, :cond_2c

    #@c
    .line 65
    const/4 v0, 0x0

    #@d
    .line 66
    .local v0, alreadyOn:Z
    iget-object v1, p0, Lcom/android/server/ResettableTimeout;->mLock:Landroid/os/ConditionVariable;

    #@f
    invoke-virtual {v1}, Landroid/os/ConditionVariable;->close()V

    #@12
    .line 67
    new-instance v1, Lcom/android/server/ResettableTimeout$T;

    #@14
    const/4 v2, 0x0

    #@15
    invoke-direct {v1, p0, v2}, Lcom/android/server/ResettableTimeout$T;-><init>(Lcom/android/server/ResettableTimeout;Lcom/android/server/ResettableTimeout$1;)V

    #@18
    iput-object v1, p0, Lcom/android/server/ResettableTimeout;->mThread:Ljava/lang/Thread;

    #@1a
    .line 68
    iget-object v1, p0, Lcom/android/server/ResettableTimeout;->mThread:Ljava/lang/Thread;

    #@1c
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    #@1f
    .line 69
    iget-object v1, p0, Lcom/android/server/ResettableTimeout;->mLock:Landroid/os/ConditionVariable;

    #@21
    invoke-virtual {v1}, Landroid/os/ConditionVariable;->block()V

    #@24
    .line 70
    const/4 v1, 0x0

    #@25
    iput-boolean v1, p0, Lcom/android/server/ResettableTimeout;->mOffCalled:Z

    #@27
    .line 76
    :goto_27
    invoke-virtual {p0, v0}, Lcom/android/server/ResettableTimeout;->on(Z)V

    #@2a
    .line 77
    monitor-exit p0

    #@2b
    .line 78
    return-void

    #@2c
    .line 72
    .end local v0           #alreadyOn:Z
    :cond_2c
    const/4 v0, 0x1

    #@2d
    .line 74
    .restart local v0       #alreadyOn:Z
    iget-object v1, p0, Lcom/android/server/ResettableTimeout;->mThread:Ljava/lang/Thread;

    #@2f
    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    #@32
    goto :goto_27

    #@33
    .line 77
    .end local v0           #alreadyOn:Z
    :catchall_33
    move-exception v1

    #@34
    monitor-exit p0
    :try_end_35
    .catchall {:try_start_1 .. :try_end_35} :catchall_33

    #@35
    throw v1
.end method

.method public abstract off()V
.end method

.method public abstract on(Z)V
.end method
