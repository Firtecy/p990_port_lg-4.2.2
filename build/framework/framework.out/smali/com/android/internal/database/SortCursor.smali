.class public Lcom/android/internal/database/SortCursor;
.super Landroid/database/AbstractCursor;
.source "SortCursor.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SortCursor"


# instance fields
.field private final ROWCACHESIZE:I

.field private mCurRowNumCache:[[I

.field private mCursor:Landroid/database/Cursor;

.field private mCursorCache:[I

.field private mCursors:[Landroid/database/Cursor;

.field private mLastCacheHit:I

.field private mObserver:Landroid/database/DataSetObserver;

.field private mRowNumCache:[I

.field private mSortColumns:[I


# direct methods
.method public constructor <init>([Landroid/database/Cursor;Ljava/lang/String;)V
    .registers 11
    .parameter "cursors"
    .parameter "sortcolumn"

    #@0
    .prologue
    const/16 v7, 0x40

    #@2
    .line 56
    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    #@5
    .line 34
    iput v7, p0, Lcom/android/internal/database/SortCursor;->ROWCACHESIZE:I

    #@7
    .line 35
    new-array v5, v7, [I

    #@9
    iput-object v5, p0, Lcom/android/internal/database/SortCursor;->mRowNumCache:[I

    #@b
    .line 36
    new-array v5, v7, [I

    #@d
    iput-object v5, p0, Lcom/android/internal/database/SortCursor;->mCursorCache:[I

    #@f
    .line 38
    const/4 v5, -0x1

    #@10
    iput v5, p0, Lcom/android/internal/database/SortCursor;->mLastCacheHit:I

    #@12
    .line 40
    new-instance v5, Lcom/android/internal/database/SortCursor$1;

    #@14
    invoke-direct {v5, p0}, Lcom/android/internal/database/SortCursor$1;-><init>(Lcom/android/internal/database/SortCursor;)V

    #@17
    iput-object v5, p0, Lcom/android/internal/database/SortCursor;->mObserver:Landroid/database/DataSetObserver;

    #@19
    .line 57
    iput-object p1, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@1b
    .line 59
    iget-object v5, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@1d
    array-length v3, v5

    #@1e
    .line 60
    .local v3, length:I
    new-array v5, v3, [I

    #@20
    iput-object v5, p0, Lcom/android/internal/database/SortCursor;->mSortColumns:[I

    #@22
    .line 61
    const/4 v1, 0x0

    #@23
    .local v1, i:I
    :goto_23
    if-ge v1, v3, :cond_4b

    #@25
    .line 62
    iget-object v5, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@27
    aget-object v5, v5, v1

    #@29
    if-nez v5, :cond_2e

    #@2b
    .line 61
    :goto_2b
    add-int/lit8 v1, v1, 0x1

    #@2d
    goto :goto_23

    #@2e
    .line 65
    :cond_2e
    iget-object v5, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@30
    aget-object v5, v5, v1

    #@32
    iget-object v6, p0, Lcom/android/internal/database/SortCursor;->mObserver:Landroid/database/DataSetObserver;

    #@34
    invoke-interface {v5, v6}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    #@37
    .line 67
    iget-object v5, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@39
    aget-object v5, v5, v1

    #@3b
    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    #@3e
    .line 70
    iget-object v5, p0, Lcom/android/internal/database/SortCursor;->mSortColumns:[I

    #@40
    iget-object v6, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@42
    aget-object v6, v6, v1

    #@44
    invoke-interface {v6, p2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@47
    move-result v6

    #@48
    aput v6, v5, v1

    #@4a
    goto :goto_2b

    #@4b
    .line 72
    :cond_4b
    const/4 v5, 0x0

    #@4c
    iput-object v5, p0, Lcom/android/internal/database/SortCursor;->mCursor:Landroid/database/Cursor;

    #@4e
    .line 73
    const-string v4, ""

    #@50
    .line 74
    .local v4, smallest:Ljava/lang/String;
    const/4 v2, 0x0

    #@51
    .local v2, j:I
    :goto_51
    if-ge v2, v3, :cond_84

    #@53
    .line 75
    iget-object v5, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@55
    aget-object v5, v5, v2

    #@57
    if-eqz v5, :cond_63

    #@59
    iget-object v5, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@5b
    aget-object v5, v5, v2

    #@5d
    invoke-interface {v5}, Landroid/database/Cursor;->isAfterLast()Z

    #@60
    move-result v5

    #@61
    if-eqz v5, :cond_66

    #@63
    .line 74
    :cond_63
    :goto_63
    add-int/lit8 v2, v2, 0x1

    #@65
    goto :goto_51

    #@66
    .line 77
    :cond_66
    iget-object v5, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@68
    aget-object v5, v5, v2

    #@6a
    iget-object v6, p0, Lcom/android/internal/database/SortCursor;->mSortColumns:[I

    #@6c
    aget v6, v6, v2

    #@6e
    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@71
    move-result-object v0

    #@72
    .line 78
    .local v0, current:Ljava/lang/String;
    iget-object v5, p0, Lcom/android/internal/database/SortCursor;->mCursor:Landroid/database/Cursor;

    #@74
    if-eqz v5, :cond_7c

    #@76
    invoke-virtual {v0, v4}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@79
    move-result v5

    #@7a
    if-gez v5, :cond_63

    #@7c
    .line 79
    :cond_7c
    move-object v4, v0

    #@7d
    .line 80
    iget-object v5, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@7f
    aget-object v5, v5, v2

    #@81
    iput-object v5, p0, Lcom/android/internal/database/SortCursor;->mCursor:Landroid/database/Cursor;

    #@83
    goto :goto_63

    #@84
    .line 84
    .end local v0           #current:Ljava/lang/String;
    :cond_84
    iget-object v5, p0, Lcom/android/internal/database/SortCursor;->mRowNumCache:[I

    #@86
    array-length v5, v5

    #@87
    add-int/lit8 v1, v5, -0x1

    #@89
    :goto_89
    if-ltz v1, :cond_93

    #@8b
    .line 85
    iget-object v5, p0, Lcom/android/internal/database/SortCursor;->mRowNumCache:[I

    #@8d
    const/4 v6, -0x2

    #@8e
    aput v6, v5, v1

    #@90
    .line 84
    add-int/lit8 v1, v1, -0x1

    #@92
    goto :goto_89

    #@93
    .line 87
    :cond_93
    filled-new-array {v7, v3}, [I

    #@96
    move-result-object v5

    #@97
    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@99
    invoke-static {v6, v5}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    #@9c
    move-result-object v5

    #@9d
    check-cast v5, [[I

    #@9f
    iput-object v5, p0, Lcom/android/internal/database/SortCursor;->mCurRowNumCache:[[I

    #@a1
    .line 88
    return-void
.end method

.method static synthetic access$002(Lcom/android/internal/database/SortCursor;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 28
    iput p1, p0, Landroid/database/AbstractCursor;->mPos:I

    #@2
    return p1
.end method

.method static synthetic access$102(Lcom/android/internal/database/SortCursor;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 28
    iput p1, p0, Landroid/database/AbstractCursor;->mPos:I

    #@2
    return p1
.end method


# virtual methods
.method public close()V
    .registers 4

    #@0
    .prologue
    .line 267
    iget-object v2, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@2
    array-length v1, v2

    #@3
    .line 268
    .local v1, length:I
    const/4 v0, 0x0

    #@4
    .local v0, i:I
    :goto_4
    if-ge v0, v1, :cond_17

    #@6
    .line 269
    iget-object v2, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@8
    aget-object v2, v2, v0

    #@a
    if-nez v2, :cond_f

    #@c
    .line 268
    :goto_c
    add-int/lit8 v0, v0, 0x1

    #@e
    goto :goto_4

    #@f
    .line 270
    :cond_f
    iget-object v2, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@11
    aget-object v2, v2, v0

    #@13
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    #@16
    goto :goto_c

    #@17
    .line 272
    :cond_17
    return-void
.end method

.method public deactivate()V
    .registers 4

    #@0
    .prologue
    .line 258
    iget-object v2, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@2
    array-length v1, v2

    #@3
    .line 259
    .local v1, length:I
    const/4 v0, 0x0

    #@4
    .local v0, i:I
    :goto_4
    if-ge v0, v1, :cond_17

    #@6
    .line 260
    iget-object v2, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@8
    aget-object v2, v2, v0

    #@a
    if-nez v2, :cond_f

    #@c
    .line 259
    :goto_c
    add-int/lit8 v0, v0, 0x1

    #@e
    goto :goto_4

    #@f
    .line 261
    :cond_f
    iget-object v2, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@11
    aget-object v2, v2, v0

    #@13
    invoke-interface {v2}, Landroid/database/Cursor;->deactivate()V

    #@16
    goto :goto_c

    #@17
    .line 263
    :cond_17
    return-void
.end method

.method public getBlob(I)[B
    .registers 3
    .parameter "column"

    #@0
    .prologue
    .line 234
    iget-object v0, p0, Lcom/android/internal/database/SortCursor;->mCursor:Landroid/database/Cursor;

    #@2
    invoke-interface {v0, p1}, Landroid/database/Cursor;->getBlob(I)[B

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getColumnNames()[Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 240
    iget-object v2, p0, Lcom/android/internal/database/SortCursor;->mCursor:Landroid/database/Cursor;

    #@2
    if-eqz v2, :cond_b

    #@4
    .line 241
    iget-object v2, p0, Lcom/android/internal/database/SortCursor;->mCursor:Landroid/database/Cursor;

    #@6
    invoke-interface {v2}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    .line 248
    :goto_a
    return-object v2

    #@b
    .line 245
    :cond_b
    iget-object v2, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@d
    array-length v1, v2

    #@e
    .line 246
    .local v1, length:I
    const/4 v0, 0x0

    #@f
    .local v0, i:I
    :goto_f
    if-ge v0, v1, :cond_23

    #@11
    .line 247
    iget-object v2, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@13
    aget-object v2, v2, v0

    #@15
    if-eqz v2, :cond_20

    #@17
    .line 248
    iget-object v2, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@19
    aget-object v2, v2, v0

    #@1b
    invoke-interface {v2}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    goto :goto_a

    #@20
    .line 246
    :cond_20
    add-int/lit8 v0, v0, 0x1

    #@22
    goto :goto_f

    #@23
    .line 251
    :cond_23
    new-instance v2, Ljava/lang/IllegalStateException;

    #@25
    const-string v3, "No cursor that can return names"

    #@27
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@2a
    throw v2
.end method

.method public getCount()I
    .registers 5

    #@0
    .prologue
    .line 93
    const/4 v0, 0x0

    #@1
    .line 94
    .local v0, count:I
    iget-object v3, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@3
    array-length v2, v3

    #@4
    .line 95
    .local v2, length:I
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    if-ge v1, v2, :cond_19

    #@7
    .line 96
    iget-object v3, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@9
    aget-object v3, v3, v1

    #@b
    if-eqz v3, :cond_16

    #@d
    .line 97
    iget-object v3, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@f
    aget-object v3, v3, v1

    #@11
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    #@14
    move-result v3

    #@15
    add-int/2addr v0, v3

    #@16
    .line 95
    :cond_16
    add-int/lit8 v1, v1, 0x1

    #@18
    goto :goto_5

    #@19
    .line 100
    :cond_19
    return v0
.end method

.method public getDouble(I)D
    .registers 4
    .parameter "column"

    #@0
    .prologue
    .line 217
    iget-object v0, p0, Lcom/android/internal/database/SortCursor;->mCursor:Landroid/database/Cursor;

    #@2
    invoke-interface {v0, p1}, Landroid/database/Cursor;->getDouble(I)D

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method public getFloat(I)F
    .registers 3
    .parameter "column"

    #@0
    .prologue
    .line 211
    iget-object v0, p0, Lcom/android/internal/database/SortCursor;->mCursor:Landroid/database/Cursor;

    #@2
    invoke-interface {v0, p1}, Landroid/database/Cursor;->getFloat(I)F

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getInt(I)I
    .registers 3
    .parameter "column"

    #@0
    .prologue
    .line 199
    iget-object v0, p0, Lcom/android/internal/database/SortCursor;->mCursor:Landroid/database/Cursor;

    #@2
    invoke-interface {v0, p1}, Landroid/database/Cursor;->getInt(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getLong(I)J
    .registers 4
    .parameter "column"

    #@0
    .prologue
    .line 205
    iget-object v0, p0, Lcom/android/internal/database/SortCursor;->mCursor:Landroid/database/Cursor;

    #@2
    invoke-interface {v0, p1}, Landroid/database/Cursor;->getLong(I)J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method public getShort(I)S
    .registers 3
    .parameter "column"

    #@0
    .prologue
    .line 193
    iget-object v0, p0, Lcom/android/internal/database/SortCursor;->mCursor:Landroid/database/Cursor;

    #@2
    invoke-interface {v0, p1}, Landroid/database/Cursor;->getShort(I)S

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getString(I)Ljava/lang/String;
    .registers 3
    .parameter "column"

    #@0
    .prologue
    .line 187
    iget-object v0, p0, Lcom/android/internal/database/SortCursor;->mCursor:Landroid/database/Cursor;

    #@2
    invoke-interface {v0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getType(I)I
    .registers 3
    .parameter "column"

    #@0
    .prologue
    .line 222
    iget-object v0, p0, Lcom/android/internal/database/SortCursor;->mCursor:Landroid/database/Cursor;

    #@2
    invoke-interface {v0, p1}, Landroid/database/Cursor;->getType(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isNull(I)Z
    .registers 3
    .parameter "column"

    #@0
    .prologue
    .line 228
    iget-object v0, p0, Lcom/android/internal/database/SortCursor;->mCursor:Landroid/database/Cursor;

    #@2
    invoke-interface {v0, p1}, Landroid/database/Cursor;->isNull(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public onMove(II)Z
    .registers 16
    .parameter "oldPosition"
    .parameter "newPosition"

    #@0
    .prologue
    const/4 v12, -0x1

    #@1
    const/4 v8, 0x1

    #@2
    .line 106
    if-ne p1, p2, :cond_5

    #@4
    .line 181
    :goto_4
    return v8

    #@5
    .line 117
    :cond_5
    rem-int/lit8 v0, p2, 0x40

    #@7
    .line 119
    .local v0, cache_entry:I
    iget-object v9, p0, Lcom/android/internal/database/SortCursor;->mRowNumCache:[I

    #@9
    aget v9, v9, v0

    #@b
    if-ne v9, p2, :cond_33

    #@d
    .line 120
    iget-object v9, p0, Lcom/android/internal/database/SortCursor;->mCursorCache:[I

    #@f
    aget v7, v9, v0

    #@11
    .line 121
    .local v7, which:I
    iget-object v9, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@13
    aget-object v9, v9, v7

    #@15
    iput-object v9, p0, Lcom/android/internal/database/SortCursor;->mCursor:Landroid/database/Cursor;

    #@17
    .line 122
    iget-object v9, p0, Lcom/android/internal/database/SortCursor;->mCursor:Landroid/database/Cursor;

    #@19
    if-nez v9, :cond_25

    #@1b
    .line 123
    const-string v8, "SortCursor"

    #@1d
    const-string/jumbo v9, "onMove: cache results in a null cursor."

    #@20
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 124
    const/4 v8, 0x0

    #@24
    goto :goto_4

    #@25
    .line 126
    :cond_25
    iget-object v9, p0, Lcom/android/internal/database/SortCursor;->mCursor:Landroid/database/Cursor;

    #@27
    iget-object v10, p0, Lcom/android/internal/database/SortCursor;->mCurRowNumCache:[[I

    #@29
    aget-object v10, v10, v0

    #@2b
    aget v10, v10, v7

    #@2d
    invoke-interface {v9, v10}, Landroid/database/Cursor;->moveToPosition(I)Z

    #@30
    .line 127
    iput v0, p0, Lcom/android/internal/database/SortCursor;->mLastCacheHit:I

    #@32
    goto :goto_4

    #@33
    .line 131
    .end local v7           #which:I
    :cond_33
    const/4 v9, 0x0

    #@34
    iput-object v9, p0, Lcom/android/internal/database/SortCursor;->mCursor:Landroid/database/Cursor;

    #@36
    .line 132
    iget-object v9, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@38
    array-length v4, v9

    #@39
    .line 134
    .local v4, length:I
    iget v9, p0, Lcom/android/internal/database/SortCursor;->mLastCacheHit:I

    #@3b
    if-ltz v9, :cond_59

    #@3d
    .line 135
    const/4 v2, 0x0

    #@3e
    .local v2, i:I
    :goto_3e
    if-ge v2, v4, :cond_59

    #@40
    .line 136
    iget-object v9, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@42
    aget-object v9, v9, v2

    #@44
    if-nez v9, :cond_49

    #@46
    .line 135
    :goto_46
    add-int/lit8 v2, v2, 0x1

    #@48
    goto :goto_3e

    #@49
    .line 137
    :cond_49
    iget-object v9, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@4b
    aget-object v9, v9, v2

    #@4d
    iget-object v10, p0, Lcom/android/internal/database/SortCursor;->mCurRowNumCache:[[I

    #@4f
    iget v11, p0, Lcom/android/internal/database/SortCursor;->mLastCacheHit:I

    #@51
    aget-object v10, v10, v11

    #@53
    aget v10, v10, v2

    #@55
    invoke-interface {v9, v10}, Landroid/database/Cursor;->moveToPosition(I)Z

    #@58
    goto :goto_46

    #@59
    .line 141
    .end local v2           #i:I
    :cond_59
    if-lt p2, p1, :cond_5d

    #@5b
    if-ne p1, v12, :cond_72

    #@5d
    .line 142
    :cond_5d
    const/4 v2, 0x0

    #@5e
    .restart local v2       #i:I
    :goto_5e
    if-ge v2, v4, :cond_71

    #@60
    .line 143
    iget-object v9, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@62
    aget-object v9, v9, v2

    #@64
    if-nez v9, :cond_69

    #@66
    .line 142
    :goto_66
    add-int/lit8 v2, v2, 0x1

    #@68
    goto :goto_5e

    #@69
    .line 144
    :cond_69
    iget-object v9, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@6b
    aget-object v9, v9, v2

    #@6d
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    #@70
    goto :goto_66

    #@71
    .line 146
    :cond_71
    const/4 p1, 0x0

    #@72
    .line 148
    .end local v2           #i:I
    :cond_72
    if-gez p1, :cond_75

    #@74
    .line 149
    const/4 p1, 0x0

    #@75
    .line 153
    :cond_75
    const/4 v6, -0x1

    #@76
    .line 154
    .local v6, smallestIdx:I
    move v2, p1

    #@77
    .restart local v2       #i:I
    :goto_77
    if-gt v2, p2, :cond_ab

    #@79
    .line 155
    const-string v5, ""

    #@7b
    .line 156
    .local v5, smallest:Ljava/lang/String;
    const/4 v6, -0x1

    #@7c
    .line 157
    const/4 v3, 0x0

    #@7d
    .local v3, j:I
    :goto_7d
    if-ge v3, v4, :cond_a9

    #@7f
    .line 158
    iget-object v9, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@81
    aget-object v9, v9, v3

    #@83
    if-eqz v9, :cond_8f

    #@85
    iget-object v9, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@87
    aget-object v9, v9, v3

    #@89
    invoke-interface {v9}, Landroid/database/Cursor;->isAfterLast()Z

    #@8c
    move-result v9

    #@8d
    if-eqz v9, :cond_92

    #@8f
    .line 157
    :cond_8f
    :goto_8f
    add-int/lit8 v3, v3, 0x1

    #@91
    goto :goto_7d

    #@92
    .line 161
    :cond_92
    iget-object v9, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@94
    aget-object v9, v9, v3

    #@96
    iget-object v10, p0, Lcom/android/internal/database/SortCursor;->mSortColumns:[I

    #@98
    aget v10, v10, v3

    #@9a
    invoke-interface {v9, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@9d
    move-result-object v1

    #@9e
    .line 162
    .local v1, current:Ljava/lang/String;
    if-ltz v6, :cond_a6

    #@a0
    invoke-virtual {v1, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@a3
    move-result v9

    #@a4
    if-gez v9, :cond_8f

    #@a6
    .line 163
    :cond_a6
    move-object v5, v1

    #@a7
    .line 164
    move v6, v3

    #@a8
    goto :goto_8f

    #@a9
    .line 167
    .end local v1           #current:Ljava/lang/String;
    :cond_a9
    if-ne v2, p2, :cond_d3

    #@ab
    .line 172
    .end local v3           #j:I
    .end local v5           #smallest:Ljava/lang/String;
    :cond_ab
    iget-object v9, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@ad
    aget-object v9, v9, v6

    #@af
    iput-object v9, p0, Lcom/android/internal/database/SortCursor;->mCursor:Landroid/database/Cursor;

    #@b1
    .line 173
    iget-object v9, p0, Lcom/android/internal/database/SortCursor;->mRowNumCache:[I

    #@b3
    aput p2, v9, v0

    #@b5
    .line 174
    iget-object v9, p0, Lcom/android/internal/database/SortCursor;->mCursorCache:[I

    #@b7
    aput v6, v9, v0

    #@b9
    .line 175
    const/4 v2, 0x0

    #@ba
    :goto_ba
    if-ge v2, v4, :cond_e3

    #@bc
    .line 176
    iget-object v9, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@be
    aget-object v9, v9, v2

    #@c0
    if-eqz v9, :cond_d0

    #@c2
    .line 177
    iget-object v9, p0, Lcom/android/internal/database/SortCursor;->mCurRowNumCache:[[I

    #@c4
    aget-object v9, v9, v0

    #@c6
    iget-object v10, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@c8
    aget-object v10, v10, v2

    #@ca
    invoke-interface {v10}, Landroid/database/Cursor;->getPosition()I

    #@cd
    move-result v10

    #@ce
    aput v10, v9, v2

    #@d0
    .line 175
    :cond_d0
    add-int/lit8 v2, v2, 0x1

    #@d2
    goto :goto_ba

    #@d3
    .line 168
    .restart local v3       #j:I
    .restart local v5       #smallest:Ljava/lang/String;
    :cond_d3
    iget-object v9, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@d5
    aget-object v9, v9, v6

    #@d7
    if-eqz v9, :cond_e0

    #@d9
    .line 169
    iget-object v9, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@db
    aget-object v9, v9, v6

    #@dd
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    #@e0
    .line 154
    :cond_e0
    add-int/lit8 v2, v2, 0x1

    #@e2
    goto :goto_77

    #@e3
    .line 180
    .end local v3           #j:I
    .end local v5           #smallest:Ljava/lang/String;
    :cond_e3
    iput v12, p0, Lcom/android/internal/database/SortCursor;->mLastCacheHit:I

    #@e5
    goto/16 :goto_4
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .registers 5
    .parameter "observer"

    #@0
    .prologue
    .line 276
    iget-object v2, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@2
    array-length v1, v2

    #@3
    .line 277
    .local v1, length:I
    const/4 v0, 0x0

    #@4
    .local v0, i:I
    :goto_4
    if-ge v0, v1, :cond_16

    #@6
    .line 278
    iget-object v2, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@8
    aget-object v2, v2, v0

    #@a
    if-eqz v2, :cond_13

    #@c
    .line 279
    iget-object v2, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@e
    aget-object v2, v2, v0

    #@10
    invoke-interface {v2, p1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    #@13
    .line 277
    :cond_13
    add-int/lit8 v0, v0, 0x1

    #@15
    goto :goto_4

    #@16
    .line 282
    :cond_16
    return-void
.end method

.method public requery()Z
    .registers 4

    #@0
    .prologue
    .line 297
    iget-object v2, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@2
    array-length v1, v2

    #@3
    .line 298
    .local v1, length:I
    const/4 v0, 0x0

    #@4
    .local v0, i:I
    :goto_4
    if-ge v0, v1, :cond_1b

    #@6
    .line 299
    iget-object v2, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@8
    aget-object v2, v2, v0

    #@a
    if-nez v2, :cond_f

    #@c
    .line 298
    :cond_c
    add-int/lit8 v0, v0, 0x1

    #@e
    goto :goto_4

    #@f
    .line 301
    :cond_f
    iget-object v2, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@11
    aget-object v2, v2, v0

    #@13
    invoke-interface {v2}, Landroid/database/Cursor;->requery()Z

    #@16
    move-result v2

    #@17
    if-nez v2, :cond_c

    #@19
    .line 302
    const/4 v2, 0x0

    #@1a
    .line 306
    :goto_1a
    return v2

    #@1b
    :cond_1b
    const/4 v2, 0x1

    #@1c
    goto :goto_1a
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .registers 5
    .parameter "observer"

    #@0
    .prologue
    .line 286
    iget-object v2, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@2
    array-length v1, v2

    #@3
    .line 287
    .local v1, length:I
    const/4 v0, 0x0

    #@4
    .local v0, i:I
    :goto_4
    if-ge v0, v1, :cond_16

    #@6
    .line 288
    iget-object v2, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@8
    aget-object v2, v2, v0

    #@a
    if-eqz v2, :cond_13

    #@c
    .line 289
    iget-object v2, p0, Lcom/android/internal/database/SortCursor;->mCursors:[Landroid/database/Cursor;

    #@e
    aget-object v2, v2, v0

    #@10
    invoke-interface {v2, p1}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    #@13
    .line 287
    :cond_13
    add-int/lit8 v0, v0, 0x1

    #@15
    goto :goto_4

    #@16
    .line 292
    :cond_16
    return-void
.end method
