.class public abstract Lcom/android/internal/backup/IBackupTransport$Stub;
.super Landroid/os/Binder;
.source "IBackupTransport.java"

# interfaces
.implements Lcom/android/internal/backup/IBackupTransport;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/backup/IBackupTransport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/backup/IBackupTransport$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.backup.IBackupTransport"

.field static final TRANSACTION_clearBackupData:I = 0x7

.field static final TRANSACTION_configurationIntent:I = 0x1

.field static final TRANSACTION_currentDestinationString:I = 0x2

.field static final TRANSACTION_finishBackup:I = 0x8

.field static final TRANSACTION_finishRestore:I = 0xe

.field static final TRANSACTION_getAvailableRestoreSets:I = 0x9

.field static final TRANSACTION_getCurrentRestoreSet:I = 0xa

.field static final TRANSACTION_getRestoreData:I = 0xd

.field static final TRANSACTION_initializeDevice:I = 0x5

.field static final TRANSACTION_nextRestorePackage:I = 0xc

.field static final TRANSACTION_performBackup:I = 0x6

.field static final TRANSACTION_requestBackupTime:I = 0x4

.field static final TRANSACTION_startRestore:I = 0xb

.field static final TRANSACTION_transportDirName:I = 0x3


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 15
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 16
    const-string v0, "com.android.internal.backup.IBackupTransport"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/backup/IBackupTransport$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 17
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/backup/IBackupTransport;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 24
    if-nez p0, :cond_4

    #@2
    .line 25
    const/4 v0, 0x0

    #@3
    .line 31
    :goto_3
    return-object v0

    #@4
    .line 27
    :cond_4
    const-string v1, "com.android.internal.backup.IBackupTransport"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 28
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/backup/IBackupTransport;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 29
    check-cast v0, Lcom/android/internal/backup/IBackupTransport;

    #@12
    goto :goto_3

    #@13
    .line 31
    :cond_13
    new-instance v0, Lcom/android/internal/backup/IBackupTransport$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/backup/IBackupTransport$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 35
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 12
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 39
    sparse-switch p1, :sswitch_data_14a

    #@4
    .line 196
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v5

    #@8
    :goto_8
    return v5

    #@9
    .line 43
    :sswitch_9
    const-string v6, "com.android.internal.backup.IBackupTransport"

    #@b
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 48
    :sswitch_f
    const-string v6, "com.android.internal.backup.IBackupTransport"

    #@11
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 49
    invoke-virtual {p0}, Lcom/android/internal/backup/IBackupTransport$Stub;->configurationIntent()Landroid/content/Intent;

    #@17
    move-result-object v3

    #@18
    .line 50
    .local v3, _result:Landroid/content/Intent;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1b
    .line 51
    if-eqz v3, :cond_24

    #@1d
    .line 52
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@20
    .line 53
    invoke-virtual {v3, p3, v5}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    #@23
    goto :goto_8

    #@24
    .line 56
    :cond_24
    const/4 v6, 0x0

    #@25
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@28
    goto :goto_8

    #@29
    .line 62
    .end local v3           #_result:Landroid/content/Intent;
    :sswitch_29
    const-string v6, "com.android.internal.backup.IBackupTransport"

    #@2b
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2e
    .line 63
    invoke-virtual {p0}, Lcom/android/internal/backup/IBackupTransport$Stub;->currentDestinationString()Ljava/lang/String;

    #@31
    move-result-object v3

    #@32
    .line 64
    .local v3, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@35
    .line 65
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@38
    goto :goto_8

    #@39
    .line 70
    .end local v3           #_result:Ljava/lang/String;
    :sswitch_39
    const-string v6, "com.android.internal.backup.IBackupTransport"

    #@3b
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3e
    .line 71
    invoke-virtual {p0}, Lcom/android/internal/backup/IBackupTransport$Stub;->transportDirName()Ljava/lang/String;

    #@41
    move-result-object v3

    #@42
    .line 72
    .restart local v3       #_result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@45
    .line 73
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@48
    goto :goto_8

    #@49
    .line 78
    .end local v3           #_result:Ljava/lang/String;
    :sswitch_49
    const-string v6, "com.android.internal.backup.IBackupTransport"

    #@4b
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4e
    .line 79
    invoke-virtual {p0}, Lcom/android/internal/backup/IBackupTransport$Stub;->requestBackupTime()J

    #@51
    move-result-wide v3

    #@52
    .line 80
    .local v3, _result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@55
    .line 81
    invoke-virtual {p3, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    #@58
    goto :goto_8

    #@59
    .line 86
    .end local v3           #_result:J
    :sswitch_59
    const-string v6, "com.android.internal.backup.IBackupTransport"

    #@5b
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5e
    .line 87
    invoke-virtual {p0}, Lcom/android/internal/backup/IBackupTransport$Stub;->initializeDevice()I

    #@61
    move-result v3

    #@62
    .line 88
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@65
    .line 89
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@68
    goto :goto_8

    #@69
    .line 94
    .end local v3           #_result:I
    :sswitch_69
    const-string v6, "com.android.internal.backup.IBackupTransport"

    #@6b
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6e
    .line 96
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@71
    move-result v6

    #@72
    if-eqz v6, :cond_96

    #@74
    .line 97
    sget-object v6, Landroid/content/pm/PackageInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@76
    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@79
    move-result-object v0

    #@7a
    check-cast v0, Landroid/content/pm/PackageInfo;

    #@7c
    .line 103
    .local v0, _arg0:Landroid/content/pm/PackageInfo;
    :goto_7c
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@7f
    move-result v6

    #@80
    if-eqz v6, :cond_98

    #@82
    .line 104
    sget-object v6, Landroid/os/ParcelFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    #@84
    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@87
    move-result-object v2

    #@88
    check-cast v2, Landroid/os/ParcelFileDescriptor;

    #@8a
    .line 109
    .local v2, _arg1:Landroid/os/ParcelFileDescriptor;
    :goto_8a
    invoke-virtual {p0, v0, v2}, Lcom/android/internal/backup/IBackupTransport$Stub;->performBackup(Landroid/content/pm/PackageInfo;Landroid/os/ParcelFileDescriptor;)I

    #@8d
    move-result v3

    #@8e
    .line 110
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@91
    .line 111
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@94
    goto/16 :goto_8

    #@96
    .line 100
    .end local v0           #_arg0:Landroid/content/pm/PackageInfo;
    .end local v2           #_arg1:Landroid/os/ParcelFileDescriptor;
    .end local v3           #_result:I
    :cond_96
    const/4 v0, 0x0

    #@97
    .restart local v0       #_arg0:Landroid/content/pm/PackageInfo;
    goto :goto_7c

    #@98
    .line 107
    :cond_98
    const/4 v2, 0x0

    #@99
    .restart local v2       #_arg1:Landroid/os/ParcelFileDescriptor;
    goto :goto_8a

    #@9a
    .line 116
    .end local v0           #_arg0:Landroid/content/pm/PackageInfo;
    .end local v2           #_arg1:Landroid/os/ParcelFileDescriptor;
    :sswitch_9a
    const-string v6, "com.android.internal.backup.IBackupTransport"

    #@9c
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9f
    .line 118
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@a2
    move-result v6

    #@a3
    if-eqz v6, :cond_b9

    #@a5
    .line 119
    sget-object v6, Landroid/content/pm/PackageInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@a7
    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@aa
    move-result-object v0

    #@ab
    check-cast v0, Landroid/content/pm/PackageInfo;

    #@ad
    .line 124
    .restart local v0       #_arg0:Landroid/content/pm/PackageInfo;
    :goto_ad
    invoke-virtual {p0, v0}, Lcom/android/internal/backup/IBackupTransport$Stub;->clearBackupData(Landroid/content/pm/PackageInfo;)I

    #@b0
    move-result v3

    #@b1
    .line 125
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@b4
    .line 126
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@b7
    goto/16 :goto_8

    #@b9
    .line 122
    .end local v0           #_arg0:Landroid/content/pm/PackageInfo;
    .end local v3           #_result:I
    :cond_b9
    const/4 v0, 0x0

    #@ba
    .restart local v0       #_arg0:Landroid/content/pm/PackageInfo;
    goto :goto_ad

    #@bb
    .line 131
    .end local v0           #_arg0:Landroid/content/pm/PackageInfo;
    :sswitch_bb
    const-string v6, "com.android.internal.backup.IBackupTransport"

    #@bd
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c0
    .line 132
    invoke-virtual {p0}, Lcom/android/internal/backup/IBackupTransport$Stub;->finishBackup()I

    #@c3
    move-result v3

    #@c4
    .line 133
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@c7
    .line 134
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@ca
    goto/16 :goto_8

    #@cc
    .line 139
    .end local v3           #_result:I
    :sswitch_cc
    const-string v6, "com.android.internal.backup.IBackupTransport"

    #@ce
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d1
    .line 140
    invoke-virtual {p0}, Lcom/android/internal/backup/IBackupTransport$Stub;->getAvailableRestoreSets()[Landroid/app/backup/RestoreSet;

    #@d4
    move-result-object v3

    #@d5
    .line 141
    .local v3, _result:[Landroid/app/backup/RestoreSet;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@d8
    .line 142
    invoke-virtual {p3, v3, v5}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@db
    goto/16 :goto_8

    #@dd
    .line 147
    .end local v3           #_result:[Landroid/app/backup/RestoreSet;
    :sswitch_dd
    const-string v6, "com.android.internal.backup.IBackupTransport"

    #@df
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e2
    .line 148
    invoke-virtual {p0}, Lcom/android/internal/backup/IBackupTransport$Stub;->getCurrentRestoreSet()J

    #@e5
    move-result-wide v3

    #@e6
    .line 149
    .local v3, _result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@e9
    .line 150
    invoke-virtual {p3, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    #@ec
    goto/16 :goto_8

    #@ee
    .line 155
    .end local v3           #_result:J
    :sswitch_ee
    const-string v6, "com.android.internal.backup.IBackupTransport"

    #@f0
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f3
    .line 157
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    #@f6
    move-result-wide v0

    #@f7
    .line 159
    .local v0, _arg0:J
    sget-object v6, Landroid/content/pm/PackageInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@f9
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    #@fc
    move-result-object v2

    #@fd
    check-cast v2, [Landroid/content/pm/PackageInfo;

    #@ff
    .line 160
    .local v2, _arg1:[Landroid/content/pm/PackageInfo;
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/internal/backup/IBackupTransport$Stub;->startRestore(J[Landroid/content/pm/PackageInfo;)I

    #@102
    move-result v3

    #@103
    .line 161
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@106
    .line 162
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@109
    goto/16 :goto_8

    #@10b
    .line 167
    .end local v0           #_arg0:J
    .end local v2           #_arg1:[Landroid/content/pm/PackageInfo;
    .end local v3           #_result:I
    :sswitch_10b
    const-string v6, "com.android.internal.backup.IBackupTransport"

    #@10d
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@110
    .line 168
    invoke-virtual {p0}, Lcom/android/internal/backup/IBackupTransport$Stub;->nextRestorePackage()Ljava/lang/String;

    #@113
    move-result-object v3

    #@114
    .line 169
    .local v3, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@117
    .line 170
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@11a
    goto/16 :goto_8

    #@11c
    .line 175
    .end local v3           #_result:Ljava/lang/String;
    :sswitch_11c
    const-string v6, "com.android.internal.backup.IBackupTransport"

    #@11e
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@121
    .line 177
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@124
    move-result v6

    #@125
    if-eqz v6, :cond_13b

    #@127
    .line 178
    sget-object v6, Landroid/os/ParcelFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    #@129
    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@12c
    move-result-object v0

    #@12d
    check-cast v0, Landroid/os/ParcelFileDescriptor;

    #@12f
    .line 183
    .local v0, _arg0:Landroid/os/ParcelFileDescriptor;
    :goto_12f
    invoke-virtual {p0, v0}, Lcom/android/internal/backup/IBackupTransport$Stub;->getRestoreData(Landroid/os/ParcelFileDescriptor;)I

    #@132
    move-result v3

    #@133
    .line 184
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@136
    .line 185
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@139
    goto/16 :goto_8

    #@13b
    .line 181
    .end local v0           #_arg0:Landroid/os/ParcelFileDescriptor;
    .end local v3           #_result:I
    :cond_13b
    const/4 v0, 0x0

    #@13c
    .restart local v0       #_arg0:Landroid/os/ParcelFileDescriptor;
    goto :goto_12f

    #@13d
    .line 190
    .end local v0           #_arg0:Landroid/os/ParcelFileDescriptor;
    :sswitch_13d
    const-string v6, "com.android.internal.backup.IBackupTransport"

    #@13f
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@142
    .line 191
    invoke-virtual {p0}, Lcom/android/internal/backup/IBackupTransport$Stub;->finishRestore()V

    #@145
    .line 192
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@148
    goto/16 :goto_8

    #@14a
    .line 39
    :sswitch_data_14a
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_29
        0x3 -> :sswitch_39
        0x4 -> :sswitch_49
        0x5 -> :sswitch_59
        0x6 -> :sswitch_69
        0x7 -> :sswitch_9a
        0x8 -> :sswitch_bb
        0x9 -> :sswitch_cc
        0xa -> :sswitch_dd
        0xb -> :sswitch_ee
        0xc -> :sswitch_10b
        0xd -> :sswitch_11c
        0xe -> :sswitch_13d
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
