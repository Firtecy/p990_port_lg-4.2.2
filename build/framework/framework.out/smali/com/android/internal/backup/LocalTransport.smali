.class public Lcom/android/internal/backup/LocalTransport;
.super Lcom/android/internal/backup/IBackupTransport$Stub;
.source "LocalTransport.java"


# static fields
.field private static final DEBUG:Z = true

.field private static final RESTORE_TOKEN:J = 0x1L

.field private static final TAG:Ljava/lang/String; = "LocalTransport"

.field private static final TRANSPORT_DESTINATION_STRING:Ljava/lang/String; = "Backing up to debug-only private cache"

.field private static final TRANSPORT_DIR_NAME:Ljava/lang/String; = "com.android.internal.backup.LocalTransport"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDataDir:Ljava/io/File;

.field private mRestorePackage:I

.field private mRestorePackages:[Landroid/content/pm/PackageInfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 65
    invoke-direct {p0}, Lcom/android/internal/backup/IBackupTransport$Stub;-><init>()V

    #@3
    .line 60
    new-instance v0, Ljava/io/File;

    #@5
    invoke-static {}, Landroid/os/Environment;->getDownloadCacheDirectory()Ljava/io/File;

    #@8
    move-result-object v1

    #@9
    const-string v2, "backup"

    #@b
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@e
    iput-object v0, p0, Lcom/android/internal/backup/LocalTransport;->mDataDir:Ljava/io/File;

    #@10
    .line 61
    const/4 v0, 0x0

    #@11
    iput-object v0, p0, Lcom/android/internal/backup/LocalTransport;->mRestorePackages:[Landroid/content/pm/PackageInfo;

    #@13
    .line 62
    const/4 v0, -0x1

    #@14
    iput v0, p0, Lcom/android/internal/backup/LocalTransport;->mRestorePackage:I

    #@16
    .line 66
    iput-object p1, p0, Lcom/android/internal/backup/LocalTransport;->mContext:Landroid/content/Context;

    #@18
    .line 67
    return-void
.end method

.method private deleteContents(Ljava/io/File;)V
    .registers 8
    .parameter "dirname"

    #@0
    .prologue
    .line 152
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    #@3
    move-result-object v1

    #@4
    .line 153
    .local v1, contents:[Ljava/io/File;
    if-eqz v1, :cond_1c

    #@6
    .line 154
    move-object v0, v1

    #@7
    .local v0, arr$:[Ljava/io/File;
    array-length v4, v0

    #@8
    .local v4, len$:I
    const/4 v3, 0x0

    #@9
    .local v3, i$:I
    :goto_9
    if-ge v3, v4, :cond_1c

    #@b
    aget-object v2, v0, v3

    #@d
    .line 155
    .local v2, f:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    #@10
    move-result v5

    #@11
    if-eqz v5, :cond_16

    #@13
    .line 158
    invoke-direct {p0, v2}, Lcom/android/internal/backup/LocalTransport;->deleteContents(Ljava/io/File;)V

    #@16
    .line 160
    :cond_16
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    #@19
    .line 154
    add-int/lit8 v3, v3, 0x1

    #@1b
    goto :goto_9

    #@1c
    .line 163
    .end local v0           #arr$:[Ljava/io/File;
    .end local v2           #f:Ljava/io/File;
    .end local v3           #i$:I
    .end local v4           #len$:I
    :cond_1c
    return-void
.end method


# virtual methods
.method public clearBackupData(Landroid/content/pm/PackageInfo;)I
    .registers 11
    .parameter "packageInfo"

    #@0
    .prologue
    .line 166
    const-string v6, "LocalTransport"

    #@2
    new-instance v7, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v8, "clearBackupData() pkg="

    #@9
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v7

    #@d
    iget-object v8, p1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v7

    #@13
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v7

    #@17
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 168
    new-instance v5, Ljava/io/File;

    #@1c
    iget-object v6, p0, Lcom/android/internal/backup/LocalTransport;->mDataDir:Ljava/io/File;

    #@1e
    iget-object v7, p1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@20
    invoke-direct {v5, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@23
    .line 169
    .local v5, packageDir:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->listFiles()[Ljava/io/File;

    #@26
    move-result-object v2

    #@27
    .line 170
    .local v2, fileset:[Ljava/io/File;
    if-eqz v2, :cond_39

    #@29
    .line 171
    move-object v0, v2

    #@2a
    .local v0, arr$:[Ljava/io/File;
    array-length v4, v0

    #@2b
    .local v4, len$:I
    const/4 v3, 0x0

    #@2c
    .local v3, i$:I
    :goto_2c
    if-ge v3, v4, :cond_36

    #@2e
    aget-object v1, v0, v3

    #@30
    .line 172
    .local v1, f:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    #@33
    .line 171
    add-int/lit8 v3, v3, 0x1

    #@35
    goto :goto_2c

    #@36
    .line 174
    .end local v1           #f:Ljava/io/File;
    :cond_36
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    #@39
    .line 176
    .end local v0           #arr$:[Ljava/io/File;
    .end local v3           #i$:I
    .end local v4           #len$:I
    :cond_39
    const/4 v6, 0x0

    #@3a
    return v6
.end method

.method public configurationIntent()Landroid/content/Intent;
    .registers 2

    #@0
    .prologue
    .line 71
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public currentDestinationString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 75
    const-string v0, "Backing up to debug-only private cache"

    #@2
    return-object v0
.end method

.method public finishBackup()I
    .registers 3

    #@0
    .prologue
    .line 180
    const-string v0, "LocalTransport"

    #@2
    const-string v1, "finishBackup()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 181
    const/4 v0, 0x0

    #@8
    return v0
.end method

.method public finishRestore()V
    .registers 3

    #@0
    .prologue
    .line 257
    const-string v0, "LocalTransport"

    #@2
    const-string v1, "finishRestore()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 258
    return-void
.end method

.method public getAvailableRestoreSets()[Landroid/app/backup/RestoreSet;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 187
    new-instance v1, Landroid/app/backup/RestoreSet;

    #@2
    const-string v2, "Local disk image"

    #@4
    const-string v3, "flash"

    #@6
    const-wide/16 v4, 0x1

    #@8
    invoke-direct {v1, v2, v3, v4, v5}, Landroid/app/backup/RestoreSet;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    #@b
    .line 188
    .local v1, set:Landroid/app/backup/RestoreSet;
    const/4 v2, 0x1

    #@c
    new-array v0, v2, [Landroid/app/backup/RestoreSet;

    #@e
    const/4 v2, 0x0

    #@f
    aput-object v1, v0, v2

    #@11
    .line 189
    .local v0, array:[Landroid/app/backup/RestoreSet;
    return-object v0
.end method

.method public getCurrentRestoreSet()J
    .registers 3

    #@0
    .prologue
    .line 194
    const-wide/16 v0, 0x1

    #@2
    return-wide v0
.end method

.method public getRestoreData(Landroid/os/ParcelFileDescriptor;)I
    .registers 17
    .parameter "outFd"

    #@0
    .prologue
    .line 219
    iget-object v12, p0, Lcom/android/internal/backup/LocalTransport;->mRestorePackages:[Landroid/content/pm/PackageInfo;

    #@2
    if-nez v12, :cond_d

    #@4
    new-instance v12, Ljava/lang/IllegalStateException;

    #@6
    const-string/jumbo v13, "startRestore not called"

    #@9
    invoke-direct {v12, v13}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v12

    #@d
    .line 220
    :cond_d
    iget v12, p0, Lcom/android/internal/backup/LocalTransport;->mRestorePackage:I

    #@f
    if-gez v12, :cond_1a

    #@11
    new-instance v12, Ljava/lang/IllegalStateException;

    #@13
    const-string/jumbo v13, "nextRestorePackage not called"

    #@16
    invoke-direct {v12, v13}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@19
    throw v12

    #@1a
    .line 221
    :cond_1a
    new-instance v10, Ljava/io/File;

    #@1c
    iget-object v12, p0, Lcom/android/internal/backup/LocalTransport;->mDataDir:Ljava/io/File;

    #@1e
    iget-object v13, p0, Lcom/android/internal/backup/LocalTransport;->mRestorePackages:[Landroid/content/pm/PackageInfo;

    #@20
    iget v14, p0, Lcom/android/internal/backup/LocalTransport;->mRestorePackage:I

    #@22
    aget-object v13, v13, v14

    #@24
    iget-object v13, v13, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@26
    invoke-direct {v10, v12, v13}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@29
    .line 225
    .local v10, packageDir:Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->listFiles()[Ljava/io/File;

    #@2c
    move-result-object v1

    #@2d
    .line 226
    .local v1, blobs:[Ljava/io/File;
    if-nez v1, :cond_49

    #@2f
    .line 227
    const-string v12, "LocalTransport"

    #@31
    new-instance v13, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v14, "Error listing directory: "

    #@38
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v13

    #@3c
    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v13

    #@40
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v13

    #@44
    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 228
    const/4 v12, 0x1

    #@48
    .line 252
    :goto_48
    return v12

    #@49
    .line 232
    :cond_49
    const-string v12, "LocalTransport"

    #@4b
    new-instance v13, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v14, "  getRestoreData() found "

    #@52
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v13

    #@56
    array-length v14, v1

    #@57
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v13

    #@5b
    const-string v14, " key files"

    #@5d
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v13

    #@61
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v13

    #@65
    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@68
    .line 233
    new-instance v9, Landroid/app/backup/BackupDataOutput;

    #@6a
    invoke-virtual/range {p1 .. p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@6d
    move-result-object v12

    #@6e
    invoke-direct {v9, v12}, Landroid/app/backup/BackupDataOutput;-><init>(Ljava/io/FileDescriptor;)V

    #@71
    .line 235
    .local v9, out:Landroid/app/backup/BackupDataOutput;
    move-object v0, v1

    #@72
    .local v0, arr$:[Ljava/io/File;
    :try_start_72
    array-length v8, v0

    #@73
    .local v8, len$:I
    const/4 v5, 0x0

    #@74
    .local v5, i$:I
    :goto_74
    if-ge v5, v8, :cond_d2

    #@76
    aget-object v4, v0, v5

    #@78
    .line 236
    .local v4, f:Ljava/io/File;
    new-instance v6, Ljava/io/FileInputStream;

    #@7a
    invoke-direct {v6, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_7d
    .catch Ljava/io/IOException; {:try_start_72 .. :try_end_7d} :catch_c7

    #@7d
    .line 238
    .local v6, in:Ljava/io/FileInputStream;
    :try_start_7d
    invoke-virtual {v4}, Ljava/io/File;->length()J

    #@80
    move-result-wide v12

    #@81
    long-to-int v11, v12

    #@82
    .line 239
    .local v11, size:I
    new-array v2, v11, [B

    #@84
    .line 240
    .local v2, buf:[B
    invoke-virtual {v6, v2}, Ljava/io/FileInputStream;->read([B)I

    #@87
    .line 241
    new-instance v7, Ljava/lang/String;

    #@89
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    #@8c
    move-result-object v12

    #@8d
    invoke-static {v12}, Lcom/android/org/bouncycastle/util/encoders/Base64;->decode(Ljava/lang/String;)[B

    #@90
    move-result-object v12

    #@91
    invoke-direct {v7, v12}, Ljava/lang/String;-><init>([B)V

    #@94
    .line 242
    .local v7, key:Ljava/lang/String;
    const-string v12, "LocalTransport"

    #@96
    new-instance v13, Ljava/lang/StringBuilder;

    #@98
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@9b
    const-string v14, "    ... key="

    #@9d
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v13

    #@a1
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v13

    #@a5
    const-string v14, " size="

    #@a7
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v13

    #@ab
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v13

    #@af
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b2
    move-result-object v13

    #@b3
    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b6
    .line 243
    invoke-virtual {v9, v7, v11}, Landroid/app/backup/BackupDataOutput;->writeEntityHeader(Ljava/lang/String;I)I

    #@b9
    .line 244
    invoke-virtual {v9, v2, v11}, Landroid/app/backup/BackupDataOutput;->writeEntityData([BI)I
    :try_end_bc
    .catchall {:try_start_7d .. :try_end_bc} :catchall_c2

    #@bc
    .line 246
    :try_start_bc
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    #@bf
    .line 235
    add-int/lit8 v5, v5, 0x1

    #@c1
    goto :goto_74

    #@c2
    .line 246
    .end local v2           #buf:[B
    .end local v7           #key:Ljava/lang/String;
    .end local v11           #size:I
    :catchall_c2
    move-exception v12

    #@c3
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    #@c6
    throw v12
    :try_end_c7
    .catch Ljava/io/IOException; {:try_start_bc .. :try_end_c7} :catch_c7

    #@c7
    .line 250
    .end local v4           #f:Ljava/io/File;
    .end local v5           #i$:I
    .end local v6           #in:Ljava/io/FileInputStream;
    .end local v8           #len$:I
    :catch_c7
    move-exception v3

    #@c8
    .line 251
    .local v3, e:Ljava/io/IOException;
    const-string v12, "LocalTransport"

    #@ca
    const-string v13, "Unable to read backup records"

    #@cc
    invoke-static {v12, v13, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@cf
    .line 252
    const/4 v12, 0x1

    #@d0
    goto/16 :goto_48

    #@d2
    .line 249
    .end local v3           #e:Ljava/io/IOException;
    .restart local v5       #i$:I
    .restart local v8       #len$:I
    :cond_d2
    const/4 v12, 0x0

    #@d3
    goto/16 :goto_48
.end method

.method public initializeDevice()I
    .registers 3

    #@0
    .prologue
    .line 88
    const-string v0, "LocalTransport"

    #@2
    const-string/jumbo v1, "wiping all data"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 89
    iget-object v0, p0, Lcom/android/internal/backup/LocalTransport;->mDataDir:Ljava/io/File;

    #@a
    invoke-direct {p0, v0}, Lcom/android/internal/backup/LocalTransport;->deleteContents(Ljava/io/File;)V

    #@d
    .line 90
    const/4 v0, 0x0

    #@e
    return v0
.end method

.method public nextRestorePackage()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 205
    iget-object v1, p0, Lcom/android/internal/backup/LocalTransport;->mRestorePackages:[Landroid/content/pm/PackageInfo;

    #@2
    if-nez v1, :cond_d

    #@4
    new-instance v1, Ljava/lang/IllegalStateException;

    #@6
    const-string/jumbo v2, "startRestore not called"

    #@9
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v1

    #@d
    .line 206
    :cond_d
    iget v1, p0, Lcom/android/internal/backup/LocalTransport;->mRestorePackage:I

    #@f
    add-int/lit8 v1, v1, 0x1

    #@11
    iput v1, p0, Lcom/android/internal/backup/LocalTransport;->mRestorePackage:I

    #@13
    iget-object v2, p0, Lcom/android/internal/backup/LocalTransport;->mRestorePackages:[Landroid/content/pm/PackageInfo;

    #@15
    array-length v2, v2

    #@16
    if-ge v1, v2, :cond_46

    #@18
    .line 207
    iget-object v1, p0, Lcom/android/internal/backup/LocalTransport;->mRestorePackages:[Landroid/content/pm/PackageInfo;

    #@1a
    iget v2, p0, Lcom/android/internal/backup/LocalTransport;->mRestorePackage:I

    #@1c
    aget-object v1, v1, v2

    #@1e
    iget-object v0, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@20
    .line 208
    .local v0, name:Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    #@22
    iget-object v2, p0, Lcom/android/internal/backup/LocalTransport;->mDataDir:Ljava/io/File;

    #@24
    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@27
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    #@2a
    move-result v1

    #@2b
    if-eqz v1, :cond_d

    #@2d
    .line 209
    const-string v1, "LocalTransport"

    #@2f
    new-instance v2, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v3, "  nextRestorePackage() = "

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 215
    .end local v0           #name:Ljava/lang/String;
    :goto_45
    return-object v0

    #@46
    .line 214
    :cond_46
    const-string v1, "LocalTransport"

    #@48
    const-string v2, "  no more packages to restore"

    #@4a
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    .line 215
    const-string v0, ""

    #@4f
    goto :goto_45
.end method

.method public performBackup(Landroid/content/pm/PackageInfo;Landroid/os/ParcelFileDescriptor;)I
    .registers 16
    .parameter "packageInfo"
    .parameter "data"

    #@0
    .prologue
    .line 94
    const-string v10, "LocalTransport"

    #@2
    new-instance v11, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v12, "performBackup() pkg="

    #@a
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v11

    #@e
    iget-object v12, p1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@10
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v11

    #@14
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v11

    #@18
    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 96
    new-instance v9, Ljava/io/File;

    #@1d
    iget-object v10, p0, Lcom/android/internal/backup/LocalTransport;->mDataDir:Ljava/io/File;

    #@1f
    iget-object v11, p1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@21
    invoke-direct {v9, v10, v11}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@24
    .line 97
    .local v9, packageDir:Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->mkdirs()Z

    #@27
    .line 103
    new-instance v3, Landroid/app/backup/BackupDataInput;

    #@29
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@2c
    move-result-object v10

    #@2d
    invoke-direct {v3, v10}, Landroid/app/backup/BackupDataInput;-><init>(Ljava/io/FileDescriptor;)V

    #@30
    .line 105
    .local v3, changeSet:Landroid/app/backup/BackupDataInput;
    const/16 v2, 0x200

    #@32
    .line 106
    .local v2, bufSize:I
    :try_start_32
    new-array v1, v2, [B

    #@34
    .line 107
    .local v1, buf:[B
    :goto_34
    invoke-virtual {v3}, Landroid/app/backup/BackupDataInput;->readNextHeader()Z

    #@37
    move-result v10

    #@38
    if-eqz v10, :cond_f0

    #@3a
    .line 108
    invoke-virtual {v3}, Landroid/app/backup/BackupDataInput;->getKey()Ljava/lang/String;

    #@3d
    move-result-object v8

    #@3e
    .line 109
    .local v8, key:Ljava/lang/String;
    new-instance v0, Ljava/lang/String;

    #@40
    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    #@43
    move-result-object v10

    #@44
    invoke-static {v10}, Lcom/android/org/bouncycastle/util/encoders/Base64;->encode([B)[B

    #@47
    move-result-object v10

    #@48
    invoke-direct {v0, v10}, Ljava/lang/String;-><init>([B)V

    #@4b
    .line 110
    .local v0, base64Key:Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    #@4d
    invoke-direct {v7, v9, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@50
    .line 112
    .local v7, entityFile:Ljava/io/File;
    invoke-virtual {v3}, Landroid/app/backup/BackupDataInput;->getDataSize()I

    #@53
    move-result v4

    #@54
    .line 114
    .local v4, dataSize:I
    const-string v10, "LocalTransport"

    #@56
    new-instance v11, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string v12, "Got change set key="

    #@5d
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v11

    #@61
    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v11

    #@65
    const-string v12, " size="

    #@67
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v11

    #@6b
    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v11

    #@6f
    const-string v12, " key64="

    #@71
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v11

    #@75
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v11

    #@79
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v11

    #@7d
    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@80
    .line 117
    if-ltz v4, :cond_eb

    #@82
    .line 118
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    #@85
    move-result v10

    #@86
    if-eqz v10, :cond_8b

    #@88
    .line 119
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    #@8b
    .line 121
    :cond_8b
    new-instance v6, Ljava/io/FileOutputStream;

    #@8d
    invoke-direct {v6, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    #@90
    .line 123
    .local v6, entity:Ljava/io/FileOutputStream;
    if-le v4, v2, :cond_95

    #@92
    .line 124
    move v2, v4

    #@93
    .line 125
    new-array v1, v2, [B

    #@95
    .line 127
    :cond_95
    const/4 v10, 0x0

    #@96
    invoke-virtual {v3, v1, v10, v4}, Landroid/app/backup/BackupDataInput;->readEntityData([BII)I

    #@99
    .line 128
    const-string v10, "LocalTransport"

    #@9b
    new-instance v11, Ljava/lang/StringBuilder;

    #@9d
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@a0
    const-string v12, "  data size "

    #@a2
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v11

    #@a6
    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v11

    #@aa
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ad
    move-result-object v11

    #@ae
    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b1
    .catch Ljava/io/IOException; {:try_start_32 .. :try_end_b1} :catch_ba

    #@b1
    .line 131
    const/4 v10, 0x0

    #@b2
    :try_start_b2
    invoke-virtual {v6, v1, v10, v4}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_b5
    .catchall {:try_start_b2 .. :try_end_b5} :catchall_e6
    .catch Ljava/io/IOException; {:try_start_b2 .. :try_end_b5} :catch_c4

    #@b5
    .line 136
    :try_start_b5
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_b8
    .catch Ljava/io/IOException; {:try_start_b5 .. :try_end_b8} :catch_ba

    #@b8
    goto/16 :goto_34

    #@ba
    .line 143
    .end local v0           #base64Key:Ljava/lang/String;
    .end local v1           #buf:[B
    .end local v4           #dataSize:I
    .end local v6           #entity:Ljava/io/FileOutputStream;
    .end local v7           #entityFile:Ljava/io/File;
    .end local v8           #key:Ljava/lang/String;
    :catch_ba
    move-exception v5

    #@bb
    .line 145
    .local v5, e:Ljava/io/IOException;
    const-string v10, "LocalTransport"

    #@bd
    const-string v11, "Exception reading backup input:"

    #@bf
    invoke-static {v10, v11, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@c2
    .line 146
    const/4 v10, 0x1

    #@c3
    .end local v5           #e:Ljava/io/IOException;
    :goto_c3
    return v10

    #@c4
    .line 132
    .restart local v0       #base64Key:Ljava/lang/String;
    .restart local v1       #buf:[B
    .restart local v4       #dataSize:I
    .restart local v6       #entity:Ljava/io/FileOutputStream;
    .restart local v7       #entityFile:Ljava/io/File;
    .restart local v8       #key:Ljava/lang/String;
    :catch_c4
    move-exception v5

    #@c5
    .line 133
    .restart local v5       #e:Ljava/io/IOException;
    :try_start_c5
    const-string v10, "LocalTransport"

    #@c7
    new-instance v11, Ljava/lang/StringBuilder;

    #@c9
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@cc
    const-string v12, "Unable to update key file "

    #@ce
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v11

    #@d2
    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@d5
    move-result-object v12

    #@d6
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v11

    #@da
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dd
    move-result-object v11

    #@de
    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e1
    .catchall {:try_start_c5 .. :try_end_e1} :catchall_e6

    #@e1
    .line 134
    const/4 v10, 0x1

    #@e2
    .line 136
    :try_start_e2
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V

    #@e5
    goto :goto_c3

    #@e6
    .end local v5           #e:Ljava/io/IOException;
    :catchall_e6
    move-exception v10

    #@e7
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V

    #@ea
    throw v10

    #@eb
    .line 139
    .end local v6           #entity:Ljava/io/FileOutputStream;
    :cond_eb
    invoke-virtual {v7}, Ljava/io/File;->delete()Z
    :try_end_ee
    .catch Ljava/io/IOException; {:try_start_e2 .. :try_end_ee} :catch_ba

    #@ee
    goto/16 :goto_34

    #@f0
    .line 142
    .end local v0           #base64Key:Ljava/lang/String;
    .end local v4           #dataSize:I
    .end local v7           #entityFile:Ljava/io/File;
    .end local v8           #key:Ljava/lang/String;
    :cond_f0
    const/4 v10, 0x0

    #@f1
    goto :goto_c3
.end method

.method public requestBackupTime()J
    .registers 3

    #@0
    .prologue
    .line 84
    const-wide/16 v0, 0x0

    #@2
    return-wide v0
.end method

.method public startRestore(J[Landroid/content/pm/PackageInfo;)I
    .registers 7
    .parameter "token"
    .parameter "packages"

    #@0
    .prologue
    .line 198
    const-string v0, "LocalTransport"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v2, "start restore "

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 199
    iput-object p3, p0, Lcom/android/internal/backup/LocalTransport;->mRestorePackages:[Landroid/content/pm/PackageInfo;

    #@1b
    .line 200
    const/4 v0, -0x1

    #@1c
    iput v0, p0, Lcom/android/internal/backup/LocalTransport;->mRestorePackage:I

    #@1e
    .line 201
    const/4 v0, 0x0

    #@1f
    return v0
.end method

.method public transportDirName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 79
    const-string v0, "com.android.internal.backup.LocalTransport"

    #@2
    return-object v0
.end method
