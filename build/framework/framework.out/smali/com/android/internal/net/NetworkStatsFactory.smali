.class public Lcom/android/internal/net/NetworkStatsFactory;
.super Ljava/lang/Object;
.source "NetworkStatsFactory.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "NetworkStatsFactory"


# instance fields
.field private final mStatsXtIfaceAll:Ljava/io/File;

.field private final mStatsXtIfaceFmt:Ljava/io/File;

.field private final mStatsXtUid:Ljava/io/File;


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 58
    new-instance v0, Ljava/io/File;

    #@2
    const-string v1, "/proc/"

    #@4
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@7
    invoke-direct {p0, v0}, Lcom/android/internal/net/NetworkStatsFactory;-><init>(Ljava/io/File;)V

    #@a
    .line 59
    return-void
.end method

.method public constructor <init>(Ljava/io/File;)V
    .registers 4
    .parameter "procRoot"

    #@0
    .prologue
    .line 62
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 63
    new-instance v0, Ljava/io/File;

    #@5
    const-string/jumbo v1, "net/xt_qtaguid/iface_stat_all"

    #@8
    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@b
    iput-object v0, p0, Lcom/android/internal/net/NetworkStatsFactory;->mStatsXtIfaceAll:Ljava/io/File;

    #@d
    .line 64
    new-instance v0, Ljava/io/File;

    #@f
    const-string/jumbo v1, "net/xt_qtaguid/iface_stat_fmt"

    #@12
    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@15
    iput-object v0, p0, Lcom/android/internal/net/NetworkStatsFactory;->mStatsXtIfaceFmt:Ljava/io/File;

    #@17
    .line 65
    new-instance v0, Ljava/io/File;

    #@19
    const-string/jumbo v1, "net/xt_qtaguid/stats"

    #@1c
    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@1f
    iput-object v0, p0, Lcom/android/internal/net/NetworkStatsFactory;->mStatsXtUid:Ljava/io/File;

    #@21
    .line 66
    return-void
.end method


# virtual methods
.method public readNetworkStatsDetail()Landroid/net/NetworkStats;
    .registers 2

    #@0
    .prologue
    .line 174
    const/4 v0, -0x1

    #@1
    invoke-virtual {p0, v0}, Lcom/android/internal/net/NetworkStatsFactory;->readNetworkStatsDetail(I)Landroid/net/NetworkStats;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public readNetworkStatsDetail(I)Landroid/net/NetworkStats;
    .registers 13
    .parameter "limitUid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 184
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    #@3
    move-result-object v6

    #@4
    .line 186
    .local v6, savedPolicy:Landroid/os/StrictMode$ThreadPolicy;
    new-instance v7, Landroid/net/NetworkStats;

    #@6
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@9
    move-result-wide v8

    #@a
    const/16 v10, 0x18

    #@c
    invoke-direct {v7, v8, v9, v10}, Landroid/net/NetworkStats;-><init>(JI)V

    #@f
    .line 187
    .local v7, stats:Landroid/net/NetworkStats;
    new-instance v1, Landroid/net/NetworkStats$Entry;

    #@11
    invoke-direct {v1}, Landroid/net/NetworkStats$Entry;-><init>()V

    #@14
    .line 189
    .local v1, entry:Landroid/net/NetworkStats$Entry;
    const/4 v2, 0x1

    #@15
    .line 190
    .local v2, idx:I
    const/4 v3, 0x1

    #@16
    .line 192
    .local v3, lastIdx:I
    const/4 v4, 0x0

    #@17
    .line 195
    .local v4, reader:Lcom/android/internal/util/ProcFileReader;
    :try_start_17
    new-instance v5, Lcom/android/internal/util/ProcFileReader;

    #@19
    new-instance v8, Ljava/io/FileInputStream;

    #@1b
    iget-object v9, p0, Lcom/android/internal/net/NetworkStatsFactory;->mStatsXtUid:Ljava/io/File;

    #@1d
    invoke-direct {v8, v9}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    #@20
    invoke-direct {v5, v8}, Lcom/android/internal/util/ProcFileReader;-><init>(Ljava/io/InputStream;)V
    :try_end_23
    .catchall {:try_start_17 .. :try_end_23} :catchall_73
    .catch Ljava/lang/NullPointerException; {:try_start_17 .. :try_end_23} :catch_106
    .catch Ljava/lang/NumberFormatException; {:try_start_17 .. :try_end_23} :catch_104
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_23} :catch_e2

    #@23
    .line 196
    .end local v4           #reader:Lcom/android/internal/util/ProcFileReader;
    .local v5, reader:Lcom/android/internal/util/ProcFileReader;
    :try_start_23
    invoke-virtual {v5}, Lcom/android/internal/util/ProcFileReader;->finishLine()V

    #@26
    .line 198
    :goto_26
    invoke-virtual {v5}, Lcom/android/internal/util/ProcFileReader;->hasMoreData()Z

    #@29
    move-result v8

    #@2a
    if-eqz v8, :cond_db

    #@2c
    .line 199
    invoke-virtual {v5}, Lcom/android/internal/util/ProcFileReader;->nextInt()I

    #@2f
    move-result v2

    #@30
    .line 200
    add-int/lit8 v8, v3, 0x1

    #@32
    if-eq v2, v8, :cond_7b

    #@34
    .line 201
    new-instance v8, Ljava/lang/IllegalStateException;

    #@36
    new-instance v9, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v10, "inconsistent idx="

    #@3d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v9

    #@41
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@44
    move-result-object v9

    #@45
    const-string v10, " after lastIdx="

    #@47
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v9

    #@4b
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v9

    #@4f
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v9

    #@53
    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@56
    throw v8
    :try_end_57
    .catchall {:try_start_23 .. :try_end_57} :catchall_fd
    .catch Ljava/lang/NullPointerException; {:try_start_23 .. :try_end_57} :catch_57
    .catch Ljava/lang/NumberFormatException; {:try_start_23 .. :try_end_57} :catch_bf
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_57} :catch_101

    #@57
    .line 221
    :catch_57
    move-exception v0

    #@58
    move-object v4, v5

    #@59
    .line 222
    .end local v5           #reader:Lcom/android/internal/util/ProcFileReader;
    .local v0, e:Ljava/lang/NullPointerException;
    .restart local v4       #reader:Lcom/android/internal/util/ProcFileReader;
    :goto_59
    :try_start_59
    new-instance v8, Ljava/lang/IllegalStateException;

    #@5b
    new-instance v9, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    const-string/jumbo v10, "problem parsing idx "

    #@63
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v9

    #@67
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v9

    #@6b
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v9

    #@6f
    invoke-direct {v8, v9, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@72
    throw v8
    :try_end_73
    .catchall {:try_start_59 .. :try_end_73} :catchall_73

    #@73
    .line 228
    .end local v0           #e:Ljava/lang/NullPointerException;
    :catchall_73
    move-exception v8

    #@74
    :goto_74
    invoke-static {v4}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@77
    .line 229
    invoke-static {v6}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@7a
    throw v8

    #@7b
    .line 204
    .end local v4           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v5       #reader:Lcom/android/internal/util/ProcFileReader;
    :cond_7b
    move v3, v2

    #@7c
    .line 206
    :try_start_7c
    invoke-virtual {v5}, Lcom/android/internal/util/ProcFileReader;->nextString()Ljava/lang/String;

    #@7f
    move-result-object v8

    #@80
    iput-object v8, v1, Landroid/net/NetworkStats$Entry;->iface:Ljava/lang/String;

    #@82
    .line 207
    invoke-virtual {v5}, Lcom/android/internal/util/ProcFileReader;->nextString()Ljava/lang/String;

    #@85
    move-result-object v8

    #@86
    invoke-static {v8}, Lcom/android/server/NetworkManagementSocketTagger;->kernelToTag(Ljava/lang/String;)I

    #@89
    move-result v8

    #@8a
    iput v8, v1, Landroid/net/NetworkStats$Entry;->tag:I

    #@8c
    .line 208
    invoke-virtual {v5}, Lcom/android/internal/util/ProcFileReader;->nextInt()I

    #@8f
    move-result v8

    #@90
    iput v8, v1, Landroid/net/NetworkStats$Entry;->uid:I

    #@92
    .line 209
    invoke-virtual {v5}, Lcom/android/internal/util/ProcFileReader;->nextInt()I

    #@95
    move-result v8

    #@96
    iput v8, v1, Landroid/net/NetworkStats$Entry;->set:I

    #@98
    .line 210
    invoke-virtual {v5}, Lcom/android/internal/util/ProcFileReader;->nextLong()J

    #@9b
    move-result-wide v8

    #@9c
    iput-wide v8, v1, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@9e
    .line 211
    invoke-virtual {v5}, Lcom/android/internal/util/ProcFileReader;->nextLong()J

    #@a1
    move-result-wide v8

    #@a2
    iput-wide v8, v1, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@a4
    .line 212
    invoke-virtual {v5}, Lcom/android/internal/util/ProcFileReader;->nextLong()J

    #@a7
    move-result-wide v8

    #@a8
    iput-wide v8, v1, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@aa
    .line 213
    invoke-virtual {v5}, Lcom/android/internal/util/ProcFileReader;->nextLong()J

    #@ad
    move-result-wide v8

    #@ae
    iput-wide v8, v1, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@b0
    .line 215
    const/4 v8, -0x1

    #@b1
    if-eq p1, v8, :cond_b7

    #@b3
    iget v8, v1, Landroid/net/NetworkStats$Entry;->uid:I

    #@b5
    if-ne p1, v8, :cond_ba

    #@b7
    .line 216
    :cond_b7
    invoke-virtual {v7, v1}, Landroid/net/NetworkStats;->addValues(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats;

    #@ba
    .line 219
    :cond_ba
    invoke-virtual {v5}, Lcom/android/internal/util/ProcFileReader;->finishLine()V
    :try_end_bd
    .catchall {:try_start_7c .. :try_end_bd} :catchall_fd
    .catch Ljava/lang/NullPointerException; {:try_start_7c .. :try_end_bd} :catch_57
    .catch Ljava/lang/NumberFormatException; {:try_start_7c .. :try_end_bd} :catch_bf
    .catch Ljava/io/IOException; {:try_start_7c .. :try_end_bd} :catch_101

    #@bd
    goto/16 :goto_26

    #@bf
    .line 223
    :catch_bf
    move-exception v0

    #@c0
    move-object v4, v5

    #@c1
    .line 224
    .end local v5           #reader:Lcom/android/internal/util/ProcFileReader;
    .local v0, e:Ljava/lang/NumberFormatException;
    .restart local v4       #reader:Lcom/android/internal/util/ProcFileReader;
    :goto_c1
    :try_start_c1
    new-instance v8, Ljava/lang/IllegalStateException;

    #@c3
    new-instance v9, Ljava/lang/StringBuilder;

    #@c5
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@c8
    const-string/jumbo v10, "problem parsing idx "

    #@cb
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v9

    #@cf
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v9

    #@d3
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d6
    move-result-object v9

    #@d7
    invoke-direct {v8, v9, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@da
    throw v8
    :try_end_db
    .catchall {:try_start_c1 .. :try_end_db} :catchall_73

    #@db
    .line 228
    .end local v0           #e:Ljava/lang/NumberFormatException;
    .end local v4           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v5       #reader:Lcom/android/internal/util/ProcFileReader;
    :cond_db
    invoke-static {v5}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@de
    .line 229
    invoke-static {v6}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@e1
    .line 232
    return-object v7

    #@e2
    .line 225
    .end local v5           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v4       #reader:Lcom/android/internal/util/ProcFileReader;
    :catch_e2
    move-exception v0

    #@e3
    .line 226
    .local v0, e:Ljava/io/IOException;
    :goto_e3
    :try_start_e3
    new-instance v8, Ljava/lang/IllegalStateException;

    #@e5
    new-instance v9, Ljava/lang/StringBuilder;

    #@e7
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@ea
    const-string/jumbo v10, "problem parsing idx "

    #@ed
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v9

    #@f1
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v9

    #@f5
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f8
    move-result-object v9

    #@f9
    invoke-direct {v8, v9, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@fc
    throw v8
    :try_end_fd
    .catchall {:try_start_e3 .. :try_end_fd} :catchall_73

    #@fd
    .line 228
    .end local v0           #e:Ljava/io/IOException;
    .end local v4           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v5       #reader:Lcom/android/internal/util/ProcFileReader;
    :catchall_fd
    move-exception v8

    #@fe
    move-object v4, v5

    #@ff
    .end local v5           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v4       #reader:Lcom/android/internal/util/ProcFileReader;
    goto/16 :goto_74

    #@101
    .line 225
    .end local v4           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v5       #reader:Lcom/android/internal/util/ProcFileReader;
    :catch_101
    move-exception v0

    #@102
    move-object v4, v5

    #@103
    .end local v5           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v4       #reader:Lcom/android/internal/util/ProcFileReader;
    goto :goto_e3

    #@104
    .line 223
    :catch_104
    move-exception v0

    #@105
    goto :goto_c1

    #@106
    .line 221
    :catch_106
    move-exception v0

    #@107
    goto/16 :goto_59
.end method

.method public readNetworkStatsSummaryDev()Landroid/net/NetworkStats;
    .registers 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 77
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    #@4
    move-result-object v5

    #@5
    .line 79
    .local v5, savedPolicy:Landroid/os/StrictMode$ThreadPolicy;
    new-instance v6, Landroid/net/NetworkStats;

    #@7
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@a
    move-result-wide v8

    #@b
    const/4 v10, 0x6

    #@c
    invoke-direct {v6, v8, v9, v10}, Landroid/net/NetworkStats;-><init>(JI)V

    #@f
    .line 80
    .local v6, stats:Landroid/net/NetworkStats;
    new-instance v2, Landroid/net/NetworkStats$Entry;

    #@11
    invoke-direct {v2}, Landroid/net/NetworkStats$Entry;-><init>()V

    #@14
    .line 82
    .local v2, entry:Landroid/net/NetworkStats$Entry;
    const/4 v3, 0x0

    #@15
    .line 84
    .local v3, reader:Lcom/android/internal/util/ProcFileReader;
    :try_start_15
    new-instance v4, Lcom/android/internal/util/ProcFileReader;

    #@17
    new-instance v8, Ljava/io/FileInputStream;

    #@19
    iget-object v9, p0, Lcom/android/internal/net/NetworkStatsFactory;->mStatsXtIfaceAll:Ljava/io/File;

    #@1b
    invoke-direct {v8, v9}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    #@1e
    invoke-direct {v4, v8}, Lcom/android/internal/util/ProcFileReader;-><init>(Ljava/io/InputStream;)V
    :try_end_21
    .catchall {:try_start_15 .. :try_end_21} :catchall_9e
    .catch Ljava/lang/NullPointerException; {:try_start_15 .. :try_end_21} :catch_ee
    .catch Ljava/lang/NumberFormatException; {:try_start_15 .. :try_end_21} :catch_af
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_21} :catch_ca

    #@21
    .line 86
    .end local v3           #reader:Lcom/android/internal/util/ProcFileReader;
    .local v4, reader:Lcom/android/internal/util/ProcFileReader;
    :goto_21
    :try_start_21
    invoke-virtual {v4}, Lcom/android/internal/util/ProcFileReader;->hasMoreData()Z

    #@24
    move-result v8

    #@25
    if-eqz v8, :cond_a8

    #@27
    .line 87
    invoke-virtual {v4}, Lcom/android/internal/util/ProcFileReader;->nextString()Ljava/lang/String;

    #@2a
    move-result-object v8

    #@2b
    iput-object v8, v2, Landroid/net/NetworkStats$Entry;->iface:Ljava/lang/String;

    #@2d
    .line 88
    const/4 v8, -0x1

    #@2e
    iput v8, v2, Landroid/net/NetworkStats$Entry;->uid:I

    #@30
    .line 89
    const/4 v8, -0x1

    #@31
    iput v8, v2, Landroid/net/NetworkStats$Entry;->set:I

    #@33
    .line 90
    const/4 v8, 0x0

    #@34
    iput v8, v2, Landroid/net/NetworkStats$Entry;->tag:I

    #@36
    .line 92
    invoke-virtual {v4}, Lcom/android/internal/util/ProcFileReader;->nextInt()I

    #@39
    move-result v8

    #@3a
    if-eqz v8, :cond_a6

    #@3c
    const/4 v0, 0x1

    #@3d
    .line 95
    .local v0, active:Z
    :goto_3d
    invoke-virtual {v4}, Lcom/android/internal/util/ProcFileReader;->nextLong()J

    #@40
    move-result-wide v8

    #@41
    iput-wide v8, v2, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@43
    .line 96
    invoke-virtual {v4}, Lcom/android/internal/util/ProcFileReader;->nextLong()J

    #@46
    move-result-wide v8

    #@47
    iput-wide v8, v2, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@49
    .line 97
    invoke-virtual {v4}, Lcom/android/internal/util/ProcFileReader;->nextLong()J

    #@4c
    move-result-wide v8

    #@4d
    iput-wide v8, v2, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@4f
    .line 98
    invoke-virtual {v4}, Lcom/android/internal/util/ProcFileReader;->nextLong()J

    #@52
    move-result-wide v8

    #@53
    iput-wide v8, v2, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@55
    .line 101
    if-eqz v0, :cond_7b

    #@57
    .line 102
    iget-wide v8, v2, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@59
    invoke-virtual {v4}, Lcom/android/internal/util/ProcFileReader;->nextLong()J

    #@5c
    move-result-wide v10

    #@5d
    add-long/2addr v8, v10

    #@5e
    iput-wide v8, v2, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@60
    .line 103
    iget-wide v8, v2, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@62
    invoke-virtual {v4}, Lcom/android/internal/util/ProcFileReader;->nextLong()J

    #@65
    move-result-wide v10

    #@66
    add-long/2addr v8, v10

    #@67
    iput-wide v8, v2, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@69
    .line 104
    iget-wide v8, v2, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@6b
    invoke-virtual {v4}, Lcom/android/internal/util/ProcFileReader;->nextLong()J

    #@6e
    move-result-wide v10

    #@6f
    add-long/2addr v8, v10

    #@70
    iput-wide v8, v2, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@72
    .line 105
    iget-wide v8, v2, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@74
    invoke-virtual {v4}, Lcom/android/internal/util/ProcFileReader;->nextLong()J

    #@77
    move-result-wide v10

    #@78
    add-long/2addr v8, v10

    #@79
    iput-wide v8, v2, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@7b
    .line 108
    :cond_7b
    invoke-virtual {v6, v2}, Landroid/net/NetworkStats;->addValues(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats;

    #@7e
    .line 109
    invoke-virtual {v4}, Lcom/android/internal/util/ProcFileReader;->finishLine()V
    :try_end_81
    .catchall {:try_start_21 .. :try_end_81} :catchall_e5
    .catch Ljava/lang/NullPointerException; {:try_start_21 .. :try_end_81} :catch_82
    .catch Ljava/lang/NumberFormatException; {:try_start_21 .. :try_end_81} :catch_eb
    .catch Ljava/io/IOException; {:try_start_21 .. :try_end_81} :catch_e8

    #@81
    goto :goto_21

    #@82
    .line 111
    .end local v0           #active:Z
    :catch_82
    move-exception v1

    #@83
    move-object v3, v4

    #@84
    .line 112
    .end local v4           #reader:Lcom/android/internal/util/ProcFileReader;
    .local v1, e:Ljava/lang/NullPointerException;
    .restart local v3       #reader:Lcom/android/internal/util/ProcFileReader;
    :goto_84
    :try_start_84
    new-instance v7, Ljava/lang/IllegalStateException;

    #@86
    new-instance v8, Ljava/lang/StringBuilder;

    #@88
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@8b
    const-string/jumbo v9, "problem parsing stats: "

    #@8e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v8

    #@92
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v8

    #@96
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@99
    move-result-object v8

    #@9a
    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@9d
    throw v7
    :try_end_9e
    .catchall {:try_start_84 .. :try_end_9e} :catchall_9e

    #@9e
    .line 118
    .end local v1           #e:Ljava/lang/NullPointerException;
    :catchall_9e
    move-exception v7

    #@9f
    :goto_9f
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@a2
    .line 119
    invoke-static {v5}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@a5
    throw v7

    #@a6
    .end local v3           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v4       #reader:Lcom/android/internal/util/ProcFileReader;
    :cond_a6
    move v0, v7

    #@a7
    .line 92
    goto :goto_3d

    #@a8
    .line 118
    :cond_a8
    invoke-static {v4}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@ab
    .line 119
    invoke-static {v5}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@ae
    .line 121
    return-object v6

    #@af
    .line 113
    .end local v4           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v3       #reader:Lcom/android/internal/util/ProcFileReader;
    :catch_af
    move-exception v1

    #@b0
    .line 114
    .local v1, e:Ljava/lang/NumberFormatException;
    :goto_b0
    :try_start_b0
    new-instance v7, Ljava/lang/IllegalStateException;

    #@b2
    new-instance v8, Ljava/lang/StringBuilder;

    #@b4
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@b7
    const-string/jumbo v9, "problem parsing stats: "

    #@ba
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v8

    #@be
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v8

    #@c2
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c5
    move-result-object v8

    #@c6
    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c9
    throw v7

    #@ca
    .line 115
    .end local v1           #e:Ljava/lang/NumberFormatException;
    :catch_ca
    move-exception v1

    #@cb
    .line 116
    .local v1, e:Ljava/io/IOException;
    :goto_cb
    new-instance v7, Ljava/lang/IllegalStateException;

    #@cd
    new-instance v8, Ljava/lang/StringBuilder;

    #@cf
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@d2
    const-string/jumbo v9, "problem parsing stats: "

    #@d5
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v8

    #@d9
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v8

    #@dd
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e0
    move-result-object v8

    #@e1
    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@e4
    throw v7
    :try_end_e5
    .catchall {:try_start_b0 .. :try_end_e5} :catchall_9e

    #@e5
    .line 118
    .end local v1           #e:Ljava/io/IOException;
    .end local v3           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v4       #reader:Lcom/android/internal/util/ProcFileReader;
    :catchall_e5
    move-exception v7

    #@e6
    move-object v3, v4

    #@e7
    .end local v4           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v3       #reader:Lcom/android/internal/util/ProcFileReader;
    goto :goto_9f

    #@e8
    .line 115
    .end local v3           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v4       #reader:Lcom/android/internal/util/ProcFileReader;
    :catch_e8
    move-exception v1

    #@e9
    move-object v3, v4

    #@ea
    .end local v4           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v3       #reader:Lcom/android/internal/util/ProcFileReader;
    goto :goto_cb

    #@eb
    .line 113
    .end local v3           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v4       #reader:Lcom/android/internal/util/ProcFileReader;
    :catch_eb
    move-exception v1

    #@ec
    move-object v3, v4

    #@ed
    .end local v4           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v3       #reader:Lcom/android/internal/util/ProcFileReader;
    goto :goto_b0

    #@ee
    .line 111
    :catch_ee
    move-exception v1

    #@ef
    goto :goto_84
.end method

.method public readNetworkStatsSummaryXt()Landroid/net/NetworkStats;
    .registers 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 132
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    #@3
    move-result-object v4

    #@4
    .line 135
    .local v4, savedPolicy:Landroid/os/StrictMode$ThreadPolicy;
    iget-object v6, p0, Lcom/android/internal/net/NetworkStatsFactory;->mStatsXtIfaceFmt:Ljava/io/File;

    #@6
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    #@9
    move-result v6

    #@a
    if-nez v6, :cond_e

    #@c
    const/4 v5, 0x0

    #@d
    .line 170
    :goto_d
    return-object v5

    #@e
    .line 137
    :cond_e
    new-instance v5, Landroid/net/NetworkStats;

    #@10
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@13
    move-result-wide v6

    #@14
    const/4 v8, 0x6

    #@15
    invoke-direct {v5, v6, v7, v8}, Landroid/net/NetworkStats;-><init>(JI)V

    #@18
    .line 138
    .local v5, stats:Landroid/net/NetworkStats;
    new-instance v1, Landroid/net/NetworkStats$Entry;

    #@1a
    invoke-direct {v1}, Landroid/net/NetworkStats$Entry;-><init>()V

    #@1d
    .line 140
    .local v1, entry:Landroid/net/NetworkStats$Entry;
    const/4 v2, 0x0

    #@1e
    .line 143
    .local v2, reader:Lcom/android/internal/util/ProcFileReader;
    :try_start_1e
    new-instance v3, Lcom/android/internal/util/ProcFileReader;

    #@20
    new-instance v6, Ljava/io/FileInputStream;

    #@22
    iget-object v7, p0, Lcom/android/internal/net/NetworkStatsFactory;->mStatsXtIfaceFmt:Ljava/io/File;

    #@24
    invoke-direct {v6, v7}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    #@27
    invoke-direct {v3, v6}, Lcom/android/internal/util/ProcFileReader;-><init>(Ljava/io/InputStream;)V
    :try_end_2a
    .catchall {:try_start_1e .. :try_end_2a} :catchall_7d
    .catch Ljava/lang/NullPointerException; {:try_start_1e .. :try_end_2a} :catch_cb
    .catch Ljava/lang/NumberFormatException; {:try_start_1e .. :try_end_2a} :catch_8c
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_2a} :catch_a7

    #@2a
    .line 144
    .end local v2           #reader:Lcom/android/internal/util/ProcFileReader;
    .local v3, reader:Lcom/android/internal/util/ProcFileReader;
    :try_start_2a
    invoke-virtual {v3}, Lcom/android/internal/util/ProcFileReader;->finishLine()V

    #@2d
    .line 146
    :goto_2d
    invoke-virtual {v3}, Lcom/android/internal/util/ProcFileReader;->hasMoreData()Z

    #@30
    move-result v6

    #@31
    if-eqz v6, :cond_85

    #@33
    .line 147
    invoke-virtual {v3}, Lcom/android/internal/util/ProcFileReader;->nextString()Ljava/lang/String;

    #@36
    move-result-object v6

    #@37
    iput-object v6, v1, Landroid/net/NetworkStats$Entry;->iface:Ljava/lang/String;

    #@39
    .line 148
    const/4 v6, -0x1

    #@3a
    iput v6, v1, Landroid/net/NetworkStats$Entry;->uid:I

    #@3c
    .line 149
    const/4 v6, -0x1

    #@3d
    iput v6, v1, Landroid/net/NetworkStats$Entry;->set:I

    #@3f
    .line 150
    const/4 v6, 0x0

    #@40
    iput v6, v1, Landroid/net/NetworkStats$Entry;->tag:I

    #@42
    .line 152
    invoke-virtual {v3}, Lcom/android/internal/util/ProcFileReader;->nextLong()J

    #@45
    move-result-wide v6

    #@46
    iput-wide v6, v1, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@48
    .line 153
    invoke-virtual {v3}, Lcom/android/internal/util/ProcFileReader;->nextLong()J

    #@4b
    move-result-wide v6

    #@4c
    iput-wide v6, v1, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@4e
    .line 154
    invoke-virtual {v3}, Lcom/android/internal/util/ProcFileReader;->nextLong()J

    #@51
    move-result-wide v6

    #@52
    iput-wide v6, v1, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@54
    .line 155
    invoke-virtual {v3}, Lcom/android/internal/util/ProcFileReader;->nextLong()J

    #@57
    move-result-wide v6

    #@58
    iput-wide v6, v1, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@5a
    .line 157
    invoke-virtual {v5, v1}, Landroid/net/NetworkStats;->addValues(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats;

    #@5d
    .line 158
    invoke-virtual {v3}, Lcom/android/internal/util/ProcFileReader;->finishLine()V
    :try_end_60
    .catchall {:try_start_2a .. :try_end_60} :catchall_c2
    .catch Ljava/lang/NullPointerException; {:try_start_2a .. :try_end_60} :catch_61
    .catch Ljava/lang/NumberFormatException; {:try_start_2a .. :try_end_60} :catch_c8
    .catch Ljava/io/IOException; {:try_start_2a .. :try_end_60} :catch_c5

    #@60
    goto :goto_2d

    #@61
    .line 160
    :catch_61
    move-exception v0

    #@62
    move-object v2, v3

    #@63
    .line 161
    .end local v3           #reader:Lcom/android/internal/util/ProcFileReader;
    .local v0, e:Ljava/lang/NullPointerException;
    .restart local v2       #reader:Lcom/android/internal/util/ProcFileReader;
    :goto_63
    :try_start_63
    new-instance v6, Ljava/lang/IllegalStateException;

    #@65
    new-instance v7, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string/jumbo v8, "problem parsing stats: "

    #@6d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v7

    #@71
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v7

    #@75
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v7

    #@79
    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@7c
    throw v6
    :try_end_7d
    .catchall {:try_start_63 .. :try_end_7d} :catchall_7d

    #@7d
    .line 167
    .end local v0           #e:Ljava/lang/NullPointerException;
    :catchall_7d
    move-exception v6

    #@7e
    :goto_7e
    invoke-static {v2}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@81
    .line 168
    invoke-static {v4}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@84
    throw v6

    #@85
    .line 167
    .end local v2           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v3       #reader:Lcom/android/internal/util/ProcFileReader;
    :cond_85
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@88
    .line 168
    invoke-static {v4}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@8b
    goto :goto_d

    #@8c
    .line 162
    .end local v3           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v2       #reader:Lcom/android/internal/util/ProcFileReader;
    :catch_8c
    move-exception v0

    #@8d
    .line 163
    .local v0, e:Ljava/lang/NumberFormatException;
    :goto_8d
    :try_start_8d
    new-instance v6, Ljava/lang/IllegalStateException;

    #@8f
    new-instance v7, Ljava/lang/StringBuilder;

    #@91
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@94
    const-string/jumbo v8, "problem parsing stats: "

    #@97
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v7

    #@9b
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v7

    #@9f
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a2
    move-result-object v7

    #@a3
    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@a6
    throw v6

    #@a7
    .line 164
    .end local v0           #e:Ljava/lang/NumberFormatException;
    :catch_a7
    move-exception v0

    #@a8
    .line 165
    .local v0, e:Ljava/io/IOException;
    :goto_a8
    new-instance v6, Ljava/lang/IllegalStateException;

    #@aa
    new-instance v7, Ljava/lang/StringBuilder;

    #@ac
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@af
    const-string/jumbo v8, "problem parsing stats: "

    #@b2
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v7

    #@b6
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v7

    #@ba
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bd
    move-result-object v7

    #@be
    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c1
    throw v6
    :try_end_c2
    .catchall {:try_start_8d .. :try_end_c2} :catchall_7d

    #@c2
    .line 167
    .end local v0           #e:Ljava/io/IOException;
    .end local v2           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v3       #reader:Lcom/android/internal/util/ProcFileReader;
    :catchall_c2
    move-exception v6

    #@c3
    move-object v2, v3

    #@c4
    .end local v3           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v2       #reader:Lcom/android/internal/util/ProcFileReader;
    goto :goto_7e

    #@c5
    .line 164
    .end local v2           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v3       #reader:Lcom/android/internal/util/ProcFileReader;
    :catch_c5
    move-exception v0

    #@c6
    move-object v2, v3

    #@c7
    .end local v3           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v2       #reader:Lcom/android/internal/util/ProcFileReader;
    goto :goto_a8

    #@c8
    .line 162
    .end local v2           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v3       #reader:Lcom/android/internal/util/ProcFileReader;
    :catch_c8
    move-exception v0

    #@c9
    move-object v2, v3

    #@ca
    .end local v3           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v2       #reader:Lcom/android/internal/util/ProcFileReader;
    goto :goto_8d

    #@cb
    .line 160
    :catch_cb
    move-exception v0

    #@cc
    goto :goto_63
.end method

.method public readNetworkStatsUidInterface(ILjava/lang/String;I)J
    .registers 26
    .parameter "limitUid"
    .parameter "iface"
    .parameter "type"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 243
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    #@3
    move-result-object v13

    #@4
    .line 244
    .local v13, savedPolicy:Landroid/os/StrictMode$ThreadPolicy;
    new-instance v4, Landroid/net/NetworkStats$Entry;

    #@6
    invoke-direct {v4}, Landroid/net/NetworkStats$Entry;-><init>()V

    #@9
    .line 246
    .local v4, entry:Landroid/net/NetworkStats$Entry;
    const/4 v5, 0x1

    #@a
    .line 247
    .local v5, idx:I
    const/4 v6, 0x1

    #@b
    .line 249
    .local v6, lastIdx:I
    const-wide/16 v9, 0x0

    #@d
    .line 250
    .local v9, rxBytesSum:J
    const-wide/16 v11, 0x0

    #@f
    .line 251
    .local v11, rxPacketsSum:J
    const-wide/16 v14, 0x0

    #@11
    .line 252
    .local v14, txBytesSum:J
    const-wide/16 v16, 0x0

    #@13
    .line 254
    .local v16, txPacketsSum:J
    const/4 v7, 0x0

    #@14
    .line 257
    .local v7, reader:Lcom/android/internal/util/ProcFileReader;
    :try_start_14
    new-instance v8, Lcom/android/internal/util/ProcFileReader;

    #@16
    new-instance v18, Ljava/io/FileInputStream;

    #@18
    move-object/from16 v0, p0

    #@1a
    iget-object v0, v0, Lcom/android/internal/net/NetworkStatsFactory;->mStatsXtUid:Ljava/io/File;

    #@1c
    move-object/from16 v19, v0

    #@1e
    invoke-direct/range {v18 .. v19}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    #@21
    move-object/from16 v0, v18

    #@23
    invoke-direct {v8, v0}, Lcom/android/internal/util/ProcFileReader;-><init>(Ljava/io/InputStream;)V
    :try_end_26
    .catchall {:try_start_14 .. :try_end_26} :catchall_82
    .catch Ljava/lang/NullPointerException; {:try_start_14 .. :try_end_26} :catch_28f
    .catch Ljava/lang/NumberFormatException; {:try_start_14 .. :try_end_26} :catch_28c
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_26} :catch_23b

    #@26
    .line 258
    .end local v7           #reader:Lcom/android/internal/util/ProcFileReader;
    .local v8, reader:Lcom/android/internal/util/ProcFileReader;
    :try_start_26
    invoke-virtual {v8}, Lcom/android/internal/util/ProcFileReader;->finishLine()V

    #@29
    .line 260
    :goto_29
    invoke-virtual {v8}, Lcom/android/internal/util/ProcFileReader;->hasMoreData()Z

    #@2c
    move-result v18

    #@2d
    if-eqz v18, :cond_1ec

    #@2f
    .line 261
    invoke-virtual {v8}, Lcom/android/internal/util/ProcFileReader;->nextInt()I

    #@32
    move-result v5

    #@33
    .line 262
    add-int/lit8 v18, v6, 0x1

    #@35
    move/from16 v0, v18

    #@37
    if-eq v5, v0, :cond_8a

    #@39
    .line 263
    new-instance v18, Ljava/lang/IllegalStateException;

    #@3b
    new-instance v19, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v20, "inconsistent idx="

    #@42
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v19

    #@46
    move-object/from16 v0, v19

    #@48
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v19

    #@4c
    const-string v20, " after lastIdx="

    #@4e
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v19

    #@52
    move-object/from16 v0, v19

    #@54
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@57
    move-result-object v19

    #@58
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v19

    #@5c
    invoke-direct/range {v18 .. v19}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@5f
    throw v18
    :try_end_60
    .catchall {:try_start_26 .. :try_end_60} :catchall_285
    .catch Ljava/lang/NullPointerException; {:try_start_26 .. :try_end_60} :catch_60
    .catch Ljava/lang/NumberFormatException; {:try_start_26 .. :try_end_60} :catch_1ca
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_60} :catch_289

    #@60
    .line 294
    :catch_60
    move-exception v3

    #@61
    move-object v7, v8

    #@62
    .line 295
    .end local v8           #reader:Lcom/android/internal/util/ProcFileReader;
    .local v3, e:Ljava/lang/NullPointerException;
    .restart local v7       #reader:Lcom/android/internal/util/ProcFileReader;
    :goto_62
    :try_start_62
    new-instance v18, Ljava/lang/IllegalStateException;

    #@64
    new-instance v19, Ljava/lang/StringBuilder;

    #@66
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string/jumbo v20, "problem parsing idx "

    #@6c
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v19

    #@70
    move-object/from16 v0, v19

    #@72
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@75
    move-result-object v19

    #@76
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v19

    #@7a
    move-object/from16 v0, v18

    #@7c
    move-object/from16 v1, v19

    #@7e
    invoke-direct {v0, v1, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@81
    throw v18
    :try_end_82
    .catchall {:try_start_62 .. :try_end_82} :catchall_82

    #@82
    .line 301
    .end local v3           #e:Ljava/lang/NullPointerException;
    :catchall_82
    move-exception v18

    #@83
    :goto_83
    invoke-static {v7}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@86
    .line 302
    invoke-static {v13}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@89
    throw v18

    #@8a
    .line 266
    .end local v7           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v8       #reader:Lcom/android/internal/util/ProcFileReader;
    :cond_8a
    move v6, v5

    #@8b
    .line 268
    :try_start_8b
    invoke-virtual {v8}, Lcom/android/internal/util/ProcFileReader;->nextString()Ljava/lang/String;

    #@8e
    move-result-object v18

    #@8f
    move-object/from16 v0, v18

    #@91
    iput-object v0, v4, Landroid/net/NetworkStats$Entry;->iface:Ljava/lang/String;

    #@93
    .line 269
    invoke-virtual {v8}, Lcom/android/internal/util/ProcFileReader;->nextString()Ljava/lang/String;

    #@96
    move-result-object v18

    #@97
    invoke-static/range {v18 .. v18}, Lcom/android/server/NetworkManagementSocketTagger;->kernelToTag(Ljava/lang/String;)I

    #@9a
    move-result v18

    #@9b
    move/from16 v0, v18

    #@9d
    iput v0, v4, Landroid/net/NetworkStats$Entry;->tag:I

    #@9f
    .line 270
    invoke-virtual {v8}, Lcom/android/internal/util/ProcFileReader;->nextInt()I

    #@a2
    move-result v18

    #@a3
    move/from16 v0, v18

    #@a5
    iput v0, v4, Landroid/net/NetworkStats$Entry;->uid:I

    #@a7
    .line 271
    invoke-virtual {v8}, Lcom/android/internal/util/ProcFileReader;->nextInt()I

    #@aa
    move-result v18

    #@ab
    move/from16 v0, v18

    #@ad
    iput v0, v4, Landroid/net/NetworkStats$Entry;->set:I

    #@af
    .line 272
    invoke-virtual {v8}, Lcom/android/internal/util/ProcFileReader;->nextLong()J

    #@b2
    move-result-wide v18

    #@b3
    move-wide/from16 v0, v18

    #@b5
    iput-wide v0, v4, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@b7
    .line 273
    invoke-virtual {v8}, Lcom/android/internal/util/ProcFileReader;->nextLong()J

    #@ba
    move-result-wide v18

    #@bb
    move-wide/from16 v0, v18

    #@bd
    iput-wide v0, v4, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@bf
    .line 274
    invoke-virtual {v8}, Lcom/android/internal/util/ProcFileReader;->nextLong()J

    #@c2
    move-result-wide v18

    #@c3
    move-wide/from16 v0, v18

    #@c5
    iput-wide v0, v4, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@c7
    .line 275
    invoke-virtual {v8}, Lcom/android/internal/util/ProcFileReader;->nextLong()J

    #@ca
    move-result-wide v18

    #@cb
    move-wide/from16 v0, v18

    #@cd
    iput-wide v0, v4, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@cf
    .line 278
    const-string v18, "NetworkStatsFactory"

    #@d1
    new-instance v19, Ljava/lang/StringBuilder;

    #@d3
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@d6
    const-string v20, "[vzw_stat] iface : "

    #@d8
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@db
    move-result-object v19

    #@dc
    iget-object v0, v4, Landroid/net/NetworkStats$Entry;->iface:Ljava/lang/String;

    #@de
    move-object/from16 v20, v0

    #@e0
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e3
    move-result-object v19

    #@e4
    const-string v20, ", uid : "

    #@e6
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v19

    #@ea
    iget v0, v4, Landroid/net/NetworkStats$Entry;->uid:I

    #@ec
    move/from16 v20, v0

    #@ee
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f1
    move-result-object v19

    #@f2
    const-string v20, ", rxBytes : "

    #@f4
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v19

    #@f8
    iget-wide v0, v4, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@fa
    move-wide/from16 v20, v0

    #@fc
    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v19

    #@100
    const-string v20, ", rxPackets : "

    #@102
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@105
    move-result-object v19

    #@106
    iget-wide v0, v4, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@108
    move-wide/from16 v20, v0

    #@10a
    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v19

    #@10e
    const-string v20, ", txBytes : "

    #@110
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@113
    move-result-object v19

    #@114
    iget-wide v0, v4, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@116
    move-wide/from16 v20, v0

    #@118
    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v19

    #@11c
    const-string v20, ", txPackets : "

    #@11e
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@121
    move-result-object v19

    #@122
    iget-wide v0, v4, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@124
    move-wide/from16 v20, v0

    #@126
    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@129
    move-result-object v19

    #@12a
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12d
    move-result-object v19

    #@12e
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@131
    .line 281
    iget v0, v4, Landroid/net/NetworkStats$Entry;->uid:I

    #@133
    move/from16 v18, v0

    #@135
    move/from16 v0, v18

    #@137
    move/from16 v1, p1

    #@139
    if-ne v0, v1, :cond_1c5

    #@13b
    if-eqz p2, :cond_1c5

    #@13d
    iget-object v0, v4, Landroid/net/NetworkStats$Entry;->iface:Ljava/lang/String;

    #@13f
    move-object/from16 v18, v0

    #@141
    move-object/from16 v0, p2

    #@143
    move-object/from16 v1, v18

    #@145
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@148
    move-result v18

    #@149
    if-eqz v18, :cond_1c5

    #@14b
    .line 283
    const-string v18, "NetworkStatsFactory"

    #@14d
    new-instance v19, Ljava/lang/StringBuilder;

    #@14f
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@152
    const-string v20, "[vzw_stat] selected uid: "

    #@154
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@157
    move-result-object v19

    #@158
    move-object/from16 v0, v19

    #@15a
    move/from16 v1, p1

    #@15c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15f
    move-result-object v19

    #@160
    const-string v20, ", selected iface : "

    #@162
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@165
    move-result-object v19

    #@166
    move-object/from16 v0, v19

    #@168
    move-object/from16 v1, p2

    #@16a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16d
    move-result-object v19

    #@16e
    const-string v20, ", rxBytes : "

    #@170
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@173
    move-result-object v19

    #@174
    iget-wide v0, v4, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@176
    move-wide/from16 v20, v0

    #@178
    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@17b
    move-result-object v19

    #@17c
    const-string v20, ", rxPackets : "

    #@17e
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@181
    move-result-object v19

    #@182
    iget-wide v0, v4, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@184
    move-wide/from16 v20, v0

    #@186
    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@189
    move-result-object v19

    #@18a
    const-string v20, ", txBytes : "

    #@18c
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18f
    move-result-object v19

    #@190
    iget-wide v0, v4, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@192
    move-wide/from16 v20, v0

    #@194
    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@197
    move-result-object v19

    #@198
    const-string v20, ", txPackets : "

    #@19a
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19d
    move-result-object v19

    #@19e
    iget-wide v0, v4, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@1a0
    move-wide/from16 v20, v0

    #@1a2
    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1a5
    move-result-object v19

    #@1a6
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a9
    move-result-object v19

    #@1aa
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1ad
    .line 286
    iget-wide v0, v4, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@1af
    move-wide/from16 v18, v0

    #@1b1
    add-long v9, v9, v18

    #@1b3
    .line 287
    iget-wide v0, v4, Landroid/net/NetworkStats$Entry;->rxPackets:J

    #@1b5
    move-wide/from16 v18, v0

    #@1b7
    add-long v11, v11, v18

    #@1b9
    .line 288
    iget-wide v0, v4, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@1bb
    move-wide/from16 v18, v0

    #@1bd
    add-long v14, v14, v18

    #@1bf
    .line 289
    iget-wide v0, v4, Landroid/net/NetworkStats$Entry;->txPackets:J

    #@1c1
    move-wide/from16 v18, v0

    #@1c3
    add-long v16, v16, v18

    #@1c5
    .line 292
    :cond_1c5
    invoke-virtual {v8}, Lcom/android/internal/util/ProcFileReader;->finishLine()V
    :try_end_1c8
    .catchall {:try_start_8b .. :try_end_1c8} :catchall_285
    .catch Ljava/lang/NullPointerException; {:try_start_8b .. :try_end_1c8} :catch_60
    .catch Ljava/lang/NumberFormatException; {:try_start_8b .. :try_end_1c8} :catch_1ca
    .catch Ljava/io/IOException; {:try_start_8b .. :try_end_1c8} :catch_289

    #@1c8
    goto/16 :goto_29

    #@1ca
    .line 296
    :catch_1ca
    move-exception v3

    #@1cb
    move-object v7, v8

    #@1cc
    .line 297
    .end local v8           #reader:Lcom/android/internal/util/ProcFileReader;
    .local v3, e:Ljava/lang/NumberFormatException;
    .restart local v7       #reader:Lcom/android/internal/util/ProcFileReader;
    :goto_1cc
    :try_start_1cc
    new-instance v18, Ljava/lang/IllegalStateException;

    #@1ce
    new-instance v19, Ljava/lang/StringBuilder;

    #@1d0
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@1d3
    const-string/jumbo v20, "problem parsing idx "

    #@1d6
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d9
    move-result-object v19

    #@1da
    move-object/from16 v0, v19

    #@1dc
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1df
    move-result-object v19

    #@1e0
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e3
    move-result-object v19

    #@1e4
    move-object/from16 v0, v18

    #@1e6
    move-object/from16 v1, v19

    #@1e8
    invoke-direct {v0, v1, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@1eb
    throw v18
    :try_end_1ec
    .catchall {:try_start_1cc .. :try_end_1ec} :catchall_82

    #@1ec
    .line 301
    .end local v3           #e:Ljava/lang/NumberFormatException;
    .end local v7           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v8       #reader:Lcom/android/internal/util/ProcFileReader;
    :cond_1ec
    invoke-static {v8}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@1ef
    .line 302
    invoke-static {v13}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@1f2
    .line 305
    const-string v18, "NetworkStatsFactory"

    #@1f4
    new-instance v19, Ljava/lang/StringBuilder;

    #@1f6
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@1f9
    const-string v20, "[vzw_stat] total Sum - rxBytesSum : "

    #@1fb
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fe
    move-result-object v19

    #@1ff
    move-object/from16 v0, v19

    #@201
    invoke-virtual {v0, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@204
    move-result-object v19

    #@205
    const-string v20, ", rxPacketsSum : "

    #@207
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20a
    move-result-object v19

    #@20b
    move-object/from16 v0, v19

    #@20d
    invoke-virtual {v0, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@210
    move-result-object v19

    #@211
    const-string v20, ", txBytesSum : "

    #@213
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@216
    move-result-object v19

    #@217
    move-object/from16 v0, v19

    #@219
    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@21c
    move-result-object v19

    #@21d
    const-string v20, ", txPacketsSum : "

    #@21f
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@222
    move-result-object v19

    #@223
    move-object/from16 v0, v19

    #@225
    move-wide/from16 v1, v16

    #@227
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@22a
    move-result-object v19

    #@22b
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22e
    move-result-object v19

    #@22f
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@232
    .line 307
    const/16 v18, 0x1

    #@234
    move/from16 v0, p3

    #@236
    move/from16 v1, v18

    #@238
    if-ne v0, v1, :cond_25c

    #@23a
    .line 321
    .end local v9           #rxBytesSum:J
    :goto_23a
    return-wide v9

    #@23b
    .line 298
    .end local v8           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v7       #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v9       #rxBytesSum:J
    :catch_23b
    move-exception v3

    #@23c
    .line 299
    .local v3, e:Ljava/io/IOException;
    :goto_23c
    :try_start_23c
    new-instance v18, Ljava/lang/IllegalStateException;

    #@23e
    new-instance v19, Ljava/lang/StringBuilder;

    #@240
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@243
    const-string/jumbo v20, "problem parsing idx "

    #@246
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@249
    move-result-object v19

    #@24a
    move-object/from16 v0, v19

    #@24c
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24f
    move-result-object v19

    #@250
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@253
    move-result-object v19

    #@254
    move-object/from16 v0, v18

    #@256
    move-object/from16 v1, v19

    #@258
    invoke-direct {v0, v1, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@25b
    throw v18
    :try_end_25c
    .catchall {:try_start_23c .. :try_end_25c} :catchall_82

    #@25c
    .line 310
    .end local v3           #e:Ljava/io/IOException;
    .end local v7           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v8       #reader:Lcom/android/internal/util/ProcFileReader;
    :cond_25c
    const/16 v18, 0x2

    #@25e
    move/from16 v0, p3

    #@260
    move/from16 v1, v18

    #@262
    if-ne v0, v1, :cond_266

    #@264
    move-wide v9, v11

    #@265
    .line 311
    goto :goto_23a

    #@266
    .line 313
    :cond_266
    const/16 v18, 0x3

    #@268
    move/from16 v0, p3

    #@26a
    move/from16 v1, v18

    #@26c
    if-ne v0, v1, :cond_270

    #@26e
    move-wide v9, v14

    #@26f
    .line 314
    goto :goto_23a

    #@270
    .line 316
    :cond_270
    const/16 v18, 0x4

    #@272
    move/from16 v0, p3

    #@274
    move/from16 v1, v18

    #@276
    if-ne v0, v1, :cond_27b

    #@278
    move-wide/from16 v9, v16

    #@27a
    .line 317
    goto :goto_23a

    #@27b
    .line 320
    :cond_27b
    const-string v18, "NetworkStatsFactory"

    #@27d
    const-string v19, "[vzw_stat] invalid type return 0"

    #@27f
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@282
    .line 321
    const-wide/16 v9, 0x0

    #@284
    goto :goto_23a

    #@285
    .line 301
    :catchall_285
    move-exception v18

    #@286
    move-object v7, v8

    #@287
    .end local v8           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v7       #reader:Lcom/android/internal/util/ProcFileReader;
    goto/16 :goto_83

    #@289
    .line 298
    .end local v7           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v8       #reader:Lcom/android/internal/util/ProcFileReader;
    :catch_289
    move-exception v3

    #@28a
    move-object v7, v8

    #@28b
    .end local v8           #reader:Lcom/android/internal/util/ProcFileReader;
    .restart local v7       #reader:Lcom/android/internal/util/ProcFileReader;
    goto :goto_23c

    #@28c
    .line 296
    :catch_28c
    move-exception v3

    #@28d
    goto/16 :goto_1cc

    #@28f
    .line 294
    :catch_28f
    move-exception v3

    #@290
    goto/16 :goto_62
.end method
