.class final Lcom/android/internal/net/VpnConfig$1;
.super Ljava/lang/Object;
.source "VpnConfig.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/net/VpnConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/android/internal/net/VpnConfig;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 93
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/android/internal/net/VpnConfig;
    .registers 5
    .parameter "in"

    #@0
    .prologue
    .line 96
    new-instance v0, Lcom/android/internal/net/VpnConfig;

    #@2
    invoke-direct {v0}, Lcom/android/internal/net/VpnConfig;-><init>()V

    #@5
    .line 97
    .local v0, config:Lcom/android/internal/net/VpnConfig;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    iput-object v1, v0, Lcom/android/internal/net/VpnConfig;->user:Ljava/lang/String;

    #@b
    .line 98
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    iput-object v1, v0, Lcom/android/internal/net/VpnConfig;->interfaze:Ljava/lang/String;

    #@11
    .line 99
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    iput-object v1, v0, Lcom/android/internal/net/VpnConfig;->session:Ljava/lang/String;

    #@17
    .line 100
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1a
    move-result v1

    #@1b
    iput v1, v0, Lcom/android/internal/net/VpnConfig;->mtu:I

    #@1d
    .line 101
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    iput-object v1, v0, Lcom/android/internal/net/VpnConfig;->addresses:Ljava/lang/String;

    #@23
    .line 102
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    iput-object v1, v0, Lcom/android/internal/net/VpnConfig;->routes:Ljava/lang/String;

    #@29
    .line 103
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    #@2c
    move-result-object v1

    #@2d
    iput-object v1, v0, Lcom/android/internal/net/VpnConfig;->dnsServers:Ljava/util/List;

    #@2f
    .line 104
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    #@32
    move-result-object v1

    #@33
    iput-object v1, v0, Lcom/android/internal/net/VpnConfig;->searchDomains:Ljava/util/List;

    #@35
    .line 105
    const/4 v1, 0x0

    #@36
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    #@39
    move-result-object v1

    #@3a
    check-cast v1, Landroid/app/PendingIntent;

    #@3c
    iput-object v1, v0, Lcom/android/internal/net/VpnConfig;->configureIntent:Landroid/app/PendingIntent;

    #@3e
    .line 106
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@41
    move-result-wide v1

    #@42
    iput-wide v1, v0, Lcom/android/internal/net/VpnConfig;->startTime:J

    #@44
    .line 107
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@47
    move-result v1

    #@48
    if-eqz v1, :cond_4e

    #@4a
    const/4 v1, 0x1

    #@4b
    :goto_4b
    iput-boolean v1, v0, Lcom/android/internal/net/VpnConfig;->legacy:Z

    #@4d
    .line 108
    return-object v0

    #@4e
    .line 107
    :cond_4e
    const/4 v1, 0x0

    #@4f
    goto :goto_4b
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 93
    invoke-virtual {p0, p1}, Lcom/android/internal/net/VpnConfig$1;->createFromParcel(Landroid/os/Parcel;)Lcom/android/internal/net/VpnConfig;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Lcom/android/internal/net/VpnConfig;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 113
    new-array v0, p1, [Lcom/android/internal/net/VpnConfig;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 93
    invoke-virtual {p0, p1}, Lcom/android/internal/net/VpnConfig$1;->newArray(I)[Lcom/android/internal/net/VpnConfig;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
