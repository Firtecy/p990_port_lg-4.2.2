.class public Lcom/android/internal/net/VpnProfile;
.super Ljava/lang/Object;
.source "VpnProfile.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/internal/net/VpnProfile;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "VpnProfile"

.field public static final TYPE_IPSEC_HYBRID_RSA:I = 0x5

.field public static final TYPE_IPSEC_XAUTH_PSK:I = 0x3

.field public static final TYPE_IPSEC_XAUTH_RSA:I = 0x4

.field public static final TYPE_L2TP_IPSEC_PSK:I = 0x1

.field public static final TYPE_L2TP_IPSEC_RSA:I = 0x2

.field public static final TYPE_MAX:I = 0x5

.field public static final TYPE_PPTP:I


# instance fields
.field public dnsServers:Ljava/lang/String;

.field public ipsecCaCert:Ljava/lang/String;

.field public ipsecIdentifier:Ljava/lang/String;

.field public ipsecSecret:Ljava/lang/String;

.field public ipsecServerCert:Ljava/lang/String;

.field public ipsecUserCert:Ljava/lang/String;

.field public final key:Ljava/lang/String;

.field public l2tpSecret:Ljava/lang/String;

.field public mppe:Z

.field public name:Ljava/lang/String;

.field public password:Ljava/lang/String;

.field public routes:Ljava/lang/String;

.field public saveLogin:Z

.field public searchDomains:Ljava/lang/String;

.field public server:Ljava/lang/String;

.field public type:I

.field public username:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 199
    new-instance v0, Lcom/android/internal/net/VpnProfile$1;

    #@2
    invoke-direct {v0}, Lcom/android/internal/net/VpnProfile$1;-><init>()V

    #@5
    sput-object v0, Lcom/android/internal/net/VpnProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 5
    .parameter "in"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 73
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 50
    const-string v0, ""

    #@7
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->name:Ljava/lang/String;

    #@9
    .line 51
    iput v2, p0, Lcom/android/internal/net/VpnProfile;->type:I

    #@b
    .line 52
    const-string v0, ""

    #@d
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->server:Ljava/lang/String;

    #@f
    .line 53
    const-string v0, ""

    #@11
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->username:Ljava/lang/String;

    #@13
    .line 54
    const-string v0, ""

    #@15
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->password:Ljava/lang/String;

    #@17
    .line 55
    const-string v0, ""

    #@19
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->dnsServers:Ljava/lang/String;

    #@1b
    .line 56
    const-string v0, ""

    #@1d
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->searchDomains:Ljava/lang/String;

    #@1f
    .line 57
    const-string v0, ""

    #@21
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->routes:Ljava/lang/String;

    #@23
    .line 58
    iput-boolean v1, p0, Lcom/android/internal/net/VpnProfile;->mppe:Z

    #@25
    .line 59
    const-string v0, ""

    #@27
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->l2tpSecret:Ljava/lang/String;

    #@29
    .line 60
    const-string v0, ""

    #@2b
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->ipsecIdentifier:Ljava/lang/String;

    #@2d
    .line 61
    const-string v0, ""

    #@2f
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->ipsecSecret:Ljava/lang/String;

    #@31
    .line 62
    const-string v0, ""

    #@33
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->ipsecUserCert:Ljava/lang/String;

    #@35
    .line 63
    const-string v0, ""

    #@37
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->ipsecCaCert:Ljava/lang/String;

    #@39
    .line 64
    const-string v0, ""

    #@3b
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->ipsecServerCert:Ljava/lang/String;

    #@3d
    .line 67
    iput-boolean v2, p0, Lcom/android/internal/net/VpnProfile;->saveLogin:Z

    #@3f
    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@42
    move-result-object v0

    #@43
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->key:Ljava/lang/String;

    #@45
    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@48
    move-result-object v0

    #@49
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->name:Ljava/lang/String;

    #@4b
    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@4e
    move-result v0

    #@4f
    iput v0, p0, Lcom/android/internal/net/VpnProfile;->type:I

    #@51
    .line 77
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@54
    move-result-object v0

    #@55
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->server:Ljava/lang/String;

    #@57
    .line 78
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5a
    move-result-object v0

    #@5b
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->username:Ljava/lang/String;

    #@5d
    .line 79
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@60
    move-result-object v0

    #@61
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->password:Ljava/lang/String;

    #@63
    .line 80
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@66
    move-result-object v0

    #@67
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->dnsServers:Ljava/lang/String;

    #@69
    .line 81
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6c
    move-result-object v0

    #@6d
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->searchDomains:Ljava/lang/String;

    #@6f
    .line 82
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@72
    move-result-object v0

    #@73
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->routes:Ljava/lang/String;

    #@75
    .line 83
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@78
    move-result v0

    #@79
    if-eqz v0, :cond_ab

    #@7b
    move v0, v1

    #@7c
    :goto_7c
    iput-boolean v0, p0, Lcom/android/internal/net/VpnProfile;->mppe:Z

    #@7e
    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@81
    move-result-object v0

    #@82
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->l2tpSecret:Ljava/lang/String;

    #@84
    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@87
    move-result-object v0

    #@88
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->ipsecIdentifier:Ljava/lang/String;

    #@8a
    .line 86
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@8d
    move-result-object v0

    #@8e
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->ipsecSecret:Ljava/lang/String;

    #@90
    .line 87
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@93
    move-result-object v0

    #@94
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->ipsecUserCert:Ljava/lang/String;

    #@96
    .line 88
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@99
    move-result-object v0

    #@9a
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->ipsecCaCert:Ljava/lang/String;

    #@9c
    .line 89
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@9f
    move-result-object v0

    #@a0
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->ipsecServerCert:Ljava/lang/String;

    #@a2
    .line 90
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@a5
    move-result v0

    #@a6
    if-eqz v0, :cond_ad

    #@a8
    :goto_a8
    iput-boolean v1, p0, Lcom/android/internal/net/VpnProfile;->saveLogin:Z

    #@aa
    .line 91
    return-void

    #@ab
    :cond_ab
    move v0, v2

    #@ac
    .line 83
    goto :goto_7c

    #@ad
    :cond_ad
    move v1, v2

    #@ae
    .line 90
    goto :goto_a8
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter "key"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 69
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 50
    const-string v0, ""

    #@6
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->name:Ljava/lang/String;

    #@8
    .line 51
    iput v1, p0, Lcom/android/internal/net/VpnProfile;->type:I

    #@a
    .line 52
    const-string v0, ""

    #@c
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->server:Ljava/lang/String;

    #@e
    .line 53
    const-string v0, ""

    #@10
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->username:Ljava/lang/String;

    #@12
    .line 54
    const-string v0, ""

    #@14
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->password:Ljava/lang/String;

    #@16
    .line 55
    const-string v0, ""

    #@18
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->dnsServers:Ljava/lang/String;

    #@1a
    .line 56
    const-string v0, ""

    #@1c
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->searchDomains:Ljava/lang/String;

    #@1e
    .line 57
    const-string v0, ""

    #@20
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->routes:Ljava/lang/String;

    #@22
    .line 58
    const/4 v0, 0x1

    #@23
    iput-boolean v0, p0, Lcom/android/internal/net/VpnProfile;->mppe:Z

    #@25
    .line 59
    const-string v0, ""

    #@27
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->l2tpSecret:Ljava/lang/String;

    #@29
    .line 60
    const-string v0, ""

    #@2b
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->ipsecIdentifier:Ljava/lang/String;

    #@2d
    .line 61
    const-string v0, ""

    #@2f
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->ipsecSecret:Ljava/lang/String;

    #@31
    .line 62
    const-string v0, ""

    #@33
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->ipsecUserCert:Ljava/lang/String;

    #@35
    .line 63
    const-string v0, ""

    #@37
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->ipsecCaCert:Ljava/lang/String;

    #@39
    .line 64
    const-string v0, ""

    #@3b
    iput-object v0, p0, Lcom/android/internal/net/VpnProfile;->ipsecServerCert:Ljava/lang/String;

    #@3d
    .line 67
    iput-boolean v1, p0, Lcom/android/internal/net/VpnProfile;->saveLogin:Z

    #@3f
    .line 70
    iput-object p1, p0, Lcom/android/internal/net/VpnProfile;->key:Ljava/lang/String;

    #@41
    .line 71
    return-void
.end method

.method public static decode(Ljava/lang/String;[B)Lcom/android/internal/net/VpnProfile;
    .registers 12
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    const/4 v9, 0x5

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v3, 0x0

    #@3
    const/16 v8, 0xe

    #@5
    const/4 v2, 0x0

    #@6
    .line 116
    if-nez p0, :cond_a

    #@8
    move-object v0, v2

    #@9
    .line 151
    :goto_9
    return-object v0

    #@a
    .line 120
    :cond_a
    :try_start_a
    new-instance v5, Ljava/lang/String;

    #@c
    sget-object v6, Ljava/nio/charset/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    #@e
    invoke-direct {v5, p1, v6}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    #@11
    const-string v6, "\u0000"

    #@13
    const/4 v7, -0x1

    #@14
    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    .line 122
    .local v1, values:[Ljava/lang/String;
    array-length v5, v1

    #@19
    if-lt v5, v8, :cond_20

    #@1b
    array-length v5, v1

    #@1c
    const/16 v6, 0xf

    #@1e
    if-le v5, v6, :cond_22

    #@20
    :cond_20
    move-object v0, v2

    #@21
    .line 123
    goto :goto_9

    #@22
    .line 126
    :cond_22
    new-instance v0, Lcom/android/internal/net/VpnProfile;

    #@24
    invoke-direct {v0, p0}, Lcom/android/internal/net/VpnProfile;-><init>(Ljava/lang/String;)V

    #@27
    .line 127
    .local v0, profile:Lcom/android/internal/net/VpnProfile;
    const/4 v5, 0x0

    #@28
    aget-object v5, v1, v5

    #@2a
    iput-object v5, v0, Lcom/android/internal/net/VpnProfile;->name:Ljava/lang/String;

    #@2c
    .line 128
    const/4 v5, 0x1

    #@2d
    aget-object v5, v1, v5

    #@2f
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    #@32
    move-result-object v5

    #@33
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    #@36
    move-result v5

    #@37
    iput v5, v0, Lcom/android/internal/net/VpnProfile;->type:I

    #@39
    .line 129
    iget v5, v0, Lcom/android/internal/net/VpnProfile;->type:I

    #@3b
    if-ltz v5, :cond_41

    #@3d
    iget v5, v0, Lcom/android/internal/net/VpnProfile;->type:I

    #@3f
    if-le v5, v9, :cond_43

    #@41
    :cond_41
    move-object v0, v2

    #@42
    .line 130
    goto :goto_9

    #@43
    .line 132
    :cond_43
    const/4 v5, 0x2

    #@44
    aget-object v5, v1, v5

    #@46
    iput-object v5, v0, Lcom/android/internal/net/VpnProfile;->server:Ljava/lang/String;

    #@48
    .line 133
    const/4 v5, 0x3

    #@49
    aget-object v5, v1, v5

    #@4b
    iput-object v5, v0, Lcom/android/internal/net/VpnProfile;->username:Ljava/lang/String;

    #@4d
    .line 134
    const/4 v5, 0x4

    #@4e
    aget-object v5, v1, v5

    #@50
    iput-object v5, v0, Lcom/android/internal/net/VpnProfile;->password:Ljava/lang/String;

    #@52
    .line 135
    const/4 v5, 0x5

    #@53
    aget-object v5, v1, v5

    #@55
    iput-object v5, v0, Lcom/android/internal/net/VpnProfile;->dnsServers:Ljava/lang/String;

    #@57
    .line 136
    const/4 v5, 0x6

    #@58
    aget-object v5, v1, v5

    #@5a
    iput-object v5, v0, Lcom/android/internal/net/VpnProfile;->searchDomains:Ljava/lang/String;

    #@5c
    .line 137
    const/4 v5, 0x7

    #@5d
    aget-object v5, v1, v5

    #@5f
    iput-object v5, v0, Lcom/android/internal/net/VpnProfile;->routes:Ljava/lang/String;

    #@61
    .line 138
    const/16 v5, 0x8

    #@63
    aget-object v5, v1, v5

    #@65
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    #@68
    move-result-object v5

    #@69
    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    #@6c
    move-result v5

    #@6d
    iput-boolean v5, v0, Lcom/android/internal/net/VpnProfile;->mppe:Z

    #@6f
    .line 139
    const/16 v5, 0x9

    #@71
    aget-object v5, v1, v5

    #@73
    iput-object v5, v0, Lcom/android/internal/net/VpnProfile;->l2tpSecret:Ljava/lang/String;

    #@75
    .line 140
    const/16 v5, 0xa

    #@77
    aget-object v5, v1, v5

    #@79
    iput-object v5, v0, Lcom/android/internal/net/VpnProfile;->ipsecIdentifier:Ljava/lang/String;

    #@7b
    .line 141
    const/16 v5, 0xb

    #@7d
    aget-object v5, v1, v5

    #@7f
    iput-object v5, v0, Lcom/android/internal/net/VpnProfile;->ipsecSecret:Ljava/lang/String;

    #@81
    .line 142
    const/16 v5, 0xc

    #@83
    aget-object v5, v1, v5

    #@85
    iput-object v5, v0, Lcom/android/internal/net/VpnProfile;->ipsecUserCert:Ljava/lang/String;

    #@87
    .line 143
    const/16 v5, 0xd

    #@89
    aget-object v5, v1, v5

    #@8b
    iput-object v5, v0, Lcom/android/internal/net/VpnProfile;->ipsecCaCert:Ljava/lang/String;

    #@8d
    .line 144
    array-length v5, v1

    #@8e
    if-le v5, v8, :cond_af

    #@90
    const/16 v5, 0xe

    #@92
    aget-object v5, v1, v5

    #@94
    :goto_94
    iput-object v5, v0, Lcom/android/internal/net/VpnProfile;->ipsecServerCert:Ljava/lang/String;

    #@96
    .line 146
    iget-object v5, v0, Lcom/android/internal/net/VpnProfile;->username:Ljava/lang/String;

    #@98
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    #@9b
    move-result v5

    #@9c
    if-eqz v5, :cond_a6

    #@9e
    iget-object v5, v0, Lcom/android/internal/net/VpnProfile;->password:Ljava/lang/String;

    #@a0
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    #@a3
    move-result v5

    #@a4
    if-nez v5, :cond_a7

    #@a6
    :cond_a6
    move v3, v4

    #@a7
    :cond_a7
    iput-boolean v3, v0, Lcom/android/internal/net/VpnProfile;->saveLogin:Z

    #@a9
    goto/16 :goto_9

    #@ab
    .line 148
    .end local v0           #profile:Lcom/android/internal/net/VpnProfile;
    .end local v1           #values:[Ljava/lang/String;
    :catch_ab
    move-exception v3

    #@ac
    move-object v0, v2

    #@ad
    .line 151
    goto/16 :goto_9

    #@af
    .line 144
    .restart local v0       #profile:Lcom/android/internal/net/VpnProfile;
    .restart local v1       #values:[Ljava/lang/String;
    :cond_af
    const-string v5, ""
    :try_end_b1
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_b1} :catch_ab

    #@b1
    goto :goto_94
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 213
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public encode()[B
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    #@3
    iget-object v1, p0, Lcom/android/internal/net/VpnProfile;->name:Ljava/lang/String;

    #@5
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@8
    .line 156
    .local v0, builder:Ljava/lang/StringBuilder;
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@b
    move-result-object v1

    #@c
    iget v2, p0, Lcom/android/internal/net/VpnProfile;->type:I

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    .line 157
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    iget-object v2, p0, Lcom/android/internal/net/VpnProfile;->server:Ljava/lang/String;

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    .line 158
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    iget-boolean v1, p0, Lcom/android/internal/net/VpnProfile;->saveLogin:Z

    #@20
    if-eqz v1, :cond_99

    #@22
    iget-object v1, p0, Lcom/android/internal/net/VpnProfile;->username:Ljava/lang/String;

    #@24
    :goto_24
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    .line 159
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    iget-boolean v1, p0, Lcom/android/internal/net/VpnProfile;->saveLogin:Z

    #@2d
    if-eqz v1, :cond_9c

    #@2f
    iget-object v1, p0, Lcom/android/internal/net/VpnProfile;->password:Ljava/lang/String;

    #@31
    :goto_31
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    .line 160
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    iget-object v2, p0, Lcom/android/internal/net/VpnProfile;->dnsServers:Ljava/lang/String;

    #@3a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    .line 161
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    iget-object v2, p0, Lcom/android/internal/net/VpnProfile;->searchDomains:Ljava/lang/String;

    #@43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    .line 162
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    iget-object v2, p0, Lcom/android/internal/net/VpnProfile;->routes:Ljava/lang/String;

    #@4c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    .line 163
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@52
    move-result-object v1

    #@53
    iget-boolean v2, p0, Lcom/android/internal/net/VpnProfile;->mppe:Z

    #@55
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@58
    .line 164
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v1

    #@5c
    iget-object v2, p0, Lcom/android/internal/net/VpnProfile;->l2tpSecret:Ljava/lang/String;

    #@5e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    .line 165
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@64
    move-result-object v1

    #@65
    iget-object v2, p0, Lcom/android/internal/net/VpnProfile;->ipsecIdentifier:Ljava/lang/String;

    #@67
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    .line 166
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v1

    #@6e
    iget-object v2, p0, Lcom/android/internal/net/VpnProfile;->ipsecSecret:Ljava/lang/String;

    #@70
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    .line 167
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@76
    move-result-object v1

    #@77
    iget-object v2, p0, Lcom/android/internal/net/VpnProfile;->ipsecUserCert:Ljava/lang/String;

    #@79
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    .line 168
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v1

    #@80
    iget-object v2, p0, Lcom/android/internal/net/VpnProfile;->ipsecCaCert:Ljava/lang/String;

    #@82
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    .line 169
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@88
    move-result-object v1

    #@89
    iget-object v2, p0, Lcom/android/internal/net/VpnProfile;->ipsecServerCert:Ljava/lang/String;

    #@8b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    .line 170
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@91
    move-result-object v1

    #@92
    sget-object v2, Ljava/nio/charset/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    #@94
    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    #@97
    move-result-object v1

    #@98
    return-object v1

    #@99
    .line 158
    :cond_99
    const-string v1, ""

    #@9b
    goto :goto_24

    #@9c
    .line 159
    :cond_9c
    const-string v1, ""

    #@9e
    goto :goto_31
.end method

.method public isValidLockdownProfile()Z
    .registers 9

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 180
    :try_start_1
    iget-object v6, p0, Lcom/android/internal/net/VpnProfile;->server:Ljava/lang/String;

    #@3
    invoke-static {v6}, Ljava/net/InetAddress;->parseNumericAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@6
    .line 182
    iget-object v6, p0, Lcom/android/internal/net/VpnProfile;->dnsServers:Ljava/lang/String;

    #@8
    const-string v7, " +"

    #@a
    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    #@f
    .local v4, len$:I
    const/4 v3, 0x0

    #@10
    .local v3, i$:I
    :goto_10
    if-ge v3, v4, :cond_1c

    #@12
    aget-object v1, v0, v3

    #@14
    .line 183
    .local v1, dnsServer:Ljava/lang/String;
    iget-object v6, p0, Lcom/android/internal/net/VpnProfile;->dnsServers:Ljava/lang/String;

    #@16
    invoke-static {v6}, Ljava/net/InetAddress;->parseNumericAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@19
    .line 182
    add-int/lit8 v3, v3, 0x1

    #@1b
    goto :goto_10

    #@1c
    .line 185
    .end local v1           #dnsServer:Ljava/lang/String;
    :cond_1c
    iget-object v6, p0, Lcom/android/internal/net/VpnProfile;->dnsServers:Ljava/lang/String;

    #@1e
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@21
    move-result v6

    #@22
    if-eqz v6, :cond_2c

    #@24
    .line 186
    const-string v6, "VpnProfile"

    #@26
    const-string v7, "DNS required"

    #@28
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_2b} :catch_2e

    #@2b
    .line 195
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #len$:I
    :goto_2b
    return v5

    #@2c
    .line 191
    .restart local v0       #arr$:[Ljava/lang/String;
    .restart local v3       #i$:I
    .restart local v4       #len$:I
    :cond_2c
    const/4 v5, 0x1

    #@2d
    goto :goto_2b

    #@2e
    .line 193
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #len$:I
    :catch_2e
    move-exception v2

    #@2f
    .line 194
    .local v2, e:Ljava/lang/IllegalArgumentException;
    const-string v6, "VpnProfile"

    #@31
    const-string v7, "Invalid address"

    #@33
    invoke-static {v6, v7, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@36
    goto :goto_2b
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 95
    iget-object v0, p0, Lcom/android/internal/net/VpnProfile;->key:Ljava/lang/String;

    #@4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@7
    .line 96
    iget-object v0, p0, Lcom/android/internal/net/VpnProfile;->name:Ljava/lang/String;

    #@9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@c
    .line 97
    iget v0, p0, Lcom/android/internal/net/VpnProfile;->type:I

    #@e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 98
    iget-object v0, p0, Lcom/android/internal/net/VpnProfile;->server:Ljava/lang/String;

    #@13
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@16
    .line 99
    iget-object v0, p0, Lcom/android/internal/net/VpnProfile;->username:Ljava/lang/String;

    #@18
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1b
    .line 100
    iget-object v0, p0, Lcom/android/internal/net/VpnProfile;->password:Ljava/lang/String;

    #@1d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@20
    .line 101
    iget-object v0, p0, Lcom/android/internal/net/VpnProfile;->dnsServers:Ljava/lang/String;

    #@22
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@25
    .line 102
    iget-object v0, p0, Lcom/android/internal/net/VpnProfile;->searchDomains:Ljava/lang/String;

    #@27
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2a
    .line 103
    iget-object v0, p0, Lcom/android/internal/net/VpnProfile;->routes:Ljava/lang/String;

    #@2c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2f
    .line 104
    iget-boolean v0, p0, Lcom/android/internal/net/VpnProfile;->mppe:Z

    #@31
    if-eqz v0, :cond_5d

    #@33
    move v0, v1

    #@34
    :goto_34
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@37
    .line 105
    iget-object v0, p0, Lcom/android/internal/net/VpnProfile;->l2tpSecret:Ljava/lang/String;

    #@39
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@3c
    .line 106
    iget-object v0, p0, Lcom/android/internal/net/VpnProfile;->ipsecIdentifier:Ljava/lang/String;

    #@3e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@41
    .line 107
    iget-object v0, p0, Lcom/android/internal/net/VpnProfile;->ipsecSecret:Ljava/lang/String;

    #@43
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@46
    .line 108
    iget-object v0, p0, Lcom/android/internal/net/VpnProfile;->ipsecUserCert:Ljava/lang/String;

    #@48
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@4b
    .line 109
    iget-object v0, p0, Lcom/android/internal/net/VpnProfile;->ipsecCaCert:Ljava/lang/String;

    #@4d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@50
    .line 110
    iget-object v0, p0, Lcom/android/internal/net/VpnProfile;->ipsecServerCert:Ljava/lang/String;

    #@52
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@55
    .line 111
    iget-boolean v0, p0, Lcom/android/internal/net/VpnProfile;->saveLogin:Z

    #@57
    if-eqz v0, :cond_5f

    #@59
    :goto_59
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@5c
    .line 112
    return-void

    #@5d
    :cond_5d
    move v0, v2

    #@5e
    .line 104
    goto :goto_34

    #@5f
    :cond_5f
    move v1, v2

    #@60
    .line 111
    goto :goto_59
.end method
