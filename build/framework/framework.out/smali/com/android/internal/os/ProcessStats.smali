.class public Lcom/android/internal/os/ProcessStats;
.super Ljava/lang/Object;
.source "ProcessStats.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/os/ProcessStats$Stats;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final LOAD_AVERAGE_FORMAT:[I = null

.field private static final PROCESS_FULL_STATS_FORMAT:[I = null

.field static final PROCESS_FULL_STAT_MAJOR_FAULTS:I = 0x2

.field static final PROCESS_FULL_STAT_MINOR_FAULTS:I = 0x1

.field static final PROCESS_FULL_STAT_STIME:I = 0x4

.field static final PROCESS_FULL_STAT_UTIME:I = 0x3

.field static final PROCESS_FULL_STAT_VSIZE:I = 0x5

.field private static final PROCESS_STATS_FORMAT:[I = null

.field static final PROCESS_STAT_MAJOR_FAULTS:I = 0x1

.field static final PROCESS_STAT_MINOR_FAULTS:I = 0x0

.field static final PROCESS_STAT_STIME:I = 0x3

.field static final PROCESS_STAT_UTIME:I = 0x2

.field private static final SYSTEM_CPU_FORMAT:[I = null

.field private static final TAG:Ljava/lang/String; = "ProcessStats"

.field private static final localLOGV:Z

.field private static final sLoadComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/android/internal/os/ProcessStats$Stats;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mBaseIdleTime:J

.field private mBaseIoWaitTime:J

.field private mBaseIrqTime:J

.field private mBaseSoftIrqTime:J

.field private mBaseSystemTime:J

.field private mBaseUserTime:J

.field private mBuffer:[B

.field private mCpuSpeedTimes:[J

.field private mCpuSpeeds:[J

.field private mCurPids:[I

.field private mCurThreadPids:[I

.field private mCurrentSampleRealTime:J

.field private mCurrentSampleTime:J

.field private mFirst:Z

.field private final mIncludeThreads:Z

.field private mLastSampleRealTime:J

.field private mLastSampleTime:J

.field private mLoad1:F

.field private mLoad15:F

.field private mLoad5:F

.field private final mLoadAverageData:[F

.field private final mProcStats:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/os/ProcessStats$Stats;",
            ">;"
        }
    .end annotation
.end field

.field private final mProcessFullStatsData:[J

.field private final mProcessFullStatsStringData:[Ljava/lang/String;

.field private final mProcessStatsData:[J

.field private mRelCpuSpeedTimes:[J

.field private mRelIdleTime:I

.field private mRelIoWaitTime:I

.field private mRelIrqTime:I

.field private mRelSoftIrqTime:I

.field private mRelSystemTime:I

.field private mRelUserTime:I

.field private final mSinglePidStatsData:[J

.field private final mSystemCpuData:[J

.field private final mWorkingProcs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/os/ProcessStats$Stats;",
            ">;"
        }
    .end annotation
.end field

.field private mWorkingProcsSorted:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 40
    const/16 v0, 0xf

    #@2
    new-array v0, v0, [I

    #@4
    fill-array-data v0, :array_2c

    #@7
    sput-object v0, Lcom/android/internal/os/ProcessStats;->PROCESS_STATS_FORMAT:[I

    #@9
    .line 68
    const/16 v0, 0x16

    #@b
    new-array v0, v0, [I

    #@d
    fill-array-data v0, :array_4e

    #@10
    sput-object v0, Lcom/android/internal/os/ProcessStats;->PROCESS_FULL_STATS_FORMAT:[I

    #@12
    .line 102
    const/16 v0, 0x8

    #@14
    new-array v0, v0, [I

    #@16
    fill-array-data v0, :array_7e

    #@19
    sput-object v0, Lcom/android/internal/os/ProcessStats;->SYSTEM_CPU_FORMAT:[I

    #@1b
    .line 115
    const/4 v0, 0x3

    #@1c
    new-array v0, v0, [I

    #@1e
    fill-array-data v0, :array_92

    #@21
    sput-object v0, Lcom/android/internal/os/ProcessStats;->LOAD_AVERAGE_FORMAT:[I

    #@23
    .line 234
    new-instance v0, Lcom/android/internal/os/ProcessStats$1;

    #@25
    invoke-direct {v0}, Lcom/android/internal/os/ProcessStats$1;-><init>()V

    #@28
    sput-object v0, Lcom/android/internal/os/ProcessStats;->sLoadComparator:Ljava/util/Comparator;

    #@2a
    return-void

    #@2b
    .line 40
    nop

    #@2c
    :array_2c
    .array-data 0x4
        0x20t 0x0t 0x0t 0x0t
        0x20t 0x2t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x20t 0x20t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x20t 0x20t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x20t 0x20t 0x0t 0x0t
        0x20t 0x20t 0x0t 0x0t
    .end array-data

    #@4e
    .line 68
    :array_4e
    .array-data 0x4
        0x20t 0x0t 0x0t 0x0t
        0x20t 0x12t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x20t 0x20t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x20t 0x20t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x20t 0x20t 0x0t 0x0t
        0x20t 0x20t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x20t 0x20t 0x0t 0x0t
    .end array-data

    #@7e
    .line 102
    :array_7e
    .array-data 0x4
        0x20t 0x1t 0x0t 0x0t
        0x20t 0x20t 0x0t 0x0t
        0x20t 0x20t 0x0t 0x0t
        0x20t 0x20t 0x0t 0x0t
        0x20t 0x20t 0x0t 0x0t
        0x20t 0x20t 0x0t 0x0t
        0x20t 0x20t 0x0t 0x0t
        0x20t 0x20t 0x0t 0x0t
    .end array-data

    #@92
    .line 115
    :array_92
    .array-data 0x4
        0x20t 0x40t 0x0t 0x0t
        0x20t 0x40t 0x0t 0x0t
        0x20t 0x40t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>(Z)V
    .registers 6
    .parameter "includeThreads"

    #@0
    .prologue
    const/4 v3, 0x6

    #@1
    const/4 v2, 0x4

    #@2
    const/4 v1, 0x0

    #@3
    .line 253
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 64
    new-array v0, v2, [J

    #@8
    iput-object v0, p0, Lcom/android/internal/os/ProcessStats;->mProcessStatsData:[J

    #@a
    .line 66
    new-array v0, v2, [J

    #@c
    iput-object v0, p0, Lcom/android/internal/os/ProcessStats;->mSinglePidStatsData:[J

    #@e
    .line 99
    new-array v0, v3, [Ljava/lang/String;

    #@10
    iput-object v0, p0, Lcom/android/internal/os/ProcessStats;->mProcessFullStatsStringData:[Ljava/lang/String;

    #@12
    .line 100
    new-array v0, v3, [J

    #@14
    iput-object v0, p0, Lcom/android/internal/os/ProcessStats;->mProcessFullStatsData:[J

    #@16
    .line 113
    const/4 v0, 0x7

    #@17
    new-array v0, v0, [J

    #@19
    iput-object v0, p0, Lcom/android/internal/os/ProcessStats;->mSystemCpuData:[J

    #@1b
    .line 121
    const/4 v0, 0x3

    #@1c
    new-array v0, v0, [F

    #@1e
    iput-object v0, p0, Lcom/android/internal/os/ProcessStats;->mLoadAverageData:[F

    #@20
    .line 125
    iput v1, p0, Lcom/android/internal/os/ProcessStats;->mLoad1:F

    #@22
    .line 126
    iput v1, p0, Lcom/android/internal/os/ProcessStats;->mLoad5:F

    #@24
    .line 127
    iput v1, p0, Lcom/android/internal/os/ProcessStats;->mLoad15:F

    #@26
    .line 151
    new-instance v0, Ljava/util/ArrayList;

    #@28
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@2b
    iput-object v0, p0, Lcom/android/internal/os/ProcessStats;->mProcStats:Ljava/util/ArrayList;

    #@2d
    .line 152
    new-instance v0, Ljava/util/ArrayList;

    #@2f
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@32
    iput-object v0, p0, Lcom/android/internal/os/ProcessStats;->mWorkingProcs:Ljava/util/ArrayList;

    #@34
    .line 155
    const/4 v0, 0x1

    #@35
    iput-boolean v0, p0, Lcom/android/internal/os/ProcessStats;->mFirst:Z

    #@37
    .line 157
    const/16 v0, 0x1000

    #@39
    new-array v0, v0, [B

    #@3b
    iput-object v0, p0, Lcom/android/internal/os/ProcessStats;->mBuffer:[B

    #@3d
    .line 254
    iput-boolean p1, p0, Lcom/android/internal/os/ProcessStats;->mIncludeThreads:Z

    #@3f
    .line 255
    return-void
.end method

.method private collectStats(Ljava/lang/String;IZ[ILjava/util/ArrayList;)[I
    .registers 33
    .parameter "statsFile"
    .parameter "parentPid"
    .parameter "first"
    .parameter "curPids"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IZ[I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/os/ProcessStats$Stats;",
            ">;)[I"
        }
    .end annotation

    #@0
    .prologue
    .line 341
    .local p5, allProcs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/os/ProcessStats$Stats;>;"
    move-object/from16 v0, p1

    #@2
    move-object/from16 v1, p4

    #@4
    invoke-static {v0, v1}, Landroid/os/Process;->getPids(Ljava/lang/String;[I)[I

    #@7
    move-result-object v17

    #@8
    .line 342
    .local v17, pids:[I
    if-nez v17, :cond_48

    #@a
    const/4 v9, 0x0

    #@b
    .line 343
    .local v9, NP:I
    :goto_b
    invoke-virtual/range {p5 .. p5}, Ljava/util/ArrayList;->size()I

    #@e
    move-result v10

    #@f
    .line 344
    .local v10, NS:I
    const/4 v11, 0x0

    #@10
    .line 345
    .local v11, curStatsIndex:I
    const/4 v12, 0x0

    #@11
    .local v12, i:I
    :goto_11
    if-ge v12, v9, :cond_18

    #@13
    .line 346
    aget v5, v17, v12

    #@15
    .line 347
    .local v5, pid:I
    if-gez v5, :cond_4c

    #@17
    .line 348
    move v9, v5

    #@18
    .line 504
    .end local v5           #pid:I
    :cond_18
    :goto_18
    if-ge v11, v10, :cond_29c

    #@1a
    .line 506
    move-object/from16 v0, p5

    #@1c
    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1f
    move-result-object v20

    #@20
    check-cast v20, Lcom/android/internal/os/ProcessStats$Stats;

    #@22
    .line 507
    .local v20, st:Lcom/android/internal/os/ProcessStats$Stats;
    const/4 v3, 0x0

    #@23
    move-object/from16 v0, v20

    #@25
    iput v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->rel_utime:I

    #@27
    .line 508
    const/4 v3, 0x0

    #@28
    move-object/from16 v0, v20

    #@2a
    iput v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->rel_stime:I

    #@2c
    .line 509
    const/4 v3, 0x0

    #@2d
    move-object/from16 v0, v20

    #@2f
    iput v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->rel_minfaults:I

    #@31
    .line 510
    const/4 v3, 0x0

    #@32
    move-object/from16 v0, v20

    #@34
    iput v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->rel_majfaults:I

    #@36
    .line 511
    const/4 v3, 0x1

    #@37
    move-object/from16 v0, v20

    #@39
    iput-boolean v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->removed:Z

    #@3b
    .line 512
    const/4 v3, 0x1

    #@3c
    move-object/from16 v0, v20

    #@3e
    iput-boolean v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->working:Z

    #@40
    .line 513
    move-object/from16 v0, p5

    #@42
    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@45
    .line 514
    add-int/lit8 v10, v10, -0x1

    #@47
    .line 516
    goto :goto_18

    #@48
    .line 342
    .end local v9           #NP:I
    .end local v10           #NS:I
    .end local v11           #curStatsIndex:I
    .end local v12           #i:I
    .end local v20           #st:Lcom/android/internal/os/ProcessStats$Stats;
    :cond_48
    move-object/from16 v0, v17

    #@4a
    array-length v9, v0

    #@4b
    goto :goto_b

    #@4c
    .line 351
    .restart local v5       #pid:I
    .restart local v9       #NP:I
    .restart local v10       #NS:I
    .restart local v11       #curStatsIndex:I
    .restart local v12       #i:I
    :cond_4c
    if-ge v11, v10, :cond_94

    #@4e
    move-object/from16 v0, p5

    #@50
    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@53
    move-result-object v3

    #@54
    check-cast v3, Lcom/android/internal/os/ProcessStats$Stats;

    #@56
    move-object/from16 v20, v3

    #@58
    .line 353
    .restart local v20       #st:Lcom/android/internal/os/ProcessStats$Stats;
    :goto_58
    if-eqz v20, :cond_15f

    #@5a
    move-object/from16 v0, v20

    #@5c
    iget v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->pid:I

    #@5e
    if-ne v3, v5, :cond_15f

    #@60
    .line 355
    const/4 v3, 0x0

    #@61
    move-object/from16 v0, v20

    #@63
    iput-boolean v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->added:Z

    #@65
    .line 356
    const/4 v3, 0x0

    #@66
    move-object/from16 v0, v20

    #@68
    iput-boolean v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->working:Z

    #@6a
    .line 357
    add-int/lit8 v11, v11, 0x1

    #@6c
    .line 362
    move-object/from16 v0, v20

    #@6e
    iget-boolean v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->interesting:Z

    #@70
    if-eqz v3, :cond_90

    #@72
    .line 363
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@75
    move-result-wide v23

    #@76
    .line 365
    .local v23, uptime:J
    move-object/from16 v0, p0

    #@78
    iget-object v0, v0, Lcom/android/internal/os/ProcessStats;->mProcessStatsData:[J

    #@7a
    move-object/from16 v18, v0

    #@7c
    .line 366
    .local v18, procStats:[J
    move-object/from16 v0, v20

    #@7e
    iget-object v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->statFile:Ljava/lang/String;

    #@80
    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    #@83
    move-result-object v3

    #@84
    sget-object v4, Lcom/android/internal/os/ProcessStats;->PROCESS_STATS_FORMAT:[I

    #@86
    const/4 v6, 0x0

    #@87
    const/4 v7, 0x0

    #@88
    move-object/from16 v0, v18

    #@8a
    invoke-static {v3, v4, v6, v0, v7}, Landroid/os/Process;->readProcFile(Ljava/lang/String;[I[Ljava/lang/String;[J[F)Z

    #@8d
    move-result v3

    #@8e
    if-nez v3, :cond_97

    #@90
    .line 345
    .end local v18           #procStats:[J
    .end local v23           #uptime:J
    :cond_90
    :goto_90
    add-int/lit8 v12, v12, 0x1

    #@92
    goto/16 :goto_11

    #@94
    .line 351
    .end local v20           #st:Lcom/android/internal/os/ProcessStats$Stats;
    :cond_94
    const/16 v20, 0x0

    #@96
    goto :goto_58

    #@97
    .line 371
    .restart local v18       #procStats:[J
    .restart local v20       #st:Lcom/android/internal/os/ProcessStats$Stats;
    .restart local v23       #uptime:J
    :cond_97
    const/4 v3, 0x0

    #@98
    aget-wide v15, v18, v3

    #@9a
    .line 372
    .local v15, minfaults:J
    const/4 v3, 0x1

    #@9b
    aget-wide v13, v18, v3

    #@9d
    .line 373
    .local v13, majfaults:J
    const/4 v3, 0x2

    #@9e
    aget-wide v25, v18, v3

    #@a0
    .line 374
    .local v25, utime:J
    const/4 v3, 0x3

    #@a1
    aget-wide v21, v18, v3

    #@a3
    .line 376
    .local v21, stime:J
    move-object/from16 v0, v20

    #@a5
    iget-wide v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->base_utime:J

    #@a7
    cmp-long v3, v25, v3

    #@a9
    if-nez v3, :cond_d3

    #@ab
    move-object/from16 v0, v20

    #@ad
    iget-wide v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->base_stime:J

    #@af
    cmp-long v3, v21, v3

    #@b1
    if-nez v3, :cond_d3

    #@b3
    .line 377
    const/4 v3, 0x0

    #@b4
    move-object/from16 v0, v20

    #@b6
    iput v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->rel_utime:I

    #@b8
    .line 378
    const/4 v3, 0x0

    #@b9
    move-object/from16 v0, v20

    #@bb
    iput v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->rel_stime:I

    #@bd
    .line 379
    const/4 v3, 0x0

    #@be
    move-object/from16 v0, v20

    #@c0
    iput v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->rel_minfaults:I

    #@c2
    .line 380
    const/4 v3, 0x0

    #@c3
    move-object/from16 v0, v20

    #@c5
    iput v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->rel_majfaults:I

    #@c7
    .line 381
    move-object/from16 v0, v20

    #@c9
    iget-boolean v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->active:Z

    #@cb
    if-eqz v3, :cond_90

    #@cd
    .line 382
    const/4 v3, 0x0

    #@ce
    move-object/from16 v0, v20

    #@d0
    iput-boolean v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->active:Z

    #@d2
    goto :goto_90

    #@d3
    .line 387
    :cond_d3
    move-object/from16 v0, v20

    #@d5
    iget-boolean v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->active:Z

    #@d7
    if-nez v3, :cond_de

    #@d9
    .line 388
    const/4 v3, 0x1

    #@da
    move-object/from16 v0, v20

    #@dc
    iput-boolean v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->active:Z

    #@de
    .line 391
    :cond_de
    if-gez p2, :cond_108

    #@e0
    .line 392
    move-object/from16 v0, v20

    #@e2
    iget-object v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->cmdlineFile:Ljava/lang/String;

    #@e4
    move-object/from16 v0, p0

    #@e6
    move-object/from16 v1, v20

    #@e8
    invoke-direct {v0, v1, v3}, Lcom/android/internal/os/ProcessStats;->getName(Lcom/android/internal/os/ProcessStats$Stats;Ljava/lang/String;)V

    #@eb
    .line 393
    move-object/from16 v0, v20

    #@ed
    iget-object v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->threadStats:Ljava/util/ArrayList;

    #@ef
    if-eqz v3, :cond_108

    #@f1
    .line 394
    move-object/from16 v0, v20

    #@f3
    iget-object v4, v0, Lcom/android/internal/os/ProcessStats$Stats;->threadsDir:Ljava/lang/String;

    #@f5
    const/4 v6, 0x0

    #@f6
    move-object/from16 v0, p0

    #@f8
    iget-object v7, v0, Lcom/android/internal/os/ProcessStats;->mCurThreadPids:[I

    #@fa
    move-object/from16 v0, v20

    #@fc
    iget-object v8, v0, Lcom/android/internal/os/ProcessStats$Stats;->threadStats:Ljava/util/ArrayList;

    #@fe
    move-object/from16 v3, p0

    #@100
    invoke-direct/range {v3 .. v8}, Lcom/android/internal/os/ProcessStats;->collectStats(Ljava/lang/String;IZ[ILjava/util/ArrayList;)[I

    #@103
    move-result-object v3

    #@104
    move-object/from16 v0, p0

    #@106
    iput-object v3, v0, Lcom/android/internal/os/ProcessStats;->mCurThreadPids:[I

    #@108
    .line 405
    :cond_108
    move-object/from16 v0, v20

    #@10a
    iget-wide v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->base_uptime:J

    #@10c
    sub-long v3, v23, v3

    #@10e
    move-object/from16 v0, v20

    #@110
    iput-wide v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->rel_uptime:J

    #@112
    .line 406
    move-wide/from16 v0, v23

    #@114
    move-object/from16 v2, v20

    #@116
    iput-wide v0, v2, Lcom/android/internal/os/ProcessStats$Stats;->base_uptime:J

    #@118
    .line 407
    move-object/from16 v0, v20

    #@11a
    iget-wide v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->base_utime:J

    #@11c
    sub-long v3, v25, v3

    #@11e
    long-to-int v3, v3

    #@11f
    move-object/from16 v0, v20

    #@121
    iput v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->rel_utime:I

    #@123
    .line 408
    move-object/from16 v0, v20

    #@125
    iget-wide v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->base_stime:J

    #@127
    sub-long v3, v21, v3

    #@129
    long-to-int v3, v3

    #@12a
    move-object/from16 v0, v20

    #@12c
    iput v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->rel_stime:I

    #@12e
    .line 409
    move-wide/from16 v0, v25

    #@130
    move-object/from16 v2, v20

    #@132
    iput-wide v0, v2, Lcom/android/internal/os/ProcessStats$Stats;->base_utime:J

    #@134
    .line 410
    move-wide/from16 v0, v21

    #@136
    move-object/from16 v2, v20

    #@138
    iput-wide v0, v2, Lcom/android/internal/os/ProcessStats$Stats;->base_stime:J

    #@13a
    .line 411
    move-object/from16 v0, v20

    #@13c
    iget-wide v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->base_minfaults:J

    #@13e
    sub-long v3, v15, v3

    #@140
    long-to-int v3, v3

    #@141
    move-object/from16 v0, v20

    #@143
    iput v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->rel_minfaults:I

    #@145
    .line 412
    move-object/from16 v0, v20

    #@147
    iget-wide v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->base_majfaults:J

    #@149
    sub-long v3, v13, v3

    #@14b
    long-to-int v3, v3

    #@14c
    move-object/from16 v0, v20

    #@14e
    iput v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->rel_majfaults:I

    #@150
    .line 413
    move-object/from16 v0, v20

    #@152
    iput-wide v15, v0, Lcom/android/internal/os/ProcessStats$Stats;->base_minfaults:J

    #@154
    .line 414
    move-object/from16 v0, v20

    #@156
    iput-wide v13, v0, Lcom/android/internal/os/ProcessStats$Stats;->base_majfaults:J

    #@158
    .line 415
    const/4 v3, 0x1

    #@159
    move-object/from16 v0, v20

    #@15b
    iput-boolean v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->working:Z

    #@15d
    goto/16 :goto_90

    #@15f
    .line 421
    .end local v13           #majfaults:J
    .end local v15           #minfaults:J
    .end local v18           #procStats:[J
    .end local v21           #stime:J
    .end local v23           #uptime:J
    .end local v25           #utime:J
    :cond_15f
    if-eqz v20, :cond_167

    #@161
    move-object/from16 v0, v20

    #@163
    iget v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->pid:I

    #@165
    if-le v3, v5, :cond_273

    #@167
    .line 423
    :cond_167
    new-instance v20, Lcom/android/internal/os/ProcessStats$Stats;

    #@169
    .end local v20           #st:Lcom/android/internal/os/ProcessStats$Stats;
    move-object/from16 v0, p0

    #@16b
    iget-boolean v3, v0, Lcom/android/internal/os/ProcessStats;->mIncludeThreads:Z

    #@16d
    move-object/from16 v0, v20

    #@16f
    move/from16 v1, p2

    #@171
    invoke-direct {v0, v5, v1, v3}, Lcom/android/internal/os/ProcessStats$Stats;-><init>(IIZ)V

    #@174
    .line 424
    .restart local v20       #st:Lcom/android/internal/os/ProcessStats$Stats;
    move-object/from16 v0, p5

    #@176
    move-object/from16 v1, v20

    #@178
    invoke-virtual {v0, v11, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@17b
    .line 425
    add-int/lit8 v11, v11, 0x1

    #@17d
    .line 426
    add-int/lit8 v10, v10, 0x1

    #@17f
    .line 431
    move-object/from16 v0, p0

    #@181
    iget-object v0, v0, Lcom/android/internal/os/ProcessStats;->mProcessFullStatsStringData:[Ljava/lang/String;

    #@183
    move-object/from16 v19, v0

    #@185
    .line 432
    .local v19, procStatsString:[Ljava/lang/String;
    move-object/from16 v0, p0

    #@187
    iget-object v0, v0, Lcom/android/internal/os/ProcessStats;->mProcessFullStatsData:[J

    #@189
    move-object/from16 v18, v0

    #@18b
    .line 433
    .restart local v18       #procStats:[J
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@18e
    move-result-wide v3

    #@18f
    move-object/from16 v0, v20

    #@191
    iput-wide v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->base_uptime:J

    #@193
    .line 434
    move-object/from16 v0, v20

    #@195
    iget-object v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->statFile:Ljava/lang/String;

    #@197
    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    #@19a
    move-result-object v3

    #@19b
    sget-object v4, Lcom/android/internal/os/ProcessStats;->PROCESS_FULL_STATS_FORMAT:[I

    #@19d
    const/4 v6, 0x0

    #@19e
    move-object/from16 v0, v19

    #@1a0
    move-object/from16 v1, v18

    #@1a2
    invoke-static {v3, v4, v0, v1, v6}, Landroid/os/Process;->readProcFile(Ljava/lang/String;[I[Ljava/lang/String;[J[F)Z

    #@1a5
    move-result v3

    #@1a6
    if-eqz v3, :cond_222

    #@1a8
    .line 442
    const/4 v3, 0x1

    #@1a9
    move-object/from16 v0, v20

    #@1ab
    iput-boolean v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->interesting:Z

    #@1ad
    .line 443
    const/4 v3, 0x0

    #@1ae
    aget-object v3, v19, v3

    #@1b0
    move-object/from16 v0, v20

    #@1b2
    iput-object v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->baseName:Ljava/lang/String;

    #@1b4
    .line 444
    const/4 v3, 0x1

    #@1b5
    aget-wide v3, v18, v3

    #@1b7
    move-object/from16 v0, v20

    #@1b9
    iput-wide v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->base_minfaults:J

    #@1bb
    .line 445
    const/4 v3, 0x2

    #@1bc
    aget-wide v3, v18, v3

    #@1be
    move-object/from16 v0, v20

    #@1c0
    iput-wide v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->base_majfaults:J

    #@1c2
    .line 446
    const/4 v3, 0x3

    #@1c3
    aget-wide v3, v18, v3

    #@1c5
    move-object/from16 v0, v20

    #@1c7
    iput-wide v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->base_utime:J

    #@1c9
    .line 447
    const/4 v3, 0x4

    #@1ca
    aget-wide v3, v18, v3

    #@1cc
    move-object/from16 v0, v20

    #@1ce
    iput-wide v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->base_stime:J

    #@1d0
    .line 460
    :goto_1d0
    if-gez p2, :cond_256

    #@1d2
    .line 461
    move-object/from16 v0, v20

    #@1d4
    iget-object v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->cmdlineFile:Ljava/lang/String;

    #@1d6
    move-object/from16 v0, p0

    #@1d8
    move-object/from16 v1, v20

    #@1da
    invoke-direct {v0, v1, v3}, Lcom/android/internal/os/ProcessStats;->getName(Lcom/android/internal/os/ProcessStats$Stats;Ljava/lang/String;)V

    #@1dd
    .line 462
    move-object/from16 v0, v20

    #@1df
    iget-object v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->threadStats:Ljava/util/ArrayList;

    #@1e1
    if-eqz v3, :cond_1fa

    #@1e3
    .line 463
    move-object/from16 v0, v20

    #@1e5
    iget-object v4, v0, Lcom/android/internal/os/ProcessStats$Stats;->threadsDir:Ljava/lang/String;

    #@1e7
    const/4 v6, 0x1

    #@1e8
    move-object/from16 v0, p0

    #@1ea
    iget-object v7, v0, Lcom/android/internal/os/ProcessStats;->mCurThreadPids:[I

    #@1ec
    move-object/from16 v0, v20

    #@1ee
    iget-object v8, v0, Lcom/android/internal/os/ProcessStats$Stats;->threadStats:Ljava/util/ArrayList;

    #@1f0
    move-object/from16 v3, p0

    #@1f2
    invoke-direct/range {v3 .. v8}, Lcom/android/internal/os/ProcessStats;->collectStats(Ljava/lang/String;IZ[ILjava/util/ArrayList;)[I

    #@1f5
    move-result-object v3

    #@1f6
    move-object/from16 v0, p0

    #@1f8
    iput-object v3, v0, Lcom/android/internal/os/ProcessStats;->mCurThreadPids:[I

    #@1fa
    .line 475
    :cond_1fa
    :goto_1fa
    const/4 v3, 0x0

    #@1fb
    move-object/from16 v0, v20

    #@1fd
    iput v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->rel_utime:I

    #@1ff
    .line 476
    const/4 v3, 0x0

    #@200
    move-object/from16 v0, v20

    #@202
    iput v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->rel_stime:I

    #@204
    .line 477
    const/4 v3, 0x0

    #@205
    move-object/from16 v0, v20

    #@207
    iput v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->rel_minfaults:I

    #@209
    .line 478
    const/4 v3, 0x0

    #@20a
    move-object/from16 v0, v20

    #@20c
    iput v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->rel_majfaults:I

    #@20e
    .line 479
    const/4 v3, 0x1

    #@20f
    move-object/from16 v0, v20

    #@211
    iput-boolean v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->added:Z

    #@213
    .line 480
    if-nez p3, :cond_90

    #@215
    move-object/from16 v0, v20

    #@217
    iget-boolean v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->interesting:Z

    #@219
    if-eqz v3, :cond_90

    #@21b
    .line 481
    const/4 v3, 0x1

    #@21c
    move-object/from16 v0, v20

    #@21e
    iput-boolean v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->working:Z

    #@220
    goto/16 :goto_90

    #@222
    .line 454
    :cond_222
    const-string v3, "ProcessStats"

    #@224
    new-instance v4, Ljava/lang/StringBuilder;

    #@226
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@229
    const-string v6, "Skipping unknown process pid "

    #@22b
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22e
    move-result-object v4

    #@22f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@232
    move-result-object v4

    #@233
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@236
    move-result-object v4

    #@237
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@23a
    .line 455
    const-string v3, "<unknown>"

    #@23c
    move-object/from16 v0, v20

    #@23e
    iput-object v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->baseName:Ljava/lang/String;

    #@240
    .line 456
    const-wide/16 v3, 0x0

    #@242
    move-object/from16 v0, v20

    #@244
    iput-wide v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->base_stime:J

    #@246
    move-object/from16 v0, v20

    #@248
    iput-wide v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->base_utime:J

    #@24a
    .line 457
    const-wide/16 v3, 0x0

    #@24c
    move-object/from16 v0, v20

    #@24e
    iput-wide v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->base_majfaults:J

    #@250
    move-object/from16 v0, v20

    #@252
    iput-wide v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->base_minfaults:J

    #@254
    goto/16 :goto_1d0

    #@256
    .line 466
    :cond_256
    move-object/from16 v0, v20

    #@258
    iget-boolean v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->interesting:Z

    #@25a
    if-eqz v3, :cond_1fa

    #@25c
    .line 467
    move-object/from16 v0, v20

    #@25e
    iget-object v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->baseName:Ljava/lang/String;

    #@260
    move-object/from16 v0, v20

    #@262
    iput-object v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->name:Ljava/lang/String;

    #@264
    .line 468
    move-object/from16 v0, v20

    #@266
    iget-object v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->name:Ljava/lang/String;

    #@268
    move-object/from16 v0, p0

    #@26a
    invoke-virtual {v0, v3}, Lcom/android/internal/os/ProcessStats;->onMeasureProcessName(Ljava/lang/String;)I

    #@26d
    move-result v3

    #@26e
    move-object/from16 v0, v20

    #@270
    iput v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->nameWidth:I

    #@272
    goto :goto_1fa

    #@273
    .line 487
    .end local v18           #procStats:[J
    .end local v19           #procStatsString:[Ljava/lang/String;
    :cond_273
    const/4 v3, 0x0

    #@274
    move-object/from16 v0, v20

    #@276
    iput v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->rel_utime:I

    #@278
    .line 488
    const/4 v3, 0x0

    #@279
    move-object/from16 v0, v20

    #@27b
    iput v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->rel_stime:I

    #@27d
    .line 489
    const/4 v3, 0x0

    #@27e
    move-object/from16 v0, v20

    #@280
    iput v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->rel_minfaults:I

    #@282
    .line 490
    const/4 v3, 0x0

    #@283
    move-object/from16 v0, v20

    #@285
    iput v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->rel_majfaults:I

    #@287
    .line 491
    const/4 v3, 0x1

    #@288
    move-object/from16 v0, v20

    #@28a
    iput-boolean v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->removed:Z

    #@28c
    .line 492
    const/4 v3, 0x1

    #@28d
    move-object/from16 v0, v20

    #@28f
    iput-boolean v3, v0, Lcom/android/internal/os/ProcessStats$Stats;->working:Z

    #@291
    .line 493
    move-object/from16 v0, p5

    #@293
    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@296
    .line 494
    add-int/lit8 v10, v10, -0x1

    #@298
    .line 500
    add-int/lit8 v12, v12, -0x1

    #@29a
    .line 501
    goto/16 :goto_90

    #@29c
    .line 518
    .end local v5           #pid:I
    .end local v20           #st:Lcom/android/internal/os/ProcessStats$Stats;
    :cond_29c
    return-object v17
.end method

.method private getCpuSpeedTimes([J)[J
    .registers 16
    .parameter "out"

    #@0
    .prologue
    const/16 v13, 0x3c

    #@2
    const/4 v12, 0x0

    #@3
    .line 557
    move-object v6, p1

    #@4
    .line 558
    .local v6, tempTimes:[J
    iget-object v5, p0, Lcom/android/internal/os/ProcessStats;->mCpuSpeeds:[J

    #@6
    .line 559
    .local v5, tempSpeeds:[J
    const/16 v0, 0x3c

    #@8
    .line 560
    .local v0, MAX_SPEEDS:I
    if-nez p1, :cond_e

    #@a
    .line 561
    new-array v6, v13, [J

    #@c
    .line 562
    new-array v5, v13, [J

    #@e
    .line 564
    :cond_e
    const/4 v3, 0x0

    #@f
    .line 565
    .local v3, speed:I
    const-string v10, "/sys/devices/system/cpu/cpu0/cpufreq/stats/time_in_state"

    #@11
    invoke-direct {p0, v10, v12}, Lcom/android/internal/os/ProcessStats;->readFile(Ljava/lang/String;C)Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    .line 567
    .local v1, file:Ljava/lang/String;
    if-eqz v1, :cond_3c

    #@17
    .line 568
    new-instance v4, Ljava/util/StringTokenizer;

    #@19
    const-string v10, "\n "

    #@1b
    invoke-direct {v4, v1, v10}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@1e
    .line 569
    .local v4, st:Ljava/util/StringTokenizer;
    :cond_1e
    :goto_1e
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    #@21
    move-result v10

    #@22
    if-eqz v10, :cond_3c

    #@24
    .line 570
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@27
    move-result-object v7

    #@28
    .line 572
    .local v7, token:Ljava/lang/String;
    :try_start_28
    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@2b
    move-result-wide v8

    #@2c
    .line 573
    .local v8, val:J
    aput-wide v8, v5, v3

    #@2e
    .line 574
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@31
    move-result-object v7

    #@32
    .line 575
    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@35
    move-result-wide v8

    #@36
    .line 576
    aput-wide v8, v6, v3
    :try_end_38
    .catch Ljava/lang/NumberFormatException; {:try_start_28 .. :try_end_38} :catch_4d

    #@38
    .line 577
    add-int/lit8 v3, v3, 0x1

    #@3a
    .line 578
    if-ne v3, v13, :cond_1e

    #@3c
    .line 588
    .end local v4           #st:Ljava/util/StringTokenizer;
    .end local v7           #token:Ljava/lang/String;
    .end local v8           #val:J
    :cond_3c
    if-nez p1, :cond_4c

    #@3e
    .line 589
    new-array p1, v3, [J

    #@40
    .line 590
    new-array v10, v3, [J

    #@42
    iput-object v10, p0, Lcom/android/internal/os/ProcessStats;->mCpuSpeeds:[J

    #@44
    .line 591
    iget-object v10, p0, Lcom/android/internal/os/ProcessStats;->mCpuSpeeds:[J

    #@46
    invoke-static {v5, v12, v10, v12, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@49
    .line 592
    invoke-static {v6, v12, p1, v12, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@4c
    .line 594
    :cond_4c
    return-object p1

    #@4d
    .line 583
    .restart local v4       #st:Ljava/util/StringTokenizer;
    .restart local v7       #token:Ljava/lang/String;
    :catch_4d
    move-exception v2

    #@4e
    .line 584
    .local v2, nfe:Ljava/lang/NumberFormatException;
    const-string v10, "ProcessStats"

    #@50
    const-string v11, "Unable to parse time_in_state"

    #@52
    invoke-static {v10, v11}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    goto :goto_1e
.end method

.method private getName(Lcom/android/internal/os/ProcessStats$Stats;Ljava/lang/String;)V
    .registers 8
    .parameter "st"
    .parameter "cmdlineFile"

    #@0
    .prologue
    .line 839
    iget-object v2, p1, Lcom/android/internal/os/ProcessStats$Stats;->name:Ljava/lang/String;

    #@2
    .line 840
    .local v2, newName:Ljava/lang/String;
    iget-object v3, p1, Lcom/android/internal/os/ProcessStats$Stats;->name:Ljava/lang/String;

    #@4
    if-eqz v3, :cond_1a

    #@6
    iget-object v3, p1, Lcom/android/internal/os/ProcessStats$Stats;->name:Ljava/lang/String;

    #@8
    const-string v4, "app_process"

    #@a
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v3

    #@e
    if-nez v3, :cond_1a

    #@10
    iget-object v3, p1, Lcom/android/internal/os/ProcessStats$Stats;->name:Ljava/lang/String;

    #@12
    const-string v4, "<pre-initialized>"

    #@14
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v3

    #@18
    if-eqz v3, :cond_43

    #@1a
    .line 842
    :cond_1a
    const/4 v3, 0x0

    #@1b
    invoke-direct {p0, p2, v3}, Lcom/android/internal/os/ProcessStats;->readFile(Ljava/lang/String;C)Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    .line 843
    .local v0, cmdName:Ljava/lang/String;
    if-eqz v0, :cond_3f

    #@21
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@24
    move-result v3

    #@25
    const/4 v4, 0x1

    #@26
    if-le v3, v4, :cond_3f

    #@28
    .line 844
    move-object v2, v0

    #@29
    .line 845
    const-string v3, "/"

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    #@2e
    move-result v1

    #@2f
    .line 846
    .local v1, i:I
    if-lez v1, :cond_3f

    #@31
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@34
    move-result v3

    #@35
    add-int/lit8 v3, v3, -0x1

    #@37
    if-ge v1, v3, :cond_3f

    #@39
    .line 847
    add-int/lit8 v3, v1, 0x1

    #@3b
    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@3e
    move-result-object v2

    #@3f
    .line 850
    .end local v1           #i:I
    :cond_3f
    if-nez v2, :cond_43

    #@41
    .line 851
    iget-object v2, p1, Lcom/android/internal/os/ProcessStats$Stats;->baseName:Ljava/lang/String;

    #@43
    .line 854
    .end local v0           #cmdName:Ljava/lang/String;
    :cond_43
    iget-object v3, p1, Lcom/android/internal/os/ProcessStats$Stats;->name:Ljava/lang/String;

    #@45
    if-eqz v3, :cond_4f

    #@47
    iget-object v3, p1, Lcom/android/internal/os/ProcessStats$Stats;->name:Ljava/lang/String;

    #@49
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4c
    move-result v3

    #@4d
    if-nez v3, :cond_59

    #@4f
    .line 855
    :cond_4f
    iput-object v2, p1, Lcom/android/internal/os/ProcessStats$Stats;->name:Ljava/lang/String;

    #@51
    .line 856
    iget-object v3, p1, Lcom/android/internal/os/ProcessStats$Stats;->name:Ljava/lang/String;

    #@53
    invoke-virtual {p0, v3}, Lcom/android/internal/os/ProcessStats;->onMeasureProcessName(Ljava/lang/String;)I

    #@56
    move-result v3

    #@57
    iput v3, p1, Lcom/android/internal/os/ProcessStats$Stats;->nameWidth:I

    #@59
    .line 858
    :cond_59
    return-void
.end method

.method private printProcessCPU(Ljava/io/PrintWriter;Ljava/lang/String;ILjava/lang/String;IIIIIIII)V
    .registers 20
    .parameter "pw"
    .parameter "prefix"
    .parameter "pid"
    .parameter "label"
    .parameter "totalTime"
    .parameter "user"
    .parameter "system"
    .parameter "iowait"
    .parameter "irq"
    .parameter "softIrq"
    .parameter "minFaults"
    .parameter "majFaults"

    #@0
    .prologue
    .line 759
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3
    .line 760
    if-nez p5, :cond_6

    #@5
    const/4 p5, 0x1

    #@6
    .line 761
    :cond_6
    add-int v1, p6, p7

    #@8
    add-int/2addr v1, p8

    #@9
    add-int v1, v1, p9

    #@b
    add-int v1, v1, p10

    #@d
    int-to-long v3, v1

    #@e
    int-to-long v5, p5

    #@f
    move-object v1, p0

    #@10
    move-object v2, p1

    #@11
    invoke-direct/range {v1 .. v6}, Lcom/android/internal/os/ProcessStats;->printRatio(Ljava/io/PrintWriter;JJ)V

    #@14
    .line 762
    const-string v1, "% "

    #@16
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@19
    .line 763
    if-ltz p3, :cond_23

    #@1b
    .line 764
    invoke-virtual {p1, p3}, Ljava/io/PrintWriter;->print(I)V

    #@1e
    .line 765
    const-string v1, "/"

    #@20
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@23
    .line 767
    :cond_23
    invoke-virtual {p1, p4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@26
    .line 768
    const-string v1, ": "

    #@28
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2b
    .line 769
    int-to-long v3, p6

    #@2c
    int-to-long v5, p5

    #@2d
    move-object v1, p0

    #@2e
    move-object v2, p1

    #@2f
    invoke-direct/range {v1 .. v6}, Lcom/android/internal/os/ProcessStats;->printRatio(Ljava/io/PrintWriter;JJ)V

    #@32
    .line 770
    const-string v1, "% user + "

    #@34
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@37
    .line 771
    int-to-long v3, p7

    #@38
    int-to-long v5, p5

    #@39
    move-object v1, p0

    #@3a
    move-object v2, p1

    #@3b
    invoke-direct/range {v1 .. v6}, Lcom/android/internal/os/ProcessStats;->printRatio(Ljava/io/PrintWriter;JJ)V

    #@3e
    .line 772
    const-string v1, "% kernel"

    #@40
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@43
    .line 773
    if-lez p8, :cond_56

    #@45
    .line 774
    const-string v1, " + "

    #@47
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4a
    .line 775
    int-to-long v3, p8

    #@4b
    int-to-long v5, p5

    #@4c
    move-object v1, p0

    #@4d
    move-object v2, p1

    #@4e
    invoke-direct/range {v1 .. v6}, Lcom/android/internal/os/ProcessStats;->printRatio(Ljava/io/PrintWriter;JJ)V

    #@51
    .line 776
    const-string v1, "% iowait"

    #@53
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@56
    .line 778
    :cond_56
    if-lez p9, :cond_6b

    #@58
    .line 779
    const-string v1, " + "

    #@5a
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5d
    .line 780
    move/from16 v0, p9

    #@5f
    int-to-long v3, v0

    #@60
    int-to-long v5, p5

    #@61
    move-object v1, p0

    #@62
    move-object v2, p1

    #@63
    invoke-direct/range {v1 .. v6}, Lcom/android/internal/os/ProcessStats;->printRatio(Ljava/io/PrintWriter;JJ)V

    #@66
    .line 781
    const-string v1, "% irq"

    #@68
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6b
    .line 783
    :cond_6b
    if-lez p10, :cond_80

    #@6d
    .line 784
    const-string v1, " + "

    #@6f
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@72
    .line 785
    move/from16 v0, p10

    #@74
    int-to-long v3, v0

    #@75
    int-to-long v5, p5

    #@76
    move-object v1, p0

    #@77
    move-object v2, p1

    #@78
    invoke-direct/range {v1 .. v6}, Lcom/android/internal/os/ProcessStats;->printRatio(Ljava/io/PrintWriter;JJ)V

    #@7b
    .line 786
    const-string v1, "% softirq"

    #@7d
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@80
    .line 788
    :cond_80
    if-gtz p11, :cond_84

    #@82
    if-lez p12, :cond_ab

    #@84
    .line 789
    :cond_84
    const-string v1, " / faults:"

    #@86
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@89
    .line 790
    if-lez p11, :cond_9a

    #@8b
    .line 791
    const-string v1, " "

    #@8d
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@90
    .line 792
    move/from16 v0, p11

    #@92
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    #@95
    .line 793
    const-string v1, " minor"

    #@97
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@9a
    .line 795
    :cond_9a
    if-lez p12, :cond_ab

    #@9c
    .line 796
    const-string v1, " "

    #@9e
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a1
    .line 797
    move/from16 v0, p12

    #@a3
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    #@a6
    .line 798
    const-string v1, " major"

    #@a8
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ab
    .line 801
    :cond_ab
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    #@ae
    .line 802
    return-void
.end method

.method private printRatio(Ljava/io/PrintWriter;JJ)V
    .registers 16
    .parameter "pw"
    .parameter "numerator"
    .parameter "denominator"

    #@0
    .prologue
    const-wide/16 v8, 0xa

    #@2
    .line 744
    const-wide/16 v6, 0x3e8

    #@4
    mul-long/2addr v6, p2

    #@5
    div-long v4, v6, p4

    #@7
    .line 745
    .local v4, thousands:J
    div-long v0, v4, v8

    #@9
    .line 746
    .local v0, hundreds:J
    invoke-virtual {p1, v0, v1}, Ljava/io/PrintWriter;->print(J)V

    #@c
    .line 747
    cmp-long v6, v0, v8

    #@e
    if-gez v6, :cond_22

    #@10
    .line 748
    mul-long v6, v0, v8

    #@12
    sub-long v2, v4, v6

    #@14
    .line 749
    .local v2, remainder:J
    const-wide/16 v6, 0x0

    #@16
    cmp-long v6, v2, v6

    #@18
    if-eqz v6, :cond_22

    #@1a
    .line 750
    const/16 v6, 0x2e

    #@1c
    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->print(C)V

    #@1f
    .line 751
    invoke-virtual {p1, v2, v3}, Ljava/io/PrintWriter;->print(J)V

    #@22
    .line 754
    .end local v2           #remainder:J
    :cond_22
    return-void
.end method

.method private readFile(Ljava/lang/String;C)Ljava/lang/String;
    .registers 11
    .parameter "file"
    .parameter "endChar"

    #@0
    .prologue
    .line 808
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    #@3
    move-result-object v4

    #@4
    .line 809
    .local v4, savedPolicy:Landroid/os/StrictMode$ThreadPolicy;
    const/4 v1, 0x0

    #@5
    .line 811
    .local v1, is:Ljava/io/FileInputStream;
    :try_start_5
    new-instance v2, Ljava/io/FileInputStream;

    #@7
    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_5 .. :try_end_a} :catchall_52
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_a} :catch_3e
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_a} :catch_48

    #@a
    .line 812
    .end local v1           #is:Ljava/io/FileInputStream;
    .local v2, is:Ljava/io/FileInputStream;
    :try_start_a
    iget-object v5, p0, Lcom/android/internal/os/ProcessStats;->mBuffer:[B

    #@c
    invoke-virtual {v2, v5}, Ljava/io/FileInputStream;->read([B)I

    #@f
    move-result v3

    #@10
    .line 813
    .local v3, len:I
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    #@13
    .line 815
    if-lez v3, :cond_33

    #@15
    .line 817
    const/4 v0, 0x0

    #@16
    .local v0, i:I
    :goto_16
    if-ge v0, v3, :cond_1e

    #@18
    .line 818
    iget-object v5, p0, Lcom/android/internal/os/ProcessStats;->mBuffer:[B

    #@1a
    aget-byte v5, v5, v0

    #@1c
    if-ne v5, p2, :cond_30

    #@1e
    .line 822
    :cond_1e
    new-instance v5, Ljava/lang/String;

    #@20
    iget-object v6, p0, Lcom/android/internal/os/ProcessStats;->mBuffer:[B

    #@22
    const/4 v7, 0x0

    #@23
    invoke-direct {v5, v6, v7, v0}, Ljava/lang/String;-><init>([BII)V
    :try_end_26
    .catchall {:try_start_a .. :try_end_26} :catchall_66
    .catch Ljava/io/FileNotFoundException; {:try_start_a .. :try_end_26} :catch_6c
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_26} :catch_69

    #@26
    .line 827
    if-eqz v2, :cond_2b

    #@28
    .line 829
    :try_start_28
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_2b
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_2b} :catch_5c

    #@2b
    .line 833
    :cond_2b
    :goto_2b
    invoke-static {v4}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@2e
    move-object v1, v2

    #@2f
    .line 835
    .end local v0           #i:I
    .end local v2           #is:Ljava/io/FileInputStream;
    .end local v3           #len:I
    .restart local v1       #is:Ljava/io/FileInputStream;
    :goto_2f
    return-object v5

    #@30
    .line 817
    .end local v1           #is:Ljava/io/FileInputStream;
    .restart local v0       #i:I
    .restart local v2       #is:Ljava/io/FileInputStream;
    .restart local v3       #len:I
    :cond_30
    add-int/lit8 v0, v0, 0x1

    #@32
    goto :goto_16

    #@33
    .line 827
    .end local v0           #i:I
    :cond_33
    if-eqz v2, :cond_38

    #@35
    .line 829
    :try_start_35
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_38
    .catch Ljava/io/IOException; {:try_start_35 .. :try_end_38} :catch_5e

    #@38
    .line 833
    :cond_38
    :goto_38
    invoke-static {v4}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@3b
    move-object v1, v2

    #@3c
    .line 835
    .end local v2           #is:Ljava/io/FileInputStream;
    .end local v3           #len:I
    .restart local v1       #is:Ljava/io/FileInputStream;
    :goto_3c
    const/4 v5, 0x0

    #@3d
    goto :goto_2f

    #@3e
    .line 824
    :catch_3e
    move-exception v5

    #@3f
    .line 827
    :goto_3f
    if-eqz v1, :cond_44

    #@41
    .line 829
    :try_start_41
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_44
    .catch Ljava/io/IOException; {:try_start_41 .. :try_end_44} :catch_60

    #@44
    .line 833
    :cond_44
    :goto_44
    invoke-static {v4}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@47
    goto :goto_3c

    #@48
    .line 825
    :catch_48
    move-exception v5

    #@49
    .line 827
    :goto_49
    if-eqz v1, :cond_4e

    #@4b
    .line 829
    :try_start_4b
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_4e
    .catch Ljava/io/IOException; {:try_start_4b .. :try_end_4e} :catch_62

    #@4e
    .line 833
    :cond_4e
    :goto_4e
    invoke-static {v4}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@51
    goto :goto_3c

    #@52
    .line 827
    :catchall_52
    move-exception v5

    #@53
    :goto_53
    if-eqz v1, :cond_58

    #@55
    .line 829
    :try_start_55
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_58
    .catch Ljava/io/IOException; {:try_start_55 .. :try_end_58} :catch_64

    #@58
    .line 833
    :cond_58
    :goto_58
    invoke-static {v4}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@5b
    throw v5

    #@5c
    .line 830
    .end local v1           #is:Ljava/io/FileInputStream;
    .restart local v0       #i:I
    .restart local v2       #is:Ljava/io/FileInputStream;
    .restart local v3       #len:I
    :catch_5c
    move-exception v6

    #@5d
    goto :goto_2b

    #@5e
    .end local v0           #i:I
    :catch_5e
    move-exception v5

    #@5f
    goto :goto_38

    #@60
    .end local v2           #is:Ljava/io/FileInputStream;
    .end local v3           #len:I
    .restart local v1       #is:Ljava/io/FileInputStream;
    :catch_60
    move-exception v5

    #@61
    goto :goto_44

    #@62
    :catch_62
    move-exception v5

    #@63
    goto :goto_4e

    #@64
    :catch_64
    move-exception v6

    #@65
    goto :goto_58

    #@66
    .line 827
    .end local v1           #is:Ljava/io/FileInputStream;
    .restart local v2       #is:Ljava/io/FileInputStream;
    :catchall_66
    move-exception v5

    #@67
    move-object v1, v2

    #@68
    .end local v2           #is:Ljava/io/FileInputStream;
    .restart local v1       #is:Ljava/io/FileInputStream;
    goto :goto_53

    #@69
    .line 825
    .end local v1           #is:Ljava/io/FileInputStream;
    .restart local v2       #is:Ljava/io/FileInputStream;
    :catch_69
    move-exception v5

    #@6a
    move-object v1, v2

    #@6b
    .end local v2           #is:Ljava/io/FileInputStream;
    .restart local v1       #is:Ljava/io/FileInputStream;
    goto :goto_49

    #@6c
    .line 824
    .end local v1           #is:Ljava/io/FileInputStream;
    .restart local v2       #is:Ljava/io/FileInputStream;
    :catch_6c
    move-exception v5

    #@6d
    move-object v1, v2

    #@6e
    .end local v2           #is:Ljava/io/FileInputStream;
    .restart local v1       #is:Ljava/io/FileInputStream;
    goto :goto_3f
.end method


# virtual methods
.method final buildWorkingProcs()V
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    .line 630
    iget-boolean v6, p0, Lcom/android/internal/os/ProcessStats;->mWorkingProcsSorted:Z

    #@3
    if-nez v6, :cond_65

    #@5
    .line 631
    iget-object v6, p0, Lcom/android/internal/os/ProcessStats;->mWorkingProcs:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    #@a
    .line 632
    iget-object v6, p0, Lcom/android/internal/os/ProcessStats;->mProcStats:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@f
    move-result v1

    #@10
    .line 633
    .local v1, N:I
    const/4 v2, 0x0

    #@11
    .local v2, i:I
    :goto_11
    if-ge v2, v1, :cond_5c

    #@13
    .line 634
    iget-object v6, p0, Lcom/android/internal/os/ProcessStats;->mProcStats:Ljava/util/ArrayList;

    #@15
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@18
    move-result-object v4

    #@19
    check-cast v4, Lcom/android/internal/os/ProcessStats$Stats;

    #@1b
    .line 635
    .local v4, stats:Lcom/android/internal/os/ProcessStats$Stats;
    iget-boolean v6, v4, Lcom/android/internal/os/ProcessStats$Stats;->working:Z

    #@1d
    if-eqz v6, :cond_59

    #@1f
    .line 636
    iget-object v6, p0, Lcom/android/internal/os/ProcessStats;->mWorkingProcs:Ljava/util/ArrayList;

    #@21
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@24
    .line 637
    iget-object v6, v4, Lcom/android/internal/os/ProcessStats$Stats;->threadStats:Ljava/util/ArrayList;

    #@26
    if-eqz v6, :cond_59

    #@28
    iget-object v6, v4, Lcom/android/internal/os/ProcessStats$Stats;->threadStats:Ljava/util/ArrayList;

    #@2a
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@2d
    move-result v6

    #@2e
    if-le v6, v8, :cond_59

    #@30
    .line 638
    iget-object v6, v4, Lcom/android/internal/os/ProcessStats$Stats;->workingThreads:Ljava/util/ArrayList;

    #@32
    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    #@35
    .line 639
    iget-object v6, v4, Lcom/android/internal/os/ProcessStats$Stats;->threadStats:Ljava/util/ArrayList;

    #@37
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@3a
    move-result v0

    #@3b
    .line 640
    .local v0, M:I
    const/4 v3, 0x0

    #@3c
    .local v3, j:I
    :goto_3c
    if-ge v3, v0, :cond_52

    #@3e
    .line 641
    iget-object v6, v4, Lcom/android/internal/os/ProcessStats$Stats;->threadStats:Ljava/util/ArrayList;

    #@40
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@43
    move-result-object v5

    #@44
    check-cast v5, Lcom/android/internal/os/ProcessStats$Stats;

    #@46
    .line 642
    .local v5, tstats:Lcom/android/internal/os/ProcessStats$Stats;
    iget-boolean v6, v5, Lcom/android/internal/os/ProcessStats$Stats;->working:Z

    #@48
    if-eqz v6, :cond_4f

    #@4a
    .line 643
    iget-object v6, v4, Lcom/android/internal/os/ProcessStats$Stats;->workingThreads:Ljava/util/ArrayList;

    #@4c
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4f
    .line 640
    :cond_4f
    add-int/lit8 v3, v3, 0x1

    #@51
    goto :goto_3c

    #@52
    .line 646
    .end local v5           #tstats:Lcom/android/internal/os/ProcessStats$Stats;
    :cond_52
    iget-object v6, v4, Lcom/android/internal/os/ProcessStats$Stats;->workingThreads:Ljava/util/ArrayList;

    #@54
    sget-object v7, Lcom/android/internal/os/ProcessStats;->sLoadComparator:Ljava/util/Comparator;

    #@56
    invoke-static {v6, v7}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    #@59
    .line 633
    .end local v0           #M:I
    .end local v3           #j:I
    :cond_59
    add-int/lit8 v2, v2, 0x1

    #@5b
    goto :goto_11

    #@5c
    .line 650
    .end local v4           #stats:Lcom/android/internal/os/ProcessStats$Stats;
    :cond_5c
    iget-object v6, p0, Lcom/android/internal/os/ProcessStats;->mWorkingProcs:Ljava/util/ArrayList;

    #@5e
    sget-object v7, Lcom/android/internal/os/ProcessStats;->sLoadComparator:Ljava/util/Comparator;

    #@60
    invoke-static {v6, v7}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    #@63
    .line 651
    iput-boolean v8, p0, Lcom/android/internal/os/ProcessStats;->mWorkingProcsSorted:Z

    #@65
    .line 653
    .end local v1           #N:I
    .end local v2           #i:I
    :cond_65
    return-void
.end method

.method public final countStats()I
    .registers 2

    #@0
    .prologue
    .line 656
    iget-object v0, p0, Lcom/android/internal/os/ProcessStats;->mProcStats:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final countWorkingStats()I
    .registers 2

    #@0
    .prologue
    .line 664
    invoke-virtual {p0}, Lcom/android/internal/os/ProcessStats;->buildWorkingProcs()V

    #@3
    .line 665
    iget-object v0, p0, Lcom/android/internal/os/ProcessStats;->mWorkingProcs:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public getCpuTimeForPid(I)J
    .registers 10
    .parameter "pid"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 522
    new-instance v4, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v5, "/proc/"

    #@8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v4

    #@c
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f
    move-result-object v4

    #@10
    const-string v5, "/stat"

    #@12
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v4

    #@16
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v0

    #@1a
    .line 523
    .local v0, statFile:Ljava/lang/String;
    iget-object v1, p0, Lcom/android/internal/os/ProcessStats;->mSinglePidStatsData:[J

    #@1c
    .line 524
    .local v1, statsData:[J
    sget-object v4, Lcom/android/internal/os/ProcessStats;->PROCESS_STATS_FORMAT:[I

    #@1e
    invoke-static {v0, v4, v6, v1, v6}, Landroid/os/Process;->readProcFile(Ljava/lang/String;[I[Ljava/lang/String;[J[F)Z

    #@21
    move-result v4

    #@22
    if-eqz v4, :cond_2d

    #@24
    .line 526
    const/4 v4, 0x2

    #@25
    aget-wide v4, v1, v4

    #@27
    const/4 v6, 0x3

    #@28
    aget-wide v6, v1, v6

    #@2a
    add-long v2, v4, v6

    #@2c
    .line 530
    :goto_2c
    return-wide v2

    #@2d
    :cond_2d
    const-wide/16 v2, 0x0

    #@2f
    goto :goto_2c
.end method

.method public getLastCpuSpeedTimes()[J
    .registers 9

    #@0
    .prologue
    .line 539
    iget-object v3, p0, Lcom/android/internal/os/ProcessStats;->mCpuSpeedTimes:[J

    #@2
    if-nez v3, :cond_21

    #@4
    .line 540
    const/4 v3, 0x0

    #@5
    invoke-direct {p0, v3}, Lcom/android/internal/os/ProcessStats;->getCpuSpeedTimes([J)[J

    #@8
    move-result-object v3

    #@9
    iput-object v3, p0, Lcom/android/internal/os/ProcessStats;->mCpuSpeedTimes:[J

    #@b
    .line 541
    iget-object v3, p0, Lcom/android/internal/os/ProcessStats;->mCpuSpeedTimes:[J

    #@d
    array-length v3, v3

    #@e
    new-array v3, v3, [J

    #@10
    iput-object v3, p0, Lcom/android/internal/os/ProcessStats;->mRelCpuSpeedTimes:[J

    #@12
    .line 542
    const/4 v0, 0x0

    #@13
    .local v0, i:I
    :goto_13
    iget-object v3, p0, Lcom/android/internal/os/ProcessStats;->mCpuSpeedTimes:[J

    #@15
    array-length v3, v3

    #@16
    if-ge v0, v3, :cond_42

    #@18
    .line 543
    iget-object v3, p0, Lcom/android/internal/os/ProcessStats;->mRelCpuSpeedTimes:[J

    #@1a
    const-wide/16 v4, 0x1

    #@1c
    aput-wide v4, v3, v0

    #@1e
    .line 542
    add-int/lit8 v0, v0, 0x1

    #@20
    goto :goto_13

    #@21
    .line 546
    .end local v0           #i:I
    :cond_21
    iget-object v3, p0, Lcom/android/internal/os/ProcessStats;->mRelCpuSpeedTimes:[J

    #@23
    invoke-direct {p0, v3}, Lcom/android/internal/os/ProcessStats;->getCpuSpeedTimes([J)[J

    #@26
    .line 547
    const/4 v0, 0x0

    #@27
    .restart local v0       #i:I
    :goto_27
    iget-object v3, p0, Lcom/android/internal/os/ProcessStats;->mCpuSpeedTimes:[J

    #@29
    array-length v3, v3

    #@2a
    if-ge v0, v3, :cond_42

    #@2c
    .line 548
    iget-object v3, p0, Lcom/android/internal/os/ProcessStats;->mRelCpuSpeedTimes:[J

    #@2e
    aget-wide v1, v3, v0

    #@30
    .line 549
    .local v1, temp:J
    iget-object v3, p0, Lcom/android/internal/os/ProcessStats;->mRelCpuSpeedTimes:[J

    #@32
    aget-wide v4, v3, v0

    #@34
    iget-object v6, p0, Lcom/android/internal/os/ProcessStats;->mCpuSpeedTimes:[J

    #@36
    aget-wide v6, v6, v0

    #@38
    sub-long/2addr v4, v6

    #@39
    aput-wide v4, v3, v0

    #@3b
    .line 550
    iget-object v3, p0, Lcom/android/internal/os/ProcessStats;->mCpuSpeedTimes:[J

    #@3d
    aput-wide v1, v3, v0

    #@3f
    .line 547
    add-int/lit8 v0, v0, 0x1

    #@41
    goto :goto_27

    #@42
    .line 553
    .end local v1           #temp:J
    :cond_42
    iget-object v3, p0, Lcom/android/internal/os/ProcessStats;->mRelCpuSpeedTimes:[J

    #@44
    return-object v3
.end method

.method public final getLastIdleTime()I
    .registers 2

    #@0
    .prologue
    .line 618
    iget v0, p0, Lcom/android/internal/os/ProcessStats;->mRelIdleTime:I

    #@2
    return v0
.end method

.method public final getLastIoWaitTime()I
    .registers 2

    #@0
    .prologue
    .line 606
    iget v0, p0, Lcom/android/internal/os/ProcessStats;->mRelIoWaitTime:I

    #@2
    return v0
.end method

.method public final getLastIrqTime()I
    .registers 2

    #@0
    .prologue
    .line 610
    iget v0, p0, Lcom/android/internal/os/ProcessStats;->mRelIrqTime:I

    #@2
    return v0
.end method

.method public final getLastSoftIrqTime()I
    .registers 2

    #@0
    .prologue
    .line 614
    iget v0, p0, Lcom/android/internal/os/ProcessStats;->mRelSoftIrqTime:I

    #@2
    return v0
.end method

.method public final getLastSystemTime()I
    .registers 2

    #@0
    .prologue
    .line 602
    iget v0, p0, Lcom/android/internal/os/ProcessStats;->mRelSystemTime:I

    #@2
    return v0
.end method

.method public final getLastUserTime()I
    .registers 2

    #@0
    .prologue
    .line 598
    iget v0, p0, Lcom/android/internal/os/ProcessStats;->mRelUserTime:I

    #@2
    return v0
.end method

.method public final getStats(I)Lcom/android/internal/os/ProcessStats$Stats;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 660
    iget-object v0, p0, Lcom/android/internal/os/ProcessStats;->mProcStats:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/os/ProcessStats$Stats;

    #@8
    return-object v0
.end method

.method public final getTotalCpuPercent()F
    .registers 4

    #@0
    .prologue
    .line 622
    iget v1, p0, Lcom/android/internal/os/ProcessStats;->mRelUserTime:I

    #@2
    iget v2, p0, Lcom/android/internal/os/ProcessStats;->mRelSystemTime:I

    #@4
    add-int/2addr v1, v2

    #@5
    iget v2, p0, Lcom/android/internal/os/ProcessStats;->mRelIrqTime:I

    #@7
    add-int/2addr v1, v2

    #@8
    iget v2, p0, Lcom/android/internal/os/ProcessStats;->mRelIdleTime:I

    #@a
    add-int v0, v1, v2

    #@c
    .line 623
    .local v0, denom:I
    if-gtz v0, :cond_10

    #@e
    .line 624
    const/4 v1, 0x0

    #@f
    .line 626
    :goto_f
    return v1

    #@10
    :cond_10
    iget v1, p0, Lcom/android/internal/os/ProcessStats;->mRelUserTime:I

    #@12
    iget v2, p0, Lcom/android/internal/os/ProcessStats;->mRelSystemTime:I

    #@14
    add-int/2addr v1, v2

    #@15
    iget v2, p0, Lcom/android/internal/os/ProcessStats;->mRelIrqTime:I

    #@17
    add-int/2addr v1, v2

    #@18
    int-to-float v1, v1

    #@19
    const/high16 v2, 0x42c8

    #@1b
    mul-float/2addr v1, v2

    #@1c
    int-to-float v2, v0

    #@1d
    div-float/2addr v1, v2

    #@1e
    goto :goto_f
.end method

.method public final getWorkingStats(I)Lcom/android/internal/os/ProcessStats$Stats;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 669
    iget-object v0, p0, Lcom/android/internal/os/ProcessStats;->mWorkingProcs:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/os/ProcessStats$Stats;

    #@8
    return-object v0
.end method

.method public init()V
    .registers 2

    #@0
    .prologue
    .line 266
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Lcom/android/internal/os/ProcessStats;->mFirst:Z

    #@3
    .line 267
    invoke-virtual {p0}, Lcom/android/internal/os/ProcessStats;->update()V

    #@6
    .line 268
    return-void
.end method

.method public onLoadChanged(FFF)V
    .registers 4
    .parameter "load1"
    .parameter "load5"
    .parameter "load15"

    #@0
    .prologue
    .line 258
    return-void
.end method

.method public onMeasureProcessName(Ljava/lang/String;)I
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 261
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public final printCurrentLoad()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 673
    new-instance v1, Ljava/io/StringWriter;

    #@2
    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    #@5
    .line 674
    .local v1, sw:Ljava/io/StringWriter;
    new-instance v0, Ljava/io/PrintWriter;

    #@7
    invoke-direct {v0, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    #@a
    .line 675
    .local v0, pw:Ljava/io/PrintWriter;
    const-string v2, "Load: "

    #@c
    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@f
    .line 676
    iget v2, p0, Lcom/android/internal/os/ProcessStats;->mLoad1:F

    #@11
    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->print(F)V

    #@14
    .line 677
    const-string v2, " / "

    #@16
    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@19
    .line 678
    iget v2, p0, Lcom/android/internal/os/ProcessStats;->mLoad5:F

    #@1b
    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->print(F)V

    #@1e
    .line 679
    const-string v2, " / "

    #@20
    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@23
    .line 680
    iget v2, p0, Lcom/android/internal/os/ProcessStats;->mLoad15:F

    #@25
    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(F)V

    #@28
    .line 681
    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    return-object v2
.end method

.method public final printCurrentState(J)Ljava/lang/String;
    .registers 32
    .parameter "now"

    #@0
    .prologue
    .line 685
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/os/ProcessStats;->buildWorkingProcs()V

    #@3
    .line 687
    new-instance v26, Ljava/io/StringWriter;

    #@5
    invoke-direct/range {v26 .. v26}, Ljava/io/StringWriter;-><init>()V

    #@8
    .line 688
    .local v26, sw:Ljava/io/StringWriter;
    new-instance v3, Ljava/io/PrintWriter;

    #@a
    move-object/from16 v0, v26

    #@c
    invoke-direct {v3, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    #@f
    .line 690
    .local v3, pw:Ljava/io/PrintWriter;
    const-string v2, "CPU usage from "

    #@11
    invoke-virtual {v3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@14
    .line 691
    move-object/from16 v0, p0

    #@16
    iget-wide v4, v0, Lcom/android/internal/os/ProcessStats;->mLastSampleTime:J

    #@18
    cmp-long v2, p1, v4

    #@1a
    if-lez v2, :cond_135

    #@1c
    .line 692
    move-object/from16 v0, p0

    #@1e
    iget-wide v4, v0, Lcom/android/internal/os/ProcessStats;->mLastSampleTime:J

    #@20
    sub-long v4, p1, v4

    #@22
    invoke-virtual {v3, v4, v5}, Ljava/io/PrintWriter;->print(J)V

    #@25
    .line 693
    const-string/jumbo v2, "ms to "

    #@28
    invoke-virtual {v3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2b
    .line 694
    move-object/from16 v0, p0

    #@2d
    iget-wide v4, v0, Lcom/android/internal/os/ProcessStats;->mCurrentSampleTime:J

    #@2f
    sub-long v4, p1, v4

    #@31
    invoke-virtual {v3, v4, v5}, Ljava/io/PrintWriter;->print(J)V

    #@34
    .line 695
    const-string/jumbo v2, "ms ago"

    #@37
    invoke-virtual {v3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3a
    .line 703
    :goto_3a
    move-object/from16 v0, p0

    #@3c
    iget-wide v4, v0, Lcom/android/internal/os/ProcessStats;->mCurrentSampleTime:J

    #@3e
    move-object/from16 v0, p0

    #@40
    iget-wide v6, v0, Lcom/android/internal/os/ProcessStats;->mLastSampleTime:J

    #@42
    sub-long v23, v4, v6

    #@44
    .line 704
    .local v23, sampleTime:J
    move-object/from16 v0, p0

    #@46
    iget-wide v4, v0, Lcom/android/internal/os/ProcessStats;->mCurrentSampleRealTime:J

    #@48
    move-object/from16 v0, p0

    #@4a
    iget-wide v6, v0, Lcom/android/internal/os/ProcessStats;->mLastSampleRealTime:J

    #@4c
    sub-long v21, v4, v6

    #@4e
    .line 705
    .local v21, sampleRealTime:J
    const-wide/16 v4, 0x0

    #@50
    cmp-long v2, v21, v4

    #@52
    if-lez v2, :cond_155

    #@54
    const-wide/16 v4, 0x64

    #@56
    mul-long v4, v4, v23

    #@58
    div-long v19, v4, v21

    #@5a
    .line 706
    .local v19, percAwake:J
    :goto_5a
    const-wide/16 v4, 0x64

    #@5c
    cmp-long v2, v19, v4

    #@5e
    if-eqz v2, :cond_6f

    #@60
    .line 707
    const-string v2, " with "

    #@62
    invoke-virtual {v3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@65
    .line 708
    move-wide/from16 v0, v19

    #@67
    invoke-virtual {v3, v0, v1}, Ljava/io/PrintWriter;->print(J)V

    #@6a
    .line 709
    const-string v2, "% awake"

    #@6c
    invoke-virtual {v3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6f
    .line 711
    :cond_6f
    const-string v2, ":"

    #@71
    invoke-virtual {v3, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@74
    .line 713
    move-object/from16 v0, p0

    #@76
    iget v2, v0, Lcom/android/internal/os/ProcessStats;->mRelUserTime:I

    #@78
    move-object/from16 v0, p0

    #@7a
    iget v4, v0, Lcom/android/internal/os/ProcessStats;->mRelSystemTime:I

    #@7c
    add-int/2addr v2, v4

    #@7d
    move-object/from16 v0, p0

    #@7f
    iget v4, v0, Lcom/android/internal/os/ProcessStats;->mRelIoWaitTime:I

    #@81
    add-int/2addr v2, v4

    #@82
    move-object/from16 v0, p0

    #@84
    iget v4, v0, Lcom/android/internal/os/ProcessStats;->mRelIrqTime:I

    #@86
    add-int/2addr v2, v4

    #@87
    move-object/from16 v0, p0

    #@89
    iget v4, v0, Lcom/android/internal/os/ProcessStats;->mRelSoftIrqTime:I

    #@8b
    add-int/2addr v2, v4

    #@8c
    move-object/from16 v0, p0

    #@8e
    iget v4, v0, Lcom/android/internal/os/ProcessStats;->mRelIdleTime:I

    #@90
    add-int v27, v2, v4

    #@92
    .line 719
    .local v27, totalTime:I
    move-object/from16 v0, p0

    #@94
    iget-object v2, v0, Lcom/android/internal/os/ProcessStats;->mWorkingProcs:Ljava/util/ArrayList;

    #@96
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@99
    move-result v16

    #@9a
    .line 720
    .local v16, N:I
    const/16 v17, 0x0

    #@9c
    .local v17, i:I
    :goto_9c
    move/from16 v0, v17

    #@9e
    move/from16 v1, v16

    #@a0
    if-ge v0, v1, :cond_177

    #@a2
    .line 721
    move-object/from16 v0, p0

    #@a4
    iget-object v2, v0, Lcom/android/internal/os/ProcessStats;->mWorkingProcs:Ljava/util/ArrayList;

    #@a6
    move/from16 v0, v17

    #@a8
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@ab
    move-result-object v25

    #@ac
    check-cast v25, Lcom/android/internal/os/ProcessStats$Stats;

    #@ae
    .line 722
    .local v25, st:Lcom/android/internal/os/ProcessStats$Stats;
    move-object/from16 v0, v25

    #@b0
    iget-boolean v2, v0, Lcom/android/internal/os/ProcessStats$Stats;->added:Z

    #@b2
    if-eqz v2, :cond_159

    #@b4
    const-string v4, " +"

    #@b6
    :goto_b6
    move-object/from16 v0, v25

    #@b8
    iget v5, v0, Lcom/android/internal/os/ProcessStats$Stats;->pid:I

    #@ba
    move-object/from16 v0, v25

    #@bc
    iget-object v6, v0, Lcom/android/internal/os/ProcessStats$Stats;->name:Ljava/lang/String;

    #@be
    move-object/from16 v0, v25

    #@c0
    iget-wide v7, v0, Lcom/android/internal/os/ProcessStats$Stats;->rel_uptime:J

    #@c2
    const-wide/16 v9, 0x5

    #@c4
    add-long/2addr v7, v9

    #@c5
    long-to-int v2, v7

    #@c6
    div-int/lit8 v7, v2, 0xa

    #@c8
    move-object/from16 v0, v25

    #@ca
    iget v8, v0, Lcom/android/internal/os/ProcessStats$Stats;->rel_utime:I

    #@cc
    move-object/from16 v0, v25

    #@ce
    iget v9, v0, Lcom/android/internal/os/ProcessStats$Stats;->rel_stime:I

    #@d0
    const/4 v10, 0x0

    #@d1
    const/4 v11, 0x0

    #@d2
    const/4 v12, 0x0

    #@d3
    move-object/from16 v0, v25

    #@d5
    iget v13, v0, Lcom/android/internal/os/ProcessStats$Stats;->rel_minfaults:I

    #@d7
    move-object/from16 v0, v25

    #@d9
    iget v14, v0, Lcom/android/internal/os/ProcessStats$Stats;->rel_majfaults:I

    #@db
    move-object/from16 v2, p0

    #@dd
    invoke-direct/range {v2 .. v14}, Lcom/android/internal/os/ProcessStats;->printProcessCPU(Ljava/io/PrintWriter;Ljava/lang/String;ILjava/lang/String;IIIIIIII)V

    #@e0
    .line 725
    move-object/from16 v0, v25

    #@e2
    iget-boolean v2, v0, Lcom/android/internal/os/ProcessStats$Stats;->removed:Z

    #@e4
    if-nez v2, :cond_173

    #@e6
    move-object/from16 v0, v25

    #@e8
    iget-object v2, v0, Lcom/android/internal/os/ProcessStats$Stats;->workingThreads:Ljava/util/ArrayList;

    #@ea
    if-eqz v2, :cond_173

    #@ec
    .line 726
    move-object/from16 v0, v25

    #@ee
    iget-object v2, v0, Lcom/android/internal/os/ProcessStats$Stats;->workingThreads:Ljava/util/ArrayList;

    #@f0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@f3
    move-result v15

    #@f4
    .line 727
    .local v15, M:I
    const/16 v18, 0x0

    #@f6
    .local v18, j:I
    :goto_f6
    move/from16 v0, v18

    #@f8
    if-ge v0, v15, :cond_173

    #@fa
    .line 728
    move-object/from16 v0, v25

    #@fc
    iget-object v2, v0, Lcom/android/internal/os/ProcessStats$Stats;->workingThreads:Ljava/util/ArrayList;

    #@fe
    move/from16 v0, v18

    #@100
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@103
    move-result-object v28

    #@104
    check-cast v28, Lcom/android/internal/os/ProcessStats$Stats;

    #@106
    .line 729
    .local v28, tst:Lcom/android/internal/os/ProcessStats$Stats;
    move-object/from16 v0, v28

    #@108
    iget-boolean v2, v0, Lcom/android/internal/os/ProcessStats$Stats;->added:Z

    #@10a
    if-eqz v2, :cond_167

    #@10c
    const-string v4, "   +"

    #@10e
    :goto_10e
    move-object/from16 v0, v28

    #@110
    iget v5, v0, Lcom/android/internal/os/ProcessStats$Stats;->pid:I

    #@112
    move-object/from16 v0, v28

    #@114
    iget-object v6, v0, Lcom/android/internal/os/ProcessStats$Stats;->name:Ljava/lang/String;

    #@116
    move-object/from16 v0, v25

    #@118
    iget-wide v7, v0, Lcom/android/internal/os/ProcessStats$Stats;->rel_uptime:J

    #@11a
    const-wide/16 v9, 0x5

    #@11c
    add-long/2addr v7, v9

    #@11d
    long-to-int v2, v7

    #@11e
    div-int/lit8 v7, v2, 0xa

    #@120
    move-object/from16 v0, v28

    #@122
    iget v8, v0, Lcom/android/internal/os/ProcessStats$Stats;->rel_utime:I

    #@124
    move-object/from16 v0, v28

    #@126
    iget v9, v0, Lcom/android/internal/os/ProcessStats$Stats;->rel_stime:I

    #@128
    const/4 v10, 0x0

    #@129
    const/4 v11, 0x0

    #@12a
    const/4 v12, 0x0

    #@12b
    const/4 v13, 0x0

    #@12c
    const/4 v14, 0x0

    #@12d
    move-object/from16 v2, p0

    #@12f
    invoke-direct/range {v2 .. v14}, Lcom/android/internal/os/ProcessStats;->printProcessCPU(Ljava/io/PrintWriter;Ljava/lang/String;ILjava/lang/String;IIIIIIII)V

    #@132
    .line 727
    add-int/lit8 v18, v18, 0x1

    #@134
    goto :goto_f6

    #@135
    .line 697
    .end local v15           #M:I
    .end local v16           #N:I
    .end local v17           #i:I
    .end local v18           #j:I
    .end local v19           #percAwake:J
    .end local v21           #sampleRealTime:J
    .end local v23           #sampleTime:J
    .end local v25           #st:Lcom/android/internal/os/ProcessStats$Stats;
    .end local v27           #totalTime:I
    .end local v28           #tst:Lcom/android/internal/os/ProcessStats$Stats;
    :cond_135
    move-object/from16 v0, p0

    #@137
    iget-wide v4, v0, Lcom/android/internal/os/ProcessStats;->mLastSampleTime:J

    #@139
    sub-long v4, v4, p1

    #@13b
    invoke-virtual {v3, v4, v5}, Ljava/io/PrintWriter;->print(J)V

    #@13e
    .line 698
    const-string/jumbo v2, "ms to "

    #@141
    invoke-virtual {v3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@144
    .line 699
    move-object/from16 v0, p0

    #@146
    iget-wide v4, v0, Lcom/android/internal/os/ProcessStats;->mCurrentSampleTime:J

    #@148
    sub-long v4, v4, p1

    #@14a
    invoke-virtual {v3, v4, v5}, Ljava/io/PrintWriter;->print(J)V

    #@14d
    .line 700
    const-string/jumbo v2, "ms later"

    #@150
    invoke-virtual {v3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@153
    goto/16 :goto_3a

    #@155
    .line 705
    .restart local v21       #sampleRealTime:J
    .restart local v23       #sampleTime:J
    :cond_155
    const-wide/16 v19, 0x0

    #@157
    goto/16 :goto_5a

    #@159
    .line 722
    .restart local v16       #N:I
    .restart local v17       #i:I
    .restart local v19       #percAwake:J
    .restart local v25       #st:Lcom/android/internal/os/ProcessStats$Stats;
    .restart local v27       #totalTime:I
    :cond_159
    move-object/from16 v0, v25

    #@15b
    iget-boolean v2, v0, Lcom/android/internal/os/ProcessStats$Stats;->removed:Z

    #@15d
    if-eqz v2, :cond_163

    #@15f
    const-string v4, " -"

    #@161
    goto/16 :goto_b6

    #@163
    :cond_163
    const-string v4, "  "

    #@165
    goto/16 :goto_b6

    #@167
    .line 729
    .restart local v15       #M:I
    .restart local v18       #j:I
    .restart local v28       #tst:Lcom/android/internal/os/ProcessStats$Stats;
    :cond_167
    move-object/from16 v0, v28

    #@169
    iget-boolean v2, v0, Lcom/android/internal/os/ProcessStats$Stats;->removed:Z

    #@16b
    if-eqz v2, :cond_170

    #@16d
    const-string v4, "   -"

    #@16f
    goto :goto_10e

    #@170
    :cond_170
    const-string v4, "    "

    #@172
    goto :goto_10e

    #@173
    .line 720
    .end local v15           #M:I
    .end local v18           #j:I
    .end local v28           #tst:Lcom/android/internal/os/ProcessStats$Stats;
    :cond_173
    add-int/lit8 v17, v17, 0x1

    #@175
    goto/16 :goto_9c

    #@177
    .line 737
    .end local v25           #st:Lcom/android/internal/os/ProcessStats$Stats;
    :cond_177
    const-string v4, ""

    #@179
    const/4 v5, -0x1

    #@17a
    const-string v6, "TOTAL"

    #@17c
    move-object/from16 v0, p0

    #@17e
    iget v8, v0, Lcom/android/internal/os/ProcessStats;->mRelUserTime:I

    #@180
    move-object/from16 v0, p0

    #@182
    iget v9, v0, Lcom/android/internal/os/ProcessStats;->mRelSystemTime:I

    #@184
    move-object/from16 v0, p0

    #@186
    iget v10, v0, Lcom/android/internal/os/ProcessStats;->mRelIoWaitTime:I

    #@188
    move-object/from16 v0, p0

    #@18a
    iget v11, v0, Lcom/android/internal/os/ProcessStats;->mRelIrqTime:I

    #@18c
    move-object/from16 v0, p0

    #@18e
    iget v12, v0, Lcom/android/internal/os/ProcessStats;->mRelSoftIrqTime:I

    #@190
    const/4 v13, 0x0

    #@191
    const/4 v14, 0x0

    #@192
    move-object/from16 v2, p0

    #@194
    move/from16 v7, v27

    #@196
    invoke-direct/range {v2 .. v14}, Lcom/android/internal/os/ProcessStats;->printProcessCPU(Ljava/io/PrintWriter;Ljava/lang/String;ILjava/lang/String;IIIIIIII)V

    #@199
    .line 740
    invoke-virtual/range {v26 .. v26}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    #@19c
    move-result-object v2

    #@19d
    return-object v2
.end method

.method public update()V
    .registers 27

    #@0
    .prologue
    .line 272
    move-object/from16 v0, p0

    #@2
    iget-wide v3, v0, Lcom/android/internal/os/ProcessStats;->mCurrentSampleTime:J

    #@4
    move-object/from16 v0, p0

    #@6
    iput-wide v3, v0, Lcom/android/internal/os/ProcessStats;->mLastSampleTime:J

    #@8
    .line 273
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@b
    move-result-wide v3

    #@c
    move-object/from16 v0, p0

    #@e
    iput-wide v3, v0, Lcom/android/internal/os/ProcessStats;->mCurrentSampleTime:J

    #@10
    .line 274
    move-object/from16 v0, p0

    #@12
    iget-wide v3, v0, Lcom/android/internal/os/ProcessStats;->mCurrentSampleRealTime:J

    #@14
    move-object/from16 v0, p0

    #@16
    iput-wide v3, v0, Lcom/android/internal/os/ProcessStats;->mLastSampleRealTime:J

    #@18
    .line 275
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@1b
    move-result-wide v3

    #@1c
    move-object/from16 v0, p0

    #@1e
    iput-wide v3, v0, Lcom/android/internal/os/ProcessStats;->mCurrentSampleRealTime:J

    #@20
    .line 277
    move-object/from16 v0, p0

    #@22
    iget-object v0, v0, Lcom/android/internal/os/ProcessStats;->mSystemCpuData:[J

    #@24
    move-object/from16 v21, v0

    #@26
    .line 278
    .local v21, sysCpu:[J
    const-string v3, "/proc/stat"

    #@28
    sget-object v4, Lcom/android/internal/os/ProcessStats;->SYSTEM_CPU_FORMAT:[I

    #@2a
    const/4 v5, 0x0

    #@2b
    const/4 v6, 0x0

    #@2c
    move-object/from16 v0, v21

    #@2e
    invoke-static {v3, v4, v5, v0, v6}, Landroid/os/Process;->readProcFile(Ljava/lang/String;[I[Ljava/lang/String;[J[F)Z

    #@31
    move-result v3

    #@32
    if-eqz v3, :cond_ab

    #@34
    .line 281
    const/4 v3, 0x0

    #@35
    aget-wide v3, v21, v3

    #@37
    const/4 v5, 0x1

    #@38
    aget-wide v5, v21, v5

    #@3a
    add-long v24, v3, v5

    #@3c
    .line 283
    .local v24, usertime:J
    const/4 v3, 0x2

    #@3d
    aget-wide v22, v21, v3

    #@3f
    .line 285
    .local v22, systemtime:J
    const/4 v3, 0x3

    #@40
    aget-wide v9, v21, v3

    #@42
    .line 287
    .local v9, idletime:J
    const/4 v3, 0x4

    #@43
    aget-wide v11, v21, v3

    #@45
    .line 288
    .local v11, iowaittime:J
    const/4 v3, 0x5

    #@46
    aget-wide v13, v21, v3

    #@48
    .line 289
    .local v13, irqtime:J
    const/4 v3, 0x6

    #@49
    aget-wide v19, v21, v3

    #@4b
    .line 291
    .local v19, softirqtime:J
    move-object/from16 v0, p0

    #@4d
    iget-wide v3, v0, Lcom/android/internal/os/ProcessStats;->mBaseUserTime:J

    #@4f
    sub-long v3, v24, v3

    #@51
    long-to-int v3, v3

    #@52
    move-object/from16 v0, p0

    #@54
    iput v3, v0, Lcom/android/internal/os/ProcessStats;->mRelUserTime:I

    #@56
    .line 292
    move-object/from16 v0, p0

    #@58
    iget-wide v3, v0, Lcom/android/internal/os/ProcessStats;->mBaseSystemTime:J

    #@5a
    sub-long v3, v22, v3

    #@5c
    long-to-int v3, v3

    #@5d
    move-object/from16 v0, p0

    #@5f
    iput v3, v0, Lcom/android/internal/os/ProcessStats;->mRelSystemTime:I

    #@61
    .line 293
    move-object/from16 v0, p0

    #@63
    iget-wide v3, v0, Lcom/android/internal/os/ProcessStats;->mBaseIoWaitTime:J

    #@65
    sub-long v3, v11, v3

    #@67
    long-to-int v3, v3

    #@68
    move-object/from16 v0, p0

    #@6a
    iput v3, v0, Lcom/android/internal/os/ProcessStats;->mRelIoWaitTime:I

    #@6c
    .line 294
    move-object/from16 v0, p0

    #@6e
    iget-wide v3, v0, Lcom/android/internal/os/ProcessStats;->mBaseIrqTime:J

    #@70
    sub-long v3, v13, v3

    #@72
    long-to-int v3, v3

    #@73
    move-object/from16 v0, p0

    #@75
    iput v3, v0, Lcom/android/internal/os/ProcessStats;->mRelIrqTime:I

    #@77
    .line 295
    move-object/from16 v0, p0

    #@79
    iget-wide v3, v0, Lcom/android/internal/os/ProcessStats;->mBaseSoftIrqTime:J

    #@7b
    sub-long v3, v19, v3

    #@7d
    long-to-int v3, v3

    #@7e
    move-object/from16 v0, p0

    #@80
    iput v3, v0, Lcom/android/internal/os/ProcessStats;->mRelSoftIrqTime:I

    #@82
    .line 296
    move-object/from16 v0, p0

    #@84
    iget-wide v3, v0, Lcom/android/internal/os/ProcessStats;->mBaseIdleTime:J

    #@86
    sub-long v3, v9, v3

    #@88
    long-to-int v3, v3

    #@89
    move-object/from16 v0, p0

    #@8b
    iput v3, v0, Lcom/android/internal/os/ProcessStats;->mRelIdleTime:I

    #@8d
    .line 307
    move-wide/from16 v0, v24

    #@8f
    move-object/from16 v2, p0

    #@91
    iput-wide v0, v2, Lcom/android/internal/os/ProcessStats;->mBaseUserTime:J

    #@93
    .line 308
    move-wide/from16 v0, v22

    #@95
    move-object/from16 v2, p0

    #@97
    iput-wide v0, v2, Lcom/android/internal/os/ProcessStats;->mBaseSystemTime:J

    #@99
    .line 309
    move-object/from16 v0, p0

    #@9b
    iput-wide v11, v0, Lcom/android/internal/os/ProcessStats;->mBaseIoWaitTime:J

    #@9d
    .line 310
    move-object/from16 v0, p0

    #@9f
    iput-wide v13, v0, Lcom/android/internal/os/ProcessStats;->mBaseIrqTime:J

    #@a1
    .line 311
    move-wide/from16 v0, v19

    #@a3
    move-object/from16 v2, p0

    #@a5
    iput-wide v0, v2, Lcom/android/internal/os/ProcessStats;->mBaseSoftIrqTime:J

    #@a7
    .line 312
    move-object/from16 v0, p0

    #@a9
    iput-wide v9, v0, Lcom/android/internal/os/ProcessStats;->mBaseIdleTime:J

    #@ab
    .line 315
    .end local v9           #idletime:J
    .end local v11           #iowaittime:J
    .end local v13           #irqtime:J
    .end local v19           #softirqtime:J
    .end local v22           #systemtime:J
    .end local v24           #usertime:J
    :cond_ab
    const-string v4, "/proc"

    #@ad
    const/4 v5, -0x1

    #@ae
    move-object/from16 v0, p0

    #@b0
    iget-boolean v6, v0, Lcom/android/internal/os/ProcessStats;->mFirst:Z

    #@b2
    move-object/from16 v0, p0

    #@b4
    iget-object v7, v0, Lcom/android/internal/os/ProcessStats;->mCurPids:[I

    #@b6
    move-object/from16 v0, p0

    #@b8
    iget-object v8, v0, Lcom/android/internal/os/ProcessStats;->mProcStats:Ljava/util/ArrayList;

    #@ba
    move-object/from16 v3, p0

    #@bc
    invoke-direct/range {v3 .. v8}, Lcom/android/internal/os/ProcessStats;->collectStats(Ljava/lang/String;IZ[ILjava/util/ArrayList;)[I

    #@bf
    move-result-object v3

    #@c0
    move-object/from16 v0, p0

    #@c2
    iput-object v3, v0, Lcom/android/internal/os/ProcessStats;->mCurPids:[I

    #@c4
    .line 317
    move-object/from16 v0, p0

    #@c6
    iget-object v0, v0, Lcom/android/internal/os/ProcessStats;->mLoadAverageData:[F

    #@c8
    move-object/from16 v18, v0

    #@ca
    .line 318
    .local v18, loadAverages:[F
    const-string v3, "/proc/loadavg"

    #@cc
    sget-object v4, Lcom/android/internal/os/ProcessStats;->LOAD_AVERAGE_FORMAT:[I

    #@ce
    const/4 v5, 0x0

    #@cf
    const/4 v6, 0x0

    #@d0
    move-object/from16 v0, v18

    #@d2
    invoke-static {v3, v4, v5, v6, v0}, Landroid/os/Process;->readProcFile(Ljava/lang/String;[I[Ljava/lang/String;[J[F)Z

    #@d5
    move-result v3

    #@d6
    if-eqz v3, :cond_112

    #@d8
    .line 320
    const/4 v3, 0x0

    #@d9
    aget v15, v18, v3

    #@db
    .line 321
    .local v15, load1:F
    const/4 v3, 0x1

    #@dc
    aget v17, v18, v3

    #@de
    .line 322
    .local v17, load5:F
    const/4 v3, 0x2

    #@df
    aget v16, v18, v3

    #@e1
    .line 323
    .local v16, load15:F
    move-object/from16 v0, p0

    #@e3
    iget v3, v0, Lcom/android/internal/os/ProcessStats;->mLoad1:F

    #@e5
    cmpl-float v3, v15, v3

    #@e7
    if-nez v3, :cond_f9

    #@e9
    move-object/from16 v0, p0

    #@eb
    iget v3, v0, Lcom/android/internal/os/ProcessStats;->mLoad5:F

    #@ed
    cmpl-float v3, v17, v3

    #@ef
    if-nez v3, :cond_f9

    #@f1
    move-object/from16 v0, p0

    #@f3
    iget v3, v0, Lcom/android/internal/os/ProcessStats;->mLoad15:F

    #@f5
    cmpl-float v3, v16, v3

    #@f7
    if-eqz v3, :cond_112

    #@f9
    .line 324
    :cond_f9
    move-object/from16 v0, p0

    #@fb
    iput v15, v0, Lcom/android/internal/os/ProcessStats;->mLoad1:F

    #@fd
    .line 325
    move/from16 v0, v17

    #@ff
    move-object/from16 v1, p0

    #@101
    iput v0, v1, Lcom/android/internal/os/ProcessStats;->mLoad5:F

    #@103
    .line 326
    move/from16 v0, v16

    #@105
    move-object/from16 v1, p0

    #@107
    iput v0, v1, Lcom/android/internal/os/ProcessStats;->mLoad15:F

    #@109
    .line 327
    move-object/from16 v0, p0

    #@10b
    move/from16 v1, v17

    #@10d
    move/from16 v2, v16

    #@10f
    invoke-virtual {v0, v15, v1, v2}, Lcom/android/internal/os/ProcessStats;->onLoadChanged(FFF)V

    #@112
    .line 334
    .end local v15           #load1:F
    .end local v16           #load15:F
    .end local v17           #load5:F
    :cond_112
    const/4 v3, 0x0

    #@113
    move-object/from16 v0, p0

    #@115
    iput-boolean v3, v0, Lcom/android/internal/os/ProcessStats;->mWorkingProcsSorted:Z

    #@117
    .line 335
    const/4 v3, 0x0

    #@118
    move-object/from16 v0, p0

    #@11a
    iput-boolean v3, v0, Lcom/android/internal/os/ProcessStats;->mFirst:Z

    #@11c
    .line 336
    return-void
.end method
