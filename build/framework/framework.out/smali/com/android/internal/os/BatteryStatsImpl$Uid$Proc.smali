.class public final Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;
.super Landroid/os/BatteryStats$Uid$Proc;
.source "BatteryStatsImpl.java"

# interfaces
.implements Lcom/android/internal/os/BatteryStatsImpl$Unpluggable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/os/BatteryStatsImpl$Uid;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "Proc"
.end annotation


# instance fields
.field mExcessivePower:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;",
            ">;"
        }
    .end annotation
.end field

.field mForegroundTime:J

.field mLastForegroundTime:J

.field mLastStarts:I

.field mLastSystemTime:J

.field mLastUserTime:J

.field mLoadedForegroundTime:J

.field mLoadedStarts:I

.field mLoadedSystemTime:J

.field mLoadedUserTime:J

.field mSpeedBins:[Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

.field mStarts:I

.field mSystemTime:J

.field mUnpluggedForegroundTime:J

.field mUnpluggedStarts:I

.field mUnpluggedSystemTime:J

.field mUnpluggedUserTime:J

.field mUserTime:J

.field final synthetic this$1:Lcom/android/internal/os/BatteryStatsImpl$Uid;


# direct methods
.method constructor <init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 3254
    iput-object p1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->this$1:Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@2
    invoke-direct {p0}, Landroid/os/BatteryStats$Uid$Proc;-><init>()V

    #@5
    .line 3255
    iget-object v0, p1, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@7
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@9
    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c
    .line 3256
    iget-object v0, p1, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@e
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl;->getCpuSpeedSteps()I

    #@11
    move-result v0

    #@12
    new-array v0, v0, [Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@14
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSpeedBins:[Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@16
    .line 3257
    return-void
.end method


# virtual methods
.method public addCpuTimeLocked(II)V
    .registers 7
    .parameter "utime"
    .parameter "stime"

    #@0
    .prologue
    .line 3415
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mUserTime:J

    #@2
    int-to-long v2, p1

    #@3
    add-long/2addr v0, v2

    #@4
    iput-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mUserTime:J

    #@6
    .line 3416
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSystemTime:J

    #@8
    int-to-long v2, p2

    #@9
    add-long/2addr v0, v2

    #@a
    iput-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSystemTime:J

    #@c
    .line 3417
    return-void
.end method

.method public addExcessiveCpu(JJ)V
    .registers 7
    .parameter "overTime"
    .parameter "usedTime"

    #@0
    .prologue
    .line 3303
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mExcessivePower:Ljava/util/ArrayList;

    #@2
    if-nez v1, :cond_b

    #@4
    .line 3304
    new-instance v1, Ljava/util/ArrayList;

    #@6
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@9
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mExcessivePower:Ljava/util/ArrayList;

    #@b
    .line 3306
    :cond_b
    new-instance v0, Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;

    #@d
    invoke-direct {v0}, Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;-><init>()V

    #@10
    .line 3307
    .local v0, ew:Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;
    const/4 v1, 0x2

    #@11
    iput v1, v0, Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;->type:I

    #@13
    .line 3308
    iput-wide p1, v0, Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;->overTime:J

    #@15
    .line 3309
    iput-wide p3, v0, Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;->usedTime:J

    #@17
    .line 3310
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mExcessivePower:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1c
    .line 3311
    return-void
.end method

.method public addExcessiveWake(JJ)V
    .registers 7
    .parameter "overTime"
    .parameter "usedTime"

    #@0
    .prologue
    .line 3292
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mExcessivePower:Ljava/util/ArrayList;

    #@2
    if-nez v1, :cond_b

    #@4
    .line 3293
    new-instance v1, Ljava/util/ArrayList;

    #@6
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@9
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mExcessivePower:Ljava/util/ArrayList;

    #@b
    .line 3295
    :cond_b
    new-instance v0, Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;

    #@d
    invoke-direct {v0}, Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;-><init>()V

    #@10
    .line 3296
    .local v0, ew:Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;
    const/4 v1, 0x1

    #@11
    iput v1, v0, Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;->type:I

    #@13
    .line 3297
    iput-wide p1, v0, Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;->overTime:J

    #@15
    .line 3298
    iput-wide p3, v0, Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;->usedTime:J

    #@17
    .line 3299
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mExcessivePower:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1c
    .line 3300
    return-void
.end method

.method public addForegroundTimeLocked(J)V
    .registers 5
    .parameter "ttime"

    #@0
    .prologue
    .line 3420
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mForegroundTime:J

    #@2
    add-long/2addr v0, p1

    #@3
    iput-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mForegroundTime:J

    #@5
    .line 3421
    return-void
.end method

.method public addSpeedStepTimes([J)V
    .registers 8
    .parameter "values"

    #@0
    .prologue
    .line 3493
    const/4 v3, 0x0

    #@1
    .local v3, i:I
    :goto_1
    iget-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSpeedBins:[Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@3
    array-length v4, v4

    #@4
    if-ge v3, v4, :cond_2e

    #@6
    array-length v4, p1

    #@7
    if-ge v3, v4, :cond_2e

    #@9
    .line 3494
    aget-wide v0, p1, v3

    #@b
    .line 3495
    .local v0, amt:J
    const-wide/16 v4, 0x0

    #@d
    cmp-long v4, v0, v4

    #@f
    if-eqz v4, :cond_2b

    #@11
    .line 3496
    iget-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSpeedBins:[Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@13
    aget-object v2, v4, v3

    #@15
    .line 3497
    .local v2, c:Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;
    if-nez v2, :cond_26

    #@17
    .line 3498
    iget-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSpeedBins:[Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@19
    new-instance v2, Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@1b
    .end local v2           #c:Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;
    iget-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->this$1:Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@1d
    iget-object v5, v5, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@1f
    iget-object v5, v5, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@21
    invoke-direct {v2, v5}, Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;-><init>(Ljava/util/ArrayList;)V

    #@24
    .restart local v2       #c:Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;
    aput-object v2, v4, v3

    #@26
    .line 3500
    :cond_26
    aget-wide v4, p1, v3

    #@28
    invoke-virtual {v2, v4, v5}, Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;->addCountAtomic(J)V

    #@2b
    .line 3493
    .end local v2           #c:Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;
    :cond_2b
    add-int/lit8 v3, v3, 0x1

    #@2d
    goto :goto_1

    #@2e
    .line 3503
    .end local v0           #amt:J
    :cond_2e
    return-void
.end method

.method public countExcessivePowers()I
    .registers 2

    #@0
    .prologue
    .line 3281
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mExcessivePower:Ljava/util/ArrayList;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mExcessivePower:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v0

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method detach()V
    .registers 5

    #@0
    .prologue
    .line 3270
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->this$1:Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@2
    iget-object v2, v2, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@4
    iget-object v2, v2, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v2, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@9
    .line 3271
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    :goto_a
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSpeedBins:[Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@c
    array-length v2, v2

    #@d
    if-ge v1, v2, :cond_26

    #@f
    .line 3272
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSpeedBins:[Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@11
    aget-object v0, v2, v1

    #@13
    .line 3273
    .local v0, c:Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;
    if-eqz v0, :cond_23

    #@15
    .line 3274
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->this$1:Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@17
    iget-object v2, v2, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@19
    iget-object v2, v2, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@1b
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@1e
    .line 3275
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSpeedBins:[Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@20
    const/4 v3, 0x0

    #@21
    aput-object v3, v2, v1

    #@23
    .line 3271
    :cond_23
    add-int/lit8 v1, v1, 0x1

    #@25
    goto :goto_a

    #@26
    .line 3278
    .end local v0           #c:Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;
    :cond_26
    return-void
.end method

.method public getBatteryStats()Lcom/android/internal/os/BatteryStatsImpl;
    .registers 2

    #@0
    .prologue
    .line 3411
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->this$1:Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@2
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@4
    return-object v0
.end method

.method public getExcessivePower(I)Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;
    .registers 3
    .parameter "i"

    #@0
    .prologue
    .line 3285
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mExcessivePower:Ljava/util/ArrayList;

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 3286
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mExcessivePower:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;

    #@c
    .line 3288
    :goto_c
    return-object v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method public getForegroundTime(I)J
    .registers 6
    .parameter "which"

    #@0
    .prologue
    .line 3462
    const/4 v2, 0x1

    #@1
    if-ne p1, v2, :cond_6

    #@3
    .line 3463
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mLastForegroundTime:J

    #@5
    .line 3472
    .local v0, val:J
    :cond_5
    :goto_5
    return-wide v0

    #@6
    .line 3465
    .end local v0           #val:J
    :cond_6
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mForegroundTime:J

    #@8
    .line 3466
    .restart local v0       #val:J
    const/4 v2, 0x2

    #@9
    if-ne p1, v2, :cond_f

    #@b
    .line 3467
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mLoadedForegroundTime:J

    #@d
    sub-long/2addr v0, v2

    #@e
    goto :goto_5

    #@f
    .line 3468
    :cond_f
    const/4 v2, 0x3

    #@10
    if-ne p1, v2, :cond_5

    #@12
    .line 3469
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mUnpluggedForegroundTime:J

    #@14
    sub-long/2addr v0, v2

    #@15
    goto :goto_5
.end method

.method public getStarts(I)I
    .registers 4
    .parameter "which"

    #@0
    .prologue
    .line 3478
    const/4 v1, 0x1

    #@1
    if-ne p1, v1, :cond_6

    #@3
    .line 3479
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mLastStarts:I

    #@5
    .line 3488
    .local v0, val:I
    :cond_5
    :goto_5
    return v0

    #@6
    .line 3481
    .end local v0           #val:I
    :cond_6
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mStarts:I

    #@8
    .line 3482
    .restart local v0       #val:I
    const/4 v1, 0x2

    #@9
    if-ne p1, v1, :cond_f

    #@b
    .line 3483
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mLoadedStarts:I

    #@d
    sub-int/2addr v0, v1

    #@e
    goto :goto_5

    #@f
    .line 3484
    :cond_f
    const/4 v1, 0x3

    #@10
    if-ne p1, v1, :cond_5

    #@12
    .line 3485
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mUnpluggedStarts:I

    #@14
    sub-int/2addr v0, v1

    #@15
    goto :goto_5
.end method

.method public getSystemTime(I)J
    .registers 6
    .parameter "which"

    #@0
    .prologue
    .line 3446
    const/4 v2, 0x1

    #@1
    if-ne p1, v2, :cond_6

    #@3
    .line 3447
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mLastSystemTime:J

    #@5
    .line 3456
    .local v0, val:J
    :cond_5
    :goto_5
    return-wide v0

    #@6
    .line 3449
    .end local v0           #val:J
    :cond_6
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSystemTime:J

    #@8
    .line 3450
    .restart local v0       #val:J
    const/4 v2, 0x2

    #@9
    if-ne p1, v2, :cond_f

    #@b
    .line 3451
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mLoadedSystemTime:J

    #@d
    sub-long/2addr v0, v2

    #@e
    goto :goto_5

    #@f
    .line 3452
    :cond_f
    const/4 v2, 0x3

    #@10
    if-ne p1, v2, :cond_5

    #@12
    .line 3453
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mUnpluggedSystemTime:J

    #@14
    sub-long/2addr v0, v2

    #@15
    goto :goto_5
.end method

.method public getTimeAtCpuSpeedStep(II)J
    .registers 7
    .parameter "speedStep"
    .parameter "which"

    #@0
    .prologue
    const-wide/16 v1, 0x0

    #@2
    .line 3507
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSpeedBins:[Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@4
    array-length v3, v3

    #@5
    if-ge p1, v3, :cond_12

    #@7
    .line 3508
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSpeedBins:[Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@9
    aget-object v0, v3, p1

    #@b
    .line 3509
    .local v0, c:Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;
    if-eqz v0, :cond_12

    #@d
    invoke-virtual {v0, p2}, Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;->getCountLocked(I)I

    #@10
    move-result v1

    #@11
    int-to-long v1, v1

    #@12
    .line 3511
    .end local v0           #c:Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;
    :cond_12
    return-wide v1
.end method

.method public getUserTime(I)J
    .registers 6
    .parameter "which"

    #@0
    .prologue
    .line 3430
    const/4 v2, 0x1

    #@1
    if-ne p1, v2, :cond_6

    #@3
    .line 3431
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mLastUserTime:J

    #@5
    .line 3440
    .local v0, val:J
    :cond_5
    :goto_5
    return-wide v0

    #@6
    .line 3433
    .end local v0           #val:J
    :cond_6
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mUserTime:J

    #@8
    .line 3434
    .restart local v0       #val:J
    const/4 v2, 0x2

    #@9
    if-ne p1, v2, :cond_f

    #@b
    .line 3435
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mLoadedUserTime:J

    #@d
    sub-long/2addr v0, v2

    #@e
    goto :goto_5

    #@f
    .line 3436
    :cond_f
    const/4 v2, 0x3

    #@10
    if-ne p1, v2, :cond_5

    #@12
    .line 3437
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mUnpluggedUserTime:J

    #@14
    sub-long/2addr v0, v2

    #@15
    goto :goto_5
.end method

.method public incStartsLocked()V
    .registers 2

    #@0
    .prologue
    .line 3424
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mStarts:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mStarts:I

    #@6
    .line 3425
    return-void
.end method

.method public plug(JJ)V
    .registers 5
    .parameter "batteryUptime"
    .parameter "batteryRealtime"

    #@0
    .prologue
    .line 3267
    return-void
.end method

.method readExcessivePowerFromParcelLocked(Landroid/os/Parcel;)Z
    .registers 8
    .parameter "in"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 3330
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@4
    move-result v0

    #@5
    .line 3331
    .local v0, N:I
    if-nez v0, :cond_b

    #@7
    .line 3332
    const/4 v4, 0x0

    #@8
    iput-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mExcessivePower:Ljava/util/ArrayList;

    #@a
    .line 3349
    :cond_a
    :goto_a
    return v3

    #@b
    .line 3336
    :cond_b
    const/16 v4, 0x2710

    #@d
    if-le v0, v4, :cond_29

    #@f
    .line 3337
    const-string v3, "BatteryStatsImpl"

    #@11
    new-instance v4, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v5, "File corrupt: too many excessive power entries "

    #@18
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v4

    #@20
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v4

    #@24
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 3338
    const/4 v3, 0x0

    #@28
    goto :goto_a

    #@29
    .line 3341
    :cond_29
    new-instance v4, Ljava/util/ArrayList;

    #@2b
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@2e
    iput-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mExcessivePower:Ljava/util/ArrayList;

    #@30
    .line 3342
    const/4 v2, 0x0

    #@31
    .local v2, i:I
    :goto_31
    if-ge v2, v0, :cond_a

    #@33
    .line 3343
    new-instance v1, Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;

    #@35
    invoke-direct {v1}, Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;-><init>()V

    #@38
    .line 3344
    .local v1, ew:Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3b
    move-result v4

    #@3c
    iput v4, v1, Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;->type:I

    #@3e
    .line 3345
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@41
    move-result-wide v4

    #@42
    iput-wide v4, v1, Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;->overTime:J

    #@44
    .line 3346
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@47
    move-result-wide v4

    #@48
    iput-wide v4, v1, Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;->usedTime:J

    #@4a
    .line 3347
    iget-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mExcessivePower:Ljava/util/ArrayList;

    #@4c
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4f
    .line 3342
    add-int/lit8 v2, v2, 0x1

    #@51
    goto :goto_31
.end method

.method readFromParcelLocked(Landroid/os/Parcel;)V
    .registers 9
    .parameter "in"

    #@0
    .prologue
    const-wide/16 v5, 0x0

    #@2
    .line 3381
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@5
    move-result-wide v3

    #@6
    iput-wide v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mUserTime:J

    #@8
    .line 3382
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@b
    move-result-wide v3

    #@c
    iput-wide v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSystemTime:J

    #@e
    .line 3383
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@11
    move-result-wide v3

    #@12
    iput-wide v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mForegroundTime:J

    #@14
    .line 3384
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v3

    #@18
    iput v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mStarts:I

    #@1a
    .line 3385
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@1d
    move-result-wide v3

    #@1e
    iput-wide v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mLoadedUserTime:J

    #@20
    .line 3386
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@23
    move-result-wide v3

    #@24
    iput-wide v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mLoadedSystemTime:J

    #@26
    .line 3387
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@29
    move-result-wide v3

    #@2a
    iput-wide v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mLoadedForegroundTime:J

    #@2c
    .line 3388
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2f
    move-result v3

    #@30
    iput v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mLoadedStarts:I

    #@32
    .line 3389
    iput-wide v5, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mLastUserTime:J

    #@34
    .line 3390
    iput-wide v5, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mLastSystemTime:J

    #@36
    .line 3391
    iput-wide v5, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mLastForegroundTime:J

    #@38
    .line 3392
    const/4 v3, 0x0

    #@39
    iput v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mLastStarts:I

    #@3b
    .line 3393
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@3e
    move-result-wide v3

    #@3f
    iput-wide v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mUnpluggedUserTime:J

    #@41
    .line 3394
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@44
    move-result-wide v3

    #@45
    iput-wide v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mUnpluggedSystemTime:J

    #@47
    .line 3395
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@4a
    move-result-wide v3

    #@4b
    iput-wide v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mUnpluggedForegroundTime:J

    #@4d
    .line 3396
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@50
    move-result v3

    #@51
    iput v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mUnpluggedStarts:I

    #@53
    .line 3398
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@56
    move-result v0

    #@57
    .line 3399
    .local v0, bins:I
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->this$1:Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@59
    iget-object v3, v3, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@5b
    invoke-virtual {v3}, Lcom/android/internal/os/BatteryStatsImpl;->getCpuSpeedSteps()I

    #@5e
    move-result v2

    #@5f
    .line 3400
    .local v2, steps:I
    if-lt v0, v2, :cond_62

    #@61
    move v2, v0

    #@62
    .end local v2           #steps:I
    :cond_62
    new-array v3, v2, [Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@64
    iput-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSpeedBins:[Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@66
    .line 3401
    const/4 v1, 0x0

    #@67
    .local v1, i:I
    :goto_67
    if-ge v1, v0, :cond_81

    #@69
    .line 3402
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6c
    move-result v3

    #@6d
    if-eqz v3, :cond_7e

    #@6f
    .line 3403
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSpeedBins:[Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@71
    new-instance v4, Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@73
    iget-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->this$1:Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@75
    iget-object v5, v5, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@77
    iget-object v5, v5, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@79
    invoke-direct {v4, v5, p1}, Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;-><init>(Ljava/util/ArrayList;Landroid/os/Parcel;)V

    #@7c
    aput-object v4, v3, v1

    #@7e
    .line 3401
    :cond_7e
    add-int/lit8 v1, v1, 0x1

    #@80
    goto :goto_67

    #@81
    .line 3407
    :cond_81
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->readExcessivePowerFromParcelLocked(Landroid/os/Parcel;)Z

    #@84
    .line 3408
    return-void
.end method

.method public unplug(JJ)V
    .registers 7
    .parameter "batteryUptime"
    .parameter "batteryRealtime"

    #@0
    .prologue
    .line 3260
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mUserTime:J

    #@2
    iput-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mUnpluggedUserTime:J

    #@4
    .line 3261
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSystemTime:J

    #@6
    iput-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mUnpluggedSystemTime:J

    #@8
    .line 3262
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mStarts:I

    #@a
    iput v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mUnpluggedStarts:I

    #@c
    .line 3263
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mForegroundTime:J

    #@e
    iput-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mUnpluggedForegroundTime:J

    #@10
    .line 3264
    return-void
.end method

.method writeExcessivePowerToParcelLocked(Landroid/os/Parcel;)V
    .registers 7
    .parameter "out"

    #@0
    .prologue
    .line 3314
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mExcessivePower:Ljava/util/ArrayList;

    #@2
    if-nez v3, :cond_9

    #@4
    .line 3315
    const/4 v3, 0x0

    #@5
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@8
    .line 3327
    :cond_8
    return-void

    #@9
    .line 3319
    :cond_9
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mExcessivePower:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@e
    move-result v0

    #@f
    .line 3320
    .local v0, N:I
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 3321
    const/4 v2, 0x0

    #@13
    .local v2, i:I
    :goto_13
    if-ge v2, v0, :cond_8

    #@15
    .line 3322
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mExcessivePower:Ljava/util/ArrayList;

    #@17
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1a
    move-result-object v1

    #@1b
    check-cast v1, Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;

    #@1d
    .line 3323
    .local v1, ew:Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;
    iget v3, v1, Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;->type:I

    #@1f
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@22
    .line 3324
    iget-wide v3, v1, Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;->overTime:J

    #@24
    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    #@27
    .line 3325
    iget-wide v3, v1, Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;->usedTime:J

    #@29
    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    #@2c
    .line 3321
    add-int/lit8 v2, v2, 0x1

    #@2e
    goto :goto_13
.end method

.method writeToParcelLocked(Landroid/os/Parcel;)V
    .registers 6
    .parameter "out"

    #@0
    .prologue
    .line 3353
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mUserTime:J

    #@2
    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    #@5
    .line 3354
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSystemTime:J

    #@7
    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    #@a
    .line 3355
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mForegroundTime:J

    #@c
    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    #@f
    .line 3356
    iget v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mStarts:I

    #@11
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 3357
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mLoadedUserTime:J

    #@16
    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    #@19
    .line 3358
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mLoadedSystemTime:J

    #@1b
    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    #@1e
    .line 3359
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mLoadedForegroundTime:J

    #@20
    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    #@23
    .line 3360
    iget v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mLoadedStarts:I

    #@25
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@28
    .line 3361
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mUnpluggedUserTime:J

    #@2a
    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    #@2d
    .line 3362
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mUnpluggedSystemTime:J

    #@2f
    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    #@32
    .line 3363
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mUnpluggedForegroundTime:J

    #@34
    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    #@37
    .line 3364
    iget v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mUnpluggedStarts:I

    #@39
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@3c
    .line 3366
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSpeedBins:[Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@3e
    array-length v2, v2

    #@3f
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@42
    .line 3367
    const/4 v1, 0x0

    #@43
    .local v1, i:I
    :goto_43
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSpeedBins:[Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@45
    array-length v2, v2

    #@46
    if-ge v1, v2, :cond_5d

    #@48
    .line 3368
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSpeedBins:[Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@4a
    aget-object v0, v2, v1

    #@4c
    .line 3369
    .local v0, c:Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;
    if-eqz v0, :cond_58

    #@4e
    .line 3370
    const/4 v2, 0x1

    #@4f
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@52
    .line 3371
    invoke-virtual {v0, p1}, Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;->writeToParcel(Landroid/os/Parcel;)V

    #@55
    .line 3367
    :goto_55
    add-int/lit8 v1, v1, 0x1

    #@57
    goto :goto_43

    #@58
    .line 3373
    :cond_58
    const/4 v2, 0x0

    #@59
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@5c
    goto :goto_55

    #@5d
    .line 3377
    .end local v0           #c:Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;
    :cond_5d
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->writeExcessivePowerToParcelLocked(Landroid/os/Parcel;)V

    #@60
    .line 3378
    return-void
.end method
