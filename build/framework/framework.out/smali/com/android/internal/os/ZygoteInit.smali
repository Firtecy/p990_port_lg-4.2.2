.class public Lcom/android/internal/os/ZygoteInit;
.super Ljava/lang/Object;
.source "ZygoteInit.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller;
    }
.end annotation


# static fields
.field private static final ANDROID_SOCKET_ENV:Ljava/lang/String; = "ANDROID_SOCKET_zygote"

.field static final GC_LOOP_COUNT:I = 0xa

.field private static final LOG_BOOT_PROGRESS_PRELOAD_END:I = 0xbd6

.field private static final LOG_BOOT_PROGRESS_PRELOAD_START:I = 0xbcc

.field private static final PRELOADED_CLASSES:Ljava/lang/String; = "preloaded-classes"

.field private static final PRELOAD_GC_THRESHOLD:I = 0xc350

.field private static final PRELOAD_RESOURCES:Z = true

.field private static final ROOT_GID:I = 0x0

.field private static final ROOT_UID:I = 0x0

.field private static final TAG:Ljava/lang/String; = "Zygote"

.field private static final UNPRIVILEGED_GID:I = 0x270f

.field private static final UNPRIVILEGED_UID:I = 0x270f

.field public static final USAGE_STRING:Ljava/lang/String; = " <\"start-system-server\"|\"\" for startSystemServer>"

.field private static final ZYGOTE_FORK_MODE:Z

.field private static mResources:Landroid/content/res/Resources;

.field private static sServerSocket:Landroid/net/LocalServerSocket;


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 774
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 775
    return-void
.end method

.method private static acceptCommandPeer()Lcom/android/internal/os/ZygoteConnection;
    .registers 3

    #@0
    .prologue
    .line 184
    :try_start_0
    new-instance v1, Lcom/android/internal/os/ZygoteConnection;

    #@2
    sget-object v2, Lcom/android/internal/os/ZygoteInit;->sServerSocket:Landroid/net/LocalServerSocket;

    #@4
    invoke-virtual {v2}, Landroid/net/LocalServerSocket;->accept()Landroid/net/LocalSocket;

    #@7
    move-result-object v2

    #@8
    invoke-direct {v1, v2}, Lcom/android/internal/os/ZygoteConnection;-><init>(Landroid/net/LocalSocket;)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_b} :catch_c

    #@b
    return-object v1

    #@c
    .line 185
    :catch_c
    move-exception v0

    #@d
    .line 186
    .local v0, ex:Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@f
    const-string v2, "IOException during accept()"

    #@11
    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@14
    throw v1
.end method

.method static native capgetPermitted(I)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method static closeServerSocket()V
    .registers 3

    #@0
    .prologue
    .line 197
    :try_start_0
    sget-object v1, Lcom/android/internal/os/ZygoteInit;->sServerSocket:Landroid/net/LocalServerSocket;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 198
    sget-object v1, Lcom/android/internal/os/ZygoteInit;->sServerSocket:Landroid/net/LocalServerSocket;

    #@6
    invoke-virtual {v1}, Landroid/net/LocalServerSocket;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_9} :catch_d

    #@9
    .line 204
    .local v0, ex:Ljava/io/IOException;
    :cond_9
    :goto_9
    const/4 v1, 0x0

    #@a
    sput-object v1, Lcom/android/internal/os/ZygoteInit;->sServerSocket:Landroid/net/LocalServerSocket;

    #@c
    .line 205
    return-void

    #@d
    .line 200
    .end local v0           #ex:Ljava/io/IOException;
    :catch_d
    move-exception v0

    #@e
    .line 201
    .restart local v0       #ex:Ljava/io/IOException;
    const-string v1, "Zygote"

    #@10
    const-string v2, "Zygote:  error closing sockets"

    #@12
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@15
    goto :goto_9
.end method

.method static native createFileDescriptor(I)Ljava/io/FileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method static gc()V
    .registers 1

    #@0
    .prologue
    .line 436
    invoke-static {}, Ldalvik/system/VMRuntime;->getRuntime()Ldalvik/system/VMRuntime;

    #@3
    move-result-object v0

    #@4
    .line 441
    .local v0, runtime:Ldalvik/system/VMRuntime;
    invoke-static {}, Ljava/lang/System;->gc()V

    #@7
    .line 442
    invoke-virtual {v0}, Ldalvik/system/VMRuntime;->runFinalizationSync()V

    #@a
    .line 443
    invoke-static {}, Ljava/lang/System;->gc()V

    #@d
    .line 444
    invoke-virtual {v0}, Ldalvik/system/VMRuntime;->runFinalizationSync()V

    #@10
    .line 445
    invoke-static {}, Ljava/lang/System;->gc()V

    #@13
    .line 446
    invoke-virtual {v0}, Ldalvik/system/VMRuntime;->runFinalizationSync()V

    #@16
    .line 447
    return-void
.end method

.method static native getpgid(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method private static handleSystemServerProcess(Lcom/android/internal/os/ZygoteConnection$Arguments;)V
    .registers 6
    .parameter "parsedArgs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller;
        }
    .end annotation

    #@0
    .prologue
    .line 456
    invoke-static {}, Lcom/android/internal/os/ZygoteInit;->closeServerSocket()V

    #@3
    .line 459
    sget-object v0, Llibcore/io/Libcore;->os:Llibcore/io/Os;

    #@5
    sget v1, Llibcore/io/OsConstants;->S_IRWXG:I

    #@7
    sget v2, Llibcore/io/OsConstants;->S_IRWXO:I

    #@9
    or-int/2addr v1, v2

    #@a
    invoke-interface {v0, v1}, Llibcore/io/Os;->umask(I)I

    #@d
    .line 461
    iget-object v0, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->niceName:Ljava/lang/String;

    #@f
    if-eqz v0, :cond_16

    #@11
    .line 462
    iget-object v0, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->niceName:Ljava/lang/String;

    #@13
    invoke-static {v0}, Landroid/os/Process;->setArgV0(Ljava/lang/String;)V

    #@16
    .line 465
    :cond_16
    iget-object v0, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->invokeWith:Ljava/lang/String;

    #@18
    if-eqz v0, :cond_27

    #@1a
    .line 466
    iget-object v0, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->invokeWith:Ljava/lang/String;

    #@1c
    iget-object v1, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->niceName:Ljava/lang/String;

    #@1e
    iget v2, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->targetSdkVersion:I

    #@20
    const/4 v3, 0x0

    #@21
    iget-object v4, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->remainingArgs:[Ljava/lang/String;

    #@23
    invoke-static {v0, v1, v2, v3, v4}, Lcom/android/internal/os/WrapperInit;->execApplication(Ljava/lang/String;Ljava/lang/String;ILjava/io/FileDescriptor;[Ljava/lang/String;)V

    #@26
    .line 477
    :goto_26
    return-void

    #@27
    .line 473
    :cond_27
    iget v0, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->targetSdkVersion:I

    #@29
    iget-object v1, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->remainingArgs:[Ljava/lang/String;

    #@2b
    invoke-static {v0, v1}, Lcom/android/internal/os/RuntimeInit;->zygoteInit(I[Ljava/lang/String;)V

    #@2e
    goto :goto_26
.end method

.method static invokeStaticMain(Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/String;)V
    .registers 11
    .parameter "loader"
    .parameter "className"
    .parameter "argv"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller;
        }
    .end annotation

    #@0
    .prologue
    .line 119
    :try_start_0
    invoke-virtual {p0, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_3} :catch_3c

    #@3
    move-result-object v0

    #@4
    .line 128
    .local v0, cl:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :try_start_4
    const-string/jumbo v4, "main"

    #@7
    const/4 v5, 0x1

    #@8
    new-array v5, v5, [Ljava/lang/Class;

    #@a
    const/4 v6, 0x0

    #@b
    const-class v7, [Ljava/lang/String;

    #@d
    aput-object v7, v5, v6

    #@f
    invoke-virtual {v0, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_12
    .catch Ljava/lang/NoSuchMethodException; {:try_start_4 .. :try_end_12} :catch_56
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_12} :catch_70

    #@12
    move-result-object v2

    #@13
    .line 137
    .local v2, m:Ljava/lang/reflect/Method;
    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getModifiers()I

    #@16
    move-result v3

    #@17
    .line 138
    .local v3, modifiers:I
    invoke-static {v3}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    #@1a
    move-result v4

    #@1b
    if-eqz v4, :cond_23

    #@1d
    invoke-static {v3}, Ljava/lang/reflect/Modifier;->isPublic(I)Z

    #@20
    move-result v4

    #@21
    if-nez v4, :cond_8a

    #@23
    .line 139
    :cond_23
    new-instance v4, Ljava/lang/RuntimeException;

    #@25
    new-instance v5, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v6, "Main method is not public and static on "

    #@2c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v5

    #@30
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v5

    #@34
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v5

    #@38
    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@3b
    throw v4

    #@3c
    .line 120
    .end local v0           #cl:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    .end local v2           #m:Ljava/lang/reflect/Method;
    .end local v3           #modifiers:I
    :catch_3c
    move-exception v1

    #@3d
    .line 121
    .local v1, ex:Ljava/lang/ClassNotFoundException;
    new-instance v4, Ljava/lang/RuntimeException;

    #@3f
    new-instance v5, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v6, "Missing class when invoking static main "

    #@46
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v5

    #@4a
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v5

    #@4e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v5

    #@52
    invoke-direct {v4, v5, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@55
    throw v4

    #@56
    .line 129
    .end local v1           #ex:Ljava/lang/ClassNotFoundException;
    .restart local v0       #cl:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :catch_56
    move-exception v1

    #@57
    .line 130
    .local v1, ex:Ljava/lang/NoSuchMethodException;
    new-instance v4, Ljava/lang/RuntimeException;

    #@59
    new-instance v5, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v6, "Missing static main on "

    #@60
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v5

    #@64
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v5

    #@68
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v5

    #@6c
    invoke-direct {v4, v5, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@6f
    throw v4

    #@70
    .line 132
    .end local v1           #ex:Ljava/lang/NoSuchMethodException;
    :catch_70
    move-exception v1

    #@71
    .line 133
    .local v1, ex:Ljava/lang/SecurityException;
    new-instance v4, Ljava/lang/RuntimeException;

    #@73
    new-instance v5, Ljava/lang/StringBuilder;

    #@75
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@78
    const-string v6, "Problem getting static main on "

    #@7a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v5

    #@7e
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v5

    #@82
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v5

    #@86
    invoke-direct {v4, v5, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@89
    throw v4

    #@8a
    .line 149
    .end local v1           #ex:Ljava/lang/SecurityException;
    .restart local v2       #m:Ljava/lang/reflect/Method;
    .restart local v3       #modifiers:I
    :cond_8a
    new-instance v4, Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller;

    #@8c
    invoke-direct {v4, v2, p2}, Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller;-><init>(Ljava/lang/reflect/Method;[Ljava/lang/String;)V

    #@8f
    throw v4
.end method

.method public static main([Ljava/lang/String;)V
    .registers 6
    .parameter "argv"

    #@0
    .prologue
    .line 528
    :try_start_0
    invoke-static {}, Lcom/android/internal/os/SamplingProfilerIntegration;->start()V

    #@3
    .line 530
    invoke-static {}, Lcom/android/internal/os/ZygoteInit;->registerZygoteSocket()V

    #@6
    .line 531
    const/16 v2, 0xbcc

    #@8
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@b
    move-result-wide v3

    #@c
    invoke-static {v2, v3, v4}, Landroid/util/EventLog;->writeEvent(IJ)I

    #@f
    .line 533
    invoke-static {}, Lcom/android/internal/os/ZygoteInit;->preload()V

    #@12
    .line 534
    const/16 v2, 0xbd6

    #@14
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@17
    move-result-wide v3

    #@18
    invoke-static {v2, v3, v4}, Landroid/util/EventLog;->writeEvent(IJ)I

    #@1b
    .line 538
    invoke-static {}, Lcom/android/internal/os/SamplingProfilerIntegration;->writeZygoteSnapshot()V

    #@1e
    .line 541
    invoke-static {}, Lcom/android/internal/os/ZygoteInit;->gc()V

    #@21
    .line 544
    array-length v2, p0

    #@22
    const/4 v3, 0x2

    #@23
    if-eq v2, v3, :cond_46

    #@25
    .line 545
    new-instance v2, Ljava/lang/RuntimeException;

    #@27
    new-instance v3, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const/4 v4, 0x0

    #@2d
    aget-object v4, p0, v4

    #@2f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    const-string v4, " <\"start-system-server\"|\"\" for startSystemServer>"

    #@35
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v3

    #@3d
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@40
    throw v2
    :try_end_41
    .catch Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller; {:try_start_0 .. :try_end_41} :catch_41
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_41} :catch_63

    #@41
    .line 563
    :catch_41
    move-exception v0

    #@42
    .line 564
    .local v0, caller:Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller;
    invoke-virtual {v0}, Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller;->run()V

    #@45
    .line 570
    .end local v0           #caller:Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller;
    :goto_45
    return-void

    #@46
    .line 548
    :cond_46
    const/4 v2, 0x1

    #@47
    :try_start_47
    aget-object v2, p0, v2

    #@49
    const-string/jumbo v3, "start-system-server"

    #@4c
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4f
    move-result v2

    #@50
    if-eqz v2, :cond_6f

    #@52
    .line 549
    invoke-static {}, Lcom/android/internal/os/ZygoteInit;->startSystemServer()Z

    #@55
    .line 554
    :cond_55
    const-string v2, "Zygote"

    #@57
    const-string v3, "Accepting command socket connections"

    #@59
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@5c
    .line 559
    invoke-static {}, Lcom/android/internal/os/ZygoteInit;->runSelectLoopMode()V

    #@5f
    .line 562
    invoke-static {}, Lcom/android/internal/os/ZygoteInit;->closeServerSocket()V
    :try_end_62
    .catch Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller; {:try_start_47 .. :try_end_62} :catch_41
    .catch Ljava/lang/RuntimeException; {:try_start_47 .. :try_end_62} :catch_63

    #@62
    goto :goto_45

    #@63
    .line 565
    :catch_63
    move-exception v1

    #@64
    .line 566
    .local v1, ex:Ljava/lang/RuntimeException;
    const-string v2, "Zygote"

    #@66
    const-string v3, "Zygote died with exception"

    #@68
    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@6b
    .line 567
    invoke-static {}, Lcom/android/internal/os/ZygoteInit;->closeServerSocket()V

    #@6e
    .line 568
    throw v1

    #@6f
    .line 550
    .end local v1           #ex:Ljava/lang/RuntimeException;
    :cond_6f
    const/4 v2, 0x1

    #@70
    :try_start_70
    aget-object v2, p0, v2

    #@72
    const-string v3, ""

    #@74
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@77
    move-result v2

    #@78
    if-nez v2, :cond_55

    #@7a
    .line 551
    new-instance v2, Ljava/lang/RuntimeException;

    #@7c
    new-instance v3, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    const/4 v4, 0x0

    #@82
    aget-object v4, p0, v4

    #@84
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v3

    #@88
    const-string v4, " <\"start-system-server\"|\"\" for startSystemServer>"

    #@8a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v3

    #@8e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@91
    move-result-object v3

    #@92
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@95
    throw v2
    :try_end_96
    .catch Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller; {:try_start_70 .. :try_end_96} :catch_41
    .catch Ljava/lang/RuntimeException; {:try_start_70 .. :try_end_96} :catch_63
.end method

.method static preload()V
    .registers 0

    #@0
    .prologue
    .line 234
    invoke-static {}, Lcom/android/internal/os/ZygoteInit;->preloadClasses()V

    #@3
    .line 235
    invoke-static {}, Lcom/android/internal/os/ZygoteInit;->preloadResources()V

    #@6
    .line 236
    return-void
.end method

.method private static preloadClasses()V
    .registers 15

    #@0
    .prologue
    const/16 v12, 0x270f

    #@2
    const/4 v14, 0x0

    #@3
    .line 246
    invoke-static {}, Ldalvik/system/VMRuntime;->getRuntime()Ldalvik/system/VMRuntime;

    #@6
    move-result-object v6

    #@7
    .line 248
    .local v6, runtime:Ldalvik/system/VMRuntime;
    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    #@a
    move-result-object v10

    #@b
    const-string/jumbo v11, "preloaded-classes"

    #@e
    invoke-virtual {v10, v11}, Ljava/lang/ClassLoader;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    #@11
    move-result-object v4

    #@12
    .line 250
    .local v4, is:Ljava/io/InputStream;
    if-nez v4, :cond_1c

    #@14
    .line 251
    const-string v10, "Zygote"

    #@16
    const-string v11, "Couldn\'t find preloaded-classes."

    #@18
    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 329
    :goto_1b
    return-void

    #@1c
    .line 253
    :cond_1c
    const-string v10, "Zygote"

    #@1e
    const-string v11, "Preloading classes..."

    #@20
    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 254
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@26
    move-result-wide v7

    #@27
    .line 257
    .local v7, startTime:J
    invoke-static {v12}, Lcom/android/internal/os/ZygoteInit;->setEffectiveGroup(I)V

    #@2a
    .line 258
    invoke-static {v12}, Lcom/android/internal/os/ZygoteInit;->setEffectiveUser(I)V

    #@2d
    .line 262
    invoke-virtual {v6}, Ldalvik/system/VMRuntime;->getTargetHeapUtilization()F

    #@30
    move-result v2

    #@31
    .line 263
    .local v2, defaultUtilization:F
    const v10, 0x3f4ccccd

    #@34
    invoke-virtual {v6, v10}, Ldalvik/system/VMRuntime;->setTargetHeapUtilization(F)F

    #@37
    .line 266
    invoke-static {}, Ljava/lang/System;->gc()V

    #@3a
    .line 267
    invoke-virtual {v6}, Ldalvik/system/VMRuntime;->runFinalizationSync()V

    #@3d
    .line 268
    invoke-static {}, Landroid/os/Debug;->startAllocCounting()V

    #@40
    .line 271
    :try_start_40
    new-instance v0, Ljava/io/BufferedReader;

    #@42
    new-instance v10, Ljava/io/InputStreamReader;

    #@44
    invoke-direct {v10, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    #@47
    const/16 v11, 0x100

    #@49
    invoke-direct {v0, v10, v11}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V

    #@4c
    .line 274
    .local v0, br:Ljava/io/BufferedReader;
    const/4 v1, 0x0

    #@4d
    .line 276
    .local v1, count:I
    :cond_4d
    :goto_4d
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@50
    move-result-object v5

    #@51
    .local v5, line:Ljava/lang/String;
    if-eqz v5, :cond_f6

    #@53
    .line 278
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@56
    move-result-object v5

    #@57
    .line 279
    const-string v10, "#"

    #@59
    invoke-virtual {v5, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@5c
    move-result v10

    #@5d
    if-nez v10, :cond_4d

    #@5f
    const-string v10, ""

    #@61
    invoke-virtual {v5, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_64
    .catchall {:try_start_40 .. :try_end_64} :catchall_d8
    .catch Ljava/io/IOException; {:try_start_40 .. :try_end_64} :catch_99

    #@64
    move-result v10

    #@65
    if-nez v10, :cond_4d

    #@67
    .line 287
    :try_start_67
    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    #@6a
    .line 289
    invoke-static {}, Landroid/os/Debug;->getGlobalAllocSize()I

    #@6d
    move-result v10

    #@6e
    const v11, 0x1e8480

    #@71
    if-le v10, v11, :cond_7c

    #@73
    .line 294
    invoke-static {}, Ljava/lang/System;->gc()V

    #@76
    .line 295
    invoke-virtual {v6}, Ldalvik/system/VMRuntime;->runFinalizationSync()V

    #@79
    .line 296
    invoke-static {}, Landroid/os/Debug;->resetGlobalAllocSize()V
    :try_end_7c
    .catchall {:try_start_67 .. :try_end_7c} :catchall_d8
    .catch Ljava/lang/ClassNotFoundException; {:try_start_67 .. :try_end_7c} :catch_7f
    .catch Ljava/lang/Throwable; {:try_start_67 .. :try_end_7c} :catch_b2
    .catch Ljava/io/IOException; {:try_start_67 .. :try_end_7c} :catch_99

    #@7c
    .line 298
    :cond_7c
    add-int/lit8 v1, v1, 0x1

    #@7e
    goto :goto_4d

    #@7f
    .line 299
    :catch_7f
    move-exception v3

    #@80
    .line 300
    .local v3, e:Ljava/lang/ClassNotFoundException;
    :try_start_80
    const-string v10, "Zygote"

    #@82
    new-instance v11, Ljava/lang/StringBuilder;

    #@84
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@87
    const-string v12, "Class not found for preloading: "

    #@89
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v11

    #@8d
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v11

    #@91
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v11

    #@95
    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_98
    .catchall {:try_start_80 .. :try_end_98} :catchall_d8
    .catch Ljava/io/IOException; {:try_start_80 .. :try_end_98} :catch_99

    #@98
    goto :goto_4d

    #@99
    .line 315
    .end local v0           #br:Ljava/io/BufferedReader;
    .end local v1           #count:I
    .end local v3           #e:Ljava/lang/ClassNotFoundException;
    .end local v5           #line:Ljava/lang/String;
    :catch_99
    move-exception v3

    #@9a
    .line 316
    .local v3, e:Ljava/io/IOException;
    :try_start_9a
    const-string v10, "Zygote"

    #@9c
    const-string v11, "Error reading preloaded-classes."

    #@9e
    invoke-static {v10, v11, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_a1
    .catchall {:try_start_9a .. :try_end_a1} :catchall_d8

    #@a1
    .line 318
    invoke-static {v4}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@a4
    .line 320
    invoke-virtual {v6, v2}, Ldalvik/system/VMRuntime;->setTargetHeapUtilization(F)F

    #@a7
    .line 322
    invoke-static {}, Landroid/os/Debug;->stopAllocCounting()V

    #@aa
    .line 325
    invoke-static {v14}, Lcom/android/internal/os/ZygoteInit;->setEffectiveUser(I)V

    #@ad
    .line 326
    invoke-static {v14}, Lcom/android/internal/os/ZygoteInit;->setEffectiveGroup(I)V

    #@b0
    goto/16 :goto_1b

    #@b2
    .line 301
    .end local v3           #e:Ljava/io/IOException;
    .restart local v0       #br:Ljava/io/BufferedReader;
    .restart local v1       #count:I
    .restart local v5       #line:Ljava/lang/String;
    :catch_b2
    move-exception v9

    #@b3
    .line 302
    .local v9, t:Ljava/lang/Throwable;
    :try_start_b3
    const-string v10, "Zygote"

    #@b5
    new-instance v11, Ljava/lang/StringBuilder;

    #@b7
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@ba
    const-string v12, "Error preloading "

    #@bc
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v11

    #@c0
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v11

    #@c4
    const-string v12, "."

    #@c6
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v11

    #@ca
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cd
    move-result-object v11

    #@ce
    invoke-static {v10, v11, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@d1
    .line 303
    instance-of v10, v9, Ljava/lang/Error;

    #@d3
    if-eqz v10, :cond_e9

    #@d5
    .line 304
    check-cast v9, Ljava/lang/Error;

    #@d7
    .end local v9           #t:Ljava/lang/Throwable;
    throw v9
    :try_end_d8
    .catchall {:try_start_b3 .. :try_end_d8} :catchall_d8
    .catch Ljava/io/IOException; {:try_start_b3 .. :try_end_d8} :catch_99

    #@d8
    .line 318
    .end local v0           #br:Ljava/io/BufferedReader;
    .end local v1           #count:I
    .end local v5           #line:Ljava/lang/String;
    :catchall_d8
    move-exception v10

    #@d9
    invoke-static {v4}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@dc
    .line 320
    invoke-virtual {v6, v2}, Ldalvik/system/VMRuntime;->setTargetHeapUtilization(F)F

    #@df
    .line 322
    invoke-static {}, Landroid/os/Debug;->stopAllocCounting()V

    #@e2
    .line 325
    invoke-static {v14}, Lcom/android/internal/os/ZygoteInit;->setEffectiveUser(I)V

    #@e5
    .line 326
    invoke-static {v14}, Lcom/android/internal/os/ZygoteInit;->setEffectiveGroup(I)V

    #@e8
    throw v10

    #@e9
    .line 306
    .restart local v0       #br:Ljava/io/BufferedReader;
    .restart local v1       #count:I
    .restart local v5       #line:Ljava/lang/String;
    .restart local v9       #t:Ljava/lang/Throwable;
    :cond_e9
    :try_start_e9
    instance-of v10, v9, Ljava/lang/RuntimeException;

    #@eb
    if-eqz v10, :cond_f0

    #@ed
    .line 307
    check-cast v9, Ljava/lang/RuntimeException;

    #@ef
    .end local v9           #t:Ljava/lang/Throwable;
    throw v9

    #@f0
    .line 309
    .restart local v9       #t:Ljava/lang/Throwable;
    :cond_f0
    new-instance v10, Ljava/lang/RuntimeException;

    #@f2
    invoke-direct {v10, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@f5
    throw v10

    #@f6
    .line 313
    .end local v9           #t:Ljava/lang/Throwable;
    :cond_f6
    const-string v10, "Zygote"

    #@f8
    new-instance v11, Ljava/lang/StringBuilder;

    #@fa
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@fd
    const-string v12, "...preloaded "

    #@ff
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@102
    move-result-object v11

    #@103
    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@106
    move-result-object v11

    #@107
    const-string v12, " classes in "

    #@109
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10c
    move-result-object v11

    #@10d
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@110
    move-result-wide v12

    #@111
    sub-long/2addr v12, v7

    #@112
    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@115
    move-result-object v11

    #@116
    const-string/jumbo v12, "ms."

    #@119
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v11

    #@11d
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@120
    move-result-object v11

    #@121
    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_124
    .catchall {:try_start_e9 .. :try_end_124} :catchall_d8
    .catch Ljava/io/IOException; {:try_start_e9 .. :try_end_124} :catch_99

    #@124
    .line 318
    invoke-static {v4}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@127
    .line 320
    invoke-virtual {v6, v2}, Ldalvik/system/VMRuntime;->setTargetHeapUtilization(F)F

    #@12a
    .line 322
    invoke-static {}, Landroid/os/Debug;->stopAllocCounting()V

    #@12d
    .line 325
    invoke-static {v14}, Lcom/android/internal/os/ZygoteInit;->setEffectiveUser(I)V

    #@130
    .line 326
    invoke-static {v14}, Lcom/android/internal/os/ZygoteInit;->setEffectiveGroup(I)V

    #@133
    goto/16 :goto_1b
.end method

.method private static preloadColorStateLists(Ldalvik/system/VMRuntime;Landroid/content/res/TypedArray;)I
    .registers 8
    .parameter "runtime"
    .parameter "ar"

    #@0
    .prologue
    .line 375
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->length()I

    #@3
    move-result v0

    #@4
    .line 376
    .local v0, N:I
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    if-ge v1, v0, :cond_5c

    #@7
    .line 377
    invoke-static {}, Landroid/os/Debug;->getGlobalAllocSize()I

    #@a
    move-result v3

    #@b
    const v4, 0xc350

    #@e
    if-le v3, v4, :cond_19

    #@10
    .line 381
    invoke-static {}, Ljava/lang/System;->gc()V

    #@13
    .line 382
    invoke-virtual {p0}, Ldalvik/system/VMRuntime;->runFinalizationSync()V

    #@16
    .line 383
    invoke-static {}, Landroid/os/Debug;->resetGlobalAllocSize()V

    #@19
    .line 385
    :cond_19
    const/4 v3, 0x0

    #@1a
    invoke-virtual {p1, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@1d
    move-result v2

    #@1e
    .line 389
    .local v2, id:I
    if-eqz v2, :cond_59

    #@20
    .line 390
    sget-object v3, Lcom/android/internal/os/ZygoteInit;->mResources:Landroid/content/res/Resources;

    #@22
    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    #@25
    move-result-object v3

    #@26
    if-nez v3, :cond_59

    #@28
    .line 391
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@2a
    new-instance v4, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v5, "Unable to find preloaded color resource #0x"

    #@31
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@38
    move-result-object v5

    #@39
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v4

    #@3d
    const-string v5, " ("

    #@3f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v4

    #@43
    invoke-virtual {p1, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@46
    move-result-object v5

    #@47
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v4

    #@4b
    const-string v5, ")"

    #@4d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v4

    #@51
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v4

    #@55
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@58
    throw v3

    #@59
    .line 376
    :cond_59
    add-int/lit8 v1, v1, 0x1

    #@5b
    goto :goto_5

    #@5c
    .line 398
    .end local v2           #id:I
    :cond_5c
    return v0
.end method

.method private static preloadDrawables(Ldalvik/system/VMRuntime;Landroid/content/res/TypedArray;)I
    .registers 8
    .parameter "runtime"
    .parameter "ar"

    #@0
    .prologue
    .line 403
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->length()I

    #@3
    move-result v0

    #@4
    .line 404
    .local v0, N:I
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    if-ge v1, v0, :cond_5c

    #@7
    .line 406
    invoke-static {}, Landroid/os/Debug;->getGlobalAllocSize()I

    #@a
    move-result v3

    #@b
    const v4, 0x3d090

    #@e
    if-le v3, v4, :cond_19

    #@10
    .line 410
    invoke-static {}, Ljava/lang/System;->gc()V

    #@13
    .line 411
    invoke-virtual {p0}, Ldalvik/system/VMRuntime;->runFinalizationSync()V

    #@16
    .line 412
    invoke-static {}, Landroid/os/Debug;->resetGlobalAllocSize()V

    #@19
    .line 414
    :cond_19
    const/4 v3, 0x0

    #@1a
    invoke-virtual {p1, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@1d
    move-result v2

    #@1e
    .line 418
    .local v2, id:I
    if-eqz v2, :cond_59

    #@20
    .line 419
    sget-object v3, Lcom/android/internal/os/ZygoteInit;->mResources:Landroid/content/res/Resources;

    #@22
    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@25
    move-result-object v3

    #@26
    if-nez v3, :cond_59

    #@28
    .line 420
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@2a
    new-instance v4, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v5, "Unable to find preloaded drawable resource #0x"

    #@31
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@38
    move-result-object v5

    #@39
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v4

    #@3d
    const-string v5, " ("

    #@3f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v4

    #@43
    invoke-virtual {p1, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@46
    move-result-object v5

    #@47
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v4

    #@4b
    const-string v5, ")"

    #@4d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v4

    #@51
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v4

    #@55
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@58
    throw v3

    #@59
    .line 404
    :cond_59
    add-int/lit8 v1, v1, 0x1

    #@5b
    goto :goto_5

    #@5c
    .line 427
    .end local v2           #id:I
    :cond_5c
    return v0
.end method

.method private static preloadResources()V
    .registers 10

    #@0
    .prologue
    .line 339
    invoke-static {}, Ldalvik/system/VMRuntime;->getRuntime()Ldalvik/system/VMRuntime;

    #@3
    move-result-object v3

    #@4
    .line 341
    .local v3, runtime:Ldalvik/system/VMRuntime;
    invoke-static {}, Landroid/os/Debug;->startAllocCounting()V

    #@7
    .line 343
    :try_start_7
    invoke-static {}, Ljava/lang/System;->gc()V

    #@a
    .line 344
    invoke-virtual {v3}, Ldalvik/system/VMRuntime;->runFinalizationSync()V

    #@d
    .line 345
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@10
    move-result-object v6

    #@11
    sput-object v6, Lcom/android/internal/os/ZygoteInit;->mResources:Landroid/content/res/Resources;

    #@13
    .line 346
    sget-object v6, Lcom/android/internal/os/ZygoteInit;->mResources:Landroid/content/res/Resources;

    #@15
    invoke-virtual {v6}, Landroid/content/res/Resources;->startPreloading()V

    #@18
    .line 348
    const-string v6, "Zygote"

    #@1a
    const-string v7, "Preloading resources..."

    #@1c
    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 350
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@22
    move-result-wide v4

    #@23
    .line 351
    .local v4, startTime:J
    sget-object v6, Lcom/android/internal/os/ZygoteInit;->mResources:Landroid/content/res/Resources;

    #@25
    const v7, 0x1070007

    #@28
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    #@2b
    move-result-object v1

    #@2c
    .line 353
    .local v1, ar:Landroid/content/res/TypedArray;
    invoke-static {v3, v1}, Lcom/android/internal/os/ZygoteInit;->preloadDrawables(Ldalvik/system/VMRuntime;Landroid/content/res/TypedArray;)I

    #@2f
    move-result v0

    #@30
    .line 354
    .local v0, N:I
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    #@33
    .line 355
    const-string v6, "Zygote"

    #@35
    new-instance v7, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v8, "...preloaded "

    #@3c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v7

    #@40
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@43
    move-result-object v7

    #@44
    const-string v8, " resources in "

    #@46
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v7

    #@4a
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@4d
    move-result-wide v8

    #@4e
    sub-long/2addr v8, v4

    #@4f
    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@52
    move-result-object v7

    #@53
    const-string/jumbo v8, "ms."

    #@56
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v7

    #@5a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v7

    #@5e
    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@61
    .line 358
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@64
    move-result-wide v4

    #@65
    .line 359
    sget-object v6, Lcom/android/internal/os/ZygoteInit;->mResources:Landroid/content/res/Resources;

    #@67
    const v7, 0x1070008

    #@6a
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    #@6d
    move-result-object v1

    #@6e
    .line 361
    invoke-static {v3, v1}, Lcom/android/internal/os/ZygoteInit;->preloadColorStateLists(Ldalvik/system/VMRuntime;Landroid/content/res/TypedArray;)I

    #@71
    move-result v0

    #@72
    .line 362
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    #@75
    .line 363
    const-string v6, "Zygote"

    #@77
    new-instance v7, Ljava/lang/StringBuilder;

    #@79
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@7c
    const-string v8, "...preloaded "

    #@7e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v7

    #@82
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@85
    move-result-object v7

    #@86
    const-string v8, " resources in "

    #@88
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v7

    #@8c
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@8f
    move-result-wide v8

    #@90
    sub-long/2addr v8, v4

    #@91
    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@94
    move-result-object v7

    #@95
    const-string/jumbo v8, "ms."

    #@98
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v7

    #@9c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9f
    move-result-object v7

    #@a0
    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@a3
    .line 366
    sget-object v6, Lcom/android/internal/os/ZygoteInit;->mResources:Landroid/content/res/Resources;

    #@a5
    invoke-virtual {v6}, Landroid/content/res/Resources;->finishPreloading()V
    :try_end_a8
    .catchall {:try_start_7 .. :try_end_a8} :catchall_b8
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_a8} :catch_ac

    #@a8
    .line 370
    invoke-static {}, Landroid/os/Debug;->stopAllocCounting()V

    #@ab
    .line 372
    .end local v0           #N:I
    .end local v1           #ar:Landroid/content/res/TypedArray;
    .end local v4           #startTime:J
    :goto_ab
    return-void

    #@ac
    .line 367
    :catch_ac
    move-exception v2

    #@ad
    .line 368
    .local v2, e:Ljava/lang/RuntimeException;
    :try_start_ad
    const-string v6, "Zygote"

    #@af
    const-string v7, "Failure preloading resources"

    #@b1
    invoke-static {v6, v7, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_b4
    .catchall {:try_start_ad .. :try_end_b4} :catchall_b8

    #@b4
    .line 370
    invoke-static {}, Landroid/os/Debug;->stopAllocCounting()V

    #@b7
    goto :goto_ab

    #@b8
    .end local v2           #e:Ljava/lang/RuntimeException;
    :catchall_b8
    move-exception v6

    #@b9
    invoke-static {}, Landroid/os/Debug;->stopAllocCounting()V

    #@bc
    throw v6
.end method

.method private static registerZygoteSocket()V
    .registers 6

    #@0
    .prologue
    .line 158
    sget-object v3, Lcom/android/internal/os/ZygoteInit;->sServerSocket:Landroid/net/LocalServerSocket;

    #@2
    if-nez v3, :cond_19

    #@4
    .line 161
    :try_start_4
    const-string v3, "ANDROID_SOCKET_zygote"

    #@6
    invoke-static {v3}, Ljava/lang/System;->getenv(Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    .line 162
    .local v0, env:Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_d
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_d} :catch_1a

    #@d
    move-result v2

    #@e
    .line 169
    .local v2, fileDesc:I
    :try_start_e
    new-instance v3, Landroid/net/LocalServerSocket;

    #@10
    invoke-static {v2}, Lcom/android/internal/os/ZygoteInit;->createFileDescriptor(I)Ljava/io/FileDescriptor;

    #@13
    move-result-object v4

    #@14
    invoke-direct {v3, v4}, Landroid/net/LocalServerSocket;-><init>(Ljava/io/FileDescriptor;)V

    #@17
    sput-object v3, Lcom/android/internal/os/ZygoteInit;->sServerSocket:Landroid/net/LocalServerSocket;
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_19} :catch_23

    #@19
    .line 176
    :cond_19
    return-void

    #@1a
    .line 163
    .end local v2           #fileDesc:I
    :catch_1a
    move-exception v1

    #@1b
    .line 164
    .local v1, ex:Ljava/lang/RuntimeException;
    new-instance v3, Ljava/lang/RuntimeException;

    #@1d
    const-string v4, "ANDROID_SOCKET_zygote unset or invalid"

    #@1f
    invoke-direct {v3, v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@22
    throw v3

    #@23
    .line 171
    .end local v1           #ex:Ljava/lang/RuntimeException;
    .restart local v2       #fileDesc:I
    :catch_23
    move-exception v1

    #@24
    .line 172
    .local v1, ex:Ljava/io/IOException;
    new-instance v3, Ljava/lang/RuntimeException;

    #@26
    new-instance v4, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v5, "Error binding to local socket \'"

    #@2d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    const-string v5, "\'"

    #@37
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v4

    #@3f
    invoke-direct {v3, v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@42
    throw v3
.end method

.method static native reopenStdio(Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method private static runForkMode()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 582
    .local v1, peer:Lcom/android/internal/os/ZygoteConnection;
    .local v2, pid:I
    :goto_1
    invoke-static {}, Lcom/android/internal/os/ZygoteInit;->acceptCommandPeer()Lcom/android/internal/os/ZygoteConnection;

    #@4
    move-result-object v1

    #@5
    .line 586
    invoke-static {}, Ldalvik/system/Zygote;->fork()I

    #@8
    .end local v2           #pid:I
    move-result v2

    #@9
    .line 588
    .restart local v2       #pid:I
    if-nez v2, :cond_25

    #@b
    .line 593
    :try_start_b
    sget-object v3, Lcom/android/internal/os/ZygoteInit;->sServerSocket:Landroid/net/LocalServerSocket;

    #@d
    invoke-virtual {v3}, Landroid/net/LocalServerSocket;->close()V
    :try_end_10
    .catchall {:try_start_b .. :try_end_10} :catchall_21
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_10} :catch_16

    #@10
    .line 597
    sput-object v5, Lcom/android/internal/os/ZygoteInit;->sServerSocket:Landroid/net/LocalServerSocket;

    #@12
    .line 600
    :goto_12
    invoke-virtual {v1}, Lcom/android/internal/os/ZygoteConnection;->run()V

    #@15
    .line 608
    return-void

    #@16
    .line 594
    :catch_16
    move-exception v0

    #@17
    .line 595
    .local v0, ex:Ljava/io/IOException;
    :try_start_17
    const-string v3, "Zygote"

    #@19
    const-string v4, "Zygote Child: error closing sockets"

    #@1b
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1e
    .catchall {:try_start_17 .. :try_end_1e} :catchall_21

    #@1e
    .line 597
    sput-object v5, Lcom/android/internal/os/ZygoteInit;->sServerSocket:Landroid/net/LocalServerSocket;

    #@20
    goto :goto_12

    #@21
    .end local v0           #ex:Ljava/io/IOException;
    :catchall_21
    move-exception v3

    #@22
    sput-object v5, Lcom/android/internal/os/ZygoteInit;->sServerSocket:Landroid/net/LocalServerSocket;

    #@24
    throw v3

    #@25
    .line 602
    :cond_25
    if-lez v2, :cond_2b

    #@27
    .line 603
    invoke-virtual {v1}, Lcom/android/internal/os/ZygoteConnection;->closeSocket()V

    #@2a
    goto :goto_1

    #@2b
    .line 605
    :cond_2b
    new-instance v3, Ljava/lang/RuntimeException;

    #@2d
    const-string v4, "Error invoking fork()"

    #@2f
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@32
    throw v3
.end method

.method private static runSelectLoopMode()V
    .registers 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller;
        }
    .end annotation

    #@0
    .prologue
    .line 619
    new-instance v4, Ljava/util/ArrayList;

    #@2
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 620
    .local v4, fds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/io/FileDescriptor;>;"
    new-instance v8, Ljava/util/ArrayList;

    #@7
    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    #@a
    .line 621
    .local v8, peers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/os/ZygoteConnection;>;"
    const/4 v9, 0x4

    #@b
    new-array v3, v9, [Ljava/io/FileDescriptor;

    #@d
    .line 623
    .local v3, fdArray:[Ljava/io/FileDescriptor;
    sget-object v9, Lcom/android/internal/os/ZygoteInit;->sServerSocket:Landroid/net/LocalServerSocket;

    #@f
    invoke-virtual {v9}, Landroid/net/LocalServerSocket;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@12
    move-result-object v9

    #@13
    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@16
    .line 624
    const/4 v9, 0x0

    #@17
    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1a
    .line 626
    const/16 v6, 0xa

    #@1c
    .line 639
    .local v6, loopCount:I
    :cond_1c
    :goto_1c
    if-gtz v6, :cond_39

    #@1e
    .line 640
    invoke-static {}, Lcom/android/internal/os/ZygoteInit;->gc()V

    #@21
    .line 641
    const/16 v6, 0xa

    #@23
    .line 648
    :goto_23
    :try_start_23
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@26
    move-result-object v9

    #@27
    move-object v0, v9

    #@28
    check-cast v0, [Ljava/io/FileDescriptor;

    #@2a
    move-object v3, v0

    #@2b
    .line 649
    invoke-static {v3}, Lcom/android/internal/os/ZygoteInit;->selectReadable([Ljava/io/FileDescriptor;)I
    :try_end_2e
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_2e} :catch_3c

    #@2e
    move-result v5

    #@2f
    .line 654
    .local v5, index:I
    if-gez v5, :cond_45

    #@31
    .line 655
    new-instance v9, Ljava/lang/RuntimeException;

    #@33
    const-string v10, "Error in select()"

    #@35
    invoke-direct {v9, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@38
    throw v9

    #@39
    .line 643
    .end local v5           #index:I
    :cond_39
    add-int/lit8 v6, v6, -0x1

    #@3b
    goto :goto_23

    #@3c
    .line 650
    :catch_3c
    move-exception v2

    #@3d
    .line 651
    .local v2, ex:Ljava/io/IOException;
    new-instance v9, Ljava/lang/RuntimeException;

    #@3f
    const-string v10, "Error in select()"

    #@41
    invoke-direct {v9, v10, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@44
    throw v9

    #@45
    .line 656
    .end local v2           #ex:Ljava/io/IOException;
    .restart local v5       #index:I
    :cond_45
    if-nez v5, :cond_56

    #@47
    .line 657
    invoke-static {}, Lcom/android/internal/os/ZygoteInit;->acceptCommandPeer()Lcom/android/internal/os/ZygoteConnection;

    #@4a
    move-result-object v7

    #@4b
    .line 658
    .local v7, newPeer:Lcom/android/internal/os/ZygoteConnection;
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4e
    .line 659
    invoke-virtual {v7}, Lcom/android/internal/os/ZygoteConnection;->getFileDesciptor()Ljava/io/FileDescriptor;

    #@51
    move-result-object v9

    #@52
    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@55
    goto :goto_1c

    #@56
    .line 662
    .end local v7           #newPeer:Lcom/android/internal/os/ZygoteConnection;
    :cond_56
    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@59
    move-result-object v9

    #@5a
    check-cast v9, Lcom/android/internal/os/ZygoteConnection;

    #@5c
    invoke-virtual {v9}, Lcom/android/internal/os/ZygoteConnection;->runOnce()Z

    #@5f
    move-result v1

    #@60
    .line 664
    .local v1, done:Z
    if-eqz v1, :cond_1c

    #@62
    .line 665
    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@65
    .line 666
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@68
    goto :goto_1c
.end method

.method static native selectReadable([Ljava/io/FileDescriptor;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method static native setCapabilities(JJ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method static native setCloseOnExec(Ljava/io/FileDescriptor;Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method private static setEffectiveGroup(I)V
    .registers 5
    .parameter "gid"

    #@0
    .prologue
    .line 227
    const/4 v1, 0x0

    #@1
    invoke-static {v1, p0}, Lcom/android/internal/os/ZygoteInit;->setregid(II)I

    #@4
    move-result v0

    #@5
    .line 228
    .local v0, errno:I
    if-eqz v0, :cond_20

    #@7
    .line 229
    const-string v1, "Zygote"

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string/jumbo v3, "setregid() failed. errno: "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 231
    :cond_20
    return-void
.end method

.method private static setEffectiveUser(I)V
    .registers 5
    .parameter "uid"

    #@0
    .prologue
    .line 217
    const/4 v1, 0x0

    #@1
    invoke-static {v1, p0}, Lcom/android/internal/os/ZygoteInit;->setreuid(II)I

    #@4
    move-result v0

    #@5
    .line 218
    .local v0, errno:I
    if-eqz v0, :cond_20

    #@7
    .line 219
    const-string v1, "Zygote"

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string/jumbo v3, "setreuid() failed. errno: "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 221
    :cond_20
    return-void
.end method

.method static native setpgid(II)I
.end method

.method static native setregid(II)I
.end method

.method static native setreuid(II)I
.end method

.method private static startSystemServer()Z
    .registers 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller;,
            Ljava/lang/RuntimeException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v14, 0x1

    #@1
    .line 485
    const/4 v0, 0x7

    #@2
    new-array v9, v0, [Ljava/lang/String;

    #@4
    const/4 v0, 0x0

    #@5
    const-string v1, "--setuid=1000"

    #@7
    aput-object v1, v9, v0

    #@9
    const-string v0, "--setgid=1000"

    #@b
    aput-object v0, v9, v14

    #@d
    const/4 v0, 0x2

    #@e
    const-string v1, "--setgroups=1001,1002,1003,1004,1005,1006,1007,1008,1009,1010,1018,1021,3001,3002,3003,3004,3006,3007,4002"

    #@10
    aput-object v1, v9, v0

    #@12
    const/4 v0, 0x3

    #@13
    const-string v1, "--capabilities=130104352,130104352"

    #@15
    aput-object v1, v9, v0

    #@17
    const/4 v0, 0x4

    #@18
    const-string v1, "--runtime-init"

    #@1a
    aput-object v1, v9, v0

    #@1c
    const/4 v0, 0x5

    #@1d
    const-string v1, "--nice-name=system_server"

    #@1f
    aput-object v1, v9, v0

    #@21
    const/4 v0, 0x6

    #@22
    const-string v1, "com.android.server.SystemServer"

    #@24
    aput-object v1, v9, v0

    #@26
    .line 496
    .local v9, args:[Ljava/lang/String;
    const/4 v11, 0x0

    #@27
    .line 501
    .local v11, parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    :try_start_27
    new-instance v12, Lcom/android/internal/os/ZygoteConnection$Arguments;

    #@29
    invoke-direct {v12, v9}, Lcom/android/internal/os/ZygoteConnection$Arguments;-><init>([Ljava/lang/String;)V
    :try_end_2c
    .catch Ljava/lang/IllegalArgumentException; {:try_start_27 .. :try_end_2c} :catch_4b

    #@2c
    .line 502
    .end local v11           #parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    .local v12, parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    :try_start_2c
    invoke-static {v12}, Lcom/android/internal/os/ZygoteConnection;->applyDebuggerSystemProperty(Lcom/android/internal/os/ZygoteConnection$Arguments;)V

    #@2f
    .line 503
    invoke-static {v12}, Lcom/android/internal/os/ZygoteConnection;->applyInvokeWithSystemProperty(Lcom/android/internal/os/ZygoteConnection$Arguments;)V

    #@32
    .line 506
    iget v0, v12, Lcom/android/internal/os/ZygoteConnection$Arguments;->uid:I

    #@34
    iget v1, v12, Lcom/android/internal/os/ZygoteConnection$Arguments;->gid:I

    #@36
    iget-object v2, v12, Lcom/android/internal/os/ZygoteConnection$Arguments;->gids:[I

    #@38
    iget v3, v12, Lcom/android/internal/os/ZygoteConnection$Arguments;->debugFlags:I

    #@3a
    const/4 v4, 0x0

    #@3b
    check-cast v4, [[I

    #@3d
    iget-wide v5, v12, Lcom/android/internal/os/ZygoteConnection$Arguments;->permittedCapabilities:J

    #@3f
    iget-wide v7, v12, Lcom/android/internal/os/ZygoteConnection$Arguments;->effectiveCapabilities:J

    #@41
    invoke-static/range {v0 .. v8}, Ldalvik/system/Zygote;->forkSystemServer(II[II[[IJJ)I
    :try_end_44
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2c .. :try_end_44} :catch_52

    #@44
    move-result v13

    #@45
    .line 518
    .local v13, pid:I
    if-nez v13, :cond_4a

    #@47
    .line 519
    invoke-static {v12}, Lcom/android/internal/os/ZygoteInit;->handleSystemServerProcess(Lcom/android/internal/os/ZygoteConnection$Arguments;)V

    #@4a
    .line 522
    :cond_4a
    return v14

    #@4b
    .line 513
    .end local v12           #parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    .end local v13           #pid:I
    .restart local v11       #parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    :catch_4b
    move-exception v10

    #@4c
    .line 514
    .local v10, ex:Ljava/lang/IllegalArgumentException;
    :goto_4c
    new-instance v0, Ljava/lang/RuntimeException;

    #@4e
    invoke-direct {v0, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@51
    throw v0

    #@52
    .line 513
    .end local v10           #ex:Ljava/lang/IllegalArgumentException;
    .end local v11           #parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    .restart local v12       #parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    :catch_52
    move-exception v10

    #@53
    move-object v11, v12

    #@54
    .end local v12           #parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    .restart local v11       #parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    goto :goto_4c
.end method
