.class public Lcom/android/internal/os/PowerProfile;
.super Ljava/lang/Object;
.source "PowerProfile.java"


# static fields
.field private static final ATTR_NAME:Ljava/lang/String; = "name"

.field public static final POWER_AUDIO:Ljava/lang/String; = "dsp.audio"

.field public static final POWER_BATTERY_CAPACITY:Ljava/lang/String; = "battery.capacity"

.field public static final POWER_BLUETOOTH_ACTIVE:Ljava/lang/String; = "bluetooth.active"

.field public static final POWER_BLUETOOTH_AT_CMD:Ljava/lang/String; = "bluetooth.at"

.field public static final POWER_BLUETOOTH_ON:Ljava/lang/String; = "bluetooth.on"

.field public static final POWER_CPU_ACTIVE:Ljava/lang/String; = "cpu.active"

.field public static final POWER_CPU_AWAKE:Ljava/lang/String; = "cpu.awake"

.field public static final POWER_CPU_IDLE:Ljava/lang/String; = "cpu.idle"

.field public static final POWER_CPU_SPEEDS:Ljava/lang/String; = "cpu.speeds"

.field public static final POWER_GPS_ON:Ljava/lang/String; = "gps.on"

.field public static final POWER_NONE:Ljava/lang/String; = "none"

.field public static final POWER_RADIO_ACTIVE:Ljava/lang/String; = "radio.active"

.field public static final POWER_RADIO_ON:Ljava/lang/String; = "radio.on"

.field public static final POWER_RADIO_SCANNING:Ljava/lang/String; = "radio.scanning"

.field public static final POWER_SCREEN_FULL:Ljava/lang/String; = "screen.full"

.field public static final POWER_SCREEN_ON:Ljava/lang/String; = "screen.on"

.field public static final POWER_VIDEO:Ljava/lang/String; = "dsp.video"

.field public static final POWER_WIFI_ACTIVE:Ljava/lang/String; = "wifi.active"

.field public static final POWER_WIFI_ON:Ljava/lang/String; = "wifi.on"

.field public static final POWER_WIFI_SCAN:Ljava/lang/String; = "wifi.scan"

.field private static final TAG_ARRAY:Ljava/lang/String; = "array"

.field private static final TAG_ARRAYITEM:Ljava/lang/String; = "value"

.field private static final TAG_DEVICE:Ljava/lang/String; = "device"

.field private static final TAG_ITEM:Ljava/lang/String; = "item"

.field static final sPowerMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 143
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    sput-object v0, Lcom/android/internal/os/PowerProfile;->sPowerMap:Ljava/util/HashMap;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 151
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 154
    sget-object v0, Lcom/android/internal/os/PowerProfile;->sPowerMap:Ljava/util/HashMap;

    #@5
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_e

    #@b
    .line 155
    invoke-direct {p0, p1}, Lcom/android/internal/os/PowerProfile;->readPowerValuesFromXml(Landroid/content/Context;)V

    #@e
    .line 157
    :cond_e
    return-void
.end method

.method private readPowerValuesFromXml(Landroid/content/Context;)V
    .registers 15
    .parameter "context"

    #@0
    .prologue
    .line 160
    const v4, 0x10f000c

    #@3
    .line 161
    .local v4, id:I
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@6
    move-result-object v11

    #@7
    invoke-virtual {v11, v4}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    #@a
    move-result-object v6

    #@b
    .line 162
    .local v6, parser:Landroid/content/res/XmlResourceParser;
    const/4 v7, 0x0

    #@c
    .line 163
    .local v7, parsingArray:Z
    new-instance v0, Ljava/util/ArrayList;

    #@e
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@11
    .line 164
    .local v0, array:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Double;>;"
    const/4 v1, 0x0

    #@12
    .line 167
    .local v1, arrayName:Ljava/lang/String;
    :try_start_12
    const-string v11, "device"

    #@14
    invoke-static {v6, v11}, Lcom/android/internal/util/XmlUtils;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    #@17
    .line 170
    :cond_17
    :goto_17
    invoke-static {v6}, Lcom/android/internal/util/XmlUtils;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V

    #@1a
    .line 172
    invoke-interface {v6}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    .line 173
    .local v3, element:Ljava/lang/String;
    if-nez v3, :cond_35

    #@20
    .line 202
    if-eqz v7, :cond_31

    #@22
    .line 203
    sget-object v11, Lcom/android/internal/os/PowerProfile;->sPowerMap:Ljava/util/HashMap;

    #@24
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@27
    move-result v12

    #@28
    new-array v12, v12, [Ljava/lang/Double;

    #@2a
    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@2d
    move-result-object v12

    #@2e
    invoke-virtual {v11, v1, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_31
    .catchall {:try_start_12 .. :try_end_31} :catchall_b2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_12 .. :try_end_31} :catch_ab
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_31} :catch_c2

    #@31
    .line 210
    :cond_31
    invoke-interface {v6}, Landroid/content/res/XmlResourceParser;->close()V

    #@34
    .line 212
    return-void

    #@35
    .line 175
    :cond_35
    if-eqz v7, :cond_50

    #@37
    :try_start_37
    const-string/jumbo v11, "value"

    #@3a
    invoke-virtual {v3, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3d
    move-result v11

    #@3e
    if-nez v11, :cond_50

    #@40
    .line 177
    sget-object v11, Lcom/android/internal/os/PowerProfile;->sPowerMap:Ljava/util/HashMap;

    #@42
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@45
    move-result v12

    #@46
    new-array v12, v12, [Ljava/lang/Double;

    #@48
    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@4b
    move-result-object v12

    #@4c
    invoke-virtual {v11, v1, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4f
    .line 178
    const/4 v7, 0x0

    #@50
    .line 180
    :cond_50
    const-string v11, "array"

    #@52
    invoke-virtual {v3, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@55
    move-result v11

    #@56
    if-eqz v11, :cond_65

    #@58
    .line 181
    const/4 v7, 0x1

    #@59
    .line 182
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@5c
    .line 183
    const/4 v11, 0x0

    #@5d
    const-string/jumbo v12, "name"

    #@60
    invoke-interface {v6, v11, v12}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@63
    move-result-object v1

    #@64
    goto :goto_17

    #@65
    .line 184
    :cond_65
    const-string/jumbo v11, "item"

    #@68
    invoke-virtual {v3, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6b
    move-result v11

    #@6c
    if-nez v11, :cond_77

    #@6e
    const-string/jumbo v11, "value"

    #@71
    invoke-virtual {v3, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@74
    move-result v11

    #@75
    if-eqz v11, :cond_17

    #@77
    .line 185
    :cond_77
    const/4 v5, 0x0

    #@78
    .line 186
    .local v5, name:Ljava/lang/String;
    if-nez v7, :cond_82

    #@7a
    const/4 v11, 0x0

    #@7b
    const-string/jumbo v12, "name"

    #@7e
    invoke-interface {v6, v11, v12}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@81
    move-result-object v5

    #@82
    .line 187
    :cond_82
    invoke-interface {v6}, Landroid/content/res/XmlResourceParser;->next()I

    #@85
    move-result v11

    #@86
    const/4 v12, 0x4

    #@87
    if-ne v11, v12, :cond_17

    #@89
    .line 188
    invoke-interface {v6}, Landroid/content/res/XmlResourceParser;->getText()Ljava/lang/String;
    :try_end_8c
    .catchall {:try_start_37 .. :try_end_8c} :catchall_b2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_37 .. :try_end_8c} :catch_ab
    .catch Ljava/io/IOException; {:try_start_37 .. :try_end_8c} :catch_c2

    #@8c
    move-result-object v8

    #@8d
    .line 189
    .local v8, power:Ljava/lang/String;
    const-wide/16 v9, 0x0

    #@8f
    .line 191
    .local v9, value:D
    :try_start_8f
    invoke-static {v8}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    #@92
    move-result-object v11

    #@93
    invoke-virtual {v11}, Ljava/lang/Double;->doubleValue()D
    :try_end_96
    .catchall {:try_start_8f .. :try_end_96} :catchall_b2
    .catch Ljava/lang/NumberFormatException; {:try_start_8f .. :try_end_96} :catch_c9
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_8f .. :try_end_96} :catch_ab
    .catch Ljava/io/IOException; {:try_start_8f .. :try_end_96} :catch_c2

    #@96
    move-result-wide v9

    #@97
    .line 194
    :goto_97
    :try_start_97
    const-string/jumbo v11, "item"

    #@9a
    invoke-virtual {v3, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9d
    move-result v11

    #@9e
    if-eqz v11, :cond_b7

    #@a0
    .line 195
    sget-object v11, Lcom/android/internal/os/PowerProfile;->sPowerMap:Ljava/util/HashMap;

    #@a2
    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    #@a5
    move-result-object v12

    #@a6
    invoke-virtual {v11, v5, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_a9
    .catchall {:try_start_97 .. :try_end_a9} :catchall_b2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_97 .. :try_end_a9} :catch_ab
    .catch Ljava/io/IOException; {:try_start_97 .. :try_end_a9} :catch_c2

    #@a9
    goto/16 :goto_17

    #@ab
    .line 205
    .end local v3           #element:Ljava/lang/String;
    .end local v5           #name:Ljava/lang/String;
    .end local v8           #power:Ljava/lang/String;
    .end local v9           #value:D
    :catch_ab
    move-exception v2

    #@ac
    .line 206
    .local v2, e:Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_ac
    new-instance v11, Ljava/lang/RuntimeException;

    #@ae
    invoke-direct {v11, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@b1
    throw v11
    :try_end_b2
    .catchall {:try_start_ac .. :try_end_b2} :catchall_b2

    #@b2
    .line 210
    .end local v2           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :catchall_b2
    move-exception v11

    #@b3
    invoke-interface {v6}, Landroid/content/res/XmlResourceParser;->close()V

    #@b6
    throw v11

    #@b7
    .line 196
    .restart local v3       #element:Ljava/lang/String;
    .restart local v5       #name:Ljava/lang/String;
    .restart local v8       #power:Ljava/lang/String;
    .restart local v9       #value:D
    :cond_b7
    if-eqz v7, :cond_17

    #@b9
    .line 197
    :try_start_b9
    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    #@bc
    move-result-object v11

    #@bd
    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_c0
    .catchall {:try_start_b9 .. :try_end_c0} :catchall_b2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_b9 .. :try_end_c0} :catch_ab
    .catch Ljava/io/IOException; {:try_start_b9 .. :try_end_c0} :catch_c2

    #@c0
    goto/16 :goto_17

    #@c2
    .line 207
    .end local v3           #element:Ljava/lang/String;
    .end local v5           #name:Ljava/lang/String;
    .end local v8           #power:Ljava/lang/String;
    .end local v9           #value:D
    :catch_c2
    move-exception v2

    #@c3
    .line 208
    .local v2, e:Ljava/io/IOException;
    :try_start_c3
    new-instance v11, Ljava/lang/RuntimeException;

    #@c5
    invoke-direct {v11, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@c8
    throw v11
    :try_end_c9
    .catchall {:try_start_c3 .. :try_end_c9} :catchall_b2

    #@c9
    .line 192
    .end local v2           #e:Ljava/io/IOException;
    .restart local v3       #element:Ljava/lang/String;
    .restart local v5       #name:Ljava/lang/String;
    .restart local v8       #power:Ljava/lang/String;
    .restart local v9       #value:D
    :catch_c9
    move-exception v11

    #@ca
    goto :goto_97
.end method


# virtual methods
.method public getAveragePower(Ljava/lang/String;)D
    .registers 5
    .parameter "type"

    #@0
    .prologue
    .line 220
    sget-object v1, Lcom/android/internal/os/PowerProfile;->sPowerMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_2b

    #@8
    .line 221
    sget-object v1, Lcom/android/internal/os/PowerProfile;->sPowerMap:Ljava/util/HashMap;

    #@a
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    .line 222
    .local v0, data:Ljava/lang/Object;
    instance-of v1, v0, [Ljava/lang/Double;

    #@10
    if-eqz v1, :cond_1e

    #@12
    .line 223
    check-cast v0, [Ljava/lang/Double;

    #@14
    .end local v0           #data:Ljava/lang/Object;
    check-cast v0, [Ljava/lang/Double;

    #@16
    const/4 v1, 0x0

    #@17
    aget-object v1, v0, v1

    #@19
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    #@1c
    move-result-wide v1

    #@1d
    .line 228
    :goto_1d
    return-wide v1

    #@1e
    .line 225
    .restart local v0       #data:Ljava/lang/Object;
    :cond_1e
    sget-object v1, Lcom/android/internal/os/PowerProfile;->sPowerMap:Ljava/util/HashMap;

    #@20
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@23
    move-result-object v1

    #@24
    check-cast v1, Ljava/lang/Double;

    #@26
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    #@29
    move-result-wide v1

    #@2a
    goto :goto_1d

    #@2b
    .line 228
    .end local v0           #data:Ljava/lang/Object;
    :cond_2b
    const-wide/16 v1, 0x0

    #@2d
    goto :goto_1d
.end method

.method public getAveragePower(Ljava/lang/String;I)D
    .registers 8
    .parameter "type"
    .parameter "level"

    #@0
    .prologue
    const-wide/16 v2, 0x0

    #@2
    .line 241
    sget-object v4, Lcom/android/internal/os/PowerProfile;->sPowerMap:Ljava/util/HashMap;

    #@4
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@7
    move-result v4

    #@8
    if-eqz v4, :cond_24

    #@a
    .line 242
    sget-object v4, Lcom/android/internal/os/PowerProfile;->sPowerMap:Ljava/util/HashMap;

    #@c
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    .line 243
    .local v0, data:Ljava/lang/Object;
    instance-of v4, v0, [Ljava/lang/Double;

    #@12
    if-eqz v4, :cond_31

    #@14
    .line 244
    check-cast v0, [Ljava/lang/Double;

    #@16
    .end local v0           #data:Ljava/lang/Object;
    move-object v1, v0

    #@17
    check-cast v1, [Ljava/lang/Double;

    #@19
    .line 245
    .local v1, values:[Ljava/lang/Double;
    array-length v4, v1

    #@1a
    if-le v4, p2, :cond_25

    #@1c
    if-ltz p2, :cond_25

    #@1e
    .line 246
    aget-object v2, v1, p2

    #@20
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    #@23
    move-result-wide v2

    #@24
    .line 256
    .end local v1           #values:[Ljava/lang/Double;
    :cond_24
    :goto_24
    return-wide v2

    #@25
    .line 247
    .restart local v1       #values:[Ljava/lang/Double;
    :cond_25
    if-ltz p2, :cond_24

    #@27
    .line 250
    array-length v2, v1

    #@28
    add-int/lit8 v2, v2, -0x1

    #@2a
    aget-object v2, v1, v2

    #@2c
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    #@2f
    move-result-wide v2

    #@30
    goto :goto_24

    #@31
    .line 253
    .end local v1           #values:[Ljava/lang/Double;
    .restart local v0       #data:Ljava/lang/Object;
    :cond_31
    check-cast v0, Ljava/lang/Double;

    #@33
    .end local v0           #data:Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    #@36
    move-result-wide v2

    #@37
    goto :goto_24
.end method

.method public getBatteryCapacity()D
    .registers 3

    #@0
    .prologue
    .line 266
    const-string v0, "battery.capacity"

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/os/PowerProfile;->getAveragePower(Ljava/lang/String;)D

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method public getNumSpeedSteps()I
    .registers 4

    #@0
    .prologue
    .line 274
    sget-object v1, Lcom/android/internal/os/PowerProfile;->sPowerMap:Ljava/util/HashMap;

    #@2
    const-string v2, "cpu.speeds"

    #@4
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    .line 275
    .local v0, value:Ljava/lang/Object;
    if-eqz v0, :cond_14

    #@a
    instance-of v1, v0, [Ljava/lang/Double;

    #@c
    if-eqz v1, :cond_14

    #@e
    .line 276
    check-cast v0, [Ljava/lang/Double;

    #@10
    .end local v0           #value:Ljava/lang/Object;
    check-cast v0, [Ljava/lang/Double;

    #@12
    array-length v1, v0

    #@13
    .line 278
    :goto_13
    return v1

    #@14
    .restart local v0       #value:Ljava/lang/Object;
    :cond_14
    const/4 v1, 0x1

    #@15
    goto :goto_13
.end method
