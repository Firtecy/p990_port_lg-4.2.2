.class public Lcom/android/internal/os/RuntimeInit;
.super Ljava/lang/Object;
.source "RuntimeInit.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/os/RuntimeInit$Arguments;,
        Lcom/android/internal/os/RuntimeInit$UncaughtHandler;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "AndroidRuntime"

.field private static initialized:Z

.field private static mApplicationObject:Landroid/os/IBinder;

.field private static volatile mCrashing:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 51
    const/4 v0, 0x0

    #@1
    sput-boolean v0, Lcom/android/internal/os/RuntimeInit;->mCrashing:Z

    #@3
    .line 364
    invoke-static {}, Landroid/ddm/DdmRegister;->registerHandlers()V

    #@6
    .line 365
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 42
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 375
    return-void
.end method

.method static synthetic access$000()Z
    .registers 1

    #@0
    .prologue
    .line 42
    sget-boolean v0, Lcom/android/internal/os/RuntimeInit;->mCrashing:Z

    #@2
    return v0
.end method

.method static synthetic access$002(Z)Z
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 42
    sput-boolean p0, Lcom/android/internal/os/RuntimeInit;->mCrashing:Z

    #@2
    return p0
.end method

.method static synthetic access$100()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 42
    sget-object v0, Lcom/android/internal/os/RuntimeInit;->mApplicationObject:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method private static applicationInit(I[Ljava/lang/String;)V
    .registers 6
    .parameter "targetSdkVersion"
    .parameter "argv"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller;
        }
    .end annotation

    #@0
    .prologue
    .line 297
    const/4 v2, 0x1

    #@1
    invoke-static {v2}, Lcom/android/internal/os/RuntimeInit;->nativeSetExitWithoutCleanup(Z)V

    #@4
    .line 301
    invoke-static {}, Ldalvik/system/VMRuntime;->getRuntime()Ldalvik/system/VMRuntime;

    #@7
    move-result-object v2

    #@8
    const/high16 v3, 0x3f40

    #@a
    invoke-virtual {v2, v3}, Ldalvik/system/VMRuntime;->setTargetHeapUtilization(F)F

    #@d
    .line 302
    invoke-static {}, Ldalvik/system/VMRuntime;->getRuntime()Ldalvik/system/VMRuntime;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p0}, Ldalvik/system/VMRuntime;->setTargetSdkVersion(I)V

    #@14
    .line 306
    :try_start_14
    new-instance v0, Lcom/android/internal/os/RuntimeInit$Arguments;

    #@16
    invoke-direct {v0, p1}, Lcom/android/internal/os/RuntimeInit$Arguments;-><init>([Ljava/lang/String;)V
    :try_end_19
    .catch Ljava/lang/IllegalArgumentException; {:try_start_14 .. :try_end_19} :catch_21

    #@19
    .line 314
    .local v0, args:Lcom/android/internal/os/RuntimeInit$Arguments;
    iget-object v2, v0, Lcom/android/internal/os/RuntimeInit$Arguments;->startClass:Ljava/lang/String;

    #@1b
    iget-object v3, v0, Lcom/android/internal/os/RuntimeInit$Arguments;->startArgs:[Ljava/lang/String;

    #@1d
    invoke-static {v2, v3}, Lcom/android/internal/os/RuntimeInit;->invokeStaticMain(Ljava/lang/String;[Ljava/lang/String;)V

    #@20
    .line 315
    .end local v0           #args:Lcom/android/internal/os/RuntimeInit$Arguments;
    :goto_20
    return-void

    #@21
    .line 307
    :catch_21
    move-exception v1

    #@22
    .line 308
    .local v1, ex:Ljava/lang/IllegalArgumentException;
    const-string v2, "AndroidRuntime"

    #@24
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    #@27
    move-result-object v3

    #@28
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    goto :goto_20
.end method

.method private static final commonInit()V
    .registers 4

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 103
    new-instance v2, Lcom/android/internal/os/RuntimeInit$UncaughtHandler;

    #@3
    invoke-direct {v2, v3}, Lcom/android/internal/os/RuntimeInit$UncaughtHandler;-><init>(Lcom/android/internal/os/RuntimeInit$1;)V

    #@6
    invoke-static {v2}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    #@9
    .line 108
    new-instance v2, Lcom/android/internal/os/RuntimeInit$1;

    #@b
    invoke-direct {v2}, Lcom/android/internal/os/RuntimeInit$1;-><init>()V

    #@e
    invoke-static {v2}, Lorg/apache/harmony/luni/internal/util/TimezoneGetter;->setInstance(Lorg/apache/harmony/luni/internal/util/TimezoneGetter;)V

    #@11
    .line 114
    invoke-static {v3}, Ljava/util/TimeZone;->setDefault(Ljava/util/TimeZone;)V

    #@14
    .line 123
    invoke-static {}, Ljava/util/logging/LogManager;->getLogManager()Ljava/util/logging/LogManager;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2}, Ljava/util/logging/LogManager;->reset()V

    #@1b
    .line 124
    new-instance v2, Lcom/android/internal/logging/AndroidConfig;

    #@1d
    invoke-direct {v2}, Lcom/android/internal/logging/AndroidConfig;-><init>()V

    #@20
    .line 129
    invoke-static {}, Lcom/android/internal/os/RuntimeInit;->getDefaultUserAgent()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    .line 130
    .local v1, userAgent:Ljava/lang/String;
    const-string v2, "http.agent"

    #@26
    invoke-static {v2, v1}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@29
    .line 135
    invoke-static {}, Lcom/android/server/NetworkManagementSocketTagger;->install()V

    #@2c
    .line 143
    const-string/jumbo v2, "ro.kernel.android.tracing"

    #@2f
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@32
    move-result-object v0

    #@33
    .line 144
    .local v0, trace:Ljava/lang/String;
    const-string v2, "1"

    #@35
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@38
    move-result v2

    #@39
    if-eqz v2, :cond_45

    #@3b
    .line 145
    const-string v2, "AndroidRuntime"

    #@3d
    const-string v3, "NOTE: emulator trace profiling enabled"

    #@3f
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 146
    invoke-static {}, Landroid/os/Debug;->enableEmulatorTraceOutput()V

    #@45
    .line 149
    :cond_45
    const/4 v2, 0x1

    #@46
    sput-boolean v2, Lcom/android/internal/os/RuntimeInit;->initialized:Z

    #@48
    .line 150
    return-void
.end method

.method public static final getApplicationObject()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 356
    sget-object v0, Lcom/android/internal/os/RuntimeInit;->mApplicationObject:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method private static getDefaultUserAgent()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 157
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    const/16 v4, 0x40

    #@4
    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 158
    .local v2, result:Ljava/lang/StringBuilder;
    const-string v4, "Dalvik/"

    #@9
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    .line 159
    const-string/jumbo v4, "java.vm.version"

    #@f
    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    #@12
    move-result-object v4

    #@13
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    .line 160
    const-string v4, " (Linux; U; Android "

    #@18
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    .line 162
    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    #@1d
    .line 163
    .local v3, version:Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@20
    move-result v4

    #@21
    if-lez v4, :cond_5a

    #@23
    .end local v3           #version:Ljava/lang/String;
    :goto_23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    .line 166
    const-string v4, "REL"

    #@28
    sget-object v5, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    #@2a
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d
    move-result v4

    #@2e
    if-eqz v4, :cond_40

    #@30
    .line 167
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    #@32
    .line 168
    .local v1, model:Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@35
    move-result v4

    #@36
    if-lez v4, :cond_40

    #@38
    .line 169
    const-string v4, "; "

    #@3a
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    .line 170
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    .line 173
    .end local v1           #model:Ljava/lang/String;
    :cond_40
    sget-object v0, Landroid/os/Build;->ID:Ljava/lang/String;

    #@42
    .line 174
    .local v0, id:Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@45
    move-result v4

    #@46
    if-lez v4, :cond_50

    #@48
    .line 175
    const-string v4, " Build/"

    #@4a
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    .line 176
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    .line 178
    :cond_50
    const-string v4, ")"

    #@52
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    .line 179
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v4

    #@59
    return-object v4

    #@5a
    .line 163
    .end local v0           #id:Ljava/lang/String;
    .restart local v3       #version:Ljava/lang/String;
    :cond_5a
    const-string v3, "1.0"

    #@5c
    goto :goto_23
.end method

.method private static invokeStaticMain(Ljava/lang/String;[Ljava/lang/String;)V
    .registers 10
    .parameter "className"
    .parameter "argv"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller;
        }
    .end annotation

    #@0
    .prologue
    .line 195
    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_3} :catch_3c

    #@3
    move-result-object v0

    #@4
    .line 204
    .local v0, cl:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :try_start_4
    const-string/jumbo v4, "main"

    #@7
    const/4 v5, 0x1

    #@8
    new-array v5, v5, [Ljava/lang/Class;

    #@a
    const/4 v6, 0x0

    #@b
    const-class v7, [Ljava/lang/String;

    #@d
    aput-object v7, v5, v6

    #@f
    invoke-virtual {v0, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_12
    .catch Ljava/lang/NoSuchMethodException; {:try_start_4 .. :try_end_12} :catch_56
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_12} :catch_70

    #@12
    move-result-object v2

    #@13
    .line 213
    .local v2, m:Ljava/lang/reflect/Method;
    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getModifiers()I

    #@16
    move-result v3

    #@17
    .line 214
    .local v3, modifiers:I
    invoke-static {v3}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    #@1a
    move-result v4

    #@1b
    if-eqz v4, :cond_23

    #@1d
    invoke-static {v3}, Ljava/lang/reflect/Modifier;->isPublic(I)Z

    #@20
    move-result v4

    #@21
    if-nez v4, :cond_8a

    #@23
    .line 215
    :cond_23
    new-instance v4, Ljava/lang/RuntimeException;

    #@25
    new-instance v5, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v6, "Main method is not public and static on "

    #@2c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v5

    #@30
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v5

    #@34
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v5

    #@38
    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@3b
    throw v4

    #@3c
    .line 196
    .end local v0           #cl:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    .end local v2           #m:Ljava/lang/reflect/Method;
    .end local v3           #modifiers:I
    :catch_3c
    move-exception v1

    #@3d
    .line 197
    .local v1, ex:Ljava/lang/ClassNotFoundException;
    new-instance v4, Ljava/lang/RuntimeException;

    #@3f
    new-instance v5, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v6, "Missing class when invoking static main "

    #@46
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v5

    #@4a
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v5

    #@4e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v5

    #@52
    invoke-direct {v4, v5, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@55
    throw v4

    #@56
    .line 205
    .end local v1           #ex:Ljava/lang/ClassNotFoundException;
    .restart local v0       #cl:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :catch_56
    move-exception v1

    #@57
    .line 206
    .local v1, ex:Ljava/lang/NoSuchMethodException;
    new-instance v4, Ljava/lang/RuntimeException;

    #@59
    new-instance v5, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v6, "Missing static main on "

    #@60
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v5

    #@64
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v5

    #@68
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v5

    #@6c
    invoke-direct {v4, v5, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@6f
    throw v4

    #@70
    .line 208
    .end local v1           #ex:Ljava/lang/NoSuchMethodException;
    :catch_70
    move-exception v1

    #@71
    .line 209
    .local v1, ex:Ljava/lang/SecurityException;
    new-instance v4, Ljava/lang/RuntimeException;

    #@73
    new-instance v5, Ljava/lang/StringBuilder;

    #@75
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@78
    const-string v6, "Problem getting static main on "

    #@7a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v5

    #@7e
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v5

    #@82
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v5

    #@86
    invoke-direct {v4, v5, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@89
    throw v4

    #@8a
    .line 225
    .end local v1           #ex:Ljava/lang/SecurityException;
    .restart local v2       #m:Ljava/lang/reflect/Method;
    .restart local v3       #modifiers:I
    :cond_8a
    new-instance v4, Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller;

    #@8c
    invoke-direct {v4, v2, p1}, Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller;-><init>(Ljava/lang/reflect/Method;[Ljava/lang/String;)V

    #@8f
    throw v4
.end method

.method public static final main([Ljava/lang/String;)V
    .registers 3
    .parameter "argv"

    #@0
    .prologue
    .line 229
    array-length v0, p0

    #@1
    const/4 v1, 0x2

    #@2
    if-ne v0, v1, :cond_12

    #@4
    const/4 v0, 0x1

    #@5
    aget-object v0, p0, v0

    #@7
    const-string v1, "application"

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_12

    #@f
    .line 231
    invoke-static {}, Lcom/android/internal/os/RuntimeInit;->redirectLogStreams()V

    #@12
    .line 236
    :cond_12
    invoke-static {}, Lcom/android/internal/os/RuntimeInit;->commonInit()V

    #@15
    .line 242
    invoke-static {}, Lcom/android/internal/os/RuntimeInit;->nativeFinishInit()V

    #@18
    .line 245
    return-void
.end method

.method private static final native nativeFinishInit()V
.end method

.method private static final native nativeSetExitWithoutCleanup(Z)V
.end method

.method private static final native nativeZygoteInit()V
.end method

.method public static redirectLogStreams()V
    .registers 3

    #@0
    .prologue
    .line 321
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    #@2
    invoke-virtual {v0}, Ljava/io/PrintStream;->close()V

    #@5
    .line 322
    new-instance v0, Lcom/android/internal/os/AndroidPrintStream;

    #@7
    const/4 v1, 0x4

    #@8
    const-string v2, "System.out"

    #@a
    invoke-direct {v0, v1, v2}, Lcom/android/internal/os/AndroidPrintStream;-><init>(ILjava/lang/String;)V

    #@d
    invoke-static {v0}, Ljava/lang/System;->setOut(Ljava/io/PrintStream;)V

    #@10
    .line 323
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    #@12
    invoke-virtual {v0}, Ljava/io/PrintStream;->close()V

    #@15
    .line 324
    new-instance v0, Lcom/android/internal/os/AndroidPrintStream;

    #@17
    const/4 v1, 0x5

    #@18
    const-string v2, "System.err"

    #@1a
    invoke-direct {v0, v1, v2}, Lcom/android/internal/os/AndroidPrintStream;-><init>(ILjava/lang/String;)V

    #@1d
    invoke-static {v0}, Ljava/lang/System;->setErr(Ljava/io/PrintStream;)V

    #@20
    .line 325
    return-void
.end method

.method public static final setApplicationObject(Landroid/os/IBinder;)V
    .registers 1
    .parameter "app"

    #@0
    .prologue
    .line 352
    sput-object p0, Lcom/android/internal/os/RuntimeInit;->mApplicationObject:Landroid/os/IBinder;

    #@2
    .line 353
    return-void
.end method

.method public static wrapperInit(I[Ljava/lang/String;)V
    .registers 2
    .parameter "targetSdkVersion"
    .parameter "argv"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller;
        }
    .end annotation

    #@0
    .prologue
    .line 287
    invoke-static {p0, p1}, Lcom/android/internal/os/RuntimeInit;->applicationInit(I[Ljava/lang/String;)V

    #@3
    .line 288
    return-void
.end method

.method public static wtf(Ljava/lang/String;Ljava/lang/Throwable;)V
    .registers 6
    .parameter "tag"
    .parameter "t"

    #@0
    .prologue
    .line 336
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    sget-object v2, Lcom/android/internal/os/RuntimeInit;->mApplicationObject:Landroid/os/IBinder;

    #@6
    new-instance v3, Landroid/app/ApplicationErrorReport$CrashInfo;

    #@8
    invoke-direct {v3, p1}, Landroid/app/ApplicationErrorReport$CrashInfo;-><init>(Ljava/lang/Throwable;)V

    #@b
    invoke-interface {v1, v2, p0, v3}, Landroid/app/IActivityManager;->handleApplicationWtf(Landroid/os/IBinder;Ljava/lang/String;Landroid/app/ApplicationErrorReport$CrashInfo;)Z

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_1d

    #@11
    .line 339
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@14
    move-result v1

    #@15
    invoke-static {v1}, Landroid/os/Process;->killProcess(I)V

    #@18
    .line 340
    const/16 v1, 0xa

    #@1a
    invoke-static {v1}, Ljava/lang/System;->exit(I)V
    :try_end_1d
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_1d} :catch_1e

    #@1d
    .line 345
    :cond_1d
    :goto_1d
    return-void

    #@1e
    .line 342
    :catch_1e
    move-exception v0

    #@1f
    .line 343
    .local v0, t2:Ljava/lang/Throwable;
    const-string v1, "AndroidRuntime"

    #@21
    const-string v2, "Error reporting WTF"

    #@23
    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@26
    goto :goto_1d
.end method

.method public static final zygoteInit(I[Ljava/lang/String;)V
    .registers 2
    .parameter "targetSdkVersion"
    .parameter "argv"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller;
        }
    .end annotation

    #@0
    .prologue
    .line 264
    invoke-static {}, Lcom/android/internal/os/RuntimeInit;->redirectLogStreams()V

    #@3
    .line 266
    invoke-static {}, Lcom/android/internal/os/RuntimeInit;->commonInit()V

    #@6
    .line 267
    invoke-static {}, Lcom/android/internal/os/RuntimeInit;->nativeZygoteInit()V

    #@9
    .line 269
    invoke-static {p0, p1}, Lcom/android/internal/os/RuntimeInit;->applicationInit(I[Ljava/lang/String;)V

    #@c
    .line 270
    return-void
.end method
