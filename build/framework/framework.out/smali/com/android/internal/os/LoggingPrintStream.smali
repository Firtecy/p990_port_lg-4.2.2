.class abstract Lcom/android/internal/os/LoggingPrintStream;
.super Ljava/io/PrintStream;
.source "LoggingPrintStream.java"


# instance fields
.field private final builder:Ljava/lang/StringBuilder;

.field private decodedChars:Ljava/nio/CharBuffer;

.field private decoder:Ljava/nio/charset/CharsetDecoder;

.field private encodedBytes:Ljava/nio/ByteBuffer;

.field private final formatter:Ljava/util/Formatter;


# direct methods
.method protected constructor <init>()V
    .registers 4

    #@0
    .prologue
    .line 62
    new-instance v0, Lcom/android/internal/os/LoggingPrintStream$1;

    #@2
    invoke-direct {v0}, Lcom/android/internal/os/LoggingPrintStream$1;-><init>()V

    #@5
    invoke-direct {p0, v0}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    #@8
    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    iput-object v0, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@f
    .line 182
    new-instance v0, Ljava/util/Formatter;

    #@11
    iget-object v1, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@13
    const/4 v2, 0x0

    #@14
    invoke-direct {v0, v1, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    #@17
    iput-object v0, p0, Lcom/android/internal/os/LoggingPrintStream;->formatter:Ljava/util/Formatter;

    #@19
    .line 67
    return-void
.end method

.method private flush(Z)V
    .registers 8
    .parameter "completely"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 86
    iget-object v3, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    #@6
    move-result v0

    #@7
    .line 88
    .local v0, length:I
    const/4 v2, 0x0

    #@8
    .line 93
    .local v2, start:I
    :goto_8
    if-ge v2, v0, :cond_21

    #@a
    iget-object v3, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@c
    const-string v4, "\n"

    #@e
    invoke-virtual {v3, v4, v2}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;I)I

    #@11
    move-result v1

    #@12
    .local v1, nextBreak:I
    const/4 v3, -0x1

    #@13
    if-eq v1, v3, :cond_21

    #@15
    .line 94
    iget-object v3, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@17
    invoke-virtual {v3, v2, v1}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {p0, v3}, Lcom/android/internal/os/LoggingPrintStream;->log(Ljava/lang/String;)V

    #@1e
    .line 95
    add-int/lit8 v2, v1, 0x1

    #@20
    goto :goto_8

    #@21
    .line 98
    .end local v1           #nextBreak:I
    :cond_21
    if-eqz p1, :cond_34

    #@23
    .line 100
    if-ge v2, v0, :cond_2e

    #@25
    .line 101
    iget-object v3, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@27
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {p0, v3}, Lcom/android/internal/os/LoggingPrintStream;->log(Ljava/lang/String;)V

    #@2e
    .line 103
    :cond_2e
    iget-object v3, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@30
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    #@33
    .line 108
    :goto_33
    return-void

    #@34
    .line 106
    :cond_34
    iget-object v3, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@36
    invoke-virtual {v3, v5, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    #@39
    goto :goto_33
.end method


# virtual methods
.method public declared-synchronized append(C)Ljava/io/PrintStream;
    .registers 3
    .parameter "c"

    #@0
    .prologue
    .line 327
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/android/internal/os/LoggingPrintStream;->print(C)V
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_6

    #@4
    .line 328
    monitor-exit p0

    #@5
    return-object p0

    #@6
    .line 327
    :catchall_6
    move-exception v0

    #@7
    monitor-exit p0

    #@8
    throw v0
.end method

.method public declared-synchronized append(Ljava/lang/CharSequence;)Ljava/io/PrintStream;
    .registers 3
    .parameter "csq"

    #@0
    .prologue
    .line 333
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@3
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@6
    .line 334
    const/4 v0, 0x0

    #@7
    invoke-direct {p0, v0}, Lcom/android/internal/os/LoggingPrintStream;->flush(Z)V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 335
    monitor-exit p0

    #@b
    return-object p0

    #@c
    .line 333
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized append(Ljava/lang/CharSequence;II)Ljava/io/PrintStream;
    .registers 5
    .parameter "csq"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 341
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@3
    invoke-virtual {v0, p1, p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    #@6
    .line 342
    const/4 v0, 0x0

    #@7
    invoke-direct {p0, v0}, Lcom/android/internal/os/LoggingPrintStream;->flush(Z)V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 343
    monitor-exit p0

    #@b
    return-object p0

    #@c
    .line 341
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public bridge synthetic append(C)Ljava/lang/Appendable;
    .registers 3
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 36
    invoke-virtual {p0, p1}, Lcom/android/internal/os/LoggingPrintStream;->append(C)Ljava/io/PrintStream;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public bridge synthetic append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;
    .registers 3
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 36
    invoke-virtual {p0, p1}, Lcom/android/internal/os/LoggingPrintStream;->append(Ljava/lang/CharSequence;)Ljava/io/PrintStream;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public bridge synthetic append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 36
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/internal/os/LoggingPrintStream;->append(Ljava/lang/CharSequence;II)Ljava/io/PrintStream;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public checkError()Z
    .registers 2

    #@0
    .prologue
    .line 156
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public close()V
    .registers 1

    #@0
    .prologue
    .line 165
    return-void
.end method

.method public declared-synchronized flush()V
    .registers 2

    #@0
    .prologue
    .line 76
    monitor-enter p0

    #@1
    const/4 v0, 0x1

    #@2
    :try_start_2
    invoke-direct {p0, v0}, Lcom/android/internal/os/LoggingPrintStream;->flush(Z)V
    :try_end_5
    .catchall {:try_start_2 .. :try_end_5} :catchall_7

    #@5
    .line 77
    monitor-exit p0

    #@6
    return-void

    #@7
    .line 76
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public varargs format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;
    .registers 4
    .parameter "format"
    .parameter "args"

    #@0
    .prologue
    .line 169
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0, p1, p2}, Lcom/android/internal/os/LoggingPrintStream;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public varargs declared-synchronized format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;
    .registers 6
    .parameter "l"
    .parameter "format"
    .parameter "args"

    #@0
    .prologue
    .line 187
    monitor-enter p0

    #@1
    if-nez p2, :cond_e

    #@3
    .line 188
    :try_start_3
    new-instance v0, Ljava/lang/NullPointerException;

    #@5
    const-string v1, "format"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0
    :try_end_b
    .catchall {:try_start_3 .. :try_end_b} :catchall_b

    #@b
    .line 187
    :catchall_b
    move-exception v0

    #@c
    monitor-exit p0

    #@d
    throw v0

    #@e
    .line 191
    :cond_e
    :try_start_e
    iget-object v0, p0, Lcom/android/internal/os/LoggingPrintStream;->formatter:Ljava/util/Formatter;

    #@10
    invoke-virtual {v0, p1, p2, p3}, Ljava/util/Formatter;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    #@13
    .line 192
    const/4 v0, 0x0

    #@14
    invoke-direct {p0, v0}, Lcom/android/internal/os/LoggingPrintStream;->flush(Z)V
    :try_end_17
    .catchall {:try_start_e .. :try_end_17} :catchall_b

    #@17
    .line 193
    monitor-exit p0

    #@18
    return-object p0
.end method

.method protected abstract log(Ljava/lang/String;)V
.end method

.method public declared-synchronized print(C)V
    .registers 3
    .parameter "ch"

    #@0
    .prologue
    .line 204
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@3
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@6
    .line 205
    const/16 v0, 0xa

    #@8
    if-ne p1, v0, :cond_e

    #@a
    .line 206
    const/4 v0, 0x0

    #@b
    invoke-direct {p0, v0}, Lcom/android/internal/os/LoggingPrintStream;->flush(Z)V
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_10

    #@e
    .line 208
    :cond_e
    monitor-exit p0

    #@f
    return-void

    #@10
    .line 204
    :catchall_10
    move-exception v0

    #@11
    monitor-exit p0

    #@12
    throw v0
.end method

.method public declared-synchronized print(D)V
    .registers 4
    .parameter "dnum"

    #@0
    .prologue
    .line 212
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@3
    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    #@6
    .line 213
    monitor-exit p0

    #@7
    return-void

    #@8
    .line 212
    :catchall_8
    move-exception v0

    #@9
    monitor-exit p0

    #@a
    throw v0
.end method

.method public declared-synchronized print(F)V
    .registers 3
    .parameter "fnum"

    #@0
    .prologue
    .line 217
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@3
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    #@6
    .line 218
    monitor-exit p0

    #@7
    return-void

    #@8
    .line 217
    :catchall_8
    move-exception v0

    #@9
    monitor-exit p0

    #@a
    throw v0
.end method

.method public declared-synchronized print(I)V
    .registers 3
    .parameter "inum"

    #@0
    .prologue
    .line 222
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@3
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    #@6
    .line 223
    monitor-exit p0

    #@7
    return-void

    #@8
    .line 222
    :catchall_8
    move-exception v0

    #@9
    monitor-exit p0

    #@a
    throw v0
.end method

.method public declared-synchronized print(J)V
    .registers 4
    .parameter "lnum"

    #@0
    .prologue
    .line 227
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@3
    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    #@6
    .line 228
    monitor-exit p0

    #@7
    return-void

    #@8
    .line 227
    :catchall_8
    move-exception v0

    #@9
    monitor-exit p0

    #@a
    throw v0
.end method

.method public declared-synchronized print(Ljava/lang/Object;)V
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 232
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@3
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6
    .line 233
    const/4 v0, 0x0

    #@7
    invoke-direct {p0, v0}, Lcom/android/internal/os/LoggingPrintStream;->flush(Z)V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 234
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 232
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized print(Ljava/lang/String;)V
    .registers 3
    .parameter "str"

    #@0
    .prologue
    .line 238
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@3
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6
    .line 239
    const/4 v0, 0x0

    #@7
    invoke-direct {p0, v0}, Lcom/android/internal/os/LoggingPrintStream;->flush(Z)V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 240
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 238
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized print(Z)V
    .registers 3
    .parameter "bool"

    #@0
    .prologue
    .line 244
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@3
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    #@6
    .line 245
    monitor-exit p0

    #@7
    return-void

    #@8
    .line 244
    :catchall_8
    move-exception v0

    #@9
    monitor-exit p0

    #@a
    throw v0
.end method

.method public declared-synchronized print([C)V
    .registers 3
    .parameter "charArray"

    #@0
    .prologue
    .line 198
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@3
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    #@6
    .line 199
    const/4 v0, 0x0

    #@7
    invoke-direct {p0, v0}, Lcom/android/internal/os/LoggingPrintStream;->flush(Z)V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 200
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 198
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public varargs printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;
    .registers 4
    .parameter "format"
    .parameter "args"

    #@0
    .prologue
    .line 174
    invoke-virtual {p0, p1, p2}, Lcom/android/internal/os/LoggingPrintStream;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public varargs printf(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;
    .registers 5
    .parameter "l"
    .parameter "format"
    .parameter "args"

    #@0
    .prologue
    .line 179
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/internal/os/LoggingPrintStream;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public declared-synchronized println()V
    .registers 2

    #@0
    .prologue
    .line 249
    monitor-enter p0

    #@1
    const/4 v0, 0x1

    #@2
    :try_start_2
    invoke-direct {p0, v0}, Lcom/android/internal/os/LoggingPrintStream;->flush(Z)V
    :try_end_5
    .catchall {:try_start_2 .. :try_end_5} :catchall_7

    #@5
    .line 250
    monitor-exit p0

    #@6
    return-void

    #@7
    .line 249
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0

    #@9
    throw v0
.end method

.method public declared-synchronized println(C)V
    .registers 3
    .parameter "ch"

    #@0
    .prologue
    .line 260
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@3
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@6
    .line 261
    const/4 v0, 0x1

    #@7
    invoke-direct {p0, v0}, Lcom/android/internal/os/LoggingPrintStream;->flush(Z)V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 262
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 260
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized println(D)V
    .registers 4
    .parameter "dnum"

    #@0
    .prologue
    .line 266
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@3
    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@6
    .line 267
    const/4 v0, 0x1

    #@7
    invoke-direct {p0, v0}, Lcom/android/internal/os/LoggingPrintStream;->flush(Z)V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 268
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 266
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized println(F)V
    .registers 3
    .parameter "fnum"

    #@0
    .prologue
    .line 272
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@3
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@6
    .line 273
    const/4 v0, 0x1

    #@7
    invoke-direct {p0, v0}, Lcom/android/internal/os/LoggingPrintStream;->flush(Z)V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 274
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 272
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized println(I)V
    .registers 3
    .parameter "inum"

    #@0
    .prologue
    .line 278
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@3
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6
    .line 279
    const/4 v0, 0x1

    #@7
    invoke-direct {p0, v0}, Lcom/android/internal/os/LoggingPrintStream;->flush(Z)V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 280
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 278
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized println(J)V
    .registers 4
    .parameter "lnum"

    #@0
    .prologue
    .line 284
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@3
    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@6
    .line 285
    const/4 v0, 0x1

    #@7
    invoke-direct {p0, v0}, Lcom/android/internal/os/LoggingPrintStream;->flush(Z)V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 286
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 284
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized println(Ljava/lang/Object;)V
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 290
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@3
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6
    .line 291
    const/4 v0, 0x1

    #@7
    invoke-direct {p0, v0}, Lcom/android/internal/os/LoggingPrintStream;->flush(Z)V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 292
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 290
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized println(Ljava/lang/String;)V
    .registers 6
    .parameter "s"

    #@0
    .prologue
    .line 296
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v3, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    #@6
    move-result v3

    #@7
    if-nez v3, :cond_2e

    #@9
    .line 298
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@c
    move-result v0

    #@d
    .line 300
    .local v0, length:I
    const/4 v2, 0x0

    #@e
    .line 305
    .local v2, start:I
    :goto_e
    if-ge v2, v0, :cond_23

    #@10
    const/16 v3, 0xa

    #@12
    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->indexOf(II)I

    #@15
    move-result v1

    #@16
    .local v1, nextBreak:I
    const/4 v3, -0x1

    #@17
    if-eq v1, v3, :cond_23

    #@19
    .line 306
    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {p0, v3}, Lcom/android/internal/os/LoggingPrintStream;->log(Ljava/lang/String;)V

    #@20
    .line 307
    add-int/lit8 v2, v1, 0x1

    #@22
    goto :goto_e

    #@23
    .line 310
    .end local v1           #nextBreak:I
    :cond_23
    if-ge v2, v0, :cond_2c

    #@25
    .line 311
    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {p0, v3}, Lcom/android/internal/os/LoggingPrintStream;->log(Ljava/lang/String;)V
    :try_end_2c
    .catchall {:try_start_1 .. :try_end_2c} :catchall_38

    #@2c
    .line 317
    .end local v0           #length:I
    .end local v2           #start:I
    :cond_2c
    :goto_2c
    monitor-exit p0

    #@2d
    return-void

    #@2e
    .line 314
    :cond_2e
    :try_start_2e
    iget-object v3, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@30
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    .line 315
    const/4 v3, 0x1

    #@34
    invoke-direct {p0, v3}, Lcom/android/internal/os/LoggingPrintStream;->flush(Z)V
    :try_end_37
    .catchall {:try_start_2e .. :try_end_37} :catchall_38

    #@37
    goto :goto_2c

    #@38
    .line 296
    :catchall_38
    move-exception v3

    #@39
    monitor-exit p0

    #@3a
    throw v3
.end method

.method public declared-synchronized println(Z)V
    .registers 3
    .parameter "bool"

    #@0
    .prologue
    .line 321
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@3
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@6
    .line 322
    const/4 v0, 0x1

    #@7
    invoke-direct {p0, v0}, Lcom/android/internal/os/LoggingPrintStream;->flush(Z)V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 323
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 321
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized println([C)V
    .registers 3
    .parameter "charArray"

    #@0
    .prologue
    .line 254
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@3
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    #@6
    .line 255
    const/4 v0, 0x1

    #@7
    invoke-direct {p0, v0}, Lcom/android/internal/os/LoggingPrintStream;->flush(Z)V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 256
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 254
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method protected setError()V
    .registers 1

    #@0
    .prologue
    .line 161
    return-void
.end method

.method public write(I)V
    .registers 6
    .parameter "oneByte"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 111
    new-array v0, v3, [B

    #@4
    int-to-byte v1, p1

    #@5
    aput-byte v1, v0, v2

    #@7
    invoke-virtual {p0, v0, v2, v3}, Lcom/android/internal/os/LoggingPrintStream;->write([BII)V

    #@a
    .line 112
    return-void
.end method

.method public write([B)V
    .registers 4
    .parameter "buffer"

    #@0
    .prologue
    .line 116
    const/4 v0, 0x0

    #@1
    array-length v1, p1

    #@2
    invoke-virtual {p0, p1, v0, v1}, Lcom/android/internal/os/LoggingPrintStream;->write([BII)V

    #@5
    .line 117
    return-void
.end method

.method public declared-synchronized write([BII)V
    .registers 11
    .parameter "bytes"
    .parameter "start"
    .parameter "count"

    #@0
    .prologue
    .line 121
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v3, p0, Lcom/android/internal/os/LoggingPrintStream;->decoder:Ljava/nio/charset/CharsetDecoder;

    #@3
    if-nez v3, :cond_2b

    #@5
    .line 122
    const/16 v3, 0x50

    #@7
    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@a
    move-result-object v3

    #@b
    iput-object v3, p0, Lcom/android/internal/os/LoggingPrintStream;->encodedBytes:Ljava/nio/ByteBuffer;

    #@d
    .line 123
    const/16 v3, 0x50

    #@f
    invoke-static {v3}, Ljava/nio/CharBuffer;->allocate(I)Ljava/nio/CharBuffer;

    #@12
    move-result-object v3

    #@13
    iput-object v3, p0, Lcom/android/internal/os/LoggingPrintStream;->decodedChars:Ljava/nio/CharBuffer;

    #@15
    .line 124
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3}, Ljava/nio/charset/Charset;->newDecoder()Ljava/nio/charset/CharsetDecoder;

    #@1c
    move-result-object v3

    #@1d
    sget-object v4, Ljava/nio/charset/CodingErrorAction;->REPLACE:Ljava/nio/charset/CodingErrorAction;

    #@1f
    invoke-virtual {v3, v4}, Ljava/nio/charset/CharsetDecoder;->onMalformedInput(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetDecoder;

    #@22
    move-result-object v3

    #@23
    sget-object v4, Ljava/nio/charset/CodingErrorAction;->REPLACE:Ljava/nio/charset/CodingErrorAction;

    #@25
    invoke-virtual {v3, v4}, Ljava/nio/charset/CharsetDecoder;->onUnmappableCharacter(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetDecoder;

    #@28
    move-result-object v3

    #@29
    iput-object v3, p0, Lcom/android/internal/os/LoggingPrintStream;->decoder:Ljava/nio/charset/CharsetDecoder;

    #@2b
    .line 129
    :cond_2b
    add-int v1, p2, p3

    #@2d
    .line 130
    .local v1, end:I
    :goto_2d
    if-ge p2, v1, :cond_71

    #@2f
    .line 133
    iget-object v3, p0, Lcom/android/internal/os/LoggingPrintStream;->encodedBytes:Ljava/nio/ByteBuffer;

    #@31
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->remaining()I

    #@34
    move-result v3

    #@35
    sub-int v4, v1, p2

    #@37
    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    #@3a
    move-result v2

    #@3b
    .line 134
    .local v2, numBytes:I
    iget-object v3, p0, Lcom/android/internal/os/LoggingPrintStream;->encodedBytes:Ljava/nio/ByteBuffer;

    #@3d
    invoke-virtual {v3, p1, p2, v2}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    #@40
    .line 135
    add-int/2addr p2, v2

    #@41
    .line 137
    iget-object v3, p0, Lcom/android/internal/os/LoggingPrintStream;->encodedBytes:Ljava/nio/ByteBuffer;

    #@43
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    #@46
    .line 141
    :cond_46
    iget-object v3, p0, Lcom/android/internal/os/LoggingPrintStream;->decoder:Ljava/nio/charset/CharsetDecoder;

    #@48
    iget-object v4, p0, Lcom/android/internal/os/LoggingPrintStream;->encodedBytes:Ljava/nio/ByteBuffer;

    #@4a
    iget-object v5, p0, Lcom/android/internal/os/LoggingPrintStream;->decodedChars:Ljava/nio/CharBuffer;

    #@4c
    const/4 v6, 0x0

    #@4d
    invoke-virtual {v3, v4, v5, v6}, Ljava/nio/charset/CharsetDecoder;->decode(Ljava/nio/ByteBuffer;Ljava/nio/CharBuffer;Z)Ljava/nio/charset/CoderResult;

    #@50
    move-result-object v0

    #@51
    .line 144
    .local v0, coderResult:Ljava/nio/charset/CoderResult;
    iget-object v3, p0, Lcom/android/internal/os/LoggingPrintStream;->decodedChars:Ljava/nio/CharBuffer;

    #@53
    invoke-virtual {v3}, Ljava/nio/CharBuffer;->flip()Ljava/nio/Buffer;

    #@56
    .line 145
    iget-object v3, p0, Lcom/android/internal/os/LoggingPrintStream;->builder:Ljava/lang/StringBuilder;

    #@58
    iget-object v4, p0, Lcom/android/internal/os/LoggingPrintStream;->decodedChars:Ljava/nio/CharBuffer;

    #@5a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@5d
    .line 146
    iget-object v3, p0, Lcom/android/internal/os/LoggingPrintStream;->decodedChars:Ljava/nio/CharBuffer;

    #@5f
    invoke-virtual {v3}, Ljava/nio/CharBuffer;->clear()Ljava/nio/Buffer;

    #@62
    .line 147
    invoke-virtual {v0}, Ljava/nio/charset/CoderResult;->isOverflow()Z

    #@65
    move-result v3

    #@66
    if-nez v3, :cond_46

    #@68
    .line 148
    iget-object v3, p0, Lcom/android/internal/os/LoggingPrintStream;->encodedBytes:Ljava/nio/ByteBuffer;

    #@6a
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->compact()Ljava/nio/ByteBuffer;
    :try_end_6d
    .catchall {:try_start_1 .. :try_end_6d} :catchall_6e

    #@6d
    goto :goto_2d

    #@6e
    .line 121
    .end local v0           #coderResult:Ljava/nio/charset/CoderResult;
    .end local v1           #end:I
    .end local v2           #numBytes:I
    :catchall_6e
    move-exception v3

    #@6f
    monitor-exit p0

    #@70
    throw v3

    #@71
    .line 150
    .restart local v1       #end:I
    :cond_71
    const/4 v3, 0x0

    #@72
    :try_start_72
    invoke-direct {p0, v3}, Lcom/android/internal/os/LoggingPrintStream;->flush(Z)V
    :try_end_75
    .catchall {:try_start_72 .. :try_end_75} :catchall_6e

    #@75
    .line 151
    monitor-exit p0

    #@76
    return-void
.end method
