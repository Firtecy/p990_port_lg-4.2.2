.class public Lcom/android/internal/os/SamplingProfilerIntegration;
.super Ljava/lang/Object;
.source "SamplingProfilerIntegration.java"


# static fields
.field public static final SNAPSHOT_DIR:Ljava/lang/String; = "/data/snapshots"

.field private static final TAG:Ljava/lang/String; = "SamplingProfilerIntegration"

.field private static final enabled:Z

.field private static final pending:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private static samplingProfiler:Ldalvik/system/profiler/SamplingProfiler;

.field private static final samplingProfilerDepth:I

.field private static final samplingProfilerMilliseconds:I

.field private static final snapshotWriter:Ljava/util/concurrent/Executor;

.field private static startMillis:J


# direct methods
.method static constructor <clinit>()V
    .registers 6

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 53
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    #@5
    invoke-direct {v1, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    #@8
    sput-object v1, Lcom/android/internal/os/SamplingProfilerIntegration;->pending:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@a
    .line 56
    const-string/jumbo v1, "persist.sys.profiler_ms"

    #@d
    invoke-static {v1, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@10
    move-result v1

    #@11
    sput v1, Lcom/android/internal/os/SamplingProfilerIntegration;->samplingProfilerMilliseconds:I

    #@13
    .line 57
    const-string/jumbo v1, "persist.sys.profiler_depth"

    #@16
    const/4 v2, 0x4

    #@17
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@1a
    move-result v1

    #@1b
    sput v1, Lcom/android/internal/os/SamplingProfilerIntegration;->samplingProfilerDepth:I

    #@1d
    .line 58
    sget v1, Lcom/android/internal/os/SamplingProfilerIntegration;->samplingProfilerMilliseconds:I

    #@1f
    if-lez v1, :cond_6b

    #@21
    .line 59
    new-instance v0, Ljava/io/File;

    #@23
    const-string v1, "/data/snapshots"

    #@25
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@28
    .line 60
    .local v0, dir:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    #@2b
    .line 62
    invoke-virtual {v0, v4, v3}, Ljava/io/File;->setWritable(ZZ)Z

    #@2e
    .line 64
    invoke-virtual {v0, v4, v3}, Ljava/io/File;->setExecutable(ZZ)Z

    #@31
    .line 65
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    #@34
    move-result v1

    #@35
    if-eqz v1, :cond_5f

    #@37
    .line 66
    new-instance v1, Lcom/android/internal/os/SamplingProfilerIntegration$1;

    #@39
    invoke-direct {v1}, Lcom/android/internal/os/SamplingProfilerIntegration$1;-><init>()V

    #@3c
    invoke-static {v1}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    #@3f
    move-result-object v1

    #@40
    sput-object v1, Lcom/android/internal/os/SamplingProfilerIntegration;->snapshotWriter:Ljava/util/concurrent/Executor;

    #@42
    .line 71
    sput-boolean v4, Lcom/android/internal/os/SamplingProfilerIntegration;->enabled:Z

    #@44
    .line 72
    const-string v1, "SamplingProfilerIntegration"

    #@46
    new-instance v2, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v3, "Profiling enabled. Sampling interval ms: "

    #@4d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    sget v3, Lcom/android/internal/os/SamplingProfilerIntegration;->samplingProfilerMilliseconds:I

    #@53
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@56
    move-result-object v2

    #@57
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v2

    #@5b
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 84
    .end local v0           #dir:Ljava/io/File;
    :goto_5e
    return-void

    #@5f
    .line 75
    .restart local v0       #dir:Ljava/io/File;
    :cond_5f
    sput-object v5, Lcom/android/internal/os/SamplingProfilerIntegration;->snapshotWriter:Ljava/util/concurrent/Executor;

    #@61
    .line 76
    sput-boolean v4, Lcom/android/internal/os/SamplingProfilerIntegration;->enabled:Z

    #@63
    .line 77
    const-string v1, "SamplingProfilerIntegration"

    #@65
    const-string v2, "Profiling setup failed. Could not create /data/snapshots"

    #@67
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    goto :goto_5e

    #@6b
    .line 80
    .end local v0           #dir:Ljava/io/File;
    :cond_6b
    sput-object v5, Lcom/android/internal/os/SamplingProfilerIntegration;->snapshotWriter:Ljava/util/concurrent/Executor;

    #@6d
    .line 81
    sput-boolean v3, Lcom/android/internal/os/SamplingProfilerIntegration;->enabled:Z

    #@6f
    .line 82
    const-string v1, "SamplingProfilerIntegration"

    #@71
    const-string v2, "Profiling disabled."

    #@73
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@76
    goto :goto_5e
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 41
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;Landroid/content/pm/PackageInfo;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 41
    invoke-static {p0, p1}, Lcom/android/internal/os/SamplingProfilerIntegration;->writeSnapshotFile(Ljava/lang/String;Landroid/content/pm/PackageInfo;)V

    #@3
    return-void
.end method

.method static synthetic access$100()Ljava/util/concurrent/atomic/AtomicBoolean;
    .registers 1

    #@0
    .prologue
    .line 41
    sget-object v0, Lcom/android/internal/os/SamplingProfilerIntegration;->pending:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    return-object v0
.end method

.method private static generateSnapshotHeader(Ljava/lang/String;Landroid/content/pm/PackageInfo;Ljava/io/PrintStream;)V
    .registers 5
    .parameter "processName"
    .parameter "packageInfo"
    .parameter "out"

    #@0
    .prologue
    .line 216
    const-string v0, "Version: 3"

    #@2
    invoke-virtual {p2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    #@5
    .line 217
    new-instance v0, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v1, "Process: "

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v0

    #@10
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v0

    #@14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v0

    #@18
    invoke-virtual {p2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    #@1b
    .line 218
    if-eqz p1, :cond_4d

    #@1d
    .line 219
    new-instance v0, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v1, "Package: "

    #@24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v0

    #@28
    iget-object v1, p1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@2a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v0

    #@2e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v0

    #@32
    invoke-virtual {p2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    #@35
    .line 220
    new-instance v0, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v1, "Package-Version: "

    #@3c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v0

    #@40
    iget v1, p1, Landroid/content/pm/PackageInfo;->versionCode:I

    #@42
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@45
    move-result-object v0

    #@46
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v0

    #@4a
    invoke-virtual {p2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    #@4d
    .line 222
    :cond_4d
    new-instance v0, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v1, "Build: "

    #@54
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v0

    #@58
    sget-object v1, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    #@5a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v0

    #@5e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v0

    #@62
    invoke-virtual {p2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    #@65
    .line 224
    invoke-virtual {p2}, Ljava/io/PrintStream;->println()V

    #@68
    .line 225
    return-void
.end method

.method public static isEnabled()Z
    .registers 1

    #@0
    .prologue
    .line 93
    sget-boolean v0, Lcom/android/internal/os/SamplingProfilerIntegration;->enabled:Z

    #@2
    return v0
.end method

.method public static start()V
    .registers 7

    #@0
    .prologue
    .line 100
    sget-boolean v2, Lcom/android/internal/os/SamplingProfilerIntegration;->enabled:Z

    #@2
    if-nez v2, :cond_5

    #@4
    .line 113
    .local v0, group:Ljava/lang/ThreadGroup;
    .local v1, threadSet:Ldalvik/system/profiler/SamplingProfiler$ThreadSet;
    :goto_4
    return-void

    #@5
    .line 103
    .end local v0           #group:Ljava/lang/ThreadGroup;
    .end local v1           #threadSet:Ldalvik/system/profiler/SamplingProfiler$ThreadSet;
    :cond_5
    sget-object v2, Lcom/android/internal/os/SamplingProfilerIntegration;->samplingProfiler:Ldalvik/system/profiler/SamplingProfiler;

    #@7
    if-eqz v2, :cond_29

    #@9
    .line 104
    const-string v2, "SamplingProfilerIntegration"

    #@b
    new-instance v3, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v4, "SamplingProfilerIntegration already started at "

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    new-instance v4, Ljava/util/Date;

    #@18
    sget-wide v5, Lcom/android/internal/os/SamplingProfilerIntegration;->startMillis:J

    #@1a
    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    goto :goto_4

    #@29
    .line 108
    :cond_29
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2}, Ljava/lang/Thread;->getThreadGroup()Ljava/lang/ThreadGroup;

    #@30
    move-result-object v0

    #@31
    .line 109
    .restart local v0       #group:Ljava/lang/ThreadGroup;
    invoke-static {v0}, Ldalvik/system/profiler/SamplingProfiler;->newThreadGroupTheadSet(Ljava/lang/ThreadGroup;)Ldalvik/system/profiler/SamplingProfiler$ThreadSet;

    #@34
    move-result-object v1

    #@35
    .line 110
    .restart local v1       #threadSet:Ldalvik/system/profiler/SamplingProfiler$ThreadSet;
    new-instance v2, Ldalvik/system/profiler/SamplingProfiler;

    #@37
    sget v3, Lcom/android/internal/os/SamplingProfilerIntegration;->samplingProfilerDepth:I

    #@39
    invoke-direct {v2, v3, v1}, Ldalvik/system/profiler/SamplingProfiler;-><init>(ILdalvik/system/profiler/SamplingProfiler$ThreadSet;)V

    #@3c
    sput-object v2, Lcom/android/internal/os/SamplingProfilerIntegration;->samplingProfiler:Ldalvik/system/profiler/SamplingProfiler;

    #@3e
    .line 111
    sget-object v2, Lcom/android/internal/os/SamplingProfilerIntegration;->samplingProfiler:Ldalvik/system/profiler/SamplingProfiler;

    #@40
    sget v3, Lcom/android/internal/os/SamplingProfilerIntegration;->samplingProfilerMilliseconds:I

    #@42
    invoke-virtual {v2, v3}, Ldalvik/system/profiler/SamplingProfiler;->start(I)V

    #@45
    .line 112
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@48
    move-result-wide v2

    #@49
    sput-wide v2, Lcom/android/internal/os/SamplingProfilerIntegration;->startMillis:J

    #@4b
    goto :goto_4
.end method

.method public static writeSnapshot(Ljava/lang/String;Landroid/content/pm/PackageInfo;)V
    .registers 5
    .parameter "processName"
    .parameter "packageInfo"

    #@0
    .prologue
    .line 119
    sget-boolean v0, Lcom/android/internal/os/SamplingProfilerIntegration;->enabled:Z

    #@2
    if-nez v0, :cond_5

    #@4
    .line 144
    :cond_4
    :goto_4
    return-void

    #@5
    .line 122
    :cond_5
    sget-object v0, Lcom/android/internal/os/SamplingProfilerIntegration;->samplingProfiler:Ldalvik/system/profiler/SamplingProfiler;

    #@7
    if-nez v0, :cond_11

    #@9
    .line 123
    const-string v0, "SamplingProfilerIntegration"

    #@b
    const-string v1, "SamplingProfilerIntegration is not started"

    #@d
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    goto :goto_4

    #@11
    .line 133
    :cond_11
    sget-object v0, Lcom/android/internal/os/SamplingProfilerIntegration;->pending:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@13
    const/4 v1, 0x0

    #@14
    const/4 v2, 0x1

    #@15
    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    #@18
    move-result v0

    #@19
    if-eqz v0, :cond_4

    #@1b
    .line 134
    sget-object v0, Lcom/android/internal/os/SamplingProfilerIntegration;->snapshotWriter:Ljava/util/concurrent/Executor;

    #@1d
    new-instance v1, Lcom/android/internal/os/SamplingProfilerIntegration$2;

    #@1f
    invoke-direct {v1, p0, p1}, Lcom/android/internal/os/SamplingProfilerIntegration$2;-><init>(Ljava/lang/String;Landroid/content/pm/PackageInfo;)V

    #@22
    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    #@25
    goto :goto_4
.end method

.method private static writeSnapshotFile(Ljava/lang/String;Landroid/content/pm/PackageInfo;)V
    .registers 15
    .parameter "processName"
    .parameter "packageInfo"

    #@0
    .prologue
    .line 163
    sget-boolean v10, Lcom/android/internal/os/SamplingProfilerIntegration;->enabled:Z

    #@2
    if-nez v10, :cond_5

    #@4
    .line 199
    :goto_4
    return-void

    #@5
    .line 166
    :cond_5
    sget-object v10, Lcom/android/internal/os/SamplingProfilerIntegration;->samplingProfiler:Ldalvik/system/profiler/SamplingProfiler;

    #@7
    invoke-virtual {v10}, Ldalvik/system/profiler/SamplingProfiler;->stop()V

    #@a
    .line 174
    const-string v10, ":"

    #@c
    const-string v11, "."

    #@e
    invoke-virtual {p0, v10, v11}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v3

    #@12
    .line 175
    .local v3, name:Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v11, "/data/snapshots/"

    #@19
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v10

    #@1d
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v10

    #@21
    const-string v11, "-"

    #@23
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v10

    #@27
    sget-wide v11, Lcom/android/internal/os/SamplingProfilerIntegration;->startMillis:J

    #@29
    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v10

    #@2d
    const-string v11, ".snapshot"

    #@2f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v10

    #@33
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v7

    #@37
    .line 176
    .local v7, path:Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3a
    move-result-wide v8

    #@3b
    .line 177
    .local v8, start:J
    const/4 v5, 0x0

    #@3c
    .line 179
    .local v5, outputStream:Ljava/io/OutputStream;
    :try_start_3c
    new-instance v6, Ljava/io/BufferedOutputStream;

    #@3e
    new-instance v10, Ljava/io/FileOutputStream;

    #@40
    invoke-direct {v10, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    #@43
    invoke-direct {v6, v10}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_46
    .catchall {:try_start_3c .. :try_end_46} :catchall_c6
    .catch Ljava/io/IOException; {:try_start_3c .. :try_end_46} :catch_ce

    #@46
    .line 180
    .end local v5           #outputStream:Ljava/io/OutputStream;
    .local v6, outputStream:Ljava/io/OutputStream;
    :try_start_46
    new-instance v4, Ljava/io/PrintStream;

    #@48
    invoke-direct {v4, v6}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    #@4b
    .line 181
    .local v4, out:Ljava/io/PrintStream;
    invoke-static {v3, p1, v4}, Lcom/android/internal/os/SamplingProfilerIntegration;->generateSnapshotHeader(Ljava/lang/String;Landroid/content/pm/PackageInfo;Ljava/io/PrintStream;)V

    #@4e
    .line 182
    invoke-virtual {v4}, Ljava/io/PrintStream;->checkError()Z

    #@51
    move-result v10

    #@52
    if-eqz v10, :cond_78

    #@54
    .line 183
    new-instance v10, Ljava/io/IOException;

    #@56
    invoke-direct {v10}, Ljava/io/IOException;-><init>()V

    #@59
    throw v10
    :try_end_5a
    .catchall {:try_start_46 .. :try_end_5a} :catchall_cb
    .catch Ljava/io/IOException; {:try_start_46 .. :try_end_5a} :catch_5a

    #@5a
    .line 186
    .end local v4           #out:Ljava/io/PrintStream;
    :catch_5a
    move-exception v0

    #@5b
    move-object v5, v6

    #@5c
    .line 187
    .end local v6           #outputStream:Ljava/io/OutputStream;
    .local v0, e:Ljava/io/IOException;
    .restart local v5       #outputStream:Ljava/io/OutputStream;
    :goto_5c
    :try_start_5c
    const-string v10, "SamplingProfilerIntegration"

    #@5e
    new-instance v11, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string v12, "Error writing snapshot to "

    #@65
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v11

    #@69
    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v11

    #@6d
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v11

    #@71
    invoke-static {v10, v11, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_74
    .catchall {:try_start_5c .. :try_end_74} :catchall_c6

    #@74
    .line 190
    invoke-static {v5}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@77
    goto :goto_4

    #@78
    .line 185
    .end local v0           #e:Ljava/io/IOException;
    .end local v5           #outputStream:Ljava/io/OutputStream;
    .restart local v4       #out:Ljava/io/PrintStream;
    .restart local v6       #outputStream:Ljava/io/OutputStream;
    :cond_78
    :try_start_78
    sget-object v10, Lcom/android/internal/os/SamplingProfilerIntegration;->samplingProfiler:Ldalvik/system/profiler/SamplingProfiler;

    #@7a
    invoke-virtual {v10}, Ldalvik/system/profiler/SamplingProfiler;->getHprofData()Ldalvik/system/profiler/HprofData;

    #@7d
    move-result-object v10

    #@7e
    invoke-static {v10, v6}, Ldalvik/system/profiler/BinaryHprofWriter;->write(Ldalvik/system/profiler/HprofData;Ljava/io/OutputStream;)V
    :try_end_81
    .catchall {:try_start_78 .. :try_end_81} :catchall_cb
    .catch Ljava/io/IOException; {:try_start_78 .. :try_end_81} :catch_5a

    #@81
    .line 190
    invoke-static {v6}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@84
    .line 194
    new-instance v10, Ljava/io/File;

    #@86
    invoke-direct {v10, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@89
    const/4 v11, 0x1

    #@8a
    const/4 v12, 0x0

    #@8b
    invoke-virtual {v10, v11, v12}, Ljava/io/File;->setReadable(ZZ)Z

    #@8e
    .line 196
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@91
    move-result-wide v10

    #@92
    sub-long v1, v10, v8

    #@94
    .line 197
    .local v1, elapsed:J
    const-string v10, "SamplingProfilerIntegration"

    #@96
    new-instance v11, Ljava/lang/StringBuilder;

    #@98
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@9b
    const-string v12, "Wrote snapshot "

    #@9d
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v11

    #@a1
    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v11

    #@a5
    const-string v12, " in "

    #@a7
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v11

    #@ab
    invoke-virtual {v11, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v11

    #@af
    const-string/jumbo v12, "ms."

    #@b2
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v11

    #@b6
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b9
    move-result-object v11

    #@ba
    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@bd
    .line 198
    sget-object v10, Lcom/android/internal/os/SamplingProfilerIntegration;->samplingProfiler:Ldalvik/system/profiler/SamplingProfiler;

    #@bf
    sget v11, Lcom/android/internal/os/SamplingProfilerIntegration;->samplingProfilerMilliseconds:I

    #@c1
    invoke-virtual {v10, v11}, Ldalvik/system/profiler/SamplingProfiler;->start(I)V

    #@c4
    goto/16 :goto_4

    #@c6
    .line 190
    .end local v1           #elapsed:J
    .end local v4           #out:Ljava/io/PrintStream;
    .end local v6           #outputStream:Ljava/io/OutputStream;
    .restart local v5       #outputStream:Ljava/io/OutputStream;
    :catchall_c6
    move-exception v10

    #@c7
    :goto_c7
    invoke-static {v5}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@ca
    throw v10

    #@cb
    .end local v5           #outputStream:Ljava/io/OutputStream;
    .restart local v6       #outputStream:Ljava/io/OutputStream;
    :catchall_cb
    move-exception v10

    #@cc
    move-object v5, v6

    #@cd
    .end local v6           #outputStream:Ljava/io/OutputStream;
    .restart local v5       #outputStream:Ljava/io/OutputStream;
    goto :goto_c7

    #@ce
    .line 186
    :catch_ce
    move-exception v0

    #@cf
    goto :goto_5c
.end method

.method public static writeZygoteSnapshot()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 150
    sget-boolean v0, Lcom/android/internal/os/SamplingProfilerIntegration;->enabled:Z

    #@3
    if-nez v0, :cond_6

    #@5
    .line 157
    :goto_5
    return-void

    #@6
    .line 153
    :cond_6
    const-string/jumbo v0, "zygote"

    #@9
    invoke-static {v0, v1}, Lcom/android/internal/os/SamplingProfilerIntegration;->writeSnapshotFile(Ljava/lang/String;Landroid/content/pm/PackageInfo;)V

    #@c
    .line 154
    sget-object v0, Lcom/android/internal/os/SamplingProfilerIntegration;->samplingProfiler:Ldalvik/system/profiler/SamplingProfiler;

    #@e
    invoke-virtual {v0}, Ldalvik/system/profiler/SamplingProfiler;->shutdown()V

    #@11
    .line 155
    sput-object v1, Lcom/android/internal/os/SamplingProfilerIntegration;->samplingProfiler:Ldalvik/system/profiler/SamplingProfiler;

    #@13
    .line 156
    const-wide/16 v0, 0x0

    #@15
    sput-wide v0, Lcom/android/internal/os/SamplingProfilerIntegration;->startMillis:J

    #@17
    goto :goto_5
.end method
