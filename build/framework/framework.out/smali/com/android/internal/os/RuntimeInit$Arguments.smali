.class Lcom/android/internal/os/RuntimeInit$Arguments;
.super Ljava/lang/Object;
.source "RuntimeInit.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/os/RuntimeInit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Arguments"
.end annotation


# instance fields
.field startArgs:[Ljava/lang/String;

.field startClass:Ljava/lang/String;


# direct methods
.method constructor <init>([Ljava/lang/String;)V
    .registers 2
    .parameter "args"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 387
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 388
    invoke-direct {p0, p1}, Lcom/android/internal/os/RuntimeInit$Arguments;->parseArgs([Ljava/lang/String;)V

    #@6
    .line 389
    return-void
.end method

.method private parseArgs([Ljava/lang/String;)V
    .registers 8
    .parameter "args"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 396
    const/4 v1, 0x0

    #@1
    .line 397
    .local v1, curArg:I
    :goto_1
    array-length v3, p1

    #@2
    if-ge v1, v3, :cond_10

    #@4
    .line 398
    aget-object v0, p1, v1

    #@6
    .line 400
    .local v0, arg:Ljava/lang/String;
    const-string v3, "--"

    #@8
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v3

    #@c
    if-eqz v3, :cond_1b

    #@e
    .line 401
    add-int/lit8 v1, v1, 0x1

    #@10
    .line 408
    .end local v0           #arg:Ljava/lang/String;
    :cond_10
    array-length v3, p1

    #@11
    if-ne v1, v3, :cond_26

    #@13
    .line 409
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@15
    const-string v4, "Missing classname argument to RuntimeInit!"

    #@17
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v3

    #@1b
    .line 403
    .restart local v0       #arg:Ljava/lang/String;
    :cond_1b
    const-string v3, "--"

    #@1d
    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@20
    move-result v3

    #@21
    if-eqz v3, :cond_10

    #@23
    .line 397
    add-int/lit8 v1, v1, 0x1

    #@25
    goto :goto_1

    #@26
    .line 412
    .end local v0           #arg:Ljava/lang/String;
    :cond_26
    add-int/lit8 v2, v1, 0x1

    #@28
    .end local v1           #curArg:I
    .local v2, curArg:I
    aget-object v3, p1, v1

    #@2a
    iput-object v3, p0, Lcom/android/internal/os/RuntimeInit$Arguments;->startClass:Ljava/lang/String;

    #@2c
    .line 413
    array-length v3, p1

    #@2d
    sub-int/2addr v3, v2

    #@2e
    new-array v3, v3, [Ljava/lang/String;

    #@30
    iput-object v3, p0, Lcom/android/internal/os/RuntimeInit$Arguments;->startArgs:[Ljava/lang/String;

    #@32
    .line 414
    iget-object v3, p0, Lcom/android/internal/os/RuntimeInit$Arguments;->startArgs:[Ljava/lang/String;

    #@34
    const/4 v4, 0x0

    #@35
    iget-object v5, p0, Lcom/android/internal/os/RuntimeInit$Arguments;->startArgs:[Ljava/lang/String;

    #@37
    array-length v5, v5

    #@38
    invoke-static {p1, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@3b
    .line 415
    return-void
.end method
