.class final Lcom/android/internal/os/BatteryStatsImpl$MyHandler;
.super Landroid/os/Handler;
.source "BatteryStatsImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/os/BatteryStatsImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/os/BatteryStatsImpl;


# direct methods
.method constructor <init>(Lcom/android/internal/os/BatteryStatsImpl;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 122
    iput-object p1, p0, Lcom/android/internal/os/BatteryStatsImpl$MyHandler;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 125
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$MyHandler;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@2
    invoke-static {v1}, Lcom/android/internal/os/BatteryStatsImpl;->access$000(Lcom/android/internal/os/BatteryStatsImpl;)Lcom/android/internal/os/BatteryStatsImpl$BatteryCallback;

    #@5
    move-result-object v0

    #@6
    .line 126
    .local v0, cb:Lcom/android/internal/os/BatteryStatsImpl$BatteryCallback;
    iget v1, p1, Landroid/os/Message;->what:I

    #@8
    packed-switch v1, :pswitch_data_20

    #@b
    .line 138
    :cond_b
    :goto_b
    return-void

    #@c
    .line 128
    :pswitch_c
    if-eqz v0, :cond_b

    #@e
    .line 129
    invoke-interface {v0}, Lcom/android/internal/os/BatteryStatsImpl$BatteryCallback;->batteryNeedsCpuUpdate()V

    #@11
    goto :goto_b

    #@12
    .line 133
    :pswitch_12
    if-eqz v0, :cond_b

    #@14
    .line 134
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@16
    if-eqz v1, :cond_1d

    #@18
    const/4 v1, 0x1

    #@19
    :goto_19
    invoke-interface {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$BatteryCallback;->batteryPowerChanged(Z)V

    #@1c
    goto :goto_b

    #@1d
    :cond_1d
    const/4 v1, 0x0

    #@1e
    goto :goto_19

    #@1f
    .line 126
    nop

    #@20
    :pswitch_data_20
    .packed-switch 0x1
        :pswitch_c
        :pswitch_12
    .end packed-switch
.end method
