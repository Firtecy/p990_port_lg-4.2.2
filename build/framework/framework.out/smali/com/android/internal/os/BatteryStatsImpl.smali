.class public final Lcom/android/internal/os/BatteryStatsImpl;
.super Landroid/os/BatteryStats;
.source "BatteryStatsImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/os/BatteryStatsImpl$Uid;,
        Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;,
        Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;,
        Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;,
        Lcom/android/internal/os/BatteryStatsImpl$Timer;,
        Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;,
        Lcom/android/internal/os/BatteryStatsImpl$Counter;,
        Lcom/android/internal/os/BatteryStatsImpl$Unpluggable;,
        Lcom/android/internal/os/BatteryStatsImpl$MyHandler;,
        Lcom/android/internal/os/BatteryStatsImpl$BatteryCallback;
    }
.end annotation


# static fields
.field private static final BATCHED_WAKELOCK_NAME:Ljava/lang/String; = "*overflow*"

.field private static final BATTERY_PLUGGED_NONE:I = 0x0

.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/internal/os/BatteryStatsImpl;",
            ">;"
        }
    .end annotation
.end field

.field private static final DEBUG:Z = false

.field private static final DEBUG_HISTORY:Z = false

.field static final DELAY_UPDATE_WAKELOCKS:J = 0x1388L

.field private static final MAGIC:I = -0x458a8b8b

.field static final MAX_HISTORY_BUFFER:I = 0x20000

.field private static final MAX_HISTORY_ITEMS:I = 0x7d0

.field static final MAX_MAX_HISTORY_BUFFER:I = 0x24000

.field private static final MAX_MAX_HISTORY_ITEMS:I = 0xbb8

.field private static final MAX_WAKELOCKS_PER_UID:I = 0x1e

.field private static final MAX_WAKELOCKS_PER_UID_IN_SYSTEM:I = 0x32

.field static final MSG_REPORT_POWER_CHANGE:I = 0x2

.field static final MSG_UPDATE_WAKELOCKS:I = 0x1

.field private static final PROC_WAKELOCKS_FORMAT:[I = null

.field private static final TAG:Ljava/lang/String; = "BatteryStatsImpl"

.field private static final USE_OLD_HISTORY:Z = false

.field private static final VERSION:I = 0x3e

.field private static final WAKEUP_SOURCES_FORMAT:[I

.field private static sKernelWakelockUpdateVersion:I

.field private static sNumSpeedSteps:I


# instance fields
.field mAudioOn:Z

.field mAudioOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

.field mBatteryLastRealtime:J

.field mBatteryLastUptime:J

.field mBatteryRealtime:J

.field mBatteryUptime:J

.field mBluetoothOn:Z

.field mBluetoothOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

.field private mBluetoothPingCount:I

.field private mBluetoothPingStart:I

.field mBtHeadset:Landroid/bluetooth/BluetoothHeadset;

.field private mCallback:Lcom/android/internal/os/BatteryStatsImpl$BatteryCallback;

.field mChangedBufferStates:I

.field mChangedStates:I

.field mDischargeAmountScreenOff:I

.field mDischargeAmountScreenOffSinceCharge:I

.field mDischargeAmountScreenOn:I

.field mDischargeAmountScreenOnSinceCharge:I

.field mDischargeCurrentLevel:I

.field mDischargeScreenOffUnplugLevel:I

.field mDischargeScreenOnUnplugLevel:I

.field mDischargeStartLevel:I

.field mDischargeUnplugLevel:I

.field private final mFile:Lcom/android/internal/util/JournaledFile;

.field final mFullTimers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;",
            ">;"
        }
    .end annotation
.end field

.field final mFullWifiLockTimers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;",
            ">;"
        }
    .end annotation
.end field

.field mGlobalWifiRunning:Z

.field mGlobalWifiRunningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

.field mGpsNesting:I

.field private final mHandler:Lcom/android/internal/os/BatteryStatsImpl$MyHandler;

.field mHaveBatteryLevel:Z

.field mHighDischargeAmountSinceCharge:I

.field mHistory:Landroid/os/BatteryStats$HistoryItem;

.field mHistoryBaseTime:J

.field final mHistoryBuffer:Landroid/os/Parcel;

.field mHistoryBufferLastPos:I

.field mHistoryCache:Landroid/os/BatteryStats$HistoryItem;

.field final mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

.field mHistoryEnd:Landroid/os/BatteryStats$HistoryItem;

.field private mHistoryIterator:Landroid/os/BatteryStats$HistoryItem;

.field mHistoryLastEnd:Landroid/os/BatteryStats$HistoryItem;

.field final mHistoryLastLastWritten:Landroid/os/BatteryStats$HistoryItem;

.field final mHistoryLastWritten:Landroid/os/BatteryStats$HistoryItem;

.field mHistoryOverflow:Z

.field final mHistoryReadTmp:Landroid/os/BatteryStats$HistoryItem;

.field mInputEventCounter:Lcom/android/internal/os/BatteryStatsImpl$Counter;

.field private mIteratingHistory:Z

.field private final mKernelWakelockStats:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;",
            ">;"
        }
    .end annotation
.end field

.field mLastHistoryTime:J

.field final mLastPartialTimers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;",
            ">;"
        }
    .end annotation
.end field

.field mLastRealtime:J

.field mLastUptime:J

.field mLastWriteTime:J

.field mLowDischargeAmountSinceCharge:I

.field private mMobileDataRx:[J

.field private mMobileDataTx:[J

.field private mMobileIfaces:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mNetworkDetailCache:Landroid/net/NetworkStats;

.field private final mNetworkStatsFactory:Lcom/android/internal/net/NetworkStatsFactory;

.field private mNetworkSummaryCache:Landroid/net/NetworkStats;

.field mNumHistoryItems:I

.field mOnBattery:Z

.field mOnBatteryInternal:Z

.field final mPartialTimers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;",
            ">;"
        }
    .end annotation
.end field

.field mPendingWrite:Landroid/os/Parcel;

.field mPhoneDataConnectionType:I

.field final mPhoneDataConnectionsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

.field mPhoneOn:Z

.field mPhoneOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

.field private mPhoneServiceState:I

.field private mPhoneServiceStateRaw:I

.field mPhoneSignalScanningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

.field mPhoneSignalStrengthBin:I

.field mPhoneSignalStrengthBinRaw:I

.field final mPhoneSignalStrengthsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

.field private mPhoneSimStateRaw:I

.field private final mProcWakelockFileStats:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;",
            ">;"
        }
    .end annotation
.end field

.field private final mProcWakelocksData:[J

.field private final mProcWakelocksName:[Ljava/lang/String;

.field private mRadioDataStart:J

.field private mRadioDataUptime:J

.field private mReadOverflow:Z

.field mRealtime:J

.field mRealtimeStart:J

.field mRecordingHistory:Z

.field mScreenBrightnessBin:I

.field final mScreenBrightnessTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

.field mScreenOn:Z

.field mScreenOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

.field mSensorNesting:I

.field final mSensorTimers:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;",
            ">;>;"
        }
    .end annotation
.end field

.field mShuttingDown:Z

.field mStartCount:I

.field private mTotalDataRx:[J

.field private mTotalDataTx:[J

.field mTrackBatteryPastRealtime:J

.field mTrackBatteryPastUptime:J

.field mTrackBatteryRealtimeStart:J

.field mTrackBatteryUptimeStart:J

.field private mUidCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final mUidStats:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/android/internal/os/BatteryStatsImpl$Uid;",
            ">;"
        }
    .end annotation
.end field

.field final mUnpluggables:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/os/BatteryStatsImpl$Unpluggable;",
            ">;"
        }
    .end annotation
.end field

.field mUnpluggedBatteryRealtime:J

.field mUnpluggedBatteryUptime:J

.field mUptime:J

.field mUptimeStart:J

.field mVideoOn:Z

.field mVideoOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

.field mWakeLockNesting:I

.field mWifiFullLockNesting:I

.field mWifiMulticastNesting:I

.field final mWifiMulticastTimers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;",
            ">;"
        }
    .end annotation
.end field

.field mWifiOn:Z

.field mWifiOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

.field mWifiOnUid:I

.field final mWifiRunningTimers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;",
            ">;"
        }
    .end annotation
.end field

.field mWifiScanNesting:I

.field final mWifiScanTimers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;",
            ">;"
        }
    .end annotation
.end field

.field final mWindowTimers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;",
            ">;"
        }
    .end annotation
.end field

.field final mWriteLock:Ljava/util/concurrent/locks/ReentrantLock;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 312
    const/4 v0, 0x0

    #@1
    sput v0, Lcom/android/internal/os/BatteryStatsImpl;->sKernelWakelockUpdateVersion:I

    #@3
    .line 314
    const/4 v0, 0x6

    #@4
    new-array v0, v0, [I

    #@6
    fill-array-data v0, :array_1c

    #@9
    sput-object v0, Lcom/android/internal/os/BatteryStatsImpl;->PROC_WAKELOCKS_FORMAT:[I

    #@b
    .line 323
    const/4 v0, 0x7

    #@c
    new-array v0, v0, [I

    #@e
    fill-array-data v0, :array_2c

    #@11
    sput-object v0, Lcom/android/internal/os/BatteryStatsImpl;->WAKEUP_SOURCES_FORMAT:[I

    #@13
    .line 5692
    new-instance v0, Lcom/android/internal/os/BatteryStatsImpl$2;

    #@15
    invoke-direct {v0}, Lcom/android/internal/os/BatteryStatsImpl$2;-><init>()V

    #@18
    sput-object v0, Lcom/android/internal/os/BatteryStatsImpl;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1a
    return-void

    #@1b
    .line 314
    nop

    #@1c
    :array_1c
    .array-data 0x4
        0x9t 0x10t 0x0t 0x0t
        0x9t 0x20t 0x0t 0x0t
        0x9t 0x0t 0x0t 0x0t
        0x9t 0x0t 0x0t 0x0t
        0x9t 0x0t 0x0t 0x0t
        0x9t 0x20t 0x0t 0x0t
    .end array-data

    #@2c
    .line 323
    :array_2c
    .array-data 0x4
        0x9t 0x10t 0x0t 0x0t
        0x9t 0x21t 0x0t 0x0t
        0x9t 0x1t 0x0t 0x0t
        0x9t 0x1t 0x0t 0x0t
        0x9t 0x1t 0x0t 0x0t
        0x9t 0x1t 0x0t 0x0t
        0x9t 0x21t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x3

    #@1
    const/4 v5, 0x0

    #@2
    const/4 v4, 0x4

    #@3
    const/4 v3, 0x0

    #@4
    const/4 v2, -0x1

    #@5
    .line 353
    invoke-direct {p0}, Landroid/os/BatteryStats;-><init>()V

    #@8
    .line 148
    new-instance v0, Landroid/util/SparseArray;

    #@a
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@d
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@f
    .line 155
    new-instance v0, Ljava/util/ArrayList;

    #@11
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@14
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPartialTimers:Ljava/util/ArrayList;

    #@16
    .line 156
    new-instance v0, Ljava/util/ArrayList;

    #@18
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@1b
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mFullTimers:Ljava/util/ArrayList;

    #@1d
    .line 157
    new-instance v0, Ljava/util/ArrayList;

    #@1f
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@22
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWindowTimers:Ljava/util/ArrayList;

    #@24
    .line 158
    new-instance v0, Landroid/util/SparseArray;

    #@26
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@29
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mSensorTimers:Landroid/util/SparseArray;

    #@2b
    .line 160
    new-instance v0, Ljava/util/ArrayList;

    #@2d
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@30
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiRunningTimers:Ljava/util/ArrayList;

    #@32
    .line 161
    new-instance v0, Ljava/util/ArrayList;

    #@34
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@37
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mFullWifiLockTimers:Ljava/util/ArrayList;

    #@39
    .line 162
    new-instance v0, Ljava/util/ArrayList;

    #@3b
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@3e
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiMulticastTimers:Ljava/util/ArrayList;

    #@40
    .line 163
    new-instance v0, Ljava/util/ArrayList;

    #@42
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@45
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiScanTimers:Ljava/util/ArrayList;

    #@47
    .line 166
    new-instance v0, Ljava/util/ArrayList;

    #@49
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@4c
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mLastPartialTimers:Ljava/util/ArrayList;

    #@4e
    .line 170
    new-instance v0, Ljava/util/ArrayList;

    #@50
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@53
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@55
    .line 175
    iput-boolean v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHaveBatteryLevel:Z

    #@57
    .line 176
    const/4 v0, 0x1

    #@58
    iput-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mRecordingHistory:Z

    #@5a
    .line 181
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5d
    move-result-object v0

    #@5e
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@60
    .line 182
    new-instance v0, Landroid/os/BatteryStats$HistoryItem;

    #@62
    invoke-direct {v0}, Landroid/os/BatteryStats$HistoryItem;-><init>()V

    #@65
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryLastWritten:Landroid/os/BatteryStats$HistoryItem;

    #@67
    .line 183
    new-instance v0, Landroid/os/BatteryStats$HistoryItem;

    #@69
    invoke-direct {v0}, Landroid/os/BatteryStats$HistoryItem;-><init>()V

    #@6c
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryLastLastWritten:Landroid/os/BatteryStats$HistoryItem;

    #@6e
    .line 184
    new-instance v0, Landroid/os/BatteryStats$HistoryItem;

    #@70
    invoke-direct {v0}, Landroid/os/BatteryStats$HistoryItem;-><init>()V

    #@73
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryReadTmp:Landroid/os/BatteryStats$HistoryItem;

    #@75
    .line 185
    iput v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBufferLastPos:I

    #@77
    .line 186
    iput-boolean v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryOverflow:Z

    #@79
    .line 187
    const-wide/16 v0, 0x0

    #@7b
    iput-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mLastHistoryTime:J

    #@7d
    .line 189
    new-instance v0, Landroid/os/BatteryStats$HistoryItem;

    #@7f
    invoke-direct {v0}, Landroid/os/BatteryStats$HistoryItem;-><init>()V

    #@82
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@84
    .line 217
    iput v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenBrightnessBin:I

    #@86
    .line 218
    const/4 v0, 0x5

    #@87
    new-array v0, v0, [Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@89
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenBrightnessTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@8b
    .line 231
    iput v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalStrengthBin:I

    #@8d
    .line 232
    iput v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalStrengthBinRaw:I

    #@8f
    .line 233
    sget v0, Landroid/telephony/SignalStrength;->NUM_SIGNAL_STRENGTH_BINS:I

    #@91
    new-array v0, v0, [Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@93
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalStrengthsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@95
    .line 238
    iput v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneDataConnectionType:I

    #@97
    .line 239
    const/16 v0, 0x10

    #@99
    new-array v0, v0, [Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@9b
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneDataConnectionsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@9d
    .line 244
    iput v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiOnUid:I

    #@9f
    .line 284
    const-wide/16 v0, 0x0

    #@a1
    iput-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mLastWriteTime:J

    #@a3
    .line 287
    new-array v0, v4, [J

    #@a5
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mMobileDataTx:[J

    #@a7
    .line 288
    new-array v0, v4, [J

    #@a9
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mMobileDataRx:[J

    #@ab
    .line 289
    new-array v0, v4, [J

    #@ad
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTotalDataTx:[J

    #@af
    .line 290
    new-array v0, v4, [J

    #@b1
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTotalDataRx:[J

    #@b3
    .line 296
    iput v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBluetoothPingStart:I

    #@b5
    .line 298
    iput v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneServiceState:I

    #@b7
    .line 299
    iput v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneServiceStateRaw:I

    #@b9
    .line 300
    iput v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSimStateRaw:I

    #@bb
    .line 305
    new-instance v0, Ljava/util/HashMap;

    #@bd
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@c0
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mKernelWakelockStats:Ljava/util/HashMap;

    #@c2
    .line 335
    new-array v0, v6, [Ljava/lang/String;

    #@c4
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mProcWakelocksName:[Ljava/lang/String;

    #@c6
    .line 336
    new-array v0, v6, [J

    #@c8
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mProcWakelocksData:[J

    #@ca
    .line 342
    new-instance v0, Ljava/util/HashMap;

    #@cc
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@cf
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mProcWakelockFileStats:Ljava/util/Map;

    #@d1
    .line 345
    new-instance v0, Ljava/util/HashMap;

    #@d3
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@d6
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUidCache:Ljava/util/HashMap;

    #@d8
    .line 347
    new-instance v0, Lcom/android/internal/net/NetworkStatsFactory;

    #@da
    invoke-direct {v0}, Lcom/android/internal/net/NetworkStatsFactory;-><init>()V

    #@dd
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mNetworkStatsFactory:Lcom/android/internal/net/NetworkStatsFactory;

    #@df
    .line 350
    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    #@e2
    move-result-object v0

    #@e3
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mMobileIfaces:Ljava/util/HashSet;

    #@e5
    .line 1265
    iput v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mChangedBufferStates:I

    #@e7
    .line 1338
    iput v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mChangedStates:I

    #@e9
    .line 2183
    iput v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiFullLockNesting:I

    #@eb
    .line 2207
    iput v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiScanNesting:I

    #@ed
    .line 2231
    iput v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiMulticastNesting:I

    #@ef
    .line 4849
    iput-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPendingWrite:Landroid/os/Parcel;

    #@f1
    .line 4850
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    #@f3
    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    #@f6
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWriteLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@f8
    .line 354
    iput-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mFile:Lcom/android/internal/util/JournaledFile;

    #@fa
    .line 355
    iput-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHandler:Lcom/android/internal/os/BatteryStatsImpl$MyHandler;

    #@fc
    .line 356
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 9
    .parameter "p"

    #@0
    .prologue
    const/4 v6, 0x3

    #@1
    const/4 v5, 0x0

    #@2
    const/4 v4, 0x4

    #@3
    const/4 v3, 0x0

    #@4
    const/4 v2, -0x1

    #@5
    .line 4121
    invoke-direct {p0}, Landroid/os/BatteryStats;-><init>()V

    #@8
    .line 148
    new-instance v0, Landroid/util/SparseArray;

    #@a
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@d
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@f
    .line 155
    new-instance v0, Ljava/util/ArrayList;

    #@11
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@14
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPartialTimers:Ljava/util/ArrayList;

    #@16
    .line 156
    new-instance v0, Ljava/util/ArrayList;

    #@18
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@1b
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mFullTimers:Ljava/util/ArrayList;

    #@1d
    .line 157
    new-instance v0, Ljava/util/ArrayList;

    #@1f
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@22
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWindowTimers:Ljava/util/ArrayList;

    #@24
    .line 158
    new-instance v0, Landroid/util/SparseArray;

    #@26
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@29
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mSensorTimers:Landroid/util/SparseArray;

    #@2b
    .line 160
    new-instance v0, Ljava/util/ArrayList;

    #@2d
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@30
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiRunningTimers:Ljava/util/ArrayList;

    #@32
    .line 161
    new-instance v0, Ljava/util/ArrayList;

    #@34
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@37
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mFullWifiLockTimers:Ljava/util/ArrayList;

    #@39
    .line 162
    new-instance v0, Ljava/util/ArrayList;

    #@3b
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@3e
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiMulticastTimers:Ljava/util/ArrayList;

    #@40
    .line 163
    new-instance v0, Ljava/util/ArrayList;

    #@42
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@45
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiScanTimers:Ljava/util/ArrayList;

    #@47
    .line 166
    new-instance v0, Ljava/util/ArrayList;

    #@49
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@4c
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mLastPartialTimers:Ljava/util/ArrayList;

    #@4e
    .line 170
    new-instance v0, Ljava/util/ArrayList;

    #@50
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@53
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@55
    .line 175
    iput-boolean v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHaveBatteryLevel:Z

    #@57
    .line 176
    const/4 v0, 0x1

    #@58
    iput-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mRecordingHistory:Z

    #@5a
    .line 181
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5d
    move-result-object v0

    #@5e
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@60
    .line 182
    new-instance v0, Landroid/os/BatteryStats$HistoryItem;

    #@62
    invoke-direct {v0}, Landroid/os/BatteryStats$HistoryItem;-><init>()V

    #@65
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryLastWritten:Landroid/os/BatteryStats$HistoryItem;

    #@67
    .line 183
    new-instance v0, Landroid/os/BatteryStats$HistoryItem;

    #@69
    invoke-direct {v0}, Landroid/os/BatteryStats$HistoryItem;-><init>()V

    #@6c
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryLastLastWritten:Landroid/os/BatteryStats$HistoryItem;

    #@6e
    .line 184
    new-instance v0, Landroid/os/BatteryStats$HistoryItem;

    #@70
    invoke-direct {v0}, Landroid/os/BatteryStats$HistoryItem;-><init>()V

    #@73
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryReadTmp:Landroid/os/BatteryStats$HistoryItem;

    #@75
    .line 185
    iput v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBufferLastPos:I

    #@77
    .line 186
    iput-boolean v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryOverflow:Z

    #@79
    .line 187
    const-wide/16 v0, 0x0

    #@7b
    iput-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mLastHistoryTime:J

    #@7d
    .line 189
    new-instance v0, Landroid/os/BatteryStats$HistoryItem;

    #@7f
    invoke-direct {v0}, Landroid/os/BatteryStats$HistoryItem;-><init>()V

    #@82
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@84
    .line 217
    iput v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenBrightnessBin:I

    #@86
    .line 218
    const/4 v0, 0x5

    #@87
    new-array v0, v0, [Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@89
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenBrightnessTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@8b
    .line 231
    iput v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalStrengthBin:I

    #@8d
    .line 232
    iput v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalStrengthBinRaw:I

    #@8f
    .line 233
    sget v0, Landroid/telephony/SignalStrength;->NUM_SIGNAL_STRENGTH_BINS:I

    #@91
    new-array v0, v0, [Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@93
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalStrengthsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@95
    .line 238
    iput v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneDataConnectionType:I

    #@97
    .line 239
    const/16 v0, 0x10

    #@99
    new-array v0, v0, [Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@9b
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneDataConnectionsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@9d
    .line 244
    iput v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiOnUid:I

    #@9f
    .line 284
    const-wide/16 v0, 0x0

    #@a1
    iput-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mLastWriteTime:J

    #@a3
    .line 287
    new-array v0, v4, [J

    #@a5
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mMobileDataTx:[J

    #@a7
    .line 288
    new-array v0, v4, [J

    #@a9
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mMobileDataRx:[J

    #@ab
    .line 289
    new-array v0, v4, [J

    #@ad
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTotalDataTx:[J

    #@af
    .line 290
    new-array v0, v4, [J

    #@b1
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTotalDataRx:[J

    #@b3
    .line 296
    iput v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBluetoothPingStart:I

    #@b5
    .line 298
    iput v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneServiceState:I

    #@b7
    .line 299
    iput v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneServiceStateRaw:I

    #@b9
    .line 300
    iput v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSimStateRaw:I

    #@bb
    .line 305
    new-instance v0, Ljava/util/HashMap;

    #@bd
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@c0
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mKernelWakelockStats:Ljava/util/HashMap;

    #@c2
    .line 335
    new-array v0, v6, [Ljava/lang/String;

    #@c4
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mProcWakelocksName:[Ljava/lang/String;

    #@c6
    .line 336
    new-array v0, v6, [J

    #@c8
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mProcWakelocksData:[J

    #@ca
    .line 342
    new-instance v0, Ljava/util/HashMap;

    #@cc
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@cf
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mProcWakelockFileStats:Ljava/util/Map;

    #@d1
    .line 345
    new-instance v0, Ljava/util/HashMap;

    #@d3
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@d6
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUidCache:Ljava/util/HashMap;

    #@d8
    .line 347
    new-instance v0, Lcom/android/internal/net/NetworkStatsFactory;

    #@da
    invoke-direct {v0}, Lcom/android/internal/net/NetworkStatsFactory;-><init>()V

    #@dd
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mNetworkStatsFactory:Lcom/android/internal/net/NetworkStatsFactory;

    #@df
    .line 350
    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    #@e2
    move-result-object v0

    #@e3
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mMobileIfaces:Ljava/util/HashSet;

    #@e5
    .line 1265
    iput v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mChangedBufferStates:I

    #@e7
    .line 1338
    iput v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mChangedStates:I

    #@e9
    .line 2183
    iput v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiFullLockNesting:I

    #@eb
    .line 2207
    iput v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiScanNesting:I

    #@ed
    .line 2231
    iput v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiMulticastNesting:I

    #@ef
    .line 4849
    iput-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPendingWrite:Landroid/os/Parcel;

    #@f1
    .line 4850
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    #@f3
    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    #@f6
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWriteLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@f8
    .line 4122
    iput-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mFile:Lcom/android/internal/util/JournaledFile;

    #@fa
    .line 4123
    iput-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHandler:Lcom/android/internal/os/BatteryStatsImpl$MyHandler;

    #@fc
    .line 4124
    invoke-virtual {p0}, Lcom/android/internal/os/BatteryStatsImpl;->clearHistoryLocked()V

    #@ff
    .line 4125
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->readFromParcel(Landroid/os/Parcel;)V

    #@102
    .line 4126
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 13
    .parameter "filename"

    #@0
    .prologue
    const-wide/16 v9, 0x0

    #@2
    const/4 v2, 0x4

    #@3
    const/4 v8, 0x0

    #@4
    const/4 v7, -0x1

    #@5
    const/4 v6, 0x0

    #@6
    .line 4084
    invoke-direct {p0}, Landroid/os/BatteryStats;-><init>()V

    #@9
    .line 148
    new-instance v1, Landroid/util/SparseArray;

    #@b
    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    #@e
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@10
    .line 155
    new-instance v1, Ljava/util/ArrayList;

    #@12
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@15
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPartialTimers:Ljava/util/ArrayList;

    #@17
    .line 156
    new-instance v1, Ljava/util/ArrayList;

    #@19
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@1c
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mFullTimers:Ljava/util/ArrayList;

    #@1e
    .line 157
    new-instance v1, Ljava/util/ArrayList;

    #@20
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@23
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWindowTimers:Ljava/util/ArrayList;

    #@25
    .line 158
    new-instance v1, Landroid/util/SparseArray;

    #@27
    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    #@2a
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mSensorTimers:Landroid/util/SparseArray;

    #@2c
    .line 160
    new-instance v1, Ljava/util/ArrayList;

    #@2e
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@31
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiRunningTimers:Ljava/util/ArrayList;

    #@33
    .line 161
    new-instance v1, Ljava/util/ArrayList;

    #@35
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@38
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mFullWifiLockTimers:Ljava/util/ArrayList;

    #@3a
    .line 162
    new-instance v1, Ljava/util/ArrayList;

    #@3c
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@3f
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiMulticastTimers:Ljava/util/ArrayList;

    #@41
    .line 163
    new-instance v1, Ljava/util/ArrayList;

    #@43
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@46
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiScanTimers:Ljava/util/ArrayList;

    #@48
    .line 166
    new-instance v1, Ljava/util/ArrayList;

    #@4a
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@4d
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mLastPartialTimers:Ljava/util/ArrayList;

    #@4f
    .line 170
    new-instance v1, Ljava/util/ArrayList;

    #@51
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@54
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@56
    .line 175
    iput-boolean v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHaveBatteryLevel:Z

    #@58
    .line 176
    const/4 v1, 0x1

    #@59
    iput-boolean v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mRecordingHistory:Z

    #@5b
    .line 181
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5e
    move-result-object v1

    #@5f
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@61
    .line 182
    new-instance v1, Landroid/os/BatteryStats$HistoryItem;

    #@63
    invoke-direct {v1}, Landroid/os/BatteryStats$HistoryItem;-><init>()V

    #@66
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryLastWritten:Landroid/os/BatteryStats$HistoryItem;

    #@68
    .line 183
    new-instance v1, Landroid/os/BatteryStats$HistoryItem;

    #@6a
    invoke-direct {v1}, Landroid/os/BatteryStats$HistoryItem;-><init>()V

    #@6d
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryLastLastWritten:Landroid/os/BatteryStats$HistoryItem;

    #@6f
    .line 184
    new-instance v1, Landroid/os/BatteryStats$HistoryItem;

    #@71
    invoke-direct {v1}, Landroid/os/BatteryStats$HistoryItem;-><init>()V

    #@74
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryReadTmp:Landroid/os/BatteryStats$HistoryItem;

    #@76
    .line 185
    iput v7, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBufferLastPos:I

    #@78
    .line 186
    iput-boolean v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryOverflow:Z

    #@7a
    .line 187
    iput-wide v9, p0, Lcom/android/internal/os/BatteryStatsImpl;->mLastHistoryTime:J

    #@7c
    .line 189
    new-instance v1, Landroid/os/BatteryStats$HistoryItem;

    #@7e
    invoke-direct {v1}, Landroid/os/BatteryStats$HistoryItem;-><init>()V

    #@81
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@83
    .line 217
    iput v7, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenBrightnessBin:I

    #@85
    .line 218
    const/4 v1, 0x5

    #@86
    new-array v1, v1, [Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@88
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenBrightnessTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@8a
    .line 231
    iput v7, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalStrengthBin:I

    #@8c
    .line 232
    iput v7, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalStrengthBinRaw:I

    #@8e
    .line 233
    sget v1, Landroid/telephony/SignalStrength;->NUM_SIGNAL_STRENGTH_BINS:I

    #@90
    new-array v1, v1, [Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@92
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalStrengthsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@94
    .line 238
    iput v7, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneDataConnectionType:I

    #@96
    .line 239
    const/16 v1, 0x10

    #@98
    new-array v1, v1, [Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@9a
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneDataConnectionsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@9c
    .line 244
    iput v7, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiOnUid:I

    #@9e
    .line 284
    iput-wide v9, p0, Lcom/android/internal/os/BatteryStatsImpl;->mLastWriteTime:J

    #@a0
    .line 287
    new-array v1, v2, [J

    #@a2
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mMobileDataTx:[J

    #@a4
    .line 288
    new-array v1, v2, [J

    #@a6
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mMobileDataRx:[J

    #@a8
    .line 289
    new-array v1, v2, [J

    #@aa
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTotalDataTx:[J

    #@ac
    .line 290
    new-array v1, v2, [J

    #@ae
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTotalDataRx:[J

    #@b0
    .line 296
    iput v7, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBluetoothPingStart:I

    #@b2
    .line 298
    iput v7, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneServiceState:I

    #@b4
    .line 299
    iput v7, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneServiceStateRaw:I

    #@b6
    .line 300
    iput v7, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSimStateRaw:I

    #@b8
    .line 305
    new-instance v1, Ljava/util/HashMap;

    #@ba
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@bd
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mKernelWakelockStats:Ljava/util/HashMap;

    #@bf
    .line 335
    const/4 v1, 0x3

    #@c0
    new-array v1, v1, [Ljava/lang/String;

    #@c2
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mProcWakelocksName:[Ljava/lang/String;

    #@c4
    .line 336
    const/4 v1, 0x3

    #@c5
    new-array v1, v1, [J

    #@c7
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mProcWakelocksData:[J

    #@c9
    .line 342
    new-instance v1, Ljava/util/HashMap;

    #@cb
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@ce
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mProcWakelockFileStats:Ljava/util/Map;

    #@d0
    .line 345
    new-instance v1, Ljava/util/HashMap;

    #@d2
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@d5
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUidCache:Ljava/util/HashMap;

    #@d7
    .line 347
    new-instance v1, Lcom/android/internal/net/NetworkStatsFactory;

    #@d9
    invoke-direct {v1}, Lcom/android/internal/net/NetworkStatsFactory;-><init>()V

    #@dc
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mNetworkStatsFactory:Lcom/android/internal/net/NetworkStatsFactory;

    #@de
    .line 350
    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    #@e1
    move-result-object v1

    #@e2
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mMobileIfaces:Ljava/util/HashSet;

    #@e4
    .line 1265
    iput v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mChangedBufferStates:I

    #@e6
    .line 1338
    iput v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mChangedStates:I

    #@e8
    .line 2183
    iput v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiFullLockNesting:I

    #@ea
    .line 2207
    iput v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiScanNesting:I

    #@ec
    .line 2231
    iput v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiMulticastNesting:I

    #@ee
    .line 4849
    iput-object v6, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPendingWrite:Landroid/os/Parcel;

    #@f0
    .line 4850
    new-instance v1, Ljava/util/concurrent/locks/ReentrantLock;

    #@f2
    invoke-direct {v1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    #@f5
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWriteLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@f7
    .line 4085
    new-instance v1, Lcom/android/internal/util/JournaledFile;

    #@f9
    new-instance v2, Ljava/io/File;

    #@fb
    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@fe
    new-instance v3, Ljava/io/File;

    #@100
    new-instance v4, Ljava/lang/StringBuilder;

    #@102
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@105
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@108
    move-result-object v4

    #@109
    const-string v5, ".tmp"

    #@10b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10e
    move-result-object v4

    #@10f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@112
    move-result-object v4

    #@113
    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@116
    invoke-direct {v1, v2, v3}, Lcom/android/internal/util/JournaledFile;-><init>(Ljava/io/File;Ljava/io/File;)V

    #@119
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mFile:Lcom/android/internal/util/JournaledFile;

    #@11b
    .line 4086
    new-instance v1, Lcom/android/internal/os/BatteryStatsImpl$MyHandler;

    #@11d
    invoke-direct {v1, p0}, Lcom/android/internal/os/BatteryStatsImpl$MyHandler;-><init>(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@120
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHandler:Lcom/android/internal/os/BatteryStatsImpl$MyHandler;

    #@122
    .line 4087
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mStartCount:I

    #@124
    add-int/lit8 v1, v1, 0x1

    #@126
    iput v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mStartCount:I

    #@128
    .line 4088
    new-instance v1, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@12a
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@12c
    invoke-direct {v1, v6, v7, v6, v2}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    #@12f
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@131
    .line 4089
    const/4 v0, 0x0

    #@132
    .local v0, i:I
    :goto_132
    const/4 v1, 0x5

    #@133
    if-ge v0, v1, :cond_145

    #@135
    .line 4090
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenBrightnessTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@137
    new-instance v2, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@139
    rsub-int/lit8 v3, v0, -0x64

    #@13b
    iget-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@13d
    invoke-direct {v2, v6, v3, v6, v4}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    #@140
    aput-object v2, v1, v0

    #@142
    .line 4089
    add-int/lit8 v0, v0, 0x1

    #@144
    goto :goto_132

    #@145
    .line 4092
    :cond_145
    new-instance v1, Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@147
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@149
    invoke-direct {v1, v2}, Lcom/android/internal/os/BatteryStatsImpl$Counter;-><init>(Ljava/util/ArrayList;)V

    #@14c
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mInputEventCounter:Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@14e
    .line 4093
    new-instance v1, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@150
    const/4 v2, -0x2

    #@151
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@153
    invoke-direct {v1, v6, v2, v6, v3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    #@156
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@158
    .line 4094
    const/4 v0, 0x0

    #@159
    :goto_159
    sget v1, Landroid/telephony/SignalStrength;->NUM_SIGNAL_STRENGTH_BINS:I

    #@15b
    if-ge v0, v1, :cond_16d

    #@15d
    .line 4095
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalStrengthsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@15f
    new-instance v2, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@161
    rsub-int v3, v0, -0xc8

    #@163
    iget-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@165
    invoke-direct {v2, v6, v3, v6, v4}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    #@168
    aput-object v2, v1, v0

    #@16a
    .line 4094
    add-int/lit8 v0, v0, 0x1

    #@16c
    goto :goto_159

    #@16d
    .line 4097
    :cond_16d
    new-instance v1, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@16f
    const/16 v2, -0xc7

    #@171
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@173
    invoke-direct {v1, v6, v2, v6, v3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    #@176
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalScanningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@178
    .line 4098
    const/4 v0, 0x0

    #@179
    :goto_179
    const/16 v1, 0x10

    #@17b
    if-ge v0, v1, :cond_18d

    #@17d
    .line 4099
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneDataConnectionsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@17f
    new-instance v2, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@181
    rsub-int v3, v0, -0x12c

    #@183
    iget-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@185
    invoke-direct {v2, v6, v3, v6, v4}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    #@188
    aput-object v2, v1, v0

    #@18a
    .line 4098
    add-int/lit8 v0, v0, 0x1

    #@18c
    goto :goto_179

    #@18d
    .line 4101
    :cond_18d
    new-instance v1, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@18f
    const/4 v2, -0x3

    #@190
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@192
    invoke-direct {v1, v6, v2, v6, v3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    #@195
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@197
    .line 4102
    new-instance v1, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@199
    const/4 v2, -0x4

    #@19a
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@19c
    invoke-direct {v1, v6, v2, v6, v3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    #@19f
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mGlobalWifiRunningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@1a1
    .line 4103
    new-instance v1, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@1a3
    const/4 v2, -0x5

    #@1a4
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@1a6
    invoke-direct {v1, v6, v2, v6, v3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    #@1a9
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBluetoothOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@1ab
    .line 4104
    new-instance v1, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@1ad
    const/4 v2, -0x6

    #@1ae
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@1b0
    invoke-direct {v1, v6, v2, v6, v3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    #@1b3
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mAudioOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@1b5
    .line 4105
    new-instance v1, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@1b7
    const/4 v2, -0x7

    #@1b8
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@1ba
    invoke-direct {v1, v6, v2, v6, v3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    #@1bd
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mVideoOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@1bf
    .line 4106
    iput-boolean v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mOnBatteryInternal:Z

    #@1c1
    iput-boolean v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mOnBattery:Z

    #@1c3
    .line 4107
    invoke-virtual {p0}, Lcom/android/internal/os/BatteryStatsImpl;->initTimes()V

    #@1c6
    .line 4108
    iput-wide v9, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTrackBatteryPastUptime:J

    #@1c8
    .line 4109
    iput-wide v9, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTrackBatteryPastRealtime:J

    #@1ca
    .line 4110
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@1cd
    move-result-wide v1

    #@1ce
    const-wide/16 v3, 0x3e8

    #@1d0
    mul-long/2addr v1, v3

    #@1d1
    iput-wide v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTrackBatteryUptimeStart:J

    #@1d3
    iput-wide v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUptimeStart:J

    #@1d5
    .line 4111
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@1d8
    move-result-wide v1

    #@1d9
    const-wide/16 v3, 0x3e8

    #@1db
    mul-long/2addr v1, v3

    #@1dc
    iput-wide v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTrackBatteryRealtimeStart:J

    #@1de
    iput-wide v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mRealtimeStart:J

    #@1e0
    .line 4112
    iget-wide v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUptimeStart:J

    #@1e2
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/os/BatteryStatsImpl;->getBatteryUptimeLocked(J)J

    #@1e5
    move-result-wide v1

    #@1e6
    iput-wide v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggedBatteryUptime:J

    #@1e8
    .line 4113
    iget-wide v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mRealtimeStart:J

    #@1ea
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/os/BatteryStatsImpl;->getBatteryRealtimeLocked(J)J

    #@1ed
    move-result-wide v1

    #@1ee
    iput-wide v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggedBatteryRealtime:J

    #@1f0
    .line 4114
    iput v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeStartLevel:I

    #@1f2
    .line 4115
    iput v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeUnplugLevel:I

    #@1f4
    .line 4116
    iput v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeCurrentLevel:I

    #@1f6
    .line 4117
    invoke-virtual {p0}, Lcom/android/internal/os/BatteryStatsImpl;->initDischarge()V

    #@1f9
    .line 4118
    invoke-virtual {p0}, Lcom/android/internal/os/BatteryStatsImpl;->clearHistoryLocked()V

    #@1fc
    .line 4119
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/os/BatteryStatsImpl;)Lcom/android/internal/os/BatteryStatsImpl$BatteryCallback;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mCallback:Lcom/android/internal/os/BatteryStatsImpl$BatteryCallback;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/internal/os/BatteryStatsImpl;)Landroid/net/NetworkStats;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 78
    invoke-direct {p0}, Lcom/android/internal/os/BatteryStatsImpl;->getNetworkStatsDetailGroupedByUid()Landroid/net/NetworkStats;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private doDataPlug([JJ)V
    .registers 8
    .parameter "dataTransfer"
    .parameter "currentBytes"

    #@0
    .prologue
    const/4 v3, 0x3

    #@1
    .line 1193
    const/4 v0, 0x1

    #@2
    aget-wide v1, p1, v3

    #@4
    aput-wide v1, p1, v0

    #@6
    .line 1194
    const-wide/16 v0, -0x1

    #@8
    aput-wide v0, p1, v3

    #@a
    .line 1195
    return-void
.end method

.method private doDataUnplug([JJ)V
    .registers 5
    .parameter "dataTransfer"
    .parameter "currentBytes"

    #@0
    .prologue
    .line 1198
    const/4 v0, 0x3

    #@1
    aput-wide p2, p1, v0

    #@3
    .line 1199
    return-void
.end method

.method private fixPhoneServiceState(II)I
    .registers 5
    .parameter "state"
    .parameter "signalBin"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1863
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSimStateRaw:I

    #@3
    if-ne v0, v1, :cond_a

    #@5
    .line 1866
    if-ne p1, v1, :cond_a

    #@7
    if-lez p2, :cond_a

    #@9
    .line 1868
    const/4 p1, 0x0

    #@a
    .line 1872
    :cond_a
    return p1
.end method

.method private getCurrentBluetoothPingCount()I
    .registers 4

    #@0
    .prologue
    .line 1240
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBtHeadset:Landroid/bluetooth/BluetoothHeadset;

    #@2
    if-eqz v1, :cond_1e

    #@4
    .line 1241
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBtHeadset:Landroid/bluetooth/BluetoothHeadset;

    #@6
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothHeadset;->getConnectedDevices()Ljava/util/List;

    #@9
    move-result-object v0

    #@a
    .line 1242
    .local v0, deviceList:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@d
    move-result v1

    #@e
    if-lez v1, :cond_1e

    #@10
    .line 1243
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBtHeadset:Landroid/bluetooth/BluetoothHeadset;

    #@12
    const/4 v1, 0x0

    #@13
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@16
    move-result-object v1

    #@17
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@19
    invoke-virtual {v2, v1}, Landroid/bluetooth/BluetoothHeadset;->getBatteryUsageHint(Landroid/bluetooth/BluetoothDevice;)I

    #@1c
    move-result v1

    #@1d
    .line 1246
    .end local v0           #deviceList:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    :goto_1d
    return v1

    #@1e
    :cond_1e
    const/4 v1, -0x1

    #@1f
    goto :goto_1d
.end method

.method private getCurrentRadioDataUptime()J
    .registers 8

    #@0
    .prologue
    const-wide/16 v3, 0x0

    #@2
    .line 1207
    :try_start_2
    new-instance v0, Ljava/io/File;

    #@4
    const-string v5, "/sys/devices/virtual/net/rmnet0/awake_time_ms"

    #@6
    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@9
    .line 1208
    .local v0, awakeTimeFile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@c
    move-result v5

    #@d
    if-nez v5, :cond_10

    #@f
    .line 1218
    .end local v0           #awakeTimeFile:Ljava/io/File;
    :goto_f
    return-wide v3

    #@10
    .line 1209
    .restart local v0       #awakeTimeFile:Ljava/io/File;
    :cond_10
    new-instance v1, Ljava/io/BufferedReader;

    #@12
    new-instance v5, Ljava/io/FileReader;

    #@14
    invoke-direct {v5, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    #@17
    invoke-direct {v1, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    #@1a
    .line 1210
    .local v1, br:Ljava/io/BufferedReader;
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    .line 1211
    .local v2, line:Ljava/lang/String;
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    #@21
    .line 1212
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_24
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_24} :catch_2b
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_24} :catch_29

    #@24
    move-result-wide v3

    #@25
    const-wide/16 v5, 0x3e8

    #@27
    mul-long/2addr v3, v5

    #@28
    goto :goto_f

    #@29
    .line 1215
    .end local v0           #awakeTimeFile:Ljava/io/File;
    .end local v1           #br:Ljava/io/BufferedReader;
    .end local v2           #line:Ljava/lang/String;
    :catch_29
    move-exception v5

    #@2a
    goto :goto_f

    #@2b
    .line 1213
    :catch_2b
    move-exception v5

    #@2c
    goto :goto_f
.end method

.method private getNetworkStatsDetailGroupedByUid()Landroid/net/NetworkStats;
    .registers 6

    #@0
    .prologue
    .line 5771
    monitor-enter p0

    #@1
    .line 5772
    :try_start_1
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mNetworkDetailCache:Landroid/net/NetworkStats;

    #@3
    if-eqz v1, :cond_11

    #@5
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mNetworkDetailCache:Landroid/net/NetworkStats;

    #@7
    invoke-virtual {v1}, Landroid/net/NetworkStats;->getElapsedRealtimeAge()J

    #@a
    move-result-wide v1

    #@b
    const-wide/16 v3, 0x3e8

    #@d
    cmp-long v1, v1, v3

    #@f
    if-lez v1, :cond_3a

    #@11
    .line 5774
    :cond_11
    const/4 v1, 0x0

    #@12
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mNetworkDetailCache:Landroid/net/NetworkStats;

    #@14
    .line 5776
    const-string/jumbo v1, "net.qtaguid_enabled"

    #@17
    const/4 v2, 0x0

    #@18
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_1b
    .catchall {:try_start_1 .. :try_end_1b} :catchall_48

    #@1b
    move-result v1

    #@1c
    if-eqz v1, :cond_2a

    #@1e
    .line 5778
    :try_start_1e
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mNetworkStatsFactory:Lcom/android/internal/net/NetworkStatsFactory;

    #@20
    invoke-virtual {v1}, Lcom/android/internal/net/NetworkStatsFactory;->readNetworkStatsDetail()Landroid/net/NetworkStats;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v1}, Landroid/net/NetworkStats;->groupedByUid()Landroid/net/NetworkStats;

    #@27
    move-result-object v1

    #@28
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mNetworkDetailCache:Landroid/net/NetworkStats;
    :try_end_2a
    .catchall {:try_start_1e .. :try_end_2a} :catchall_48
    .catch Ljava/lang/IllegalStateException; {:try_start_1e .. :try_end_2a} :catch_3e

    #@2a
    .line 5785
    :cond_2a
    :goto_2a
    :try_start_2a
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mNetworkDetailCache:Landroid/net/NetworkStats;

    #@2c
    if-nez v1, :cond_3a

    #@2e
    .line 5786
    new-instance v1, Landroid/net/NetworkStats;

    #@30
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@33
    move-result-wide v2

    #@34
    const/4 v4, 0x0

    #@35
    invoke-direct {v1, v2, v3, v4}, Landroid/net/NetworkStats;-><init>(JI)V

    #@38
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mNetworkDetailCache:Landroid/net/NetworkStats;

    #@3a
    .line 5789
    :cond_3a
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mNetworkDetailCache:Landroid/net/NetworkStats;

    #@3c
    monitor-exit p0

    #@3d
    return-object v1

    #@3e
    .line 5780
    :catch_3e
    move-exception v0

    #@3f
    .line 5781
    .local v0, e:Ljava/lang/IllegalStateException;
    const-string v1, "BatteryStatsImpl"

    #@41
    const-string/jumbo v2, "problem reading network stats"

    #@44
    invoke-static {v1, v2, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@47
    goto :goto_2a

    #@48
    .line 5790
    .end local v0           #e:Ljava/lang/IllegalStateException;
    :catchall_48
    move-exception v1

    #@49
    monitor-exit p0
    :try_end_4a
    .catchall {:try_start_2a .. :try_end_4a} :catchall_48

    #@4a
    throw v1
.end method

.method private getNetworkStatsSummary()Landroid/net/NetworkStats;
    .registers 6

    #@0
    .prologue
    .line 5748
    monitor-enter p0

    #@1
    .line 5749
    :try_start_1
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mNetworkSummaryCache:Landroid/net/NetworkStats;

    #@3
    if-eqz v1, :cond_11

    #@5
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mNetworkSummaryCache:Landroid/net/NetworkStats;

    #@7
    invoke-virtual {v1}, Landroid/net/NetworkStats;->getElapsedRealtimeAge()J

    #@a
    move-result-wide v1

    #@b
    const-wide/16 v3, 0x3e8

    #@d
    cmp-long v1, v1, v3

    #@f
    if-lez v1, :cond_36

    #@11
    .line 5751
    :cond_11
    const/4 v1, 0x0

    #@12
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mNetworkSummaryCache:Landroid/net/NetworkStats;

    #@14
    .line 5753
    const-string/jumbo v1, "net.qtaguid_enabled"

    #@17
    const/4 v2, 0x0

    #@18
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_1b
    .catchall {:try_start_1 .. :try_end_1b} :catchall_44

    #@1b
    move-result v1

    #@1c
    if-eqz v1, :cond_26

    #@1e
    .line 5755
    :try_start_1e
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mNetworkStatsFactory:Lcom/android/internal/net/NetworkStatsFactory;

    #@20
    invoke-virtual {v1}, Lcom/android/internal/net/NetworkStatsFactory;->readNetworkStatsSummaryDev()Landroid/net/NetworkStats;

    #@23
    move-result-object v1

    #@24
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mNetworkSummaryCache:Landroid/net/NetworkStats;
    :try_end_26
    .catchall {:try_start_1e .. :try_end_26} :catchall_44
    .catch Ljava/lang/IllegalStateException; {:try_start_1e .. :try_end_26} :catch_3a

    #@26
    .line 5761
    :cond_26
    :goto_26
    :try_start_26
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mNetworkSummaryCache:Landroid/net/NetworkStats;

    #@28
    if-nez v1, :cond_36

    #@2a
    .line 5762
    new-instance v1, Landroid/net/NetworkStats;

    #@2c
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@2f
    move-result-wide v2

    #@30
    const/4 v4, 0x0

    #@31
    invoke-direct {v1, v2, v3, v4}, Landroid/net/NetworkStats;-><init>(JI)V

    #@34
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mNetworkSummaryCache:Landroid/net/NetworkStats;

    #@36
    .line 5765
    :cond_36
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mNetworkSummaryCache:Landroid/net/NetworkStats;

    #@38
    monitor-exit p0

    #@39
    return-object v1

    #@3a
    .line 5756
    :catch_3a
    move-exception v0

    #@3b
    .line 5757
    .local v0, e:Ljava/lang/IllegalStateException;
    const-string v1, "BatteryStatsImpl"

    #@3d
    const-string/jumbo v2, "problem reading network stats"

    #@40
    invoke-static {v1, v2, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@43
    goto :goto_26

    #@44
    .line 5766
    .end local v0           #e:Ljava/lang/IllegalStateException;
    :catchall_44
    move-exception v1

    #@45
    monitor-exit p0
    :try_end_46
    .catchall {:try_start_26 .. :try_end_46} :catchall_44

    #@46
    throw v1
.end method

.method private getTcpBytes(J[JI)J
    .registers 11
    .parameter "current"
    .parameter "dataBytes"
    .parameter "which"

    #@0
    .prologue
    const/4 v0, 0x2

    #@1
    const/4 v5, 0x3

    #@2
    const/4 v4, 0x1

    #@3
    .line 4596
    if-ne p4, v4, :cond_8

    #@5
    .line 4597
    aget-wide v0, p3, v4

    #@7
    .line 4608
    :goto_7
    return-wide v0

    #@8
    .line 4599
    :cond_8
    if-ne p4, v5, :cond_1a

    #@a
    .line 4600
    aget-wide v0, p3, v5

    #@c
    const-wide/16 v2, 0x0

    #@e
    cmp-long v0, v0, v2

    #@10
    if-gez v0, :cond_15

    #@12
    .line 4601
    aget-wide v0, p3, v4

    #@14
    goto :goto_7

    #@15
    .line 4603
    :cond_15
    aget-wide v0, p3, v5

    #@17
    sub-long v0, p1, v0

    #@19
    goto :goto_7

    #@1a
    .line 4605
    :cond_1a
    if-nez p4, :cond_25

    #@1c
    .line 4606
    aget-wide v0, p3, v0

    #@1e
    sub-long v0, p1, v0

    #@20
    const/4 v2, 0x0

    #@21
    aget-wide v2, p3, v2

    #@23
    add-long/2addr v0, v2

    #@24
    goto :goto_7

    #@25
    .line 4608
    :cond_25
    aget-wide v0, p3, v0

    #@27
    sub-long v0, p1, v0

    #@29
    goto :goto_7
.end method

.method private final parseProcWakelocks([BIZ)Ljava/util/Map;
    .registers 28
    .parameter "wlBuffer"
    .parameter "len"
    .parameter "wakeup_sources"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([BIZ)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1086
    const/16 v20, 0x0

    #@2
    .line 1090
    .local v20, numUpdatedWlNames:I
    const/4 v14, 0x0

    #@3
    .local v14, i:I
    :goto_3
    move/from16 v0, p2

    #@5
    if-ge v14, v0, :cond_14

    #@7
    aget-byte v2, p1, v14

    #@9
    const/16 v5, 0xa

    #@b
    if-eq v2, v5, :cond_14

    #@d
    aget-byte v2, p1, v14

    #@f
    if-eqz v2, :cond_14

    #@11
    add-int/lit8 v14, v14, 0x1

    #@13
    goto :goto_3

    #@14
    .line 1091
    :cond_14
    add-int/lit8 v4, v14, 0x1

    #@16
    .local v4, endIndex:I
    move v3, v4

    #@17
    .line 1093
    .local v3, startIndex:I
    monitor-enter p0

    #@18
    .line 1094
    :try_start_18
    move-object/from16 v0, p0

    #@1a
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mProcWakelockFileStats:Ljava/util/Map;

    #@1c
    move-object/from16 v18, v0

    #@1e
    .line 1096
    .local v18, m:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;>;"
    sget v2, Lcom/android/internal/os/BatteryStatsImpl;->sKernelWakelockUpdateVersion:I

    #@20
    add-int/lit8 v2, v2, 0x1

    #@22
    sput v2, Lcom/android/internal/os/BatteryStatsImpl;->sKernelWakelockUpdateVersion:I

    #@24
    .line 1097
    :goto_24
    move/from16 v0, p2

    #@26
    if-ge v4, v0, :cond_de

    #@28
    .line 1098
    move v4, v3

    #@29
    .line 1099
    :goto_29
    move/from16 v0, p2

    #@2b
    if-ge v4, v0, :cond_3a

    #@2d
    aget-byte v2, p1, v4

    #@2f
    const/16 v5, 0xa

    #@31
    if-eq v2, v5, :cond_3a

    #@33
    aget-byte v2, p1, v4

    #@35
    if-eqz v2, :cond_3a

    #@37
    .line 1100
    add-int/lit8 v4, v4, 0x1

    #@39
    goto :goto_29

    #@3a
    .line 1101
    :cond_3a
    add-int/lit8 v4, v4, 0x1

    #@3c
    .line 1104
    add-int/lit8 v2, p2, -0x1

    #@3e
    if-lt v4, v2, :cond_42

    #@40
    .line 1105
    monitor-exit p0

    #@41
    .line 1162
    :goto_41
    return-object v18

    #@42
    .line 1108
    :cond_42
    move-object/from16 v0, p0

    #@44
    iget-object v6, v0, Lcom/android/internal/os/BatteryStatsImpl;->mProcWakelocksName:[Ljava/lang/String;

    #@46
    .line 1109
    .local v6, nameStringArray:[Ljava/lang/String;
    move-object/from16 v0, p0

    #@48
    iget-object v7, v0, Lcom/android/internal/os/BatteryStatsImpl;->mProcWakelocksData:[J

    #@4a
    .line 1113
    .local v7, wlData:[J
    move/from16 v16, v3

    #@4c
    .local v16, j:I
    :goto_4c
    move/from16 v0, v16

    #@4e
    if-ge v0, v4, :cond_5d

    #@50
    .line 1114
    aget-byte v2, p1, v16

    #@52
    and-int/lit16 v2, v2, 0x80

    #@54
    if-eqz v2, :cond_5a

    #@56
    const/16 v2, 0x3f

    #@58
    aput-byte v2, p1, v16

    #@5a
    .line 1113
    :cond_5a
    add-int/lit8 v16, v16, 0x1

    #@5c
    goto :goto_4c

    #@5d
    .line 1116
    :cond_5d
    if-eqz p3, :cond_9a

    #@5f
    sget-object v5, Lcom/android/internal/os/BatteryStatsImpl;->WAKEUP_SOURCES_FORMAT:[I

    #@61
    :goto_61
    const/4 v8, 0x0

    #@62
    move-object/from16 v2, p1

    #@64
    invoke-static/range {v2 .. v8}, Landroid/os/Process;->parseProcLine([BII[I[Ljava/lang/String;[J[F)Z

    #@67
    move-result v21

    #@68
    .line 1121
    .local v21, parsed:Z
    const/4 v2, 0x0

    #@69
    aget-object v19, v6, v2

    #@6b
    .line 1122
    .local v19, name:Ljava/lang/String;
    const/4 v2, 0x1

    #@6c
    aget-wide v8, v7, v2

    #@6e
    long-to-int v10, v8

    #@6f
    .line 1124
    .local v10, count:I
    if-eqz p3, :cond_9d

    #@71
    .line 1126
    const/4 v2, 0x2

    #@72
    aget-wide v8, v7, v2

    #@74
    const-wide/16 v22, 0x3e8

    #@76
    mul-long v11, v8, v22

    #@78
    .line 1132
    .local v11, totalTime:J
    :goto_78
    if-eqz v21, :cond_98

    #@7a
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    #@7d
    move-result v2

    #@7e
    if-lez v2, :cond_98

    #@80
    .line 1133
    invoke-interface/range {v18 .. v19}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    #@83
    move-result v2

    #@84
    if-nez v2, :cond_a9

    #@86
    .line 1134
    new-instance v8, Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;

    #@88
    sget v13, Lcom/android/internal/os/BatteryStatsImpl;->sKernelWakelockUpdateVersion:I

    #@8a
    move-object/from16 v9, p0

    #@8c
    invoke-direct/range {v8 .. v13}, Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;-><init>(Lcom/android/internal/os/BatteryStatsImpl;IJI)V

    #@8f
    move-object/from16 v0, v18

    #@91
    move-object/from16 v1, v19

    #@93
    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@96
    .line 1136
    add-int/lit8 v20, v20, 0x1

    #@98
    .line 1150
    :cond_98
    :goto_98
    move v3, v4

    #@99
    .line 1151
    goto :goto_24

    #@9a
    .line 1116
    .end local v10           #count:I
    .end local v11           #totalTime:J
    .end local v19           #name:Ljava/lang/String;
    .end local v21           #parsed:Z
    :cond_9a
    sget-object v5, Lcom/android/internal/os/BatteryStatsImpl;->PROC_WAKELOCKS_FORMAT:[I

    #@9c
    goto :goto_61

    #@9d
    .line 1129
    .restart local v10       #count:I
    .restart local v19       #name:Ljava/lang/String;
    .restart local v21       #parsed:Z
    :cond_9d
    const/4 v2, 0x2

    #@9e
    aget-wide v8, v7, v2

    #@a0
    const-wide/16 v22, 0x1f4

    #@a2
    add-long v8, v8, v22

    #@a4
    const-wide/16 v22, 0x3e8

    #@a6
    div-long v11, v8, v22

    #@a8
    .restart local v11       #totalTime:J
    goto :goto_78

    #@a9
    .line 1138
    :cond_a9
    invoke-interface/range {v18 .. v19}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@ac
    move-result-object v17

    #@ad
    check-cast v17, Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;

    #@af
    .line 1139
    .local v17, kwlStats:Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;
    move-object/from16 v0, v17

    #@b1
    iget v2, v0, Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;->mVersion:I

    #@b3
    sget v5, Lcom/android/internal/os/BatteryStatsImpl;->sKernelWakelockUpdateVersion:I

    #@b5
    if-ne v2, v5, :cond_cd

    #@b7
    .line 1140
    move-object/from16 v0, v17

    #@b9
    iget v2, v0, Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;->mCount:I

    #@bb
    add-int/2addr v2, v10

    #@bc
    move-object/from16 v0, v17

    #@be
    iput v2, v0, Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;->mCount:I

    #@c0
    .line 1141
    move-object/from16 v0, v17

    #@c2
    iget-wide v8, v0, Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;->mTotalTime:J

    #@c4
    add-long/2addr v8, v11

    #@c5
    move-object/from16 v0, v17

    #@c7
    iput-wide v8, v0, Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;->mTotalTime:J

    #@c9
    goto :goto_98

    #@ca
    .line 1163
    .end local v6           #nameStringArray:[Ljava/lang/String;
    .end local v7           #wlData:[J
    .end local v10           #count:I
    .end local v11           #totalTime:J
    .end local v16           #j:I
    .end local v17           #kwlStats:Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;
    .end local v18           #m:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;>;"
    .end local v19           #name:Ljava/lang/String;
    .end local v21           #parsed:Z
    :catchall_ca
    move-exception v2

    #@cb
    monitor-exit p0
    :try_end_cc
    .catchall {:try_start_18 .. :try_end_cc} :catchall_ca

    #@cc
    throw v2

    #@cd
    .line 1143
    .restart local v6       #nameStringArray:[Ljava/lang/String;
    .restart local v7       #wlData:[J
    .restart local v10       #count:I
    .restart local v11       #totalTime:J
    .restart local v16       #j:I
    .restart local v17       #kwlStats:Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;
    .restart local v18       #m:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;>;"
    .restart local v19       #name:Ljava/lang/String;
    .restart local v21       #parsed:Z
    :cond_cd
    :try_start_cd
    move-object/from16 v0, v17

    #@cf
    iput v10, v0, Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;->mCount:I

    #@d1
    .line 1144
    move-object/from16 v0, v17

    #@d3
    iput-wide v11, v0, Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;->mTotalTime:J

    #@d5
    .line 1145
    sget v2, Lcom/android/internal/os/BatteryStatsImpl;->sKernelWakelockUpdateVersion:I

    #@d7
    move-object/from16 v0, v17

    #@d9
    iput v2, v0, Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;->mVersion:I

    #@db
    .line 1146
    add-int/lit8 v20, v20, 0x1

    #@dd
    goto :goto_98

    #@de
    .line 1153
    .end local v6           #nameStringArray:[Ljava/lang/String;
    .end local v7           #wlData:[J
    .end local v10           #count:I
    .end local v11           #totalTime:J
    .end local v16           #j:I
    .end local v17           #kwlStats:Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;
    .end local v19           #name:Ljava/lang/String;
    .end local v21           #parsed:Z
    :cond_de
    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->size()I

    #@e1
    move-result v2

    #@e2
    move/from16 v0, v20

    #@e4
    if-eq v2, v0, :cond_104

    #@e6
    .line 1155
    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->values()Ljava/util/Collection;

    #@e9
    move-result-object v2

    #@ea
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@ed
    move-result-object v15

    #@ee
    .line 1156
    .local v15, itr:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;>;"
    :cond_ee
    :goto_ee
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    #@f1
    move-result v2

    #@f2
    if-eqz v2, :cond_104

    #@f4
    .line 1157
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f7
    move-result-object v2

    #@f8
    check-cast v2, Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;

    #@fa
    iget v2, v2, Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;->mVersion:I

    #@fc
    sget v5, Lcom/android/internal/os/BatteryStatsImpl;->sKernelWakelockUpdateVersion:I

    #@fe
    if-eq v2, v5, :cond_ee

    #@100
    .line 1158
    invoke-interface {v15}, Ljava/util/Iterator;->remove()V

    #@103
    goto :goto_ee

    #@104
    .line 1162
    .end local v15           #itr:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;>;"
    :cond_104
    monitor-exit p0
    :try_end_105
    .catchall {:try_start_cd .. :try_end_105} :catchall_ca

    #@105
    goto/16 :goto_41
.end method

.method static readFully(Ljava/io/FileInputStream;)[B
    .registers 8
    .parameter "stream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 4922
    const/4 v4, 0x0

    #@2
    .line 4923
    .local v4, pos:I
    invoke-virtual {p0}, Ljava/io/FileInputStream;->available()I

    #@5
    move-result v1

    #@6
    .line 4924
    .local v1, avail:I
    new-array v2, v1, [B

    #@8
    .line 4926
    .local v2, data:[B
    :cond_8
    :goto_8
    array-length v5, v2

    #@9
    sub-int/2addr v5, v4

    #@a
    invoke-virtual {p0, v2, v4, v5}, Ljava/io/FileInputStream;->read([BII)I

    #@d
    move-result v0

    #@e
    .line 4929
    .local v0, amt:I
    if-gtz v0, :cond_11

    #@10
    .line 4932
    return-object v2

    #@11
    .line 4934
    :cond_11
    add-int/2addr v4, v0

    #@12
    .line 4935
    invoke-virtual {p0}, Ljava/io/FileInputStream;->available()I

    #@15
    move-result v1

    #@16
    .line 4936
    array-length v5, v2

    #@17
    sub-int/2addr v5, v4

    #@18
    if-le v1, v5, :cond_8

    #@1a
    .line 4937
    add-int v5, v4, v1

    #@1c
    new-array v3, v5, [B

    #@1e
    .line 4938
    .local v3, newData:[B
    invoke-static {v2, v6, v3, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@21
    .line 4939
    move-object v2, v3

    #@22
    goto :goto_8
.end method

.method private final readKernelWakelockStats()Ljava/util/Map;
    .registers 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 1044
    const/16 v8, 0x2000

    #@3
    new-array v0, v8, [B

    #@5
    .line 1046
    .local v0, buffer:[B
    const/4 v6, 0x0

    #@6
    .line 1050
    .local v6, wakeup_sources:Z
    :try_start_6
    new-instance v4, Ljava/io/FileInputStream;

    #@8
    const-string v8, "/proc/wakelocks"

    #@a
    invoke-direct {v4, v8}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_d} :catch_23
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_d} :catch_2f

    #@d
    .line 1060
    .local v4, is:Ljava/io/FileInputStream;
    :goto_d
    :try_start_d
    invoke-virtual {v4, v0}, Ljava/io/FileInputStream;->read([B)I

    #@10
    move-result v5

    #@11
    .line 1061
    .local v5, len:I
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_14} :catch_2f

    #@14
    .line 1066
    if-lez v5, :cond_1e

    #@16
    .line 1068
    const/4 v3, 0x0

    #@17
    .local v3, i:I
    :goto_17
    if-ge v3, v5, :cond_1e

    #@19
    .line 1069
    aget-byte v7, v0, v3

    #@1b
    if-nez v7, :cond_31

    #@1d
    .line 1070
    move v5, v3

    #@1e
    .line 1076
    .end local v3           #i:I
    :cond_1e
    invoke-direct {p0, v0, v5, v6}, Lcom/android/internal/os/BatteryStatsImpl;->parseProcWakelocks([BIZ)Ljava/util/Map;

    #@21
    move-result-object v7

    #@22
    .end local v4           #is:Ljava/io/FileInputStream;
    .end local v5           #len:I
    :goto_22
    return-object v7

    #@23
    .line 1051
    :catch_23
    move-exception v1

    #@24
    .line 1053
    .local v1, e:Ljava/io/FileNotFoundException;
    :try_start_24
    new-instance v4, Ljava/io/FileInputStream;

    #@26
    const-string v8, "/d/wakeup_sources"

    #@28
    invoke-direct {v4, v8}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_2b
    .catch Ljava/io/FileNotFoundException; {:try_start_24 .. :try_end_2b} :catch_2d
    .catch Ljava/io/IOException; {:try_start_24 .. :try_end_2b} :catch_2f

    #@2b
    .line 1054
    .restart local v4       #is:Ljava/io/FileInputStream;
    const/4 v6, 0x1

    #@2c
    goto :goto_d

    #@2d
    .line 1055
    .end local v4           #is:Ljava/io/FileInputStream;
    :catch_2d
    move-exception v2

    #@2e
    .line 1056
    .local v2, e2:Ljava/io/FileNotFoundException;
    goto :goto_22

    #@2f
    .line 1062
    .end local v1           #e:Ljava/io/FileNotFoundException;
    .end local v2           #e2:Ljava/io/FileNotFoundException;
    :catch_2f
    move-exception v1

    #@30
    .line 1063
    .local v1, e:Ljava/io/IOException;
    goto :goto_22

    #@31
    .line 1068
    .end local v1           #e:Ljava/io/IOException;
    .restart local v3       #i:I
    .restart local v4       #is:Ljava/io/FileInputStream;
    .restart local v5       #len:I
    :cond_31
    add-int/lit8 v3, v3, 0x1

    #@33
    goto :goto_17
.end method

.method private readSummaryFromParcel(Landroid/os/Parcel;)V
    .registers 31
    .parameter "in"

    #@0
    .prologue
    .line 5077
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v24

    #@4
    .line 5078
    .local v24, version:I
    const/16 v26, 0x3e

    #@6
    move/from16 v0, v24

    #@8
    move/from16 v1, v26

    #@a
    if-eq v0, v1, :cond_3c

    #@c
    .line 5079
    const-string v26, "BatteryStats"

    #@e
    new-instance v27, Ljava/lang/StringBuilder;

    #@10
    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string/jumbo v28, "readFromParcel: version got "

    #@16
    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v27

    #@1a
    move-object/from16 v0, v27

    #@1c
    move/from16 v1, v24

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v27

    #@22
    const-string v28, ", expected "

    #@24
    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v27

    #@28
    const/16 v28, 0x3e

    #@2a
    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v27

    #@2e
    const-string v28, "; erasing old stats"

    #@30
    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v27

    #@34
    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v27

    #@38
    invoke-static/range {v26 .. v27}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 5265
    :cond_3b
    :goto_3b
    return-void

    #@3c
    .line 5084
    :cond_3c
    const/16 v26, 0x1

    #@3e
    move-object/from16 v0, p0

    #@40
    move-object/from16 v1, p1

    #@42
    move/from16 v2, v26

    #@44
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/os/BatteryStatsImpl;->readHistory(Landroid/os/Parcel;Z)V

    #@47
    .line 5086
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@4a
    move-result v26

    #@4b
    move/from16 v0, v26

    #@4d
    move-object/from16 v1, p0

    #@4f
    iput v0, v1, Lcom/android/internal/os/BatteryStatsImpl;->mStartCount:I

    #@51
    .line 5087
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    #@54
    move-result-wide v26

    #@55
    move-wide/from16 v0, v26

    #@57
    move-object/from16 v2, p0

    #@59
    iput-wide v0, v2, Lcom/android/internal/os/BatteryStatsImpl;->mBatteryUptime:J

    #@5b
    .line 5088
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    #@5e
    move-result-wide v26

    #@5f
    move-wide/from16 v0, v26

    #@61
    move-object/from16 v2, p0

    #@63
    iput-wide v0, v2, Lcom/android/internal/os/BatteryStatsImpl;->mBatteryRealtime:J

    #@65
    .line 5089
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    #@68
    move-result-wide v26

    #@69
    move-wide/from16 v0, v26

    #@6b
    move-object/from16 v2, p0

    #@6d
    iput-wide v0, v2, Lcom/android/internal/os/BatteryStatsImpl;->mUptime:J

    #@6f
    .line 5090
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    #@72
    move-result-wide v26

    #@73
    move-wide/from16 v0, v26

    #@75
    move-object/from16 v2, p0

    #@77
    iput-wide v0, v2, Lcom/android/internal/os/BatteryStatsImpl;->mRealtime:J

    #@79
    .line 5091
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@7c
    move-result v26

    #@7d
    move/from16 v0, v26

    #@7f
    move-object/from16 v1, p0

    #@81
    iput v0, v1, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeUnplugLevel:I

    #@83
    .line 5092
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@86
    move-result v26

    #@87
    move/from16 v0, v26

    #@89
    move-object/from16 v1, p0

    #@8b
    iput v0, v1, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeCurrentLevel:I

    #@8d
    .line 5093
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@90
    move-result v26

    #@91
    move/from16 v0, v26

    #@93
    move-object/from16 v1, p0

    #@95
    iput v0, v1, Lcom/android/internal/os/BatteryStatsImpl;->mLowDischargeAmountSinceCharge:I

    #@97
    .line 5094
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@9a
    move-result v26

    #@9b
    move/from16 v0, v26

    #@9d
    move-object/from16 v1, p0

    #@9f
    iput v0, v1, Lcom/android/internal/os/BatteryStatsImpl;->mHighDischargeAmountSinceCharge:I

    #@a1
    .line 5095
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@a4
    move-result v26

    #@a5
    move/from16 v0, v26

    #@a7
    move-object/from16 v1, p0

    #@a9
    iput v0, v1, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeAmountScreenOnSinceCharge:I

    #@ab
    .line 5096
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@ae
    move-result v26

    #@af
    move/from16 v0, v26

    #@b1
    move-object/from16 v1, p0

    #@b3
    iput v0, v1, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeAmountScreenOffSinceCharge:I

    #@b5
    .line 5098
    move-object/from16 v0, p0

    #@b7
    iget v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mStartCount:I

    #@b9
    move/from16 v26, v0

    #@bb
    add-int/lit8 v26, v26, 0x1

    #@bd
    move/from16 v0, v26

    #@bf
    move-object/from16 v1, p0

    #@c1
    iput v0, v1, Lcom/android/internal/os/BatteryStatsImpl;->mStartCount:I

    #@c3
    .line 5100
    const/16 v26, 0x0

    #@c5
    move/from16 v0, v26

    #@c7
    move-object/from16 v1, p0

    #@c9
    iput-boolean v0, v1, Lcom/android/internal/os/BatteryStatsImpl;->mScreenOn:Z

    #@cb
    .line 5101
    move-object/from16 v0, p0

    #@cd
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@cf
    move-object/from16 v26, v0

    #@d1
    move-object/from16 v0, v26

    #@d3
    move-object/from16 v1, p1

    #@d5
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->readSummaryFromParcelLocked(Landroid/os/Parcel;)V

    #@d8
    .line 5102
    const/4 v9, 0x0

    #@d9
    .local v9, i:I
    :goto_d9
    const/16 v26, 0x5

    #@db
    move/from16 v0, v26

    #@dd
    if-ge v9, v0, :cond_f1

    #@df
    .line 5103
    move-object/from16 v0, p0

    #@e1
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenBrightnessTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@e3
    move-object/from16 v26, v0

    #@e5
    aget-object v26, v26, v9

    #@e7
    move-object/from16 v0, v26

    #@e9
    move-object/from16 v1, p1

    #@eb
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->readSummaryFromParcelLocked(Landroid/os/Parcel;)V

    #@ee
    .line 5102
    add-int/lit8 v9, v9, 0x1

    #@f0
    goto :goto_d9

    #@f1
    .line 5105
    :cond_f1
    move-object/from16 v0, p0

    #@f3
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mInputEventCounter:Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@f5
    move-object/from16 v26, v0

    #@f7
    move-object/from16 v0, v26

    #@f9
    move-object/from16 v1, p1

    #@fb
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$Counter;->readSummaryFromParcelLocked(Landroid/os/Parcel;)V

    #@fe
    .line 5106
    const/16 v26, 0x0

    #@100
    move/from16 v0, v26

    #@102
    move-object/from16 v1, p0

    #@104
    iput-boolean v0, v1, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneOn:Z

    #@106
    .line 5107
    move-object/from16 v0, p0

    #@108
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@10a
    move-object/from16 v26, v0

    #@10c
    move-object/from16 v0, v26

    #@10e
    move-object/from16 v1, p1

    #@110
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->readSummaryFromParcelLocked(Landroid/os/Parcel;)V

    #@113
    .line 5108
    const/4 v9, 0x0

    #@114
    :goto_114
    sget v26, Landroid/telephony/SignalStrength;->NUM_SIGNAL_STRENGTH_BINS:I

    #@116
    move/from16 v0, v26

    #@118
    if-ge v9, v0, :cond_12c

    #@11a
    .line 5109
    move-object/from16 v0, p0

    #@11c
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalStrengthsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@11e
    move-object/from16 v26, v0

    #@120
    aget-object v26, v26, v9

    #@122
    move-object/from16 v0, v26

    #@124
    move-object/from16 v1, p1

    #@126
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->readSummaryFromParcelLocked(Landroid/os/Parcel;)V

    #@129
    .line 5108
    add-int/lit8 v9, v9, 0x1

    #@12b
    goto :goto_114

    #@12c
    .line 5111
    :cond_12c
    move-object/from16 v0, p0

    #@12e
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalScanningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@130
    move-object/from16 v26, v0

    #@132
    move-object/from16 v0, v26

    #@134
    move-object/from16 v1, p1

    #@136
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->readSummaryFromParcelLocked(Landroid/os/Parcel;)V

    #@139
    .line 5112
    const/4 v9, 0x0

    #@13a
    :goto_13a
    const/16 v26, 0x10

    #@13c
    move/from16 v0, v26

    #@13e
    if-ge v9, v0, :cond_152

    #@140
    .line 5113
    move-object/from16 v0, p0

    #@142
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneDataConnectionsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@144
    move-object/from16 v26, v0

    #@146
    aget-object v26, v26, v9

    #@148
    move-object/from16 v0, v26

    #@14a
    move-object/from16 v1, p1

    #@14c
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->readSummaryFromParcelLocked(Landroid/os/Parcel;)V

    #@14f
    .line 5112
    add-int/lit8 v9, v9, 0x1

    #@151
    goto :goto_13a

    #@152
    .line 5115
    :cond_152
    const/16 v26, 0x0

    #@154
    move/from16 v0, v26

    #@156
    move-object/from16 v1, p0

    #@158
    iput-boolean v0, v1, Lcom/android/internal/os/BatteryStatsImpl;->mWifiOn:Z

    #@15a
    .line 5116
    move-object/from16 v0, p0

    #@15c
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@15e
    move-object/from16 v26, v0

    #@160
    move-object/from16 v0, v26

    #@162
    move-object/from16 v1, p1

    #@164
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->readSummaryFromParcelLocked(Landroid/os/Parcel;)V

    #@167
    .line 5117
    const/16 v26, 0x0

    #@169
    move/from16 v0, v26

    #@16b
    move-object/from16 v1, p0

    #@16d
    iput-boolean v0, v1, Lcom/android/internal/os/BatteryStatsImpl;->mGlobalWifiRunning:Z

    #@16f
    .line 5118
    move-object/from16 v0, p0

    #@171
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mGlobalWifiRunningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@173
    move-object/from16 v26, v0

    #@175
    move-object/from16 v0, v26

    #@177
    move-object/from16 v1, p1

    #@179
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->readSummaryFromParcelLocked(Landroid/os/Parcel;)V

    #@17c
    .line 5119
    const/16 v26, 0x0

    #@17e
    move/from16 v0, v26

    #@180
    move-object/from16 v1, p0

    #@182
    iput-boolean v0, v1, Lcom/android/internal/os/BatteryStatsImpl;->mBluetoothOn:Z

    #@184
    .line 5120
    move-object/from16 v0, p0

    #@186
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mBluetoothOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@188
    move-object/from16 v26, v0

    #@18a
    move-object/from16 v0, v26

    #@18c
    move-object/from16 v1, p1

    #@18e
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->readSummaryFromParcelLocked(Landroid/os/Parcel;)V

    #@191
    .line 5122
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@194
    move-result v3

    #@195
    .line 5123
    .local v3, NKW:I
    const/16 v26, 0x2710

    #@197
    move/from16 v0, v26

    #@199
    if-le v3, v0, :cond_1b7

    #@19b
    .line 5124
    const-string v26, "BatteryStatsImpl"

    #@19d
    new-instance v27, Ljava/lang/StringBuilder;

    #@19f
    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    #@1a2
    const-string v28, "File corrupt: too many kernel wake locks "

    #@1a4
    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a7
    move-result-object v27

    #@1a8
    move-object/from16 v0, v27

    #@1aa
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1ad
    move-result-object v27

    #@1ae
    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b1
    move-result-object v27

    #@1b2
    invoke-static/range {v26 .. v27}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1b5
    goto/16 :goto_3b

    #@1b7
    .line 5127
    :cond_1b7
    const/4 v10, 0x0

    #@1b8
    .local v10, ikw:I
    :goto_1b8
    if-ge v10, v3, :cond_1d4

    #@1ba
    .line 5128
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@1bd
    move-result v26

    #@1be
    if-eqz v26, :cond_1d1

    #@1c0
    .line 5129
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1c3
    move-result-object v15

    #@1c4
    .line 5130
    .local v15, kwltName:Ljava/lang/String;
    move-object/from16 v0, p0

    #@1c6
    invoke-virtual {v0, v15}, Lcom/android/internal/os/BatteryStatsImpl;->getKernelWakelockTimerLocked(Ljava/lang/String;)Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;

    #@1c9
    move-result-object v26

    #@1ca
    move-object/from16 v0, v26

    #@1cc
    move-object/from16 v1, p1

    #@1ce
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;->readSummaryFromParcelLocked(Landroid/os/Parcel;)V

    #@1d1
    .line 5127
    .end local v15           #kwltName:Ljava/lang/String;
    :cond_1d1
    add-int/lit8 v10, v10, 0x1

    #@1d3
    goto :goto_1b8

    #@1d4
    .line 5134
    :cond_1d4
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@1d7
    move-result v26

    #@1d8
    sput v26, Lcom/android/internal/os/BatteryStatsImpl;->sNumSpeedSteps:I

    #@1da
    .line 5136
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@1dd
    move-result v7

    #@1de
    .line 5137
    .local v7, NU:I
    const/16 v26, 0x2710

    #@1e0
    move/from16 v0, v26

    #@1e2
    if-le v7, v0, :cond_200

    #@1e4
    .line 5138
    const-string v26, "BatteryStatsImpl"

    #@1e6
    new-instance v27, Ljava/lang/StringBuilder;

    #@1e8
    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    #@1eb
    const-string v28, "File corrupt: too many uids "

    #@1ed
    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f0
    move-result-object v27

    #@1f1
    move-object/from16 v0, v27

    #@1f3
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f6
    move-result-object v27

    #@1f7
    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1fa
    move-result-object v27

    #@1fb
    invoke-static/range {v26 .. v27}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1fe
    goto/16 :goto_3b

    #@200
    .line 5141
    :cond_200
    const/4 v13, 0x0

    #@201
    .local v13, iu:I
    :goto_201
    if-ge v13, v7, :cond_3b

    #@203
    .line 5142
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@206
    move-result v23

    #@207
    .line 5143
    .local v23, uid:I
    new-instance v22, Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@209
    move-object/from16 v0, v22

    #@20b
    move-object/from16 v1, p0

    #@20d
    move/from16 v2, v23

    #@20f
    invoke-direct {v0, v1, v2}, Lcom/android/internal/os/BatteryStatsImpl$Uid;-><init>(Lcom/android/internal/os/BatteryStatsImpl;I)V

    #@212
    .line 5144
    .local v22, u:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    move-object/from16 v0, p0

    #@214
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@216
    move-object/from16 v26, v0

    #@218
    move-object/from16 v0, v26

    #@21a
    move/from16 v1, v23

    #@21c
    move-object/from16 v2, v22

    #@21e
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@221
    .line 5146
    const/16 v26, 0x0

    #@223
    move/from16 v0, v26

    #@225
    move-object/from16 v1, v22

    #@227
    iput-boolean v0, v1, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiRunning:Z

    #@229
    .line 5147
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@22c
    move-result v26

    #@22d
    if-eqz v26, :cond_23c

    #@22f
    .line 5148
    move-object/from16 v0, v22

    #@231
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiRunningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@233
    move-object/from16 v26, v0

    #@235
    move-object/from16 v0, v26

    #@237
    move-object/from16 v1, p1

    #@239
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->readSummaryFromParcelLocked(Landroid/os/Parcel;)V

    #@23c
    .line 5150
    :cond_23c
    const/16 v26, 0x0

    #@23e
    move/from16 v0, v26

    #@240
    move-object/from16 v1, v22

    #@242
    iput-boolean v0, v1, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mFullWifiLockOut:Z

    #@244
    .line 5151
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@247
    move-result v26

    #@248
    if-eqz v26, :cond_257

    #@24a
    .line 5152
    move-object/from16 v0, v22

    #@24c
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mFullWifiLockTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@24e
    move-object/from16 v26, v0

    #@250
    move-object/from16 v0, v26

    #@252
    move-object/from16 v1, p1

    #@254
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->readSummaryFromParcelLocked(Landroid/os/Parcel;)V

    #@257
    .line 5154
    :cond_257
    const/16 v26, 0x0

    #@259
    move/from16 v0, v26

    #@25b
    move-object/from16 v1, v22

    #@25d
    iput-boolean v0, v1, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiScanStarted:Z

    #@25f
    .line 5155
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@262
    move-result v26

    #@263
    if-eqz v26, :cond_272

    #@265
    .line 5156
    move-object/from16 v0, v22

    #@267
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiScanTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@269
    move-object/from16 v26, v0

    #@26b
    move-object/from16 v0, v26

    #@26d
    move-object/from16 v1, p1

    #@26f
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->readSummaryFromParcelLocked(Landroid/os/Parcel;)V

    #@272
    .line 5158
    :cond_272
    const/16 v26, 0x0

    #@274
    move/from16 v0, v26

    #@276
    move-object/from16 v1, v22

    #@278
    iput-boolean v0, v1, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiMulticastEnabled:Z

    #@27a
    .line 5159
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@27d
    move-result v26

    #@27e
    if-eqz v26, :cond_28d

    #@280
    .line 5160
    move-object/from16 v0, v22

    #@282
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiMulticastTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@284
    move-object/from16 v26, v0

    #@286
    move-object/from16 v0, v26

    #@288
    move-object/from16 v1, p1

    #@28a
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->readSummaryFromParcelLocked(Landroid/os/Parcel;)V

    #@28d
    .line 5162
    :cond_28d
    const/16 v26, 0x0

    #@28f
    move/from16 v0, v26

    #@291
    move-object/from16 v1, v22

    #@293
    iput-boolean v0, v1, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mAudioTurnedOn:Z

    #@295
    .line 5163
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@298
    move-result v26

    #@299
    if-eqz v26, :cond_2a8

    #@29b
    .line 5164
    move-object/from16 v0, v22

    #@29d
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mAudioTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@29f
    move-object/from16 v26, v0

    #@2a1
    move-object/from16 v0, v26

    #@2a3
    move-object/from16 v1, p1

    #@2a5
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->readSummaryFromParcelLocked(Landroid/os/Parcel;)V

    #@2a8
    .line 5166
    :cond_2a8
    const/16 v26, 0x0

    #@2aa
    move/from16 v0, v26

    #@2ac
    move-object/from16 v1, v22

    #@2ae
    iput-boolean v0, v1, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mVideoTurnedOn:Z

    #@2b0
    .line 5167
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@2b3
    move-result v26

    #@2b4
    if-eqz v26, :cond_2c3

    #@2b6
    .line 5168
    move-object/from16 v0, v22

    #@2b8
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mVideoTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2ba
    move-object/from16 v26, v0

    #@2bc
    move-object/from16 v0, v26

    #@2be
    move-object/from16 v1, p1

    #@2c0
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->readSummaryFromParcelLocked(Landroid/os/Parcel;)V

    #@2c3
    .line 5171
    :cond_2c3
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@2c6
    move-result v26

    #@2c7
    if-eqz v26, :cond_2ed

    #@2c9
    .line 5172
    move-object/from16 v0, v22

    #@2cb
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mUserActivityCounters:[Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@2cd
    move-object/from16 v26, v0

    #@2cf
    if-nez v26, :cond_2d4

    #@2d1
    .line 5173
    invoke-virtual/range {v22 .. v22}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->initUserActivityLocked()V

    #@2d4
    .line 5175
    :cond_2d4
    const/4 v9, 0x0

    #@2d5
    :goto_2d5
    const/16 v26, 0x3

    #@2d7
    move/from16 v0, v26

    #@2d9
    if-ge v9, v0, :cond_2ed

    #@2db
    .line 5176
    move-object/from16 v0, v22

    #@2dd
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mUserActivityCounters:[Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@2df
    move-object/from16 v26, v0

    #@2e1
    aget-object v26, v26, v9

    #@2e3
    move-object/from16 v0, v26

    #@2e5
    move-object/from16 v1, p1

    #@2e7
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$Counter;->readSummaryFromParcelLocked(Landroid/os/Parcel;)V

    #@2ea
    .line 5175
    add-int/lit8 v9, v9, 0x1

    #@2ec
    goto :goto_2d5

    #@2ed
    .line 5180
    :cond_2ed
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@2f0
    move-result v8

    #@2f1
    .line 5181
    .local v8, NW:I
    const/16 v26, 0x64

    #@2f3
    move/from16 v0, v26

    #@2f5
    if-le v8, v0, :cond_313

    #@2f7
    .line 5182
    const-string v26, "BatteryStatsImpl"

    #@2f9
    new-instance v27, Ljava/lang/StringBuilder;

    #@2fb
    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    #@2fe
    const-string v28, "File corrupt: too many wake locks "

    #@300
    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@303
    move-result-object v27

    #@304
    move-object/from16 v0, v27

    #@306
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@309
    move-result-object v27

    #@30a
    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30d
    move-result-object v27

    #@30e
    invoke-static/range {v26 .. v27}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@311
    goto/16 :goto_3b

    #@313
    .line 5185
    :cond_313
    const/4 v14, 0x0

    #@314
    .local v14, iw:I
    :goto_314
    if-ge v14, v8, :cond_368

    #@316
    .line 5186
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@319
    move-result-object v25

    #@31a
    .line 5187
    .local v25, wlName:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@31d
    move-result v26

    #@31e
    if-eqz v26, :cond_333

    #@320
    .line 5188
    const/16 v26, 0x1

    #@322
    move-object/from16 v0, v22

    #@324
    move-object/from16 v1, v25

    #@326
    move/from16 v2, v26

    #@328
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->getWakeTimerLocked(Ljava/lang/String;I)Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@32b
    move-result-object v26

    #@32c
    move-object/from16 v0, v26

    #@32e
    move-object/from16 v1, p1

    #@330
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->readSummaryFromParcelLocked(Landroid/os/Parcel;)V

    #@333
    .line 5190
    :cond_333
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@336
    move-result v26

    #@337
    if-eqz v26, :cond_34c

    #@339
    .line 5191
    const/16 v26, 0x0

    #@33b
    move-object/from16 v0, v22

    #@33d
    move-object/from16 v1, v25

    #@33f
    move/from16 v2, v26

    #@341
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->getWakeTimerLocked(Ljava/lang/String;I)Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@344
    move-result-object v26

    #@345
    move-object/from16 v0, v26

    #@347
    move-object/from16 v1, p1

    #@349
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->readSummaryFromParcelLocked(Landroid/os/Parcel;)V

    #@34c
    .line 5193
    :cond_34c
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@34f
    move-result v26

    #@350
    if-eqz v26, :cond_365

    #@352
    .line 5194
    const/16 v26, 0x2

    #@354
    move-object/from16 v0, v22

    #@356
    move-object/from16 v1, v25

    #@358
    move/from16 v2, v26

    #@35a
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->getWakeTimerLocked(Ljava/lang/String;I)Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@35d
    move-result-object v26

    #@35e
    move-object/from16 v0, v26

    #@360
    move-object/from16 v1, p1

    #@362
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->readSummaryFromParcelLocked(Landroid/os/Parcel;)V

    #@365
    .line 5185
    :cond_365
    add-int/lit8 v14, v14, 0x1

    #@367
    goto :goto_314

    #@368
    .line 5198
    .end local v25           #wlName:Ljava/lang/String;
    :cond_368
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@36b
    move-result v4

    #@36c
    .line 5199
    .local v4, NP:I
    const/16 v26, 0x3e8

    #@36e
    move/from16 v0, v26

    #@370
    if-le v4, v0, :cond_38e

    #@372
    .line 5200
    const-string v26, "BatteryStatsImpl"

    #@374
    new-instance v27, Ljava/lang/StringBuilder;

    #@376
    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    #@379
    const-string v28, "File corrupt: too many sensors "

    #@37b
    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37e
    move-result-object v27

    #@37f
    move-object/from16 v0, v27

    #@381
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@384
    move-result-object v27

    #@385
    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@388
    move-result-object v27

    #@389
    invoke-static/range {v26 .. v27}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@38c
    goto/16 :goto_3b

    #@38e
    .line 5203
    :cond_38e
    const/4 v12, 0x0

    #@38f
    .local v12, is:I
    :goto_38f
    if-ge v12, v4, :cond_3b1

    #@391
    .line 5204
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@394
    move-result v20

    #@395
    .line 5205
    .local v20, seNumber:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@398
    move-result v26

    #@399
    if-eqz v26, :cond_3ae

    #@39b
    .line 5206
    const/16 v26, 0x1

    #@39d
    move-object/from16 v0, v22

    #@39f
    move/from16 v1, v20

    #@3a1
    move/from16 v2, v26

    #@3a3
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->getSensorTimerLocked(IZ)Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@3a6
    move-result-object v26

    #@3a7
    move-object/from16 v0, v26

    #@3a9
    move-object/from16 v1, p1

    #@3ab
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->readSummaryFromParcelLocked(Landroid/os/Parcel;)V

    #@3ae
    .line 5203
    :cond_3ae
    add-int/lit8 v12, v12, 0x1

    #@3b0
    goto :goto_38f

    #@3b1
    .line 5211
    .end local v20           #seNumber:I
    :cond_3b1
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@3b4
    move-result v4

    #@3b5
    .line 5212
    const/16 v26, 0x3e8

    #@3b7
    move/from16 v0, v26

    #@3b9
    if-le v4, v0, :cond_3d7

    #@3bb
    .line 5213
    const-string v26, "BatteryStatsImpl"

    #@3bd
    new-instance v27, Ljava/lang/StringBuilder;

    #@3bf
    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    #@3c2
    const-string v28, "File corrupt: too many processes "

    #@3c4
    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c7
    move-result-object v27

    #@3c8
    move-object/from16 v0, v27

    #@3ca
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3cd
    move-result-object v27

    #@3ce
    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d1
    move-result-object v27

    #@3d2
    invoke-static/range {v26 .. v27}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3d5
    goto/16 :goto_3b

    #@3d7
    .line 5216
    :cond_3d7
    const/4 v11, 0x0

    #@3d8
    .local v11, ip:I
    :goto_3d8
    if-ge v11, v4, :cond_482

    #@3da
    .line 5217
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3dd
    move-result-object v18

    #@3de
    .line 5218
    .local v18, procName:Ljava/lang/String;
    move-object/from16 v0, v22

    #@3e0
    move-object/from16 v1, v18

    #@3e2
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->getProcessStatsLocked(Ljava/lang/String;)Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;

    #@3e5
    move-result-object v16

    #@3e6
    .line 5219
    .local v16, p:Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    #@3e9
    move-result-wide v26

    #@3ea
    move-wide/from16 v0, v26

    #@3ec
    move-object/from16 v2, v16

    #@3ee
    iput-wide v0, v2, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mLoadedUserTime:J

    #@3f0
    move-wide/from16 v0, v26

    #@3f2
    move-object/from16 v2, v16

    #@3f4
    iput-wide v0, v2, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mUserTime:J

    #@3f6
    .line 5220
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    #@3f9
    move-result-wide v26

    #@3fa
    move-wide/from16 v0, v26

    #@3fc
    move-object/from16 v2, v16

    #@3fe
    iput-wide v0, v2, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mLoadedSystemTime:J

    #@400
    move-wide/from16 v0, v26

    #@402
    move-object/from16 v2, v16

    #@404
    iput-wide v0, v2, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSystemTime:J

    #@406
    .line 5221
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@409
    move-result v26

    #@40a
    move/from16 v0, v26

    #@40c
    move-object/from16 v1, v16

    #@40e
    iput v0, v1, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mLoadedStarts:I

    #@410
    move/from16 v0, v26

    #@412
    move-object/from16 v1, v16

    #@414
    iput v0, v1, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mStarts:I

    #@416
    .line 5222
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@419
    move-result v6

    #@41a
    .line 5223
    .local v6, NSB:I
    const/16 v26, 0x64

    #@41c
    move/from16 v0, v26

    #@41e
    if-le v6, v0, :cond_43c

    #@420
    .line 5224
    const-string v26, "BatteryStatsImpl"

    #@422
    new-instance v27, Ljava/lang/StringBuilder;

    #@424
    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    #@427
    const-string v28, "File corrupt: too many speed bins "

    #@429
    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42c
    move-result-object v27

    #@42d
    move-object/from16 v0, v27

    #@42f
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@432
    move-result-object v27

    #@433
    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@436
    move-result-object v27

    #@437
    invoke-static/range {v26 .. v27}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@43a
    goto/16 :goto_3b

    #@43c
    .line 5227
    :cond_43c
    new-array v0, v6, [Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@43e
    move-object/from16 v26, v0

    #@440
    move-object/from16 v0, v26

    #@442
    move-object/from16 v1, v16

    #@444
    iput-object v0, v1, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSpeedBins:[Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@446
    .line 5228
    const/4 v9, 0x0

    #@447
    :goto_447
    if-ge v9, v6, :cond_474

    #@449
    .line 5229
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@44c
    move-result v26

    #@44d
    if-eqz v26, :cond_471

    #@44f
    .line 5230
    move-object/from16 v0, v16

    #@451
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSpeedBins:[Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@453
    move-object/from16 v26, v0

    #@455
    new-instance v27, Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@457
    move-object/from16 v0, p0

    #@459
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@45b
    move-object/from16 v28, v0

    #@45d
    invoke-direct/range {v27 .. v28}, Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;-><init>(Ljava/util/ArrayList;)V

    #@460
    aput-object v27, v26, v9

    #@462
    .line 5231
    move-object/from16 v0, v16

    #@464
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSpeedBins:[Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@466
    move-object/from16 v26, v0

    #@468
    aget-object v26, v26, v9

    #@46a
    move-object/from16 v0, v26

    #@46c
    move-object/from16 v1, p1

    #@46e
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;->readSummaryFromParcelLocked(Landroid/os/Parcel;)V

    #@471
    .line 5228
    :cond_471
    add-int/lit8 v9, v9, 0x1

    #@473
    goto :goto_447

    #@474
    .line 5234
    :cond_474
    move-object/from16 v0, v16

    #@476
    move-object/from16 v1, p1

    #@478
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->readExcessivePowerFromParcelLocked(Landroid/os/Parcel;)Z

    #@47b
    move-result v26

    #@47c
    if-eqz v26, :cond_3b

    #@47e
    .line 5216
    add-int/lit8 v11, v11, 0x1

    #@480
    goto/16 :goto_3d8

    #@482
    .line 5239
    .end local v6           #NSB:I
    .end local v16           #p:Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;
    .end local v18           #procName:Ljava/lang/String;
    :cond_482
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@485
    move-result v4

    #@486
    .line 5240
    const/16 v26, 0x2710

    #@488
    move/from16 v0, v26

    #@48a
    if-le v4, v0, :cond_4a8

    #@48c
    .line 5241
    const-string v26, "BatteryStatsImpl"

    #@48e
    new-instance v27, Ljava/lang/StringBuilder;

    #@490
    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    #@493
    const-string v28, "File corrupt: too many packages "

    #@495
    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@498
    move-result-object v27

    #@499
    move-object/from16 v0, v27

    #@49b
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@49e
    move-result-object v27

    #@49f
    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a2
    move-result-object v27

    #@4a3
    invoke-static/range {v26 .. v27}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4a6
    goto/16 :goto_3b

    #@4a8
    .line 5244
    :cond_4a8
    const/4 v11, 0x0

    #@4a9
    :goto_4a9
    if-ge v11, v4, :cond_535

    #@4ab
    .line 5245
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4ae
    move-result-object v17

    #@4af
    .line 5246
    .local v17, pkgName:Ljava/lang/String;
    move-object/from16 v0, v22

    #@4b1
    move-object/from16 v1, v17

    #@4b3
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->getPackageStatsLocked(Ljava/lang/String;)Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;

    #@4b6
    move-result-object v16

    #@4b7
    .line 5247
    .local v16, p:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@4ba
    move-result v26

    #@4bb
    move/from16 v0, v26

    #@4bd
    move-object/from16 v1, v16

    #@4bf
    iput v0, v1, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;->mLoadedWakeups:I

    #@4c1
    move/from16 v0, v26

    #@4c3
    move-object/from16 v1, v16

    #@4c5
    iput v0, v1, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;->mWakeups:I

    #@4c7
    .line 5248
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@4ca
    move-result v5

    #@4cb
    .line 5249
    .local v5, NS:I
    const/16 v26, 0x3e8

    #@4cd
    move/from16 v0, v26

    #@4cf
    if-le v5, v0, :cond_4ed

    #@4d1
    .line 5250
    const-string v26, "BatteryStatsImpl"

    #@4d3
    new-instance v27, Ljava/lang/StringBuilder;

    #@4d5
    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    #@4d8
    const-string v28, "File corrupt: too many services "

    #@4da
    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4dd
    move-result-object v27

    #@4de
    move-object/from16 v0, v27

    #@4e0
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4e3
    move-result-object v27

    #@4e4
    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e7
    move-result-object v27

    #@4e8
    invoke-static/range {v26 .. v27}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4eb
    goto/16 :goto_3b

    #@4ed
    .line 5253
    :cond_4ed
    const/4 v12, 0x0

    #@4ee
    :goto_4ee
    if-ge v12, v5, :cond_531

    #@4f0
    .line 5254
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4f3
    move-result-object v21

    #@4f4
    .line 5255
    .local v21, servName:Ljava/lang/String;
    move-object/from16 v0, v22

    #@4f6
    move-object/from16 v1, v17

    #@4f8
    move-object/from16 v2, v21

    #@4fa
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->getServiceStatsLocked(Ljava/lang/String;Ljava/lang/String;)Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;

    #@4fd
    move-result-object v19

    #@4fe
    .line 5256
    .local v19, s:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    #@501
    move-result-wide v26

    #@502
    move-wide/from16 v0, v26

    #@504
    move-object/from16 v2, v19

    #@506
    iput-wide v0, v2, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;->mLoadedStartTime:J

    #@508
    move-wide/from16 v0, v26

    #@50a
    move-object/from16 v2, v19

    #@50c
    iput-wide v0, v2, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;->mStartTime:J

    #@50e
    .line 5257
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@511
    move-result v26

    #@512
    move/from16 v0, v26

    #@514
    move-object/from16 v1, v19

    #@516
    iput v0, v1, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;->mLoadedStarts:I

    #@518
    move/from16 v0, v26

    #@51a
    move-object/from16 v1, v19

    #@51c
    iput v0, v1, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;->mStarts:I

    #@51e
    .line 5258
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@521
    move-result v26

    #@522
    move/from16 v0, v26

    #@524
    move-object/from16 v1, v19

    #@526
    iput v0, v1, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;->mLoadedLaunches:I

    #@528
    move/from16 v0, v26

    #@52a
    move-object/from16 v1, v19

    #@52c
    iput v0, v1, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;->mLaunches:I

    #@52e
    .line 5253
    add-int/lit8 v12, v12, 0x1

    #@530
    goto :goto_4ee

    #@531
    .line 5244
    .end local v19           #s:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;
    .end local v21           #servName:Ljava/lang/String;
    :cond_531
    add-int/lit8 v11, v11, 0x1

    #@533
    goto/16 :goto_4a9

    #@535
    .line 5262
    .end local v5           #NS:I
    .end local v16           #p:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;
    .end local v17           #pkgName:Ljava/lang/String;
    :cond_535
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    #@538
    move-result-wide v26

    #@539
    move-wide/from16 v0, v26

    #@53b
    move-object/from16 v2, v22

    #@53d
    iput-wide v0, v2, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mLoadedTcpBytesReceived:J

    #@53f
    .line 5263
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    #@542
    move-result-wide v26

    #@543
    move-wide/from16 v0, v26

    #@545
    move-object/from16 v2, v22

    #@547
    iput-wide v0, v2, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mLoadedTcpBytesSent:J

    #@549
    .line 5141
    add-int/lit8 v13, v13, 0x1

    #@54b
    goto/16 :goto_201
.end method

.method private updateAllPhoneStateLocked(III)V
    .registers 9
    .parameter "state"
    .parameter "simState"
    .parameter "bin"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1876
    const/4 v1, 0x0

    #@2
    .line 1877
    .local v1, scanning:Z
    const/4 v0, 0x0

    #@3
    .line 1879
    .local v0, newHistory:Z
    iput p1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneServiceStateRaw:I

    #@5
    .line 1880
    iput p2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSimStateRaw:I

    #@7
    .line 1881
    iput p3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalStrengthBinRaw:I

    #@9
    .line 1883
    if-ne p2, v3, :cond_10

    #@b
    .line 1886
    if-ne p1, v3, :cond_10

    #@d
    if-lez p3, :cond_10

    #@f
    .line 1888
    const/4 p1, 0x0

    #@10
    .line 1893
    :cond_10
    const/4 v2, 0x3

    #@11
    if-ne p1, v2, :cond_80

    #@13
    .line 1894
    const/4 p3, -0x1

    #@14
    .line 1914
    :cond_14
    :goto_14
    if-nez v1, :cond_2e

    #@16
    .line 1916
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalScanningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@18
    invoke-virtual {v2}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->isRunningLocked()Z

    #@1b
    move-result v2

    #@1c
    if-eqz v2, :cond_2e

    #@1e
    .line 1917
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@20
    iget v3, v2, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@22
    const v4, -0x8000001

    #@25
    and-int/2addr v3, v4

    #@26
    iput v3, v2, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@28
    .line 1920
    const/4 v0, 0x1

    #@29
    .line 1921
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalScanningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2b
    invoke-virtual {v2, p0}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->stopRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@2e
    .line 1925
    :cond_2e
    iget v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneServiceState:I

    #@30
    if-eq v2, p1, :cond_42

    #@32
    .line 1926
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@34
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@36
    iget v3, v3, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@38
    and-int/lit16 v3, v3, -0xf01

    #@3a
    shl-int/lit8 v4, p1, 0x8

    #@3c
    or-int/2addr v3, v4

    #@3d
    iput v3, v2, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@3f
    .line 1930
    const/4 v0, 0x1

    #@40
    .line 1931
    iput p1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneServiceState:I

    #@42
    .line 1934
    :cond_42
    iget v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalStrengthBin:I

    #@44
    if-eq v2, p3, :cond_76

    #@46
    .line 1935
    iget v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalStrengthBin:I

    #@48
    if-ltz v2, :cond_53

    #@4a
    .line 1936
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalStrengthsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@4c
    iget v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalStrengthBin:I

    #@4e
    aget-object v2, v2, v3

    #@50
    invoke-virtual {v2, p0}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->stopRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@53
    .line 1938
    :cond_53
    if-ltz p3, :cond_9f

    #@55
    .line 1939
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalStrengthsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@57
    aget-object v2, v2, p3

    #@59
    invoke-virtual {v2}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->isRunningLocked()Z

    #@5c
    move-result v2

    #@5d
    if-nez v2, :cond_66

    #@5f
    .line 1940
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalStrengthsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@61
    aget-object v2, v2, p3

    #@63
    invoke-virtual {v2, p0}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@66
    .line 1942
    :cond_66
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@68
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@6a
    iget v3, v3, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@6c
    and-int/lit16 v3, v3, -0xf1

    #@6e
    shl-int/lit8 v4, p3, 0x4

    #@70
    or-int/2addr v3, v4

    #@71
    iput v3, v2, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@73
    .line 1946
    const/4 v0, 0x1

    #@74
    .line 1950
    :goto_74
    iput p3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalStrengthBin:I

    #@76
    .line 1953
    :cond_76
    if-eqz v0, :cond_7f

    #@78
    .line 1954
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@7b
    move-result-wide v2

    #@7c
    invoke-virtual {p0, v2, v3}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@7f
    .line 1956
    :cond_7f
    return-void

    #@80
    .line 1897
    :cond_80
    if-eqz p1, :cond_14

    #@82
    .line 1902
    if-ne p1, v3, :cond_14

    #@84
    .line 1903
    const/4 v1, 0x1

    #@85
    .line 1904
    const/4 p3, 0x0

    #@86
    .line 1905
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalScanningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@88
    invoke-virtual {v2}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->isRunningLocked()Z

    #@8b
    move-result v2

    #@8c
    if-nez v2, :cond_14

    #@8e
    .line 1906
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@90
    iget v3, v2, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@92
    const/high16 v4, 0x800

    #@94
    or-int/2addr v3, v4

    #@95
    iput v3, v2, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@97
    .line 1907
    const/4 v0, 0x1

    #@98
    .line 1910
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalScanningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@9a
    invoke-virtual {v2, p0}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@9d
    goto/16 :goto_14

    #@9f
    .line 1948
    :cond_9f
    const/4 v2, -0x1

    #@a0
    invoke-virtual {p0, v2}, Lcom/android/internal/os/BatteryStatsImpl;->stopAllSignalStrengthTimersLocked(I)V

    #@a3
    goto :goto_74
.end method


# virtual methods
.method addHistoryBufferLocked(J)V
    .registers 12
    .parameter "curTime"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v7, 0x1

    #@2
    .line 1268
    iget-boolean v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHaveBatteryLevel:Z

    #@4
    if-eqz v3, :cond_a

    #@6
    iget-boolean v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mRecordingHistory:Z

    #@8
    if-nez v3, :cond_b

    #@a
    .line 1317
    :cond_a
    :goto_a
    return-void

    #@b
    .line 1272
    :cond_b
    iget-wide v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBaseTime:J

    #@d
    add-long/2addr v3, p1

    #@e
    iget-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryLastWritten:Landroid/os/BatteryStats$HistoryItem;

    #@10
    iget-wide v5, v5, Landroid/os/BatteryStats$HistoryItem;->time:J

    #@12
    sub-long v1, v3, v5

    #@14
    .line 1273
    .local v1, timeDiff:J
    iget v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBufferLastPos:I

    #@16
    if-ltz v3, :cond_b8

    #@18
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryLastWritten:Landroid/os/BatteryStats$HistoryItem;

    #@1a
    iget-byte v3, v3, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    #@1c
    if-ne v3, v7, :cond_b8

    #@1e
    const-wide/16 v3, 0x7d0

    #@20
    cmp-long v3, v1, v3

    #@22
    if-gez v3, :cond_b8

    #@24
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryLastWritten:Landroid/os/BatteryStats$HistoryItem;

    #@26
    iget v3, v3, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@28
    iget-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@2a
    iget v4, v4, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@2c
    xor-int/2addr v3, v4

    #@2d
    iget v4, p0, Lcom/android/internal/os/BatteryStatsImpl;->mChangedBufferStates:I

    #@2f
    and-int/2addr v3, v4

    #@30
    if-nez v3, :cond_b8

    #@32
    .line 1278
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@34
    iget v4, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBufferLastPos:I

    #@36
    invoke-virtual {v3, v4}, Landroid/os/Parcel;->setDataSize(I)V

    #@39
    .line 1279
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@3b
    iget v4, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBufferLastPos:I

    #@3d
    invoke-virtual {v3, v4}, Landroid/os/Parcel;->setDataPosition(I)V

    #@40
    .line 1280
    const/4 v3, -0x1

    #@41
    iput v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBufferLastPos:I

    #@43
    .line 1281
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryLastLastWritten:Landroid/os/BatteryStats$HistoryItem;

    #@45
    iget-byte v3, v3, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    #@47
    if-ne v3, v7, :cond_65

    #@49
    const-wide/16 v3, 0x1f4

    #@4b
    cmp-long v3, v1, v3

    #@4d
    if-gez v3, :cond_65

    #@4f
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryLastLastWritten:Landroid/os/BatteryStats$HistoryItem;

    #@51
    iget-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@53
    invoke-virtual {v3, v4}, Landroid/os/BatteryStats$HistoryItem;->same(Landroid/os/BatteryStats$HistoryItem;)Z

    #@56
    move-result v3

    #@57
    if-eqz v3, :cond_65

    #@59
    .line 1286
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryLastWritten:Landroid/os/BatteryStats$HistoryItem;

    #@5b
    iget-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryLastLastWritten:Landroid/os/BatteryStats$HistoryItem;

    #@5d
    invoke-virtual {v3, v4}, Landroid/os/BatteryStats$HistoryItem;->setTo(Landroid/os/BatteryStats$HistoryItem;)V

    #@60
    .line 1287
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryLastLastWritten:Landroid/os/BatteryStats$HistoryItem;

    #@62
    iput-byte v8, v3, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    #@64
    goto :goto_a

    #@65
    .line 1290
    :cond_65
    iget v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mChangedBufferStates:I

    #@67
    iget-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryLastWritten:Landroid/os/BatteryStats$HistoryItem;

    #@69
    iget v4, v4, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@6b
    iget-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@6d
    iget v5, v5, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@6f
    xor-int/2addr v4, v5

    #@70
    or-int/2addr v3, v4

    #@71
    iput v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mChangedBufferStates:I

    #@73
    .line 1291
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryLastWritten:Landroid/os/BatteryStats$HistoryItem;

    #@75
    iget-wide v3, v3, Landroid/os/BatteryStats$HistoryItem;->time:J

    #@77
    iget-wide v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBaseTime:J

    #@79
    sub-long p1, v3, v5

    #@7b
    .line 1292
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryLastWritten:Landroid/os/BatteryStats$HistoryItem;

    #@7d
    iget-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryLastLastWritten:Landroid/os/BatteryStats$HistoryItem;

    #@7f
    invoke-virtual {v3, v4}, Landroid/os/BatteryStats$HistoryItem;->setTo(Landroid/os/BatteryStats$HistoryItem;)V

    #@82
    .line 1297
    :goto_82
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@84
    invoke-virtual {v3}, Landroid/os/Parcel;->dataSize()I

    #@87
    move-result v0

    #@88
    .line 1298
    .local v0, dataSize:I
    const/high16 v3, 0x2

    #@8a
    if-lt v0, v3, :cond_b3

    #@8c
    .line 1299
    iget-boolean v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryOverflow:Z

    #@8e
    if-nez v3, :cond_96

    #@90
    .line 1300
    iput-boolean v7, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryOverflow:Z

    #@92
    .line 1301
    const/4 v3, 0x3

    #@93
    invoke-virtual {p0, p1, p2, v3}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryBufferLocked(JB)V

    #@96
    .line 1308
    :cond_96
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryLastWritten:Landroid/os/BatteryStats$HistoryItem;

    #@98
    iget-byte v3, v3, Landroid/os/BatteryStats$HistoryItem;->batteryLevel:B

    #@9a
    iget-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@9c
    iget-byte v4, v4, Landroid/os/BatteryStats$HistoryItem;->batteryLevel:B

    #@9e
    if-ne v3, v4, :cond_b3

    #@a0
    const v3, 0x24000

    #@a3
    if-ge v0, v3, :cond_a

    #@a5
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryLastWritten:Landroid/os/BatteryStats$HistoryItem;

    #@a7
    iget v3, v3, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@a9
    iget-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@ab
    iget v4, v4, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@ad
    xor-int/2addr v3, v4

    #@ae
    const/high16 v4, 0x101c

    #@b0
    and-int/2addr v3, v4

    #@b1
    if-eqz v3, :cond_a

    #@b3
    .line 1316
    :cond_b3
    invoke-virtual {p0, p1, p2, v7}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryBufferLocked(JB)V

    #@b6
    goto/16 :goto_a

    #@b8
    .line 1294
    .end local v0           #dataSize:I
    :cond_b8
    iput v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mChangedBufferStates:I

    #@ba
    goto :goto_82
.end method

.method addHistoryBufferLocked(JB)V
    .registers 9
    .parameter "curTime"
    .parameter "cmd"

    #@0
    .prologue
    .line 1320
    const/4 v0, 0x0

    #@1
    .line 1321
    .local v0, origPos:I
    iget-boolean v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mIteratingHistory:Z

    #@3
    if-eqz v1, :cond_16

    #@5
    .line 1322
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@7
    invoke-virtual {v1}, Landroid/os/Parcel;->dataPosition()I

    #@a
    move-result v0

    #@b
    .line 1323
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@d
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@f
    invoke-virtual {v2}, Landroid/os/Parcel;->dataSize()I

    #@12
    move-result v2

    #@13
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->setDataPosition(I)V

    #@16
    .line 1325
    :cond_16
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@18
    invoke-virtual {v1}, Landroid/os/Parcel;->dataPosition()I

    #@1b
    move-result v1

    #@1c
    iput v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBufferLastPos:I

    #@1e
    .line 1326
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryLastLastWritten:Landroid/os/BatteryStats$HistoryItem;

    #@20
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryLastWritten:Landroid/os/BatteryStats$HistoryItem;

    #@22
    invoke-virtual {v1, v2}, Landroid/os/BatteryStats$HistoryItem;->setTo(Landroid/os/BatteryStats$HistoryItem;)V

    #@25
    .line 1327
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryLastWritten:Landroid/os/BatteryStats$HistoryItem;

    #@27
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBaseTime:J

    #@29
    add-long/2addr v2, p1

    #@2a
    iget-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@2c
    invoke-virtual {v1, v2, v3, p3, v4}, Landroid/os/BatteryStats$HistoryItem;->setTo(JBLandroid/os/BatteryStats$HistoryItem;)V

    #@2f
    .line 1328
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryLastWritten:Landroid/os/BatteryStats$HistoryItem;

    #@31
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@33
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryLastLastWritten:Landroid/os/BatteryStats$HistoryItem;

    #@35
    invoke-virtual {v1, v2, v3}, Landroid/os/BatteryStats$HistoryItem;->writeDelta(Landroid/os/Parcel;Landroid/os/BatteryStats$HistoryItem;)V

    #@38
    .line 1329
    iput-wide p1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mLastHistoryTime:J

    #@3a
    .line 1333
    iget-boolean v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mIteratingHistory:Z

    #@3c
    if-eqz v1, :cond_43

    #@3e
    .line 1334
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@40
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    #@43
    .line 1336
    :cond_43
    return-void
.end method

.method addHistoryRecordLocked(J)V
    .registers 3
    .parameter "curTime"

    #@0
    .prologue
    .line 1341
    invoke-virtual {p0, p1, p2}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryBufferLocked(J)V

    #@3
    .line 1344
    return-void
.end method

.method addHistoryRecordLocked(JB)V
    .registers 8
    .parameter "curTime"
    .parameter "cmd"

    #@0
    .prologue
    .line 1400
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCache:Landroid/os/BatteryStats$HistoryItem;

    #@2
    .line 1401
    .local v0, rec:Landroid/os/BatteryStats$HistoryItem;
    if-eqz v0, :cond_14

    #@4
    .line 1402
    iget-object v1, v0, Landroid/os/BatteryStats$HistoryItem;->next:Landroid/os/BatteryStats$HistoryItem;

    #@6
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCache:Landroid/os/BatteryStats$HistoryItem;

    #@8
    .line 1406
    :goto_8
    iget-wide v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBaseTime:J

    #@a
    add-long/2addr v1, p1

    #@b
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@d
    invoke-virtual {v0, v1, v2, p3, v3}, Landroid/os/BatteryStats$HistoryItem;->setTo(JBLandroid/os/BatteryStats$HistoryItem;)V

    #@10
    .line 1408
    invoke-virtual {p0, v0}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(Landroid/os/BatteryStats$HistoryItem;)V

    #@13
    .line 1409
    return-void

    #@14
    .line 1404
    :cond_14
    new-instance v0, Landroid/os/BatteryStats$HistoryItem;

    #@16
    .end local v0           #rec:Landroid/os/BatteryStats$HistoryItem;
    invoke-direct {v0}, Landroid/os/BatteryStats$HistoryItem;-><init>()V

    #@19
    .restart local v0       #rec:Landroid/os/BatteryStats$HistoryItem;
    goto :goto_8
.end method

.method addHistoryRecordLocked(Landroid/os/BatteryStats$HistoryItem;)V
    .registers 3
    .parameter "rec"

    #@0
    .prologue
    .line 1412
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mNumHistoryItems:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mNumHistoryItems:I

    #@6
    .line 1413
    const/4 v0, 0x0

    #@7
    iput-object v0, p1, Landroid/os/BatteryStats$HistoryItem;->next:Landroid/os/BatteryStats$HistoryItem;

    #@9
    .line 1414
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryEnd:Landroid/os/BatteryStats$HistoryItem;

    #@b
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryLastEnd:Landroid/os/BatteryStats$HistoryItem;

    #@d
    .line 1415
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryEnd:Landroid/os/BatteryStats$HistoryItem;

    #@f
    if-eqz v0, :cond_18

    #@11
    .line 1416
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryEnd:Landroid/os/BatteryStats$HistoryItem;

    #@13
    iput-object p1, v0, Landroid/os/BatteryStats$HistoryItem;->next:Landroid/os/BatteryStats$HistoryItem;

    #@15
    .line 1417
    iput-object p1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryEnd:Landroid/os/BatteryStats$HistoryItem;

    #@17
    .line 1421
    :goto_17
    return-void

    #@18
    .line 1419
    :cond_18
    iput-object p1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryEnd:Landroid/os/BatteryStats$HistoryItem;

    #@1a
    iput-object p1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistory:Landroid/os/BatteryStats$HistoryItem;

    #@1c
    goto :goto_17
.end method

.method clearHistoryLocked()V
    .registers 4

    #@0
    .prologue
    const-wide/16 v0, 0x0

    #@2
    const/4 v2, 0x0

    #@3
    .line 1434
    iput-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBaseTime:J

    #@5
    .line 1435
    iput-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mLastHistoryTime:J

    #@7
    .line 1437
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@9
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->setDataSize(I)V

    #@c
    .line 1438
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@e
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->setDataPosition(I)V

    #@11
    .line 1439
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@13
    const/high16 v1, 0x1

    #@15
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataCapacity(I)V

    #@18
    .line 1440
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryLastLastWritten:Landroid/os/BatteryStats$HistoryItem;

    #@1a
    iput-byte v2, v0, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    #@1c
    .line 1441
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryLastWritten:Landroid/os/BatteryStats$HistoryItem;

    #@1e
    iput-byte v2, v0, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    #@20
    .line 1442
    const/4 v0, -0x1

    #@21
    iput v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBufferLastPos:I

    #@23
    .line 1443
    iput-boolean v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryOverflow:Z

    #@25
    .line 1444
    return-void
.end method

.method public commitPendingDataToDisk()V
    .registers 6

    #@0
    .prologue
    .line 4895
    monitor-enter p0

    #@1
    .line 4896
    :try_start_1
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPendingWrite:Landroid/os/Parcel;

    #@3
    .line 4897
    .local v1, next:Landroid/os/Parcel;
    const/4 v3, 0x0

    #@4
    iput-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPendingWrite:Landroid/os/Parcel;

    #@6
    .line 4898
    if-nez v1, :cond_a

    #@8
    .line 4899
    monitor-exit p0

    #@9
    .line 4919
    :goto_9
    return-void

    #@a
    .line 4902
    :cond_a
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWriteLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@c
    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    #@f
    .line 4903
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_1 .. :try_end_10} :catchall_39

    #@10
    .line 4906
    :try_start_10
    new-instance v2, Ljava/io/FileOutputStream;

    #@12
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mFile:Lcom/android/internal/util/JournaledFile;

    #@14
    invoke-virtual {v3}, Lcom/android/internal/util/JournaledFile;->chooseForWrite()Ljava/io/File;

    #@17
    move-result-object v3

    #@18
    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    #@1b
    .line 4907
    .local v2, stream:Ljava/io/FileOutputStream;
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/io/FileOutputStream;->write([B)V

    #@22
    .line 4908
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V

    #@25
    .line 4909
    invoke-static {v2}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z

    #@28
    .line 4910
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    #@2b
    .line 4911
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mFile:Lcom/android/internal/util/JournaledFile;

    #@2d
    invoke-virtual {v3}, Lcom/android/internal/util/JournaledFile;->commit()V
    :try_end_30
    .catchall {:try_start_10 .. :try_end_30} :catchall_4f
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_30} :catch_3c

    #@30
    .line 4916
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@33
    .line 4917
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWriteLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@35
    .end local v2           #stream:Ljava/io/FileOutputStream;
    :goto_35
    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    #@38
    goto :goto_9

    #@39
    .line 4903
    .end local v1           #next:Landroid/os/Parcel;
    :catchall_39
    move-exception v3

    #@3a
    :try_start_3a
    monitor-exit p0
    :try_end_3b
    .catchall {:try_start_3a .. :try_end_3b} :catchall_39

    #@3b
    throw v3

    #@3c
    .line 4912
    .restart local v1       #next:Landroid/os/Parcel;
    :catch_3c
    move-exception v0

    #@3d
    .line 4913
    .local v0, e:Ljava/io/IOException;
    :try_start_3d
    const-string v3, "BatteryStats"

    #@3f
    const-string v4, "Error writing battery statistics"

    #@41
    invoke-static {v3, v4, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@44
    .line 4914
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mFile:Lcom/android/internal/util/JournaledFile;

    #@46
    invoke-virtual {v3}, Lcom/android/internal/util/JournaledFile;->rollback()V
    :try_end_49
    .catchall {:try_start_3d .. :try_end_49} :catchall_4f

    #@49
    .line 4916
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@4c
    .line 4917
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWriteLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@4e
    goto :goto_35

    #@4f
    .line 4916
    .end local v0           #e:Ljava/io/IOException;
    :catchall_4f
    move-exception v3

    #@50
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@53
    .line 4917
    iget-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWriteLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@55
    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    #@58
    .line 4916
    throw v3
.end method

.method public computeBatteryRealtime(JI)J
    .registers 8
    .parameter "curTime"
    .parameter "which"

    #@0
    .prologue
    .line 4552
    packed-switch p3, :pswitch_data_1e

    #@3
    .line 4562
    const-wide/16 v0, 0x0

    #@5
    :goto_5
    return-wide v0

    #@6
    .line 4554
    :pswitch_6
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBatteryRealtime:J

    #@8
    invoke-virtual {p0, p1, p2}, Lcom/android/internal/os/BatteryStatsImpl;->getBatteryRealtimeLocked(J)J

    #@b
    move-result-wide v2

    #@c
    add-long/2addr v0, v2

    #@d
    goto :goto_5

    #@e
    .line 4556
    :pswitch_e
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBatteryLastRealtime:J

    #@10
    goto :goto_5

    #@11
    .line 4558
    :pswitch_11
    invoke-virtual {p0, p1, p2}, Lcom/android/internal/os/BatteryStatsImpl;->getBatteryRealtimeLocked(J)J

    #@14
    move-result-wide v0

    #@15
    goto :goto_5

    #@16
    .line 4560
    :pswitch_16
    invoke-virtual {p0, p1, p2}, Lcom/android/internal/os/BatteryStatsImpl;->getBatteryRealtimeLocked(J)J

    #@19
    move-result-wide v0

    #@1a
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggedBatteryRealtime:J

    #@1c
    sub-long/2addr v0, v2

    #@1d
    goto :goto_5

    #@1e
    .line 4552
    :pswitch_data_1e
    .packed-switch 0x0
        :pswitch_6
        :pswitch_e
        :pswitch_11
        :pswitch_16
    .end packed-switch
.end method

.method public computeBatteryUptime(JI)J
    .registers 8
    .parameter "curTime"
    .parameter "which"

    #@0
    .prologue
    .line 4537
    packed-switch p3, :pswitch_data_1e

    #@3
    .line 4547
    const-wide/16 v0, 0x0

    #@5
    :goto_5
    return-wide v0

    #@6
    .line 4539
    :pswitch_6
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBatteryUptime:J

    #@8
    invoke-virtual {p0, p1, p2}, Lcom/android/internal/os/BatteryStatsImpl;->getBatteryUptime(J)J

    #@b
    move-result-wide v2

    #@c
    add-long/2addr v0, v2

    #@d
    goto :goto_5

    #@e
    .line 4541
    :pswitch_e
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBatteryLastUptime:J

    #@10
    goto :goto_5

    #@11
    .line 4543
    :pswitch_11
    invoke-virtual {p0, p1, p2}, Lcom/android/internal/os/BatteryStatsImpl;->getBatteryUptime(J)J

    #@14
    move-result-wide v0

    #@15
    goto :goto_5

    #@16
    .line 4545
    :pswitch_16
    invoke-virtual {p0, p1, p2}, Lcom/android/internal/os/BatteryStatsImpl;->getBatteryUptimeLocked(J)J

    #@19
    move-result-wide v0

    #@1a
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggedBatteryUptime:J

    #@1c
    sub-long/2addr v0, v2

    #@1d
    goto :goto_5

    #@1e
    .line 4537
    :pswitch_data_1e
    .packed-switch 0x0
        :pswitch_6
        :pswitch_e
        :pswitch_11
        :pswitch_16
    .end packed-switch
.end method

.method public computeRealtime(JI)J
    .registers 8
    .parameter "curTime"
    .parameter "which"

    #@0
    .prologue
    .line 4526
    packed-switch p3, :pswitch_data_1c

    #@3
    .line 4532
    const-wide/16 v0, 0x0

    #@5
    :goto_5
    return-wide v0

    #@6
    .line 4527
    :pswitch_6
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mRealtime:J

    #@8
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mRealtimeStart:J

    #@a
    sub-long v2, p1, v2

    #@c
    add-long/2addr v0, v2

    #@d
    goto :goto_5

    #@e
    .line 4528
    :pswitch_e
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mLastRealtime:J

    #@10
    goto :goto_5

    #@11
    .line 4529
    :pswitch_11
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mRealtimeStart:J

    #@13
    sub-long v0, p1, v0

    #@15
    goto :goto_5

    #@16
    .line 4530
    :pswitch_16
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTrackBatteryRealtimeStart:J

    #@18
    sub-long v0, p1, v0

    #@1a
    goto :goto_5

    #@1b
    .line 4526
    nop

    #@1c
    :pswitch_data_1c
    .packed-switch 0x0
        :pswitch_6
        :pswitch_e
        :pswitch_11
        :pswitch_16
    .end packed-switch
.end method

.method public computeUptime(JI)J
    .registers 8
    .parameter "curTime"
    .parameter "which"

    #@0
    .prologue
    .line 4515
    packed-switch p3, :pswitch_data_1c

    #@3
    .line 4521
    const-wide/16 v0, 0x0

    #@5
    :goto_5
    return-wide v0

    #@6
    .line 4516
    :pswitch_6
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUptime:J

    #@8
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUptimeStart:J

    #@a
    sub-long v2, p1, v2

    #@c
    add-long/2addr v0, v2

    #@d
    goto :goto_5

    #@e
    .line 4517
    :pswitch_e
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mLastUptime:J

    #@10
    goto :goto_5

    #@11
    .line 4518
    :pswitch_11
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUptimeStart:J

    #@13
    sub-long v0, p1, v0

    #@15
    goto :goto_5

    #@16
    .line 4519
    :pswitch_16
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTrackBatteryUptimeStart:J

    #@18
    sub-long v0, p1, v0

    #@1a
    goto :goto_5

    #@1b
    .line 4515
    nop

    #@1c
    :pswitch_data_1c
    .packed-switch 0x0
        :pswitch_6
        :pswitch_e
        :pswitch_11
        :pswitch_16
    .end packed-switch
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 4978
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public distributeWorkLocked(I)V
    .registers 27
    .parameter "which"

    #@0
    .prologue
    .line 4799
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@4
    move-object/from16 v21, v0

    #@6
    const/16 v22, 0x3f2

    #@8
    invoke-virtual/range {v21 .. v22}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@b
    move-result-object v20

    #@c
    check-cast v20, Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@e
    .line 4800
    .local v20, wifiUid:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    if-eqz v20, :cond_14f

    #@10
    .line 4801
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@13
    move-result-wide v21

    #@14
    const-wide/16 v23, 0x3e8

    #@16
    mul-long v21, v21, v23

    #@18
    move-object/from16 v0, p0

    #@1a
    move-wide/from16 v1, v21

    #@1c
    move/from16 v3, p1

    #@1e
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/os/BatteryStatsImpl;->computeBatteryRealtime(JI)J

    #@21
    move-result-wide v13

    #@22
    .line 4802
    .local v13, uSecTime:J
    move-object/from16 v0, v20

    #@24
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mProcessStats:Ljava/util/HashMap;

    #@26
    move-object/from16 v21, v0

    #@28
    invoke-virtual/range {v21 .. v21}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@2b
    move-result-object v21

    #@2c
    invoke-interface/range {v21 .. v21}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@2f
    move-result-object v5

    #@30
    .local v5, i$:Ljava/util/Iterator;
    :cond_30
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@33
    move-result v21

    #@34
    if-eqz v21, :cond_14f

    #@36
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@39
    move-result-object v6

    #@3a
    check-cast v6, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;

    #@3c
    .line 4803
    .local v6, proc:Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;
    move-object/from16 v0, p0

    #@3e
    move/from16 v1, p1

    #@40
    invoke-virtual {v0, v13, v14, v1}, Lcom/android/internal/os/BatteryStatsImpl;->getGlobalWifiRunningTime(JI)J

    #@43
    move-result-wide v11

    #@44
    .line 4804
    .local v11, totalRunningTime:J
    const/4 v4, 0x0

    #@45
    .local v4, i:I
    :goto_45
    move-object/from16 v0, p0

    #@47
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@49
    move-object/from16 v21, v0

    #@4b
    invoke-virtual/range {v21 .. v21}, Landroid/util/SparseArray;->size()I

    #@4e
    move-result v21

    #@4f
    move/from16 v0, v21

    #@51
    if-ge v4, v0, :cond_30

    #@53
    .line 4805
    move-object/from16 v0, p0

    #@55
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@57
    move-object/from16 v21, v0

    #@59
    move-object/from16 v0, v21

    #@5b
    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@5e
    move-result-object v15

    #@5f
    check-cast v15, Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@61
    .line 4806
    .local v15, uid:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    iget v0, v15, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mUid:I

    #@63
    move/from16 v21, v0

    #@65
    const/16 v22, 0x3f2

    #@67
    move/from16 v0, v21

    #@69
    move/from16 v1, v22

    #@6b
    if-eq v0, v1, :cond_14b

    #@6d
    .line 4807
    move/from16 v0, p1

    #@6f
    invoke-virtual {v15, v13, v14, v0}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->getWifiRunningTime(JI)J

    #@72
    move-result-wide v17

    #@73
    .line 4808
    .local v17, uidRunningTime:J
    const-wide/16 v21, 0x0

    #@75
    cmp-long v21, v17, v21

    #@77
    if-lez v21, :cond_14b

    #@79
    .line 4809
    const-string v21, "*wifi*"

    #@7b
    move-object/from16 v0, v21

    #@7d
    invoke-virtual {v15, v0}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->getProcessStatsLocked(Ljava/lang/String;)Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;

    #@80
    move-result-object v16

    #@81
    .line 4810
    .local v16, uidProc:Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;
    move/from16 v0, p1

    #@83
    invoke-virtual {v6, v0}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->getUserTime(I)J

    #@86
    move-result-wide v9

    #@87
    .line 4811
    .local v9, time:J
    mul-long v21, v9, v17

    #@89
    div-long v9, v21, v11

    #@8b
    .line 4812
    move-object/from16 v0, v16

    #@8d
    iget-wide v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mUserTime:J

    #@8f
    move-wide/from16 v21, v0

    #@91
    add-long v21, v21, v9

    #@93
    move-wide/from16 v0, v21

    #@95
    move-object/from16 v2, v16

    #@97
    iput-wide v0, v2, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mUserTime:J

    #@99
    .line 4813
    iget-wide v0, v6, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mUserTime:J

    #@9b
    move-wide/from16 v21, v0

    #@9d
    sub-long v21, v21, v9

    #@9f
    move-wide/from16 v0, v21

    #@a1
    iput-wide v0, v6, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mUserTime:J

    #@a3
    .line 4814
    move/from16 v0, p1

    #@a5
    invoke-virtual {v6, v0}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->getSystemTime(I)J

    #@a8
    move-result-wide v9

    #@a9
    .line 4815
    mul-long v21, v9, v17

    #@ab
    div-long v9, v21, v11

    #@ad
    .line 4816
    move-object/from16 v0, v16

    #@af
    iget-wide v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSystemTime:J

    #@b1
    move-wide/from16 v21, v0

    #@b3
    add-long v21, v21, v9

    #@b5
    move-wide/from16 v0, v21

    #@b7
    move-object/from16 v2, v16

    #@b9
    iput-wide v0, v2, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSystemTime:J

    #@bb
    .line 4817
    iget-wide v0, v6, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSystemTime:J

    #@bd
    move-wide/from16 v21, v0

    #@bf
    sub-long v21, v21, v9

    #@c1
    move-wide/from16 v0, v21

    #@c3
    iput-wide v0, v6, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSystemTime:J

    #@c5
    .line 4818
    move/from16 v0, p1

    #@c7
    invoke-virtual {v6, v0}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->getForegroundTime(I)J

    #@ca
    move-result-wide v9

    #@cb
    .line 4819
    mul-long v21, v9, v17

    #@cd
    div-long v9, v21, v11

    #@cf
    .line 4820
    move-object/from16 v0, v16

    #@d1
    iget-wide v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mForegroundTime:J

    #@d3
    move-wide/from16 v21, v0

    #@d5
    add-long v21, v21, v9

    #@d7
    move-wide/from16 v0, v21

    #@d9
    move-object/from16 v2, v16

    #@db
    iput-wide v0, v2, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mForegroundTime:J

    #@dd
    .line 4821
    iget-wide v0, v6, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mForegroundTime:J

    #@df
    move-wide/from16 v21, v0

    #@e1
    sub-long v21, v21, v9

    #@e3
    move-wide/from16 v0, v21

    #@e5
    iput-wide v0, v6, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mForegroundTime:J

    #@e7
    .line 4822
    const/4 v7, 0x0

    #@e8
    .local v7, sb:I
    :goto_e8
    iget-object v0, v6, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSpeedBins:[Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@ea
    move-object/from16 v21, v0

    #@ec
    move-object/from16 v0, v21

    #@ee
    array-length v0, v0

    #@ef
    move/from16 v21, v0

    #@f1
    move/from16 v0, v21

    #@f3
    if-ge v7, v0, :cond_149

    #@f5
    .line 4823
    iget-object v0, v6, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSpeedBins:[Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@f7
    move-object/from16 v21, v0

    #@f9
    aget-object v8, v21, v7

    #@fb
    .line 4824
    .local v8, sc:Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;
    if-eqz v8, :cond_146

    #@fd
    .line 4825
    move/from16 v0, p1

    #@ff
    invoke-virtual {v8, v0}, Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;->getCountLocked(I)I

    #@102
    move-result v21

    #@103
    move/from16 v0, v21

    #@105
    int-to-long v9, v0

    #@106
    .line 4826
    mul-long v21, v9, v17

    #@108
    div-long v9, v21, v11

    #@10a
    .line 4827
    move-object/from16 v0, v16

    #@10c
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSpeedBins:[Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@10e
    move-object/from16 v21, v0

    #@110
    aget-object v19, v21, v7

    #@112
    .line 4828
    .local v19, uidSc:Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;
    if-nez v19, :cond_12b

    #@114
    .line 4829
    new-instance v19, Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@116
    .end local v19           #uidSc:Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;
    move-object/from16 v0, p0

    #@118
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@11a
    move-object/from16 v21, v0

    #@11c
    move-object/from16 v0, v19

    #@11e
    move-object/from16 v1, v21

    #@120
    invoke-direct {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;-><init>(Ljava/util/ArrayList;)V

    #@123
    .line 4830
    .restart local v19       #uidSc:Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;
    move-object/from16 v0, v16

    #@125
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSpeedBins:[Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@127
    move-object/from16 v21, v0

    #@129
    aput-object v19, v21, v7

    #@12b
    .line 4832
    :cond_12b
    move-object/from16 v0, v19

    #@12d
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Counter;->mCount:Ljava/util/concurrent/atomic/AtomicInteger;

    #@12f
    move-object/from16 v21, v0

    #@131
    long-to-int v0, v9

    #@132
    move/from16 v22, v0

    #@134
    invoke-virtual/range {v21 .. v22}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    #@137
    .line 4833
    iget-object v0, v8, Lcom/android/internal/os/BatteryStatsImpl$Counter;->mCount:Ljava/util/concurrent/atomic/AtomicInteger;

    #@139
    move-object/from16 v21, v0

    #@13b
    neg-long v0, v9

    #@13c
    move-wide/from16 v22, v0

    #@13e
    move-wide/from16 v0, v22

    #@140
    long-to-int v0, v0

    #@141
    move/from16 v22, v0

    #@143
    invoke-virtual/range {v21 .. v22}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    #@146
    .line 4822
    .end local v19           #uidSc:Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;
    :cond_146
    add-int/lit8 v7, v7, 0x1

    #@148
    goto :goto_e8

    #@149
    .line 4836
    .end local v8           #sc:Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;
    :cond_149
    sub-long v11, v11, v17

    #@14b
    .line 4804
    .end local v7           #sb:I
    .end local v9           #time:J
    .end local v16           #uidProc:Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;
    .end local v17           #uidRunningTime:J
    :cond_14b
    add-int/lit8 v4, v4, 0x1

    #@14d
    goto/16 :goto_45

    #@14f
    .line 4842
    .end local v4           #i:I
    .end local v5           #i$:Ljava/util/Iterator;
    .end local v6           #proc:Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;
    .end local v11           #totalRunningTime:J
    .end local v13           #uSecTime:J
    .end local v15           #uid:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    :cond_14f
    return-void
.end method

.method public doPlugLocked(JJ)V
    .registers 14
    .parameter "batteryUptime"
    .parameter "batteryRealtime"

    #@0
    .prologue
    .line 1487
    const/4 v0, 0x0

    #@1
    .line 1489
    .local v0, entry:Landroid/net/NetworkStats$Entry;
    iget-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@3
    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    #@6
    move-result v5

    #@7
    add-int/lit8 v3, v5, -0x1

    #@9
    .local v3, iu:I
    :goto_9
    if-ltz v3, :cond_3a

    #@b
    .line 1490
    iget-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@d
    invoke-virtual {v5, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@10
    move-result-object v4

    #@11
    check-cast v4, Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@13
    .line 1491
    .local v4, u:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    iget-wide v5, v4, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mStartedTcpBytesReceived:J

    #@15
    const-wide/16 v7, 0x0

    #@17
    cmp-long v5, v5, v7

    #@19
    if-ltz v5, :cond_25

    #@1b
    .line 1492
    invoke-virtual {v4}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->computeCurrentTcpBytesReceived()J

    #@1e
    move-result-wide v5

    #@1f
    iput-wide v5, v4, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mCurrentTcpBytesReceived:J

    #@21
    .line 1493
    const-wide/16 v5, -0x1

    #@23
    iput-wide v5, v4, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mStartedTcpBytesReceived:J

    #@25
    .line 1495
    :cond_25
    iget-wide v5, v4, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mStartedTcpBytesSent:J

    #@27
    const-wide/16 v7, 0x0

    #@29
    cmp-long v5, v5, v7

    #@2b
    if-ltz v5, :cond_37

    #@2d
    .line 1496
    invoke-virtual {v4}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->computeCurrentTcpBytesSent()J

    #@30
    move-result-wide v5

    #@31
    iput-wide v5, v4, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mCurrentTcpBytesSent:J

    #@33
    .line 1497
    const-wide/16 v5, -0x1

    #@35
    iput-wide v5, v4, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mStartedTcpBytesSent:J

    #@37
    .line 1489
    :cond_37
    add-int/lit8 v3, v3, -0x1

    #@39
    goto :goto_9

    #@3a
    .line 1500
    .end local v4           #u:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    :cond_3a
    iget-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@3c
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@3f
    move-result v5

    #@40
    add-int/lit8 v1, v5, -0x1

    #@42
    .local v1, i:I
    :goto_42
    if-ltz v1, :cond_52

    #@44
    .line 1501
    iget-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@46
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@49
    move-result-object v5

    #@4a
    check-cast v5, Lcom/android/internal/os/BatteryStatsImpl$Unpluggable;

    #@4c
    invoke-interface {v5, p1, p2, p3, p4}, Lcom/android/internal/os/BatteryStatsImpl$Unpluggable;->plug(JJ)V

    #@4f
    .line 1500
    add-int/lit8 v1, v1, -0x1

    #@51
    goto :goto_42

    #@52
    .line 1505
    :cond_52
    invoke-direct {p0}, Lcom/android/internal/os/BatteryStatsImpl;->getNetworkStatsSummary()Landroid/net/NetworkStats;

    #@55
    move-result-object v2

    #@56
    .line 1506
    .local v2, ifaceStats:Landroid/net/NetworkStats;
    iget-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mMobileIfaces:Ljava/util/HashSet;

    #@58
    invoke-virtual {v2, v0, v5}, Landroid/net/NetworkStats;->getTotal(Landroid/net/NetworkStats$Entry;Ljava/util/HashSet;)Landroid/net/NetworkStats$Entry;

    #@5b
    move-result-object v0

    #@5c
    .line 1507
    iget-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mMobileDataRx:[J

    #@5e
    iget-wide v6, v0, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@60
    invoke-direct {p0, v5, v6, v7}, Lcom/android/internal/os/BatteryStatsImpl;->doDataPlug([JJ)V

    #@63
    .line 1508
    iget-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mMobileDataTx:[J

    #@65
    iget-wide v6, v0, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@67
    invoke-direct {p0, v5, v6, v7}, Lcom/android/internal/os/BatteryStatsImpl;->doDataPlug([JJ)V

    #@6a
    .line 1509
    invoke-virtual {v2, v0}, Landroid/net/NetworkStats;->getTotal(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats$Entry;

    #@6d
    move-result-object v0

    #@6e
    .line 1510
    iget-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTotalDataRx:[J

    #@70
    iget-wide v6, v0, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@72
    invoke-direct {p0, v5, v6, v7}, Lcom/android/internal/os/BatteryStatsImpl;->doDataPlug([JJ)V

    #@75
    .line 1511
    iget-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTotalDataTx:[J

    #@77
    iget-wide v6, v0, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@79
    invoke-direct {p0, v5, v6, v7}, Lcom/android/internal/os/BatteryStatsImpl;->doDataPlug([JJ)V

    #@7c
    .line 1514
    invoke-virtual {p0}, Lcom/android/internal/os/BatteryStatsImpl;->getRadioDataUptime()J

    #@7f
    move-result-wide v5

    #@80
    iput-wide v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mRadioDataUptime:J

    #@82
    .line 1515
    const-wide/16 v5, -0x1

    #@84
    iput-wide v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mRadioDataStart:J

    #@86
    .line 1518
    invoke-virtual {p0}, Lcom/android/internal/os/BatteryStatsImpl;->getBluetoothPingCount()I

    #@89
    move-result v5

    #@8a
    iput v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBluetoothPingCount:I

    #@8c
    .line 1519
    const/4 v5, -0x1

    #@8d
    iput v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBluetoothPingStart:I

    #@8f
    .line 1520
    return-void
.end method

.method public doUnplugLocked(JJ)V
    .registers 14
    .parameter "batteryUptime"
    .parameter "batteryRealtime"

    #@0
    .prologue
    .line 1447
    const/4 v0, 0x0

    #@1
    .line 1450
    .local v0, entry:Landroid/net/NetworkStats$Entry;
    invoke-direct {p0}, Lcom/android/internal/os/BatteryStatsImpl;->getNetworkStatsDetailGroupedByUid()Landroid/net/NetworkStats;

    #@4
    move-result-object v5

    #@5
    .line 1451
    .local v5, uidStats:Landroid/net/NetworkStats;
    invoke-virtual {v5}, Landroid/net/NetworkStats;->size()I

    #@8
    move-result v3

    #@9
    .line 1452
    .local v3, size:I
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    :goto_a
    if-ge v1, v3, :cond_30

    #@c
    .line 1453
    invoke-virtual {v5, v1, v0}, Landroid/net/NetworkStats;->getValues(ILandroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats$Entry;

    #@f
    move-result-object v0

    #@10
    .line 1455
    iget-object v6, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@12
    iget v7, v0, Landroid/net/NetworkStats$Entry;->uid:I

    #@14
    invoke-virtual {v6, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@17
    move-result-object v4

    #@18
    check-cast v4, Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@1a
    .line 1456
    .local v4, u:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    if-nez v4, :cond_1f

    #@1c
    .line 1452
    :goto_1c
    add-int/lit8 v1, v1, 0x1

    #@1e
    goto :goto_a

    #@1f
    .line 1458
    :cond_1f
    iget-wide v6, v0, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@21
    iput-wide v6, v4, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mStartedTcpBytesReceived:J

    #@23
    .line 1459
    iget-wide v6, v0, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@25
    iput-wide v6, v4, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mStartedTcpBytesSent:J

    #@27
    .line 1460
    iget-wide v6, v4, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mCurrentTcpBytesReceived:J

    #@29
    iput-wide v6, v4, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mTcpBytesReceivedAtLastUnplug:J

    #@2b
    .line 1461
    iget-wide v6, v4, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mCurrentTcpBytesSent:J

    #@2d
    iput-wide v6, v4, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mTcpBytesSentAtLastUnplug:J

    #@2f
    goto :goto_1c

    #@30
    .line 1464
    .end local v4           #u:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    :cond_30
    iget-object v6, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@32
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@35
    move-result v6

    #@36
    add-int/lit8 v1, v6, -0x1

    #@38
    :goto_38
    if-ltz v1, :cond_48

    #@3a
    .line 1465
    iget-object v6, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@3c
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3f
    move-result-object v6

    #@40
    check-cast v6, Lcom/android/internal/os/BatteryStatsImpl$Unpluggable;

    #@42
    invoke-interface {v6, p1, p2, p3, p4}, Lcom/android/internal/os/BatteryStatsImpl$Unpluggable;->unplug(JJ)V

    #@45
    .line 1464
    add-int/lit8 v1, v1, -0x1

    #@47
    goto :goto_38

    #@48
    .line 1469
    :cond_48
    invoke-direct {p0}, Lcom/android/internal/os/BatteryStatsImpl;->getNetworkStatsSummary()Landroid/net/NetworkStats;

    #@4b
    move-result-object v2

    #@4c
    .line 1470
    .local v2, ifaceStats:Landroid/net/NetworkStats;
    iget-object v6, p0, Lcom/android/internal/os/BatteryStatsImpl;->mMobileIfaces:Ljava/util/HashSet;

    #@4e
    invoke-virtual {v2, v0, v6}, Landroid/net/NetworkStats;->getTotal(Landroid/net/NetworkStats$Entry;Ljava/util/HashSet;)Landroid/net/NetworkStats$Entry;

    #@51
    move-result-object v0

    #@52
    .line 1471
    iget-object v6, p0, Lcom/android/internal/os/BatteryStatsImpl;->mMobileDataRx:[J

    #@54
    iget-wide v7, v0, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@56
    invoke-direct {p0, v6, v7, v8}, Lcom/android/internal/os/BatteryStatsImpl;->doDataUnplug([JJ)V

    #@59
    .line 1472
    iget-object v6, p0, Lcom/android/internal/os/BatteryStatsImpl;->mMobileDataTx:[J

    #@5b
    iget-wide v7, v0, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@5d
    invoke-direct {p0, v6, v7, v8}, Lcom/android/internal/os/BatteryStatsImpl;->doDataUnplug([JJ)V

    #@60
    .line 1473
    invoke-virtual {v2, v0}, Landroid/net/NetworkStats;->getTotal(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats$Entry;

    #@63
    move-result-object v0

    #@64
    .line 1474
    iget-object v6, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTotalDataRx:[J

    #@66
    iget-wide v7, v0, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@68
    invoke-direct {p0, v6, v7, v8}, Lcom/android/internal/os/BatteryStatsImpl;->doDataUnplug([JJ)V

    #@6b
    .line 1475
    iget-object v6, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTotalDataTx:[J

    #@6d
    iget-wide v7, v0, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@6f
    invoke-direct {p0, v6, v7, v8}, Lcom/android/internal/os/BatteryStatsImpl;->doDataUnplug([JJ)V

    #@72
    .line 1478
    invoke-direct {p0}, Lcom/android/internal/os/BatteryStatsImpl;->getCurrentRadioDataUptime()J

    #@75
    move-result-wide v6

    #@76
    iput-wide v6, p0, Lcom/android/internal/os/BatteryStatsImpl;->mRadioDataStart:J

    #@78
    .line 1479
    const-wide/16 v6, 0x0

    #@7a
    iput-wide v6, p0, Lcom/android/internal/os/BatteryStatsImpl;->mRadioDataUptime:J

    #@7c
    .line 1482
    invoke-direct {p0}, Lcom/android/internal/os/BatteryStatsImpl;->getCurrentBluetoothPingCount()I

    #@7f
    move-result v6

    #@80
    iput v6, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBluetoothPingStart:I

    #@82
    .line 1483
    const/4 v6, 0x0

    #@83
    iput v6, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBluetoothPingCount:I

    #@85
    .line 1484
    return-void
.end method

.method public dumpLocked(Ljava/io/PrintWriter;)V
    .registers 2
    .parameter "pw"

    #@0
    .prologue
    .line 5740
    invoke-super {p0, p1}, Landroid/os/BatteryStats;->dumpLocked(Ljava/io/PrintWriter;)V

    #@3
    .line 5741
    return-void
.end method

.method public finishAddingCpuLocked(III[J)V
    .registers 18
    .parameter "perc"
    .parameter "utime"
    .parameter "stime"
    .parameter "cpuSpeedTimes"

    #@0
    .prologue
    .line 1609
    iget-object v11, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPartialTimers:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    .line 1610
    .local v1, N:I
    if-eqz p1, :cond_78

    #@8
    .line 1611
    const/4 v7, 0x0

    #@9
    .line 1612
    .local v7, num:I
    const/4 v4, 0x0

    #@a
    .local v4, i:I
    :goto_a
    if-ge v4, v1, :cond_27

    #@c
    .line 1613
    iget-object v11, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPartialTimers:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v9

    #@12
    check-cast v9, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@14
    .line 1614
    .local v9, st:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    iget-boolean v11, v9, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->mInList:Z

    #@16
    if-eqz v11, :cond_24

    #@18
    .line 1615
    iget-object v10, v9, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->mUid:Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@1a
    .line 1618
    .local v10, uid:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    if-eqz v10, :cond_24

    #@1c
    iget v11, v10, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mUid:I

    #@1e
    const/16 v12, 0x3e8

    #@20
    if-eq v11, v12, :cond_24

    #@22
    .line 1619
    add-int/lit8 v7, v7, 0x1

    #@24
    .line 1612
    .end local v10           #uid:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    :cond_24
    add-int/lit8 v4, v4, 0x1

    #@26
    goto :goto_a

    #@27
    .line 1623
    .end local v9           #st:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    :cond_27
    if-eqz v7, :cond_5c

    #@29
    .line 1624
    const/4 v4, 0x0

    #@2a
    :goto_2a
    if-ge v4, v1, :cond_5c

    #@2c
    .line 1625
    iget-object v11, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPartialTimers:Ljava/util/ArrayList;

    #@2e
    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@31
    move-result-object v9

    #@32
    check-cast v9, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@34
    .line 1626
    .restart local v9       #st:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    iget-boolean v11, v9, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->mInList:Z

    #@36
    if-eqz v11, :cond_59

    #@38
    .line 1627
    iget-object v10, v9, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->mUid:Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@3a
    .line 1628
    .restart local v10       #uid:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    if-eqz v10, :cond_59

    #@3c
    iget v11, v10, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mUid:I

    #@3e
    const/16 v12, 0x3e8

    #@40
    if-eq v11, v12, :cond_59

    #@42
    .line 1629
    div-int v6, p2, v7

    #@44
    .line 1630
    .local v6, myUTime:I
    div-int v5, p3, v7

    #@46
    .line 1631
    .local v5, mySTime:I
    sub-int/2addr p2, v6

    #@47
    .line 1632
    sub-int p3, p3, v5

    #@49
    .line 1633
    add-int/lit8 v7, v7, -0x1

    #@4b
    .line 1634
    const-string v11, "*wakelock*"

    #@4d
    invoke-virtual {v10, v11}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->getProcessStatsLocked(Ljava/lang/String;)Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;

    #@50
    move-result-object v8

    #@51
    .line 1635
    .local v8, proc:Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;
    invoke-virtual {v8, v6, v5}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->addCpuTimeLocked(II)V

    #@54
    .line 1636
    move-object/from16 v0, p4

    #@56
    invoke-virtual {v8, v0}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->addSpeedStepTimes([J)V

    #@59
    .line 1624
    .end local v5           #mySTime:I
    .end local v6           #myUTime:I
    .end local v8           #proc:Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;
    .end local v10           #uid:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    :cond_59
    add-int/lit8 v4, v4, 0x1

    #@5b
    goto :goto_2a

    #@5c
    .line 1643
    .end local v9           #st:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    :cond_5c
    if-nez p2, :cond_60

    #@5e
    if-eqz p3, :cond_78

    #@60
    .line 1644
    :cond_60
    const/16 v11, 0x3e8

    #@62
    invoke-virtual {p0, v11}, Lcom/android/internal/os/BatteryStatsImpl;->getUidStatsLocked(I)Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@65
    move-result-object v10

    #@66
    .line 1645
    .restart local v10       #uid:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    if-eqz v10, :cond_78

    #@68
    .line 1646
    const-string v11, "*lost*"

    #@6a
    invoke-virtual {v10, v11}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->getProcessStatsLocked(Ljava/lang/String;)Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;

    #@6d
    move-result-object v8

    #@6e
    .line 1647
    .restart local v8       #proc:Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;
    move/from16 v0, p3

    #@70
    invoke-virtual {v8, p2, v0}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->addCpuTimeLocked(II)V

    #@73
    .line 1648
    move-object/from16 v0, p4

    #@75
    invoke-virtual {v8, v0}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->addSpeedStepTimes([J)V

    #@78
    .line 1653
    .end local v4           #i:I
    .end local v7           #num:I
    .end local v8           #proc:Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;
    .end local v10           #uid:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    :cond_78
    iget-object v11, p0, Lcom/android/internal/os/BatteryStatsImpl;->mLastPartialTimers:Ljava/util/ArrayList;

    #@7a
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    #@7d
    move-result v2

    #@7e
    .line 1654
    .local v2, NL:I
    if-eq v1, v2, :cond_99

    #@80
    const/4 v3, 0x1

    #@81
    .line 1655
    .local v3, diff:Z
    :goto_81
    const/4 v4, 0x0

    #@82
    .restart local v4       #i:I
    :goto_82
    if-ge v4, v2, :cond_9d

    #@84
    if-nez v3, :cond_9d

    #@86
    .line 1656
    iget-object v11, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPartialTimers:Ljava/util/ArrayList;

    #@88
    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@8b
    move-result-object v11

    #@8c
    iget-object v12, p0, Lcom/android/internal/os/BatteryStatsImpl;->mLastPartialTimers:Ljava/util/ArrayList;

    #@8e
    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@91
    move-result-object v12

    #@92
    if-eq v11, v12, :cond_9b

    #@94
    const/4 v11, 0x1

    #@95
    :goto_95
    or-int/2addr v3, v11

    #@96
    .line 1655
    add-int/lit8 v4, v4, 0x1

    #@98
    goto :goto_82

    #@99
    .line 1654
    .end local v3           #diff:Z
    .end local v4           #i:I
    :cond_99
    const/4 v3, 0x0

    #@9a
    goto :goto_81

    #@9b
    .line 1656
    .restart local v3       #diff:Z
    .restart local v4       #i:I
    :cond_9b
    const/4 v11, 0x0

    #@9c
    goto :goto_95

    #@9d
    .line 1658
    :cond_9d
    if-nez v3, :cond_b0

    #@9f
    .line 1659
    const/4 v4, 0x0

    #@a0
    :goto_a0
    if-ge v4, v2, :cond_dc

    #@a2
    .line 1660
    iget-object v11, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPartialTimers:Ljava/util/ArrayList;

    #@a4
    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@a7
    move-result-object v11

    #@a8
    check-cast v11, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@aa
    const/4 v12, 0x1

    #@ab
    iput-boolean v12, v11, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->mInList:Z

    #@ad
    .line 1659
    add-int/lit8 v4, v4, 0x1

    #@af
    goto :goto_a0

    #@b0
    .line 1665
    :cond_b0
    const/4 v4, 0x0

    #@b1
    :goto_b1
    if-ge v4, v2, :cond_c1

    #@b3
    .line 1666
    iget-object v11, p0, Lcom/android/internal/os/BatteryStatsImpl;->mLastPartialTimers:Ljava/util/ArrayList;

    #@b5
    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@b8
    move-result-object v11

    #@b9
    check-cast v11, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@bb
    const/4 v12, 0x0

    #@bc
    iput-boolean v12, v11, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->mInList:Z

    #@be
    .line 1665
    add-int/lit8 v4, v4, 0x1

    #@c0
    goto :goto_b1

    #@c1
    .line 1668
    :cond_c1
    iget-object v11, p0, Lcom/android/internal/os/BatteryStatsImpl;->mLastPartialTimers:Ljava/util/ArrayList;

    #@c3
    invoke-virtual {v11}, Ljava/util/ArrayList;->clear()V

    #@c6
    .line 1669
    const/4 v4, 0x0

    #@c7
    :goto_c7
    if-ge v4, v1, :cond_dc

    #@c9
    .line 1670
    iget-object v11, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPartialTimers:Ljava/util/ArrayList;

    #@cb
    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@ce
    move-result-object v9

    #@cf
    check-cast v9, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@d1
    .line 1671
    .restart local v9       #st:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    const/4 v11, 0x1

    #@d2
    iput-boolean v11, v9, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->mInList:Z

    #@d4
    .line 1672
    iget-object v11, p0, Lcom/android/internal/os/BatteryStatsImpl;->mLastPartialTimers:Ljava/util/ArrayList;

    #@d6
    invoke-virtual {v11, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@d9
    .line 1669
    add-int/lit8 v4, v4, 0x1

    #@db
    goto :goto_c7

    #@dc
    .line 1674
    .end local v9           #st:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    :cond_dc
    return-void
.end method

.method public finishIteratingHistoryLocked()V
    .registers 3

    #@0
    .prologue
    .line 4218
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mIteratingHistory:Z

    #@3
    .line 4219
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@5
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@7
    invoke-virtual {v1}, Landroid/os/Parcel;->dataSize()I

    #@a
    move-result v1

    #@b
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    #@e
    .line 4220
    return-void
.end method

.method public finishIteratingOldHistoryLocked()V
    .registers 3

    #@0
    .prologue
    .line 4187
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mIteratingHistory:Z

    #@3
    .line 4188
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@5
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@7
    invoke-virtual {v1}, Landroid/os/Parcel;->dataSize()I

    #@a
    move-result v1

    #@b
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    #@e
    .line 4189
    return-void
.end method

.method public getAwakeTimeBattery()J
    .registers 4

    #@0
    .prologue
    .line 4506
    invoke-virtual {p0}, Lcom/android/internal/os/BatteryStatsImpl;->getBatteryUptimeLocked()J

    #@3
    move-result-wide v0

    #@4
    const/4 v2, 0x2

    #@5
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/internal/os/BatteryStatsImpl;->computeBatteryUptime(JI)J

    #@8
    move-result-wide v0

    #@9
    return-wide v0
.end method

.method public getAwakeTimePlugged()J
    .registers 5

    #@0
    .prologue
    .line 4510
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3
    move-result-wide v0

    #@4
    const-wide/16 v2, 0x3e8

    #@6
    mul-long/2addr v0, v2

    #@7
    invoke-virtual {p0}, Lcom/android/internal/os/BatteryStatsImpl;->getAwakeTimeBattery()J

    #@a
    move-result-wide v2

    #@b
    sub-long/2addr v0, v2

    #@c
    return-wide v0
.end method

.method public getBatteryRealtime(J)J
    .registers 5
    .parameter "curTime"

    #@0
    .prologue
    .line 4592
    invoke-virtual {p0, p1, p2}, Lcom/android/internal/os/BatteryStatsImpl;->getBatteryRealtimeLocked(J)J

    #@3
    move-result-wide v0

    #@4
    return-wide v0
.end method

.method getBatteryRealtimeLocked(J)J
    .registers 7
    .parameter "curTime"

    #@0
    .prologue
    .line 4583
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTrackBatteryPastRealtime:J

    #@2
    .line 4584
    .local v0, time:J
    iget-boolean v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mOnBatteryInternal:Z

    #@4
    if-eqz v2, :cond_b

    #@6
    .line 4585
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTrackBatteryRealtimeStart:J

    #@8
    sub-long v2, p1, v2

    #@a
    add-long/2addr v0, v2

    #@b
    .line 4587
    :cond_b
    return-wide v0
.end method

.method public getBatteryUptime(J)J
    .registers 5
    .parameter "curTime"

    #@0
    .prologue
    .line 4579
    invoke-virtual {p0, p1, p2}, Lcom/android/internal/os/BatteryStatsImpl;->getBatteryUptimeLocked(J)J

    #@3
    move-result-wide v0

    #@4
    return-wide v0
.end method

.method getBatteryUptimeLocked()J
    .registers 5

    #@0
    .prologue
    .line 4574
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3
    move-result-wide v0

    #@4
    const-wide/16 v2, 0x3e8

    #@6
    mul-long/2addr v0, v2

    #@7
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->getBatteryUptime(J)J

    #@a
    move-result-wide v0

    #@b
    return-wide v0
.end method

.method getBatteryUptimeLocked(J)J
    .registers 7
    .parameter "curTime"

    #@0
    .prologue
    .line 4566
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTrackBatteryPastUptime:J

    #@2
    .line 4567
    .local v0, time:J
    iget-boolean v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mOnBatteryInternal:Z

    #@4
    if-eqz v2, :cond_b

    #@6
    .line 4568
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTrackBatteryUptimeStart:J

    #@8
    sub-long v2, p1, v2

    #@a
    add-long/2addr v0, v2

    #@b
    .line 4570
    :cond_b
    return-wide v0
.end method

.method public getBluetoothOnTime(JI)J
    .registers 6
    .parameter "batteryRealtime"
    .parameter "which"

    #@0
    .prologue
    .line 2358
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBluetoothOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->getTotalTimeLocked(JI)J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method public getBluetoothPingCount()I
    .registers 3

    #@0
    .prologue
    .line 1250
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBluetoothPingStart:I

    #@2
    const/4 v1, -0x1

    #@3
    if-ne v0, v1, :cond_8

    #@5
    .line 1251
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBluetoothPingCount:I

    #@7
    .line 1255
    :goto_7
    return v0

    #@8
    .line 1252
    :cond_8
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBtHeadset:Landroid/bluetooth/BluetoothHeadset;

    #@a
    if-eqz v0, :cond_14

    #@c
    .line 1253
    invoke-direct {p0}, Lcom/android/internal/os/BatteryStatsImpl;->getCurrentBluetoothPingCount()I

    #@f
    move-result v0

    #@10
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBluetoothPingStart:I

    #@12
    sub-int/2addr v0, v1

    #@13
    goto :goto_7

    #@14
    .line 1255
    :cond_14
    const/4 v0, 0x0

    #@15
    goto :goto_7
.end method

.method public getCpuSpeedSteps()I
    .registers 2

    #@0
    .prologue
    .line 4726
    sget v0, Lcom/android/internal/os/BatteryStatsImpl;->sNumSpeedSteps:I

    #@2
    return v0
.end method

.method public getDischargeAmountScreenOff()I
    .registers 4

    #@0
    .prologue
    .line 4703
    monitor-enter p0

    #@1
    .line 4704
    :try_start_1
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeAmountScreenOff:I

    #@3
    .line 4705
    .local v0, val:I
    iget-boolean v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mOnBattery:Z

    #@5
    if-eqz v1, :cond_17

    #@7
    iget-boolean v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenOn:Z

    #@9
    if-nez v1, :cond_17

    #@b
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeCurrentLevel:I

    #@d
    iget v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeScreenOffUnplugLevel:I

    #@f
    if-ge v1, v2, :cond_17

    #@11
    .line 4707
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeScreenOffUnplugLevel:I

    #@13
    iget v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeCurrentLevel:I

    #@15
    sub-int/2addr v1, v2

    #@16
    add-int/2addr v0, v1

    #@17
    .line 4709
    :cond_17
    monitor-exit p0

    #@18
    return v0

    #@19
    .line 4710
    .end local v0           #val:I
    :catchall_19
    move-exception v1

    #@1a
    monitor-exit p0
    :try_end_1b
    .catchall {:try_start_1 .. :try_end_1b} :catchall_19

    #@1b
    throw v1
.end method

.method public getDischargeAmountScreenOffSinceCharge()I
    .registers 4

    #@0
    .prologue
    .line 4714
    monitor-enter p0

    #@1
    .line 4715
    :try_start_1
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeAmountScreenOffSinceCharge:I

    #@3
    .line 4716
    .local v0, val:I
    iget-boolean v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mOnBattery:Z

    #@5
    if-eqz v1, :cond_17

    #@7
    iget-boolean v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenOn:Z

    #@9
    if-nez v1, :cond_17

    #@b
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeCurrentLevel:I

    #@d
    iget v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeScreenOffUnplugLevel:I

    #@f
    if-ge v1, v2, :cond_17

    #@11
    .line 4718
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeScreenOffUnplugLevel:I

    #@13
    iget v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeCurrentLevel:I

    #@15
    sub-int/2addr v1, v2

    #@16
    add-int/2addr v0, v1

    #@17
    .line 4720
    :cond_17
    monitor-exit p0

    #@18
    return v0

    #@19
    .line 4721
    .end local v0           #val:I
    :catchall_19
    move-exception v1

    #@1a
    monitor-exit p0
    :try_end_1b
    .catchall {:try_start_1 .. :try_end_1b} :catchall_19

    #@1b
    throw v1
.end method

.method public getDischargeAmountScreenOn()I
    .registers 4

    #@0
    .prologue
    .line 4681
    monitor-enter p0

    #@1
    .line 4682
    :try_start_1
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeAmountScreenOn:I

    #@3
    .line 4683
    .local v0, val:I
    iget-boolean v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mOnBattery:Z

    #@5
    if-eqz v1, :cond_17

    #@7
    iget-boolean v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenOn:Z

    #@9
    if-eqz v1, :cond_17

    #@b
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeCurrentLevel:I

    #@d
    iget v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeScreenOnUnplugLevel:I

    #@f
    if-ge v1, v2, :cond_17

    #@11
    .line 4685
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeScreenOnUnplugLevel:I

    #@13
    iget v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeCurrentLevel:I

    #@15
    sub-int/2addr v1, v2

    #@16
    add-int/2addr v0, v1

    #@17
    .line 4687
    :cond_17
    monitor-exit p0

    #@18
    return v0

    #@19
    .line 4688
    .end local v0           #val:I
    :catchall_19
    move-exception v1

    #@1a
    monitor-exit p0
    :try_end_1b
    .catchall {:try_start_1 .. :try_end_1b} :catchall_19

    #@1b
    throw v1
.end method

.method public getDischargeAmountScreenOnSinceCharge()I
    .registers 4

    #@0
    .prologue
    .line 4692
    monitor-enter p0

    #@1
    .line 4693
    :try_start_1
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeAmountScreenOnSinceCharge:I

    #@3
    .line 4694
    .local v0, val:I
    iget-boolean v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mOnBattery:Z

    #@5
    if-eqz v1, :cond_17

    #@7
    iget-boolean v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenOn:Z

    #@9
    if-eqz v1, :cond_17

    #@b
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeCurrentLevel:I

    #@d
    iget v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeScreenOnUnplugLevel:I

    #@f
    if-ge v1, v2, :cond_17

    #@11
    .line 4696
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeScreenOnUnplugLevel:I

    #@13
    iget v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeCurrentLevel:I

    #@15
    sub-int/2addr v1, v2

    #@16
    add-int/2addr v0, v1

    #@17
    .line 4698
    :cond_17
    monitor-exit p0

    #@18
    return v0

    #@19
    .line 4699
    .end local v0           #val:I
    :catchall_19
    move-exception v1

    #@1a
    monitor-exit p0
    :try_end_1b
    .catchall {:try_start_1 .. :try_end_1b} :catchall_19

    #@1b
    throw v1
.end method

.method public getDischargeCurrentLevel()I
    .registers 2

    #@0
    .prologue
    .line 4649
    monitor-enter p0

    #@1
    .line 4650
    :try_start_1
    invoke-virtual {p0}, Lcom/android/internal/os/BatteryStatsImpl;->getDischargeCurrentLevelLocked()I

    #@4
    move-result v0

    #@5
    monitor-exit p0

    #@6
    return v0

    #@7
    .line 4651
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public getDischargeCurrentLevelLocked()I
    .registers 2

    #@0
    .prologue
    .line 4655
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeCurrentLevel:I

    #@2
    return v0
.end method

.method public getDischargeStartLevel()I
    .registers 2

    #@0
    .prologue
    .line 4638
    monitor-enter p0

    #@1
    .line 4639
    :try_start_1
    invoke-virtual {p0}, Lcom/android/internal/os/BatteryStatsImpl;->getDischargeStartLevelLocked()I

    #@4
    move-result v0

    #@5
    monitor-exit p0

    #@6
    return v0

    #@7
    .line 4640
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public getDischargeStartLevelLocked()I
    .registers 2

    #@0
    .prologue
    .line 4644
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeUnplugLevel:I

    #@2
    return v0
.end method

.method public getGlobalWifiRunningTime(JI)J
    .registers 6
    .parameter "batteryRealtime"
    .parameter "which"

    #@0
    .prologue
    .line 2354
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mGlobalWifiRunningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->getTotalTimeLocked(JI)J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method public getHighDischargeAmountSinceCharge()I
    .registers 4

    #@0
    .prologue
    .line 4671
    monitor-enter p0

    #@1
    .line 4672
    :try_start_1
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHighDischargeAmountSinceCharge:I

    #@3
    .line 4673
    .local v0, val:I
    iget-boolean v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mOnBattery:Z

    #@5
    if-eqz v1, :cond_13

    #@7
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeCurrentLevel:I

    #@9
    iget v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeUnplugLevel:I

    #@b
    if-ge v1, v2, :cond_13

    #@d
    .line 4674
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeUnplugLevel:I

    #@f
    iget v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeCurrentLevel:I

    #@11
    sub-int/2addr v1, v2

    #@12
    add-int/2addr v0, v1

    #@13
    .line 4676
    :cond_13
    monitor-exit p0

    #@14
    return v0

    #@15
    .line 4677
    .end local v0           #val:I
    :catchall_15
    move-exception v1

    #@16
    monitor-exit p0
    :try_end_17
    .catchall {:try_start_1 .. :try_end_17} :catchall_15

    #@17
    throw v1
.end method

.method public getHistoryBaseTime()J
    .registers 3

    #@0
    .prologue
    .line 4224
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBaseTime:J

    #@2
    return-wide v0
.end method

.method public getInputEventCount(I)I
    .registers 3
    .parameter "which"

    #@0
    .prologue
    .line 2316
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mInputEventCounter:Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/os/BatteryStatsImpl$Counter;->getCountLocked(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getIsOnBattery()Z
    .registers 2

    #@0
    .prologue
    .line 2362
    iget-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mOnBattery:Z

    #@2
    return v0
.end method

.method public getKernelWakelockStats()Ljava/util/Map;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "+",
            "Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 309
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mKernelWakelockStats:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method public getKernelWakelockTimerLocked(Ljava/lang/String;)Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;
    .registers 6
    .parameter "name"

    #@0
    .prologue
    .line 1183
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mKernelWakelockStats:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;

    #@8
    .line 1184
    .local v0, kwlt:Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;
    if-nez v0, :cond_19

    #@a
    .line 1185
    new-instance v0, Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;

    #@c
    .end local v0           #kwlt:Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@e
    iget-boolean v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mOnBatteryInternal:Z

    #@10
    const/4 v3, 0x1

    #@11
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;-><init>(Ljava/util/ArrayList;ZZ)V

    #@14
    .line 1187
    .restart local v0       #kwlt:Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mKernelWakelockStats:Ljava/util/HashMap;

    #@16
    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@19
    .line 1189
    :cond_19
    return-object v0
.end method

.method public getLowDischargeAmountSinceCharge()I
    .registers 4

    #@0
    .prologue
    .line 4660
    monitor-enter p0

    #@1
    .line 4661
    :try_start_1
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mLowDischargeAmountSinceCharge:I

    #@3
    .line 4662
    .local v0, val:I
    iget-boolean v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mOnBattery:Z

    #@5
    if-eqz v1, :cond_15

    #@7
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeCurrentLevel:I

    #@9
    iget v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeUnplugLevel:I

    #@b
    if-ge v1, v2, :cond_15

    #@d
    .line 4663
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeUnplugLevel:I

    #@f
    iget v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeCurrentLevel:I

    #@11
    sub-int/2addr v1, v2

    #@12
    add-int/lit8 v1, v1, -0x1

    #@14
    add-int/2addr v0, v1

    #@15
    .line 4665
    :cond_15
    monitor-exit p0

    #@16
    return v0

    #@17
    .line 4666
    .end local v0           #val:I
    :catchall_17
    move-exception v1

    #@18
    monitor-exit p0
    :try_end_19
    .catchall {:try_start_1 .. :try_end_19} :catchall_17

    #@19
    throw v1
.end method

.method public getMobileTcpBytesReceived(I)J
    .registers 7
    .parameter "which"

    #@0
    .prologue
    .line 4620
    invoke-direct {p0}, Lcom/android/internal/os/BatteryStatsImpl;->getNetworkStatsSummary()Landroid/net/NetworkStats;

    #@3
    move-result-object v2

    #@4
    const/4 v3, 0x0

    #@5
    iget-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl;->mMobileIfaces:Ljava/util/HashSet;

    #@7
    invoke-virtual {v2, v3, v4}, Landroid/net/NetworkStats;->getTotal(Landroid/net/NetworkStats$Entry;Ljava/util/HashSet;)Landroid/net/NetworkStats$Entry;

    #@a
    move-result-object v2

    #@b
    iget-wide v0, v2, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@d
    .line 4621
    .local v0, mobileRxBytes:J
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mMobileDataRx:[J

    #@f
    invoke-direct {p0, v0, v1, v2, p1}, Lcom/android/internal/os/BatteryStatsImpl;->getTcpBytes(J[JI)J

    #@12
    move-result-wide v2

    #@13
    return-wide v2
.end method

.method public getMobileTcpBytesSent(I)J
    .registers 7
    .parameter "which"

    #@0
    .prologue
    .line 4614
    invoke-direct {p0}, Lcom/android/internal/os/BatteryStatsImpl;->getNetworkStatsSummary()Landroid/net/NetworkStats;

    #@3
    move-result-object v2

    #@4
    const/4 v3, 0x0

    #@5
    iget-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl;->mMobileIfaces:Ljava/util/HashSet;

    #@7
    invoke-virtual {v2, v3, v4}, Landroid/net/NetworkStats;->getTotal(Landroid/net/NetworkStats$Entry;Ljava/util/HashSet;)Landroid/net/NetworkStats$Entry;

    #@a
    move-result-object v2

    #@b
    iget-wide v0, v2, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@d
    .line 4615
    .local v0, mobileTxBytes:J
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mMobileDataTx:[J

    #@f
    invoke-direct {p0, v0, v1, v2, p1}, Lcom/android/internal/os/BatteryStatsImpl;->getTcpBytes(J[JI)J

    #@12
    move-result-wide v2

    #@13
    return-wide v2
.end method

.method public getNextHistoryLocked(Landroid/os/BatteryStats$HistoryItem;)Z
    .registers 7
    .parameter "out"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 4203
    iget-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@4
    invoke-virtual {v4}, Landroid/os/Parcel;->dataPosition()I

    #@7
    move-result v1

    #@8
    .line 4204
    .local v1, pos:I
    if-nez v1, :cond_d

    #@a
    .line 4205
    invoke-virtual {p1}, Landroid/os/BatteryStats$HistoryItem;->clear()V

    #@d
    .line 4207
    :cond_d
    iget-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@f
    invoke-virtual {v4}, Landroid/os/Parcel;->dataSize()I

    #@12
    move-result v4

    #@13
    if-lt v1, v4, :cond_19

    #@15
    move v0, v3

    #@16
    .line 4208
    .local v0, end:Z
    :goto_16
    if-eqz v0, :cond_1b

    #@18
    .line 4213
    :goto_18
    return v2

    #@19
    .end local v0           #end:Z
    :cond_19
    move v0, v2

    #@1a
    .line 4207
    goto :goto_16

    #@1b
    .line 4212
    .restart local v0       #end:Z
    :cond_1b
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@1d
    invoke-virtual {p1, v2}, Landroid/os/BatteryStats$HistoryItem;->readDelta(Landroid/os/Parcel;)V

    #@20
    move v2, v3

    #@21
    .line 4213
    goto :goto_18
.end method

.method public getNextOldHistoryLocked(Landroid/os/BatteryStats$HistoryItem;)Z
    .registers 13
    .parameter "out"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 4155
    iget-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@4
    invoke-virtual {v5}, Landroid/os/Parcel;->dataPosition()I

    #@7
    move-result v5

    #@8
    iget-object v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@a
    invoke-virtual {v8}, Landroid/os/Parcel;->dataSize()I

    #@d
    move-result v8

    #@e
    if-lt v5, v8, :cond_39

    #@10
    move v1, v6

    #@11
    .line 4156
    .local v1, end:Z
    :goto_11
    if-nez v1, :cond_27

    #@13
    .line 4157
    iget-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryReadTmp:Landroid/os/BatteryStats$HistoryItem;

    #@15
    iget-object v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@17
    invoke-virtual {v5, v8}, Landroid/os/BatteryStats$HistoryItem;->readDelta(Landroid/os/Parcel;)V

    #@1a
    .line 4158
    iget-boolean v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mReadOverflow:Z

    #@1c
    iget-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryReadTmp:Landroid/os/BatteryStats$HistoryItem;

    #@1e
    iget-byte v5, v5, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    #@20
    const/4 v9, 0x3

    #@21
    if-ne v5, v9, :cond_3b

    #@23
    move v5, v6

    #@24
    :goto_24
    or-int/2addr v5, v8

    #@25
    iput-boolean v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mReadOverflow:Z

    #@27
    .line 4160
    :cond_27
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryIterator:Landroid/os/BatteryStats$HistoryItem;

    #@29
    .line 4161
    .local v0, cur:Landroid/os/BatteryStats$HistoryItem;
    if-nez v0, :cond_3d

    #@2b
    .line 4162
    iget-boolean v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mReadOverflow:Z

    #@2d
    if-nez v5, :cond_38

    #@2f
    if-nez v1, :cond_38

    #@31
    .line 4163
    const-string v5, "BatteryStatsImpl"

    #@33
    const-string v6, "Old history ends before new history!"

    #@35
    invoke-static {v5, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 4182
    :cond_38
    :goto_38
    return v7

    #@39
    .end local v0           #cur:Landroid/os/BatteryStats$HistoryItem;
    .end local v1           #end:Z
    :cond_39
    move v1, v7

    #@3a
    .line 4155
    goto :goto_11

    #@3b
    .restart local v1       #end:Z
    :cond_3b
    move v5, v7

    #@3c
    .line 4158
    goto :goto_24

    #@3d
    .line 4167
    .restart local v0       #cur:Landroid/os/BatteryStats$HistoryItem;
    :cond_3d
    invoke-virtual {p1, v0}, Landroid/os/BatteryStats$HistoryItem;->setTo(Landroid/os/BatteryStats$HistoryItem;)V

    #@40
    .line 4168
    iget-object v5, v0, Landroid/os/BatteryStats$HistoryItem;->next:Landroid/os/BatteryStats$HistoryItem;

    #@42
    iput-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryIterator:Landroid/os/BatteryStats$HistoryItem;

    #@44
    .line 4169
    iget-boolean v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mReadOverflow:Z

    #@46
    if-nez v5, :cond_51

    #@48
    .line 4170
    if-eqz v1, :cond_53

    #@4a
    .line 4171
    const-string v5, "BatteryStatsImpl"

    #@4c
    const-string v7, "New history ends before old history!"

    #@4e
    invoke-static {v5, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    :cond_51
    :goto_51
    move v7, v6

    #@52
    .line 4182
    goto :goto_38

    #@53
    .line 4172
    :cond_53
    iget-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryReadTmp:Landroid/os/BatteryStats$HistoryItem;

    #@55
    invoke-virtual {p1, v5}, Landroid/os/BatteryStats$HistoryItem;->same(Landroid/os/BatteryStats$HistoryItem;)Z

    #@58
    move-result v5

    #@59
    if-nez v5, :cond_51

    #@5b
    .line 4173
    invoke-virtual {p0}, Lcom/android/internal/os/BatteryStatsImpl;->getHistoryBaseTime()J

    #@5e
    move-result-wide v7

    #@5f
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@62
    move-result-wide v9

    #@63
    add-long v2, v7, v9

    #@65
    .line 4174
    .local v2, now:J
    new-instance v4, Ljava/io/PrintWriter;

    #@67
    new-instance v5, Landroid/util/LogWriter;

    #@69
    const/4 v7, 0x5

    #@6a
    const-string v8, "BatteryStatsImpl"

    #@6c
    invoke-direct {v5, v7, v8}, Landroid/util/LogWriter;-><init>(ILjava/lang/String;)V

    #@6f
    invoke-direct {v4, v5}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    #@72
    .line 4175
    .local v4, pw:Ljava/io/PrintWriter;
    const-string v5, "Histories differ!"

    #@74
    invoke-virtual {v4, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@77
    .line 4176
    const-string v5, "Old history:"

    #@79
    invoke-virtual {v4, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@7c
    .line 4177
    new-instance v5, Landroid/os/BatteryStats$HistoryPrinter;

    #@7e
    invoke-direct {v5}, Landroid/os/BatteryStats$HistoryPrinter;-><init>()V

    #@81
    invoke-virtual {v5, v4, p1, v2, v3}, Landroid/os/BatteryStats$HistoryPrinter;->printNextItem(Ljava/io/PrintWriter;Landroid/os/BatteryStats$HistoryItem;J)V

    #@84
    .line 4178
    const-string v5, "New history:"

    #@86
    invoke-virtual {v4, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@89
    .line 4179
    new-instance v5, Landroid/os/BatteryStats$HistoryPrinter;

    #@8b
    invoke-direct {v5}, Landroid/os/BatteryStats$HistoryPrinter;-><init>()V

    #@8e
    iget-object v7, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryReadTmp:Landroid/os/BatteryStats$HistoryItem;

    #@90
    invoke-virtual {v5, v4, v7, v2, v3}, Landroid/os/BatteryStats$HistoryPrinter;->printNextItem(Ljava/io/PrintWriter;Landroid/os/BatteryStats$HistoryItem;J)V

    #@93
    goto :goto_51
.end method

.method public getPackageStatsLocked(ILjava/lang/String;)Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;
    .registers 5
    .parameter "uid"
    .parameter "pkg"

    #@0
    .prologue
    .line 4780
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->getUidStatsLocked(I)Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@3
    move-result-object v0

    #@4
    .line 4781
    .local v0, u:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    invoke-virtual {v0, p2}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->getPackageStatsLocked(Ljava/lang/String;)Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;

    #@7
    move-result-object v1

    #@8
    return-object v1
.end method

.method public getPhoneDataConnectionCount(II)I
    .registers 4
    .parameter "dataType"
    .parameter "which"

    #@0
    .prologue
    .line 2346
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneDataConnectionsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2
    aget-object v0, v0, p1

    #@4
    invoke-virtual {v0, p2}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->getCountLocked(I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getPhoneDataConnectionTime(IJI)J
    .registers 7
    .parameter "dataType"
    .parameter "batteryRealtime"
    .parameter "which"

    #@0
    .prologue
    .line 2341
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneDataConnectionsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2
    aget-object v0, v0, p1

    #@4
    invoke-virtual {v0, p2, p3, p4}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->getTotalTimeLocked(JI)J

    #@7
    move-result-wide v0

    #@8
    return-wide v0
.end method

.method public getPhoneOnTime(JI)J
    .registers 6
    .parameter "batteryRealtime"
    .parameter "which"

    #@0
    .prologue
    .line 2320
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->getTotalTimeLocked(JI)J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method public getPhoneSignalScanningTime(JI)J
    .registers 6
    .parameter "batteryRealtime"
    .parameter "which"

    #@0
    .prologue
    .line 2331
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalScanningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->getTotalTimeLocked(JI)J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method public getPhoneSignalStrengthCount(II)I
    .registers 4
    .parameter "strengthBin"
    .parameter "which"

    #@0
    .prologue
    .line 2336
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalStrengthsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2
    aget-object v0, v0, p1

    #@4
    invoke-virtual {v0, p2}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->getCountLocked(I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getPhoneSignalStrengthTime(IJI)J
    .registers 7
    .parameter "strengthBin"
    .parameter "batteryRealtime"
    .parameter "which"

    #@0
    .prologue
    .line 2325
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalStrengthsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2
    aget-object v0, v0, p1

    #@4
    invoke-virtual {v0, p2, p3, p4}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->getTotalTimeLocked(JI)J

    #@7
    move-result-wide v0

    #@8
    return-wide v0
.end method

.method public getProcessStatsLocked(ILjava/lang/String;)Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;
    .registers 5
    .parameter "uid"
    .parameter "name"

    #@0
    .prologue
    .line 4753
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->getUidStatsLocked(I)Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@3
    move-result-object v0

    #@4
    .line 4754
    .local v0, u:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    invoke-virtual {v0, p2}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->getProcessStatsLocked(Ljava/lang/String;)Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;

    #@7
    move-result-object v1

    #@8
    return-object v1
.end method

.method public getProcessStatsLocked(Ljava/lang/String;I)Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;
    .registers 7
    .parameter "name"
    .parameter "pid"

    #@0
    .prologue
    .line 4765
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUidCache:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@5
    move-result v2

    #@6
    if-eqz v2, :cond_1d

    #@8
    .line 4766
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUidCache:Ljava/util/HashMap;

    #@a
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@d
    move-result-object v2

    #@e
    check-cast v2, Ljava/lang/Integer;

    #@10
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@13
    move-result v1

    #@14
    .line 4771
    .local v1, uid:I
    :goto_14
    invoke-virtual {p0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->getUidStatsLocked(I)Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@17
    move-result-object v0

    #@18
    .line 4772
    .local v0, u:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    invoke-virtual {v0, p1}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->getProcessStatsLocked(Ljava/lang/String;)Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;

    #@1b
    move-result-object v2

    #@1c
    return-object v2

    #@1d
    .line 4768
    .end local v0           #u:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    .end local v1           #uid:I
    :cond_1d
    invoke-static {p2}, Landroid/os/Process;->getUidForPid(I)I

    #@20
    move-result v1

    #@21
    .line 4769
    .restart local v1       #uid:I
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUidCache:Ljava/util/HashMap;

    #@23
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v2, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2a
    goto :goto_14
.end method

.method public getProcessWakeTime(IIJ)J
    .registers 13
    .parameter "uid"
    .parameter "pid"
    .parameter "realtime"

    #@0
    .prologue
    const-wide/16 v2, 0x0

    #@2
    .line 1684
    iget-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@4
    invoke-virtual {v4, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@7
    move-result-object v1

    #@8
    check-cast v1, Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@a
    .line 1685
    .local v1, u:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    if-eqz v1, :cond_23

    #@c
    .line 1686
    iget-object v4, v1, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mPids:Landroid/util/SparseArray;

    #@e
    invoke-virtual {v4, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Landroid/os/BatteryStats$Uid$Pid;

    #@14
    .line 1687
    .local v0, p:Landroid/os/BatteryStats$Uid$Pid;
    if-eqz v0, :cond_23

    #@16
    .line 1688
    iget-wide v4, v0, Landroid/os/BatteryStats$Uid$Pid;->mWakeSum:J

    #@18
    iget-wide v6, v0, Landroid/os/BatteryStats$Uid$Pid;->mWakeStart:J

    #@1a
    cmp-long v6, v6, v2

    #@1c
    if-eqz v6, :cond_22

    #@1e
    iget-wide v2, v0, Landroid/os/BatteryStats$Uid$Pid;->mWakeStart:J

    #@20
    sub-long v2, p3, v2

    #@22
    :cond_22
    add-long/2addr v2, v4

    #@23
    .line 1691
    .end local v0           #p:Landroid/os/BatteryStats$Uid$Pid;
    :cond_23
    return-wide v2
.end method

.method public getRadioDataUptime()J
    .registers 5

    #@0
    .prologue
    .line 1232
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mRadioDataStart:J

    #@2
    const-wide/16 v2, -0x1

    #@4
    cmp-long v0, v0, v2

    #@6
    if-nez v0, :cond_b

    #@8
    .line 1233
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mRadioDataUptime:J

    #@a
    .line 1235
    :goto_a
    return-wide v0

    #@b
    :cond_b
    invoke-direct {p0}, Lcom/android/internal/os/BatteryStatsImpl;->getCurrentRadioDataUptime()J

    #@e
    move-result-wide v0

    #@f
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mRadioDataStart:J

    #@11
    sub-long/2addr v0, v2

    #@12
    goto :goto_a
.end method

.method public getRadioDataUptimeMs()J
    .registers 5

    #@0
    .prologue
    .line 1225
    invoke-virtual {p0}, Lcom/android/internal/os/BatteryStatsImpl;->getRadioDataUptime()J

    #@3
    move-result-wide v0

    #@4
    const-wide/16 v2, 0x3e8

    #@6
    div-long/2addr v0, v2

    #@7
    return-wide v0
.end method

.method public getScreenBrightnessTime(IJI)J
    .registers 7
    .parameter "brightnessBin"
    .parameter "batteryRealtime"
    .parameter "which"

    #@0
    .prologue
    .line 2311
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenBrightnessTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2
    aget-object v0, v0, p1

    #@4
    invoke-virtual {v0, p2, p3, p4}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->getTotalTimeLocked(JI)J

    #@7
    move-result-wide v0

    #@8
    return-wide v0
.end method

.method public getScreenOnTime(JI)J
    .registers 6
    .parameter "batteryRealtime"
    .parameter "which"

    #@0
    .prologue
    .line 2306
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->getTotalTimeLocked(JI)J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method public getServiceStatsLocked(ILjava/lang/String;Ljava/lang/String;)Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;
    .registers 6
    .parameter "uid"
    .parameter "pkg"
    .parameter "name"

    #@0
    .prologue
    .line 4789
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->getUidStatsLocked(I)Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@3
    move-result-object v0

    #@4
    .line 4790
    .local v0, u:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    invoke-virtual {v0, p2, p3}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->getServiceStatsLocked(Ljava/lang/String;Ljava/lang/String;)Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;

    #@7
    move-result-object v1

    #@8
    return-object v1
.end method

.method public getStartCount()I
    .registers 2

    #@0
    .prologue
    .line 4229
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mStartCount:I

    #@2
    return v0
.end method

.method public getTotalTcpBytesReceived(I)J
    .registers 6
    .parameter "which"

    #@0
    .prologue
    .line 4632
    invoke-direct {p0}, Lcom/android/internal/os/BatteryStatsImpl;->getNetworkStatsSummary()Landroid/net/NetworkStats;

    #@3
    move-result-object v2

    #@4
    const/4 v3, 0x0

    #@5
    invoke-virtual {v2, v3}, Landroid/net/NetworkStats;->getTotal(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats$Entry;

    #@8
    move-result-object v2

    #@9
    iget-wide v0, v2, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@b
    .line 4633
    .local v0, totalRxBytes:J
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTotalDataRx:[J

    #@d
    invoke-direct {p0, v0, v1, v2, p1}, Lcom/android/internal/os/BatteryStatsImpl;->getTcpBytes(J[JI)J

    #@10
    move-result-wide v2

    #@11
    return-wide v2
.end method

.method public getTotalTcpBytesSent(I)J
    .registers 6
    .parameter "which"

    #@0
    .prologue
    .line 4626
    invoke-direct {p0}, Lcom/android/internal/os/BatteryStatsImpl;->getNetworkStatsSummary()Landroid/net/NetworkStats;

    #@3
    move-result-object v2

    #@4
    const/4 v3, 0x0

    #@5
    invoke-virtual {v2, v3}, Landroid/net/NetworkStats;->getTotal(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats$Entry;

    #@8
    move-result-object v2

    #@9
    iget-wide v0, v2, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@b
    .line 4627
    .local v0, totalTxBytes:J
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTotalDataTx:[J

    #@d
    invoke-direct {p0, v0, v1, v2, p1}, Lcom/android/internal/os/BatteryStatsImpl;->getTcpBytes(J[JI)J

    #@10
    move-result-wide v2

    #@11
    return-wide v2
.end method

.method public getUidStats()Landroid/util/SparseArray;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<+",
            "Landroid/os/BatteryStats$Uid;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 2366
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@2
    return-object v0
.end method

.method public getUidStatsLocked(I)Lcom/android/internal/os/BatteryStatsImpl$Uid;
    .registers 4
    .parameter "uid"

    #@0
    .prologue
    .line 4733
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@8
    .line 4734
    .local v0, u:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    if-nez v0, :cond_14

    #@a
    .line 4735
    new-instance v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@c
    .end local v0           #u:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    invoke-direct {v0, p0, p1}, Lcom/android/internal/os/BatteryStatsImpl$Uid;-><init>(Lcom/android/internal/os/BatteryStatsImpl;I)V

    #@f
    .line 4736
    .restart local v0       #u:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@11
    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@14
    .line 4738
    :cond_14
    return-object v0
.end method

.method public getWifiOnTime(JI)J
    .registers 6
    .parameter "batteryRealtime"
    .parameter "which"

    #@0
    .prologue
    .line 2350
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->getTotalTimeLocked(JI)J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method initDischarge()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 4250
    iput v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mLowDischargeAmountSinceCharge:I

    #@3
    .line 4251
    iput v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHighDischargeAmountSinceCharge:I

    #@5
    .line 4252
    iput v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeAmountScreenOn:I

    #@7
    .line 4253
    iput v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeAmountScreenOnSinceCharge:I

    #@9
    .line 4254
    iput v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeAmountScreenOff:I

    #@b
    .line 4255
    iput v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeAmountScreenOffSinceCharge:I

    #@d
    .line 4256
    return-void
.end method

.method initTimes()V
    .registers 5

    #@0
    .prologue
    const-wide/16 v2, 0x3e8

    #@2
    const-wide/16 v0, 0x0

    #@4
    .line 4241
    iput-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTrackBatteryPastUptime:J

    #@6
    iput-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBatteryRealtime:J

    #@8
    .line 4242
    iput-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTrackBatteryPastRealtime:J

    #@a
    iput-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBatteryUptime:J

    #@c
    .line 4243
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@f
    move-result-wide v0

    #@10
    mul-long/2addr v0, v2

    #@11
    iput-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTrackBatteryUptimeStart:J

    #@13
    iput-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUptimeStart:J

    #@15
    .line 4244
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@18
    move-result-wide v0

    #@19
    mul-long/2addr v0, v2

    #@1a
    iput-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTrackBatteryRealtimeStart:J

    #@1c
    iput-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mRealtimeStart:J

    #@1e
    .line 4245
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUptimeStart:J

    #@20
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->getBatteryUptimeLocked(J)J

    #@23
    move-result-wide v0

    #@24
    iput-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggedBatteryUptime:J

    #@26
    .line 4246
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mRealtimeStart:J

    #@28
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->getBatteryRealtimeLocked(J)J

    #@2b
    move-result-wide v0

    #@2c
    iput-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggedBatteryRealtime:J

    #@2e
    .line 4247
    return-void
.end method

.method public isOnBattery()Z
    .registers 2

    #@0
    .prologue
    .line 4233
    iget-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mOnBattery:Z

    #@2
    return v0
.end method

.method public isScreenOn()Z
    .registers 2

    #@0
    .prologue
    .line 4237
    iget-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenOn:Z

    #@2
    return v0
.end method

.method public noteAudioOffLocked(I)V
    .registers 5
    .parameter "uid"

    #@0
    .prologue
    .line 2077
    iget-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mAudioOn:Z

    #@2
    if-eqz v0, :cond_1d

    #@4
    .line 2078
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@6
    iget v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@8
    const v2, -0x400001

    #@b
    and-int/2addr v1, v2

    #@c
    iput v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@e
    .line 2081
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@11
    move-result-wide v0

    #@12
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@15
    .line 2082
    const/4 v0, 0x0

    #@16
    iput-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mAudioOn:Z

    #@18
    .line 2083
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mAudioOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@1a
    invoke-virtual {v0, p0}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->stopRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@1d
    .line 2085
    :cond_1d
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->getUidStatsLocked(I)Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->noteAudioTurnedOffLocked()V

    #@24
    .line 2086
    return-void
.end method

.method public noteAudioOnLocked(I)V
    .registers 5
    .parameter "uid"

    #@0
    .prologue
    .line 2065
    iget-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mAudioOn:Z

    #@2
    if-nez v0, :cond_1c

    #@4
    .line 2066
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@6
    iget v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@8
    const/high16 v2, 0x40

    #@a
    or-int/2addr v1, v2

    #@b
    iput v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@d
    .line 2069
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@10
    move-result-wide v0

    #@11
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@14
    .line 2070
    const/4 v0, 0x1

    #@15
    iput-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mAudioOn:Z

    #@17
    .line 2071
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mAudioOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@19
    invoke-virtual {v0, p0}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@1c
    .line 2073
    :cond_1c
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->getUidStatsLocked(I)Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@1f
    move-result-object v0

    #@20
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->noteAudioTurnedOnLocked()V

    #@23
    .line 2074
    return-void
.end method

.method public noteBluetoothOffLocked()V
    .registers 4

    #@0
    .prologue
    .line 2173
    iget-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBluetoothOn:Z

    #@2
    if-eqz v0, :cond_1d

    #@4
    .line 2174
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@6
    iget v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@8
    const v2, -0x10001

    #@b
    and-int/2addr v1, v2

    #@c
    iput v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@e
    .line 2177
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@11
    move-result-wide v0

    #@12
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@15
    .line 2178
    const/4 v0, 0x0

    #@16
    iput-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBluetoothOn:Z

    #@18
    .line 2179
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBluetoothOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@1a
    invoke-virtual {v0, p0}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->stopRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@1d
    .line 2181
    :cond_1d
    return-void
.end method

.method public noteBluetoothOnLocked()V
    .registers 4

    #@0
    .prologue
    .line 2162
    iget-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBluetoothOn:Z

    #@2
    if-nez v0, :cond_1c

    #@4
    .line 2163
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@6
    iget v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@8
    const/high16 v2, 0x1

    #@a
    or-int/2addr v1, v2

    #@b
    iput v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@d
    .line 2166
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@10
    move-result-wide v0

    #@11
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@14
    .line 2167
    const/4 v0, 0x1

    #@15
    iput-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBluetoothOn:Z

    #@17
    .line 2168
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBluetoothOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@19
    invoke-virtual {v0, p0}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@1c
    .line 2170
    :cond_1c
    return-void
.end method

.method public noteFullWifiLockAcquiredFromSourceLocked(Landroid/os/WorkSource;)V
    .registers 5
    .parameter "ws"

    #@0
    .prologue
    .line 2256
    invoke-virtual {p1}, Landroid/os/WorkSource;->size()I

    #@3
    move-result v0

    #@4
    .line 2257
    .local v0, N:I
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    if-ge v1, v0, :cond_11

    #@7
    .line 2258
    invoke-virtual {p1, v1}, Landroid/os/WorkSource;->get(I)I

    #@a
    move-result v2

    #@b
    invoke-virtual {p0, v2}, Lcom/android/internal/os/BatteryStatsImpl;->noteFullWifiLockAcquiredLocked(I)V

    #@e
    .line 2257
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_5

    #@11
    .line 2260
    :cond_11
    return-void
.end method

.method public noteFullWifiLockAcquiredLocked(I)V
    .registers 5
    .parameter "uid"

    #@0
    .prologue
    .line 2186
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiFullLockNesting:I

    #@2
    if-nez v0, :cond_14

    #@4
    .line 2187
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@6
    iget v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@8
    const/high16 v2, 0x200

    #@a
    or-int/2addr v1, v2

    #@b
    iput v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@d
    .line 2190
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@10
    move-result-wide v0

    #@11
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@14
    .line 2192
    :cond_14
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiFullLockNesting:I

    #@16
    add-int/lit8 v0, v0, 0x1

    #@18
    iput v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiFullLockNesting:I

    #@1a
    .line 2193
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->getUidStatsLocked(I)Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->noteFullWifiLockAcquiredLocked()V

    #@21
    .line 2194
    return-void
.end method

.method public noteFullWifiLockReleasedFromSourceLocked(Landroid/os/WorkSource;)V
    .registers 5
    .parameter "ws"

    #@0
    .prologue
    .line 2263
    invoke-virtual {p1}, Landroid/os/WorkSource;->size()I

    #@3
    move-result v0

    #@4
    .line 2264
    .local v0, N:I
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    if-ge v1, v0, :cond_11

    #@7
    .line 2265
    invoke-virtual {p1, v1}, Landroid/os/WorkSource;->get(I)I

    #@a
    move-result v2

    #@b
    invoke-virtual {p0, v2}, Lcom/android/internal/os/BatteryStatsImpl;->noteFullWifiLockReleasedLocked(I)V

    #@e
    .line 2264
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_5

    #@11
    .line 2267
    :cond_11
    return-void
.end method

.method public noteFullWifiLockReleasedLocked(I)V
    .registers 5
    .parameter "uid"

    #@0
    .prologue
    .line 2197
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiFullLockNesting:I

    #@2
    add-int/lit8 v0, v0, -0x1

    #@4
    iput v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiFullLockNesting:I

    #@6
    .line 2198
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiFullLockNesting:I

    #@8
    if-nez v0, :cond_1b

    #@a
    .line 2199
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@c
    iget v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@e
    const v2, -0x2000001

    #@11
    and-int/2addr v1, v2

    #@12
    iput v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@14
    .line 2202
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@17
    move-result-wide v0

    #@18
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@1b
    .line 2204
    :cond_1b
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->getUidStatsLocked(I)Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->noteFullWifiLockReleasedLocked()V

    #@22
    .line 2205
    return-void
.end method

.method public noteInputEventAtomic()V
    .registers 2

    #@0
    .prologue
    .line 1822
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mInputEventCounter:Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl$Counter;->stepAtomic()V

    #@5
    .line 1823
    return-void
.end method

.method public noteNetworkInterfaceTypeLocked(Ljava/lang/String;I)V
    .registers 4
    .parameter "iface"
    .parameter "networkType"

    #@0
    .prologue
    .line 2298
    invoke-static {p2}, Landroid/net/ConnectivityManager;->isNetworkTypeMobile(I)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_c

    #@6
    .line 2299
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mMobileIfaces:Ljava/util/HashSet;

    #@8
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@b
    .line 2303
    :goto_b
    return-void

    #@c
    .line 2301
    :cond_c
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mMobileIfaces:Ljava/util/HashSet;

    #@e
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@11
    goto :goto_b
.end method

.method public notePhoneDataConnectionStateLocked(IZ)V
    .registers 7
    .parameter "dataType"
    .parameter "hasData"

    #@0
    .prologue
    .line 1973
    const/4 v0, 0x0

    #@1
    .line 1974
    .local v0, bin:I
    if-eqz p2, :cond_8

    #@3
    .line 1975
    packed-switch p1, :pswitch_data_5c

    #@6
    .line 2019
    const/16 v0, 0xf

    #@8
    .line 2024
    :cond_8
    :goto_8
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneDataConnectionType:I

    #@a
    if-eq v1, v0, :cond_38

    #@c
    .line 2025
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@e
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@10
    iget v2, v2, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@12
    const v3, -0xf001

    #@15
    and-int/2addr v2, v3

    #@16
    shl-int/lit8 v3, v0, 0xc

    #@18
    or-int/2addr v2, v3

    #@19
    iput v2, v1, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@1b
    .line 2029
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@1e
    move-result-wide v1

    #@1f
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@22
    .line 2030
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneDataConnectionType:I

    #@24
    if-ltz v1, :cond_2f

    #@26
    .line 2031
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneDataConnectionsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@28
    iget v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneDataConnectionType:I

    #@2a
    aget-object v1, v1, v2

    #@2c
    invoke-virtual {v1, p0}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->stopRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@2f
    .line 2033
    :cond_2f
    iput v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneDataConnectionType:I

    #@31
    .line 2034
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneDataConnectionsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@33
    aget-object v1, v1, v0

    #@35
    invoke-virtual {v1, p0}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@38
    .line 2036
    :cond_38
    return-void

    #@39
    .line 1977
    :pswitch_39
    const/4 v0, 0x2

    #@3a
    .line 1978
    goto :goto_8

    #@3b
    .line 1980
    :pswitch_3b
    const/4 v0, 0x1

    #@3c
    .line 1981
    goto :goto_8

    #@3d
    .line 1983
    :pswitch_3d
    const/4 v0, 0x3

    #@3e
    .line 1984
    goto :goto_8

    #@3f
    .line 1986
    :pswitch_3f
    const/4 v0, 0x4

    #@40
    .line 1987
    goto :goto_8

    #@41
    .line 1989
    :pswitch_41
    const/4 v0, 0x5

    #@42
    .line 1990
    goto :goto_8

    #@43
    .line 1992
    :pswitch_43
    const/4 v0, 0x6

    #@44
    .line 1993
    goto :goto_8

    #@45
    .line 1995
    :pswitch_45
    const/4 v0, 0x7

    #@46
    .line 1996
    goto :goto_8

    #@47
    .line 1998
    :pswitch_47
    const/16 v0, 0x8

    #@49
    .line 1999
    goto :goto_8

    #@4a
    .line 2001
    :pswitch_4a
    const/16 v0, 0x9

    #@4c
    .line 2002
    goto :goto_8

    #@4d
    .line 2004
    :pswitch_4d
    const/16 v0, 0xa

    #@4f
    .line 2005
    goto :goto_8

    #@50
    .line 2007
    :pswitch_50
    const/16 v0, 0xb

    #@52
    .line 2008
    goto :goto_8

    #@53
    .line 2010
    :pswitch_53
    const/16 v0, 0xc

    #@55
    .line 2011
    goto :goto_8

    #@56
    .line 2013
    :pswitch_56
    const/16 v0, 0xd

    #@58
    .line 2014
    goto :goto_8

    #@59
    .line 2016
    :pswitch_59
    const/16 v0, 0xe

    #@5b
    .line 2017
    goto :goto_8

    #@5c
    .line 1975
    :pswitch_data_5c
    .packed-switch 0x1
        :pswitch_3b
        :pswitch_39
        :pswitch_3d
        :pswitch_3f
        :pswitch_41
        :pswitch_43
        :pswitch_45
        :pswitch_47
        :pswitch_4a
        :pswitch_4d
        :pswitch_50
        :pswitch_53
        :pswitch_56
        :pswitch_59
    .end packed-switch
.end method

.method public notePhoneOffLocked()V
    .registers 4

    #@0
    .prologue
    .line 1841
    iget-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneOn:Z

    #@2
    if-eqz v0, :cond_1d

    #@4
    .line 1842
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@6
    iget v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@8
    const v2, -0x40001

    #@b
    and-int/2addr v1, v2

    #@c
    iput v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@e
    .line 1845
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@11
    move-result-wide v0

    #@12
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@15
    .line 1846
    const/4 v0, 0x0

    #@16
    iput-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneOn:Z

    #@18
    .line 1847
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@1a
    invoke-virtual {v0, p0}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->stopRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@1d
    .line 1849
    :cond_1d
    return-void
.end method

.method public notePhoneOnLocked()V
    .registers 4

    #@0
    .prologue
    .line 1830
    iget-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneOn:Z

    #@2
    if-nez v0, :cond_1c

    #@4
    .line 1831
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@6
    iget v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@8
    const/high16 v2, 0x4

    #@a
    or-int/2addr v1, v2

    #@b
    iput v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@d
    .line 1834
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@10
    move-result-wide v0

    #@11
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@14
    .line 1835
    const/4 v0, 0x1

    #@15
    iput-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneOn:Z

    #@17
    .line 1836
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@19
    invoke-virtual {v0, p0}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@1c
    .line 1838
    :cond_1c
    return-void
.end method

.method public notePhoneSignalStrengthLocked(Landroid/telephony/SignalStrength;)V
    .registers 5
    .parameter "signalStrength"

    #@0
    .prologue
    .line 1968
    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getLevel()I

    #@3
    move-result v0

    #@4
    .line 1969
    .local v0, bin:I
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneServiceStateRaw:I

    #@6
    iget v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSimStateRaw:I

    #@8
    invoke-direct {p0, v1, v2, v0}, Lcom/android/internal/os/BatteryStatsImpl;->updateAllPhoneStateLocked(III)V

    #@b
    .line 1970
    return-void
.end method

.method public notePhoneStateLocked(II)V
    .registers 4
    .parameter "state"
    .parameter "simState"

    #@0
    .prologue
    .line 1963
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalStrengthBinRaw:I

    #@2
    invoke-direct {p0, p1, p2, v0}, Lcom/android/internal/os/BatteryStatsImpl;->updateAllPhoneStateLocked(III)V

    #@5
    .line 1964
    return-void
.end method

.method public noteProcessDiedLocked(II)V
    .registers 5
    .parameter "uid"
    .parameter "pid"

    #@0
    .prologue
    .line 1677
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@8
    .line 1678
    .local v0, u:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    if-eqz v0, :cond_f

    #@a
    .line 1679
    iget-object v1, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mPids:Landroid/util/SparseArray;

    #@c
    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->remove(I)V

    #@f
    .line 1681
    :cond_f
    return-void
.end method

.method public noteScreenBrightnessLocked(I)V
    .registers 6
    .parameter "brightness"

    #@0
    .prologue
    .line 1802
    div-int/lit8 v0, p1, 0x33

    #@2
    .line 1803
    .local v0, bin:I
    if-gez v0, :cond_38

    #@4
    const/4 v0, 0x0

    #@5
    .line 1805
    :cond_5
    :goto_5
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenBrightnessBin:I

    #@7
    if-eq v1, v0, :cond_37

    #@9
    .line 1806
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@b
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@d
    iget v2, v2, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@f
    and-int/lit8 v2, v2, -0x10

    #@11
    shl-int/lit8 v3, v0, 0x0

    #@13
    or-int/2addr v2, v3

    #@14
    iput v2, v1, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@16
    .line 1810
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@19
    move-result-wide v1

    #@1a
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@1d
    .line 1811
    iget-boolean v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenOn:Z

    #@1f
    if-eqz v1, :cond_35

    #@21
    .line 1812
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenBrightnessBin:I

    #@23
    if-ltz v1, :cond_2e

    #@25
    .line 1813
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenBrightnessTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@27
    iget v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenBrightnessBin:I

    #@29
    aget-object v1, v1, v2

    #@2b
    invoke-virtual {v1, p0}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->stopRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@2e
    .line 1815
    :cond_2e
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenBrightnessTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@30
    aget-object v1, v1, v0

    #@32
    invoke-virtual {v1, p0}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@35
    .line 1817
    :cond_35
    iput v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenBrightnessBin:I

    #@37
    .line 1819
    :cond_37
    return-void

    #@38
    .line 1804
    :cond_38
    const/4 v1, 0x5

    #@39
    if-lt v0, v1, :cond_5

    #@3b
    const/4 v0, 0x4

    #@3c
    goto :goto_5
.end method

.method public noteScreenOffLocked()V
    .registers 6

    #@0
    .prologue
    const/4 v4, -0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 1780
    iget-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenOn:Z

    #@4
    if-eqz v0, :cond_38

    #@6
    .line 1781
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@8
    iget v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@a
    const v2, -0x100001

    #@d
    and-int/2addr v1, v2

    #@e
    iput v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@10
    .line 1784
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@13
    move-result-wide v0

    #@14
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@17
    .line 1785
    iput-boolean v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenOn:Z

    #@19
    .line 1786
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@1b
    invoke-virtual {v0, p0}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->stopRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@1e
    .line 1787
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenBrightnessBin:I

    #@20
    if-ltz v0, :cond_2b

    #@22
    .line 1788
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenBrightnessTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@24
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenBrightnessBin:I

    #@26
    aget-object v0, v0, v1

    #@28
    invoke-virtual {v0, p0}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->stopRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@2b
    .line 1791
    :cond_2b
    const-string v0, "dummy"

    #@2d
    invoke-virtual {p0, v4, v4, v0, v3}, Lcom/android/internal/os/BatteryStatsImpl;->noteStopWakeLocked(IILjava/lang/String;I)V

    #@30
    .line 1794
    iget-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mOnBatteryInternal:Z

    #@32
    if-eqz v0, :cond_38

    #@34
    .line 1795
    const/4 v0, 0x1

    #@35
    invoke-virtual {p0, v0, v3}, Lcom/android/internal/os/BatteryStatsImpl;->updateDischargeScreenLevelsLocked(ZZ)V

    #@38
    .line 1798
    :cond_38
    return-void
.end method

.method public noteScreenOnLocked()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    const/4 v3, -0x1

    #@3
    .line 1757
    iget-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenOn:Z

    #@5
    if-nez v0, :cond_37

    #@7
    .line 1758
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@9
    iget v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@b
    const/high16 v2, 0x10

    #@d
    or-int/2addr v1, v2

    #@e
    iput v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@10
    .line 1761
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@13
    move-result-wide v0

    #@14
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@17
    .line 1762
    iput-boolean v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenOn:Z

    #@19
    .line 1763
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@1b
    invoke-virtual {v0, p0}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@1e
    .line 1764
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenBrightnessBin:I

    #@20
    if-ltz v0, :cond_2b

    #@22
    .line 1765
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenBrightnessTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@24
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenBrightnessBin:I

    #@26
    aget-object v0, v0, v1

    #@28
    invoke-virtual {v0, p0}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@2b
    .line 1770
    :cond_2b
    const-string v0, "dummy"

    #@2d
    invoke-virtual {p0, v3, v3, v0, v4}, Lcom/android/internal/os/BatteryStatsImpl;->noteStartWakeLocked(IILjava/lang/String;I)V

    #@30
    .line 1773
    iget-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mOnBatteryInternal:Z

    #@32
    if-eqz v0, :cond_37

    #@34
    .line 1774
    invoke-virtual {p0, v4, v5}, Lcom/android/internal/os/BatteryStatsImpl;->updateDischargeScreenLevelsLocked(ZZ)V

    #@37
    .line 1777
    :cond_37
    return-void
.end method

.method public noteStartGpsLocked(I)V
    .registers 5
    .parameter "uid"

    #@0
    .prologue
    .line 1735
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mGpsNesting:I

    #@2
    if-nez v0, :cond_14

    #@4
    .line 1736
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@6
    iget v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@8
    const/high16 v2, 0x1000

    #@a
    or-int/2addr v1, v2

    #@b
    iput v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@d
    .line 1739
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@10
    move-result-wide v0

    #@11
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@14
    .line 1741
    :cond_14
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mGpsNesting:I

    #@16
    add-int/lit8 v0, v0, 0x1

    #@18
    iput v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mGpsNesting:I

    #@1a
    .line 1742
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->getUidStatsLocked(I)Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->noteStartGps()V

    #@21
    .line 1743
    return-void
.end method

.method public noteStartSensorLocked(II)V
    .registers 6
    .parameter "uid"
    .parameter "sensor"

    #@0
    .prologue
    .line 1711
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mSensorNesting:I

    #@2
    if-nez v0, :cond_14

    #@4
    .line 1712
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@6
    iget v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@8
    const/high16 v2, 0x2000

    #@a
    or-int/2addr v1, v2

    #@b
    iput v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@d
    .line 1715
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@10
    move-result-wide v0

    #@11
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@14
    .line 1717
    :cond_14
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mSensorNesting:I

    #@16
    add-int/lit8 v0, v0, 0x1

    #@18
    iput v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mSensorNesting:I

    #@1a
    .line 1718
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->getUidStatsLocked(I)Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0, p2}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->noteStartSensor(I)V

    #@21
    .line 1719
    return-void
.end method

.method public noteStartWakeFromSourceLocked(Landroid/os/WorkSource;ILjava/lang/String;I)V
    .registers 8
    .parameter "ws"
    .parameter "pid"
    .parameter "name"
    .parameter "type"

    #@0
    .prologue
    .line 1565
    invoke-virtual {p1}, Landroid/os/WorkSource;->size()I

    #@3
    move-result v0

    #@4
    .line 1566
    .local v0, N:I
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    if-ge v1, v0, :cond_11

    #@7
    .line 1567
    invoke-virtual {p1, v1}, Landroid/os/WorkSource;->get(I)I

    #@a
    move-result v2

    #@b
    invoke-virtual {p0, v2, p2, p3, p4}, Lcom/android/internal/os/BatteryStatsImpl;->noteStartWakeLocked(IILjava/lang/String;I)V

    #@e
    .line 1566
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_5

    #@11
    .line 1569
    :cond_11
    return-void
.end method

.method public noteStartWakeLocked(IILjava/lang/String;I)V
    .registers 10
    .parameter "uid"
    .parameter "pid"
    .parameter "name"
    .parameter "type"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 1525
    if-nez p4, :cond_1d

    #@3
    .line 1528
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWakeLockNesting:I

    #@5
    if-nez v1, :cond_17

    #@7
    .line 1529
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@9
    iget v2, v1, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@b
    const/high16 v3, 0x4000

    #@d
    or-int/2addr v2, v3

    #@e
    iput v2, v1, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@10
    .line 1532
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@13
    move-result-wide v1

    #@14
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@17
    .line 1534
    :cond_17
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWakeLockNesting:I

    #@19
    add-int/lit8 v1, v1, 0x1

    #@1b
    iput v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWakeLockNesting:I

    #@1d
    .line 1536
    :cond_1d
    if-ltz p1, :cond_3b

    #@1f
    .line 1537
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHandler:Lcom/android/internal/os/BatteryStatsImpl$MyHandler;

    #@21
    invoke-virtual {v1, v4}, Lcom/android/internal/os/BatteryStatsImpl$MyHandler;->hasMessages(I)Z

    #@24
    move-result v1

    #@25
    if-nez v1, :cond_34

    #@27
    .line 1538
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHandler:Lcom/android/internal/os/BatteryStatsImpl$MyHandler;

    #@29
    invoke-virtual {v1, v4}, Lcom/android/internal/os/BatteryStatsImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;

    #@2c
    move-result-object v0

    #@2d
    .line 1539
    .local v0, m:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHandler:Lcom/android/internal/os/BatteryStatsImpl$MyHandler;

    #@2f
    const-wide/16 v2, 0x1388

    #@31
    invoke-virtual {v1, v0, v2, v3}, Lcom/android/internal/os/BatteryStatsImpl$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@34
    .line 1541
    .end local v0           #m:Landroid/os/Message;
    :cond_34
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->getUidStatsLocked(I)Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@37
    move-result-object v1

    #@38
    invoke-virtual {v1, p2, p3, p4}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->noteStartWakeLocked(ILjava/lang/String;I)V

    #@3b
    .line 1543
    :cond_3b
    return-void
.end method

.method public noteStopGpsLocked(I)V
    .registers 5
    .parameter "uid"

    #@0
    .prologue
    .line 1746
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mGpsNesting:I

    #@2
    add-int/lit8 v0, v0, -0x1

    #@4
    iput v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mGpsNesting:I

    #@6
    .line 1747
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mGpsNesting:I

    #@8
    if-nez v0, :cond_1b

    #@a
    .line 1748
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@c
    iget v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@e
    const v2, -0x10000001

    #@11
    and-int/2addr v1, v2

    #@12
    iput v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@14
    .line 1751
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@17
    move-result-wide v0

    #@18
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@1b
    .line 1753
    :cond_1b
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->getUidStatsLocked(I)Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->noteStopGps()V

    #@22
    .line 1754
    return-void
.end method

.method public noteStopSensorLocked(II)V
    .registers 6
    .parameter "uid"
    .parameter "sensor"

    #@0
    .prologue
    .line 1722
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mSensorNesting:I

    #@2
    add-int/lit8 v0, v0, -0x1

    #@4
    iput v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mSensorNesting:I

    #@6
    .line 1723
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mSensorNesting:I

    #@8
    if-nez v0, :cond_1b

    #@a
    .line 1724
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@c
    iget v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@e
    const v2, -0x20000001

    #@11
    and-int/2addr v1, v2

    #@12
    iput v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@14
    .line 1727
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@17
    move-result-wide v0

    #@18
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@1b
    .line 1729
    :cond_1b
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->getUidStatsLocked(I)Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {v0, p2}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->noteStopSensor(I)V

    #@22
    .line 1730
    return-void
.end method

.method public noteStopWakeFromSourceLocked(Landroid/os/WorkSource;ILjava/lang/String;I)V
    .registers 8
    .parameter "ws"
    .parameter "pid"
    .parameter "name"
    .parameter "type"

    #@0
    .prologue
    .line 1572
    invoke-virtual {p1}, Landroid/os/WorkSource;->size()I

    #@3
    move-result v0

    #@4
    .line 1573
    .local v0, N:I
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    if-ge v1, v0, :cond_11

    #@7
    .line 1574
    invoke-virtual {p1, v1}, Landroid/os/WorkSource;->get(I)I

    #@a
    move-result v2

    #@b
    invoke-virtual {p0, v2, p2, p3, p4}, Lcom/android/internal/os/BatteryStatsImpl;->noteStopWakeLocked(IILjava/lang/String;I)V

    #@e
    .line 1573
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_5

    #@11
    .line 1576
    :cond_11
    return-void
.end method

.method public noteStopWakeLocked(IILjava/lang/String;I)V
    .registers 10
    .parameter "uid"
    .parameter "pid"
    .parameter "name"
    .parameter "type"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 1546
    if-nez p4, :cond_1e

    #@3
    .line 1547
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWakeLockNesting:I

    #@5
    add-int/lit8 v1, v1, -0x1

    #@7
    iput v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWakeLockNesting:I

    #@9
    .line 1548
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWakeLockNesting:I

    #@b
    if-nez v1, :cond_1e

    #@d
    .line 1549
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@f
    iget v2, v1, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@11
    const v3, -0x40000001

    #@14
    and-int/2addr v2, v3

    #@15
    iput v2, v1, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@17
    .line 1552
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@1a
    move-result-wide v1

    #@1b
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@1e
    .line 1555
    :cond_1e
    if-ltz p1, :cond_3c

    #@20
    .line 1556
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHandler:Lcom/android/internal/os/BatteryStatsImpl$MyHandler;

    #@22
    invoke-virtual {v1, v4}, Lcom/android/internal/os/BatteryStatsImpl$MyHandler;->hasMessages(I)Z

    #@25
    move-result v1

    #@26
    if-nez v1, :cond_35

    #@28
    .line 1557
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHandler:Lcom/android/internal/os/BatteryStatsImpl$MyHandler;

    #@2a
    invoke-virtual {v1, v4}, Lcom/android/internal/os/BatteryStatsImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;

    #@2d
    move-result-object v0

    #@2e
    .line 1558
    .local v0, m:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHandler:Lcom/android/internal/os/BatteryStatsImpl$MyHandler;

    #@30
    const-wide/16 v2, 0x1388

    #@32
    invoke-virtual {v1, v0, v2, v3}, Lcom/android/internal/os/BatteryStatsImpl$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@35
    .line 1560
    .end local v0           #m:Landroid/os/Message;
    :cond_35
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->getUidStatsLocked(I)Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@38
    move-result-object v1

    #@39
    invoke-virtual {v1, p2, p3, p4}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->noteStopWakeLocked(ILjava/lang/String;I)V

    #@3c
    .line 1562
    :cond_3c
    return-void
.end method

.method public noteUserActivityLocked(II)V
    .registers 4
    .parameter "uid"
    .parameter "event"

    #@0
    .prologue
    .line 1826
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->getUidStatsLocked(I)Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p2}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->noteUserActivityLocked(I)V

    #@7
    .line 1827
    return-void
.end method

.method public noteVideoOffLocked(I)V
    .registers 5
    .parameter "uid"

    #@0
    .prologue
    .line 2101
    iget-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mVideoOn:Z

    #@2
    if-eqz v0, :cond_1d

    #@4
    .line 2102
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@6
    iget v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@8
    const v2, -0x200001

    #@b
    and-int/2addr v1, v2

    #@c
    iput v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@e
    .line 2105
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@11
    move-result-wide v0

    #@12
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@15
    .line 2106
    const/4 v0, 0x0

    #@16
    iput-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mVideoOn:Z

    #@18
    .line 2107
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mVideoOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@1a
    invoke-virtual {v0, p0}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->stopRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@1d
    .line 2109
    :cond_1d
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->getUidStatsLocked(I)Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->noteVideoTurnedOffLocked()V

    #@24
    .line 2110
    return-void
.end method

.method public noteVideoOnLocked(I)V
    .registers 5
    .parameter "uid"

    #@0
    .prologue
    .line 2089
    iget-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mVideoOn:Z

    #@2
    if-nez v0, :cond_1c

    #@4
    .line 2090
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@6
    iget v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@8
    const/high16 v2, 0x20

    #@a
    or-int/2addr v1, v2

    #@b
    iput v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@d
    .line 2093
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@10
    move-result-wide v0

    #@11
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@14
    .line 2094
    const/4 v0, 0x1

    #@15
    iput-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mVideoOn:Z

    #@17
    .line 2095
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mVideoOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@19
    invoke-virtual {v0, p0}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@1c
    .line 2097
    :cond_1c
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->getUidStatsLocked(I)Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@1f
    move-result-object v0

    #@20
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->noteVideoTurnedOnLocked()V

    #@23
    .line 2098
    return-void
.end method

.method public noteWifiMulticastDisabledFromSourceLocked(Landroid/os/WorkSource;)V
    .registers 5
    .parameter "ws"

    #@0
    .prologue
    .line 2291
    invoke-virtual {p1}, Landroid/os/WorkSource;->size()I

    #@3
    move-result v0

    #@4
    .line 2292
    .local v0, N:I
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    if-ge v1, v0, :cond_11

    #@7
    .line 2293
    invoke-virtual {p1, v1}, Landroid/os/WorkSource;->get(I)I

    #@a
    move-result v2

    #@b
    invoke-virtual {p0, v2}, Lcom/android/internal/os/BatteryStatsImpl;->noteWifiMulticastDisabledLocked(I)V

    #@e
    .line 2292
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_5

    #@11
    .line 2295
    :cond_11
    return-void
.end method

.method public noteWifiMulticastDisabledLocked(I)V
    .registers 5
    .parameter "uid"

    #@0
    .prologue
    .line 2245
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiMulticastNesting:I

    #@2
    add-int/lit8 v0, v0, -0x1

    #@4
    iput v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiMulticastNesting:I

    #@6
    .line 2246
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiMulticastNesting:I

    #@8
    if-nez v0, :cond_1b

    #@a
    .line 2247
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@c
    iget v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@e
    const v2, -0x800001

    #@11
    and-int/2addr v1, v2

    #@12
    iput v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@14
    .line 2250
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@17
    move-result-wide v0

    #@18
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@1b
    .line 2252
    :cond_1b
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->getUidStatsLocked(I)Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->noteWifiMulticastDisabledLocked()V

    #@22
    .line 2253
    return-void
.end method

.method public noteWifiMulticastEnabledFromSourceLocked(Landroid/os/WorkSource;)V
    .registers 5
    .parameter "ws"

    #@0
    .prologue
    .line 2284
    invoke-virtual {p1}, Landroid/os/WorkSource;->size()I

    #@3
    move-result v0

    #@4
    .line 2285
    .local v0, N:I
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    if-ge v1, v0, :cond_11

    #@7
    .line 2286
    invoke-virtual {p1, v1}, Landroid/os/WorkSource;->get(I)I

    #@a
    move-result v2

    #@b
    invoke-virtual {p0, v2}, Lcom/android/internal/os/BatteryStatsImpl;->noteWifiMulticastEnabledLocked(I)V

    #@e
    .line 2285
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_5

    #@11
    .line 2288
    :cond_11
    return-void
.end method

.method public noteWifiMulticastEnabledLocked(I)V
    .registers 5
    .parameter "uid"

    #@0
    .prologue
    .line 2234
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiMulticastNesting:I

    #@2
    if-nez v0, :cond_14

    #@4
    .line 2235
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@6
    iget v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@8
    const/high16 v2, 0x80

    #@a
    or-int/2addr v1, v2

    #@b
    iput v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@d
    .line 2238
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@10
    move-result-wide v0

    #@11
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@14
    .line 2240
    :cond_14
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiMulticastNesting:I

    #@16
    add-int/lit8 v0, v0, 0x1

    #@18
    iput v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiMulticastNesting:I

    #@1a
    .line 2241
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->getUidStatsLocked(I)Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->noteWifiMulticastEnabledLocked()V

    #@21
    .line 2242
    return-void
.end method

.method public noteWifiOffLocked()V
    .registers 4

    #@0
    .prologue
    .line 2050
    iget-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiOn:Z

    #@2
    if-eqz v0, :cond_1d

    #@4
    .line 2051
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@6
    iget v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@8
    const v2, -0x20001

    #@b
    and-int/2addr v1, v2

    #@c
    iput v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@e
    .line 2054
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@11
    move-result-wide v0

    #@12
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@15
    .line 2055
    const/4 v0, 0x0

    #@16
    iput-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiOn:Z

    #@18
    .line 2056
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@1a
    invoke-virtual {v0, p0}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->stopRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@1d
    .line 2058
    :cond_1d
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiOnUid:I

    #@1f
    if-ltz v0, :cond_2d

    #@21
    .line 2059
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiOnUid:I

    #@23
    invoke-virtual {p0, v0}, Lcom/android/internal/os/BatteryStatsImpl;->getUidStatsLocked(I)Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@26
    move-result-object v0

    #@27
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->noteWifiStoppedLocked()V

    #@2a
    .line 2060
    const/4 v0, -0x1

    #@2b
    iput v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiOnUid:I

    #@2d
    .line 2062
    :cond_2d
    return-void
.end method

.method public noteWifiOnLocked()V
    .registers 4

    #@0
    .prologue
    .line 2039
    iget-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiOn:Z

    #@2
    if-nez v0, :cond_1c

    #@4
    .line 2040
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@6
    iget v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@8
    const/high16 v2, 0x2

    #@a
    or-int/2addr v1, v2

    #@b
    iput v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@d
    .line 2043
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@10
    move-result-wide v0

    #@11
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@14
    .line 2044
    const/4 v0, 0x1

    #@15
    iput-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiOn:Z

    #@17
    .line 2045
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@19
    invoke-virtual {v0, p0}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@1c
    .line 2047
    :cond_1c
    return-void
.end method

.method public noteWifiRunningChangedLocked(Landroid/os/WorkSource;Landroid/os/WorkSource;)V
    .registers 7
    .parameter "oldWs"
    .parameter "newWs"

    #@0
    .prologue
    .line 2130
    iget-boolean v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mGlobalWifiRunning:Z

    #@2
    if-eqz v2, :cond_2e

    #@4
    .line 2131
    invoke-virtual {p1}, Landroid/os/WorkSource;->size()I

    #@7
    move-result v0

    #@8
    .line 2132
    .local v0, N:I
    const/4 v1, 0x0

    #@9
    .local v1, i:I
    :goto_9
    if-ge v1, v0, :cond_19

    #@b
    .line 2133
    invoke-virtual {p1, v1}, Landroid/os/WorkSource;->get(I)I

    #@e
    move-result v2

    #@f
    invoke-virtual {p0, v2}, Lcom/android/internal/os/BatteryStatsImpl;->getUidStatsLocked(I)Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->noteWifiStoppedLocked()V

    #@16
    .line 2132
    add-int/lit8 v1, v1, 0x1

    #@18
    goto :goto_9

    #@19
    .line 2135
    :cond_19
    invoke-virtual {p2}, Landroid/os/WorkSource;->size()I

    #@1c
    move-result v0

    #@1d
    .line 2136
    const/4 v1, 0x0

    #@1e
    :goto_1e
    if-ge v1, v0, :cond_36

    #@20
    .line 2137
    invoke-virtual {p2, v1}, Landroid/os/WorkSource;->get(I)I

    #@23
    move-result v2

    #@24
    invoke-virtual {p0, v2}, Lcom/android/internal/os/BatteryStatsImpl;->getUidStatsLocked(I)Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v2}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->noteWifiRunningLocked()V

    #@2b
    .line 2136
    add-int/lit8 v1, v1, 0x1

    #@2d
    goto :goto_1e

    #@2e
    .line 2140
    .end local v0           #N:I
    .end local v1           #i:I
    :cond_2e
    const-string v2, "BatteryStatsImpl"

    #@30
    const-string/jumbo v3, "noteWifiRunningChangedLocked -- called while WIFI not running"

    #@33
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 2142
    :cond_36
    return-void
.end method

.method public noteWifiRunningLocked(Landroid/os/WorkSource;)V
    .registers 7
    .parameter "ws"

    #@0
    .prologue
    .line 2113
    iget-boolean v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mGlobalWifiRunning:Z

    #@2
    if-nez v2, :cond_31

    #@4
    .line 2114
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@6
    iget v3, v2, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@8
    const/high16 v4, 0x400

    #@a
    or-int/2addr v3, v4

    #@b
    iput v3, v2, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@d
    .line 2117
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@10
    move-result-wide v2

    #@11
    invoke-virtual {p0, v2, v3}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@14
    .line 2118
    const/4 v2, 0x1

    #@15
    iput-boolean v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mGlobalWifiRunning:Z

    #@17
    .line 2119
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mGlobalWifiRunningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@19
    invoke-virtual {v2, p0}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@1c
    .line 2120
    invoke-virtual {p1}, Landroid/os/WorkSource;->size()I

    #@1f
    move-result v0

    #@20
    .line 2121
    .local v0, N:I
    const/4 v1, 0x0

    #@21
    .local v1, i:I
    :goto_21
    if-ge v1, v0, :cond_39

    #@23
    .line 2122
    invoke-virtual {p1, v1}, Landroid/os/WorkSource;->get(I)I

    #@26
    move-result v2

    #@27
    invoke-virtual {p0, v2}, Lcom/android/internal/os/BatteryStatsImpl;->getUidStatsLocked(I)Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->noteWifiRunningLocked()V

    #@2e
    .line 2121
    add-int/lit8 v1, v1, 0x1

    #@30
    goto :goto_21

    #@31
    .line 2125
    .end local v0           #N:I
    .end local v1           #i:I
    :cond_31
    const-string v2, "BatteryStatsImpl"

    #@33
    const-string/jumbo v3, "noteWifiRunningLocked -- called while WIFI running"

    #@36
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 2127
    :cond_39
    return-void
.end method

.method public noteWifiScanStartedFromSourceLocked(Landroid/os/WorkSource;)V
    .registers 5
    .parameter "ws"

    #@0
    .prologue
    .line 2270
    invoke-virtual {p1}, Landroid/os/WorkSource;->size()I

    #@3
    move-result v0

    #@4
    .line 2271
    .local v0, N:I
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    if-ge v1, v0, :cond_11

    #@7
    .line 2272
    invoke-virtual {p1, v1}, Landroid/os/WorkSource;->get(I)I

    #@a
    move-result v2

    #@b
    invoke-virtual {p0, v2}, Lcom/android/internal/os/BatteryStatsImpl;->noteWifiScanStartedLocked(I)V

    #@e
    .line 2271
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_5

    #@11
    .line 2274
    :cond_11
    return-void
.end method

.method public noteWifiScanStartedLocked(I)V
    .registers 5
    .parameter "uid"

    #@0
    .prologue
    .line 2210
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiScanNesting:I

    #@2
    if-nez v0, :cond_14

    #@4
    .line 2211
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@6
    iget v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@8
    const/high16 v2, 0x100

    #@a
    or-int/2addr v1, v2

    #@b
    iput v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@d
    .line 2214
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@10
    move-result-wide v0

    #@11
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@14
    .line 2216
    :cond_14
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiScanNesting:I

    #@16
    add-int/lit8 v0, v0, 0x1

    #@18
    iput v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiScanNesting:I

    #@1a
    .line 2217
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->getUidStatsLocked(I)Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->noteWifiScanStartedLocked()V

    #@21
    .line 2218
    return-void
.end method

.method public noteWifiScanStoppedFromSourceLocked(Landroid/os/WorkSource;)V
    .registers 5
    .parameter "ws"

    #@0
    .prologue
    .line 2277
    invoke-virtual {p1}, Landroid/os/WorkSource;->size()I

    #@3
    move-result v0

    #@4
    .line 2278
    .local v0, N:I
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    if-ge v1, v0, :cond_11

    #@7
    .line 2279
    invoke-virtual {p1, v1}, Landroid/os/WorkSource;->get(I)I

    #@a
    move-result v2

    #@b
    invoke-virtual {p0, v2}, Lcom/android/internal/os/BatteryStatsImpl;->noteWifiScanStoppedLocked(I)V

    #@e
    .line 2278
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_5

    #@11
    .line 2281
    :cond_11
    return-void
.end method

.method public noteWifiScanStoppedLocked(I)V
    .registers 5
    .parameter "uid"

    #@0
    .prologue
    .line 2221
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiScanNesting:I

    #@2
    add-int/lit8 v0, v0, -0x1

    #@4
    iput v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiScanNesting:I

    #@6
    .line 2222
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiScanNesting:I

    #@8
    if-nez v0, :cond_1b

    #@a
    .line 2223
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@c
    iget v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@e
    const v2, -0x1000001

    #@11
    and-int/2addr v1, v2

    #@12
    iput v1, v0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@14
    .line 2226
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@17
    move-result-wide v0

    #@18
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@1b
    .line 2228
    :cond_1b
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->getUidStatsLocked(I)Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->noteWifiScanStoppedLocked()V

    #@22
    .line 2229
    return-void
.end method

.method public noteWifiStoppedLocked(Landroid/os/WorkSource;)V
    .registers 7
    .parameter "ws"

    #@0
    .prologue
    .line 2145
    iget-boolean v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mGlobalWifiRunning:Z

    #@2
    if-eqz v2, :cond_32

    #@4
    .line 2146
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@6
    iget v3, v2, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@8
    const v4, -0x4000001

    #@b
    and-int/2addr v3, v4

    #@c
    iput v3, v2, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@e
    .line 2149
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@11
    move-result-wide v2

    #@12
    invoke-virtual {p0, v2, v3}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@15
    .line 2150
    const/4 v2, 0x0

    #@16
    iput-boolean v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mGlobalWifiRunning:Z

    #@18
    .line 2151
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mGlobalWifiRunningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@1a
    invoke-virtual {v2, p0}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->stopRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@1d
    .line 2152
    invoke-virtual {p1}, Landroid/os/WorkSource;->size()I

    #@20
    move-result v0

    #@21
    .line 2153
    .local v0, N:I
    const/4 v1, 0x0

    #@22
    .local v1, i:I
    :goto_22
    if-ge v1, v0, :cond_3a

    #@24
    .line 2154
    invoke-virtual {p1, v1}, Landroid/os/WorkSource;->get(I)I

    #@27
    move-result v2

    #@28
    invoke-virtual {p0, v2}, Lcom/android/internal/os/BatteryStatsImpl;->getUidStatsLocked(I)Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v2}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->noteWifiStoppedLocked()V

    #@2f
    .line 2153
    add-int/lit8 v1, v1, 0x1

    #@31
    goto :goto_22

    #@32
    .line 2157
    .end local v0           #N:I
    .end local v1           #i:I
    :cond_32
    const-string v2, "BatteryStatsImpl"

    #@34
    const-string/jumbo v3, "noteWifiStoppedLocked -- called while WIFI not running"

    #@37
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 2159
    :cond_3a
    return-void
.end method

.method public prepareForDumpLocked()V
    .registers 1

    #@0
    .prologue
    .line 5705
    invoke-virtual {p0}, Lcom/android/internal/os/BatteryStatsImpl;->updateKernelWakelocksLocked()V

    #@3
    .line 5706
    return-void
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .registers 2
    .parameter "in"

    #@0
    .prologue
    .line 5478
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->readFromParcelLocked(Landroid/os/Parcel;)V

    #@3
    .line 5479
    return-void
.end method

.method readFromParcelLocked(Landroid/os/Parcel;)V
    .registers 20
    .parameter "in"

    #@0
    .prologue
    .line 5482
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v12

    #@4
    .line 5483
    .local v12, magic:I
    const v2, -0x458a8b8b

    #@7
    if-eq v12, v2, :cond_11

    #@9
    .line 5484
    new-instance v2, Landroid/os/ParcelFormatException;

    #@b
    const-string v3, "Bad magic number"

    #@d
    invoke-direct {v2, v3}, Landroid/os/ParcelFormatException;-><init>(Ljava/lang/String;)V

    #@10
    throw v2

    #@11
    .line 5487
    :cond_11
    const/4 v2, 0x0

    #@12
    move-object/from16 v0, p0

    #@14
    move-object/from16 v1, p1

    #@16
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/os/BatteryStatsImpl;->readHistory(Landroid/os/Parcel;Z)V

    #@19
    .line 5489
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@1c
    move-result v2

    #@1d
    move-object/from16 v0, p0

    #@1f
    iput v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mStartCount:I

    #@21
    .line 5490
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    #@24
    move-result-wide v2

    #@25
    move-object/from16 v0, p0

    #@27
    iput-wide v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mBatteryUptime:J

    #@29
    .line 5491
    const-wide/16 v2, 0x0

    #@2b
    move-object/from16 v0, p0

    #@2d
    iput-wide v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mBatteryLastUptime:J

    #@2f
    .line 5492
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    #@32
    move-result-wide v2

    #@33
    move-object/from16 v0, p0

    #@35
    iput-wide v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mBatteryRealtime:J

    #@37
    .line 5493
    const-wide/16 v2, 0x0

    #@39
    move-object/from16 v0, p0

    #@3b
    iput-wide v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mBatteryLastRealtime:J

    #@3d
    .line 5494
    const/4 v2, 0x0

    #@3e
    move-object/from16 v0, p0

    #@40
    iput-boolean v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenOn:Z

    #@42
    .line 5495
    new-instance v2, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@44
    const/4 v3, 0x0

    #@45
    const/4 v4, -0x1

    #@46
    const/4 v5, 0x0

    #@47
    move-object/from16 v0, p0

    #@49
    iget-object v6, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@4b
    move-object/from16 v7, p1

    #@4d
    invoke-direct/range {v2 .. v7}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;Landroid/os/Parcel;)V

    #@50
    move-object/from16 v0, p0

    #@52
    iput-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@54
    .line 5496
    const/4 v9, 0x0

    #@55
    .local v9, i:I
    :goto_55
    const/4 v2, 0x5

    #@56
    if-ge v9, v2, :cond_72

    #@58
    .line 5497
    move-object/from16 v0, p0

    #@5a
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenBrightnessTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@5c
    move-object/from16 v17, v0

    #@5e
    new-instance v2, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@60
    const/4 v3, 0x0

    #@61
    rsub-int/lit8 v4, v9, -0x64

    #@63
    const/4 v5, 0x0

    #@64
    move-object/from16 v0, p0

    #@66
    iget-object v6, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@68
    move-object/from16 v7, p1

    #@6a
    invoke-direct/range {v2 .. v7}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;Landroid/os/Parcel;)V

    #@6d
    aput-object v2, v17, v9

    #@6f
    .line 5496
    add-int/lit8 v9, v9, 0x1

    #@71
    goto :goto_55

    #@72
    .line 5500
    :cond_72
    new-instance v2, Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@74
    move-object/from16 v0, p0

    #@76
    iget-object v3, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@78
    move-object/from16 v0, p1

    #@7a
    invoke-direct {v2, v3, v0}, Lcom/android/internal/os/BatteryStatsImpl$Counter;-><init>(Ljava/util/ArrayList;Landroid/os/Parcel;)V

    #@7d
    move-object/from16 v0, p0

    #@7f
    iput-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mInputEventCounter:Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@81
    .line 5501
    const/4 v2, 0x0

    #@82
    move-object/from16 v0, p0

    #@84
    iput-boolean v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneOn:Z

    #@86
    .line 5502
    new-instance v2, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@88
    const/4 v3, 0x0

    #@89
    const/4 v4, -0x2

    #@8a
    const/4 v5, 0x0

    #@8b
    move-object/from16 v0, p0

    #@8d
    iget-object v6, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@8f
    move-object/from16 v7, p1

    #@91
    invoke-direct/range {v2 .. v7}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;Landroid/os/Parcel;)V

    #@94
    move-object/from16 v0, p0

    #@96
    iput-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@98
    .line 5503
    const/4 v9, 0x0

    #@99
    :goto_99
    sget v2, Landroid/telephony/SignalStrength;->NUM_SIGNAL_STRENGTH_BINS:I

    #@9b
    if-ge v9, v2, :cond_b7

    #@9d
    .line 5504
    move-object/from16 v0, p0

    #@9f
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalStrengthsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@a1
    move-object/from16 v17, v0

    #@a3
    new-instance v2, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@a5
    const/4 v3, 0x0

    #@a6
    rsub-int v4, v9, -0xc8

    #@a8
    const/4 v5, 0x0

    #@a9
    move-object/from16 v0, p0

    #@ab
    iget-object v6, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@ad
    move-object/from16 v7, p1

    #@af
    invoke-direct/range {v2 .. v7}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;Landroid/os/Parcel;)V

    #@b2
    aput-object v2, v17, v9

    #@b4
    .line 5503
    add-int/lit8 v9, v9, 0x1

    #@b6
    goto :goto_99

    #@b7
    .line 5507
    :cond_b7
    new-instance v2, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@b9
    const/4 v3, 0x0

    #@ba
    const/16 v4, -0xc7

    #@bc
    const/4 v5, 0x0

    #@bd
    move-object/from16 v0, p0

    #@bf
    iget-object v6, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@c1
    move-object/from16 v7, p1

    #@c3
    invoke-direct/range {v2 .. v7}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;Landroid/os/Parcel;)V

    #@c6
    move-object/from16 v0, p0

    #@c8
    iput-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalScanningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@ca
    .line 5508
    const/4 v9, 0x0

    #@cb
    :goto_cb
    const/16 v2, 0x10

    #@cd
    if-ge v9, v2, :cond_e9

    #@cf
    .line 5509
    move-object/from16 v0, p0

    #@d1
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneDataConnectionsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@d3
    move-object/from16 v17, v0

    #@d5
    new-instance v2, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@d7
    const/4 v3, 0x0

    #@d8
    rsub-int v4, v9, -0x12c

    #@da
    const/4 v5, 0x0

    #@db
    move-object/from16 v0, p0

    #@dd
    iget-object v6, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@df
    move-object/from16 v7, p1

    #@e1
    invoke-direct/range {v2 .. v7}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;Landroid/os/Parcel;)V

    #@e4
    aput-object v2, v17, v9

    #@e6
    .line 5508
    add-int/lit8 v9, v9, 0x1

    #@e8
    goto :goto_cb

    #@e9
    .line 5512
    :cond_e9
    const/4 v2, 0x0

    #@ea
    move-object/from16 v0, p0

    #@ec
    iput-boolean v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiOn:Z

    #@ee
    .line 5513
    new-instance v2, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@f0
    const/4 v3, 0x0

    #@f1
    const/4 v4, -0x2

    #@f2
    const/4 v5, 0x0

    #@f3
    move-object/from16 v0, p0

    #@f5
    iget-object v6, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@f7
    move-object/from16 v7, p1

    #@f9
    invoke-direct/range {v2 .. v7}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;Landroid/os/Parcel;)V

    #@fc
    move-object/from16 v0, p0

    #@fe
    iput-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@100
    .line 5514
    const/4 v2, 0x0

    #@101
    move-object/from16 v0, p0

    #@103
    iput-boolean v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mGlobalWifiRunning:Z

    #@105
    .line 5515
    new-instance v2, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@107
    const/4 v3, 0x0

    #@108
    const/4 v4, -0x2

    #@109
    const/4 v5, 0x0

    #@10a
    move-object/from16 v0, p0

    #@10c
    iget-object v6, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@10e
    move-object/from16 v7, p1

    #@110
    invoke-direct/range {v2 .. v7}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;Landroid/os/Parcel;)V

    #@113
    move-object/from16 v0, p0

    #@115
    iput-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mGlobalWifiRunningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@117
    .line 5516
    const/4 v2, 0x0

    #@118
    move-object/from16 v0, p0

    #@11a
    iput-boolean v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mBluetoothOn:Z

    #@11c
    .line 5517
    new-instance v2, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@11e
    const/4 v3, 0x0

    #@11f
    const/4 v4, -0x2

    #@120
    const/4 v5, 0x0

    #@121
    move-object/from16 v0, p0

    #@123
    iget-object v6, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@125
    move-object/from16 v7, p1

    #@127
    invoke-direct/range {v2 .. v7}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;Landroid/os/Parcel;)V

    #@12a
    move-object/from16 v0, p0

    #@12c
    iput-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mBluetoothOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@12e
    .line 5518
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    #@131
    move-result-wide v2

    #@132
    move-object/from16 v0, p0

    #@134
    iput-wide v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUptime:J

    #@136
    .line 5519
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    #@139
    move-result-wide v2

    #@13a
    move-object/from16 v0, p0

    #@13c
    iput-wide v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUptimeStart:J

    #@13e
    .line 5520
    const-wide/16 v2, 0x0

    #@140
    move-object/from16 v0, p0

    #@142
    iput-wide v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mLastUptime:J

    #@144
    .line 5521
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    #@147
    move-result-wide v2

    #@148
    move-object/from16 v0, p0

    #@14a
    iput-wide v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mRealtime:J

    #@14c
    .line 5522
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    #@14f
    move-result-wide v2

    #@150
    move-object/from16 v0, p0

    #@152
    iput-wide v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mRealtimeStart:J

    #@154
    .line 5523
    const-wide/16 v2, 0x0

    #@156
    move-object/from16 v0, p0

    #@158
    iput-wide v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mLastRealtime:J

    #@15a
    .line 5524
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@15d
    move-result v2

    #@15e
    if-eqz v2, :cond_283

    #@160
    const/4 v2, 0x1

    #@161
    :goto_161
    move-object/from16 v0, p0

    #@163
    iput-boolean v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mOnBattery:Z

    #@165
    .line 5525
    const/4 v2, 0x0

    #@166
    move-object/from16 v0, p0

    #@168
    iput-boolean v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mOnBatteryInternal:Z

    #@16a
    .line 5526
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    #@16d
    move-result-wide v2

    #@16e
    move-object/from16 v0, p0

    #@170
    iput-wide v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mTrackBatteryPastUptime:J

    #@172
    .line 5527
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    #@175
    move-result-wide v2

    #@176
    move-object/from16 v0, p0

    #@178
    iput-wide v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mTrackBatteryUptimeStart:J

    #@17a
    .line 5528
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    #@17d
    move-result-wide v2

    #@17e
    move-object/from16 v0, p0

    #@180
    iput-wide v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mTrackBatteryPastRealtime:J

    #@182
    .line 5529
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    #@185
    move-result-wide v2

    #@186
    move-object/from16 v0, p0

    #@188
    iput-wide v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mTrackBatteryRealtimeStart:J

    #@18a
    .line 5530
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    #@18d
    move-result-wide v2

    #@18e
    move-object/from16 v0, p0

    #@190
    iput-wide v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggedBatteryUptime:J

    #@192
    .line 5531
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    #@195
    move-result-wide v2

    #@196
    move-object/from16 v0, p0

    #@198
    iput-wide v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggedBatteryRealtime:J

    #@19a
    .line 5532
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@19d
    move-result v2

    #@19e
    move-object/from16 v0, p0

    #@1a0
    iput v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeUnplugLevel:I

    #@1a2
    .line 5533
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@1a5
    move-result v2

    #@1a6
    move-object/from16 v0, p0

    #@1a8
    iput v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeCurrentLevel:I

    #@1aa
    .line 5534
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@1ad
    move-result v2

    #@1ae
    move-object/from16 v0, p0

    #@1b0
    iput v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mLowDischargeAmountSinceCharge:I

    #@1b2
    .line 5535
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@1b5
    move-result v2

    #@1b6
    move-object/from16 v0, p0

    #@1b8
    iput v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mHighDischargeAmountSinceCharge:I

    #@1ba
    .line 5536
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@1bd
    move-result v2

    #@1be
    move-object/from16 v0, p0

    #@1c0
    iput v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeAmountScreenOn:I

    #@1c2
    .line 5537
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@1c5
    move-result v2

    #@1c6
    move-object/from16 v0, p0

    #@1c8
    iput v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeAmountScreenOnSinceCharge:I

    #@1ca
    .line 5538
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@1cd
    move-result v2

    #@1ce
    move-object/from16 v0, p0

    #@1d0
    iput v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeAmountScreenOff:I

    #@1d2
    .line 5539
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@1d5
    move-result v2

    #@1d6
    move-object/from16 v0, p0

    #@1d8
    iput v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeAmountScreenOffSinceCharge:I

    #@1da
    .line 5540
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    #@1dd
    move-result-wide v2

    #@1de
    move-object/from16 v0, p0

    #@1e0
    iput-wide v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mLastWriteTime:J

    #@1e2
    .line 5542
    move-object/from16 v0, p0

    #@1e4
    iget-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mMobileDataRx:[J

    #@1e6
    const/4 v3, 0x1

    #@1e7
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    #@1ea
    move-result-wide v4

    #@1eb
    aput-wide v4, v2, v3

    #@1ed
    .line 5543
    move-object/from16 v0, p0

    #@1ef
    iget-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mMobileDataRx:[J

    #@1f1
    const/4 v3, 0x3

    #@1f2
    const-wide/16 v4, -0x1

    #@1f4
    aput-wide v4, v2, v3

    #@1f6
    .line 5544
    move-object/from16 v0, p0

    #@1f8
    iget-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mMobileDataTx:[J

    #@1fa
    const/4 v3, 0x1

    #@1fb
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    #@1fe
    move-result-wide v4

    #@1ff
    aput-wide v4, v2, v3

    #@201
    .line 5545
    move-object/from16 v0, p0

    #@203
    iget-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mMobileDataTx:[J

    #@205
    const/4 v3, 0x3

    #@206
    const-wide/16 v4, -0x1

    #@208
    aput-wide v4, v2, v3

    #@20a
    .line 5546
    move-object/from16 v0, p0

    #@20c
    iget-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mTotalDataRx:[J

    #@20e
    const/4 v3, 0x1

    #@20f
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    #@212
    move-result-wide v4

    #@213
    aput-wide v4, v2, v3

    #@215
    .line 5547
    move-object/from16 v0, p0

    #@217
    iget-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mTotalDataRx:[J

    #@219
    const/4 v3, 0x3

    #@21a
    const-wide/16 v4, -0x1

    #@21c
    aput-wide v4, v2, v3

    #@21e
    .line 5548
    move-object/from16 v0, p0

    #@220
    iget-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mTotalDataTx:[J

    #@222
    const/4 v3, 0x1

    #@223
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    #@226
    move-result-wide v4

    #@227
    aput-wide v4, v2, v3

    #@229
    .line 5549
    move-object/from16 v0, p0

    #@22b
    iget-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mTotalDataTx:[J

    #@22d
    const/4 v3, 0x3

    #@22e
    const-wide/16 v4, -0x1

    #@230
    aput-wide v4, v2, v3

    #@232
    .line 5551
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    #@235
    move-result-wide v2

    #@236
    move-object/from16 v0, p0

    #@238
    iput-wide v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mRadioDataUptime:J

    #@23a
    .line 5552
    const-wide/16 v2, -0x1

    #@23c
    move-object/from16 v0, p0

    #@23e
    iput-wide v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mRadioDataStart:J

    #@240
    .line 5554
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@243
    move-result v2

    #@244
    move-object/from16 v0, p0

    #@246
    iput v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mBluetoothPingCount:I

    #@248
    .line 5555
    const/4 v2, -0x1

    #@249
    move-object/from16 v0, p0

    #@24b
    iput v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mBluetoothPingStart:I

    #@24d
    .line 5557
    move-object/from16 v0, p0

    #@24f
    iget-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mKernelWakelockStats:Ljava/util/HashMap;

    #@251
    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    #@254
    .line 5558
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@257
    move-result v8

    #@258
    .line 5559
    .local v8, NKW:I
    const/4 v10, 0x0

    #@259
    .local v10, ikw:I
    :goto_259
    if-ge v10, v8, :cond_286

    #@25b
    .line 5560
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@25e
    move-result v2

    #@25f
    if-eqz v2, :cond_280

    #@261
    .line 5561
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@264
    move-result-object v16

    #@265
    .line 5562
    .local v16, wakelockName:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@268
    .line 5563
    new-instance v11, Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;

    #@26a
    move-object/from16 v0, p0

    #@26c
    iget-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@26e
    move-object/from16 v0, p0

    #@270
    iget-boolean v3, v0, Lcom/android/internal/os/BatteryStatsImpl;->mOnBattery:Z

    #@272
    move-object/from16 v0, p1

    #@274
    invoke-direct {v11, v2, v3, v0}, Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;-><init>(Ljava/util/ArrayList;ZLandroid/os/Parcel;)V

    #@277
    .line 5564
    .local v11, kwlt:Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;
    move-object/from16 v0, p0

    #@279
    iget-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mKernelWakelockStats:Ljava/util/HashMap;

    #@27b
    move-object/from16 v0, v16

    #@27d
    invoke-virtual {v2, v0, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@280
    .line 5559
    .end local v11           #kwlt:Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;
    .end local v16           #wakelockName:Ljava/lang/String;
    :cond_280
    add-int/lit8 v10, v10, 0x1

    #@282
    goto :goto_259

    #@283
    .line 5524
    .end local v8           #NKW:I
    .end local v10           #ikw:I
    :cond_283
    const/4 v2, 0x0

    #@284
    goto/16 :goto_161

    #@286
    .line 5568
    .restart local v8       #NKW:I
    .restart local v10       #ikw:I
    :cond_286
    move-object/from16 v0, p0

    #@288
    iget-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mPartialTimers:Ljava/util/ArrayList;

    #@28a
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@28d
    .line 5569
    move-object/from16 v0, p0

    #@28f
    iget-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mFullTimers:Ljava/util/ArrayList;

    #@291
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@294
    .line 5570
    move-object/from16 v0, p0

    #@296
    iget-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mWindowTimers:Ljava/util/ArrayList;

    #@298
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@29b
    .line 5571
    move-object/from16 v0, p0

    #@29d
    iget-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiRunningTimers:Ljava/util/ArrayList;

    #@29f
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@2a2
    .line 5572
    move-object/from16 v0, p0

    #@2a4
    iget-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mFullWifiLockTimers:Ljava/util/ArrayList;

    #@2a6
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@2a9
    .line 5573
    move-object/from16 v0, p0

    #@2ab
    iget-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiScanTimers:Ljava/util/ArrayList;

    #@2ad
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@2b0
    .line 5574
    move-object/from16 v0, p0

    #@2b2
    iget-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiMulticastTimers:Ljava/util/ArrayList;

    #@2b4
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@2b7
    .line 5576
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@2ba
    move-result v2

    #@2bb
    sput v2, Lcom/android/internal/os/BatteryStatsImpl;->sNumSpeedSteps:I

    #@2bd
    .line 5578
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@2c0
    move-result v13

    #@2c1
    .line 5579
    .local v13, numUids:I
    move-object/from16 v0, p0

    #@2c3
    iget-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@2c5
    invoke-virtual {v2}, Landroid/util/SparseArray;->clear()V

    #@2c8
    .line 5580
    const/4 v9, 0x0

    #@2c9
    :goto_2c9
    if-ge v9, v13, :cond_2e9

    #@2cb
    .line 5581
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@2ce
    move-result v15

    #@2cf
    .line 5582
    .local v15, uid:I
    new-instance v14, Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@2d1
    move-object/from16 v0, p0

    #@2d3
    invoke-direct {v14, v0, v15}, Lcom/android/internal/os/BatteryStatsImpl$Uid;-><init>(Lcom/android/internal/os/BatteryStatsImpl;I)V

    #@2d6
    .line 5583
    .local v14, u:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    move-object/from16 v0, p0

    #@2d8
    iget-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@2da
    move-object/from16 v0, p1

    #@2dc
    invoke-virtual {v14, v2, v0}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->readFromParcelLocked(Ljava/util/ArrayList;Landroid/os/Parcel;)V

    #@2df
    .line 5584
    move-object/from16 v0, p0

    #@2e1
    iget-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@2e3
    invoke-virtual {v2, v15, v14}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@2e6
    .line 5580
    add-int/lit8 v9, v9, 0x1

    #@2e8
    goto :goto_2c9

    #@2e9
    .line 5586
    .end local v14           #u:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    .end local v15           #uid:I
    :cond_2e9
    return-void
.end method

.method readHistory(Landroid/os/Parcel;Z)V
    .registers 13
    .parameter "in"
    .parameter "andOldHistory"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 4982
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@4
    move-result-wide v2

    #@5
    .line 4984
    .local v2, historyBaseTime:J
    iget-object v6, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@7
    invoke-virtual {v6, v7}, Landroid/os/Parcel;->setDataSize(I)V

    #@a
    .line 4985
    iget-object v6, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@c
    invoke-virtual {v6, v7}, Landroid/os/Parcel;->setDataPosition(I)V

    #@f
    .line 4987
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@12
    move-result v0

    #@13
    .line 4988
    .local v0, bufSize:I
    invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I

    #@16
    move-result v1

    #@17
    .line 4989
    .local v1, curPos:I
    const v6, 0x6c000

    #@1a
    if-lt v0, v6, :cond_51

    #@1c
    .line 4990
    const-string v6, "BatteryStatsImpl"

    #@1e
    new-instance v7, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v8, "File corrupt: history data buffer too large "

    #@25
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v7

    #@29
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v7

    #@2d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v7

    #@31
    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 5000
    :goto_34
    if-eqz p2, :cond_39

    #@36
    .line 5001
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->readOldHistory(Landroid/os/Parcel;)V

    #@39
    .line 5010
    :cond_39
    iput-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBaseTime:J

    #@3b
    .line 5020
    iget-wide v6, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBaseTime:J

    #@3d
    const-wide/16 v8, 0x0

    #@3f
    cmp-long v6, v6, v8

    #@41
    if-lez v6, :cond_50

    #@43
    .line 5021
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@46
    move-result-wide v4

    #@47
    .line 5022
    .local v4, oldnow:J
    iget-wide v6, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBaseTime:J

    #@49
    sub-long/2addr v6, v4

    #@4a
    const-wide/32 v8, 0xea60

    #@4d
    add-long/2addr v6, v8

    #@4e
    iput-wide v6, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBaseTime:J

    #@50
    .line 5030
    .end local v4           #oldnow:J
    :cond_50
    return-void

    #@51
    .line 4991
    :cond_51
    and-int/lit8 v6, v0, -0x4

    #@53
    if-eq v6, v0, :cond_6e

    #@55
    .line 4992
    const-string v6, "BatteryStatsImpl"

    #@57
    new-instance v7, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v8, "File corrupt: history data buffer not aligned "

    #@5e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v7

    #@62
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@65
    move-result-object v7

    #@66
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v7

    #@6a
    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    goto :goto_34

    #@6e
    .line 4996
    :cond_6e
    iget-object v6, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@70
    invoke-virtual {v6, p1, v1, v0}, Landroid/os/Parcel;->appendFrom(Landroid/os/Parcel;II)V

    #@73
    .line 4997
    add-int v6, v1, v0

    #@75
    invoke-virtual {p1, v6}, Landroid/os/Parcel;->setDataPosition(I)V

    #@78
    goto :goto_34
.end method

.method public readLocked()V
    .registers 10

    #@0
    .prologue
    .line 4945
    iget-object v7, p0, Lcom/android/internal/os/BatteryStatsImpl;->mFile:Lcom/android/internal/util/JournaledFile;

    #@2
    if-nez v7, :cond_d

    #@4
    .line 4946
    const-string v7, "BatteryStats"

    #@6
    const-string/jumbo v8, "readLocked: no file associated with this instance"

    #@9
    invoke-static {v7, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 4975
    :cond_c
    :goto_c
    return-void

    #@d
    .line 4950
    :cond_d
    iget-object v7, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@f
    invoke-virtual {v7}, Landroid/util/SparseArray;->clear()V

    #@12
    .line 4953
    :try_start_12
    iget-object v7, p0, Lcom/android/internal/os/BatteryStatsImpl;->mFile:Lcom/android/internal/util/JournaledFile;

    #@14
    invoke-virtual {v7}, Lcom/android/internal/util/JournaledFile;->chooseForRead()Ljava/io/File;

    #@17
    move-result-object v1

    #@18
    .line 4954
    .local v1, file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@1b
    move-result v7

    #@1c
    if-eqz v7, :cond_c

    #@1e
    .line 4957
    new-instance v6, Ljava/io/FileInputStream;

    #@20
    invoke-direct {v6, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    #@23
    .line 4959
    .local v6, stream:Ljava/io/FileInputStream;
    invoke-static {v6}, Lcom/android/internal/os/BatteryStatsImpl;->readFully(Ljava/io/FileInputStream;)[B

    #@26
    move-result-object v5

    #@27
    .line 4960
    .local v5, raw:[B
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@2a
    move-result-object v2

    #@2b
    .line 4961
    .local v2, in:Landroid/os/Parcel;
    const/4 v7, 0x0

    #@2c
    array-length v8, v5

    #@2d
    invoke-virtual {v2, v5, v7, v8}, Landroid/os/Parcel;->unmarshall([BII)V

    #@30
    .line 4962
    const/4 v7, 0x0

    #@31
    invoke-virtual {v2, v7}, Landroid/os/Parcel;->setDataPosition(I)V

    #@34
    .line 4963
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    #@37
    .line 4965
    invoke-direct {p0, v2}, Lcom/android/internal/os/BatteryStatsImpl;->readSummaryFromParcel(Landroid/os/Parcel;)V
    :try_end_3a
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_3a} :catch_43

    #@3a
    .line 4970
    .end local v1           #file:Ljava/io/File;
    .end local v2           #in:Landroid/os/Parcel;
    .end local v5           #raw:[B
    .end local v6           #stream:Ljava/io/FileInputStream;
    :goto_3a
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@3d
    move-result-wide v3

    #@3e
    .line 4974
    .local v3, now:J
    const/4 v7, 0x2

    #@3f
    invoke-virtual {p0, v3, v4, v7}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryBufferLocked(JB)V

    #@42
    goto :goto_c

    #@43
    .line 4966
    .end local v3           #now:J
    :catch_43
    move-exception v0

    #@44
    .line 4967
    .local v0, e:Ljava/io/IOException;
    const-string v7, "BatteryStats"

    #@46
    const-string v8, "Error reading battery statistics"

    #@48
    invoke-static {v7, v8, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4b
    goto :goto_3a
.end method

.method readOldHistory(Landroid/os/Parcel;)V
    .registers 2
    .parameter "in"

    #@0
    .prologue
    .line 5034
    return-void
.end method

.method public removeUidStatsLocked(I)V
    .registers 3
    .parameter "uid"

    #@0
    .prologue
    .line 4745
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    #@5
    .line 4746
    return-void
.end method

.method public reportExcessiveCpuLocked(ILjava/lang/String;JJ)V
    .registers 13
    .parameter "uid"
    .parameter "proc"
    .parameter "overTime"
    .parameter "usedTime"

    #@0
    .prologue
    .line 1702
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@8
    .line 1703
    .local v0, u:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    if-eqz v0, :cond_10

    #@a
    move-object v1, p2

    #@b
    move-wide v2, p3

    #@c
    move-wide v4, p5

    #@d
    .line 1704
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->reportExcessiveCpuLocked(Ljava/lang/String;JJ)V

    #@10
    .line 1706
    :cond_10
    return-void
.end method

.method public reportExcessiveWakeLocked(ILjava/lang/String;JJ)V
    .registers 13
    .parameter "uid"
    .parameter "proc"
    .parameter "overTime"
    .parameter "usedTime"

    #@0
    .prologue
    .line 1695
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@8
    .line 1696
    .local v0, u:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    if-eqz v0, :cond_10

    #@a
    move-object v1, p2

    #@b
    move-wide v2, p3

    #@c
    move-wide v4, p5

    #@d
    .line 1697
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->reportExcessiveWakeLocked(Ljava/lang/String;JJ)V

    #@10
    .line 1699
    :cond_10
    return-void
.end method

.method public resetAllStatsLocked()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 4259
    iput v4, p0, Lcom/android/internal/os/BatteryStatsImpl;->mStartCount:I

    #@3
    .line 4260
    invoke-virtual {p0}, Lcom/android/internal/os/BatteryStatsImpl;->initTimes()V

    #@6
    .line 4261
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@8
    invoke-virtual {v3, p0, v4}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->reset(Lcom/android/internal/os/BatteryStatsImpl;Z)Z

    #@b
    .line 4262
    const/4 v0, 0x0

    #@c
    .local v0, i:I
    :goto_c
    const/4 v3, 0x5

    #@d
    if-ge v0, v3, :cond_19

    #@f
    .line 4263
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenBrightnessTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@11
    aget-object v3, v3, v0

    #@13
    invoke-virtual {v3, p0, v4}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->reset(Lcom/android/internal/os/BatteryStatsImpl;Z)Z

    #@16
    .line 4262
    add-int/lit8 v0, v0, 0x1

    #@18
    goto :goto_c

    #@19
    .line 4265
    :cond_19
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mInputEventCounter:Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@1b
    invoke-virtual {v3, v4}, Lcom/android/internal/os/BatteryStatsImpl$Counter;->reset(Z)V

    #@1e
    .line 4266
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@20
    invoke-virtual {v3, p0, v4}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->reset(Lcom/android/internal/os/BatteryStatsImpl;Z)Z

    #@23
    .line 4267
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mAudioOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@25
    invoke-virtual {v3, p0, v4}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->reset(Lcom/android/internal/os/BatteryStatsImpl;Z)Z

    #@28
    .line 4268
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mVideoOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2a
    invoke-virtual {v3, p0, v4}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->reset(Lcom/android/internal/os/BatteryStatsImpl;Z)Z

    #@2d
    .line 4269
    const/4 v0, 0x0

    #@2e
    :goto_2e
    sget v3, Landroid/telephony/SignalStrength;->NUM_SIGNAL_STRENGTH_BINS:I

    #@30
    if-ge v0, v3, :cond_3c

    #@32
    .line 4270
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalStrengthsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@34
    aget-object v3, v3, v0

    #@36
    invoke-virtual {v3, p0, v4}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->reset(Lcom/android/internal/os/BatteryStatsImpl;Z)Z

    #@39
    .line 4269
    add-int/lit8 v0, v0, 0x1

    #@3b
    goto :goto_2e

    #@3c
    .line 4272
    :cond_3c
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalScanningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@3e
    invoke-virtual {v3, p0, v4}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->reset(Lcom/android/internal/os/BatteryStatsImpl;Z)Z

    #@41
    .line 4273
    const/4 v0, 0x0

    #@42
    :goto_42
    const/16 v3, 0x10

    #@44
    if-ge v0, v3, :cond_50

    #@46
    .line 4274
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneDataConnectionsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@48
    aget-object v3, v3, v0

    #@4a
    invoke-virtual {v3, p0, v4}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->reset(Lcom/android/internal/os/BatteryStatsImpl;Z)Z

    #@4d
    .line 4273
    add-int/lit8 v0, v0, 0x1

    #@4f
    goto :goto_42

    #@50
    .line 4276
    :cond_50
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@52
    invoke-virtual {v3, p0, v4}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->reset(Lcom/android/internal/os/BatteryStatsImpl;Z)Z

    #@55
    .line 4277
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mGlobalWifiRunningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@57
    invoke-virtual {v3, p0, v4}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->reset(Lcom/android/internal/os/BatteryStatsImpl;Z)Z

    #@5a
    .line 4278
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBluetoothOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@5c
    invoke-virtual {v3, p0, v4}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->reset(Lcom/android/internal/os/BatteryStatsImpl;Z)Z

    #@5f
    .line 4280
    const/4 v0, 0x0

    #@60
    :goto_60
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@62
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    #@65
    move-result v3

    #@66
    if-ge v0, v3, :cond_86

    #@68
    .line 4281
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@6a
    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@6d
    move-result-object v3

    #@6e
    check-cast v3, Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@70
    invoke-virtual {v3}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->reset()Z

    #@73
    move-result v3

    #@74
    if-eqz v3, :cond_83

    #@76
    .line 4282
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@78
    iget-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@7a
    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->keyAt(I)I

    #@7d
    move-result v4

    #@7e
    invoke-virtual {v3, v4}, Landroid/util/SparseArray;->remove(I)V

    #@81
    .line 4283
    add-int/lit8 v0, v0, -0x1

    #@83
    .line 4280
    :cond_83
    add-int/lit8 v0, v0, 0x1

    #@85
    goto :goto_60

    #@86
    .line 4287
    :cond_86
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mKernelWakelockStats:Ljava/util/HashMap;

    #@88
    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    #@8b
    move-result v3

    #@8c
    if-lez v3, :cond_af

    #@8e
    .line 4288
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mKernelWakelockStats:Ljava/util/HashMap;

    #@90
    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@93
    move-result-object v3

    #@94
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@97
    move-result-object v1

    #@98
    .local v1, i$:Ljava/util/Iterator;
    :goto_98
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9b
    move-result v3

    #@9c
    if-eqz v3, :cond_aa

    #@9e
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@a1
    move-result-object v2

    #@a2
    check-cast v2, Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;

    #@a4
    .line 4289
    .local v2, timer:Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@a6
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@a9
    goto :goto_98

    #@aa
    .line 4291
    .end local v2           #timer:Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;
    :cond_aa
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mKernelWakelockStats:Ljava/util/HashMap;

    #@ac
    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    #@af
    .line 4294
    .end local v1           #i$:Ljava/util/Iterator;
    :cond_af
    invoke-virtual {p0}, Lcom/android/internal/os/BatteryStatsImpl;->initDischarge()V

    #@b2
    .line 4296
    invoke-virtual {p0}, Lcom/android/internal/os/BatteryStatsImpl;->clearHistoryLocked()V

    #@b5
    .line 4297
    return-void
.end method

.method public setBatteryState(IIIIII)V
    .registers 13
    .parameter "status"
    .parameter "health"
    .parameter "plugType"
    .parameter "level"
    .parameter "temp"
    .parameter "volt"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 4400
    monitor-enter p0

    #@3
    .line 4401
    if-nez p3, :cond_57

    #@5
    .line 4402
    .local v2, onBattery:Z
    :goto_5
    :try_start_5
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@7
    iget-byte v1, v3, Landroid/os/BatteryStats$HistoryItem;->batteryStatus:B

    #@9
    .line 4403
    .local v1, oldStatus:I
    iget-boolean v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHaveBatteryLevel:Z

    #@b
    if-nez v3, :cond_21

    #@d
    .line 4404
    const/4 v3, 0x1

    #@e
    iput-boolean v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHaveBatteryLevel:Z

    #@10
    .line 4409
    iget-boolean v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mOnBattery:Z

    #@12
    if-ne v2, v3, :cond_20

    #@14
    .line 4410
    if-eqz v2, :cond_59

    #@16
    .line 4411
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@18
    iget v4, v3, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@1a
    const v5, -0x80001

    #@1d
    and-int/2addr v4, v5

    #@1e
    iput v4, v3, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@20
    .line 4416
    :cond_20
    :goto_20
    move v1, p1

    #@21
    .line 4418
    :cond_21
    if-eqz v2, :cond_28

    #@23
    .line 4419
    iput p4, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeCurrentLevel:I

    #@25
    .line 4420
    const/4 v3, 0x1

    #@26
    iput-boolean v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mRecordingHistory:Z

    #@28
    .line 4422
    :cond_28
    iget-boolean v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mOnBattery:Z

    #@2a
    if-eq v2, v3, :cond_66

    #@2c
    .line 4423
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@2e
    int-to-byte v4, p4

    #@2f
    iput-byte v4, v3, Landroid/os/BatteryStats$HistoryItem;->batteryLevel:B

    #@31
    .line 4424
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@33
    int-to-byte v4, p1

    #@34
    iput-byte v4, v3, Landroid/os/BatteryStats$HistoryItem;->batteryStatus:B

    #@36
    .line 4425
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@38
    int-to-byte v4, p2

    #@39
    iput-byte v4, v3, Landroid/os/BatteryStats$HistoryItem;->batteryHealth:B

    #@3b
    .line 4426
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@3d
    int-to-byte v4, p3

    #@3e
    iput-byte v4, v3, Landroid/os/BatteryStats$HistoryItem;->batteryPlugType:B

    #@40
    .line 4427
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@42
    int-to-char v4, p5

    #@43
    iput-char v4, v3, Landroid/os/BatteryStats$HistoryItem;->batteryTemperature:C

    #@45
    .line 4428
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@47
    int-to-char v4, p6

    #@48
    iput-char v4, v3, Landroid/os/BatteryStats$HistoryItem;->batteryVoltage:C

    #@4a
    .line 4429
    invoke-virtual {p0, v2, v1, p4}, Lcom/android/internal/os/BatteryStatsImpl;->setOnBatteryLocked(ZII)V

    #@4d
    .line 4462
    :cond_4d
    :goto_4d
    if-nez v2, :cond_55

    #@4f
    const/4 v3, 0x5

    #@50
    if-ne p1, v3, :cond_55

    #@52
    .line 4465
    const/4 v3, 0x0

    #@53
    iput-boolean v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mRecordingHistory:Z

    #@55
    .line 4467
    :cond_55
    monitor-exit p0

    #@56
    .line 4468
    return-void

    #@57
    .end local v1           #oldStatus:I
    .end local v2           #onBattery:Z
    :cond_57
    move v2, v3

    #@58
    .line 4401
    goto :goto_5

    #@59
    .line 4413
    .restart local v1       #oldStatus:I
    .restart local v2       #onBattery:Z
    :cond_59
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@5b
    iget v4, v3, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@5d
    const/high16 v5, 0x8

    #@5f
    or-int/2addr v4, v5

    #@60
    iput v4, v3, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@62
    goto :goto_20

    #@63
    .line 4467
    .end local v1           #oldStatus:I
    :catchall_63
    move-exception v3

    #@64
    monitor-exit p0
    :try_end_65
    .catchall {:try_start_5 .. :try_end_65} :catchall_63

    #@65
    throw v3

    #@66
    .line 4431
    .restart local v1       #oldStatus:I
    :cond_66
    const/4 v0, 0x0

    #@67
    .line 4432
    .local v0, changed:Z
    :try_start_67
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@69
    iget-byte v3, v3, Landroid/os/BatteryStats$HistoryItem;->batteryLevel:B

    #@6b
    if-eq v3, p4, :cond_73

    #@6d
    .line 4433
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@6f
    int-to-byte v4, p4

    #@70
    iput-byte v4, v3, Landroid/os/BatteryStats$HistoryItem;->batteryLevel:B

    #@72
    .line 4434
    const/4 v0, 0x1

    #@73
    .line 4436
    :cond_73
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@75
    iget-byte v3, v3, Landroid/os/BatteryStats$HistoryItem;->batteryStatus:B

    #@77
    if-eq v3, p1, :cond_7f

    #@79
    .line 4437
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@7b
    int-to-byte v4, p1

    #@7c
    iput-byte v4, v3, Landroid/os/BatteryStats$HistoryItem;->batteryStatus:B

    #@7e
    .line 4438
    const/4 v0, 0x1

    #@7f
    .line 4440
    :cond_7f
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@81
    iget-byte v3, v3, Landroid/os/BatteryStats$HistoryItem;->batteryHealth:B

    #@83
    if-eq v3, p2, :cond_8b

    #@85
    .line 4441
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@87
    int-to-byte v4, p2

    #@88
    iput-byte v4, v3, Landroid/os/BatteryStats$HistoryItem;->batteryHealth:B

    #@8a
    .line 4442
    const/4 v0, 0x1

    #@8b
    .line 4444
    :cond_8b
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@8d
    iget-byte v3, v3, Landroid/os/BatteryStats$HistoryItem;->batteryPlugType:B

    #@8f
    if-eq v3, p3, :cond_97

    #@91
    .line 4445
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@93
    int-to-byte v4, p3

    #@94
    iput-byte v4, v3, Landroid/os/BatteryStats$HistoryItem;->batteryPlugType:B

    #@96
    .line 4446
    const/4 v0, 0x1

    #@97
    .line 4448
    :cond_97
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@99
    iget-char v3, v3, Landroid/os/BatteryStats$HistoryItem;->batteryTemperature:C

    #@9b
    add-int/lit8 v3, v3, 0xa

    #@9d
    if-ge p5, v3, :cond_a7

    #@9f
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@a1
    iget-char v3, v3, Landroid/os/BatteryStats$HistoryItem;->batteryTemperature:C

    #@a3
    add-int/lit8 v3, v3, -0xa

    #@a5
    if-gt p5, v3, :cond_ad

    #@a7
    .line 4450
    :cond_a7
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@a9
    int-to-char v4, p5

    #@aa
    iput-char v4, v3, Landroid/os/BatteryStats$HistoryItem;->batteryTemperature:C

    #@ac
    .line 4451
    const/4 v0, 0x1

    #@ad
    .line 4453
    :cond_ad
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@af
    iget-char v3, v3, Landroid/os/BatteryStats$HistoryItem;->batteryVoltage:C

    #@b1
    add-int/lit8 v3, v3, 0x14

    #@b3
    if-gt p6, v3, :cond_bd

    #@b5
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@b7
    iget-char v3, v3, Landroid/os/BatteryStats$HistoryItem;->batteryVoltage:C

    #@b9
    add-int/lit8 v3, v3, -0x14

    #@bb
    if-ge p6, v3, :cond_c3

    #@bd
    .line 4455
    :cond_bd
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@bf
    int-to-char v4, p6

    #@c0
    iput-char v4, v3, Landroid/os/BatteryStats$HistoryItem;->batteryVoltage:C

    #@c2
    .line 4456
    const/4 v0, 0x1

    #@c3
    .line 4458
    :cond_c3
    if-eqz v0, :cond_4d

    #@c5
    .line 4459
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@c8
    move-result-wide v3

    #@c9
    invoke-virtual {p0, v3, v4}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V
    :try_end_cc
    .catchall {:try_start_67 .. :try_end_cc} :catchall_63

    #@cc
    goto :goto_4d
.end method

.method public setBtHeadset(Landroid/bluetooth/BluetoothHeadset;)V
    .registers 4
    .parameter "headset"

    #@0
    .prologue
    .line 1259
    if-eqz p1, :cond_17

    #@2
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBtHeadset:Landroid/bluetooth/BluetoothHeadset;

    #@4
    if-nez v0, :cond_17

    #@6
    invoke-virtual {p0}, Lcom/android/internal/os/BatteryStatsImpl;->isOnBattery()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_17

    #@c
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBluetoothPingStart:I

    #@e
    const/4 v1, -0x1

    #@f
    if-ne v0, v1, :cond_17

    #@11
    .line 1260
    invoke-direct {p0}, Lcom/android/internal/os/BatteryStatsImpl;->getCurrentBluetoothPingCount()I

    #@14
    move-result v0

    #@15
    iput v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBluetoothPingStart:I

    #@17
    .line 1262
    :cond_17
    iput-object p1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBtHeadset:Landroid/bluetooth/BluetoothHeadset;

    #@19
    .line 1263
    return-void
.end method

.method public setCallback(Lcom/android/internal/os/BatteryStatsImpl$BatteryCallback;)V
    .registers 2
    .parameter "cb"

    #@0
    .prologue
    .line 4129
    iput-object p1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mCallback:Lcom/android/internal/os/BatteryStatsImpl$BatteryCallback;

    #@2
    .line 4130
    return-void
.end method

.method public setNumSpeedSteps(I)V
    .registers 3
    .parameter "steps"

    #@0
    .prologue
    .line 4133
    sget v0, Lcom/android/internal/os/BatteryStatsImpl;->sNumSpeedSteps:I

    #@2
    if-nez v0, :cond_6

    #@4
    sput p1, Lcom/android/internal/os/BatteryStatsImpl;->sNumSpeedSteps:I

    #@6
    .line 4134
    :cond_6
    return-void
.end method

.method setOnBattery(ZII)V
    .registers 5
    .parameter "onBattery"
    .parameter "oldStatus"
    .parameter "level"

    #@0
    .prologue
    .line 4323
    monitor-enter p0

    #@1
    .line 4324
    :try_start_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/internal/os/BatteryStatsImpl;->setOnBatteryLocked(ZII)V

    #@4
    .line 4325
    monitor-exit p0

    #@5
    .line 4326
    return-void

    #@6
    .line 4325
    :catchall_6
    move-exception v0

    #@7
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_6

    #@8
    throw v0
.end method

.method setOnBatteryLocked(ZII)V
    .registers 16
    .parameter "onBattery"
    .parameter "oldStatus"
    .parameter "level"

    #@0
    .prologue
    .line 4329
    const/4 v0, 0x0

    #@1
    .line 4330
    .local v0, doWrite:Z
    iget-object v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHandler:Lcom/android/internal/os/BatteryStatsImpl$MyHandler;

    #@3
    const/4 v9, 0x2

    #@4
    invoke-virtual {v8, v9}, Lcom/android/internal/os/BatteryStatsImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;

    #@7
    move-result-object v1

    #@8
    .line 4331
    .local v1, m:Landroid/os/Message;
    if-eqz p1, :cond_92

    #@a
    const/4 v8, 0x1

    #@b
    :goto_b
    iput v8, v1, Landroid/os/Message;->arg1:I

    #@d
    .line 4332
    iget-object v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHandler:Lcom/android/internal/os/BatteryStatsImpl$MyHandler;

    #@f
    invoke-virtual {v8, v1}, Lcom/android/internal/os/BatteryStatsImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z

    #@12
    .line 4333
    iput-boolean p1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mOnBatteryInternal:Z

    #@14
    iput-boolean p1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mOnBattery:Z

    #@16
    .line 4335
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@19
    move-result-wide v8

    #@1a
    const-wide/16 v10, 0x3e8

    #@1c
    mul-long v6, v8, v10

    #@1e
    .line 4336
    .local v6, uptime:J
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@21
    move-result-wide v2

    #@22
    .line 4337
    .local v2, mSecRealtime:J
    const-wide/16 v8, 0x3e8

    #@24
    mul-long v4, v2, v8

    #@26
    .line 4338
    .local v4, realtime:J
    if-eqz p1, :cond_9b

    #@28
    .line 4343
    const/4 v8, 0x5

    #@29
    if-eq p2, v8, :cond_39

    #@2b
    const/16 v8, 0x5a

    #@2d
    if-ge p3, v8, :cond_39

    #@2f
    iget v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeCurrentLevel:I

    #@31
    const/16 v9, 0x14

    #@33
    if-ge v8, v9, :cond_3f

    #@35
    const/16 v8, 0x50

    #@37
    if-lt p3, v8, :cond_3f

    #@39
    .line 4346
    :cond_39
    const/4 v0, 0x1

    #@3a
    .line 4347
    invoke-virtual {p0}, Lcom/android/internal/os/BatteryStatsImpl;->resetAllStatsLocked()V

    #@3d
    .line 4348
    iput p3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeStartLevel:I

    #@3f
    .line 4350
    :cond_3f
    invoke-virtual {p0}, Lcom/android/internal/os/BatteryStatsImpl;->updateKernelWakelocksLocked()V

    #@42
    .line 4351
    iget-object v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@44
    int-to-byte v9, p3

    #@45
    iput-byte v9, v8, Landroid/os/BatteryStats$HistoryItem;->batteryLevel:B

    #@47
    .line 4352
    iget-object v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@49
    iget v9, v8, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@4b
    const v10, -0x80001

    #@4e
    and-int/2addr v9, v10

    #@4f
    iput v9, v8, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@51
    .line 4355
    invoke-virtual {p0, v2, v3}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@54
    .line 4356
    iput-wide v6, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTrackBatteryUptimeStart:J

    #@56
    .line 4357
    iput-wide v4, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTrackBatteryRealtimeStart:J

    #@58
    .line 4358
    invoke-virtual {p0, v6, v7}, Lcom/android/internal/os/BatteryStatsImpl;->getBatteryUptimeLocked(J)J

    #@5b
    move-result-wide v8

    #@5c
    iput-wide v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggedBatteryUptime:J

    #@5e
    .line 4359
    invoke-virtual {p0, v4, v5}, Lcom/android/internal/os/BatteryStatsImpl;->getBatteryRealtimeLocked(J)J

    #@61
    move-result-wide v8

    #@62
    iput-wide v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggedBatteryRealtime:J

    #@64
    .line 4360
    iput p3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeUnplugLevel:I

    #@66
    iput p3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeCurrentLevel:I

    #@68
    .line 4361
    iget-boolean v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenOn:Z

    #@6a
    if-eqz v8, :cond_95

    #@6c
    .line 4362
    iput p3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeScreenOnUnplugLevel:I

    #@6e
    .line 4363
    const/4 v8, 0x0

    #@6f
    iput v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeScreenOffUnplugLevel:I

    #@71
    .line 4368
    :goto_71
    const/4 v8, 0x0

    #@72
    iput v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeAmountScreenOn:I

    #@74
    .line 4369
    const/4 v8, 0x0

    #@75
    iput v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeAmountScreenOff:I

    #@77
    .line 4370
    iget-wide v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggedBatteryUptime:J

    #@79
    iget-wide v10, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggedBatteryRealtime:J

    #@7b
    invoke-virtual {p0, v8, v9, v10, v11}, Lcom/android/internal/os/BatteryStatsImpl;->doUnplugLocked(JJ)V

    #@7e
    .line 4388
    :goto_7e
    if-nez v0, :cond_8a

    #@80
    iget-wide v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mLastWriteTime:J

    #@82
    const-wide/32 v10, 0xea60

    #@85
    add-long/2addr v8, v10

    #@86
    cmp-long v8, v8, v2

    #@88
    if-gez v8, :cond_91

    #@8a
    .line 4389
    :cond_8a
    iget-object v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mFile:Lcom/android/internal/util/JournaledFile;

    #@8c
    if-eqz v8, :cond_91

    #@8e
    .line 4390
    invoke-virtual {p0}, Lcom/android/internal/os/BatteryStatsImpl;->writeAsyncLocked()V

    #@91
    .line 4393
    :cond_91
    return-void

    #@92
    .line 4331
    .end local v2           #mSecRealtime:J
    .end local v4           #realtime:J
    .end local v6           #uptime:J
    :cond_92
    const/4 v8, 0x0

    #@93
    goto/16 :goto_b

    #@95
    .line 4365
    .restart local v2       #mSecRealtime:J
    .restart local v4       #realtime:J
    .restart local v6       #uptime:J
    :cond_95
    const/4 v8, 0x0

    #@96
    iput v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeScreenOnUnplugLevel:I

    #@98
    .line 4366
    iput p3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeScreenOffUnplugLevel:I

    #@9a
    goto :goto_71

    #@9b
    .line 4372
    :cond_9b
    invoke-virtual {p0}, Lcom/android/internal/os/BatteryStatsImpl;->updateKernelWakelocksLocked()V

    #@9e
    .line 4373
    iget-object v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@a0
    int-to-byte v9, p3

    #@a1
    iput-byte v9, v8, Landroid/os/BatteryStats$HistoryItem;->batteryLevel:B

    #@a3
    .line 4374
    iget-object v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryCur:Landroid/os/BatteryStats$HistoryItem;

    #@a5
    iget v9, v8, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@a7
    const/high16 v10, 0x8

    #@a9
    or-int/2addr v9, v10

    #@aa
    iput v9, v8, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@ac
    .line 4377
    invoke-virtual {p0, v2, v3}, Lcom/android/internal/os/BatteryStatsImpl;->addHistoryRecordLocked(J)V

    #@af
    .line 4378
    iget-wide v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTrackBatteryPastUptime:J

    #@b1
    iget-wide v10, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTrackBatteryUptimeStart:J

    #@b3
    sub-long v10, v6, v10

    #@b5
    add-long/2addr v8, v10

    #@b6
    iput-wide v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTrackBatteryPastUptime:J

    #@b8
    .line 4379
    iget-wide v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTrackBatteryPastRealtime:J

    #@ba
    iget-wide v10, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTrackBatteryRealtimeStart:J

    #@bc
    sub-long v10, v4, v10

    #@be
    add-long/2addr v8, v10

    #@bf
    iput-wide v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mTrackBatteryPastRealtime:J

    #@c1
    .line 4380
    iput p3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeCurrentLevel:I

    #@c3
    .line 4381
    iget v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeUnplugLevel:I

    #@c5
    if-ge p3, v8, :cond_d9

    #@c7
    .line 4382
    iget v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mLowDischargeAmountSinceCharge:I

    #@c9
    iget v9, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeUnplugLevel:I

    #@cb
    sub-int/2addr v9, p3

    #@cc
    add-int/lit8 v9, v9, -0x1

    #@ce
    add-int/2addr v8, v9

    #@cf
    iput v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mLowDischargeAmountSinceCharge:I

    #@d1
    .line 4383
    iget v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHighDischargeAmountSinceCharge:I

    #@d3
    iget v9, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeUnplugLevel:I

    #@d5
    sub-int/2addr v9, p3

    #@d6
    add-int/2addr v8, v9

    #@d7
    iput v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHighDischargeAmountSinceCharge:I

    #@d9
    .line 4385
    :cond_d9
    iget-boolean v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenOn:Z

    #@db
    iget-boolean v9, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenOn:Z

    #@dd
    invoke-virtual {p0, v8, v9}, Lcom/android/internal/os/BatteryStatsImpl;->updateDischargeScreenLevelsLocked(ZZ)V

    #@e0
    .line 4386
    invoke-virtual {p0, v6, v7}, Lcom/android/internal/os/BatteryStatsImpl;->getBatteryUptimeLocked(J)J

    #@e3
    move-result-wide v8

    #@e4
    invoke-virtual {p0, v4, v5}, Lcom/android/internal/os/BatteryStatsImpl;->getBatteryRealtimeLocked(J)J

    #@e7
    move-result-wide v10

    #@e8
    invoke-virtual {p0, v8, v9, v10, v11}, Lcom/android/internal/os/BatteryStatsImpl;->doPlugLocked(JJ)V

    #@eb
    goto :goto_7e
.end method

.method public setRadioScanningTimeout(J)V
    .registers 4
    .parameter "timeout"

    #@0
    .prologue
    .line 4137
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalScanningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 4138
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalScanningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@6
    invoke-virtual {v0, p1, p2}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->setTimeout(J)V

    #@9
    .line 4140
    :cond_9
    return-void
.end method

.method public shutdownLocked()V
    .registers 2

    #@0
    .prologue
    .line 4845
    invoke-virtual {p0}, Lcom/android/internal/os/BatteryStatsImpl;->writeSyncLocked()V

    #@3
    .line 4846
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mShuttingDown:Z

    #@6
    .line 4847
    return-void
.end method

.method public startAddingCpuLocked()I
    .registers 8

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1579
    iget-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHandler:Lcom/android/internal/os/BatteryStatsImpl$MyHandler;

    #@3
    const/4 v6, 0x1

    #@4
    invoke-virtual {v5, v6}, Lcom/android/internal/os/BatteryStatsImpl$MyHandler;->removeMessages(I)V

    #@7
    .line 1581
    iget-boolean v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenOn:Z

    #@9
    if-eqz v5, :cond_c

    #@b
    .line 1605
    :cond_b
    :goto_b
    return v4

    #@c
    .line 1585
    :cond_c
    iget-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPartialTimers:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@11
    move-result v0

    #@12
    .line 1586
    .local v0, N:I
    if-nez v0, :cond_1a

    #@14
    .line 1587
    iget-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mLastPartialTimers:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    #@19
    goto :goto_b

    #@1a
    .line 1593
    :cond_1a
    const/4 v1, 0x0

    #@1b
    .local v1, i:I
    :goto_1b
    if-ge v1, v0, :cond_b

    #@1d
    .line 1594
    iget-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPartialTimers:Ljava/util/ArrayList;

    #@1f
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@22
    move-result-object v2

    #@23
    check-cast v2, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@25
    .line 1595
    .local v2, st:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    iget-boolean v5, v2, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->mInList:Z

    #@27
    if-eqz v5, :cond_36

    #@29
    .line 1596
    iget-object v3, v2, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->mUid:Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@2b
    .line 1599
    .local v3, uid:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    if-eqz v3, :cond_36

    #@2d
    iget v5, v3, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mUid:I

    #@2f
    const/16 v6, 0x3e8

    #@31
    if-eq v5, v6, :cond_36

    #@33
    .line 1600
    const/16 v4, 0x32

    #@35
    goto :goto_b

    #@36
    .line 1593
    .end local v3           #uid:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    :cond_36
    add-int/lit8 v1, v1, 0x1

    #@38
    goto :goto_1b
.end method

.method public startIteratingHistoryLocked()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 4195
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@4
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    #@7
    .line 4196
    iput-boolean v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mReadOverflow:Z

    #@9
    .line 4197
    iput-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mIteratingHistory:Z

    #@b
    .line 4198
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@d
    invoke-virtual {v2}, Landroid/os/Parcel;->dataSize()I

    #@10
    move-result v2

    #@11
    if-lez v2, :cond_14

    #@13
    :goto_13
    return v0

    #@14
    :cond_14
    move v0, v1

    #@15
    goto :goto_13
.end method

.method public startIteratingOldHistoryLocked()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 4146
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@4
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    #@7
    .line 4147
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryReadTmp:Landroid/os/BatteryStats$HistoryItem;

    #@9
    invoke-virtual {v2}, Landroid/os/BatteryStats$HistoryItem;->clear()V

    #@c
    .line 4148
    iput-boolean v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mReadOverflow:Z

    #@e
    .line 4149
    iput-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mIteratingHistory:Z

    #@10
    .line 4150
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistory:Landroid/os/BatteryStats$HistoryItem;

    #@12
    iput-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryIterator:Landroid/os/BatteryStats$HistoryItem;

    #@14
    if-eqz v2, :cond_17

    #@16
    :goto_16
    return v0

    #@17
    :cond_17
    move v0, v1

    #@18
    goto :goto_16
.end method

.method stopAllSignalStrengthTimersLocked(I)V
    .registers 4
    .parameter "except"

    #@0
    .prologue
    .line 1852
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    sget v1, Landroid/telephony/SignalStrength;->NUM_SIGNAL_STRENGTH_BINS:I

    #@3
    if-ge v0, v1, :cond_1c

    #@5
    .line 1853
    if-ne v0, p1, :cond_a

    #@7
    .line 1852
    :cond_7
    add-int/lit8 v0, v0, 0x1

    #@9
    goto :goto_1

    #@a
    .line 1856
    :cond_a
    :goto_a
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalStrengthsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@c
    aget-object v1, v1, v0

    #@e
    invoke-virtual {v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->isRunningLocked()Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_7

    #@14
    .line 1857
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalStrengthsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@16
    aget-object v1, v1, v0

    #@18
    invoke-virtual {v1, p0}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->stopRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@1b
    goto :goto_a

    #@1c
    .line 1860
    :cond_1c
    return-void
.end method

.method updateDischargeScreenLevelsLocked(ZZ)V
    .registers 7
    .parameter "oldScreenOn"
    .parameter "newScreenOn"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 4300
    if-eqz p1, :cond_1e

    #@3
    .line 4301
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeScreenOnUnplugLevel:I

    #@5
    iget v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeCurrentLevel:I

    #@7
    sub-int v0, v1, v2

    #@9
    .line 4302
    .local v0, diff:I
    if-lez v0, :cond_15

    #@b
    .line 4303
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeAmountScreenOn:I

    #@d
    add-int/2addr v1, v0

    #@e
    iput v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeAmountScreenOn:I

    #@10
    .line 4304
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeAmountScreenOnSinceCharge:I

    #@12
    add-int/2addr v1, v0

    #@13
    iput v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeAmountScreenOnSinceCharge:I

    #@15
    .line 4313
    :cond_15
    :goto_15
    if-eqz p2, :cond_31

    #@17
    .line 4314
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeCurrentLevel:I

    #@19
    iput v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeScreenOnUnplugLevel:I

    #@1b
    .line 4315
    iput v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeScreenOffUnplugLevel:I

    #@1d
    .line 4320
    :goto_1d
    return-void

    #@1e
    .line 4307
    .end local v0           #diff:I
    :cond_1e
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeScreenOffUnplugLevel:I

    #@20
    iget v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeCurrentLevel:I

    #@22
    sub-int v0, v1, v2

    #@24
    .line 4308
    .restart local v0       #diff:I
    if-lez v0, :cond_15

    #@26
    .line 4309
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeAmountScreenOff:I

    #@28
    add-int/2addr v1, v0

    #@29
    iput v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeAmountScreenOff:I

    #@2b
    .line 4310
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeAmountScreenOffSinceCharge:I

    #@2d
    add-int/2addr v1, v0

    #@2e
    iput v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeAmountScreenOffSinceCharge:I

    #@30
    goto :goto_15

    #@31
    .line 4317
    :cond_31
    iput v3, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeScreenOnUnplugLevel:I

    #@33
    .line 4318
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeCurrentLevel:I

    #@35
    iput v1, p0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeScreenOffUnplugLevel:I

    #@37
    goto :goto_1d
.end method

.method public updateKernelWakelocksLocked()V
    .registers 12

    #@0
    .prologue
    .line 4471
    invoke-direct {p0}, Lcom/android/internal/os/BatteryStatsImpl;->readKernelWakelockStats()Ljava/util/Map;

    #@3
    move-result-object v5

    #@4
    .line 4473
    .local v5, m:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;>;"
    if-nez v5, :cond_e

    #@6
    .line 4475
    const-string v8, "BatteryStatsImpl"

    #@8
    const-string v9, "Couldn\'t get kernel wake lock stats"

    #@a
    invoke-static {v8, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 4503
    :cond_d
    return-void

    #@e
    .line 4479
    :cond_e
    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@11
    move-result-object v8

    #@12
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@15
    move-result-object v2

    #@16
    .local v2, i$:Ljava/util/Iterator;
    :goto_16
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@19
    move-result v8

    #@1a
    if-eqz v8, :cond_57

    #@1c
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Ljava/util/Map$Entry;

    #@22
    .line 4480
    .local v0, ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@25
    move-result-object v6

    #@26
    check-cast v6, Ljava/lang/String;

    #@28
    .line 4481
    .local v6, name:Ljava/lang/String;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@2b
    move-result-object v4

    #@2c
    check-cast v4, Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;

    #@2e
    .line 4483
    .local v4, kws:Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;
    iget-object v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mKernelWakelockStats:Ljava/util/HashMap;

    #@30
    invoke-virtual {v8, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@33
    move-result-object v3

    #@34
    check-cast v3, Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;

    #@36
    .line 4484
    .local v3, kwlt:Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;
    if-nez v3, :cond_47

    #@38
    .line 4485
    new-instance v3, Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;

    #@3a
    .end local v3           #kwlt:Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;
    iget-object v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@3c
    iget-boolean v9, p0, Lcom/android/internal/os/BatteryStatsImpl;->mOnBatteryInternal:Z

    #@3e
    const/4 v10, 0x1

    #@3f
    invoke-direct {v3, v8, v9, v10}, Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;-><init>(Ljava/util/ArrayList;ZZ)V

    #@42
    .line 4487
    .restart local v3       #kwlt:Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;
    iget-object v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mKernelWakelockStats:Ljava/util/HashMap;

    #@44
    invoke-virtual {v8, v6, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@47
    .line 4489
    :cond_47
    iget v8, v4, Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;->mCount:I

    #@49
    invoke-virtual {v3, v8}, Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;->updateCurrentReportedCount(I)V

    #@4c
    .line 4490
    iget-wide v8, v4, Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;->mTotalTime:J

    #@4e
    invoke-virtual {v3, v8, v9}, Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;->updateCurrentReportedTotalTime(J)V

    #@51
    .line 4491
    sget v8, Lcom/android/internal/os/BatteryStatsImpl;->sKernelWakelockUpdateVersion:I

    #@53
    invoke-virtual {v3, v8}, Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;->setUpdateVersion(I)V

    #@56
    goto :goto_16

    #@57
    .line 4494
    .end local v0           #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;>;"
    .end local v3           #kwlt:Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;
    .end local v4           #kws:Lcom/android/internal/os/BatteryStatsImpl$KernelWakelockStats;
    .end local v6           #name:Ljava/lang/String;
    :cond_57
    invoke-interface {v5}, Ljava/util/Map;->size()I

    #@5a
    move-result v8

    #@5b
    iget-object v9, p0, Lcom/android/internal/os/BatteryStatsImpl;->mKernelWakelockStats:Ljava/util/HashMap;

    #@5d
    invoke-virtual {v9}, Ljava/util/HashMap;->size()I

    #@60
    move-result v9

    #@61
    if-eq v8, v9, :cond_d

    #@63
    .line 4496
    iget-object v8, p0, Lcom/android/internal/os/BatteryStatsImpl;->mKernelWakelockStats:Ljava/util/HashMap;

    #@65
    invoke-virtual {v8}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@68
    move-result-object v8

    #@69
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@6c
    move-result-object v2

    #@6d
    :cond_6d
    :goto_6d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@70
    move-result v8

    #@71
    if-eqz v8, :cond_d

    #@73
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@76
    move-result-object v1

    #@77
    check-cast v1, Ljava/util/Map$Entry;

    #@79
    .line 4497
    .local v1, ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@7c
    move-result-object v7

    #@7d
    check-cast v7, Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;

    #@7f
    .line 4498
    .local v7, st:Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;
    invoke-virtual {v7}, Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;->getUpdateVersion()I

    #@82
    move-result v8

    #@83
    sget v9, Lcom/android/internal/os/BatteryStatsImpl;->sKernelWakelockUpdateVersion:I

    #@85
    if-eq v8, v9, :cond_6d

    #@87
    .line 4499
    invoke-virtual {v7}, Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;->setStale()V

    #@8a
    goto :goto_6d
.end method

.method public writeAsyncLocked()V
    .registers 2

    #@0
    .prologue
    .line 4853
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Lcom/android/internal/os/BatteryStatsImpl;->writeLocked(Z)V

    #@4
    .line 4854
    return-void
.end method

.method writeHistory(Landroid/os/Parcel;Z)V
    .registers 7
    .parameter "out"
    .parameter "andOldHistory"

    #@0
    .prologue
    .line 5053
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBaseTime:J

    #@2
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mLastHistoryTime:J

    #@4
    add-long/2addr v0, v2

    #@5
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@8
    .line 5054
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@a
    invoke-virtual {v0}, Landroid/os/Parcel;->dataSize()I

    #@d
    move-result v0

    #@e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 5057
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@13
    const/4 v1, 0x0

    #@14
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mHistoryBuffer:Landroid/os/Parcel;

    #@16
    invoke-virtual {v2}, Landroid/os/Parcel;->dataSize()I

    #@19
    move-result v2

    #@1a
    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Parcel;->appendFrom(Landroid/os/Parcel;II)V

    #@1d
    .line 5059
    if-eqz p2, :cond_22

    #@1f
    .line 5060
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl;->writeOldHistory(Landroid/os/Parcel;)V

    #@22
    .line 5062
    :cond_22
    return-void
.end method

.method writeLocked(Z)V
    .registers 6
    .parameter "sync"

    #@0
    .prologue
    .line 4861
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mFile:Lcom/android/internal/util/JournaledFile;

    #@2
    if-nez v2, :cond_d

    #@4
    .line 4862
    const-string v2, "BatteryStats"

    #@6
    const-string/jumbo v3, "writeLocked: no file associated with this instance"

    #@9
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 4891
    :cond_c
    :goto_c
    return-void

    #@d
    .line 4866
    :cond_d
    iget-boolean v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mShuttingDown:Z

    #@f
    if-nez v2, :cond_c

    #@11
    .line 4870
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@14
    move-result-object v0

    #@15
    .line 4871
    .local v0, out:Landroid/os/Parcel;
    invoke-virtual {p0, v0}, Lcom/android/internal/os/BatteryStatsImpl;->writeSummaryToParcel(Landroid/os/Parcel;)V

    #@18
    .line 4872
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@1b
    move-result-wide v2

    #@1c
    iput-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mLastWriteTime:J

    #@1e
    .line 4874
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPendingWrite:Landroid/os/Parcel;

    #@20
    if-eqz v2, :cond_27

    #@22
    .line 4875
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPendingWrite:Landroid/os/Parcel;

    #@24
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 4877
    :cond_27
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mPendingWrite:Landroid/os/Parcel;

    #@29
    .line 4879
    if-eqz p1, :cond_2f

    #@2b
    .line 4880
    invoke-virtual {p0}, Lcom/android/internal/os/BatteryStatsImpl;->commitPendingDataToDisk()V

    #@2e
    goto :goto_c

    #@2f
    .line 4882
    :cond_2f
    new-instance v1, Lcom/android/internal/os/BatteryStatsImpl$1;

    #@31
    const-string v2, "BatteryStats-Write"

    #@33
    invoke-direct {v1, p0, v2}, Lcom/android/internal/os/BatteryStatsImpl$1;-><init>(Lcom/android/internal/os/BatteryStatsImpl;Ljava/lang/String;)V

    #@36
    .line 4889
    .local v1, thr:Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    #@39
    goto :goto_c
.end method

.method writeOldHistory(Landroid/os/Parcel;)V
    .registers 2
    .parameter "out"

    #@0
    .prologue
    .line 5066
    return-void
.end method

.method public writeSummaryToParcel(Landroid/os/Parcel;)V
    .registers 41
    .parameter "out"

    #@0
    .prologue
    .line 5275
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/os/BatteryStatsImpl;->updateKernelWakelocksLocked()V

    #@3
    .line 5277
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@6
    move-result-wide v35

    #@7
    const-wide/16 v37, 0x3e8

    #@9
    mul-long v10, v35, v37

    #@b
    .line 5278
    .local v10, NOW_SYS:J
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@e
    move-result-wide v35

    #@f
    const-wide/16 v37, 0x3e8

    #@11
    mul-long v8, v35, v37

    #@13
    .line 5279
    .local v8, NOWREAL_SYS:J
    move-object/from16 v0, p0

    #@15
    invoke-virtual {v0, v10, v11}, Lcom/android/internal/os/BatteryStatsImpl;->getBatteryUptimeLocked(J)J

    #@18
    move-result-wide v4

    #@19
    .line 5280
    .local v4, NOW:J
    move-object/from16 v0, p0

    #@1b
    invoke-virtual {v0, v8, v9}, Lcom/android/internal/os/BatteryStatsImpl;->getBatteryRealtimeLocked(J)J

    #@1e
    move-result-wide v6

    #@1f
    .line 5282
    .local v6, NOWREAL:J
    const/16 v35, 0x3e

    #@21
    move-object/from16 v0, p1

    #@23
    move/from16 v1, v35

    #@25
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@28
    .line 5284
    const/16 v35, 0x1

    #@2a
    move-object/from16 v0, p0

    #@2c
    move-object/from16 v1, p1

    #@2e
    move/from16 v2, v35

    #@30
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/os/BatteryStatsImpl;->writeHistory(Landroid/os/Parcel;Z)V

    #@33
    .line 5286
    move-object/from16 v0, p0

    #@35
    iget v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mStartCount:I

    #@37
    move/from16 v35, v0

    #@39
    move-object/from16 v0, p1

    #@3b
    move/from16 v1, v35

    #@3d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@40
    .line 5287
    const/16 v35, 0x0

    #@42
    move-object/from16 v0, p0

    #@44
    move/from16 v1, v35

    #@46
    invoke-virtual {v0, v10, v11, v1}, Lcom/android/internal/os/BatteryStatsImpl;->computeBatteryUptime(JI)J

    #@49
    move-result-wide v35

    #@4a
    move-object/from16 v0, p1

    #@4c
    move-wide/from16 v1, v35

    #@4e
    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@51
    .line 5288
    const/16 v35, 0x0

    #@53
    move-object/from16 v0, p0

    #@55
    move/from16 v1, v35

    #@57
    invoke-virtual {v0, v8, v9, v1}, Lcom/android/internal/os/BatteryStatsImpl;->computeBatteryRealtime(JI)J

    #@5a
    move-result-wide v35

    #@5b
    move-object/from16 v0, p1

    #@5d
    move-wide/from16 v1, v35

    #@5f
    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@62
    .line 5289
    const/16 v35, 0x0

    #@64
    move-object/from16 v0, p0

    #@66
    move/from16 v1, v35

    #@68
    invoke-virtual {v0, v10, v11, v1}, Lcom/android/internal/os/BatteryStatsImpl;->computeUptime(JI)J

    #@6b
    move-result-wide v35

    #@6c
    move-object/from16 v0, p1

    #@6e
    move-wide/from16 v1, v35

    #@70
    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@73
    .line 5290
    const/16 v35, 0x0

    #@75
    move-object/from16 v0, p0

    #@77
    move/from16 v1, v35

    #@79
    invoke-virtual {v0, v8, v9, v1}, Lcom/android/internal/os/BatteryStatsImpl;->computeRealtime(JI)J

    #@7c
    move-result-wide v35

    #@7d
    move-object/from16 v0, p1

    #@7f
    move-wide/from16 v1, v35

    #@81
    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@84
    .line 5291
    move-object/from16 v0, p0

    #@86
    iget v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeUnplugLevel:I

    #@88
    move/from16 v35, v0

    #@8a
    move-object/from16 v0, p1

    #@8c
    move/from16 v1, v35

    #@8e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@91
    .line 5292
    move-object/from16 v0, p0

    #@93
    iget v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeCurrentLevel:I

    #@95
    move/from16 v35, v0

    #@97
    move-object/from16 v0, p1

    #@99
    move/from16 v1, v35

    #@9b
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@9e
    .line 5293
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/os/BatteryStatsImpl;->getLowDischargeAmountSinceCharge()I

    #@a1
    move-result v35

    #@a2
    move-object/from16 v0, p1

    #@a4
    move/from16 v1, v35

    #@a6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@a9
    .line 5294
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/os/BatteryStatsImpl;->getHighDischargeAmountSinceCharge()I

    #@ac
    move-result v35

    #@ad
    move-object/from16 v0, p1

    #@af
    move/from16 v1, v35

    #@b1
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@b4
    .line 5295
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/os/BatteryStatsImpl;->getDischargeAmountScreenOnSinceCharge()I

    #@b7
    move-result v35

    #@b8
    move-object/from16 v0, p1

    #@ba
    move/from16 v1, v35

    #@bc
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@bf
    .line 5296
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/os/BatteryStatsImpl;->getDischargeAmountScreenOffSinceCharge()I

    #@c2
    move-result v35

    #@c3
    move-object/from16 v0, p1

    #@c5
    move/from16 v1, v35

    #@c7
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@ca
    .line 5298
    move-object/from16 v0, p0

    #@cc
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@ce
    move-object/from16 v35, v0

    #@d0
    move-object/from16 v0, v35

    #@d2
    move-object/from16 v1, p1

    #@d4
    invoke-virtual {v0, v1, v6, v7}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeSummaryFromParcelLocked(Landroid/os/Parcel;J)V

    #@d7
    .line 5299
    const/16 v22, 0x0

    #@d9
    .local v22, i:I
    :goto_d9
    const/16 v35, 0x5

    #@db
    move/from16 v0, v22

    #@dd
    move/from16 v1, v35

    #@df
    if-ge v0, v1, :cond_f3

    #@e1
    .line 5300
    move-object/from16 v0, p0

    #@e3
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenBrightnessTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@e5
    move-object/from16 v35, v0

    #@e7
    aget-object v35, v35, v22

    #@e9
    move-object/from16 v0, v35

    #@eb
    move-object/from16 v1, p1

    #@ed
    invoke-virtual {v0, v1, v6, v7}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeSummaryFromParcelLocked(Landroid/os/Parcel;J)V

    #@f0
    .line 5299
    add-int/lit8 v22, v22, 0x1

    #@f2
    goto :goto_d9

    #@f3
    .line 5302
    :cond_f3
    move-object/from16 v0, p0

    #@f5
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mInputEventCounter:Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@f7
    move-object/from16 v35, v0

    #@f9
    move-object/from16 v0, v35

    #@fb
    move-object/from16 v1, p1

    #@fd
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$Counter;->writeSummaryFromParcelLocked(Landroid/os/Parcel;)V

    #@100
    .line 5303
    move-object/from16 v0, p0

    #@102
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@104
    move-object/from16 v35, v0

    #@106
    move-object/from16 v0, v35

    #@108
    move-object/from16 v1, p1

    #@10a
    invoke-virtual {v0, v1, v6, v7}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeSummaryFromParcelLocked(Landroid/os/Parcel;J)V

    #@10d
    .line 5304
    const/16 v22, 0x0

    #@10f
    :goto_10f
    sget v35, Landroid/telephony/SignalStrength;->NUM_SIGNAL_STRENGTH_BINS:I

    #@111
    move/from16 v0, v22

    #@113
    move/from16 v1, v35

    #@115
    if-ge v0, v1, :cond_129

    #@117
    .line 5305
    move-object/from16 v0, p0

    #@119
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalStrengthsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@11b
    move-object/from16 v35, v0

    #@11d
    aget-object v35, v35, v22

    #@11f
    move-object/from16 v0, v35

    #@121
    move-object/from16 v1, p1

    #@123
    invoke-virtual {v0, v1, v6, v7}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeSummaryFromParcelLocked(Landroid/os/Parcel;J)V

    #@126
    .line 5304
    add-int/lit8 v22, v22, 0x1

    #@128
    goto :goto_10f

    #@129
    .line 5307
    :cond_129
    move-object/from16 v0, p0

    #@12b
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalScanningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@12d
    move-object/from16 v35, v0

    #@12f
    move-object/from16 v0, v35

    #@131
    move-object/from16 v1, p1

    #@133
    invoke-virtual {v0, v1, v6, v7}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeSummaryFromParcelLocked(Landroid/os/Parcel;J)V

    #@136
    .line 5308
    const/16 v22, 0x0

    #@138
    :goto_138
    const/16 v35, 0x10

    #@13a
    move/from16 v0, v22

    #@13c
    move/from16 v1, v35

    #@13e
    if-ge v0, v1, :cond_152

    #@140
    .line 5309
    move-object/from16 v0, p0

    #@142
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneDataConnectionsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@144
    move-object/from16 v35, v0

    #@146
    aget-object v35, v35, v22

    #@148
    move-object/from16 v0, v35

    #@14a
    move-object/from16 v1, p1

    #@14c
    invoke-virtual {v0, v1, v6, v7}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeSummaryFromParcelLocked(Landroid/os/Parcel;J)V

    #@14f
    .line 5308
    add-int/lit8 v22, v22, 0x1

    #@151
    goto :goto_138

    #@152
    .line 5311
    :cond_152
    move-object/from16 v0, p0

    #@154
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@156
    move-object/from16 v35, v0

    #@158
    move-object/from16 v0, v35

    #@15a
    move-object/from16 v1, p1

    #@15c
    invoke-virtual {v0, v1, v6, v7}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeSummaryFromParcelLocked(Landroid/os/Parcel;J)V

    #@15f
    .line 5312
    move-object/from16 v0, p0

    #@161
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mGlobalWifiRunningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@163
    move-object/from16 v35, v0

    #@165
    move-object/from16 v0, v35

    #@167
    move-object/from16 v1, p1

    #@169
    invoke-virtual {v0, v1, v6, v7}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeSummaryFromParcelLocked(Landroid/os/Parcel;J)V

    #@16c
    .line 5313
    move-object/from16 v0, p0

    #@16e
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mBluetoothOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@170
    move-object/from16 v35, v0

    #@172
    move-object/from16 v0, v35

    #@174
    move-object/from16 v1, p1

    #@176
    invoke-virtual {v0, v1, v6, v7}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeSummaryFromParcelLocked(Landroid/os/Parcel;J)V

    #@179
    .line 5315
    move-object/from16 v0, p0

    #@17b
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mKernelWakelockStats:Ljava/util/HashMap;

    #@17d
    move-object/from16 v35, v0

    #@17f
    invoke-virtual/range {v35 .. v35}, Ljava/util/HashMap;->size()I

    #@182
    move-result v35

    #@183
    move-object/from16 v0, p1

    #@185
    move/from16 v1, v35

    #@187
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@18a
    .line 5316
    move-object/from16 v0, p0

    #@18c
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mKernelWakelockStats:Ljava/util/HashMap;

    #@18e
    move-object/from16 v35, v0

    #@190
    invoke-virtual/range {v35 .. v35}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@193
    move-result-object v35

    #@194
    invoke-interface/range {v35 .. v35}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@197
    move-result-object v23

    #@198
    .local v23, i$:Ljava/util/Iterator;
    :goto_198
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    #@19b
    move-result v35

    #@19c
    if-eqz v35, :cond_1da

    #@19e
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1a1
    move-result-object v18

    #@1a2
    check-cast v18, Ljava/util/Map$Entry;

    #@1a4
    .line 5317
    .local v18, ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;>;"
    invoke-interface/range {v18 .. v18}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@1a7
    move-result-object v26

    #@1a8
    check-cast v26, Lcom/android/internal/os/BatteryStatsImpl$Timer;

    #@1aa
    .line 5318
    .local v26, kwlt:Lcom/android/internal/os/BatteryStatsImpl$Timer;
    if-eqz v26, :cond_1d0

    #@1ac
    .line 5319
    const/16 v35, 0x1

    #@1ae
    move-object/from16 v0, p1

    #@1b0
    move/from16 v1, v35

    #@1b2
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@1b5
    .line 5320
    invoke-interface/range {v18 .. v18}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@1b8
    move-result-object v35

    #@1b9
    check-cast v35, Ljava/lang/String;

    #@1bb
    move-object/from16 v0, p1

    #@1bd
    move-object/from16 v1, v35

    #@1bf
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1c2
    .line 5321
    invoke-interface/range {v18 .. v18}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@1c5
    move-result-object v35

    #@1c6
    check-cast v35, Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;

    #@1c8
    move-object/from16 v0, v35

    #@1ca
    move-object/from16 v1, p1

    #@1cc
    invoke-virtual {v0, v1, v6, v7}, Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;->writeSummaryFromParcelLocked(Landroid/os/Parcel;J)V

    #@1cf
    goto :goto_198

    #@1d0
    .line 5323
    :cond_1d0
    const/16 v35, 0x0

    #@1d2
    move-object/from16 v0, p1

    #@1d4
    move/from16 v1, v35

    #@1d6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@1d9
    goto :goto_198

    #@1da
    .line 5327
    .end local v18           #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;>;"
    .end local v26           #kwlt:Lcom/android/internal/os/BatteryStatsImpl$Timer;
    :cond_1da
    sget v35, Lcom/android/internal/os/BatteryStatsImpl;->sNumSpeedSteps:I

    #@1dc
    move-object/from16 v0, p1

    #@1de
    move/from16 v1, v35

    #@1e0
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@1e3
    .line 5328
    move-object/from16 v0, p0

    #@1e5
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@1e7
    move-object/from16 v35, v0

    #@1e9
    invoke-virtual/range {v35 .. v35}, Landroid/util/SparseArray;->size()I

    #@1ec
    move-result v15

    #@1ed
    .line 5329
    .local v15, NU:I
    move-object/from16 v0, p1

    #@1ef
    invoke-virtual {v0, v15}, Landroid/os/Parcel;->writeInt(I)V

    #@1f2
    .line 5330
    const/16 v25, 0x0

    #@1f4
    .end local v23           #i$:Ljava/util/Iterator;
    .local v25, iu:I
    :goto_1f4
    move/from16 v0, v25

    #@1f6
    if-ge v0, v15, :cond_5f8

    #@1f8
    .line 5331
    move-object/from16 v0, p0

    #@1fa
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@1fc
    move-object/from16 v35, v0

    #@1fe
    move-object/from16 v0, v35

    #@200
    move/from16 v1, v25

    #@202
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    #@205
    move-result v35

    #@206
    move-object/from16 v0, p1

    #@208
    move/from16 v1, v35

    #@20a
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@20d
    .line 5332
    move-object/from16 v0, p0

    #@20f
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@211
    move-object/from16 v35, v0

    #@213
    move-object/from16 v0, v35

    #@215
    move/from16 v1, v25

    #@217
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@21a
    move-result-object v33

    #@21b
    check-cast v33, Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@21d
    .line 5334
    .local v33, u:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    move-object/from16 v0, v33

    #@21f
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiRunningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@221
    move-object/from16 v35, v0

    #@223
    if-eqz v35, :cond_37d

    #@225
    .line 5335
    const/16 v35, 0x1

    #@227
    move-object/from16 v0, p1

    #@229
    move/from16 v1, v35

    #@22b
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@22e
    .line 5336
    move-object/from16 v0, v33

    #@230
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiRunningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@232
    move-object/from16 v35, v0

    #@234
    move-object/from16 v0, v35

    #@236
    move-object/from16 v1, p1

    #@238
    invoke-virtual {v0, v1, v6, v7}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeSummaryFromParcelLocked(Landroid/os/Parcel;J)V

    #@23b
    .line 5340
    :goto_23b
    move-object/from16 v0, v33

    #@23d
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mFullWifiLockTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@23f
    move-object/from16 v35, v0

    #@241
    if-eqz v35, :cond_388

    #@243
    .line 5341
    const/16 v35, 0x1

    #@245
    move-object/from16 v0, p1

    #@247
    move/from16 v1, v35

    #@249
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@24c
    .line 5342
    move-object/from16 v0, v33

    #@24e
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mFullWifiLockTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@250
    move-object/from16 v35, v0

    #@252
    move-object/from16 v0, v35

    #@254
    move-object/from16 v1, p1

    #@256
    invoke-virtual {v0, v1, v6, v7}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeSummaryFromParcelLocked(Landroid/os/Parcel;J)V

    #@259
    .line 5346
    :goto_259
    move-object/from16 v0, v33

    #@25b
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiScanTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@25d
    move-object/from16 v35, v0

    #@25f
    if-eqz v35, :cond_393

    #@261
    .line 5347
    const/16 v35, 0x1

    #@263
    move-object/from16 v0, p1

    #@265
    move/from16 v1, v35

    #@267
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@26a
    .line 5348
    move-object/from16 v0, v33

    #@26c
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiScanTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@26e
    move-object/from16 v35, v0

    #@270
    move-object/from16 v0, v35

    #@272
    move-object/from16 v1, p1

    #@274
    invoke-virtual {v0, v1, v6, v7}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeSummaryFromParcelLocked(Landroid/os/Parcel;J)V

    #@277
    .line 5352
    :goto_277
    move-object/from16 v0, v33

    #@279
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiMulticastTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@27b
    move-object/from16 v35, v0

    #@27d
    if-eqz v35, :cond_39e

    #@27f
    .line 5353
    const/16 v35, 0x1

    #@281
    move-object/from16 v0, p1

    #@283
    move/from16 v1, v35

    #@285
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@288
    .line 5354
    move-object/from16 v0, v33

    #@28a
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiMulticastTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@28c
    move-object/from16 v35, v0

    #@28e
    move-object/from16 v0, v35

    #@290
    move-object/from16 v1, p1

    #@292
    invoke-virtual {v0, v1, v6, v7}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeSummaryFromParcelLocked(Landroid/os/Parcel;J)V

    #@295
    .line 5358
    :goto_295
    move-object/from16 v0, v33

    #@297
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mAudioTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@299
    move-object/from16 v35, v0

    #@29b
    if-eqz v35, :cond_3a9

    #@29d
    .line 5359
    const/16 v35, 0x1

    #@29f
    move-object/from16 v0, p1

    #@2a1
    move/from16 v1, v35

    #@2a3
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@2a6
    .line 5360
    move-object/from16 v0, v33

    #@2a8
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mAudioTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2aa
    move-object/from16 v35, v0

    #@2ac
    move-object/from16 v0, v35

    #@2ae
    move-object/from16 v1, p1

    #@2b0
    invoke-virtual {v0, v1, v6, v7}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeSummaryFromParcelLocked(Landroid/os/Parcel;J)V

    #@2b3
    .line 5364
    :goto_2b3
    move-object/from16 v0, v33

    #@2b5
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mVideoTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2b7
    move-object/from16 v35, v0

    #@2b9
    if-eqz v35, :cond_3b4

    #@2bb
    .line 5365
    const/16 v35, 0x1

    #@2bd
    move-object/from16 v0, p1

    #@2bf
    move/from16 v1, v35

    #@2c1
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@2c4
    .line 5366
    move-object/from16 v0, v33

    #@2c6
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mVideoTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2c8
    move-object/from16 v35, v0

    #@2ca
    move-object/from16 v0, v35

    #@2cc
    move-object/from16 v1, p1

    #@2ce
    invoke-virtual {v0, v1, v6, v7}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeSummaryFromParcelLocked(Landroid/os/Parcel;J)V

    #@2d1
    .line 5371
    :goto_2d1
    move-object/from16 v0, v33

    #@2d3
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mUserActivityCounters:[Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@2d5
    move-object/from16 v35, v0

    #@2d7
    if-nez v35, :cond_3bf

    #@2d9
    .line 5372
    const/16 v35, 0x0

    #@2db
    move-object/from16 v0, p1

    #@2dd
    move/from16 v1, v35

    #@2df
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@2e2
    .line 5380
    :cond_2e2
    move-object/from16 v0, v33

    #@2e4
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWakelockStats:Ljava/util/HashMap;

    #@2e6
    move-object/from16 v35, v0

    #@2e8
    invoke-virtual/range {v35 .. v35}, Ljava/util/HashMap;->size()I

    #@2eb
    move-result v16

    #@2ec
    .line 5381
    .local v16, NW:I
    move-object/from16 v0, p1

    #@2ee
    move/from16 v1, v16

    #@2f0
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@2f3
    .line 5382
    if-lez v16, :cond_405

    #@2f5
    .line 5384
    move-object/from16 v0, v33

    #@2f7
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWakelockStats:Ljava/util/HashMap;

    #@2f9
    move-object/from16 v35, v0

    #@2fb
    invoke-virtual/range {v35 .. v35}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@2fe
    move-result-object v35

    #@2ff
    invoke-interface/range {v35 .. v35}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@302
    move-result-object v23

    #@303
    .restart local v23       #i$:Ljava/util/Iterator;
    :goto_303
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    #@306
    move-result v35

    #@307
    if-eqz v35, :cond_405

    #@309
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@30c
    move-result-object v21

    #@30d
    check-cast v21, Ljava/util/Map$Entry;

    #@30f
    .line 5385
    .local v21, ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;>;"
    invoke-interface/range {v21 .. v21}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@312
    move-result-object v35

    #@313
    check-cast v35, Ljava/lang/String;

    #@315
    move-object/from16 v0, p1

    #@317
    move-object/from16 v1, v35

    #@319
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@31c
    .line 5386
    invoke-interface/range {v21 .. v21}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@31f
    move-result-object v34

    #@320
    check-cast v34, Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;

    #@322
    .line 5387
    .local v34, wl:Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;
    move-object/from16 v0, v34

    #@324
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;->mTimerFull:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@326
    move-object/from16 v35, v0

    #@328
    if-eqz v35, :cond_3e4

    #@32a
    .line 5388
    const/16 v35, 0x1

    #@32c
    move-object/from16 v0, p1

    #@32e
    move/from16 v1, v35

    #@330
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@333
    .line 5389
    move-object/from16 v0, v34

    #@335
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;->mTimerFull:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@337
    move-object/from16 v35, v0

    #@339
    move-object/from16 v0, v35

    #@33b
    move-object/from16 v1, p1

    #@33d
    invoke-virtual {v0, v1, v6, v7}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeSummaryFromParcelLocked(Landroid/os/Parcel;J)V

    #@340
    .line 5393
    :goto_340
    move-object/from16 v0, v34

    #@342
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;->mTimerPartial:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@344
    move-object/from16 v35, v0

    #@346
    if-eqz v35, :cond_3ef

    #@348
    .line 5394
    const/16 v35, 0x1

    #@34a
    move-object/from16 v0, p1

    #@34c
    move/from16 v1, v35

    #@34e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@351
    .line 5395
    move-object/from16 v0, v34

    #@353
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;->mTimerPartial:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@355
    move-object/from16 v35, v0

    #@357
    move-object/from16 v0, v35

    #@359
    move-object/from16 v1, p1

    #@35b
    invoke-virtual {v0, v1, v6, v7}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeSummaryFromParcelLocked(Landroid/os/Parcel;J)V

    #@35e
    .line 5399
    :goto_35e
    move-object/from16 v0, v34

    #@360
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;->mTimerWindow:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@362
    move-object/from16 v35, v0

    #@364
    if-eqz v35, :cond_3fa

    #@366
    .line 5400
    const/16 v35, 0x1

    #@368
    move-object/from16 v0, p1

    #@36a
    move/from16 v1, v35

    #@36c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@36f
    .line 5401
    move-object/from16 v0, v34

    #@371
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;->mTimerWindow:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@373
    move-object/from16 v35, v0

    #@375
    move-object/from16 v0, v35

    #@377
    move-object/from16 v1, p1

    #@379
    invoke-virtual {v0, v1, v6, v7}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeSummaryFromParcelLocked(Landroid/os/Parcel;J)V

    #@37c
    goto :goto_303

    #@37d
    .line 5338
    .end local v16           #NW:I
    .end local v21           #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;>;"
    .end local v23           #i$:Ljava/util/Iterator;
    .end local v34           #wl:Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;
    :cond_37d
    const/16 v35, 0x0

    #@37f
    move-object/from16 v0, p1

    #@381
    move/from16 v1, v35

    #@383
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@386
    goto/16 :goto_23b

    #@388
    .line 5344
    :cond_388
    const/16 v35, 0x0

    #@38a
    move-object/from16 v0, p1

    #@38c
    move/from16 v1, v35

    #@38e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@391
    goto/16 :goto_259

    #@393
    .line 5350
    :cond_393
    const/16 v35, 0x0

    #@395
    move-object/from16 v0, p1

    #@397
    move/from16 v1, v35

    #@399
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@39c
    goto/16 :goto_277

    #@39e
    .line 5356
    :cond_39e
    const/16 v35, 0x0

    #@3a0
    move-object/from16 v0, p1

    #@3a2
    move/from16 v1, v35

    #@3a4
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@3a7
    goto/16 :goto_295

    #@3a9
    .line 5362
    :cond_3a9
    const/16 v35, 0x0

    #@3ab
    move-object/from16 v0, p1

    #@3ad
    move/from16 v1, v35

    #@3af
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@3b2
    goto/16 :goto_2b3

    #@3b4
    .line 5368
    :cond_3b4
    const/16 v35, 0x0

    #@3b6
    move-object/from16 v0, p1

    #@3b8
    move/from16 v1, v35

    #@3ba
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@3bd
    goto/16 :goto_2d1

    #@3bf
    .line 5374
    :cond_3bf
    const/16 v35, 0x1

    #@3c1
    move-object/from16 v0, p1

    #@3c3
    move/from16 v1, v35

    #@3c5
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@3c8
    .line 5375
    const/16 v22, 0x0

    #@3ca
    :goto_3ca
    const/16 v35, 0x3

    #@3cc
    move/from16 v0, v22

    #@3ce
    move/from16 v1, v35

    #@3d0
    if-ge v0, v1, :cond_2e2

    #@3d2
    .line 5376
    move-object/from16 v0, v33

    #@3d4
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mUserActivityCounters:[Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@3d6
    move-object/from16 v35, v0

    #@3d8
    aget-object v35, v35, v22

    #@3da
    move-object/from16 v0, v35

    #@3dc
    move-object/from16 v1, p1

    #@3de
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$Counter;->writeSummaryFromParcelLocked(Landroid/os/Parcel;)V

    #@3e1
    .line 5375
    add-int/lit8 v22, v22, 0x1

    #@3e3
    goto :goto_3ca

    #@3e4
    .line 5391
    .restart local v16       #NW:I
    .restart local v21       #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;>;"
    .restart local v23       #i$:Ljava/util/Iterator;
    .restart local v34       #wl:Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;
    :cond_3e4
    const/16 v35, 0x0

    #@3e6
    move-object/from16 v0, p1

    #@3e8
    move/from16 v1, v35

    #@3ea
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@3ed
    goto/16 :goto_340

    #@3ef
    .line 5397
    :cond_3ef
    const/16 v35, 0x0

    #@3f1
    move-object/from16 v0, p1

    #@3f3
    move/from16 v1, v35

    #@3f5
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@3f8
    goto/16 :goto_35e

    #@3fa
    .line 5403
    :cond_3fa
    const/16 v35, 0x0

    #@3fc
    move-object/from16 v0, p1

    #@3fe
    move/from16 v1, v35

    #@400
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@403
    goto/16 :goto_303

    #@405
    .line 5408
    .end local v21           #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;>;"
    .end local v23           #i$:Ljava/util/Iterator;
    .end local v34           #wl:Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;
    :cond_405
    move-object/from16 v0, v33

    #@407
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mSensorStats:Ljava/util/HashMap;

    #@409
    move-object/from16 v35, v0

    #@40b
    invoke-virtual/range {v35 .. v35}, Ljava/util/HashMap;->size()I

    #@40e
    move-result v14

    #@40f
    .line 5409
    .local v14, NSE:I
    move-object/from16 v0, p1

    #@411
    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeInt(I)V

    #@414
    .line 5410
    if-lez v14, :cond_470

    #@416
    .line 5412
    move-object/from16 v0, v33

    #@418
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mSensorStats:Ljava/util/HashMap;

    #@41a
    move-object/from16 v35, v0

    #@41c
    invoke-virtual/range {v35 .. v35}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@41f
    move-result-object v35

    #@420
    invoke-interface/range {v35 .. v35}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@423
    move-result-object v23

    #@424
    .restart local v23       #i$:Ljava/util/Iterator;
    :goto_424
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    #@427
    move-result v35

    #@428
    if-eqz v35, :cond_470

    #@42a
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@42d
    move-result-object v17

    #@42e
    check-cast v17, Ljava/util/Map$Entry;

    #@430
    .line 5413
    .local v17, ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;>;"
    invoke-interface/range {v17 .. v17}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@433
    move-result-object v35

    #@434
    check-cast v35, Ljava/lang/Integer;

    #@436
    invoke-virtual/range {v35 .. v35}, Ljava/lang/Integer;->intValue()I

    #@439
    move-result v35

    #@43a
    move-object/from16 v0, p1

    #@43c
    move/from16 v1, v35

    #@43e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@441
    .line 5414
    invoke-interface/range {v17 .. v17}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@444
    move-result-object v28

    #@445
    check-cast v28, Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;

    #@447
    .line 5415
    .local v28, se:Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;
    move-object/from16 v0, v28

    #@449
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;->mTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@44b
    move-object/from16 v35, v0

    #@44d
    if-eqz v35, :cond_466

    #@44f
    .line 5416
    const/16 v35, 0x1

    #@451
    move-object/from16 v0, p1

    #@453
    move/from16 v1, v35

    #@455
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@458
    .line 5417
    move-object/from16 v0, v28

    #@45a
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;->mTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@45c
    move-object/from16 v35, v0

    #@45e
    move-object/from16 v0, v35

    #@460
    move-object/from16 v1, p1

    #@462
    invoke-virtual {v0, v1, v6, v7}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeSummaryFromParcelLocked(Landroid/os/Parcel;J)V

    #@465
    goto :goto_424

    #@466
    .line 5419
    :cond_466
    const/16 v35, 0x0

    #@468
    move-object/from16 v0, p1

    #@46a
    move/from16 v1, v35

    #@46c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@46f
    goto :goto_424

    #@470
    .line 5424
    .end local v17           #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;>;"
    .end local v23           #i$:Ljava/util/Iterator;
    .end local v28           #se:Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;
    :cond_470
    move-object/from16 v0, v33

    #@472
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mProcessStats:Ljava/util/HashMap;

    #@474
    move-object/from16 v35, v0

    #@476
    invoke-virtual/range {v35 .. v35}, Ljava/util/HashMap;->size()I

    #@479
    move-result v12

    #@47a
    .line 5425
    .local v12, NP:I
    move-object/from16 v0, p1

    #@47c
    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeInt(I)V

    #@47f
    .line 5426
    if-lez v12, :cond_521

    #@481
    .line 5428
    move-object/from16 v0, v33

    #@483
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mProcessStats:Ljava/util/HashMap;

    #@485
    move-object/from16 v35, v0

    #@487
    invoke-virtual/range {v35 .. v35}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@48a
    move-result-object v35

    #@48b
    invoke-interface/range {v35 .. v35}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@48e
    move-result-object v23

    #@48f
    .restart local v23       #i$:Ljava/util/Iterator;
    :goto_48f
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    #@492
    move-result v35

    #@493
    if-eqz v35, :cond_521

    #@495
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@498
    move-result-object v20

    #@499
    check-cast v20, Ljava/util/Map$Entry;

    #@49b
    .line 5429
    .local v20, ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@49e
    move-result-object v35

    #@49f
    check-cast v35, Ljava/lang/String;

    #@4a1
    move-object/from16 v0, p1

    #@4a3
    move-object/from16 v1, v35

    #@4a5
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@4a8
    .line 5430
    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@4ab
    move-result-object v27

    #@4ac
    check-cast v27, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;

    #@4ae
    .line 5431
    .local v27, ps:Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;
    move-object/from16 v0, v27

    #@4b0
    iget-wide v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mUserTime:J

    #@4b2
    move-wide/from16 v35, v0

    #@4b4
    move-object/from16 v0, p1

    #@4b6
    move-wide/from16 v1, v35

    #@4b8
    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@4bb
    .line 5432
    move-object/from16 v0, v27

    #@4bd
    iget-wide v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSystemTime:J

    #@4bf
    move-wide/from16 v35, v0

    #@4c1
    move-object/from16 v0, p1

    #@4c3
    move-wide/from16 v1, v35

    #@4c5
    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@4c8
    .line 5433
    move-object/from16 v0, v27

    #@4ca
    iget v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mStarts:I

    #@4cc
    move/from16 v35, v0

    #@4ce
    move-object/from16 v0, p1

    #@4d0
    move/from16 v1, v35

    #@4d2
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@4d5
    .line 5434
    move-object/from16 v0, v27

    #@4d7
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSpeedBins:[Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@4d9
    move-object/from16 v35, v0

    #@4db
    move-object/from16 v0, v35

    #@4dd
    array-length v3, v0

    #@4de
    .line 5435
    .local v3, N:I
    move-object/from16 v0, p1

    #@4e0
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@4e3
    .line 5436
    const/16 v22, 0x0

    #@4e5
    :goto_4e5
    move/from16 v0, v22

    #@4e7
    if-ge v0, v3, :cond_518

    #@4e9
    .line 5437
    move-object/from16 v0, v27

    #@4eb
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSpeedBins:[Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@4ed
    move-object/from16 v35, v0

    #@4ef
    aget-object v35, v35, v22

    #@4f1
    if-eqz v35, :cond_50e

    #@4f3
    .line 5438
    const/16 v35, 0x1

    #@4f5
    move-object/from16 v0, p1

    #@4f7
    move/from16 v1, v35

    #@4f9
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@4fc
    .line 5439
    move-object/from16 v0, v27

    #@4fe
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->mSpeedBins:[Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;

    #@500
    move-object/from16 v35, v0

    #@502
    aget-object v35, v35, v22

    #@504
    move-object/from16 v0, v35

    #@506
    move-object/from16 v1, p1

    #@508
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$SamplingCounter;->writeSummaryFromParcelLocked(Landroid/os/Parcel;)V

    #@50b
    .line 5436
    :goto_50b
    add-int/lit8 v22, v22, 0x1

    #@50d
    goto :goto_4e5

    #@50e
    .line 5441
    :cond_50e
    const/16 v35, 0x0

    #@510
    move-object/from16 v0, p1

    #@512
    move/from16 v1, v35

    #@514
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@517
    goto :goto_50b

    #@518
    .line 5444
    :cond_518
    move-object/from16 v0, v27

    #@51a
    move-object/from16 v1, p1

    #@51c
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->writeExcessivePowerToParcelLocked(Landroid/os/Parcel;)V

    #@51f
    goto/16 :goto_48f

    #@521
    .line 5448
    .end local v3           #N:I
    .end local v20           #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;>;"
    .end local v23           #i$:Ljava/util/Iterator;
    .end local v27           #ps:Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;
    :cond_521
    move-object/from16 v0, v33

    #@523
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mPackageStats:Ljava/util/HashMap;

    #@525
    move-object/from16 v35, v0

    #@527
    invoke-virtual/range {v35 .. v35}, Ljava/util/HashMap;->size()I

    #@52a
    move-result v12

    #@52b
    .line 5449
    move-object/from16 v0, p1

    #@52d
    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeInt(I)V

    #@530
    .line 5450
    if-lez v12, :cond_5d2

    #@532
    .line 5452
    move-object/from16 v0, v33

    #@534
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mPackageStats:Ljava/util/HashMap;

    #@536
    move-object/from16 v35, v0

    #@538
    invoke-virtual/range {v35 .. v35}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@53b
    move-result-object v35

    #@53c
    invoke-interface/range {v35 .. v35}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@53f
    move-result-object v23

    #@540
    :cond_540
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    #@543
    move-result v35

    #@544
    if-eqz v35, :cond_5d2

    #@546
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@549
    move-result-object v19

    #@54a
    check-cast v19, Ljava/util/Map$Entry;

    #@54c
    .line 5453
    .local v19, ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;>;"
    invoke-interface/range {v19 .. v19}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@54f
    move-result-object v35

    #@550
    check-cast v35, Ljava/lang/String;

    #@552
    move-object/from16 v0, p1

    #@554
    move-object/from16 v1, v35

    #@556
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@559
    .line 5454
    invoke-interface/range {v19 .. v19}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@55c
    move-result-object v27

    #@55d
    check-cast v27, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;

    #@55f
    .line 5455
    .local v27, ps:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;
    move-object/from16 v0, v27

    #@561
    iget v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;->mWakeups:I

    #@563
    move/from16 v35, v0

    #@565
    move-object/from16 v0, p1

    #@567
    move/from16 v1, v35

    #@569
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@56c
    .line 5456
    move-object/from16 v0, v27

    #@56e
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;->mServiceStats:Ljava/util/HashMap;

    #@570
    move-object/from16 v35, v0

    #@572
    invoke-virtual/range {v35 .. v35}, Ljava/util/HashMap;->size()I

    #@575
    move-result v13

    #@576
    .line 5457
    .local v13, NS:I
    move-object/from16 v0, p1

    #@578
    invoke-virtual {v0, v13}, Landroid/os/Parcel;->writeInt(I)V

    #@57b
    .line 5458
    if-lez v13, :cond_540

    #@57d
    .line 5460
    move-object/from16 v0, v27

    #@57f
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;->mServiceStats:Ljava/util/HashMap;

    #@581
    move-object/from16 v35, v0

    #@583
    invoke-virtual/range {v35 .. v35}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@586
    move-result-object v35

    #@587
    invoke-interface/range {v35 .. v35}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@58a
    move-result-object v24

    #@58b
    .local v24, i$:Ljava/util/Iterator;
    :goto_58b
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    #@58e
    move-result v35

    #@58f
    if-eqz v35, :cond_540

    #@591
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@594
    move-result-object v29

    #@595
    check-cast v29, Ljava/util/Map$Entry;

    #@597
    .line 5461
    .local v29, sent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;>;"
    invoke-interface/range {v29 .. v29}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@59a
    move-result-object v35

    #@59b
    check-cast v35, Ljava/lang/String;

    #@59d
    move-object/from16 v0, p1

    #@59f
    move-object/from16 v1, v35

    #@5a1
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5a4
    .line 5462
    invoke-interface/range {v29 .. v29}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@5a7
    move-result-object v30

    #@5a8
    check-cast v30, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;

    #@5aa
    .line 5463
    .local v30, ss:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;
    move-object/from16 v0, v30

    #@5ac
    invoke-virtual {v0, v4, v5}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;->getStartTimeToNowLocked(J)J

    #@5af
    move-result-wide v31

    #@5b0
    .line 5464
    .local v31, time:J
    move-object/from16 v0, p1

    #@5b2
    move-wide/from16 v1, v31

    #@5b4
    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@5b7
    .line 5465
    move-object/from16 v0, v30

    #@5b9
    iget v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;->mStarts:I

    #@5bb
    move/from16 v35, v0

    #@5bd
    move-object/from16 v0, p1

    #@5bf
    move/from16 v1, v35

    #@5c1
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@5c4
    .line 5466
    move-object/from16 v0, v30

    #@5c6
    iget v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;->mLaunches:I

    #@5c8
    move/from16 v35, v0

    #@5ca
    move-object/from16 v0, p1

    #@5cc
    move/from16 v1, v35

    #@5ce
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@5d1
    goto :goto_58b

    #@5d2
    .line 5472
    .end local v13           #NS:I
    .end local v19           #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;>;"
    .end local v24           #i$:Ljava/util/Iterator;
    .end local v27           #ps:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;
    .end local v29           #sent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;>;"
    .end local v30           #ss:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;
    .end local v31           #time:J
    :cond_5d2
    const/16 v35, 0x0

    #@5d4
    move-object/from16 v0, v33

    #@5d6
    move/from16 v1, v35

    #@5d8
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->getTcpBytesReceived(I)J

    #@5db
    move-result-wide v35

    #@5dc
    move-object/from16 v0, p1

    #@5de
    move-wide/from16 v1, v35

    #@5e0
    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@5e3
    .line 5473
    const/16 v35, 0x0

    #@5e5
    move-object/from16 v0, v33

    #@5e7
    move/from16 v1, v35

    #@5e9
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->getTcpBytesSent(I)J

    #@5ec
    move-result-wide v35

    #@5ed
    move-object/from16 v0, p1

    #@5ef
    move-wide/from16 v1, v35

    #@5f1
    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@5f4
    .line 5330
    add-int/lit8 v25, v25, 0x1

    #@5f6
    goto/16 :goto_1f4

    #@5f8
    .line 5475
    .end local v12           #NP:I
    .end local v14           #NSE:I
    .end local v16           #NW:I
    .end local v33           #u:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    :cond_5f8
    return-void
.end method

.method public writeSyncLocked()V
    .registers 2

    #@0
    .prologue
    .line 4857
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, v0}, Lcom/android/internal/os/BatteryStatsImpl;->writeLocked(Z)V

    #@4
    .line 4858
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    .line 5589
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, p1, v0, p2}, Lcom/android/internal/os/BatteryStatsImpl;->writeToParcelLocked(Landroid/os/Parcel;ZI)V

    #@4
    .line 5590
    return-void
.end method

.method writeToParcelLocked(Landroid/os/Parcel;ZI)V
    .registers 25
    .parameter "out"
    .parameter "inclUids"
    .parameter "flags"

    #@0
    .prologue
    .line 5599
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/os/BatteryStatsImpl;->updateKernelWakelocksLocked()V

    #@3
    .line 5601
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@6
    move-result-wide v17

    #@7
    const-wide/16 v19, 0x3e8

    #@9
    mul-long v14, v17, v19

    #@b
    .line 5602
    .local v14, uSecUptime:J
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@e
    move-result-wide v17

    #@f
    const-wide/16 v19, 0x3e8

    #@11
    mul-long v12, v17, v19

    #@13
    .line 5603
    .local v12, uSecRealtime:J
    move-object/from16 v0, p0

    #@15
    invoke-virtual {v0, v14, v15}, Lcom/android/internal/os/BatteryStatsImpl;->getBatteryUptimeLocked(J)J

    #@18
    move-result-wide v5

    #@19
    .line 5604
    .local v5, batteryUptime:J
    move-object/from16 v0, p0

    #@1b
    invoke-virtual {v0, v12, v13}, Lcom/android/internal/os/BatteryStatsImpl;->getBatteryRealtimeLocked(J)J

    #@1e
    move-result-wide v3

    #@1f
    .line 5606
    .local v3, batteryRealtime:J
    const v17, -0x458a8b8b

    #@22
    move-object/from16 v0, p1

    #@24
    move/from16 v1, v17

    #@26
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    .line 5608
    const/16 v17, 0x0

    #@2b
    move-object/from16 v0, p0

    #@2d
    move-object/from16 v1, p1

    #@2f
    move/from16 v2, v17

    #@31
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/os/BatteryStatsImpl;->writeHistory(Landroid/os/Parcel;Z)V

    #@34
    .line 5610
    move-object/from16 v0, p0

    #@36
    iget v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mStartCount:I

    #@38
    move/from16 v17, v0

    #@3a
    move-object/from16 v0, p1

    #@3c
    move/from16 v1, v17

    #@3e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@41
    .line 5611
    move-object/from16 v0, p0

    #@43
    iget-wide v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mBatteryUptime:J

    #@45
    move-wide/from16 v17, v0

    #@47
    move-object/from16 v0, p1

    #@49
    move-wide/from16 v1, v17

    #@4b
    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@4e
    .line 5612
    move-object/from16 v0, p0

    #@50
    iget-wide v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mBatteryRealtime:J

    #@52
    move-wide/from16 v17, v0

    #@54
    move-object/from16 v0, p1

    #@56
    move-wide/from16 v1, v17

    #@58
    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@5b
    .line 5613
    move-object/from16 v0, p0

    #@5d
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@5f
    move-object/from16 v17, v0

    #@61
    move-object/from16 v0, v17

    #@63
    move-object/from16 v1, p1

    #@65
    invoke-virtual {v0, v1, v3, v4}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeToParcel(Landroid/os/Parcel;J)V

    #@68
    .line 5614
    const/4 v8, 0x0

    #@69
    .local v8, i:I
    :goto_69
    const/16 v17, 0x5

    #@6b
    move/from16 v0, v17

    #@6d
    if-ge v8, v0, :cond_81

    #@6f
    .line 5615
    move-object/from16 v0, p0

    #@71
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mScreenBrightnessTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@73
    move-object/from16 v17, v0

    #@75
    aget-object v17, v17, v8

    #@77
    move-object/from16 v0, v17

    #@79
    move-object/from16 v1, p1

    #@7b
    invoke-virtual {v0, v1, v3, v4}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeToParcel(Landroid/os/Parcel;J)V

    #@7e
    .line 5614
    add-int/lit8 v8, v8, 0x1

    #@80
    goto :goto_69

    #@81
    .line 5617
    :cond_81
    move-object/from16 v0, p0

    #@83
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mInputEventCounter:Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@85
    move-object/from16 v17, v0

    #@87
    move-object/from16 v0, v17

    #@89
    move-object/from16 v1, p1

    #@8b
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$Counter;->writeToParcel(Landroid/os/Parcel;)V

    #@8e
    .line 5618
    move-object/from16 v0, p0

    #@90
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@92
    move-object/from16 v17, v0

    #@94
    move-object/from16 v0, v17

    #@96
    move-object/from16 v1, p1

    #@98
    invoke-virtual {v0, v1, v3, v4}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeToParcel(Landroid/os/Parcel;J)V

    #@9b
    .line 5619
    const/4 v8, 0x0

    #@9c
    :goto_9c
    sget v17, Landroid/telephony/SignalStrength;->NUM_SIGNAL_STRENGTH_BINS:I

    #@9e
    move/from16 v0, v17

    #@a0
    if-ge v8, v0, :cond_b4

    #@a2
    .line 5620
    move-object/from16 v0, p0

    #@a4
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalStrengthsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@a6
    move-object/from16 v17, v0

    #@a8
    aget-object v17, v17, v8

    #@aa
    move-object/from16 v0, v17

    #@ac
    move-object/from16 v1, p1

    #@ae
    invoke-virtual {v0, v1, v3, v4}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeToParcel(Landroid/os/Parcel;J)V

    #@b1
    .line 5619
    add-int/lit8 v8, v8, 0x1

    #@b3
    goto :goto_9c

    #@b4
    .line 5622
    :cond_b4
    move-object/from16 v0, p0

    #@b6
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneSignalScanningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@b8
    move-object/from16 v17, v0

    #@ba
    move-object/from16 v0, v17

    #@bc
    move-object/from16 v1, p1

    #@be
    invoke-virtual {v0, v1, v3, v4}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeToParcel(Landroid/os/Parcel;J)V

    #@c1
    .line 5623
    const/4 v8, 0x0

    #@c2
    :goto_c2
    const/16 v17, 0x10

    #@c4
    move/from16 v0, v17

    #@c6
    if-ge v8, v0, :cond_da

    #@c8
    .line 5624
    move-object/from16 v0, p0

    #@ca
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mPhoneDataConnectionsTimer:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@cc
    move-object/from16 v17, v0

    #@ce
    aget-object v17, v17, v8

    #@d0
    move-object/from16 v0, v17

    #@d2
    move-object/from16 v1, p1

    #@d4
    invoke-virtual {v0, v1, v3, v4}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeToParcel(Landroid/os/Parcel;J)V

    #@d7
    .line 5623
    add-int/lit8 v8, v8, 0x1

    #@d9
    goto :goto_c2

    #@da
    .line 5626
    :cond_da
    move-object/from16 v0, p0

    #@dc
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mWifiOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@de
    move-object/from16 v17, v0

    #@e0
    move-object/from16 v0, v17

    #@e2
    move-object/from16 v1, p1

    #@e4
    invoke-virtual {v0, v1, v3, v4}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeToParcel(Landroid/os/Parcel;J)V

    #@e7
    .line 5627
    move-object/from16 v0, p0

    #@e9
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mGlobalWifiRunningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@eb
    move-object/from16 v17, v0

    #@ed
    move-object/from16 v0, v17

    #@ef
    move-object/from16 v1, p1

    #@f1
    invoke-virtual {v0, v1, v3, v4}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeToParcel(Landroid/os/Parcel;J)V

    #@f4
    .line 5628
    move-object/from16 v0, p0

    #@f6
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mBluetoothOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@f8
    move-object/from16 v17, v0

    #@fa
    move-object/from16 v0, v17

    #@fc
    move-object/from16 v1, p1

    #@fe
    invoke-virtual {v0, v1, v3, v4}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeToParcel(Landroid/os/Parcel;J)V

    #@101
    .line 5629
    move-object/from16 v0, p0

    #@103
    iget-wide v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUptime:J

    #@105
    move-wide/from16 v17, v0

    #@107
    move-object/from16 v0, p1

    #@109
    move-wide/from16 v1, v17

    #@10b
    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@10e
    .line 5630
    move-object/from16 v0, p0

    #@110
    iget-wide v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUptimeStart:J

    #@112
    move-wide/from16 v17, v0

    #@114
    move-object/from16 v0, p1

    #@116
    move-wide/from16 v1, v17

    #@118
    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@11b
    .line 5631
    move-object/from16 v0, p0

    #@11d
    iget-wide v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mRealtime:J

    #@11f
    move-wide/from16 v17, v0

    #@121
    move-object/from16 v0, p1

    #@123
    move-wide/from16 v1, v17

    #@125
    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@128
    .line 5632
    move-object/from16 v0, p0

    #@12a
    iget-wide v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mRealtimeStart:J

    #@12c
    move-wide/from16 v17, v0

    #@12e
    move-object/from16 v0, p1

    #@130
    move-wide/from16 v1, v17

    #@132
    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@135
    .line 5633
    move-object/from16 v0, p0

    #@137
    iget-boolean v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mOnBattery:Z

    #@139
    move/from16 v17, v0

    #@13b
    if-eqz v17, :cond_2a4

    #@13d
    const/16 v17, 0x1

    #@13f
    :goto_13f
    move-object/from16 v0, p1

    #@141
    move/from16 v1, v17

    #@143
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@146
    .line 5634
    move-object/from16 v0, p1

    #@148
    invoke-virtual {v0, v5, v6}, Landroid/os/Parcel;->writeLong(J)V

    #@14b
    .line 5635
    move-object/from16 v0, p0

    #@14d
    iget-wide v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mTrackBatteryUptimeStart:J

    #@14f
    move-wide/from16 v17, v0

    #@151
    move-object/from16 v0, p1

    #@153
    move-wide/from16 v1, v17

    #@155
    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@158
    .line 5636
    move-object/from16 v0, p1

    #@15a
    invoke-virtual {v0, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    #@15d
    .line 5637
    move-object/from16 v0, p0

    #@15f
    iget-wide v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mTrackBatteryRealtimeStart:J

    #@161
    move-wide/from16 v17, v0

    #@163
    move-object/from16 v0, p1

    #@165
    move-wide/from16 v1, v17

    #@167
    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@16a
    .line 5638
    move-object/from16 v0, p0

    #@16c
    iget-wide v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggedBatteryUptime:J

    #@16e
    move-wide/from16 v17, v0

    #@170
    move-object/from16 v0, p1

    #@172
    move-wide/from16 v1, v17

    #@174
    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@177
    .line 5639
    move-object/from16 v0, p0

    #@179
    iget-wide v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggedBatteryRealtime:J

    #@17b
    move-wide/from16 v17, v0

    #@17d
    move-object/from16 v0, p1

    #@17f
    move-wide/from16 v1, v17

    #@181
    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@184
    .line 5640
    move-object/from16 v0, p0

    #@186
    iget v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeUnplugLevel:I

    #@188
    move/from16 v17, v0

    #@18a
    move-object/from16 v0, p1

    #@18c
    move/from16 v1, v17

    #@18e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@191
    .line 5641
    move-object/from16 v0, p0

    #@193
    iget v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeCurrentLevel:I

    #@195
    move/from16 v17, v0

    #@197
    move-object/from16 v0, p1

    #@199
    move/from16 v1, v17

    #@19b
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@19e
    .line 5642
    move-object/from16 v0, p0

    #@1a0
    iget v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mLowDischargeAmountSinceCharge:I

    #@1a2
    move/from16 v17, v0

    #@1a4
    move-object/from16 v0, p1

    #@1a6
    move/from16 v1, v17

    #@1a8
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@1ab
    .line 5643
    move-object/from16 v0, p0

    #@1ad
    iget v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mHighDischargeAmountSinceCharge:I

    #@1af
    move/from16 v17, v0

    #@1b1
    move-object/from16 v0, p1

    #@1b3
    move/from16 v1, v17

    #@1b5
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@1b8
    .line 5644
    move-object/from16 v0, p0

    #@1ba
    iget v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeAmountScreenOn:I

    #@1bc
    move/from16 v17, v0

    #@1be
    move-object/from16 v0, p1

    #@1c0
    move/from16 v1, v17

    #@1c2
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@1c5
    .line 5645
    move-object/from16 v0, p0

    #@1c7
    iget v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeAmountScreenOnSinceCharge:I

    #@1c9
    move/from16 v17, v0

    #@1cb
    move-object/from16 v0, p1

    #@1cd
    move/from16 v1, v17

    #@1cf
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@1d2
    .line 5646
    move-object/from16 v0, p0

    #@1d4
    iget v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeAmountScreenOff:I

    #@1d6
    move/from16 v17, v0

    #@1d8
    move-object/from16 v0, p1

    #@1da
    move/from16 v1, v17

    #@1dc
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@1df
    .line 5647
    move-object/from16 v0, p0

    #@1e1
    iget v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mDischargeAmountScreenOffSinceCharge:I

    #@1e3
    move/from16 v17, v0

    #@1e5
    move-object/from16 v0, p1

    #@1e7
    move/from16 v1, v17

    #@1e9
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@1ec
    .line 5648
    move-object/from16 v0, p0

    #@1ee
    iget-wide v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mLastWriteTime:J

    #@1f0
    move-wide/from16 v17, v0

    #@1f2
    move-object/from16 v0, p1

    #@1f4
    move-wide/from16 v1, v17

    #@1f6
    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@1f9
    .line 5650
    const/16 v17, 0x3

    #@1fb
    move-object/from16 v0, p0

    #@1fd
    move/from16 v1, v17

    #@1ff
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->getMobileTcpBytesReceived(I)J

    #@202
    move-result-wide v17

    #@203
    move-object/from16 v0, p1

    #@205
    move-wide/from16 v1, v17

    #@207
    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@20a
    .line 5651
    const/16 v17, 0x3

    #@20c
    move-object/from16 v0, p0

    #@20e
    move/from16 v1, v17

    #@210
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->getMobileTcpBytesSent(I)J

    #@213
    move-result-wide v17

    #@214
    move-object/from16 v0, p1

    #@216
    move-wide/from16 v1, v17

    #@218
    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@21b
    .line 5652
    const/16 v17, 0x3

    #@21d
    move-object/from16 v0, p0

    #@21f
    move/from16 v1, v17

    #@221
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->getTotalTcpBytesReceived(I)J

    #@224
    move-result-wide v17

    #@225
    move-object/from16 v0, p1

    #@227
    move-wide/from16 v1, v17

    #@229
    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@22c
    .line 5653
    const/16 v17, 0x3

    #@22e
    move-object/from16 v0, p0

    #@230
    move/from16 v1, v17

    #@232
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl;->getTotalTcpBytesSent(I)J

    #@235
    move-result-wide v17

    #@236
    move-object/from16 v0, p1

    #@238
    move-wide/from16 v1, v17

    #@23a
    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@23d
    .line 5656
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/os/BatteryStatsImpl;->getRadioDataUptime()J

    #@240
    move-result-wide v17

    #@241
    move-object/from16 v0, p1

    #@243
    move-wide/from16 v1, v17

    #@245
    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@248
    .line 5658
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/os/BatteryStatsImpl;->getBluetoothPingCount()I

    #@24b
    move-result v17

    #@24c
    move-object/from16 v0, p1

    #@24e
    move/from16 v1, v17

    #@250
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@253
    .line 5660
    if-eqz p2, :cond_2b2

    #@255
    .line 5661
    move-object/from16 v0, p0

    #@257
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mKernelWakelockStats:Ljava/util/HashMap;

    #@259
    move-object/from16 v17, v0

    #@25b
    invoke-virtual/range {v17 .. v17}, Ljava/util/HashMap;->size()I

    #@25e
    move-result v17

    #@25f
    move-object/from16 v0, p1

    #@261
    move/from16 v1, v17

    #@263
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@266
    .line 5662
    move-object/from16 v0, p0

    #@268
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mKernelWakelockStats:Ljava/util/HashMap;

    #@26a
    move-object/from16 v17, v0

    #@26c
    invoke-virtual/range {v17 .. v17}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@26f
    move-result-object v17

    #@270
    invoke-interface/range {v17 .. v17}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@273
    move-result-object v9

    #@274
    .local v9, i$:Ljava/util/Iterator;
    :goto_274
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    #@277
    move-result v17

    #@278
    if-eqz v17, :cond_2bb

    #@27a
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@27d
    move-result-object v7

    #@27e
    check-cast v7, Ljava/util/Map$Entry;

    #@280
    .line 5663
    .local v7, ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;>;"
    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@283
    move-result-object v10

    #@284
    check-cast v10, Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;

    #@286
    .line 5664
    .local v10, kwlt:Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;
    if-eqz v10, :cond_2a8

    #@288
    .line 5665
    const/16 v17, 0x1

    #@28a
    move-object/from16 v0, p1

    #@28c
    move/from16 v1, v17

    #@28e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@291
    .line 5666
    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@294
    move-result-object v17

    #@295
    check-cast v17, Ljava/lang/String;

    #@297
    move-object/from16 v0, p1

    #@299
    move-object/from16 v1, v17

    #@29b
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@29e
    .line 5667
    move-object/from16 v0, p1

    #@2a0
    invoke-static {v0, v10, v3, v4}, Lcom/android/internal/os/BatteryStatsImpl$Timer;->writeTimerToParcel(Landroid/os/Parcel;Lcom/android/internal/os/BatteryStatsImpl$Timer;J)V

    #@2a3
    goto :goto_274

    #@2a4
    .line 5633
    .end local v7           #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;>;"
    .end local v9           #i$:Ljava/util/Iterator;
    .end local v10           #kwlt:Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;
    :cond_2a4
    const/16 v17, 0x0

    #@2a6
    goto/16 :goto_13f

    #@2a8
    .line 5669
    .restart local v7       #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;>;"
    .restart local v9       #i$:Ljava/util/Iterator;
    .restart local v10       #kwlt:Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;
    :cond_2a8
    const/16 v17, 0x0

    #@2aa
    move-object/from16 v0, p1

    #@2ac
    move/from16 v1, v17

    #@2ae
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@2b1
    goto :goto_274

    #@2b2
    .line 5673
    .end local v7           #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;>;"
    .end local v9           #i$:Ljava/util/Iterator;
    .end local v10           #kwlt:Lcom/android/internal/os/BatteryStatsImpl$SamplingTimer;
    :cond_2b2
    const/16 v17, 0x0

    #@2b4
    move-object/from16 v0, p1

    #@2b6
    move/from16 v1, v17

    #@2b8
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@2bb
    .line 5676
    :cond_2bb
    sget v17, Lcom/android/internal/os/BatteryStatsImpl;->sNumSpeedSteps:I

    #@2bd
    move-object/from16 v0, p1

    #@2bf
    move/from16 v1, v17

    #@2c1
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@2c4
    .line 5678
    if-eqz p2, :cond_303

    #@2c6
    .line 5679
    move-object/from16 v0, p0

    #@2c8
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@2ca
    move-object/from16 v17, v0

    #@2cc
    invoke-virtual/range {v17 .. v17}, Landroid/util/SparseArray;->size()I

    #@2cf
    move-result v11

    #@2d0
    .line 5680
    .local v11, size:I
    move-object/from16 v0, p1

    #@2d2
    invoke-virtual {v0, v11}, Landroid/os/Parcel;->writeInt(I)V

    #@2d5
    .line 5681
    const/4 v8, 0x0

    #@2d6
    :goto_2d6
    if-ge v8, v11, :cond_30c

    #@2d8
    .line 5682
    move-object/from16 v0, p0

    #@2da
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@2dc
    move-object/from16 v17, v0

    #@2de
    move-object/from16 v0, v17

    #@2e0
    invoke-virtual {v0, v8}, Landroid/util/SparseArray;->keyAt(I)I

    #@2e3
    move-result v17

    #@2e4
    move-object/from16 v0, p1

    #@2e6
    move/from16 v1, v17

    #@2e8
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@2eb
    .line 5683
    move-object/from16 v0, p0

    #@2ed
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl;->mUidStats:Landroid/util/SparseArray;

    #@2ef
    move-object/from16 v17, v0

    #@2f1
    move-object/from16 v0, v17

    #@2f3
    invoke-virtual {v0, v8}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@2f6
    move-result-object v16

    #@2f7
    check-cast v16, Lcom/android/internal/os/BatteryStatsImpl$Uid;

    #@2f9
    .line 5685
    .local v16, uid:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    move-object/from16 v0, v16

    #@2fb
    move-object/from16 v1, p1

    #@2fd
    invoke-virtual {v0, v1, v3, v4}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->writeToParcelLocked(Landroid/os/Parcel;J)V

    #@300
    .line 5681
    add-int/lit8 v8, v8, 0x1

    #@302
    goto :goto_2d6

    #@303
    .line 5688
    .end local v11           #size:I
    .end local v16           #uid:Lcom/android/internal/os/BatteryStatsImpl$Uid;
    :cond_303
    const/16 v17, 0x0

    #@305
    move-object/from16 v0, p1

    #@307
    move/from16 v1, v17

    #@309
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@30c
    .line 5690
    :cond_30c
    return-void
.end method

.method public writeToParcelWithoutUids(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    .line 5593
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0, p2}, Lcom/android/internal/os/BatteryStatsImpl;->writeToParcelLocked(Landroid/os/Parcel;ZI)V

    #@4
    .line 5594
    return-void
.end method
