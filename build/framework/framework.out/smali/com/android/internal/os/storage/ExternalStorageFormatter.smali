.class public Lcom/android/internal/os/storage/ExternalStorageFormatter;
.super Landroid/app/Service;
.source "ExternalStorageFormatter.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# static fields
.field public static final COMPONENT_NAME:Landroid/content/ComponentName; = null

.field public static final EXTRA_ALWAYS_RESET:Ljava/lang/String; = "always_reset"

.field public static final FORMAT_AND_FACTORY_RESET:Ljava/lang/String; = "com.android.internal.os.storage.FORMAT_AND_FACTORY_RESET"

.field public static final FORMAT_ONLY:Ljava/lang/String; = "com.android.internal.os.storage.FORMAT_ONLY"

.field static final TAG:Ljava/lang/String; = "ExternalStorageFormatter"


# instance fields
.field private mAlwaysReset:Z

.field private mFactoryReset:Z

.field private mMountService:Landroid/os/storage/IMountService;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field mStorageListener:Landroid/os/storage/StorageEventListener;

.field private mStorageManager:Landroid/os/storage/StorageManager;

.field private mStorageVolume:Landroid/os/storage/StorageVolume;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 40
    new-instance v0, Landroid/content/ComponentName;

    #@2
    const-string v1, "android"

    #@4
    const-class v2, Lcom/android/internal/os/storage/ExternalStorageFormatter;

    #@6
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    sput-object v0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->COMPONENT_NAME:Landroid/content/ComponentName;

    #@f
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 28
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@5
    .line 44
    iput-object v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mMountService:Landroid/os/storage/IMountService;

    #@7
    .line 46
    iput-object v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mStorageManager:Landroid/os/storage/StorageManager;

    #@9
    .line 50
    iput-object v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mProgressDialog:Landroid/app/ProgressDialog;

    #@b
    .line 52
    iput-boolean v1, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mFactoryReset:Z

    #@d
    .line 53
    iput-boolean v1, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mAlwaysReset:Z

    #@f
    .line 55
    new-instance v0, Lcom/android/internal/os/storage/ExternalStorageFormatter$1;

    #@11
    invoke-direct {v0, p0}, Lcom/android/internal/os/storage/ExternalStorageFormatter$1;-><init>(Lcom/android/internal/os/storage/ExternalStorageFormatter;)V

    #@14
    iput-object v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mStorageListener:Landroid/os/storage/StorageEventListener;

    #@16
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/os/storage/ExternalStorageFormatter;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mFactoryReset:Z

    #@2
    return v0
.end method

.method static synthetic access$100(Lcom/android/internal/os/storage/ExternalStorageFormatter;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mAlwaysReset:Z

    #@2
    return v0
.end method


# virtual methods
.method fail(I)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 145
    const/4 v0, 0x1

    #@1
    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    #@4
    move-result-object v0

    #@5
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    #@8
    .line 146
    iget-boolean v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mAlwaysReset:Z

    #@a
    if-eqz v0, :cond_16

    #@c
    .line 147
    new-instance v0, Landroid/content/Intent;

    #@e
    const-string v1, "android.intent.action.MASTER_CLEAR"

    #@10
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@13
    invoke-virtual {p0, v0}, Lcom/android/internal/os/storage/ExternalStorageFormatter;->sendBroadcast(Landroid/content/Intent;)V

    #@16
    .line 149
    :cond_16
    invoke-virtual {p0}, Lcom/android/internal/os/storage/ExternalStorageFormatter;->stopSelf()V

    #@19
    .line 150
    return-void
.end method

.method getMountService()Landroid/os/storage/IMountService;
    .registers 4

    #@0
    .prologue
    .line 266
    iget-object v1, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mMountService:Landroid/os/storage/IMountService;

    #@2
    if-nez v1, :cond_13

    #@4
    .line 267
    const-string/jumbo v1, "mount"

    #@7
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@a
    move-result-object v0

    #@b
    .line 268
    .local v0, service:Landroid/os/IBinder;
    if-eqz v0, :cond_16

    #@d
    .line 269
    invoke-static {v0}, Landroid/os/storage/IMountService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/storage/IMountService;

    #@10
    move-result-object v1

    #@11
    iput-object v1, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mMountService:Landroid/os/storage/IMountService;

    #@13
    .line 274
    .end local v0           #service:Landroid/os/IBinder;
    :cond_13
    :goto_13
    iget-object v1, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mMountService:Landroid/os/storage/IMountService;

    #@15
    return-object v1

    #@16
    .line 271
    .restart local v0       #service:Landroid/os/IBinder;
    :cond_16
    const-string v1, "ExternalStorageFormatter"

    #@18
    const-string v2, "Can\'t get mount service"

    #@1a
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    goto :goto_13
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 127
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .registers 7
    .parameter "dialog"

    #@0
    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/android/internal/os/storage/ExternalStorageFormatter;->getMountService()Landroid/os/storage/IMountService;

    #@3
    move-result-object v2

    #@4
    .line 133
    .local v2, mountService:Landroid/os/storage/IMountService;
    iget-object v3, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mStorageVolume:Landroid/os/storage/StorageVolume;

    #@6
    if-nez v3, :cond_17

    #@8
    invoke-static {}, Landroid/os/Environment;->getLegacyExternalStorageDirectory()Ljava/io/File;

    #@b
    move-result-object v3

    #@c
    invoke-virtual {v3}, Ljava/io/File;->toString()Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    .line 137
    .local v1, extStoragePath:Ljava/lang/String;
    :goto_10
    :try_start_10
    invoke-interface {v2, v1}, Landroid/os/storage/IMountService;->mountVolume(Ljava/lang/String;)I
    :try_end_13
    .catch Landroid/os/RemoteException; {:try_start_10 .. :try_end_13} :catch_1e

    #@13
    .line 141
    :goto_13
    invoke-virtual {p0}, Lcom/android/internal/os/storage/ExternalStorageFormatter;->stopSelf()V

    #@16
    .line 142
    return-void

    #@17
    .line 133
    .end local v1           #extStoragePath:Ljava/lang/String;
    :cond_17
    iget-object v3, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mStorageVolume:Landroid/os/storage/StorageVolume;

    #@19
    invoke-virtual {v3}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    goto :goto_10

    #@1e
    .line 138
    .restart local v1       #extStoragePath:Ljava/lang/String;
    :catch_1e
    move-exception v0

    #@1f
    .line 139
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "ExternalStorageFormatter"

    #@21
    const-string v4, "Failed talking with mount service"

    #@23
    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@26
    goto :goto_13
.end method

.method public onCreate()V
    .registers 4

    #@0
    .prologue
    .line 67
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    #@3
    .line 69
    iget-object v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mStorageManager:Landroid/os/storage/StorageManager;

    #@5
    if-nez v0, :cond_19

    #@7
    .line 70
    const-string/jumbo v0, "storage"

    #@a
    invoke-virtual {p0, v0}, Lcom/android/internal/os/storage/ExternalStorageFormatter;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Landroid/os/storage/StorageManager;

    #@10
    iput-object v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mStorageManager:Landroid/os/storage/StorageManager;

    #@12
    .line 71
    iget-object v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mStorageManager:Landroid/os/storage/StorageManager;

    #@14
    iget-object v1, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mStorageListener:Landroid/os/storage/StorageEventListener;

    #@16
    invoke-virtual {v0, v1}, Landroid/os/storage/StorageManager;->registerListener(Landroid/os/storage/StorageEventListener;)V

    #@19
    .line 74
    :cond_19
    const-string/jumbo v0, "power"

    #@1c
    invoke-virtual {p0, v0}, Lcom/android/internal/os/storage/ExternalStorageFormatter;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Landroid/os/PowerManager;

    #@22
    const/4 v1, 0x1

    #@23
    const-string v2, "ExternalStorageFormatter"

    #@25
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@28
    move-result-object v0

    #@29
    iput-object v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2b
    .line 76
    iget-object v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2d
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@30
    .line 77
    return-void
.end method

.method public onDestroy()V
    .registers 3

    #@0
    .prologue
    .line 115
    iget-object v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mStorageManager:Landroid/os/storage/StorageManager;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 116
    iget-object v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mStorageManager:Landroid/os/storage/StorageManager;

    #@6
    iget-object v1, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mStorageListener:Landroid/os/storage/StorageEventListener;

    #@8
    invoke-virtual {v0, v1}, Landroid/os/storage/StorageManager;->unregisterListener(Landroid/os/storage/StorageEventListener;)V

    #@b
    .line 118
    :cond_b
    iget-object v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mProgressDialog:Landroid/app/ProgressDialog;

    #@d
    if-eqz v0, :cond_14

    #@f
    .line 119
    iget-object v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mProgressDialog:Landroid/app/ProgressDialog;

    #@11
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    #@14
    .line 121
    :cond_14
    iget-object v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@16
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@19
    .line 122
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    #@1c
    .line 123
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .registers 9
    .parameter "intent"
    .parameter "flags"
    .parameter "startId"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 82
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@5
    move-result-object v1

    #@6
    if-eqz v1, :cond_19

    #@8
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@b
    move-result-object v1

    #@c
    const/4 v2, 0x0

    #@d
    const-string v3, "LGMDMWiperAdapter"

    #@f
    invoke-interface {v1, v2, v3}, Lcom/lge/cappuccino/IMdm;->checkDisabledSystemService(Landroid/content/ComponentName;Ljava/lang/String;)Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_19

    #@15
    .line 85
    invoke-virtual {p0}, Lcom/android/internal/os/storage/ExternalStorageFormatter;->stopSelf()V

    #@18
    .line 110
    :goto_18
    return v0

    #@19
    .line 89
    :cond_19
    const-string v1, "com.android.internal.os.storage.FORMAT_AND_FACTORY_RESET"

    #@1b
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v1

    #@23
    if-eqz v1, :cond_27

    #@25
    .line 90
    iput-boolean v4, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mFactoryReset:Z

    #@27
    .line 92
    :cond_27
    const-string v1, "always_reset"

    #@29
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@2c
    move-result v0

    #@2d
    if-eqz v0, :cond_31

    #@2f
    .line 93
    iput-boolean v4, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mAlwaysReset:Z

    #@31
    .line 96
    :cond_31
    const-string/jumbo v0, "storage_volume"

    #@34
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@37
    move-result-object v0

    #@38
    check-cast v0, Landroid/os/storage/StorageVolume;

    #@3a
    iput-object v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mStorageVolume:Landroid/os/storage/StorageVolume;

    #@3c
    .line 98
    iget-object v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mProgressDialog:Landroid/app/ProgressDialog;

    #@3e
    if-nez v0, :cond_6d

    #@40
    .line 99
    new-instance v0, Landroid/app/ProgressDialog;

    #@42
    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    #@45
    iput-object v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mProgressDialog:Landroid/app/ProgressDialog;

    #@47
    .line 100
    iget-object v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mProgressDialog:Landroid/app/ProgressDialog;

    #@49
    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    #@4c
    .line 101
    iget-object v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mProgressDialog:Landroid/app/ProgressDialog;

    #@4e
    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    #@51
    .line 102
    iget-object v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mProgressDialog:Landroid/app/ProgressDialog;

    #@53
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    #@56
    move-result-object v0

    #@57
    const/16 v1, 0x7d3

    #@59
    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    #@5c
    .line 103
    iget-boolean v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mAlwaysReset:Z

    #@5e
    if-nez v0, :cond_65

    #@60
    .line 104
    iget-object v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mProgressDialog:Landroid/app/ProgressDialog;

    #@62
    invoke-virtual {v0, p0}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    #@65
    .line 106
    :cond_65
    invoke-virtual {p0}, Lcom/android/internal/os/storage/ExternalStorageFormatter;->updateProgressState()V

    #@68
    .line 107
    iget-object v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mProgressDialog:Landroid/app/ProgressDialog;

    #@6a
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    #@6d
    .line 110
    :cond_6d
    const/4 v0, 0x3

    #@6e
    goto :goto_18
.end method

.method public updateProgressDialog(I)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 254
    iget-object v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mProgressDialog:Landroid/app/ProgressDialog;

    #@2
    if-nez v0, :cond_27

    #@4
    .line 255
    new-instance v0, Landroid/app/ProgressDialog;

    #@6
    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    #@9
    iput-object v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mProgressDialog:Landroid/app/ProgressDialog;

    #@b
    .line 256
    iget-object v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mProgressDialog:Landroid/app/ProgressDialog;

    #@d
    const/4 v1, 0x1

    #@e
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    #@11
    .line 257
    iget-object v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mProgressDialog:Landroid/app/ProgressDialog;

    #@13
    const/4 v1, 0x0

    #@14
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    #@17
    .line 258
    iget-object v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mProgressDialog:Landroid/app/ProgressDialog;

    #@19
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    #@1c
    move-result-object v0

    #@1d
    const/16 v1, 0x7d3

    #@1f
    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    #@22
    .line 259
    iget-object v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mProgressDialog:Landroid/app/ProgressDialog;

    #@24
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    #@27
    .line 262
    :cond_27
    iget-object v0, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mProgressDialog:Landroid/app/ProgressDialog;

    #@29
    invoke-virtual {p0, p1}, Lcom/android/internal/os/storage/ExternalStorageFormatter;->getText(I)Ljava/lang/CharSequence;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    #@30
    .line 263
    return-void
.end method

.method updateProgressState()V
    .registers 9

    #@0
    .prologue
    .line 153
    iget-object v5, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mStorageVolume:Landroid/os/storage/StorageVolume;

    #@2
    if-nez v5, :cond_4a

    #@4
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    #@7
    move-result-object v4

    #@8
    .line 157
    .local v4, status:Ljava/lang/String;
    :goto_8
    const/4 v2, 0x0

    #@9
    .line 158
    .local v2, isUSBMemory:Z
    iget-object v5, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mStorageVolume:Landroid/os/storage/StorageVolume;

    #@b
    if-eqz v5, :cond_19

    #@d
    .line 159
    iget-object v5, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mStorageVolume:Landroid/os/storage/StorageVolume;

    #@f
    invoke-virtual {v5}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    #@12
    move-result-object v5

    #@13
    const-string v6, "/storage/USBstorage"

    #@15
    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@18
    move-result v2

    #@19
    .line 162
    :cond_19
    const-string/jumbo v5, "mounted"

    #@1c
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v5

    #@20
    if-nez v5, :cond_2b

    #@22
    const-string/jumbo v5, "mounted_ro"

    #@25
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v5

    #@29
    if-eqz v5, :cond_6e

    #@2b
    .line 164
    :cond_2b
    if-eqz v2, :cond_57

    #@2d
    .line 165
    const v5, 0x20902f1

    #@30
    invoke-virtual {p0, v5}, Lcom/android/internal/os/storage/ExternalStorageFormatter;->updateProgressDialog(I)V

    #@33
    .line 169
    :goto_33
    invoke-virtual {p0}, Lcom/android/internal/os/storage/ExternalStorageFormatter;->getMountService()Landroid/os/storage/IMountService;

    #@36
    move-result-object v3

    #@37
    .line 170
    .local v3, mountService:Landroid/os/storage/IMountService;
    iget-object v5, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mStorageVolume:Landroid/os/storage/StorageVolume;

    #@39
    if-nez v5, :cond_5e

    #@3b
    invoke-static {}, Landroid/os/Environment;->getLegacyExternalStorageDirectory()Ljava/io/File;

    #@3e
    move-result-object v5

    #@3f
    invoke-virtual {v5}, Ljava/io/File;->toString()Ljava/lang/String;

    #@42
    move-result-object v1

    #@43
    .line 175
    .local v1, extStoragePath:Ljava/lang/String;
    :goto_43
    const/4 v5, 0x1

    #@44
    :try_start_44
    iget-boolean v6, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mFactoryReset:Z

    #@46
    invoke-interface {v3, v1, v5, v6}, Landroid/os/storage/IMountService;->unmountVolume(Ljava/lang/String;ZZ)V
    :try_end_49
    .catch Landroid/os/RemoteException; {:try_start_44 .. :try_end_49} :catch_65

    #@49
    .line 251
    .end local v1           #extStoragePath:Ljava/lang/String;
    .end local v3           #mountService:Landroid/os/storage/IMountService;
    :goto_49
    return-void

    #@4a
    .line 153
    .end local v2           #isUSBMemory:Z
    .end local v4           #status:Ljava/lang/String;
    :cond_4a
    iget-object v5, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mStorageManager:Landroid/os/storage/StorageManager;

    #@4c
    iget-object v6, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mStorageVolume:Landroid/os/storage/StorageVolume;

    #@4e
    invoke-virtual {v6}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    #@51
    move-result-object v6

    #@52
    invoke-virtual {v5, v6}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    #@55
    move-result-object v4

    #@56
    goto :goto_8

    #@57
    .line 167
    .restart local v2       #isUSBMemory:Z
    .restart local v4       #status:Ljava/lang/String;
    :cond_57
    const v5, 0x10404c9

    #@5a
    invoke-virtual {p0, v5}, Lcom/android/internal/os/storage/ExternalStorageFormatter;->updateProgressDialog(I)V

    #@5d
    goto :goto_33

    #@5e
    .line 170
    .restart local v3       #mountService:Landroid/os/storage/IMountService;
    :cond_5e
    iget-object v5, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mStorageVolume:Landroid/os/storage/StorageVolume;

    #@60
    invoke-virtual {v5}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    #@63
    move-result-object v1

    #@64
    goto :goto_43

    #@65
    .line 176
    .restart local v1       #extStoragePath:Ljava/lang/String;
    :catch_65
    move-exception v0

    #@66
    .line 177
    .local v0, e:Landroid/os/RemoteException;
    const-string v5, "ExternalStorageFormatter"

    #@68
    const-string v6, "Failed talking with mount service"

    #@6a
    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@6d
    goto :goto_49

    #@6e
    .line 179
    .end local v0           #e:Landroid/os/RemoteException;
    .end local v1           #extStoragePath:Ljava/lang/String;
    .end local v3           #mountService:Landroid/os/storage/IMountService;
    :cond_6e
    const-string/jumbo v5, "nofs"

    #@71
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@74
    move-result v5

    #@75
    if-nez v5, :cond_89

    #@77
    const-string/jumbo v5, "unmounted"

    #@7a
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7d
    move-result v5

    #@7e
    if-nez v5, :cond_89

    #@80
    const-string/jumbo v5, "unmountable"

    #@83
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@86
    move-result v5

    #@87
    if-eqz v5, :cond_c2

    #@89
    .line 182
    :cond_89
    if-eqz v2, :cond_ac

    #@8b
    .line 183
    const v5, 0x20902ef

    #@8e
    invoke-virtual {p0, v5}, Lcom/android/internal/os/storage/ExternalStorageFormatter;->updateProgressDialog(I)V

    #@91
    .line 188
    :goto_91
    invoke-virtual {p0}, Lcom/android/internal/os/storage/ExternalStorageFormatter;->getMountService()Landroid/os/storage/IMountService;

    #@94
    move-result-object v3

    #@95
    .line 189
    .restart local v3       #mountService:Landroid/os/storage/IMountService;
    iget-object v5, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mStorageVolume:Landroid/os/storage/StorageVolume;

    #@97
    if-nez v5, :cond_b3

    #@99
    invoke-static {}, Landroid/os/Environment;->getLegacyExternalStorageDirectory()Ljava/io/File;

    #@9c
    move-result-object v5

    #@9d
    invoke-virtual {v5}, Ljava/io/File;->toString()Ljava/lang/String;

    #@a0
    move-result-object v1

    #@a1
    .line 192
    .restart local v1       #extStoragePath:Ljava/lang/String;
    :goto_a1
    if-eqz v3, :cond_ba

    #@a3
    .line 193
    new-instance v5, Lcom/android/internal/os/storage/ExternalStorageFormatter$2;

    #@a5
    invoke-direct {v5, p0, v3, v1}, Lcom/android/internal/os/storage/ExternalStorageFormatter$2;-><init>(Lcom/android/internal/os/storage/ExternalStorageFormatter;Landroid/os/storage/IMountService;Ljava/lang/String;)V

    #@a8
    invoke-virtual {v5}, Lcom/android/internal/os/storage/ExternalStorageFormatter$2;->start()V

    #@ab
    goto :goto_49

    #@ac
    .line 185
    .end local v1           #extStoragePath:Ljava/lang/String;
    .end local v3           #mountService:Landroid/os/storage/IMountService;
    :cond_ac
    const v5, 0x10404ca

    #@af
    invoke-virtual {p0, v5}, Lcom/android/internal/os/storage/ExternalStorageFormatter;->updateProgressDialog(I)V

    #@b2
    goto :goto_91

    #@b3
    .line 189
    .restart local v3       #mountService:Landroid/os/storage/IMountService;
    :cond_b3
    iget-object v5, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter;->mStorageVolume:Landroid/os/storage/StorageVolume;

    #@b5
    invoke-virtual {v5}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    #@b8
    move-result-object v1

    #@b9
    goto :goto_a1

    #@ba
    .line 236
    .restart local v1       #extStoragePath:Ljava/lang/String;
    :cond_ba
    const-string v5, "ExternalStorageFormatter"

    #@bc
    const-string v6, "Unable to locate IMountService"

    #@be
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@c1
    goto :goto_49

    #@c2
    .line 238
    .end local v1           #extStoragePath:Ljava/lang/String;
    .end local v3           #mountService:Landroid/os/storage/IMountService;
    :cond_c2
    const-string v5, "bad_removal"

    #@c4
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c7
    move-result v5

    #@c8
    if-eqz v5, :cond_d2

    #@ca
    .line 239
    const v5, 0x10404cc

    #@cd
    invoke-virtual {p0, v5}, Lcom/android/internal/os/storage/ExternalStorageFormatter;->fail(I)V

    #@d0
    goto/16 :goto_49

    #@d2
    .line 240
    :cond_d2
    const-string v5, "checking"

    #@d4
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d7
    move-result v5

    #@d8
    if-eqz v5, :cond_e8

    #@da
    .line 241
    if-eqz v2, :cond_e4

    #@dc
    const v5, 0x2090309

    #@df
    :goto_df
    invoke-virtual {p0, v5}, Lcom/android/internal/os/storage/ExternalStorageFormatter;->fail(I)V

    #@e2
    goto/16 :goto_49

    #@e4
    :cond_e4
    const v5, 0x10404cd

    #@e7
    goto :goto_df

    #@e8
    .line 242
    :cond_e8
    const-string/jumbo v5, "removed"

    #@eb
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ee
    move-result v5

    #@ef
    if-eqz v5, :cond_f9

    #@f1
    .line 243
    const v5, 0x10404ce

    #@f4
    invoke-virtual {p0, v5}, Lcom/android/internal/os/storage/ExternalStorageFormatter;->fail(I)V

    #@f7
    goto/16 :goto_49

    #@f9
    .line 244
    :cond_f9
    const-string/jumbo v5, "shared"

    #@fc
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ff
    move-result v5

    #@100
    if-eqz v5, :cond_110

    #@102
    .line 245
    if-eqz v2, :cond_10c

    #@104
    const v5, 0x209030a

    #@107
    :goto_107
    invoke-virtual {p0, v5}, Lcom/android/internal/os/storage/ExternalStorageFormatter;->fail(I)V

    #@10a
    goto/16 :goto_49

    #@10c
    :cond_10c
    const v5, 0x10404cf

    #@10f
    goto :goto_107

    #@110
    .line 247
    :cond_110
    const v5, 0x10404d0

    #@113
    invoke-virtual {p0, v5}, Lcom/android/internal/os/storage/ExternalStorageFormatter;->fail(I)V

    #@116
    .line 248
    const-string v5, "ExternalStorageFormatter"

    #@118
    new-instance v6, Ljava/lang/StringBuilder;

    #@11a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@11d
    const-string v7, "Unknown storage state: "

    #@11f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@122
    move-result-object v6

    #@123
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@126
    move-result-object v6

    #@127
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12a
    move-result-object v6

    #@12b
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@12e
    .line 249
    invoke-virtual {p0}, Lcom/android/internal/os/storage/ExternalStorageFormatter;->stopSelf()V

    #@131
    goto/16 :goto_49
.end method
