.class Lcom/android/internal/os/ZygoteConnection;
.super Ljava/lang/Object;
.source "ZygoteConnection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/os/ZygoteConnection$Arguments;
    }
.end annotation


# static fields
.field private static final CONNECTION_TIMEOUT_MILLIS:I = 0x3e8

.field private static final MAX_ZYGOTE_ARGC:I = 0x400

.field private static final TAG:Ljava/lang/String; = "Zygote"

.field private static final intArray2d:[[I

.field private static sPeerWaitSocket:Landroid/net/LocalSocket;


# instance fields
.field private final mSocket:Landroid/net/LocalSocket;

.field private final mSocketOutStream:Ljava/io/DataOutputStream;

.field private final mSocketReader:Ljava/io/BufferedReader;

.field private final peer:Landroid/net/Credentials;

.field private final peerSecurityContext:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 51
    filled-new-array {v0, v0}, [I

    #@4
    move-result-object v0

    #@5
    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@7
    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    #@a
    move-result-object v0

    #@b
    check-cast v0, [[I

    #@d
    sput-object v0, Lcom/android/internal/os/ZygoteConnection;->intArray2d:[[I

    #@f
    .line 88
    const/4 v0, 0x0

    #@10
    sput-object v0, Lcom/android/internal/os/ZygoteConnection;->sPeerWaitSocket:Landroid/net/LocalSocket;

    #@12
    return-void
.end method

.method constructor <init>(Landroid/net/LocalSocket;)V
    .registers 6
    .parameter "socket"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 96
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 97
    iput-object p1, p0, Lcom/android/internal/os/ZygoteConnection;->mSocket:Landroid/net/LocalSocket;

    #@5
    .line 99
    new-instance v1, Ljava/io/DataOutputStream;

    #@7
    invoke-virtual {p1}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    #@a
    move-result-object v2

    #@b
    invoke-direct {v1, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@e
    iput-object v1, p0, Lcom/android/internal/os/ZygoteConnection;->mSocketOutStream:Ljava/io/DataOutputStream;

    #@10
    .line 102
    new-instance v1, Ljava/io/BufferedReader;

    #@12
    new-instance v2, Ljava/io/InputStreamReader;

    #@14
    invoke-virtual {p1}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    #@17
    move-result-object v3

    #@18
    invoke-direct {v2, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    #@1b
    const/16 v3, 0x100

    #@1d
    invoke-direct {v1, v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V

    #@20
    iput-object v1, p0, Lcom/android/internal/os/ZygoteConnection;->mSocketReader:Ljava/io/BufferedReader;

    #@22
    .line 105
    iget-object v1, p0, Lcom/android/internal/os/ZygoteConnection;->mSocket:Landroid/net/LocalSocket;

    #@24
    const/16 v2, 0x3e8

    #@26
    invoke-virtual {v1, v2}, Landroid/net/LocalSocket;->setSoTimeout(I)V

    #@29
    .line 108
    :try_start_29
    iget-object v1, p0, Lcom/android/internal/os/ZygoteConnection;->mSocket:Landroid/net/LocalSocket;

    #@2b
    invoke-virtual {v1}, Landroid/net/LocalSocket;->getPeerCredentials()Landroid/net/Credentials;

    #@2e
    move-result-object v1

    #@2f
    iput-object v1, p0, Lcom/android/internal/os/ZygoteConnection;->peer:Landroid/net/Credentials;
    :try_end_31
    .catch Ljava/io/IOException; {:try_start_29 .. :try_end_31} :catch_3e

    #@31
    .line 114
    iget-object v1, p0, Lcom/android/internal/os/ZygoteConnection;->mSocket:Landroid/net/LocalSocket;

    #@33
    invoke-virtual {v1}, Landroid/net/LocalSocket;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@36
    move-result-object v1

    #@37
    invoke-static {v1}, Landroid/os/SELinux;->getPeerContext(Ljava/io/FileDescriptor;)Ljava/lang/String;

    #@3a
    move-result-object v1

    #@3b
    iput-object v1, p0, Lcom/android/internal/os/ZygoteConnection;->peerSecurityContext:Ljava/lang/String;

    #@3d
    .line 115
    return-void

    #@3e
    .line 109
    :catch_3e
    move-exception v0

    #@3f
    .line 110
    .local v0, ex:Ljava/io/IOException;
    const-string v1, "Zygote"

    #@41
    const-string v2, "Cannot read peer credentials"

    #@43
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@46
    .line 111
    throw v0
.end method

.method private static applyCapabilitiesSecurityPolicy(Lcom/android/internal/os/ZygoteConnection$Arguments;Landroid/net/Credentials;Ljava/lang/String;)V
    .registers 15
    .parameter "args"
    .parameter "peer"
    .parameter "peerSecurityContext"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/os/ZygoteSecurityException;
        }
    .end annotation

    #@0
    .prologue
    const-wide/16 v10, -0x1

    #@2
    const-wide/16 v8, 0x0

    #@4
    .line 746
    iget-wide v4, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->permittedCapabilities:J

    #@6
    cmp-long v4, v4, v8

    #@8
    if-nez v4, :cond_11

    #@a
    iget-wide v4, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->effectiveCapabilities:J

    #@c
    cmp-long v4, v4, v8

    #@e
    if-nez v4, :cond_11

    #@10
    .line 795
    :cond_10
    return-void

    #@11
    .line 752
    :cond_11
    const-string/jumbo v4, "zygote"

    #@14
    const-string/jumbo v5, "specifycapabilities"

    #@17
    invoke-static {p2, p2, v4, v5}, Landroid/os/SELinux;->checkSELinuxAccess(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@1a
    move-result v0

    #@1b
    .line 756
    .local v0, allowed:Z
    if-nez v0, :cond_25

    #@1d
    .line 757
    new-instance v4, Lcom/android/internal/os/ZygoteSecurityException;

    #@1f
    const-string v5, "Peer may not specify capabilities"

    #@21
    invoke-direct {v4, v5}, Lcom/android/internal/os/ZygoteSecurityException;-><init>(Ljava/lang/String;)V

    #@24
    throw v4

    #@25
    .line 761
    :cond_25
    invoke-virtual {p1}, Landroid/net/Credentials;->getUid()I

    #@28
    move-result v4

    #@29
    if-eqz v4, :cond_10

    #@2b
    .line 769
    :try_start_2b
    invoke-virtual {p1}, Landroid/net/Credentials;->getPid()I

    #@2e
    move-result v4

    #@2f
    invoke-static {v4}, Lcom/android/internal/os/ZygoteInit;->capgetPermitted(I)J
    :try_end_32
    .catch Ljava/io/IOException; {:try_start_2b .. :try_end_32} :catch_45

    #@32
    move-result-wide v2

    #@33
    .line 780
    .local v2, permittedCaps:J
    iget-wide v4, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->permittedCapabilities:J

    #@35
    xor-long/2addr v4, v10

    #@36
    iget-wide v6, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->effectiveCapabilities:J

    #@38
    and-long/2addr v4, v6

    #@39
    cmp-long v4, v4, v8

    #@3b
    if-eqz v4, :cond_4e

    #@3d
    .line 781
    new-instance v4, Lcom/android/internal/os/ZygoteSecurityException;

    #@3f
    const-string v5, "Effective capabilities cannot be superset of  permitted capabilities"

    #@41
    invoke-direct {v4, v5}, Lcom/android/internal/os/ZygoteSecurityException;-><init>(Ljava/lang/String;)V

    #@44
    throw v4

    #@45
    .line 770
    .end local v2           #permittedCaps:J
    :catch_45
    move-exception v1

    #@46
    .line 771
    .local v1, ex:Ljava/io/IOException;
    new-instance v4, Lcom/android/internal/os/ZygoteSecurityException;

    #@48
    const-string v5, "Error retrieving peer\'s capabilities."

    #@4a
    invoke-direct {v4, v5}, Lcom/android/internal/os/ZygoteSecurityException;-><init>(Ljava/lang/String;)V

    #@4d
    throw v4

    #@4e
    .line 791
    .end local v1           #ex:Ljava/io/IOException;
    .restart local v2       #permittedCaps:J
    :cond_4e
    xor-long v4, v2, v10

    #@50
    iget-wide v6, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->permittedCapabilities:J

    #@52
    and-long/2addr v4, v6

    #@53
    cmp-long v4, v4, v8

    #@55
    if-eqz v4, :cond_10

    #@57
    .line 792
    new-instance v4, Lcom/android/internal/os/ZygoteSecurityException;

    #@59
    const-string v5, "Peer specified unpermitted capabilities"

    #@5b
    invoke-direct {v4, v5}, Lcom/android/internal/os/ZygoteSecurityException;-><init>(Ljava/lang/String;)V

    #@5e
    throw v4
.end method

.method public static applyDebuggerSystemProperty(Lcom/android/internal/os/ZygoteConnection$Arguments;)V
    .registers 3
    .parameter "args"

    #@0
    .prologue
    .line 689
    const-string v0, "1"

    #@2
    const-string/jumbo v1, "ro.debuggable"

    #@5
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_15

    #@f
    .line 690
    iget v0, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->debugFlags:I

    #@11
    or-int/lit8 v0, v0, 0x1

    #@13
    iput v0, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->debugFlags:I

    #@15
    .line 692
    :cond_15
    return-void
.end method

.method private static applyInvokeWithSecurityPolicy(Lcom/android/internal/os/ZygoteConnection$Arguments;Landroid/net/Credentials;Ljava/lang/String;)V
    .registers 7
    .parameter "args"
    .parameter "peer"
    .parameter "peerSecurityContext"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/os/ZygoteSecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 813
    invoke-virtual {p1}, Landroid/net/Credentials;->getUid()I

    #@3
    move-result v1

    #@4
    .line 815
    .local v1, peerUid:I
    iget-object v2, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->invokeWith:Ljava/lang/String;

    #@6
    if-eqz v2, :cond_12

    #@8
    if-eqz v1, :cond_12

    #@a
    .line 816
    new-instance v2, Lcom/android/internal/os/ZygoteSecurityException;

    #@c
    const-string v3, "Peer is not permitted to specify an explicit invoke-with wrapper command"

    #@e
    invoke-direct {v2, v3}, Lcom/android/internal/os/ZygoteSecurityException;-><init>(Ljava/lang/String;)V

    #@11
    throw v2

    #@12
    .line 820
    :cond_12
    iget-object v2, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->invokeWith:Ljava/lang/String;

    #@14
    if-eqz v2, :cond_2a

    #@16
    .line 821
    const-string/jumbo v2, "zygote"

    #@19
    const-string/jumbo v3, "specifyinvokewith"

    #@1c
    invoke-static {p2, p2, v2, v3}, Landroid/os/SELinux;->checkSELinuxAccess(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@1f
    move-result v0

    #@20
    .line 825
    .local v0, allowed:Z
    if-nez v0, :cond_2a

    #@22
    .line 826
    new-instance v2, Lcom/android/internal/os/ZygoteSecurityException;

    #@24
    const-string v3, "Peer is not permitted to specify an explicit invoke-with wrapper command"

    #@26
    invoke-direct {v2, v3}, Lcom/android/internal/os/ZygoteSecurityException;-><init>(Ljava/lang/String;)V

    #@29
    throw v2

    #@2a
    .line 830
    .end local v0           #allowed:Z
    :cond_2a
    return-void
.end method

.method public static applyInvokeWithSystemProperty(Lcom/android/internal/os/ZygoteConnection$Arguments;)V
    .registers 5
    .parameter "args"

    #@0
    .prologue
    const/16 v3, 0x1f

    #@2
    .line 873
    iget-object v1, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->invokeWith:Ljava/lang/String;

    #@4
    if-nez v1, :cond_44

    #@6
    iget-object v1, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->niceName:Ljava/lang/String;

    #@8
    if-eqz v1, :cond_44

    #@a
    .line 874
    iget-object v1, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->niceName:Ljava/lang/String;

    #@c
    if-eqz v1, :cond_44

    #@e
    .line 875
    new-instance v1, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string/jumbo v2, "wrap."

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    iget-object v2, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->niceName:Ljava/lang/String;

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v0

    #@24
    .line 876
    .local v0, property:Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@27
    move-result v1

    #@28
    if-le v1, v3, :cond_2f

    #@2a
    .line 877
    const/4 v1, 0x0

    #@2b
    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2e
    move-result-object v0

    #@2f
    .line 879
    :cond_2f
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    iput-object v1, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->invokeWith:Ljava/lang/String;

    #@35
    .line 880
    iget-object v1, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->invokeWith:Ljava/lang/String;

    #@37
    if-eqz v1, :cond_44

    #@39
    iget-object v1, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->invokeWith:Ljava/lang/String;

    #@3b
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@3e
    move-result v1

    #@3f
    if-nez v1, :cond_44

    #@41
    .line 881
    const/4 v1, 0x0

    #@42
    iput-object v1, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->invokeWith:Ljava/lang/String;

    #@44
    .line 885
    .end local v0           #property:Ljava/lang/String;
    :cond_44
    return-void
.end method

.method private static applyRlimitSecurityPolicy(Lcom/android/internal/os/ZygoteConnection$Arguments;Landroid/net/Credentials;Ljava/lang/String;)V
    .registers 7
    .parameter "args"
    .parameter "peer"
    .parameter "peerSecurityContext"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/os/ZygoteSecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 710
    invoke-virtual {p1}, Landroid/net/Credentials;->getUid()I

    #@3
    move-result v1

    #@4
    .line 712
    .local v1, peerUid:I
    if-eqz v1, :cond_16

    #@6
    const/16 v2, 0x3e8

    #@8
    if-eq v1, v2, :cond_16

    #@a
    .line 714
    iget-object v2, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->rlimits:Ljava/util/ArrayList;

    #@c
    if-eqz v2, :cond_16

    #@e
    .line 715
    new-instance v2, Lcom/android/internal/os/ZygoteSecurityException;

    #@10
    const-string v3, "This UID may not specify rlimits."

    #@12
    invoke-direct {v2, v3}, Lcom/android/internal/os/ZygoteSecurityException;-><init>(Ljava/lang/String;)V

    #@15
    throw v2

    #@16
    .line 720
    :cond_16
    iget-object v2, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->rlimits:Ljava/util/ArrayList;

    #@18
    if-eqz v2, :cond_2e

    #@1a
    .line 721
    const-string/jumbo v2, "zygote"

    #@1d
    const-string/jumbo v3, "specifyrlimits"

    #@20
    invoke-static {p2, p2, v2, v3}, Landroid/os/SELinux;->checkSELinuxAccess(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@23
    move-result v0

    #@24
    .line 725
    .local v0, allowed:Z
    if-nez v0, :cond_2e

    #@26
    .line 726
    new-instance v2, Lcom/android/internal/os/ZygoteSecurityException;

    #@28
    const-string v3, "Peer may not specify rlimits"

    #@2a
    invoke-direct {v2, v3}, Lcom/android/internal/os/ZygoteSecurityException;-><init>(Ljava/lang/String;)V

    #@2d
    throw v2

    #@2e
    .line 730
    .end local v0           #allowed:Z
    :cond_2e
    return-void
.end method

.method private static applyUidSecurityPolicy(Lcom/android/internal/os/ZygoteConnection$Arguments;Landroid/net/Credentials;Ljava/lang/String;)V
    .registers 10
    .parameter "args"
    .parameter "peer"
    .parameter "peerSecurityContext"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/os/ZygoteSecurityException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v6, 0x3e8

    #@2
    const/4 v4, 0x1

    #@3
    .line 621
    invoke-virtual {p1}, Landroid/net/Credentials;->getUid()I

    #@6
    move-result v2

    #@7
    .line 623
    .local v2, peerUid:I
    if-nez v2, :cond_29

    #@9
    .line 656
    :cond_9
    iget-boolean v5, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->uidSpecified:Z

    #@b
    if-nez v5, :cond_15

    #@d
    iget-boolean v5, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->gidSpecified:Z

    #@f
    if-nez v5, :cond_15

    #@11
    iget-object v5, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->gids:[I

    #@13
    if-eqz v5, :cond_63

    #@15
    .line 657
    :cond_15
    const-string/jumbo v5, "zygote"

    #@18
    const-string/jumbo v6, "specifyids"

    #@1b
    invoke-static {p2, p2, v5, v6}, Landroid/os/SELinux;->checkSELinuxAccess(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@1e
    move-result v0

    #@1f
    .line 661
    .local v0, allowed:Z
    if-nez v0, :cond_63

    #@21
    .line 662
    new-instance v4, Lcom/android/internal/os/ZygoteSecurityException;

    #@23
    const-string v5, "Peer may not specify uid\'s or gid\'s"

    #@25
    invoke-direct {v4, v5}, Lcom/android/internal/os/ZygoteSecurityException;-><init>(Ljava/lang/String;)V

    #@28
    throw v4

    #@29
    .line 625
    .end local v0           #allowed:Z
    :cond_29
    if-ne v2, v6, :cond_4f

    #@2b
    .line 627
    const-string/jumbo v5, "ro.factorytest"

    #@2e
    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@31
    move-result-object v1

    #@32
    .line 638
    .local v1, factoryTest:Ljava/lang/String;
    const-string v5, "1"

    #@34
    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@37
    move-result v5

    #@38
    if-nez v5, :cond_4d

    #@3a
    move v3, v4

    #@3b
    .line 641
    .local v3, uidRestricted:Z
    :goto_3b
    if-eqz v3, :cond_9

    #@3d
    iget-boolean v5, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->uidSpecified:Z

    #@3f
    if-eqz v5, :cond_9

    #@41
    iget v5, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->uid:I

    #@43
    if-ge v5, v6, :cond_9

    #@45
    .line 643
    new-instance v4, Lcom/android/internal/os/ZygoteSecurityException;

    #@47
    const-string v5, "System UID may not launch process with UID < 1000"

    #@49
    invoke-direct {v4, v5}, Lcom/android/internal/os/ZygoteSecurityException;-><init>(Ljava/lang/String;)V

    #@4c
    throw v4

    #@4d
    .line 638
    .end local v3           #uidRestricted:Z
    :cond_4d
    const/4 v3, 0x0

    #@4e
    goto :goto_3b

    #@4f
    .line 649
    .end local v1           #factoryTest:Ljava/lang/String;
    :cond_4f
    iget-boolean v5, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->uidSpecified:Z

    #@51
    if-nez v5, :cond_5b

    #@53
    iget-boolean v5, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->gidSpecified:Z

    #@55
    if-nez v5, :cond_5b

    #@57
    iget-object v5, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->gids:[I

    #@59
    if-eqz v5, :cond_9

    #@5b
    .line 651
    :cond_5b
    new-instance v4, Lcom/android/internal/os/ZygoteSecurityException;

    #@5d
    const-string v5, "App UIDs may not specify uid\'s or gid\'s"

    #@5f
    invoke-direct {v4, v5}, Lcom/android/internal/os/ZygoteSecurityException;-><init>(Ljava/lang/String;)V

    #@62
    throw v4

    #@63
    .line 668
    :cond_63
    iget-boolean v5, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->uidSpecified:Z

    #@65
    if-nez v5, :cond_6f

    #@67
    .line 669
    invoke-virtual {p1}, Landroid/net/Credentials;->getUid()I

    #@6a
    move-result v5

    #@6b
    iput v5, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->uid:I

    #@6d
    .line 670
    iput-boolean v4, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->uidSpecified:Z

    #@6f
    .line 672
    :cond_6f
    iget-boolean v5, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->gidSpecified:Z

    #@71
    if-nez v5, :cond_7b

    #@73
    .line 673
    invoke-virtual {p1}, Landroid/net/Credentials;->getGid()I

    #@76
    move-result v5

    #@77
    iput v5, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->gid:I

    #@79
    .line 674
    iput-boolean v4, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->gidSpecified:Z

    #@7b
    .line 676
    :cond_7b
    return-void
.end method

.method private static applyseInfoSecurityPolicy(Lcom/android/internal/os/ZygoteConnection$Arguments;Landroid/net/Credentials;Ljava/lang/String;)V
    .registers 7
    .parameter "args"
    .parameter "peer"
    .parameter "peerSecurityContext"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/os/ZygoteSecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 842
    invoke-virtual {p1}, Landroid/net/Credentials;->getUid()I

    #@3
    move-result v1

    #@4
    .line 844
    .local v1, peerUid:I
    iget-object v2, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->seInfo:Ljava/lang/String;

    #@6
    if-nez v2, :cond_9

    #@8
    .line 864
    :cond_8
    return-void

    #@9
    .line 849
    :cond_9
    if-eqz v1, :cond_17

    #@b
    const/16 v2, 0x3e8

    #@d
    if-eq v1, v2, :cond_17

    #@f
    .line 851
    new-instance v2, Lcom/android/internal/os/ZygoteSecurityException;

    #@11
    const-string v3, "This UID may not specify SEAndroid info."

    #@13
    invoke-direct {v2, v3}, Lcom/android/internal/os/ZygoteSecurityException;-><init>(Ljava/lang/String;)V

    #@16
    throw v2

    #@17
    .line 855
    :cond_17
    const-string/jumbo v2, "zygote"

    #@1a
    const-string/jumbo v3, "specifyseinfo"

    #@1d
    invoke-static {p2, p2, v2, v3}, Landroid/os/SELinux;->checkSELinuxAccess(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@20
    move-result v0

    #@21
    .line 859
    .local v0, allowed:Z
    if-nez v0, :cond_8

    #@23
    .line 860
    new-instance v2, Lcom/android/internal/os/ZygoteSecurityException;

    #@25
    const-string v3, "Peer may not specify SEAndroid info"

    #@27
    invoke-direct {v2, v3}, Lcom/android/internal/os/ZygoteSecurityException;-><init>(Ljava/lang/String;)V

    #@2a
    throw v2
.end method

.method private handleChildProc(Lcom/android/internal/os/ZygoteConnection$Arguments;[Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/PrintStream;)V
    .registers 18
    .parameter "parsedArgs"
    .parameter "descriptors"
    .parameter "pipeFd"
    .parameter "newStderr"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller;
        }
    .end annotation

    #@0
    .prologue
    .line 910
    iget-boolean v9, p1, Lcom/android/internal/os/ZygoteConnection$Arguments;->peerWait:Z

    #@2
    if-eqz v9, :cond_36

    #@4
    .line 912
    :try_start_4
    iget-object v9, p0, Lcom/android/internal/os/ZygoteConnection;->mSocket:Landroid/net/LocalSocket;

    #@6
    invoke-virtual {v9}, Landroid/net/LocalSocket;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@9
    move-result-object v9

    #@a
    const/4 v10, 0x1

    #@b
    invoke-static {v9, v10}, Lcom/android/internal/os/ZygoteInit;->setCloseOnExec(Ljava/io/FileDescriptor;Z)V

    #@e
    .line 913
    iget-object v9, p0, Lcom/android/internal/os/ZygoteConnection;->mSocket:Landroid/net/LocalSocket;

    #@10
    sput-object v9, Lcom/android/internal/os/ZygoteConnection;->sPeerWaitSocket:Landroid/net/LocalSocket;
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_12} :catch_2d

    #@12
    .line 923
    :goto_12
    if-eqz p2, :cond_3f

    #@14
    .line 925
    const/4 v9, 0x0

    #@15
    :try_start_15
    aget-object v9, p2, v9

    #@17
    const/4 v10, 0x1

    #@18
    aget-object v10, p2, v10

    #@1a
    const/4 v11, 0x2

    #@1b
    aget-object v11, p2, v11

    #@1d
    invoke-static {v9, v10, v11}, Lcom/android/internal/os/ZygoteInit;->reopenStdio(Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;)V

    #@20
    .line 928
    move-object v1, p2

    #@21
    .local v1, arr$:[Ljava/io/FileDescriptor;
    array-length v7, v1

    #@22
    .local v7, len$:I
    const/4 v6, 0x0

    #@23
    .local v6, i$:I
    :goto_23
    if-ge v6, v7, :cond_3d

    #@25
    aget-object v5, v1, v6

    #@27
    .line 929
    .local v5, fd:Ljava/io/FileDescriptor;
    invoke-static {v5}, Llibcore/io/IoUtils;->closeQuietly(Ljava/io/FileDescriptor;)V
    :try_end_2a
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_2a} :catch_5e

    #@2a
    .line 928
    add-int/lit8 v6, v6, 0x1

    #@2c
    goto :goto_23

    #@2d
    .line 914
    .end local v1           #arr$:[Ljava/io/FileDescriptor;
    .end local v5           #fd:Ljava/io/FileDescriptor;
    .end local v6           #i$:I
    .end local v7           #len$:I
    :catch_2d
    move-exception v4

    #@2e
    .line 915
    .local v4, ex:Ljava/io/IOException;
    const-string v9, "Zygote"

    #@30
    const-string v10, "Zygote Child: error setting peer wait socket to be close-on-exec"

    #@32
    invoke-static {v9, v10, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@35
    goto :goto_12

    #@36
    .line 919
    .end local v4           #ex:Ljava/io/IOException;
    :cond_36
    invoke-virtual {p0}, Lcom/android/internal/os/ZygoteConnection;->closeSocket()V

    #@39
    .line 920
    invoke-static {}, Lcom/android/internal/os/ZygoteInit;->closeServerSocket()V

    #@3c
    goto :goto_12

    #@3d
    .line 931
    .restart local v1       #arr$:[Ljava/io/FileDescriptor;
    .restart local v6       #i$:I
    .restart local v7       #len$:I
    :cond_3d
    :try_start_3d
    sget-object p4, Ljava/lang/System;->err:Ljava/io/PrintStream;
    :try_end_3f
    .catch Ljava/io/IOException; {:try_start_3d .. :try_end_3f} :catch_5e

    #@3f
    .line 937
    .end local v1           #arr$:[Ljava/io/FileDescriptor;
    .end local v6           #i$:I
    .end local v7           #len$:I
    :cond_3f
    :goto_3f
    iget-object v9, p1, Lcom/android/internal/os/ZygoteConnection$Arguments;->niceName:Ljava/lang/String;

    #@41
    if-eqz v9, :cond_48

    #@43
    .line 938
    iget-object v9, p1, Lcom/android/internal/os/ZygoteConnection$Arguments;->niceName:Ljava/lang/String;

    #@45
    invoke-static {v9}, Landroid/os/Process;->setArgV0(Ljava/lang/String;)V

    #@48
    .line 941
    :cond_48
    iget-boolean v9, p1, Lcom/android/internal/os/ZygoteConnection$Arguments;->runtimeInit:Z

    #@4a
    if-eqz v9, :cond_6f

    #@4c
    .line 942
    iget-object v9, p1, Lcom/android/internal/os/ZygoteConnection$Arguments;->invokeWith:Ljava/lang/String;

    #@4e
    if-eqz v9, :cond_67

    #@50
    .line 943
    iget-object v9, p1, Lcom/android/internal/os/ZygoteConnection$Arguments;->invokeWith:Ljava/lang/String;

    #@52
    iget-object v10, p1, Lcom/android/internal/os/ZygoteConnection$Arguments;->niceName:Ljava/lang/String;

    #@54
    iget v11, p1, Lcom/android/internal/os/ZygoteConnection$Arguments;->targetSdkVersion:I

    #@56
    iget-object v12, p1, Lcom/android/internal/os/ZygoteConnection$Arguments;->remainingArgs:[Ljava/lang/String;

    #@58
    move-object/from16 v0, p3

    #@5a
    invoke-static {v9, v10, v11, v0, v12}, Lcom/android/internal/os/WrapperInit;->execApplication(Ljava/lang/String;Ljava/lang/String;ILjava/io/FileDescriptor;[Ljava/lang/String;)V

    #@5d
    .line 983
    :goto_5d
    return-void

    #@5e
    .line 932
    :catch_5e
    move-exception v4

    #@5f
    .line 933
    .restart local v4       #ex:Ljava/io/IOException;
    const-string v9, "Zygote"

    #@61
    const-string v10, "Error reopening stdio"

    #@63
    invoke-static {v9, v10, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@66
    goto :goto_3f

    #@67
    .line 947
    .end local v4           #ex:Ljava/io/IOException;
    :cond_67
    iget v9, p1, Lcom/android/internal/os/ZygoteConnection$Arguments;->targetSdkVersion:I

    #@69
    iget-object v10, p1, Lcom/android/internal/os/ZygoteConnection$Arguments;->remainingArgs:[Ljava/lang/String;

    #@6b
    invoke-static {v9, v10}, Lcom/android/internal/os/RuntimeInit;->zygoteInit(I[Ljava/lang/String;)V

    #@6e
    goto :goto_5d

    #@6f
    .line 953
    :cond_6f
    :try_start_6f
    iget-object v9, p1, Lcom/android/internal/os/ZygoteConnection$Arguments;->remainingArgs:[Ljava/lang/String;

    #@71
    const/4 v10, 0x0

    #@72
    aget-object v2, v9, v10
    :try_end_74
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_6f .. :try_end_74} :catch_8f

    #@74
    .line 960
    .local v2, className:Ljava/lang/String;
    iget-object v9, p1, Lcom/android/internal/os/ZygoteConnection$Arguments;->remainingArgs:[Ljava/lang/String;

    #@76
    array-length v9, v9

    #@77
    add-int/lit8 v9, v9, -0x1

    #@79
    new-array v8, v9, [Ljava/lang/String;

    #@7b
    .line 961
    .local v8, mainArgs:[Ljava/lang/String;
    iget-object v9, p1, Lcom/android/internal/os/ZygoteConnection$Arguments;->remainingArgs:[Ljava/lang/String;

    #@7d
    const/4 v10, 0x1

    #@7e
    const/4 v11, 0x0

    #@7f
    array-length v12, v8

    #@80
    invoke-static {v9, v10, v8, v11, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@83
    .line 964
    iget-object v9, p1, Lcom/android/internal/os/ZygoteConnection$Arguments;->invokeWith:Ljava/lang/String;

    #@85
    if-eqz v9, :cond_99

    #@87
    .line 965
    iget-object v9, p1, Lcom/android/internal/os/ZygoteConnection$Arguments;->invokeWith:Ljava/lang/String;

    #@89
    iget-object v10, p1, Lcom/android/internal/os/ZygoteConnection$Arguments;->classpath:Ljava/lang/String;

    #@8b
    invoke-static {v9, v10, v2, v8}, Lcom/android/internal/os/WrapperInit;->execStandalone(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    #@8e
    goto :goto_5d

    #@8f
    .line 954
    .end local v2           #className:Ljava/lang/String;
    .end local v8           #mainArgs:[Ljava/lang/String;
    :catch_8f
    move-exception v4

    #@90
    .line 955
    .local v4, ex:Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string v9, "Missing required class name argument"

    #@92
    const/4 v10, 0x0

    #@93
    move-object/from16 v0, p4

    #@95
    invoke-static {v0, v9, v10}, Lcom/android/internal/os/ZygoteConnection;->logAndPrintError(Ljava/io/PrintStream;Ljava/lang/String;Ljava/lang/Throwable;)V

    #@98
    goto :goto_5d

    #@99
    .line 969
    .end local v4           #ex:Ljava/lang/ArrayIndexOutOfBoundsException;
    .restart local v2       #className:Ljava/lang/String;
    .restart local v8       #mainArgs:[Ljava/lang/String;
    :cond_99
    iget-object v9, p1, Lcom/android/internal/os/ZygoteConnection$Arguments;->classpath:Ljava/lang/String;

    #@9b
    if-eqz v9, :cond_b5

    #@9d
    .line 970
    new-instance v3, Ldalvik/system/PathClassLoader;

    #@9f
    iget-object v9, p1, Lcom/android/internal/os/ZygoteConnection$Arguments;->classpath:Ljava/lang/String;

    #@a1
    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    #@a4
    move-result-object v10

    #@a5
    invoke-direct {v3, v9, v10}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    #@a8
    .line 977
    .local v3, cloader:Ljava/lang/ClassLoader;
    :goto_a8
    :try_start_a8
    invoke-static {v3, v2, v8}, Lcom/android/internal/os/ZygoteInit;->invokeStaticMain(Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_ab
    .catch Ljava/lang/RuntimeException; {:try_start_a8 .. :try_end_ab} :catch_ac

    #@ab
    goto :goto_5d

    #@ac
    .line 978
    :catch_ac
    move-exception v4

    #@ad
    .line 979
    .local v4, ex:Ljava/lang/RuntimeException;
    const-string v9, "Error starting."

    #@af
    move-object/from16 v0, p4

    #@b1
    invoke-static {v0, v9, v4}, Lcom/android/internal/os/ZygoteConnection;->logAndPrintError(Ljava/io/PrintStream;Ljava/lang/String;Ljava/lang/Throwable;)V

    #@b4
    goto :goto_5d

    #@b5
    .line 973
    .end local v3           #cloader:Ljava/lang/ClassLoader;
    .end local v4           #ex:Ljava/lang/RuntimeException;
    :cond_b5
    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    #@b8
    move-result-object v3

    #@b9
    .restart local v3       #cloader:Ljava/lang/ClassLoader;
    goto :goto_a8
.end method

.method private handleParentProc(I[Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Lcom/android/internal/os/ZygoteConnection$Arguments;)Z
    .registers 18
    .parameter "pid"
    .parameter "descriptors"
    .parameter "pipeFd"
    .parameter "parsedArgs"

    #@0
    .prologue
    .line 1000
    if-lez p1, :cond_5

    #@2
    .line 1001
    invoke-direct {p0, p1}, Lcom/android/internal/os/ZygoteConnection;->setChildPgid(I)V

    #@5
    .line 1004
    :cond_5
    if-eqz p2, :cond_14

    #@7
    .line 1005
    move-object v1, p2

    #@8
    .local v1, arr$:[Ljava/io/FileDescriptor;
    array-length v7, v1

    #@9
    .local v7, len$:I
    const/4 v4, 0x0

    #@a
    .local v4, i$:I
    :goto_a
    if-ge v4, v7, :cond_14

    #@c
    aget-object v3, v1, v4

    #@e
    .line 1006
    .local v3, fd:Ljava/io/FileDescriptor;
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/io/FileDescriptor;)V

    #@11
    .line 1005
    add-int/lit8 v4, v4, 0x1

    #@13
    goto :goto_a

    #@14
    .line 1010
    .end local v1           #arr$:[Ljava/io/FileDescriptor;
    .end local v3           #fd:Ljava/io/FileDescriptor;
    .end local v4           #i$:I
    .end local v7           #len$:I
    :cond_14
    const/4 v9, 0x0

    #@15
    .line 1011
    .local v9, usingWrapper:Z
    if-eqz p3, :cond_68

    #@17
    if-lez p1, :cond_68

    #@19
    .line 1012
    new-instance v6, Ljava/io/DataInputStream;

    #@1b
    new-instance v10, Ljava/io/FileInputStream;

    #@1d
    move-object/from16 v0, p3

    #@1f
    invoke-direct {v10, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    #@22
    invoke-direct {v6, v10}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    #@25
    .line 1013
    .local v6, is:Ljava/io/DataInputStream;
    const/4 v5, -0x1

    #@26
    .line 1015
    .local v5, innerPid:I
    :try_start_26
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I
    :try_end_29
    .catchall {:try_start_26 .. :try_end_29} :catchall_47
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_29} :catch_39

    #@29
    move-result v5

    #@2a
    .line 1020
    :try_start_2a
    invoke-virtual {v6}, Ljava/io/DataInputStream;->close()V
    :try_end_2d
    .catch Ljava/io/IOException; {:try_start_2a .. :try_end_2d} :catch_b7

    #@2d
    .line 1027
    :goto_2d
    if-lez v5, :cond_68

    #@2f
    .line 1028
    move v8, v5

    #@30
    .line 1029
    .local v8, parentPid:I
    :goto_30
    if-lez v8, :cond_4c

    #@32
    if-eq v8, p1, :cond_4c

    #@34
    .line 1030
    invoke-static {v8}, Landroid/os/Process;->getParentPid(I)I

    #@37
    move-result v8

    #@38
    goto :goto_30

    #@39
    .line 1016
    .end local v8           #parentPid:I
    :catch_39
    move-exception v2

    #@3a
    .line 1017
    .local v2, ex:Ljava/io/IOException;
    :try_start_3a
    const-string v10, "Zygote"

    #@3c
    const-string v11, "Error reading pid from wrapped process, child may have died"

    #@3e
    invoke-static {v10, v11, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_41
    .catchall {:try_start_3a .. :try_end_41} :catchall_47

    #@41
    .line 1020
    :try_start_41
    invoke-virtual {v6}, Ljava/io/DataInputStream;->close()V
    :try_end_44
    .catch Ljava/io/IOException; {:try_start_41 .. :try_end_44} :catch_45

    #@44
    goto :goto_2d

    #@45
    .line 1021
    :catch_45
    move-exception v10

    #@46
    goto :goto_2d

    #@47
    .line 1019
    .end local v2           #ex:Ljava/io/IOException;
    :catchall_47
    move-exception v10

    #@48
    .line 1020
    :try_start_48
    invoke-virtual {v6}, Ljava/io/DataInputStream;->close()V
    :try_end_4b
    .catch Ljava/io/IOException; {:try_start_48 .. :try_end_4b} :catch_ba

    #@4b
    .line 1022
    :goto_4b
    throw v10

    #@4c
    .line 1032
    .restart local v8       #parentPid:I
    :cond_4c
    if-lez v8, :cond_7f

    #@4e
    .line 1033
    const-string v10, "Zygote"

    #@50
    new-instance v11, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v12, "Wrapped process has pid "

    #@57
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v11

    #@5b
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v11

    #@5f
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v11

    #@63
    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    .line 1034
    move p1, v5

    #@67
    .line 1035
    const/4 v9, 0x1

    #@68
    .line 1045
    .end local v5           #innerPid:I
    .end local v6           #is:Ljava/io/DataInputStream;
    .end local v8           #parentPid:I
    :cond_68
    :goto_68
    :try_start_68
    iget-object v10, p0, Lcom/android/internal/os/ZygoteConnection;->mSocketOutStream:Ljava/io/DataOutputStream;

    #@6a
    invoke-virtual {v10, p1}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@6d
    .line 1046
    iget-object v10, p0, Lcom/android/internal/os/ZygoteConnection;->mSocketOutStream:Ljava/io/DataOutputStream;

    #@6f
    invoke-virtual {v10, v9}, Ljava/io/DataOutputStream;->writeBoolean(Z)V
    :try_end_72
    .catch Ljava/io/IOException; {:try_start_68 .. :try_end_72} :catch_a2

    #@72
    .line 1056
    move-object/from16 v0, p4

    #@74
    iget-boolean v10, v0, Lcom/android/internal/os/ZygoteConnection$Arguments;->peerWait:Z

    #@76
    if-eqz v10, :cond_b5

    #@78
    .line 1058
    :try_start_78
    iget-object v10, p0, Lcom/android/internal/os/ZygoteConnection;->mSocket:Landroid/net/LocalSocket;

    #@7a
    invoke-virtual {v10}, Landroid/net/LocalSocket;->close()V
    :try_end_7d
    .catch Ljava/io/IOException; {:try_start_78 .. :try_end_7d} :catch_ac

    #@7d
    .line 1062
    :goto_7d
    const/4 v10, 0x1

    #@7e
    .line 1064
    :goto_7e
    return v10

    #@7f
    .line 1037
    .restart local v5       #innerPid:I
    .restart local v6       #is:Ljava/io/DataInputStream;
    .restart local v8       #parentPid:I
    :cond_7f
    const-string v10, "Zygote"

    #@81
    new-instance v11, Ljava/lang/StringBuilder;

    #@83
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@86
    const-string v12, "Wrapped process reported a pid that is not a child of the process that we forked: childPid="

    #@88
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v11

    #@8c
    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v11

    #@90
    const-string v12, " innerPid="

    #@92
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v11

    #@96
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@99
    move-result-object v11

    #@9a
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9d
    move-result-object v11

    #@9e
    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@a1
    goto :goto_68

    #@a2
    .line 1047
    .end local v5           #innerPid:I
    .end local v6           #is:Ljava/io/DataInputStream;
    .end local v8           #parentPid:I
    :catch_a2
    move-exception v2

    #@a3
    .line 1048
    .restart local v2       #ex:Ljava/io/IOException;
    const-string v10, "Zygote"

    #@a5
    const-string v11, "Error reading from command socket"

    #@a7
    invoke-static {v10, v11, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@aa
    .line 1049
    const/4 v10, 0x1

    #@ab
    goto :goto_7e

    #@ac
    .line 1059
    .end local v2           #ex:Ljava/io/IOException;
    :catch_ac
    move-exception v2

    #@ad
    .line 1060
    .restart local v2       #ex:Ljava/io/IOException;
    const-string v10, "Zygote"

    #@af
    const-string v11, "Zygote: error closing sockets"

    #@b1
    invoke-static {v10, v11, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@b4
    goto :goto_7d

    #@b5
    .line 1064
    .end local v2           #ex:Ljava/io/IOException;
    :cond_b5
    const/4 v10, 0x0

    #@b6
    goto :goto_7e

    #@b7
    .line 1021
    .restart local v5       #innerPid:I
    .restart local v6       #is:Ljava/io/DataInputStream;
    :catch_b7
    move-exception v10

    #@b8
    goto/16 :goto_2d

    #@ba
    :catch_ba
    move-exception v11

    #@bb
    goto :goto_4b
.end method

.method private static logAndPrintError(Ljava/io/PrintStream;Ljava/lang/String;Ljava/lang/Throwable;)V
    .registers 4
    .parameter "newStderr"
    .parameter "message"
    .parameter "ex"

    #@0
    .prologue
    .line 1091
    const-string v0, "Zygote"

    #@2
    invoke-static {v0, p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@5
    .line 1092
    if-eqz p0, :cond_1f

    #@7
    .line 1093
    new-instance v0, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v0

    #@10
    if-nez p2, :cond_14

    #@12
    const-string p2, ""

    #@14
    .end local p2
    :cond_14
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {p0, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    #@1f
    .line 1095
    :cond_1f
    return-void
.end method

.method private readArgumentList()[Ljava/lang/String;
    .registers 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 572
    :try_start_0
    iget-object v5, p0, Lcom/android/internal/os/ZygoteConnection;->mSocketReader:Ljava/io/BufferedReader;

    #@2
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@5
    move-result-object v4

    #@6
    .line 574
    .local v4, s:Ljava/lang/String;
    if-nez v4, :cond_a

    #@8
    .line 576
    const/4 v3, 0x0

    #@9
    .line 598
    :cond_9
    return-object v3

    #@a
    .line 578
    :cond_a
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_d
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_d} :catch_1b

    #@d
    move-result v0

    #@e
    .line 585
    .local v0, argc:I
    const/16 v5, 0x400

    #@10
    if-le v0, v5, :cond_2b

    #@12
    .line 586
    new-instance v5, Ljava/io/IOException;

    #@14
    const-string/jumbo v6, "max arg count exceeded"

    #@17
    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v5

    #@1b
    .line 579
    .end local v0           #argc:I
    .end local v4           #s:Ljava/lang/String;
    :catch_1b
    move-exception v1

    #@1c
    .line 580
    .local v1, ex:Ljava/lang/NumberFormatException;
    const-string v5, "Zygote"

    #@1e
    const-string v6, "invalid Zygote wire format: non-int at argc"

    #@20
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 581
    new-instance v5, Ljava/io/IOException;

    #@25
    const-string v6, "invalid wire format"

    #@27
    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@2a
    throw v5

    #@2b
    .line 589
    .end local v1           #ex:Ljava/lang/NumberFormatException;
    .restart local v0       #argc:I
    .restart local v4       #s:Ljava/lang/String;
    :cond_2b
    new-array v3, v0, [Ljava/lang/String;

    #@2d
    .line 590
    .local v3, result:[Ljava/lang/String;
    const/4 v2, 0x0

    #@2e
    .local v2, i:I
    :goto_2e
    if-ge v2, v0, :cond_9

    #@30
    .line 591
    iget-object v5, p0, Lcom/android/internal/os/ZygoteConnection;->mSocketReader:Ljava/io/BufferedReader;

    #@32
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@35
    move-result-object v5

    #@36
    aput-object v5, v3, v2

    #@38
    .line 592
    aget-object v5, v3, v2

    #@3a
    if-nez v5, :cond_45

    #@3c
    .line 594
    new-instance v5, Ljava/io/IOException;

    #@3e
    const-string/jumbo v6, "truncated request"

    #@41
    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@44
    throw v5

    #@45
    .line 590
    :cond_45
    add-int/lit8 v2, v2, 0x1

    #@47
    goto :goto_2e
.end method

.method private setChildPgid(I)V
    .registers 5
    .parameter "pid"

    #@0
    .prologue
    .line 1070
    :try_start_0
    iget-object v1, p0, Lcom/android/internal/os/ZygoteConnection;->peer:Landroid/net/Credentials;

    #@2
    invoke-virtual {v1}, Landroid/net/Credentials;->getPid()I

    #@5
    move-result v1

    #@6
    invoke-static {v1}, Lcom/android/internal/os/ZygoteInit;->getpgid(I)I

    #@9
    move-result v1

    #@a
    invoke-static {p1, v1}, Lcom/android/internal/os/ZygoteInit;->setpgid(II)I
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_d} :catch_e

    #@d
    .line 1079
    :goto_d
    return-void

    #@e
    .line 1071
    :catch_e
    move-exception v0

    #@f
    .line 1076
    .local v0, ex:Ljava/io/IOException;
    const-string v1, "Zygote"

    #@11
    const-string v2, "Zygote: setpgid failed. This is normal if peer is not in our session"

    #@13
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    goto :goto_d
.end method


# virtual methods
.method closeSocket()V
    .registers 4

    #@0
    .prologue
    .line 276
    :try_start_0
    iget-object v1, p0, Lcom/android/internal/os/ZygoteConnection;->mSocket:Landroid/net/LocalSocket;

    #@2
    invoke-virtual {v1}, Landroid/net/LocalSocket;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 281
    :goto_5
    return-void

    #@6
    .line 277
    :catch_6
    move-exception v0

    #@7
    .line 278
    .local v0, ex:Ljava/io/IOException;
    const-string v1, "Zygote"

    #@9
    const-string v2, "Exception while closing command socket in parent"

    #@b
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e
    goto :goto_5
.end method

.method getFileDesciptor()Ljava/io/FileDescriptor;
    .registers 2

    #@0
    .prologue
    .line 123
    iget-object v0, p0, Lcom/android/internal/os/ZygoteConnection;->mSocket:Landroid/net/LocalSocket;

    #@2
    invoke-virtual {v0}, Landroid/net/LocalSocket;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method run()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller;
        }
    .end annotation

    #@0
    .prologue
    .line 138
    const/16 v0, 0xa

    #@2
    .line 150
    .local v0, loopCount:I
    :cond_2
    if-gtz v0, :cond_10

    #@4
    .line 151
    invoke-static {}, Lcom/android/internal/os/ZygoteInit;->gc()V

    #@7
    .line 152
    const/16 v0, 0xa

    #@9
    .line 157
    :goto_9
    invoke-virtual {p0}, Lcom/android/internal/os/ZygoteConnection;->runOnce()Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_2

    #@f
    .line 161
    return-void

    #@10
    .line 154
    :cond_10
    add-int/lit8 v0, v0, -0x1

    #@12
    goto :goto_9
.end method

.method runOnce()Z
    .registers 23
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller;
        }
    .end annotation

    #@0
    .prologue
    .line 180
    const/16 v17, 0x0

    #@2
    .line 184
    .local v17, parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    :try_start_2
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/os/ZygoteConnection;->readArgumentList()[Ljava/lang/String;

    #@5
    move-result-object v12

    #@6
    .line 185
    .local v12, args:[Ljava/lang/String;
    move-object/from16 v0, p0

    #@8
    iget-object v4, v0, Lcom/android/internal/os/ZygoteConnection;->mSocket:Landroid/net/LocalSocket;

    #@a
    invoke-virtual {v4}, Landroid/net/LocalSocket;->getAncillaryFileDescriptors()[Ljava/io/FileDescriptor;
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_d} :catch_15

    #@d
    move-result-object v14

    #@e
    .line 192
    .local v14, descriptors:[Ljava/io/FileDescriptor;
    if-nez v12, :cond_37

    #@10
    .line 194
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/os/ZygoteConnection;->closeSocket()V

    #@13
    .line 195
    const/4 v4, 0x1

    #@14
    .line 267
    .end local v12           #args:[Ljava/lang/String;
    .end local v14           #descriptors:[Ljava/io/FileDescriptor;
    :goto_14
    return v4

    #@15
    .line 186
    :catch_15
    move-exception v15

    #@16
    .line 187
    .local v15, ex:Ljava/io/IOException;
    const-string v4, "Zygote"

    #@18
    new-instance v5, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v6, "IOException on command socket "

    #@1f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v5

    #@23
    invoke-virtual {v15}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    #@26
    move-result-object v6

    #@27
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v5

    #@2b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v5

    #@2f
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 188
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/os/ZygoteConnection;->closeSocket()V

    #@35
    .line 189
    const/4 v4, 0x1

    #@36
    goto :goto_14

    #@37
    .line 199
    .end local v15           #ex:Ljava/io/IOException;
    .restart local v12       #args:[Ljava/lang/String;
    .restart local v14       #descriptors:[Ljava/io/FileDescriptor;
    :cond_37
    const/16 v16, 0x0

    #@39
    .line 201
    .local v16, newStderr:Ljava/io/PrintStream;
    if-eqz v14, :cond_4e

    #@3b
    array-length v4, v14

    #@3c
    const/4 v5, 0x3

    #@3d
    if-lt v4, v5, :cond_4e

    #@3f
    .line 202
    new-instance v16, Ljava/io/PrintStream;

    #@41
    .end local v16           #newStderr:Ljava/io/PrintStream;
    new-instance v4, Ljava/io/FileOutputStream;

    #@43
    const/4 v5, 0x2

    #@44
    aget-object v5, v14, v5

    #@46
    invoke-direct {v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    #@49
    move-object/from16 v0, v16

    #@4b
    invoke-direct {v0, v4}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    #@4e
    .line 206
    .restart local v16       #newStderr:Ljava/io/PrintStream;
    :cond_4e
    const/16 v19, -0x1

    #@50
    .line 207
    .local v19, pid:I
    const/4 v13, 0x0

    #@51
    .line 208
    .local v13, childPipeFd:Ljava/io/FileDescriptor;
    const/16 v21, 0x0

    #@53
    .line 211
    .local v21, serverPipeFd:Ljava/io/FileDescriptor;
    :try_start_53
    new-instance v18, Lcom/android/internal/os/ZygoteConnection$Arguments;

    #@55
    move-object/from16 v0, v18

    #@57
    invoke-direct {v0, v12}, Lcom/android/internal/os/ZygoteConnection$Arguments;-><init>([Ljava/lang/String;)V
    :try_end_5a
    .catch Ljava/io/IOException; {:try_start_53 .. :try_end_5a} :catch_10f
    .catch Llibcore/io/ErrnoException; {:try_start_53 .. :try_end_5a} :catch_118
    .catch Ljava/lang/IllegalArgumentException; {:try_start_53 .. :try_end_5a} :catch_121
    .catch Lcom/android/internal/os/ZygoteSecurityException; {:try_start_53 .. :try_end_5a} :catch_12a

    #@5a
    .line 213
    .end local v17           #parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    .local v18, parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    :try_start_5a
    move-object/from16 v0, p0

    #@5c
    iget-object v4, v0, Lcom/android/internal/os/ZygoteConnection;->peer:Landroid/net/Credentials;

    #@5e
    move-object/from16 v0, p0

    #@60
    iget-object v5, v0, Lcom/android/internal/os/ZygoteConnection;->peerSecurityContext:Ljava/lang/String;

    #@62
    move-object/from16 v0, v18

    #@64
    invoke-static {v0, v4, v5}, Lcom/android/internal/os/ZygoteConnection;->applyUidSecurityPolicy(Lcom/android/internal/os/ZygoteConnection$Arguments;Landroid/net/Credentials;Ljava/lang/String;)V

    #@67
    .line 214
    move-object/from16 v0, p0

    #@69
    iget-object v4, v0, Lcom/android/internal/os/ZygoteConnection;->peer:Landroid/net/Credentials;

    #@6b
    move-object/from16 v0, p0

    #@6d
    iget-object v5, v0, Lcom/android/internal/os/ZygoteConnection;->peerSecurityContext:Ljava/lang/String;

    #@6f
    move-object/from16 v0, v18

    #@71
    invoke-static {v0, v4, v5}, Lcom/android/internal/os/ZygoteConnection;->applyRlimitSecurityPolicy(Lcom/android/internal/os/ZygoteConnection$Arguments;Landroid/net/Credentials;Ljava/lang/String;)V

    #@74
    .line 215
    move-object/from16 v0, p0

    #@76
    iget-object v4, v0, Lcom/android/internal/os/ZygoteConnection;->peer:Landroid/net/Credentials;

    #@78
    move-object/from16 v0, p0

    #@7a
    iget-object v5, v0, Lcom/android/internal/os/ZygoteConnection;->peerSecurityContext:Ljava/lang/String;

    #@7c
    move-object/from16 v0, v18

    #@7e
    invoke-static {v0, v4, v5}, Lcom/android/internal/os/ZygoteConnection;->applyCapabilitiesSecurityPolicy(Lcom/android/internal/os/ZygoteConnection$Arguments;Landroid/net/Credentials;Ljava/lang/String;)V

    #@81
    .line 216
    move-object/from16 v0, p0

    #@83
    iget-object v4, v0, Lcom/android/internal/os/ZygoteConnection;->peer:Landroid/net/Credentials;

    #@85
    move-object/from16 v0, p0

    #@87
    iget-object v5, v0, Lcom/android/internal/os/ZygoteConnection;->peerSecurityContext:Ljava/lang/String;

    #@89
    move-object/from16 v0, v18

    #@8b
    invoke-static {v0, v4, v5}, Lcom/android/internal/os/ZygoteConnection;->applyInvokeWithSecurityPolicy(Lcom/android/internal/os/ZygoteConnection$Arguments;Landroid/net/Credentials;Ljava/lang/String;)V

    #@8e
    .line 217
    move-object/from16 v0, p0

    #@90
    iget-object v4, v0, Lcom/android/internal/os/ZygoteConnection;->peer:Landroid/net/Credentials;

    #@92
    move-object/from16 v0, p0

    #@94
    iget-object v5, v0, Lcom/android/internal/os/ZygoteConnection;->peerSecurityContext:Ljava/lang/String;

    #@96
    move-object/from16 v0, v18

    #@98
    invoke-static {v0, v4, v5}, Lcom/android/internal/os/ZygoteConnection;->applyseInfoSecurityPolicy(Lcom/android/internal/os/ZygoteConnection$Arguments;Landroid/net/Credentials;Ljava/lang/String;)V

    #@9b
    .line 219
    invoke-static/range {v18 .. v18}, Lcom/android/internal/os/ZygoteConnection;->applyDebuggerSystemProperty(Lcom/android/internal/os/ZygoteConnection$Arguments;)V

    #@9e
    .line 220
    invoke-static/range {v18 .. v18}, Lcom/android/internal/os/ZygoteConnection;->applyInvokeWithSystemProperty(Lcom/android/internal/os/ZygoteConnection$Arguments;)V

    #@a1
    .line 222
    const/4 v8, 0x0

    #@a2
    check-cast v8, [[I

    #@a4
    .line 224
    .local v8, rlimits:[[I
    move-object/from16 v0, v18

    #@a6
    iget-object v4, v0, Lcom/android/internal/os/ZygoteConnection$Arguments;->rlimits:Ljava/util/ArrayList;

    #@a8
    if-eqz v4, :cond_b6

    #@aa
    .line 225
    move-object/from16 v0, v18

    #@ac
    iget-object v4, v0, Lcom/android/internal/os/ZygoteConnection$Arguments;->rlimits:Ljava/util/ArrayList;

    #@ae
    sget-object v5, Lcom/android/internal/os/ZygoteConnection;->intArray2d:[[I

    #@b0
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@b3
    move-result-object v8

    #@b4
    .end local v8           #rlimits:[[I
    check-cast v8, [[I

    #@b6
    .line 228
    .restart local v8       #rlimits:[[I
    :cond_b6
    move-object/from16 v0, v18

    #@b8
    iget-boolean v4, v0, Lcom/android/internal/os/ZygoteConnection$Arguments;->runtimeInit:Z

    #@ba
    if-eqz v4, :cond_d4

    #@bc
    move-object/from16 v0, v18

    #@be
    iget-object v4, v0, Lcom/android/internal/os/ZygoteConnection$Arguments;->invokeWith:Ljava/lang/String;

    #@c0
    if-eqz v4, :cond_d4

    #@c2
    .line 229
    sget-object v4, Llibcore/io/Libcore;->os:Llibcore/io/Os;

    #@c4
    invoke-interface {v4}, Llibcore/io/Os;->pipe()[Ljava/io/FileDescriptor;

    #@c7
    move-result-object v20

    #@c8
    .line 230
    .local v20, pipeFds:[Ljava/io/FileDescriptor;
    const/4 v4, 0x1

    #@c9
    aget-object v13, v20, v4

    #@cb
    .line 231
    const/4 v4, 0x0

    #@cc
    aget-object v21, v20, v4

    #@ce
    .line 232
    const/4 v4, 0x1

    #@cf
    move-object/from16 v0, v21

    #@d1
    invoke-static {v0, v4}, Lcom/android/internal/os/ZygoteInit;->setCloseOnExec(Ljava/io/FileDescriptor;Z)V

    #@d4
    .line 235
    .end local v20           #pipeFds:[Ljava/io/FileDescriptor;
    :cond_d4
    move-object/from16 v0, v18

    #@d6
    iget v4, v0, Lcom/android/internal/os/ZygoteConnection$Arguments;->uid:I

    #@d8
    move-object/from16 v0, v18

    #@da
    iget v5, v0, Lcom/android/internal/os/ZygoteConnection$Arguments;->gid:I

    #@dc
    move-object/from16 v0, v18

    #@de
    iget-object v6, v0, Lcom/android/internal/os/ZygoteConnection$Arguments;->gids:[I

    #@e0
    move-object/from16 v0, v18

    #@e2
    iget v7, v0, Lcom/android/internal/os/ZygoteConnection$Arguments;->debugFlags:I

    #@e4
    move-object/from16 v0, v18

    #@e6
    iget v9, v0, Lcom/android/internal/os/ZygoteConnection$Arguments;->mountExternal:I

    #@e8
    move-object/from16 v0, v18

    #@ea
    iget-object v10, v0, Lcom/android/internal/os/ZygoteConnection$Arguments;->seInfo:Ljava/lang/String;

    #@ec
    move-object/from16 v0, v18

    #@ee
    iget-object v11, v0, Lcom/android/internal/os/ZygoteConnection$Arguments;->niceName:Ljava/lang/String;

    #@f0
    invoke-static/range {v4 .. v11}, Ldalvik/system/Zygote;->forkAndSpecialize(II[II[[IILjava/lang/String;Ljava/lang/String;)I
    :try_end_f3
    .catch Ljava/io/IOException; {:try_start_5a .. :try_end_f3} :catch_15f
    .catch Llibcore/io/ErrnoException; {:try_start_5a .. :try_end_f3} :catch_15b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5a .. :try_end_f3} :catch_157
    .catch Lcom/android/internal/os/ZygoteSecurityException; {:try_start_5a .. :try_end_f3} :catch_153

    #@f3
    move-result v19

    #@f4
    move-object/from16 v17, v18

    #@f6
    .line 250
    .end local v8           #rlimits:[[I
    .end local v18           #parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    .restart local v17       #parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    :goto_f6
    if-nez v19, :cond_133

    #@f8
    .line 252
    :try_start_f8
    invoke-static/range {v21 .. v21}, Llibcore/io/IoUtils;->closeQuietly(Ljava/io/FileDescriptor;)V

    #@fb
    .line 253
    const/16 v21, 0x0

    #@fd
    .line 254
    move-object/from16 v0, p0

    #@ff
    move-object/from16 v1, v17

    #@101
    move-object/from16 v2, v16

    #@103
    invoke-direct {v0, v1, v14, v13, v2}, Lcom/android/internal/os/ZygoteConnection;->handleChildProc(Lcom/android/internal/os/ZygoteConnection$Arguments;[Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/PrintStream;)V
    :try_end_106
    .catchall {:try_start_f8 .. :try_end_106} :catchall_14b

    #@106
    .line 258
    const/4 v4, 0x1

    #@107
    .line 266
    invoke-static {v13}, Llibcore/io/IoUtils;->closeQuietly(Ljava/io/FileDescriptor;)V

    #@10a
    .line 267
    invoke-static/range {v21 .. v21}, Llibcore/io/IoUtils;->closeQuietly(Ljava/io/FileDescriptor;)V

    #@10d
    goto/16 :goto_14

    #@10f
    .line 238
    :catch_10f
    move-exception v15

    #@110
    .line 239
    .restart local v15       #ex:Ljava/io/IOException;
    :goto_110
    const-string v4, "Exception creating pipe"

    #@112
    move-object/from16 v0, v16

    #@114
    invoke-static {v0, v4, v15}, Lcom/android/internal/os/ZygoteConnection;->logAndPrintError(Ljava/io/PrintStream;Ljava/lang/String;Ljava/lang/Throwable;)V

    #@117
    goto :goto_f6

    #@118
    .line 240
    .end local v15           #ex:Ljava/io/IOException;
    :catch_118
    move-exception v15

    #@119
    .line 241
    .local v15, ex:Llibcore/io/ErrnoException;
    :goto_119
    const-string v4, "Exception creating pipe"

    #@11b
    move-object/from16 v0, v16

    #@11d
    invoke-static {v0, v4, v15}, Lcom/android/internal/os/ZygoteConnection;->logAndPrintError(Ljava/io/PrintStream;Ljava/lang/String;Ljava/lang/Throwable;)V

    #@120
    goto :goto_f6

    #@121
    .line 242
    .end local v15           #ex:Llibcore/io/ErrnoException;
    :catch_121
    move-exception v15

    #@122
    .line 243
    .local v15, ex:Ljava/lang/IllegalArgumentException;
    :goto_122
    const-string v4, "Invalid zygote arguments"

    #@124
    move-object/from16 v0, v16

    #@126
    invoke-static {v0, v4, v15}, Lcom/android/internal/os/ZygoteConnection;->logAndPrintError(Ljava/io/PrintStream;Ljava/lang/String;Ljava/lang/Throwable;)V

    #@129
    goto :goto_f6

    #@12a
    .line 244
    .end local v15           #ex:Ljava/lang/IllegalArgumentException;
    :catch_12a
    move-exception v15

    #@12b
    .line 245
    .local v15, ex:Lcom/android/internal/os/ZygoteSecurityException;
    :goto_12b
    const-string v4, "Zygote security policy prevents request: "

    #@12d
    move-object/from16 v0, v16

    #@12f
    invoke-static {v0, v4, v15}, Lcom/android/internal/os/ZygoteConnection;->logAndPrintError(Ljava/io/PrintStream;Ljava/lang/String;Ljava/lang/Throwable;)V

    #@132
    goto :goto_f6

    #@133
    .line 261
    .end local v15           #ex:Lcom/android/internal/os/ZygoteSecurityException;
    :cond_133
    :try_start_133
    invoke-static {v13}, Llibcore/io/IoUtils;->closeQuietly(Ljava/io/FileDescriptor;)V

    #@136
    .line 262
    const/4 v13, 0x0

    #@137
    .line 263
    move-object/from16 v0, p0

    #@139
    move/from16 v1, v19

    #@13b
    move-object/from16 v2, v21

    #@13d
    move-object/from16 v3, v17

    #@13f
    invoke-direct {v0, v1, v14, v2, v3}, Lcom/android/internal/os/ZygoteConnection;->handleParentProc(I[Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Lcom/android/internal/os/ZygoteConnection$Arguments;)Z
    :try_end_142
    .catchall {:try_start_133 .. :try_end_142} :catchall_14b

    #@142
    move-result v4

    #@143
    .line 266
    invoke-static {v13}, Llibcore/io/IoUtils;->closeQuietly(Ljava/io/FileDescriptor;)V

    #@146
    .line 267
    invoke-static/range {v21 .. v21}, Llibcore/io/IoUtils;->closeQuietly(Ljava/io/FileDescriptor;)V

    #@149
    goto/16 :goto_14

    #@14b
    .line 266
    :catchall_14b
    move-exception v4

    #@14c
    invoke-static {v13}, Llibcore/io/IoUtils;->closeQuietly(Ljava/io/FileDescriptor;)V

    #@14f
    .line 267
    invoke-static/range {v21 .. v21}, Llibcore/io/IoUtils;->closeQuietly(Ljava/io/FileDescriptor;)V

    #@152
    throw v4

    #@153
    .line 244
    .end local v17           #parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    .restart local v18       #parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    :catch_153
    move-exception v15

    #@154
    move-object/from16 v17, v18

    #@156
    .end local v18           #parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    .restart local v17       #parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    goto :goto_12b

    #@157
    .line 242
    .end local v17           #parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    .restart local v18       #parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    :catch_157
    move-exception v15

    #@158
    move-object/from16 v17, v18

    #@15a
    .end local v18           #parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    .restart local v17       #parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    goto :goto_122

    #@15b
    .line 240
    .end local v17           #parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    .restart local v18       #parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    :catch_15b
    move-exception v15

    #@15c
    move-object/from16 v17, v18

    #@15e
    .end local v18           #parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    .restart local v17       #parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    goto :goto_119

    #@15f
    .line 238
    .end local v17           #parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    .restart local v18       #parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    :catch_15f
    move-exception v15

    #@160
    move-object/from16 v17, v18

    #@162
    .end local v18           #parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    .restart local v17       #parsedArgs:Lcom/android/internal/os/ZygoteConnection$Arguments;
    goto :goto_110
.end method
