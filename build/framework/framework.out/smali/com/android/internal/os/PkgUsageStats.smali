.class public Lcom/android/internal/os/PkgUsageStats;
.super Ljava/lang/Object;
.source "PkgUsageStats.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/internal/os/PkgUsageStats;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public componentResumeTimes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public launchCount:I

.field public packageName:Ljava/lang/String;

.field public usageTime:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 36
    new-instance v0, Lcom/android/internal/os/PkgUsageStats$1;

    #@2
    invoke-direct {v0}, Lcom/android/internal/os/PkgUsageStats$1;-><init>()V

    #@5
    sput-object v0, Lcom/android/internal/os/PkgUsageStats;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 9
    .parameter "source"

    #@0
    .prologue
    .line 60
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 61
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6
    move-result-object v5

    #@7
    iput-object v5, p0, Lcom/android/internal/os/PkgUsageStats;->packageName:Ljava/lang/String;

    #@9
    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v5

    #@d
    iput v5, p0, Lcom/android/internal/os/PkgUsageStats;->launchCount:I

    #@f
    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@12
    move-result-wide v5

    #@13
    iput-wide v5, p0, Lcom/android/internal/os/PkgUsageStats;->usageTime:J

    #@15
    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v0

    #@19
    .line 65
    .local v0, N:I
    new-instance v5, Ljava/util/HashMap;

    #@1b
    invoke-direct {v5, v0}, Ljava/util/HashMap;-><init>(I)V

    #@1e
    iput-object v5, p0, Lcom/android/internal/os/PkgUsageStats;->componentResumeTimes:Ljava/util/Map;

    #@20
    .line 66
    const/4 v2, 0x0

    #@21
    .local v2, i:I
    :goto_21
    if-ge v2, v0, :cond_37

    #@23
    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    .line 68
    .local v1, component:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@2a
    move-result-wide v3

    #@2b
    .line 69
    .local v3, lastResumeTime:J
    iget-object v5, p0, Lcom/android/internal/os/PkgUsageStats;->componentResumeTimes:Ljava/util/Map;

    #@2d
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@30
    move-result-object v6

    #@31
    invoke-interface {v5, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@34
    .line 66
    add-int/lit8 v2, v2, 0x1

    #@36
    goto :goto_21

    #@37
    .line 71
    .end local v1           #component:Ljava/lang/String;
    .end local v3           #lastResumeTime:J
    :cond_37
    return-void
.end method

.method public constructor <init>(Lcom/android/internal/os/PkgUsageStats;)V
    .registers 4
    .parameter "pStats"

    #@0
    .prologue
    .line 73
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 74
    iget-object v0, p1, Lcom/android/internal/os/PkgUsageStats;->packageName:Ljava/lang/String;

    #@5
    iput-object v0, p0, Lcom/android/internal/os/PkgUsageStats;->packageName:Ljava/lang/String;

    #@7
    .line 75
    iget v0, p1, Lcom/android/internal/os/PkgUsageStats;->launchCount:I

    #@9
    iput v0, p0, Lcom/android/internal/os/PkgUsageStats;->launchCount:I

    #@b
    .line 76
    iget-wide v0, p1, Lcom/android/internal/os/PkgUsageStats;->usageTime:J

    #@d
    iput-wide v0, p0, Lcom/android/internal/os/PkgUsageStats;->usageTime:J

    #@f
    .line 77
    new-instance v0, Ljava/util/HashMap;

    #@11
    iget-object v1, p1, Lcom/android/internal/os/PkgUsageStats;->componentResumeTimes:Ljava/util/Map;

    #@13
    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    #@16
    iput-object v0, p0, Lcom/android/internal/os/PkgUsageStats;->componentResumeTimes:Ljava/util/Map;

    #@18
    .line 78
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IJLjava/util/Map;)V
    .registers 7
    .parameter "pkgName"
    .parameter "count"
    .parameter "time"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IJ",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 53
    .local p5, lastResumeTimes:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 54
    iput-object p1, p0, Lcom/android/internal/os/PkgUsageStats;->packageName:Ljava/lang/String;

    #@5
    .line 55
    iput p2, p0, Lcom/android/internal/os/PkgUsageStats;->launchCount:I

    #@7
    .line 56
    iput-wide p3, p0, Lcom/android/internal/os/PkgUsageStats;->usageTime:J

    #@9
    .line 57
    new-instance v0, Ljava/util/HashMap;

    #@b
    invoke-direct {v0, p5}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    #@e
    iput-object v0, p0, Lcom/android/internal/os/PkgUsageStats;->componentResumeTimes:Ljava/util/Map;

    #@10
    .line 58
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 81
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "PkgUsageStats{"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@e
    move-result v1

    #@f
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    const-string v1, " "

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    iget-object v1, p0, Lcom/android/internal/os/PkgUsageStats;->packageName:Ljava/lang/String;

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    const-string/jumbo v1, "}"

    #@26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v0

    #@2a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v0

    #@2e
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 7
    .parameter "dest"
    .parameter "parcelableFlags"

    #@0
    .prologue
    .line 85
    iget-object v2, p0, Lcom/android/internal/os/PkgUsageStats;->packageName:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5
    .line 86
    iget v2, p0, Lcom/android/internal/os/PkgUsageStats;->launchCount:I

    #@7
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 87
    iget-wide v2, p0, Lcom/android/internal/os/PkgUsageStats;->usageTime:J

    #@c
    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    #@f
    .line 88
    iget-object v2, p0, Lcom/android/internal/os/PkgUsageStats;->componentResumeTimes:Ljava/util/Map;

    #@11
    invoke-interface {v2}, Ljava/util/Map;->size()I

    #@14
    move-result v2

    #@15
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 89
    iget-object v2, p0, Lcom/android/internal/os/PkgUsageStats;->componentResumeTimes:Ljava/util/Map;

    #@1a
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@1d
    move-result-object v2

    #@1e
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@21
    move-result-object v1

    #@22
    .local v1, i$:Ljava/util/Iterator;
    :goto_22
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@25
    move-result v2

    #@26
    if-eqz v2, :cond_45

    #@28
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2b
    move-result-object v0

    #@2c
    check-cast v0, Ljava/util/Map$Entry;

    #@2e
    .line 90
    .local v0, ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@31
    move-result-object v2

    #@32
    check-cast v2, Ljava/lang/String;

    #@34
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@37
    .line 91
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@3a
    move-result-object v2

    #@3b
    check-cast v2, Ljava/lang/Long;

    #@3d
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    #@40
    move-result-wide v2

    #@41
    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    #@44
    goto :goto_22

    #@45
    .line 93
    .end local v0           #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
    :cond_45
    return-void
.end method
