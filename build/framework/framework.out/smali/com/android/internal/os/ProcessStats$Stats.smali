.class public Lcom/android/internal/os/ProcessStats$Stats;
.super Ljava/lang/Object;
.source "ProcessStats.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/os/ProcessStats;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Stats"
.end annotation


# instance fields
.field public active:Z

.field public added:Z

.field public baseName:Ljava/lang/String;

.field public base_majfaults:J

.field public base_minfaults:J

.field public base_stime:J

.field public base_uptime:J

.field public base_utime:J

.field final cmdlineFile:Ljava/lang/String;

.field public interesting:Z

.field public name:Ljava/lang/String;

.field public nameWidth:I

.field public final pid:I

.field public rel_majfaults:I

.field public rel_minfaults:I

.field public rel_stime:I

.field public rel_uptime:J

.field public rel_utime:I

.field public removed:Z

.field final statFile:Ljava/lang/String;

.field final threadStats:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/os/ProcessStats$Stats;",
            ">;"
        }
    .end annotation
.end field

.field final threadsDir:Ljava/lang/String;

.field public working:Z

.field final workingThreads:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/os/ProcessStats$Stats;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(IIZ)V
    .registers 9
    .parameter "_pid"
    .parameter "parentPid"
    .parameter "includeThreads"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 206
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 207
    iput p1, p0, Lcom/android/internal/os/ProcessStats$Stats;->pid:I

    #@6
    .line 208
    if-gez p2, :cond_54

    #@8
    .line 209
    new-instance v0, Ljava/io/File;

    #@a
    const-string v2, "/proc"

    #@c
    iget v3, p0, Lcom/android/internal/os/ProcessStats$Stats;->pid:I

    #@e
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@11
    move-result-object v3

    #@12
    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@15
    .line 210
    .local v0, procDir:Ljava/io/File;
    new-instance v2, Ljava/io/File;

    #@17
    const-string/jumbo v3, "stat"

    #@1a
    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@1d
    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    iput-object v2, p0, Lcom/android/internal/os/ProcessStats$Stats;->statFile:Ljava/lang/String;

    #@23
    .line 211
    new-instance v2, Ljava/io/File;

    #@25
    const-string v3, "cmdline"

    #@27
    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@2a
    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    iput-object v2, p0, Lcom/android/internal/os/ProcessStats$Stats;->cmdlineFile:Ljava/lang/String;

    #@30
    .line 212
    new-instance v2, Ljava/io/File;

    #@32
    const-string/jumbo v3, "task"

    #@35
    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@38
    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    #@3b
    move-result-object v2

    #@3c
    iput-object v2, p0, Lcom/android/internal/os/ProcessStats$Stats;->threadsDir:Ljava/lang/String;

    #@3e
    .line 213
    if-eqz p3, :cond_4f

    #@40
    .line 214
    new-instance v2, Ljava/util/ArrayList;

    #@42
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@45
    iput-object v2, p0, Lcom/android/internal/os/ProcessStats$Stats;->threadStats:Ljava/util/ArrayList;

    #@47
    .line 215
    new-instance v2, Ljava/util/ArrayList;

    #@49
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@4c
    iput-object v2, p0, Lcom/android/internal/os/ProcessStats$Stats;->workingThreads:Ljava/util/ArrayList;

    #@4e
    .line 231
    :goto_4e
    return-void

    #@4f
    .line 217
    :cond_4f
    iput-object v4, p0, Lcom/android/internal/os/ProcessStats$Stats;->threadStats:Ljava/util/ArrayList;

    #@51
    .line 218
    iput-object v4, p0, Lcom/android/internal/os/ProcessStats$Stats;->workingThreads:Ljava/util/ArrayList;

    #@53
    goto :goto_4e

    #@54
    .line 221
    .end local v0           #procDir:Ljava/io/File;
    :cond_54
    new-instance v0, Ljava/io/File;

    #@56
    const-string v2, "/proc"

    #@58
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@5b
    move-result-object v3

    #@5c
    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@5f
    .line 223
    .restart local v0       #procDir:Ljava/io/File;
    new-instance v1, Ljava/io/File;

    #@61
    new-instance v2, Ljava/io/File;

    #@63
    const-string/jumbo v3, "task"

    #@66
    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@69
    iget v3, p0, Lcom/android/internal/os/ProcessStats$Stats;->pid:I

    #@6b
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@6e
    move-result-object v3

    #@6f
    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@72
    .line 225
    .local v1, taskDir:Ljava/io/File;
    new-instance v2, Ljava/io/File;

    #@74
    const-string/jumbo v3, "stat"

    #@77
    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@7a
    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    #@7d
    move-result-object v2

    #@7e
    iput-object v2, p0, Lcom/android/internal/os/ProcessStats$Stats;->statFile:Ljava/lang/String;

    #@80
    .line 226
    iput-object v4, p0, Lcom/android/internal/os/ProcessStats$Stats;->cmdlineFile:Ljava/lang/String;

    #@82
    .line 227
    iput-object v4, p0, Lcom/android/internal/os/ProcessStats$Stats;->threadsDir:Ljava/lang/String;

    #@84
    .line 228
    iput-object v4, p0, Lcom/android/internal/os/ProcessStats$Stats;->threadStats:Ljava/util/ArrayList;

    #@86
    .line 229
    iput-object v4, p0, Lcom/android/internal/os/ProcessStats$Stats;->workingThreads:Ljava/util/ArrayList;

    #@88
    goto :goto_4e
.end method
