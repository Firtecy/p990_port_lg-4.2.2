.class Lcom/android/internal/os/RuntimeInit$UncaughtHandler;
.super Ljava/lang/Object;
.source "RuntimeInit.java"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/os/RuntimeInit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UncaughtHandler"
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 62
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/os/RuntimeInit$1;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    invoke-direct {p0}, Lcom/android/internal/os/RuntimeInit$UncaughtHandler;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .registers 8
    .parameter "t"
    .parameter "e"

    #@0
    .prologue
    const/16 v4, 0xa

    #@2
    .line 66
    invoke-virtual {p1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    const-string v2, "FinalizerWatchdogDaemon"

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_39

    #@e
    .line 67
    const-string v1, "AndroidRuntime"

    #@10
    new-instance v2, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v3, "UncaughtHandler(Pid:"

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@1e
    move-result v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    const-string v3, ") "

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {p1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v2

    #@35
    invoke-static {v1, v2, p2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@38
    .line 96
    :goto_38
    return-void

    #@39
    .line 73
    :cond_39
    :try_start_39
    invoke-static {}, Lcom/android/internal/os/RuntimeInit;->access$000()Z
    :try_end_3c
    .catchall {:try_start_39 .. :try_end_3c} :catchall_bc
    .catch Ljava/lang/Throwable; {:try_start_39 .. :try_end_3c} :catch_a8

    #@3c
    move-result v1

    #@3d
    if-eqz v1, :cond_4a

    #@3f
    .line 93
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@42
    move-result v1

    #@43
    invoke-static {v1}, Landroid/os/Process;->killProcess(I)V

    #@46
    .line 94
    invoke-static {v4}, Ljava/lang/System;->exit(I)V

    #@49
    goto :goto_38

    #@4a
    .line 74
    :cond_4a
    const/4 v1, 0x1

    #@4b
    :try_start_4b
    invoke-static {v1}, Lcom/android/internal/os/RuntimeInit;->access$002(Z)Z

    #@4e
    .line 76
    invoke-static {}, Lcom/android/internal/os/RuntimeInit;->access$100()Landroid/os/IBinder;

    #@51
    move-result-object v1

    #@52
    if-nez v1, :cond_8b

    #@54
    .line 77
    const-string v1, "AndroidRuntime"

    #@56
    new-instance v2, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string v3, "*** FATAL EXCEPTION IN SYSTEM PROCESS: "

    #@5d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v2

    #@61
    invoke-virtual {p1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    #@64
    move-result-object v3

    #@65
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v2

    #@69
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v2

    #@6d
    invoke-static {v1, v2, p2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@70
    .line 83
    :goto_70
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@73
    move-result-object v1

    #@74
    invoke-static {}, Lcom/android/internal/os/RuntimeInit;->access$100()Landroid/os/IBinder;

    #@77
    move-result-object v2

    #@78
    new-instance v3, Landroid/app/ApplicationErrorReport$CrashInfo;

    #@7a
    invoke-direct {v3, p2}, Landroid/app/ApplicationErrorReport$CrashInfo;-><init>(Ljava/lang/Throwable;)V

    #@7d
    invoke-interface {v1, v2, v3}, Landroid/app/IActivityManager;->handleApplicationCrash(Landroid/os/IBinder;Landroid/app/ApplicationErrorReport$CrashInfo;)V
    :try_end_80
    .catchall {:try_start_4b .. :try_end_80} :catchall_bc
    .catch Ljava/lang/Throwable; {:try_start_4b .. :try_end_80} :catch_a8

    #@80
    .line 93
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@83
    move-result v1

    #@84
    invoke-static {v1}, Landroid/os/Process;->killProcess(I)V

    #@87
    .line 94
    invoke-static {v4}, Ljava/lang/System;->exit(I)V

    #@8a
    goto :goto_38

    #@8b
    .line 79
    :cond_8b
    :try_start_8b
    const-string v1, "AndroidRuntime"

    #@8d
    new-instance v2, Ljava/lang/StringBuilder;

    #@8f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@92
    const-string v3, "FATAL EXCEPTION: "

    #@94
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v2

    #@98
    invoke-virtual {p1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    #@9b
    move-result-object v3

    #@9c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v2

    #@a0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a3
    move-result-object v2

    #@a4
    invoke-static {v1, v2, p2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_a7
    .catchall {:try_start_8b .. :try_end_a7} :catchall_bc
    .catch Ljava/lang/Throwable; {:try_start_8b .. :try_end_a7} :catch_a8

    #@a7
    goto :goto_70

    #@a8
    .line 85
    :catch_a8
    move-exception v0

    #@a9
    .line 87
    .local v0, t2:Ljava/lang/Throwable;
    :try_start_a9
    const-string v1, "AndroidRuntime"

    #@ab
    const-string v2, "Error reporting crash"

    #@ad
    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_b0
    .catchall {:try_start_a9 .. :try_end_b0} :catchall_bc
    .catch Ljava/lang/Throwable; {:try_start_a9 .. :try_end_b0} :catch_c8

    #@b0
    .line 93
    :goto_b0
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@b3
    move-result v1

    #@b4
    invoke-static {v1}, Landroid/os/Process;->killProcess(I)V

    #@b7
    .line 94
    invoke-static {v4}, Ljava/lang/System;->exit(I)V

    #@ba
    goto/16 :goto_38

    #@bc
    .line 93
    .end local v0           #t2:Ljava/lang/Throwable;
    :catchall_bc
    move-exception v1

    #@bd
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@c0
    move-result v2

    #@c1
    invoke-static {v2}, Landroid/os/Process;->killProcess(I)V

    #@c4
    .line 94
    invoke-static {v4}, Ljava/lang/System;->exit(I)V

    #@c7
    throw v1

    #@c8
    .line 88
    .restart local v0       #t2:Ljava/lang/Throwable;
    :catch_c8
    move-exception v1

    #@c9
    goto :goto_b0
.end method
