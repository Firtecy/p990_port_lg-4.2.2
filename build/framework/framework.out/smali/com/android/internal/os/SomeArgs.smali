.class public final Lcom/android/internal/os/SomeArgs;
.super Ljava/lang/Object;
.source "SomeArgs.java"


# static fields
.field private static final MAX_POOL_SIZE:I = 0xa

.field private static sPool:Lcom/android/internal/os/SomeArgs;

.field private static sPoolLock:Ljava/lang/Object;

.field private static sPoolSize:I


# instance fields
.field public arg1:Ljava/lang/Object;

.field public arg2:Ljava/lang/Object;

.field public arg3:Ljava/lang/Object;

.field public arg4:Ljava/lang/Object;

.field public argi1:I

.field public argi2:I

.field public argi3:I

.field public argi4:I

.field public argi5:I

.field public argi6:I

.field private mInPool:Z

.field private mNext:Lcom/android/internal/os/SomeArgs;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 32
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Lcom/android/internal/os/SomeArgs;->sPoolLock:Ljava/lang/Object;

    #@7
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 49
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 51
    return-void
.end method

.method private clear()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 84
    iput-object v1, p0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@4
    .line 85
    iput-object v1, p0, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    #@6
    .line 86
    iput-object v1, p0, Lcom/android/internal/os/SomeArgs;->arg3:Ljava/lang/Object;

    #@8
    .line 87
    iput-object v1, p0, Lcom/android/internal/os/SomeArgs;->arg4:Ljava/lang/Object;

    #@a
    .line 88
    iput v0, p0, Lcom/android/internal/os/SomeArgs;->argi1:I

    #@c
    .line 89
    iput v0, p0, Lcom/android/internal/os/SomeArgs;->argi2:I

    #@e
    .line 90
    iput v0, p0, Lcom/android/internal/os/SomeArgs;->argi3:I

    #@10
    .line 91
    iput v0, p0, Lcom/android/internal/os/SomeArgs;->argi4:I

    #@12
    .line 92
    iput v0, p0, Lcom/android/internal/os/SomeArgs;->argi5:I

    #@14
    .line 93
    iput v0, p0, Lcom/android/internal/os/SomeArgs;->argi6:I

    #@16
    .line 94
    return-void
.end method

.method public static obtain()Lcom/android/internal/os/SomeArgs;
    .registers 3

    #@0
    .prologue
    .line 54
    sget-object v2, Lcom/android/internal/os/SomeArgs;->sPoolLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 55
    :try_start_3
    sget v1, Lcom/android/internal/os/SomeArgs;->sPoolSize:I

    #@5
    if-lez v1, :cond_1d

    #@7
    .line 56
    sget-object v0, Lcom/android/internal/os/SomeArgs;->sPool:Lcom/android/internal/os/SomeArgs;

    #@9
    .line 57
    .local v0, args:Lcom/android/internal/os/SomeArgs;
    sget-object v1, Lcom/android/internal/os/SomeArgs;->sPool:Lcom/android/internal/os/SomeArgs;

    #@b
    iget-object v1, v1, Lcom/android/internal/os/SomeArgs;->mNext:Lcom/android/internal/os/SomeArgs;

    #@d
    sput-object v1, Lcom/android/internal/os/SomeArgs;->sPool:Lcom/android/internal/os/SomeArgs;

    #@f
    .line 58
    const/4 v1, 0x0

    #@10
    iput-object v1, v0, Lcom/android/internal/os/SomeArgs;->mNext:Lcom/android/internal/os/SomeArgs;

    #@12
    .line 59
    const/4 v1, 0x0

    #@13
    iput-boolean v1, v0, Lcom/android/internal/os/SomeArgs;->mInPool:Z

    #@15
    .line 60
    sget v1, Lcom/android/internal/os/SomeArgs;->sPoolSize:I

    #@17
    add-int/lit8 v1, v1, -0x1

    #@19
    sput v1, Lcom/android/internal/os/SomeArgs;->sPoolSize:I

    #@1b
    .line 61
    monitor-exit v2

    #@1c
    .line 63
    .end local v0           #args:Lcom/android/internal/os/SomeArgs;
    :goto_1c
    return-object v0

    #@1d
    :cond_1d
    new-instance v0, Lcom/android/internal/os/SomeArgs;

    #@1f
    invoke-direct {v0}, Lcom/android/internal/os/SomeArgs;-><init>()V

    #@22
    monitor-exit v2

    #@23
    goto :goto_1c

    #@24
    .line 65
    :catchall_24
    move-exception v1

    #@25
    monitor-exit v2
    :try_end_26
    .catchall {:try_start_3 .. :try_end_26} :catchall_24

    #@26
    throw v1
.end method


# virtual methods
.method public recycle()V
    .registers 4

    #@0
    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/android/internal/os/SomeArgs;->mInPool:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 70
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "Already recycled."

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 72
    :cond_c
    sget-object v1, Lcom/android/internal/os/SomeArgs;->sPoolLock:Ljava/lang/Object;

    #@e
    monitor-enter v1

    #@f
    .line 73
    :try_start_f
    invoke-direct {p0}, Lcom/android/internal/os/SomeArgs;->clear()V

    #@12
    .line 74
    sget v0, Lcom/android/internal/os/SomeArgs;->sPoolSize:I

    #@14
    const/16 v2, 0xa

    #@16
    if-ge v0, v2, :cond_27

    #@18
    .line 75
    sget-object v0, Lcom/android/internal/os/SomeArgs;->sPool:Lcom/android/internal/os/SomeArgs;

    #@1a
    iput-object v0, p0, Lcom/android/internal/os/SomeArgs;->mNext:Lcom/android/internal/os/SomeArgs;

    #@1c
    .line 76
    const/4 v0, 0x1

    #@1d
    iput-boolean v0, p0, Lcom/android/internal/os/SomeArgs;->mInPool:Z

    #@1f
    .line 77
    sput-object p0, Lcom/android/internal/os/SomeArgs;->sPool:Lcom/android/internal/os/SomeArgs;

    #@21
    .line 78
    sget v0, Lcom/android/internal/os/SomeArgs;->sPoolSize:I

    #@23
    add-int/lit8 v0, v0, 0x1

    #@25
    sput v0, Lcom/android/internal/os/SomeArgs;->sPoolSize:I

    #@27
    .line 80
    :cond_27
    monitor-exit v1

    #@28
    .line 81
    return-void

    #@29
    .line 80
    :catchall_29
    move-exception v0

    #@2a
    monitor-exit v1
    :try_end_2b
    .catchall {:try_start_f .. :try_end_2b} :catchall_29

    #@2b
    throw v0
.end method
