.class public final Lcom/android/internal/os/BatteryStatsImpl$Uid;
.super Landroid/os/BatteryStats$Uid;
.source "BatteryStatsImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/os/BatteryStatsImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "Uid"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;,
        Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;,
        Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;,
        Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;
    }
.end annotation


# instance fields
.field mAudioTurnedOn:Z

.field mAudioTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

.field mCurrentTcpBytesReceived:J

.field mCurrentTcpBytesSent:J

.field mFullWifiLockOut:Z

.field mFullWifiLockTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

.field mLoadedTcpBytesReceived:J

.field mLoadedTcpBytesSent:J

.field final mPackageStats:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;",
            ">;"
        }
    .end annotation
.end field

.field final mPids:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/BatteryStats$Uid$Pid;",
            ">;"
        }
    .end annotation
.end field

.field final mProcessStats:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;",
            ">;"
        }
    .end annotation
.end field

.field final mSensorStats:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;",
            ">;"
        }
    .end annotation
.end field

.field mStartedTcpBytesReceived:J

.field mStartedTcpBytesSent:J

.field mTcpBytesReceivedAtLastUnplug:J

.field mTcpBytesSentAtLastUnplug:J

.field final mUid:I

.field mUserActivityCounters:[Lcom/android/internal/os/BatteryStatsImpl$Counter;

.field mVideoTurnedOn:Z

.field mVideoTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

.field final mWakelockStats:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;",
            ">;"
        }
    .end annotation
.end field

.field mWifiMulticastEnabled:Z

.field mWifiMulticastTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

.field mWifiRunning:Z

.field mWifiRunningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

.field mWifiScanStarted:Z

.field mWifiScanTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

.field final synthetic this$0:Lcom/android/internal/os/BatteryStatsImpl;


# direct methods
.method public constructor <init>(Lcom/android/internal/os/BatteryStatsImpl;I)V
    .registers 9
    .parameter
    .parameter "uid"

    #@0
    .prologue
    const-wide/16 v0, -0x1

    #@2
    const/4 v5, 0x0

    #@3
    const/4 v4, 0x7

    #@4
    .line 2432
    iput-object p1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@6
    invoke-direct {p0}, Landroid/os/BatteryStats$Uid;-><init>()V

    #@9
    .line 2384
    iput-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mStartedTcpBytesReceived:J

    #@b
    .line 2385
    iput-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mStartedTcpBytesSent:J

    #@d
    .line 2410
    new-instance v0, Ljava/util/HashMap;

    #@f
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@12
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWakelockStats:Ljava/util/HashMap;

    #@14
    .line 2415
    new-instance v0, Ljava/util/HashMap;

    #@16
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@19
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mSensorStats:Ljava/util/HashMap;

    #@1b
    .line 2420
    new-instance v0, Ljava/util/HashMap;

    #@1d
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@20
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mProcessStats:Ljava/util/HashMap;

    #@22
    .line 2425
    new-instance v0, Ljava/util/HashMap;

    #@24
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@27
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mPackageStats:Ljava/util/HashMap;

    #@29
    .line 2430
    new-instance v0, Landroid/util/SparseArray;

    #@2b
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@2e
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mPids:Landroid/util/SparseArray;

    #@30
    .line 2433
    iput p2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mUid:I

    #@32
    .line 2434
    new-instance v0, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@34
    const/4 v1, 0x4

    #@35
    iget-object v2, p1, Lcom/android/internal/os/BatteryStatsImpl;->mWifiRunningTimers:Ljava/util/ArrayList;

    #@37
    iget-object v3, p1, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@39
    invoke-direct {v0, p0, v1, v2, v3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    #@3c
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiRunningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@3e
    .line 2436
    new-instance v0, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@40
    const/4 v1, 0x5

    #@41
    iget-object v2, p1, Lcom/android/internal/os/BatteryStatsImpl;->mFullWifiLockTimers:Ljava/util/ArrayList;

    #@43
    iget-object v3, p1, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@45
    invoke-direct {v0, p0, v1, v2, v3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    #@48
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mFullWifiLockTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@4a
    .line 2438
    new-instance v0, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@4c
    const/4 v1, 0x6

    #@4d
    iget-object v2, p1, Lcom/android/internal/os/BatteryStatsImpl;->mWifiScanTimers:Ljava/util/ArrayList;

    #@4f
    iget-object v3, p1, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@51
    invoke-direct {v0, p0, v1, v2, v3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    #@54
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiScanTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@56
    .line 2440
    new-instance v0, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@58
    iget-object v1, p1, Lcom/android/internal/os/BatteryStatsImpl;->mWifiMulticastTimers:Ljava/util/ArrayList;

    #@5a
    iget-object v2, p1, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@5c
    invoke-direct {v0, p0, v4, v1, v2}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    #@5f
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiMulticastTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@61
    .line 2442
    new-instance v0, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@63
    iget-object v1, p1, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@65
    invoke-direct {v0, p0, v4, v5, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    #@68
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mAudioTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@6a
    .line 2444
    new-instance v0, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@6c
    const/16 v1, 0x8

    #@6e
    iget-object v2, p1, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@70
    invoke-direct {v0, p0, v1, v5, v2}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    #@73
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mVideoTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@75
    .line 2446
    return-void
.end method


# virtual methods
.method public computeCurrentTcpBytesReceived()J
    .registers 9

    #@0
    .prologue
    const-wide/16 v2, 0x0

    #@2
    .line 2489
    iget-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@4
    invoke-static {v4}, Lcom/android/internal/os/BatteryStatsImpl;->access$100(Lcom/android/internal/os/BatteryStatsImpl;)Landroid/net/NetworkStats;

    #@7
    move-result-object v4

    #@8
    const/4 v5, 0x0

    #@9
    iget v6, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mUid:I

    #@b
    invoke-virtual {v4, v5, v6}, Landroid/net/NetworkStats;->getTotal(Landroid/net/NetworkStats$Entry;I)Landroid/net/NetworkStats$Entry;

    #@e
    move-result-object v4

    #@f
    iget-wide v0, v4, Landroid/net/NetworkStats$Entry;->rxBytes:J

    #@11
    .line 2491
    .local v0, uidRxBytes:J
    iget-wide v4, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mCurrentTcpBytesReceived:J

    #@13
    iget-wide v6, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mStartedTcpBytesReceived:J

    #@15
    cmp-long v6, v6, v2

    #@17
    if-ltz v6, :cond_1d

    #@19
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mStartedTcpBytesReceived:J

    #@1b
    sub-long v2, v0, v2

    #@1d
    :cond_1d
    add-long/2addr v2, v4

    #@1e
    return-wide v2
.end method

.method public computeCurrentTcpBytesSent()J
    .registers 9

    #@0
    .prologue
    const-wide/16 v2, 0x0

    #@2
    .line 2713
    iget-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@4
    invoke-static {v4}, Lcom/android/internal/os/BatteryStatsImpl;->access$100(Lcom/android/internal/os/BatteryStatsImpl;)Landroid/net/NetworkStats;

    #@7
    move-result-object v4

    #@8
    const/4 v5, 0x0

    #@9
    iget v6, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mUid:I

    #@b
    invoke-virtual {v4, v5, v6}, Landroid/net/NetworkStats;->getTotal(Landroid/net/NetworkStats$Entry;I)Landroid/net/NetworkStats$Entry;

    #@e
    move-result-object v4

    #@f
    iget-wide v0, v4, Landroid/net/NetworkStats$Entry;->txBytes:J

    #@11
    .line 2715
    .local v0, uidTxBytes:J
    iget-wide v4, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mCurrentTcpBytesSent:J

    #@13
    iget-wide v6, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mStartedTcpBytesSent:J

    #@15
    cmp-long v6, v6, v2

    #@17
    if-ltz v6, :cond_1d

    #@19
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mStartedTcpBytesSent:J

    #@1b
    sub-long v2, v0, v2

    #@1d
    :cond_1d
    add-long/2addr v2, v4

    #@1e
    return-wide v2
.end method

.method public getAudioTurnedOnTime(JI)J
    .registers 6
    .parameter "batteryRealtime"
    .parameter "which"

    #@0
    .prologue
    .line 2665
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mAudioTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2
    if-nez v0, :cond_7

    #@4
    .line 2666
    const-wide/16 v0, 0x0

    #@6
    .line 2668
    :goto_6
    return-wide v0

    #@7
    :cond_7
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mAudioTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@9
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->getTotalTimeLocked(JI)J

    #@c
    move-result-wide v0

    #@d
    goto :goto_6
.end method

.method public getBatteryStats()Lcom/android/internal/os/BatteryStatsImpl;
    .registers 2

    #@0
    .prologue
    .line 4080
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@2
    return-object v0
.end method

.method public getFullWifiLockTime(JI)J
    .registers 6
    .parameter "batteryRealtime"
    .parameter "which"

    #@0
    .prologue
    .line 2640
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mFullWifiLockTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2
    if-nez v0, :cond_7

    #@4
    .line 2641
    const-wide/16 v0, 0x0

    #@6
    .line 2643
    :goto_6
    return-wide v0

    #@7
    :cond_7
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mFullWifiLockTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@9
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->getTotalTimeLocked(JI)J

    #@c
    move-result-wide v0

    #@d
    goto :goto_6
.end method

.method public getPackageStats()Ljava/util/Map;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "+",
            "Landroid/os/BatteryStats$Uid$Pkg;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 2465
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mPackageStats:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method public getPackageStatsLocked(Ljava/lang/String;)Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;
    .registers 4
    .parameter "name"

    #@0
    .prologue
    .line 3917
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mPackageStats:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;

    #@8
    .line 3918
    .local v0, ps:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;
    if-nez v0, :cond_14

    #@a
    .line 3919
    new-instance v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;

    #@c
    .end local v0           #ps:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;
    invoke-direct {v0, p0}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;)V

    #@f
    .line 3920
    .restart local v0       #ps:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mPackageStats:Ljava/util/HashMap;

    #@11
    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@14
    .line 3923
    :cond_14
    return-object v0
.end method

.method public getPidStats()Landroid/util/SparseArray;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<+",
            "Landroid/os/BatteryStats$Uid$Pid;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 3900
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mPids:Landroid/util/SparseArray;

    #@2
    return-object v0
.end method

.method public getPidStatsLocked(I)Landroid/os/BatteryStats$Uid$Pid;
    .registers 4
    .parameter "pid"

    #@0
    .prologue
    .line 3904
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mPids:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/os/BatteryStats$Uid$Pid;

    #@8
    .line 3905
    .local v0, p:Landroid/os/BatteryStats$Uid$Pid;
    if-nez v0, :cond_14

    #@a
    .line 3906
    new-instance v0, Landroid/os/BatteryStats$Uid$Pid;

    #@c
    .end local v0           #p:Landroid/os/BatteryStats$Uid$Pid;
    invoke-direct {v0, p0}, Landroid/os/BatteryStats$Uid$Pid;-><init>(Landroid/os/BatteryStats$Uid;)V

    #@f
    .line 3907
    .restart local v0       #p:Landroid/os/BatteryStats$Uid$Pid;
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mPids:Landroid/util/SparseArray;

    #@11
    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@14
    .line 3909
    :cond_14
    return-object v0
.end method

.method public getProcessStats()Ljava/util/Map;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "+",
            "Landroid/os/BatteryStats$Uid$Proc;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 2460
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mProcessStats:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method public getProcessStatsLocked(Ljava/lang/String;)Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;
    .registers 4
    .parameter "name"

    #@0
    .prologue
    .line 3890
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mProcessStats:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;

    #@8
    .line 3891
    .local v0, ps:Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;
    if-nez v0, :cond_14

    #@a
    .line 3892
    new-instance v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;

    #@c
    .end local v0           #ps:Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;
    invoke-direct {v0, p0}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;)V

    #@f
    .line 3893
    .restart local v0       #ps:Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mProcessStats:Ljava/util/HashMap;

    #@11
    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@14
    .line 3896
    :cond_14
    return-object v0
.end method

.method public getSensorStats()Ljava/util/Map;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "+",
            "Landroid/os/BatteryStats$Uid$Sensor;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 2455
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mSensorStats:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method public getSensorTimerLocked(IZ)Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    .registers 8
    .parameter "sensor"
    .parameter "create"

    #@0
    .prologue
    .line 3987
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mSensorStats:Ljava/util/HashMap;

    #@2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v4

    #@6
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;

    #@c
    .line 3988
    .local v0, se:Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;
    if-nez v0, :cond_20

    #@e
    .line 3989
    if-nez p2, :cond_12

    #@10
    .line 3990
    const/4 v1, 0x0

    #@11
    .line 4006
    :cond_11
    :goto_11
    return-object v1

    #@12
    .line 3992
    :cond_12
    new-instance v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;

    #@14
    .end local v0           #se:Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;
    invoke-direct {v0, p0, p1}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;I)V

    #@17
    .line 3993
    .restart local v0       #se:Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mSensorStats:Ljava/util/HashMap;

    #@19
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1c
    move-result-object v4

    #@1d
    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@20
    .line 3995
    :cond_20
    iget-object v1, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;->mTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@22
    .line 3996
    .local v1, t:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    if-nez v1, :cond_11

    #@24
    .line 3999
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@26
    iget-object v3, v3, Lcom/android/internal/os/BatteryStatsImpl;->mSensorTimers:Landroid/util/SparseArray;

    #@28
    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@2b
    move-result-object v2

    #@2c
    check-cast v2, Ljava/util/ArrayList;

    #@2e
    .line 4000
    .local v2, timers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;>;"
    if-nez v2, :cond_3c

    #@30
    .line 4001
    new-instance v2, Ljava/util/ArrayList;

    #@32
    .end local v2           #timers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;>;"
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@35
    .line 4002
    .restart local v2       #timers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;>;"
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@37
    iget-object v3, v3, Lcom/android/internal/os/BatteryStatsImpl;->mSensorTimers:Landroid/util/SparseArray;

    #@39
    invoke-virtual {v3, p1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@3c
    .line 4004
    :cond_3c
    new-instance v1, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@3e
    .end local v1           #t:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    const/4 v3, 0x3

    #@3f
    iget-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@41
    iget-object v4, v4, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@43
    invoke-direct {v1, p0, v3, v2, v4}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    #@46
    .line 4005
    .restart local v1       #t:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    iput-object v1, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;->mTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@48
    goto :goto_11
.end method

.method public getServiceStatsLocked(Ljava/lang/String;Ljava/lang/String;)Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;
    .registers 6
    .parameter "pkg"
    .parameter "serv"

    #@0
    .prologue
    .line 3931
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->getPackageStatsLocked(Ljava/lang/String;)Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;

    #@3
    move-result-object v0

    #@4
    .line 3932
    .local v0, ps:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;
    iget-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;->mServiceStats:Ljava/util/HashMap;

    #@6
    invoke-virtual {v2, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;

    #@c
    .line 3933
    .local v1, ss:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;
    if-nez v1, :cond_17

    #@e
    .line 3934
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;->newServiceStatsLocked()Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;

    #@11
    move-result-object v1

    #@12
    .line 3935
    iget-object v2, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;->mServiceStats:Ljava/util/HashMap;

    #@14
    invoke-virtual {v2, p2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@17
    .line 3938
    :cond_17
    return-object v1
.end method

.method public getTcpBytesReceived(I)J
    .registers 6
    .parameter "which"

    #@0
    .prologue
    .line 2475
    const/4 v2, 0x1

    #@1
    if-ne p1, v2, :cond_6

    #@3
    .line 2476
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mLoadedTcpBytesReceived:J

    #@5
    .line 2484
    :cond_5
    :goto_5
    return-wide v0

    #@6
    .line 2478
    :cond_6
    invoke-virtual {p0}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->computeCurrentTcpBytesReceived()J

    #@9
    move-result-wide v0

    #@a
    .line 2479
    .local v0, current:J
    const/4 v2, 0x3

    #@b
    if-ne p1, v2, :cond_11

    #@d
    .line 2480
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mTcpBytesReceivedAtLastUnplug:J

    #@f
    sub-long/2addr v0, v2

    #@10
    goto :goto_5

    #@11
    .line 2481
    :cond_11
    if-nez p1, :cond_5

    #@13
    .line 2482
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mLoadedTcpBytesReceived:J

    #@15
    add-long/2addr v0, v2

    #@16
    goto :goto_5
.end method

.method public getTcpBytesSent(I)J
    .registers 6
    .parameter "which"

    #@0
    .prologue
    .line 2497
    const/4 v2, 0x1

    #@1
    if-ne p1, v2, :cond_6

    #@3
    .line 2498
    iget-wide v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mLoadedTcpBytesSent:J

    #@5
    .line 2506
    :cond_5
    :goto_5
    return-wide v0

    #@6
    .line 2500
    :cond_6
    invoke-virtual {p0}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->computeCurrentTcpBytesSent()J

    #@9
    move-result-wide v0

    #@a
    .line 2501
    .local v0, current:J
    const/4 v2, 0x3

    #@b
    if-ne p1, v2, :cond_11

    #@d
    .line 2502
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mTcpBytesSentAtLastUnplug:J

    #@f
    sub-long/2addr v0, v2

    #@10
    goto :goto_5

    #@11
    .line 2503
    :cond_11
    if-nez p1, :cond_5

    #@13
    .line 2504
    iget-wide v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mLoadedTcpBytesSent:J

    #@15
    add-long/2addr v0, v2

    #@16
    goto :goto_5
.end method

.method public getUid()I
    .registers 2

    #@0
    .prologue
    .line 2470
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mUid:I

    #@2
    return v0
.end method

.method public getUserActivityCount(II)I
    .registers 4
    .parameter "type"
    .parameter "which"

    #@0
    .prologue
    .line 2699
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mUserActivityCounters:[Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 2700
    const/4 v0, 0x0

    #@5
    .line 2702
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mUserActivityCounters:[Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@8
    aget-object v0, v0, p1

    #@a
    invoke-virtual {v0, p2}, Lcom/android/internal/os/BatteryStatsImpl$Counter;->getCountLocked(I)I

    #@d
    move-result v0

    #@e
    goto :goto_5
.end method

.method public getVideoTurnedOnTime(JI)J
    .registers 6
    .parameter "batteryRealtime"
    .parameter "which"

    #@0
    .prologue
    .line 2673
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mVideoTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2
    if-nez v0, :cond_7

    #@4
    .line 2674
    const-wide/16 v0, 0x0

    #@6
    .line 2676
    :goto_6
    return-wide v0

    #@7
    :cond_7
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mVideoTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@9
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->getTotalTimeLocked(JI)J

    #@c
    move-result-wide v0

    #@d
    goto :goto_6
.end method

.method public getWakeTimerLocked(Ljava/lang/String;I)Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    .registers 10
    .parameter "name"
    .parameter "type"

    #@0
    .prologue
    .line 3942
    iget-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWakelockStats:Ljava/util/HashMap;

    #@2
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v3

    #@6
    check-cast v3, Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;

    #@8
    .line 3943
    .local v3, wl:Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;
    if-nez v3, :cond_34

    #@a
    .line 3944
    iget-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWakelockStats:Ljava/util/HashMap;

    #@c
    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    #@f
    move-result v0

    #@10
    .line 3945
    .local v0, N:I
    const/16 v4, 0x1e

    #@12
    if-le v0, v4, :cond_28

    #@14
    iget v4, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mUid:I

    #@16
    const/16 v5, 0x3e8

    #@18
    if-ne v4, v5, :cond_1e

    #@1a
    const/16 v4, 0x32

    #@1c
    if-le v0, v4, :cond_28

    #@1e
    .line 3947
    :cond_1e
    const-string p1, "*overflow*"

    #@20
    .line 3948
    iget-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWakelockStats:Ljava/util/HashMap;

    #@22
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@25
    move-result-object v3

    #@26
    .end local v3           #wl:Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;
    check-cast v3, Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;

    #@28
    .line 3950
    .restart local v3       #wl:Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;
    :cond_28
    if-nez v3, :cond_34

    #@2a
    .line 3951
    new-instance v3, Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;

    #@2c
    .end local v3           #wl:Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;
    invoke-direct {v3, p0}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;)V

    #@2f
    .line 3952
    .restart local v3       #wl:Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;
    iget-object v4, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWakelockStats:Ljava/util/HashMap;

    #@31
    invoke-virtual {v4, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@34
    .line 3955
    .end local v0           #N:I
    :cond_34
    const/4 v1, 0x0

    #@35
    .line 3956
    .local v1, t:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    packed-switch p2, :pswitch_data_94

    #@38
    .line 3982
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@3a
    new-instance v5, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string/jumbo v6, "type="

    #@42
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v5

    #@46
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@49
    move-result-object v5

    #@4a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v5

    #@4e
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@51
    throw v4

    #@52
    .line 3958
    :pswitch_52
    iget-object v1, v3, Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;->mTimerPartial:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@54
    .line 3959
    if-nez v1, :cond_66

    #@56
    .line 3960
    new-instance v1, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@58
    .end local v1           #t:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    const/4 v4, 0x0

    #@59
    iget-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@5b
    iget-object v5, v5, Lcom/android/internal/os/BatteryStatsImpl;->mPartialTimers:Ljava/util/ArrayList;

    #@5d
    iget-object v6, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@5f
    iget-object v6, v6, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@61
    invoke-direct {v1, p0, v4, v5, v6}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    #@64
    .line 3962
    .restart local v1       #t:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    iput-object v1, v3, Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;->mTimerPartial:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@66
    :cond_66
    move-object v2, v1

    #@67
    .line 3980
    .end local v1           #t:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    .local v2, t:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    :goto_67
    return-object v2

    #@68
    .line 3966
    .end local v2           #t:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    .restart local v1       #t:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    :pswitch_68
    iget-object v1, v3, Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;->mTimerFull:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@6a
    .line 3967
    if-nez v1, :cond_7c

    #@6c
    .line 3968
    new-instance v1, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@6e
    .end local v1           #t:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    const/4 v4, 0x1

    #@6f
    iget-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@71
    iget-object v5, v5, Lcom/android/internal/os/BatteryStatsImpl;->mFullTimers:Ljava/util/ArrayList;

    #@73
    iget-object v6, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@75
    iget-object v6, v6, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@77
    invoke-direct {v1, p0, v4, v5, v6}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    #@7a
    .line 3970
    .restart local v1       #t:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    iput-object v1, v3, Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;->mTimerFull:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@7c
    :cond_7c
    move-object v2, v1

    #@7d
    .line 3972
    .end local v1           #t:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    .restart local v2       #t:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    goto :goto_67

    #@7e
    .line 3974
    .end local v2           #t:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    .restart local v1       #t:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    :pswitch_7e
    iget-object v1, v3, Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;->mTimerWindow:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@80
    .line 3975
    if-nez v1, :cond_92

    #@82
    .line 3976
    new-instance v1, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@84
    .end local v1           #t:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    const/4 v4, 0x2

    #@85
    iget-object v5, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@87
    iget-object v5, v5, Lcom/android/internal/os/BatteryStatsImpl;->mWindowTimers:Ljava/util/ArrayList;

    #@89
    iget-object v6, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@8b
    iget-object v6, v6, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@8d
    invoke-direct {v1, p0, v4, v5, v6}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    #@90
    .line 3978
    .restart local v1       #t:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    iput-object v1, v3, Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;->mTimerWindow:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@92
    :cond_92
    move-object v2, v1

    #@93
    .line 3980
    .end local v1           #t:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    .restart local v2       #t:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    goto :goto_67

    #@94
    .line 3956
    :pswitch_data_94
    .packed-switch 0x0
        :pswitch_52
        :pswitch_68
        :pswitch_7e
    .end packed-switch
.end method

.method public getWakelockStats()Ljava/util/Map;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "+",
            "Landroid/os/BatteryStats$Uid$Wakelock;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 2450
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWakelockStats:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method public getWifiMulticastTime(JI)J
    .registers 6
    .parameter "batteryRealtime"
    .parameter "which"

    #@0
    .prologue
    .line 2656
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiMulticastTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2
    if-nez v0, :cond_7

    #@4
    .line 2657
    const-wide/16 v0, 0x0

    #@6
    .line 2659
    :goto_6
    return-wide v0

    #@7
    :cond_7
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiMulticastTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@9
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->getTotalTimeLocked(JI)J

    #@c
    move-result-wide v0

    #@d
    goto :goto_6
.end method

.method public getWifiRunningTime(JI)J
    .registers 6
    .parameter "batteryRealtime"
    .parameter "which"

    #@0
    .prologue
    .line 2632
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiRunningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2
    if-nez v0, :cond_7

    #@4
    .line 2633
    const-wide/16 v0, 0x0

    #@6
    .line 2635
    :goto_6
    return-wide v0

    #@7
    :cond_7
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiRunningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@9
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->getTotalTimeLocked(JI)J

    #@c
    move-result-wide v0

    #@d
    goto :goto_6
.end method

.method public getWifiScanTime(JI)J
    .registers 6
    .parameter "batteryRealtime"
    .parameter "which"

    #@0
    .prologue
    .line 2648
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiScanTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2
    if-nez v0, :cond_7

    #@4
    .line 2649
    const-wide/16 v0, 0x0

    #@6
    .line 2651
    :goto_6
    return-wide v0

    #@7
    :cond_7
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiScanTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@9
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->getTotalTimeLocked(JI)J

    #@c
    move-result-wide v0

    #@d
    goto :goto_6
.end method

.method public hasUserActivity()Z
    .registers 2

    #@0
    .prologue
    .line 2694
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mUserActivityCounters:[Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method initUserActivityLocked()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x3

    #@1
    .line 2706
    new-array v1, v4, [Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@3
    iput-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mUserActivityCounters:[Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@5
    .line 2707
    const/4 v0, 0x0

    #@6
    .local v0, i:I
    :goto_6
    if-ge v0, v4, :cond_18

    #@8
    .line 2708
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mUserActivityCounters:[Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@a
    new-instance v2, Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@c
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@e
    iget-object v3, v3, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@10
    invoke-direct {v2, v3}, Lcom/android/internal/os/BatteryStatsImpl$Counter;-><init>(Ljava/util/ArrayList;)V

    #@13
    aput-object v2, v1, v0

    #@15
    .line 2707
    add-int/lit8 v0, v0, 0x1

    #@17
    goto :goto_6

    #@18
    .line 2710
    :cond_18
    return-void
.end method

.method public noteAudioTurnedOffLocked()V
    .registers 3

    #@0
    .prologue
    .line 2604
    iget-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mAudioTurnedOn:Z

    #@2
    if-eqz v0, :cond_e

    #@4
    .line 2605
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mAudioTurnedOn:Z

    #@7
    .line 2606
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mAudioTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@9
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@b
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->stopRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@e
    .line 2608
    :cond_e
    return-void
.end method

.method public noteAudioTurnedOnLocked()V
    .registers 5

    #@0
    .prologue
    .line 2592
    iget-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mAudioTurnedOn:Z

    #@2
    if-nez v0, :cond_1f

    #@4
    .line 2593
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mAudioTurnedOn:Z

    #@7
    .line 2594
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mAudioTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@9
    if-nez v0, :cond_18

    #@b
    .line 2595
    new-instance v0, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@d
    const/4 v1, 0x7

    #@e
    const/4 v2, 0x0

    #@f
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@11
    iget-object v3, v3, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@13
    invoke-direct {v0, p0, v1, v2, v3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    #@16
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mAudioTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@18
    .line 2598
    :cond_18
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mAudioTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@1a
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@1c
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@1f
    .line 2600
    :cond_1f
    return-void
.end method

.method public noteFullWifiLockAcquiredLocked()V
    .registers 5

    #@0
    .prologue
    .line 2532
    iget-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mFullWifiLockOut:Z

    #@2
    if-nez v0, :cond_22

    #@4
    .line 2533
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mFullWifiLockOut:Z

    #@7
    .line 2534
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mFullWifiLockTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@9
    if-nez v0, :cond_1b

    #@b
    .line 2535
    new-instance v0, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@d
    const/4 v1, 0x5

    #@e
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@10
    iget-object v2, v2, Lcom/android/internal/os/BatteryStatsImpl;->mFullWifiLockTimers:Ljava/util/ArrayList;

    #@12
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@14
    iget-object v3, v3, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@16
    invoke-direct {v0, p0, v1, v2, v3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    #@19
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mFullWifiLockTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@1b
    .line 2538
    :cond_1b
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mFullWifiLockTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@1d
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@1f
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@22
    .line 2540
    :cond_22
    return-void
.end method

.method public noteFullWifiLockReleasedLocked()V
    .registers 3

    #@0
    .prologue
    .line 2544
    iget-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mFullWifiLockOut:Z

    #@2
    if-eqz v0, :cond_e

    #@4
    .line 2545
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mFullWifiLockOut:Z

    #@7
    .line 2546
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mFullWifiLockTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@9
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@b
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->stopRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@e
    .line 2548
    :cond_e
    return-void
.end method

.method public noteStartGps()V
    .registers 4

    #@0
    .prologue
    .line 4066
    const/16 v1, -0x2710

    #@2
    const/4 v2, 0x1

    #@3
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->getSensorTimerLocked(IZ)Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@6
    move-result-object v0

    #@7
    .line 4067
    .local v0, t:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    if-eqz v0, :cond_e

    #@9
    .line 4068
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@b
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@e
    .line 4070
    :cond_e
    return-void
.end method

.method public noteStartSensor(I)V
    .registers 4
    .parameter "sensor"

    #@0
    .prologue
    .line 4051
    const/4 v1, 0x1

    #@1
    invoke-virtual {p0, p1, v1}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->getSensorTimerLocked(IZ)Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@4
    move-result-object v0

    #@5
    .line 4052
    .local v0, t:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    if-eqz v0, :cond_c

    #@7
    .line 4053
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@9
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@c
    .line 4055
    :cond_c
    return-void
.end method

.method public noteStartWakeLocked(ILjava/lang/String;I)V
    .registers 10
    .parameter "pid"
    .parameter "name"
    .parameter "type"

    #@0
    .prologue
    .line 4010
    invoke-virtual {p0, p2, p3}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->getWakeTimerLocked(Ljava/lang/String;I)Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@3
    move-result-object v1

    #@4
    .line 4011
    .local v1, t:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    if-eqz v1, :cond_b

    #@6
    .line 4012
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@8
    invoke-virtual {v1, v2}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@b
    .line 4014
    :cond_b
    if-ltz p1, :cond_21

    #@d
    if-nez p3, :cond_21

    #@f
    .line 4015
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->getPidStatsLocked(I)Landroid/os/BatteryStats$Uid$Pid;

    #@12
    move-result-object v0

    #@13
    .line 4016
    .local v0, p:Landroid/os/BatteryStats$Uid$Pid;
    iget-wide v2, v0, Landroid/os/BatteryStats$Uid$Pid;->mWakeStart:J

    #@15
    const-wide/16 v4, 0x0

    #@17
    cmp-long v2, v2, v4

    #@19
    if-nez v2, :cond_21

    #@1b
    .line 4017
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@1e
    move-result-wide v2

    #@1f
    iput-wide v2, v0, Landroid/os/BatteryStats$Uid$Pid;->mWakeStart:J

    #@21
    .line 4020
    .end local v0           #p:Landroid/os/BatteryStats$Uid$Pid;
    :cond_21
    return-void
.end method

.method public noteStopGps()V
    .registers 4

    #@0
    .prologue
    .line 4073
    const/16 v1, -0x2710

    #@2
    const/4 v2, 0x0

    #@3
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->getSensorTimerLocked(IZ)Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@6
    move-result-object v0

    #@7
    .line 4074
    .local v0, t:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    if-eqz v0, :cond_e

    #@9
    .line 4075
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@b
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->stopRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@e
    .line 4077
    :cond_e
    return-void
.end method

.method public noteStopSensor(I)V
    .registers 4
    .parameter "sensor"

    #@0
    .prologue
    .line 4059
    const/4 v1, 0x0

    #@1
    invoke-virtual {p0, p1, v1}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->getSensorTimerLocked(IZ)Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@4
    move-result-object v0

    #@5
    .line 4060
    .local v0, t:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    if-eqz v0, :cond_c

    #@7
    .line 4061
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@9
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->stopRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@c
    .line 4063
    :cond_c
    return-void
.end method

.method public noteStopWakeLocked(ILjava/lang/String;I)V
    .registers 14
    .parameter "pid"
    .parameter "name"
    .parameter "type"

    #@0
    .prologue
    const-wide/16 v8, 0x0

    #@2
    .line 4023
    invoke-virtual {p0, p2, p3}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->getWakeTimerLocked(Ljava/lang/String;I)Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@5
    move-result-object v1

    #@6
    .line 4024
    .local v1, t:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    if-eqz v1, :cond_d

    #@8
    .line 4025
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@a
    invoke-virtual {v1, v2}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->stopRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@d
    .line 4027
    :cond_d
    if-ltz p1, :cond_2f

    #@f
    if-nez p3, :cond_2f

    #@11
    .line 4028
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mPids:Landroid/util/SparseArray;

    #@13
    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@16
    move-result-object v0

    #@17
    check-cast v0, Landroid/os/BatteryStats$Uid$Pid;

    #@19
    .line 4029
    .local v0, p:Landroid/os/BatteryStats$Uid$Pid;
    if-eqz v0, :cond_2f

    #@1b
    iget-wide v2, v0, Landroid/os/BatteryStats$Uid$Pid;->mWakeStart:J

    #@1d
    cmp-long v2, v2, v8

    #@1f
    if-eqz v2, :cond_2f

    #@21
    .line 4030
    iget-wide v2, v0, Landroid/os/BatteryStats$Uid$Pid;->mWakeSum:J

    #@23
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@26
    move-result-wide v4

    #@27
    iget-wide v6, v0, Landroid/os/BatteryStats$Uid$Pid;->mWakeStart:J

    #@29
    sub-long/2addr v4, v6

    #@2a
    add-long/2addr v2, v4

    #@2b
    iput-wide v2, v0, Landroid/os/BatteryStats$Uid$Pid;->mWakeSum:J

    #@2d
    .line 4031
    iput-wide v8, v0, Landroid/os/BatteryStats$Uid$Pid;->mWakeStart:J

    #@2f
    .line 4034
    .end local v0           #p:Landroid/os/BatteryStats$Uid$Pid;
    :cond_2f
    return-void
.end method

.method public noteUserActivityLocked(I)V
    .registers 5
    .parameter "type"

    #@0
    .prologue
    .line 2681
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mUserActivityCounters:[Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@2
    if-nez v0, :cond_7

    #@4
    .line 2682
    invoke-virtual {p0}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->initUserActivityLocked()V

    #@7
    .line 2684
    :cond_7
    if-ltz p1, :cond_14

    #@9
    const/4 v0, 0x3

    #@a
    if-ge p1, v0, :cond_14

    #@c
    .line 2685
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mUserActivityCounters:[Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@e
    aget-object v0, v0, p1

    #@10
    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsImpl$Counter;->stepAtomic()V

    #@13
    .line 2690
    :goto_13
    return-void

    #@14
    .line 2687
    :cond_14
    const-string v0, "BatteryStatsImpl"

    #@16
    new-instance v1, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v2, "Unknown user activity type "

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    const-string v2, " was specified."

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    new-instance v2, Ljava/lang/Throwable;

    #@31
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@34
    invoke-static {v0, v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@37
    goto :goto_13
.end method

.method public noteVideoTurnedOffLocked()V
    .registers 3

    #@0
    .prologue
    .line 2624
    iget-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mVideoTurnedOn:Z

    #@2
    if-eqz v0, :cond_e

    #@4
    .line 2625
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mVideoTurnedOn:Z

    #@7
    .line 2626
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mVideoTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@9
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@b
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->stopRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@e
    .line 2628
    :cond_e
    return-void
.end method

.method public noteVideoTurnedOnLocked()V
    .registers 5

    #@0
    .prologue
    .line 2612
    iget-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mVideoTurnedOn:Z

    #@2
    if-nez v0, :cond_20

    #@4
    .line 2613
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mVideoTurnedOn:Z

    #@7
    .line 2614
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mVideoTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@9
    if-nez v0, :cond_19

    #@b
    .line 2615
    new-instance v0, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@d
    const/16 v1, 0x8

    #@f
    const/4 v2, 0x0

    #@10
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@12
    iget-object v3, v3, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@14
    invoke-direct {v0, p0, v1, v2, v3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    #@17
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mVideoTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@19
    .line 2618
    :cond_19
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mVideoTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@1b
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@1d
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@20
    .line 2620
    :cond_20
    return-void
.end method

.method public noteWifiMulticastDisabledLocked()V
    .registers 3

    #@0
    .prologue
    .line 2584
    iget-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiMulticastEnabled:Z

    #@2
    if-eqz v0, :cond_e

    #@4
    .line 2585
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiMulticastEnabled:Z

    #@7
    .line 2586
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiMulticastTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@9
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@b
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->stopRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@e
    .line 2588
    :cond_e
    return-void
.end method

.method public noteWifiMulticastEnabledLocked()V
    .registers 5

    #@0
    .prologue
    .line 2572
    iget-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiMulticastEnabled:Z

    #@2
    if-nez v0, :cond_22

    #@4
    .line 2573
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiMulticastEnabled:Z

    #@7
    .line 2574
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiMulticastTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@9
    if-nez v0, :cond_1b

    #@b
    .line 2575
    new-instance v0, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@d
    const/4 v1, 0x7

    #@e
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@10
    iget-object v2, v2, Lcom/android/internal/os/BatteryStatsImpl;->mWifiMulticastTimers:Ljava/util/ArrayList;

    #@12
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@14
    iget-object v3, v3, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@16
    invoke-direct {v0, p0, v1, v2, v3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    #@19
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiMulticastTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@1b
    .line 2578
    :cond_1b
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiMulticastTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@1d
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@1f
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@22
    .line 2580
    :cond_22
    return-void
.end method

.method public noteWifiRunningLocked()V
    .registers 5

    #@0
    .prologue
    .line 2512
    iget-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiRunning:Z

    #@2
    if-nez v0, :cond_22

    #@4
    .line 2513
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiRunning:Z

    #@7
    .line 2514
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiRunningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@9
    if-nez v0, :cond_1b

    #@b
    .line 2515
    new-instance v0, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@d
    const/4 v1, 0x4

    #@e
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@10
    iget-object v2, v2, Lcom/android/internal/os/BatteryStatsImpl;->mWifiRunningTimers:Ljava/util/ArrayList;

    #@12
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@14
    iget-object v3, v3, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@16
    invoke-direct {v0, p0, v1, v2, v3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    #@19
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiRunningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@1b
    .line 2518
    :cond_1b
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiRunningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@1d
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@1f
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@22
    .line 2520
    :cond_22
    return-void
.end method

.method public noteWifiScanStartedLocked()V
    .registers 5

    #@0
    .prologue
    .line 2552
    iget-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiScanStarted:Z

    #@2
    if-nez v0, :cond_22

    #@4
    .line 2553
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiScanStarted:Z

    #@7
    .line 2554
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiScanTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@9
    if-nez v0, :cond_1b

    #@b
    .line 2555
    new-instance v0, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@d
    const/4 v1, 0x6

    #@e
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@10
    iget-object v2, v2, Lcom/android/internal/os/BatteryStatsImpl;->mWifiScanTimers:Ljava/util/ArrayList;

    #@12
    iget-object v3, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@14
    iget-object v3, v3, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@16
    invoke-direct {v0, p0, v1, v2, v3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    #@19
    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiScanTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@1b
    .line 2558
    :cond_1b
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiScanTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@1d
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@1f
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@22
    .line 2560
    :cond_22
    return-void
.end method

.method public noteWifiScanStoppedLocked()V
    .registers 3

    #@0
    .prologue
    .line 2564
    iget-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiScanStarted:Z

    #@2
    if-eqz v0, :cond_e

    #@4
    .line 2565
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiScanStarted:Z

    #@7
    .line 2566
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiScanTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@9
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@b
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->stopRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@e
    .line 2568
    :cond_e
    return-void
.end method

.method public noteWifiStoppedLocked()V
    .registers 3

    #@0
    .prologue
    .line 2524
    iget-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiRunning:Z

    #@2
    if-eqz v0, :cond_e

    #@4
    .line 2525
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiRunning:Z

    #@7
    .line 2526
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiRunningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@9
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@b
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->stopRunningLocked(Lcom/android/internal/os/BatteryStatsImpl;)V

    #@e
    .line 2528
    :cond_e
    return-void
.end method

.method readFromParcelLocked(Ljava/util/ArrayList;Landroid/os/Parcel;)V
    .registers 28
    .parameter
    .parameter "in"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/os/BatteryStatsImpl$Unpluggable;",
            ">;",
            "Landroid/os/Parcel;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 2931
    .local p1, unpluggables:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/os/BatteryStatsImpl$Unpluggable;>;"
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v16

    #@4
    .line 2932
    .local v16, numWakelocks:I
    move-object/from16 v0, p0

    #@6
    iget-object v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWakelockStats:Ljava/util/HashMap;

    #@8
    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    #@b
    .line 2933
    const/4 v10, 0x0

    #@c
    .local v10, j:I
    :goto_c
    move/from16 v0, v16

    #@e
    if-ge v10, v0, :cond_34

    #@10
    .line 2934
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@13
    move-result-object v24

    #@14
    .line 2935
    .local v24, wakelockName:Ljava/lang/String;
    new-instance v23, Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;

    #@16
    move-object/from16 v0, v23

    #@18
    move-object/from16 v1, p0

    #@1a
    invoke-direct {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;)V

    #@1d
    .line 2936
    .local v23, wakelock:Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;
    move-object/from16 v0, v23

    #@1f
    move-object/from16 v1, p1

    #@21
    move-object/from16 v2, p2

    #@23
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;->readFromParcelLocked(Ljava/util/ArrayList;Landroid/os/Parcel;)V

    #@26
    .line 2940
    move-object/from16 v0, p0

    #@28
    iget-object v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWakelockStats:Ljava/util/HashMap;

    #@2a
    move-object/from16 v0, v24

    #@2c
    move-object/from16 v1, v23

    #@2e
    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@31
    .line 2933
    add-int/lit8 v10, v10, 0x1

    #@33
    goto :goto_c

    #@34
    .line 2943
    .end local v23           #wakelock:Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;
    .end local v24           #wakelockName:Ljava/lang/String;
    :cond_34
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@37
    move-result v15

    #@38
    .line 2944
    .local v15, numSensors:I
    move-object/from16 v0, p0

    #@3a
    iget-object v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mSensorStats:Ljava/util/HashMap;

    #@3c
    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    #@3f
    .line 2945
    const/4 v11, 0x0

    #@40
    .local v11, k:I
    :goto_40
    if-ge v11, v15, :cond_6e

    #@42
    .line 2946
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@45
    move-result v22

    #@46
    .line 2947
    .local v22, sensorNumber:I
    new-instance v21, Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;

    #@48
    move-object/from16 v0, v21

    #@4a
    move-object/from16 v1, p0

    #@4c
    move/from16 v2, v22

    #@4e
    invoke-direct {v0, v1, v2}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;I)V

    #@51
    .line 2948
    .local v21, sensor:Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;
    move-object/from16 v0, p0

    #@53
    iget-object v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@55
    iget-object v3, v3, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@57
    move-object/from16 v0, v21

    #@59
    move-object/from16 v1, p2

    #@5b
    invoke-virtual {v0, v3, v1}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;->readFromParcelLocked(Ljava/util/ArrayList;Landroid/os/Parcel;)V

    #@5e
    .line 2949
    move-object/from16 v0, p0

    #@60
    iget-object v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mSensorStats:Ljava/util/HashMap;

    #@62
    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@65
    move-result-object v4

    #@66
    move-object/from16 v0, v21

    #@68
    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6b
    .line 2945
    add-int/lit8 v11, v11, 0x1

    #@6d
    goto :goto_40

    #@6e
    .line 2952
    .end local v21           #sensor:Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;
    .end local v22           #sensorNumber:I
    :cond_6e
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@71
    move-result v14

    #@72
    .line 2953
    .local v14, numProcs:I
    move-object/from16 v0, p0

    #@74
    iget-object v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mProcessStats:Ljava/util/HashMap;

    #@76
    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    #@79
    .line 2954
    const/4 v11, 0x0

    #@7a
    :goto_7a
    if-ge v11, v14, :cond_9e

    #@7c
    .line 2955
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7f
    move-result-object v20

    #@80
    .line 2956
    .local v20, processName:Ljava/lang/String;
    new-instance v19, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;

    #@82
    move-object/from16 v0, v19

    #@84
    move-object/from16 v1, p0

    #@86
    invoke-direct {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;)V

    #@89
    .line 2957
    .local v19, proc:Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;
    move-object/from16 v0, v19

    #@8b
    move-object/from16 v1, p2

    #@8d
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->readFromParcelLocked(Landroid/os/Parcel;)V

    #@90
    .line 2958
    move-object/from16 v0, p0

    #@92
    iget-object v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mProcessStats:Ljava/util/HashMap;

    #@94
    move-object/from16 v0, v20

    #@96
    move-object/from16 v1, v19

    #@98
    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@9b
    .line 2954
    add-int/lit8 v11, v11, 0x1

    #@9d
    goto :goto_7a

    #@9e
    .line 2961
    .end local v19           #proc:Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;
    .end local v20           #processName:Ljava/lang/String;
    :cond_9e
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@a1
    move-result v13

    #@a2
    .line 2962
    .local v13, numPkgs:I
    move-object/from16 v0, p0

    #@a4
    iget-object v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mPackageStats:Ljava/util/HashMap;

    #@a6
    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    #@a9
    .line 2963
    const/4 v12, 0x0

    #@aa
    .local v12, l:I
    :goto_aa
    if-ge v12, v13, :cond_ce

    #@ac
    .line 2964
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@af
    move-result-object v17

    #@b0
    .line 2965
    .local v17, packageName:Ljava/lang/String;
    new-instance v18, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;

    #@b2
    move-object/from16 v0, v18

    #@b4
    move-object/from16 v1, p0

    #@b6
    invoke-direct {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;)V

    #@b9
    .line 2966
    .local v18, pkg:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;
    move-object/from16 v0, v18

    #@bb
    move-object/from16 v1, p2

    #@bd
    invoke-virtual {v0, v1}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;->readFromParcelLocked(Landroid/os/Parcel;)V

    #@c0
    .line 2967
    move-object/from16 v0, p0

    #@c2
    iget-object v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mPackageStats:Ljava/util/HashMap;

    #@c4
    move-object/from16 v0, v17

    #@c6
    move-object/from16 v1, v18

    #@c8
    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@cb
    .line 2963
    add-int/lit8 v12, v12, 0x1

    #@cd
    goto :goto_aa

    #@ce
    .line 2970
    .end local v17           #packageName:Ljava/lang/String;
    .end local v18           #pkg:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;
    :cond_ce
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@d1
    move-result-wide v3

    #@d2
    move-object/from16 v0, p0

    #@d4
    iput-wide v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mLoadedTcpBytesReceived:J

    #@d6
    .line 2971
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@d9
    move-result-wide v3

    #@da
    move-object/from16 v0, p0

    #@dc
    iput-wide v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mLoadedTcpBytesSent:J

    #@de
    .line 2972
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@e1
    move-result-wide v3

    #@e2
    move-object/from16 v0, p0

    #@e4
    iput-wide v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mCurrentTcpBytesReceived:J

    #@e6
    .line 2973
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@e9
    move-result-wide v3

    #@ea
    move-object/from16 v0, p0

    #@ec
    iput-wide v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mCurrentTcpBytesSent:J

    #@ee
    .line 2974
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@f1
    move-result-wide v3

    #@f2
    move-object/from16 v0, p0

    #@f4
    iput-wide v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mTcpBytesReceivedAtLastUnplug:J

    #@f6
    .line 2975
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@f9
    move-result-wide v3

    #@fa
    move-object/from16 v0, p0

    #@fc
    iput-wide v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mTcpBytesSentAtLastUnplug:J

    #@fe
    .line 2976
    const/4 v3, 0x0

    #@ff
    move-object/from16 v0, p0

    #@101
    iput-boolean v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiRunning:Z

    #@103
    .line 2977
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@106
    move-result v3

    #@107
    if-eqz v3, :cond_1fa

    #@109
    .line 2978
    new-instance v3, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@10b
    const/4 v5, 0x4

    #@10c
    move-object/from16 v0, p0

    #@10e
    iget-object v4, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@110
    iget-object v6, v4, Lcom/android/internal/os/BatteryStatsImpl;->mWifiRunningTimers:Ljava/util/ArrayList;

    #@112
    move-object/from16 v0, p0

    #@114
    iget-object v4, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@116
    iget-object v7, v4, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@118
    move-object/from16 v4, p0

    #@11a
    move-object/from16 v8, p2

    #@11c
    invoke-direct/range {v3 .. v8}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;Landroid/os/Parcel;)V

    #@11f
    move-object/from16 v0, p0

    #@121
    iput-object v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiRunningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@123
    .line 2983
    :goto_123
    const/4 v3, 0x0

    #@124
    move-object/from16 v0, p0

    #@126
    iput-boolean v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mFullWifiLockOut:Z

    #@128
    .line 2984
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@12b
    move-result v3

    #@12c
    if-eqz v3, :cond_201

    #@12e
    .line 2985
    new-instance v3, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@130
    const/4 v5, 0x5

    #@131
    move-object/from16 v0, p0

    #@133
    iget-object v4, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@135
    iget-object v6, v4, Lcom/android/internal/os/BatteryStatsImpl;->mFullWifiLockTimers:Ljava/util/ArrayList;

    #@137
    move-object/from16 v0, p0

    #@139
    iget-object v4, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@13b
    iget-object v7, v4, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@13d
    move-object/from16 v4, p0

    #@13f
    move-object/from16 v8, p2

    #@141
    invoke-direct/range {v3 .. v8}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;Landroid/os/Parcel;)V

    #@144
    move-object/from16 v0, p0

    #@146
    iput-object v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mFullWifiLockTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@148
    .line 2990
    :goto_148
    const/4 v3, 0x0

    #@149
    move-object/from16 v0, p0

    #@14b
    iput-boolean v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiScanStarted:Z

    #@14d
    .line 2991
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@150
    move-result v3

    #@151
    if-eqz v3, :cond_208

    #@153
    .line 2992
    new-instance v3, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@155
    const/4 v5, 0x6

    #@156
    move-object/from16 v0, p0

    #@158
    iget-object v4, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@15a
    iget-object v6, v4, Lcom/android/internal/os/BatteryStatsImpl;->mWifiScanTimers:Ljava/util/ArrayList;

    #@15c
    move-object/from16 v0, p0

    #@15e
    iget-object v4, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@160
    iget-object v7, v4, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@162
    move-object/from16 v4, p0

    #@164
    move-object/from16 v8, p2

    #@166
    invoke-direct/range {v3 .. v8}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;Landroid/os/Parcel;)V

    #@169
    move-object/from16 v0, p0

    #@16b
    iput-object v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiScanTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@16d
    .line 2997
    :goto_16d
    const/4 v3, 0x0

    #@16e
    move-object/from16 v0, p0

    #@170
    iput-boolean v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiMulticastEnabled:Z

    #@172
    .line 2998
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@175
    move-result v3

    #@176
    if-eqz v3, :cond_20f

    #@178
    .line 2999
    new-instance v3, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@17a
    const/4 v5, 0x7

    #@17b
    move-object/from16 v0, p0

    #@17d
    iget-object v4, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@17f
    iget-object v6, v4, Lcom/android/internal/os/BatteryStatsImpl;->mWifiMulticastTimers:Ljava/util/ArrayList;

    #@181
    move-object/from16 v0, p0

    #@183
    iget-object v4, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@185
    iget-object v7, v4, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@187
    move-object/from16 v4, p0

    #@189
    move-object/from16 v8, p2

    #@18b
    invoke-direct/range {v3 .. v8}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;Landroid/os/Parcel;)V

    #@18e
    move-object/from16 v0, p0

    #@190
    iput-object v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiMulticastTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@192
    .line 3004
    :goto_192
    const/4 v3, 0x0

    #@193
    move-object/from16 v0, p0

    #@195
    iput-boolean v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mAudioTurnedOn:Z

    #@197
    .line 3005
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@19a
    move-result v3

    #@19b
    if-eqz v3, :cond_216

    #@19d
    .line 3006
    new-instance v3, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@19f
    const/4 v5, 0x7

    #@1a0
    const/4 v6, 0x0

    #@1a1
    move-object/from16 v0, p0

    #@1a3
    iget-object v4, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@1a5
    iget-object v7, v4, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@1a7
    move-object/from16 v4, p0

    #@1a9
    move-object/from16 v8, p2

    #@1ab
    invoke-direct/range {v3 .. v8}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;Landroid/os/Parcel;)V

    #@1ae
    move-object/from16 v0, p0

    #@1b0
    iput-object v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mAudioTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@1b2
    .line 3011
    :goto_1b2
    const/4 v3, 0x0

    #@1b3
    move-object/from16 v0, p0

    #@1b5
    iput-boolean v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mVideoTurnedOn:Z

    #@1b7
    .line 3012
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1ba
    move-result v3

    #@1bb
    if-eqz v3, :cond_21c

    #@1bd
    .line 3013
    new-instance v3, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@1bf
    const/16 v5, 0x8

    #@1c1
    const/4 v6, 0x0

    #@1c2
    move-object/from16 v0, p0

    #@1c4
    iget-object v4, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@1c6
    iget-object v7, v4, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@1c8
    move-object/from16 v4, p0

    #@1ca
    move-object/from16 v8, p2

    #@1cc
    invoke-direct/range {v3 .. v8}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Ljava/util/ArrayList;Landroid/os/Parcel;)V

    #@1cf
    move-object/from16 v0, p0

    #@1d1
    iput-object v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mVideoTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@1d3
    .line 3018
    :goto_1d3
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1d6
    move-result v3

    #@1d7
    if-eqz v3, :cond_222

    #@1d9
    .line 3019
    const/4 v3, 0x3

    #@1da
    new-array v3, v3, [Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@1dc
    move-object/from16 v0, p0

    #@1de
    iput-object v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mUserActivityCounters:[Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@1e0
    .line 3020
    const/4 v9, 0x0

    #@1e1
    .local v9, i:I
    :goto_1e1
    const/4 v3, 0x3

    #@1e2
    if-ge v9, v3, :cond_227

    #@1e4
    .line 3021
    move-object/from16 v0, p0

    #@1e6
    iget-object v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mUserActivityCounters:[Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@1e8
    new-instance v4, Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@1ea
    move-object/from16 v0, p0

    #@1ec
    iget-object v5, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@1ee
    iget-object v5, v5, Lcom/android/internal/os/BatteryStatsImpl;->mUnpluggables:Ljava/util/ArrayList;

    #@1f0
    move-object/from16 v0, p2

    #@1f2
    invoke-direct {v4, v5, v0}, Lcom/android/internal/os/BatteryStatsImpl$Counter;-><init>(Ljava/util/ArrayList;Landroid/os/Parcel;)V

    #@1f5
    aput-object v4, v3, v9

    #@1f7
    .line 3020
    add-int/lit8 v9, v9, 0x1

    #@1f9
    goto :goto_1e1

    #@1fa
    .line 2981
    .end local v9           #i:I
    :cond_1fa
    const/4 v3, 0x0

    #@1fb
    move-object/from16 v0, p0

    #@1fd
    iput-object v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiRunningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@1ff
    goto/16 :goto_123

    #@201
    .line 2988
    :cond_201
    const/4 v3, 0x0

    #@202
    move-object/from16 v0, p0

    #@204
    iput-object v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mFullWifiLockTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@206
    goto/16 :goto_148

    #@208
    .line 2995
    :cond_208
    const/4 v3, 0x0

    #@209
    move-object/from16 v0, p0

    #@20b
    iput-object v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiScanTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@20d
    goto/16 :goto_16d

    #@20f
    .line 3002
    :cond_20f
    const/4 v3, 0x0

    #@210
    move-object/from16 v0, p0

    #@212
    iput-object v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiMulticastTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@214
    goto/16 :goto_192

    #@216
    .line 3009
    :cond_216
    const/4 v3, 0x0

    #@217
    move-object/from16 v0, p0

    #@219
    iput-object v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mAudioTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@21b
    goto :goto_1b2

    #@21c
    .line 3016
    :cond_21c
    const/4 v3, 0x0

    #@21d
    move-object/from16 v0, p0

    #@21f
    iput-object v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mVideoTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@221
    goto :goto_1d3

    #@222
    .line 3024
    :cond_222
    const/4 v3, 0x0

    #@223
    move-object/from16 v0, p0

    #@225
    iput-object v3, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mUserActivityCounters:[Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@227
    .line 3026
    :cond_227
    return-void
.end method

.method public reportExcessiveCpuLocked(Ljava/lang/String;JJ)V
    .registers 7
    .parameter "proc"
    .parameter "overTime"
    .parameter "usedTime"

    #@0
    .prologue
    .line 4044
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->getProcessStatsLocked(Ljava/lang/String;)Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;

    #@3
    move-result-object v0

    #@4
    .line 4045
    .local v0, p:Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;
    if-eqz v0, :cond_9

    #@6
    .line 4046
    invoke-virtual {v0, p2, p3, p4, p5}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->addExcessiveCpu(JJ)V

    #@9
    .line 4048
    :cond_9
    return-void
.end method

.method public reportExcessiveWakeLocked(Ljava/lang/String;JJ)V
    .registers 7
    .parameter "proc"
    .parameter "overTime"
    .parameter "usedTime"

    #@0
    .prologue
    .line 4037
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->getProcessStatsLocked(Ljava/lang/String;)Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;

    #@3
    move-result-object v0

    #@4
    .line 4038
    .local v0, p:Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;
    if-eqz v0, :cond_9

    #@6
    .line 4039
    invoke-virtual {v0, p2, p3, p4, p5}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->addExcessiveWake(JJ)V

    #@9
    .line 4041
    :cond_9
    return-void
.end method

.method reset()Z
    .registers 24

    #@0
    .prologue
    .line 2724
    const/4 v3, 0x0

    #@1
    .line 2726
    .local v3, active:Z
    move-object/from16 v0, p0

    #@3
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiRunningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@5
    move-object/from16 v19, v0

    #@7
    if-eqz v19, :cond_29

    #@9
    .line 2727
    move-object/from16 v0, p0

    #@b
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiRunningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@d
    move-object/from16 v19, v0

    #@f
    move-object/from16 v0, p0

    #@11
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@13
    move-object/from16 v20, v0

    #@15
    const/16 v21, 0x0

    #@17
    invoke-virtual/range {v19 .. v21}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->reset(Lcom/android/internal/os/BatteryStatsImpl;Z)Z

    #@1a
    move-result v19

    #@1b
    if-nez v19, :cond_12c

    #@1d
    const/16 v19, 0x1

    #@1f
    :goto_1f
    or-int v3, v3, v19

    #@21
    .line 2728
    move-object/from16 v0, p0

    #@23
    iget-boolean v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiRunning:Z

    #@25
    move/from16 v19, v0

    #@27
    or-int v3, v3, v19

    #@29
    .line 2730
    :cond_29
    move-object/from16 v0, p0

    #@2b
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mFullWifiLockTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2d
    move-object/from16 v19, v0

    #@2f
    if-eqz v19, :cond_51

    #@31
    .line 2731
    move-object/from16 v0, p0

    #@33
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mFullWifiLockTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@35
    move-object/from16 v19, v0

    #@37
    move-object/from16 v0, p0

    #@39
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@3b
    move-object/from16 v20, v0

    #@3d
    const/16 v21, 0x0

    #@3f
    invoke-virtual/range {v19 .. v21}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->reset(Lcom/android/internal/os/BatteryStatsImpl;Z)Z

    #@42
    move-result v19

    #@43
    if-nez v19, :cond_130

    #@45
    const/16 v19, 0x1

    #@47
    :goto_47
    or-int v3, v3, v19

    #@49
    .line 2732
    move-object/from16 v0, p0

    #@4b
    iget-boolean v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mFullWifiLockOut:Z

    #@4d
    move/from16 v19, v0

    #@4f
    or-int v3, v3, v19

    #@51
    .line 2734
    :cond_51
    move-object/from16 v0, p0

    #@53
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiScanTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@55
    move-object/from16 v19, v0

    #@57
    if-eqz v19, :cond_79

    #@59
    .line 2735
    move-object/from16 v0, p0

    #@5b
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiScanTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@5d
    move-object/from16 v19, v0

    #@5f
    move-object/from16 v0, p0

    #@61
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@63
    move-object/from16 v20, v0

    #@65
    const/16 v21, 0x0

    #@67
    invoke-virtual/range {v19 .. v21}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->reset(Lcom/android/internal/os/BatteryStatsImpl;Z)Z

    #@6a
    move-result v19

    #@6b
    if-nez v19, :cond_134

    #@6d
    const/16 v19, 0x1

    #@6f
    :goto_6f
    or-int v3, v3, v19

    #@71
    .line 2736
    move-object/from16 v0, p0

    #@73
    iget-boolean v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiScanStarted:Z

    #@75
    move/from16 v19, v0

    #@77
    or-int v3, v3, v19

    #@79
    .line 2738
    :cond_79
    move-object/from16 v0, p0

    #@7b
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiMulticastTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@7d
    move-object/from16 v19, v0

    #@7f
    if-eqz v19, :cond_a1

    #@81
    .line 2739
    move-object/from16 v0, p0

    #@83
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiMulticastTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@85
    move-object/from16 v19, v0

    #@87
    move-object/from16 v0, p0

    #@89
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@8b
    move-object/from16 v20, v0

    #@8d
    const/16 v21, 0x0

    #@8f
    invoke-virtual/range {v19 .. v21}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->reset(Lcom/android/internal/os/BatteryStatsImpl;Z)Z

    #@92
    move-result v19

    #@93
    if-nez v19, :cond_138

    #@95
    const/16 v19, 0x1

    #@97
    :goto_97
    or-int v3, v3, v19

    #@99
    .line 2740
    move-object/from16 v0, p0

    #@9b
    iget-boolean v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiMulticastEnabled:Z

    #@9d
    move/from16 v19, v0

    #@9f
    or-int v3, v3, v19

    #@a1
    .line 2742
    :cond_a1
    move-object/from16 v0, p0

    #@a3
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mAudioTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@a5
    move-object/from16 v19, v0

    #@a7
    if-eqz v19, :cond_c9

    #@a9
    .line 2743
    move-object/from16 v0, p0

    #@ab
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mAudioTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@ad
    move-object/from16 v19, v0

    #@af
    move-object/from16 v0, p0

    #@b1
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@b3
    move-object/from16 v20, v0

    #@b5
    const/16 v21, 0x0

    #@b7
    invoke-virtual/range {v19 .. v21}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->reset(Lcom/android/internal/os/BatteryStatsImpl;Z)Z

    #@ba
    move-result v19

    #@bb
    if-nez v19, :cond_13c

    #@bd
    const/16 v19, 0x1

    #@bf
    :goto_bf
    or-int v3, v3, v19

    #@c1
    .line 2744
    move-object/from16 v0, p0

    #@c3
    iget-boolean v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mAudioTurnedOn:Z

    #@c5
    move/from16 v19, v0

    #@c7
    or-int v3, v3, v19

    #@c9
    .line 2746
    :cond_c9
    move-object/from16 v0, p0

    #@cb
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mVideoTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@cd
    move-object/from16 v19, v0

    #@cf
    if-eqz v19, :cond_f1

    #@d1
    .line 2747
    move-object/from16 v0, p0

    #@d3
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mVideoTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@d5
    move-object/from16 v19, v0

    #@d7
    move-object/from16 v0, p0

    #@d9
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    #@db
    move-object/from16 v20, v0

    #@dd
    const/16 v21, 0x0

    #@df
    invoke-virtual/range {v19 .. v21}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->reset(Lcom/android/internal/os/BatteryStatsImpl;Z)Z

    #@e2
    move-result v19

    #@e3
    if-nez v19, :cond_13f

    #@e5
    const/16 v19, 0x1

    #@e7
    :goto_e7
    or-int v3, v3, v19

    #@e9
    .line 2748
    move-object/from16 v0, p0

    #@eb
    iget-boolean v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mVideoTurnedOn:Z

    #@ed
    move/from16 v19, v0

    #@ef
    or-int v3, v3, v19

    #@f1
    .line 2751
    :cond_f1
    const-wide/16 v19, 0x0

    #@f3
    move-wide/from16 v0, v19

    #@f5
    move-object/from16 v2, p0

    #@f7
    iput-wide v0, v2, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mLoadedTcpBytesSent:J

    #@f9
    move-wide/from16 v0, v19

    #@fb
    move-object/from16 v2, p0

    #@fd
    iput-wide v0, v2, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mLoadedTcpBytesReceived:J

    #@ff
    .line 2752
    const-wide/16 v19, 0x0

    #@101
    move-wide/from16 v0, v19

    #@103
    move-object/from16 v2, p0

    #@105
    iput-wide v0, v2, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mCurrentTcpBytesSent:J

    #@107
    move-wide/from16 v0, v19

    #@109
    move-object/from16 v2, p0

    #@10b
    iput-wide v0, v2, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mCurrentTcpBytesReceived:J

    #@10d
    .line 2754
    move-object/from16 v0, p0

    #@10f
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mUserActivityCounters:[Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@111
    move-object/from16 v19, v0

    #@113
    if-eqz v19, :cond_142

    #@115
    .line 2755
    const/4 v4, 0x0

    #@116
    .local v4, i:I
    :goto_116
    const/16 v19, 0x3

    #@118
    move/from16 v0, v19

    #@11a
    if-ge v4, v0, :cond_142

    #@11c
    .line 2756
    move-object/from16 v0, p0

    #@11e
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mUserActivityCounters:[Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@120
    move-object/from16 v19, v0

    #@122
    aget-object v19, v19, v4

    #@124
    const/16 v20, 0x0

    #@126
    invoke-virtual/range {v19 .. v20}, Lcom/android/internal/os/BatteryStatsImpl$Counter;->reset(Z)V

    #@129
    .line 2755
    add-int/lit8 v4, v4, 0x1

    #@12b
    goto :goto_116

    #@12c
    .line 2727
    .end local v4           #i:I
    :cond_12c
    const/16 v19, 0x0

    #@12e
    goto/16 :goto_1f

    #@130
    .line 2731
    :cond_130
    const/16 v19, 0x0

    #@132
    goto/16 :goto_47

    #@134
    .line 2735
    :cond_134
    const/16 v19, 0x0

    #@136
    goto/16 :goto_6f

    #@138
    .line 2739
    :cond_138
    const/16 v19, 0x0

    #@13a
    goto/16 :goto_97

    #@13c
    .line 2743
    :cond_13c
    const/16 v19, 0x0

    #@13e
    goto :goto_bf

    #@13f
    .line 2747
    :cond_13f
    const/16 v19, 0x0

    #@141
    goto :goto_e7

    #@142
    .line 2760
    :cond_142
    move-object/from16 v0, p0

    #@144
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWakelockStats:Ljava/util/HashMap;

    #@146
    move-object/from16 v19, v0

    #@148
    invoke-virtual/range {v19 .. v19}, Ljava/util/HashMap;->size()I

    #@14b
    move-result v19

    #@14c
    if-lez v19, :cond_17a

    #@14e
    .line 2761
    move-object/from16 v0, p0

    #@150
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWakelockStats:Ljava/util/HashMap;

    #@152
    move-object/from16 v19, v0

    #@154
    invoke-virtual/range {v19 .. v19}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@157
    move-result-object v19

    #@158
    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@15b
    move-result-object v8

    #@15c
    .line 2762
    .local v8, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;>;>;"
    :goto_15c
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@15f
    move-result v19

    #@160
    if-eqz v19, :cond_17a

    #@162
    .line 2763
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@165
    move-result-object v17

    #@166
    check-cast v17, Ljava/util/Map$Entry;

    #@168
    .line 2764
    .local v17, wakelockEntry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;>;"
    invoke-interface/range {v17 .. v17}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@16b
    move-result-object v18

    #@16c
    check-cast v18, Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;

    #@16e
    .line 2765
    .local v18, wl:Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;
    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;->reset()Z

    #@171
    move-result v19

    #@172
    if-eqz v19, :cond_178

    #@174
    .line 2766
    invoke-interface {v8}, Ljava/util/Iterator;->remove()V

    #@177
    goto :goto_15c

    #@178
    .line 2768
    :cond_178
    const/4 v3, 0x1

    #@179
    goto :goto_15c

    #@17a
    .line 2772
    .end local v8           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;>;>;"
    .end local v17           #wakelockEntry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;>;"
    .end local v18           #wl:Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;
    :cond_17a
    move-object/from16 v0, p0

    #@17c
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mSensorStats:Ljava/util/HashMap;

    #@17e
    move-object/from16 v19, v0

    #@180
    invoke-virtual/range {v19 .. v19}, Ljava/util/HashMap;->size()I

    #@183
    move-result v19

    #@184
    if-lez v19, :cond_1b2

    #@186
    .line 2773
    move-object/from16 v0, p0

    #@188
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mSensorStats:Ljava/util/HashMap;

    #@18a
    move-object/from16 v19, v0

    #@18c
    invoke-virtual/range {v19 .. v19}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@18f
    move-result-object v19

    #@190
    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@193
    move-result-object v5

    #@194
    .line 2774
    .local v5, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;>;>;"
    :goto_194
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@197
    move-result v19

    #@198
    if-eqz v19, :cond_1b2

    #@19a
    .line 2775
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@19d
    move-result-object v15

    #@19e
    check-cast v15, Ljava/util/Map$Entry;

    #@1a0
    .line 2776
    .local v15, sensorEntry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;>;"
    invoke-interface {v15}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@1a3
    move-result-object v14

    #@1a4
    check-cast v14, Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;

    #@1a6
    .line 2777
    .local v14, s:Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;
    invoke-virtual {v14}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;->reset()Z

    #@1a9
    move-result v19

    #@1aa
    if-eqz v19, :cond_1b0

    #@1ac
    .line 2778
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    #@1af
    goto :goto_194

    #@1b0
    .line 2780
    :cond_1b0
    const/4 v3, 0x1

    #@1b1
    goto :goto_194

    #@1b2
    .line 2784
    .end local v5           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;>;>;"
    .end local v14           #s:Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;
    .end local v15           #sensorEntry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;>;"
    :cond_1b2
    move-object/from16 v0, p0

    #@1b4
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mProcessStats:Ljava/util/HashMap;

    #@1b6
    move-object/from16 v19, v0

    #@1b8
    invoke-virtual/range {v19 .. v19}, Ljava/util/HashMap;->size()I

    #@1bb
    move-result v19

    #@1bc
    if-lez v19, :cond_1eb

    #@1be
    .line 2785
    move-object/from16 v0, p0

    #@1c0
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mProcessStats:Ljava/util/HashMap;

    #@1c2
    move-object/from16 v19, v0

    #@1c4
    invoke-virtual/range {v19 .. v19}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@1c7
    move-result-object v19

    #@1c8
    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@1cb
    move-result-object v7

    #@1cc
    .line 2786
    .local v7, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;>;>;"
    :goto_1cc
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@1cf
    move-result v19

    #@1d0
    if-eqz v19, :cond_1e2

    #@1d2
    .line 2787
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1d5
    move-result-object v13

    #@1d6
    check-cast v13, Ljava/util/Map$Entry;

    #@1d8
    .line 2788
    .local v13, procEntry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;>;"
    invoke-interface {v13}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@1db
    move-result-object v19

    #@1dc
    check-cast v19, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;

    #@1de
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->detach()V

    #@1e1
    goto :goto_1cc

    #@1e2
    .line 2790
    .end local v13           #procEntry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;>;"
    :cond_1e2
    move-object/from16 v0, p0

    #@1e4
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mProcessStats:Ljava/util/HashMap;

    #@1e6
    move-object/from16 v19, v0

    #@1e8
    invoke-virtual/range {v19 .. v19}, Ljava/util/HashMap;->clear()V

    #@1eb
    .line 2792
    .end local v7           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;>;>;"
    :cond_1eb
    move-object/from16 v0, p0

    #@1ed
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mPids:Landroid/util/SparseArray;

    #@1ef
    move-object/from16 v19, v0

    #@1f1
    invoke-virtual/range {v19 .. v19}, Landroid/util/SparseArray;->size()I

    #@1f4
    move-result v19

    #@1f5
    if-lez v19, :cond_224

    #@1f7
    .line 2793
    const/4 v4, 0x0

    #@1f8
    .restart local v4       #i:I
    :goto_1f8
    if-nez v3, :cond_224

    #@1fa
    move-object/from16 v0, p0

    #@1fc
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mPids:Landroid/util/SparseArray;

    #@1fe
    move-object/from16 v19, v0

    #@200
    invoke-virtual/range {v19 .. v19}, Landroid/util/SparseArray;->size()I

    #@203
    move-result v19

    #@204
    move/from16 v0, v19

    #@206
    if-ge v4, v0, :cond_224

    #@208
    .line 2794
    move-object/from16 v0, p0

    #@20a
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mPids:Landroid/util/SparseArray;

    #@20c
    move-object/from16 v19, v0

    #@20e
    move-object/from16 v0, v19

    #@210
    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@213
    move-result-object v11

    #@214
    check-cast v11, Landroid/os/BatteryStats$Uid$Pid;

    #@216
    .line 2795
    .local v11, pid:Landroid/os/BatteryStats$Uid$Pid;
    iget-wide v0, v11, Landroid/os/BatteryStats$Uid$Pid;->mWakeStart:J

    #@218
    move-wide/from16 v19, v0

    #@21a
    const-wide/16 v21, 0x0

    #@21c
    cmp-long v19, v19, v21

    #@21e
    if-eqz v19, :cond_221

    #@220
    .line 2796
    const/4 v3, 0x1

    #@221
    .line 2793
    :cond_221
    add-int/lit8 v4, v4, 0x1

    #@223
    goto :goto_1f8

    #@224
    .line 2800
    .end local v4           #i:I
    .end local v11           #pid:Landroid/os/BatteryStats$Uid$Pid;
    :cond_224
    move-object/from16 v0, p0

    #@226
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mPackageStats:Ljava/util/HashMap;

    #@228
    move-object/from16 v19, v0

    #@22a
    invoke-virtual/range {v19 .. v19}, Ljava/util/HashMap;->size()I

    #@22d
    move-result v19

    #@22e
    if-lez v19, :cond_288

    #@230
    .line 2801
    move-object/from16 v0, p0

    #@232
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mPackageStats:Ljava/util/HashMap;

    #@234
    move-object/from16 v19, v0

    #@236
    invoke-virtual/range {v19 .. v19}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@239
    move-result-object v19

    #@23a
    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@23d
    move-result-object v6

    #@23e
    .line 2802
    .local v6, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;>;>;"
    :cond_23e
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@241
    move-result v19

    #@242
    if-eqz v19, :cond_27f

    #@244
    .line 2803
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@247
    move-result-object v12

    #@248
    check-cast v12, Ljava/util/Map$Entry;

    #@24a
    .line 2804
    .local v12, pkgEntry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;>;"
    invoke-interface {v12}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@24d
    move-result-object v10

    #@24e
    check-cast v10, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;

    #@250
    .line 2805
    .local v10, p:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;
    invoke-virtual {v10}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;->detach()V

    #@253
    .line 2806
    iget-object v0, v10, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;->mServiceStats:Ljava/util/HashMap;

    #@255
    move-object/from16 v19, v0

    #@257
    invoke-virtual/range {v19 .. v19}, Ljava/util/HashMap;->size()I

    #@25a
    move-result v19

    #@25b
    if-lez v19, :cond_23e

    #@25d
    .line 2807
    iget-object v0, v10, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;->mServiceStats:Ljava/util/HashMap;

    #@25f
    move-object/from16 v19, v0

    #@261
    invoke-virtual/range {v19 .. v19}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@264
    move-result-object v19

    #@265
    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@268
    move-result-object v9

    #@269
    .line 2809
    .local v9, it2:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;>;>;"
    :goto_269
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    #@26c
    move-result v19

    #@26d
    if-eqz v19, :cond_23e

    #@26f
    .line 2810
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@272
    move-result-object v16

    #@273
    check-cast v16, Ljava/util/Map$Entry;

    #@275
    .line 2811
    .local v16, servEntry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;>;"
    invoke-interface/range {v16 .. v16}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@278
    move-result-object v19

    #@279
    check-cast v19, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;

    #@27b
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;->detach()V

    #@27e
    goto :goto_269

    #@27f
    .line 2815
    .end local v9           #it2:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;>;>;"
    .end local v10           #p:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;
    .end local v12           #pkgEntry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;>;"
    .end local v16           #servEntry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg$Serv;>;"
    :cond_27f
    move-object/from16 v0, p0

    #@281
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mPackageStats:Ljava/util/HashMap;

    #@283
    move-object/from16 v19, v0

    #@285
    invoke-virtual/range {v19 .. v19}, Ljava/util/HashMap;->clear()V

    #@288
    .line 2818
    .end local v6           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;>;>;"
    :cond_288
    move-object/from16 v0, p0

    #@28a
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mPids:Landroid/util/SparseArray;

    #@28c
    move-object/from16 v19, v0

    #@28e
    invoke-virtual/range {v19 .. v19}, Landroid/util/SparseArray;->clear()V

    #@291
    .line 2820
    if-nez v3, :cond_316

    #@293
    .line 2821
    move-object/from16 v0, p0

    #@295
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiRunningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@297
    move-object/from16 v19, v0

    #@299
    if-eqz v19, :cond_2a4

    #@29b
    .line 2822
    move-object/from16 v0, p0

    #@29d
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiRunningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@29f
    move-object/from16 v19, v0

    #@2a1
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->detach()V

    #@2a4
    .line 2824
    :cond_2a4
    move-object/from16 v0, p0

    #@2a6
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mFullWifiLockTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2a8
    move-object/from16 v19, v0

    #@2aa
    if-eqz v19, :cond_2b5

    #@2ac
    .line 2825
    move-object/from16 v0, p0

    #@2ae
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mFullWifiLockTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2b0
    move-object/from16 v19, v0

    #@2b2
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->detach()V

    #@2b5
    .line 2827
    :cond_2b5
    move-object/from16 v0, p0

    #@2b7
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiScanTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2b9
    move-object/from16 v19, v0

    #@2bb
    if-eqz v19, :cond_2c6

    #@2bd
    .line 2828
    move-object/from16 v0, p0

    #@2bf
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiScanTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2c1
    move-object/from16 v19, v0

    #@2c3
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->detach()V

    #@2c6
    .line 2830
    :cond_2c6
    move-object/from16 v0, p0

    #@2c8
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiMulticastTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2ca
    move-object/from16 v19, v0

    #@2cc
    if-eqz v19, :cond_2d7

    #@2ce
    .line 2831
    move-object/from16 v0, p0

    #@2d0
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiMulticastTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2d2
    move-object/from16 v19, v0

    #@2d4
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->detach()V

    #@2d7
    .line 2833
    :cond_2d7
    move-object/from16 v0, p0

    #@2d9
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mAudioTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2db
    move-object/from16 v19, v0

    #@2dd
    if-eqz v19, :cond_2e8

    #@2df
    .line 2834
    move-object/from16 v0, p0

    #@2e1
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mAudioTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2e3
    move-object/from16 v19, v0

    #@2e5
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->detach()V

    #@2e8
    .line 2836
    :cond_2e8
    move-object/from16 v0, p0

    #@2ea
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mVideoTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2ec
    move-object/from16 v19, v0

    #@2ee
    if-eqz v19, :cond_2f9

    #@2f0
    .line 2837
    move-object/from16 v0, p0

    #@2f2
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mVideoTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@2f4
    move-object/from16 v19, v0

    #@2f6
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->detach()V

    #@2f9
    .line 2839
    :cond_2f9
    move-object/from16 v0, p0

    #@2fb
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mUserActivityCounters:[Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@2fd
    move-object/from16 v19, v0

    #@2ff
    if-eqz v19, :cond_316

    #@301
    .line 2840
    const/4 v4, 0x0

    #@302
    .restart local v4       #i:I
    :goto_302
    const/16 v19, 0x3

    #@304
    move/from16 v0, v19

    #@306
    if-ge v4, v0, :cond_316

    #@308
    .line 2841
    move-object/from16 v0, p0

    #@30a
    iget-object v0, v0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mUserActivityCounters:[Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@30c
    move-object/from16 v19, v0

    #@30e
    aget-object v19, v19, v4

    #@310
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/os/BatteryStatsImpl$Counter;->detach()V

    #@313
    .line 2840
    add-int/lit8 v4, v4, 0x1

    #@315
    goto :goto_302

    #@316
    .line 2846
    .end local v4           #i:I
    :cond_316
    if-nez v3, :cond_31b

    #@318
    const/16 v19, 0x1

    #@31a
    :goto_31a
    return v19

    #@31b
    :cond_31b
    const/16 v19, 0x0

    #@31d
    goto :goto_31a
.end method

.method writeToParcelLocked(Landroid/os/Parcel;J)V
    .registers 16
    .parameter "out"
    .parameter "batteryRealtime"

    #@0
    .prologue
    .line 2850
    iget-object v10, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWakelockStats:Ljava/util/HashMap;

    #@2
    invoke-virtual {v10}, Ljava/util/HashMap;->size()I

    #@5
    move-result v10

    #@6
    invoke-virtual {p1, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@9
    .line 2851
    iget-object v10, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWakelockStats:Ljava/util/HashMap;

    #@b
    invoke-virtual {v10}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@e
    move-result-object v10

    #@f
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@12
    move-result-object v1

    #@13
    .local v1, i$:Ljava/util/Iterator;
    :goto_13
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@16
    move-result v10

    #@17
    if-eqz v10, :cond_32

    #@19
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1c
    move-result-object v9

    #@1d
    check-cast v9, Ljava/util/Map$Entry;

    #@1f
    .line 2852
    .local v9, wakelockEntry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;>;"
    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@22
    move-result-object v10

    #@23
    check-cast v10, Ljava/lang/String;

    #@25
    invoke-virtual {p1, v10}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@28
    .line 2853
    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@2b
    move-result-object v8

    #@2c
    check-cast v8, Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;

    #@2e
    .line 2854
    .local v8, wakelock:Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;
    invoke-virtual {v8, p1, p2, p3}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;->writeToParcelLocked(Landroid/os/Parcel;J)V

    #@31
    goto :goto_13

    #@32
    .line 2857
    .end local v8           #wakelock:Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;
    .end local v9           #wakelockEntry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Wakelock;>;"
    :cond_32
    iget-object v10, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mSensorStats:Ljava/util/HashMap;

    #@34
    invoke-virtual {v10}, Ljava/util/HashMap;->size()I

    #@37
    move-result v10

    #@38
    invoke-virtual {p1, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@3b
    .line 2858
    iget-object v10, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mSensorStats:Ljava/util/HashMap;

    #@3d
    invoke-virtual {v10}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@40
    move-result-object v10

    #@41
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@44
    move-result-object v1

    #@45
    :goto_45
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@48
    move-result v10

    #@49
    if-eqz v10, :cond_68

    #@4b
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@4e
    move-result-object v7

    #@4f
    check-cast v7, Ljava/util/Map$Entry;

    #@51
    .line 2859
    .local v7, sensorEntry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;>;"
    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@54
    move-result-object v10

    #@55
    check-cast v10, Ljava/lang/Integer;

    #@57
    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    #@5a
    move-result v10

    #@5b
    invoke-virtual {p1, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@5e
    .line 2860
    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@61
    move-result-object v6

    #@62
    check-cast v6, Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;

    #@64
    .line 2861
    .local v6, sensor:Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;
    invoke-virtual {v6, p1, p2, p3}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;->writeToParcelLocked(Landroid/os/Parcel;J)V

    #@67
    goto :goto_45

    #@68
    .line 2864
    .end local v6           #sensor:Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;
    .end local v7           #sensorEntry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/internal/os/BatteryStatsImpl$Uid$Sensor;>;"
    :cond_68
    iget-object v10, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mProcessStats:Ljava/util/HashMap;

    #@6a
    invoke-virtual {v10}, Ljava/util/HashMap;->size()I

    #@6d
    move-result v10

    #@6e
    invoke-virtual {p1, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@71
    .line 2865
    iget-object v10, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mProcessStats:Ljava/util/HashMap;

    #@73
    invoke-virtual {v10}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@76
    move-result-object v10

    #@77
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@7a
    move-result-object v1

    #@7b
    :goto_7b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@7e
    move-result v10

    #@7f
    if-eqz v10, :cond_9a

    #@81
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@84
    move-result-object v5

    #@85
    check-cast v5, Ljava/util/Map$Entry;

    #@87
    .line 2866
    .local v5, procEntry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@8a
    move-result-object v10

    #@8b
    check-cast v10, Ljava/lang/String;

    #@8d
    invoke-virtual {p1, v10}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@90
    .line 2867
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@93
    move-result-object v4

    #@94
    check-cast v4, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;

    #@96
    .line 2868
    .local v4, proc:Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;
    invoke-virtual {v4, p1}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;->writeToParcelLocked(Landroid/os/Parcel;)V

    #@99
    goto :goto_7b

    #@9a
    .line 2871
    .end local v4           #proc:Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;
    .end local v5           #procEntry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Proc;>;"
    :cond_9a
    iget-object v10, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mPackageStats:Ljava/util/HashMap;

    #@9c
    invoke-virtual {v10}, Ljava/util/HashMap;->size()I

    #@9f
    move-result v10

    #@a0
    invoke-virtual {p1, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@a3
    .line 2872
    iget-object v10, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mPackageStats:Ljava/util/HashMap;

    #@a5
    invoke-virtual {v10}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@a8
    move-result-object v10

    #@a9
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@ac
    move-result-object v1

    #@ad
    :goto_ad
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@b0
    move-result v10

    #@b1
    if-eqz v10, :cond_cc

    #@b3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@b6
    move-result-object v3

    #@b7
    check-cast v3, Ljava/util/Map$Entry;

    #@b9
    .line 2873
    .local v3, pkgEntry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@bc
    move-result-object v10

    #@bd
    check-cast v10, Ljava/lang/String;

    #@bf
    invoke-virtual {p1, v10}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@c2
    .line 2874
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@c5
    move-result-object v2

    #@c6
    check-cast v2, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;

    #@c8
    .line 2875
    .local v2, pkg:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;
    invoke-virtual {v2, p1}, Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;->writeToParcelLocked(Landroid/os/Parcel;)V

    #@cb
    goto :goto_ad

    #@cc
    .line 2878
    .end local v2           #pkg:Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;
    .end local v3           #pkgEntry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/internal/os/BatteryStatsImpl$Uid$Pkg;>;"
    :cond_cc
    iget-wide v10, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mLoadedTcpBytesReceived:J

    #@ce
    invoke-virtual {p1, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@d1
    .line 2879
    iget-wide v10, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mLoadedTcpBytesSent:J

    #@d3
    invoke-virtual {p1, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@d6
    .line 2880
    invoke-virtual {p0}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->computeCurrentTcpBytesReceived()J

    #@d9
    move-result-wide v10

    #@da
    invoke-virtual {p1, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@dd
    .line 2881
    invoke-virtual {p0}, Lcom/android/internal/os/BatteryStatsImpl$Uid;->computeCurrentTcpBytesSent()J

    #@e0
    move-result-wide v10

    #@e1
    invoke-virtual {p1, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@e4
    .line 2882
    iget-wide v10, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mTcpBytesReceivedAtLastUnplug:J

    #@e6
    invoke-virtual {p1, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@e9
    .line 2883
    iget-wide v10, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mTcpBytesSentAtLastUnplug:J

    #@eb
    invoke-virtual {p1, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@ee
    .line 2884
    iget-object v10, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiRunningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@f0
    if-eqz v10, :cond_152

    #@f2
    .line 2885
    const/4 v10, 0x1

    #@f3
    invoke-virtual {p1, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@f6
    .line 2886
    iget-object v10, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiRunningTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@f8
    invoke-virtual {v10, p1, p2, p3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeToParcel(Landroid/os/Parcel;J)V

    #@fb
    .line 2890
    :goto_fb
    iget-object v10, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mFullWifiLockTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@fd
    if-eqz v10, :cond_157

    #@ff
    .line 2891
    const/4 v10, 0x1

    #@100
    invoke-virtual {p1, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@103
    .line 2892
    iget-object v10, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mFullWifiLockTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@105
    invoke-virtual {v10, p1, p2, p3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeToParcel(Landroid/os/Parcel;J)V

    #@108
    .line 2896
    :goto_108
    iget-object v10, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiScanTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@10a
    if-eqz v10, :cond_15c

    #@10c
    .line 2897
    const/4 v10, 0x1

    #@10d
    invoke-virtual {p1, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@110
    .line 2898
    iget-object v10, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiScanTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@112
    invoke-virtual {v10, p1, p2, p3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeToParcel(Landroid/os/Parcel;J)V

    #@115
    .line 2902
    :goto_115
    iget-object v10, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiMulticastTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@117
    if-eqz v10, :cond_161

    #@119
    .line 2903
    const/4 v10, 0x1

    #@11a
    invoke-virtual {p1, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@11d
    .line 2904
    iget-object v10, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mWifiMulticastTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@11f
    invoke-virtual {v10, p1, p2, p3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeToParcel(Landroid/os/Parcel;J)V

    #@122
    .line 2908
    :goto_122
    iget-object v10, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mAudioTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@124
    if-eqz v10, :cond_166

    #@126
    .line 2909
    const/4 v10, 0x1

    #@127
    invoke-virtual {p1, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@12a
    .line 2910
    iget-object v10, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mAudioTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@12c
    invoke-virtual {v10, p1, p2, p3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeToParcel(Landroid/os/Parcel;J)V

    #@12f
    .line 2914
    :goto_12f
    iget-object v10, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mVideoTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@131
    if-eqz v10, :cond_16b

    #@133
    .line 2915
    const/4 v10, 0x1

    #@134
    invoke-virtual {p1, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@137
    .line 2916
    iget-object v10, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mVideoTurnedOnTimer:Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    #@139
    invoke-virtual {v10, p1, p2, p3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->writeToParcel(Landroid/os/Parcel;J)V

    #@13c
    .line 2920
    :goto_13c
    iget-object v10, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mUserActivityCounters:[Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@13e
    if-eqz v10, :cond_170

    #@140
    .line 2921
    const/4 v10, 0x1

    #@141
    invoke-virtual {p1, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@144
    .line 2922
    const/4 v0, 0x0

    #@145
    .local v0, i:I
    :goto_145
    const/4 v10, 0x3

    #@146
    if-ge v0, v10, :cond_174

    #@148
    .line 2923
    iget-object v10, p0, Lcom/android/internal/os/BatteryStatsImpl$Uid;->mUserActivityCounters:[Lcom/android/internal/os/BatteryStatsImpl$Counter;

    #@14a
    aget-object v10, v10, v0

    #@14c
    invoke-virtual {v10, p1}, Lcom/android/internal/os/BatteryStatsImpl$Counter;->writeToParcel(Landroid/os/Parcel;)V

    #@14f
    .line 2922
    add-int/lit8 v0, v0, 0x1

    #@151
    goto :goto_145

    #@152
    .line 2888
    .end local v0           #i:I
    :cond_152
    const/4 v10, 0x0

    #@153
    invoke-virtual {p1, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@156
    goto :goto_fb

    #@157
    .line 2894
    :cond_157
    const/4 v10, 0x0

    #@158
    invoke-virtual {p1, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@15b
    goto :goto_108

    #@15c
    .line 2900
    :cond_15c
    const/4 v10, 0x0

    #@15d
    invoke-virtual {p1, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@160
    goto :goto_115

    #@161
    .line 2906
    :cond_161
    const/4 v10, 0x0

    #@162
    invoke-virtual {p1, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@165
    goto :goto_122

    #@166
    .line 2912
    :cond_166
    const/4 v10, 0x0

    #@167
    invoke-virtual {p1, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@16a
    goto :goto_12f

    #@16b
    .line 2918
    :cond_16b
    const/4 v10, 0x0

    #@16c
    invoke-virtual {p1, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@16f
    goto :goto_13c

    #@170
    .line 2926
    :cond_170
    const/4 v10, 0x0

    #@171
    invoke-virtual {p1, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@174
    .line 2928
    :cond_174
    return-void
.end method
