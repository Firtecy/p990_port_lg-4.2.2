.class public Lcom/android/internal/os/WrapperInit;
.super Ljava/lang/Object;
.source "WrapperInit.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "AndroidRuntime"


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 42
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 43
    return-void
.end method

.method public static execApplication(Ljava/lang/String;Ljava/lang/String;ILjava/io/FileDescriptor;[Ljava/lang/String;)V
    .registers 8
    .parameter "invokeWith"
    .parameter "niceName"
    .parameter "targetSdkVersion"
    .parameter "pipeFd"
    .parameter "args"

    #@0
    .prologue
    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@5
    .line 103
    .local v0, command:Ljava/lang/StringBuilder;
    const-string v1, " /system/bin/app_process /system/bin --application"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 104
    if-eqz p1, :cond_1b

    #@c
    .line 105
    const-string v1, " \'--nice-name="

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    const-string v2, "\'"

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    .line 107
    :cond_1b
    const-string v1, " com.android.internal.os.WrapperInit "

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    .line 108
    if-eqz p3, :cond_3c

    #@22
    invoke-virtual {p3}, Ljava/io/FileDescriptor;->getInt$()I

    #@25
    move-result v1

    #@26
    :goto_26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    .line 109
    const/16 v1, 0x20

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2e
    .line 110
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@31
    .line 111
    invoke-static {v0, p4}, Ldalvik/system/Zygote;->appendQuotedShellArgs(Ljava/lang/StringBuilder;[Ljava/lang/String;)V

    #@34
    .line 112
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v1

    #@38
    invoke-static {v1}, Ldalvik/system/Zygote;->execShell(Ljava/lang/String;)V

    #@3b
    .line 113
    return-void

    #@3c
    .line 108
    :cond_3c
    const/4 v1, 0x0

    #@3d
    goto :goto_26
.end method

.method public static execStandalone(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .registers 6
    .parameter "invokeWith"
    .parameter "classPath"
    .parameter "className"
    .parameter "args"

    #@0
    .prologue
    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@5
    .line 127
    .local v0, command:Ljava/lang/StringBuilder;
    const-string v1, " /system/bin/dalvikvm -classpath \'"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    .line 128
    const-string v1, "\' "

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    .line 129
    invoke-static {v0, p3}, Ldalvik/system/Zygote;->appendQuotedShellArgs(Ljava/lang/StringBuilder;[Ljava/lang/String;)V

    #@1a
    .line 130
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-static {v1}, Ldalvik/system/Zygote;->execShell(Ljava/lang/String;)V

    #@21
    .line 131
    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .registers 11
    .parameter "args"

    #@0
    .prologue
    .line 61
    const/4 v7, 0x0

    #@1
    :try_start_1
    aget-object v7, p0, v7

    #@3
    const/16 v8, 0xa

    #@5
    invoke-static {v7, v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@8
    move-result v3

    #@9
    .line 62
    .local v3, fdNum:I
    const/4 v7, 0x1

    #@a
    aget-object v7, p0, v7

    #@c
    const/16 v8, 0xa

    #@e
    invoke-static {v7, v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_11
    .catch Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller; {:try_start_1 .. :try_end_11} :catch_4a

    #@11
    move-result v6

    #@12
    .line 66
    .local v6, targetSdkVersion:I
    if-eqz v3, :cond_2f

    #@14
    .line 68
    :try_start_14
    invoke-static {v3}, Lcom/android/internal/os/ZygoteInit;->createFileDescriptor(I)Ljava/io/FileDescriptor;

    #@17
    move-result-object v2

    #@18
    .line 69
    .local v2, fd:Ljava/io/FileDescriptor;
    new-instance v4, Ljava/io/DataOutputStream;

    #@1a
    new-instance v7, Ljava/io/FileOutputStream;

    #@1c
    invoke-direct {v7, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    #@1f
    invoke-direct {v4, v7}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@22
    .line 70
    .local v4, os:Ljava/io/DataOutputStream;
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@25
    move-result v7

    #@26
    invoke-virtual {v4, v7}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@29
    .line 71
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V

    #@2c
    .line 72
    invoke-static {v2}, Llibcore/io/IoUtils;->closeQuietly(Ljava/io/FileDescriptor;)V
    :try_end_2f
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_2f} :catch_41
    .catch Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller; {:try_start_14 .. :try_end_2f} :catch_4a

    #@2f
    .line 79
    .end local v2           #fd:Ljava/io/FileDescriptor;
    .end local v4           #os:Ljava/io/DataOutputStream;
    :cond_2f
    :goto_2f
    :try_start_2f
    invoke-static {}, Lcom/android/internal/os/ZygoteInit;->preload()V

    #@32
    .line 82
    array-length v7, p0

    #@33
    add-int/lit8 v7, v7, -0x2

    #@35
    new-array v5, v7, [Ljava/lang/String;

    #@37
    .line 83
    .local v5, runtimeArgs:[Ljava/lang/String;
    const/4 v7, 0x2

    #@38
    const/4 v8, 0x0

    #@39
    array-length v9, v5

    #@3a
    invoke-static {p0, v7, v5, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@3d
    .line 84
    invoke-static {v6, v5}, Lcom/android/internal/os/RuntimeInit;->wrapperInit(I[Ljava/lang/String;)V

    #@40
    .line 88
    .end local v3           #fdNum:I
    .end local v5           #runtimeArgs:[Ljava/lang/String;
    .end local v6           #targetSdkVersion:I
    :goto_40
    return-void

    #@41
    .line 73
    .restart local v3       #fdNum:I
    .restart local v6       #targetSdkVersion:I
    :catch_41
    move-exception v1

    #@42
    .line 74
    .local v1, ex:Ljava/io/IOException;
    const-string v7, "AndroidRuntime"

    #@44
    const-string v8, "Could not write pid of wrapped process to Zygote pipe."

    #@46
    invoke-static {v7, v8, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_49
    .catch Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller; {:try_start_2f .. :try_end_49} :catch_4a

    #@49
    goto :goto_2f

    #@4a
    .line 85
    .end local v1           #ex:Ljava/io/IOException;
    .end local v3           #fdNum:I
    .end local v6           #targetSdkVersion:I
    :catch_4a
    move-exception v0

    #@4b
    .line 86
    .local v0, caller:Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller;
    invoke-virtual {v0}, Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller;->run()V

    #@4e
    goto :goto_40
.end method
