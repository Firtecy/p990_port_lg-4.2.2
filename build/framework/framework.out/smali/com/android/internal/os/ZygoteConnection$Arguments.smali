.class Lcom/android/internal/os/ZygoteConnection$Arguments;
.super Ljava/lang/Object;
.source "ZygoteConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/os/ZygoteConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Arguments"
.end annotation


# instance fields
.field capabilitiesSpecified:Z

.field classpath:Ljava/lang/String;

.field debugFlags:I

.field effectiveCapabilities:J

.field gid:I

.field gidSpecified:Z

.field gids:[I

.field invokeWith:Ljava/lang/String;

.field mountExternal:I

.field niceName:Ljava/lang/String;

.field peerWait:Z

.field permittedCapabilities:J

.field remainingArgs:[Ljava/lang/String;

.field rlimits:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[I>;"
        }
    .end annotation
.end field

.field runtimeInit:Z

.field seInfo:Ljava/lang/String;

.field seInfoSpecified:Z

.field targetSdkVersion:I

.field targetSdkVersionSpecified:Z

.field uid:I

.field uidSpecified:Z


# direct methods
.method constructor <init>([Ljava/lang/String;)V
    .registers 3
    .parameter "args"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 384
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 323
    iput v0, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->uid:I

    #@6
    .line 327
    iput v0, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->gid:I

    #@8
    .line 343
    iput v0, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->mountExternal:I

    #@a
    .line 385
    invoke-direct {p0, p1}, Lcom/android/internal/os/ZygoteConnection$Arguments;->parseArgs([Ljava/lang/String;)V

    #@d
    .line 386
    return-void
.end method

.method private parseArgs([Ljava/lang/String;)V
    .registers 14
    .parameter "args"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 398
    const/4 v3, 0x0

    #@1
    .line 400
    .local v3, curArg:I
    :goto_1
    array-length v9, p1

    #@2
    if-ge v3, v9, :cond_10

    #@4
    .line 401
    aget-object v0, p1, v3

    #@6
    .line 403
    .local v0, arg:Ljava/lang/String;
    const-string v9, "--"

    #@8
    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v9

    #@c
    if-eqz v9, :cond_20

    #@e
    .line 404
    add-int/lit8 v3, v3, 0x1

    #@10
    .line 539
    .end local v0           #arg:Ljava/lang/String;
    :cond_10
    iget-boolean v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->runtimeInit:Z

    #@12
    if-eqz v9, :cond_29b

    #@14
    iget-object v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->classpath:Ljava/lang/String;

    #@16
    if-eqz v9, :cond_29b

    #@18
    .line 540
    new-instance v9, Ljava/lang/IllegalArgumentException;

    #@1a
    const-string v10, "--runtime-init and -classpath are incompatible"

    #@1c
    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v9

    #@20
    .line 406
    .restart local v0       #arg:Ljava/lang/String;
    :cond_20
    const-string v9, "--setuid="

    #@22
    invoke-virtual {v0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@25
    move-result v9

    #@26
    if-eqz v9, :cond_4c

    #@28
    .line 407
    iget-boolean v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->uidSpecified:Z

    #@2a
    if-eqz v9, :cond_34

    #@2c
    .line 408
    new-instance v9, Ljava/lang/IllegalArgumentException;

    #@2e
    const-string v10, "Duplicate arg specified"

    #@30
    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@33
    throw v9

    #@34
    .line 411
    :cond_34
    const/4 v9, 0x1

    #@35
    iput-boolean v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->uidSpecified:Z

    #@37
    .line 412
    const/16 v9, 0x3d

    #@39
    invoke-virtual {v0, v9}, Ljava/lang/String;->indexOf(I)I

    #@3c
    move-result v9

    #@3d
    add-int/lit8 v9, v9, 0x1

    #@3f
    invoke-virtual {v0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@42
    move-result-object v9

    #@43
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@46
    move-result v9

    #@47
    iput v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->uid:I

    #@49
    .line 400
    :cond_49
    :goto_49
    add-int/lit8 v3, v3, 0x1

    #@4b
    goto :goto_1

    #@4c
    .line 414
    :cond_4c
    const-string v9, "--setgid="

    #@4e
    invoke-virtual {v0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@51
    move-result v9

    #@52
    if-eqz v9, :cond_76

    #@54
    .line 415
    iget-boolean v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->gidSpecified:Z

    #@56
    if-eqz v9, :cond_60

    #@58
    .line 416
    new-instance v9, Ljava/lang/IllegalArgumentException;

    #@5a
    const-string v10, "Duplicate arg specified"

    #@5c
    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@5f
    throw v9

    #@60
    .line 419
    :cond_60
    const/4 v9, 0x1

    #@61
    iput-boolean v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->gidSpecified:Z

    #@63
    .line 420
    const/16 v9, 0x3d

    #@65
    invoke-virtual {v0, v9}, Ljava/lang/String;->indexOf(I)I

    #@68
    move-result v9

    #@69
    add-int/lit8 v9, v9, 0x1

    #@6b
    invoke-virtual {v0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@6e
    move-result-object v9

    #@6f
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@72
    move-result v9

    #@73
    iput v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->gid:I

    #@75
    goto :goto_49

    #@76
    .line 422
    :cond_76
    const-string v9, "--target-sdk-version="

    #@78
    invoke-virtual {v0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@7b
    move-result v9

    #@7c
    if-eqz v9, :cond_a0

    #@7e
    .line 423
    iget-boolean v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->targetSdkVersionSpecified:Z

    #@80
    if-eqz v9, :cond_8a

    #@82
    .line 424
    new-instance v9, Ljava/lang/IllegalArgumentException;

    #@84
    const-string v10, "Duplicate target-sdk-version specified"

    #@86
    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@89
    throw v9

    #@8a
    .line 427
    :cond_8a
    const/4 v9, 0x1

    #@8b
    iput-boolean v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->targetSdkVersionSpecified:Z

    #@8d
    .line 428
    const/16 v9, 0x3d

    #@8f
    invoke-virtual {v0, v9}, Ljava/lang/String;->indexOf(I)I

    #@92
    move-result v9

    #@93
    add-int/lit8 v9, v9, 0x1

    #@95
    invoke-virtual {v0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@98
    move-result-object v9

    #@99
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@9c
    move-result v9

    #@9d
    iput v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->targetSdkVersion:I

    #@9f
    goto :goto_49

    #@a0
    .line 430
    :cond_a0
    const-string v9, "--enable-debugger"

    #@a2
    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a5
    move-result v9

    #@a6
    if-eqz v9, :cond_af

    #@a8
    .line 431
    iget v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->debugFlags:I

    #@aa
    or-int/lit8 v9, v9, 0x1

    #@ac
    iput v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->debugFlags:I

    #@ae
    goto :goto_49

    #@af
    .line 432
    :cond_af
    const-string v9, "--enable-safemode"

    #@b1
    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b4
    move-result v9

    #@b5
    if-eqz v9, :cond_be

    #@b7
    .line 433
    iget v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->debugFlags:I

    #@b9
    or-int/lit8 v9, v9, 0x8

    #@bb
    iput v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->debugFlags:I

    #@bd
    goto :goto_49

    #@be
    .line 434
    :cond_be
    const-string v9, "--enable-checkjni"

    #@c0
    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c3
    move-result v9

    #@c4
    if-eqz v9, :cond_ce

    #@c6
    .line 435
    iget v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->debugFlags:I

    #@c8
    or-int/lit8 v9, v9, 0x2

    #@ca
    iput v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->debugFlags:I

    #@cc
    goto/16 :goto_49

    #@ce
    .line 436
    :cond_ce
    const-string v9, "--enable-jni-logging"

    #@d0
    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d3
    move-result v9

    #@d4
    if-eqz v9, :cond_de

    #@d6
    .line 437
    iget v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->debugFlags:I

    #@d8
    or-int/lit8 v9, v9, 0x10

    #@da
    iput v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->debugFlags:I

    #@dc
    goto/16 :goto_49

    #@de
    .line 438
    :cond_de
    const-string v9, "--enable-assert"

    #@e0
    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e3
    move-result v9

    #@e4
    if-eqz v9, :cond_ee

    #@e6
    .line 439
    iget v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->debugFlags:I

    #@e8
    or-int/lit8 v9, v9, 0x4

    #@ea
    iput v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->debugFlags:I

    #@ec
    goto/16 :goto_49

    #@ee
    .line 440
    :cond_ee
    const-string v9, "--peer-wait"

    #@f0
    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f3
    move-result v9

    #@f4
    if-eqz v9, :cond_fb

    #@f6
    .line 441
    const/4 v9, 0x1

    #@f7
    iput-boolean v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->peerWait:Z

    #@f9
    goto/16 :goto_49

    #@fb
    .line 442
    :cond_fb
    const-string v9, "--runtime-init"

    #@fd
    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@100
    move-result v9

    #@101
    if-eqz v9, :cond_108

    #@103
    .line 443
    const/4 v9, 0x1

    #@104
    iput-boolean v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->runtimeInit:Z

    #@106
    goto/16 :goto_49

    #@108
    .line 444
    :cond_108
    const-string v9, "--seinfo="

    #@10a
    invoke-virtual {v0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@10d
    move-result v9

    #@10e
    if-eqz v9, :cond_12f

    #@110
    .line 445
    iget-boolean v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->seInfoSpecified:Z

    #@112
    if-eqz v9, :cond_11c

    #@114
    .line 446
    new-instance v9, Ljava/lang/IllegalArgumentException;

    #@116
    const-string v10, "Duplicate arg specified"

    #@118
    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@11b
    throw v9

    #@11c
    .line 449
    :cond_11c
    const/4 v9, 0x1

    #@11d
    iput-boolean v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->seInfoSpecified:Z

    #@11f
    .line 450
    const/16 v9, 0x3d

    #@121
    invoke-virtual {v0, v9}, Ljava/lang/String;->indexOf(I)I

    #@124
    move-result v9

    #@125
    add-int/lit8 v9, v9, 0x1

    #@127
    invoke-virtual {v0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@12a
    move-result-object v9

    #@12b
    iput-object v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->seInfo:Ljava/lang/String;

    #@12d
    goto/16 :goto_49

    #@12f
    .line 451
    :cond_12f
    const-string v9, "--capabilities="

    #@131
    invoke-virtual {v0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@134
    move-result v9

    #@135
    if-eqz v9, :cond_18c

    #@137
    .line 452
    iget-boolean v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->capabilitiesSpecified:Z

    #@139
    if-eqz v9, :cond_143

    #@13b
    .line 453
    new-instance v9, Ljava/lang/IllegalArgumentException;

    #@13d
    const-string v10, "Duplicate arg specified"

    #@13f
    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@142
    throw v9

    #@143
    .line 456
    :cond_143
    const/4 v9, 0x1

    #@144
    iput-boolean v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->capabilitiesSpecified:Z

    #@146
    .line 457
    const/16 v9, 0x3d

    #@148
    invoke-virtual {v0, v9}, Ljava/lang/String;->indexOf(I)I

    #@14b
    move-result v9

    #@14c
    add-int/lit8 v9, v9, 0x1

    #@14e
    invoke-virtual {v0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@151
    move-result-object v1

    #@152
    .line 459
    .local v1, capString:Ljava/lang/String;
    const-string v9, ","

    #@154
    const/4 v10, 0x2

    #@155
    invoke-virtual {v1, v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    #@158
    move-result-object v2

    #@159
    .line 461
    .local v2, capStrings:[Ljava/lang/String;
    array-length v9, v2

    #@15a
    const/4 v10, 0x1

    #@15b
    if-ne v9, v10, :cond_170

    #@15d
    .line 462
    const/4 v9, 0x0

    #@15e
    aget-object v9, v2, v9

    #@160
    invoke-static {v9}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;

    #@163
    move-result-object v9

    #@164
    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    #@167
    move-result-wide v9

    #@168
    iput-wide v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->effectiveCapabilities:J

    #@16a
    .line 463
    iget-wide v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->effectiveCapabilities:J

    #@16c
    iput-wide v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->permittedCapabilities:J

    #@16e
    goto/16 :goto_49

    #@170
    .line 465
    :cond_170
    const/4 v9, 0x0

    #@171
    aget-object v9, v2, v9

    #@173
    invoke-static {v9}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;

    #@176
    move-result-object v9

    #@177
    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    #@17a
    move-result-wide v9

    #@17b
    iput-wide v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->permittedCapabilities:J

    #@17d
    .line 466
    const/4 v9, 0x1

    #@17e
    aget-object v9, v2, v9

    #@180
    invoke-static {v9}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;

    #@183
    move-result-object v9

    #@184
    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    #@187
    move-result-wide v9

    #@188
    iput-wide v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->effectiveCapabilities:J

    #@18a
    goto/16 :goto_49

    #@18c
    .line 468
    .end local v1           #capString:Ljava/lang/String;
    .end local v2           #capStrings:[Ljava/lang/String;
    :cond_18c
    const-string v9, "--rlimit="

    #@18e
    invoke-virtual {v0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@191
    move-result v9

    #@192
    if-eqz v9, :cond_1d6

    #@194
    .line 470
    const/16 v9, 0x3d

    #@196
    invoke-virtual {v0, v9}, Ljava/lang/String;->indexOf(I)I

    #@199
    move-result v9

    #@19a
    add-int/lit8 v9, v9, 0x1

    #@19c
    invoke-virtual {v0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@19f
    move-result-object v9

    #@1a0
    const-string v10, ","

    #@1a2
    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@1a5
    move-result-object v6

    #@1a6
    .line 473
    .local v6, limitStrings:[Ljava/lang/String;
    array-length v9, v6

    #@1a7
    const/4 v10, 0x3

    #@1a8
    if-eq v9, v10, :cond_1b2

    #@1aa
    .line 474
    new-instance v9, Ljava/lang/IllegalArgumentException;

    #@1ac
    const-string v10, "--rlimit= should have 3 comma-delimited ints"

    #@1ae
    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b1
    throw v9

    #@1b2
    .line 477
    :cond_1b2
    array-length v9, v6

    #@1b3
    new-array v8, v9, [I

    #@1b5
    .line 479
    .local v8, rlimitTuple:[I
    const/4 v5, 0x0

    #@1b6
    .local v5, i:I
    :goto_1b6
    array-length v9, v6

    #@1b7
    if-ge v5, v9, :cond_1c4

    #@1b9
    .line 480
    aget-object v9, v6, v5

    #@1bb
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1be
    move-result v9

    #@1bf
    aput v9, v8, v5

    #@1c1
    .line 479
    add-int/lit8 v5, v5, 0x1

    #@1c3
    goto :goto_1b6

    #@1c4
    .line 483
    :cond_1c4
    iget-object v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->rlimits:Ljava/util/ArrayList;

    #@1c6
    if-nez v9, :cond_1cf

    #@1c8
    .line 484
    new-instance v9, Ljava/util/ArrayList;

    #@1ca
    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    #@1cd
    iput-object v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->rlimits:Ljava/util/ArrayList;

    #@1cf
    .line 487
    :cond_1cf
    iget-object v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->rlimits:Ljava/util/ArrayList;

    #@1d1
    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1d4
    goto/16 :goto_49

    #@1d6
    .line 488
    .end local v5           #i:I
    .end local v6           #limitStrings:[Ljava/lang/String;
    .end local v8           #rlimitTuple:[I
    :cond_1d6
    const-string v9, "-classpath"

    #@1d8
    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1db
    move-result v9

    #@1dc
    if-eqz v9, :cond_1fb

    #@1de
    .line 489
    iget-object v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->classpath:Ljava/lang/String;

    #@1e0
    if-eqz v9, :cond_1ea

    #@1e2
    .line 490
    new-instance v9, Ljava/lang/IllegalArgumentException;

    #@1e4
    const-string v10, "Duplicate arg specified"

    #@1e6
    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e9
    throw v9

    #@1ea
    .line 494
    :cond_1ea
    add-int/lit8 v3, v3, 0x1

    #@1ec
    :try_start_1ec
    aget-object v9, p1, v3

    #@1ee
    iput-object v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->classpath:Ljava/lang/String;
    :try_end_1f0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1ec .. :try_end_1f0} :catch_1f2

    #@1f0
    goto/16 :goto_49

    #@1f2
    .line 495
    :catch_1f2
    move-exception v4

    #@1f3
    .line 496
    .local v4, ex:Ljava/lang/IndexOutOfBoundsException;
    new-instance v9, Ljava/lang/IllegalArgumentException;

    #@1f5
    const-string v10, "-classpath requires argument"

    #@1f7
    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1fa
    throw v9

    #@1fb
    .line 499
    .end local v4           #ex:Ljava/lang/IndexOutOfBoundsException;
    :cond_1fb
    const-string v9, "--setgroups="

    #@1fd
    invoke-virtual {v0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@200
    move-result v9

    #@201
    if-eqz v9, :cond_238

    #@203
    .line 500
    iget-object v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->gids:[I

    #@205
    if-eqz v9, :cond_20f

    #@207
    .line 501
    new-instance v9, Ljava/lang/IllegalArgumentException;

    #@209
    const-string v10, "Duplicate arg specified"

    #@20b
    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@20e
    throw v9

    #@20f
    .line 505
    :cond_20f
    const/16 v9, 0x3d

    #@211
    invoke-virtual {v0, v9}, Ljava/lang/String;->indexOf(I)I

    #@214
    move-result v9

    #@215
    add-int/lit8 v9, v9, 0x1

    #@217
    invoke-virtual {v0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@21a
    move-result-object v9

    #@21b
    const-string v10, ","

    #@21d
    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@220
    move-result-object v7

    #@221
    .line 508
    .local v7, params:[Ljava/lang/String;
    array-length v9, v7

    #@222
    new-array v9, v9, [I

    #@224
    iput-object v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->gids:[I

    #@226
    .line 510
    array-length v9, v7

    #@227
    add-int/lit8 v5, v9, -0x1

    #@229
    .restart local v5       #i:I
    :goto_229
    if-ltz v5, :cond_49

    #@22b
    .line 511
    iget-object v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->gids:[I

    #@22d
    aget-object v10, v7, v5

    #@22f
    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@232
    move-result v10

    #@233
    aput v10, v9, v5

    #@235
    .line 510
    add-int/lit8 v5, v5, -0x1

    #@237
    goto :goto_229

    #@238
    .line 513
    .end local v5           #i:I
    .end local v7           #params:[Ljava/lang/String;
    :cond_238
    const-string v9, "--invoke-with"

    #@23a
    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23d
    move-result v9

    #@23e
    if-eqz v9, :cond_25d

    #@240
    .line 514
    iget-object v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->invokeWith:Ljava/lang/String;

    #@242
    if-eqz v9, :cond_24c

    #@244
    .line 515
    new-instance v9, Ljava/lang/IllegalArgumentException;

    #@246
    const-string v10, "Duplicate arg specified"

    #@248
    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@24b
    throw v9

    #@24c
    .line 519
    :cond_24c
    add-int/lit8 v3, v3, 0x1

    #@24e
    :try_start_24e
    aget-object v9, p1, v3

    #@250
    iput-object v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->invokeWith:Ljava/lang/String;
    :try_end_252
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_24e .. :try_end_252} :catch_254

    #@252
    goto/16 :goto_49

    #@254
    .line 520
    :catch_254
    move-exception v4

    #@255
    .line 521
    .restart local v4       #ex:Ljava/lang/IndexOutOfBoundsException;
    new-instance v9, Ljava/lang/IllegalArgumentException;

    #@257
    const-string v10, "--invoke-with requires argument"

    #@259
    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@25c
    throw v9

    #@25d
    .line 524
    .end local v4           #ex:Ljava/lang/IndexOutOfBoundsException;
    :cond_25d
    const-string v9, "--nice-name="

    #@25f
    invoke-virtual {v0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@262
    move-result v9

    #@263
    if-eqz v9, :cond_281

    #@265
    .line 525
    iget-object v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->niceName:Ljava/lang/String;

    #@267
    if-eqz v9, :cond_271

    #@269
    .line 526
    new-instance v9, Ljava/lang/IllegalArgumentException;

    #@26b
    const-string v10, "Duplicate arg specified"

    #@26d
    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@270
    throw v9

    #@271
    .line 529
    :cond_271
    const/16 v9, 0x3d

    #@273
    invoke-virtual {v0, v9}, Ljava/lang/String;->indexOf(I)I

    #@276
    move-result v9

    #@277
    add-int/lit8 v9, v9, 0x1

    #@279
    invoke-virtual {v0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@27c
    move-result-object v9

    #@27d
    iput-object v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->niceName:Ljava/lang/String;

    #@27f
    goto/16 :goto_49

    #@281
    .line 530
    :cond_281
    const-string v9, "--mount-external-multiuser"

    #@283
    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@286
    move-result v9

    #@287
    if-eqz v9, :cond_28e

    #@289
    .line 531
    const/4 v9, 0x2

    #@28a
    iput v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->mountExternal:I

    #@28c
    goto/16 :goto_49

    #@28e
    .line 532
    :cond_28e
    const-string v9, "--mount-external-multiuser-all"

    #@290
    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@293
    move-result v9

    #@294
    if-eqz v9, :cond_10

    #@296
    .line 533
    const/4 v9, 0x3

    #@297
    iput v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->mountExternal:I

    #@299
    goto/16 :goto_49

    #@29b
    .line 544
    .end local v0           #arg:Ljava/lang/String;
    :cond_29b
    array-length v9, p1

    #@29c
    sub-int/2addr v9, v3

    #@29d
    new-array v9, v9, [Ljava/lang/String;

    #@29f
    iput-object v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->remainingArgs:[Ljava/lang/String;

    #@2a1
    .line 546
    iget-object v9, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->remainingArgs:[Ljava/lang/String;

    #@2a3
    const/4 v10, 0x0

    #@2a4
    iget-object v11, p0, Lcom/android/internal/os/ZygoteConnection$Arguments;->remainingArgs:[Ljava/lang/String;

    #@2a6
    array-length v11, v11

    #@2a7
    invoke-static {p1, v3, v9, v10, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@2aa
    .line 548
    return-void
.end method
