.class public abstract Lcom/android/internal/os/IDropBoxManagerService$Stub;
.super Landroid/os/Binder;
.source "IDropBoxManagerService.java"

# interfaces
.implements Lcom/android/internal/os/IDropBoxManagerService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/os/IDropBoxManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/os/IDropBoxManagerService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.os.IDropBoxManagerService"

.field static final TRANSACTION_add:I = 0x1

.field static final TRANSACTION_getNextEntry:I = 0x3

.field static final TRANSACTION_isTagEnabled:I = 0x2


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 21
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 22
    const-string v0, "com.android.internal.os.IDropBoxManagerService"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/os/IDropBoxManagerService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 23
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/os/IDropBoxManagerService;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 30
    if-nez p0, :cond_4

    #@2
    .line 31
    const/4 v0, 0x0

    #@3
    .line 37
    :goto_3
    return-object v0

    #@4
    .line 33
    :cond_4
    const-string v1, "com.android.internal.os.IDropBoxManagerService"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 34
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/os/IDropBoxManagerService;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 35
    check-cast v0, Lcom/android/internal/os/IDropBoxManagerService;

    #@12
    goto :goto_3

    #@13
    .line 37
    :cond_13
    new-instance v0, Lcom/android/internal/os/IDropBoxManagerService$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/os/IDropBoxManagerService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 41
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 12
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 45
    sparse-switch p1, :sswitch_data_64

    #@5
    .line 95
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v5

    #@9
    :goto_9
    return v5

    #@a
    .line 49
    :sswitch_a
    const-string v4, "com.android.internal.os.IDropBoxManagerService"

    #@c
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 54
    :sswitch_10
    const-string v4, "com.android.internal.os.IDropBoxManagerService"

    #@12
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 56
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v4

    #@19
    if-eqz v4, :cond_2a

    #@1b
    .line 57
    sget-object v4, Landroid/os/DropBoxManager$Entry;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1d
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@20
    move-result-object v0

    #@21
    check-cast v0, Landroid/os/DropBoxManager$Entry;

    #@23
    .line 62
    .local v0, _arg0:Landroid/os/DropBoxManager$Entry;
    :goto_23
    invoke-virtual {p0, v0}, Lcom/android/internal/os/IDropBoxManagerService$Stub;->add(Landroid/os/DropBoxManager$Entry;)V

    #@26
    .line 63
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@29
    goto :goto_9

    #@2a
    .line 60
    .end local v0           #_arg0:Landroid/os/DropBoxManager$Entry;
    :cond_2a
    const/4 v0, 0x0

    #@2b
    .restart local v0       #_arg0:Landroid/os/DropBoxManager$Entry;
    goto :goto_23

    #@2c
    .line 68
    .end local v0           #_arg0:Landroid/os/DropBoxManager$Entry;
    :sswitch_2c
    const-string v6, "com.android.internal.os.IDropBoxManagerService"

    #@2e
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@31
    .line 70
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@34
    move-result-object v0

    #@35
    .line 71
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/android/internal/os/IDropBoxManagerService$Stub;->isTagEnabled(Ljava/lang/String;)Z

    #@38
    move-result v3

    #@39
    .line 72
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3c
    .line 73
    if-eqz v3, :cond_3f

    #@3e
    move v4, v5

    #@3f
    :cond_3f
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@42
    goto :goto_9

    #@43
    .line 78
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v3           #_result:Z
    :sswitch_43
    const-string v6, "com.android.internal.os.IDropBoxManagerService"

    #@45
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@48
    .line 80
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4b
    move-result-object v0

    #@4c
    .line 82
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    #@4f
    move-result-wide v1

    #@50
    .line 83
    .local v1, _arg1:J
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/internal/os/IDropBoxManagerService$Stub;->getNextEntry(Ljava/lang/String;J)Landroid/os/DropBoxManager$Entry;

    #@53
    move-result-object v3

    #@54
    .line 84
    .local v3, _result:Landroid/os/DropBoxManager$Entry;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@57
    .line 85
    if-eqz v3, :cond_60

    #@59
    .line 86
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@5c
    .line 87
    invoke-virtual {v3, p3, v5}, Landroid/os/DropBoxManager$Entry;->writeToParcel(Landroid/os/Parcel;I)V

    #@5f
    goto :goto_9

    #@60
    .line 90
    :cond_60
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@63
    goto :goto_9

    #@64
    .line 45
    :sswitch_data_64
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_2c
        0x3 -> :sswitch_43
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
