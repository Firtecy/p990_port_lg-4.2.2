.class public Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller;
.super Ljava/lang/Exception;
.source "ZygoteInit.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/os/ZygoteInit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MethodAndArgsCaller"
.end annotation


# instance fields
.field private final mArgs:[Ljava/lang/String;

.field private final mMethod:Ljava/lang/reflect/Method;


# direct methods
.method public constructor <init>(Ljava/lang/reflect/Method;[Ljava/lang/String;)V
    .registers 3
    .parameter "method"
    .parameter "args"

    #@0
    .prologue
    .line 790
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    #@3
    .line 791
    iput-object p1, p0, Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller;->mMethod:Ljava/lang/reflect/Method;

    #@5
    .line 792
    iput-object p2, p0, Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller;->mArgs:[Ljava/lang/String;

    #@7
    .line 793
    return-void
.end method


# virtual methods
.method public run()V
    .registers 8

    #@0
    .prologue
    .line 797
    :try_start_0
    iget-object v2, p0, Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller;->mMethod:Ljava/lang/reflect/Method;

    #@2
    const/4 v3, 0x0

    #@3
    const/4 v4, 0x1

    #@4
    new-array v4, v4, [Ljava/lang/Object;

    #@6
    const/4 v5, 0x0

    #@7
    iget-object v6, p0, Lcom/android/internal/os/ZygoteInit$MethodAndArgsCaller;->mArgs:[Ljava/lang/String;

    #@9
    aput-object v6, v4, v5

    #@b
    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_e
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_e} :catch_f
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_e} :catch_16

    #@e
    .line 809
    return-void

    #@f
    .line 798
    :catch_f
    move-exception v1

    #@10
    .line 799
    .local v1, ex:Ljava/lang/IllegalAccessException;
    new-instance v2, Ljava/lang/RuntimeException;

    #@12
    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@15
    throw v2

    #@16
    .line 800
    .end local v1           #ex:Ljava/lang/IllegalAccessException;
    :catch_16
    move-exception v1

    #@17
    .line 801
    .local v1, ex:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    #@1a
    move-result-object v0

    #@1b
    .line 802
    .local v0, cause:Ljava/lang/Throwable;
    instance-of v2, v0, Ljava/lang/RuntimeException;

    #@1d
    if-eqz v2, :cond_22

    #@1f
    .line 803
    check-cast v0, Ljava/lang/RuntimeException;

    #@21
    .end local v0           #cause:Ljava/lang/Throwable;
    throw v0

    #@22
    .line 804
    .restart local v0       #cause:Ljava/lang/Throwable;
    :cond_22
    instance-of v2, v0, Ljava/lang/Error;

    #@24
    if-eqz v2, :cond_29

    #@26
    .line 805
    check-cast v0, Ljava/lang/Error;

    #@28
    .end local v0           #cause:Ljava/lang/Throwable;
    throw v0

    #@29
    .line 807
    .restart local v0       #cause:Ljava/lang/Throwable;
    :cond_29
    new-instance v2, Ljava/lang/RuntimeException;

    #@2b
    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@2e
    throw v2
.end method
