.class Lcom/android/internal/os/storage/ExternalStorageFormatter$2;
.super Ljava/lang/Thread;
.source "ExternalStorageFormatter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/os/storage/ExternalStorageFormatter;->updateProgressState()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/os/storage/ExternalStorageFormatter;

.field final synthetic val$extStoragePath:Ljava/lang/String;

.field final synthetic val$mountService:Landroid/os/storage/IMountService;


# direct methods
.method constructor <init>(Lcom/android/internal/os/storage/ExternalStorageFormatter;Landroid/os/storage/IMountService;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 193
    iput-object p1, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter$2;->this$0:Lcom/android/internal/os/storage/ExternalStorageFormatter;

    #@2
    iput-object p2, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter$2;->val$mountService:Landroid/os/storage/IMountService;

    #@4
    iput-object p3, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter$2;->val$extStoragePath:Ljava/lang/String;

    #@6
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@9
    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    #@0
    .prologue
    .line 196
    const/4 v1, 0x0

    #@1
    .line 198
    .local v1, success:Z
    :try_start_1
    iget-object v2, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter$2;->val$mountService:Landroid/os/storage/IMountService;

    #@3
    iget-object v3, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter$2;->val$extStoragePath:Ljava/lang/String;

    #@5
    invoke-interface {v2, v3}, Landroid/os/storage/IMountService;->formatVolume(Ljava/lang/String;)I
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_8} :catch_25

    #@8
    .line 199
    const/4 v1, 0x1

    #@9
    .line 206
    :goto_9
    if-eqz v1, :cond_3a

    #@b
    .line 207
    iget-object v2, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter$2;->this$0:Lcom/android/internal/os/storage/ExternalStorageFormatter;

    #@d
    invoke-static {v2}, Lcom/android/internal/os/storage/ExternalStorageFormatter;->access$000(Lcom/android/internal/os/storage/ExternalStorageFormatter;)Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_3a

    #@13
    .line 208
    iget-object v2, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter$2;->this$0:Lcom/android/internal/os/storage/ExternalStorageFormatter;

    #@15
    new-instance v3, Landroid/content/Intent;

    #@17
    const-string v4, "android.intent.action.MASTER_CLEAR"

    #@19
    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1c
    invoke-virtual {v2, v3}, Lcom/android/internal/os/storage/ExternalStorageFormatter;->sendBroadcast(Landroid/content/Intent;)V

    #@1f
    .line 210
    iget-object v2, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter$2;->this$0:Lcom/android/internal/os/storage/ExternalStorageFormatter;

    #@21
    invoke-virtual {v2}, Lcom/android/internal/os/storage/ExternalStorageFormatter;->stopSelf()V

    #@24
    .line 232
    :goto_24
    return-void

    #@25
    .line 200
    :catch_25
    move-exception v0

    #@26
    .line 201
    .local v0, e:Ljava/lang/Exception;
    invoke-static {}, Landroid/os/Looper;->prepare()V

    #@29
    .line 202
    invoke-static {}, Landroid/os/Looper;->loop()V

    #@2c
    .line 203
    iget-object v2, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter$2;->this$0:Lcom/android/internal/os/storage/ExternalStorageFormatter;

    #@2e
    const v3, 0x10404cb

    #@31
    const/4 v4, 0x1

    #@32
    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    #@35
    move-result-object v2

    #@36
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    #@39
    goto :goto_9

    #@3a
    .line 216
    .end local v0           #e:Ljava/lang/Exception;
    :cond_3a
    if-nez v1, :cond_56

    #@3c
    iget-object v2, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter$2;->this$0:Lcom/android/internal/os/storage/ExternalStorageFormatter;

    #@3e
    invoke-static {v2}, Lcom/android/internal/os/storage/ExternalStorageFormatter;->access$100(Lcom/android/internal/os/storage/ExternalStorageFormatter;)Z

    #@41
    move-result v2

    #@42
    if-eqz v2, :cond_56

    #@44
    .line 217
    iget-object v2, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter$2;->this$0:Lcom/android/internal/os/storage/ExternalStorageFormatter;

    #@46
    new-instance v3, Landroid/content/Intent;

    #@48
    const-string v4, "android.intent.action.MASTER_CLEAR"

    #@4a
    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@4d
    invoke-virtual {v2, v3}, Lcom/android/internal/os/storage/ExternalStorageFormatter;->sendBroadcast(Landroid/content/Intent;)V

    #@50
    .line 231
    :goto_50
    iget-object v2, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter$2;->this$0:Lcom/android/internal/os/storage/ExternalStorageFormatter;

    #@52
    invoke-virtual {v2}, Lcom/android/internal/os/storage/ExternalStorageFormatter;->stopSelf()V

    #@55
    goto :goto_24

    #@56
    .line 220
    :cond_56
    :try_start_56
    iget-object v2, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter$2;->val$mountService:Landroid/os/storage/IMountService;

    #@58
    iget-object v3, p0, Lcom/android/internal/os/storage/ExternalStorageFormatter$2;->val$extStoragePath:Ljava/lang/String;

    #@5a
    invoke-interface {v2, v3}, Landroid/os/storage/IMountService;->mountVolume(Ljava/lang/String;)I
    :try_end_5d
    .catch Landroid/os/RemoteException; {:try_start_56 .. :try_end_5d} :catch_5e
    .catch Ljava/lang/NullPointerException; {:try_start_56 .. :try_end_5d} :catch_67

    #@5d
    goto :goto_50

    #@5e
    .line 221
    :catch_5e
    move-exception v0

    #@5f
    .line 222
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "ExternalStorageFormatter"

    #@61
    const-string v3, "Failed talking with mount service"

    #@63
    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@66
    goto :goto_50

    #@67
    .line 226
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_67
    move-exception v0

    #@68
    .line 227
    .local v0, e:Ljava/lang/NullPointerException;
    const-string v2, "ExternalStorageFormatter"

    #@6a
    const-string v3, "Timeout, null returned"

    #@6c
    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@6f
    goto :goto_50
.end method
