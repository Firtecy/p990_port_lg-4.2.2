.class public abstract Lcom/android/internal/appwidget/IAppWidgetService$Stub;
.super Landroid/os/Binder;
.source "IAppWidgetService.java"

# interfaces
.implements Lcom/android/internal/appwidget/IAppWidgetService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/appwidget/IAppWidgetService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/appwidget/IAppWidgetService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.appwidget.IAppWidgetService"

.field static final TRANSACTION_allocateAppWidgetId:I = 0x5

.field static final TRANSACTION_bindAppWidgetId:I = 0x15

.field static final TRANSACTION_bindAppWidgetIdIfAllowed:I = 0x16

.field static final TRANSACTION_bindRemoteViewsService:I = 0x17

.field static final TRANSACTION_deleteAllHosts:I = 0x8

.field static final TRANSACTION_deleteAppWidgetId:I = 0x6

.field static final TRANSACTION_deleteHost:I = 0x7

.field static final TRANSACTION_getAppWidgetIds:I = 0x19

.field static final TRANSACTION_getAppWidgetIdsForHost:I = 0xa

.field static final TRANSACTION_getAppWidgetInfo:I = 0x12

.field static final TRANSACTION_getAppWidgetOptions:I = 0xd

.field static final TRANSACTION_getAppWidgetViews:I = 0x9

.field static final TRANSACTION_getInstalledProviders:I = 0x11

.field static final TRANSACTION_hasBindAppWidgetPermission:I = 0x13

.field static final TRANSACTION_notifyAppWidgetViewDataChanged:I = 0x10

.field static final TRANSACTION_partiallyUpdateAppWidgetIds:I = 0xe

.field static final TRANSACTION_setBindAppWidgetPermission:I = 0x14

.field static final TRANSACTION_startListening:I = 0x1

.field static final TRANSACTION_startListeningAsUser:I = 0x2

.field static final TRANSACTION_stopListening:I = 0x3

.field static final TRANSACTION_stopListeningAsUser:I = 0x4

.field static final TRANSACTION_unbindRemoteViewsService:I = 0x18

.field static final TRANSACTION_updateAppWidgetIds:I = 0xb

.field static final TRANSACTION_updateAppWidgetOptions:I = 0xc

.field static final TRANSACTION_updateAppWidgetProvider:I = 0xf


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 15
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 16
    const-string v0, "com.android.internal.appwidget.IAppWidgetService"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/appwidget/IAppWidgetService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 17
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/appwidget/IAppWidgetService;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 24
    if-nez p0, :cond_4

    #@2
    .line 25
    const/4 v0, 0x0

    #@3
    .line 31
    :goto_3
    return-object v0

    #@4
    .line 27
    :cond_4
    const-string v1, "com.android.internal.appwidget.IAppWidgetService"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 28
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/appwidget/IAppWidgetService;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 29
    check-cast v0, Lcom/android/internal/appwidget/IAppWidgetService;

    #@12
    goto :goto_3

    #@13
    .line 31
    :cond_13
    new-instance v0, Lcom/android/internal/appwidget/IAppWidgetService$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/appwidget/IAppWidgetService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 35
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 16
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v9, 0x1

    #@2
    .line 39
    sparse-switch p1, :sswitch_data_30c

    #@5
    .line 411
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v9

    #@9
    :goto_9
    return v9

    #@a
    .line 43
    :sswitch_a
    const-string v0, "com.android.internal.appwidget.IAppWidgetService"

    #@c
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 48
    :sswitch_10
    const-string v0, "com.android.internal.appwidget.IAppWidgetService"

    #@12
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 50
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@18
    move-result-object v0

    #@19
    invoke-static {v0}, Lcom/android/internal/appwidget/IAppWidgetHost$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/appwidget/IAppWidgetHost;

    #@1c
    move-result-object v1

    #@1d
    .line 52
    .local v1, _arg0:Lcom/android/internal/appwidget/IAppWidgetHost;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    .line 54
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@24
    move-result v3

    #@25
    .line 56
    .local v3, _arg2:I
    new-instance v4, Ljava/util/ArrayList;

    #@27
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@2a
    .line 57
    .local v4, _arg3:Ljava/util/List;,"Ljava/util/List<Landroid/widget/RemoteViews;>;"
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/android/internal/appwidget/IAppWidgetService$Stub;->startListening(Lcom/android/internal/appwidget/IAppWidgetHost;Ljava/lang/String;ILjava/util/List;)[I

    #@2d
    move-result-object v7

    #@2e
    .line 58
    .local v7, _result:[I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@31
    .line 59
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeIntArray([I)V

    #@34
    .line 60
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@37
    goto :goto_9

    #@38
    .line 65
    .end local v1           #_arg0:Lcom/android/internal/appwidget/IAppWidgetHost;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:I
    .end local v4           #_arg3:Ljava/util/List;,"Ljava/util/List<Landroid/widget/RemoteViews;>;"
    .end local v7           #_result:[I
    :sswitch_38
    const-string v0, "com.android.internal.appwidget.IAppWidgetService"

    #@3a
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3d
    .line 67
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@40
    move-result-object v0

    #@41
    invoke-static {v0}, Lcom/android/internal/appwidget/IAppWidgetHost$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/appwidget/IAppWidgetHost;

    #@44
    move-result-object v1

    #@45
    .line 69
    .restart local v1       #_arg0:Lcom/android/internal/appwidget/IAppWidgetHost;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@48
    move-result-object v2

    #@49
    .line 71
    .restart local v2       #_arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4c
    move-result v3

    #@4d
    .line 73
    .restart local v3       #_arg2:I
    new-instance v4, Ljava/util/ArrayList;

    #@4f
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@52
    .line 75
    .restart local v4       #_arg3:Ljava/util/List;,"Ljava/util/List<Landroid/widget/RemoteViews;>;"
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@55
    move-result v5

    #@56
    .local v5, _arg4:I
    move-object v0, p0

    #@57
    .line 76
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/appwidget/IAppWidgetService$Stub;->startListeningAsUser(Lcom/android/internal/appwidget/IAppWidgetHost;Ljava/lang/String;ILjava/util/List;I)[I

    #@5a
    move-result-object v7

    #@5b
    .line 77
    .restart local v7       #_result:[I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5e
    .line 78
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeIntArray([I)V

    #@61
    .line 79
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@64
    goto :goto_9

    #@65
    .line 84
    .end local v1           #_arg0:Lcom/android/internal/appwidget/IAppWidgetHost;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:I
    .end local v4           #_arg3:Ljava/util/List;,"Ljava/util/List<Landroid/widget/RemoteViews;>;"
    .end local v5           #_arg4:I
    .end local v7           #_result:[I
    :sswitch_65
    const-string v0, "com.android.internal.appwidget.IAppWidgetService"

    #@67
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6a
    .line 86
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@6d
    move-result v1

    #@6e
    .line 87
    .local v1, _arg0:I
    invoke-virtual {p0, v1}, Lcom/android/internal/appwidget/IAppWidgetService$Stub;->stopListening(I)V

    #@71
    .line 88
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@74
    goto :goto_9

    #@75
    .line 93
    .end local v1           #_arg0:I
    :sswitch_75
    const-string v0, "com.android.internal.appwidget.IAppWidgetService"

    #@77
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7a
    .line 95
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@7d
    move-result v1

    #@7e
    .line 97
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@81
    move-result v2

    #@82
    .line 98
    .local v2, _arg1:I
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/appwidget/IAppWidgetService$Stub;->stopListeningAsUser(II)V

    #@85
    .line 99
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@88
    goto :goto_9

    #@89
    .line 104
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    :sswitch_89
    const-string v0, "com.android.internal.appwidget.IAppWidgetService"

    #@8b
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8e
    .line 106
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@91
    move-result-object v1

    #@92
    .line 108
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@95
    move-result v2

    #@96
    .line 109
    .restart local v2       #_arg1:I
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/appwidget/IAppWidgetService$Stub;->allocateAppWidgetId(Ljava/lang/String;I)I

    #@99
    move-result v7

    #@9a
    .line 110
    .local v7, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@9d
    .line 111
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@a0
    goto/16 :goto_9

    #@a2
    .line 116
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:I
    .end local v7           #_result:I
    :sswitch_a2
    const-string v0, "com.android.internal.appwidget.IAppWidgetService"

    #@a4
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a7
    .line 118
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@aa
    move-result v1

    #@ab
    .line 119
    .local v1, _arg0:I
    invoke-virtual {p0, v1}, Lcom/android/internal/appwidget/IAppWidgetService$Stub;->deleteAppWidgetId(I)V

    #@ae
    .line 120
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@b1
    goto/16 :goto_9

    #@b3
    .line 125
    .end local v1           #_arg0:I
    :sswitch_b3
    const-string v0, "com.android.internal.appwidget.IAppWidgetService"

    #@b5
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b8
    .line 127
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@bb
    move-result v1

    #@bc
    .line 128
    .restart local v1       #_arg0:I
    invoke-virtual {p0, v1}, Lcom/android/internal/appwidget/IAppWidgetService$Stub;->deleteHost(I)V

    #@bf
    .line 129
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@c2
    goto/16 :goto_9

    #@c4
    .line 134
    .end local v1           #_arg0:I
    :sswitch_c4
    const-string v0, "com.android.internal.appwidget.IAppWidgetService"

    #@c6
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c9
    .line 135
    invoke-virtual {p0}, Lcom/android/internal/appwidget/IAppWidgetService$Stub;->deleteAllHosts()V

    #@cc
    .line 136
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@cf
    goto/16 :goto_9

    #@d1
    .line 141
    :sswitch_d1
    const-string v10, "com.android.internal.appwidget.IAppWidgetService"

    #@d3
    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d6
    .line 143
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@d9
    move-result v1

    #@da
    .line 144
    .restart local v1       #_arg0:I
    invoke-virtual {p0, v1}, Lcom/android/internal/appwidget/IAppWidgetService$Stub;->getAppWidgetViews(I)Landroid/widget/RemoteViews;

    #@dd
    move-result-object v7

    #@de
    .line 145
    .local v7, _result:Landroid/widget/RemoteViews;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@e1
    .line 146
    if-eqz v7, :cond_eb

    #@e3
    .line 147
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    #@e6
    .line 148
    invoke-virtual {v7, p3, v9}, Landroid/widget/RemoteViews;->writeToParcel(Landroid/os/Parcel;I)V

    #@e9
    goto/16 :goto_9

    #@eb
    .line 151
    :cond_eb
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@ee
    goto/16 :goto_9

    #@f0
    .line 157
    .end local v1           #_arg0:I
    .end local v7           #_result:Landroid/widget/RemoteViews;
    :sswitch_f0
    const-string v0, "com.android.internal.appwidget.IAppWidgetService"

    #@f2
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f5
    .line 159
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@f8
    move-result v1

    #@f9
    .line 160
    .restart local v1       #_arg0:I
    invoke-virtual {p0, v1}, Lcom/android/internal/appwidget/IAppWidgetService$Stub;->getAppWidgetIdsForHost(I)[I

    #@fc
    move-result-object v7

    #@fd
    .line 161
    .local v7, _result:[I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@100
    .line 162
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeIntArray([I)V

    #@103
    goto/16 :goto_9

    #@105
    .line 167
    .end local v1           #_arg0:I
    .end local v7           #_result:[I
    :sswitch_105
    const-string v0, "com.android.internal.appwidget.IAppWidgetService"

    #@107
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@10a
    .line 169
    invoke-virtual {p2}, Landroid/os/Parcel;->createIntArray()[I

    #@10d
    move-result-object v1

    #@10e
    .line 171
    .local v1, _arg0:[I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@111
    move-result v0

    #@112
    if-eqz v0, :cond_124

    #@114
    .line 172
    sget-object v0, Landroid/widget/RemoteViews;->CREATOR:Landroid/os/Parcelable$Creator;

    #@116
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@119
    move-result-object v2

    #@11a
    check-cast v2, Landroid/widget/RemoteViews;

    #@11c
    .line 177
    .local v2, _arg1:Landroid/widget/RemoteViews;
    :goto_11c
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/appwidget/IAppWidgetService$Stub;->updateAppWidgetIds([ILandroid/widget/RemoteViews;)V

    #@11f
    .line 178
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@122
    goto/16 :goto_9

    #@124
    .line 175
    .end local v2           #_arg1:Landroid/widget/RemoteViews;
    :cond_124
    const/4 v2, 0x0

    #@125
    .restart local v2       #_arg1:Landroid/widget/RemoteViews;
    goto :goto_11c

    #@126
    .line 183
    .end local v1           #_arg0:[I
    .end local v2           #_arg1:Landroid/widget/RemoteViews;
    :sswitch_126
    const-string v0, "com.android.internal.appwidget.IAppWidgetService"

    #@128
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@12b
    .line 185
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@12e
    move-result v1

    #@12f
    .line 187
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@132
    move-result v0

    #@133
    if-eqz v0, :cond_145

    #@135
    .line 188
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@137
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@13a
    move-result-object v2

    #@13b
    check-cast v2, Landroid/os/Bundle;

    #@13d
    .line 193
    .local v2, _arg1:Landroid/os/Bundle;
    :goto_13d
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/appwidget/IAppWidgetService$Stub;->updateAppWidgetOptions(ILandroid/os/Bundle;)V

    #@140
    .line 194
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@143
    goto/16 :goto_9

    #@145
    .line 191
    .end local v2           #_arg1:Landroid/os/Bundle;
    :cond_145
    const/4 v2, 0x0

    #@146
    .restart local v2       #_arg1:Landroid/os/Bundle;
    goto :goto_13d

    #@147
    .line 199
    .end local v1           #_arg0:I
    .end local v2           #_arg1:Landroid/os/Bundle;
    :sswitch_147
    const-string v10, "com.android.internal.appwidget.IAppWidgetService"

    #@149
    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14c
    .line 201
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@14f
    move-result v1

    #@150
    .line 202
    .restart local v1       #_arg0:I
    invoke-virtual {p0, v1}, Lcom/android/internal/appwidget/IAppWidgetService$Stub;->getAppWidgetOptions(I)Landroid/os/Bundle;

    #@153
    move-result-object v7

    #@154
    .line 203
    .local v7, _result:Landroid/os/Bundle;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@157
    .line 204
    if-eqz v7, :cond_161

    #@159
    .line 205
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    #@15c
    .line 206
    invoke-virtual {v7, p3, v9}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@15f
    goto/16 :goto_9

    #@161
    .line 209
    :cond_161
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@164
    goto/16 :goto_9

    #@166
    .line 215
    .end local v1           #_arg0:I
    .end local v7           #_result:Landroid/os/Bundle;
    :sswitch_166
    const-string v0, "com.android.internal.appwidget.IAppWidgetService"

    #@168
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@16b
    .line 217
    invoke-virtual {p2}, Landroid/os/Parcel;->createIntArray()[I

    #@16e
    move-result-object v1

    #@16f
    .line 219
    .local v1, _arg0:[I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@172
    move-result v0

    #@173
    if-eqz v0, :cond_185

    #@175
    .line 220
    sget-object v0, Landroid/widget/RemoteViews;->CREATOR:Landroid/os/Parcelable$Creator;

    #@177
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@17a
    move-result-object v2

    #@17b
    check-cast v2, Landroid/widget/RemoteViews;

    #@17d
    .line 225
    .local v2, _arg1:Landroid/widget/RemoteViews;
    :goto_17d
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/appwidget/IAppWidgetService$Stub;->partiallyUpdateAppWidgetIds([ILandroid/widget/RemoteViews;)V

    #@180
    .line 226
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@183
    goto/16 :goto_9

    #@185
    .line 223
    .end local v2           #_arg1:Landroid/widget/RemoteViews;
    :cond_185
    const/4 v2, 0x0

    #@186
    .restart local v2       #_arg1:Landroid/widget/RemoteViews;
    goto :goto_17d

    #@187
    .line 231
    .end local v1           #_arg0:[I
    .end local v2           #_arg1:Landroid/widget/RemoteViews;
    :sswitch_187
    const-string v0, "com.android.internal.appwidget.IAppWidgetService"

    #@189
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18c
    .line 233
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18f
    move-result v0

    #@190
    if-eqz v0, :cond_1b0

    #@192
    .line 234
    sget-object v0, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@194
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@197
    move-result-object v1

    #@198
    check-cast v1, Landroid/content/ComponentName;

    #@19a
    .line 240
    .local v1, _arg0:Landroid/content/ComponentName;
    :goto_19a
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@19d
    move-result v0

    #@19e
    if-eqz v0, :cond_1b2

    #@1a0
    .line 241
    sget-object v0, Landroid/widget/RemoteViews;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1a2
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1a5
    move-result-object v2

    #@1a6
    check-cast v2, Landroid/widget/RemoteViews;

    #@1a8
    .line 246
    .restart local v2       #_arg1:Landroid/widget/RemoteViews;
    :goto_1a8
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/appwidget/IAppWidgetService$Stub;->updateAppWidgetProvider(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    #@1ab
    .line 247
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1ae
    goto/16 :goto_9

    #@1b0
    .line 237
    .end local v1           #_arg0:Landroid/content/ComponentName;
    .end local v2           #_arg1:Landroid/widget/RemoteViews;
    :cond_1b0
    const/4 v1, 0x0

    #@1b1
    .restart local v1       #_arg0:Landroid/content/ComponentName;
    goto :goto_19a

    #@1b2
    .line 244
    :cond_1b2
    const/4 v2, 0x0

    #@1b3
    .restart local v2       #_arg1:Landroid/widget/RemoteViews;
    goto :goto_1a8

    #@1b4
    .line 252
    .end local v1           #_arg0:Landroid/content/ComponentName;
    .end local v2           #_arg1:Landroid/widget/RemoteViews;
    :sswitch_1b4
    const-string v0, "com.android.internal.appwidget.IAppWidgetService"

    #@1b6
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1b9
    .line 254
    invoke-virtual {p2}, Landroid/os/Parcel;->createIntArray()[I

    #@1bc
    move-result-object v1

    #@1bd
    .line 256
    .local v1, _arg0:[I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1c0
    move-result v2

    #@1c1
    .line 257
    .local v2, _arg1:I
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/appwidget/IAppWidgetService$Stub;->notifyAppWidgetViewDataChanged([II)V

    #@1c4
    .line 258
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1c7
    goto/16 :goto_9

    #@1c9
    .line 263
    .end local v1           #_arg0:[I
    .end local v2           #_arg1:I
    :sswitch_1c9
    const-string v0, "com.android.internal.appwidget.IAppWidgetService"

    #@1cb
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1ce
    .line 265
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1d1
    move-result v1

    #@1d2
    .line 266
    .local v1, _arg0:I
    invoke-virtual {p0, v1}, Lcom/android/internal/appwidget/IAppWidgetService$Stub;->getInstalledProviders(I)Ljava/util/List;

    #@1d5
    move-result-object v8

    #@1d6
    .line 267
    .local v8, _result:Ljava/util/List;,"Ljava/util/List<Landroid/appwidget/AppWidgetProviderInfo;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1d9
    .line 268
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@1dc
    goto/16 :goto_9

    #@1de
    .line 273
    .end local v1           #_arg0:I
    .end local v8           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/appwidget/AppWidgetProviderInfo;>;"
    :sswitch_1de
    const-string v10, "com.android.internal.appwidget.IAppWidgetService"

    #@1e0
    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1e3
    .line 275
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1e6
    move-result v1

    #@1e7
    .line 276
    .restart local v1       #_arg0:I
    invoke-virtual {p0, v1}, Lcom/android/internal/appwidget/IAppWidgetService$Stub;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    #@1ea
    move-result-object v7

    #@1eb
    .line 277
    .local v7, _result:Landroid/appwidget/AppWidgetProviderInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1ee
    .line 278
    if-eqz v7, :cond_1f8

    #@1f0
    .line 279
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    #@1f3
    .line 280
    invoke-virtual {v7, p3, v9}, Landroid/appwidget/AppWidgetProviderInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@1f6
    goto/16 :goto_9

    #@1f8
    .line 283
    :cond_1f8
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1fb
    goto/16 :goto_9

    #@1fd
    .line 289
    .end local v1           #_arg0:I
    .end local v7           #_result:Landroid/appwidget/AppWidgetProviderInfo;
    :sswitch_1fd
    const-string v10, "com.android.internal.appwidget.IAppWidgetService"

    #@1ff
    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@202
    .line 291
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@205
    move-result-object v1

    #@206
    .line 292
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/android/internal/appwidget/IAppWidgetService$Stub;->hasBindAppWidgetPermission(Ljava/lang/String;)Z

    #@209
    move-result v7

    #@20a
    .line 293
    .local v7, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@20d
    .line 294
    if-eqz v7, :cond_210

    #@20f
    move v0, v9

    #@210
    :cond_210
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@213
    goto/16 :goto_9

    #@215
    .line 299
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v7           #_result:Z
    :sswitch_215
    const-string v10, "com.android.internal.appwidget.IAppWidgetService"

    #@217
    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@21a
    .line 301
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@21d
    move-result-object v1

    #@21e
    .line 303
    .restart local v1       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@221
    move-result v10

    #@222
    if-eqz v10, :cond_22d

    #@224
    move v2, v9

    #@225
    .line 304
    .local v2, _arg1:Z
    :goto_225
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/appwidget/IAppWidgetService$Stub;->setBindAppWidgetPermission(Ljava/lang/String;Z)V

    #@228
    .line 305
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@22b
    goto/16 :goto_9

    #@22d
    .end local v2           #_arg1:Z
    :cond_22d
    move v2, v0

    #@22e
    .line 303
    goto :goto_225

    #@22f
    .line 310
    .end local v1           #_arg0:Ljava/lang/String;
    :sswitch_22f
    const-string v0, "com.android.internal.appwidget.IAppWidgetService"

    #@231
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@234
    .line 312
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@237
    move-result v1

    #@238
    .line 314
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@23b
    move-result v0

    #@23c
    if-eqz v0, :cond_25c

    #@23e
    .line 315
    sget-object v0, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@240
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@243
    move-result-object v2

    #@244
    check-cast v2, Landroid/content/ComponentName;

    #@246
    .line 321
    .local v2, _arg1:Landroid/content/ComponentName;
    :goto_246
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@249
    move-result v0

    #@24a
    if-eqz v0, :cond_25e

    #@24c
    .line 322
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@24e
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@251
    move-result-object v3

    #@252
    check-cast v3, Landroid/os/Bundle;

    #@254
    .line 327
    .local v3, _arg2:Landroid/os/Bundle;
    :goto_254
    invoke-virtual {p0, v1, v2, v3}, Lcom/android/internal/appwidget/IAppWidgetService$Stub;->bindAppWidgetId(ILandroid/content/ComponentName;Landroid/os/Bundle;)V

    #@257
    .line 328
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@25a
    goto/16 :goto_9

    #@25c
    .line 318
    .end local v2           #_arg1:Landroid/content/ComponentName;
    .end local v3           #_arg2:Landroid/os/Bundle;
    :cond_25c
    const/4 v2, 0x0

    #@25d
    .restart local v2       #_arg1:Landroid/content/ComponentName;
    goto :goto_246

    #@25e
    .line 325
    :cond_25e
    const/4 v3, 0x0

    #@25f
    .restart local v3       #_arg2:Landroid/os/Bundle;
    goto :goto_254

    #@260
    .line 333
    .end local v1           #_arg0:I
    .end local v2           #_arg1:Landroid/content/ComponentName;
    .end local v3           #_arg2:Landroid/os/Bundle;
    :sswitch_260
    const-string v10, "com.android.internal.appwidget.IAppWidgetService"

    #@262
    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@265
    .line 335
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@268
    move-result-object v1

    #@269
    .line 337
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@26c
    move-result v2

    #@26d
    .line 339
    .local v2, _arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@270
    move-result v10

    #@271
    if-eqz v10, :cond_298

    #@273
    .line 340
    sget-object v10, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@275
    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@278
    move-result-object v3

    #@279
    check-cast v3, Landroid/content/ComponentName;

    #@27b
    .line 346
    .local v3, _arg2:Landroid/content/ComponentName;
    :goto_27b
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@27e
    move-result v10

    #@27f
    if-eqz v10, :cond_29a

    #@281
    .line 347
    sget-object v10, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@283
    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@286
    move-result-object v6

    #@287
    check-cast v6, Landroid/os/Bundle;

    #@289
    .line 352
    .local v6, _arg3:Landroid/os/Bundle;
    :goto_289
    invoke-virtual {p0, v1, v2, v3, v6}, Lcom/android/internal/appwidget/IAppWidgetService$Stub;->bindAppWidgetIdIfAllowed(Ljava/lang/String;ILandroid/content/ComponentName;Landroid/os/Bundle;)Z

    #@28c
    move-result v7

    #@28d
    .line 353
    .restart local v7       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@290
    .line 354
    if-eqz v7, :cond_293

    #@292
    move v0, v9

    #@293
    :cond_293
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@296
    goto/16 :goto_9

    #@298
    .line 343
    .end local v3           #_arg2:Landroid/content/ComponentName;
    .end local v6           #_arg3:Landroid/os/Bundle;
    .end local v7           #_result:Z
    :cond_298
    const/4 v3, 0x0

    #@299
    .restart local v3       #_arg2:Landroid/content/ComponentName;
    goto :goto_27b

    #@29a
    .line 350
    :cond_29a
    const/4 v6, 0x0

    #@29b
    .restart local v6       #_arg3:Landroid/os/Bundle;
    goto :goto_289

    #@29c
    .line 359
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:I
    .end local v3           #_arg2:Landroid/content/ComponentName;
    .end local v6           #_arg3:Landroid/os/Bundle;
    :sswitch_29c
    const-string v0, "com.android.internal.appwidget.IAppWidgetService"

    #@29e
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2a1
    .line 361
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2a4
    move-result v1

    #@2a5
    .line 363
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2a8
    move-result v0

    #@2a9
    if-eqz v0, :cond_2c3

    #@2ab
    .line 364
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2ad
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2b0
    move-result-object v2

    #@2b1
    check-cast v2, Landroid/content/Intent;

    #@2b3
    .line 370
    .local v2, _arg1:Landroid/content/Intent;
    :goto_2b3
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@2b6
    move-result-object v3

    #@2b7
    .line 372
    .local v3, _arg2:Landroid/os/IBinder;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2ba
    move-result v6

    #@2bb
    .line 373
    .local v6, _arg3:I
    invoke-virtual {p0, v1, v2, v3, v6}, Lcom/android/internal/appwidget/IAppWidgetService$Stub;->bindRemoteViewsService(ILandroid/content/Intent;Landroid/os/IBinder;I)V

    #@2be
    .line 374
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2c1
    goto/16 :goto_9

    #@2c3
    .line 367
    .end local v2           #_arg1:Landroid/content/Intent;
    .end local v3           #_arg2:Landroid/os/IBinder;
    .end local v6           #_arg3:I
    :cond_2c3
    const/4 v2, 0x0

    #@2c4
    .restart local v2       #_arg1:Landroid/content/Intent;
    goto :goto_2b3

    #@2c5
    .line 379
    .end local v1           #_arg0:I
    .end local v2           #_arg1:Landroid/content/Intent;
    :sswitch_2c5
    const-string v0, "com.android.internal.appwidget.IAppWidgetService"

    #@2c7
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2ca
    .line 381
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2cd
    move-result v1

    #@2ce
    .line 383
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2d1
    move-result v0

    #@2d2
    if-eqz v0, :cond_2e8

    #@2d4
    .line 384
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2d6
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2d9
    move-result-object v2

    #@2da
    check-cast v2, Landroid/content/Intent;

    #@2dc
    .line 390
    .restart local v2       #_arg1:Landroid/content/Intent;
    :goto_2dc
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2df
    move-result v3

    #@2e0
    .line 391
    .local v3, _arg2:I
    invoke-virtual {p0, v1, v2, v3}, Lcom/android/internal/appwidget/IAppWidgetService$Stub;->unbindRemoteViewsService(ILandroid/content/Intent;I)V

    #@2e3
    .line 392
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2e6
    goto/16 :goto_9

    #@2e8
    .line 387
    .end local v2           #_arg1:Landroid/content/Intent;
    .end local v3           #_arg2:I
    :cond_2e8
    const/4 v2, 0x0

    #@2e9
    .restart local v2       #_arg1:Landroid/content/Intent;
    goto :goto_2dc

    #@2ea
    .line 397
    .end local v1           #_arg0:I
    .end local v2           #_arg1:Landroid/content/Intent;
    :sswitch_2ea
    const-string v0, "com.android.internal.appwidget.IAppWidgetService"

    #@2ec
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2ef
    .line 399
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2f2
    move-result v0

    #@2f3
    if-eqz v0, :cond_309

    #@2f5
    .line 400
    sget-object v0, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2f7
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2fa
    move-result-object v1

    #@2fb
    check-cast v1, Landroid/content/ComponentName;

    #@2fd
    .line 405
    .local v1, _arg0:Landroid/content/ComponentName;
    :goto_2fd
    invoke-virtual {p0, v1}, Lcom/android/internal/appwidget/IAppWidgetService$Stub;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    #@300
    move-result-object v7

    #@301
    .line 406
    .local v7, _result:[I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@304
    .line 407
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeIntArray([I)V

    #@307
    goto/16 :goto_9

    #@309
    .line 403
    .end local v1           #_arg0:Landroid/content/ComponentName;
    .end local v7           #_result:[I
    :cond_309
    const/4 v1, 0x0

    #@30a
    .restart local v1       #_arg0:Landroid/content/ComponentName;
    goto :goto_2fd

    #@30b
    .line 39
    nop

    #@30c
    :sswitch_data_30c
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_38
        0x3 -> :sswitch_65
        0x4 -> :sswitch_75
        0x5 -> :sswitch_89
        0x6 -> :sswitch_a2
        0x7 -> :sswitch_b3
        0x8 -> :sswitch_c4
        0x9 -> :sswitch_d1
        0xa -> :sswitch_f0
        0xb -> :sswitch_105
        0xc -> :sswitch_126
        0xd -> :sswitch_147
        0xe -> :sswitch_166
        0xf -> :sswitch_187
        0x10 -> :sswitch_1b4
        0x11 -> :sswitch_1c9
        0x12 -> :sswitch_1de
        0x13 -> :sswitch_1fd
        0x14 -> :sswitch_215
        0x15 -> :sswitch_22f
        0x16 -> :sswitch_260
        0x17 -> :sswitch_29c
        0x18 -> :sswitch_2c5
        0x19 -> :sswitch_2ea
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
