.class public Lcom/android/internal/util/AsyncChannel;
.super Ljava/lang/Object;
.source "AsyncChannel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/util/AsyncChannel$1;,
        Lcom/android/internal/util/AsyncChannel$AsyncChannelConnection;,
        Lcom/android/internal/util/AsyncChannel$SyncMessenger;
    }
.end annotation


# static fields
.field private static final BASE:I = 0x11000

.field public static final CMD_CHANNEL_DISCONNECT:I = 0x11003

.field public static final CMD_CHANNEL_DISCONNECTED:I = 0x11004

.field public static final CMD_CHANNEL_FULLY_CONNECTED:I = 0x11002

.field public static final CMD_CHANNEL_FULL_CONNECTION:I = 0x11001

.field public static final CMD_CHANNEL_HALF_CONNECTED:I = 0x11000

.field private static final CMD_TO_STRING_COUNT:I = 0x5

.field private static final DBG:Z = false

.field public static final STATUS_BINDING_UNSUCCESSFUL:I = 0x1

.field public static final STATUS_FULL_CONNECTION_REFUSED_ALREADY_CONNECTED:I = 0x3

.field public static final STATUS_SEND_UNSUCCESSFUL:I = 0x2

.field public static final STATUS_SUCCESSFUL:I = 0x0

.field private static final TAG:Ljava/lang/String; = "AsyncChannel"

.field private static sCmdToString:[Ljava/lang/String;


# instance fields
.field private mConnection:Lcom/android/internal/util/AsyncChannel$AsyncChannelConnection;

.field private mDstMessenger:Landroid/os/Messenger;

.field private mSrcContext:Landroid/content/Context;

.field private mSrcHandler:Landroid/os/Handler;

.field private mSrcMessenger:Landroid/os/Messenger;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 154
    const/4 v0, 0x5

    #@1
    new-array v0, v0, [Ljava/lang/String;

    #@3
    sput-object v0, Lcom/android/internal/util/AsyncChannel;->sCmdToString:[Ljava/lang/String;

    #@5
    .line 156
    sget-object v0, Lcom/android/internal/util/AsyncChannel;->sCmdToString:[Ljava/lang/String;

    #@7
    const/4 v1, 0x0

    #@8
    const-string v2, "CMD_CHANNEL_HALF_CONNECTED"

    #@a
    aput-object v2, v0, v1

    #@c
    .line 157
    sget-object v0, Lcom/android/internal/util/AsyncChannel;->sCmdToString:[Ljava/lang/String;

    #@e
    const/4 v1, 0x1

    #@f
    const-string v2, "CMD_CHANNEL_FULL_CONNECTION"

    #@11
    aput-object v2, v0, v1

    #@13
    .line 158
    sget-object v0, Lcom/android/internal/util/AsyncChannel;->sCmdToString:[Ljava/lang/String;

    #@15
    const/4 v1, 0x2

    #@16
    const-string v2, "CMD_CHANNEL_FULLY_CONNECTED"

    #@18
    aput-object v2, v0, v1

    #@1a
    .line 159
    sget-object v0, Lcom/android/internal/util/AsyncChannel;->sCmdToString:[Ljava/lang/String;

    #@1c
    const/4 v1, 0x3

    #@1d
    const-string v2, "CMD_CHANNEL_DISCONNECT"

    #@1f
    aput-object v2, v0, v1

    #@21
    .line 160
    sget-object v0, Lcom/android/internal/util/AsyncChannel;->sCmdToString:[Ljava/lang/String;

    #@23
    const/4 v1, 0x4

    #@24
    const-string v2, "CMD_CHANNEL_DISCONNECTED"

    #@26
    aput-object v2, v0, v1

    #@28
    .line 161
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 201
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 202
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/util/AsyncChannel;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/android/internal/util/AsyncChannel;->replyHalfConnected(I)V

    #@3
    return-void
.end method

.method static synthetic access$502(Lcom/android/internal/util/AsyncChannel;Landroid/os/Messenger;)Landroid/os/Messenger;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 86
    iput-object p1, p0, Lcom/android/internal/util/AsyncChannel;->mDstMessenger:Landroid/os/Messenger;

    #@2
    return-object p1
.end method

.method static synthetic access$600(Lcom/android/internal/util/AsyncChannel;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/android/internal/util/AsyncChannel;->replyDisconnected(I)V

    #@3
    return-void
.end method

.method protected static cmdToString(I)Ljava/lang/String;
    .registers 2
    .parameter "cmd"

    #@0
    .prologue
    .line 163
    const v0, 0x11000

    #@3
    sub-int/2addr p0, v0

    #@4
    .line 164
    if-ltz p0, :cond_10

    #@6
    sget-object v0, Lcom/android/internal/util/AsyncChannel;->sCmdToString:[Ljava/lang/String;

    #@8
    array-length v0, v0

    #@9
    if-ge p0, v0, :cond_10

    #@b
    .line 165
    sget-object v0, Lcom/android/internal/util/AsyncChannel;->sCmdToString:[Ljava/lang/String;

    #@d
    aget-object v0, v0, p0

    #@f
    .line 167
    :goto_f
    return-object v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method private static log(Ljava/lang/String;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 878
    const-string v0, "AsyncChannel"

    #@2
    invoke-static {v0, p0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 879
    return-void
.end method

.method private replyDisconnected(I)V
    .registers 5
    .parameter "status"

    #@0
    .prologue
    .line 845
    iget-object v1, p0, Lcom/android/internal/util/AsyncChannel;->mSrcHandler:Landroid/os/Handler;

    #@2
    const v2, 0x11004

    #@5
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    .line 846
    .local v0, msg:Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    #@b
    .line 847
    iput-object p0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@d
    .line 848
    iget-object v1, p0, Lcom/android/internal/util/AsyncChannel;->mDstMessenger:Landroid/os/Messenger;

    #@f
    iput-object v1, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@11
    .line 849
    iget-object v1, p0, Lcom/android/internal/util/AsyncChannel;->mSrcHandler:Landroid/os/Handler;

    #@13
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@16
    .line 850
    return-void
.end method

.method private replyHalfConnected(I)V
    .registers 5
    .parameter "status"

    #@0
    .prologue
    .line 831
    iget-object v1, p0, Lcom/android/internal/util/AsyncChannel;->mSrcHandler:Landroid/os/Handler;

    #@2
    const v2, 0x11000

    #@5
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    .line 832
    .local v0, msg:Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    #@b
    .line 833
    iput-object p0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@d
    .line 834
    iget-object v1, p0, Lcom/android/internal/util/AsyncChannel;->mDstMessenger:Landroid/os/Messenger;

    #@f
    iput-object v1, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@11
    .line 835
    iget-object v1, p0, Lcom/android/internal/util/AsyncChannel;->mSrcHandler:Landroid/os/Handler;

    #@13
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@16
    .line 836
    return-void
.end method


# virtual methods
.method public connect(Landroid/content/Context;Landroid/os/Handler;Landroid/os/Handler;)V
    .registers 5
    .parameter "srcContext"
    .parameter "srcHandler"
    .parameter "dstHandler"

    #@0
    .prologue
    .line 412
    new-instance v0, Landroid/os/Messenger;

    #@2
    invoke-direct {v0, p3}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    #@5
    invoke-virtual {p0, p1, p2, v0}, Lcom/android/internal/util/AsyncChannel;->connect(Landroid/content/Context;Landroid/os/Handler;Landroid/os/Messenger;)V

    #@8
    .line 413
    return-void
.end method

.method public connect(Landroid/content/Context;Landroid/os/Handler;Landroid/os/Messenger;)V
    .registers 5
    .parameter "srcContext"
    .parameter "srcHandler"
    .parameter "dstMessenger"

    #@0
    .prologue
    .line 371
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/internal/util/AsyncChannel;->connected(Landroid/content/Context;Landroid/os/Handler;Landroid/os/Messenger;)V

    #@3
    .line 374
    const/4 v0, 0x0

    #@4
    invoke-direct {p0, v0}, Lcom/android/internal/util/AsyncChannel;->replyHalfConnected(I)V

    #@7
    .line 377
    return-void
.end method

.method public connect(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/Class;)V
    .registers 6
    .parameter "srcContext"
    .parameter "srcHandler"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/os/Handler;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 353
    .local p3, klass:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    invoke-virtual {p3}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Ljava/lang/Package;->getName()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {p3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/android/internal/util/AsyncChannel;->connect(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V

    #@f
    .line 354
    return-void
.end method

.method public connect(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V
    .registers 11
    .parameter "srcContext"
    .parameter "srcHandler"
    .parameter "dstPackageName"
    .parameter "dstClassName"

    #@0
    .prologue
    .line 335
    new-instance v0, Lcom/android/internal/util/AsyncChannel$1ConnectAsync;

    #@2
    move-object v1, p0

    #@3
    move-object v2, p1

    #@4
    move-object v3, p2

    #@5
    move-object v4, p3

    #@6
    move-object v5, p4

    #@7
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/util/AsyncChannel$1ConnectAsync;-><init>(Lcom/android/internal/util/AsyncChannel;Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 336
    .local v0, ca:Lcom/android/internal/util/AsyncChannel$1ConnectAsync;
    new-instance v1, Ljava/lang/Thread;

    #@c
    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@f
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    #@12
    .line 339
    return-void
.end method

.method public connect(Lcom/android/internal/util/AsyncService;Landroid/os/Messenger;)V
    .registers 4
    .parameter "srcAsyncService"
    .parameter "dstMessenger"

    #@0
    .prologue
    .line 426
    invoke-virtual {p1}, Lcom/android/internal/util/AsyncService;->getHandler()Landroid/os/Handler;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, p1, v0, p2}, Lcom/android/internal/util/AsyncChannel;->connect(Landroid/content/Context;Landroid/os/Handler;Landroid/os/Messenger;)V

    #@7
    .line 427
    return-void
.end method

.method public connectSrcHandlerToPackageSync(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)I
    .registers 9
    .parameter "srcContext"
    .parameter "srcHandler"
    .parameter "dstPackageName"
    .parameter "dstClassName"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 220
    new-instance v3, Lcom/android/internal/util/AsyncChannel$AsyncChannelConnection;

    #@3
    invoke-direct {v3, p0}, Lcom/android/internal/util/AsyncChannel$AsyncChannelConnection;-><init>(Lcom/android/internal/util/AsyncChannel;)V

    #@6
    iput-object v3, p0, Lcom/android/internal/util/AsyncChannel;->mConnection:Lcom/android/internal/util/AsyncChannel$AsyncChannelConnection;

    #@8
    .line 223
    iput-object p1, p0, Lcom/android/internal/util/AsyncChannel;->mSrcContext:Landroid/content/Context;

    #@a
    .line 224
    iput-object p2, p0, Lcom/android/internal/util/AsyncChannel;->mSrcHandler:Landroid/os/Handler;

    #@c
    .line 225
    new-instance v3, Landroid/os/Messenger;

    #@e
    invoke-direct {v3, p2}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    #@11
    iput-object v3, p0, Lcom/android/internal/util/AsyncChannel;->mSrcMessenger:Landroid/os/Messenger;

    #@13
    .line 232
    const/4 v3, 0x0

    #@14
    iput-object v3, p0, Lcom/android/internal/util/AsyncChannel;->mDstMessenger:Landroid/os/Messenger;

    #@16
    .line 235
    new-instance v0, Landroid/content/Intent;

    #@18
    const-string v3, "android.intent.action.MAIN"

    #@1a
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1d
    .line 236
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {v0, p3, p4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@20
    .line 237
    iget-object v3, p0, Lcom/android/internal/util/AsyncChannel;->mConnection:Lcom/android/internal/util/AsyncChannel$AsyncChannelConnection;

    #@22
    invoke-virtual {p1, v0, v3, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    #@25
    move-result v1

    #@26
    .line 239
    .local v1, result:Z
    if-eqz v1, :cond_29

    #@28
    const/4 v2, 0x0

    #@29
    :cond_29
    return v2
.end method

.method public connectSync(Landroid/content/Context;Landroid/os/Handler;Landroid/os/Handler;)I
    .registers 5
    .parameter "srcContext"
    .parameter "srcHandler"
    .parameter "dstHandler"

    #@0
    .prologue
    .line 273
    new-instance v0, Landroid/os/Messenger;

    #@2
    invoke-direct {v0, p3}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    #@5
    invoke-virtual {p0, p1, p2, v0}, Lcom/android/internal/util/AsyncChannel;->connectSync(Landroid/content/Context;Landroid/os/Handler;Landroid/os/Messenger;)I

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public connectSync(Landroid/content/Context;Landroid/os/Handler;Landroid/os/Messenger;)I
    .registers 5
    .parameter "srcContext"
    .parameter "srcHandler"
    .parameter "dstMessenger"

    #@0
    .prologue
    .line 256
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/internal/util/AsyncChannel;->connected(Landroid/content/Context;Landroid/os/Handler;Landroid/os/Messenger;)V

    #@3
    .line 259
    const/4 v0, 0x0

    #@4
    return v0
.end method

.method public connected(Landroid/content/Context;Landroid/os/Handler;Landroid/os/Messenger;)V
    .registers 6
    .parameter "srcContext"
    .parameter "srcHandler"
    .parameter "dstMessenger"

    #@0
    .prologue
    .line 393
    iput-object p1, p0, Lcom/android/internal/util/AsyncChannel;->mSrcContext:Landroid/content/Context;

    #@2
    .line 394
    iput-object p2, p0, Lcom/android/internal/util/AsyncChannel;->mSrcHandler:Landroid/os/Handler;

    #@4
    .line 395
    new-instance v0, Landroid/os/Messenger;

    #@6
    iget-object v1, p0, Lcom/android/internal/util/AsyncChannel;->mSrcHandler:Landroid/os/Handler;

    #@8
    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    #@b
    iput-object v0, p0, Lcom/android/internal/util/AsyncChannel;->mSrcMessenger:Landroid/os/Messenger;

    #@d
    .line 398
    iput-object p3, p0, Lcom/android/internal/util/AsyncChannel;->mDstMessenger:Landroid/os/Messenger;

    #@f
    .line 401
    return-void
.end method

.method public disconnect()V
    .registers 4

    #@0
    .prologue
    .line 444
    iget-object v1, p0, Lcom/android/internal/util/AsyncChannel;->mConnection:Lcom/android/internal/util/AsyncChannel$AsyncChannelConnection;

    #@2
    if-eqz v1, :cond_f

    #@4
    iget-object v1, p0, Lcom/android/internal/util/AsyncChannel;->mSrcContext:Landroid/content/Context;

    #@6
    if-eqz v1, :cond_f

    #@8
    .line 445
    iget-object v1, p0, Lcom/android/internal/util/AsyncChannel;->mSrcContext:Landroid/content/Context;

    #@a
    iget-object v2, p0, Lcom/android/internal/util/AsyncChannel;->mConnection:Lcom/android/internal/util/AsyncChannel$AsyncChannelConnection;

    #@c
    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    #@f
    .line 450
    :cond_f
    :try_start_f
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@12
    move-result-object v0

    #@13
    .line 451
    .local v0, msg:Landroid/os/Message;
    const v1, 0x11004

    #@16
    iput v1, v0, Landroid/os/Message;->what:I

    #@18
    .line 452
    iget-object v1, p0, Lcom/android/internal/util/AsyncChannel;->mSrcMessenger:Landroid/os/Messenger;

    #@1a
    iput-object v1, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@1c
    .line 453
    iget-object v1, p0, Lcom/android/internal/util/AsyncChannel;->mDstMessenger:Landroid/os/Messenger;

    #@1e
    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_21
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_21} :catch_2a

    #@21
    .line 457
    .end local v0           #msg:Landroid/os/Message;
    :goto_21
    iget-object v1, p0, Lcom/android/internal/util/AsyncChannel;->mSrcHandler:Landroid/os/Handler;

    #@23
    if-eqz v1, :cond_29

    #@25
    .line 458
    const/4 v1, 0x0

    #@26
    invoke-direct {p0, v1}, Lcom/android/internal/util/AsyncChannel;->replyDisconnected(I)V

    #@29
    .line 460
    :cond_29
    return-void

    #@2a
    .line 454
    :catch_2a
    move-exception v1

    #@2b
    goto :goto_21
.end method

.method public disconnected()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 433
    iput-object v0, p0, Lcom/android/internal/util/AsyncChannel;->mSrcContext:Landroid/content/Context;

    #@3
    .line 434
    iput-object v0, p0, Lcom/android/internal/util/AsyncChannel;->mSrcHandler:Landroid/os/Handler;

    #@5
    .line 435
    iput-object v0, p0, Lcom/android/internal/util/AsyncChannel;->mSrcMessenger:Landroid/os/Messenger;

    #@7
    .line 436
    iput-object v0, p0, Lcom/android/internal/util/AsyncChannel;->mDstMessenger:Landroid/os/Messenger;

    #@9
    .line 437
    iput-object v0, p0, Lcom/android/internal/util/AsyncChannel;->mConnection:Lcom/android/internal/util/AsyncChannel$AsyncChannelConnection;

    #@b
    .line 438
    return-void
.end method

.method public fullyConnectSync(Landroid/content/Context;Landroid/os/Handler;Landroid/os/Handler;)I
    .registers 7
    .parameter "srcContext"
    .parameter "srcHandler"
    .parameter "dstHandler"

    #@0
    .prologue
    .line 287
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/internal/util/AsyncChannel;->connectSync(Landroid/content/Context;Landroid/os/Handler;Landroid/os/Handler;)I

    #@3
    move-result v1

    #@4
    .line 288
    .local v1, status:I
    if-nez v1, :cond_f

    #@6
    .line 289
    const v2, 0x11001

    #@9
    invoke-virtual {p0, v2}, Lcom/android/internal/util/AsyncChannel;->sendMessageSynchronously(I)Landroid/os/Message;

    #@c
    move-result-object v0

    #@d
    .line 290
    .local v0, response:Landroid/os/Message;
    iget v1, v0, Landroid/os/Message;->arg1:I

    #@f
    .line 292
    .end local v0           #response:Landroid/os/Message;
    :cond_f
    return v1
.end method

.method public replyToMessage(Landroid/os/Message;I)V
    .registers 4
    .parameter "srcMsg"
    .parameter "what"

    #@0
    .prologue
    .line 568
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 569
    .local v0, msg:Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->what:I

    #@6
    .line 570
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;Landroid/os/Message;)V

    #@9
    .line 571
    return-void
.end method

.method public replyToMessage(Landroid/os/Message;II)V
    .registers 5
    .parameter "srcMsg"
    .parameter "what"
    .parameter "arg1"

    #@0
    .prologue
    .line 581
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 582
    .local v0, msg:Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->what:I

    #@6
    .line 583
    iput p3, v0, Landroid/os/Message;->arg1:I

    #@8
    .line 584
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;Landroid/os/Message;)V

    #@b
    .line 585
    return-void
.end method

.method public replyToMessage(Landroid/os/Message;III)V
    .registers 6
    .parameter "srcMsg"
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"

    #@0
    .prologue
    .line 596
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 597
    .local v0, msg:Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->what:I

    #@6
    .line 598
    iput p3, v0, Landroid/os/Message;->arg1:I

    #@8
    .line 599
    iput p4, v0, Landroid/os/Message;->arg2:I

    #@a
    .line 600
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;Landroid/os/Message;)V

    #@d
    .line 601
    return-void
.end method

.method public replyToMessage(Landroid/os/Message;IIILjava/lang/Object;)V
    .registers 7
    .parameter "srcMsg"
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"
    .parameter "obj"

    #@0
    .prologue
    .line 613
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 614
    .local v0, msg:Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->what:I

    #@6
    .line 615
    iput p3, v0, Landroid/os/Message;->arg1:I

    #@8
    .line 616
    iput p4, v0, Landroid/os/Message;->arg2:I

    #@a
    .line 617
    iput-object p5, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@c
    .line 618
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;Landroid/os/Message;)V

    #@f
    .line 619
    return-void
.end method

.method public replyToMessage(Landroid/os/Message;ILjava/lang/Object;)V
    .registers 5
    .parameter "srcMsg"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 629
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 630
    .local v0, msg:Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->what:I

    #@6
    .line 631
    iput-object p3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8
    .line 632
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/util/AsyncChannel;->replyToMessage(Landroid/os/Message;Landroid/os/Message;)V

    #@b
    .line 633
    return-void
.end method

.method public replyToMessage(Landroid/os/Message;Landroid/os/Message;)V
    .registers 6
    .parameter "srcMsg"
    .parameter "dstMsg"

    #@0
    .prologue
    .line 553
    :try_start_0
    iget-object v1, p0, Lcom/android/internal/util/AsyncChannel;->mSrcMessenger:Landroid/os/Messenger;

    #@2
    iput-object v1, p2, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@4
    .line 554
    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@6
    invoke-virtual {v1, p2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_a

    #@9
    .line 559
    :goto_9
    return-void

    #@a
    .line 555
    :catch_a
    move-exception v0

    #@b
    .line 556
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "TODO: handle replyToMessage RemoteException"

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-static {v1}, Lcom/android/internal/util/AsyncChannel;->log(Ljava/lang/String;)V

    #@21
    .line 557
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@24
    goto :goto_9
.end method

.method public sendMessage(I)V
    .registers 3
    .parameter "what"

    #@0
    .prologue
    .line 482
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 483
    .local v0, msg:Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    #@6
    .line 484
    invoke-virtual {p0, v0}, Lcom/android/internal/util/AsyncChannel;->sendMessage(Landroid/os/Message;)V

    #@9
    .line 485
    return-void
.end method

.method public sendMessage(II)V
    .registers 4
    .parameter "what"
    .parameter "arg1"

    #@0
    .prologue
    .line 494
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 495
    .local v0, msg:Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    #@6
    .line 496
    iput p2, v0, Landroid/os/Message;->arg1:I

    #@8
    .line 497
    invoke-virtual {p0, v0}, Lcom/android/internal/util/AsyncChannel;->sendMessage(Landroid/os/Message;)V

    #@b
    .line 498
    return-void
.end method

.method public sendMessage(III)V
    .registers 5
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"

    #@0
    .prologue
    .line 508
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 509
    .local v0, msg:Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    #@6
    .line 510
    iput p2, v0, Landroid/os/Message;->arg1:I

    #@8
    .line 511
    iput p3, v0, Landroid/os/Message;->arg2:I

    #@a
    .line 512
    invoke-virtual {p0, v0}, Lcom/android/internal/util/AsyncChannel;->sendMessage(Landroid/os/Message;)V

    #@d
    .line 513
    return-void
.end method

.method public sendMessage(IIILjava/lang/Object;)V
    .registers 6
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"
    .parameter "obj"

    #@0
    .prologue
    .line 524
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 525
    .local v0, msg:Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    #@6
    .line 526
    iput p2, v0, Landroid/os/Message;->arg1:I

    #@8
    .line 527
    iput p3, v0, Landroid/os/Message;->arg2:I

    #@a
    .line 528
    iput-object p4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@c
    .line 529
    invoke-virtual {p0, v0}, Lcom/android/internal/util/AsyncChannel;->sendMessage(Landroid/os/Message;)V

    #@f
    .line 530
    return-void
.end method

.method public sendMessage(ILjava/lang/Object;)V
    .registers 4
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 539
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 540
    .local v0, msg:Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    #@6
    .line 541
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8
    .line 542
    invoke-virtual {p0, v0}, Lcom/android/internal/util/AsyncChannel;->sendMessage(Landroid/os/Message;)V

    #@b
    .line 543
    return-void
.end method

.method public sendMessage(Landroid/os/Message;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 468
    iget-object v1, p0, Lcom/android/internal/util/AsyncChannel;->mSrcMessenger:Landroid/os/Messenger;

    #@2
    iput-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@4
    .line 470
    :try_start_4
    iget-object v1, p0, Lcom/android/internal/util/AsyncChannel;->mDstMessenger:Landroid/os/Messenger;

    #@6
    invoke-virtual {v1, p1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 474
    :goto_9
    return-void

    #@a
    .line 471
    :catch_a
    move-exception v0

    #@b
    .line 472
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x2

    #@c
    invoke-direct {p0, v1}, Lcom/android/internal/util/AsyncChannel;->replyDisconnected(I)V

    #@f
    goto :goto_9
.end method

.method public sendMessageSynchronously(I)Landroid/os/Message;
    .registers 4
    .parameter "what"

    #@0
    .prologue
    .line 653
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 654
    .local v0, msg:Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    #@6
    .line 655
    invoke-virtual {p0, v0}, Lcom/android/internal/util/AsyncChannel;->sendMessageSynchronously(Landroid/os/Message;)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    .line 656
    .local v1, resultMsg:Landroid/os/Message;
    return-object v1
.end method

.method public sendMessageSynchronously(II)Landroid/os/Message;
    .registers 5
    .parameter "what"
    .parameter "arg1"

    #@0
    .prologue
    .line 667
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 668
    .local v0, msg:Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    #@6
    .line 669
    iput p2, v0, Landroid/os/Message;->arg1:I

    #@8
    .line 670
    invoke-virtual {p0, v0}, Lcom/android/internal/util/AsyncChannel;->sendMessageSynchronously(Landroid/os/Message;)Landroid/os/Message;

    #@b
    move-result-object v1

    #@c
    .line 671
    .local v1, resultMsg:Landroid/os/Message;
    return-object v1
.end method

.method public sendMessageSynchronously(III)Landroid/os/Message;
    .registers 6
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"

    #@0
    .prologue
    .line 683
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 684
    .local v0, msg:Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    #@6
    .line 685
    iput p2, v0, Landroid/os/Message;->arg1:I

    #@8
    .line 686
    iput p3, v0, Landroid/os/Message;->arg2:I

    #@a
    .line 687
    invoke-virtual {p0, v0}, Lcom/android/internal/util/AsyncChannel;->sendMessageSynchronously(Landroid/os/Message;)Landroid/os/Message;

    #@d
    move-result-object v1

    #@e
    .line 688
    .local v1, resultMsg:Landroid/os/Message;
    return-object v1
.end method

.method public sendMessageSynchronously(IIILjava/lang/Object;)Landroid/os/Message;
    .registers 7
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"
    .parameter "obj"

    #@0
    .prologue
    .line 701
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 702
    .local v0, msg:Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    #@6
    .line 703
    iput p2, v0, Landroid/os/Message;->arg1:I

    #@8
    .line 704
    iput p3, v0, Landroid/os/Message;->arg2:I

    #@a
    .line 705
    iput-object p4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@c
    .line 706
    invoke-virtual {p0, v0}, Lcom/android/internal/util/AsyncChannel;->sendMessageSynchronously(Landroid/os/Message;)Landroid/os/Message;

    #@f
    move-result-object v1

    #@10
    .line 707
    .local v1, resultMsg:Landroid/os/Message;
    return-object v1
.end method

.method public sendMessageSynchronously(ILjava/lang/Object;)Landroid/os/Message;
    .registers 5
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 718
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 719
    .local v0, msg:Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    #@6
    .line 720
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8
    .line 721
    invoke-virtual {p0, v0}, Lcom/android/internal/util/AsyncChannel;->sendMessageSynchronously(Landroid/os/Message;)Landroid/os/Message;

    #@b
    move-result-object v1

    #@c
    .line 722
    .local v1, resultMsg:Landroid/os/Message;
    return-object v1
.end method

.method public sendMessageSynchronously(Landroid/os/Message;)Landroid/os/Message;
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 642
    iget-object v1, p0, Lcom/android/internal/util/AsyncChannel;->mDstMessenger:Landroid/os/Messenger;

    #@2
    invoke-static {v1, p1}, Lcom/android/internal/util/AsyncChannel$SyncMessenger;->access$100(Landroid/os/Messenger;Landroid/os/Message;)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    .line 643
    .local v0, resultMsg:Landroid/os/Message;
    return-object v0
.end method
