.class public Lcom/android/internal/util/TypedProperties;
.super Ljava/util/HashMap;
.source "TypedProperties.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/util/TypedProperties$TypeException;,
        Lcom/android/internal/util/TypedProperties$ParseException;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field static final NULL_STRING:Ljava/lang/String; = null

.field public static final STRING_NOT_SET:I = -0x1

.field public static final STRING_NULL:I = 0x0

.field public static final STRING_SET:I = 0x1

.field public static final STRING_TYPE_MISMATCH:I = -0x2

.field static final TYPE_BOOLEAN:I = 0x5a

.field static final TYPE_BYTE:I = 0x149

.field static final TYPE_DOUBLE:I = 0x846

.field static final TYPE_ERROR:I = -0x1

.field static final TYPE_FLOAT:I = 0x446

.field static final TYPE_INT:I = 0x449

.field static final TYPE_LONG:I = 0x849

.field static final TYPE_SHORT:I = 0x249

.field static final TYPE_STRING:I = 0x734c

.field static final TYPE_UNSET:I = 0x78


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 88
    new-instance v0, Ljava/lang/String;

    #@2
    const-string v1, "<TypedProperties:NULL_STRING>"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@7
    sput-object v0, Lcom/android/internal/util/TypedProperties;->NULL_STRING:Ljava/lang/String;

    #@9
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 343
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    #@3
    .line 344
    return-void
.end method

.method static initTokenizer(Ljava/io/Reader;)Ljava/io/StreamTokenizer;
    .registers 9
    .parameter "r"

    #@0
    .prologue
    const/16 v7, 0x20

    #@2
    const/16 v6, 0xd

    #@4
    const/16 v5, 0xa

    #@6
    const/16 v4, 0x9

    #@8
    const/4 v3, 0x1

    #@9
    .line 39
    new-instance v0, Ljava/io/StreamTokenizer;

    #@b
    invoke-direct {v0, p0}, Ljava/io/StreamTokenizer;-><init>(Ljava/io/Reader;)V

    #@e
    .line 42
    .local v0, st:Ljava/io/StreamTokenizer;
    invoke-virtual {v0}, Ljava/io/StreamTokenizer;->resetSyntax()V

    #@11
    .line 50
    const/16 v1, 0x30

    #@13
    const/16 v2, 0x39

    #@15
    invoke-virtual {v0, v1, v2}, Ljava/io/StreamTokenizer;->wordChars(II)V

    #@18
    .line 51
    const/16 v1, 0x41

    #@1a
    const/16 v2, 0x5a

    #@1c
    invoke-virtual {v0, v1, v2}, Ljava/io/StreamTokenizer;->wordChars(II)V

    #@1f
    .line 52
    const/16 v1, 0x61

    #@21
    const/16 v2, 0x7a

    #@23
    invoke-virtual {v0, v1, v2}, Ljava/io/StreamTokenizer;->wordChars(II)V

    #@26
    .line 53
    const/16 v1, 0x5f

    #@28
    const/16 v2, 0x5f

    #@2a
    invoke-virtual {v0, v1, v2}, Ljava/io/StreamTokenizer;->wordChars(II)V

    #@2d
    .line 54
    const/16 v1, 0x24

    #@2f
    const/16 v2, 0x24

    #@31
    invoke-virtual {v0, v1, v2}, Ljava/io/StreamTokenizer;->wordChars(II)V

    #@34
    .line 55
    const/16 v1, 0x2e

    #@36
    const/16 v2, 0x2e

    #@38
    invoke-virtual {v0, v1, v2}, Ljava/io/StreamTokenizer;->wordChars(II)V

    #@3b
    .line 56
    const/16 v1, 0x2d

    #@3d
    const/16 v2, 0x2d

    #@3f
    invoke-virtual {v0, v1, v2}, Ljava/io/StreamTokenizer;->wordChars(II)V

    #@42
    .line 57
    const/16 v1, 0x2b

    #@44
    const/16 v2, 0x2b

    #@46
    invoke-virtual {v0, v1, v2}, Ljava/io/StreamTokenizer;->wordChars(II)V

    #@49
    .line 60
    const/16 v1, 0x3d

    #@4b
    invoke-virtual {v0, v1}, Ljava/io/StreamTokenizer;->ordinaryChar(I)V

    #@4e
    .line 63
    invoke-virtual {v0, v7, v7}, Ljava/io/StreamTokenizer;->whitespaceChars(II)V

    #@51
    .line 64
    invoke-virtual {v0, v4, v4}, Ljava/io/StreamTokenizer;->whitespaceChars(II)V

    #@54
    .line 65
    invoke-virtual {v0, v5, v5}, Ljava/io/StreamTokenizer;->whitespaceChars(II)V

    #@57
    .line 66
    invoke-virtual {v0, v6, v6}, Ljava/io/StreamTokenizer;->whitespaceChars(II)V

    #@5a
    .line 67
    const/16 v1, 0x22

    #@5c
    invoke-virtual {v0, v1}, Ljava/io/StreamTokenizer;->quoteChar(I)V

    #@5f
    .line 70
    invoke-virtual {v0, v3}, Ljava/io/StreamTokenizer;->slashStarComments(Z)V

    #@62
    .line 71
    invoke-virtual {v0, v3}, Ljava/io/StreamTokenizer;->slashSlashComments(Z)V

    #@65
    .line 73
    return-object v0
.end method

.method static interpretType(Ljava/lang/String;)I
    .registers 2
    .parameter "typeName"

    #@0
    .prologue
    .line 111
    const-string/jumbo v0, "unset"

    #@3
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_c

    #@9
    .line 112
    const/16 v0, 0x78

    #@b
    .line 130
    :goto_b
    return v0

    #@c
    .line 113
    :cond_c
    const-string v0, "boolean"

    #@e
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_17

    #@14
    .line 114
    const/16 v0, 0x5a

    #@16
    goto :goto_b

    #@17
    .line 115
    :cond_17
    const-string v0, "byte"

    #@19
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v0

    #@1d
    if-eqz v0, :cond_22

    #@1f
    .line 116
    const/16 v0, 0x149

    #@21
    goto :goto_b

    #@22
    .line 117
    :cond_22
    const-string/jumbo v0, "short"

    #@25
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v0

    #@29
    if-eqz v0, :cond_2e

    #@2b
    .line 118
    const/16 v0, 0x249

    #@2d
    goto :goto_b

    #@2e
    .line 119
    :cond_2e
    const-string v0, "int"

    #@30
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v0

    #@34
    if-eqz v0, :cond_39

    #@36
    .line 120
    const/16 v0, 0x449

    #@38
    goto :goto_b

    #@39
    .line 121
    :cond_39
    const-string/jumbo v0, "long"

    #@3c
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3f
    move-result v0

    #@40
    if-eqz v0, :cond_45

    #@42
    .line 122
    const/16 v0, 0x849

    #@44
    goto :goto_b

    #@45
    .line 123
    :cond_45
    const-string v0, "float"

    #@47
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4a
    move-result v0

    #@4b
    if-eqz v0, :cond_50

    #@4d
    .line 124
    const/16 v0, 0x446

    #@4f
    goto :goto_b

    #@50
    .line 125
    :cond_50
    const-string v0, "double"

    #@52
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@55
    move-result v0

    #@56
    if-eqz v0, :cond_5b

    #@58
    .line 126
    const/16 v0, 0x846

    #@5a
    goto :goto_b

    #@5b
    .line 127
    :cond_5b
    const-string v0, "String"

    #@5d
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@60
    move-result v0

    #@61
    if-eqz v0, :cond_66

    #@63
    .line 128
    const/16 v0, 0x734c

    #@65
    goto :goto_b

    #@66
    .line 130
    :cond_66
    const/4 v0, -0x1

    #@67
    goto :goto_b
.end method

.method static parse(Ljava/io/Reader;Ljava/util/Map;)V
    .registers 16
    .parameter "r"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/Reader;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/TypedProperties$ParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .local p1, map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v13, 0x0

    #@1
    const/16 v12, 0x78

    #@3
    const/4 v11, -0x1

    #@4
    const/4 v10, -0x3

    #@5
    .line 142
    invoke-static {p0}, Lcom/android/internal/util/TypedProperties;->initTokenizer(Ljava/io/Reader;)Ljava/io/StreamTokenizer;

    #@8
    move-result-object v4

    #@9
    .line 147
    .local v4, st:Ljava/io/StreamTokenizer;
    const-string v0, "[a-zA-Z_$][0-9a-zA-Z_$]*"

    #@b
    .line 148
    .local v0, identifierPattern:Ljava/lang/String;
    const-string v8, "([a-zA-Z_$][0-9a-zA-Z_$]*\\.)*[a-zA-Z_$][0-9a-zA-Z_$]*"

    #@d
    invoke-static {v8}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@10
    move-result-object v3

    #@11
    .line 156
    .local v3, propertyNamePattern:Ljava/util/regex/Pattern;
    :cond_11
    invoke-virtual {v4}, Ljava/io/StreamTokenizer;->nextToken()I

    #@14
    move-result v5

    #@15
    .line 157
    .local v5, token:I
    if-ne v5, v11, :cond_18

    #@17
    .line 222
    return-void

    #@18
    .line 160
    :cond_18
    if-eq v5, v10, :cond_23

    #@1a
    .line 161
    new-instance v8, Lcom/android/internal/util/TypedProperties$ParseException;

    #@1c
    const-string/jumbo v9, "type name"

    #@1f
    invoke-direct {v8, v4, v9}, Lcom/android/internal/util/TypedProperties$ParseException;-><init>(Ljava/io/StreamTokenizer;Ljava/lang/String;)V

    #@22
    throw v8

    #@23
    .line 163
    :cond_23
    iget-object v8, v4, Ljava/io/StreamTokenizer;->sval:Ljava/lang/String;

    #@25
    invoke-static {v8}, Lcom/android/internal/util/TypedProperties;->interpretType(Ljava/lang/String;)I

    #@28
    move-result v6

    #@29
    .line 164
    .local v6, type:I
    if-ne v6, v11, :cond_34

    #@2b
    .line 165
    new-instance v8, Lcom/android/internal/util/TypedProperties$ParseException;

    #@2d
    const-string/jumbo v9, "valid type name"

    #@30
    invoke-direct {v8, v4, v9}, Lcom/android/internal/util/TypedProperties$ParseException;-><init>(Ljava/io/StreamTokenizer;Ljava/lang/String;)V

    #@33
    throw v8

    #@34
    .line 167
    :cond_34
    iput-object v13, v4, Ljava/io/StreamTokenizer;->sval:Ljava/lang/String;

    #@36
    .line 169
    if-ne v6, v12, :cond_48

    #@38
    .line 171
    invoke-virtual {v4}, Ljava/io/StreamTokenizer;->nextToken()I

    #@3b
    move-result v5

    #@3c
    .line 172
    const/16 v8, 0x28

    #@3e
    if-eq v5, v8, :cond_48

    #@40
    .line 173
    new-instance v8, Lcom/android/internal/util/TypedProperties$ParseException;

    #@42
    const-string v9, "\'(\'"

    #@44
    invoke-direct {v8, v4, v9}, Lcom/android/internal/util/TypedProperties$ParseException;-><init>(Ljava/io/StreamTokenizer;Ljava/lang/String;)V

    #@47
    throw v8

    #@48
    .line 178
    :cond_48
    invoke-virtual {v4}, Ljava/io/StreamTokenizer;->nextToken()I

    #@4b
    move-result v5

    #@4c
    .line 179
    if-eq v5, v10, :cond_57

    #@4e
    .line 180
    new-instance v8, Lcom/android/internal/util/TypedProperties$ParseException;

    #@50
    const-string/jumbo v9, "property name"

    #@53
    invoke-direct {v8, v4, v9}, Lcom/android/internal/util/TypedProperties$ParseException;-><init>(Ljava/io/StreamTokenizer;Ljava/lang/String;)V

    #@56
    throw v8

    #@57
    .line 182
    :cond_57
    iget-object v2, v4, Ljava/io/StreamTokenizer;->sval:Ljava/lang/String;

    #@59
    .line 183
    .local v2, propertyName:Ljava/lang/String;
    invoke-virtual {v3, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@5c
    move-result-object v8

    #@5d
    invoke-virtual {v8}, Ljava/util/regex/Matcher;->matches()Z

    #@60
    move-result v8

    #@61
    if-nez v8, :cond_6c

    #@63
    .line 184
    new-instance v8, Lcom/android/internal/util/TypedProperties$ParseException;

    #@65
    const-string/jumbo v9, "valid property name"

    #@68
    invoke-direct {v8, v4, v9}, Lcom/android/internal/util/TypedProperties$ParseException;-><init>(Ljava/io/StreamTokenizer;Ljava/lang/String;)V

    #@6b
    throw v8

    #@6c
    .line 186
    :cond_6c
    iput-object v13, v4, Ljava/io/StreamTokenizer;->sval:Ljava/lang/String;

    #@6e
    .line 188
    if-ne v6, v12, :cond_93

    #@70
    .line 190
    invoke-virtual {v4}, Ljava/io/StreamTokenizer;->nextToken()I

    #@73
    move-result v5

    #@74
    .line 191
    const/16 v8, 0x29

    #@76
    if-eq v5, v8, :cond_80

    #@78
    .line 192
    new-instance v8, Lcom/android/internal/util/TypedProperties$ParseException;

    #@7a
    const-string v9, "\')\'"

    #@7c
    invoke-direct {v8, v4, v9}, Lcom/android/internal/util/TypedProperties$ParseException;-><init>(Ljava/io/StreamTokenizer;Ljava/lang/String;)V

    #@7f
    throw v8

    #@80
    .line 194
    :cond_80
    invoke-interface {p1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@83
    .line 217
    :goto_83
    invoke-virtual {v4}, Ljava/io/StreamTokenizer;->nextToken()I

    #@86
    move-result v5

    #@87
    .line 218
    const/16 v8, 0x3b

    #@89
    if-eq v5, v8, :cond_11

    #@8b
    .line 219
    new-instance v8, Lcom/android/internal/util/TypedProperties$ParseException;

    #@8d
    const-string v9, "\';\'"

    #@8f
    invoke-direct {v8, v4, v9}, Lcom/android/internal/util/TypedProperties$ParseException;-><init>(Ljava/io/StreamTokenizer;Ljava/lang/String;)V

    #@92
    throw v8

    #@93
    .line 197
    :cond_93
    invoke-virtual {v4}, Ljava/io/StreamTokenizer;->nextToken()I

    #@96
    move-result v5

    #@97
    .line 198
    const/16 v8, 0x3d

    #@99
    if-eq v5, v8, :cond_a3

    #@9b
    .line 199
    new-instance v8, Lcom/android/internal/util/TypedProperties$ParseException;

    #@9d
    const-string v9, "\'=\'"

    #@9f
    invoke-direct {v8, v4, v9}, Lcom/android/internal/util/TypedProperties$ParseException;-><init>(Ljava/io/StreamTokenizer;Ljava/lang/String;)V

    #@a2
    throw v8

    #@a3
    .line 203
    :cond_a3
    invoke-static {v4, v6}, Lcom/android/internal/util/TypedProperties;->parseValue(Ljava/io/StreamTokenizer;I)Ljava/lang/Object;

    #@a6
    move-result-object v7

    #@a7
    .line 204
    .local v7, value:Ljava/lang/Object;
    invoke-interface {p1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@aa
    move-result-object v1

    #@ab
    .line 205
    .local v1, oldValue:Ljava/lang/Object;
    if-eqz v1, :cond_bf

    #@ad
    .line 208
    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@b0
    move-result-object v8

    #@b1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@b4
    move-result-object v9

    #@b5
    if-eq v8, v9, :cond_bf

    #@b7
    .line 209
    new-instance v8, Lcom/android/internal/util/TypedProperties$ParseException;

    #@b9
    const-string v9, "(property previously declared as a different type)"

    #@bb
    invoke-direct {v8, v4, v9}, Lcom/android/internal/util/TypedProperties$ParseException;-><init>(Ljava/io/StreamTokenizer;Ljava/lang/String;)V

    #@be
    throw v8

    #@bf
    .line 213
    :cond_bf
    invoke-interface {p1, v2, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@c2
    goto :goto_83
.end method

.method static parseValue(Ljava/io/StreamTokenizer;I)Ljava/lang/Object;
    .registers 12
    .parameter "st"
    .parameter "type"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v9, -0x3

    #@1
    .line 234
    invoke-virtual {p0}, Ljava/io/StreamTokenizer;->nextToken()I

    #@4
    move-result v3

    #@5
    .line 236
    .local v3, token:I
    const/16 v7, 0x5a

    #@7
    if-ne p1, v7, :cond_36

    #@9
    .line 237
    if-eq v3, v9, :cond_13

    #@b
    .line 238
    new-instance v7, Lcom/android/internal/util/TypedProperties$ParseException;

    #@d
    const-string v8, "boolean constant"

    #@f
    invoke-direct {v7, p0, v8}, Lcom/android/internal/util/TypedProperties$ParseException;-><init>(Ljava/io/StreamTokenizer;Ljava/lang/String;)V

    #@12
    throw v7

    #@13
    .line 241
    :cond_13
    const-string/jumbo v7, "true"

    #@16
    iget-object v8, p0, Ljava/io/StreamTokenizer;->sval:Ljava/lang/String;

    #@18
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v7

    #@1c
    if-eqz v7, :cond_21

    #@1e
    .line 242
    sget-object v7, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    #@20
    .line 330
    :goto_20
    return-object v7

    #@21
    .line 243
    :cond_21
    const-string v7, "false"

    #@23
    iget-object v8, p0, Ljava/io/StreamTokenizer;->sval:Ljava/lang/String;

    #@25
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v7

    #@29
    if-eqz v7, :cond_2e

    #@2b
    .line 244
    sget-object v7, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    #@2d
    goto :goto_20

    #@2e
    .line 247
    :cond_2e
    new-instance v7, Lcom/android/internal/util/TypedProperties$ParseException;

    #@30
    const-string v8, "boolean constant"

    #@32
    invoke-direct {v7, p0, v8}, Lcom/android/internal/util/TypedProperties$ParseException;-><init>(Ljava/io/StreamTokenizer;Ljava/lang/String;)V

    #@35
    throw v7

    #@36
    .line 248
    :cond_36
    and-int/lit16 v7, p1, 0xff

    #@38
    const/16 v8, 0x49

    #@3a
    if-ne v7, v8, :cond_ee

    #@3c
    .line 249
    if-eq v3, v9, :cond_46

    #@3e
    .line 250
    new-instance v7, Lcom/android/internal/util/TypedProperties$ParseException;

    #@40
    const-string v8, "integer constant"

    #@42
    invoke-direct {v7, p0, v8}, Lcom/android/internal/util/TypedProperties$ParseException;-><init>(Ljava/io/StreamTokenizer;Ljava/lang/String;)V

    #@45
    throw v7

    #@46
    .line 259
    :cond_46
    :try_start_46
    iget-object v7, p0, Ljava/io/StreamTokenizer;->sval:Ljava/lang/String;

    #@48
    invoke-static {v7}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;

    #@4b
    move-result-object v7

    #@4c
    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J
    :try_end_4f
    .catch Ljava/lang/NumberFormatException; {:try_start_46 .. :try_end_4f} :catch_70

    #@4f
    move-result-wide v4

    #@50
    .line 265
    .local v4, value:J
    shr-int/lit8 v7, p1, 0x8

    #@52
    and-int/lit16 v6, v7, 0xff

    #@54
    .line 266
    .local v6, width:I
    packed-switch v6, :pswitch_data_18e

    #@57
    .line 288
    :pswitch_57
    new-instance v7, Ljava/lang/IllegalStateException;

    #@59
    new-instance v8, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v9, "Internal error; unexpected integer type width "

    #@60
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v8

    #@64
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@67
    move-result-object v8

    #@68
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v8

    #@6c
    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@6f
    throw v7

    #@70
    .line 260
    .end local v4           #value:J
    .end local v6           #width:I
    :catch_70
    move-exception v2

    #@71
    .line 261
    .local v2, ex:Ljava/lang/NumberFormatException;
    new-instance v7, Lcom/android/internal/util/TypedProperties$ParseException;

    #@73
    const-string v8, "integer constant"

    #@75
    invoke-direct {v7, p0, v8}, Lcom/android/internal/util/TypedProperties$ParseException;-><init>(Ljava/io/StreamTokenizer;Ljava/lang/String;)V

    #@78
    throw v7

    #@79
    .line 268
    .end local v2           #ex:Ljava/lang/NumberFormatException;
    .restart local v4       #value:J
    .restart local v6       #width:I
    :pswitch_79
    const-wide/16 v7, -0x80

    #@7b
    cmp-long v7, v4, v7

    #@7d
    if-ltz v7, :cond_85

    #@7f
    const-wide/16 v7, 0x7f

    #@81
    cmp-long v7, v4, v7

    #@83
    if-lez v7, :cond_8d

    #@85
    .line 269
    :cond_85
    new-instance v7, Lcom/android/internal/util/TypedProperties$ParseException;

    #@87
    const-string v8, "8-bit integer constant"

    #@89
    invoke-direct {v7, p0, v8}, Lcom/android/internal/util/TypedProperties$ParseException;-><init>(Ljava/io/StreamTokenizer;Ljava/lang/String;)V

    #@8c
    throw v7

    #@8d
    .line 271
    :cond_8d
    new-instance v7, Ljava/lang/Byte;

    #@8f
    long-to-int v8, v4

    #@90
    int-to-byte v8, v8

    #@91
    invoke-direct {v7, v8}, Ljava/lang/Byte;-><init>(B)V

    #@94
    goto :goto_20

    #@95
    .line 273
    :pswitch_95
    const-wide/16 v7, -0x8000

    #@97
    cmp-long v7, v4, v7

    #@99
    if-ltz v7, :cond_a1

    #@9b
    const-wide/16 v7, 0x7fff

    #@9d
    cmp-long v7, v4, v7

    #@9f
    if-lez v7, :cond_a9

    #@a1
    .line 274
    :cond_a1
    new-instance v7, Lcom/android/internal/util/TypedProperties$ParseException;

    #@a3
    const-string v8, "16-bit integer constant"

    #@a5
    invoke-direct {v7, p0, v8}, Lcom/android/internal/util/TypedProperties$ParseException;-><init>(Ljava/io/StreamTokenizer;Ljava/lang/String;)V

    #@a8
    throw v7

    #@a9
    .line 276
    :cond_a9
    new-instance v7, Ljava/lang/Short;

    #@ab
    long-to-int v8, v4

    #@ac
    int-to-short v8, v8

    #@ad
    invoke-direct {v7, v8}, Ljava/lang/Short;-><init>(S)V

    #@b0
    goto/16 :goto_20

    #@b2
    .line 278
    :pswitch_b2
    const-wide/32 v7, -0x80000000

    #@b5
    cmp-long v7, v4, v7

    #@b7
    if-ltz v7, :cond_c0

    #@b9
    const-wide/32 v7, 0x7fffffff

    #@bc
    cmp-long v7, v4, v7

    #@be
    if-lez v7, :cond_c8

    #@c0
    .line 279
    :cond_c0
    new-instance v7, Lcom/android/internal/util/TypedProperties$ParseException;

    #@c2
    const-string v8, "32-bit integer constant"

    #@c4
    invoke-direct {v7, p0, v8}, Lcom/android/internal/util/TypedProperties$ParseException;-><init>(Ljava/io/StreamTokenizer;Ljava/lang/String;)V

    #@c7
    throw v7

    #@c8
    .line 281
    :cond_c8
    new-instance v7, Ljava/lang/Integer;

    #@ca
    long-to-int v8, v4

    #@cb
    invoke-direct {v7, v8}, Ljava/lang/Integer;-><init>(I)V

    #@ce
    goto/16 :goto_20

    #@d0
    .line 283
    :pswitch_d0
    const-wide/high16 v7, -0x8000

    #@d2
    cmp-long v7, v4, v7

    #@d4
    if-ltz v7, :cond_df

    #@d6
    const-wide v7, 0x7fffffffffffffffL

    #@db
    cmp-long v7, v4, v7

    #@dd
    if-lez v7, :cond_e7

    #@df
    .line 284
    :cond_df
    new-instance v7, Lcom/android/internal/util/TypedProperties$ParseException;

    #@e1
    const-string v8, "64-bit integer constant"

    #@e3
    invoke-direct {v7, p0, v8}, Lcom/android/internal/util/TypedProperties$ParseException;-><init>(Ljava/io/StreamTokenizer;Ljava/lang/String;)V

    #@e6
    throw v7

    #@e7
    .line 286
    :cond_e7
    new-instance v7, Ljava/lang/Long;

    #@e9
    invoke-direct {v7, v4, v5}, Ljava/lang/Long;-><init>(J)V

    #@ec
    goto/16 :goto_20

    #@ee
    .line 291
    .end local v4           #value:J
    .end local v6           #width:I
    :cond_ee
    and-int/lit16 v7, p1, 0xff

    #@f0
    const/16 v8, 0x46

    #@f2
    if-ne v7, v8, :cond_150

    #@f4
    .line 292
    if-eq v3, v9, :cond_fe

    #@f6
    .line 293
    new-instance v7, Lcom/android/internal/util/TypedProperties$ParseException;

    #@f8
    const-string v8, "float constant"

    #@fa
    invoke-direct {v7, p0, v8}, Lcom/android/internal/util/TypedProperties$ParseException;-><init>(Ljava/io/StreamTokenizer;Ljava/lang/String;)V

    #@fd
    throw v7

    #@fe
    .line 306
    :cond_fe
    :try_start_fe
    iget-object v7, p0, Ljava/io/StreamTokenizer;->sval:Ljava/lang/String;

    #@100
    invoke-static {v7}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_103
    .catch Ljava/lang/NumberFormatException; {:try_start_fe .. :try_end_103} :catch_138

    #@103
    move-result-wide v4

    #@104
    .line 312
    .local v4, value:D
    shr-int/lit8 v7, p1, 0x8

    #@106
    and-int/lit16 v7, v7, 0xff

    #@108
    const/4 v8, 0x4

    #@109
    if-ne v7, v8, :cond_149

    #@10b
    .line 314
    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    #@10e
    move-result-wide v0

    #@10f
    .line 315
    .local v0, absValue:D
    const-wide/16 v7, 0x0

    #@111
    cmpl-double v7, v0, v7

    #@113
    if-eqz v7, :cond_141

    #@115
    invoke-static {v4, v5}, Ljava/lang/Double;->isInfinite(D)Z

    #@118
    move-result v7

    #@119
    if-nez v7, :cond_141

    #@11b
    invoke-static {v4, v5}, Ljava/lang/Double;->isNaN(D)Z

    #@11e
    move-result v7

    #@11f
    if-nez v7, :cond_141

    #@121
    .line 316
    const-wide/high16 v7, 0x36a0

    #@123
    cmpg-double v7, v0, v7

    #@125
    if-ltz v7, :cond_130

    #@127
    const-wide v7, 0x47efffffe0000000L

    #@12c
    cmpl-double v7, v0, v7

    #@12e
    if-lez v7, :cond_141

    #@130
    .line 317
    :cond_130
    new-instance v7, Lcom/android/internal/util/TypedProperties$ParseException;

    #@132
    const-string v8, "32-bit float constant"

    #@134
    invoke-direct {v7, p0, v8}, Lcom/android/internal/util/TypedProperties$ParseException;-><init>(Ljava/io/StreamTokenizer;Ljava/lang/String;)V

    #@137
    throw v7

    #@138
    .line 307
    .end local v0           #absValue:D
    .end local v4           #value:D
    :catch_138
    move-exception v2

    #@139
    .line 308
    .restart local v2       #ex:Ljava/lang/NumberFormatException;
    new-instance v7, Lcom/android/internal/util/TypedProperties$ParseException;

    #@13b
    const-string v8, "float constant"

    #@13d
    invoke-direct {v7, p0, v8}, Lcom/android/internal/util/TypedProperties$ParseException;-><init>(Ljava/io/StreamTokenizer;Ljava/lang/String;)V

    #@140
    throw v7

    #@141
    .line 320
    .end local v2           #ex:Ljava/lang/NumberFormatException;
    .restart local v0       #absValue:D
    .restart local v4       #value:D
    :cond_141
    new-instance v7, Ljava/lang/Float;

    #@143
    double-to-float v8, v4

    #@144
    invoke-direct {v7, v8}, Ljava/lang/Float;-><init>(F)V

    #@147
    goto/16 :goto_20

    #@149
    .line 323
    .end local v0           #absValue:D
    :cond_149
    new-instance v7, Ljava/lang/Double;

    #@14b
    invoke-direct {v7, v4, v5}, Ljava/lang/Double;-><init>(D)V

    #@14e
    goto/16 :goto_20

    #@150
    .line 325
    .end local v4           #value:D
    :cond_150
    const/16 v7, 0x734c

    #@152
    if-ne p1, v7, :cond_175

    #@154
    .line 327
    const/16 v7, 0x22

    #@156
    if-ne v3, v7, :cond_15c

    #@158
    .line 328
    iget-object v7, p0, Ljava/io/StreamTokenizer;->sval:Ljava/lang/String;

    #@15a
    goto/16 :goto_20

    #@15c
    .line 329
    :cond_15c
    if-ne v3, v9, :cond_16d

    #@15e
    const-string/jumbo v7, "null"

    #@161
    iget-object v8, p0, Ljava/io/StreamTokenizer;->sval:Ljava/lang/String;

    #@163
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@166
    move-result v7

    #@167
    if-eqz v7, :cond_16d

    #@169
    .line 330
    sget-object v7, Lcom/android/internal/util/TypedProperties;->NULL_STRING:Ljava/lang/String;

    #@16b
    goto/16 :goto_20

    #@16d
    .line 332
    :cond_16d
    new-instance v7, Lcom/android/internal/util/TypedProperties$ParseException;

    #@16f
    const-string v8, "double-quoted string or \'null\'"

    #@171
    invoke-direct {v7, p0, v8}, Lcom/android/internal/util/TypedProperties$ParseException;-><init>(Ljava/io/StreamTokenizer;Ljava/lang/String;)V

    #@174
    throw v7

    #@175
    .line 335
    :cond_175
    new-instance v7, Ljava/lang/IllegalStateException;

    #@177
    new-instance v8, Ljava/lang/StringBuilder;

    #@179
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@17c
    const-string v9, "Internal error; unknown type "

    #@17e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@181
    move-result-object v8

    #@182
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@185
    move-result-object v8

    #@186
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@189
    move-result-object v8

    #@18a
    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@18d
    throw v7

    #@18e
    .line 266
    :pswitch_data_18e
    .packed-switch 0x1
        :pswitch_79
        :pswitch_95
        :pswitch_57
        :pswitch_b2
        :pswitch_57
        :pswitch_57
        :pswitch_57
        :pswitch_d0
    .end packed-switch
.end method


# virtual methods
.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 403
    invoke-super {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    .line 404
    .local v0, value:Ljava/lang/Object;
    sget-object v1, Lcom/android/internal/util/TypedProperties;->NULL_STRING:Ljava/lang/String;

    #@6
    if-ne v0, v1, :cond_9

    #@8
    .line 405
    const/4 v0, 0x0

    #@9
    .line 407
    .end local v0           #value:Ljava/lang/Object;
    :cond_9
    return-object v0
.end method

.method public getBoolean(Ljava/lang/String;)Z
    .registers 3
    .parameter "property"

    #@0
    .prologue
    .line 600
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/util/TypedProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public getBoolean(Ljava/lang/String;Z)Z
    .registers 6
    .parameter "property"
    .parameter "def"

    #@0
    .prologue
    .line 435
    invoke-super {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    .line 436
    .local v0, value:Ljava/lang/Object;
    if-nez v0, :cond_7

    #@6
    .line 440
    .end local v0           #value:Ljava/lang/Object;
    .end local p2
    :goto_6
    return p2

    #@7
    .line 439
    .restart local v0       #value:Ljava/lang/Object;
    .restart local p2
    :cond_7
    instance-of v1, v0, Ljava/lang/Boolean;

    #@9
    if-eqz v1, :cond_12

    #@b
    .line 440
    check-cast v0, Ljava/lang/Boolean;

    #@d
    .end local v0           #value:Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    #@10
    move-result p2

    #@11
    goto :goto_6

    #@12
    .line 442
    .restart local v0       #value:Ljava/lang/Object;
    :cond_12
    new-instance v1, Lcom/android/internal/util/TypedProperties$TypeException;

    #@14
    const-string v2, "boolean"

    #@16
    invoke-direct {v1, p1, v0, v2}, Lcom/android/internal/util/TypedProperties$TypeException;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    #@19
    throw v1
.end method

.method public getByte(Ljava/lang/String;)B
    .registers 3
    .parameter "property"

    #@0
    .prologue
    .line 612
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/util/TypedProperties;->getByte(Ljava/lang/String;B)B

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public getByte(Ljava/lang/String;B)B
    .registers 6
    .parameter "property"
    .parameter "def"

    #@0
    .prologue
    .line 455
    invoke-super {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    .line 456
    .local v0, value:Ljava/lang/Object;
    if-nez v0, :cond_7

    #@6
    .line 460
    .end local v0           #value:Ljava/lang/Object;
    .end local p2
    :goto_6
    return p2

    #@7
    .line 459
    .restart local v0       #value:Ljava/lang/Object;
    .restart local p2
    :cond_7
    instance-of v1, v0, Ljava/lang/Byte;

    #@9
    if-eqz v1, :cond_12

    #@b
    .line 460
    check-cast v0, Ljava/lang/Byte;

    #@d
    .end local v0           #value:Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    #@10
    move-result p2

    #@11
    goto :goto_6

    #@12
    .line 462
    .restart local v0       #value:Ljava/lang/Object;
    :cond_12
    new-instance v1, Lcom/android/internal/util/TypedProperties$TypeException;

    #@14
    const-string v2, "byte"

    #@16
    invoke-direct {v1, p1, v0, v2}, Lcom/android/internal/util/TypedProperties$TypeException;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    #@19
    throw v1
.end method

.method public getDouble(Ljava/lang/String;)D
    .registers 4
    .parameter "property"

    #@0
    .prologue
    .line 672
    const-wide/16 v0, 0x0

    #@2
    invoke-virtual {p0, p1, v0, v1}, Lcom/android/internal/util/TypedProperties;->getDouble(Ljava/lang/String;D)D

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method public getDouble(Ljava/lang/String;D)D
    .registers 7
    .parameter "property"
    .parameter "def"

    #@0
    .prologue
    .line 555
    invoke-super {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    .line 556
    .local v0, value:Ljava/lang/Object;
    if-nez v0, :cond_7

    #@6
    .line 560
    .end local v0           #value:Ljava/lang/Object;
    .end local p2
    :goto_6
    return-wide p2

    #@7
    .line 559
    .restart local v0       #value:Ljava/lang/Object;
    .restart local p2
    :cond_7
    instance-of v1, v0, Ljava/lang/Double;

    #@9
    if-eqz v1, :cond_12

    #@b
    .line 560
    check-cast v0, Ljava/lang/Double;

    #@d
    .end local v0           #value:Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    #@10
    move-result-wide p2

    #@11
    goto :goto_6

    #@12
    .line 562
    .restart local v0       #value:Ljava/lang/Object;
    :cond_12
    new-instance v1, Lcom/android/internal/util/TypedProperties$TypeException;

    #@14
    const-string v2, "double"

    #@16
    invoke-direct {v1, p1, v0, v2}, Lcom/android/internal/util/TypedProperties$TypeException;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    #@19
    throw v1
.end method

.method public getFloat(Ljava/lang/String;)F
    .registers 3
    .parameter "property"

    #@0
    .prologue
    .line 660
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/util/TypedProperties;->getFloat(Ljava/lang/String;F)F

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public getFloat(Ljava/lang/String;F)F
    .registers 6
    .parameter "property"
    .parameter "def"

    #@0
    .prologue
    .line 535
    invoke-super {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    .line 536
    .local v0, value:Ljava/lang/Object;
    if-nez v0, :cond_7

    #@6
    .line 540
    .end local v0           #value:Ljava/lang/Object;
    .end local p2
    :goto_6
    return p2

    #@7
    .line 539
    .restart local v0       #value:Ljava/lang/Object;
    .restart local p2
    :cond_7
    instance-of v1, v0, Ljava/lang/Float;

    #@9
    if-eqz v1, :cond_12

    #@b
    .line 540
    check-cast v0, Ljava/lang/Float;

    #@d
    .end local v0           #value:Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    #@10
    move-result p2

    #@11
    goto :goto_6

    #@12
    .line 542
    .restart local v0       #value:Ljava/lang/Object;
    :cond_12
    new-instance v1, Lcom/android/internal/util/TypedProperties$TypeException;

    #@14
    const-string v2, "float"

    #@16
    invoke-direct {v1, p1, v0, v2}, Lcom/android/internal/util/TypedProperties$TypeException;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    #@19
    throw v1
.end method

.method public getInt(Ljava/lang/String;)I
    .registers 3
    .parameter "property"

    #@0
    .prologue
    .line 636
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/util/TypedProperties;->getInt(Ljava/lang/String;I)I

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public getInt(Ljava/lang/String;I)I
    .registers 6
    .parameter "property"
    .parameter "def"

    #@0
    .prologue
    .line 495
    invoke-super {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    .line 496
    .local v0, value:Ljava/lang/Object;
    if-nez v0, :cond_7

    #@6
    .line 500
    .end local v0           #value:Ljava/lang/Object;
    .end local p2
    :goto_6
    return p2

    #@7
    .line 499
    .restart local v0       #value:Ljava/lang/Object;
    .restart local p2
    :cond_7
    instance-of v1, v0, Ljava/lang/Integer;

    #@9
    if-eqz v1, :cond_12

    #@b
    .line 500
    check-cast v0, Ljava/lang/Integer;

    #@d
    .end local v0           #value:Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@10
    move-result p2

    #@11
    goto :goto_6

    #@12
    .line 502
    .restart local v0       #value:Ljava/lang/Object;
    :cond_12
    new-instance v1, Lcom/android/internal/util/TypedProperties$TypeException;

    #@14
    const-string v2, "int"

    #@16
    invoke-direct {v1, p1, v0, v2}, Lcom/android/internal/util/TypedProperties$TypeException;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    #@19
    throw v1
.end method

.method public getLong(Ljava/lang/String;)J
    .registers 4
    .parameter "property"

    #@0
    .prologue
    .line 648
    const-wide/16 v0, 0x0

    #@2
    invoke-virtual {p0, p1, v0, v1}, Lcom/android/internal/util/TypedProperties;->getLong(Ljava/lang/String;J)J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method public getLong(Ljava/lang/String;J)J
    .registers 7
    .parameter "property"
    .parameter "def"

    #@0
    .prologue
    .line 515
    invoke-super {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    .line 516
    .local v0, value:Ljava/lang/Object;
    if-nez v0, :cond_7

    #@6
    .line 520
    .end local v0           #value:Ljava/lang/Object;
    .end local p2
    :goto_6
    return-wide p2

    #@7
    .line 519
    .restart local v0       #value:Ljava/lang/Object;
    .restart local p2
    :cond_7
    instance-of v1, v0, Ljava/lang/Long;

    #@9
    if-eqz v1, :cond_12

    #@b
    .line 520
    check-cast v0, Ljava/lang/Long;

    #@d
    .end local v0           #value:Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    #@10
    move-result-wide p2

    #@11
    goto :goto_6

    #@12
    .line 522
    .restart local v0       #value:Ljava/lang/Object;
    :cond_12
    new-instance v1, Lcom/android/internal/util/TypedProperties$TypeException;

    #@14
    const-string/jumbo v2, "long"

    #@17
    invoke-direct {v1, p1, v0, v2}, Lcom/android/internal/util/TypedProperties$TypeException;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    #@1a
    throw v1
.end method

.method public getShort(Ljava/lang/String;)S
    .registers 3
    .parameter "property"

    #@0
    .prologue
    .line 624
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/util/TypedProperties;->getShort(Ljava/lang/String;S)S

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public getShort(Ljava/lang/String;S)S
    .registers 6
    .parameter "property"
    .parameter "def"

    #@0
    .prologue
    .line 475
    invoke-super {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    .line 476
    .local v0, value:Ljava/lang/Object;
    if-nez v0, :cond_7

    #@6
    .line 480
    .end local v0           #value:Ljava/lang/Object;
    .end local p2
    :goto_6
    return p2

    #@7
    .line 479
    .restart local v0       #value:Ljava/lang/Object;
    .restart local p2
    :cond_7
    instance-of v1, v0, Ljava/lang/Short;

    #@9
    if-eqz v1, :cond_12

    #@b
    .line 480
    check-cast v0, Ljava/lang/Short;

    #@d
    .end local v0           #value:Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S

    #@10
    move-result p2

    #@11
    goto :goto_6

    #@12
    .line 482
    .restart local v0       #value:Ljava/lang/Object;
    :cond_12
    new-instance v1, Lcom/android/internal/util/TypedProperties$TypeException;

    #@14
    const-string/jumbo v2, "short"

    #@17
    invoke-direct {v1, p1, v0, v2}, Lcom/android/internal/util/TypedProperties$TypeException;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    #@1a
    throw v1
.end method

.method public getString(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "property"

    #@0
    .prologue
    .line 684
    const-string v0, ""

    #@2
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/util/TypedProperties;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "property"
    .parameter "def"

    #@0
    .prologue
    .line 575
    invoke-super {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    .line 576
    .local v0, value:Ljava/lang/Object;
    if-nez v0, :cond_7

    #@6
    .line 582
    .end local v0           #value:Ljava/lang/Object;
    .end local p2
    :goto_6
    return-object p2

    #@7
    .line 579
    .restart local v0       #value:Ljava/lang/Object;
    .restart local p2
    :cond_7
    sget-object v1, Lcom/android/internal/util/TypedProperties;->NULL_STRING:Ljava/lang/String;

    #@9
    if-ne v0, v1, :cond_d

    #@b
    .line 580
    const/4 p2, 0x0

    #@c
    goto :goto_6

    #@d
    .line 581
    :cond_d
    instance-of v1, v0, Ljava/lang/String;

    #@f
    if-eqz v1, :cond_15

    #@11
    .line 582
    check-cast v0, Ljava/lang/String;

    #@13
    .end local v0           #value:Ljava/lang/Object;
    move-object p2, v0

    #@14
    goto :goto_6

    #@15
    .line 584
    .restart local v0       #value:Ljava/lang/Object;
    :cond_15
    new-instance v1, Lcom/android/internal/util/TypedProperties$TypeException;

    #@17
    const-string/jumbo v2, "string"

    #@1a
    invoke-direct {v1, p1, v0, v2}, Lcom/android/internal/util/TypedProperties$TypeException;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    #@1d
    throw v1
.end method

.method public getStringInfo(Ljava/lang/String;)I
    .registers 4
    .parameter "property"

    #@0
    .prologue
    .line 703
    invoke-super {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    .line 704
    .local v0, value:Ljava/lang/Object;
    if-nez v0, :cond_8

    #@6
    .line 705
    const/4 v1, -0x1

    #@7
    .line 712
    :goto_7
    return v1

    #@8
    .line 707
    :cond_8
    sget-object v1, Lcom/android/internal/util/TypedProperties;->NULL_STRING:Ljava/lang/String;

    #@a
    if-ne v0, v1, :cond_e

    #@c
    .line 708
    const/4 v1, 0x0

    #@d
    goto :goto_7

    #@e
    .line 709
    :cond_e
    instance-of v1, v0, Ljava/lang/String;

    #@10
    if-eqz v1, :cond_14

    #@12
    .line 710
    const/4 v1, 0x1

    #@13
    goto :goto_7

    #@14
    .line 712
    :cond_14
    const/4 v1, -0x2

    #@15
    goto :goto_7
.end method

.method public load(Ljava/io/Reader;)V
    .registers 2
    .parameter "r"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 398
    invoke-static {p1, p0}, Lcom/android/internal/util/TypedProperties;->parse(Ljava/io/Reader;Ljava/util/Map;)V

    #@3
    .line 399
    return-void
.end method
