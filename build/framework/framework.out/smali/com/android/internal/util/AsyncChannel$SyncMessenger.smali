.class Lcom/android/internal/util/AsyncChannel$SyncMessenger;
.super Ljava/lang/Object;
.source "AsyncChannel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/util/AsyncChannel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SyncMessenger"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;
    }
.end annotation


# static fields
.field private static sCount:I

.field private static sStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/android/internal/util/AsyncChannel$SyncMessenger;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mHandler:Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mMessenger:Landroid/os/Messenger;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 730
    new-instance v0, Ljava/util/Stack;

    #@2
    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    #@5
    sput-object v0, Lcom/android/internal/util/AsyncChannel$SyncMessenger;->sStack:Ljava/util/Stack;

    #@7
    .line 732
    const/4 v0, 0x0

    #@8
    sput v0, Lcom/android/internal/util/AsyncChannel$SyncMessenger;->sCount:I

    #@a
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 741
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 742
    return-void
.end method

.method static synthetic access$100(Landroid/os/Messenger;Landroid/os/Message;)Landroid/os/Message;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 728
    invoke-static {p0, p1}, Lcom/android/internal/util/AsyncChannel$SyncMessenger;->sendMessageSynchronously(Landroid/os/Messenger;Landroid/os/Message;)Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private static obtain()Lcom/android/internal/util/AsyncChannel$SyncMessenger;
    .registers 6

    #@0
    .prologue
    .line 772
    sget-object v2, Lcom/android/internal/util/AsyncChannel$SyncMessenger;->sStack:Ljava/util/Stack;

    #@2
    monitor-enter v2

    #@3
    .line 773
    :try_start_3
    sget-object v1, Lcom/android/internal/util/AsyncChannel$SyncMessenger;->sStack:Ljava/util/Stack;

    #@5
    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_51

    #@b
    .line 774
    new-instance v0, Lcom/android/internal/util/AsyncChannel$SyncMessenger;

    #@d
    invoke-direct {v0}, Lcom/android/internal/util/AsyncChannel$SyncMessenger;-><init>()V

    #@10
    .line 775
    .local v0, sm:Lcom/android/internal/util/AsyncChannel$SyncMessenger;
    new-instance v1, Landroid/os/HandlerThread;

    #@12
    new-instance v3, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v4, "SyncHandler-"

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    sget v4, Lcom/android/internal/util/AsyncChannel$SyncMessenger;->sCount:I

    #@1f
    add-int/lit8 v5, v4, 0x1

    #@21
    sput v5, Lcom/android/internal/util/AsyncChannel$SyncMessenger;->sCount:I

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    invoke-direct {v1, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@2e
    iput-object v1, v0, Lcom/android/internal/util/AsyncChannel$SyncMessenger;->mHandlerThread:Landroid/os/HandlerThread;

    #@30
    .line 776
    iget-object v1, v0, Lcom/android/internal/util/AsyncChannel$SyncMessenger;->mHandlerThread:Landroid/os/HandlerThread;

    #@32
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    #@35
    .line 777
    new-instance v1, Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;

    #@37
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@3a
    iget-object v3, v0, Lcom/android/internal/util/AsyncChannel$SyncMessenger;->mHandlerThread:Landroid/os/HandlerThread;

    #@3c
    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@3f
    move-result-object v3

    #@40
    const/4 v4, 0x0

    #@41
    invoke-direct {v1, v0, v3, v4}, Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;-><init>(Lcom/android/internal/util/AsyncChannel$SyncMessenger;Landroid/os/Looper;Lcom/android/internal/util/AsyncChannel$1;)V

    #@44
    iput-object v1, v0, Lcom/android/internal/util/AsyncChannel$SyncMessenger;->mHandler:Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;

    #@46
    .line 778
    new-instance v1, Landroid/os/Messenger;

    #@48
    iget-object v3, v0, Lcom/android/internal/util/AsyncChannel$SyncMessenger;->mHandler:Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;

    #@4a
    invoke-direct {v1, v3}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    #@4d
    iput-object v1, v0, Lcom/android/internal/util/AsyncChannel$SyncMessenger;->mMessenger:Landroid/os/Messenger;

    #@4f
    .line 782
    :goto_4f
    monitor-exit v2

    #@50
    .line 783
    return-object v0

    #@51
    .line 780
    .end local v0           #sm:Lcom/android/internal/util/AsyncChannel$SyncMessenger;
    :cond_51
    sget-object v1, Lcom/android/internal/util/AsyncChannel$SyncMessenger;->sStack:Ljava/util/Stack;

    #@53
    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    #@56
    move-result-object v0

    #@57
    check-cast v0, Lcom/android/internal/util/AsyncChannel$SyncMessenger;

    #@59
    .restart local v0       #sm:Lcom/android/internal/util/AsyncChannel$SyncMessenger;
    goto :goto_4f

    #@5a
    .line 782
    .end local v0           #sm:Lcom/android/internal/util/AsyncChannel$SyncMessenger;
    :catchall_5a
    move-exception v1

    #@5b
    monitor-exit v2
    :try_end_5c
    .catchall {:try_start_3 .. :try_end_5c} :catchall_5a

    #@5c
    throw v1
.end method

.method private recycle()V
    .registers 3

    #@0
    .prologue
    .line 790
    sget-object v1, Lcom/android/internal/util/AsyncChannel$SyncMessenger;->sStack:Ljava/util/Stack;

    #@2
    monitor-enter v1

    #@3
    .line 791
    :try_start_3
    sget-object v0, Lcom/android/internal/util/AsyncChannel$SyncMessenger;->sStack:Ljava/util/Stack;

    #@5
    invoke-virtual {v0, p0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    .line 792
    monitor-exit v1

    #@9
    .line 793
    return-void

    #@a
    .line 792
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method private static sendMessageSynchronously(Landroid/os/Messenger;Landroid/os/Message;)Landroid/os/Message;
    .registers 8
    .parameter "dstMessenger"
    .parameter "msg"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 802
    invoke-static {}, Lcom/android/internal/util/AsyncChannel$SyncMessenger;->obtain()Lcom/android/internal/util/AsyncChannel$SyncMessenger;

    #@4
    move-result-object v2

    #@5
    .line 804
    .local v2, sm:Lcom/android/internal/util/AsyncChannel$SyncMessenger;
    if-eqz p0, :cond_35

    #@7
    if-eqz p1, :cond_35

    #@9
    .line 805
    :try_start_9
    iget-object v3, v2, Lcom/android/internal/util/AsyncChannel$SyncMessenger;->mMessenger:Landroid/os/Messenger;

    #@b
    iput-object v3, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@d
    .line 806
    iget-object v3, v2, Lcom/android/internal/util/AsyncChannel$SyncMessenger;->mHandler:Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;

    #@f
    invoke-static {v3}, Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;->access$300(Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;)Ljava/lang/Object;

    #@12
    move-result-object v4

    #@13
    monitor-enter v4
    :try_end_14
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_14} :catch_2e
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_14} :catch_3c

    #@14
    .line 807
    :try_start_14
    invoke-virtual {p0, p1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V

    #@17
    .line 808
    iget-object v3, v2, Lcom/android/internal/util/AsyncChannel$SyncMessenger;->mHandler:Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;

    #@19
    invoke-static {v3}, Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;->access$300(Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;)Ljava/lang/Object;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3}, Ljava/lang/Object;->wait()V

    #@20
    .line 809
    monitor-exit v4
    :try_end_21
    .catchall {:try_start_14 .. :try_end_21} :catchall_2b

    #@21
    .line 818
    :goto_21
    iget-object v3, v2, Lcom/android/internal/util/AsyncChannel$SyncMessenger;->mHandler:Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;

    #@23
    invoke-static {v3}, Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;->access$400(Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;)Landroid/os/Message;

    #@26
    move-result-object v1

    #@27
    .line 819
    .local v1, resultMsg:Landroid/os/Message;
    invoke-direct {v2}, Lcom/android/internal/util/AsyncChannel$SyncMessenger;->recycle()V

    #@2a
    .line 820
    return-object v1

    #@2b
    .line 809
    .end local v1           #resultMsg:Landroid/os/Message;
    :catchall_2b
    move-exception v3

    #@2c
    :try_start_2c
    monitor-exit v4
    :try_end_2d
    .catchall {:try_start_2c .. :try_end_2d} :catchall_2b

    #@2d
    :try_start_2d
    throw v3
    :try_end_2e
    .catch Ljava/lang/InterruptedException; {:try_start_2d .. :try_end_2e} :catch_2e
    .catch Landroid/os/RemoteException; {:try_start_2d .. :try_end_2e} :catch_3c

    #@2e
    .line 813
    :catch_2e
    move-exception v0

    #@2f
    .line 814
    .local v0, e:Ljava/lang/InterruptedException;
    iget-object v3, v2, Lcom/android/internal/util/AsyncChannel$SyncMessenger;->mHandler:Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;

    #@31
    invoke-static {v3, v5}, Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;->access$402(Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;Landroid/os/Message;)Landroid/os/Message;

    #@34
    goto :goto_21

    #@35
    .line 811
    .end local v0           #e:Ljava/lang/InterruptedException;
    :cond_35
    :try_start_35
    iget-object v3, v2, Lcom/android/internal/util/AsyncChannel$SyncMessenger;->mHandler:Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;

    #@37
    const/4 v4, 0x0

    #@38
    invoke-static {v3, v4}, Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;->access$402(Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;Landroid/os/Message;)Landroid/os/Message;
    :try_end_3b
    .catch Ljava/lang/InterruptedException; {:try_start_35 .. :try_end_3b} :catch_2e
    .catch Landroid/os/RemoteException; {:try_start_35 .. :try_end_3b} :catch_3c

    #@3b
    goto :goto_21

    #@3c
    .line 815
    :catch_3c
    move-exception v0

    #@3d
    .line 816
    .local v0, e:Landroid/os/RemoteException;
    iget-object v3, v2, Lcom/android/internal/util/AsyncChannel$SyncMessenger;->mHandler:Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;

    #@3f
    invoke-static {v3, v5}, Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;->access$402(Lcom/android/internal/util/AsyncChannel$SyncMessenger$SyncHandler;Landroid/os/Message;)Landroid/os/Message;

    #@42
    goto :goto_21
.end method
