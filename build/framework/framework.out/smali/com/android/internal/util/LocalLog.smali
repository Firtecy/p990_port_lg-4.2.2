.class public Lcom/android/internal/util/LocalLog;
.super Ljava/lang/Object;
.source "LocalLog.java"


# instance fields
.field private final mLines:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mMaxLines:I

.field private final mTag:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter "tag"

    #@0
    .prologue
    const/16 v1, 0x14

    #@2
    .line 35
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 32
    iput v1, p0, Lcom/android/internal/util/LocalLog;->mMaxLines:I

    #@7
    .line 33
    new-instance v0, Ljava/util/ArrayList;

    #@9
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@c
    iput-object v0, p0, Lcom/android/internal/util/LocalLog;->mLines:Ljava/util/ArrayList;

    #@e
    .line 36
    iput-object p1, p0, Lcom/android/internal/util/LocalLog;->mTag:Ljava/lang/String;

    #@10
    .line 37
    return-void
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 7
    .parameter "pw"
    .parameter "header"
    .parameter "prefix"

    #@0
    .prologue
    .line 50
    iget-object v2, p0, Lcom/android/internal/util/LocalLog;->mLines:Ljava/util/ArrayList;

    #@2
    monitor-enter v2

    #@3
    .line 51
    :try_start_3
    iget-object v1, p0, Lcom/android/internal/util/LocalLog;->mLines:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v1

    #@9
    if-gtz v1, :cond_e

    #@b
    .line 52
    const/4 v1, 0x0

    #@c
    monitor-exit v2

    #@d
    .line 63
    :goto_d
    return v1

    #@e
    .line 54
    :cond_e
    if-eqz p2, :cond_13

    #@10
    .line 55
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@13
    .line 57
    :cond_13
    const/4 v0, 0x0

    #@14
    .local v0, i:I
    :goto_14
    iget-object v1, p0, Lcom/android/internal/util/LocalLog;->mLines:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@19
    move-result v1

    #@1a
    if-ge v0, v1, :cond_2f

    #@1c
    .line 58
    if-eqz p3, :cond_21

    #@1e
    .line 59
    invoke-virtual {p1, p3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@21
    .line 61
    :cond_21
    iget-object v1, p0, Lcom/android/internal/util/LocalLog;->mLines:Ljava/util/ArrayList;

    #@23
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@26
    move-result-object v1

    #@27
    check-cast v1, Ljava/lang/String;

    #@29
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2c
    .line 57
    add-int/lit8 v0, v0, 0x1

    #@2e
    goto :goto_14

    #@2f
    .line 63
    :cond_2f
    const/4 v1, 0x1

    #@30
    monitor-exit v2

    #@31
    goto :goto_d

    #@32
    .line 64
    .end local v0           #i:I
    :catchall_32
    move-exception v1

    #@33
    monitor-exit v2
    :try_end_34
    .catchall {:try_start_3 .. :try_end_34} :catchall_32

    #@34
    throw v1
.end method

.method public w(Ljava/lang/String;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 40
    iget-object v1, p0, Lcom/android/internal/util/LocalLog;->mLines:Ljava/util/ArrayList;

    #@2
    monitor-enter v1

    #@3
    .line 41
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/util/LocalLog;->mTag:Ljava/lang/String;

    #@5
    invoke-static {v0, p1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 42
    iget-object v0, p0, Lcom/android/internal/util/LocalLog;->mLines:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@d
    move-result v0

    #@e
    const/16 v2, 0x14

    #@10
    if-lt v0, v2, :cond_18

    #@12
    .line 43
    iget-object v0, p0, Lcom/android/internal/util/LocalLog;->mLines:Ljava/util/ArrayList;

    #@14
    const/4 v2, 0x0

    #@15
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@18
    .line 45
    :cond_18
    iget-object v0, p0, Lcom/android/internal/util/LocalLog;->mLines:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1d
    .line 46
    monitor-exit v1

    #@1e
    .line 47
    return-void

    #@1f
    .line 46
    :catchall_1f
    move-exception v0

    #@20
    monitor-exit v1
    :try_end_21
    .catchall {:try_start_3 .. :try_end_21} :catchall_1f

    #@21
    throw v0
.end method
