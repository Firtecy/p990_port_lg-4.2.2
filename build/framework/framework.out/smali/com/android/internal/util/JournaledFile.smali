.class public Lcom/android/internal/util/JournaledFile;
.super Ljava/lang/Object;
.source "JournaledFile.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field mReal:Ljava/io/File;

.field mTemp:Ljava/io/File;

.field mWriting:Z


# direct methods
.method public constructor <init>(Ljava/io/File;Ljava/io/File;)V
    .registers 3
    .parameter "real"
    .parameter "temp"

    #@0
    .prologue
    .line 36
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 37
    iput-object p1, p0, Lcom/android/internal/util/JournaledFile;->mReal:Ljava/io/File;

    #@5
    .line 38
    iput-object p2, p0, Lcom/android/internal/util/JournaledFile;->mTemp:Ljava/io/File;

    #@7
    .line 39
    return-void
.end method


# virtual methods
.method public chooseForRead()Ljava/io/File;
    .registers 4

    #@0
    .prologue
    .line 49
    iget-object v1, p0, Lcom/android/internal/util/JournaledFile;->mReal:Ljava/io/File;

    #@2
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_18

    #@8
    .line 50
    iget-object v0, p0, Lcom/android/internal/util/JournaledFile;->mReal:Ljava/io/File;

    #@a
    .line 51
    .local v0, result:Ljava/io/File;
    iget-object v1, p0, Lcom/android/internal/util/JournaledFile;->mTemp:Ljava/io/File;

    #@c
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@f
    move-result v1

    #@10
    if-eqz v1, :cond_17

    #@12
    .line 52
    iget-object v1, p0, Lcom/android/internal/util/JournaledFile;->mTemp:Ljava/io/File;

    #@14
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    #@17
    .line 60
    .end local v0           #result:Ljava/io/File;
    :cond_17
    :goto_17
    return-object v0

    #@18
    .line 54
    :cond_18
    iget-object v1, p0, Lcom/android/internal/util/JournaledFile;->mTemp:Ljava/io/File;

    #@1a
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@1d
    move-result v1

    #@1e
    if-eqz v1, :cond_2a

    #@20
    .line 55
    iget-object v0, p0, Lcom/android/internal/util/JournaledFile;->mTemp:Ljava/io/File;

    #@22
    .line 56
    .restart local v0       #result:Ljava/io/File;
    iget-object v1, p0, Lcom/android/internal/util/JournaledFile;->mTemp:Ljava/io/File;

    #@24
    iget-object v2, p0, Lcom/android/internal/util/JournaledFile;->mReal:Ljava/io/File;

    #@26
    invoke-virtual {v1, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@29
    goto :goto_17

    #@2a
    .line 58
    .end local v0           #result:Ljava/io/File;
    :cond_2a
    iget-object v0, p0, Lcom/android/internal/util/JournaledFile;->mReal:Ljava/io/File;

    #@2c
    goto :goto_17
.end method

.method public chooseForWrite()Ljava/io/File;
    .registers 3

    #@0
    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/android/internal/util/JournaledFile;->mWriting:Z

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 73
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string/jumbo v1, "uncommitted write already in progress"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 75
    :cond_d
    iget-object v0, p0, Lcom/android/internal/util/JournaledFile;->mReal:Ljava/io/File;

    #@f
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@12
    move-result v0

    #@13
    if-nez v0, :cond_1a

    #@15
    .line 82
    :try_start_15
    iget-object v0, p0, Lcom/android/internal/util/JournaledFile;->mReal:Ljava/io/File;

    #@17
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_1a} :catch_2d

    #@1a
    .line 88
    :cond_1a
    :goto_1a
    iget-object v0, p0, Lcom/android/internal/util/JournaledFile;->mTemp:Ljava/io/File;

    #@1c
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@1f
    move-result v0

    #@20
    if-eqz v0, :cond_27

    #@22
    .line 89
    iget-object v0, p0, Lcom/android/internal/util/JournaledFile;->mTemp:Ljava/io/File;

    #@24
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@27
    .line 91
    :cond_27
    const/4 v0, 0x1

    #@28
    iput-boolean v0, p0, Lcom/android/internal/util/JournaledFile;->mWriting:Z

    #@2a
    .line 92
    iget-object v0, p0, Lcom/android/internal/util/JournaledFile;->mTemp:Ljava/io/File;

    #@2c
    return-object v0

    #@2d
    .line 83
    :catch_2d
    move-exception v0

    #@2e
    goto :goto_1a
.end method

.method public commit()V
    .registers 3

    #@0
    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/android/internal/util/JournaledFile;->mWriting:Z

    #@2
    if-nez v0, :cond_d

    #@4
    .line 100
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string/jumbo v1, "no file to commit"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 102
    :cond_d
    const/4 v0, 0x0

    #@e
    iput-boolean v0, p0, Lcom/android/internal/util/JournaledFile;->mWriting:Z

    #@10
    .line 103
    iget-object v0, p0, Lcom/android/internal/util/JournaledFile;->mTemp:Ljava/io/File;

    #@12
    iget-object v1, p0, Lcom/android/internal/util/JournaledFile;->mReal:Ljava/io/File;

    #@14
    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@17
    .line 104
    return-void
.end method

.method public rollback()V
    .registers 3

    #@0
    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/android/internal/util/JournaledFile;->mWriting:Z

    #@2
    if-nez v0, :cond_d

    #@4
    .line 111
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string/jumbo v1, "no file to roll back"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 113
    :cond_d
    const/4 v0, 0x0

    #@e
    iput-boolean v0, p0, Lcom/android/internal/util/JournaledFile;->mWriting:Z

    #@10
    .line 114
    iget-object v0, p0, Lcom/android/internal/util/JournaledFile;->mTemp:Ljava/io/File;

    #@12
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@15
    .line 115
    return-void
.end method
