.class public Lcom/android/internal/util/State;
.super Ljava/lang/Object;
.source "State.java"

# interfaces
.implements Lcom/android/internal/util/IState;


# direct methods
.method protected constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 31
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 32
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 1

    #@0
    .prologue
    .line 39
    return-void
.end method

.method public exit()V
    .registers 1

    #@0
    .prologue
    .line 46
    return-void
.end method

.method public getName()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 70
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@3
    move-result-object v2

    #@4
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    .line 71
    .local v1, name:Ljava/lang/String;
    const/16 v2, 0x24

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    #@d
    move-result v0

    #@e
    .line 72
    .local v0, lastDollar:I
    add-int/lit8 v2, v0, 0x1

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    return-object v2
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 53
    const/4 v0, 0x0

    #@1
    return v0
.end method
