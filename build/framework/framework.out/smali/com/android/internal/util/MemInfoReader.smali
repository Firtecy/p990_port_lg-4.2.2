.class public Lcom/android/internal/util/MemInfoReader;
.super Ljava/lang/Object;
.source "MemInfoReader.java"


# instance fields
.field mBuffer:[B

.field private mCachedSize:J

.field private mFreeSize:J

.field private mTotalSize:J


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 23
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 24
    const/16 v0, 0x400

    #@5
    new-array v0, v0, [B

    #@7
    iput-object v0, p0, Lcom/android/internal/util/MemInfoReader;->mBuffer:[B

    #@9
    return-void
.end method

.method private extractMemValue([BI)J
    .registers 9
    .parameter "buffer"
    .parameter "index"

    #@0
    .prologue
    const/16 v5, 0x39

    #@2
    const/16 v4, 0x30

    #@4
    .line 44
    :goto_4
    array-length v2, p1

    #@5
    if-ge p2, v2, :cond_3a

    #@7
    aget-byte v2, p1, p2

    #@9
    const/16 v3, 0xa

    #@b
    if-eq v2, v3, :cond_3a

    #@d
    .line 45
    aget-byte v2, p1, p2

    #@f
    if-lt v2, v4, :cond_37

    #@11
    aget-byte v2, p1, p2

    #@13
    if-gt v2, v5, :cond_37

    #@15
    .line 46
    move v0, p2

    #@16
    .line 47
    .local v0, start:I
    add-int/lit8 p2, p2, 0x1

    #@18
    .line 49
    :goto_18
    array-length v2, p1

    #@19
    if-ge p2, v2, :cond_26

    #@1b
    aget-byte v2, p1, p2

    #@1d
    if-lt v2, v4, :cond_26

    #@1f
    aget-byte v2, p1, p2

    #@21
    if-gt v2, v5, :cond_26

    #@23
    .line 50
    add-int/lit8 p2, p2, 0x1

    #@25
    goto :goto_18

    #@26
    .line 52
    :cond_26
    new-instance v1, Ljava/lang/String;

    #@28
    const/4 v2, 0x0

    #@29
    sub-int v3, p2, v0

    #@2b
    invoke-direct {v1, p1, v2, v0, v3}, Ljava/lang/String;-><init>([BIII)V

    #@2e
    .line 53
    .local v1, str:Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@31
    move-result v2

    #@32
    int-to-long v2, v2

    #@33
    const-wide/16 v4, 0x400

    #@35
    mul-long/2addr v2, v4

    #@36
    .line 57
    .end local v0           #start:I
    .end local v1           #str:Ljava/lang/String;
    :goto_36
    return-wide v2

    #@37
    .line 55
    :cond_37
    add-int/lit8 p2, p2, 0x1

    #@39
    goto :goto_4

    #@3a
    .line 57
    :cond_3a
    const-wide/16 v2, 0x0

    #@3c
    goto :goto_36
.end method

.method private matchText([BILjava/lang/String;)Z
    .registers 9
    .parameter "buffer"
    .parameter "index"
    .parameter "text"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 31
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    #@4
    move-result v0

    #@5
    .line 32
    .local v0, N:I
    add-int v3, p2, v0

    #@7
    array-length v4, p1

    #@8
    if-lt v3, v4, :cond_b

    #@a
    .line 40
    :cond_a
    :goto_a
    return v2

    #@b
    .line 35
    :cond_b
    const/4 v1, 0x0

    #@c
    .local v1, i:I
    :goto_c
    if-ge v1, v0, :cond_1b

    #@e
    .line 36
    add-int v3, p2, v1

    #@10
    aget-byte v3, p1, v3

    #@12
    invoke-virtual {p3, v1}, Ljava/lang/String;->charAt(I)C

    #@15
    move-result v4

    #@16
    if-ne v3, v4, :cond_a

    #@18
    .line 35
    add-int/lit8 v1, v1, 0x1

    #@1a
    goto :goto_c

    #@1b
    .line 40
    :cond_1b
    const/4 v2, 0x1

    #@1c
    goto :goto_a
.end method


# virtual methods
.method public getCachedSize()J
    .registers 3

    #@0
    .prologue
    .line 108
    iget-wide v0, p0, Lcom/android/internal/util/MemInfoReader;->mCachedSize:J

    #@2
    return-wide v0
.end method

.method public getFreeSize()J
    .registers 3

    #@0
    .prologue
    .line 104
    iget-wide v0, p0, Lcom/android/internal/util/MemInfoReader;->mFreeSize:J

    #@2
    return-wide v0
.end method

.method public getTotalSize()J
    .registers 3

    #@0
    .prologue
    .line 100
    iget-wide v0, p0, Lcom/android/internal/util/MemInfoReader;->mTotalSize:J

    #@2
    return-wide v0
.end method

.method public readMemInfo()V
    .registers 9

    #@0
    .prologue
    .line 64
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    #@3
    move-result-object v5

    #@4
    .line 66
    .local v5, savedPolicy:Landroid/os/StrictMode$ThreadPolicy;
    const-wide/16 v6, 0x0

    #@6
    :try_start_6
    iput-wide v6, p0, Lcom/android/internal/util/MemInfoReader;->mTotalSize:J

    #@8
    .line 67
    const-wide/16 v6, 0x0

    #@a
    iput-wide v6, p0, Lcom/android/internal/util/MemInfoReader;->mFreeSize:J

    #@c
    .line 68
    const-wide/16 v6, 0x0

    #@e
    iput-wide v6, p0, Lcom/android/internal/util/MemInfoReader;->mCachedSize:J

    #@10
    .line 69
    new-instance v3, Ljava/io/FileInputStream;

    #@12
    const-string v6, "/proc/meminfo"

    #@14
    invoke-direct {v3, v6}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    #@17
    .line 70
    .local v3, is:Ljava/io/FileInputStream;
    iget-object v6, p0, Lcom/android/internal/util/MemInfoReader;->mBuffer:[B

    #@19
    invoke-virtual {v3, v6}, Ljava/io/FileInputStream;->read([B)I

    #@1c
    move-result v4

    #@1d
    .line 71
    .local v4, len:I
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    #@20
    .line 72
    iget-object v6, p0, Lcom/android/internal/util/MemInfoReader;->mBuffer:[B

    #@22
    array-length v0, v6

    #@23
    .line 73
    .local v0, BUFLEN:I
    const/4 v1, 0x0

    #@24
    .line 74
    .local v1, count:I
    const/4 v2, 0x0

    #@25
    .local v2, i:I
    :goto_25
    if-ge v2, v4, :cond_7e

    #@27
    const/4 v6, 0x3

    #@28
    if-ge v1, v6, :cond_7e

    #@2a
    .line 75
    iget-object v6, p0, Lcom/android/internal/util/MemInfoReader;->mBuffer:[B

    #@2c
    const-string v7, "MemTotal"

    #@2e
    invoke-direct {p0, v6, v2, v7}, Lcom/android/internal/util/MemInfoReader;->matchText([BILjava/lang/String;)Z

    #@31
    move-result v6

    #@32
    if-eqz v6, :cond_4d

    #@34
    .line 76
    add-int/lit8 v2, v2, 0x8

    #@36
    .line 77
    iget-object v6, p0, Lcom/android/internal/util/MemInfoReader;->mBuffer:[B

    #@38
    invoke-direct {p0, v6, v2}, Lcom/android/internal/util/MemInfoReader;->extractMemValue([BI)J

    #@3b
    move-result-wide v6

    #@3c
    iput-wide v6, p0, Lcom/android/internal/util/MemInfoReader;->mTotalSize:J

    #@3e
    .line 78
    add-int/lit8 v1, v1, 0x1

    #@40
    .line 88
    :cond_40
    :goto_40
    if-ge v2, v0, :cond_7b

    #@42
    iget-object v6, p0, Lcom/android/internal/util/MemInfoReader;->mBuffer:[B

    #@44
    aget-byte v6, v6, v2

    #@46
    const/16 v7, 0xa

    #@48
    if-eq v6, v7, :cond_7b

    #@4a
    .line 89
    add-int/lit8 v2, v2, 0x1

    #@4c
    goto :goto_40

    #@4d
    .line 79
    :cond_4d
    iget-object v6, p0, Lcom/android/internal/util/MemInfoReader;->mBuffer:[B

    #@4f
    const-string v7, "MemFree"

    #@51
    invoke-direct {p0, v6, v2, v7}, Lcom/android/internal/util/MemInfoReader;->matchText([BILjava/lang/String;)Z

    #@54
    move-result v6

    #@55
    if-eqz v6, :cond_64

    #@57
    .line 80
    add-int/lit8 v2, v2, 0x7

    #@59
    .line 81
    iget-object v6, p0, Lcom/android/internal/util/MemInfoReader;->mBuffer:[B

    #@5b
    invoke-direct {p0, v6, v2}, Lcom/android/internal/util/MemInfoReader;->extractMemValue([BI)J

    #@5e
    move-result-wide v6

    #@5f
    iput-wide v6, p0, Lcom/android/internal/util/MemInfoReader;->mFreeSize:J

    #@61
    .line 82
    add-int/lit8 v1, v1, 0x1

    #@63
    goto :goto_40

    #@64
    .line 83
    :cond_64
    iget-object v6, p0, Lcom/android/internal/util/MemInfoReader;->mBuffer:[B

    #@66
    const-string v7, "Cached"

    #@68
    invoke-direct {p0, v6, v2, v7}, Lcom/android/internal/util/MemInfoReader;->matchText([BILjava/lang/String;)Z

    #@6b
    move-result v6

    #@6c
    if-eqz v6, :cond_40

    #@6e
    .line 84
    add-int/lit8 v2, v2, 0x6

    #@70
    .line 85
    iget-object v6, p0, Lcom/android/internal/util/MemInfoReader;->mBuffer:[B

    #@72
    invoke-direct {p0, v6, v2}, Lcom/android/internal/util/MemInfoReader;->extractMemValue([BI)J

    #@75
    move-result-wide v6

    #@76
    iput-wide v6, p0, Lcom/android/internal/util/MemInfoReader;->mCachedSize:J
    :try_end_78
    .catchall {:try_start_6 .. :try_end_78} :catchall_8c
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_78} :catch_82
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_78} :catch_87

    #@78
    .line 86
    add-int/lit8 v1, v1, 0x1

    #@7a
    goto :goto_40

    #@7b
    .line 74
    :cond_7b
    add-int/lit8 v2, v2, 0x1

    #@7d
    goto :goto_25

    #@7e
    .line 95
    :cond_7e
    invoke-static {v5}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@81
    .line 97
    .end local v0           #BUFLEN:I
    .end local v1           #count:I
    .end local v2           #i:I
    .end local v3           #is:Ljava/io/FileInputStream;
    .end local v4           #len:I
    :goto_81
    return-void

    #@82
    .line 92
    :catch_82
    move-exception v6

    #@83
    .line 95
    invoke-static {v5}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@86
    goto :goto_81

    #@87
    .line 93
    :catch_87
    move-exception v6

    #@88
    .line 95
    invoke-static {v5}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@8b
    goto :goto_81

    #@8c
    :catchall_8c
    move-exception v6

    #@8d
    invoke-static {v5}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@90
    throw v6
.end method
