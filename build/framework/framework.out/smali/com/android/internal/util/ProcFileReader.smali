.class public Lcom/android/internal/util/ProcFileReader;
.super Ljava/lang/Object;
.source "ProcFileReader.java"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final mBuffer:[B

.field private mLineFinished:Z

.field private final mStream:Ljava/io/InputStream;

.field private mTail:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .registers 3
    .parameter "stream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 43
    const/16 v0, 0x1000

    #@2
    invoke-direct {p0, p1, v0}, Lcom/android/internal/util/ProcFileReader;-><init>(Ljava/io/InputStream;I)V

    #@5
    .line 44
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;I)V
    .registers 4
    .parameter "stream"
    .parameter "bufferSize"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 46
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 47
    iput-object p1, p0, Lcom/android/internal/util/ProcFileReader;->mStream:Ljava/io/InputStream;

    #@5
    .line 48
    new-array v0, p2, [B

    #@7
    iput-object v0, p0, Lcom/android/internal/util/ProcFileReader;->mBuffer:[B

    #@9
    .line 51
    invoke-direct {p0}, Lcom/android/internal/util/ProcFileReader;->fillBuf()I

    #@c
    .line 52
    return-void
.end method

.method private consumeBuf(I)V
    .registers 6
    .parameter "count"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 77
    iget-object v0, p0, Lcom/android/internal/util/ProcFileReader;->mBuffer:[B

    #@2
    iget-object v1, p0, Lcom/android/internal/util/ProcFileReader;->mBuffer:[B

    #@4
    const/4 v2, 0x0

    #@5
    iget v3, p0, Lcom/android/internal/util/ProcFileReader;->mTail:I

    #@7
    sub-int/2addr v3, p1

    #@8
    invoke-static {v0, p1, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@b
    .line 78
    iget v0, p0, Lcom/android/internal/util/ProcFileReader;->mTail:I

    #@d
    sub-int/2addr v0, p1

    #@e
    iput v0, p0, Lcom/android/internal/util/ProcFileReader;->mTail:I

    #@10
    .line 79
    iget v0, p0, Lcom/android/internal/util/ProcFileReader;->mTail:I

    #@12
    if-nez v0, :cond_17

    #@14
    .line 80
    invoke-direct {p0}, Lcom/android/internal/util/ProcFileReader;->fillBuf()I

    #@17
    .line 82
    :cond_17
    return-void
.end method

.method private fillBuf()I
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 58
    iget-object v2, p0, Lcom/android/internal/util/ProcFileReader;->mBuffer:[B

    #@2
    array-length v2, v2

    #@3
    iget v3, p0, Lcom/android/internal/util/ProcFileReader;->mTail:I

    #@5
    sub-int v0, v2, v3

    #@7
    .line 59
    .local v0, length:I
    if-nez v0, :cond_11

    #@9
    .line 60
    new-instance v2, Ljava/io/IOException;

    #@b
    const-string v3, "attempting to fill already-full buffer"

    #@d
    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@10
    throw v2

    #@11
    .line 63
    :cond_11
    iget-object v2, p0, Lcom/android/internal/util/ProcFileReader;->mStream:Ljava/io/InputStream;

    #@13
    iget-object v3, p0, Lcom/android/internal/util/ProcFileReader;->mBuffer:[B

    #@15
    iget v4, p0, Lcom/android/internal/util/ProcFileReader;->mTail:I

    #@17
    invoke-virtual {v2, v3, v4, v0}, Ljava/io/InputStream;->read([BII)I

    #@1a
    move-result v1

    #@1b
    .line 64
    .local v1, read:I
    const/4 v2, -0x1

    #@1c
    if-eq v1, v2, :cond_23

    #@1e
    .line 65
    iget v2, p0, Lcom/android/internal/util/ProcFileReader;->mTail:I

    #@20
    add-int/2addr v2, v1

    #@21
    iput v2, p0, Lcom/android/internal/util/ProcFileReader;->mTail:I

    #@23
    .line 67
    :cond_23
    return v1
.end method

.method private invalidLong(I)Ljava/lang/NumberFormatException;
    .registers 8
    .parameter "tokenIndex"

    #@0
    .prologue
    .line 181
    new-instance v0, Ljava/lang/NumberFormatException;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "invalid long: "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    new-instance v2, Ljava/lang/String;

    #@f
    iget-object v3, p0, Lcom/android/internal/util/ProcFileReader;->mBuffer:[B

    #@11
    const/4 v4, 0x0

    #@12
    sget-object v5, Ljava/nio/charset/Charsets;->US_ASCII:Ljava/nio/charset/Charset;

    #@14
    invoke-direct {v2, v3, v4, p1, v5}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    #@22
    return-object v0
.end method

.method private nextTokenIndex()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 89
    iget-boolean v2, p0, Lcom/android/internal/util/ProcFileReader;->mLineFinished:Z

    #@2
    if-eqz v2, :cond_d

    #@4
    .line 90
    new-instance v2, Ljava/io/IOException;

    #@6
    const-string/jumbo v3, "no tokens remaining on current line"

    #@9
    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@c
    throw v2

    #@d
    .line 93
    :cond_d
    const/4 v1, 0x0

    #@e
    .line 96
    .local v1, i:I
    :cond_e
    :goto_e
    iget v2, p0, Lcom/android/internal/util/ProcFileReader;->mTail:I

    #@10
    if-ge v1, v2, :cond_25

    #@12
    .line 97
    iget-object v2, p0, Lcom/android/internal/util/ProcFileReader;->mBuffer:[B

    #@14
    aget-byte v0, v2, v1

    #@16
    .line 98
    .local v0, b:B
    const/16 v2, 0xa

    #@18
    if-ne v0, v2, :cond_1e

    #@1a
    .line 99
    const/4 v2, 0x1

    #@1b
    iput-boolean v2, p0, Lcom/android/internal/util/ProcFileReader;->mLineFinished:Z

    #@1d
    .line 103
    :cond_1d
    return v1

    #@1e
    .line 102
    :cond_1e
    const/16 v2, 0x20

    #@20
    if-eq v0, v2, :cond_1d

    #@22
    .line 96
    add-int/lit8 v1, v1, 0x1

    #@24
    goto :goto_e

    #@25
    .line 106
    .end local v0           #b:B
    :cond_25
    invoke-direct {p0}, Lcom/android/internal/util/ProcFileReader;->fillBuf()I

    #@28
    move-result v2

    #@29
    if-gtz v2, :cond_e

    #@2b
    .line 108
    new-instance v2, Ljava/io/IOException;

    #@2d
    const-string v3, "end of stream while looking for token boundary"

    #@2f
    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@32
    throw v2
.end method


# virtual methods
.method public close()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 197
    iget-object v0, p0, Lcom/android/internal/util/ProcFileReader;->mStream:Ljava/io/InputStream;

    #@2
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    #@5
    .line 198
    return-void
.end method

.method public finishLine()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 123
    iget-boolean v1, p0, Lcom/android/internal/util/ProcFileReader;->mLineFinished:Z

    #@2
    if-eqz v1, :cond_8

    #@4
    .line 124
    const/4 v1, 0x0

    #@5
    iput-boolean v1, p0, Lcom/android/internal/util/ProcFileReader;->mLineFinished:Z

    #@7
    .line 134
    :goto_7
    return-void

    #@8
    .line 128
    :cond_8
    const/4 v0, 0x0

    #@9
    .line 131
    .local v0, i:I
    :cond_9
    :goto_9
    iget v1, p0, Lcom/android/internal/util/ProcFileReader;->mTail:I

    #@b
    if-ge v0, v1, :cond_1e

    #@d
    .line 132
    iget-object v1, p0, Lcom/android/internal/util/ProcFileReader;->mBuffer:[B

    #@f
    aget-byte v1, v1, v0

    #@11
    const/16 v2, 0xa

    #@13
    if-ne v1, v2, :cond_1b

    #@15
    .line 133
    add-int/lit8 v1, v0, 0x1

    #@17
    invoke-direct {p0, v1}, Lcom/android/internal/util/ProcFileReader;->consumeBuf(I)V

    #@1a
    goto :goto_7

    #@1b
    .line 131
    :cond_1b
    add-int/lit8 v0, v0, 0x1

    #@1d
    goto :goto_9

    #@1e
    .line 137
    :cond_1e
    invoke-direct {p0}, Lcom/android/internal/util/ProcFileReader;->fillBuf()I

    #@21
    move-result v1

    #@22
    if-gtz v1, :cond_9

    #@24
    .line 139
    new-instance v1, Ljava/io/IOException;

    #@26
    const-string v2, "end of stream while looking for line boundary"

    #@28
    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@2b
    throw v1
.end method

.method public hasMoreData()Z
    .registers 2

    #@0
    .prologue
    .line 115
    iget v0, p0, Lcom/android/internal/util/ProcFileReader;->mTail:I

    #@2
    if-lez v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public nextInt()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 189
    invoke-virtual {p0}, Lcom/android/internal/util/ProcFileReader;->nextLong()J

    #@3
    move-result-wide v0

    #@4
    .line 190
    .local v0, value:J
    const-wide/32 v2, 0x7fffffff

    #@7
    cmp-long v2, v0, v2

    #@9
    if-gtz v2, :cond_12

    #@b
    const-wide/32 v2, -0x80000000

    #@e
    cmp-long v2, v0, v2

    #@10
    if-gez v2, :cond_1b

    #@12
    .line 191
    :cond_12
    new-instance v2, Ljava/lang/NumberFormatException;

    #@14
    const-string/jumbo v3, "parsed value larger than integer"

    #@17
    invoke-direct {v2, v3}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v2

    #@1b
    .line 193
    :cond_1b
    long-to-int v2, v0

    #@1c
    return v2
.end method

.method public nextLong()J
    .registers 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 156
    invoke-direct {p0}, Lcom/android/internal/util/ProcFileReader;->nextTokenIndex()I

    #@5
    move-result v7

    #@6
    .line 157
    .local v7, tokenIndex:I
    iget-object v9, p0, Lcom/android/internal/util/ProcFileReader;->mBuffer:[B

    #@8
    aget-byte v9, v9, v8

    #@a
    const/16 v10, 0x2d

    #@c
    if-ne v9, v10, :cond_26

    #@e
    move v2, v1

    #@f
    .line 160
    .local v2, negative:Z
    :goto_f
    const-wide/16 v5, 0x0

    #@11
    .line 161
    .local v5, result:J
    if-eqz v2, :cond_28

    #@13
    .local v1, i:I
    :goto_13
    if-ge v1, v7, :cond_3d

    #@15
    .line 162
    iget-object v8, p0, Lcom/android/internal/util/ProcFileReader;->mBuffer:[B

    #@17
    aget-byte v8, v8, v1

    #@19
    add-int/lit8 v0, v8, -0x30

    #@1b
    .line 163
    .local v0, digit:I
    if-ltz v0, :cond_21

    #@1d
    const/16 v8, 0x9

    #@1f
    if-le v0, v8, :cond_2a

    #@21
    .line 164
    :cond_21
    invoke-direct {p0, v7}, Lcom/android/internal/util/ProcFileReader;->invalidLong(I)Ljava/lang/NumberFormatException;

    #@24
    move-result-object v8

    #@25
    throw v8

    #@26
    .end local v0           #digit:I
    .end local v1           #i:I
    .end local v2           #negative:Z
    .end local v5           #result:J
    :cond_26
    move v2, v8

    #@27
    .line 157
    goto :goto_f

    #@28
    .restart local v2       #negative:Z
    .restart local v5       #result:J
    :cond_28
    move v1, v8

    #@29
    .line 161
    goto :goto_13

    #@2a
    .line 169
    .restart local v0       #digit:I
    .restart local v1       #i:I
    :cond_2a
    const-wide/16 v8, 0xa

    #@2c
    mul-long/2addr v8, v5

    #@2d
    int-to-long v10, v0

    #@2e
    sub-long v3, v8, v10

    #@30
    .line 170
    .local v3, next:J
    cmp-long v8, v3, v5

    #@32
    if-lez v8, :cond_39

    #@34
    .line 171
    invoke-direct {p0, v7}, Lcom/android/internal/util/ProcFileReader;->invalidLong(I)Ljava/lang/NumberFormatException;

    #@37
    move-result-object v8

    #@38
    throw v8

    #@39
    .line 173
    :cond_39
    move-wide v5, v3

    #@3a
    .line 161
    add-int/lit8 v1, v1, 0x1

    #@3c
    goto :goto_13

    #@3d
    .line 176
    .end local v0           #digit:I
    .end local v3           #next:J
    :cond_3d
    add-int/lit8 v8, v7, 0x1

    #@3f
    invoke-direct {p0, v8}, Lcom/android/internal/util/ProcFileReader;->consumeBuf(I)V

    #@42
    .line 177
    if-eqz v2, :cond_45

    #@44
    .end local v5           #result:J
    :goto_44
    return-wide v5

    #@45
    .restart local v5       #result:J
    :cond_45
    neg-long v5, v5

    #@46
    goto :goto_44
.end method

.method public nextString()Ljava/lang/String;
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 146
    invoke-direct {p0}, Lcom/android/internal/util/ProcFileReader;->nextTokenIndex()I

    #@3
    move-result v1

    #@4
    .line 147
    .local v1, tokenIndex:I
    new-instance v0, Ljava/lang/String;

    #@6
    iget-object v2, p0, Lcom/android/internal/util/ProcFileReader;->mBuffer:[B

    #@8
    const/4 v3, 0x0

    #@9
    sget-object v4, Ljava/nio/charset/Charsets;->US_ASCII:Ljava/nio/charset/Charset;

    #@b
    invoke-direct {v0, v2, v3, v1, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    #@e
    .line 148
    .local v0, s:Ljava/lang/String;
    add-int/lit8 v2, v1, 0x1

    #@10
    invoke-direct {p0, v2}, Lcom/android/internal/util/ProcFileReader;->consumeBuf(I)V

    #@13
    .line 149
    return-object v0
.end method
