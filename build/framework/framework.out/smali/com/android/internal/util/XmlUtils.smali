.class public Lcom/android/internal/util/XmlUtils;
.super Ljava/lang/Object;
.source "XmlUtils.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 38
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static final beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V
    .registers 6
    .parameter "parser"
    .parameter "firstElementName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    .line 869
    :cond_1
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@4
    move-result v0

    #@5
    .local v0, type:I
    if-eq v0, v2, :cond_a

    #@7
    const/4 v1, 0x1

    #@8
    if-ne v0, v1, :cond_1

    #@a
    .line 873
    :cond_a
    if-eq v0, v2, :cond_14

    #@c
    .line 874
    new-instance v1, Lorg/xmlpull/v1/XmlPullParserException;

    #@e
    const-string v2, "No start tag found"

    #@10
    invoke-direct {v1, v2}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@13
    throw v1

    #@14
    .line 877
    :cond_14
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v1

    #@1c
    if-nez v1, :cond_45

    #@1e
    .line 878
    new-instance v1, Lorg/xmlpull/v1/XmlPullParserException;

    #@20
    new-instance v2, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v3, "Unexpected start tag: found "

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    const-string v3, ", expected "

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v2

    #@41
    invoke-direct {v1, v2}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@44
    throw v1

    #@45
    .line 881
    :cond_45
    return-void
.end method

.method public static final convertValueToBoolean(Ljava/lang/CharSequence;Z)Z
    .registers 4
    .parameter "value"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 67
    const/4 v0, 0x0

    #@1
    .line 69
    .local v0, result:Z
    if-nez p0, :cond_4

    #@3
    .line 77
    .end local p1
    :goto_3
    return p1

    #@4
    .line 72
    .restart local p1
    :cond_4
    const-string v1, "1"

    #@6
    invoke-virtual {p0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_1d

    #@c
    const-string/jumbo v1, "true"

    #@f
    invoke-virtual {p0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v1

    #@13
    if-nez v1, :cond_1d

    #@15
    const-string v1, "TRUE"

    #@17
    invoke-virtual {p0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@1a
    move-result v1

    #@1b
    if-eqz v1, :cond_1e

    #@1d
    .line 75
    :cond_1d
    const/4 v0, 0x1

    #@1e
    :cond_1e
    move p1, v0

    #@1f
    .line 77
    goto :goto_3
.end method

.method public static final convertValueToInt(Ljava/lang/CharSequence;I)I
    .registers 11
    .parameter "charSeq"
    .parameter "defaultValue"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 83
    if-nez p0, :cond_4

    #@3
    .line 123
    .end local p1
    :goto_3
    return p1

    #@4
    .line 86
    .restart local p1
    :cond_4
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@7
    move-result-object v4

    #@8
    .line 92
    .local v4, nm:Ljava/lang/String;
    const/4 v5, 0x1

    #@9
    .line 93
    .local v5, sign:I
    const/4 v2, 0x0

    #@a
    .line 94
    .local v2, index:I
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@d
    move-result v3

    #@e
    .line 95
    .local v3, len:I
    const/16 v0, 0xa

    #@10
    .line 97
    .local v0, base:I
    const/16 v7, 0x2d

    #@12
    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    #@15
    move-result v8

    #@16
    if-ne v7, v8, :cond_1b

    #@18
    .line 98
    const/4 v5, -0x1

    #@19
    .line 99
    add-int/lit8 v2, v2, 0x1

    #@1b
    .line 102
    :cond_1b
    const/16 v7, 0x30

    #@1d
    invoke-virtual {v4, v2}, Ljava/lang/String;->charAt(I)C

    #@20
    move-result v8

    #@21
    if-ne v7, v8, :cond_4b

    #@23
    .line 104
    add-int/lit8 v7, v3, -0x1

    #@25
    if-ne v2, v7, :cond_29

    #@27
    move p1, v6

    #@28
    .line 105
    goto :goto_3

    #@29
    .line 107
    :cond_29
    add-int/lit8 v6, v2, 0x1

    #@2b
    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    #@2e
    move-result v1

    #@2f
    .line 109
    .local v1, c:C
    const/16 v6, 0x78

    #@31
    if-eq v6, v1, :cond_37

    #@33
    const/16 v6, 0x58

    #@35
    if-ne v6, v1, :cond_46

    #@37
    .line 110
    :cond_37
    add-int/lit8 v2, v2, 0x2

    #@39
    .line 111
    const/16 v0, 0x10

    #@3b
    .line 123
    .end local v1           #c:C
    :cond_3b
    :goto_3b
    invoke-virtual {v4, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@3e
    move-result-object v6

    #@3f
    invoke-static {v6, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@42
    move-result v6

    #@43
    mul-int p1, v6, v5

    #@45
    goto :goto_3

    #@46
    .line 113
    .restart local v1       #c:C
    :cond_46
    add-int/lit8 v2, v2, 0x1

    #@48
    .line 114
    const/16 v0, 0x8

    #@4a
    goto :goto_3b

    #@4b
    .line 117
    .end local v1           #c:C
    :cond_4b
    const/16 v6, 0x23

    #@4d
    invoke-virtual {v4, v2}, Ljava/lang/String;->charAt(I)C

    #@50
    move-result v7

    #@51
    if-ne v6, v7, :cond_3b

    #@53
    .line 119
    add-int/lit8 v2, v2, 0x1

    #@55
    .line 120
    const/16 v0, 0x10

    #@57
    goto :goto_3b
.end method

.method public static final convertValueToList(Ljava/lang/CharSequence;[Ljava/lang/String;I)I
    .registers 5
    .parameter "value"
    .parameter "options"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 54
    if-eqz p0, :cond_12

    #@2
    .line 55
    const/4 v0, 0x0

    #@3
    .local v0, i:I
    :goto_3
    array-length v1, p1

    #@4
    if-ge v0, v1, :cond_12

    #@6
    .line 56
    aget-object v1, p1, v0

    #@8
    invoke-virtual {p0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_f

    #@e
    .line 61
    .end local v0           #i:I
    :goto_e
    return v0

    #@f
    .line 55
    .restart local v0       #i:I
    :cond_f
    add-int/lit8 v0, v0, 0x1

    #@11
    goto :goto_3

    #@12
    .end local v0           #i:I
    :cond_12
    move v0, p2

    #@13
    .line 61
    goto :goto_e
.end method

.method public static final convertValueToUnsignedInt(Ljava/lang/String;I)I
    .registers 2
    .parameter "value"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 129
    if-nez p0, :cond_3

    #@2
    .line 132
    .end local p1
    :goto_2
    return p1

    #@3
    .restart local p1
    :cond_3
    invoke-static {p0}, Lcom/android/internal/util/XmlUtils;->parseUnsignedIntAttribute(Ljava/lang/CharSequence;)I

    #@6
    move-result p1

    #@7
    goto :goto_2
.end method

.method public static final nextElement(Lorg/xmlpull/v1/XmlPullParser;)V
    .registers 3
    .parameter "parser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 887
    :cond_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@3
    move-result v0

    #@4
    .local v0, type:I
    const/4 v1, 0x2

    #@5
    if-eq v0, v1, :cond_a

    #@7
    const/4 v1, 0x1

    #@8
    if-ne v0, v1, :cond_0

    #@a
    .line 890
    :cond_a
    return-void
.end method

.method public static nextElementWithin(Lorg/xmlpull/v1/XmlPullParser;I)Z
    .registers 6
    .parameter "parser"
    .parameter "outerDepth"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 895
    :cond_1
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@4
    move-result v0

    #@5
    .line 896
    .local v0, type:I
    if-eq v0, v1, :cond_10

    #@7
    const/4 v2, 0x3

    #@8
    if-ne v0, v2, :cond_12

    #@a
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@d
    move-result v2

    #@e
    if-ne v2, p1, :cond_12

    #@10
    .line 898
    :cond_10
    const/4 v1, 0x0

    #@11
    .line 902
    :goto_11
    return v1

    #@12
    .line 900
    :cond_12
    const/4 v2, 0x2

    #@13
    if-ne v0, v2, :cond_1

    #@15
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@18
    move-result v2

    #@19
    add-int/lit8 v3, p1, 0x1

    #@1b
    if-ne v2, v3, :cond_1

    #@1d
    goto :goto_11
.end method

.method public static final parseUnsignedIntAttribute(Ljava/lang/CharSequence;)I
    .registers 8
    .parameter "charSeq"

    #@0
    .prologue
    .line 138
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@3
    move-result-object v4

    #@4
    .line 141
    .local v4, value:Ljava/lang/String;
    const/4 v2, 0x0

    #@5
    .line 142
    .local v2, index:I
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@8
    move-result v3

    #@9
    .line 143
    .local v3, len:I
    const/16 v0, 0xa

    #@b
    .line 145
    .local v0, base:I
    const/16 v5, 0x30

    #@d
    invoke-virtual {v4, v2}, Ljava/lang/String;->charAt(I)C

    #@10
    move-result v6

    #@11
    if-ne v5, v6, :cond_39

    #@13
    .line 147
    add-int/lit8 v5, v3, -0x1

    #@15
    if-ne v2, v5, :cond_19

    #@17
    .line 148
    const/4 v5, 0x0

    #@18
    .line 164
    :goto_18
    return v5

    #@19
    .line 150
    :cond_19
    const/4 v5, 0x1

    #@1a
    invoke-virtual {v4, v5}, Ljava/lang/String;->charAt(I)C

    #@1d
    move-result v1

    #@1e
    .line 152
    .local v1, c:C
    const/16 v5, 0x78

    #@20
    if-eq v5, v1, :cond_26

    #@22
    const/16 v5, 0x58

    #@24
    if-ne v5, v1, :cond_34

    #@26
    .line 153
    :cond_26
    add-int/lit8 v2, v2, 0x2

    #@28
    .line 154
    const/16 v0, 0x10

    #@2a
    .line 164
    .end local v1           #c:C
    :cond_2a
    :goto_2a
    invoke-virtual {v4, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@2d
    move-result-object v5

    #@2e
    invoke-static {v5, v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    #@31
    move-result-wide v5

    #@32
    long-to-int v5, v5

    #@33
    goto :goto_18

    #@34
    .line 156
    .restart local v1       #c:C
    :cond_34
    add-int/lit8 v2, v2, 0x1

    #@36
    .line 157
    const/16 v0, 0x8

    #@38
    goto :goto_2a

    #@39
    .line 159
    .end local v1           #c:C
    :cond_39
    const/16 v5, 0x23

    #@3b
    invoke-virtual {v4, v2}, Ljava/lang/String;->charAt(I)C

    #@3e
    move-result v6

    #@3f
    if-ne v5, v6, :cond_2a

    #@41
    .line 160
    add-int/lit8 v2, v2, 0x1

    #@43
    .line 161
    const/16 v0, 0x10

    #@45
    goto :goto_2a
.end method

.method public static final readListXml(Ljava/io/InputStream;)Ljava/util/ArrayList;
    .registers 3
    .parameter "in"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 513
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@3
    move-result-object v0

    #@4
    .line 514
    .local v0, parser:Lorg/xmlpull/v1/XmlPullParser;
    const/4 v1, 0x0

    #@5
    invoke-interface {v0, p0, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    #@8
    .line 515
    const/4 v1, 0x1

    #@9
    new-array v1, v1, [Ljava/lang/String;

    #@b
    invoke-static {v0, v1}, Lcom/android/internal/util/XmlUtils;->readValueXml(Lorg/xmlpull/v1/XmlPullParser;[Ljava/lang/String;)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Ljava/util/ArrayList;

    #@11
    return-object v1
.end method

.method public static final readMapXml(Ljava/io/InputStream;)Ljava/util/HashMap;
    .registers 3
    .parameter "in"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 492
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@3
    move-result-object v0

    #@4
    .line 493
    .local v0, parser:Lorg/xmlpull/v1/XmlPullParser;
    const/4 v1, 0x0

    #@5
    invoke-interface {v0, p0, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    #@8
    .line 494
    const/4 v1, 0x1

    #@9
    new-array v1, v1, [Ljava/lang/String;

    #@b
    invoke-static {v0, v1}, Lcom/android/internal/util/XmlUtils;->readValueXml(Lorg/xmlpull/v1/XmlPullParser;[Ljava/lang/String;)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Ljava/util/HashMap;

    #@11
    return-object v1
.end method

.method public static final readSetXml(Ljava/io/InputStream;)Ljava/util/HashSet;
    .registers 3
    .parameter "in"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 536
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@3
    move-result-object v0

    #@4
    .line 537
    .local v0, parser:Lorg/xmlpull/v1/XmlPullParser;
    const/4 v1, 0x0

    #@5
    invoke-interface {v0, p0, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    #@8
    .line 538
    const/4 v1, 0x1

    #@9
    new-array v1, v1, [Ljava/lang/String;

    #@b
    invoke-static {v0, v1}, Lcom/android/internal/util/XmlUtils;->readValueXml(Lorg/xmlpull/v1/XmlPullParser;[Ljava/lang/String;)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Ljava/util/HashSet;

    #@11
    return-object v1
.end method

.method public static final readThisIntArrayXml(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;[Ljava/lang/String;)[I
    .registers 11
    .parameter "parser"
    .parameter "endTag"
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 685
    const/4 v5, 0x0

    #@1
    :try_start_1
    const-string/jumbo v6, "num"

    #@4
    invoke-interface {p0, v5, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v5

    #@8
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_b
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_b} :catch_57
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_b} :catch_60

    #@b
    move-result v4

    #@c
    .line 694
    .local v4, num:I
    new-array v0, v4, [I

    #@e
    .line 695
    .local v0, array:[I
    const/4 v3, 0x0

    #@f
    .line 697
    .local v3, i:I
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    #@12
    move-result v2

    #@13
    .line 699
    .local v2, eventType:I
    :cond_13
    const/4 v5, 0x2

    #@14
    if-ne v2, v5, :cond_98

    #@16
    .line 700
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@19
    move-result-object v5

    #@1a
    const-string/jumbo v6, "item"

    #@1d
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v5

    #@21
    if-eqz v5, :cond_7b

    #@23
    .line 702
    const/4 v5, 0x0

    #@24
    :try_start_24
    const-string/jumbo v6, "value"

    #@27
    invoke-interface {p0, v5, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2a
    move-result-object v5

    #@2b
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@2e
    move-result v5

    #@2f
    aput v5, v0, v3
    :try_end_31
    .catch Ljava/lang/NullPointerException; {:try_start_24 .. :try_end_31} :catch_69
    .catch Ljava/lang/NumberFormatException; {:try_start_24 .. :try_end_31} :catch_72

    #@31
    .line 726
    :cond_31
    :goto_31
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@34
    move-result v2

    #@35
    .line 727
    const/4 v5, 0x1

    #@36
    if-ne v2, v5, :cond_13

    #@38
    .line 729
    new-instance v5, Lorg/xmlpull/v1/XmlPullParserException;

    #@3a
    new-instance v6, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v7, "Document ended before "

    #@41
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v6

    #@45
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v6

    #@49
    const-string v7, " end tag"

    #@4b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v6

    #@4f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v6

    #@53
    invoke-direct {v5, v6}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@56
    throw v5

    #@57
    .line 686
    .end local v0           #array:[I
    .end local v2           #eventType:I
    .end local v3           #i:I
    .end local v4           #num:I
    :catch_57
    move-exception v1

    #@58
    .line 687
    .local v1, e:Ljava/lang/NullPointerException;
    new-instance v5, Lorg/xmlpull/v1/XmlPullParserException;

    #@5a
    const-string v6, "Need num attribute in byte-array"

    #@5c
    invoke-direct {v5, v6}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@5f
    throw v5

    #@60
    .line 689
    .end local v1           #e:Ljava/lang/NullPointerException;
    :catch_60
    move-exception v1

    #@61
    .line 690
    .local v1, e:Ljava/lang/NumberFormatException;
    new-instance v5, Lorg/xmlpull/v1/XmlPullParserException;

    #@63
    const-string v6, "Not a number in num attribute in byte-array"

    #@65
    invoke-direct {v5, v6}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@68
    throw v5

    #@69
    .line 704
    .end local v1           #e:Ljava/lang/NumberFormatException;
    .restart local v0       #array:[I
    .restart local v2       #eventType:I
    .restart local v3       #i:I
    .restart local v4       #num:I
    :catch_69
    move-exception v1

    #@6a
    .line 705
    .local v1, e:Ljava/lang/NullPointerException;
    new-instance v5, Lorg/xmlpull/v1/XmlPullParserException;

    #@6c
    const-string v6, "Need value attribute in item"

    #@6e
    invoke-direct {v5, v6}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@71
    throw v5

    #@72
    .line 707
    .end local v1           #e:Ljava/lang/NullPointerException;
    :catch_72
    move-exception v1

    #@73
    .line 708
    .local v1, e:Ljava/lang/NumberFormatException;
    new-instance v5, Lorg/xmlpull/v1/XmlPullParserException;

    #@75
    const-string v6, "Not a number in value attribute in item"

    #@77
    invoke-direct {v5, v6}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@7a
    throw v5

    #@7b
    .line 712
    .end local v1           #e:Ljava/lang/NumberFormatException;
    :cond_7b
    new-instance v5, Lorg/xmlpull/v1/XmlPullParserException;

    #@7d
    new-instance v6, Ljava/lang/StringBuilder;

    #@7f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@82
    const-string v7, "Expected item tag at: "

    #@84
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v6

    #@88
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@8b
    move-result-object v7

    #@8c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v6

    #@90
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@93
    move-result-object v6

    #@94
    invoke-direct {v5, v6}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@97
    throw v5

    #@98
    .line 715
    :cond_98
    const/4 v5, 0x3

    #@99
    if-ne v2, v5, :cond_31

    #@9b
    .line 716
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@9e
    move-result-object v5

    #@9f
    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a2
    move-result v5

    #@a3
    if-eqz v5, :cond_a6

    #@a5
    .line 717
    return-object v0

    #@a6
    .line 718
    :cond_a6
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@a9
    move-result-object v5

    #@aa
    const-string/jumbo v6, "item"

    #@ad
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b0
    move-result v5

    #@b1
    if-eqz v5, :cond_b7

    #@b3
    .line 719
    add-int/lit8 v3, v3, 0x1

    #@b5
    goto/16 :goto_31

    #@b7
    .line 721
    :cond_b7
    new-instance v5, Lorg/xmlpull/v1/XmlPullParserException;

    #@b9
    new-instance v6, Ljava/lang/StringBuilder;

    #@bb
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@be
    const-string v7, "Expected "

    #@c0
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v6

    #@c4
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v6

    #@c8
    const-string v7, " end tag at: "

    #@ca
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v6

    #@ce
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@d1
    move-result-object v7

    #@d2
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v6

    #@d6
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d9
    move-result-object v6

    #@da
    invoke-direct {v5, v6}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@dd
    throw v5
.end method

.method public static final readThisListXml(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 9
    .parameter "parser"
    .parameter "endTag"
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 602
    new-instance v1, Ljava/util/ArrayList;

    #@2
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 604
    .local v1, list:Ljava/util/ArrayList;
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    #@8
    move-result v0

    #@9
    .line 606
    .local v0, eventType:I
    :cond_9
    const/4 v3, 0x2

    #@a
    if-ne v0, v3, :cond_39

    #@c
    .line 607
    invoke-static {p0, p2}, Lcom/android/internal/util/XmlUtils;->readThisValueXml(Lorg/xmlpull/v1/XmlPullParser;[Ljava/lang/String;)Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    .line 608
    .local v2, val:Ljava/lang/Object;
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@13
    .line 617
    .end local v2           #val:Ljava/lang/Object;
    :cond_13
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@16
    move-result v0

    #@17
    .line 618
    const/4 v3, 0x1

    #@18
    if-ne v0, v3, :cond_9

    #@1a
    .line 620
    new-instance v3, Lorg/xmlpull/v1/XmlPullParserException;

    #@1c
    new-instance v4, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v5, "Document ended before "

    #@23
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v4

    #@27
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    const-string v5, " end tag"

    #@2d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v4

    #@35
    invoke-direct {v3, v4}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@38
    throw v3

    #@39
    .line 610
    :cond_39
    const/4 v3, 0x3

    #@3a
    if-ne v0, v3, :cond_13

    #@3c
    .line 611
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@43
    move-result v3

    #@44
    if-eqz v3, :cond_47

    #@46
    .line 612
    return-object v1

    #@47
    .line 614
    :cond_47
    new-instance v3, Lorg/xmlpull/v1/XmlPullParserException;

    #@49
    new-instance v4, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string v5, "Expected "

    #@50
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v4

    #@54
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v4

    #@58
    const-string v5, " end tag at: "

    #@5a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v4

    #@5e
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@61
    move-result-object v5

    #@62
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v4

    #@66
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v4

    #@6a
    invoke-direct {v3, v4}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@6d
    throw v3
.end method

.method public static final readThisMapXml(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/HashMap;
    .registers 9
    .parameter "parser"
    .parameter "endTag"
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 558
    new-instance v1, Ljava/util/HashMap;

    #@3
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@6
    .line 560
    .local v1, map:Ljava/util/HashMap;
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    #@9
    move-result v0

    #@a
    .line 562
    .local v0, eventType:I
    :cond_a
    const/4 v3, 0x2

    #@b
    if-ne v0, v3, :cond_5d

    #@d
    .line 563
    invoke-static {p0, p2}, Lcom/android/internal/util/XmlUtils;->readThisValueXml(Lorg/xmlpull/v1/XmlPullParser;[Ljava/lang/String;)Ljava/lang/Object;

    #@10
    move-result-object v2

    #@11
    .line 564
    .local v2, val:Ljava/lang/Object;
    aget-object v3, p2, v4

    #@13
    if-eqz v3, :cond_40

    #@15
    .line 566
    aget-object v3, p2, v4

    #@17
    invoke-virtual {v1, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1a
    .line 578
    .end local v2           #val:Ljava/lang/Object;
    :cond_1a
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@1d
    move-result v0

    #@1e
    .line 579
    const/4 v3, 0x1

    #@1f
    if-ne v0, v3, :cond_a

    #@21
    .line 581
    new-instance v3, Lorg/xmlpull/v1/XmlPullParserException;

    #@23
    new-instance v4, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v5, "Document ended before "

    #@2a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v4

    #@2e
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v4

    #@32
    const-string v5, " end tag"

    #@34
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v4

    #@38
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v4

    #@3c
    invoke-direct {v3, v4}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@3f
    throw v3

    #@40
    .line 568
    .restart local v2       #val:Ljava/lang/Object;
    :cond_40
    new-instance v3, Lorg/xmlpull/v1/XmlPullParserException;

    #@42
    new-instance v4, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v5, "Map value without name attribute: "

    #@49
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v4

    #@4d
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@50
    move-result-object v5

    #@51
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v4

    #@55
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v4

    #@59
    invoke-direct {v3, v4}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@5c
    throw v3

    #@5d
    .line 571
    .end local v2           #val:Ljava/lang/Object;
    :cond_5d
    const/4 v3, 0x3

    #@5e
    if-ne v0, v3, :cond_1a

    #@60
    .line 572
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@63
    move-result-object v3

    #@64
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@67
    move-result v3

    #@68
    if-eqz v3, :cond_6b

    #@6a
    .line 573
    return-object v1

    #@6b
    .line 575
    :cond_6b
    new-instance v3, Lorg/xmlpull/v1/XmlPullParserException;

    #@6d
    new-instance v4, Ljava/lang/StringBuilder;

    #@6f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@72
    const-string v5, "Expected "

    #@74
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v4

    #@78
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v4

    #@7c
    const-string v5, " end tag at: "

    #@7e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v4

    #@82
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@85
    move-result-object v5

    #@86
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v4

    #@8a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8d
    move-result-object v4

    #@8e
    invoke-direct {v3, v4}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@91
    throw v3
.end method

.method public static final readThisSetXml(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/HashSet;
    .registers 9
    .parameter "parser"
    .parameter "endTag"
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 643
    new-instance v1, Ljava/util/HashSet;

    #@2
    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    #@5
    .line 645
    .local v1, set:Ljava/util/HashSet;
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    #@8
    move-result v0

    #@9
    .line 647
    .local v0, eventType:I
    :cond_9
    const/4 v3, 0x2

    #@a
    if-ne v0, v3, :cond_39

    #@c
    .line 648
    invoke-static {p0, p2}, Lcom/android/internal/util/XmlUtils;->readThisValueXml(Lorg/xmlpull/v1/XmlPullParser;[Ljava/lang/String;)Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    .line 649
    .local v2, val:Ljava/lang/Object;
    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@13
    .line 658
    .end local v2           #val:Ljava/lang/Object;
    :cond_13
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@16
    move-result v0

    #@17
    .line 659
    const/4 v3, 0x1

    #@18
    if-ne v0, v3, :cond_9

    #@1a
    .line 661
    new-instance v3, Lorg/xmlpull/v1/XmlPullParserException;

    #@1c
    new-instance v4, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v5, "Document ended before "

    #@23
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v4

    #@27
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    const-string v5, " end tag"

    #@2d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v4

    #@35
    invoke-direct {v3, v4}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@38
    throw v3

    #@39
    .line 651
    :cond_39
    const/4 v3, 0x3

    #@3a
    if-ne v0, v3, :cond_13

    #@3c
    .line 652
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@43
    move-result v3

    #@44
    if-eqz v3, :cond_47

    #@46
    .line 653
    return-object v1

    #@47
    .line 655
    :cond_47
    new-instance v3, Lorg/xmlpull/v1/XmlPullParserException;

    #@49
    new-instance v4, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string v5, "Expected "

    #@50
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v4

    #@54
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v4

    #@58
    const-string v5, " end tag at: "

    #@5a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v4

    #@5e
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@61
    move-result-object v5

    #@62
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v4

    #@66
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v4

    #@6a
    invoke-direct {v3, v4}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@6d
    throw v3
.end method

.method private static final readThisValueXml(Lorg/xmlpull/v1/XmlPullParser;[Ljava/lang/String;)Ljava/lang/Object;
    .registers 13
    .parameter "parser"
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v10, 0x3

    #@1
    const/4 v9, 0x2

    #@2
    const/4 v8, 0x1

    #@3
    const/4 v6, 0x0

    #@4
    const/4 v7, 0x0

    #@5
    .line 773
    const-string/jumbo v5, "name"

    #@8
    invoke-interface {p0, v6, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v4

    #@c
    .line 774
    .local v4, valueName:Ljava/lang/String;
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@f
    move-result-object v2

    #@10
    .line 780
    .local v2, tagName:Ljava/lang/String;
    const-string/jumbo v5, "null"

    #@13
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v5

    #@17
    if-eqz v5, :cond_2f

    #@19
    .line 781
    const/4 v1, 0x0

    #@1a
    .line 844
    :cond_1a
    :goto_1a
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@1d
    move-result v0

    #@1e
    .local v0, eventType:I
    if-eq v0, v8, :cond_20d

    #@20
    .line 845
    if-ne v0, v10, :cond_1ba

    #@22
    .line 846
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@25
    move-result-object v5

    #@26
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@29
    move-result v5

    #@2a
    if-eqz v5, :cond_193

    #@2c
    .line 847
    aput-object v4, p1, v7

    #@2e
    .line 849
    .end local v0           #eventType:I
    :goto_2e
    return-object v1

    #@2f
    .line 782
    :cond_2f
    const-string/jumbo v5, "string"

    #@32
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@35
    move-result v5

    #@36
    if-eqz v5, :cond_b0

    #@38
    .line 783
    const-string v3, ""

    #@3a
    .line 785
    .local v3, value:Ljava/lang/String;
    :cond_3a
    :goto_3a
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@3d
    move-result v0

    #@3e
    .restart local v0       #eventType:I
    if-eq v0, v8, :cond_a8

    #@40
    .line 786
    if-ne v0, v10, :cond_70

    #@42
    .line 787
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@45
    move-result-object v5

    #@46
    const-string/jumbo v6, "string"

    #@49
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4c
    move-result v5

    #@4d
    if-eqz v5, :cond_53

    #@4f
    .line 788
    aput-object v4, p1, v7

    #@51
    move-object v1, v3

    #@52
    .line 790
    goto :goto_2e

    #@53
    .line 792
    :cond_53
    new-instance v5, Lorg/xmlpull/v1/XmlPullParserException;

    #@55
    new-instance v6, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v7, "Unexpected end tag in <string>: "

    #@5c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v6

    #@60
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@63
    move-result-object v7

    #@64
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v6

    #@68
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v6

    #@6c
    invoke-direct {v5, v6}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@6f
    throw v5

    #@70
    .line 794
    :cond_70
    const/4 v5, 0x4

    #@71
    if-ne v0, v5, :cond_89

    #@73
    .line 795
    new-instance v5, Ljava/lang/StringBuilder;

    #@75
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@78
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v5

    #@7c
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    #@7f
    move-result-object v6

    #@80
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v5

    #@84
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v3

    #@88
    goto :goto_3a

    #@89
    .line 796
    :cond_89
    if-ne v0, v9, :cond_3a

    #@8b
    .line 797
    new-instance v5, Lorg/xmlpull/v1/XmlPullParserException;

    #@8d
    new-instance v6, Ljava/lang/StringBuilder;

    #@8f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@92
    const-string v7, "Unexpected start tag in <string>: "

    #@94
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v6

    #@98
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@9b
    move-result-object v7

    #@9c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v6

    #@a0
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a3
    move-result-object v6

    #@a4
    invoke-direct {v5, v6}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@a7
    throw v5

    #@a8
    .line 801
    :cond_a8
    new-instance v5, Lorg/xmlpull/v1/XmlPullParserException;

    #@aa
    const-string v6, "Unexpected end of document in <string>"

    #@ac
    invoke-direct {v5, v6}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@af
    throw v5

    #@b0
    .line 803
    .end local v0           #eventType:I
    .end local v3           #value:Ljava/lang/String;
    :cond_b0
    const-string v5, "int"

    #@b2
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b5
    move-result v5

    #@b6
    if-eqz v5, :cond_c9

    #@b8
    .line 804
    const-string/jumbo v5, "value"

    #@bb
    invoke-interface {p0, v6, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@be
    move-result-object v5

    #@bf
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@c2
    move-result v5

    #@c3
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c6
    move-result-object v1

    #@c7
    .local v1, res:Ljava/lang/Integer;
    goto/16 :goto_1a

    #@c9
    .line 805
    .end local v1           #res:Ljava/lang/Integer;
    :cond_c9
    const-string/jumbo v5, "long"

    #@cc
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@cf
    move-result v5

    #@d0
    if-eqz v5, :cond_df

    #@d2
    .line 806
    const-string/jumbo v5, "value"

    #@d5
    invoke-interface {p0, v6, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@d8
    move-result-object v5

    #@d9
    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    #@dc
    move-result-object v1

    #@dd
    .local v1, res:Ljava/lang/Long;
    goto/16 :goto_1a

    #@df
    .line 807
    .end local v1           #res:Ljava/lang/Long;
    :cond_df
    const-string v5, "float"

    #@e1
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e4
    move-result v5

    #@e5
    if-eqz v5, :cond_f5

    #@e7
    .line 808
    new-instance v1, Ljava/lang/Float;

    #@e9
    const-string/jumbo v5, "value"

    #@ec
    invoke-interface {p0, v6, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@ef
    move-result-object v5

    #@f0
    invoke-direct {v1, v5}, Ljava/lang/Float;-><init>(Ljava/lang/String;)V

    #@f3
    .local v1, res:Ljava/lang/Float;
    goto/16 :goto_1a

    #@f5
    .line 809
    .end local v1           #res:Ljava/lang/Float;
    :cond_f5
    const-string v5, "double"

    #@f7
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@fa
    move-result v5

    #@fb
    if-eqz v5, :cond_10b

    #@fd
    .line 810
    new-instance v1, Ljava/lang/Double;

    #@ff
    const-string/jumbo v5, "value"

    #@102
    invoke-interface {p0, v6, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@105
    move-result-object v5

    #@106
    invoke-direct {v1, v5}, Ljava/lang/Double;-><init>(Ljava/lang/String;)V

    #@109
    .local v1, res:Ljava/lang/Double;
    goto/16 :goto_1a

    #@10b
    .line 811
    .end local v1           #res:Ljava/lang/Double;
    :cond_10b
    const-string v5, "boolean"

    #@10d
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@110
    move-result v5

    #@111
    if-eqz v5, :cond_120

    #@113
    .line 812
    const-string/jumbo v5, "value"

    #@116
    invoke-interface {p0, v6, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@119
    move-result-object v5

    #@11a
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    #@11d
    move-result-object v1

    #@11e
    .local v1, res:Ljava/lang/Boolean;
    goto/16 :goto_1a

    #@120
    .line 813
    .end local v1           #res:Ljava/lang/Boolean;
    :cond_120
    const-string v5, "int-array"

    #@122
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@125
    move-result v5

    #@126
    if-eqz v5, :cond_135

    #@128
    .line 814
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@12b
    .line 815
    const-string v5, "int-array"

    #@12d
    invoke-static {p0, v5, p1}, Lcom/android/internal/util/XmlUtils;->readThisIntArrayXml(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;[Ljava/lang/String;)[I

    #@130
    move-result-object v1

    #@131
    .line 816
    .local v1, res:[I
    aput-object v4, p1, v7

    #@133
    goto/16 :goto_2e

    #@135
    .line 819
    .end local v1           #res:[I
    :cond_135
    const-string/jumbo v5, "map"

    #@138
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13b
    move-result v5

    #@13c
    if-eqz v5, :cond_14c

    #@13e
    .line 820
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@141
    .line 821
    const-string/jumbo v5, "map"

    #@144
    invoke-static {p0, v5, p1}, Lcom/android/internal/util/XmlUtils;->readThisMapXml(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/HashMap;

    #@147
    move-result-object v1

    #@148
    .line 822
    .local v1, res:Ljava/util/HashMap;
    aput-object v4, p1, v7

    #@14a
    goto/16 :goto_2e

    #@14c
    .line 825
    .end local v1           #res:Ljava/util/HashMap;
    :cond_14c
    const-string/jumbo v5, "list"

    #@14f
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@152
    move-result v5

    #@153
    if-eqz v5, :cond_163

    #@155
    .line 826
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@158
    .line 827
    const-string/jumbo v5, "list"

    #@15b
    invoke-static {p0, v5, p1}, Lcom/android/internal/util/XmlUtils;->readThisListXml(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/ArrayList;

    #@15e
    move-result-object v1

    #@15f
    .line 828
    .local v1, res:Ljava/util/ArrayList;
    aput-object v4, p1, v7

    #@161
    goto/16 :goto_2e

    #@163
    .line 831
    .end local v1           #res:Ljava/util/ArrayList;
    :cond_163
    const-string/jumbo v5, "set"

    #@166
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@169
    move-result v5

    #@16a
    if-eqz v5, :cond_17a

    #@16c
    .line 832
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@16f
    .line 833
    const-string/jumbo v5, "set"

    #@172
    invoke-static {p0, v5, p1}, Lcom/android/internal/util/XmlUtils;->readThisSetXml(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/HashSet;

    #@175
    move-result-object v1

    #@176
    .line 834
    .local v1, res:Ljava/util/HashSet;
    aput-object v4, p1, v7

    #@178
    goto/16 :goto_2e

    #@17a
    .line 838
    .end local v1           #res:Ljava/util/HashSet;
    :cond_17a
    new-instance v5, Lorg/xmlpull/v1/XmlPullParserException;

    #@17c
    new-instance v6, Ljava/lang/StringBuilder;

    #@17e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@181
    const-string v7, "Unknown tag: "

    #@183
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@186
    move-result-object v6

    #@187
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18a
    move-result-object v6

    #@18b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18e
    move-result-object v6

    #@18f
    invoke-direct {v5, v6}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@192
    throw v5

    #@193
    .line 851
    .restart local v0       #eventType:I
    :cond_193
    new-instance v5, Lorg/xmlpull/v1/XmlPullParserException;

    #@195
    new-instance v6, Ljava/lang/StringBuilder;

    #@197
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@19a
    const-string v7, "Unexpected end tag in <"

    #@19c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19f
    move-result-object v6

    #@1a0
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a3
    move-result-object v6

    #@1a4
    const-string v7, ">: "

    #@1a6
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a9
    move-result-object v6

    #@1aa
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@1ad
    move-result-object v7

    #@1ae
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b1
    move-result-object v6

    #@1b2
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b5
    move-result-object v6

    #@1b6
    invoke-direct {v5, v6}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@1b9
    throw v5

    #@1ba
    .line 853
    :cond_1ba
    const/4 v5, 0x4

    #@1bb
    if-ne v0, v5, :cond_1e4

    #@1bd
    .line 854
    new-instance v5, Lorg/xmlpull/v1/XmlPullParserException;

    #@1bf
    new-instance v6, Ljava/lang/StringBuilder;

    #@1c1
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1c4
    const-string v7, "Unexpected text in <"

    #@1c6
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c9
    move-result-object v6

    #@1ca
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cd
    move-result-object v6

    #@1ce
    const-string v7, ">: "

    #@1d0
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d3
    move-result-object v6

    #@1d4
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@1d7
    move-result-object v7

    #@1d8
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1db
    move-result-object v6

    #@1dc
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1df
    move-result-object v6

    #@1e0
    invoke-direct {v5, v6}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@1e3
    throw v5

    #@1e4
    .line 856
    :cond_1e4
    if-ne v0, v9, :cond_1a

    #@1e6
    .line 857
    new-instance v5, Lorg/xmlpull/v1/XmlPullParserException;

    #@1e8
    new-instance v6, Ljava/lang/StringBuilder;

    #@1ea
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1ed
    const-string v7, "Unexpected start tag in <"

    #@1ef
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f2
    move-result-object v6

    #@1f3
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f6
    move-result-object v6

    #@1f7
    const-string v7, ">: "

    #@1f9
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fc
    move-result-object v6

    #@1fd
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@200
    move-result-object v7

    #@201
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@204
    move-result-object v6

    #@205
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@208
    move-result-object v6

    #@209
    invoke-direct {v5, v6}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@20c
    throw v5

    #@20d
    .line 861
    :cond_20d
    new-instance v5, Lorg/xmlpull/v1/XmlPullParserException;

    #@20f
    new-instance v6, Ljava/lang/StringBuilder;

    #@211
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@214
    const-string v7, "Unexpected end of document in <"

    #@216
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@219
    move-result-object v6

    #@21a
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21d
    move-result-object v6

    #@21e
    const-string v7, ">"

    #@220
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@223
    move-result-object v6

    #@224
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@227
    move-result-object v6

    #@228
    invoke-direct {v5, v6}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@22b
    throw v5
.end method

.method public static final readValueXml(Lorg/xmlpull/v1/XmlPullParser;[Ljava/lang/String;)Ljava/lang/Object;
    .registers 6
    .parameter "parser"
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 752
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    #@3
    move-result v0

    #@4
    .line 754
    .local v0, eventType:I
    :cond_4
    const/4 v1, 0x2

    #@5
    if-ne v0, v1, :cond_c

    #@7
    .line 755
    invoke-static {p0, p1}, Lcom/android/internal/util/XmlUtils;->readThisValueXml(Lorg/xmlpull/v1/XmlPullParser;[Ljava/lang/String;)Ljava/lang/Object;

    #@a
    move-result-object v1

    #@b
    return-object v1

    #@c
    .line 756
    :cond_c
    const/4 v1, 0x3

    #@d
    if-ne v0, v1, :cond_2c

    #@f
    .line 757
    new-instance v1, Lorg/xmlpull/v1/XmlPullParserException;

    #@11
    new-instance v2, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v3, "Unexpected end tag at: "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-direct {v1, v2}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@2b
    throw v1

    #@2c
    .line 759
    :cond_2c
    const/4 v1, 0x4

    #@2d
    if-ne v0, v1, :cond_4c

    #@2f
    .line 760
    new-instance v1, Lorg/xmlpull/v1/XmlPullParserException;

    #@31
    new-instance v2, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v3, "Unexpected text: "

    #@38
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v2

    #@3c
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v2

    #@48
    invoke-direct {v1, v2}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@4b
    throw v1

    #@4c
    .line 763
    :cond_4c
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@4f
    move-result v0

    #@50
    .line 764
    const/4 v1, 0x1

    #@51
    if-ne v0, v1, :cond_4

    #@53
    .line 766
    new-instance v1, Lorg/xmlpull/v1/XmlPullParserException;

    #@55
    const-string v2, "Unexpected end of document"

    #@57
    invoke-direct {v1, v2}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@5a
    throw v1
.end method

.method public static skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V
    .registers 4
    .parameter "parser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 43
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@3
    move-result v0

    #@4
    .line 46
    .local v0, outerDepth:I
    :cond_4
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@7
    move-result v1

    #@8
    .local v1, type:I
    const/4 v2, 0x1

    #@9
    if-eq v1, v2, :cond_14

    #@b
    const/4 v2, 0x3

    #@c
    if-ne v1, v2, :cond_4

    #@e
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@11
    move-result v2

    #@12
    if-gt v2, v0, :cond_4

    #@14
    .line 49
    :cond_14
    return-void
.end method

.method public static final writeByteArrayXml([BLjava/lang/String;Lorg/xmlpull/v1/XmlSerializer;)V
    .registers 12
    .parameter "val"
    .parameter "name"
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v8, 0xa

    #@2
    const/4 v7, 0x0

    #@3
    .line 325
    if-nez p0, :cond_12

    #@5
    .line 326
    const-string/jumbo v5, "null"

    #@8
    invoke-interface {p2, v7, v5}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@b
    .line 327
    const-string/jumbo v5, "null"

    #@e
    invoke-interface {p2, v7, v5}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@11
    .line 351
    :goto_11
    return-void

    #@12
    .line 331
    :cond_12
    const-string v5, "byte-array"

    #@14
    invoke-interface {p2, v7, v5}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@17
    .line 332
    if-eqz p1, :cond_1f

    #@19
    .line 333
    const-string/jumbo v5, "name"

    #@1c
    invoke-interface {p2, v7, v5, p1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@1f
    .line 336
    :cond_1f
    array-length v0, p0

    #@20
    .line 337
    .local v0, N:I
    const-string/jumbo v5, "num"

    #@23
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@26
    move-result-object v6

    #@27
    invoke-interface {p2, v7, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@2a
    .line 339
    new-instance v4, Ljava/lang/StringBuilder;

    #@2c
    array-length v5, p0

    #@2d
    mul-int/lit8 v5, v5, 0x2

    #@2f
    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    #@32
    .line 340
    .local v4, sb:Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    #@33
    .local v3, i:I
    :goto_33
    if-ge v3, v0, :cond_56

    #@35
    .line 341
    aget-byte v1, p0, v3

    #@37
    .line 342
    .local v1, b:I
    shr-int/lit8 v2, v1, 0x4

    #@39
    .line 343
    .local v2, h:I
    if-lt v2, v8, :cond_50

    #@3b
    add-int/lit8 v5, v2, 0x61

    #@3d
    add-int/lit8 v5, v5, -0xa

    #@3f
    :goto_3f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    .line 344
    and-int/lit16 v2, v1, 0xff

    #@44
    .line 345
    if-lt v2, v8, :cond_53

    #@46
    add-int/lit8 v5, v2, 0x61

    #@48
    add-int/lit8 v5, v5, -0xa

    #@4a
    :goto_4a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4d
    .line 340
    add-int/lit8 v3, v3, 0x1

    #@4f
    goto :goto_33

    #@50
    .line 343
    :cond_50
    add-int/lit8 v5, v2, 0x30

    #@52
    goto :goto_3f

    #@53
    .line 345
    :cond_53
    add-int/lit8 v5, v2, 0x30

    #@55
    goto :goto_4a

    #@56
    .line 348
    .end local v1           #b:I
    .end local v2           #h:I
    :cond_56
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v5

    #@5a
    invoke-interface {p2, v5}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@5d
    .line 350
    const-string v5, "byte-array"

    #@5f
    invoke-interface {p2, v7, v5}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@62
    goto :goto_11
.end method

.method public static final writeIntArrayXml([ILjava/lang/String;Lorg/xmlpull/v1/XmlSerializer;)V
    .registers 8
    .parameter "val"
    .parameter "name"
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 370
    if-nez p0, :cond_10

    #@3
    .line 371
    const-string/jumbo v2, "null"

    #@6
    invoke-interface {p2, v4, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@9
    .line 372
    const-string/jumbo v2, "null"

    #@c
    invoke-interface {p2, v4, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@f
    .line 391
    :goto_f
    return-void

    #@10
    .line 376
    :cond_10
    const-string v2, "int-array"

    #@12
    invoke-interface {p2, v4, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@15
    .line 377
    if-eqz p1, :cond_1d

    #@17
    .line 378
    const-string/jumbo v2, "name"

    #@1a
    invoke-interface {p2, v4, v2, p1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@1d
    .line 381
    :cond_1d
    array-length v0, p0

    #@1e
    .line 382
    .local v0, N:I
    const-string/jumbo v2, "num"

    #@21
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    invoke-interface {p2, v4, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@28
    .line 384
    const/4 v1, 0x0

    #@29
    .local v1, i:I
    :goto_29
    if-ge v1, v0, :cond_46

    #@2b
    .line 385
    const-string/jumbo v2, "item"

    #@2e
    invoke-interface {p2, v4, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@31
    .line 386
    const-string/jumbo v2, "value"

    #@34
    aget v3, p0, v1

    #@36
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@39
    move-result-object v3

    #@3a
    invoke-interface {p2, v4, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@3d
    .line 387
    const-string/jumbo v2, "item"

    #@40
    invoke-interface {p2, v4, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@43
    .line 384
    add-int/lit8 v1, v1, 0x1

    #@45
    goto :goto_29

    #@46
    .line 390
    :cond_46
    const-string v2, "int-array"

    #@48
    invoke-interface {p2, v4, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@4b
    goto :goto_f
.end method

.method public static final writeListXml(Ljava/util/List;Ljava/io/OutputStream;)V
    .registers 6
    .parameter "val"
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 204
    invoke-static {}, Landroid/util/Xml;->newSerializer()Lorg/xmlpull/v1/XmlSerializer;

    #@5
    move-result-object v0

    #@6
    .line 205
    .local v0, serializer:Lorg/xmlpull/v1/XmlSerializer;
    const-string/jumbo v1, "utf-8"

    #@9
    invoke-interface {v0, p1, v1}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    #@c
    .line 206
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@f
    move-result-object v1

    #@10
    invoke-interface {v0, v3, v1}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@13
    .line 207
    const-string v1, "http://xmlpull.org/v1/doc/features.html#indent-output"

    #@15
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->setFeature(Ljava/lang/String;Z)V

    #@18
    .line 208
    invoke-static {p0, v3, v0}, Lcom/android/internal/util/XmlUtils;->writeListXml(Ljava/util/List;Ljava/lang/String;Lorg/xmlpull/v1/XmlSerializer;)V

    #@1b
    .line 209
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    #@1e
    .line 210
    return-void
.end method

.method public static final writeListXml(Ljava/util/List;Ljava/lang/String;Lorg/xmlpull/v1/XmlSerializer;)V
    .registers 7
    .parameter "val"
    .parameter "name"
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 268
    if-nez p0, :cond_10

    #@3
    .line 269
    const-string/jumbo v2, "null"

    #@6
    invoke-interface {p2, v3, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@9
    .line 270
    const-string/jumbo v2, "null"

    #@c
    invoke-interface {p2, v3, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@f
    .line 287
    :goto_f
    return-void

    #@10
    .line 274
    :cond_10
    const-string/jumbo v2, "list"

    #@13
    invoke-interface {p2, v3, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@16
    .line 275
    if-eqz p1, :cond_1e

    #@18
    .line 276
    const-string/jumbo v2, "name"

    #@1b
    invoke-interface {p2, v3, v2, p1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@1e
    .line 279
    :cond_1e
    invoke-interface {p0}, Ljava/util/List;->size()I

    #@21
    move-result v0

    #@22
    .line 280
    .local v0, N:I
    const/4 v1, 0x0

    #@23
    .line 281
    .local v1, i:I
    :goto_23
    if-ge v1, v0, :cond_2f

    #@25
    .line 282
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@28
    move-result-object v2

    #@29
    invoke-static {v2, v3, p2}, Lcom/android/internal/util/XmlUtils;->writeValueXml(Ljava/lang/Object;Ljava/lang/String;Lorg/xmlpull/v1/XmlSerializer;)V

    #@2c
    .line 283
    add-int/lit8 v1, v1, 0x1

    #@2e
    goto :goto_23

    #@2f
    .line 286
    :cond_2f
    const-string/jumbo v2, "list"

    #@32
    invoke-interface {p2, v3, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@35
    goto :goto_f
.end method

.method public static final writeMapXml(Ljava/util/Map;Ljava/io/OutputStream;)V
    .registers 6
    .parameter "val"
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 181
    new-instance v0, Lcom/android/internal/util/FastXmlSerializer;

    #@4
    invoke-direct {v0}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    #@7
    .line 182
    .local v0, serializer:Lorg/xmlpull/v1/XmlSerializer;
    const-string/jumbo v1, "utf-8"

    #@a
    invoke-interface {v0, p1, v1}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    #@d
    .line 183
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@10
    move-result-object v1

    #@11
    invoke-interface {v0, v3, v1}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@14
    .line 184
    const-string v1, "http://xmlpull.org/v1/doc/features.html#indent-output"

    #@16
    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->setFeature(Ljava/lang/String;Z)V

    #@19
    .line 185
    invoke-static {p0, v3, v0}, Lcom/android/internal/util/XmlUtils;->writeMapXml(Ljava/util/Map;Ljava/lang/String;Lorg/xmlpull/v1/XmlSerializer;)V

    #@1c
    .line 186
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    #@1f
    .line 187
    return-void
.end method

.method public static final writeMapXml(Ljava/util/Map;Ljava/lang/String;Lorg/xmlpull/v1/XmlSerializer;)V
    .registers 9
    .parameter "val"
    .parameter "name"
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 229
    if-nez p0, :cond_10

    #@3
    .line 230
    const-string/jumbo v3, "null"

    #@6
    invoke-interface {p2, v5, v3}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@9
    .line 231
    const-string/jumbo v3, "null"

    #@c
    invoke-interface {p2, v5, v3}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@f
    .line 249
    :goto_f
    return-void

    #@10
    .line 235
    :cond_10
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@13
    move-result-object v2

    #@14
    .line 236
    .local v2, s:Ljava/util/Set;
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@17
    move-result-object v1

    #@18
    .line 238
    .local v1, i:Ljava/util/Iterator;
    const-string/jumbo v3, "map"

    #@1b
    invoke-interface {p2, v5, v3}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@1e
    .line 239
    if-eqz p1, :cond_26

    #@20
    .line 240
    const-string/jumbo v3, "name"

    #@23
    invoke-interface {p2, v5, v3, p1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@26
    .line 243
    :cond_26
    :goto_26
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@29
    move-result v3

    #@2a
    if-eqz v3, :cond_40

    #@2c
    .line 244
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2f
    move-result-object v0

    #@30
    check-cast v0, Ljava/util/Map$Entry;

    #@32
    .line 245
    .local v0, e:Ljava/util/Map$Entry;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@35
    move-result-object v4

    #@36
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@39
    move-result-object v3

    #@3a
    check-cast v3, Ljava/lang/String;

    #@3c
    invoke-static {v4, v3, p2}, Lcom/android/internal/util/XmlUtils;->writeValueXml(Ljava/lang/Object;Ljava/lang/String;Lorg/xmlpull/v1/XmlSerializer;)V

    #@3f
    goto :goto_26

    #@40
    .line 248
    .end local v0           #e:Ljava/util/Map$Entry;
    :cond_40
    const-string/jumbo v3, "map"

    #@43
    invoke-interface {p2, v5, v3}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@46
    goto :goto_f
.end method

.method public static final writeSetXml(Ljava/util/Set;Ljava/lang/String;Lorg/xmlpull/v1/XmlSerializer;)V
    .registers 7
    .parameter "val"
    .parameter "name"
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 291
    if-nez p0, :cond_10

    #@3
    .line 292
    const-string/jumbo v2, "null"

    #@6
    invoke-interface {p2, v3, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@9
    .line 293
    const-string/jumbo v2, "null"

    #@c
    invoke-interface {p2, v3, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@f
    .line 307
    :goto_f
    return-void

    #@10
    .line 297
    :cond_10
    const-string/jumbo v2, "set"

    #@13
    invoke-interface {p2, v3, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@16
    .line 298
    if-eqz p1, :cond_1e

    #@18
    .line 299
    const-string/jumbo v2, "name"

    #@1b
    invoke-interface {p2, v3, v2, p1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@1e
    .line 302
    :cond_1e
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@21
    move-result-object v0

    #@22
    .local v0, i$:Ljava/util/Iterator;
    :goto_22
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@25
    move-result v2

    #@26
    if-eqz v2, :cond_30

    #@28
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2b
    move-result-object v1

    #@2c
    .line 303
    .local v1, v:Ljava/lang/Object;
    invoke-static {v1, v3, p2}, Lcom/android/internal/util/XmlUtils;->writeValueXml(Ljava/lang/Object;Ljava/lang/String;Lorg/xmlpull/v1/XmlSerializer;)V

    #@2f
    goto :goto_22

    #@30
    .line 306
    .end local v1           #v:Ljava/lang/Object;
    :cond_30
    const-string/jumbo v2, "set"

    #@33
    invoke-interface {p2, v3, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@36
    goto :goto_f
.end method

.method public static final writeValueXml(Ljava/lang/Object;Ljava/lang/String;Lorg/xmlpull/v1/XmlSerializer;)V
    .registers 7
    .parameter "v"
    .parameter "name"
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 413
    if-nez p0, :cond_18

    #@3
    .line 414
    const-string/jumbo v1, "null"

    #@6
    invoke-interface {p2, v3, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@9
    .line 415
    if-eqz p1, :cond_11

    #@b
    .line 416
    const-string/jumbo v1, "name"

    #@e
    invoke-interface {p2, v3, v1, p1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@11
    .line 418
    :cond_11
    const-string/jumbo v1, "null"

    #@14
    invoke-interface {p2, v3, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@17
    .line 474
    .end local p0
    :goto_17
    return-void

    #@18
    .line 420
    .restart local p0
    :cond_18
    instance-of v1, p0, Ljava/lang/String;

    #@1a
    if-eqz v1, :cond_38

    #@1c
    .line 421
    const-string/jumbo v1, "string"

    #@1f
    invoke-interface {p2, v3, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@22
    .line 422
    if-eqz p1, :cond_2a

    #@24
    .line 423
    const-string/jumbo v1, "name"

    #@27
    invoke-interface {p2, v3, v1, p1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@2a
    .line 425
    :cond_2a
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@2d
    move-result-object v1

    #@2e
    invoke-interface {p2, v1}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@31
    .line 426
    const-string/jumbo v1, "string"

    #@34
    invoke-interface {p2, v3, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@37
    goto :goto_17

    #@38
    .line 428
    :cond_38
    instance-of v1, p0, Ljava/lang/Integer;

    #@3a
    if-eqz v1, :cond_57

    #@3c
    .line 429
    const-string v0, "int"

    #@3e
    .line 468
    .local v0, typeStr:Ljava/lang/String;
    :goto_3e
    invoke-interface {p2, v3, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@41
    .line 469
    if-eqz p1, :cond_49

    #@43
    .line 470
    const-string/jumbo v1, "name"

    #@46
    invoke-interface {p2, v3, v1, p1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@49
    .line 472
    :cond_49
    const-string/jumbo v1, "value"

    #@4c
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@4f
    move-result-object v2

    #@50
    invoke-interface {p2, v3, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@53
    .line 473
    invoke-interface {p2, v3, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@56
    goto :goto_17

    #@57
    .line 430
    .end local v0           #typeStr:Ljava/lang/String;
    :cond_57
    instance-of v1, p0, Ljava/lang/Long;

    #@59
    if-eqz v1, :cond_5f

    #@5b
    .line 431
    const-string/jumbo v0, "long"

    #@5e
    .restart local v0       #typeStr:Ljava/lang/String;
    goto :goto_3e

    #@5f
    .line 432
    .end local v0           #typeStr:Ljava/lang/String;
    :cond_5f
    instance-of v1, p0, Ljava/lang/Float;

    #@61
    if-eqz v1, :cond_66

    #@63
    .line 433
    const-string v0, "float"

    #@65
    .restart local v0       #typeStr:Ljava/lang/String;
    goto :goto_3e

    #@66
    .line 434
    .end local v0           #typeStr:Ljava/lang/String;
    :cond_66
    instance-of v1, p0, Ljava/lang/Double;

    #@68
    if-eqz v1, :cond_6d

    #@6a
    .line 435
    const-string v0, "double"

    #@6c
    .restart local v0       #typeStr:Ljava/lang/String;
    goto :goto_3e

    #@6d
    .line 436
    .end local v0           #typeStr:Ljava/lang/String;
    :cond_6d
    instance-of v1, p0, Ljava/lang/Boolean;

    #@6f
    if-eqz v1, :cond_74

    #@71
    .line 437
    const-string v0, "boolean"

    #@73
    .restart local v0       #typeStr:Ljava/lang/String;
    goto :goto_3e

    #@74
    .line 438
    .end local v0           #typeStr:Ljava/lang/String;
    :cond_74
    instance-of v1, p0, [B

    #@76
    if-eqz v1, :cond_80

    #@78
    .line 439
    check-cast p0, [B

    #@7a
    .end local p0
    check-cast p0, [B

    #@7c
    invoke-static {p0, p1, p2}, Lcom/android/internal/util/XmlUtils;->writeByteArrayXml([BLjava/lang/String;Lorg/xmlpull/v1/XmlSerializer;)V

    #@7f
    goto :goto_17

    #@80
    .line 441
    .restart local p0
    :cond_80
    instance-of v1, p0, [I

    #@82
    if-eqz v1, :cond_8c

    #@84
    .line 442
    check-cast p0, [I

    #@86
    .end local p0
    check-cast p0, [I

    #@88
    invoke-static {p0, p1, p2}, Lcom/android/internal/util/XmlUtils;->writeIntArrayXml([ILjava/lang/String;Lorg/xmlpull/v1/XmlSerializer;)V

    #@8b
    goto :goto_17

    #@8c
    .line 444
    .restart local p0
    :cond_8c
    instance-of v1, p0, Ljava/util/Map;

    #@8e
    if-eqz v1, :cond_96

    #@90
    .line 445
    check-cast p0, Ljava/util/Map;

    #@92
    .end local p0
    invoke-static {p0, p1, p2}, Lcom/android/internal/util/XmlUtils;->writeMapXml(Ljava/util/Map;Ljava/lang/String;Lorg/xmlpull/v1/XmlSerializer;)V

    #@95
    goto :goto_17

    #@96
    .line 447
    .restart local p0
    :cond_96
    instance-of v1, p0, Ljava/util/List;

    #@98
    if-eqz v1, :cond_a1

    #@9a
    .line 448
    check-cast p0, Ljava/util/List;

    #@9c
    .end local p0
    invoke-static {p0, p1, p2}, Lcom/android/internal/util/XmlUtils;->writeListXml(Ljava/util/List;Ljava/lang/String;Lorg/xmlpull/v1/XmlSerializer;)V

    #@9f
    goto/16 :goto_17

    #@a1
    .line 450
    .restart local p0
    :cond_a1
    instance-of v1, p0, Ljava/util/Set;

    #@a3
    if-eqz v1, :cond_ac

    #@a5
    .line 451
    check-cast p0, Ljava/util/Set;

    #@a7
    .end local p0
    invoke-static {p0, p1, p2}, Lcom/android/internal/util/XmlUtils;->writeSetXml(Ljava/util/Set;Ljava/lang/String;Lorg/xmlpull/v1/XmlSerializer;)V

    #@aa
    goto/16 :goto_17

    #@ac
    .line 453
    .restart local p0
    :cond_ac
    instance-of v1, p0, Ljava/lang/CharSequence;

    #@ae
    if-eqz v1, :cond_cd

    #@b0
    .line 457
    const-string/jumbo v1, "string"

    #@b3
    invoke-interface {p2, v3, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@b6
    .line 458
    if-eqz p1, :cond_be

    #@b8
    .line 459
    const-string/jumbo v1, "name"

    #@bb
    invoke-interface {p2, v3, v1, p1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@be
    .line 461
    :cond_be
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@c1
    move-result-object v1

    #@c2
    invoke-interface {p2, v1}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@c5
    .line 462
    const-string/jumbo v1, "string"

    #@c8
    invoke-interface {p2, v3, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@cb
    goto/16 :goto_17

    #@cd
    .line 465
    :cond_cd
    new-instance v1, Ljava/lang/RuntimeException;

    #@cf
    new-instance v2, Ljava/lang/StringBuilder;

    #@d1
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d4
    const-string/jumbo v3, "writeValueXml: unable to write value "

    #@d7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@da
    move-result-object v2

    #@db
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v2

    #@df
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e2
    move-result-object v2

    #@e3
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@e6
    throw v1
.end method
