.class public Lcom/android/internal/util/BitwiseOutputStream;
.super Ljava/lang/Object;
.source "BitwiseOutputStream.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/util/BitwiseOutputStream$AccessException;
    }
.end annotation


# instance fields
.field private mBuf:[B

.field private mEnd:I

.field private mPos:I


# direct methods
.method public constructor <init>(I)V
    .registers 3
    .parameter "startingLength"

    #@0
    .prologue
    .line 52
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 53
    new-array v0, p1, [B

    #@5
    iput-object v0, p0, Lcom/android/internal/util/BitwiseOutputStream;->mBuf:[B

    #@7
    .line 54
    shl-int/lit8 v0, p1, 0x3

    #@9
    iput v0, p0, Lcom/android/internal/util/BitwiseOutputStream;->mEnd:I

    #@b
    .line 55
    const/4 v0, 0x0

    #@c
    iput v0, p0, Lcom/android/internal/util/BitwiseOutputStream;->mPos:I

    #@e
    .line 56
    return-void
.end method

.method private possExpand(I)V
    .registers 6
    .parameter "bits"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 76
    iget v1, p0, Lcom/android/internal/util/BitwiseOutputStream;->mPos:I

    #@3
    add-int/2addr v1, p1

    #@4
    iget v2, p0, Lcom/android/internal/util/BitwiseOutputStream;->mEnd:I

    #@6
    if-ge v1, v2, :cond_9

    #@8
    .line 81
    :goto_8
    return-void

    #@9
    .line 77
    :cond_9
    iget v1, p0, Lcom/android/internal/util/BitwiseOutputStream;->mPos:I

    #@b
    add-int/2addr v1, p1

    #@c
    ushr-int/lit8 v1, v1, 0x2

    #@e
    new-array v0, v1, [B

    #@10
    .line 78
    .local v0, newBuf:[B
    iget-object v1, p0, Lcom/android/internal/util/BitwiseOutputStream;->mBuf:[B

    #@12
    iget v2, p0, Lcom/android/internal/util/BitwiseOutputStream;->mEnd:I

    #@14
    ushr-int/lit8 v2, v2, 0x3

    #@16
    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@19
    .line 79
    iput-object v0, p0, Lcom/android/internal/util/BitwiseOutputStream;->mBuf:[B

    #@1b
    .line 80
    array-length v1, v0

    #@1c
    shl-int/lit8 v1, v1, 0x3

    #@1e
    iput v1, p0, Lcom/android/internal/util/BitwiseOutputStream;->mEnd:I

    #@20
    goto :goto_8
.end method


# virtual methods
.method public skip(I)V
    .registers 3
    .parameter "bits"

    #@0
    .prologue
    .line 127
    invoke-direct {p0, p1}, Lcom/android/internal/util/BitwiseOutputStream;->possExpand(I)V

    #@3
    .line 128
    iget v0, p0, Lcom/android/internal/util/BitwiseOutputStream;->mPos:I

    #@5
    add-int/2addr v0, p1

    #@6
    iput v0, p0, Lcom/android/internal/util/BitwiseOutputStream;->mPos:I

    #@8
    .line 129
    return-void
.end method

.method public toByteArray()[B
    .registers 6

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 64
    iget v2, p0, Lcom/android/internal/util/BitwiseOutputStream;->mPos:I

    #@3
    ushr-int/lit8 v4, v2, 0x3

    #@5
    iget v2, p0, Lcom/android/internal/util/BitwiseOutputStream;->mPos:I

    #@7
    and-int/lit8 v2, v2, 0x7

    #@9
    if-lez v2, :cond_16

    #@b
    const/4 v2, 0x1

    #@c
    :goto_c
    add-int v0, v4, v2

    #@e
    .line 65
    .local v0, len:I
    new-array v1, v0, [B

    #@10
    .line 66
    .local v1, newBuf:[B
    iget-object v2, p0, Lcom/android/internal/util/BitwiseOutputStream;->mBuf:[B

    #@12
    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@15
    .line 67
    return-object v1

    #@16
    .end local v0           #len:I
    .end local v1           #newBuf:[B
    :cond_16
    move v2, v3

    #@17
    .line 64
    goto :goto_c
.end method

.method public write(II)V
    .registers 9
    .parameter "bits"
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseOutputStream$AccessException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v5, 0x8

    #@2
    .line 93
    if-ltz p1, :cond_6

    #@4
    if-le p1, v5, :cond_25

    #@6
    .line 94
    :cond_6
    new-instance v2, Lcom/android/internal/util/BitwiseOutputStream$AccessException;

    #@8
    new-instance v3, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v4, "illegal write ("

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    const-string v4, " bits)"

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    invoke-direct {v2, v3}, Lcom/android/internal/util/BitwiseOutputStream$AccessException;-><init>(Ljava/lang/String;)V

    #@24
    throw v2

    #@25
    .line 96
    :cond_25
    invoke-direct {p0, p1}, Lcom/android/internal/util/BitwiseOutputStream;->possExpand(I)V

    #@28
    .line 97
    const/4 v2, -0x1

    #@29
    rsub-int/lit8 v3, p1, 0x20

    #@2b
    ushr-int/2addr v2, v3

    #@2c
    and-int/2addr p2, v2

    #@2d
    .line 98
    iget v2, p0, Lcom/android/internal/util/BitwiseOutputStream;->mPos:I

    #@2f
    ushr-int/lit8 v0, v2, 0x3

    #@31
    .line 99
    .local v0, index:I
    iget v2, p0, Lcom/android/internal/util/BitwiseOutputStream;->mPos:I

    #@33
    and-int/lit8 v2, v2, 0x7

    #@35
    rsub-int/lit8 v2, v2, 0x10

    #@37
    sub-int v1, v2, p1

    #@39
    .line 100
    .local v1, offset:I
    shl-int/2addr p2, v1

    #@3a
    .line 101
    iget v2, p0, Lcom/android/internal/util/BitwiseOutputStream;->mPos:I

    #@3c
    add-int/2addr v2, p1

    #@3d
    iput v2, p0, Lcom/android/internal/util/BitwiseOutputStream;->mPos:I

    #@3f
    .line 102
    iget-object v2, p0, Lcom/android/internal/util/BitwiseOutputStream;->mBuf:[B

    #@41
    aget-byte v3, v2, v0

    #@43
    ushr-int/lit8 v4, p2, 0x8

    #@45
    or-int/2addr v3, v4

    #@46
    int-to-byte v3, v3

    #@47
    aput-byte v3, v2, v0

    #@49
    .line 103
    if-ge v1, v5, :cond_57

    #@4b
    iget-object v2, p0, Lcom/android/internal/util/BitwiseOutputStream;->mBuf:[B

    #@4d
    add-int/lit8 v3, v0, 0x1

    #@4f
    aget-byte v4, v2, v3

    #@51
    and-int/lit16 v5, p2, 0xff

    #@53
    or-int/2addr v4, v5

    #@54
    int-to-byte v4, v4

    #@55
    aput-byte v4, v2, v3

    #@57
    .line 104
    :cond_57
    return-void
.end method

.method public writeByteArray(I[B)V
    .registers 7
    .parameter "bits"
    .parameter "arr"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseOutputStream$AccessException;
        }
    .end annotation

    #@0
    .prologue
    .line 113
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    array-length v2, p2

    #@2
    if-ge v0, v2, :cond_1c

    #@4
    .line 114
    const/16 v2, 0x8

    #@6
    shl-int/lit8 v3, v0, 0x3

    #@8
    sub-int v3, p1, v3

    #@a
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    #@d
    move-result v1

    #@e
    .line 115
    .local v1, increment:I
    if-lez v1, :cond_19

    #@10
    .line 116
    aget-byte v2, p2, v0

    #@12
    rsub-int/lit8 v3, v1, 0x8

    #@14
    ushr-int/2addr v2, v3

    #@15
    int-to-byte v2, v2

    #@16
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/util/BitwiseOutputStream;->write(II)V

    #@19
    .line 113
    :cond_19
    add-int/lit8 v0, v0, 0x1

    #@1b
    goto :goto_1

    #@1c
    .line 119
    .end local v1           #increment:I
    :cond_1c
    return-void
.end method
