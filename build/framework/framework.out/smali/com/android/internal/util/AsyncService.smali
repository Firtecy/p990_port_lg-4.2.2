.class public abstract Lcom/android/internal/util/AsyncService;
.super Landroid/app/Service;
.source "AsyncService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/util/AsyncService$AsyncServiceInfo;
    }
.end annotation


# static fields
.field public static final CMD_ASYNC_SERVICE_DESTROY:I = 0x1000000

.field public static final CMD_ASYNC_SERVICE_ON_START_INTENT:I = 0xffffff

.field protected static final DBG:Z = true

.field private static final TAG:Ljava/lang/String; = "AsyncService"


# instance fields
.field mAsyncServiceInfo:Lcom/android/internal/util/AsyncService$AsyncServiceInfo;

.field mHandler:Landroid/os/Handler;

.field protected mMessenger:Landroid/os/Messenger;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 35
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@3
    .line 52
    return-void
.end method


# virtual methods
.method public abstract createHandler()Lcom/android/internal/util/AsyncService$AsyncServiceInfo;
.end method

.method public getHandler()Landroid/os/Handler;
    .registers 2

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/internal/util/AsyncService;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 126
    iget-object v0, p0, Lcom/android/internal/util/AsyncService;->mMessenger:Landroid/os/Messenger;

    #@2
    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public onCreate()V
    .registers 3

    #@0
    .prologue
    .line 84
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    #@3
    .line 85
    invoke-virtual {p0}, Lcom/android/internal/util/AsyncService;->createHandler()Lcom/android/internal/util/AsyncService$AsyncServiceInfo;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Lcom/android/internal/util/AsyncService;->mAsyncServiceInfo:Lcom/android/internal/util/AsyncService$AsyncServiceInfo;

    #@9
    .line 86
    iget-object v0, p0, Lcom/android/internal/util/AsyncService;->mAsyncServiceInfo:Lcom/android/internal/util/AsyncService$AsyncServiceInfo;

    #@b
    iget-object v0, v0, Lcom/android/internal/util/AsyncService$AsyncServiceInfo;->mHandler:Landroid/os/Handler;

    #@d
    iput-object v0, p0, Lcom/android/internal/util/AsyncService;->mHandler:Landroid/os/Handler;

    #@f
    .line 87
    new-instance v0, Landroid/os/Messenger;

    #@11
    iget-object v1, p0, Lcom/android/internal/util/AsyncService;->mHandler:Landroid/os/Handler;

    #@13
    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    #@16
    iput-object v0, p0, Lcom/android/internal/util/AsyncService;->mMessenger:Landroid/os/Messenger;

    #@18
    .line 88
    return-void
.end method

.method public onDestroy()V
    .registers 4

    #@0
    .prologue
    .line 114
    const-string v1, "AsyncService"

    #@2
    const-string/jumbo v2, "onDestroy"

    #@5
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 116
    iget-object v1, p0, Lcom/android/internal/util/AsyncService;->mHandler:Landroid/os/Handler;

    #@a
    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    #@d
    move-result-object v0

    #@e
    .line 117
    .local v0, msg:Landroid/os/Message;
    const/high16 v1, 0x100

    #@10
    iput v1, v0, Landroid/os/Message;->what:I

    #@12
    .line 118
    iget-object v1, p0, Lcom/android/internal/util/AsyncService;->mHandler:Landroid/os/Handler;

    #@14
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@17
    .line 119
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .registers 7
    .parameter "intent"
    .parameter "flags"
    .parameter "startId"

    #@0
    .prologue
    .line 95
    const-string v1, "AsyncService"

    #@2
    const-string/jumbo v2, "onStartCommand"

    #@5
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 97
    iget-object v1, p0, Lcom/android/internal/util/AsyncService;->mHandler:Landroid/os/Handler;

    #@a
    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    #@d
    move-result-object v0

    #@e
    .line 98
    .local v0, msg:Landroid/os/Message;
    const v1, 0xffffff

    #@11
    iput v1, v0, Landroid/os/Message;->what:I

    #@13
    .line 99
    iput p2, v0, Landroid/os/Message;->arg1:I

    #@15
    .line 100
    iput p3, v0, Landroid/os/Message;->arg2:I

    #@17
    .line 101
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@19
    .line 102
    iget-object v1, p0, Lcom/android/internal/util/AsyncService;->mHandler:Landroid/os/Handler;

    #@1b
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@1e
    .line 104
    iget-object v1, p0, Lcom/android/internal/util/AsyncService;->mAsyncServiceInfo:Lcom/android/internal/util/AsyncService$AsyncServiceInfo;

    #@20
    iget v1, v1, Lcom/android/internal/util/AsyncService$AsyncServiceInfo;->mRestartFlags:I

    #@22
    return v1
.end method
