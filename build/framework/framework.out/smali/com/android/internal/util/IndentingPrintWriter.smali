.class public Lcom/android/internal/util/IndentingPrintWriter;
.super Ljava/io/PrintWriter;
.source "IndentingPrintWriter.java"


# instance fields
.field private mBuilder:Ljava/lang/StringBuilder;

.field private mCurrent:[C

.field private mEmptyLine:Z

.field private final mIndent:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/io/Writer;Ljava/lang/String;)V
    .registers 4
    .parameter "writer"
    .parameter "indent"

    #@0
    .prologue
    .line 35
    invoke-direct {p0, p1}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    #@3
    .line 30
    new-instance v0, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/internal/util/IndentingPrintWriter;->mBuilder:Ljava/lang/StringBuilder;

    #@a
    .line 32
    const/4 v0, 0x1

    #@b
    iput-boolean v0, p0, Lcom/android/internal/util/IndentingPrintWriter;->mEmptyLine:Z

    #@d
    .line 36
    iput-object p2, p0, Lcom/android/internal/util/IndentingPrintWriter;->mIndent:Ljava/lang/String;

    #@f
    .line 37
    return-void
.end method

.method private writeIndent()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 75
    iget-boolean v0, p0, Lcom/android/internal/util/IndentingPrintWriter;->mEmptyLine:Z

    #@3
    if-eqz v0, :cond_27

    #@5
    .line 76
    iput-boolean v2, p0, Lcom/android/internal/util/IndentingPrintWriter;->mEmptyLine:Z

    #@7
    .line 77
    iget-object v0, p0, Lcom/android/internal/util/IndentingPrintWriter;->mBuilder:Ljava/lang/StringBuilder;

    #@9
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_27

    #@f
    .line 78
    iget-object v0, p0, Lcom/android/internal/util/IndentingPrintWriter;->mCurrent:[C

    #@11
    if-nez v0, :cond_1f

    #@13
    .line 79
    iget-object v0, p0, Lcom/android/internal/util/IndentingPrintWriter;->mBuilder:Ljava/lang/StringBuilder;

    #@15
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    #@1c
    move-result-object v0

    #@1d
    iput-object v0, p0, Lcom/android/internal/util/IndentingPrintWriter;->mCurrent:[C

    #@1f
    .line 81
    :cond_1f
    iget-object v0, p0, Lcom/android/internal/util/IndentingPrintWriter;->mCurrent:[C

    #@21
    iget-object v1, p0, Lcom/android/internal/util/IndentingPrintWriter;->mCurrent:[C

    #@23
    array-length v1, v1

    #@24
    invoke-super {p0, v0, v2, v1}, Ljava/io/PrintWriter;->write([CII)V

    #@27
    .line 84
    :cond_27
    return-void
.end method


# virtual methods
.method public decreaseIndent()V
    .registers 4

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/internal/util/IndentingPrintWriter;->mBuilder:Ljava/lang/StringBuilder;

    #@2
    const/4 v1, 0x0

    #@3
    iget-object v2, p0, Lcom/android/internal/util/IndentingPrintWriter;->mIndent:Ljava/lang/String;

    #@5
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@8
    move-result v2

    #@9
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    #@c
    .line 46
    const/4 v0, 0x0

    #@d
    iput-object v0, p0, Lcom/android/internal/util/IndentingPrintWriter;->mCurrent:[C

    #@f
    .line 47
    return-void
.end method

.method public increaseIndent()V
    .registers 3

    #@0
    .prologue
    .line 40
    iget-object v0, p0, Lcom/android/internal/util/IndentingPrintWriter;->mBuilder:Ljava/lang/StringBuilder;

    #@2
    iget-object v1, p0, Lcom/android/internal/util/IndentingPrintWriter;->mIndent:Ljava/lang/String;

    #@4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7
    .line 41
    const/4 v0, 0x0

    #@8
    iput-object v0, p0, Lcom/android/internal/util/IndentingPrintWriter;->mCurrent:[C

    #@a
    .line 42
    return-void
.end method

.method public printPair(Ljava/lang/String;Ljava/lang/Object;)V
    .registers 5
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8
    move-result-object v0

    #@9
    const-string v1, "="

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    const-string v1, " "

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {p0, v0}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;)V

    #@24
    .line 51
    return-void
.end method

.method public write([CII)V
    .registers 10
    .parameter "buf"
    .parameter "offset"
    .parameter "count"

    #@0
    .prologue
    .line 55
    add-int v0, p2, p3

    #@2
    .line 56
    .local v0, bufferEnd:I
    move v4, p2

    #@3
    .line 57
    .local v4, lineStart:I
    move v2, p2

    #@4
    .local v2, lineEnd:I
    move v3, v2

    #@5
    .line 58
    .end local v2           #lineEnd:I
    .local v3, lineEnd:I
    :goto_5
    if-ge v3, v0, :cond_1d

    #@7
    .line 59
    add-int/lit8 v2, v3, 0x1

    #@9
    .end local v3           #lineEnd:I
    .restart local v2       #lineEnd:I
    aget-char v1, p1, v3

    #@b
    .line 60
    .local v1, ch:C
    const/16 v5, 0xa

    #@d
    if-ne v1, v5, :cond_1b

    #@f
    .line 61
    invoke-direct {p0}, Lcom/android/internal/util/IndentingPrintWriter;->writeIndent()V

    #@12
    .line 62
    sub-int v5, v2, v4

    #@14
    invoke-super {p0, p1, v4, v5}, Ljava/io/PrintWriter;->write([CII)V

    #@17
    .line 63
    move v4, v2

    #@18
    .line 64
    const/4 v5, 0x1

    #@19
    iput-boolean v5, p0, Lcom/android/internal/util/IndentingPrintWriter;->mEmptyLine:Z

    #@1b
    :cond_1b
    move v3, v2

    #@1c
    .line 66
    .end local v2           #lineEnd:I
    .restart local v3       #lineEnd:I
    goto :goto_5

    #@1d
    .line 68
    .end local v1           #ch:C
    :cond_1d
    if-eq v4, v3, :cond_27

    #@1f
    .line 69
    invoke-direct {p0}, Lcom/android/internal/util/IndentingPrintWriter;->writeIndent()V

    #@22
    .line 70
    sub-int v5, v3, v4

    #@24
    invoke-super {p0, p1, v4, v5}, Ljava/io/PrintWriter;->write([CII)V

    #@27
    .line 72
    :cond_27
    return-void
.end method
