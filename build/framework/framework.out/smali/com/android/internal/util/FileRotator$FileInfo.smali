.class Lcom/android/internal/util/FileRotator$FileInfo;
.super Ljava/lang/Object;
.source "FileRotator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/util/FileRotator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FileInfo"
.end annotation


# instance fields
.field public endMillis:J

.field public final prefix:Ljava/lang/String;

.field public startMillis:J


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "prefix"

    #@0
    .prologue
    .line 403
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 404
    invoke-static {p1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Ljava/lang/String;

    #@9
    iput-object v0, p0, Lcom/android/internal/util/FileRotator$FileInfo;->prefix:Ljava/lang/String;

    #@b
    .line 405
    return-void
.end method


# virtual methods
.method public build()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 443
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 444
    .local v0, name:Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/android/internal/util/FileRotator$FileInfo;->prefix:Ljava/lang/String;

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    const/16 v2, 0x2e

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    iget-wide v2, p0, Lcom/android/internal/util/FileRotator$FileInfo;->startMillis:J

    #@13
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const/16 v2, 0x2d

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1c
    .line 445
    iget-wide v1, p0, Lcom/android/internal/util/FileRotator$FileInfo;->endMillis:J

    #@1e
    const-wide v3, 0x7fffffffffffffffL

    #@23
    cmp-long v1, v1, v3

    #@25
    if-eqz v1, :cond_2c

    #@27
    .line 446
    iget-wide v1, p0, Lcom/android/internal/util/FileRotator$FileInfo;->endMillis:J

    #@29
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@2c
    .line 448
    :cond_2c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    return-object v1
.end method

.method public isActive()Z
    .registers 5

    #@0
    .prologue
    .line 455
    iget-wide v0, p0, Lcom/android/internal/util/FileRotator$FileInfo;->endMillis:J

    #@2
    const-wide v2, 0x7fffffffffffffffL

    #@7
    cmp-long v0, v0, v2

    #@9
    if-nez v0, :cond_d

    #@b
    const/4 v0, 0x1

    #@c
    :goto_c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method public parse(Ljava/lang/String;)Z
    .registers 10
    .parameter "name"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v7, -0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 413
    const-wide/16 v5, -0x1

    #@5
    iput-wide v5, p0, Lcom/android/internal/util/FileRotator$FileInfo;->endMillis:J

    #@7
    iput-wide v5, p0, Lcom/android/internal/util/FileRotator$FileInfo;->startMillis:J

    #@9
    .line 415
    const/16 v5, 0x2e

    #@b
    invoke-virtual {p1, v5}, Ljava/lang/String;->lastIndexOf(I)I

    #@e
    move-result v1

    #@f
    .line 416
    .local v1, dotIndex:I
    const/16 v5, 0x2d

    #@11
    invoke-virtual {p1, v5}, Ljava/lang/String;->lastIndexOf(I)I

    #@14
    move-result v0

    #@15
    .line 419
    .local v0, dashIndex:I
    if-eq v1, v7, :cond_19

    #@17
    if-ne v0, v7, :cond_1a

    #@19
    .line 435
    :cond_19
    :goto_19
    return v3

    #@1a
    .line 422
    :cond_1a
    iget-object v5, p0, Lcom/android/internal/util/FileRotator$FileInfo;->prefix:Ljava/lang/String;

    #@1c
    invoke-virtual {p1, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1f
    move-result-object v6

    #@20
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v5

    #@24
    if-eqz v5, :cond_19

    #@26
    .line 425
    add-int/lit8 v5, v1, 0x1

    #@28
    :try_start_28
    invoke-virtual {p1, v5, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2b
    move-result-object v5

    #@2c
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@2f
    move-result-wide v5

    #@30
    iput-wide v5, p0, Lcom/android/internal/util/FileRotator$FileInfo;->startMillis:J

    #@32
    .line 427
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@35
    move-result v5

    #@36
    sub-int/2addr v5, v0

    #@37
    if-ne v5, v4, :cond_42

    #@39
    .line 428
    const-wide v5, 0x7fffffffffffffffL

    #@3e
    iput-wide v5, p0, Lcom/android/internal/util/FileRotator$FileInfo;->endMillis:J

    #@40
    :goto_40
    move v3, v4

    #@41
    .line 433
    goto :goto_19

    #@42
    .line 430
    :cond_42
    add-int/lit8 v5, v0, 0x1

    #@44
    invoke-virtual {p1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@47
    move-result-object v5

    #@48
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@4b
    move-result-wide v5

    #@4c
    iput-wide v5, p0, Lcom/android/internal/util/FileRotator$FileInfo;->endMillis:J
    :try_end_4e
    .catch Ljava/lang/NumberFormatException; {:try_start_28 .. :try_end_4e} :catch_4f

    #@4e
    goto :goto_40

    #@4f
    .line 434
    :catch_4f
    move-exception v2

    #@50
    .line 435
    .local v2, e:Ljava/lang/NumberFormatException;
    goto :goto_19
.end method
