.class Lcom/android/internal/util/StateMachine$LogRecords;
.super Ljava/lang/Object;
.source "StateMachine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/util/StateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LogRecords"
.end annotation


# static fields
.field private static final DEFAULT_SIZE:I = 0x14


# instance fields
.field private mCount:I

.field private mLogRecords:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/android/internal/util/StateMachine$LogRec;",
            ">;"
        }
    .end annotation
.end field

.field private mMaxSize:I

.field private mOldestIndex:I


# direct methods
.method private constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 571
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 563
    new-instance v0, Ljava/util/Vector;

    #@6
    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    #@9
    iput-object v0, p0, Lcom/android/internal/util/StateMachine$LogRecords;->mLogRecords:Ljava/util/Vector;

    #@b
    .line 564
    const/16 v0, 0x14

    #@d
    iput v0, p0, Lcom/android/internal/util/StateMachine$LogRecords;->mMaxSize:I

    #@f
    .line 565
    iput v1, p0, Lcom/android/internal/util/StateMachine$LogRecords;->mOldestIndex:I

    #@11
    .line 566
    iput v1, p0, Lcom/android/internal/util/StateMachine$LogRecords;->mCount:I

    #@13
    .line 572
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/util/StateMachine$1;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 559
    invoke-direct {p0}, Lcom/android/internal/util/StateMachine$LogRecords;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method declared-synchronized add(Landroid/os/Message;Ljava/lang/String;Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V
    .registers 8
    .parameter "msg"
    .parameter "messageInfo"
    .parameter "state"
    .parameter "orgState"

    #@0
    .prologue
    .line 633
    monitor-enter p0

    #@1
    :try_start_1
    iget v1, p0, Lcom/android/internal/util/StateMachine$LogRecords;->mCount:I

    #@3
    add-int/lit8 v1, v1, 0x1

    #@5
    iput v1, p0, Lcom/android/internal/util/StateMachine$LogRecords;->mCount:I

    #@7
    .line 634
    iget-object v1, p0, Lcom/android/internal/util/StateMachine$LogRecords;->mLogRecords:Ljava/util/Vector;

    #@9
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    #@c
    move-result v1

    #@d
    iget v2, p0, Lcom/android/internal/util/StateMachine$LogRecords;->mMaxSize:I

    #@f
    if-ge v1, v2, :cond_1d

    #@11
    .line 635
    iget-object v1, p0, Lcom/android/internal/util/StateMachine$LogRecords;->mLogRecords:Ljava/util/Vector;

    #@13
    new-instance v2, Lcom/android/internal/util/StateMachine$LogRec;

    #@15
    invoke-direct {v2, p1, p2, p3, p4}, Lcom/android/internal/util/StateMachine$LogRec;-><init>(Landroid/os/Message;Ljava/lang/String;Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@18
    invoke-virtual {v1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_1b
    .catchall {:try_start_1 .. :try_end_1b} :catchall_3a

    #@1b
    .line 644
    :goto_1b
    monitor-exit p0

    #@1c
    return-void

    #@1d
    .line 637
    :cond_1d
    :try_start_1d
    iget-object v1, p0, Lcom/android/internal/util/StateMachine$LogRecords;->mLogRecords:Ljava/util/Vector;

    #@1f
    iget v2, p0, Lcom/android/internal/util/StateMachine$LogRecords;->mOldestIndex:I

    #@21
    invoke-virtual {v1, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    #@24
    move-result-object v0

    #@25
    check-cast v0, Lcom/android/internal/util/StateMachine$LogRec;

    #@27
    .line 638
    .local v0, pmi:Lcom/android/internal/util/StateMachine$LogRec;
    iget v1, p0, Lcom/android/internal/util/StateMachine$LogRecords;->mOldestIndex:I

    #@29
    add-int/lit8 v1, v1, 0x1

    #@2b
    iput v1, p0, Lcom/android/internal/util/StateMachine$LogRecords;->mOldestIndex:I

    #@2d
    .line 639
    iget v1, p0, Lcom/android/internal/util/StateMachine$LogRecords;->mOldestIndex:I

    #@2f
    iget v2, p0, Lcom/android/internal/util/StateMachine$LogRecords;->mMaxSize:I

    #@31
    if-lt v1, v2, :cond_36

    #@33
    .line 640
    const/4 v1, 0x0

    #@34
    iput v1, p0, Lcom/android/internal/util/StateMachine$LogRecords;->mOldestIndex:I

    #@36
    .line 642
    :cond_36
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/internal/util/StateMachine$LogRec;->update(Landroid/os/Message;Ljava/lang/String;Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V
    :try_end_39
    .catchall {:try_start_1d .. :try_end_39} :catchall_3a

    #@39
    goto :goto_1b

    #@3a
    .line 633
    .end local v0           #pmi:Lcom/android/internal/util/StateMachine$LogRec;
    :catchall_3a
    move-exception v1

    #@3b
    monitor-exit p0

    #@3c
    throw v1
.end method

.method declared-synchronized cleanup()V
    .registers 2

    #@0
    .prologue
    .line 603
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/util/StateMachine$LogRecords;->mLogRecords:Ljava/util/Vector;

    #@3
    invoke-virtual {v0}, Ljava/util/Vector;->clear()V
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    #@6
    .line 604
    monitor-exit p0

    #@7
    return-void

    #@8
    .line 603
    :catchall_8
    move-exception v0

    #@9
    monitor-exit p0

    #@a
    throw v0
.end method

.method declared-synchronized count()I
    .registers 2

    #@0
    .prologue
    .line 596
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Lcom/android/internal/util/StateMachine$LogRecords;->mCount:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method declared-synchronized get(I)Lcom/android/internal/util/StateMachine$LogRec;
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 612
    monitor-enter p0

    #@1
    :try_start_1
    iget v1, p0, Lcom/android/internal/util/StateMachine$LogRecords;->mOldestIndex:I

    #@3
    add-int v0, v1, p1

    #@5
    .line 613
    .local v0, nextIndex:I
    iget v1, p0, Lcom/android/internal/util/StateMachine$LogRecords;->mMaxSize:I

    #@7
    if-lt v0, v1, :cond_c

    #@9
    .line 614
    iget v1, p0, Lcom/android/internal/util/StateMachine$LogRecords;->mMaxSize:I

    #@b
    sub-int/2addr v0, v1

    #@c
    .line 616
    :cond_c
    invoke-virtual {p0}, Lcom/android/internal/util/StateMachine$LogRecords;->size()I
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_1e

    #@f
    move-result v1

    #@10
    if-lt v0, v1, :cond_15

    #@12
    .line 617
    const/4 v1, 0x0

    #@13
    .line 619
    :goto_13
    monitor-exit p0

    #@14
    return-object v1

    #@15
    :cond_15
    :try_start_15
    iget-object v1, p0, Lcom/android/internal/util/StateMachine$LogRecords;->mLogRecords:Ljava/util/Vector;

    #@17
    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    #@1a
    move-result-object v1

    #@1b
    check-cast v1, Lcom/android/internal/util/StateMachine$LogRec;
    :try_end_1d
    .catchall {:try_start_15 .. :try_end_1d} :catchall_1e

    #@1d
    goto :goto_13

    #@1e
    .line 612
    .end local v0           #nextIndex:I
    :catchall_1e
    move-exception v1

    #@1f
    monitor-exit p0

    #@20
    throw v1
.end method

.method declared-synchronized setSize(I)V
    .registers 3
    .parameter "maxSize"

    #@0
    .prologue
    .line 580
    monitor-enter p0

    #@1
    :try_start_1
    iput p1, p0, Lcom/android/internal/util/StateMachine$LogRecords;->mMaxSize:I

    #@3
    .line 581
    const/4 v0, 0x0

    #@4
    iput v0, p0, Lcom/android/internal/util/StateMachine$LogRecords;->mCount:I

    #@6
    .line 582
    iget-object v0, p0, Lcom/android/internal/util/StateMachine$LogRecords;->mLogRecords:Ljava/util/Vector;

    #@8
    invoke-virtual {v0}, Ljava/util/Vector;->clear()V
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_d

    #@b
    .line 583
    monitor-exit p0

    #@c
    return-void

    #@d
    .line 580
    :catchall_d
    move-exception v0

    #@e
    monitor-exit p0

    #@f
    throw v0
.end method

.method declared-synchronized size()I
    .registers 2

    #@0
    .prologue
    .line 589
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/util/StateMachine$LogRecords;->mLogRecords:Ljava/util/Vector;

    #@3
    invoke-virtual {v0}, Ljava/util/Vector;->size()I
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_9

    #@6
    move-result v0

    #@7
    monitor-exit p0

    #@8
    return v0

    #@9
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method
