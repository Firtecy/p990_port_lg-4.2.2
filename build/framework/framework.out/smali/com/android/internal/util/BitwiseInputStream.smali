.class public Lcom/android/internal/util/BitwiseInputStream;
.super Ljava/lang/Object;
.source "BitwiseInputStream.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/util/BitwiseInputStream$AccessException;
    }
.end annotation


# instance fields
.field private mBuf:[B

.field private mEnd:I

.field private mPos:I


# direct methods
.method public constructor <init>([B)V
    .registers 3
    .parameter "buf"

    #@0
    .prologue
    .line 52
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 53
    iput-object p1, p0, Lcom/android/internal/util/BitwiseInputStream;->mBuf:[B

    #@5
    .line 54
    array-length v0, p1

    #@6
    shl-int/lit8 v0, v0, 0x3

    #@8
    iput v0, p0, Lcom/android/internal/util/BitwiseInputStream;->mEnd:I

    #@a
    .line 55
    const/4 v0, 0x0

    #@b
    iput v0, p0, Lcom/android/internal/util/BitwiseInputStream;->mPos:I

    #@d
    .line 56
    return-void
.end method


# virtual methods
.method public available()I
    .registers 3

    #@0
    .prologue
    .line 62
    iget v0, p0, Lcom/android/internal/util/BitwiseInputStream;->mEnd:I

    #@2
    iget v1, p0, Lcom/android/internal/util/BitwiseInputStream;->mPos:I

    #@4
    sub-int/2addr v0, v1

    #@5
    return v0
.end method

.method public read(I)I
    .registers 8
    .parameter "bits"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v5, 0x8

    #@2
    .line 75
    iget v3, p0, Lcom/android/internal/util/BitwiseInputStream;->mPos:I

    #@4
    ushr-int/lit8 v1, v3, 0x3

    #@6
    .line 76
    .local v1, index:I
    iget v3, p0, Lcom/android/internal/util/BitwiseInputStream;->mPos:I

    #@8
    and-int/lit8 v3, v3, 0x7

    #@a
    rsub-int/lit8 v3, v3, 0x10

    #@c
    sub-int v2, v3, p1

    #@e
    .line 77
    .local v2, offset:I
    if-ltz p1, :cond_19

    #@10
    if-gt p1, v5, :cond_19

    #@12
    iget v3, p0, Lcom/android/internal/util/BitwiseInputStream;->mPos:I

    #@14
    add-int/2addr v3, p1

    #@15
    iget v4, p0, Lcom/android/internal/util/BitwiseInputStream;->mEnd:I

    #@17
    if-le v3, v4, :cond_50

    #@19
    .line 78
    :cond_19
    new-instance v3, Lcom/android/internal/util/BitwiseInputStream$AccessException;

    #@1b
    new-instance v4, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v5, "illegal read (pos "

    #@22
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v4

    #@26
    iget v5, p0, Lcom/android/internal/util/BitwiseInputStream;->mPos:I

    #@28
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v4

    #@2c
    const-string v5, ", end "

    #@2e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v4

    #@32
    iget v5, p0, Lcom/android/internal/util/BitwiseInputStream;->mEnd:I

    #@34
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37
    move-result-object v4

    #@38
    const-string v5, ", bits "

    #@3a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v4

    #@3e
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v4

    #@42
    const-string v5, ")"

    #@44
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v4

    #@48
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v4

    #@4c
    invoke-direct {v3, v4}, Lcom/android/internal/util/BitwiseInputStream$AccessException;-><init>(Ljava/lang/String;)V

    #@4f
    throw v3

    #@50
    .line 81
    :cond_50
    iget-object v3, p0, Lcom/android/internal/util/BitwiseInputStream;->mBuf:[B

    #@52
    aget-byte v3, v3, v1

    #@54
    and-int/lit16 v3, v3, 0xff

    #@56
    shl-int/lit8 v0, v3, 0x8

    #@58
    .line 82
    .local v0, data:I
    if-ge v2, v5, :cond_63

    #@5a
    iget-object v3, p0, Lcom/android/internal/util/BitwiseInputStream;->mBuf:[B

    #@5c
    add-int/lit8 v4, v1, 0x1

    #@5e
    aget-byte v3, v3, v4

    #@60
    and-int/lit16 v3, v3, 0xff

    #@62
    or-int/2addr v0, v3

    #@63
    .line 83
    :cond_63
    ushr-int/2addr v0, v2

    #@64
    .line 84
    const/4 v3, -0x1

    #@65
    rsub-int/lit8 v4, p1, 0x20

    #@67
    ushr-int/2addr v3, v4

    #@68
    and-int/2addr v0, v3

    #@69
    .line 85
    iget v3, p0, Lcom/android/internal/util/BitwiseInputStream;->mPos:I

    #@6b
    add-int/2addr v3, p1

    #@6c
    iput v3, p0, Lcom/android/internal/util/BitwiseInputStream;->mPos:I

    #@6e
    .line 86
    return v0
.end method

.method public readByteArray(I)[B
    .registers 8
    .parameter "bits"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;
        }
    .end annotation

    #@0
    .prologue
    .line 96
    ushr-int/lit8 v5, p1, 0x3

    #@2
    and-int/lit8 v4, p1, 0x7

    #@4
    if-lez v4, :cond_25

    #@6
    const/4 v4, 0x1

    #@7
    :goto_7
    add-int v1, v5, v4

    #@9
    .line 97
    .local v1, bytes:I
    new-array v0, v1, [B

    #@b
    .line 98
    .local v0, arr:[B
    const/4 v2, 0x0

    #@c
    .local v2, i:I
    :goto_c
    if-ge v2, v1, :cond_27

    #@e
    .line 99
    const/16 v4, 0x8

    #@10
    shl-int/lit8 v5, v2, 0x3

    #@12
    sub-int v5, p1, v5

    #@14
    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    #@17
    move-result v3

    #@18
    .line 100
    .local v3, increment:I
    invoke-virtual {p0, v3}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@1b
    move-result v4

    #@1c
    rsub-int/lit8 v5, v3, 0x8

    #@1e
    shl-int/2addr v4, v5

    #@1f
    int-to-byte v4, v4

    #@20
    aput-byte v4, v0, v2

    #@22
    .line 98
    add-int/lit8 v2, v2, 0x1

    #@24
    goto :goto_c

    #@25
    .line 96
    .end local v0           #arr:[B
    .end local v1           #bytes:I
    .end local v2           #i:I
    .end local v3           #increment:I
    :cond_25
    const/4 v4, 0x0

    #@26
    goto :goto_7

    #@27
    .line 102
    .restart local v0       #arr:[B
    .restart local v1       #bytes:I
    .restart local v2       #i:I
    :cond_27
    return-object v0
.end method

.method public skip(I)V
    .registers 5
    .parameter "bits"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/util/BitwiseInputStream$AccessException;
        }
    .end annotation

    #@0
    .prologue
    .line 111
    iget v0, p0, Lcom/android/internal/util/BitwiseInputStream;->mPos:I

    #@2
    add-int/2addr v0, p1

    #@3
    iget v1, p0, Lcom/android/internal/util/BitwiseInputStream;->mEnd:I

    #@5
    if-le v0, v1, :cond_3e

    #@7
    .line 112
    new-instance v0, Lcom/android/internal/util/BitwiseInputStream$AccessException;

    #@9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v2, "illegal skip (pos "

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    iget v2, p0, Lcom/android/internal/util/BitwiseInputStream;->mPos:I

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    const-string v2, ", end "

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    iget v2, p0, Lcom/android/internal/util/BitwiseInputStream;->mEnd:I

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    const-string v2, ", bits "

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    const-string v2, ")"

    #@32
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v1

    #@36
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v1

    #@3a
    invoke-direct {v0, v1}, Lcom/android/internal/util/BitwiseInputStream$AccessException;-><init>(Ljava/lang/String;)V

    #@3d
    throw v0

    #@3e
    .line 115
    :cond_3e
    iget v0, p0, Lcom/android/internal/util/BitwiseInputStream;->mPos:I

    #@40
    add-int/2addr v0, p1

    #@41
    iput v0, p0, Lcom/android/internal/util/BitwiseInputStream;->mPos:I

    #@43
    .line 116
    return-void
.end method
