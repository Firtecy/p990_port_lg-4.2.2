.class public Lcom/android/internal/util/Preconditions;
.super Ljava/lang/Object;
.source "Preconditions.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 23
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)TT;"
        }
    .end annotation

    #@0
    .prologue
    .line 34
    .local p0, reference:Ljava/lang/Object;,"TT;"
    if-nez p0, :cond_8

    #@2
    .line 35
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    #@7
    throw v0

    #@8
    .line 37
    :cond_8
    return-object p0
.end method

.method public static checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter "errorMessage"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    #@0
    .prologue
    .line 51
    .local p0, reference:Ljava/lang/Object;,"TT;"
    if-nez p0, :cond_c

    #@2
    .line 52
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 54
    :cond_c
    return-object p0
.end method

.method public static checkState(Z)V
    .registers 2
    .parameter "expression"

    #@0
    .prologue
    .line 65
    if-nez p0, :cond_8

    #@2
    .line 66
    new-instance v0, Ljava/lang/IllegalStateException;

    #@4
    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    #@7
    throw v0

    #@8
    .line 68
    :cond_8
    return-void
.end method
