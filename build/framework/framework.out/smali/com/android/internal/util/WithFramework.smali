.class Lcom/android/internal/util/WithFramework;
.super Ljava/lang/Object;
.source "WithFramework.java"


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 25
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .registers 9
    .parameter "args"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 31
    array-length v3, p0

    #@3
    if-nez v3, :cond_9

    #@5
    .line 32
    invoke-static {}, Lcom/android/internal/util/WithFramework;->printUsage()V

    #@8
    .line 47
    :goto_8
    return-void

    #@9
    .line 36
    :cond_9
    aget-object v3, p0, v6

    #@b
    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    #@e
    move-result-object v0

    #@f
    .line 38
    .local v0, mainClass:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    const-string v3, "android_runtime"

    #@11
    invoke-static {v3}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@14
    .line 39
    invoke-static {}, Lcom/android/internal/util/WithFramework;->registerNatives()I

    #@17
    move-result v3

    #@18
    if-gez v3, :cond_22

    #@1a
    .line 40
    new-instance v3, Ljava/lang/RuntimeException;

    #@1c
    const-string v4, "Error registering natives."

    #@1e
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@21
    throw v3

    #@22
    .line 43
    :cond_22
    array-length v3, p0

    #@23
    add-int/lit8 v3, v3, -0x1

    #@25
    new-array v2, v3, [Ljava/lang/String;

    #@27
    .line 44
    .local v2, newArgs:[Ljava/lang/String;
    array-length v3, v2

    #@28
    invoke-static {p0, v7, v2, v6, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@2b
    .line 45
    const-string/jumbo v3, "main"

    #@2e
    new-array v4, v7, [Ljava/lang/Class;

    #@30
    const-class v5, [Ljava/lang/String;

    #@32
    aput-object v5, v4, v6

    #@34
    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@37
    move-result-object v1

    #@38
    .line 46
    .local v1, mainMethod:Ljava/lang/reflect/Method;
    const/4 v3, 0x0

    #@39
    new-array v4, v7, [Ljava/lang/Object;

    #@3b
    aput-object v2, v4, v6

    #@3d
    invoke-virtual {v1, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@40
    goto :goto_8
.end method

.method private static printUsage()V
    .registers 3

    #@0
    .prologue
    .line 50
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Usage: dalvikvm "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    const-class v2, Lcom/android/internal/util/WithFramework;

    #@f
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, " [main class] [args]"

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    #@24
    .line 52
    return-void
.end method

.method static native registerNatives()I
.end method
