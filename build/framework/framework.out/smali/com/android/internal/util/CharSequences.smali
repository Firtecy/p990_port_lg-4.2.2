.class public Lcom/android/internal/util/CharSequences;
.super Ljava/lang/Object;
.source "CharSequences.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 22
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static compareToIgnoreCase(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I
    .registers 12
    .parameter "me"
    .parameter "another"

    #@0
    .prologue
    .line 119
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@3
    move-result v4

    #@4
    .local v4, myLen:I
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@7
    move-result v0

    #@8
    .line 120
    .local v0, anotherLen:I
    const/4 v5, 0x0

    #@9
    .local v5, myPos:I
    const/4 v1, 0x0

    #@a
    .line 121
    .local v1, anotherPos:I
    if-ge v4, v0, :cond_2a

    #@c
    move v3, v4

    #@d
    .local v3, end:I
    :goto_d
    move v2, v1

    #@e
    .end local v1           #anotherPos:I
    .local v2, anotherPos:I
    move v6, v5

    #@f
    .line 123
    .end local v5           #myPos:I
    .local v6, myPos:I
    :goto_f
    if-ge v6, v3, :cond_2c

    #@11
    .line 124
    add-int/lit8 v5, v6, 0x1

    #@13
    .end local v6           #myPos:I
    .restart local v5       #myPos:I
    invoke-interface {p0, v6}, Ljava/lang/CharSequence;->charAt(I)C

    #@16
    move-result v8

    #@17
    invoke-static {v8}, Ljava/lang/Character;->toLowerCase(C)C

    #@1a
    move-result v8

    #@1b
    add-int/lit8 v1, v2, 0x1

    #@1d
    .end local v2           #anotherPos:I
    .restart local v1       #anotherPos:I
    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    #@20
    move-result v9

    #@21
    invoke-static {v9}, Ljava/lang/Character;->toLowerCase(C)C

    #@24
    move-result v9

    #@25
    sub-int v7, v8, v9

    #@27
    .local v7, result:I
    if-eqz v7, :cond_31

    #@29
    .line 129
    .end local v7           #result:I
    :goto_29
    return v7

    #@2a
    .end local v3           #end:I
    :cond_2a
    move v3, v0

    #@2b
    .line 121
    goto :goto_d

    #@2c
    .line 129
    .end local v1           #anotherPos:I
    .end local v5           #myPos:I
    .restart local v2       #anotherPos:I
    .restart local v3       #end:I
    .restart local v6       #myPos:I
    :cond_2c
    sub-int v7, v4, v0

    #@2e
    move v1, v2

    #@2f
    .end local v2           #anotherPos:I
    .restart local v1       #anotherPos:I
    move v5, v6

    #@30
    .end local v6           #myPos:I
    .restart local v5       #myPos:I
    goto :goto_29

    #@31
    .restart local v7       #result:I
    :cond_31
    move v2, v1

    #@32
    .end local v1           #anotherPos:I
    .restart local v2       #anotherPos:I
    move v6, v5

    #@33
    .end local v5           #myPos:I
    .restart local v6       #myPos:I
    goto :goto_f
.end method

.method public static equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    .registers 7
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 97
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@4
    move-result v3

    #@5
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@8
    move-result v4

    #@9
    if-eq v3, v4, :cond_c

    #@b
    .line 107
    :cond_b
    :goto_b
    return v2

    #@c
    .line 101
    :cond_c
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@f
    move-result v1

    #@10
    .line 102
    .local v1, length:I
    const/4 v0, 0x0

    #@11
    .local v0, i:I
    :goto_11
    if-ge v0, v1, :cond_20

    #@13
    .line 103
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    #@16
    move-result v3

    #@17
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    #@1a
    move-result v4

    #@1b
    if-ne v3, v4, :cond_b

    #@1d
    .line 102
    add-int/lit8 v0, v0, 0x1

    #@1f
    goto :goto_11

    #@20
    .line 107
    :cond_20
    const/4 v2, 0x1

    #@21
    goto :goto_b
.end method

.method public static forAsciiBytes([B)Ljava/lang/CharSequence;
    .registers 2
    .parameter "bytes"

    #@0
    .prologue
    .line 31
    new-instance v0, Lcom/android/internal/util/CharSequences$1;

    #@2
    invoke-direct {v0, p0}, Lcom/android/internal/util/CharSequences$1;-><init>([B)V

    #@5
    return-object v0
.end method

.method public static forAsciiBytes([BII)Ljava/lang/CharSequence;
    .registers 4
    .parameter "bytes"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 63
    array-length v0, p0

    #@1
    invoke-static {p1, p2, v0}, Lcom/android/internal/util/CharSequences;->validate(III)V

    #@4
    .line 64
    new-instance v0, Lcom/android/internal/util/CharSequences$2;

    #@6
    invoke-direct {v0, p0, p1, p2}, Lcom/android/internal/util/CharSequences$2;-><init>([BII)V

    #@9
    return-object v0
.end method

.method static validate(III)V
    .registers 4
    .parameter "start"
    .parameter "end"
    .parameter "length"

    #@0
    .prologue
    .line 87
    if-gez p0, :cond_8

    #@2
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    #@4
    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@7
    throw v0

    #@8
    .line 88
    :cond_8
    if-gez p1, :cond_10

    #@a
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    #@c
    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@f
    throw v0

    #@10
    .line 89
    :cond_10
    if-le p1, p2, :cond_18

    #@12
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    #@14
    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@17
    throw v0

    #@18
    .line 90
    :cond_18
    if-le p0, p1, :cond_20

    #@1a
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    #@1c
    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@1f
    throw v0

    #@20
    .line 91
    :cond_20
    return-void
.end method
