.class public Lcom/android/internal/util/StateMachine$LogRec;
.super Ljava/lang/Object;
.source "StateMachine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/util/StateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LogRec"
.end annotation


# instance fields
.field private mInfo:Ljava/lang/String;

.field private mOrgState:Lcom/android/internal/util/State;

.field private mState:Lcom/android/internal/util/State;

.field private mTime:J

.field private mWhat:I


# direct methods
.method constructor <init>(Landroid/os/Message;Ljava/lang/String;Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V
    .registers 5
    .parameter "msg"
    .parameter "info"
    .parameter "state"
    .parameter "orgState"

    #@0
    .prologue
    .line 464
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 465
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/android/internal/util/StateMachine$LogRec;->update(Landroid/os/Message;Ljava/lang/String;Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    #@6
    .line 466
    return-void
.end method


# virtual methods
.method public getInfo()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 500
    iget-object v0, p0, Lcom/android/internal/util/StateMachine$LogRec;->mInfo:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getOriginalState()Lcom/android/internal/util/State;
    .registers 2

    #@0
    .prologue
    .line 514
    iget-object v0, p0, Lcom/android/internal/util/StateMachine$LogRec;->mOrgState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method public getState()Lcom/android/internal/util/State;
    .registers 2

    #@0
    .prologue
    .line 507
    iget-object v0, p0, Lcom/android/internal/util/StateMachine$LogRec;->mState:Lcom/android/internal/util/State;

    #@2
    return-object v0
.end method

.method public getTime()J
    .registers 3

    #@0
    .prologue
    .line 486
    iget-wide v0, p0, Lcom/android/internal/util/StateMachine$LogRec;->mTime:J

    #@2
    return-wide v0
.end method

.method public getWhat()J
    .registers 3

    #@0
    .prologue
    .line 493
    iget v0, p0, Lcom/android/internal/util/StateMachine$LogRec;->mWhat:I

    #@2
    int-to-long v0, v0

    #@3
    return-wide v0
.end method

.method public toString(Lcom/android/internal/util/StateMachine;)Ljava/lang/String;
    .registers 8
    .parameter "sm"

    #@0
    .prologue
    .line 521
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 522
    .local v1, sb:Ljava/lang/StringBuilder;
    const-string/jumbo v3, "time="

    #@8
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    .line 523
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    #@e
    move-result-object v0

    #@f
    .line 524
    .local v0, c:Ljava/util/Calendar;
    iget-wide v3, p0, Lcom/android/internal/util/StateMachine$LogRec;->mTime:J

    #@11
    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@14
    .line 525
    const-string v3, "%tm-%td %tH:%tM:%tS.%tL"

    #@16
    const/4 v4, 0x6

    #@17
    new-array v4, v4, [Ljava/lang/Object;

    #@19
    const/4 v5, 0x0

    #@1a
    aput-object v0, v4, v5

    #@1c
    const/4 v5, 0x1

    #@1d
    aput-object v0, v4, v5

    #@1f
    const/4 v5, 0x2

    #@20
    aput-object v0, v4, v5

    #@22
    const/4 v5, 0x3

    #@23
    aput-object v0, v4, v5

    #@25
    const/4 v5, 0x4

    #@26
    aput-object v0, v4, v5

    #@28
    const/4 v5, 0x5

    #@29
    aput-object v0, v4, v5

    #@2b
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    .line 526
    const-string v3, " state="

    #@34
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    .line 527
    iget-object v3, p0, Lcom/android/internal/util/StateMachine$LogRec;->mState:Lcom/android/internal/util/State;

    #@39
    if-nez v3, :cond_8e

    #@3b
    const-string v3, "<null>"

    #@3d
    :goto_3d
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    .line 528
    const-string v3, " orgState="

    #@42
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    .line 529
    iget-object v3, p0, Lcom/android/internal/util/StateMachine$LogRec;->mOrgState:Lcom/android/internal/util/State;

    #@47
    if-nez v3, :cond_95

    #@49
    const-string v3, "<null>"

    #@4b
    :goto_4b
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    .line 530
    const-string v3, " what="

    #@50
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    .line 531
    iget v3, p0, Lcom/android/internal/util/StateMachine$LogRec;->mWhat:I

    #@55
    invoke-virtual {p1, v3}, Lcom/android/internal/util/StateMachine;->getWhatToString(I)Ljava/lang/String;

    #@58
    move-result-object v2

    #@59
    .line 532
    .local v2, what:Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@5c
    move-result v3

    #@5d
    if-eqz v3, :cond_9c

    #@5f
    .line 533
    iget v3, p0, Lcom/android/internal/util/StateMachine$LogRec;->mWhat:I

    #@61
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@64
    .line 534
    const-string v3, "(0x"

    #@66
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    .line 535
    iget v3, p0, Lcom/android/internal/util/StateMachine$LogRec;->mWhat:I

    #@6b
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@6e
    move-result-object v3

    #@6f
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    .line 536
    const-string v3, ")"

    #@74
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    .line 540
    :goto_77
    iget-object v3, p0, Lcom/android/internal/util/StateMachine$LogRec;->mInfo:Ljava/lang/String;

    #@79
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@7c
    move-result v3

    #@7d
    if-nez v3, :cond_89

    #@7f
    .line 541
    const-string v3, " "

    #@81
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    .line 542
    iget-object v3, p0, Lcom/android/internal/util/StateMachine$LogRec;->mInfo:Ljava/lang/String;

    #@86
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    .line 544
    :cond_89
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8c
    move-result-object v3

    #@8d
    return-object v3

    #@8e
    .line 527
    .end local v2           #what:Ljava/lang/String;
    :cond_8e
    iget-object v3, p0, Lcom/android/internal/util/StateMachine$LogRec;->mState:Lcom/android/internal/util/State;

    #@90
    invoke-virtual {v3}, Lcom/android/internal/util/State;->getName()Ljava/lang/String;

    #@93
    move-result-object v3

    #@94
    goto :goto_3d

    #@95
    .line 529
    :cond_95
    iget-object v3, p0, Lcom/android/internal/util/StateMachine$LogRec;->mOrgState:Lcom/android/internal/util/State;

    #@97
    invoke-virtual {v3}, Lcom/android/internal/util/State;->getName()Ljava/lang/String;

    #@9a
    move-result-object v3

    #@9b
    goto :goto_4b

    #@9c
    .line 538
    .restart local v2       #what:Ljava/lang/String;
    :cond_9c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    goto :goto_77
.end method

.method public update(Landroid/os/Message;Ljava/lang/String;Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V
    .registers 7
    .parameter "msg"
    .parameter "info"
    .parameter "state"
    .parameter "orgState"

    #@0
    .prologue
    .line 475
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3
    move-result-wide v0

    #@4
    iput-wide v0, p0, Lcom/android/internal/util/StateMachine$LogRec;->mTime:J

    #@6
    .line 476
    if-eqz p1, :cond_13

    #@8
    iget v0, p1, Landroid/os/Message;->what:I

    #@a
    :goto_a
    iput v0, p0, Lcom/android/internal/util/StateMachine$LogRec;->mWhat:I

    #@c
    .line 477
    iput-object p2, p0, Lcom/android/internal/util/StateMachine$LogRec;->mInfo:Ljava/lang/String;

    #@e
    .line 478
    iput-object p3, p0, Lcom/android/internal/util/StateMachine$LogRec;->mState:Lcom/android/internal/util/State;

    #@10
    .line 479
    iput-object p4, p0, Lcom/android/internal/util/StateMachine$LogRec;->mOrgState:Lcom/android/internal/util/State;

    #@12
    .line 480
    return-void

    #@13
    .line 476
    :cond_13
    const/4 v0, 0x0

    #@14
    goto :goto_a
.end method
