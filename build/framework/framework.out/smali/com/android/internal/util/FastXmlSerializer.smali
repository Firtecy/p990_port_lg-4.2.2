.class public Lcom/android/internal/util/FastXmlSerializer;
.super Ljava/lang/Object;
.source "FastXmlSerializer.java"

# interfaces
.implements Lorg/xmlpull/v1/XmlSerializer;


# static fields
.field private static final BUFFER_LEN:I = 0x2000

.field private static final ESCAPE_TABLE:[Ljava/lang/String;


# instance fields
.field private mBytes:Ljava/nio/ByteBuffer;

.field private mCharset:Ljava/nio/charset/CharsetEncoder;

.field private mInTag:Z

.field private mOutputStream:Ljava/io/OutputStream;

.field private mPos:I

.field private final mText:[C

.field private mWriter:Ljava/io/Writer;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 40
    const/16 v0, 0x40

    #@3
    new-array v0, v0, [Ljava/lang/String;

    #@5
    const/4 v1, 0x0

    #@6
    aput-object v3, v0, v1

    #@8
    const/4 v1, 0x1

    #@9
    aput-object v3, v0, v1

    #@b
    const/4 v1, 0x2

    #@c
    aput-object v3, v0, v1

    #@e
    const/4 v1, 0x3

    #@f
    aput-object v3, v0, v1

    #@11
    const/4 v1, 0x4

    #@12
    aput-object v3, v0, v1

    #@14
    const/4 v1, 0x5

    #@15
    aput-object v3, v0, v1

    #@17
    const/4 v1, 0x6

    #@18
    aput-object v3, v0, v1

    #@1a
    const/4 v1, 0x7

    #@1b
    aput-object v3, v0, v1

    #@1d
    const/16 v1, 0x8

    #@1f
    aput-object v3, v0, v1

    #@21
    const/16 v1, 0x9

    #@23
    aput-object v3, v0, v1

    #@25
    const/16 v1, 0xa

    #@27
    aput-object v3, v0, v1

    #@29
    const/16 v1, 0xb

    #@2b
    aput-object v3, v0, v1

    #@2d
    const/16 v1, 0xc

    #@2f
    aput-object v3, v0, v1

    #@31
    const/16 v1, 0xd

    #@33
    aput-object v3, v0, v1

    #@35
    const/16 v1, 0xe

    #@37
    aput-object v3, v0, v1

    #@39
    const/16 v1, 0xf

    #@3b
    aput-object v3, v0, v1

    #@3d
    const/16 v1, 0x10

    #@3f
    aput-object v3, v0, v1

    #@41
    const/16 v1, 0x11

    #@43
    aput-object v3, v0, v1

    #@45
    const/16 v1, 0x12

    #@47
    aput-object v3, v0, v1

    #@49
    const/16 v1, 0x13

    #@4b
    aput-object v3, v0, v1

    #@4d
    const/16 v1, 0x14

    #@4f
    aput-object v3, v0, v1

    #@51
    const/16 v1, 0x15

    #@53
    aput-object v3, v0, v1

    #@55
    const/16 v1, 0x16

    #@57
    aput-object v3, v0, v1

    #@59
    const/16 v1, 0x17

    #@5b
    aput-object v3, v0, v1

    #@5d
    const/16 v1, 0x18

    #@5f
    aput-object v3, v0, v1

    #@61
    const/16 v1, 0x19

    #@63
    aput-object v3, v0, v1

    #@65
    const/16 v1, 0x1a

    #@67
    aput-object v3, v0, v1

    #@69
    const/16 v1, 0x1b

    #@6b
    aput-object v3, v0, v1

    #@6d
    const/16 v1, 0x1c

    #@6f
    aput-object v3, v0, v1

    #@71
    const/16 v1, 0x1d

    #@73
    aput-object v3, v0, v1

    #@75
    const/16 v1, 0x1e

    #@77
    aput-object v3, v0, v1

    #@79
    const/16 v1, 0x1f

    #@7b
    aput-object v3, v0, v1

    #@7d
    const/16 v1, 0x20

    #@7f
    aput-object v3, v0, v1

    #@81
    const/16 v1, 0x21

    #@83
    aput-object v3, v0, v1

    #@85
    const/16 v1, 0x22

    #@87
    const-string v2, "&quot;"

    #@89
    aput-object v2, v0, v1

    #@8b
    const/16 v1, 0x23

    #@8d
    aput-object v3, v0, v1

    #@8f
    const/16 v1, 0x24

    #@91
    aput-object v3, v0, v1

    #@93
    const/16 v1, 0x25

    #@95
    aput-object v3, v0, v1

    #@97
    const/16 v1, 0x26

    #@99
    const-string v2, "&amp;"

    #@9b
    aput-object v2, v0, v1

    #@9d
    const/16 v1, 0x27

    #@9f
    aput-object v3, v0, v1

    #@a1
    const/16 v1, 0x28

    #@a3
    aput-object v3, v0, v1

    #@a5
    const/16 v1, 0x29

    #@a7
    aput-object v3, v0, v1

    #@a9
    const/16 v1, 0x2a

    #@ab
    aput-object v3, v0, v1

    #@ad
    const/16 v1, 0x2b

    #@af
    aput-object v3, v0, v1

    #@b1
    const/16 v1, 0x2c

    #@b3
    aput-object v3, v0, v1

    #@b5
    const/16 v1, 0x2d

    #@b7
    aput-object v3, v0, v1

    #@b9
    const/16 v1, 0x2e

    #@bb
    aput-object v3, v0, v1

    #@bd
    const/16 v1, 0x2f

    #@bf
    aput-object v3, v0, v1

    #@c1
    const/16 v1, 0x30

    #@c3
    aput-object v3, v0, v1

    #@c5
    const/16 v1, 0x31

    #@c7
    aput-object v3, v0, v1

    #@c9
    const/16 v1, 0x32

    #@cb
    aput-object v3, v0, v1

    #@cd
    const/16 v1, 0x33

    #@cf
    aput-object v3, v0, v1

    #@d1
    const/16 v1, 0x34

    #@d3
    aput-object v3, v0, v1

    #@d5
    const/16 v1, 0x35

    #@d7
    aput-object v3, v0, v1

    #@d9
    const/16 v1, 0x36

    #@db
    aput-object v3, v0, v1

    #@dd
    const/16 v1, 0x37

    #@df
    aput-object v3, v0, v1

    #@e1
    const/16 v1, 0x38

    #@e3
    aput-object v3, v0, v1

    #@e5
    const/16 v1, 0x39

    #@e7
    aput-object v3, v0, v1

    #@e9
    const/16 v1, 0x3a

    #@eb
    aput-object v3, v0, v1

    #@ed
    const/16 v1, 0x3b

    #@ef
    aput-object v3, v0, v1

    #@f1
    const/16 v1, 0x3c

    #@f3
    const-string v2, "&lt;"

    #@f5
    aput-object v2, v0, v1

    #@f7
    const/16 v1, 0x3d

    #@f9
    aput-object v3, v0, v1

    #@fb
    const/16 v1, 0x3e

    #@fd
    const-string v2, "&gt;"

    #@ff
    aput-object v2, v0, v1

    #@101
    const/16 v1, 0x3f

    #@103
    aput-object v3, v0, v1

    #@105
    sput-object v0, Lcom/android/internal/util/FastXmlSerializer;->ESCAPE_TABLE:[Ljava/lang/String;

    #@107
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/16 v1, 0x2000

    #@2
    .line 39
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 53
    new-array v0, v1, [C

    #@7
    iput-object v0, p0, Lcom/android/internal/util/FastXmlSerializer;->mText:[C

    #@9
    .line 60
    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@c
    move-result-object v0

    #@d
    iput-object v0, p0, Lcom/android/internal/util/FastXmlSerializer;->mBytes:Ljava/nio/ByteBuffer;

    #@f
    return-void
.end method

.method private append(C)V
    .registers 4
    .parameter "c"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 65
    iget v0, p0, Lcom/android/internal/util/FastXmlSerializer;->mPos:I

    #@2
    .line 66
    .local v0, pos:I
    const/16 v1, 0x1fff

    #@4
    if-lt v0, v1, :cond_b

    #@6
    .line 67
    invoke-virtual {p0}, Lcom/android/internal/util/FastXmlSerializer;->flush()V

    #@9
    .line 68
    iget v0, p0, Lcom/android/internal/util/FastXmlSerializer;->mPos:I

    #@b
    .line 70
    :cond_b
    iget-object v1, p0, Lcom/android/internal/util/FastXmlSerializer;->mText:[C

    #@d
    aput-char p1, v1, v0

    #@f
    .line 71
    add-int/lit8 v1, v0, 0x1

    #@11
    iput v1, p0, Lcom/android/internal/util/FastXmlSerializer;->mPos:I

    #@13
    .line 72
    return-void
.end method

.method private append(Ljava/lang/String;)V
    .registers 4
    .parameter "str"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 113
    const/4 v0, 0x0

    #@1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@4
    move-result v1

    #@5
    invoke-direct {p0, p1, v0, v1}, Lcom/android/internal/util/FastXmlSerializer;->append(Ljava/lang/String;II)V

    #@8
    .line 114
    return-void
.end method

.method private append(Ljava/lang/String;II)V
    .registers 9
    .parameter "str"
    .parameter "i"
    .parameter "length"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v4, 0x2000

    #@2
    .line 75
    if-le p3, v4, :cond_15

    #@4
    .line 76
    add-int v0, p2, p3

    #@6
    .line 77
    .local v0, end:I
    :goto_6
    if-ge p2, v0, :cond_2b

    #@8
    .line 78
    add-int/lit16 v1, p2, 0x2000

    #@a
    .line 79
    .local v1, next:I
    if-ge v1, v0, :cond_12

    #@c
    move v3, v4

    #@d
    :goto_d
    invoke-direct {p0, p1, p2, v3}, Lcom/android/internal/util/FastXmlSerializer;->append(Ljava/lang/String;II)V

    #@10
    .line 80
    move p2, v1

    #@11
    .line 81
    goto :goto_6

    #@12
    .line 79
    :cond_12
    sub-int v3, v0, p2

    #@14
    goto :goto_d

    #@15
    .line 84
    .end local v0           #end:I
    .end local v1           #next:I
    :cond_15
    iget v2, p0, Lcom/android/internal/util/FastXmlSerializer;->mPos:I

    #@17
    .line 85
    .local v2, pos:I
    add-int v3, v2, p3

    #@19
    if-le v3, v4, :cond_20

    #@1b
    .line 86
    invoke-virtual {p0}, Lcom/android/internal/util/FastXmlSerializer;->flush()V

    #@1e
    .line 87
    iget v2, p0, Lcom/android/internal/util/FastXmlSerializer;->mPos:I

    #@20
    .line 89
    :cond_20
    add-int v3, p2, p3

    #@22
    iget-object v4, p0, Lcom/android/internal/util/FastXmlSerializer;->mText:[C

    #@24
    invoke-virtual {p1, p2, v3, v4, v2}, Ljava/lang/String;->getChars(II[CI)V

    #@27
    .line 90
    add-int v3, v2, p3

    #@29
    iput v3, p0, Lcom/android/internal/util/FastXmlSerializer;->mPos:I

    #@2b
    .line 91
    .end local v2           #pos:I
    :cond_2b
    return-void
.end method

.method private append([CII)V
    .registers 9
    .parameter "buf"
    .parameter "i"
    .parameter "length"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v4, 0x2000

    #@2
    .line 94
    if-le p3, v4, :cond_15

    #@4
    .line 95
    add-int v0, p2, p3

    #@6
    .line 96
    .local v0, end:I
    :goto_6
    if-ge p2, v0, :cond_29

    #@8
    .line 97
    add-int/lit16 v1, p2, 0x2000

    #@a
    .line 98
    .local v1, next:I
    if-ge v1, v0, :cond_12

    #@c
    move v3, v4

    #@d
    :goto_d
    invoke-direct {p0, p1, p2, v3}, Lcom/android/internal/util/FastXmlSerializer;->append([CII)V

    #@10
    .line 99
    move p2, v1

    #@11
    .line 100
    goto :goto_6

    #@12
    .line 98
    :cond_12
    sub-int v3, v0, p2

    #@14
    goto :goto_d

    #@15
    .line 103
    .end local v0           #end:I
    .end local v1           #next:I
    :cond_15
    iget v2, p0, Lcom/android/internal/util/FastXmlSerializer;->mPos:I

    #@17
    .line 104
    .local v2, pos:I
    add-int v3, v2, p3

    #@19
    if-le v3, v4, :cond_20

    #@1b
    .line 105
    invoke-virtual {p0}, Lcom/android/internal/util/FastXmlSerializer;->flush()V

    #@1e
    .line 106
    iget v2, p0, Lcom/android/internal/util/FastXmlSerializer;->mPos:I

    #@20
    .line 108
    :cond_20
    iget-object v3, p0, Lcom/android/internal/util/FastXmlSerializer;->mText:[C

    #@22
    invoke-static {p1, p2, v3, v2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@25
    .line 109
    add-int v3, v2, p3

    #@27
    iput v3, p0, Lcom/android/internal/util/FastXmlSerializer;->mPos:I

    #@29
    .line 110
    .end local v2           #pos:I
    :cond_29
    return-void
.end method

.method private escapeAndAppendString(Ljava/lang/String;)V
    .registers 10
    .parameter "string"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 117
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@3
    move-result v0

    #@4
    .line 118
    .local v0, N:I
    sget-object v7, Lcom/android/internal/util/FastXmlSerializer;->ESCAPE_TABLE:[Ljava/lang/String;

    #@6
    array-length v7, v7

    #@7
    int-to-char v1, v7

    #@8
    .line 119
    .local v1, NE:C
    sget-object v4, Lcom/android/internal/util/FastXmlSerializer;->ESCAPE_TABLE:[Ljava/lang/String;

    #@a
    .line 120
    .local v4, escapes:[Ljava/lang/String;
    const/4 v5, 0x0

    #@b
    .line 122
    .local v5, lastPos:I
    const/4 v6, 0x0

    #@c
    .local v6, pos:I
    :goto_c
    if-ge v6, v0, :cond_28

    #@e
    .line 123
    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    #@11
    move-result v2

    #@12
    .line 124
    .local v2, c:C
    if-lt v2, v1, :cond_17

    #@14
    .line 122
    :cond_14
    :goto_14
    add-int/lit8 v6, v6, 0x1

    #@16
    goto :goto_c

    #@17
    .line 125
    :cond_17
    aget-object v3, v4, v2

    #@19
    .line 126
    .local v3, escape:Ljava/lang/String;
    if-eqz v3, :cond_14

    #@1b
    .line 127
    if-ge v5, v6, :cond_22

    #@1d
    sub-int v7, v6, v5

    #@1f
    invoke-direct {p0, p1, v5, v7}, Lcom/android/internal/util/FastXmlSerializer;->append(Ljava/lang/String;II)V

    #@22
    .line 128
    :cond_22
    add-int/lit8 v5, v6, 0x1

    #@24
    .line 129
    invoke-direct {p0, v3}, Lcom/android/internal/util/FastXmlSerializer;->append(Ljava/lang/String;)V

    #@27
    goto :goto_14

    #@28
    .line 131
    .end local v2           #c:C
    .end local v3           #escape:Ljava/lang/String;
    :cond_28
    if-ge v5, v6, :cond_2f

    #@2a
    sub-int v7, v6, v5

    #@2c
    invoke-direct {p0, p1, v5, v7}, Lcom/android/internal/util/FastXmlSerializer;->append(Ljava/lang/String;II)V

    #@2f
    .line 132
    :cond_2f
    return-void
.end method

.method private escapeAndAppendString([CII)V
    .registers 12
    .parameter "buf"
    .parameter "start"
    .parameter "len"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 135
    sget-object v7, Lcom/android/internal/util/FastXmlSerializer;->ESCAPE_TABLE:[Ljava/lang/String;

    #@2
    array-length v7, v7

    #@3
    int-to-char v0, v7

    #@4
    .line 136
    .local v0, NE:C
    sget-object v4, Lcom/android/internal/util/FastXmlSerializer;->ESCAPE_TABLE:[Ljava/lang/String;

    #@6
    .line 137
    .local v4, escapes:[Ljava/lang/String;
    add-int v2, p2, p3

    #@8
    .line 138
    .local v2, end:I
    move v5, p2

    #@9
    .line 140
    .local v5, lastPos:I
    move v6, p2

    #@a
    .local v6, pos:I
    :goto_a
    if-ge v6, v2, :cond_24

    #@c
    .line 141
    aget-char v1, p1, v6

    #@e
    .line 142
    .local v1, c:C
    if-lt v1, v0, :cond_13

    #@10
    .line 140
    :cond_10
    :goto_10
    add-int/lit8 v6, v6, 0x1

    #@12
    goto :goto_a

    #@13
    .line 143
    :cond_13
    aget-object v3, v4, v1

    #@15
    .line 144
    .local v3, escape:Ljava/lang/String;
    if-eqz v3, :cond_10

    #@17
    .line 145
    if-ge v5, v6, :cond_1e

    #@19
    sub-int v7, v6, v5

    #@1b
    invoke-direct {p0, p1, v5, v7}, Lcom/android/internal/util/FastXmlSerializer;->append([CII)V

    #@1e
    .line 146
    :cond_1e
    add-int/lit8 v5, v6, 0x1

    #@20
    .line 147
    invoke-direct {p0, v3}, Lcom/android/internal/util/FastXmlSerializer;->append(Ljava/lang/String;)V

    #@23
    goto :goto_10

    #@24
    .line 149
    .end local v1           #c:C
    .end local v3           #escape:Ljava/lang/String;
    :cond_24
    if-ge v5, v6, :cond_2b

    #@26
    sub-int v7, v6, v5

    #@28
    invoke-direct {p0, p1, v5, v7}, Lcom/android/internal/util/FastXmlSerializer;->append([CII)V

    #@2b
    .line 150
    :cond_2b
    return-void
.end method

.method private flushBytes()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 210
    iget-object v1, p0, Lcom/android/internal/util/FastXmlSerializer;->mBytes:Ljava/nio/ByteBuffer;

    #@2
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    #@5
    move-result v0

    #@6
    .local v0, position:I
    if-lez v0, :cond_1e

    #@8
    .line 211
    iget-object v1, p0, Lcom/android/internal/util/FastXmlSerializer;->mBytes:Ljava/nio/ByteBuffer;

    #@a
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    #@d
    .line 212
    iget-object v1, p0, Lcom/android/internal/util/FastXmlSerializer;->mOutputStream:Ljava/io/OutputStream;

    #@f
    iget-object v2, p0, Lcom/android/internal/util/FastXmlSerializer;->mBytes:Ljava/nio/ByteBuffer;

    #@11
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    #@14
    move-result-object v2

    #@15
    const/4 v3, 0x0

    #@16
    invoke-virtual {v1, v2, v3, v0}, Ljava/io/OutputStream;->write([BII)V

    #@19
    .line 213
    iget-object v1, p0, Lcom/android/internal/util/FastXmlSerializer;->mBytes:Ljava/nio/ByteBuffer;

    #@1b
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    #@1e
    .line 215
    :cond_1e
    return-void
.end method


# virtual methods
.method public attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    .registers 5
    .parameter "namespace"
    .parameter "name"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 154
    const/16 v0, 0x20

    #@2
    invoke-direct {p0, v0}, Lcom/android/internal/util/FastXmlSerializer;->append(C)V

    #@5
    .line 155
    if-eqz p1, :cond_f

    #@7
    .line 156
    invoke-direct {p0, p1}, Lcom/android/internal/util/FastXmlSerializer;->append(Ljava/lang/String;)V

    #@a
    .line 157
    const/16 v0, 0x3a

    #@c
    invoke-direct {p0, v0}, Lcom/android/internal/util/FastXmlSerializer;->append(C)V

    #@f
    .line 159
    :cond_f
    invoke-direct {p0, p2}, Lcom/android/internal/util/FastXmlSerializer;->append(Ljava/lang/String;)V

    #@12
    .line 160
    const-string v0, "=\""

    #@14
    invoke-direct {p0, v0}, Lcom/android/internal/util/FastXmlSerializer;->append(Ljava/lang/String;)V

    #@17
    .line 162
    invoke-direct {p0, p3}, Lcom/android/internal/util/FastXmlSerializer;->escapeAndAppendString(Ljava/lang/String;)V

    #@1a
    .line 163
    const/16 v0, 0x22

    #@1c
    invoke-direct {p0, v0}, Lcom/android/internal/util/FastXmlSerializer;->append(C)V

    #@1f
    .line 164
    return-object p0
.end method

.method public cdsect(Ljava/lang/String;)V
    .registers 3
    .parameter "text"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 169
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@5
    throw v0
.end method

.method public comment(Ljava/lang/String;)V
    .registers 3
    .parameter "text"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 174
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@5
    throw v0
.end method

.method public docdecl(Ljava/lang/String;)V
    .registers 3
    .parameter "text"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 179
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@5
    throw v0
.end method

.method public endDocument()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/android/internal/util/FastXmlSerializer;->flush()V

    #@3
    .line 184
    return-void
.end method

.method public endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    .registers 4
    .parameter "namespace"
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 188
    iget-boolean v0, p0, Lcom/android/internal/util/FastXmlSerializer;->mInTag:Z

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 189
    const-string v0, " />\n"

    #@6
    invoke-direct {p0, v0}, Lcom/android/internal/util/FastXmlSerializer;->append(Ljava/lang/String;)V

    #@9
    .line 199
    :goto_9
    const/4 v0, 0x0

    #@a
    iput-boolean v0, p0, Lcom/android/internal/util/FastXmlSerializer;->mInTag:Z

    #@c
    .line 200
    return-object p0

    #@d
    .line 191
    :cond_d
    const-string v0, "</"

    #@f
    invoke-direct {p0, v0}, Lcom/android/internal/util/FastXmlSerializer;->append(Ljava/lang/String;)V

    #@12
    .line 192
    if-eqz p1, :cond_1c

    #@14
    .line 193
    invoke-direct {p0, p1}, Lcom/android/internal/util/FastXmlSerializer;->append(Ljava/lang/String;)V

    #@17
    .line 194
    const/16 v0, 0x3a

    #@19
    invoke-direct {p0, v0}, Lcom/android/internal/util/FastXmlSerializer;->append(C)V

    #@1c
    .line 196
    :cond_1c
    invoke-direct {p0, p2}, Lcom/android/internal/util/FastXmlSerializer;->append(Ljava/lang/String;)V

    #@1f
    .line 197
    const-string v0, ">\n"

    #@21
    invoke-direct {p0, v0}, Lcom/android/internal/util/FastXmlSerializer;->append(Ljava/lang/String;)V

    #@24
    goto :goto_9
.end method

.method public entityRef(Ljava/lang/String;)V
    .registers 3
    .parameter "text"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 205
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@5
    throw v0
.end method

.method public flush()V
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 219
    iget v2, p0, Lcom/android/internal/util/FastXmlSerializer;->mPos:I

    #@4
    if-lez v2, :cond_46

    #@6
    .line 220
    iget-object v2, p0, Lcom/android/internal/util/FastXmlSerializer;->mOutputStream:Ljava/io/OutputStream;

    #@8
    if-eqz v2, :cond_47

    #@a
    .line 221
    iget-object v2, p0, Lcom/android/internal/util/FastXmlSerializer;->mText:[C

    #@c
    iget v3, p0, Lcom/android/internal/util/FastXmlSerializer;->mPos:I

    #@e
    invoke-static {v2, v5, v3}, Ljava/nio/CharBuffer;->wrap([CII)Ljava/nio/CharBuffer;

    #@11
    move-result-object v0

    #@12
    .line 222
    .local v0, charBuffer:Ljava/nio/CharBuffer;
    iget-object v2, p0, Lcom/android/internal/util/FastXmlSerializer;->mCharset:Ljava/nio/charset/CharsetEncoder;

    #@14
    iget-object v3, p0, Lcom/android/internal/util/FastXmlSerializer;->mBytes:Ljava/nio/ByteBuffer;

    #@16
    invoke-virtual {v2, v0, v3, v4}, Ljava/nio/charset/CharsetEncoder;->encode(Ljava/nio/CharBuffer;Ljava/nio/ByteBuffer;Z)Ljava/nio/charset/CoderResult;

    #@19
    move-result-object v1

    #@1a
    .line 224
    .local v1, result:Ljava/nio/charset/CoderResult;
    :goto_1a
    invoke-virtual {v1}, Ljava/nio/charset/CoderResult;->isError()Z

    #@1d
    move-result v2

    #@1e
    if-eqz v2, :cond_2a

    #@20
    .line 225
    new-instance v2, Ljava/io/IOException;

    #@22
    invoke-virtual {v1}, Ljava/nio/charset/CoderResult;->toString()Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@29
    throw v2

    #@2a
    .line 226
    :cond_2a
    invoke-virtual {v1}, Ljava/nio/charset/CoderResult;->isOverflow()Z

    #@2d
    move-result v2

    #@2e
    if-eqz v2, :cond_3c

    #@30
    .line 227
    invoke-direct {p0}, Lcom/android/internal/util/FastXmlSerializer;->flushBytes()V

    #@33
    .line 228
    iget-object v2, p0, Lcom/android/internal/util/FastXmlSerializer;->mCharset:Ljava/nio/charset/CharsetEncoder;

    #@35
    iget-object v3, p0, Lcom/android/internal/util/FastXmlSerializer;->mBytes:Ljava/nio/ByteBuffer;

    #@37
    invoke-virtual {v2, v0, v3, v4}, Ljava/nio/charset/CharsetEncoder;->encode(Ljava/nio/CharBuffer;Ljava/nio/ByteBuffer;Z)Ljava/nio/charset/CoderResult;

    #@3a
    move-result-object v1

    #@3b
    .line 229
    goto :goto_1a

    #@3c
    .line 233
    :cond_3c
    invoke-direct {p0}, Lcom/android/internal/util/FastXmlSerializer;->flushBytes()V

    #@3f
    .line 234
    iget-object v2, p0, Lcom/android/internal/util/FastXmlSerializer;->mOutputStream:Ljava/io/OutputStream;

    #@41
    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V

    #@44
    .line 239
    .end local v0           #charBuffer:Ljava/nio/CharBuffer;
    .end local v1           #result:Ljava/nio/charset/CoderResult;
    :goto_44
    iput v5, p0, Lcom/android/internal/util/FastXmlSerializer;->mPos:I

    #@46
    .line 241
    :cond_46
    return-void

    #@47
    .line 236
    :cond_47
    iget-object v2, p0, Lcom/android/internal/util/FastXmlSerializer;->mWriter:Ljava/io/Writer;

    #@49
    iget-object v3, p0, Lcom/android/internal/util/FastXmlSerializer;->mText:[C

    #@4b
    iget v4, p0, Lcom/android/internal/util/FastXmlSerializer;->mPos:I

    #@4d
    invoke-virtual {v2, v3, v5, v4}, Ljava/io/Writer;->write([CII)V

    #@50
    .line 237
    iget-object v2, p0, Lcom/android/internal/util/FastXmlSerializer;->mWriter:Ljava/io/Writer;

    #@52
    invoke-virtual {v2}, Ljava/io/Writer;->flush()V

    #@55
    goto :goto_44
.end method

.method public getDepth()I
    .registers 2

    #@0
    .prologue
    .line 244
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@5
    throw v0
.end method

.method public getFeature(Ljava/lang/String;)Z
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 248
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@5
    throw v0
.end method

.method public getName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 252
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@5
    throw v0
.end method

.method public getNamespace()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 256
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@5
    throw v0
.end method

.method public getPrefix(Ljava/lang/String;Z)Ljava/lang/String;
    .registers 4
    .parameter "namespace"
    .parameter "generatePrefix"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 261
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@5
    throw v0
.end method

.method public getProperty(Ljava/lang/String;)Ljava/lang/Object;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 265
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@5
    throw v0
.end method

.method public ignorableWhitespace(Ljava/lang/String;)V
    .registers 3
    .parameter "text"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 270
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@5
    throw v0
.end method

.method public processingInstruction(Ljava/lang/String;)V
    .registers 3
    .parameter "text"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 275
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@5
    throw v0
.end method

.method public setFeature(Ljava/lang/String;Z)V
    .registers 4
    .parameter "name"
    .parameter "state"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 280
    const-string v0, "http://xmlpull.org/v1/doc/features.html#indent-output"

    #@2
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_9

    #@8
    .line 281
    return-void

    #@9
    .line 283
    :cond_9
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@b
    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@e
    throw v0
.end method

.method public setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V
    .registers 5
    .parameter "os"
    .parameter "encoding"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 288
    if-nez p1, :cond_8

    #@2
    .line 289
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@7
    throw v1

    #@8
    .line 292
    :cond_8
    :try_start_8
    invoke-static {p2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v1}, Ljava/nio/charset/Charset;->newEncoder()Ljava/nio/charset/CharsetEncoder;

    #@f
    move-result-object v1

    #@10
    iput-object v1, p0, Lcom/android/internal/util/FastXmlSerializer;->mCharset:Ljava/nio/charset/CharsetEncoder;
    :try_end_12
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_8 .. :try_end_12} :catch_15
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_8 .. :try_end_12} :catch_24

    #@12
    .line 300
    iput-object p1, p0, Lcom/android/internal/util/FastXmlSerializer;->mOutputStream:Ljava/io/OutputStream;

    #@14
    .line 307
    return-void

    #@15
    .line 293
    :catch_15
    move-exception v0

    #@16
    .line 294
    .local v0, e:Ljava/nio/charset/IllegalCharsetNameException;
    new-instance v1, Ljava/io/UnsupportedEncodingException;

    #@18
    invoke-direct {v1, p2}, Ljava/io/UnsupportedEncodingException;-><init>(Ljava/lang/String;)V

    #@1b
    invoke-virtual {v1, v0}, Ljava/io/UnsupportedEncodingException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    #@1e
    move-result-object v1

    #@1f
    check-cast v1, Ljava/io/UnsupportedEncodingException;

    #@21
    check-cast v1, Ljava/io/UnsupportedEncodingException;

    #@23
    throw v1

    #@24
    .line 296
    .end local v0           #e:Ljava/nio/charset/IllegalCharsetNameException;
    :catch_24
    move-exception v0

    #@25
    .line 297
    .local v0, e:Ljava/nio/charset/UnsupportedCharsetException;
    new-instance v1, Ljava/io/UnsupportedEncodingException;

    #@27
    invoke-direct {v1, p2}, Ljava/io/UnsupportedEncodingException;-><init>(Ljava/lang/String;)V

    #@2a
    invoke-virtual {v1, v0}, Ljava/io/UnsupportedEncodingException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    #@2d
    move-result-object v1

    #@2e
    check-cast v1, Ljava/io/UnsupportedEncodingException;

    #@30
    check-cast v1, Ljava/io/UnsupportedEncodingException;

    #@32
    throw v1
.end method

.method public setOutput(Ljava/io/Writer;)V
    .registers 2
    .parameter "writer"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 311
    iput-object p1, p0, Lcom/android/internal/util/FastXmlSerializer;->mWriter:Ljava/io/Writer;

    #@2
    .line 312
    return-void
.end method

.method public setPrefix(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "prefix"
    .parameter "namespace"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 316
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@5
    throw v0
.end method

.method public setProperty(Ljava/lang/String;Ljava/lang/Object;)V
    .registers 4
    .parameter "name"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 321
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@5
    throw v0
.end method

.method public startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V
    .registers 5
    .parameter "encoding"
    .parameter "standalone"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 326
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "<?xml version=\'1.0\' encoding=\'utf-8\' standalone=\'"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_26

    #@11
    const-string/jumbo v0, "yes"

    #@14
    :goto_14
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    const-string v1, "\' ?>\n"

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v0

    #@22
    invoke-direct {p0, v0}, Lcom/android/internal/util/FastXmlSerializer;->append(Ljava/lang/String;)V

    #@25
    .line 328
    return-void

    #@26
    .line 326
    :cond_26
    const-string/jumbo v0, "no"

    #@29
    goto :goto_14
.end method

.method public startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    .registers 4
    .parameter "namespace"
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 332
    iget-boolean v0, p0, Lcom/android/internal/util/FastXmlSerializer;->mInTag:Z

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 333
    const-string v0, ">\n"

    #@6
    invoke-direct {p0, v0}, Lcom/android/internal/util/FastXmlSerializer;->append(Ljava/lang/String;)V

    #@9
    .line 335
    :cond_9
    const/16 v0, 0x3c

    #@b
    invoke-direct {p0, v0}, Lcom/android/internal/util/FastXmlSerializer;->append(C)V

    #@e
    .line 336
    if-eqz p1, :cond_18

    #@10
    .line 337
    invoke-direct {p0, p1}, Lcom/android/internal/util/FastXmlSerializer;->append(Ljava/lang/String;)V

    #@13
    .line 338
    const/16 v0, 0x3a

    #@15
    invoke-direct {p0, v0}, Lcom/android/internal/util/FastXmlSerializer;->append(C)V

    #@18
    .line 340
    :cond_18
    invoke-direct {p0, p2}, Lcom/android/internal/util/FastXmlSerializer;->append(Ljava/lang/String;)V

    #@1b
    .line 341
    const/4 v0, 0x1

    #@1c
    iput-boolean v0, p0, Lcom/android/internal/util/FastXmlSerializer;->mInTag:Z

    #@1e
    .line 342
    return-object p0
.end method

.method public text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    .registers 3
    .parameter "text"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 357
    iget-boolean v0, p0, Lcom/android/internal/util/FastXmlSerializer;->mInTag:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 358
    const-string v0, ">"

    #@6
    invoke-direct {p0, v0}, Lcom/android/internal/util/FastXmlSerializer;->append(Ljava/lang/String;)V

    #@9
    .line 359
    const/4 v0, 0x0

    #@a
    iput-boolean v0, p0, Lcom/android/internal/util/FastXmlSerializer;->mInTag:Z

    #@c
    .line 361
    :cond_c
    invoke-direct {p0, p1}, Lcom/android/internal/util/FastXmlSerializer;->escapeAndAppendString(Ljava/lang/String;)V

    #@f
    .line 362
    return-object p0
.end method

.method public text([CII)Lorg/xmlpull/v1/XmlSerializer;
    .registers 5
    .parameter "buf"
    .parameter "start"
    .parameter "len"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 347
    iget-boolean v0, p0, Lcom/android/internal/util/FastXmlSerializer;->mInTag:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 348
    const-string v0, ">"

    #@6
    invoke-direct {p0, v0}, Lcom/android/internal/util/FastXmlSerializer;->append(Ljava/lang/String;)V

    #@9
    .line 349
    const/4 v0, 0x0

    #@a
    iput-boolean v0, p0, Lcom/android/internal/util/FastXmlSerializer;->mInTag:Z

    #@c
    .line 351
    :cond_c
    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/util/FastXmlSerializer;->escapeAndAppendString([CII)V

    #@f
    .line 352
    return-object p0
.end method
